<?php
/*+**********************************************************************************
 * The contents of this file are subject to the GCM Factory System License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  GCM Factory System
 * The Initial Developer of the Original Code is GCM Factory System.
 * Portions created by vtiger are Copyright (C) GCM Factory System.
 * All Rights Reserved.
 ************************************************************************************/
 /*
 * CRM Site URL
 */ 
$sugarurl = 'https://qa.devgcm-system.com/';

 /*
 * CRM API URL
 */ 
define('SUGARURL', $sugarurl.'service/v4/rest.php');
 /*
 * Site URL
 */

define('OAUTH_CONSUMER_KEY','GCMQAOAuthCloudProtal');
define('OAUTH_CONSUMER_SECRET','GCMQAOAuthCloudProtal_1');
define('OAUTH_TMP_DIR', function_exists('sys_get_temp_dir') ? sys_get_temp_dir() : realpath($_ENV["TMP"]));
?>