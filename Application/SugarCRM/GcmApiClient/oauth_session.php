<?php
$settingFile = dirname(__FILE__).'/token/'.gethostname().'.php';
require_once($settingFile);

$oauthObj = new OAuth(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
$oauthObj->setToken($tokenID, $secretID);
$oauthObj->disableSSLChecks();
$data = $oauthObj->fetch(SUGARURL."?method=oauth_access&input_type=JSON&request_type=JSON&response_type=JSON");
$responseInfo = $oauthObj->getLastResponseInfo();
$responseData = json_decode($oauthObj->getLastResponse());

//print_r($responseData); // Debug

$sessionId = $responseData->id;

header('Content-type: text/plain');
echo $sessionId;