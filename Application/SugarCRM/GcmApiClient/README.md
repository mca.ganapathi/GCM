# Client Tools for GCM-API.
This tool is a client for calling the GCM API.
It is a simple implementation that receives API parameters in HTML (Form) and performs API call processing with JavaScript.

## List of tools
1. For V1-API
  - contract-information_post.html  -> POST /api/v1/contract-information
  - contract-information_get.html -> GET /api/v1/contract-information/${objectId}
  - contract-information_get2.html -> GET /api/v1/contract-information?contractID=${contractId}
  - contract-information_delete.html -> DELETE /api/v1/contract-information/${objectId}
  - contract-information_delete2.html -> DELETE /api/v1/contract-information?contractID=${contractId}
  - contract-information_patch.html -> PATCH /api/v1/contract-information/${objectId}
  - contract-information_patch2.html -> PATCH /api/v1/contract-information?contractID=${contractId}
  - contract-information_put.html -> PUT /api/v1/contract-information/${objectId}
1. For V2-API
  - v2_contract-information_post.html -> POST /api/v2/contract-information/${objectId}:${objectVersion}
  - v2_contract-information_post_search.html -> POST /api/v2/contract-information/search?...
  - v2_contract-information_get.html -> GET /api/v2/contract-information/${objectId}:${objectVersion}
  - v2_contract-information_get_search.html -> GET /api/v2/contract-information/search?...
  - v2_contract-information_delete.html -> DELETE /api/v2/contract-information/${objectId}:${objectVersion}
  - v2_contract-information_patch.html -> PATCH /api/v2/contract-information/${objectId}:${objectVersion}
  - v2_contract-information_patch_search.html -> PATCH /api/v2/contract-information/search?...
  - v2_export-csv-for-gbs.html -> GET /api/v2/export-csv-for-gbs?...
  - v2_views_list-of-product-contracts.html -> GET /api/v2/views/list-of-product-contracts?...
  - v2_contract-information_head.html -> HEAD /api/v2/contract-information/${objectId}:${objectVersion}
  - v2_contract-information_put.html -> PUT /api/v2/contract-information/${objectId}:${objectVersion}
1. Common
  - oauth_session.php : This script requests session-id using AccessToken. (Used by html above.)
  - token/${HostName of WebAP Server}.php : This script defines issued AccessToken for each environment.

## Tool execution (Usage)
1. Deploy a set of GcmApiClient folders to the public area of the WebAP server.(i.e. /var/www/html/)
1. Access to the HTML of the API you want to call by web browser.  
e.g. https://www.testgcm-system.com/GcmApiClient/v2_contract-information_get.html
1. Enter parameters required for API call and press "Submit" button.
1. Result from API call is displayed into the lower part.


## Supplementary explanation
Since this tool contains confidential information necessary to call the API (e.g. ConsumerKey, AccessToken), you should not deploy it usually.  
Please deploy it temporarily for checking the API on release. **And be sure to delete it after checking.**
