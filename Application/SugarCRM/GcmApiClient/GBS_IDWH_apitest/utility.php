<?php
//============================
// Common(Utility) Functions
//============================
/**
 * Connect to Database(MySQL).
 * @param string $dbHost Host name
 * @param string $dbName DB name
 * @param string $dbUser User name
 * @param string $dbPass Password
 * @param string $charset CharacterSet(Default: UTF-8)
 * @return Instance of PDO
 */
function connectToDatabase($dbHost, $dbName, $dbUser, $dbPass, $charset='utf8') {
	$dataSource = 'mysql:host=' . $dbHost . ';dbname=' . $dbName;
	$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $charset);
	return new PDO($dataSource, $dbUser, $dbPass, $options);
}

/**
 * Get Element's value by XPath from XML(SimpleXMLElement)
 * @param SimpleXMLElement $sxe
 * @param string $xpath
 * @return NULL|string
 */
function getElementValueByXpath($sxe, $xpath) {
	$elements = $sxe->xpath($xpath);
	if(empty($elements)) {
		return null;
	}
	return (string)$elements[0];
}

/**
 * Checks the string ends with suffix or not.
 * @param string $str     String to be checked
 * @param string $suffix  Suffix
 * @return boolean true: ends with suffix / false: not ends with suffix
 */
function endsWith($str, $suffix)
{
	$length = strlen($suffix);
	if ($length == 0) {
		return true;
	}
	return (substr($str, -$length) === $suffix);
}
