<?php
error_reporting(E_ALL);
ini_set('max_execution_time', 0);
ini_set('display_errors', 1);
require("config.php");
// Has the Offering XML File path and API URL's
require_once("utility.php");
// Has the Common(Utility) Functions
require_once("../../config_override.php");
// Has the DB Configuration
echo "Execution Started : ".date('Ymd_His')."<br>";
$outputString = "";
$finalString = "";
// String variables that will hold the output string which will be written to the log file
$failedSummary = array();
$i=0;
$counter = 1;
$csvArray = array();
$sessionID = "";
$finalString = "Execution Started : " . date('Ymd_His')."\n\nFailed Scenarios: \n";
try{
    $logFile = fopen("/var/log/sugarcrm/reg_idwh_api_".date('Ymd_His')."_".$sugar_config['dbconfig']['db_host_name'].".log","w");
}
catch(Exception $ex){
    die("Could not create Logfile.");
}
$csvfileName = "parameters_idwh.csv";
// parameters_idwh.csv holds the paramters
$connectionInstance      = connectToDatabase($sugar_config['dbconfig']['db_host_name'],
                                             $sugar_config['dbconfig']['db_name'],
                                             $sugar_config['dbconfig']['db_user_name'],
                                             $sugar_config['dbconfig']['db_password']);
// $connectionInstance will have the instance of PDO returned from the connectToDatabase Function

$session = curl_init();
curl_setopt($session, CURLOPT_URL,$sessionURL);
curl_setopt($session,CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($session, CURLOPT_RETURNTRANSFER, 1);
$sessionID = curl_exec($session);
if (trim($sessionID) == "") {
    fwrite($logFile,"Session ID is not Generated");
    fclose($logFile);
    echo "<br> Execution Completed";
    exit;
}
// Execution will stop if session ID is not generated
curl_close($session);

if(file_exists ($csvfileName)){
    $file = fopen($csvfileName, "r");
}else{
    echo "<br> Execution Completed";
    die(fwrite($logFile, "Paramters file not found. Please check the file or Filename."));
}
//Checking for the availabilty of parameters_idwh.csv file

while (!feof($file)){
    // Based on the number of executions provided in the Parameters.csv while loop will be executed
    $apiResponsearray = array();
	$dbResponsearray  = array();
	$responsearray = array();
	$statusArrayAPI = array();
	$statusArrayDB = array();
	$responsearray = array();
	$param = "";
	$query = "";
	$param_simple = "";
	$csvArray = fgetcsv($file,',');
	if(isset($flag)){
	    unset($flag);
	}
	// $flag is to check whether any other paramter has been added to API URL.
	if ($i!=0)
	{
	    //$i will be zero for the first iteration so as to skip the first row of the CSV file since, the first row will contain CSV header.
	    if(strcasecmp($csvArray[6],'y') == 0)
	    {
	        // Execution will be done only if the value is y
    		if(trim($csvArray[1])!=""){
    			$query = $query .' AND acstm.common_customer_id_c = '."'".trim($csvArray[1])."'".'';
    			$param = $param .'contract-customer-id='.trim($csvArray[1]).'';
    			$flag = 0;
    		}
    		if(trim($csvArray[2])!=""){
    			$poffering = ltrim(trim($csvArray[2]),'0,-');
    			$poffering = explode (":", $poffering); 
    			$query = $query .' AND contracts.product_offering = '."'".$poffering[0]."'".'';
    			if(isset($flag)){
    				$param = $param .'&contract-product-id='.trim($csvArray[2]).'';
    			}else{
    				$param = $param .'contract-product-id='.trim($csvArray[2]).'';
    			}
    			$flag = 0;
    		}
    		if(trim($csvArray[3])!=""){
    			$apiDT = $csvArray[3];
    			$apiDT = date_create($apiDT);
    			$csvArray[3] = explode(" ",trim($csvArray[3]));
    			$timeZone = new DateTimeZone($csvArray[3][2]);
    			$date = new DateTime($csvArray[3][0]." ".$csvArray[3][1], $timeZone);
    			$date->setTimeZone(new DateTimeZone('UTC'));
    			// Converting time and date to UTC from the time zone which is in the parameters_idwh.csv
    			$dateTime = $date->format('Y-m-d H:i:s');
    			$query = $query .' And (contracts.date_modified >='."'".$dateTime."'".' OR accnts.date_modified >='."'".$dateTime."'".' OR cntcts.date_modified >='."'".$dateTime."'".')';
    			if(isset($flag)){
    				$param = $param .'&updated-after='.$csvArray[3][0]."T".$csvArray[3][1].$csvArray[3][2].'';
    			}else{
    				$param = $param .'updated-after='.$csvArray[3][0]."T".$csvArray[3][1].$csvArray[3][2].'';
    			}
    			$flag = 0;
    		}
    		if(trim($csvArray[4])!=""){
    			$statusArrayAPI = explode(",",$csvArray[4]);
    			for($c=0;$c<count($statusArrayAPI);$c++){
    					if(strcasecmp($statusArrayAPI[$c],'Draft') == 0){
    						$statusArrayDB[$c] = 0;
    					}elseif(strcasecmp($statusArrayAPI[$c],'Approved') == 0){
    						$statusArrayDB[$c] = 1;
    					}elseif(strcasecmp($statusArrayAPI[$c],'Deactivated') == 0){
    						$statusArrayDB[$c] = 2;
    					}elseif(strcasecmp($statusArrayAPI[$c],'Sent for Approval') == 0){
    						$statusArrayDB[$c] = 3;
    					}elseif(strcasecmp($statusArrayAPI[$c],'Activated') == 0){
    						$statusArrayDB[$c] = 4;
    					}elseif(strcasecmp($statusArrayAPI[$c],'Sent for Termination Approval') == 0){
    						$statusArrayDB[$c] = 5;
    					}elseif(strcasecmp($statusArrayAPI[$c],'Termination Approved') == 0){
    						$statusArrayDB[$c] = 6;
    					}elseif(strcasecmp($statusArrayAPI[$c],'Upgraded') == 0){
    						$statusArrayDB[$c] = 7;
    					}else{
							continue;
    					}
    				}
    				if(isset($flag)){
    				    $param = $param .'&status='.urlencode(implode(",",$statusArrayAPI)).'';
    				}else{
    				    $param = $param .'status='.urlencode(implode(",",$statusArrayAPI)).'';
    				}
    				$query = $query .'AND contracts.contract_status IN("' . implode('", "', $statusArrayDB) . '")';
    				$flag = 0;
    		}
    		if(trim($csvArray[5])!=""){
    		    if(isset($flag)){
    			    $param = $param .'&all-versions';
    		    }else{
    		        $param = $param .'all-versions';
    		    }
    			$sql = "SELECT contracts.global_contract_id_uuid,contracts.contract_version FROM gc_contracts AS contracts
    LEFT JOIN accounts_gc_contracts_1_c AS agcontracts ON agcontracts.accounts_gc_contracts_1gc_contracts_idb = contracts.id AND agcontracts.deleted = 0
    LEFT JOIN accounts AS accnts ON accnts.id = agcontracts.accounts_gc_contracts_1accounts_ida AND accnts.deleted = 0
    LEFT JOIN accounts_cstm AS acstm ON acstm.id_c = accnts.id
    LEFT JOIN contacts_gc_contracts_1_c AS cgcontracts ON cgcontracts.contacts_gc_contracts_1gc_contracts_idb = contracts.id AND cgcontracts.deleted = 0
    LEFT JOIN contacts AS cntcts ON cntcts.id = cgcontracts.contacts_gc_contracts_1contacts_ida AND cntcts.deleted = 0
    WHERE
    contracts.deleted = 0  AND contracts.global_contract_id_uuid IS NOT NULL AND contracts.contract_version IS NOT NULL $query ORDER BY contracts.global_contract_id ASC,contracts.contract_version ASC";
    		}else{
    			$sql = "SELECT
 contracts.global_contract_id_uuid, contracts.contract_version 
FROM gc_contracts AS contracts
 LEFT JOIN accounts_gc_contracts_1_c AS agcontracts ON agcontracts.accounts_gc_contracts_1gc_contracts_idb = contracts.id AND agcontracts.deleted = 0
 LEFT JOIN accounts AS accnts ON accnts.id = agcontracts.accounts_gc_contracts_1accounts_ida AND accnts.deleted = 0
 LEFT JOIN accounts_cstm AS acstm ON acstm.id_c = accnts.id
 LEFT JOIN contacts_gc_contracts_1_c AS cgcontracts ON cgcontracts.contacts_gc_contracts_1gc_contracts_idb = contracts.id AND cgcontracts.deleted = 0
 LEFT JOIN contacts AS cntcts ON cntcts.id = cgcontracts.contacts_gc_contracts_1contacts_ida AND cntcts.deleted = 0
    WHERE
    contracts.deleted = 0  AND contracts.global_contract_id_uuid IS NOT NULL AND contracts.contract_version IS NOT NULL $query AND contracts.contract_version = (SELECT MAX(gc.contract_version) FROM gc_contracts gc WHERE gc.deleted = 0 AND gc.global_contract_id = contracts.global_contract_id)
ORDER BY contracts.global_contract_id ASC,contracts.contract_version ASC";
    		}
    		$apicall = curl_init();
    		curl_setopt($apicall, CURLOPT_URL,$baseURL_IDWH . $param);
    		curl_setopt($apicall, CURLOPT_HTTPHEADER, array(
                                                'Content-Type: application/xml',
                                                'Connection: Keep-Alive'
                                                ));
    		curl_setopt($apicall,CURLOPT_SSL_VERIFYPEER, false);
    		curl_setopt($apicall, CURLOPT_RETURNTRANSFER, 1);
    		curl_setopt($apicall,CURLOPT_HTTPHEADER,array("session-id: $sessionID"));
    		$response = curl_exec($apicall);
    		$responseCode = curl_getinfo($apicall, CURLINFO_HTTP_CODE);
    		curl_close($apicall);
            if ($responseCode != 200) {
                $outputString = $outputString . "\n". $csvArray[0] ."\n\n" .$baseURL_IDWH . $param ."\n\nStatus: Fail\n\n" . $responseCode . "\n" . $response;
				array_push($failedSummary,$csvArray[0]);
                continue;
            }
            // If API response is negative. Response is written to the log file and this iteration is skipped.
    		$i=0;
    		$dom = new DOMDocument;
    		if($response){
    		$dom->loadXML($response);
    		$contracts = $dom->getElementsByTagNameNS('https://gcm.ntt.com/api/xml-schema/v2/system-specific/gcm/gcm-lopc','product-contract');
    		foreach ($contracts as $contract) {
    			$responsearray = explode(':',$contract->getAttribute('id'));
    			$apiResponsearray[$i]['global_contract_id_uuid']=$responsearray[0];
    			$apiResponsearray[$i]['contract_version'] = $responsearray[1];
    			$i++;
    		}}
    		try{
                $retval = $connectionInstance->query($sql);
    	    }
    	catch(PDOException $e) {
         $outputString = $outputString."\n" .$csvArray[0]."\n\nStatus: Failed\n".$e->getMessage();
         array_push($failedSummary,$csvArray[0]);
    	 continue;
    	}
    	// If there is any issue during query execution the error is appended to the output string and this iteration is skipped.
        while($fetch = $retval->fetch(PDO::FETCH_ASSOC)){
    		$fetch['contract_version'] = rtrim($fetch['contract_version'],'.0');
            array_push($dbResponsearray, $fetch);
        }
        $outputString = $outputString . "\n\n" . $csvArray[0] . "\n";
        $outputString = $outputString . "\n" . $baseURL_IDWH . $param . "\n";
        $outputString = $outputString . "\nSummary \n";
        $outputString = $outputString . "TotalCount in DB Results: " . count($dbResponsearray) . "\n";
        $outputString = $outputString . "TotalCount in API Response: " . count($apiResponsearray) . "\n";
        if (count($dbResponsearray) > 0 || count($apiResponsearray) > 0) {
    		$apibuffer = $apiResponsearray;
    		$dbbuffer = $dbResponsearray;
    		for($j=0;$j<count($dbResponsearray);$j++)
    		{
    			for($i=0;$i<count($apiResponsearray);$i++)
    			{
    				if($dbResponsearray[$j]['global_contract_id_uuid'] == $apiResponsearray[$i]['global_contract_id_uuid'] && $dbResponsearray[$j]['contract_version'] == $apiResponsearray[$i]['contract_version']){
    					unset($apibuffer[$i],$dbbuffer[$j]);
    				}
    			}
    		}
    		if(count($apibuffer)>0 || count($dbbuffer)>0){
    		    $outputString = $outputString . "Available in API Response but not in DB Results: " . count($apibuffer) . "\n";
    		    $outputString = $outputString . "Available in DB Results but not in API Response: " . count($dbbuffer) . "\n";
    		    array_push($failedSummary,$csvArray[0]);
    			$outputString = $outputString . " \nStatus : Fail\n";
    			if (count($dbbuffer) > 0) {
                    $outputString = $outputString . "\nAvailable in DB Results but not in API Response\n\n";
                    foreach ($dbbuffer as $key) {
    					$outputString = $outputString.$counter.". ";
                        foreach ($key as $a => $value) {
                                $outputString = $outputString .$a . " => " .$value. " || ";
                        }
    					$counter++;
    					$outputString = $outputString."\n";
                    }
                }
    			$counter=1;
                if (count($apibuffer) > 0) {
                    $outputString = $outputString . "\nAvailable in API Response but not in DB Results\n\n";
                    foreach ($apibuffer as $key) {
    					$outputString = $outputString.$counter.". ";
                        foreach ($key as $a => $value) {
                                $outputString = $outputString . $a . " => " . $value . " || ";
                        }
    					$counter++;
    					$outputString = $outputString."\n";
                    }
                }
    			$counter=1;
    		}else{
    		    $outputString = $outputString . "\nStatus : Pass\n";
    		    }
    	}else{
    	    $outputString = $outputString . "\nNo Records Available for the given parameters.\n";
    	}
    		
    	}elseif(strcasecmp($csvArray[6],'n')==0){
			 $outputString = $outputString ."\n". $csvArray[0] . "\n\nTest Case Skipped\n";
		}else{
		    $outputString = $outputString ."\n". $csvArray[0] . "\n\nInvalid Execution value. It should only be 'Y' or 'N'\n";
		    array_push($failedSummary,$csvArray[0]);
		}
		// After every iteration, the result of that particular iterartion is appended to $outputString which will be written to the log file after all the iteration
	}
	$i=1;
	// After first iteration $1 will be 1
}
$outputString = $outputString . "\n\nExecution Completed : " . date('Ymd_His');
$finalString = $finalString.implode(",", $failedSummary);
$finalString = $finalString ."\n". $outputString;
fwrite($logFile, $finalString);
// After executing the complete suite output is written to the log file.
fclose($logFile);
echo "<br>Execution completed : ".date('Ymd_His');
?>