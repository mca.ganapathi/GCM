/**
 * Common functions for GCM API Client tool.
 */
function getSessionId() {
  var req = new XMLHttpRequest();
  if (req) {
    // executes http-request
    req.open('GET', './oauth_session.php', false);
    req.send(null);
    return req.responseText;
  }
  return "";
}