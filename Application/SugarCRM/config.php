<?php
// created: 2018-03-07 11:46:22
$sugar_config = array (
  'SAML_X509Cert' => '',
  'SAML_loginurl' => '',
  'additional_js_config' => 
  array (
  ),
  'admin_access_control' => false,
  'admin_export_only' => true,
  'beluga_export_path' => '/var/export/beluga',
  'cache_dir' => 'cache/',
  'calculate_response_time' => true,
  'calendar' => 
  array (
    'default_view' => 'week',
    'show_calls_by_default' => true,
    'show_tasks_by_default' => true,
    'show_completed_by_default' => true,
    'editview_width' => 990,
    'editview_height' => 485,
    'day_timestep' => 15,
    'week_timestep' => 30,
    'items_draggable' => true,
    'items_resizable' => true,
    'enable_repeat' => true,
    'max_repeat_count' => 1000,
  ),
  'chartEngine' => 'nvd3',
  'cidas' => 
  array (
    'user' => 'GCMTEST',
    'passcode' => 'cidasapp',
    'proxy_url' => 'tcp://dev-gcm-proxy:8888',
    'url' => 'https://dev-gcm-stub/',
  ),
  'collapse_subpanels' => false,
  'common_ml_dir' => '',
  'create_default_user' => false,
  'cron' => 
  array (
    'max_cron_jobs' => 10,
    'max_cron_runtime' => 1800,
    'min_cron_interval' => 30,
    'enforce_runtime' => false,
  ),
  'cstm_default_timezone' => 
  array (
    0 => '4131828e-0b23-4fb1-ac2c-13b92b111079',
    1 => '4131828e-0b23-4fb1-ac2c-13b92b111079',
    3 => '4131828e-0b23-4fb1-ac2c-13b92b111079',
    5 => '4131828e-0b23-4fb1-ac2c-13b92b111079',
  ),
  'currency' => '',
  'dashlet_auto_refresh_min' => '60',
  'dashlet_display_row_options' => 
  array (
    0 => '1',
    1 => '3',
    2 => '5',
    3 => '10',
  ),
  'date_formats' => 
  array (
    'Y-m-d' => '2010-12-23',
    'm-d-Y' => '12-23-2010',
    'd-m-Y' => '23-12-2010',
    'Y/m/d' => '2010/12/23',
    'm/d/Y' => '12/23/2010',
    'd/m/Y' => '23/12/2010',
    'Y.m.d' => '2010.12.23',
    'd.m.Y' => '23.12.2010',
    'm.d.Y' => '12.23.2010',
  ),
  'datef' => 'm/d/Y',
  'dbconfig' => 
  array (
    'db_host_name' => 'dev-gcm-db41',
    'db_host_instance' => 'SQLEXPRESS',
    'db_user_name' => 'sugarcrm',
    'db_password' => 'QYqKQxeWYR',
    'db_name' => 'sugarcrm',
    'db_type' => 'mysql',
    'db_port' => '',
    'db_manager' => 'MysqliManager',
    'db_slave_host_name' => 'dev-gcm-db41',
  ),
  'dbconfigoption' => 
  array (
    'persistent' => true,
    'autofree' => false,
    'debug' => 0,
    'ssl' => false,
    'collation' => 'utf8_general_ci',
  ),
  'default_action' => 'index',
  'default_charset' => 'UTF-8',
  'default_currencies' => 
  array (
    'AUD' => 
    array (
      'name' => 'Australian Dollars',
      'iso4217' => 'AUD',
      'symbol' => '$',
    ),
    'BRL' => 
    array (
      'name' => 'Brazilian Reais',
      'iso4217' => 'BRL',
      'symbol' => 'R$',
    ),
    'GBP' => 
    array (
      'name' => 'British Pounds',
      'iso4217' => 'GBP',
      'symbol' => '£',
    ),
    'CAD' => 
    array (
      'name' => 'Canadian Dollars',
      'iso4217' => 'CAD',
      'symbol' => '$',
    ),
    'CNY' => 
    array (
      'name' => 'Chinese Yuan',
      'iso4217' => 'CNY',
      'symbol' => '￥',
    ),
    'EUR' => 
    array (
      'name' => 'Euro',
      'iso4217' => 'EUR',
      'symbol' => '€',
    ),
    'HKD' => 
    array (
      'name' => 'Hong Kong Dollars',
      'iso4217' => 'HKD',
      'symbol' => '$',
    ),
    'INR' => 
    array (
      'name' => 'Indian Rupees',
      'iso4217' => 'INR',
      'symbol' => '₨',
    ),
    'KRW' => 
    array (
      'name' => 'Korean Won',
      'iso4217' => 'KRW',
      'symbol' => '₩',
    ),
    'YEN' => 
    array (
      'name' => 'Japanese Yen',
      'iso4217' => 'JPY',
      'symbol' => '¥',
    ),
    'MXM' => 
    array (
      'name' => 'Mexican Pesos',
      'iso4217' => 'MXM',
      'symbol' => '$',
    ),
    'SGD' => 
    array (
      'name' => 'Singaporean Dollars',
      'iso4217' => 'SGD',
      'symbol' => '$',
    ),
    'CHF' => 
    array (
      'name' => 'Swiss Franc',
      'iso4217' => 'CHF',
      'symbol' => 'SFr.',
    ),
    'THB' => 
    array (
      'name' => 'Thai Baht',
      'iso4217' => 'THB',
      'symbol' => '฿',
    ),
    'USD' => 
    array (
      'name' => 'US Dollars',
      'iso4217' => 'USD',
      'symbol' => '$',
    ),
  ),
  'default_currency_iso4217' => 'USD',
  'default_currency_name' => 'US Dollars',
  'default_currency_show_preferred' => false,
  'default_currency_significant_digits' => 2,
  'default_currency_symbol' => '$',
  'default_date_format' => 'm/d/Y',
  'default_decimal_seperator' => '.',
  'default_email_charset' => 'UTF-8',
  'default_email_client' => 'sugar',
  'default_email_editor' => 'html',
  'default_export_charset' => 'UTF-8',
  'default_language' => 'en_us',
  'default_locale_name_format' => 's f l',
  'default_max_tabs' => '7',
  'default_module' => 'Home',
  'default_module_favicon' => false,
  'default_navigation_paradigm' => 'gm',
  'default_number_grouping_seperator' => ',',
  'default_password' => '',
  'default_permissions' => 
  array (
    'dir_mode' => 1528,
    'file_mode' => 432,
    'user' => '',
    'group' => '',
  ),
  'default_sequence_start' => 
  array (
    'pi_product_item' => 100001,
    'gc_Contracts' => '910000000000001',
    'gc_LineItem' => 100001,
    'pm_Products' => 100001,
  ),
  'default_subpanel_links' => false,
  'default_subpanel_tabs' => true,
  'default_swap_last_viewed' => false,
  'default_swap_shortcuts' => false,
  'default_theme' => 'RacerX',
  'default_time_format' => 'h:ia',
  'default_user_is_admin' => false,
  'default_user_name' => '',
  'demoData' => 'no',
  'developerMode' => true,
  'diagnostic_file_max_lifetime' => 604800,
  'disable_convert_lead' => false,
  'disable_export' => true,
  'disable_persistent_connections' => 'false',
  'disable_unknown_platforms' => true,
  'disabled_languages' => 'bg_BG,cs_CZ,da_DK,de_DE,es_ES,fr_FR,he_IL,hu_HU,it_it,lt_LT,lv_LV,nb_NO,nl_NL,pl_PL,pt_PT,ro_RO,ru_RU,sv_SE,tr_TR,zh_CN,pt_BR,ca_ES,en_UK,sr_RS',
  'display_email_template_variable_chooser' => false,
  'display_inbound_email_buttons' => false,
  'dump_slow_queries' => true,
  'email_address_separator' => ',',
  'email_default_client' => 'sugar',
  'email_default_delete_attachments' => true,
  'email_default_editor' => 'html',
  'email_mailer_timeout' => 10,
  'enable_action_menu' => false,
  'enable_legacy_dashboards' => true,
  'enable_mobile_redirect' => true,
  'export_delimiter' => ',',
  'export_excel_compatible' => false,
  'full_text_engine' => 
  array (
    'Elastic' => 
    array (
      'host' => '127.0.0.1',
      'port' => '9200',
    ),
  ),
  'gbs_api_po' => 
  array (
    0 => '2808',
    1 => '2809',
    2 => '2808',
    4 => '2808',
    6 => '2808',
  ),
  'gcm_cstm_const' => 
  array (
    'country_code' => 
    array (
      'JPN' => '392',
      'USA' => '840',
      'CAN' => '124',
    ),
    'timezone' => 
    array (
      'gmt' => '+00:00',
    ),
    'default_end_datetime' => '9000-01-01T00:00:00Z',
    'gbs_api_exclude' => 
    array (
      'billing_affiliate_code' => 
      array (
        0 => 'DMY',
      ),
    ),
    'view_api_log' => 
    array (
      'row_limit' => '20',
      'log_db_name' => 'apilog',
    ),
    'email_max_length' => 3,
  ),
  'hide_history_contacts_emails' => 
  array (
    'Cases' => false,
    'Accounts' => false,
    'Opportunities' => true,
  ),
  'history_max_viewed' => 50,
  'host_name' => 'dev41.devgcm-system.com',
  'http_referer' => 
  array (
    'list' => 
    array (
      1 => 'dev21.devgcm-system.com',
      2 => 'appf.cloud-idf.com',
      0 => 'localhost',
    ),
    'actions' => 
    array (
      0 => 'index',
      1 => 'ListView',
      2 => 'DetailView',
      3 => 'EditView',
      4 => 'oauth',
      5 => 'authorize',
      6 => 'Authenticate',
      7 => 'Login',
      8 => 'SupportPortal',
      9 => 'GoogleOauth2Redirect',
      10 => 'deployCustomFields',
      11 => 'index',
      13 => 'index',
      15 => 'index',
    ),
  ),
  'import_max_execution_time' => 3600,
  'import_max_records_per_file' => 100,
  'import_max_records_total_limit' => '',
  'installer_locked' => true,
  'jobs' => 
  array (
    'min_retry_interval' => 30,
    'max_retries' => 5,
    'timeout' => 3600,
  ),
  'js_custom_version' => 1,
  'js_lang_version' => 11,
  'languages' => 
  array (
    'en_us' => 'English (US)',
    'bg_BG' => 'Български',
    'cs_CZ' => 'Česky',
    'da_DK' => 'Dansk',
    'de_DE' => 'Deutsch',
    'es_ES' => 'Español',
    'fr_FR' => 'Français',
    'he_IL' => 'עברית',
    'hu_HU' => 'Magyar',
    'it_it' => 'Italiano',
    'lt_LT' => 'Lietuvių',
    'ja_JP' => '日本語',
    'lv_LV' => 'Latviešu',
    'nb_NO' => 'Bokmål',
    'nl_NL' => 'Nederlands',
    'pl_PL' => 'Polski',
    'pt_PT' => 'Português',
    'ro_RO' => 'Română',
    'ru_RU' => 'Русский',
    'sv_SE' => 'Svenska',
    'tr_TR' => 'Türkçe',
    'zh_CN' => '简体中文',
    'pt_BR' => 'Português Brasileiro',
    'ca_ES' => 'Català',
    'en_UK' => 'English (UK)',
    'sr_RS' => 'Српски',
    'el_EL' => 'Ελληνικά',
    'ko_KR' => '한국어',
    'sk_SK' => 'Slovenčina',
    'sq_AL' => 'Shqip',
    'et_EE' => 'Eesti',
    'es_LA' => 'Español (Latinoamérica)',
    'fi_FI' => 'Suomi',
    'ar_SA' => 'العربية',
    'uk_UA' => 'Українська',
    'zh_TW' => '繁體中文',
    'hr_HR' => 'Hrvatski',
    'th_TH' => 'ไทย',
  ),
  'large_scale_test' => false,
  'lead_conv_activity_opt' => 'donothing',
  'list_max_entries_per_page' => 20,
  'list_max_entries_per_subpanel' => 10,
  'lock_default_user_name' => false,
  'lock_homepage' => false,
  'lock_subpanels' => true,
  'log_dir' => '/var/log/sugarcrm',
  'log_file' => 'sugarcrm.log',
  'log_memory_usage' => false,
  'logger' => 
  array (
    'level' => 'debug',
    'file' => 
    array (
      'ext' => '.log',
      'name' => 'sugarcrm',
      'dateFormat' => '%c',
      'maxSize' => '30MB',
      'maxLogs' => '10',
      'suffix' => '%m_%d_%y',
    ),
  ),
  'mass_actions' => 
  array (
  ),
  'max_dashlets_homepage' => '15',
  'max_record_fetch_size' => 1000,
  'max_record_link_fetch_size' => 5000,
  'merge_duplicates' => 
  array (
    'merge_relate_fetch_concurrency' => 2,
    'merge_relate_fetch_timeout' => 90000,
    'merge_relate_fetch_limit' => 20,
    'merge_relate_update_concurrency' => 4,
    'merge_relate_update_timeout' => 90000,
    'merge_relate_max_attempt' => 3,
  ),
  'meta_tags' => 
  array (
    'IE_COMPAT_MODE' => '<meta http-equiv="X-UA-Compatible" content="IE=11" />',
  ),
  'moduleInstaller' => 
  array (
    'disableFileScan' => true,
  ),
  'name_formats' => 
  array (
    's f l' => 's f l',
    'f l' => 'f l',
    's l' => 's l',
    'l, s f' => 'l, s f',
    'l, f' => 'l, f',
    's l, f' => 's l, f',
    'l s f' => 'l s f',
    'l f s' => 'l f s',
  ),
  'oauth_token_expiry' => 0,
  'oauth_token_life' => 86400,
  'openidconnect' => 
  array (
    'iss' => 'https://pfed04.cloudidf.com',
    'aud' => 'GCMdev41.openidconnect.cloud-idf.com',
    'client_secret' => 'TvcXUVahnk0SNbMfUxpUNbVEA4TZl16tLcb6eYs1CqsCcQgW74i2LsantfHkTrCH',
    'alg' => 'HS256',
  ),
  'passwordsetting' => 
  array (
    'minpwdlength' => 6,
    'maxpwdlength' => '',
    'oneupper' => true,
    'onelower' => true,
    'onenumber' => true,
    'onespecial' => '0',
    'SystemGeneratedPasswordON' => '0',
    'generatepasswordtmpl' => '896ec48c-1290-0868-0212-55e46bb45076',
    'lostpasswordtmpl' => '907c24e9-089f-6d94-ea19-55e46b9a2f1a',
    'customregex' => '',
    'regexcomment' => '',
    'forgotpasswordON' => '0',
    'linkexpiration' => true,
    'linkexpirationtime' => 24,
    'linkexpirationtype' => 60,
    'userexpiration' => '0',
    'userexpirationtime' => '',
    'userexpirationtype' => '1',
    'userexpirationlogin' => '',
    'systexpiration' => 1,
    'systexpirationtime' => 7,
    'systexpirationtype' => '0',
    'systexpirationlogin' => '',
    'lockoutexpiration' => '0',
    'lockoutexpirationtime' => '',
    'lockoutexpirationtype' => '1',
    'lockoutexpirationlogin' => '',
  ),
  'pdf_file_max_lifetime' => 86400,
  'pms_product' => 
  array (
    'catalog_id' => '173',
    'po_prefix' => 'po',
    'pod_prefix' => 'pod',
    'base_url' => 'http://localhost/ossb-pms-services',
    'public_key' => '',
    'secret_key' => '',
    'state_id' => '5',
  ),
  'pmse_settings_default' => 
  array (
    'logger_level' => 'critical',
    'error_number_of_cycles' => '10',
    'error_timeout' => '40',
  ),
  'portal_view' => 'single_user',
  'preview_edit' => false,
  'require_accounts' => true,
  'resource_management' => 
  array (
    'special_query_limit' => 50000,
    'special_query_modules' => 
    array (
      0 => 'Reports',
      1 => 'Export',
      2 => 'Import',
      3 => 'Administration',
      4 => 'Sync',
      5 => 'gc_Contracts',
      6 => 'gc_Contracts',
      7 => 'gc_Contracts',
      8 => 'gc_Contracts',
      9 => 'gc_Contracts',
      10 => 'gc_Contracts',
      12 => 'gc_Contracts',
      14 => 'gc_Contracts',
    ),
    'default_limit' => 1000,
  ),
  'roleBasedViews' => true,
  'root_dir_path' => '/var/www/html',
  'rss_cache_time' => '10800',
  'save_query' => 'all',
  'search_engine' => 
  array (
    'force_async_index' => true,
  ),
  'search_wildcard_char' => '%',
  'search_wildcard_infront' => false,
  'session_dir' => '',
  'showDetailData' => true,
  'showThemePicker' => true,
  'show_download_tab' => true,
  'site_url' => 'https://dev41.devgcm-system.com/',
  'slow_query_time_msec' => '1000',
  'smtp_mailer_debug' => 0,
  'snip_url' => 'https://ease.sugarcrm.com/',
  'stack_trace_errors' => false,
  'sugar_max_int' => 2147483647,
  'sugar_min_int' => -2147483648,
  'sugar_version' => '7.9.3.0',
  'time_formats' => 
  array (
    'H:i' => '23:00',
    'h:ia' => '11:00pm',
    'h:iA' => '11:00PM',
    'h:i a' => '11:00 pm',
    'h:i A' => '11:00 PM',
    'H.i' => '23.00',
    'h.ia' => '11.00pm',
    'h.iA' => '11.00PM',
    'h.i a' => '11.00 pm',
    'h.i A' => '11.00 PM',
  ),
  'timef' => 'H:i',
  'tmp_dir' => 'cache/xml/',
  'tmp_file_max_lifetime' => 86400,
  'tracker_max_display_length' => 30,
  'translation_string_prefix' => false,
  'unique_key' => '320d8500353257a52ab3e47201d4588e',
  'upload_badext' => 
  array (
    0 => 'php',
    1 => 'php3',
    2 => 'php4',
    3 => 'php5',
    4 => 'pl',
    5 => 'cgi',
    6 => 'py',
    7 => 'asp',
    8 => 'cfm',
    9 => 'js',
    10 => 'vbs',
    11 => 'html',
    12 => 'htm',
  ),
  'upload_dir' => 'upload',
  'upload_maxsize' => 30000000,
  'use_common_ml_dir' => false,
  'use_real_names' => true,
  'use_sprites' => true,
  'vcal_time' => '2',
  'verify_client_ip' => false,
  'wl_list_max_entries_per_page' => 10,
  'wl_list_max_entries_per_subpanel' => 3,
);