<?php
/* Created by SugarUpgrader for module pi_product_item */
$viewdefs['pi_product_item']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#pi_product_item/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'pi_product_item',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#pi_product_item',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'pi_product_item',
    'icon' => 'fa-bars',
  ),
);
