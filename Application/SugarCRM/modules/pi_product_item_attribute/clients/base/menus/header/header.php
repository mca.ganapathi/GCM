<?php
/* Created by SugarUpgrader for module pi_product_item_attribute */
$viewdefs['pi_product_item_attribute']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#pi_product_item_attribute/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'pi_product_item_attribute',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#pi_product_item_attribute',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'pi_product_item_attribute',
    'icon' => 'fa-bars',
  ),
);
