<?php
/* Created by SugarUpgrader for module gc_comments */
$viewdefs['gc_comments']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_comments/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_comments',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_comments',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_comments',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=gc_comments',
    'label' => 'LNK_IMPORT_GC_COMMENTS',
    'acl_action' => 'import',
    'acl_module' => 'gc_comments',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
