<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_TEAM' => 'Team',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettelse Dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_FORM_TITLE' => 'Contract Comments Liste',
  'LBL_MODULE_NAME' => 'Contract Comments',
  'LBL_MODULE_TITLE' => 'Contract Comments',
  'LBL_HOMEPAGE_TITLE' => 'Min Contract Comments',
  'LNK_NEW_RECORD' => 'Opprett Contract Comments',
  'LNK_LIST' => 'Vis Contract Comments',
  'LNK_IMPORT_GC_COMMENTS' => 'Import Contract Comments',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Contract Comments',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteter',
  'LBL_GC_COMMENTS_SUBPANEL_TITLE' => 'Contract Comments',
  'LBL_NEW_FORM_TITLE' => 'Ny Contract Comments',
  'LBL_CONTRACT_NAME_GC_CONTRACTS_ID' => 'Contract Name (related  ID)',
  'LBL_CONTRACT_NAME' => 'Contract Name',
);