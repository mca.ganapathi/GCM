<?php
/* Created by SugarUpgrader for module gc_NTTComGroupCompany */
$viewdefs['gc_NTTComGroupCompany']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_NTTComGroupCompany/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_NTTComGroupCompany',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_NTTComGroupCompany',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_NTTComGroupCompany',
    'icon' => 'fa-bars',
  ),
);
