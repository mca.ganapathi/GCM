<?php
/* Created by SugarUpgrader for module gc_Contracts */
$viewdefs['gc_Contracts']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_Contracts/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_Contracts',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_Contracts',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_Contracts',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=gc_Contracts',
    'label' => 'LNK_IMPORT_GC_CONTRACTS',
    'acl_action' => 'import',
    'acl_module' => 'gc_Contracts',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
