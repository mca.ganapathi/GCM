<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_ASSIGNED_TO_ID' => 'Zugewiesene BenutzerID',
  'LBL_ASSIGNED_TO_NAME' => 'Zugewiesen an',
  'LBL_CREATED' => 'Erstellt von:',
  'LBL_CREATED_ID' => 'Erstellt von ID:',
  'LBL_CREATED_USER' => 'Erstellt von Benutzer:',
  'LBL_DATE_ENTERED' => 'Erstellt am:',
  'LBL_DATE_MODIFIED' => 'Geändert am',
  'LBL_DELETED' => 'Gelöscht',
  'LBL_DESCRIPTION' => 'Beschreibung',
  'LBL_EDIT_BUTTON' => 'Bearbeiten',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Name',
  'LBL_MODIFIED' => 'Geändert von',
  'LBL_MODIFIED_ID' => 'Geändert von ID',
  'LBL_MODIFIED_NAME' => 'Geändert von Name',
  'LBL_MODIFIED_USER' => 'Geändert von',
  'LBL_NAME' => 'Name',
  'LBL_REMOVE' => 'Entfernen',
  'LBL_LIST_FORM_TITLE' => 'Contracts Liste',
  'LBL_MODULE_NAME' => 'Contracts',
  'LBL_MODULE_TITLE' => 'Contracts',
  'LBL_HOMEPAGE_TITLE' => 'Mein Contracts',
  'LNK_NEW_RECORD' => 'Erstellen Contracts',
  'LNK_LIST' => 'Ansicht Contracts',
  'LNK_IMPORT_GC_CONTRACTS' => 'Import Contracts',
  'LBL_SEARCH_FORM_TITLE' => 'Suchen Contracts',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Verlauf ansehen',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitäten',
  'LBL_GC_CONTRACTS_SUBPANEL_TITLE' => 'Contracts',
  'LBL_NEW_FORM_TITLE' => 'Neu Contracts',
  'LBL_GLOBAL_CONTRACT_ID' => 'Global Contract ID',
  'LBL_CONTRACT_TYPE' => 'Contract Type',
  'LBL_CONTRACT_VERSION' => 'Contract Version',
  'LBL_CONTRACT_STATUS' => 'Contract Status',
  'LBL_LOI_CONTRACT' => 'LOI Contract',
  'LBL_BILLING_TYPE' => 'Billing Type',
  'LBL_TOTAL_NRC' => 'Total NRC',
  'LBL_TOTAL_MRC' => 'Total MRC',
  'LBL_GRAND_TOTAL' => 'Grand Total',
  'LBL_CONTRACT_REQUEST_ID' => 'Contract Request ID',
  'LBL_EXT_SYS_CREATED_BY' => 'External System Created By',
  'LBL_EXT_SYS_MODIFIED_BY' => 'External System Modified By',
  'LBL_DATE_ENTERED_TIMEZONE' => 'Created Date Time Zone',
  'LBL_DATE_MODIFIED_TIMEZONE' => 'Modified Date Time Zone',
  'LBL_FACTORY_REFERENCE_NO' => 'Factory Reference Number',
  'LBL_CONTRACT_STAGE' => 'Contract Stage',
  'LBL_WORKFLOW_FLAG' => 'Workflow Flag',
  'LBL_CONTRACT_SCHEME' => 'Contract Scheme',
  'LBL_CONTRACT_STARTDATE' => 'Contract Start Date',
  'LBL_CONTRACT_STARTDATE_TIMEZONE' => 'Contract Start Date Time Zone',
  'LBL_CONTRACT_PERIOD' => 'Contract Period',
  'LBL_CONTRACT_PERIOD_UNIT' => 'Contract Period Unit',
  'LBL_PRODUCT_OFFERING' => 'Product Offering',
  'LBL_CONTRACT_STARTDATE_TIMEZONE_GC_TIMEZONE_ID' => 'Contract Start Date TimeZone (related  ID)',
  'LBL_GLOBAL_CONTRACT_ID_UUID' => 'Global Contract ID corresponding UUID',
  'LBL_CONTRACT_SCHEME_UUID' => 'Contract Scheme Value UUID',
  'LBL_CONTRACT_TYPE_UUID' => 'Contract Type Value UUID',
  'LBL_LOI_CONTRACT_UUID' => 'LOI Contract Value UUID',
  'LBL_BILLING_TYPE_UUID' => 'Billing Scheme Value UUID',
  'LBL_PRODUCT_OFFERING_UUID' => 'Contract Product Offering UUID',
  'LBL_FACTORY_REFERENCE_NO_UUID' => 'Factory Reference Number Value UUID',
  'LBL_LOCK_VERSION' => 'Lock Version',
  'LBL_CONTRACT_ENDDATE' => 'Contract End Date',
  'LBL_CONTRACT_ENDDATE_TIMEZONE_GC_TIMEZONE_ID' => 'Contract End Date TimeZone (related  ID)',
  'LBL_CONTRACT_ENDDATE_TIMEZONE' => 'Contract End Date TimeZone',
  'LBL_VAT_NO' => 'VAT Number',
  'LBL_MODULE_NAME_SINGULAR' => 'GCM Contracts',
);