<?php
/* Created by SugarUpgrader for module gc_CreditCard */
$viewdefs['gc_CreditCard']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_CreditCard/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_CreditCard',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_CreditCard',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_CreditCard',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=gc_CreditCard',
    'label' => 'LNK_IMPORT_GC_CREDITCARD',
    'acl_action' => 'import',
    'acl_module' => 'gc_CreditCard',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
