<?php
/* Created by SugarUpgrader for module gc_LineItem */
$viewdefs['gc_LineItem']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_LineItem/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_LineItem',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_LineItem',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_LineItem',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=gc_LineItem',
    'label' => 'LNK_IMPORT_GC_LINEITEM',
    'acl_action' => 'import',
    'acl_module' => 'gc_LineItem',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
