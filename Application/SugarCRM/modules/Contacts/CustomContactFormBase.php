<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

/*********************************************************************************

 * Description:  Base form for contact
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/Contacts/ContactFormBase.php');

class CustomContactFormBase extends ContactFormBase {

function handleSave($prefix, $redirect=true, $useRequired=false){
	global $theme, $current_user;




	require_once('include/formbase.php');

	global $timedate;

	$focus = $this->getContact();

	if($useRequired &&  !checkRequired($prefix, array_keys($focus->required_fields))){
		return null;
	}

	if (!empty($_POST[$prefix.'new_reports_to_id'])) {
		$focus->retrieve($_POST[$prefix.'new_reports_to_id']);
		$focus->reports_to_id = $_POST[$prefix.'record'];
	} else {

        $focus = populateFromPost($prefix, $focus);
        if( isset($_POST[$prefix.'old_portal_password']) && !empty($focus->portal_password) && $focus->portal_password != $_POST[$prefix.'old_portal_password']){
            $focus->portal_password = User::getPasswordHash($focus->portal_password);
        }
		if (!isset($_POST[$prefix.'email_opt_out'])) $focus->email_opt_out = 0;
		if (!isset($_POST[$prefix.'do_not_call'])) $focus->do_not_call = 0;

	}
	if(!$focus->ACLAccess('Save')){
			ACLController::displayNoAccess(true);
			sugar_cleanup(true);
	}
	if($_REQUEST['action'] != 'BusinessCard' && $_REQUEST['action'] != 'ConvertLead' && $_REQUEST['action'] != 'ConvertProspect')
	{

		if (!empty($_POST[$prefix.'sync_contact']) || !empty($focus->sync_contact)){
			 $focus->contacts_users_id = $current_user->id;
		}
		else{
			if (!isset($focus->users))
			{
	      	  	$focus->load_relationship('user_sync');
			}
	      	$focus->contacts_users_id = null;
			$focus->user_sync->delete($focus->id, $current_user->id);
		}
	}

	if (isset($GLOBALS['check_notify'])) {
		$check_notify = $GLOBALS['check_notify'];
	}
	else {
		$check_notify = FALSE;
	}


	if (empty($_POST['record']) && empty($_POST['dup_checked'])) {

		$duplicateContacts = $this->checkForDuplicates($prefix);
		unset($duplicateContacts);		//avoid duplicate check	-- Sagar.Salunkhe 30-12-2015
		if(isset($duplicateContacts)){
			$location='module=Contacts&action=ShowDuplicates';
			$get = '';
			if(isset($_POST['inbound_email_id']) && !empty($_POST['inbound_email_id'])) {
				$get .= '&inbound_email_id='.$_POST['inbound_email_id'];
			}

			// Bug 25311 - Add special handling for when the form specifies many-to-many relationships
			if(isset($_POST['relate_to']) && !empty($_POST['relate_to'])) {
				$get .= '&Contactsrelate_to='.$_POST['relate_to'];
			}
			if(isset($_POST['relate_id']) && !empty($_POST['relate_id'])) {
				$get .= '&Contactsrelate_id='.$_POST['relate_id'];
			}

			//add all of the post fields to redirect get string
			foreach ($focus->column_fields as $field)
			{
				if (!empty($focus->$field) && !is_object($focus->$field))
				{
					$get .= "&Contacts$field=".urlencode($focus->$field);
				}
			}

			foreach ($focus->additional_column_fields as $field)
			{
				if (!empty($focus->$field))
				{
					$get .= "&Contacts$field=".urlencode($focus->$field);
				}
			}

			if($focus->hasCustomFields()) {
				foreach($focus->field_defs as $name=>$field) {
					if (!empty($field['source']) && $field['source'] == 'custom_fields')
					{
						$get .= "&Contacts$name=".urlencode($focus->$name);
					}
				}
			}


			$emailAddress = new SugarEmailAddress();
			$get .= $emailAddress->getFormBaseURL($focus);

			$get .= get_teams_url('Contacts');

			//create list of suspected duplicate contact id's in redirect get string
			$i=0;
			foreach ($duplicateContacts as $contact)
			{
				$get .= "&duplicate[$i]=".$contact['id'];
				$i++;
			}

			//add return_module, return_action, and return_id to redirect get string
			$urlData = array('return_module' => 'Contacts', 'return_action' => '');
			foreach (array('return_module', 'return_action', 'return_id', 'popup', 'create', 'start') as $var) {
			    if (!empty($_POST[$var])) {
			        $urlData[$var] = $_POST[$var];
			    }
			}
			$get .= "&".http_build_query($urlData);
			$_SESSION['SHOW_DUPLICATES'] = $get;

            //now redirect the post to modules/Contacts/ShowDuplicates.php
            if (!empty($_POST['is_ajax_call']) && $_POST['is_ajax_call'] == '1')
            {
            	ob_clean();
                $json = getJSONobj();
                echo $json->encode(array('status' => 'dupe', 'get' => $location));
            }
            else if(!empty($_REQUEST['ajax_load']))
            {
                echo "<script>SUGAR.ajaxUI.loadContent('index.php?$location');</script>";
            }
            else {
                if(!empty($_POST['to_pdf'])) $location .= '&to_pdf='.urlencode($_POST['to_pdf']);
                header("Location: index.php?$location");
            }
            return null;
		}
	}

	global $current_user;
	if(is_admin($current_user)){
		if (!isset($_POST[$prefix.'portal_active'])) $focus->portal_active = '0';
		//if no password is set set account to inactive for portal
		if(empty($_POST[$prefix.'portal_name']))$focus->portal_active = '0';

	}

	///////////////////////////////////////////////////////////////////////////////
	////	INBOUND EMAIL HANDLING
	///////////////////////////////////////////////////////////////////////////////
	if(isset($_REQUEST['inbound_email_id']) && !empty($_REQUEST['inbound_email_id'])) {
		// fake this case like it's already saved.
		$focus->save($check_notify);

		$email = new Email();
		$email->retrieve($_REQUEST['inbound_email_id']);
		$email->parent_type = 'Contacts';
		$email->parent_id = $focus->id;
		$email->assigned_user_id = $current_user->id;
		$email->status = 'read';
		$email->save();
		$email->load_relationship('contacts');
		$email->contacts->add($focus->id);

		header("Location: index.php?&module=Emails&action=EditView&type=out&inbound_email_id=".$_REQUEST['inbound_email_id']."&parent_id=".$email->parent_id."&parent_type=".$email->parent_type.'&start='.$_REQUEST['start'].'&assigned_user_id='.$current_user->id);
		exit();
	}
	////	END INBOUND EMAIL HANDLING
	///////////////////////////////////////////////////////////////////////////////

	$focus->save($check_notify);
	$return_id = $focus->id;

	$GLOBALS['log']->debug("Saved record with id of ".$return_id);

    if ($redirect && !empty($_POST['is_ajax_call']) && $_POST['is_ajax_call'] == '1') {
        $json = getJSONobj();
        echo $json->encode(array('status' => 'success',
                                 'get' => ''));
    	$trackerManager = TrackerManager::getInstance();
        $timeStamp = TimeDate::getInstance()->nowDb();
        if($monitor = $trackerManager->getMonitor('tracker')){
	        $monitor->setValue('team_id', $GLOBALS['current_user']->getPrivateTeamID());
	        $monitor->setValue('action', 'detailview');
	        $monitor->setValue('user_id', $GLOBALS['current_user']->id);
	        $monitor->setValue('module_name', 'Contacts');
	        $monitor->setValue('date_modified', $timeStamp);
	        $monitor->setValue('visible', 1);

	        if (!empty($this->bean->id)) {
	            $monitor->setValue('item_id', $return_id);
	            $monitor->setValue('item_summary', $focus->get_summary_text());
	        }
			$trackerManager->saveMonitor($monitor, true, true);
		}
        return null;
    }

	if($redirect && isset($_POST['popup']) && $_POST['popup'] == 'true') {
	    $urlData = array("query" => true, "first_name" => $focus->first_name, "last_name" => $focus->last_name,
	       "module" => 'Accounts', 'action' => 'Popup');
    	if (!empty($_POST['return_module'])) {
    	    $urlData['module'] = $_POST['return_module'];
    	}
        if (!empty($_POST['return_action'])) {
    	    $urlData['action'] = $_POST['return_action'];
    	}
    	foreach(array('return_id', 'popup', 'create', 'to_pdf') as $var) {
    	    if (!empty($_POST[$var])) {
    	        $urlData[$var] = $_POST[$var];
    	    }
    	}
		header("Location: index.php?".http_build_query($urlData));
		return;
	}

	if($redirect){
		$this->handleRedirect($return_id);
	}else{
		return $focus;
	}
}

}

