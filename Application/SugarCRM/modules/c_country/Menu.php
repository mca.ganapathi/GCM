<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (â€œMSAâ€�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


global $mod_strings, $app_strings, $sugar_config;
 
/*if(ACLController::checkAccess('c_country', 'edit', true))$module_menu[]=Array("index.php?module=c_country&action=EditView&return_module=c_country&return_action=DetailView", $mod_strings['LNK_NEW_RECORD'],"Createc_country", 'c_country');*/
if(ACLController::checkAccess('c_country', 'list', true))$module_menu[]=Array("index.php?module=c_country&action=index&return_module=c_country&return_action=DetailView", $mod_strings['LNK_LIST'],"c_country", 'c_country');
if(ACLController::checkAccess('c_country', 'import', true))$module_menu[]=Array("index.php?module=Import&action=Step1&import_module=c_country&return_module=c_country&return_action=index", $app_strings['LBL_IMPORT'],"Import", 'c_country');