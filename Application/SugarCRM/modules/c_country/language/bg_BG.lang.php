<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Основен екип по подрабиране',
  'LBL_TEAM' => 'Екипи',
  'LBL_TEAMS' => 'Екипи',
  'LBL_TEAM_ID' => 'Идентификатор на екипа',
  'LBL_TEAM_SET' => 'Дефинирани екипи',
  'LBL_ASSIGNED_TO_ID' => 'Отговорник',
  'LBL_ASSIGNED_TO_NAME' => 'Потребител',
  'LBL_CREATED' => 'Създадено от',
  'LBL_CREATED_ID' => 'Създадено от',
  'LBL_CREATED_USER' => 'Създадено от потребител',
  'LBL_DATE_ENTERED' => 'Създадено на',
  'LBL_DATE_MODIFIED' => 'Модифицирано на',
  'LBL_DELETED' => 'Изтрити',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_EDIT_BUTTON' => 'Редактирай',
  'LBL_ID' => 'Идентификатор',
  'LBL_LIST_NAME' => 'Име',
  'LBL_MODIFIED' => 'Модифицирано от',
  'LBL_MODIFIED_ID' => 'Модифицирано от',
  'LBL_MODIFIED_NAME' => 'Модифицирано от',
  'LBL_MODIFIED_USER' => 'Модифицирано от потребител',
  'LBL_NAME' => 'Име',
  'LBL_REMOVE' => 'Премахни',
  'LBL_LIST_FORM_TITLE' => 'Country Списък',
  'LBL_MODULE_NAME' => 'Country',
  'LBL_MODULE_TITLE' => 'Country',
  'LBL_HOMEPAGE_TITLE' => 'Мои Country',
  'LNK_NEW_RECORD' => 'Създай Country',
  'LNK_LIST' => 'Разгледай Country',
  'LNK_IMPORT_C_COUNTRY' => 'Import Country',
  'LBL_SEARCH_FORM_TITLE' => 'Търси Country',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дейности',
  'LBL_C_COUNTRY_SUBPANEL_TITLE' => 'Country',
  'LBL_NEW_FORM_TITLE' => 'Нов Country',
  'LBL_COUNTRY_CODE_ALPHABET' => 'Country Code (alphabet)',
  'LBL_COUNTRY_CODE_NUMERIC' => 'Country Code (numeric)',
  'LBL_REGION' => 'Region',
);