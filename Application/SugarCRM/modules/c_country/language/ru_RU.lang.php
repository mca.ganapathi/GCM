<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Основная команда по умолчанию',
  'LBL_TEAM' => 'Команды',
  'LBL_TEAMS' => 'Команды',
  'LBL_TEAM_ID' => 'Команда',
  'LBL_TEAM_SET' => 'Установка команды',
  'LBL_ASSIGNED_TO_ID' => 'Ответственный (-ая)',
  'LBL_ASSIGNED_TO_NAME' => 'Ответственный (-ая)',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано (Id)',
  'LBL_CREATED_USER' => 'Создано',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_DELETED' => 'Удалено',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_EDIT_BUTTON' => 'Правка',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Название',
  'LBL_MODIFIED' => 'Изменено',
  'LBL_MODIFIED_ID' => 'Изменено (Id)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_MODIFIED_USER' => 'Изменено',
  'LBL_NAME' => 'Название',
  'LBL_REMOVE' => 'Удалить',
  'LBL_LIST_FORM_TITLE' => 'Country Список',
  'LBL_MODULE_NAME' => 'Country',
  'LBL_MODULE_TITLE' => 'Country',
  'LBL_HOMEPAGE_TITLE' => 'Моя Country',
  'LNK_NEW_RECORD' => 'Создать Country',
  'LNK_LIST' => 'Просмотр Country',
  'LNK_IMPORT_C_COUNTRY' => 'Import Country',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск Country',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_C_COUNTRY_SUBPANEL_TITLE' => 'Country',
  'LBL_NEW_FORM_TITLE' => 'Новая Country',
  'LBL_COUNTRY_CODE_ALPHABET' => 'Country Code (alphabet)',
  'LBL_COUNTRY_CODE_NUMERIC' => 'Country Code (numeric)',
  'LBL_REGION' => 'Region',
);