<?php
/* Created by SugarUpgrader for module c_country */
$viewdefs['c_country']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#c_country/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'c_country',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#c_country',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'c_country',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=c_country',
    'label' => 'LNK_IMPORT_C_COUNTRY',
    'acl_action' => 'import',
    'acl_module' => 'c_country',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
