<?php
/* Created by SugarUpgrader for module pa_ProductAttributeValues */
$viewdefs['pa_ProductAttributeValues']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#pa_ProductAttributeValues/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'pa_ProductAttributeValues',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#pa_ProductAttributeValues',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'pa_ProductAttributeValues',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=pa_ProductAttributeValues',
    'label' => 'LNK_IMPORT_PA_PRODUCTATTRIBUTEVALUES',
    'acl_action' => 'import',
    'acl_module' => 'pa_ProductAttributeValues',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
