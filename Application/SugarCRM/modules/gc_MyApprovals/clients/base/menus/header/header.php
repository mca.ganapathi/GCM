<?php
/* Created by SugarUpgrader for module gc_MyApprovals */
$viewdefs['gc_MyApprovals']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_MyApprovals/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_MyApprovals',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_MyApprovals',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_MyApprovals',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=gc_MyApprovals',
    'label' => 'LNK_IMPORT_GC_MYAPPROVALS',
    'acl_action' => 'import',
    'acl_module' => 'gc_MyApprovals',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
