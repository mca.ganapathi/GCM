<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Varsayılan Asıl Takım',
  'LBL_TEAM' => 'Takımlar',
  'LBL_TEAMS' => 'Takımlar',
  'LBL_TEAM_ID' => 'Takım ID',
  'LBL_TEAM_SET' => 'Takım Ayarla',
  'LBL_ASSIGNED_TO_ID' => 'Atanan Kullanıcı ID',
  'LBL_ASSIGNED_TO_NAME' => 'Atanan Kişi',
  'LBL_CREATED' => 'Oluşturan',
  'LBL_CREATED_ID' => 'Oluşturan ID',
  'LBL_CREATED_USER' => 'Oluşturan Kullanıcı',
  'LBL_DATE_ENTERED' => 'Oluşturulma Tarihi',
  'LBL_DATE_MODIFIED' => 'Değiştirilme Tarihi',
  'LBL_DELETED' => 'Silindi',
  'LBL_DESCRIPTION' => 'Tanım',
  'LBL_EDIT_BUTTON' => 'Değiştir',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'İsim',
  'LBL_MODIFIED' => 'Değiştiren',
  'LBL_MODIFIED_ID' => 'Değiştiren ID',
  'LBL_MODIFIED_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_MODIFIED_USER' => 'Değiştiren Kullanıcı',
  'LBL_NAME' => 'İsim',
  'LBL_REMOVE' => 'Sil',
  'LBL_LIST_FORM_TITLE' => 'My Approvals Liste',
  'LBL_MODULE_NAME' => 'My Approvals',
  'LBL_MODULE_TITLE' => 'My Approvals',
  'LBL_HOMEPAGE_TITLE' => 'Benim My Approvals',
  'LNK_NEW_RECORD' => 'Oluştur My Approvals',
  'LNK_LIST' => 'Göster My Approvals',
  'LNK_IMPORT_GC_MYAPPROVALS' => 'Import My Approvals',
  'LBL_SEARCH_FORM_TITLE' => 'Ara My Approvals',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Tarihçeyi Gör',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteler',
  'LBL_GC_MYAPPROVALS_SUBPANEL_TITLE' => 'My Approvals',
  'LBL_NEW_FORM_TITLE' => 'Yeni My Approvals',
  'LBL_ACTION' => 'Action',
  'LBL_FROM_STAGE' => 'From Stage',
  'LBL_TO_STAGE' => 'To Stage',
  'LBL_GLOBAL_CONTRACT_ID_GC_CONTRACTS_ID' => 'Global Contract Id (related  ID)',
  'LBL_GLOBAL_CONTRACT_ID' => 'Global Contract Id',
  'LBL_CONTRACT_ACTION' => 'Contract Action',
);