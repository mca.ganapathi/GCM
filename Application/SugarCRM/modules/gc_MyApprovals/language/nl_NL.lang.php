<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Standaard Primaire Team',
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_TEAM_SET' => 'Team Set',
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen aan ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_CREATED' => 'Gemaakt door',
  'LBL_CREATED_ID' => 'Gemaakt door ID',
  'LBL_CREATED_USER' => 'Gemaakt door Gebruiker',
  'LBL_DATE_ENTERED' => 'Datum ingevoerd',
  'LBL_DATE_MODIFIED' => 'Laatste wijziging',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_DESCRIPTION' => 'Beschrijving',
  'LBL_EDIT_BUTTON' => 'Wijzig',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door Naam',
  'LBL_MODIFIED_USER' => 'Gewijzigd door Gebruiker',
  'LBL_NAME' => 'Naam',
  'LBL_REMOVE' => 'Verwijder',
  'LBL_LIST_FORM_TITLE' => 'My Approvals List',
  'LBL_MODULE_NAME' => 'My Approvals',
  'LBL_MODULE_TITLE' => 'My Approvals',
  'LBL_HOMEPAGE_TITLE' => 'My My Approvals',
  'LNK_NEW_RECORD' => 'Create My Approvals',
  'LNK_LIST' => 'View My Approvals',
  'LNK_IMPORT_GC_MYAPPROVALS' => 'Import My Approvals',
  'LBL_SEARCH_FORM_TITLE' => 'Search My Approvals',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_GC_MYAPPROVALS_SUBPANEL_TITLE' => 'My Approvals',
  'LBL_NEW_FORM_TITLE' => 'New My Approvals',
  'LBL_ACTION' => 'Action',
  'LBL_FROM_STAGE' => 'From Stage',
  'LBL_TO_STAGE' => 'To Stage',
  'LBL_GLOBAL_CONTRACT_ID_GC_CONTRACTS_ID' => 'Global Contract Id (related  ID)',
  'LBL_GLOBAL_CONTRACT_ID' => 'Global Contract Id',
  'LBL_CONTRACT_ACTION' => 'Contract Action',
);