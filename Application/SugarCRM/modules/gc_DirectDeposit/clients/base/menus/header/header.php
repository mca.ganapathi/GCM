<?php
/* Created by SugarUpgrader for module gc_DirectDeposit */
$viewdefs['gc_DirectDeposit']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_DirectDeposit/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_DirectDeposit',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_DirectDeposit',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_DirectDeposit',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=gc_DirectDeposit',
    'label' => 'LNK_IMPORT_GC_DIRECTDEPOSIT',
    'acl_action' => 'import',
    'acl_module' => 'gc_DirectDeposit',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
