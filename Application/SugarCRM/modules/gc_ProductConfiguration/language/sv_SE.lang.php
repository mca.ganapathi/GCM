<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Tilldelad användare',
  'LBL_ASSIGNED_TO_NAME' => 'Tilldelad användare',
  'LBL_CREATED' => 'Skapat Av',
  'LBL_CREATED_ID' => 'Skapat Av',
  'LBL_CREATED_USER' => 'Skapat Av Användare',
  'LBL_DATE_ENTERED' => 'Skapat Datum',
  'LBL_DATE_MODIFIED' => 'Modifierat Datum',
  'LBL_DELETED' => 'Raderad',
  'LBL_DESCRIPTION' => 'Beskrivning',
  'LBL_EDIT_BUTTON' => 'Redigera',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Namn',
  'LBL_MODIFIED' => 'Modifierat Av',
  'LBL_MODIFIED_ID' => 'Modifierat Av Id',
  'LBL_MODIFIED_NAME' => 'Modifierat Av Namn',
  'LBL_MODIFIED_USER' => 'Modifierat Av Användare',
  'LBL_NAME' => 'Namn',
  'LBL_REMOVE' => 'Ta bort',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_TEAM' => 'Team',
  'LBL_LIST_FORM_TITLE' => 'Product Configuration List',
  'LBL_MODULE_NAME' => 'Product Configuration',
  'LBL_MODULE_TITLE' => 'Product Configuration',
  'LBL_HOMEPAGE_TITLE' => 'My Product Configuration',
  'LNK_NEW_RECORD' => 'Create Product Configuration',
  'LNK_LIST' => 'Visa Product Configuration',
  'LNK_IMPORT_GC_PRODUCTCONFIGURATION' => 'Import Product Configuration',
  'LBL_SEARCH_FORM_TITLE' => 'Search Product Configuration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_GC_PRODUCTCONFIGURATION_SUBPANEL_TITLE' => 'Product Configuration',
  'LBL_NEW_FORM_TITLE' => 'New Product Configuration',
  'LBL_CHARACTERISTIC_VALUE' => 'Characteristic Value',
  'LBL_LINE_ITEM_ID' => 'Line Item Id',
  'LBL_CHARACTERISTIC_UUID' => 'Characteristic UUID',
);