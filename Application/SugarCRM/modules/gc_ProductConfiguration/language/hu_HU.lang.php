<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Hozzárendelt felhasználó azonosító',
  'LBL_ASSIGNED_TO_NAME' => 'Felelős felhasználó',
  'LBL_CREATED' => 'Létrehozta',
  'LBL_CREATED_ID' => 'Létrehozó azonosítója',
  'LBL_CREATED_USER' => 'Felhasználó által létrehozva',
  'LBL_DATE_ENTERED' => 'Létrehozás dátuma',
  'LBL_DATE_MODIFIED' => 'Módosítás dátuma',
  'LBL_DELETED' => 'Törölve',
  'LBL_DESCRIPTION' => 'Leírás',
  'LBL_EDIT_BUTTON' => 'Szerkesztés',
  'LBL_ID' => 'Azonosító',
  'LBL_LIST_NAME' => 'Név',
  'LBL_MODIFIED' => 'Módosította',
  'LBL_MODIFIED_ID' => 'Módosította (azonosító szerint)',
  'LBL_MODIFIED_NAME' => 'Módosította (név szerint)',
  'LBL_MODIFIED_USER' => 'Felhasználó által módosítva',
  'LBL_NAME' => 'Név',
  'LBL_REMOVE' => 'Eltávolítás',
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Alapértelmezett elsődleges csoport',
  'LBL_TEAM' => 'Csoportok',
  'LBL_TEAMS' => 'Csoportok',
  'LBL_TEAM_ID' => 'Csoport azonosító',
  'LBL_TEAM_SET' => 'Csoport készlet',
  'LBL_LIST_FORM_TITLE' => 'Product Configuration Lista',
  'LBL_MODULE_NAME' => 'Product Configuration',
  'LBL_MODULE_TITLE' => 'Product Configuration',
  'LBL_HOMEPAGE_TITLE' => 'Saját Product Configuration',
  'LNK_NEW_RECORD' => 'Új létrehozása Product Configuration',
  'LNK_LIST' => 'Megtekintés Product Configuration',
  'LNK_IMPORT_GC_PRODUCTCONFIGURATION' => 'Import Product Configuration',
  'LBL_SEARCH_FORM_TITLE' => 'Keresés Product Configuration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Előzmények megtekintése',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tevékenységek',
  'LBL_GC_PRODUCTCONFIGURATION_SUBPANEL_TITLE' => 'Product Configuration',
  'LBL_NEW_FORM_TITLE' => 'Új Product Configuration',
  'LBL_CHARACTERISTIC_VALUE' => 'Characteristic Value',
  'LBL_LINE_ITEM_ID' => 'Line Item Id',
  'LBL_CHARACTERISTIC_UUID' => 'Characteristic UUID',
);