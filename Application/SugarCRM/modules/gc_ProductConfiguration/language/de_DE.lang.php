<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Zugewiesene BenutzerID',
  'LBL_ASSIGNED_TO_NAME' => 'Zugewiesen an',
  'LBL_CREATED' => 'Erstellt von:',
  'LBL_CREATED_ID' => 'Erstellt von ID:',
  'LBL_CREATED_USER' => 'Erstellt von Benutzer:',
  'LBL_DATE_ENTERED' => 'Erstellt am:',
  'LBL_DATE_MODIFIED' => 'Geändert am',
  'LBL_DELETED' => 'Gelöscht',
  'LBL_DESCRIPTION' => 'Beschreibung',
  'LBL_EDIT_BUTTON' => 'Bearbeiten',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Name',
  'LBL_MODIFIED' => 'Geändert von',
  'LBL_MODIFIED_ID' => 'Geändert von ID',
  'LBL_MODIFIED_NAME' => 'Geändert von Name',
  'LBL_MODIFIED_USER' => 'Geändert von',
  'LBL_NAME' => 'Name',
  'LBL_REMOVE' => 'Entfernen',
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_LIST_FORM_TITLE' => 'Product Configuration Liste',
  'LBL_MODULE_NAME' => 'Product Configuration',
  'LBL_MODULE_TITLE' => 'Product Configuration',
  'LBL_HOMEPAGE_TITLE' => 'Mein Product Configuration',
  'LNK_NEW_RECORD' => 'Erstellen Product Configuration',
  'LNK_LIST' => 'Ansicht Product Configuration',
  'LNK_IMPORT_GC_PRODUCTCONFIGURATION' => 'Import Product Configuration',
  'LBL_SEARCH_FORM_TITLE' => 'Suchen Product Configuration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Verlauf ansehen',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitäten',
  'LBL_GC_PRODUCTCONFIGURATION_SUBPANEL_TITLE' => 'Product Configuration',
  'LBL_NEW_FORM_TITLE' => 'Neu Product Configuration',
  'LBL_CHARACTERISTIC_VALUE' => 'Characteristic Value',
  'LBL_LINE_ITEM_ID' => 'Line Item Id',
  'LBL_CHARACTERISTIC_UUID' => 'Characteristic UUID',
);