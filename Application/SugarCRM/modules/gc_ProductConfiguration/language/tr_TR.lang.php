<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Atanan Kullanıcı ID',
  'LBL_ASSIGNED_TO_NAME' => 'Atanan Kişi',
  'LBL_CREATED' => 'Oluşturan',
  'LBL_CREATED_ID' => 'Oluşturan ID',
  'LBL_CREATED_USER' => 'Oluşturan Kullanıcı',
  'LBL_DATE_ENTERED' => 'Oluşturulma Tarihi',
  'LBL_DATE_MODIFIED' => 'Değiştirilme Tarihi',
  'LBL_DELETED' => 'Silindi',
  'LBL_DESCRIPTION' => 'Tanım',
  'LBL_EDIT_BUTTON' => 'Değiştir',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'İsim',
  'LBL_MODIFIED' => 'Değiştiren',
  'LBL_MODIFIED_ID' => 'Değiştiren ID',
  'LBL_MODIFIED_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_MODIFIED_USER' => 'Değiştiren Kullanıcı',
  'LBL_NAME' => 'İsim',
  'LBL_REMOVE' => 'Sil',
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Varsayılan Asıl Takım',
  'LBL_TEAM' => 'Takımlar',
  'LBL_TEAMS' => 'Takımlar',
  'LBL_TEAM_ID' => 'Takım ID',
  'LBL_TEAM_SET' => 'Takım Ayarla',
  'LBL_LIST_FORM_TITLE' => 'Product Configuration Liste',
  'LBL_MODULE_NAME' => 'Product Configuration',
  'LBL_MODULE_TITLE' => 'Product Configuration',
  'LBL_HOMEPAGE_TITLE' => 'Benim Product Configuration',
  'LNK_NEW_RECORD' => 'Oluştur Product Configuration',
  'LNK_LIST' => 'Göster Product Configuration',
  'LNK_IMPORT_GC_PRODUCTCONFIGURATION' => 'Import Product Configuration',
  'LBL_SEARCH_FORM_TITLE' => 'Ara Product Configuration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Tarihçeyi Gör',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteler',
  'LBL_GC_PRODUCTCONFIGURATION_SUBPANEL_TITLE' => 'Product Configuration',
  'LBL_NEW_FORM_TITLE' => 'Yeni Product Configuration',
  'LBL_CHARACTERISTIC_VALUE' => 'Characteristic Value',
  'LBL_LINE_ITEM_ID' => 'Line Item Id',
  'LBL_CHARACTERISTIC_UUID' => 'Characteristic UUID',
);