<?php
/* Created by SugarUpgrader for module gc_ProductConfiguration */
$viewdefs['gc_ProductConfiguration']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_ProductConfiguration/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_ProductConfiguration',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_ProductConfiguration',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_ProductConfiguration',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=gc_ProductConfiguration',
    'label' => 'LNK_IMPORT_GC_PRODUCTCONFIGURATION',
    'acl_action' => 'import',
    'acl_module' => 'gc_ProductConfiguration',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
