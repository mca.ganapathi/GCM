<?php
/* Created by SugarUpgrader for module pm_Products */
$viewdefs['pm_Products']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#pm_Products/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'pm_Products',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#pm_Products',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'pm_Products',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=pm_Products',
    'label' => 'LNK_IMPORT_PM_PRODUCTS',
    'acl_action' => 'import',
    'acl_module' => 'pm_Products',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
