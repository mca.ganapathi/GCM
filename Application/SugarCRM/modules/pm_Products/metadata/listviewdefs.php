<?php
// created: 2018-03-07 11:45:26
$listViewDefs['pm_Products'] = array (
  'NAME' => 
  array (
    'width' => '32',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'PRODUCTS_ID' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PRODUCTS_ID',
    'width' => '10',
    'default' => true,
  ),
  'PRODUCT_STATUS' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_PRODUCT_STATUS',
    'width' => '10',
  ),
  'PRODUCT_VERSION' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_PRODUCT_VERSION',
    'width' => '10',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => false,
  ),
);