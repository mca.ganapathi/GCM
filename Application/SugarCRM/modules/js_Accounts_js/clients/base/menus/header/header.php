<?php
/* Created by SugarUpgrader for module js_Accounts_js */
$viewdefs['js_Accounts_js']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#js_Accounts_js/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'js_Accounts_js',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#js_Accounts_js',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'js_Accounts_js',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=js_Accounts_js',
    'label' => 'LNK_IMPORT_JS_ACCOUNTS_JS',
    'acl_action' => 'import',
    'acl_module' => 'js_Accounts_js',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
