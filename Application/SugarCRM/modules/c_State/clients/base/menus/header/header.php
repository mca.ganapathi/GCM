<?php
/* Created by SugarUpgrader for module c_State */
$viewdefs['c_State']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#c_State/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'c_State',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#c_State',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'c_State',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=c_State',
    'label' => 'LNK_IMPORT_C_STATE',
    'acl_action' => 'import',
    'acl_module' => 'c_State',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
