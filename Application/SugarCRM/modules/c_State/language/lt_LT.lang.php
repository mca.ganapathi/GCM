<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_TEAM' => 'Komandos',
  'LBL_TEAMS' => 'Komandos',
  'LBL_TEAM_ID' => 'Komandos Id',
  'LBL_ASSIGNED_TO_ID' => 'Atsakingo Id',
  'LBL_ASSIGNED_TO_NAME' => 'Atsakingas',
  'LBL_CREATED' => 'Sukūrė',
  'LBL_CREATED_ID' => 'Kūrėjo Id',
  'LBL_CREATED_USER' => 'Sukūrė',
  'LBL_DATE_ENTERED' => 'Sukurta',
  'LBL_DATE_MODIFIED' => 'Redaguota',
  'LBL_DELETED' => 'Ištrintas',
  'LBL_DESCRIPTION' => 'Aprašymas',
  'LBL_EDIT_BUTTON' => 'Redaguoti',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Pavadinimas',
  'LBL_MODIFIED' => 'Redagavo',
  'LBL_MODIFIED_ID' => 'Redaguotojo Id',
  'LBL_MODIFIED_NAME' => 'Redaguotojo vardas',
  'LBL_MODIFIED_USER' => 'Redagavo',
  'LBL_NAME' => 'Pavadinimas',
  'LBL_REMOVE' => 'Išimti',
  'LBL_LIST_FORM_TITLE' => 'State Sąrašas',
  'LBL_MODULE_NAME' => 'State',
  'LBL_MODULE_TITLE' => 'State',
  'LBL_HOMEPAGE_TITLE' => 'Mano State',
  'LNK_NEW_RECORD' => 'Sukurti State',
  'LNK_LIST' => 'View State',
  'LNK_IMPORT_C_STATE' => 'Import State',
  'LBL_SEARCH_FORM_TITLE' => 'Paieška State',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Rodyti istoriją',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Priminimai',
  'LBL_C_STATE_SUBPANEL_TITLE' => 'State',
  'LBL_NEW_FORM_TITLE' => 'Naujas State',
  'LBL_STATE_TYPE' => 'State Type',
  'LBL_STATE_CODE' => 'State Code',
);