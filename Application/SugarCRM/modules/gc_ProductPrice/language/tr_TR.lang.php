<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Atanan Kullanıcı ID',
  'LBL_ASSIGNED_TO_NAME' => 'Atanan Kişi',
  'LBL_CREATED' => 'Oluşturan',
  'LBL_CREATED_ID' => 'Oluşturan ID',
  'LBL_CREATED_USER' => 'Oluşturan Kullanıcı',
  'LBL_DATE_ENTERED' => 'Oluşturulma Tarihi',
  'LBL_DATE_MODIFIED' => 'Değiştirilme Tarihi',
  'LBL_DELETED' => 'Silindi',
  'LBL_DESCRIPTION' => 'Tanım',
  'LBL_EDIT_BUTTON' => 'Değiştir',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'İsim',
  'LBL_MODIFIED' => 'Değiştiren',
  'LBL_MODIFIED_ID' => 'Değiştiren ID',
  'LBL_MODIFIED_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_MODIFIED_USER' => 'Değiştiren Kullanıcı',
  'LBL_NAME' => 'İsim',
  'LBL_REMOVE' => 'Sil',
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Varsayılan Asıl Takım',
  'LBL_TEAM' => 'Takımlar',
  'LBL_TEAMS' => 'Takımlar',
  'LBL_TEAM_ID' => 'Takım ID',
  'LBL_TEAM_SET' => 'Takım Ayarla',
  'LBL_LIST_FORM_TITLE' => 'Product Price Liste',
  'LBL_MODULE_NAME' => 'Product Price',
  'LBL_MODULE_TITLE' => 'Product Price',
  'LBL_HOMEPAGE_TITLE' => 'Benim Product Price',
  'LNK_NEW_RECORD' => 'Oluştur Product Price',
  'LNK_LIST' => 'Göster Product Price',
  'LNK_IMPORT_GC_PRODUCTPRICE' => 'Import Product Price',
  'LBL_SEARCH_FORM_TITLE' => 'Ara Product Price',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Tarihçeyi Gör',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteler',
  'LBL_GC_PRODUCTPRICE_SUBPANEL_TITLE' => 'Product Price',
  'LBL_NEW_FORM_TITLE' => 'Yeni Product Price',
  'LBL_CHARGE_TYPE' => 'Charge Type',
  'LBL_CHARGE_PERIOD' => 'Charge Period',
  'LBL_LIST_PRICE' => 'List Price',
  'LBL_LINE_ITEM_ID' => 'Line Item Id',
  'LBL_ICB_FLAG' => 'ICB Flag',
  'LBL_DISCOUNT' => 'Discount',
  'LBL_PERCENT_DISCOUNT_FLAG' => '% Discount Flag',
  'LBL_CUSTOMER_CONTRACT_PERIOD' => 'Customer Contract Price',
  'LBL_IGC_SETTLEMENT_PRICE' => 'Inter-Group-Company Settlement Price',
  'LBL_CUSTOMER_CONTRACT_PRICE' => 'Customer Contract Price',
  'LBL_CURRENCYID' => 'Currency ID',
  'LBL_CHARGE_UUID' => 'Charge UUID',
  'LBL_CURRENCYID_UUID' => 'Currency ID Value UUID',
  'LBL_CHARGE_TYPE_UUID' => 'Charge Type Value UUID',
  'LBL_CHARGE_PERIOD_UUID' => 'Charge Period Value UUID',
  'LBL_LIST_PRICE_UUID' => 'List Price Value UUID',
  'LBL_ICB_FLAG_UUID' => 'ICB Flag Value UUID',
  'LBL_DISCOUNT_AMOUNT_UUID' => 'Discount Amount Value UUID',
  'LBL_DISCOUNT_RATE_UUID' => 'Discount Rate Value UUID',
  'LBL_CUSTOMER_CONTRACT_PRICE_UUID' => 'Customer Contract Price Value UUID',
  'LBL_IGC_SETTLEMENT_PRICE_UUID' => 'Inter-Group-Company Settlement Price Value UUID',
);