<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assegnato a ID:',
  'LBL_ASSIGNED_TO_NAME' => 'Assegnato a:',
  'LBL_CREATED' => 'Creato da',
  'LBL_CREATED_ID' => 'Creato da Id',
  'LBL_CREATED_USER' => 'Creato da utente',
  'LBL_DATE_ENTERED' => 'Data Inserimento',
  'LBL_DATE_MODIFIED' => 'Data Modifica',
  'LBL_DELETED' => 'Cancellato',
  'LBL_DESCRIPTION' => 'Descrizione',
  'LBL_EDIT_BUTTON' => 'Modifica',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_MODIFIED' => 'Modificato da',
  'LBL_MODIFIED_ID' => 'Modificato da Id',
  'LBL_MODIFIED_NAME' => 'Modificato da Nome',
  'LBL_MODIFIED_USER' => 'Modificato da utente',
  'LBL_NAME' => 'Nome',
  'LBL_REMOVE' => 'Rimuovi',
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Gruppo Primario di default',
  'LBL_TEAM' => 'Gruppi',
  'LBL_TEAMS' => 'Gruppi',
  'LBL_TEAM_ID' => 'Id Gruppo',
  'LBL_TEAM_SET' => 'Impostazioni Gruppo',
  'LBL_LIST_FORM_TITLE' => 'Product Price Lista',
  'LBL_MODULE_NAME' => 'Product Price',
  'LBL_MODULE_TITLE' => 'Product Price',
  'LBL_HOMEPAGE_TITLE' => 'Mio Product Price',
  'LNK_NEW_RECORD' => 'Crea Product Price',
  'LNK_LIST' => 'Visualizza Product Price',
  'LNK_IMPORT_GC_PRODUCTPRICE' => 'Import Product Price',
  'LBL_SEARCH_FORM_TITLE' => 'Ricerca Product Price',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Cronologia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Attività',
  'LBL_GC_PRODUCTPRICE_SUBPANEL_TITLE' => 'Product Price',
  'LBL_NEW_FORM_TITLE' => 'Nuovo Product Price',
  'LBL_CHARGE_TYPE' => 'Charge Type',
  'LBL_CHARGE_PERIOD' => 'Charge Period',
  'LBL_LIST_PRICE' => 'List Price',
  'LBL_LINE_ITEM_ID' => 'Line Item Id',
  'LBL_ICB_FLAG' => 'ICB Flag',
  'LBL_DISCOUNT' => 'Discount',
  'LBL_PERCENT_DISCOUNT_FLAG' => '% Discount Flag',
  'LBL_CUSTOMER_CONTRACT_PERIOD' => 'Customer Contract Price',
  'LBL_IGC_SETTLEMENT_PRICE' => 'Inter-Group-Company Settlement Price',
  'LBL_CUSTOMER_CONTRACT_PRICE' => 'Customer Contract Price',
  'LBL_CURRENCYID' => 'Currency ID',
  'LBL_CHARGE_UUID' => 'Charge UUID',
  'LBL_CURRENCYID_UUID' => 'Currency ID Value UUID',
  'LBL_CHARGE_TYPE_UUID' => 'Charge Type Value UUID',
  'LBL_CHARGE_PERIOD_UUID' => 'Charge Period Value UUID',
  'LBL_LIST_PRICE_UUID' => 'List Price Value UUID',
  'LBL_ICB_FLAG_UUID' => 'ICB Flag Value UUID',
  'LBL_DISCOUNT_AMOUNT_UUID' => 'Discount Amount Value UUID',
  'LBL_DISCOUNT_RATE_UUID' => 'Discount Rate Value UUID',
  'LBL_CUSTOMER_CONTRACT_PRICE_UUID' => 'Customer Contract Price Value UUID',
  'LBL_IGC_SETTLEMENT_PRICE_UUID' => 'Inter-Group-Company Settlement Price Value UUID',
);