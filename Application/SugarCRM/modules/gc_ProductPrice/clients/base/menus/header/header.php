<?php
/* Created by SugarUpgrader for module gc_ProductPrice */
$viewdefs['gc_ProductPrice']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_ProductPrice/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_ProductPrice',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_ProductPrice',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_ProductPrice',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=gc_ProductPrice',
    'label' => 'LNK_IMPORT_GC_PRODUCTPRICE',
    'acl_action' => 'import',
    'acl_module' => 'gc_ProductPrice',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
