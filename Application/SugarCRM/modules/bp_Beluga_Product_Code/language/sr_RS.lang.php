<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Podrazumevani primarni tim',
  'LBL_TEAM' => 'Timovi',
  'LBL_TEAMS' => 'Timovi',
  'LBL_TEAM_ID' => 'ID broj tima',
  'LBL_TEAM_SET' => 'Postavljeni tim',
  'LBL_ASSIGNED_TO_ID' => 'ID broj dodeljenog korisnika',
  'LBL_ASSIGNED_TO_NAME' => 'Korisnik',
  'LBL_CREATED' => 'Autor',
  'LBL_CREATED_ID' => 'ID broj korisnika koji je kreirao',
  'LBL_CREATED_USER' => 'Kreirao korisnik',
  'LBL_DATE_ENTERED' => 'Datum kreiranja',
  'LBL_DATE_MODIFIED' => 'Datum izmene',
  'LBL_DELETED' => 'Obrisan',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_EDIT_BUTTON' => 'Izmeni',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Ime',
  'LBL_MODIFIED' => 'Promenio',
  'LBL_MODIFIED_ID' => 'ID broj korisnika koji je promenio',
  'LBL_MODIFIED_NAME' => 'Ime korisnika koji je promenio',
  'LBL_MODIFIED_USER' => 'Promenio korisnik',
  'LBL_NAME' => 'Ime',
  'LBL_REMOVE' => 'Ukloni',
  'LBL_LIST_FORM_TITLE' => 'Beluga Product Code Lista',
  'LBL_MODULE_NAME' => 'Beluga Product Code',
  'LBL_MODULE_TITLE' => 'Beluga Product Code',
  'LBL_HOMEPAGE_TITLE' => 'Moja Beluga Product Code',
  'LNK_NEW_RECORD' => 'Kreiraj Beluga Product Code',
  'LNK_LIST' => 'Pregled Beluga Product Code',
  'LNK_IMPORT_BP_BELUGA_PRODUCT_CODE' => 'Import Beluga Product Code',
  'LBL_SEARCH_FORM_TITLE' => 'Pretraga Beluga Product Code',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Pregled istorije',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivnosti',
  'LBL_BP_BELUGA_PRODUCT_CODE_SUBPANEL_TITLE' => 'Beluga Product Code',
  'LBL_NEW_FORM_TITLE' => 'Novo Beluga Product Code',
  'LBL_GCM_PRODUCT_ITEM_ID_PI_PRODUCT_ITEM_ID' => 'gcm product item id (related  ID)',
  'LBL_GCM_PRODUCT_ITEM_ID' => 'gcm product item id',
  'LBL_BELUGA_PRD_TYPE' => 'Beluga Product Type',
  'LBL_BELUGA_SRV_TYPE' => 'Beluga Service Type',
  'LBL_MODULE_NAME_SINGULAR' => 'Beluga Product Code',
);