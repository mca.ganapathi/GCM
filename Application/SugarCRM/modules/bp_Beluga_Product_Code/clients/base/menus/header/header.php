<?php
/* Created by SugarUpgrader for module bp_Beluga_Product_Code */
$viewdefs['bp_Beluga_Product_Code']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#bp_Beluga_Product_Code/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'bp_Beluga_Product_Code',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#bp_Beluga_Product_Code',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'bp_Beluga_Product_Code',
    'icon' => 'fa-bars',
  ),
);
