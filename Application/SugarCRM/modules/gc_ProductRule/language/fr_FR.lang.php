<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assigné à (ID)',
  'LBL_ASSIGNED_TO_NAME' => 'Assigné à',
  'LBL_CREATED' => 'Créé par',
  'LBL_CREATED_ID' => 'Créé par (ID)',
  'LBL_CREATED_USER' => 'Créé par',
  'LBL_DATE_ENTERED' => 'Date de création',
  'LBL_DATE_MODIFIED' => 'Date de modification',
  'LBL_DELETED' => 'Supprimé',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_EDIT_BUTTON' => 'Editer',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_MODIFIED' => 'Modifié par',
  'LBL_MODIFIED_ID' => 'Modifié par (ID)',
  'LBL_MODIFIED_NAME' => 'Modifié par',
  'LBL_MODIFIED_USER' => 'Modifié par',
  'LBL_NAME' => 'Nom',
  'LBL_REMOVE' => 'Supprimer',
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Equipe principale par défaut',
  'LBL_TEAM' => 'Equipe',
  'LBL_TEAMS' => 'Equipes',
  'LBL_TEAM_ID' => 'Equipe (ID)',
  'LBL_TEAM_SET' => 'Groupement d&#39;équipes',
  'LBL_LIST_FORM_TITLE' => 'Product Rule Liste',
  'LBL_MODULE_NAME' => 'Product Rule',
  'LBL_MODULE_TITLE' => 'Product Rule',
  'LBL_HOMEPAGE_TITLE' => 'Mes Product Rule',
  'LNK_NEW_RECORD' => 'Créer Product Rule',
  'LNK_LIST' => 'Vue Product Rule',
  'LNK_IMPORT_GC_PRODUCTRULE' => 'Import Product Rule',
  'LBL_SEARCH_FORM_TITLE' => 'Recherche Product Rule',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Voir l&#39;Historique',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activités',
  'LBL_GC_PRODUCTRULE_SUBPANEL_TITLE' => 'Product Rule',
  'LBL_NEW_FORM_TITLE' => 'Nouveau Product Rule',
  'LBL_LINE_ITEM_ID' => 'Line Item Id',
  'LBL_RULE_UUID' => 'Rule UUID ',
);