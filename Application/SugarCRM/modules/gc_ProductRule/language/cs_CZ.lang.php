<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Přidělené ID uživatele',
  'LBL_ASSIGNED_TO_NAME' => 'Uživatel',
  'LBL_CREATED' => 'Vytvořeno',
  'LBL_CREATED_ID' => 'Vytvořeno (ID)',
  'LBL_CREATED_USER' => 'Vytvořeno uživatelem',
  'LBL_DATE_ENTERED' => 'Datum vytvoření',
  'LBL_DATE_MODIFIED' => 'Datum změny',
  'LBL_DELETED' => 'Smazáno',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_EDIT_BUTTON' => 'Upravit',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Jméno',
  'LBL_MODIFIED' => 'Změnil',
  'LBL_MODIFIED_ID' => 'Změněno podle ID',
  'LBL_MODIFIED_NAME' => 'Změněno kým:',
  'LBL_MODIFIED_USER' => 'Změněno uživatelem',
  'LBL_NAME' => 'Jméno',
  'LBL_REMOVE' => 'Odstranit',
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Výchozí primární tým',
  'LBL_TEAM' => 'Týmy',
  'LBL_TEAMS' => 'Týmy',
  'LBL_TEAM_ID' => 'ID týmu',
  'LBL_TEAM_SET' => 'Nastavení týmu',
  'LBL_LIST_FORM_TITLE' => 'Product Rule Seznam',
  'LBL_MODULE_NAME' => 'Product Rule',
  'LBL_MODULE_TITLE' => 'Product Rule',
  'LBL_HOMEPAGE_TITLE' => 'Moje Product Rule',
  'LNK_NEW_RECORD' => 'Přidat Product Rule',
  'LNK_LIST' => 'Zobrazit Product Rule',
  'LNK_IMPORT_GC_PRODUCTRULE' => 'Import Product Rule',
  'LBL_SEARCH_FORM_TITLE' => 'Hledat Product Rule',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_GC_PRODUCTRULE_SUBPANEL_TITLE' => 'Product Rule',
  'LBL_NEW_FORM_TITLE' => 'Nový Product Rule',
  'LBL_LINE_ITEM_ID' => 'Line Item Id',
  'LBL_RULE_UUID' => 'Rule UUID ',
);