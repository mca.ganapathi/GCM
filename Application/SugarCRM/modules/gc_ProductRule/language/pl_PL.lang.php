<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Przydzielono do',
  'LBL_ASSIGNED_TO_NAME' => 'Użytkownik',
  'LBL_CREATED' => 'Utworzone przez',
  'LBL_CREATED_ID' => 'ID tworzącego',
  'LBL_CREATED_USER' => 'Utworzone przez użytkownika',
  'LBL_DATE_ENTERED' => 'Data utworzenia',
  'LBL_DATE_MODIFIED' => 'Data modyfikacji',
  'LBL_DELETED' => 'Usunięto',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_EDIT_BUTTON' => 'Edytuj',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nazwa',
  'LBL_MODIFIED' => 'Zmodyfikowane przez',
  'LBL_MODIFIED_ID' => 'ID modyfikującego',
  'LBL_MODIFIED_NAME' => 'Zmodyfikowano przez',
  'LBL_MODIFIED_USER' => 'Zmodyfikowane przez użytkownika',
  'LBL_NAME' => 'Nazwa',
  'LBL_REMOVE' => 'Usuń',
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Domyślny zespół podstawowy',
  'LBL_TEAM' => 'Zespoły',
  'LBL_TEAMS' => 'Zespoły',
  'LBL_TEAM_ID' => 'ID zespołu',
  'LBL_TEAM_SET' => 'Ustawiono zespół',
  'LBL_LIST_FORM_TITLE' => 'Product Rule Lista',
  'LBL_MODULE_NAME' => 'Product Rule',
  'LBL_MODULE_TITLE' => 'Product Rule',
  'LBL_HOMEPAGE_TITLE' => 'Moje Product Rule',
  'LNK_NEW_RECORD' => 'Utwórz Product Rule',
  'LNK_LIST' => 'Widok Product Rule',
  'LNK_IMPORT_GC_PRODUCTRULE' => 'Import Product Rule',
  'LBL_SEARCH_FORM_TITLE' => 'Wyszukiwanie Product Rule',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historia zmian',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Wydarzenia',
  'LBL_GC_PRODUCTRULE_SUBPANEL_TITLE' => 'Product Rule',
  'LBL_NEW_FORM_TITLE' => 'Nowy Product Rule',
  'LBL_LINE_ITEM_ID' => 'Line Item Id',
  'LBL_RULE_UUID' => 'Rule UUID ',
);