<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruger-id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_ID' => 'Id',
  'LBL_DATE_ENTERED' => 'Oprettet den',
  'LBL_DATE_MODIFIED' => 'Ændret den',
  'LBL_MODIFIED' => 'Ændret af',
  'LBL_MODIFIED_ID' => 'Ændret af id',
  'LBL_MODIFIED_NAME' => 'Ændret af navn',
  'LBL_CREATED' => 'Oprettet af',
  'LBL_CREATED_ID' => 'Oprettet af id',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Oprettet af bruger',
  'LBL_MODIFIED_USER' => 'Ændret af bruger',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_TEAM' => 'Team',
  'LBL_LIST_FORM_TITLE' => 'Product Rule Liste',
  'LBL_MODULE_NAME' => 'Product Rule',
  'LBL_MODULE_TITLE' => 'Product Rule',
  'LBL_HOMEPAGE_TITLE' => 'Min Product Rule',
  'LNK_NEW_RECORD' => 'Opret Product Rule',
  'LNK_LIST' => 'Vis Product Rule',
  'LNK_IMPORT_GC_PRODUCTRULE' => 'Import Product Rule',
  'LBL_SEARCH_FORM_TITLE' => 'Søg Product Rule',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vis historik',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteter',
  'LBL_GC_PRODUCTRULE_SUBPANEL_TITLE' => 'Product Rule',
  'LBL_NEW_FORM_TITLE' => 'Ny Product Rule',
  'LBL_LINE_ITEM_ID' => 'Line Item Id',
  'LBL_RULE_UUID' => 'Rule UUID ',
);