<?php
/* Created by SugarUpgrader for module gc_ProductRule */
$viewdefs['gc_ProductRule']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_ProductRule/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_ProductRule',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_ProductRule',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_ProductRule',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=gc_ProductRule',
    'label' => 'LNK_IMPORT_GC_PRODUCTRULE',
    'acl_action' => 'import',
    'acl_module' => 'gc_ProductRule',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
