<?php
/* Created by SugarUpgrader for module fm_Factory */
$viewdefs['fm_Factory']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#fm_Factory/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'fm_Factory',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#fm_Factory',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'fm_Factory',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=fm_Factory',
    'label' => 'LNK_IMPORT_FM_FACTORY',
    'acl_action' => 'import',
    'acl_module' => 'fm_Factory',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
