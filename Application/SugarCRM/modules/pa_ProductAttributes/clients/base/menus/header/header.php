<?php
/* Created by SugarUpgrader for module pa_ProductAttributes */
$viewdefs['pa_ProductAttributes']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#pa_ProductAttributes/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'pa_ProductAttributes',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#pa_ProductAttributes',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'pa_ProductAttributes',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=pa_ProductAttributes',
    'label' => 'LNK_IMPORT_PA_PRODUCTATTRIBUTES',
    'acl_action' => 'import',
    'acl_module' => 'pa_ProductAttributes',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
