<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_TEAM' => 'Ομάδες',
  'LBL_TEAMS' => 'Ομάδες',
  'LBL_TEAM_ID' => 'Ταυτότητα Ομάδας',
  'LBL_ASSIGNED_TO_ID' => 'Ταυτότητα Ανατεθειμένου Χρήστη',
  'LBL_ASSIGNED_TO_NAME' => 'Ανατέθηκε σε',
  'LBL_ID' => 'Ταυτότητα',
  'LBL_DATE_ENTERED' => 'Ημερομηνία Δημιουργίας',
  'LBL_DATE_MODIFIED' => 'Ημερομηνία Τροποποίησης',
  'LBL_MODIFIED' => 'Τροποποιήθηκε Από',
  'LBL_MODIFIED_ID' => 'Τροποποιήθηκε Από Ταυτότητα',
  'LBL_MODIFIED_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_CREATED' => 'Δημιουργήθηκε Από',
  'LBL_CREATED_ID' => 'Δημιουργήθηκε Από Ταυτότητα',
  'LBL_DOC_OWNER' => 'Κάτοχος του εγγράφου',
  'LBL_USER_FAVORITES' => 'Αγαπημένα Χρηστών',
  'LBL_DESCRIPTION' => 'Περιγραφή',
  'LBL_DELETED' => 'Διαγράφηκε',
  'LBL_NAME' => 'Όνομα',
  'LBL_CREATED_USER' => 'Δημιουργήθηκε Από Χρήστη',
  'LBL_MODIFIED_USER' => 'Τροποποιήθηκε από Χρήστη',
  'LBL_LIST_NAME' => 'Όνομα',
  'LBL_EDIT_BUTTON' => 'Επεξεργασία',
  'LBL_REMOVE' => 'Αφαίρεση',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_LIST_FORM_TITLE' => 'Sales Representative Λίστα',
  'LBL_MODULE_NAME' => 'Sales Representative',
  'LBL_MODULE_TITLE' => 'Sales Representative',
  'LBL_MODULE_NAME_SINGULAR' => 'Sales Representative',
  'LBL_HOMEPAGE_TITLE' => 'Δική Μου Sales Representative',
  'LNK_NEW_RECORD' => 'Δημιουργία Sales Representative',
  'LNK_LIST' => 'Προβολή Sales Representative',
  'LNK_IMPORT_GC_SALESREPRESENTATIVE' => 'Import Sales Representative',
  'LBL_SEARCH_FORM_TITLE' => 'Αναζήτηση Sales Representative',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Προβολή Ιστορικού',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Δραστηριότητες',
  'LBL_GC_SALESREPRESENTATIVE_SUBPANEL_TITLE' => 'Sales Representative',
  'LBL_NEW_FORM_TITLE' => 'Νέα Sales Representative',
  'LNK_IMPORT_VCARD' => 'Import Sales Representative vCard',
  'LBL_IMPORT' => 'Import Sales Representative',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Sales Representative record by importing a vCard from your file system.',
  'LBL_SALES_REP_NAME_1' => 'Sales Representative  Name (Local Character)',
);