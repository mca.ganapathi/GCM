<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_TEAM' => 'Zespoły',
  'LBL_TEAMS' => 'Zespoły',
  'LBL_TEAM_ID' => 'ID zespołu',
  'LBL_ASSIGNED_TO_ID' => 'Przydzielono do',
  'LBL_ASSIGNED_TO_NAME' => 'Użytkownik',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data utworzenia',
  'LBL_DATE_MODIFIED' => 'Data modyfikacji',
  'LBL_MODIFIED' => 'Zmodyfikowane przez',
  'LBL_MODIFIED_ID' => 'ID modyfikującego',
  'LBL_MODIFIED_NAME' => 'Zmodyfikowano przez',
  'LBL_CREATED' => 'Utworzone przez',
  'LBL_CREATED_ID' => 'ID tworzącego',
  'LBL_DOC_OWNER' => 'Właściciel dokumentu',
  'LBL_USER_FAVORITES' => 'Użytkownicy, którzy dodali rekord do ulubionych',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Usunięto',
  'LBL_NAME' => 'Nazwa',
  'LBL_CREATED_USER' => 'Utworzone przez użytkownika',
  'LBL_MODIFIED_USER' => 'Zmodyfikowane przez użytkownika',
  'LBL_LIST_NAME' => 'Nazwa',
  'LBL_EDIT_BUTTON' => 'Edytuj',
  'LBL_REMOVE' => 'Usuń',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmodyfikowane przez (nazwa)',
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Domyślny zespół podstawowy',
  'LBL_TEAM_SET' => 'Ustawiono zespół',
  'LBL_LIST_FORM_TITLE' => 'Sales Representative Lista',
  'LBL_MODULE_NAME' => 'Sales Representative',
  'LBL_MODULE_TITLE' => 'Sales Representative',
  'LBL_HOMEPAGE_TITLE' => 'Moje Sales Representative',
  'LNK_NEW_RECORD' => 'Utwórz Sales Representative',
  'LNK_LIST' => 'Widok Sales Representative',
  'LNK_IMPORT_GC_SALESREPRESENTATIVE' => 'Import Sales Representative',
  'LBL_SEARCH_FORM_TITLE' => 'Wyszukiwanie Sales Representative',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historia zmian',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Wydarzenia',
  'LBL_GC_SALESREPRESENTATIVE_SUBPANEL_TITLE' => 'Sales Representative',
  'LBL_NEW_FORM_TITLE' => 'Nowy Sales Representative',
  'LBL_SALES_CHANNEL_CODE' => 'Sales Channel Code',
  'LBL_SALES_REP_NAME_1' => 'Sales Representative Name (Latin Character)',
  'LBL_SALES_REP_NAME_2' => 'Sales Representative  Name (Local Character)',
  'LBL_CORPORATE_ID' => 'Corporate Name',
  'LBL_DIVISION_1' => 'Division (Latin Character)',
  'LBL_DIVISION_2' => 'Division (Local Character)',
  'LBL_TITLE_1' => 'Title (Latin Character)',
  'LBL_TITLE_2' => 'Title (Local Character)',
  'LBL_TEL_NO' => 'Telephone number',
  'LBL_EXT_NO' => 'Extension number',
  'LBL_FAX_NO' => 'FAX number',
  'LBL_MOB_NO' => 'Mobile Number',
  'LBL_EMAIL1' => 'Email Address',
  'LBL_CONTRACTS_ID' => 'Contract ID',
  'LBL_SALES_COMPANY_GC_NTTCOMGROUPCOMPANY_ID' => 'Sales Company (related  ID)',
  'LBL_SALES_COMPANY' => 'Sales Company',
  'LBL_MODULE_NAME_SINGULAR' => 'Sales Representative',
  'LNK_IMPORT_VCARD' => 'Import Sales Representative vCard',
  'LBL_IMPORT' => 'Import Sales Representative',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Sales Representative record by importing a vCard from your file system.',
);