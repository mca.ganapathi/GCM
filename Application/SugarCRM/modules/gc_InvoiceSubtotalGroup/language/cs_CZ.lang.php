<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Výchozí primární tým',
  'LBL_TEAM' => 'Týmy',
  'LBL_TEAMS' => 'Týmy',
  'LBL_TEAM_ID' => 'ID týmu',
  'LBL_TEAM_SET' => 'Nastavení týmu',
  'LBL_ASSIGNED_TO_ID' => 'Přidělené ID uživatele',
  'LBL_ASSIGNED_TO_NAME' => 'Uživatel',
  'LBL_CREATED' => 'Vytvořeno',
  'LBL_CREATED_ID' => 'Vytvořeno (ID)',
  'LBL_CREATED_USER' => 'Vytvořeno uživatelem',
  'LBL_DATE_ENTERED' => 'Datum vytvoření',
  'LBL_DATE_MODIFIED' => 'Datum změny',
  'LBL_DELETED' => 'Smazáno',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_EDIT_BUTTON' => 'Upravit',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Jméno',
  'LBL_MODIFIED' => 'Změnil',
  'LBL_MODIFIED_ID' => 'Změněno podle ID',
  'LBL_MODIFIED_NAME' => 'Změněno kým:',
  'LBL_MODIFIED_USER' => 'Změněno uživatelem',
  'LBL_NAME' => 'Jméno',
  'LBL_REMOVE' => 'Odstranit',
  'LBL_LIST_FORM_TITLE' => 'Invoice Subtotal Group Seznam',
  'LBL_MODULE_NAME' => 'Invoice Subtotal Group',
  'LBL_MODULE_TITLE' => 'Invoice Subtotal Group',
  'LBL_HOMEPAGE_TITLE' => 'Moje Invoice Subtotal Group',
  'LNK_NEW_RECORD' => 'Přidat Invoice Subtotal Group',
  'LNK_LIST' => 'Zobrazit Invoice Subtotal Group',
  'LNK_IMPORT_GC_INVOICESUBTOTALGROUP' => 'Import Invoice Subtotal Group',
  'LBL_SEARCH_FORM_TITLE' => 'Hledat Invoice Subtotal Group',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_GC_INVOICESUBTOTALGROUP_SUBPANEL_TITLE' => 'Invoice Subtotal Group',
  'LBL_NEW_FORM_TITLE' => 'Nový Invoice Subtotal Group',
  'LBL_NAME_UUID' => 'Group Name Value UUID',
  'LBL_DESCRIPTION_UUID' => 'Group Description Value UUID',
);