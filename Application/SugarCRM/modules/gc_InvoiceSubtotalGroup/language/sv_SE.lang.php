<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_TEAM' => 'Team',
  'LBL_ASSIGNED_TO_ID' => 'Tilldelad användare',
  'LBL_ASSIGNED_TO_NAME' => 'Tilldelad användare',
  'LBL_CREATED' => 'Skapat Av',
  'LBL_CREATED_ID' => 'Skapat Av',
  'LBL_CREATED_USER' => 'Skapat Av Användare',
  'LBL_DATE_ENTERED' => 'Skapat Datum',
  'LBL_DATE_MODIFIED' => 'Modifierat Datum',
  'LBL_DELETED' => 'Raderad',
  'LBL_DESCRIPTION' => 'Beskrivning',
  'LBL_EDIT_BUTTON' => 'Redigera',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Namn',
  'LBL_MODIFIED' => 'Modifierat Av',
  'LBL_MODIFIED_ID' => 'Modifierat Av Id',
  'LBL_MODIFIED_NAME' => 'Modifierat Av Namn',
  'LBL_MODIFIED_USER' => 'Modifierat Av Användare',
  'LBL_NAME' => 'Namn',
  'LBL_REMOVE' => 'Ta bort',
  'LBL_LIST_FORM_TITLE' => 'Invoice Subtotal Group List',
  'LBL_MODULE_NAME' => 'Invoice Subtotal Group',
  'LBL_MODULE_TITLE' => 'Invoice Subtotal Group',
  'LBL_HOMEPAGE_TITLE' => 'My Invoice Subtotal Group',
  'LNK_NEW_RECORD' => 'Create Invoice Subtotal Group',
  'LNK_LIST' => 'Visa Invoice Subtotal Group',
  'LNK_IMPORT_GC_INVOICESUBTOTALGROUP' => 'Import Invoice Subtotal Group',
  'LBL_SEARCH_FORM_TITLE' => 'Search Invoice Subtotal Group',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_GC_INVOICESUBTOTALGROUP_SUBPANEL_TITLE' => 'Invoice Subtotal Group',
  'LBL_NEW_FORM_TITLE' => 'New Invoice Subtotal Group',
  'LBL_NAME_UUID' => 'Group Name Value UUID',
  'LBL_DESCRIPTION_UUID' => 'Group Description Value UUID',
);