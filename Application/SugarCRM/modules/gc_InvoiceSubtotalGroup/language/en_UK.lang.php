<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Default Primary Team',
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_TEAM_SET' => 'Team Set',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_DELETED' => 'Deleted',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Name',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_NAME' => 'Name',
  'LBL_REMOVE' => 'Remove',
  'LBL_LIST_FORM_TITLE' => 'Invoice Subtotal Group List',
  'LBL_MODULE_NAME' => 'Invoice Subtotal Group',
  'LBL_MODULE_TITLE' => 'Invoice Subtotal Group',
  'LBL_HOMEPAGE_TITLE' => 'My Invoice Subtotal Group',
  'LNK_NEW_RECORD' => 'Create Invoice Subtotal Group',
  'LNK_LIST' => 'View Invoice Subtotal Group',
  'LNK_IMPORT_GC_INVOICESUBTOTALGROUP' => 'Import Invoice Subtotal Group',
  'LBL_SEARCH_FORM_TITLE' => 'Search Invoice Subtotal Group',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_GC_INVOICESUBTOTALGROUP_SUBPANEL_TITLE' => 'Invoice Subtotal Group',
  'LBL_NEW_FORM_TITLE' => 'New Invoice Subtotal Group',
  'LBL_NAME_UUID' => 'Group Name Value UUID',
  'LBL_DESCRIPTION_UUID' => 'Group Description Value UUID',
);