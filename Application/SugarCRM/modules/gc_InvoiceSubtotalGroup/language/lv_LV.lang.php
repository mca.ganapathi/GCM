<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Noklusētā primārā darba grupa',
  'LBL_TEAM' => 'Darba grupa',
  'LBL_TEAMS' => 'Darba grupas',
  'LBL_TEAM_ID' => 'Darba grupas ID',
  'LBL_TEAM_SET' => 'Darba grupu kopa',
  'LBL_ASSIGNED_TO_ID' => 'Piešķirts lietotājam ar Id',
  'LBL_ASSIGNED_TO_NAME' => 'Piešķirts lietotājam',
  'LBL_CREATED' => 'Izveidoja',
  'LBL_CREATED_ID' => 'Izveidotāja ID',
  'LBL_CREATED_USER' => 'Izveidoja',
  'LBL_DATE_ENTERED' => 'Izveidots',
  'LBL_DATE_MODIFIED' => 'Modificēts',
  'LBL_DELETED' => 'Dzēsts',
  'LBL_DESCRIPTION' => 'Apraksts',
  'LBL_EDIT_BUTTON' => 'Rediģēt',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nosaukums',
  'LBL_MODIFIED' => 'Modificēja',
  'LBL_MODIFIED_ID' => 'Modificētāja ID',
  'LBL_MODIFIED_NAME' => 'Modificēja',
  'LBL_MODIFIED_USER' => 'Modificēja lietotājs',
  'LBL_NAME' => 'Nosaukums',
  'LBL_REMOVE' => 'Noņemt',
  'LBL_LIST_FORM_TITLE' => 'Invoice Subtotal Group Saraksts',
  'LBL_MODULE_NAME' => 'Invoice Subtotal Group',
  'LBL_MODULE_TITLE' => 'Invoice Subtotal Group',
  'LBL_HOMEPAGE_TITLE' => 'Mans Invoice Subtotal Group',
  'LNK_NEW_RECORD' => 'Izveidot Invoice Subtotal Group',
  'LNK_LIST' => 'Skats Invoice Subtotal Group',
  'LNK_IMPORT_GC_INVOICESUBTOTALGROUP' => 'Import Invoice Subtotal Group',
  'LBL_SEARCH_FORM_TITLE' => 'Meklēt Invoice Subtotal Group',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Aplūkot vēsturi',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Darbības',
  'LBL_GC_INVOICESUBTOTALGROUP_SUBPANEL_TITLE' => 'Invoice Subtotal Group',
  'LBL_NEW_FORM_TITLE' => 'Jauns Invoice Subtotal Group',
  'LBL_NAME_UUID' => 'Group Name Value UUID',
  'LBL_DESCRIPTION_UUID' => 'Group Description Value UUID',
);