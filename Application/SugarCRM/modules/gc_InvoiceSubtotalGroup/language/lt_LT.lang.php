<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

$mod_strings = array (
  'LBL_TEAM' => 'Komandos',
  'LBL_TEAMS' => 'Komandos',
  'LBL_TEAM_ID' => 'Komandos Id',
  'LBL_ASSIGNED_TO_ID' => 'Atsakingo Id',
  'LBL_ASSIGNED_TO_NAME' => 'Atsakingas',
  'LBL_CREATED' => 'Sukūrė',
  'LBL_CREATED_ID' => 'Kūrėjo Id',
  'LBL_CREATED_USER' => 'Sukūrė',
  'LBL_DATE_ENTERED' => 'Sukurta',
  'LBL_DATE_MODIFIED' => 'Redaguota',
  'LBL_DELETED' => 'Ištrintas',
  'LBL_DESCRIPTION' => 'Aprašymas',
  'LBL_EDIT_BUTTON' => 'Redaguoti',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Pavadinimas',
  'LBL_MODIFIED' => 'Redagavo',
  'LBL_MODIFIED_ID' => 'Redaguotojo Id',
  'LBL_MODIFIED_NAME' => 'Redaguotojo vardas',
  'LBL_MODIFIED_USER' => 'Redagavo',
  'LBL_NAME' => 'Pavadinimas',
  'LBL_REMOVE' => 'Išimti',
  'LBL_LIST_FORM_TITLE' => 'Invoice Subtotal Group Sąrašas',
  'LBL_MODULE_NAME' => 'Invoice Subtotal Group',
  'LBL_MODULE_TITLE' => 'Invoice Subtotal Group',
  'LBL_HOMEPAGE_TITLE' => 'Mano Invoice Subtotal Group',
  'LNK_NEW_RECORD' => 'Sukurti Invoice Subtotal Group',
  'LNK_LIST' => 'View Invoice Subtotal Group',
  'LNK_IMPORT_GC_INVOICESUBTOTALGROUP' => 'Import Invoice Subtotal Group',
  'LBL_SEARCH_FORM_TITLE' => 'Paieška Invoice Subtotal Group',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Rodyti istoriją',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Priminimai',
  'LBL_GC_INVOICESUBTOTALGROUP_SUBPANEL_TITLE' => 'Invoice Subtotal Group',
  'LBL_NEW_FORM_TITLE' => 'Naujas Invoice Subtotal Group',
  'LBL_NAME_UUID' => 'Group Name Value UUID',
  'LBL_DESCRIPTION_UUID' => 'Group Description Value UUID',
);