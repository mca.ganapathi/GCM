<?php
/* Created by SugarUpgrader for module gc_InvoiceSubtotalGroup */
$viewdefs['gc_InvoiceSubtotalGroup']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_InvoiceSubtotalGroup/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_InvoiceSubtotalGroup',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_InvoiceSubtotalGroup',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_InvoiceSubtotalGroup',
    'icon' => 'fa-bars',
  ),
);
