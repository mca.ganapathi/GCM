<?php
/* Created by SugarUpgrader for module gc_Line_Item_Config_Summary */
$viewdefs['gc_Line_Item_Config_Summary']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_Line_Item_Config_Summary/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_Line_Item_Config_Summary',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_Line_Item_Config_Summary',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_Line_Item_Config_Summary',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=gc_Line_Item_Config_Summary',
    'label' => 'LNK_IMPORT_GC_LINE_ITEM_CONFIG_SUMMARY',
    'acl_action' => 'import',
    'acl_module' => 'gc_Line_Item_Config_Summary',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
