<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_ASSIGNED_TO_ID' => 'Zugewiesene BenutzerID',
  'LBL_ASSIGNED_TO_NAME' => 'Zugewiesen an',
  'LBL_CREATED' => 'Erstellt von:',
  'LBL_CREATED_ID' => 'Erstellt von ID:',
  'LBL_CREATED_USER' => 'Erstellt von Benutzer:',
  'LBL_DATE_ENTERED' => 'Erstellt am:',
  'LBL_DATE_MODIFIED' => 'Geändert am',
  'LBL_DELETED' => 'Gelöscht',
  'LBL_DESCRIPTION' => 'Beschreibung',
  'LBL_EDIT_BUTTON' => 'Bearbeiten',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Name',
  'LBL_MODIFIED' => 'Geändert von',
  'LBL_MODIFIED_ID' => 'Geändert von ID',
  'LBL_MODIFIED_NAME' => 'Geändert von Name',
  'LBL_MODIFIED_USER' => 'Geändert von',
  'LBL_NAME' => 'Name',
  'LBL_REMOVE' => 'Entfernen',
  'LBL_LIST_FORM_TITLE' => 'Account Transfer Liste',
  'LBL_MODULE_NAME' => 'Account Transfer',
  'LBL_MODULE_TITLE' => 'Account Transfer',
  'LBL_HOMEPAGE_TITLE' => 'Mein Account Transfer',
  'LNK_NEW_RECORD' => 'Erstellen Account Transfer',
  'LNK_LIST' => 'Ansicht Account Transfer',
  'LNK_IMPORT_GC_ACCOUNTTRANSFER' => 'Import Account Transfer',
  'LBL_SEARCH_FORM_TITLE' => 'Suchen Account Transfer',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Verlauf ansehen',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitäten',
  'LBL_GC_ACCOUNTTRANSFER_SUBPANEL_TITLE' => 'Account Transfer',
  'LBL_NEW_FORM_TITLE' => 'Neu Account Transfer',
  'LBL_CONTRACT_HISTORY_ID' => 'Contract Line Item History Id',
  'LBL_BANK_CODE' => 'Bank Code',
  'LBL_BRANCH_CODE' => 'Branch Code',
  'LBL_DEPOSIT_TYPE' => 'Deposit Type',
  'LBL_ACCOUNT_NO' => 'account no',
  'LBL_ACCOUNT_NO_DISPLAY' => 'Account Number Display',
);