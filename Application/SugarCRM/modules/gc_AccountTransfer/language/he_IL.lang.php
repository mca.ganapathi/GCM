<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_TEAM' => 'צוותים',
  'LBL_TEAMS' => 'צוותים',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'משתמש שהוקצה Id',
  'LBL_ASSIGNED_TO_NAME' => 'משתמש',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'נוצר בתאריך',
  'LBL_DATE_MODIFIED' => 'שונה בתאריך',
  'LBL_MODIFIED' => 'שונה על ידי',
  'LBL_MODIFIED_ID' => 'שונה על ידי Id',
  'LBL_MODIFIED_NAME' => 'שונה על ידי ששמו',
  'LBL_CREATED' => 'נוצר על ידי',
  'LBL_CREATED_ID' => 'נוצר על ידי Id',
  'LBL_DESCRIPTION' => 'תיאור',
  'LBL_DELETED' => 'נמחק',
  'LBL_NAME' => 'שם',
  'LBL_CREATED_USER' => 'נוצר על ידי משתמש',
  'LBL_MODIFIED_USER' => 'שונה על ידי משתמש',
  'LBL_LIST_NAME' => 'שם',
  'LBL_LIST_FORM_TITLE' => 'Account Transfer List',
  'LBL_MODULE_NAME' => 'Account Transfer',
  'LBL_MODULE_TITLE' => 'Account Transfer',
  'LBL_HOMEPAGE_TITLE' => 'My Account Transfer',
  'LNK_NEW_RECORD' => 'Create Account Transfer',
  'LNK_LIST' => 'View Account Transfer',
  'LNK_IMPORT_GC_ACCOUNTTRANSFER' => 'Import Account Transfer',
  'LBL_SEARCH_FORM_TITLE' => 'Search Account Transfer',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_GC_ACCOUNTTRANSFER_SUBPANEL_TITLE' => 'Account Transfer',
  'LBL_NEW_FORM_TITLE' => 'New Account Transfer',
  'LBL_CONTRACT_HISTORY_ID' => 'Contract Line Item History Id',
  'LBL_BANK_CODE' => 'Bank Code',
  'LBL_BRANCH_CODE' => 'Branch Code',
  'LBL_DEPOSIT_TYPE' => 'Deposit Type',
  'LBL_ACCOUNT_NO' => 'account no',
  'LBL_ACCOUNT_NO_DISPLAY' => 'Account Number Display',
);