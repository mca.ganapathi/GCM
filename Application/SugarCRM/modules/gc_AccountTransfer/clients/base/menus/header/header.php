<?php
/* Created by SugarUpgrader for module gc_AccountTransfer */
$viewdefs['gc_AccountTransfer']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_AccountTransfer/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_AccountTransfer',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_AccountTransfer',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_AccountTransfer',
    'icon' => 'fa-bars',
  ),
  2 => 
  array (
    'route' => '#bwc/index.php?module=Import&action=Step1&import_module=gc_AccountTransfer',
    'label' => 'LNK_IMPORT_GC_ACCOUNTTRANSFER',
    'acl_action' => 'import',
    'acl_module' => 'gc_AccountTransfer',
    'icon' => 'fa-arrow-circle-o-up',
  ),
);
