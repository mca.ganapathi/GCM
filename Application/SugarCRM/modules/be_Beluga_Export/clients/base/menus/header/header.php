<?php
/* Created by SugarUpgrader for module be_Beluga_Export */
$viewdefs['be_Beluga_Export']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#be_Beluga_Export/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'be_Beluga_Export',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#be_Beluga_Export',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'be_Beluga_Export',
    'icon' => 'fa-bars',
  ),
);
