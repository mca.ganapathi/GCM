<?php
/**
 *  Common functions and constants for RESTful Proxy Modules.
 *
 *  # These functions and constants are depends on the particular application logic.
 */

$HTTP_RESPONSE_CODE_MAPPING = array(

    '1001' => 201,   //  Create was success
    '2001' => 400,   //  Cannot process blank data.
    '2002' => 400,   //  Cannot create Contract Record. Client Request ID already present in the system.
    '2004' => 400,   //  Contract is already in Deactivated stage.
    '2005' => 400,   //  Contract Id is Empty.
    '2006' => 400,   //  Cannot update Deactivated Contract
    '2011' => 400,   //  Product ID is Empty.
    '2012' => 400,   //  Corporate Name is Empty.
    '2013' => 400,   //  Created User is Empty.
    '2014' => 400,   //  Billing Start Date is Empty.
    '2015' => 400,   //  Billing End Date is Empty.
    '2016' => 400,   //  Service Start Date is Empty.
    '2017' => 400,   //  Service End Date is Empty.
    '2018' => 400,   //  Corporate ID is Empty.
    '2019' => 400,   //  Billing Corporate Name is Empty.
    '2021' => 400,   //  Sales Channel Code is Empty.
    '2022' => 400,   //  Billing Country Code is Empty.
    '2023' => 400,   //  Billing Postal Code is Empty.
    '2024' => 400,   //  Billing Address is Empty.
    '2025' => 400,   //  Billing Address Line is Empty.
    '2030' => 400,   //  Billing Corporate Name field validation error. The max length is 20.
    '2031' => 400,   //  Billing Corporate Name Furigana field validation error. The max length is 35.
    '2032' => 400,   //  Contract Country Code is Empty.
    '2033' => 400,   //  Billing Corporate Name Furigana is Empty.
    '3002' => 400,   //  XML data not found.
    '3004' => 400,   //  XSD Validation failed.
    '3005' => 400,   //  Contract process terminated. Contracting company record not found.
    '3001' => 401,   //  Invalid Session Id.
    '2003' => 404,   //  Cannot find requested Contract Record Id into the GCM System.
    '3003' => 500,   //  CIDAS Connection failed.
    'E001' => 500,   //  The number of search result exceeds threshold or the company name and the company name in English after cleansing is blank.
    'E002' => 500,   //  Wrong authentication pass code.
    'E003' => 500,   //  Wrong GET parameter.
    'E004' => 500,   //  System error.
);


/**
 * Returns response status code from application message code.
 * @param  $messageCode Message code by application (integer)
 * @return HTTP response status code (integer)
 */
function getResponseStatusCode($messageCode)
{
    global $HTTP_RESPONSE_CODE_MAPPING;
    if(array_key_exists($messageCode, $HTTP_RESPONSE_CODE_MAPPING)) {
        return $HTTP_RESPONSE_CODE_MAPPING[$messageCode];
    } else {
        // GCM error code is not mapped with http status code
        return 500;
    }
}