<?php
require_once(API_ROOT.'/sugar_api_proxy.php');
require_once('common_application.php');

if(!defined('sugarEntry'))define('sugarEntry', true);

class ContractInformation extends SugarApiProxy {

 	const REST_SCHEME_HOST = 'http://localhost';
	const REST_SELFPATH = '/api/v1/contract-information/';
	const REST_REALPATH = '/custom/service/v4_1_custom/rest.php';

	const REST_FUNCTION_POST   = 'fnContractRegistration';
	const REST_FUNCTION_PATCH  = 'fnContractUpdate';
	const REST_FUNCTION_GET    = 'fnContractBrowse';
	const REST_FUNCTION_DELETE = 'fnContractDeactivate';

	const ERR_IDENTIFIER = 'exp-v0:exception';


	protected function getErrorResponseBody($statusCode) {
		require_once (API_ROOT . '/../custom/include/XMLUtils.php');
		$XMLUtilsObj = new XMLUtils();

		$errorBody = '';
		switch($statusCode) {
			case 405: $errorBody=$XMLUtilsObj->fnGetErrorXMLData(array('4002'), null, false); break;
			case 500: $errorBody=$XMLUtilsObj->fnGetErrorXMLData(array('5004'), null, false); break;
			default:  break;
		}
		return $errorBody;
	}

	/**
	 * Calls SugarCRM's REST(fnContractBrowse)
	 * @param array  $headers HTTP request headers(key => value)
	 * @param string $body empty string (not use)
	 * @return string BrowseAPI's response(HTTP response header and response body)
	 */
	protected function executeGet($headers, $body)
	{
		$httpRequestHeadersCase = array_change_key_case($headers, CASE_LOWER);
		$sessionId = $httpRequestHeadersCase[parent::HEADER_SESSION_ID];

		$objectId  = self::extractParameterInRequestPath();
		$contractUUID = $contractGID = '';
		if(mb_strlen($objectId , 'UTF-8') > 15) {
			$contractUUID = $objectId;
		} else {
			$contractGID  = $objectId;
		}

		// Call SugarCRM's REST
		$httpClient = curl_init(self::REST_SCHEME_HOST . self::REST_REALPATH);
		$restData = json_encode(array($sessionId, $contractGID, $contractUUID));

		$postFields = array("method"        => self::REST_FUNCTION_GET,
		                    "input_type"    => "JSON",
		                    "response_type" => "XML",
		                    "rest_data"     => $restData
		);
		curl_setopt($httpClient, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($httpClient, CURLOPT_HTTPHEADER, array('Expect:')); // Header suppression "Expect: 100-continue"
		curl_setopt($httpClient, CURLOPT_HEADER, true);  // If this flag is true, header information is included in response string.
		curl_setopt($httpClient, CURLOPT_POST, true);
		curl_setopt($httpClient, CURLOPT_POSTFIELDS, $postFields);

		//  Execute HTTP request and read response
		$responseContents = curl_exec($httpClient);
		curl_close($httpClient);

		return $responseContents;
	}

	/**
	 * Process the response header from SugarCRM's REST(fnContractBrowse) result
	 * (Excludes 'Content-Type' header)
	 * @param $headers HTTP headers (string[] ref.)
	 * @param $body    HTTP body (string)
	 * @return none
	 */
	protected function processResultGet(&$headers, $body)
	{
		$errorOccured = mb_strpos($body, self::ERR_IDENTIFIER);
		if($errorOccured) {

			$messageCodes = array();
			$sxe = new SimpleXMLElement($body);
			$messageCodeElements = $sxe->xpath('//exp-v0:messageCode');
			foreach($messageCodeElements as $messageCodeElement) {
				$messageCodes[] = (string)$messageCodeElement;
			}
			if(empty($messageCodes)) return;  // if messageCode element value is empty, can't determines status.

			$messageCode = (string)$messageCodes[0]; // Determines using first element value.
			$httpStatusCode = getResponseStatusCode($messageCode);

			$headers[0] = buildHttpResponseStatus($httpStatusCode);
		}
	}


	/**
	 * Calls SugarCRM's REST(fnContractRegistration)
	 * @param array  $headers HTTP request headers(key => value)
	 * @param string $body request information(XML)
	 * @return string BrowseAPI's response(HTTP response header and response body)
	 */
	protected function executePost($headers, $body)
	{
		$httpRequestHeadersCase = array_change_key_case($headers, CASE_LOWER);
		$sessionId = $httpRequestHeadersCase[parent::HEADER_SESSION_ID];

		// Call SugarCRM's REST
		$httpClient = curl_init(self::REST_SCHEME_HOST . self::REST_REALPATH);
		$restData = json_encode(array($sessionId, $body));

		$postFields = array("method" => self::REST_FUNCTION_POST,
		                    "input_type"    => "JSON",
		                    "response_type" => "XML",
		                    "rest_data"     => $restData
		);
		curl_setopt($httpClient, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($httpClient, CURLOPT_HTTPHEADER, array('Expect:')); // Header suppression "Expect: 100-continue"
		curl_setopt($httpClient, CURLOPT_HEADER, true);  // If this flag is true, header information is included in response string.
		curl_setopt($httpClient, CURLOPT_POST, true);
		curl_setopt($httpClient, CURLOPT_POSTFIELDS, $postFields);

		//  Execute HTTP request and read response
		$responseContents = curl_exec($httpClient);
		curl_close($httpClient);

		return $responseContents;
	}

	/**
	 * Process the response header from SugarCRM's REST(fnContractRegistration) result
	 * (Excludes 'Content-Type' header)
	 * @param $headers HTTP headers (string[] ref.)
	 * @param $body    HTTP body (string)
	 * @return none
	 */
	protected function processResultPost(&$headers, $body)
	{
		// 1.Determines the registration has succeeded or failed.
		$errorOccured = mb_strpos($body, self::ERR_IDENTIFIER);

		// 2.Divides the process
		if(!$errorOccured) { // 2-1.Registration succeeded

			// 2-1.(1)Modify HTTP status code to 201(Created)
			$headers[0] = buildHttpResponseStatus(201);

			// 2-1.(2)Add Location Header
			$productContractObjectId = '';
			$sxe = new SimpleXMLElement($body);
			$productContractObjectIdElements = $sxe->xpath('/res-v0:response/bi-v0:productContract/cbe-v0:identifiedBy/bi-v0:productContractKey/cbe-v0:objectID');
			if(!empty($productContractObjectIdElements)) {
				$productContractObjectId = (string)$productContractObjectIdElements[0];
			}

			global $sugar_config;
			$locationHeaderValue = $sugar_config['site_url'];
			$lastChar = mb_substr($locationHeaderValue, -1);
			$locationHeaderValue .= ($lastChar=='/' ? mb_substr(self::REST_SELFPATH, 1) : self::REST_SELFPATH);
			$locationHeaderValue .= $productContractObjectId;

			modifyHeaderValue($headers, 'Location', $locationHeaderValue);
		}
		else {  // 2-2.Registration failed

			// 2-2.(1)Determines HTTP status code from (Application's) messageCode.
			$messageCodes = array();
			$sxe = new SimpleXMLElement($body);
			$messageCodeElements = $sxe->xpath('//exp-v0:messageCode');
			foreach($messageCodeElements as $messageCodeElement) {
				$messageCodes[] = (string)$messageCodeElement;
			}
			if(empty($messageCodes)) return;  // if messageCode element value is empty, can't determines status.

			$messageCode = (string)$messageCodes[0]; // Determines using first element value.
			$httpStatusCode = getResponseStatusCode($messageCode);

			// 2-2.(2)Modify HTTP status code.
			$headers[0] = buildHttpResponseStatus($httpStatusCode);
		}
	}

	/**
	 * Calls SugarCRM's REST(fnContractUpdate)
	 * @param array  $headers HTTP request headers(key => value)
	 * @param string $body request information(XML)
	 * @return string BrowseAPI's response(HTTP response header and response body)
	 */
	protected function executePatch($headers, $body)
	{
		$httpRequestHeadersCase = array_change_key_case($headers, CASE_LOWER);
		$sessionId = $httpRequestHeadersCase[parent::HEADER_SESSION_ID];
		$objectId  = self::extractParameterInRequestPath();

		// Call SugarCRM's REST
		$httpClient = curl_init(self::REST_SCHEME_HOST . self::REST_REALPATH);
		$restData = json_encode(array($sessionId, $objectId, $body));

		$postFields = array("method"        => self::REST_FUNCTION_PATCH,
		                    "input_type"    => "JSON",
		                    "response_type" => "XML",
		                    "rest_data"     => $restData
		);
		curl_setopt($httpClient, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($httpClient, CURLOPT_HTTPHEADER, array('Expect:')); // Header suppression "Expect: 100-continue"
		curl_setopt($httpClient, CURLOPT_HEADER, true);  // If this flag is true, header information is included in response string.
		curl_setopt($httpClient, CURLOPT_POST, true);
		curl_setopt($httpClient, CURLOPT_POSTFIELDS, $postFields);

		//  Execute HTTP request and read response
		$responseContents = curl_exec($httpClient);
		curl_close($httpClient);

		return $responseContents;
	}

	/**
	 * Process the response header from SugarCRM's REST(fnContractUpdate) result
	 * (Excludes 'Content-Type' header)
	 * @param $headers HTTP headers (string[] ref.)
	 * @param $body    HTTP body (string)
	 * @return none
	 */
	protected function processResultPatch(&$headers, $body)
	{
		$errorOccured = mb_strpos($body, self::ERR_IDENTIFIER);

		if($errorOccured) {

			$messageCodes = array();
			$sxe = new SimpleXMLElement($body);
			$messageCodeElements = $sxe->xpath('//exp-v0:messageCode');
			foreach($messageCodeElements as $messageCodeElement) {
				$messageCodes[] = (string)$messageCodeElement;
			}
			if(empty($messageCodes)) return;  // if messageCode element value is empty, can't determines status.

			$messageCode = (string)$messageCodes[0]; // Determines using first element value.
			$httpStatusCode = getResponseStatusCode($messageCode);

			$headers[0] = buildHttpResponseStatus($httpStatusCode);
		}
	}


	/**
	 * Calls SugarCRM's REST(fnContractDeactivate)
	 * @param array  $headers HTTP request headers(key => value)
	 * @param string $body empty string (not use)
	 * @return string BrowseAPI's response(HTTP response header and response body)
	 */
	protected function executeDelete($headers, $body)
	{
		$httpRequestHeadersCase = array_change_key_case($headers, CASE_LOWER);
		$sessionId = $httpRequestHeadersCase[parent::HEADER_SESSION_ID];
		$deleteId  = self::extractParameterInRequestPath();

		// Call SugarCRM's REST
		$httpClient = curl_init(self::REST_SCHEME_HOST . self::REST_REALPATH);
		$restData = json_encode(array($sessionId, $deleteId));

		$postFields = array("method"        => self::REST_FUNCTION_DELETE,
		                    "input_type"    => "JSON",
		                    "response_type" => "XML",
		                    "rest_data"     => $restData
		);
		curl_setopt($httpClient, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($httpClient, CURLOPT_HTTPHEADER, array('Expect:')); // Header suppression "Expect: 100-continue"
		curl_setopt($httpClient, CURLOPT_HEADER, true);  // If this flag is true, header information is included in response string.
		curl_setopt($httpClient, CURLOPT_POST, true);
		curl_setopt($httpClient, CURLOPT_POSTFIELDS, $postFields);

		//  Execute HTTP request and read response
		$responseContents = curl_exec($httpClient);
		curl_close($httpClient);

		return $responseContents;
	}

	/**
	 * Process the response header from SugarCRM's REST(fnContractDeactivate) result
	 * (Excludes 'Content-Type' header)
	 * @param $headers HTTP headers (string[] ref.)
	 * @param $body    HTTP body (string)
	 * @return none
	 */
	protected function processResultDelete(&$headers, $body)
	{
		$errorOccured = mb_strpos($body, self::ERR_IDENTIFIER);

		if($errorOccured) {

			$messageCodes = array();
			$sxe = new SimpleXMLElement($body);
			$messageCodeElements = $sxe->xpath('//exp-v0:messageCode');
			foreach($messageCodeElements as $messageCodeElement) {
				$messageCodes[] = (string)$messageCodeElement;
			}
			if(empty($messageCodes)) return;  // if messageCode element value is empty, can't determines status.

			$messageCode = (string)$messageCodes[0]; // Determines using first element value.
			$httpStatusCode = getResponseStatusCode($messageCode);

			$headers[0] = buildHttpResponseStatus($httpStatusCode);
		}
	}


	private static function extractParameterInRequestPath()
	{
		$result = substr($_SERVER["REQUEST_URI"], strlen(self::REST_SELFPATH));
		if(!empty($_SERVER['QUERY_STRING'])) {
			if(strpos($result, 'contractID') !== false ) {
				$arrResult = array();
				$arrResult = explode('=',$result);
				$result = $arrResult[1];
				unset($arrResult);
			} else {
				$result = substr($result, 0, strlen($result)-(strlen($_SERVER['QUERY_STRING'])+1)); // +1 is Query Separator:'?'
			}
		}
		return $result;
	}

}
