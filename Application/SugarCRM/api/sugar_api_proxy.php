<?php
abstract class SugarApiProxy extends RestfulProxy {

	/** HTTP header name of OAuth-SessionID */
	const HEADER_SESSION_ID = 'session-id';

	const CONTENT_TYPE_XML  = 'text/xml';
	const CONTENT_TYPE_JSON = 'application/json';
	const CONTENT_TYPE_CSV_UTF8  = 'text/csv; charset=utf-8';

	/**
	 * (non-PHPdoc)
	 * @see RestfulProxy::getContentType()
	 * @param  1 : API Request Type ($apiRequestType) 1 => Contract Information ; 2 => GBS API
	 * @param  2 : Respponse Status Code
	 * @return Content Type
	 */
	protected function getContentType($apiRequestType, $responseStatusCode) {
		$returnContentType = '';
		if($apiRequestType == 2 && $responseStatusCode == 200) {
			$returnContentType = self::CONTENT_TYPE_CSV_UTF8; // CSV 
		} else {
			$returnContentType = self::CONTENT_TYPE_XML;  // Default is XML
		}
		return $returnContentType;
	}
}