<?php
require_once(API_ROOT.'/sugar_api_proxy.php');
require_once('common_application.php');

if(!defined('sugarEntry'))define('sugarEntry', true);

class ExportCsvForGb extends SugarApiProxy {

 	const REST_SCHEME_HOST = 'http://localhost';	
	const REST_PATH_SEARCH = '/api/v2/export-csv-for-gbs';
	const REST_PATH_REAL   = '/custom/service/v4_1_custom/rest.php';

	const PARAM_KEY_TYPE = 'type';
	const PARAM_KEY_DATE_AFTER = 'date-after';
	const PARAM_KEY_DATE_BEFORE = 'date-before';

	const REST_FUNCTION_GET = 'fnGBSContractBrowse';

	const ERR_IDENTIFIER = 'exp:exceptions';

	protected function getErrorResponseBody($statusCode, $requestMethod) {
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'statusCode: '.$statusCode, 'Function to return the error response body');
		require_once (API_ROOT . '/../custom/include/XMLUtils.php');
		$XMLUtilsObj = new XMLUtils();

		$errorBody = '';
		/**
		 * Api Validation Helper
		 */
		include API_ROOT . '/../custom/service/v4_1_custom/APIHelper/v2/helpers/ApiValidationHelper.php';
		$xpath_ids = [];
		if($statusCode == '405') {
			$xpath_ids = ['http_method' => $requestMethod];
			$api_config_details = [
	            'field'   => 'http_method_gbs', // field
	            'type'    => 'invalid', // type
	            'methods' => 'get', // methods
	        ];
		} else if($statusCode == '500') {
			$xpath_ids = [];
			$api_config_details = [
	            'field'   => 'internal_server_error_gbs', // field
	            'type'    => 'invalid', // type
	            'methods' => 'get', // methods
	        ];
		}
        $error = ApiValidationHelper::generateError($api_config_details, $xpath_ids);

        if (!empty($error)) {
            $error_codes = '';
            $error_messages = '';
            $error_details = [];

            $error_codes = $error['codes'];
            $error_messages = $error['messages'];
            if(!empty($error['details'])) {
                $error_details = $error['details'];
            } else {
                $error_details = [];
            }

            $errors['codes'][] = $error_codes;
            $errors['messages'][$error_codes][] = $error_messages;
            $errors['details'][$error_codes][] = $error_details;
        }

		switch($statusCode) {
			case 405: $errorBody=$XMLUtilsObj->fnGetErrorXMLDataV2($errors); break;
			case 500: $errorBody=$XMLUtilsObj->fnGetErrorXMLDataV2($errors);; break;
			default:  break;
		}
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'errorBody: '.$errorBody, '');
		return $errorBody;
	}


	/**
	 * Calls SugarCRM's REST(fnContractBrowseV2)
	 * @param array  $headers HTTP request headers(key => value)
	 * @param string $body empty string (not use)
	 * @return string BrowseAPI's response(HTTP response header and response body)
	 */
	protected function executeGet($headers, $body)
	{
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'headers: '.print_r($headers,true).' '.GCM_GL_LOG_VAR_SEPARATOR.'body: '.print_r($body,true), 'Calls SugarCRM REST(fnContractBrowseV2)');
		$httpRequestHeadersCase = array_change_key_case($headers, CASE_LOWER);
		$sessionId = !empty($httpRequestHeadersCase[parent::HEADER_SESSION_ID]) ? $httpRequestHeadersCase[parent::HEADER_SESSION_ID] : '';

		// Initialize variables to pass parameters
		$accountType = $dateAfter = $dateBefore = '';

		// Extracts request parameters
		if(endsWith(getRequestPath(), self::REST_PATH_SEARCH)) { // export-csv-for-gbs?key=value&key2=value2...
			$accountType  = array_key_exists(self::PARAM_KEY_TYPE,   $_REQUEST) ? $_REQUEST[self::PARAM_KEY_TYPE]   : '';
			$dateAfter     = array_key_exists(self::PARAM_KEY_DATE_AFTER, $_REQUEST) ? $_REQUEST[self::PARAM_KEY_DATE_AFTER] : '';
			$dateBefore = array_key_exists(self::PARAM_KEY_DATE_BEFORE, $_REQUEST) ? $_REQUEST[self::PARAM_KEY_DATE_BEFORE] : '';
		}

		// Call SugarCRM's REST
		$httpClient = curl_init(self::REST_SCHEME_HOST . self::REST_PATH_REAL);
		$restData = json_encode(array($sessionId, $accountType, $dateAfter, $dateBefore));

		$postFields = array("method"        => self::REST_FUNCTION_GET,
		                    "input_type"    => "JSON",
		                    //"response_type" => "XML", Removed as it is not needed
		                    "rest_data"     => $restData
		);
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'postFields: '.print_r($postFields,true), 'Post data details');
		curl_setopt($httpClient, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($httpClient, CURLOPT_HTTPHEADER, array('Expect:')); // Header suppression "Expect: 100-continue"
		curl_setopt($httpClient, CURLOPT_HEADER, true);  // If this flag is true, header information is included in response string.
		curl_setopt($httpClient, CURLOPT_POST, true);
		curl_setopt($httpClient, CURLOPT_POSTFIELDS, $postFields);

		//  Execute HTTP request and read response
		$responseContents = curl_exec($httpClient);
		curl_close($httpClient);
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'responseContents: '.$responseContents, '');
		return $responseContents;
	}

	/**
	 * Process the response header from SugarCRM's REST(fnContractBrowseV2) result
	 * (Excludes 'Content-Type' header)
	 * @param $headers HTTP headers (string[] ref.)
	 * @param $body    HTTP body (string)
	 * @return none
	 */
	protected function processResultGet(&$headers, $body)
	{
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'headers: '.print_r($headers,true).' '.GCM_GL_LOG_VAR_SEPARATOR.'body: '.print_r($body,true), 'Process the response header from SugarCRM REST(fnContractBrowseV2) result');
	    $errorOccured = mb_strpos($body, self::ERR_IDENTIFIER);
		if($errorOccured) {
			$messageCode = self::getErrorCodeFromErrorXml($body);
			$httpStatusCode = getResponseStatusCode($messageCode);

			$headers[0] = buildHttpResponseStatus($httpStatusCode);
		}
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'headers: '.print_r($headers,true), '');
	}	

	/**
	 * Get ErrorCode from XML.
	 * If multiple ErrorCode element is present, to return the value of the first element.
	 * @param string $errorXml XML(contract-information V2 exception)
	 * @return string ErrorCode(value of 'exp:error-code' element)
	 */
	private static function getErrorCodeFromErrorXml($errorXml) {
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'errorXml: '.print_r($errorXml,true), 'Get ErrorCode from XML. If multiple ErrorCode element is present, to return the value of the first element');
		$result = '';
		try {
			$sxe = new SimpleXMLElement($errorXml);
			$messageCodeElements = $sxe->xpath('//exp:error-code');
			if(!empty($messageCodeElements)) {
				$result = (string)$messageCodeElements[0];
			}
		}
		catch(Exception $e) {
			global $logger;
			$logger->warn(get_class($e).' occured: '.__CLASS__.'->'.__FUNCTION__.'() '.$e->getMessage(), $e);
		}
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'result: '.print_r($result,true), '');
		return $result;
	}

}