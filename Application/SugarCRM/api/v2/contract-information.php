<?php
require_once(API_ROOT.'/sugar_api_proxy.php');
require_once('common_application.php');

if(!defined('sugarEntry'))define('sugarEntry', true);

class ContractInformation extends SugarApiProxy {

 	const REST_SCHEME_HOST = 'http://localhost';
	const REST_PATH_SELF   = '/api/v2/contract-information';
	const REST_PATH_SEARCH = '/api/v2/contract-information/search';
	const REST_PATH_REAL   = '/custom/service/v4_1_custom/rest.php';

	const PARAM_KEY_GCID   = 'bi:contract-id';
	const PARAM_KEY_OBJVER = 'cbe:object-version';
	const PARAM_KEY_STATUS = 'bi:interaction-status';

	const REST_FUNCTION_POST   = 'fnContractRegistrationV2';
	const REST_FUNCTION_PATCH  = 'fnContractUpdateV2';
	const REST_FUNCTION_GET    = 'fnContractBrowseV2';

	const ERR_IDENTIFIER = 'exp:exceptions';

	const REQUEST_TYPE_POST = 'Post';
	const REQUEST_TYPE_PATCH = 'Patch';

	protected function getErrorResponseBody($statusCode, $requestMethod) {
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'statusCode: '.$statusCode, 'Function to return the error response body');
		require_once (API_ROOT . '/../custom/include/XMLUtils.php');
		$XMLUtilsObj = new XMLUtils();

		$errorBody = '';
		/**
		 * Api Validation Helper
		 */
		include API_ROOT . '/../custom/service/v4_1_custom/APIHelper/v2/helpers/ApiValidationHelper.php';
		$xpath_ids = [];
		if($statusCode == '405') {
			$xpath_ids = ['http_method' => $requestMethod];
			$api_config_details = [
	            'field'   => 'http_method', // field
	            'type'    => 'invalid', // type
	            'methods' => 'post', // methods
	        ];
		} else if($statusCode == '500') {
			$xpath_ids = [];
			$api_config_details = [
	            'field'   => 'internal_server_error', // field
	            'type'    => 'invalid', // type
	            'methods' => 'post', // methods
	        ];
		}
        $error = ApiValidationHelper::generateError($api_config_details, $xpath_ids);

        if (!empty($error)) {
            $error_codes = '';
            $error_messages = '';
            $error_details = [];

            $error_codes = $error['codes'];
            $error_messages = $error['messages'];
            if(!empty($error['details'])) {
                $error_details = $error['details'];
            } else {
                $error_details = [];
            }

            // Initialize variables to pass parameters
            $global_contract_uuid = $global_contract_id = '';
            // Extracts request parameters
            if(endsWith(getRequestPath(), self::REST_PATH_SEARCH)) {
                $global_contract_id  = array_key_exists(self::PARAM_KEY_GCID,   $_REQUEST) ? $_REQUEST[self::PARAM_KEY_GCID]   : '';
            } else {
                $params = self::extractParameterInRequestPath();$global_contract_uuid = $params[0];
            }
            // GCM ID of the target contract
            $gcm_id = !empty($global_contract_id) ? $global_contract_id : $global_contract_uuid;
            // if request = version_up|patch add global_contract_id|global_contract_uuid in message details
            if(!empty(trim($gcm_id)) && in_array($requestMethod, [self::REQUEST_TYPE_POST, self::REQUEST_TYPE_PATCH])) {
                $error_messages = 'GCM ID of the target contract is ['. $gcm_id .']. ' . $error_messages;
            }

            $errors['codes'][] = $error_codes;
            $errors['messages'][$error_codes][] = $error_messages;
            $errors['details'][$error_codes][] = $error_details;
        }

		switch($statusCode) {
			case 405: $errorBody=$XMLUtilsObj->fnGetErrorXMLDataV2($errors); break;
			case 500: $errorBody=$XMLUtilsObj->fnGetErrorXMLDataV2($errors);; break;
			default:  break;
		}
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'errorBody: '.$errorBody, '');
		return $errorBody;
	}


	/**
	 * Calls SugarCRM's REST(fnContractBrowseV2)
	 * @param array  $headers HTTP request headers(key => value)
	 * @param string $body empty string (not use)
	 * @return string BrowseAPI's response(HTTP response header and response body)
	 */
	protected function executeGet($headers, $body)
	{
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'headers: '.print_r($headers,true).' '.GCM_GL_LOG_VAR_SEPARATOR.'body: '.print_r($body,true), 'Calls SugarCRM REST(fnContractBrowseV2)');
		$httpRequestHeadersCase = array_change_key_case($headers, CASE_LOWER);
		$sessionId = !empty($httpRequestHeadersCase[parent::HEADER_SESSION_ID]) ? $httpRequestHeadersCase[parent::HEADER_SESSION_ID] : '';

		// Initialize variables to pass parameters
		$objectId = $globalContractId = $versionNumber = $interactionStatus = '';

		// Extracts request parameters
		if(endsWith(getRequestPath(), self::REST_PATH_SEARCH)) { // contract-information/search?key=value&key2=value2...
			$globalContractId  = array_key_exists(self::PARAM_KEY_GCID,   $_REQUEST) ? $_REQUEST[self::PARAM_KEY_GCID]   : '';
			$versionNumber     = array_key_exists(self::PARAM_KEY_OBJVER, $_REQUEST) ? $_REQUEST[self::PARAM_KEY_OBJVER] : '';
			$interactionStatus = array_key_exists(self::PARAM_KEY_STATUS, $_REQUEST) ? $_REQUEST[self::PARAM_KEY_STATUS] : '';
		}
		else {  // contract-information/[object-id]:[object-version]
			$params = self::extractParameterInRequestPath();
			$objectId      = $params[0];
			$versionNumber = $params[1];
		}

		// Call SugarCRM's REST
		$httpClient = curl_init(self::REST_SCHEME_HOST . self::REST_PATH_REAL);
		$restData = json_encode(array($sessionId, $objectId, $globalContractId, $versionNumber, $interactionStatus));

		$postFields = array("method"        => self::REST_FUNCTION_GET,
		                    "input_type"    => "JSON",
		                    "response_type" => "XML",
		                    "rest_data"     => $restData
		);
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'postFields: '.print_r($postFields,true), 'Post data details');
		curl_setopt($httpClient, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($httpClient, CURLOPT_HTTPHEADER, array('Expect:')); // Header suppression "Expect: 100-continue"
		curl_setopt($httpClient, CURLOPT_HEADER, true);  // If this flag is true, header information is included in response string.
		curl_setopt($httpClient, CURLOPT_POST, true);
		curl_setopt($httpClient, CURLOPT_POSTFIELDS, $postFields);

		//  Execute HTTP request and read response
		$responseContents = curl_exec($httpClient);
		curl_close($httpClient);
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'responseContents: '.$responseContents, '');
		return $responseContents;
	}

	/**
	 * Process the response header from SugarCRM's REST(fnContractBrowseV2) result
	 * (Excludes 'Content-Type' header)
	 * @param $headers HTTP headers (string[] ref.)
	 * @param $body    HTTP body (string)
	 * @return none
	 */
	protected function processResultGet(&$headers, $body)
	{
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'headers: '.print_r($headers,true).' '.GCM_GL_LOG_VAR_SEPARATOR.'body: '.print_r($body,true), 'Process the response header from SugarCRM REST(fnContractBrowseV2) result');
	    $errorOccured = mb_strpos($body, self::ERR_IDENTIFIER);
		if($errorOccured) {
			$messageCode = self::getErrorCodeFromErrorXml($body);
			$httpStatusCode = getResponseStatusCode($messageCode);

			$headers[0] = buildHttpResponseStatus($httpStatusCode);
		}
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'headers: '.print_r($headers,true), '');
	}


	/**
	 * Calls SugarCRM's REST(fnContractRegistrationV2)
	 * @param array  $headers HTTP request headers(key => value)
	 * @param string $body request information(XML)
	 * @return string BrowseAPI's response(HTTP response header and response body)
	 */
	protected function executePost($headers, $body)
	{
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'headers: '.print_r($headers,true).' '.GCM_GL_LOG_VAR_SEPARATOR.'body: '.print_r($body,true), 'Calls SugarCRM REST(fnContractRegistrationV2)');
		$httpRequestHeadersCase = array_change_key_case($headers, CASE_LOWER);
		$sessionId = !empty($httpRequestHeadersCase[parent::HEADER_SESSION_ID]) ? $httpRequestHeadersCase[parent::HEADER_SESSION_ID] : '';

		// Initialize variables to pass parameters
		$objectId = $globalContractId = $versionNumber = '';

		// Extracts request parameters
		if(endsWith(getRequestPath(), self::REST_PATH_SEARCH)) { // contract-information/search?key=value&key2=value2...
			$globalContractId  = array_key_exists(self::PARAM_KEY_GCID,   $_REQUEST) ? $_REQUEST[self::PARAM_KEY_GCID]   : '';
			$versionNumber     = array_key_exists(self::PARAM_KEY_OBJVER, $_REQUEST) ? $_REQUEST[self::PARAM_KEY_OBJVER] : '';
		}
		else {  // contract-information/[object-id]:[object-version]
			$params = self::extractParameterInRequestPath();
			$objectId      = $params[0];
			$versionNumber = $params[1];
		}

		// Call SugarCRM's REST
		$httpClient = curl_init(self::REST_SCHEME_HOST . self::REST_PATH_REAL);
		$restData = json_encode(array($sessionId, $body, $objectId, $globalContractId, $versionNumber));

		$postFields = array("method" => self::REST_FUNCTION_POST,
		                    "input_type"    => "JSON",
		                    "response_type" => "XML",
		                    "rest_data"     => $restData
		);
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'postFields: '.print_r($postFields,true), 'Post data details');
		curl_setopt($httpClient, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($httpClient, CURLOPT_HTTPHEADER, array('Expect:')); // Header suppression "Expect: 100-continue"
		curl_setopt($httpClient, CURLOPT_HEADER, true);  // If this flag is true, header information is included in response string.
		curl_setopt($httpClient, CURLOPT_POST, true);
		curl_setopt($httpClient, CURLOPT_POSTFIELDS, $postFields);

		//  Execute HTTP request and read response
		$responseContents = curl_exec($httpClient);
		curl_close($httpClient);
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'responseContents: '.$responseContents, '');
		return $responseContents;
	}

	/**
	 * Process the response header from SugarCRM's REST(fnContractRegistrationV2) result
	 * (Excludes 'Content-Type' header)
	 * @param $headers HTTP headers (string[] ref.)
	 * @param $body    HTTP body (string)
	 * @return none
	 */
	protected function processResultPost(&$headers, $body)
	{
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'headers: '.print_r($headers,true).' '.GCM_GL_LOG_VAR_SEPARATOR.'body: '.print_r($body,true), 'Process the response header from SugarCRMs REST(fnContractRegistrationV2) result');
		// 1.Determines the registration has succeeded or failed.
		$errorOccured = mb_strpos($body, self::ERR_IDENTIFIER);

		// 2.Divides the process
		if(!$errorOccured) { // 2-1.Registration succeeded
		  
			// 2-1.(1)Modify HTTP status code to 201(Created)
			$headers[0] = buildHttpResponseStatus(201);

			// 2-1.(2)Add Location Header
			$productContractObjectId      = '';
			$productContractObjectVersion = '';
			try { // Get ObjectId and ObjectVersion value from reponse XML
				$sxe = new SimpleXMLElement($body);
				$productContractObjectIdElements = $sxe->xpath('/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:identified-by/bi:product-contract-key/cbe:key-type-for-cbe-standard/cbe:object-id');
				if(!empty($productContractObjectIdElements)) {
					$productContractObjectId = (string)$productContractObjectIdElements[0];
				}
				$productContractObjectVersionElements = $sxe->xpath('/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:identified-by/bi:product-contract-key/cbe:key-type-for-cbe-standard/cbe:object-version');
				if(!empty($productContractObjectVersionElements)) {
					$productContractObjectVersion = (string)$productContractObjectVersionElements[0];
				}
			}
			catch(Exception $e) {
				global $logger;
				$logger->warn(get_class($e).' occured: '.__CLASS__.'->'.__FUNCTION__.'() '.$e->getMessage(), $e);
			}

			if(!empty($productContractObjectId)) {
				global $sugar_config;
				$locationHeaderValue  = $sugar_config['site_url'];
				$locationHeaderValue .= (endsWith($locationHeaderValue, '/') ? substr(self::REST_PATH_SELF, 1) : self::REST_PATH_SELF);
				$locationHeaderValue .= '/' . $productContractObjectId;
				if(!empty($productContractObjectVersion)) {
					$locationHeaderValue .= ':' . $productContractObjectVersion;
				}

				modifyHeaderValue($headers, 'Location', $locationHeaderValue);
			}
		}
		else {  // 2-2.Registration failed
			$messageCode = self::getErrorCodeFromErrorXml($body);
			$httpStatusCode = getResponseStatusCode($messageCode);

			// 2-2.(2)Modify HTTP status code.
			$headers[0] = buildHttpResponseStatus($httpStatusCode);
		}
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'headers: '.print_r($headers,true), '');
	}

	/**
	 * Calls SugarCRM's REST(fnContractUpdateV2)
	 * @param array  $headers HTTP request headers(key => value)
	 * @param string $body request information(XML)
	 * @return string BrowseAPI's response(HTTP response header and response body)
	 */
	protected function executePatch($headers, $body)
	{
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'headers: '.print_r($headers,true).' '.GCM_GL_LOG_VAR_SEPARATOR.'body: '.print_r($body,true), 'Calls SugarCRMs REST(fnContractUpdateV2)');
		$httpRequestHeadersCase = array_change_key_case($headers, CASE_LOWER);
		$sessionId = !empty($httpRequestHeadersCase[parent::HEADER_SESSION_ID]) ? $httpRequestHeadersCase[parent::HEADER_SESSION_ID] : '';

		// Initialize variables to pass parameters
		$objectId = $globalContractId = $versionNumber = '';

		// Extracts request parameters
		if(endsWith(getRequestPath(), self::REST_PATH_SEARCH)) { // contract-information/search?key=value&key2=value2...
			$globalContractId  = array_key_exists(self::PARAM_KEY_GCID,   $_REQUEST) ? $_REQUEST[self::PARAM_KEY_GCID]   : '';
			$versionNumber     = array_key_exists(self::PARAM_KEY_OBJVER, $_REQUEST) ? $_REQUEST[self::PARAM_KEY_OBJVER] : '';
		}
		else {  // contract-information/[object-id]:[object-version]
			$params = self::extractParameterInRequestPath();
			$objectId      = $params[0];
			$versionNumber = $params[1];
		}

		// Call SugarCRM's REST
		$httpClient = curl_init(self::REST_SCHEME_HOST . self::REST_PATH_REAL);
		$restData = json_encode(array($sessionId, $body, $objectId, $globalContractId, $versionNumber));

		$postFields = array("method"        => self::REST_FUNCTION_PATCH,
		                    "input_type"    => "JSON",
		                    "response_type" => "XML",
		                    "rest_data"     => $restData
		);
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'postFields: '.print_r($postFields,true), 'Post data details');
		curl_setopt($httpClient, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($httpClient, CURLOPT_HTTPHEADER, array('Expect:')); // Header suppression "Expect: 100-continue"
		curl_setopt($httpClient, CURLOPT_HEADER, true);  // If this flag is true, header information is included in response string.
		curl_setopt($httpClient, CURLOPT_POST, true);
		curl_setopt($httpClient, CURLOPT_POSTFIELDS, $postFields);

		//  Execute HTTP request and read response
		$responseContents = curl_exec($httpClient);
		curl_close($httpClient);
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'responseContents: '.$responseContents, '');
		return $responseContents;
	}

	/**
	 * Process the response header from SugarCRM's REST(fnContractUpdateV2) result
	 * (Excludes 'Content-Type' header)
	 * @param $headers HTTP headers (string[] ref.)
	 * @param $body    HTTP body (string)
	 * @return none
	 */
	protected function processResultPatch(&$headers, $body)
	{
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'headers: '.print_r($headers,true).' '.GCM_GL_LOG_VAR_SEPARATOR.'body: '.print_r($body,true), 'Process the response header from SugarCRMs REST(fnContractRegistrationV2) result');
		$errorOccured = mb_strpos($body, self::ERR_IDENTIFIER);

		if($errorOccured) {
			$messageCode = self::getErrorCodeFromErrorXml($body);
			$httpStatusCode = getResponseStatusCode($messageCode);

			$headers[0] = buildHttpResponseStatus($httpStatusCode);
		}		
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'headers: '.print_r($headers,true), '');
	}

	/**
	 * Extracts requested parameter in path.
	 * @return array [0]:ObjectId / [1]:ObjectVersion
	 */
	private static function extractParameterInRequestPath()
	{
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Extracts requested parameter in path');
		$paramString = substr(getRequestPath(), strlen(self::REST_PATH_SELF) + 1); // + 1 is Length of '/'
		$params = explode(':', $paramString);

		$return_val = array(count($params)>0 ? $params[0] : '', count($params)>1 ? $params[1] : '');
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_val: '.print_r($return_val,true), '');
		return $return_val;
	}

	/**
	 * Get ErrorCode from XML.
	 * If multiple ErrorCode element is present, to return the value of the first element.
	 * @param string $errorXml XML(contract-information V2 exception)
	 * @return string ErrorCode(value of 'exp:error-code' element)
	 */
	private static function getErrorCodeFromErrorXml($errorXml) {
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'errorXml: '.print_r($errorXml,true), 'Get ErrorCode from XML. If multiple ErrorCode element is present, to return the value of the first element');
		$result = '';
		try {
			$sxe = new SimpleXMLElement($errorXml);
			$messageCodeElements = $sxe->xpath('//exp:error-code');
			if(!empty($messageCodeElements)) {
				$result = (string)$messageCodeElements[0];
			}
		}
		catch(Exception $e) {
			global $logger;
			$logger->warn(get_class($e).' occured: '.__CLASS__.'->'.__FUNCTION__.'() '.$e->getMessage(), $e);
		}
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'result: '.print_r($result,true), '');
		return $result;
	}

}