<?php
/**
 *  Common functions for RESTful Proxy Modules.
 *
 *  # These functions are depends on the particular application logic.
 */

/**
 * Returns response status code from application message code.
 * @param  $messageCode Message code by application (integer)
 * @return HTTP response status code (integer)
 */
function getResponseStatusCode($messageCode)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'messageCode: '.$messageCode, 'Returns response status code from application message code');
    // GCM error code is not mapped with http status code
    $http_code = 500;
    $errorCode = trim($messageCode);
    if(!empty($errorCode)){
        require_once 'custom/service/v4_1_custom/APIHelper/v2/configs/api_http_status_code_list.php';
        $trim_http_code = substr($errorCode, 0, 3);
        global $http_status_code_list;
        if (in_array($trim_http_code, $http_status_code_list)) {
            $http_code = $trim_http_code;
        }
    }
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'http_code: '.$http_code, '');
    return $http_code;
}