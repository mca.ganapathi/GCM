<?php
/**
 * RESTful Proxy - Main (Routing Control)
 */

//=====================
// Define - Valiables
//=====================
ini_set('display_errors', 0);  // Uncomment when git push.
define('API_ROOT', dirname(__FILE__));

//=====================
// Define - Functions
//=====================
/**
 * Find concrete class(.php) file in recursive.
 * @param array $pathHierarchy relative path heerarchy
 * @return string Concrete class file name(if not found, then return null)
 */
function findConcreteClassFile($pathHierarchy) {
	$relativePath = join('/', $pathHierarchy);
	$concreteClassFile = API_ROOT . '/' . $relativePath . '.php';
	if(is_file($concreteClassFile)) {
		return $concreteClassFile;
	}

	if(count($pathHierarchy) < 2) {
		return null; // Not Found.
	}
	array_pop($pathHierarchy); // Deletes Last Element
	return findConcreteClassFile($pathHierarchy);
}

function buildLogMessage($exceptionTime, $exception) {
	return $_SERVER['REQUEST_METHOD'] . ' ' . $_SERVER["REQUEST_URI"] . "\n"
	    . "---- ExceptionTime - "  . $exceptionTime->format('Y-m-d H:i:s.u') . "\n"
	    . "---- ExceptionInfo -\n" . $exception;
}


//=====================
//  Entry Point(Main)
//=====================

// Reuires common library, modules.
require_once('lib/log4php/Logger.php');
require_once('lib/Cake/Utility/Inflector.php');
require_once('common.php');
require_once('restful_proxy.php');

// Read SugarCRM's configuration
chdir(API_ROOT . '/..');
if(is_file('config.php'))          require_once('config.php');
if(is_file('config_override.php')) require_once('config_override.php');

// For Sugar Log mechanism which is implemented in custon Modutils.php 
if(!defined('sugarEntry'))define('sugarEntry', true);
if(is_file('include/entryPoint.php')) require_once('include/entryPoint.php');

chdir(API_ROOT);

// (Prepare) Configure logger and creates instance in global.
Logger::configure('config/log4php.xml');
$logger = Logger::getLogger("apiLogger");

// Routing
$requestPath = getRequestPath(); //echo $requestPath."\n";
$selfFile = basename(__FILE__);  //echo $selfFile."\n";
$selfPath = substr($_SERVER['PHP_SELF'], 0, strlen($_SERVER['PHP_SELF'])-strlen(basename(__FILE__))); //echo $selfPath."\n";
$relativePath=substr($requestPath, strlen($selfPath)); //echo $relativePath."\n";

$concreteClassFile = findConcreteClassFile(explode('/', $relativePath));
if(empty($concreteClassFile)) {
    header(buildHttpResponseStatus(404)); // 404: Not Found
    return;
}
require_once($concreteClassFile);	// Load concrete class (.php) file.

try {
	$apiRequestType = '';
	// Derives log table name from file name
	$logTableName = substr(basename($concreteClassFile), 0, -4); // -4 is length of '.php'
	// Derives a class name from file name
	$className = Inflector::classify(str_replace('-', '_', $logTableName)); //echo $className."\n";
	// Checks to class definition is exist(e.g. Class was loaded)
	if(!class_exists($className)) {
		throw new Exception("Class \"" . $className ."\" is not exist in " . $concreteClassFile);
	}

	// Create Instance
	$restfulProxyInstance = new $className($logTableName); //var_dump($restfulProxyInstance);
	// Checks whether the instance is a RestfulProxy
	if(!($restfulProxyInstance instanceof RestfulProxy)) {
		throw new Exception("Bad implementation class '" . $className ."' in " . $concreteClassFile);
	}
	// API Request Type
	if(trim($logTableName) == 'contract-information' || trim($logTableName) == 'list-of-product-contracts') {
		$apiRequestType = 1;		
	} else if(trim($logTableName) == 'export-csv-for-gbs') {
		$apiRequestType = 2;
	} else {
		throw new Exception("API Request not exist in '" . $className ."'");
	}	
		
	// Calls API using RestfulProxy instance.
	$restfulProxyInstance->callApi($apiRequestType);

} catch(Exception $e) {
	$logger->info(buildLogMessage(new DateTime(), $e));
    header(buildHttpResponseStatus(500)); // 500: Internal Server Error
	exit(1);
}
