<?php
/**
 *  Common functions for RESTful Proxy Modules.
 *
 *  # These functions are not dependent on a specific application logic.
 */

//======================================================================
//  Define
//======================================================================
define('READ_CONTENTS_SIZE', 102400); // 100KB
define('HTTP_VERSION', '1.1');
$HTTP_STATUS_MESSAGES = array( // if PHP version>=5.4.0 then this variables should not be used.
    100=>'Continue', 101=>'Switching Protocols', 102=>'Processing',
    200=>'OK', 201=>'Created', 202=>'Accepted', 203=>'Non-Authoritative Information', 204=>'No Content',
    205=>'Reset Content', 206=>'Partial Content', 207=>'Multi-Status', 208=>'Already Reported', 226=>'IM Used',
    300=>'Multiple Choices', 301=>'Moved Permanently', 302=>'Found', 303=>'See Other', 304=>'Not Modified',
    305=>'Use Proxy', 307=>'Temporary Redirect', 308=>'Permanent Redirect',
    400=>'Bad Request', 401=>'Unauthorized', 402=>'Payment Required', 403=>'Forbidden', 404=>'Not Found',
    405=>'Method Not Allowed', 406=>'Not Acceptable', 407=>'Proxy Authentication Required', 408=>'Request Timeout',
    409=>'Conflict', 410=>'Gone', 411=>'Length Required', 412=>'Precondition Failed', 413=>'Payload Too Large',
    414=>'URI Too Long', 415=>'Unsupported Media Type', 416=>'Range Not Satisfiable', 417=>'Expectation Failed',
    418=>"I'm a teapot", 422=>'Unprocessable Entity', 423=>'Locked', 424=>'Failed Dependency', 426=>'Upgrade Required',
    500=>'Internal Server Error', 501=>'Not Implemented', 502=>'Bad Gateway', 503=>'Service Unavailable',
    504=>'Gateway Timeout', 505=>'HTTP Version Not Supported', 506=>'Variant Also Negotiates',
    507=>'Insufficient Storage', 509=>'Bandwidth Limit Exceeded', 510=>'Not Extended'
);


/**
 * Read HTTP Request-body contents.
 * @param none
 * @return string HTTP Request Body
 */
function readHttpRequestBody()
{
    $result = '';

    $stdin = fopen('php://input', 'r');
    while(!feof($stdin)) {
        $result .= fgets($stdin, READ_CONTENTS_SIZE);
    }
    fclose($stdin);

    return $result;
}


/**
 * Get Request-Path (from $_SERVER['REQUEST_URI'])
 * <pre>
 * Example:
 *  # REQUEST_URI "/api/v1/foobar"         -> Return "/api/v1/foobar"
 *  # REQUEST_URI "/api/v1/foobar/any?q=a" -> Return "/api/v1/foobar/any"
 * </pre>
 * @return string Request-Path
 */
function getRequestPath()
{
    if(!isset($_SERVER['REQUEST_URI'])) return '';

    if(($pos=strpos($_SERVER['REQUEST_URI'], '?'))===false) {  // Query Separator('?') was not found,
        return $_SERVER['REQUEST_URI'];
    }
    return substr($_SERVER['REQUEST_URI'], 0, $pos);
}

/**
 * Separate to Headers and Body.
 * @param  string $contents
 * @return array [0]:Headers(string[]), [1]:Body(string)
 */
function separateHeaderBody($contents)
{
    $headers = array();
    $body = $contents;

    $contentLines = preg_split("/\R/", $contents);  // Sequencing without distinguishing line feed code(\r, \n, \r\n)
    foreach($contentLines as $line) {
        if(empty($line)) break;  // Blank Line = Boundary of the header and body

        $headers[] = $line;
        $body = ltrim(mb_substr($body, mb_strlen($line)));
    }

    return array($headers, $body);
}


/**
 * Modifies header's value.
 * If not found Header by $headerName, then this method adds "$headerName: $headerValue" to $headers array;
 * @param string[](ref.) $headers HTTP headers
 * @param string $headerName  Header's key name
 * @param string $headerValue Header's value
 * @return none
 */
function modifyHeaderValue(&$headers, $headerName, $headerValue)
{
    if(empty($headers)) {
        $headers = array($headerName . ': ' . $headerValue);
        return;
    }

    for($i=0; $i<count($headers); $i++) {
        if(mb_strpos($headers[$i], $headerName.':')===false) {  // If $headerName: was not found,
            continue;                                           //                 then ignore current string.
        }
        // Found -> Replace(Modify) Header
        $headers[$i] = $headerName . ': ' . $headerValue;
        return;
    }
    // Not Found -> Add Header.
    $headers[] = $headerName . ': ' . $headerValue;
}


/**
 * Remove header.
 * If not found Header by $headerName, then this method do nothing.
 * @param string[](ref.) $headers HTTP headers
 * @param string         $headerName Header's key name
 * @return none
 */
function removeHeader(&$headers, $headerName)
{
    if(empty($headers)) {
        $headers = array($headerName . ': ' . $headerValue);
        return;
    }

    for($i=0; $i<count($headers); $i++) {
        if(mb_strpos($headers[$i], $headerName.':')===false) {  // If $headerName: was not found,
            continue;                                           //                 then ignore current string.
        }
        // Found -> Remove Header
        array_splice($headers, $i, 1);
        return;
    }
    // Not Found -> Do nothing.
}

/**
 * Get HTTP status code from status line(Response header's first line)
 * @param string $statusLine (Ex."HTTP/1.1 200 OK")
 * @return int HTTP status code(if unknown, returns -1)
 */
function getHttpResponseStatusCode($statusLine)
{
	$elements = preg_split("/[\s]+/", $statusLine);
	if(count($elements)<2) return -1;

	return (int)$elements[1];
}

/**
 * Builds HTTP status line from status code
 * @param  int $statusCode HTTP status code
 * @return string HTTP Status line
 */
function buildHttpResponseStatus($statusCode)
{
    global $HTTP_STATUS_MESSAGES;
    return 'HTTP/'.HTTP_VERSION.' '.$statusCode.
           (array_key_exists($statusCode, $HTTP_STATUS_MESSAGES) ? ' '.$HTTP_STATUS_MESSAGES[$statusCode] : '');
}

/**
 * Checks the string ends with suffix or not.
 * @param string $str     String to be checked
 * @param string $suffix  Suffix
 * @return boolean true: ends with suffix / false: not ends with suffix
 */
function endsWith($str, $suffix)
{
	$length = strlen($suffix);
	if ($length == 0) {
		return true;
	}
	return (substr($str, -$length) === $suffix);
}
