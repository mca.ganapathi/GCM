<?php
/**
 * RESTful Proxy - base logic class
 * @author Satoshi Mamada
 */
abstract class RestfulProxy {

	/**
	 * MySQL's ErrorCode (Table is not exist)
	 * @see https://dev.mysql.com/doc/refman/5.5/en/error-messages-server.html#error_er_no_such_table
	 */
	const ER_NO_SUCH_TABLE = 1146; //

	/** Database name for logging */
	const LOG_DB_NAME = 'apilog';

	/** Prefix for method name to execute API calling. */
	const PREFIX_METHOD_EXECUTE     = 'execute';
	/** Prefix for method name to process the result from API call. */
	const PREFIX_METHOD_PROC_RESULT = 'processResult';

	/** Excludes apilog writing that matches condition below. (This constant description is valid only for PHP 5.6 or higher) */
	const CONDITION_EXCLUDE_APILOG = array(
		array('path'=>'/api/v2/contract-information', 'method'=>'GET', 'http_status'=>200)
	// If you add other exclusion conditions in the future, add an associate array like this
	//, array('path'=>'', 'method'=>'', 'http_status'=> )
	);

	/** Database Connection  */
	private $logPdo;
	/** Table name of writing log */
	private $logTableName;

	public function __construct($logTableName) {
		// Keep logTableName
		$this->logTableName = $logTableName;

		// Connect to Database(using SugarCRM's configuration)
		global $sugar_config;
		$dbConfig = $sugar_config['dbconfig'];
		$dataSource = 'mysql:host=' . $dbConfig['db_host_name'] . ';dbname=' . self::LOG_DB_NAME;
		$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
		$this->logPdo = new PDO($dataSource, $dbConfig['db_user_name'], $dbConfig['db_password'], $options);
	}


	/**
	 * [Template Method] CallAPI with logging.
	 * This method is final, so sub-class can not over write this logic.
	 * @param  API Request Type ($apiRequestType) 1 => Contract Information ; 2 => GBS API
	 * @return none
	 */
	public final function callApi($apiRequestType) {
		$requestMethod = strtolower($_SERVER['REQUEST_METHOD']);

		// Step1. Keeps request information. (RequestTime, RequestHeader, RequestBody(if exists))
		$requestTime = new DateTime();
		$httpRequestHeaders = getallheaders();
		$httpRequestBody = '';  // default is empty.
		switch($requestMethod) {
			case 'put': case 'post': case 'patch':
				$httpRequestBody = readHttpRequestBody();
				break;
			default:
				break;
		}
		$executeMethod = self::PREFIX_METHOD_EXECUTE . Inflector::camelize($requestMethod);
		if(!method_exists($this, $executeMethod)) { // Method not exist => Return 405(Method Not Allowed)
			header(buildHttpResponseStatus(405));
			$errorResponseBody = $this->getErrorResponseBody(405, Inflector::camelize($requestMethod));
			if(!empty($errorResponseBody)) {
				header('Content-Type: text/xml');
				print $errorResponseBody;
			} 
			return;
		}

		// Step2. Calls API execution method(=template method) and receives API response.
		$restResponse = $this->$executeMethod($httpRequestHeaders, $httpRequestBody);

		// Step3. Check the API response, rebuild a REST response information.(ResponseTime, StatusCode, Header)
		if(empty($restResponse)) {                  // Response is empty => Return 500(Internal Server Error)
			header(buildHttpResponseStatus(500));
			$errorResponseBody = $this->getErrorResponseBody(500, Inflector::camelize($requestMethod));
			if(!empty($errorResponseBody)) {
				header('Content-Type: text/xml');
				print $errorResponseBody;
			} 
			return;
		}
		$responseAry = separateHeaderBody($restResponse); //-> $responseAry[0]=headers, $responseAry[1]=body
		$responseHeaders = $responseAry[0]; // string[]
		$responseBody    = $responseAry[1]; // string

		$processResultMethod = self::PREFIX_METHOD_PROC_RESULT . Inflector::camelize($requestMethod);
		if(method_exists($this, $processResultMethod)) {
			$this->$processResultMethod($responseHeaders, $responseBody);
		}

		// Step4. Write to access-log into the DB(If writing DB was faild, then write to logger instead)
		$responseStatusCode = getHttpResponseStatusCode($responseHeaders[0]); // Get response status code(int) from Response Header's first element
		$responseTime = new DateTime();
		$this->writeApiAccessLog($_SERVER['REQUEST_METHOD'],
		                         $_SERVER['REQUEST_URI'], $httpRequestBody, $requestTime,
		                         $responseStatusCode, $responseBody, $responseTime);

		// Step5. Outputs HTTP response.
		$contentType = $this->getContentType($apiRequestType, $responseStatusCode);
		if(!empty($contentType)) {
			modifyHeaderValue($responseHeaders, 'Content-Type', $contentType); // Modify Content-Type header value
		}
		removeHeader($responseHeaders, 'Transfer-Encoding'); // Remove Transfer-Encoding header (if exist)

		// Write response headers
		foreach($responseHeaders as $header) {
			header($header);
		}

		// Write response body
		print $responseBody;
	}

	/**
	 * Returns HTTP response body string when http error was occured.
	 * @param  int $statusCode http status code
	 * @return string EmptyString
	 */
	protected function getErrorResponseBody($statusCode, $requestMethod) {
		return '';
	}

	/**
	 * Returns HTTP response header "Content-Type" value.
	 */
	abstract protected function getContentType($apiRequestType, $responseStatusCode);


	/**
	 * Write(Insert) API access-log into the DB if it does not correspond to the exclusion condition.
	 * *If writing DB was faild, then write to logger instead.
	 * @param string   $requestMethod
	 * @param string   $requestPath
	 * @param string   $requestBody
	 * @param DateTime $requestTime
	 * @param int      $responseStatusCode
	 * @param string   $responseBody
	 * @param DateTime $responseTime
	 * @return none
	 */
	private function writeApiAccessLog($requestMethod, $requestPath, $requestBody, $requestTime, $responseStatusCode, $responseBody, $responseTime)
	{
		// Check if it corresponds exclusion conditions.
		foreach (self::CONDITION_EXCLUDE_APILOG as $excludeCondition) {
			if ((strpos($requestPath, $excludeCondition['path'])!==false) &&
				($requestMethod     ==$excludeCondition['method'])        &&
				($responseStatusCode==$excludeCondition['http_status'])) {
				return;		// Exit this function because it matched exclude condision.
			}
		}

		$insertResult = false;
		try {
			$stmt = $this->getInsertStatement($requestMethod, $requestPath, $requestBody, $requestTime, $responseStatusCode, $responseBody, $responseTime);
			$insertResult = $stmt->execute();
			if($insertResult) { // Insert Success
				return;
			}

			// Check the insert failure reason
			$reason = $stmt->errorInfo();
			if($reason[1] == self::ER_NO_SUCH_TABLE) {   // Log table was not exist(=FirstTime)
				$createResult = $this->createLogTable(); // Try to create log table.
				if($createResult !== false) {
					// If create table was success, then retry log insert.
					$stmt = $this->getInsertStatement($requestMethod, $requestPath, $requestBody, $requestTime, $responseStatusCode, $responseBody, $responseTime);
					$insertResult = $stmt->execute();
				}
			}
		} catch(Exception $e) {
			// Insert log was failed by exception (unknown reason)
			$insertResult = false;
		}
		// finally   //---- This syntax can be used PHP>=5.5
		if(!$insertResult) {
			// Since the failed log insert, write to the log file instead.
			global $logger;
			$logger->info($this->buildLogMessage($requestMethod, $requestPath, $requestBody, $requestTime, $responseStatusCode, $responseBody, $responseTime));
		}
	}

	private function getInsertStatement($requestMethod, $requestPath, $requestBody, $requestTime, $responseStatusCode, $responseBody, $responseTime)
	{
		$sql = 'INSERT INTO `' . $this->logTableName . '` '.
				'(method,  path, request_body, request_datetime, http_status, response_body, response_datetime) '.
				' VALUES '.
				"(:method, :path, :request_body,  STR_TO_DATE(:request_datetime, '%Y-%m-%d %H:%i:%s'), :http_status, :response_body, STR_TO_DATE(:response_datetime, '%Y-%m-%d %H:%i:%s'))";

		$stmt = $this->logPdo->prepare($sql);
		$stmt->bindValue(':method',            $requestMethod,                       PDO::PARAM_STR);
		$stmt->bindValue(':path',              $requestPath,                         PDO::PARAM_STR);
		$stmt->bindValue(':request_body',      $requestBody,                         PDO::PARAM_STR);
		$stmt->bindValue(':request_datetime',  $requestTime->format('Y-m-d H:i:s'),  PDO::PARAM_STR);
		$stmt->bindValue(':http_status',       $responseStatusCode,                  PDO::PARAM_INT);
		$stmt->bindValue(':response_body',     $responseBody,                        PDO::PARAM_STR);
		$stmt->bindValue(':response_datetime', $responseTime->format('Y-m-d H:i:s'), PDO::PARAM_STR);

		return $stmt;
	}

	private function createLogTable()
	{
		$sql = 'CREATE TABLE `' . $this->logTableName . '` ('.
		       '  seq int(11) NOT NULL AUTO_INCREMENT'.
		       ', method varchar(16) NOT NULL'.
		       ', path varchar(512) NOT NULL'.
		       ', request_body longtext NOT NULL'.
		       ', request_datetime datetime NOT NULL'.
		       ', http_status smallint(6) NOT NULL'.
		       ', response_body longtext NOT NULL'.
		       ', response_datetime datetime NOT NULL'.
		       ", checked char(1) NOT NULL DEFAULT '0'".
		       ', check_datetime datetime DEFAULT NULL'.
		       ', PRIMARY KEY (seq)'.
		       ') ENGINE=InnoDB DEFAULT CHARSET=utf8';
		return $this->logPdo->exec($sql);
	}

	function buildLogMessage($requestMethod, $requestPath, $requestBody, $requestTime, $responseStatusCode, $responseBody, $responseTime) {
		return $requestMethod . ' ' . $requestPath . "\n"
		        . "---- RequestTime - " . $requestTime->format('Y-m-d H:i:s.u') . "\n"
		        . "---- RequestBody -\n" . (empty($requestBody) ? '(empty)' : $requestBody) . "\n"
		        . "---- ResponseTime - " . $responseTime->format('Y-m-d H:i:s.u') . "\n"
		        . "---- ResponseStatusCode - " . $responseStatusCode . "\n"
		        . "---- ResponseBody -\n" . (empty($responseBody) ? '(empty)' : $responseBody);
	}
}
