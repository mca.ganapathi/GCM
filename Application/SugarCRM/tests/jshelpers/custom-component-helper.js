(function(test) {
    var app = SUGAR.App;
    test.loadCustomComponent = function(client, type, name, module) {
        var path = "/clients/" + client + "/" + type + "s/" + name;
        path = (module) ? "../custom/modules/" + module + path : "../custom" + path;

        SugarTest.loadFile(path, name, "js", function(data) {
            try {
                data = eval(data);
            } catch(e) {
                app.logger.error("Failed to eval view controller for " + name + ": " + e + ":\n" + data);
            }
            test.addComponent(client, type, name, data, module);
        });
    };

    test.loadCustomPlugin = function(name, subdir) {
        subdir = subdir ? '/' + subdir : '';
        var path = '../custom/include/javascript/sugar7/plugins' + subdir;
        SugarTest.loadFile(path, name, 'js', function(d) {
            app.events.off('app:init');
            eval(d);
            app.events.trigger('app:init');
        });
    };

    test.loadCustomHandlebarsTemplate = function(name, type, client, template, module) {
        var templateName = template || name;
        var path = "/clients/" + client + "/" + type + "s/" + name;
        path = (module) ? "../custom/modules/" + module + path : "../custom" + path;
        SugarTest.loadFile(path, templateName, "hbs", function(data) {
            test.testMetadata.addTemplate(name, type, data, templateName, module);
        });
    };

    test.createCustomView = function(client, module, viewName, meta, context, loadFromModule, layout, loadComponent) {
        if (_.isUndefined(loadComponent) || loadComponent)
        {
            if (loadFromModule) {
                test.loadCustomComponent(client, "view", viewName, module);
            } else {
                test.loadCustomComponent(client, "view", viewName, null);
            }
        }
        if (!context) {
            context = app.context.getContext();
            context.set({
                module: module
            });
            context.prepare();
        }

        var view = app.view.createView({
            name : viewName,
            context : context,
            module : module,
            meta : meta,
            layout: layout,
            platform: client
        });

        var _origDispose = view._dispose;
        view._dispose = function() {
            if(this.context) {
                SugarTest._events.context.push(this.context._events);
            }
            if(this.model) {
                SugarTest._events.model.push(this.model._events);
            }
            _origDispose.apply(this, arguments);
        };

        SugarTest.components.push(view);
        return view;
    };

    test.createCustomLayout = function(client, module, layoutName, meta, context, loadFromModule, params) {
        if (loadFromModule) {
            test.loadCustomComponent(client, "layout", layoutName, module);
        } else {
            test.loadCustomComponent(client, "layout", layoutName);
        }
        if (!context) {
            context = app.context.getContext();
            context.set({
                module: module,
                layout: layoutName
            });
            context.prepare();
        }

        var layout = app.view.createLayout(_.extend({
            name: layoutName,
            context: context,
            module: module,
            meta: meta,
            platform: client
        }, params));
        var _origDispose = layout._dispose;
        layout._dispose = function() {
            if(this.context) {
                SugarTest._events.context.push(this.context._events);
            }
            if(this.model) {
                SugarTest._events.model.push(this.model._events);
            }
            _origDispose.apply(this, arguments);
        };
        SugarTest.components.push(layout);
        return layout;
    };

    /**
     * Used to create a field object of a given type. Can load the relevent controller automatically from source.
     *
     * @param {Object} options Object containing the list of options to pass to this function
     *
     * - client {String} optional name of client to load the controller from
     * - name {String}
     * - type {String}
     * - viewName {String}
     * - fieldDef {Object}
     * - module {String} optional
     * - model {Backbone.Model} optional
     * - context {app.Context} optional
     * - loadFromModule {boolean} optional when true will attempt to load source file from the Module directory.
     * - loadJsFile {boolean optional defaults to true. When true will attempt to load the controller from source file.
     * @return {app.view.Field}
     */
    test.createCustomField = function(client, name, type, viewName, fieldDef, module, model, context, loadFromModule) {
        var loadJsFile = true;
        //Handle a params object instead of a huge list of params
        if (_.isObject(client)) {
            name = client.name;
            type = client.type;
            viewName = client.viewName;
            fieldDef = client.fieldDef;
            module = client.module;
            model = client.model;
            context = client.context;
            loadFromModule = client.loadFromModule;
            loadJsFile = !_.isUndefined(client.loadJsFile) ? client.loadJsFile : loadJsFile;
            client = client.client || "base";
        }

        if(loadJsFile) {
            if (loadFromModule) {
                test.loadCustomComponent(client, "field", type, module);
            } else {
                test.loadCustomComponent(client, "field", type);
            }
        }

        var view = new app.view.View({ name: viewName, context: context });
        var def = { name: name, type: type, events: (fieldDef) ? fieldDef.events : {} };
        if (!context) {
            context = app.context.getContext();
            context.set({
                module: module
            });
            context.prepare();
        }

        model = model || new app.data.createBean();

        if (fieldDef) {
            model.fields = model.fields || {};
            model.fields[name] = fieldDef;
        }

        var field = app.view.createField({
            def: def,
            view: view,
            context: context,
            model: model,
            module:module,
            platform: client
        });


        var _origDispose = field._dispose;
        field._dispose = function() {
            if(this.context) {
                SugarTest._events.context.push(this.context._events);
            }
            if(this.model) {
                SugarTest._events.model.push(this.model._events);
            }
            _origDispose.apply(this, arguments);
        };

        SugarTest.components.push(view);
        return field;
    };

}(SugarTest));
