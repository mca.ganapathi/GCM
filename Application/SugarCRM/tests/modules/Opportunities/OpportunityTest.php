<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

class OpportunityTest extends Sugar_PHPUnit_Framework_TestCase
{

    public static function setUpBeforeClass()
    {
        SugarTestHelper::setUp('current_user');
        SugarTestHelper::setUp('app_strings');
        SugarTestHelper::setUp('beanFiles');
        SugarTestHelper::setUp('beanList');
        SugarTestCurrencyUtilities::createCurrency('MonkeyDollars', '$', 'MOD', 2.0);

        SugarTestForecastUtilities::setUpForecastConfig(array(
                'sales_stage_won' => array('Closed Won'),
                'sales_stage_lost' => array('Closed Lost'),
                'forecast_by' => 'opportunities',
            ));
    }

    public function tearDown()
    {
        SugarTestRevenueLineItemUtilities::removeAllCreatedRevenueLineItems();
        SugarTestOpportunityUtilities::removeAllCreatedOpportunities();
        SugarTestCurrencyUtilities::removeAllCreatedCurrencies();
        SugarTestTimePeriodUtilities::removeAllCreatedTimePeriods();
        SugarTestProductUtilities::removeAllCreatedProducts();
        SugarTestAccountUtilities::removeAllCreatedAccounts();
    }

    public static function tearDownAfterClass()
    {
        SugarTestForecastUtilities::tearDownForecastConfig();
        SugarTestHelper::tearDown();
    }

    public function dataProviderCaseFieldEqualsAmountWhenCaseFieldEmpty()
    {
        return array(array('best_case'), array('worst_case'));
    }

    /**
     * @dataProvider dataProviderCaseFieldEqualsAmountWhenCaseFieldEmpty
     * @group opportunities
     */
    public function testCaseFieldEqualsAmountWhenCaseFieldEmpty($case)
    {
        $id = create_guid();
        $rli = SugarTestRevenueLineItemUtilities::createRevenueLineItem();
        $opportunity = SugarTestOpportunityUtilities::createOpportunity($id);
        $opportunity->revenuelineitems->add($rli);
        $rli->$case = '';
        $rli->opportunity_id = $id;
        $rli->save();
        $opportunity->save();
        $this->assertEquals($opportunity->$case, $opportunity->amount);
    }


    /**
     * @dataProvider dataProviderCaseFieldEqualsAmountWhenCaseFieldEmpty
     * @group opportunities
     */
    public function testCaseFieldEqualsZeroWhenCaseFieldSetToZero($case)
    {
        $id = create_guid();
        $rli = SugarTestRevenueLineItemUtilities::createRevenueLineItem();
        $opportunity = SugarTestOpportunityUtilities::createOpportunity($id);
        $opportunity->revenuelineitems->add($rli);
        $opportunity->sales_stage = "Prospecting";
        $rli->$case = $rli->likely_case = 0;
        $rli->opportunity_id = $id;
        $rli->save();
        $opportunity->$case = 0;
        $opportunity->save();
        $this->assertEquals(0, $opportunity->$case);
    }
    /**
     * This test checks to see if we correctly set the timeperiod_id value of an Opportunity record
     * @group forecasts
     * @group opportunities
     */
    public function testOpportunitySaveSelectProperTimePeriod()
    {
        global $timedate;
        $timedate->getNow();

        $tp = TimePeriod::retrieveFromDate('2009-02-15');

        if (empty($tp)) {
            $tp = SugarTestTimePeriodUtilities::createTimePeriod('2009-01-01', '2009-03-31');
        }

        $opp = SugarTestOpportunityUtilities::createOpportunity();
        $opp->date_closed = "2009-02-15";
        $opp->save();

        //check that the timeperiod covers the date closed timestamp
        $this->assertLessThan($opp->date_closed_timestamp, $tp->start_date_timestamp);
        $this->assertGreaterThanOrEqual($opp->date_closed_timestamp, $tp->end_date_timestamp);
    }

    /**
     * This test checks to see if we the opportunity is still included on the time period on the first day of the span
     * @group forecasts
     * @group opportunities
     */
    public function testOpportunitySaveFirstDayOfTimePeriod()
    {
        global $timedate;
        $timedate->getNow();

        $tp = TimePeriod::retrieveFromDate('2009-02-15');

        if (empty($tp)) {
            $tp = SugarTestTimePeriodUtilities::createTimePeriod('2009-01-01', '2009-03-31');
        }

        $opp = SugarTestOpportunityUtilities::createOpportunity();
        $opp->date_closed = "2009-01-02";
        $opp->save();

        //check that the timeperiod covers the date closed timestamp
        $this->assertLessThan($opp->date_closed_timestamp, $tp->start_date_timestamp);
        $this->assertGreaterThanOrEqual($opp->date_closed_timestamp, $tp->end_date_timestamp);
    }

    /**
     * This test checks to ensure that opportunities created with a date_closed value have a date_closed_timestamp
     * value that correctly falls within range of the timeperiod for that period.
     * @group forecasts
     * @group opportunities
     */
    public function testOpportunitySaveLastDayOfTimePeriod()
    {
        global $timedate;
        $timedate->getNow();

        $tp = TimePeriod::retrieveFromDate('2009-02-15');

        if (empty($tp)) {
            $tp = SugarTestTimePeriodUtilities::createTimePeriod('2009-01-01', '2009-03-31');
        }

        $opp = SugarTestOpportunityUtilities::createOpportunity();
        $opp->date_closed = "2009-03-31";
        $opp->save();

        //check that the timeperiod covers the date closed timestamp
        $this->assertLessThan($opp->date_closed_timestamp, $tp->start_date_timestamp);
        $this->assertGreaterThanOrEqual($opp->date_closed_timestamp, $tp->end_date_timestamp);
    }


    /**
     * Test that the base_rate field is populated with rate of currency_id
     * @group forecasts
     * @group opportunities
     */
    public function testCurrencyRate()
    {
        $opportunity = SugarTestOpportunityUtilities::createOpportunity();
        $currency = SugarTestCurrencyUtilities::getCurrencyByISO('MOD');
        // if Euro does not exist, will use default currency
        $opportunity->currency_id = $currency->id;
        $opportunity->name = "Test Opportunity Delete Me";
        $opportunity->amount = "5000.00";
        $opportunity->date_closed = TimeDate::getInstance()->getNow()->modify("+10 days")->asDbDate();
        $opportunity->best_case = "1000.00";
        $opportunity->worst_case = "600.00";
        $opportunity->save();
        $this->assertEquals(
            sprintf('%.6f', $opportunity->base_rate),
            sprintf('%.6f', $currency->conversion_rate)
        );
    }

    /**
     * Test that base currency exchange rates from EUR are working properly.
     * @group forecasts
     * @group opportunities
     */
    public function testBaseCurrencyAmounts()
    {
        $opportunity = SugarTestOpportunityUtilities::createOpportunity();
        $currency = SugarTestCurrencyUtilities::getCurrencyByISO('MOD');
        // if Euro does not exist, will use default currency
        $opportunity->currency_id = $currency->id;
        $opportunity->name = "Test Opportunity Delete Me";
        $opportunity->amount = "5000.00";
        $opportunity->date_closed = TimeDate::getInstance()->getNow()->modify("+10 days")->asDbDate();
        $opportunity->best_case = "1000.00";
        $opportunity->worst_case = "600.00";
        $opportunity->save();

        $this->assertEquals(
            sprintf('%.6f', $opportunity->base_rate),
            sprintf('%.6f', $currency->conversion_rate)
        );
    }

    /*
     * This method tests that a product record is created for new opportunity and that the necessary opportunity
     * field values are mapped to the product record
     * @group forecasts
     * @group opportunities
     */
    public function testProductEntryWasCreated()
    {
        $this->markTestIncomplete('Needs to be fixed by FRM team.');
        $opp = SugarTestOpportunityUtilities::createOpportunity();
        $opportunity = BeanFactory::getBean('Products');
        $opportunity->retrieve_by_string_fields(array('opportunity_id' => $opp->id));

        SugarTestProductUtilities::setCreatedProduct(array($opportunity->id));

        $expected = array($opp->name, $opp->amount, $opp->best_case, $opp->worst_case);
        $actual = array($opportunity->name, $opportunity->likely_case, $opportunity->best_case, $opportunity->worst_case);

        $this->assertEquals($expected, $actual);
    }


    /**
     * This method tests that subsequent changes to an opportunity will also update the associated product's data
     * @group forecasts
     * @group opportunities
     * @bug 56433
     */
    public function testOpportunityChangesUpdateRelatedProduct()
    {
        $opp = SugarTestOpportunityUtilities::createOpportunity();
        $opportunity = BeanFactory::getBean('Products');
        $opportunity->retrieve_by_string_fields(array('opportunity_id' => $opp->id));

        SugarTestProductUtilities::setCreatedProduct(array($opportunity->id));

        //Now we change the opportunity's values again
        $currency = SugarTestCurrencyUtilities::getCurrencyByISO('MOD');
        $opp->currency_id = $currency->id;
        $opp->save();

        $opportunity->retrieve_by_string_fields(array('opportunity_id' => $opp->id));
        $this->assertEquals(
            $opp->currency_id,
            $opportunity->currency_id,
            'The opportunity and product currency_id values differ'
        );
    }

    /**
     * This method tests that best/worst cases will be set to opp amount when sales stage is changed to Closed Won
     * @group forecasts
     * @group opportunities
     */
    public function testCaseFieldsEqualsAmountWhenSalesStageEqualsClosedWon()
    {
        $this->markTestIncomplete('SFA - This test is broken on Stack94 ENT');
        $opp = SugarTestOpportunityUtilities::createOpportunity();
        $opp->best_case = $opp->amount * 2;
        $opp->worst_case = $opp->amount / 2;
        $opp->save();

        $this->assertNotEquals($opp->best_case, $opp->amount);
        $this->assertNotEquals($opp->worst_case, $opp->amount);
        $opp->sales_stage = Opportunity::STAGE_CLOSED_WON;
        $opp->save();

        $this->assertEquals($opp->best_case, $opp->amount);
        $this->assertEquals($opp->worst_case, $opp->amount);
    }
    
    /**
     * @group opportunities
     * @group forecasts
     */
    public function testMarkDeleteDeletesForecastWorksheet()
    {
        SugarTestTimePeriodUtilities::createTimePeriod('2013-01-01', '2013-03-31');

        $opp = SugarTestOpportunityUtilities::createOpportunity();
        $opp->date_closed = '2013-01-01';
        $opp->save();

        $worksheet = SugarTestWorksheetUtilities::loadWorksheetForBean($opp);

        // assert that worksheet is not deleted
        $this->assertEquals(0, $worksheet->deleted);

        $opp->mark_deleted($opp->id);

        $this->assertEquals(1, $opp->deleted);

        // fetch the worksheet again
        unset($worksheet);
        $worksheet = SugarTestWorksheetUtilities::loadWorksheetForBean($opp, false, true);
        $this->assertEquals(1, $worksheet->deleted);
    }

    public function testMarkDeleteDeletesRelatedProducts()
    {
        SugarTestTimePeriodUtilities::createTimePeriod('2013-01-01', '2013-03-31');

        $opp = SugarTestOpportunityUtilities::createOpportunity();
        $opp->date_closed = '2013-01-01';
        $opp->save();

        $products = $opp->get_linked_beans('products', 'Products');

        $opp->mark_deleted($opp->id);
        $this->assertEquals(1, $opp->deleted);

        foreach($products as $product) {
            $p = BeanFactory::getBean($product->module_name);
            $p->retrieve($product->id, true, false);

            $this->assertEquals(1, $p->deleted);
        }
    }

    /**
     * @group opportunities
     */
    public function testGetClosedStages()
    {
        $opp = SugarTestOpportunityUtilities::createOpportunity();
        $closedStages = $opp->getClosedStages();
        $this->assertTrue(is_array($closedStages));
    }


    /**
     * @dataProvider dataProviderMapProbabilityFromSalesStage
     * @group opportunities
     */
    public function testMapProbabilityFromSalesStage($sales_stage, $probability)
    {
        /* @var $oppMock Opportunity */
        $oppMock = $this->getMock('Opportunity', array('save'));
        $oppMock->sales_stage = $sales_stage;
        // use the Reflection Helper to call the Protected Method
        SugarTestReflection::callProtectedMethod($oppMock, 'mapProbabilityFromSalesStage');

        $this->assertEquals($probability, $oppMock->probability);
    }

    /**
     * Test that related RLI's Account is always updated when we change it in Opportunity.
     * @group opportunities
     */
    public function testRelatedRLIUpdatesAccountChange()
    {
        $opportunity = SugarTestOpportunityUtilities::createOpportunity();
        $account_1 = SugarTestAccountUtilities::createAccount();
        $opportunity->account_id = $account_1->id;
        $rli = SugarTestRevenueLineItemUtilities::createRevenueLineItem();
        $opportunity->revenuelineitems->add($rli);

        $opportunity->save();
        $this->assertEquals($account_1->id, $rli->account_id, '1st save');

        //let's change Opportunity's Account and see what happens with RLI's related Account
        $opportunity->retrieve($opportunity->id);
        $account_2 = SugarTestAccountUtilities::createAccount();
        $opportunity->account_id = $account_2->id;
        $opportunity->save();
        $this->assertEquals($account_2->id, $rli->account_id, '2nd save');
    }

    public static function dataProviderMapProbabilityFromSalesStage()
    {
        return array(
            array('Prospecting', '10'),
            array('Qualification', '20'),
            array('Needs Analysis', '25'),
            array('Value Proposition', '30'),
            array('Id. Decision Makers', '40'),
            array('Perception Analysis', '50'),
            array('Proposal/Price Quote', '65'),
            array('Negotiation/Review', '80'),
            array('Closed Won', '100'),
            array('Closed Lost', '0')
        );
    }
}

class MockOpportunityBean extends Opportunity
{
    
}
