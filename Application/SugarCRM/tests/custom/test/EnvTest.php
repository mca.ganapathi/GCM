<?php

use PHPUnitColors\Display;
/**
* EnvTest
*/
class EnvTest extends Sugar_PHPUnit_Framework_TestCase
{
	public $feature_file_path = 'EnvTestData.json';

	public function setUp()
    {
        parent::setUp();
        $GLOBALS['current_user'] = SugarTestUserUtilities::createAnonymousUser();
        $GLOBALS['current_user']->is_admin = 1;
        SugarTestHelper::setUp('mod_strings', ['gc_Contracts']);
        ModUtils::setGlobalConfigWorkflowOfferings();
    }

    /**
     * @dataProvider providerOne
     */
    public function testOne($utid, $actual_result, $expected, $skip = false)
    {
    	global $current_user, $invalid_contract_id;
        // skip test data
        if(!empty($skip)) {
            $this->markTestSkipped($utid);
        }

        // check return array with our expected array
        $this->assertEquals($expected, $actual_result, $utid);
    }

    public function providerOne()
    {
    	$test_data = file_get_contents(dirname(__FILE__) . '/' . $this->feature_file_path);
        $test_data = ModUtils::object_to_array(json_decode($test_data));

        return $test_data;
    }
}
