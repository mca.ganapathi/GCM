<?php 

include_once 'custom/service/v4_1_custom/APIHelper/v2/helpers/ApiValidationHelper.php';

class CustomFunctionApiV2Test extends Sugar_PHPUnit_Framework_TestCase
{
	/**
     * @var string test data file name
     */
    public $test_case_02 = 'test_case_02.json';

    public function setUp()
    {
        parent::setUp();
        $GLOBALS['current_user'] = SugarTestUserUtilities::createAnonymousUser();
        $GLOBALS['current_user']->is_admin = 1;
        SugarTestHelper::setUp('mod_strings', ['gc_Contracts']);
        ModUtils::setGlobalConfigWorkflowOfferings();
    }

    /**
     * @dataProvider providerValidateXmlBillingAccountForBelugaProduct
     * @group DF-2009
     * @group DF-2127
     */
    public function testValidateXmlBillingAccountForBelugaProduct($utid, $request_type, $global_contract_id_uuid, $product_offering_id, $expected_result, $skip = false)
    {
        $request_data = [];
        $actual_result = [];
        $request_type = strtolower($request_type);
        $request_xml = dirname(__FILE__) . '/sample_data/' . $request_type . '/' . $utid . '.json';

        // skip test data
        if(!empty($skip) || !file_exists($request_xml)) {
            $this->markTestSkipped($utid);
        }
        // get xml contents
        $request_xml = file_get_contents($request_xml);
        $request_data = ModUtils::object_to_array(json_decode($request_xml));
        $actual_result = ApiValidationHelper::validateXmlBillingAccountForBelugaProduct($request_data, $request_type, $product_offering_id);

        $this->assertEquals($expected_result, $actual_result, $utid);
        
    }

    public function providerValidateXmlBillingAccountForBelugaProduct()
    {
        $test_data = file_get_contents(dirname(__FILE__) . '/test_data/' . $this->test_case_02);
        $test_data = ModUtils::object_to_array(json_decode($test_data));

        return $test_data;
    }

    public function tearDown()
    {
        parent::tearDown();
        SugarTestUserUtilities::removeAllCreatedAnonymousUsers();
    }
}