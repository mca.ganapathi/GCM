<?php

use PHPUnitColors\Display;
/**
 * ModUtils Class
 */
require_once 'custom/include/ModUtils.php';
include_once 'custom/modules/gc_Contracts/CustomFunctionGcContract.php';

class CustomFunctionGcContractTest extends Sugar_PHPUnit_Framework_TestCase
{
    private $gc_bean;
    private $custom_gc_contracts;
    private $actual_result;
	/**
     * @var object mod_utils
     */
    public $mod_utils;

    /**
     * @var string test data file name
     */
    public $test_case_01 = 'test_case_01.json';
    public $test_case_03 = 'test_case_03.json';
    public $test_case_04 = 'test_case_04.json';

    public function setUp()
    {
        parent::setUp();
        $GLOBALS['current_user'] = SugarTestUserUtilities::createAnonymousUser();
        $GLOBALS['current_user']->is_admin = 1;
        SugarTestHelper::setUp('mod_strings', ['gc_Contracts']);
        ModUtils::setGlobalConfigWorkflowOfferings();
        $this->mod_utils = new ModUtils();
    }

    /**
     * @dataProvider providerValidateBillingAccForActivation
     * @group DF-1939
     * @group DF-2127
     */
    public function testValidateBillingAccForActivation($utid, $global_contract_object_id, $expected, $skip = false)
    {
        global $current_user, $invalid_contract_id;
        // skip test data
        if(!empty($skip)) {
            $this->markTestSkipped($utid);
        }

        $gc_bean = BeanFactory::getBean('gc_Contracts', $global_contract_object_id);
        // check contract id is valid or not
        $this->assertEquals($gc_bean->id, $global_contract_object_id, $utid);

        $this->custom_gc_contracts = new CustomFunctionGcContract();
        $this->actual_result = $this->custom_gc_contracts->validateBillingAccForActivation($global_contract_object_id, '');
        // check return array with our expected array
        $this->assertEquals($expected, $this->actual_result, $utid);
    }

    /**
     * @dataProvider providerArrayDiffAssocRecursive
     * @group DF-2096
     */
    public function testArrayDiffAssocRecursive($utid, $expected_result, $skip = false)
    {
    	$actual_result = [];
    	$sample_json = dirname(__FILE__) . '/sample_data/' . $utid . '.json';
    	// skip test data
        if(!empty($skip) || !file_exists($sample_json)) {
            $this->markTestSkipped($utid);
        }

        $sample_data = file_get_contents($sample_json);
        $sample_data = ModUtils::object_to_array(json_decode($sample_data));
        $xml_line_item = !empty($sample_data['xml']) ? $sample_data['xml'] : [];
        $db_line_item = !empty($sample_data['db']) ? $sample_data['db'] : [];

        $actual_result = $this->mod_utils->array_diff_assoc_recursive($xml_line_item, $db_line_item);

    	$this->assertEquals($expected_result, $actual_result, $utid);
    }

    /**
     * @dataProvider providerGetDbDate
     * @group OM-356
     */
    public function testGetDbDate($utid, $current_user_dateformat, $datetime, $expected_result, $skip = false)
    {
        global $current_user;
        $actual_result = '';

        // skip test data
        if(!empty($skip)) {
            $this->markTestSkipped($utid);
        }

        // set current user's date format
        if(!empty($current_user_dateformat)) {
            $current_user->setPreference('datef', $current_user_dateformat);
        }

        // get the actual result from the function
        $actual_result =  $this->mod_utils->getDbDate($datetime);

        $this->assertEquals($expected_result, $actual_result, $utid);
    }

    public function providerValidateBillingAccForActivation()
    {
        $test_data = file_get_contents(dirname(__FILE__) . '/test_data/' . $this->test_case_01);
        $test_data = ModUtils::object_to_array(json_decode($test_data));

        return $test_data;
    }

    public function providerArrayDiffAssocRecursive()
    {
        $test_data = file_get_contents(dirname(__FILE__) . '/test_data/' . $this->test_case_03);
        $test_data = ModUtils::object_to_array(json_decode($test_data));

        return $test_data;
    }

    public function providerGetDbDate()
    {
        $test_data = file_get_contents(dirname(__FILE__) . '/test_data/' . $this->test_case_04);
        $test_data = ModUtils::object_to_array(json_decode($test_data));

        return $test_data;
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->gc_bean);
        unset($this->custom_gc_contracts);
        unset($this->actual_result);
        unset($this->mod_utils);
        SugarTestUserUtilities::removeAllCreatedAnonymousUsers();
    }
}
