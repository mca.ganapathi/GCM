<?php
$viewdefs['Accounts']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'common_customer_id_c' => 
    array (
      'type' => 'varchar',
      'default' => true,
      'width' => '10%',
      'name' => 'common_customer_id_c',
      'vname' => 'LBL_COMMON_CUSTOMER_ID',
    ),
    'name' => 
    array (
    ),
    'name_2_c' => 
    array (
      'type' => 'varchar',
      'default' => true,
      'width' => '10%',
      'name' => 'name_2_c',
      'vname' => 'LBL_NAME_2',
    ),
    'name_3_c' => 
    array (
      'type' => 'varchar',
      'default' => true,
      'width' => '10%',
      'name' => 'name_3_c',
      'vname' => 'LBL_NAME_3',
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);
