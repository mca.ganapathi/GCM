<?php

/**
 * Extend sugar config
 */

/**
 * Set Japan country code.
 */
$sugar_config['gcm_cstm_const']['country_code']['JPN'] = '392';

/**
 * Set US country code.
 */
$sugar_config['gcm_cstm_const']['country_code']['USA'] = '840';

/**
 * Set Canada country code.
 */
$sugar_config['gcm_cstm_const']['country_code']['CAN'] = '124';

/**
 * Set Default timezone value.
 */
$sugar_config['gcm_cstm_const']['timezone']['gmt'] = '+00:00';

/**
 * Set default contract end datetime value.
 */
$sugar_config['gcm_cstm_const']['default_end_datetime'] = '9000-01-01T00:00:00Z';

/**
 * Set default exclude array for GBS API billing_affiliate_code.
 */
$sugar_config['gcm_cstm_const']['gbs_api_exclude']['billing_affiliate_code'][0] = 'DMY';

/**
 * Set default ViewAPILog row limit.
 */
$sugar_config['gcm_cstm_const']['view_api_log']['row_limit'] = '20';

/**
 * Set default ViewAPILog database name.
 */
$sugar_config['gcm_cstm_const']['view_api_log']['log_db_name'] = 'apilog';

/**
 * Set max email length for application
 */
$sugar_config['gcm_cstm_const']['email_max_length'] = 3;
?>