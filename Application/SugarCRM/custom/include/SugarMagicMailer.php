<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
require_once ('data/BeanFactory.php');
require_once ('include/SugarPHPMailer.php');
require_once ('include/OutboundEmail/OutboundEmail.php');
require_once ('modules/EmailTemplates/EmailTemplate.php');
require_once ('include/workflow/alert_utils.php');
require_once ('modules/Administration/Administration.php');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

/**
 * SugarMagicMailer Class
 */
class SugarMagicMailer
{

    /* Class Variable for template */
    var $template;

    /* Class Variable for mail */
    var $mail;

    /**
     * Constructor Method
     * 
     * @author shrikant.gaware
     * @since Mar 01, 2016
     */
    public function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Constructor Method');
        $this->mail = new SugarPHPMailer();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method get bean of email template based on supplied email template name
     * 
     * @param string $template_name template name
     * @param string $template_type template type
     * @author shrikant.gaware
     * @since Mar 01, 2016
     */
    private function setEmailTemplate($template_name, $template_type)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'template_name : ' . $template_name . GCM_GL_LOG_VAR_SEPARATOR . 'template_type : ' . $template_type, 'Method get bean of email template based on supplied email template name');
        $this->template = new EmailTemplate();
        $this->template->retrieve_by_string_fields(array(
                                                        'name' => $template_name, 
                                                        'type' => $template_type
        ));
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method to replace all the fields name in the email body & subject
     * 
     * @param string $record_id record id of the module
     * @param string $bean_name module name
     * @author shrikant.gaware
     * @since Mar 01, 2016
     */
    private function parseEmailTemplate($record_id, $bean_name)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'record_id : ' . $record_id . GCM_GL_LOG_VAR_SEPARATOR . 'bean_name : ' . $bean_name, 'Method to replace all the fields name in the email body & subject');
        $module_bean = BeanFactory::getBean($bean_name);
        $module_bean->retrieve($record_id);
        
        $this->template->subject = parse_alert_template($module_bean, $this->template->subject, '');
        $this->template->subject = (!empty($this->template->subject) && isset($this->template->subject)) ? $this->template->subject : '';
        $this->template->body_html = parse_alert_template($module_bean, $this->template->body_html, '');
        $this->template->body_html = (!empty($this->template->body_html) && isset($this->template->body_html)) ? $this->template->body_html : '';
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method to send mail to supplied email ids
     * 
     * @param array $email_parms = array('to_addr'=> array(), 'cc_addr' => array(), 'bcc_addr'=> array(), 'attachments'=> array(), 'template_name' => '<name of the email template>') <array> $cc_addr email-is for cc email fields <array> $bcc_addr email-is for bcc email fields <array> $attachments mail attachments if any
     * @param string $bean_name module name
     * @param string $record_id record id of the module
     * @return boolean true/false mail sent of not
     * @author shrikant.gaware
     * @since Mar 01, 2016
     */
    public function sendSugarMail($email_parms, $bean_name, $record_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'email_parms : ' . print_r($email_parms, true) . GCM_GL_LOG_VAR_SEPARATOR . 'bean_name : ' . print_r($bean_name, true) . GCM_GL_LOG_VAR_SEPARATOR . 'record_id : ' . $record_id, 'Method to send mail to supplied email ids');
        $oe = new OutboundEmail();
        $oe = $oe->getSystemMailerSettings();
        
        $reply_to_addr = $oe->mail_smtpuser;
        $emailreplyto = $reply_to_addr;
        
        $this->configureMailObject();
        
        $this->setEmailTemplate($email_parms['template_name'], $email_parms['template_type']);
        $this->parseEmailTemplate($record_id, $bean_name);
        $this->mail->Subject = $this->template->subject; // email subject
        
        $process_message_body = html_entity_decode(from_html($this->template->body_html), ENT_QUOTES, 'UTF-8');
        
        $this->mail->Body = $process_message_body;
        if(!empty($email_parms['attachments'])) {
            $this->handleAddEmailAddress($email_parms['attachments'], 'AddAttachment'); // add attachment
        }
        if(!empty($email_parms['to_addr'])) {
            $this->handleAddEmailAddress($email_parms['to_addr'], 'AddAddress'); // add "to" email address
        }
        if(!empty($email_parms['cc_addr'])) {
            $this->handleAddEmailAddress($email_parms['cc_addr'], 'AddCC'); // add "cc" email address
        }
        if(!empty($email_parms['bcc_addr'])) {
            $this->handleAddEmailAddress($email_parms['bcc_addr'], 'AddBCC'); // add "bcc" email address
        }
        
        /* Override admin settings From email address & From name of admin settings */
        // $this->mail->Sender='';
        // $this->mail->SetFrom('<email>', '<name>', FALSE);
        /* Override admin settings From email address & From name of admin settings */
        
        if (!$this->mail->send()) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_TRACER, __FILE__ , __METHOD__, 'EMAIL SENT FAIL', '');
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
            return false;
        } else {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_TRACER, __FILE__ , __METHOD__, 'EMAIL SENT SUCESS', '');
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
            return true;
        }
    }

    /**
     * Method to configure prerequisite to send an email
     * 
     * @author shrikant.gaware
     * @since Mar 01, 2016
     */
    private function configureMailObject()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method to configure prerequisite to send an email');
        $admin = new Administration();
        $admin->retrieveSettings();
        
        // Start : Configure email Object
        if ($admin->settings['mail_sendtype'] == "SMTP") {
            $this->mail->Mailer = "smtp";
            $this->mail->SMTPKeepAlive = true;
            $this->mail->Host = $admin->settings['mail_smtpserver'];
            $this->mail->Port = $admin->settings['mail_smtpport'];
            
            if ($admin->settings['mail_smtpssl'] == 1)
                $this->mail->SMTPSecure = 'ssl';
            else 
                if ($admin->settings['mail_smtpssl'] == 2)
                    $this->mail->SMTPSecure = 'tls';
                else
                    $this->mail->SMTPSecure = '';
            
            if ($admin->settings['mail_smtpauth_req']) {
                $this->mail->SMTPAuth = TRUE;
                $this->mail->Username = $admin->settings['mail_smtpuser'];
                $this->mail->Password = $admin->settings['mail_smtppass'];
            }
        } else {
            $this->mail->mailer = 'sendmail';
        }
        $this->mail->ContentType = "text/html"; // "text/plain"
        $this->mail->From = $admin->settings['notify_fromaddress'];
        $this->mail->FromName = $admin->settings['notify_fromname'];
        // End : Configure email Object
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method to add email address to email object
     * 
     * @param array $to_addr array of email ids or attachements
     * @param string $objAddressType type of object email/attachment
     * @return boolean true/false
     * @author shrikant.gaware
     * @since Mar 01, 2016
     */
    private function handleAddEmailAddress($to_addr, $objAddressType)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'to_addr : ' . print_r($to_addr, true) . GCM_GL_LOG_VAR_SEPARATOR . 'objAddressType : ' . $objAddressType, 'Method to add email address to email object');
        $result = true;
        if (is_array($to_addr)) {
            // if parameter is array
            foreach ($to_addr as $i => $e_id) {
                // check whether array value is not empty/blank
                if (!empty($e_id))
                    $this->mail->$objAddressType($e_id, $i); // params (email address,recipient name)
            }
        } else
            $result = false;
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'result : ' . $result, '');
        return $result;
    }
}
?>