<?php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
require_once ('include/MVC/View/views/view.list.php');

/**
 * Class to extend List View
 */
class CustomViewList extends ViewList
{

    /**
     * Method of listViewProcess
     */
    function listViewProcess()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method of listViewProcess');
        // Hide Mass Update action
        $this->lv->showMassupdateFields = false;
        
        // Hide Merge action
        $this->lv->mergeduplicates = false;
        
        // Hide Delete action
        $this->lv->delete = false;
        
        // Hide Export action
        $this->lv->export = false;
        
        $this->processSearchForm();
        $this->lv->searchColumns = $this->searchForm->searchColumns;
        if (!$this->headers)
            return;
        if (empty($_REQUEST['search_form_only']) || $_REQUEST['search_form_only'] == false) {
            $this->lv->ss->assign("SEARCH", true);
            $this->lv->setup($this->seed, 'custom/include/ListView/ListViewGeneric.tpl', $this->where, $this->params);
            $savedSearchName = empty($_REQUEST['saved_search_select_name']) ? '' : (' - ' . $_REQUEST['saved_search_select_name']);
            echo $this->lv->display();
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method of prepareSearchForm
     */
    function prepareSearchForm()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        $this->searchForm = null;
        
        // search
        $view = 'basic_search';
        if (!empty($_REQUEST['search_form_view']) && $_REQUEST['search_form_view'] == 'advanced_search')
            $view = $_REQUEST['search_form_view'];
        $this->headers = true;
        
        if (!empty($_REQUEST['search_form_only']) && $_REQUEST['search_form_only'])
            $this->headers = false;
        elseif (!isset($_REQUEST['search_form']) || $_REQUEST['search_form'] != 'false') {
            if (isset($_REQUEST['searchFormTab']) && $_REQUEST['searchFormTab'] == 'advanced_search') {
                $view = 'advanced_search';
            } else {
                $view = 'basic_search';
            }
        }
        $this->view = $view;
        
        $this->use_old_search = true;
        if (SugarAutoLoader::existingCustom('modules/' . $this->module . '/SearchForm.html') && !SugarAutoLoader::existingCustom('modules/' . $this->module . '/metadata/searchdefs.php')) {
            
            require_once ('custom/include/SearchForm/SearchForm.php');
            $this->searchForm = new CustomSearchForm($this->module, $this->seed);
        } else {
            $this->use_old_search = false;
            require_once ('custom/include/SearchForm/SearchForm2.php');
            $searchMetaData = SearchForm::retrieveSearchDefs($this->module);
            $this->searchForm = $this->getSearchForm2($this->seed, $this->module, $this->action);
            $this->searchForm->setup($searchMetaData['searchdefs'], $searchMetaData['searchFields'], 'SearchFormGeneric.tpl', $view, $this->listViewDefs);
            $this->searchForm->lv = $this->lv;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method of getSearchForm2
     * 
     * @param object $seed
     * @param object $module
     * @param string $action
     * @return object of SearchForm
     */
    protected function getSearchForm2($seed, $module, $action = "index")
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'seed : '. print_r($seed, true) .GCM_GL_LOG_VAR_SEPARATOR. 'module ' . print_r($module, true) .GCM_GL_LOG_VAR_SEPARATOR. 'action : ' . $action , '');
        // SearchForm2.php is required_onced above before calling this function
        // hence the order of parameters is different from SearchForm.php
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return new CustomSearchForm($seed, $module, $action);
    }
}
