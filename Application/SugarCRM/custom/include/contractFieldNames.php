<?php 
$CONTRACT_FIELD_NAMES = array
    (
        'ContractsHeader' => array
            (
                'global_contract_id_xml_id' => 'Global contract key',
                'global_contract_id' => 'Global contract key',
                'object_id' => 'global uuid for contract',
                'contract_version' => 'Contract Version',
                'name' => 'Contract Name',
                'description' => 'Contract Description',
                'contract_status' => 'Contract Status',
                'contract_request_id' => 'Contract Request Id',
                'product_offering' => 'Product offering id',
                'product_offering_xml_id' => 'Product Offering Key',
                'ext_sys_created_by' => 'Create By',
                'ext_sys_modified_by' => 'Modified By',
                'contract_startdate' => 'Contract Start Date',
                'contract_startdate_timezone' => 'Contract Start Date Time Zone',
                'contract_enddate' => 'Contract End Date',
                'contract_enddate_timezone' => 'Contract End Date Time Zone',
                'lock_version' => 'Lock Version',
                'total_nrc' => 'Total NRC',
                'total_mrc' => 'Total MRC',
                'grand_total' => 'Contract Grand Total',
                'workflow_flag' => 'Workflow Flag',
                'contract_scheme' => 'Contract Scheme',
                'contract_scheme_xml_id' => 'Contract Scheme key',
                'contract_type' => 'Contract Type',
                'contract_type_xml_id' => 'Contract type key',
                'loi_contract' => 'LOI Contract',
                'loi_contract_xml_id' => 'LOI Contract Key',
                'billing_type' => 'Billing Type',
                'billing_type_xml_id' => 'Billing Type',
                'factory_reference_no' => 'Factory Reference Number',
                'factory_reference_no_xml_id' => 'Factory Reference Number Key',
				'vat_no' => 'VAT Number',
                /*'surrogate_teams' => array
                    (
                        '0' => 'GMOne SO Team',
                    ),

                'surrogate_teams_xml_id' => array
                    (
                        '0' => 'ffc7726f-a440-414e-8771-b37f954835f0',
                    ),*/

            ),

        'ContractSalesRepresentative' => array
            (
                'sales_channel_code' => 'X1234567',
                'name' => '',
                'sales_rep_name_1' => 'Jeff Beck',
                'division_1' => '',
                'division_2' => 'Cloud sales division',
                'title_1' => '',
                'title_2' => '',
                'tel_no' => '+44-20-7977-1000',
                'ext_no' => '',
                'fax_no' => '+44-20-7977-1001',
                'mob_no' => '',
                'email1' => 'jeff.beck@ntt.com',
                'sales_company' => 'NTT Europe Ltd.',
            ),

        'ContractLineItemHeader' => array
            (
                'service_start_date' => 'Service Start Date',
                'service_start_date_xml_id' => 'Service Star Date key',
                'billing_start_date' => 'Billing Start Date',
                'billing_start_date_xml_id' => 'Billing Start Date Key',
                'service_end_date' => 'Service End Date',
                'service_end_date_xml_id' => 'Service End Date key',
                'billing_end_date' => 'Billing End Date',
                'billing_end_date_xml_id' => 'Billing End Date Key',
                'service_start_date_timezone' => 'Service Start Date Time Zone',
                'billing_start_date_timezone' => 'Billing Start Date Time Zone',
                'service_end_date_timezone' => 'Service End Date Time Zone',
                'billing_end_date_timezone' => 'Billing End Date Time Zone',
                'factory_reference_no' => 'Factory reference number of LineItem',
                'factory_reference_no_xml_id' => 'Factory reference number key of LineItem',
                'product_specification' => 'Procut Specification',
                'product_specification_xml_id' => 'Procut Specification Key of LineItem',
                'cust_contract_currency' => 'Customer Contract Currency',
                'cust_contract_currency_xml_id' => 'Customer Contract Currency Key',
                'cust_billing_currency' => 'Customer Billing Currency',
                'cust_billing_currency_xml_id' => 'Customer Billing Currency Key',
                'igc_contract_currency' => 'IGC Contract Currency',
                'igc_contract_currency_xml_id' => 'IGC Contract Currency Key',
                'igc_billing_currency' => 'IGC Billing Currency',
                'igc_billing_currency_xml_id' => 'IGC Billing Currency Key',
                'customer_billing_method' => 'Customer Billing Method',
                'customer_billing_method_xml_id' => 'Customer Billing Method Key',
                'igc_settlement_method' => 'IGC Settlement Method',
                'igc_settlement_method_xml_id' => 'IGC Settlement Method Key',
                'description' => 'LineItem Description',
                'description_xml_id' => 'LineItem Description Key',
                'contract_line_item_type' => 'Contract LineItem Type',
                'bi-action' => 'bi-action',
                'line_item_id_xml_id' => 'LineItem Key',
                'payment_type' => 'Payment Type',
            ),

        'InvoiceGroupDtls' => array
            (
                'name' => 'Invoice Group Name',
                'name_xml_id' => 'Invoice Group Name Key',
                'description' => 'Invoice Group Description',
                'description_xml_id' => 'Invoice Group Description Key',
            ),
        'ContractLineProductConfiguration' => array
            (
                'name' => 'Contract LineItem Prodcut Configuration Name',
                'characteristic_value' => 'Contract LineItem Prodcut Configuration Value',
                'characteristic_xml_id' => 'Contract LineItem Prodcut Configuration Value Key',
            ),
        'ContractLineProductRule' => array
            (
                'name' => 'Contract LineItem Prodcut Rule Name',
                'description' => 'Contract LineItem Prodcut Rule Value',
                'rule_xml_id' => 'Contract LineItem Prodcut Rule Value Key',
            ),
        'ContractLineProductPrice' => array
            (
                
                'name' => 'Contract LineItem Prodcut Price Name',
                'charge_xml_id' => 'Contract LineItem Prodcut Price Key',
                'currencyid' => array
                    (
                        'xml_id' => 'Contract LineItem Prodcut Price Currency Key',
                        'xml_value' => 'Contract LineItem Prodcut Price Currency Value',
                    ),
                'icb_flag' => array
                    (
                        'xml_id' => 'Contract LineItem Prodcut ICB Flag Key',
                        'xml_value' => 'Contract LineItem Prodcut ICB Flag Value',
                    ),
                'charge_type' => array
                    (
                        'xml_id' => 'Contract LineItem Prodcut Charge Type Key',
                        'xml_value' => 'Contract LineItem Prodcut Charge Type Value',
                    ),
                'charge_period' => array
                    (
                        'xml_id' => 'Contract LineItem Prodcut Charge Period Key',
                        'xml_value' => 'Contract LineItem Prodcut Charge Period Value',
                    ),
                'list_price' => array
                    (
                        'xml_id' => 'Contract LineItem Prodcut List Price Key',
                        'xml_value' => 'Contract LineItem Prodcut List Price Value',
                    ),
                'discount_rate' => array
                    (
                        'xml_id' => 'Contract LineItem Prodcut Dicount Rate Key',
                        'xml_value' => 'Contract LineItem Prodcut Dicount Rate Value',
                    ),
                'discount_amount' => array
                    (
                        'xml_id' => 'Contract LineItem Prodcut Discount Amount Currency Key',
                        'xml_value' => 'Contract LineItem Prodcut Discount Amount Currency Value',
                    ),
                'customer_contract_price' => array
                    (
                        'xml_id' => 'Contract LineItem Prodcut Customer Contract Price Key',
                        'xml_value' => 'Contract LineItem Prodcut Customer Contract Price Value',
                    ),
                'igc_settlement_price' => array
                    (
                        'xml_id' => 'Contract LineItem Prodcut IGC Settlement Key',
                        'xml_value' => 'Contract LineItem Prodcut IGC Settlement Value',
                    ),
            ),
        'ContractCreditCard' => array
            (
                'credit_card_no' => 'Credit card number',
                'expiration_date' => 'Credit card expiration date',
                'payment_type' => 'Payment Type',
            ),
            
        

        'Accounts' => array
            (
                'Contract' => array
                    (
                        'contact_type_c' => 'Contract',
                        'last_name' => '',
                        'name_2_c' => 'Howard Duane Allman',
                        'postal_no_c' => '10017',
                        'addr_1_c' => '',
                        'addr_2_c' => '',
                        'division_1_c' => '',
                        'division_2_c' => 'Institute of network technology',
                        'title' => '',
                        'title_2_c' => 'Architect',
                        'phone_work' => '+1-212-661-0810',
                        'ext_no_c' => '',
                        'phone_fax' => '+1-212-661-1078',
                        'phone_mobile' => '',
                        'email1' => 'howard.duane.allman@ntt.com',
                        'country_code_numeric' => '840',
                        'Accounts_js' => array
                            (
                                'addr1' => '',
                                'addr2' => '',
                            ),

                        'general_address' => array
                            (
                                'addr_general_c' => '757 Third Avenue, 14th Floor',
                                'city_general_c' => 'New York',
                                'state_general_code' => 'NY',
                            ),

                        'CorporateDetails' => array
                            (
                                'common_customer_id_c' => 'C0250124868',
                                'name' => 'NTT COMMUNICATIONS CORPORATION',
                                'name_2_c' => 'エヌ・ティ・ティ・コミュニケーションズ株式会社',
                                'name_3_c' => 'エヌテイテイコミユニケーシヨンズ',
                                'country_code_c' => '392',
                                'country_name_c' => 'JAPAN',
                                'customer_status_code_c' => '0',
                                'hq_addr_1_c' => 'NTT HIBIYA BLDG. 1-1-6  UCHISAIWAICHO CHIYODA-KU TOKYO 1000011 JAPAN',
                                'hq_addr_2_c' => '東京都千代田区内幸町１丁目１－６　ＮＴＴ日比谷ビル',
                                'hq_postal_no_c' => '1000011',
                                'reg_addr_1_c' => '',
                                'reg_addr_2_c' => '東京都千代田区内幸町１丁目１－６',
                                'reg_postal_no_c' => '1000011',
                                'cidas_data' => 'Yes',
                                'cidas_search' => 'Yes',
                            ),

                    ),

                'Billing' => array
                    (
                        'contact_type_c' => 'Billing',
                        'last_name' => '',
                        'name_2_c' => 'Oscar Emmanuel Peterson',
                        'postal_no_c' => 'T1L 1B3',
                        'addr_1_c' => '',
                        'addr_2_c' => '',
                        'division_1_c' => '',
                        'division_2_c' => 'Global procurement group',
                        'title' => '',
                        'title_2_c' => 'Manager',
                        'phone_work' => '+1-403-762-8686',
                        'ext_no_c' => '4',
                        'phone_fax' => '',
                        'phone_mobile' => '',
                        'email1' => 'oscar.emmanuel.peterson@ntt.com',
                        'country_code_numeric' => '124',
                        'Accounts_js' => array
                            (
                                'addr1' => '',
                                'addr2' => '',
                            ),

                        'general_address' => array
                            (
                                'addr_general_c' => '222 Lynx St., C/O Banff Park Lodge, P.O.Box 1431',
                                'city_general_c' => 'Banff',
                                'state_general_code' => 'AB',
                            ),

                        'CorporateDetails' => array
                            (
                                'common_customer_id_c' => '',
                                'name' => '',
                                'name_2_c' => 'NTT Canada',
                                'name_3_c' => '',
                                'country_code_c' => '',
                                'country_name_c' => '',
                                'customer_status_code_c' => '0',
                                'hq_addr_1_c' => '',
                                'hq_addr_2_c' => '',
                                'hq_postal_no_c' => '',
                                'reg_addr_1_c' => '',
                                'reg_addr_2_c' => '',
                                'reg_postal_no_c' => '',
                                'cidas_data' => 'No',
                                'cidas_search' => 'No',
                            ),

                    ),

                'Technology' => array
                    (
                        'contact_type_c' => 'Technology',
                        'last_name' => 'Haruomi Hosono',
                        'name_2_c' => '細野　晴臣',
                        'postal_no_c' => '1008019',
                        'addr_1_c' => '',
                        'addr_2_c' => '',
                        'division_1_c' => 'System Division',
                        'division_2_c' => 'システム部',
                        'title' => '',
                        'title_2_c' => '',
                        'phone_work' => '03-6700-9927',
                        'ext_no_c' => '',
                        'phone_fax' => '03-3509-7582',
                        'phone_mobile' => '',
                        'email1' => 'haruomi.hosono@ntt.com',
                        'country_code_numeric' => '392',
                        'Accounts_js' => array
                            (
                                'addr1' => '東京都千代田区内幸町1丁目',
                                'addr2' => '1-6 NTT日比谷ビル3階',
                            ),

                        'general_address' => array
                            (
                                'addr_general_c' => '222 Lynx St., C/O Banff Park Lodge, P.O.Box 1431',
                                'city_general_c' => '',
                                'state_general_code' => '',
                            ),

                        'CorporateDetails' => array
                            (
                                'common_customer_id_c' => '',
                                'name' => 'NTT Communications Corporation',
                                'name_2_c' => 'エヌ・ティ・ティ・コミュニケーションズ（株）',
                                'name_3_c' => '',
                                'country_code_c' => '',
                                'country_name_c' => '',
                                'customer_status_code_c' => '0',
                                'hq_addr_1_c' => '',
                                'hq_addr_2_c' => '',
                                'hq_postal_no_c' => '',
                                'reg_addr_1_c' => '',
                                'reg_addr_2_c' => '',
                                'reg_postal_no_c' => '',
                                'cidas_data' => 'No',
                                'cidas_search' => 'No',
                            ),

                    ),

            ),

    );


$LI_MANDATORY_VERSIONUP_FIELDS = array
    (
        'ContractLineItemHeader' => array
            (
                'service_start_date_xml_id' => 'Service Start Date Key',
                'billing_start_date_xml_id' => 'Billing Start Date Key',
                'service_end_date_xml_id' => 'Service End Date Key',
                'billing_end_date_xml_id' => 'Billing End Date Key',
                'factory_reference_no_xml_id' => 'Factory reference number Key',
                'product_specification_xml_id' => 'Product Specification Key',
                'cust_contract_currency_xml_id' => 'Customer Contract Currency Key',
                'cust_billing_currency_xml_id' => 'Customer Billing Currency Key',
                'igc_contract_currency_xml_id' => 'Inter Group Company Contract Currency Key',
                'igc_billing_currency_xml_id' => 'Inter Group Company Billing Currency Key',
                'customer_billing_method_xml_id' => 'Customer Billing Method Key',
                'igc_settlement_method_xml_id' => 'Inter Group Company Settlement Method Key ',
                'description_xml_id' => 'Remarks Value Key',
                'line_item_id_xml_id' => 'Bi-Item Key'        
            ),
        'ContractLineProductConfiguration' => array
            (
                'characteristic_xml_id' => 'Product Configuration Characteristics Key'
            ),
        'ContractLineProductRule' => array
            (
                'rule_xml_id' => ' Product Rule Key'
            ),
        'ContractLineProductPrice' => array
            (
                'charge_xml_id' => ' Product Price Charge',
                'xml_data' => array 
                    (
                        'currencyid' => array
                            (
                                'xml_id' => 'Currency Id'
                            ),
                        'icb_flag' => array
                            (
                                'xml_id' => 'ICB Flag Id'
                            ),
                        'charge_type' => array
                            (
                                'xml_id' => 'Charge Type Id'
                            ),
                        'charge_period' => array
                            (
                                'xml_id' => 'Charge Period Id'
                            ),
                        'list_price' => array
                            (
                                'xml_id' => 'List Price',
                            ),
                        'discount_rate' => array
                            (
                                'xml_id' => 'Discount Rate',
                            ),
                        'discount_amount' => array
                            (
                                'xml_id' => 'Discount Amount',
                            ),
                        'customer_contract_price' => array
                            (
                                'xml_id' => 'Customer Contract Price',
                            ),
                        'igc_settlement_price' => array
                            (
                                'xml_id' => 'IGC Settlement Price',
                            ),
                    )
            ),
        'InvoiceGroupDtls' => array
            (
                'name_xml_id' => 'Group Name Value Id',
                'description_xml_id' => 'Group Description Value Id',
            )
        
    );
$LI_OPTIONAL_VERSIONUP_FIELDS = array
(
    'ContractLineItemHeader' => array
        (
            'service_start_date_xml_id' => 'Service Start Date Key',
            'billing_start_date_xml_id' => 'Billing Start Date Key',
            'service_end_date_xml_id' => 'Service End Date Key',
            'billing_end_date_xml_id' => 'Billing End Date Key',
            'factory_reference_no_xml_id' => 'Factory reference number Key',
            'cust_billing_currency_xml_id' => 'Customer Billing Currency Key',
            'igc_contract_currency_xml_id' => 'Inter Group Company Contract Currency Key',
            'igc_billing_currency_xml_id' => 'Inter Group Company Billing Currency Key',
            'customer_billing_method_xml_id' => 'Customer Billing Method Key',
            'igc_settlement_method_xml_id' => 'Inter Group Company Settlement Method Key ',
            'description_xml_id' => 'Remarks Value Key'
        ),
);