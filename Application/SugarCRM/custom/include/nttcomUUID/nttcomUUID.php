<?php

/* Handle NTT Communications' UUID
 *
 * MMXV by IMOTO Takashi, Systems, NTT Communications (t.imoto@ntt.com)
 *
 * Refere to another document about NTT Communications' UUID.
 */


class RFC4122UUID {

    protected $data;

    public function __construct($data) {
        $this->data = $data;
    }
    
    /*
     *                                                                                                                     *
     * fromString($ame)
     * 
     * Creates a UUID from the string standard representation as described in the toString() method.
     * 
     * Parameters:
     *     name - A string that specifies a UUID
     * Returns:
     *     A UUID with the specified value
     * Throws:
     * IllegalArgumentException - If name does not conform to the string representation as described in toString()    
     */
    public static function fromString($name)
    {
        /*
         * 128bit UUID is represented in the from: aaaaaaaa-bbbb-cccc-dddd-eeeeffffgggg
         * where aaaaaaaa is msb 32bit, bbbb is next 16bit, cccc is next 16bit, dddd is next 16bit, eeeeffffgggg is lsb 48bit
         */
        if (!preg_match("/([[:xdigit:]]{8})-([[:xdigit:]]{4})-([[:xdigit:]]{4})-([[:xdigit:]]{4})-([[:xdigit:]]{4})([[:xdigit:]]{4})([[:xdigit:]]{4})/", $name, $uuid_part))
            return -1;

        $part1 = ("0x" . $uuid_part[1]) + 0;	// aaaaaaaa
        $part2 = ("0x" . $uuid_part[2]) + 0;	// bbbb
        $part3 = ("0x" . $uuid_part[3]) + 0;	// cccc
        $part4 = ("0x" . $uuid_part[4]) + 0;	// dddd
        $part5a = ("0x" . $uuid_part[5]) + 0;	// eeee
        $part5b = ("0x" . $uuid_part[6]) + 0;	// ffff
        $part5c = ("0x" . $uuid_part[7]) + 0;	// gggg
        return new RFC4122UUID(array($part1, $part2, $part3, $part4, $part5a, $part5b, $part5c));
    }

    /*
     * equals($uuid): Compares this uuid to the specified uuid.
     *
     * The result is true if and only if the argument is not null, is a UUID object, has the same variant, and contains the same value, bit for bit, as this UUID.
     * 
     * Parameters:
     *    uuid - The uuid to be compared
     * Returns:
     *    true if the objects are the same; false otherwise
     */
    public function equals($uuid)
    {
        return $this->data == $uuid->data;
    }

    /*
     * toString(): Returns a String representing this UUID.
     *
     * Returns:
     *    A string representation of this UUID
     */
    public function toString()
    {
        return sprintf("%08x-%04x-%04x-%04x-%04x%04x%04x", $this->data[0], $this->data[1], $this->data[2], $this->data[3], $this->data[4], $this->data[5], $this->data[6]);
    }

    /*
     * variant():  The variant number associated with this UUID.
     *
     * The variant number describes the layout of the UUID.The variant number has the following meaning:
     *
     *    0 Reserved for NCS backward compatibility
     *    2 IETF RFC 4122 (Leach-Salz), used by this class
     *    6 Reserved, Microsoft Corporation backward compatibility
     *    7 Reserved for future definition 
     *
     * Returns:
     *    The variant number of this UUID
     */
    public function variant()
    {
        if (($this->data[3] & 0x8000) == 0)
            return 0;
        elseif (($this->data[3] & 0xc000) == 0x8000)
            return 2;
        elseif (($this->data[3] & 0xd000) == 0xc000)
            return 6;
        else
            return 7;
    }

    /*
     * version(): The version number associated with this UUID.
     *
     * The version number describes how this UUID was generated. The version number has the following meaning:
     *    1 Time-based UUID
     *    2 DCE security UUID
     *    3 Name-based UUID
     *    4 Randomly generated UUID 
     *
     * Returns:
     *    The version number of this UUID
     */
    public function	version()
    {
        return ($this->data[2] & 0xf000) >> 12;
    }

}

class NTTComUUID extends RFC4122UUID
{
    /*
     *                                                                                                                     *
     * fromString($ame)
     * 
     * Creates a UUID from the string standard representation as described in the toString() method.
     * 
     * Parameters:
     *     name - A string that specifies a UUID
     * Returns:
     *     A UUID with the specified value
     * Throws:
     * IllegalArgumentException - If name does not conform to the string representation as described in toString()    
     */
    public static function fromString($name)
    {
        $obj = parent::fromString($name);
        return new NTTComUUID($obj->data);
    }

    /*
     * type(): The NTT Communications' UUID type.
     *
     * The type number describes layout of NTT Communications' UUID.
     *
     * Returns:
     *    The type number of this UUID
     */
    public function type()
    {
        if ($this->data[0] == crc32(pack("nnnnnn", $this->data[1], $this->data[2], $this->data[3], $this->data[4], $this->data[5], $this->data[6])))
            return 1;;
        if ($this->data[0] == 0x20000000 | (crc32(pack("Cnnnnnn", 0x2, $this->data[1], $this->data[2], $this->data[3], $this->data[4], $this->data[5], $this->data[6])) & 0xfffffff))
            return 2;
        throw new Exception('AssertionFailure');
    }

    /*
     * systemId(): System ID encoded in UUID
     *
     * Rerturns System ID encoded in UUID.
     *
     * Returns:
     *    SystemID (>=0)
     *    negative number if it is not a NTT Communications's UUID, or any other error.
     */
    public function systemId()
    {
        switch ($this->type()) {
        case 1:
            return ($this->data[1] << 8) + (($this->data[2] & 0xff0) >> 4);
            break;
        case 2:
            return ($this->data[1] << 12) + ($this->data[2] & 0xfff);
            break;
        default:
            return -1;
            break;
        }
    }
    
    /*
     * isValid(): is this a valid NTT Communications' UUID?
     *
     * Returns:
     *    true if this is a valid NTT Communications' UUID
     *    false otherwise.
     */
    public static function isValid()
    {
        return self::$systemId >= 0;
    }


    private static $systemId = -1;
    private static $type = 1;
    
    /* setSystemId
     *
     * Register SystemID.  Call setSystemId() once at the initialization of system.
     *
     * Parameters:
     *    systgemId - ISMP system id (integer)
     * Throws:
     *    Exception('SystemIdAlreadyRegist') - SystemID has already been registerd.
     *    Exception('SystemIdTooBig') - SystemID is too big to handle.
     */
    public static function setSystemId($systemId)
    {
        if (self::$systemId != -1)
            throw new Exception('SystemIdAlreadyRegist');

        if ((self::$type == 0) && ($systemId > 0xffff))
            throw new Exception('SystemIdTooBig');

        if (((self::$type == 1) || (self::$type == 2)) && ($systemId > 0xfffffff))
            throw new Exception('SystemIdTooBig');
        
        self::$systemId = $systemId;
    }

    /* setType
     *
     * Set type of NTT Communications' UUID.  Default is type 2.
     *
     * Parameters:
     *    type - UUID type
     * Throws:
     *    Exception('UnknownNTTComUUIDType') - unknown NTT Communications' UUID type.
     */
    public static function setType($type)
    {
        if (($type != 0) && ($type != 1) && ($type != 2))
            throw new Exception('UnknownNTTComUUIDType');

        self::$type = $type;
    }

    private static function generateType0UUID()
    {
        $part2 = (self::$systemId) & 0xffff;
        $part3 = (0x4 << 12) + mt_rand(0, 0xfff);
        $part4 = mt_rand(0, 0xffff);
        $part5a = mt_rand(0, 0xffff);
        $part5b = mt_rand(0, 0xffff);
        $part5c = mt_rand(0, 0xffff);
        $part1 = crc32(pack("nnnnnn", $part2, $part3, $part4, $part5a, $part5b, $part5c));

        return array($part1, $part2, $part3, $part4, $part5a, $part5b, $part5c);
    }

    private static function generateType1UUID()
    {
        $part2 = (self::$systemId & 0xffff00) >> 8;
        $part3 = (0x4 << 12) + ((self::$systemId & 0xff) << 4) + mt_rand(0, 0xf);
        $part4 = 0x8000 | mt_rand(0, 0x3fff);
        $part5a = mt_rand(0, 0xffff);
        $part5b = mt_rand(0, 0xffff);
        $part5c = mt_rand(0, 0xffff);
        $part1 = crc32(pack("nnnnnn", $part2, $part3, $part4, $part5a, $part5b, $part5c));

        return array($part1, $part2, $part3, $part4, $part5a, $part5b, $part5c);
    }

    private static function generateType2UUID()
    {
        $part2 = ((self::$systemId) & 0xffff000) >> 12;
        $part3 = (0x4 << 12) + (self::$systemId & 0xfff);
        $part4 = 0x8000 | mt_rand(0, 0x3fff);
        $part5a = mt_rand(0, 0xffff);
        $part5b = mt_rand(0, 0xffff);
        $part5c = mt_rand(0, 0xffff);
        $part1 = 0x20000000 | (crc32(pack("Cnnnnnn", 0x2, $part2, $part3, $part4, $part5a, $part5b, $part5c)) & 0xfffffff);

        return array($part1, $part2, $part3, $part4, $part5a, $part5b, $part5c);
    }

    public static function randomUUID()
    {
        if (self::$systemId < 0)
            throw new Exception('SystemIdUnregist');
        switch (self::$type) {
        case 0:
            return new NTTComUUID(self::generateType0UUID());
            break;
        case 1:
            return new NTTComUUID(self::generateType1UUID());
            break;
        case 2:
            return new NTTComUUID(self::generateType2UUID());
            break;
        default:
            throw new Exception('AssertionFailure');
        }
    }


}
?>