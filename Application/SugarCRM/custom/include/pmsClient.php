<?php
/**
 * PMS-Client class.
 * @author Satoshi Mamada
 */
class PmsClient {

	private $baseUrl;
	private $absolutePathRoot;
	private $publicKey;
	private $secretKey;

	/**
	 * Constructor.
	 * Keeps setting information to connect PMS
	 * @param string $baseUrl  BaseURL of PMS - i.e. https://fl1-vmp24apa000.securesites.com/ossb-pms-services
	 * @param string $publicKey PublicKey to inquire to PMS
	 * @param string $secretKey SecretKey to inquire to PMS
	 */
	public function __construct($baseUrl, $absolutePathRoot, $publicKey, $secretKey) {
		$this->baseUrl          = $baseUrl;
		$this->absolutePathRoot = $absolutePathRoot;
		$this->publicKey        = $publicKey;
		$this->secretKey        = $secretKey;
	}

	/**
	 * Requests to /catalog/${catalogId}/offering
	 * @param  string $catalogId CatalogID
	 * @return string API-Response(XML-string)
	 */
	public function getOffering($catalogId)
	{
		$relativePath = '/catalog/' . $catalogId . '/offering';
		return $this->executeGetRequest($relativePath);
	}

	/**
	 * Requests to /catalog/${catalogId}/offering/${productOfferingId}/detail
	 * @param  string $catalogId CatalogID
	 * @param  string $productOfferingId ProductOfferingID
	 * @return string API-Response(XML-string)
	 */
	public function getOfferingDetail($catalogId, $productOfferingId)
	{
		$relativePath = '/catalog/' . $catalogId . '/offering/' . $productOfferingId . '/detail';
		return $this->executeGetRequest($relativePath);
	}

	/**
	 * Executes HTTP-Request(GET)
	 * @param string $relativePath  Relative-path from $baseUrl.$absolutePathRoot that given in constructor.
	 * @param string $query (*Optional)Query-String
	 * @return string HTTP-Response Body
	 */
	protected function executeGetRequest($relativePath, $query='')
	{
		$httpVerb = 'GET';
		$timestamp = date('D, d M Y H:i:s O', time());   //echo $timestamp;
		$api_key = $this->publicKey . ':' . $this->generateSignature($httpVerb, $relativePath, $timestamp);    //echo $api_key;

		// HTTP Request(Call API)
		$httpClient = curl_init($this->baseUrl .$this->absolutePathRoot . $relativePath . (strlen($query)>0 ? '?' : '') . $query);
		curl_setopt($httpClient, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($httpClient, CURLOPT_HTTPHEADER,
			array('api-key: '   . $api_key,
			      'x-mp-date: ' . $timestamp
			)
		);
		curl_setopt($httpClient, CURLOPT_SSL_VERIFYPEER, false); // Ignore SSL-Verify

		$responseContents = curl_exec($httpClient);
		curl_close($httpClient);

		return $responseContents;
	}


	/**
	 * Executes HTTP-Request(POST)
	 * @param string $relativePath  Relative-path from $baseUrl.$absolutePathRoot that given in constructor.
	 * $param string $postContents
	 * @param string $query (*Optional)Query-String
	 * @return string HTTP-Response Body
	 */
	protected function executePostRequest($relativePath, $postContents, $query='')
	{
		$httpVerb = 'POST';
		throw new Exception('This function is not implemented yet.');
	}

	/**
	 * Generates Signature String.
	 * @param string $httpVerb  HTTP Method - i.e. GET, POST ... etc.
	 * @param string $absolutePathRoot
	 * @param string $relativeUri
	 * @param string $timestamp   Format must be 'D, d M Y H:i:s O'
	 * @return string
	 */
	private function generateSignature($httpVerb, $relativeUri, $timestamp)
	{
		$stringToSign = $this->publicKey . '$' . $httpVerb . '$' . $this->absolutePathRoot . '$' . $relativeUri . '$' . $timestamp;
		return base64_encode(hash_hmac('sha1', $stringToSign, $this->secretKey, true));
	}
}