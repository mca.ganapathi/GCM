$(document).ready(function() {
    //open-all close-all btn active/inactive
    var checkBtnStatus = (function() {
        var opened = false;
        $('[id^="li-"]').each(function() {
            if ($(this).css('display') !== 'none') opened = true;
        });
        if (opened) {
            $('.btn-close-all').removeClass('primary');
            $('.btn-open-all').addClass('primary');
        } else {
            $('.btn-close-all').addClass('primary');
            $('.btn-open-all').removeClass('primary');
        }
    });
    //call this function for subpannel
    var toggleMe = (function(subPannels) {
        subPannels.each(function() {
            var _subToggle = $(this).data('toggle');
            var sub_item_id = _subToggle.replace('dropdown-', '');
            $('#' + _subToggle).show();
            $('#arrow-' + sub_item_id).removeClass('fa-chevron-down').addClass('fa-chevron-up');
        });
    });
    //expand 1st lineitem where there is only one lineitem
    if ($('[id^="li-"]').length === 1) {
        $('#li-1').show();
        $('#arrow-li-1').toggleClass('fa-chevron-down').toggleClass('fa-chevron-up');
        var subPannel = $('#li-1').parent().find('.toggle:not([data-toggle^="li-"])');
        if (subPannel.length > 1) toggleMe(subPannel);
        checkBtnStatus();
    }
    //expand/collapse single lineitem 
    $('.toggle').click(function() {
        var _toggle = $(this).data('toggle');
        var item_id = _toggle.replace('dropdown-', '');
        $('#' + _toggle).toggle();
        $('#arrow-' + item_id).toggleClass('fa-chevron-down').toggleClass('fa-chevron-up');
        var subPannel = $(this).parent().find('.toggle:not([data-toggle^="li-"])');
        if (subPannel.length > 1) toggleMe(subPannel);
        checkBtnStatus();
    });
    //expand all lineitems
    $('.btn-open-all').on('click', function() {
        $(this).parent().parent().parent().find('.toggle').each(function() {
            var _toggle = $(this).data('toggle');
            var item_id = _toggle.replace('dropdown-', '');
            $('#' + _toggle).show();
            $('#arrow-' + item_id).removeClass('fa-chevron-down').addClass('fa-chevron-up');
        });
        checkBtnStatus();
    });
    //collapse all lineitems
    $('.btn-close-all').on('click', function() {
        $(this).parent().parent().parent().find('.toggle').each(function() {
            var _toggle = $(this).data('toggle');
            var item_id = _toggle.replace('dropdown-', '');
            $('#' + _toggle).hide();
            $('#arrow-' + item_id).removeClass('fa-chevron-up').addClass('fa-chevron-down');
        });
        checkBtnStatus();
    });
});