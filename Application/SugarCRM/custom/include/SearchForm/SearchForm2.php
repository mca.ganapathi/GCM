<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
require_once ('include/SearchForm/SearchForm2.php');

/**
 * Class to extend Search Form
 */
class CustomSearchForm extends SearchForm
{

    /**
     * Method for displaying the SearchForm
     * 
     * @param boolean $header
     * @param boolean $ajaxSave
     * @return string
     */
    public function display($header = true, $ajaxSave = false)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'header : '. $header .GCM_GL_LOG_VAR_SEPARATOR. 'ajaxSave : ' . $ajaxSave, 'Method for displaying the SearchForm');
        global $theme, $timedate, $current_user;
        $header_txt = '';
        $footer_txt = '';
        $return_txt = '';
        $this->th->ss->assign('module', htmlspecialchars($this->module, ENT_QUOTES, "UTF-8"));
        $this->th->ss->assign('action', htmlspecialchars($this->action, ENT_QUOTES, "UTF-8"));
        SugarACL::listFilter($this->module, $this->fieldDefs, array(
                                                                    "owner_override" => true
        ), array(
                "use_value" => true, 
                "suffix" => '_' . $this->parsedView, 
                "add_acl" => true
        ));
        $this->th->ss->assign('displayView', $this->displayView);
        $this->th->ss->assign('APP', $GLOBALS['app_strings']);
        // Show the tabs only if there is more than one
        if ($this->nbTabs > 1) {
            $this->th->ss->assign('TABS', $this->_displayTabs($this->module . '|' . $this->displayView));
        }
        $this->th->ss->assign('searchTableColumnCount', ((isset($this->searchdefs['templateMeta']['maxColumns']) ? $this->searchdefs['templateMeta']['maxColumns'] : 2) * 2) - 1);
        $this->th->ss->assign('fields', $this->fieldDefs);
        $this->th->ss->assign('customFields', $this->customFieldDefs);
        $this->th->ss->assign('formData', $this->formData);
        $time_format = $timedate->get_user_time_format();
        $this->th->ss->assign('TIME_FORMAT', $time_format);
        $this->th->ss->assign('USER_DATEFORMAT', $timedate->get_user_date_format());
        $this->th->ss->assign('CALENDAR_FDOW', $current_user->get_first_day_of_week());
        
        $date_format = $timedate->get_cal_date_format();
        $time_separator = ":";
        if (preg_match('/\d+([^\d])\d+([^\d]*)/s', $time_format, $match)) {
            $time_separator = $match[1];
        }
        // Create Smarty variables for the Calendar picker widget
        $t23 = strpos($time_format, '23') !== false ? '%H' : '%I';
        if (!isset($match[2]) || $match[2] == '') {
            $this->th->ss->assign('CALENDAR_FORMAT', $date_format . ' ' . $t23 . $time_separator . "%M");
        } else {
            $pm = $match[2] == "pm" ? "%P" : "%p";
            $this->th->ss->assign('CALENDAR_FORMAT', $date_format . ' ' . $t23 . $time_separator . "%M" . $pm);
        }
        $this->th->ss->assign('TIME_SEPARATOR', $time_separator);
        
        // Show and hide the good tab form
        foreach ($this->tabs as $tabkey => $viewtab) {
            $viewName = str_replace(array(
                                        $this->module . '|', 
                                        '_search'
            ), '', $viewtab['key']);
            if (strpos($this->view, $viewName) !== false) {
                $this->tabs[$tabkey]['displayDiv'] = '';
                // if this is advanced tab, use form with saved search sub form built in
                if ($viewName == 'advanced') {
                    $this->tpl = 'SearchFormGenericAdvanced.tpl';
                    if ($this->action == 'ListView') {
                        $this->th->ss->assign('DISPLAY_SEARCH_HELP', true);
                    }
                    $this->th->ss->assign('DISPLAY_SAVED_SEARCH', $this->displaySavedSearch);
                    $this->th->ss->assign('SAVED_SEARCH', $this->displaySavedSearch());
                    // this determines whether the saved search subform should be rendered open or not
                    if (isset($_REQUEST['showSSDIV']) && $_REQUEST['showSSDIV'] == 'yes') {
                        $this->th->ss->assign('SHOWSSDIV', 'yes');
                        $this->th->ss->assign('DISPLAYSS', '');
                    } else {
                        $this->th->ss->assign('SHOWSSDIV', 'no');
                        $this->th->ss->assign('DISPLAYSS', 'display:none');
                    }
                }
            } else {
                $this->tabs[$tabkey]['displayDiv'] = 'display:none';
            }
        }
        
        $this->th->ss->assign('TAB_ARRAY', $this->tabs);
        
        $totalWidth = 0;
        if (isset($this->searchdefs['templateMeta']['widths']) && isset($this->searchdefs['templateMeta']['maxColumns'])) {
            $totalWidth = ($this->searchdefs['templateMeta']['widths']['label'] + $this->searchdefs['templateMeta']['widths']['field']) * $this->searchdefs['templateMeta']['maxColumns'];
            // redo the widths in case they are too big
            if ($totalWidth > 100) {
                $resize = 100 / $totalWidth;
                $this->searchdefs['templateMeta']['widths']['label'] = $this->searchdefs['templateMeta']['widths']['label'] * $resize;
                $this->searchdefs['templateMeta']['widths']['field'] = $this->searchdefs['templateMeta']['widths']['field'] * $resize;
            }
        }
        $this->th->ss->assign('templateMeta', $this->searchdefs['templateMeta']);
        $this->th->ss->assign('HAS_ADVANCED_SEARCH', !empty($this->searchdefs['layout']['advanced_search']));
        $this->th->ss->assign('displayType', $this->displayType);
        // return the form of the shown tab only
        if ($this->showSavedSearchesOptions) {
            $this->th->ss->assign('SAVED_SEARCHES_OPTIONS', $this->displaySavedSearchSelect());
        }
        if ($this->module == 'Documents') {
            $this->th->ss->assign('DOCUMENTS_MODULE', true);
        }
        
        $return_txt = $this->th->displayTemplate($this->seed->module_dir, 'SearchForm_' . $this->parsedView, $this->locateFile($this->tpl));
        
        if ($header) {
            $this->th->ss->assign('return_txt', $return_txt);
            $header_txt = $this->th->displayTemplate($this->seed->module_dir, 'SearchFormHeader', $this->locateFile('header.tpl'));
            // pass in info to render the select dropdown below the form
            $footer_txt = $this->th->displayTemplate($this->seed->module_dir, 'SearchFormFooter', $this->locateFile('footer.tpl'));
            $return_txt = $header_txt . $footer_txt;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'return_txt : '. $return_txt, '');
        return $return_txt;
    }

    /**
     * displays the tabs (top of the search form)
     * 
     * @param string $currentKey key in $this->tabs to show as the current tab
     * @return string
     */
    function _displayTabs($currentKey)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'currentKey : '. $currentKey, 'displays the tabs (top of the search form)');
        if (isset($_REQUEST['saved_search_select']) && $_REQUEST['saved_search_select'] != '_none') {
            $saved_search = BeanFactory::getBean('SavedSearch');
            $saved_search->retrieveSavedSearch($_REQUEST['saved_search_select']);
        } else {
            $saved_search = null;
        }
        
        $params = array(
                        'displayColumns', 
                        'hideTabs', 
                        'orderBy', 
                        'sortOrder'
        );
        $values = $script = array();
        
        foreach ($params as $param) {
            if (!empty($saved_search->contents[$param])) {
                $default = $saved_search->contents[$param];
            } else {
                $default = null;
            }
            $values[$param] = $this->request->getValidInputRequest($param, null, $default);
        }
        
        $values = array_filter($values);
        if (count($values) == 0) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
            return '';
        }
        
        foreach ($values as $param => $value) {
            $script[] = 'SUGAR.savedViews.' . $param . ' = ' . json_encode($value) . ';';
        }
        
        if (!empty($_REQUEST['orderBy']))
            $script[] = 'SUGAR.savedViews.selectedOrderBy = ' . json_encode($_REQUEST['orderBy']) . ';';
        elseif (isset($saved_search->contents['orderBy']) && !empty($saved_search->contents['orderBy']))
            $script[] = 'SUGAR.savedViews.selectedOrderBy = ' . json_encode($saved_search->contents['orderBy']) . ';';
        if (!empty($_REQUEST['sortOrder']))
            $script[] = 'SUGAR.savedViews.selectedSortOrder = ' . json_encode($_REQUEST['sortOrder']) . ';';
        elseif (isset($saved_search->contents['sortOrder']) && !empty($saved_search->contents['sortOrder']))
            $script[] = 'SUGAR.savedViews.selectedSortOrder = ' . json_encode($saved_search->contents['sortOrder']) . ';';
        
        $str = '<script>' . implode("\n", $script) . '</script>';
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'str : '. $str, '');
        return $str;
    }
}
