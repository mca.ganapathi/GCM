<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
require_once ('include/SearchForm/SearchForm.php');

/**
 * Class to extend Search Form
 */
class CustomSearchForm extends SearchForm
{

    /**
     * displays the search form header
     * 
     * @param string $view which view is currently being displayed
     */
    function displayHeader($view)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'view : '.$view, 'displays the search form header');
        global $current_user;
        $GLOBALS['log']->debug('SearchForm.php->displayHeader()');
        $header_text = '';
        $module = $this->request->getValidInputRequest('module', 'Assert\Mvc\ModuleName');
        $action = $this->request->getValidInputRequest('action');
        if (is_admin($current_user) && $module != 'DynamicLayout' && !empty($_SESSION['editinplace'])) {
            $header_text = "<a href='index.php?action=index&module=DynamicLayout&from_action=SearchForm&from_module=" . htmlspecialchars($module, ENT_QUOTES, 'UTF-8') . "'>" . SugarThemeRegistry::current()->getImage("EditLayout", "border='0' align='bottom'", null, null, '.gif', 'Edit Layout') . "</a>";
        }
        
        echo $header_text . $this->displayTabs($this->module . '|' . $view);
        echo "<form name='search_form' class='search_form'>" . "<input type='hidden' name='searchFormTab' value='{$view}'/>" . "<input type='hidden' name='module' value='" . htmlspecialchars($module, ENT_QUOTES, 'UTF-8') . "'/>" . "<input type='hidden' name='action' value='" . htmlspecialchars($action, ENT_QUOTES, 'UTF-8') . "'/>" . "<input type='hidden' name='query' value='true'/>";
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * displays the tabs (top of the search form)
     * 
     * @param string $currentKey key in $this->tabs to show as the current tab
     * @return string
     */
    function displayTabs($currentKey)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'currentKey : '.$currentKey, 'displays the tabs (top of the search form)');
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'tabs : '.print_r($this->tabs, true), 'displays the tabs (top of the search form)');
        
        $tabPanel = new SugarWidgetTabs($this->tabs, $currentKey, 'SUGAR.searchForm.searchFormSelect');
        
        if (isset($_REQUEST['saved_search_select']) && $_REQUEST['saved_search_select'] != '_none') {
            $saved_search = BeanFactory::getBean('SavedSearch');
            $saved_search->retrieveSavedSearch($_REQUEST['saved_search_select']);
        }
        
        $str = $tabPanel->display();
        $params = array();
        foreach (array(
                    'displayColumns', 
                    'hideTabs', 
                    'orderBy', 
                    'sortOrder'
        ) as $param) {
            $value = $this->request->getValidInputRequest($param);
            if (!empty($value)) {
                $params[$param] = $value;
            } elseif (!empty($saved_search->contents[$param])) {
                $params[$param] = $saved_search->contents[$param];
            }
        }
        
        if (!empty($_REQUEST['orderBy']))
            $params['selectedOrderBy'] = $_REQUEST['orderBy'];
        elseif (isset($saved_search->contents['orderBy']) && !empty($saved_search->contents['orderBy']))
            $params['selectedOrderBy'] = $saved_search->contents['orderBy'];
        if (!empty($_REQUEST['sortOrder']))
            $params['selectedSortOrder'] = $_REQUEST['sortOrder'];
        elseif (isset($saved_search->contents['sortOrder']) && !empty($saved_search->contents['sortOrder']))
            $params['selectedSortOrder'] = $saved_search->contents['sortOrder'];
        
        $str .= '<script>$.extend(SUGAR.savedViews, ' . json_encode($params) . ');</script>';
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'str : ' . $str, '');
        return $str;
    }
}
