{*
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

*}
{assign var="displayParameters" value=$fields.$fieldname}
<select name="{$displayParameters.name}" id="{$displayParameters.name}" {$action_event}>
	<option value=""></option>
	{foreach from=$fieldlist key=k item=v}
		<option value="{$k}" {if ($displayParameters.value) eq $k } selected {/if} {if $v.code} data="{$v.code}"{/if} {if $v.label} label="{$v.label}"{/if}>{if $v.name} {$v.name} {else} $v {/if}</option>
	{/foreach}
</select>