{*
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/
*}				
<input type="text" autocomplete="off" title="" value="{$c_name}" size="30" id="{$c_fld[1]}" tabindex="0" class="sqsEnabled yui-ac-input {if !isset($c_fld[4])}req{/if}" name="{$c_fld[1]}" readonly="true" data ="relate"/>
<div id="EditView_account_name_results" class="yui-ac-container">
    <div class="yui-ac-content" style="display: none;">
        <div class="yui-ac-hd" style="display: none;"></div>
        <div class="yui-ac-bd">
            <ul>
                <li style="display: none;"></li>
                <li style="display: none;"></li>
                <li style="display: none;"></li>
                <li style="display: none;"></li>
                <li style="display: none;"></li>
                <li style="display: none;"></li>
                <li style="display: none;"></li>
                <li style="display: none;"></li>
                <li style="display: none;"></li>
                <li style="display: none;"></li>
            </ul>
        </div>
        <div class="yui-ac-ft" style="display: none;"></div>
    </div>
</div>
<input type="hidden" value="{$c_id}" id="{$c_fld[0]}" name="{$c_fld[0]}" class="idhidden"/>
<span class="id-ff multiple"> 
  <button onclick='if(checkParent(this,"{$c_valid}","{$c_parent}")) 
            open_popup("{$c_fld[2]}", 600, 400, "{$c_filter}", true, false, 
     {ldelim}"call_back_function":"{$c_return}","form_name":"EditView","field_to_name_array":{ldelim}"id":"{$c_fld[0]}","name":"{$c_fld[1]}", "c_country_id_c":"con_country_id"{rdelim}{rdelim}, 
        "single", true);' 
        value="Select {$c_fld[3]}" class="button firstChild" title="Select {$c_fld[3]}" tabindex="0" id="btn_{$c_fld[1]}" name="btn_{$c_fld[1]}" type="button">
		<img src="themes/default/images/id-ff-select.png" />
	</button>
	<button value="Clear {$c_fld[3]}" onclick="SUGAR.clearRelateField(this.form, '{$c_fld[1]}', '{$c_fld[0]}'); clear_custom_popup(this.id);" class="button lastChild" title="Clear {$c_fld[3]}" tabindex="0" id="btn_clr_{$c_fld[1]}" name="btn_clr_{$c_fld[1]}" type="button">
		<img src="themes/default/images/id-ff-clear.png" />
	</button>
</span>