{*
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

*}
<table border="0" cellpadding="0" cellspacing="0" class="dateTime ffixed">
	<tbody>
		<tr valign="top">
			<td nowrap="">
                <input class="date_input {$c_calss}" autocomplete="off" type="text" name="{$c_fld}{$c_app}" id="{$c_fld}{$c_app}" value="" title="" tabindex="0" size="8" maxlength="10" data="date" dependentField = "{$c_dependentfield}">
                <img src="themes/Sugar/images/jscalendar.png" alt="Enter Date" border="0" id="{$c_fld}_trigger{$c_app}">&nbsp;
            </td>
			<td nowrap="">
                <div id="date_start_time_section">
					<span>
						<select class="datetimecombo_hr" size="1" id="{$c_fld}_hours{$c_app}" name="{$c_fld}_hours{$c_app}" tabindex="0">
							{section name=i start=0 loop=24 step=1}
								<option value='{"%02d"|sprintf:$smarty.section.i.index}'>{"%02d"|sprintf:$smarty.section.i.index}</option>
							{/section}
						</select>&nbsp;:&nbsp;
						<select class="datetimecombo_min" size="1" id="{$c_fld}_minutes{$c_app}" name = "{$c_fld}_minutes{$c_app}" tabindex="0">
							{section name=i start=0 loop=60 step=1}
								<option value='{"%02d"|sprintf:$smarty.section.i.index}'>{"%02d"|sprintf:$smarty.section.i.index}</option>
							{/section}
						</select>&nbsp;:&nbsp;
						<select class="datetimecombo_sec" size="1" id="{$c_fld}_seconds{$c_app}" name = "{$c_fld}_seconds{$c_app}" tabindex="0">
							{section name=i start=0 loop=60 step=1}
								<option value='{"%02d"|sprintf:$smarty.section.i.index}'>{"%02d"|sprintf:$smarty.section.i.index}</option>
							{/section}
						</select>
					</span>
				</div>
            </td>
		</tr>
	</tbody>
</table>
<script type="text/javascript">
Calendar.setup ({ldelim}
inputField : "{$c_fld}{$c_app}",
form : "{$c_view}",
ifFormat : "{$CALENDAR_FORMAT}",
daFormat : "{$CALENDAR_FORMAT}",
button : "{$c_fld}_trigger{$c_app}",
singleClick : true,
dateStr : "",
startWeekday: {$CALENDAR_FDOW|default:'0'},
step : 1,
weekNumbers:false
{rdelim}
);
</script>
