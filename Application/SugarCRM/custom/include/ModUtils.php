<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * ModUtils Class
 */
class ModUtils
{

    public $db;

    public function __construct()
    {
    }

    /**
     * Method to return the class object
     *
     * @param string $module name of the module
     * @param string $classname
     * @return object ModUtils Class object / ModUtils Child Class object
     * @author shrikant.gaware
     * @since Sep 30, 2015
     */
    static function getInstance($module, $classname)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'module : ' . $module . GCM_GL_LOG_VAR_SEPARATOR . 'classname : ' . $classname, 'Method to return the class object');
        $instance = NULL;
        if ($module != '' && $classname != '') {
            if (file_exists("custom/modules/$module/include/$classname.php")) {
                include_once ("custom/modules/$module/include/$classname.php");
                $instance = new $classname();
            }
        } else {
            $instance = new self();
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'instance : ' . print_r($instance, true), '');
        return $instance;
    }

    /**
     * Method to initialize class variables and invoke respective method
     *
     * @author shrikant.gaware
     * @since Sep 30, 2015
     */
    public function process()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to initialize class variables and invoke respective method');
        $method = $_REQUEST['method'];
        if (method_exists($this, $method))
            $this->$method();
        else
            $this->outputData(array('error' => 'Invalid Method'));
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * Method to output JSON Encoded data
     *
     * @param array $data need to be encoded
     * @author shrikant.gaware
     * @since Sep 30, 2015
     */
    public function outputData($data)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'data : ' . print_r($data, true), 'Method to output JSON Encoded data');
        $data = ModUtils::object_to_array($data);
        $data = (!empty($data)) ? $data : '';
        echo json_encode($data);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'data : ' . print_r($data, true), '');
        exit();
    }

    /**
     * method to search path and value from an array
     *
     * @author sagar.salunkhe
     * @param array $input_array array in which to search key or value
     * @param string $type option can be 'key' or 'value'
     * @param string $search_key search value
     * @return array method will return array path and value
     * @since Jan 06, 2016
     */
    public function searchArrayElement($input_array, $type, $search_key)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'input_array : ' . print_r($input_array, true), 'method to search path and value from an array');
        // create a recursive iterator to loop over the array recursively
        $iter = new RecursiveIteratorIterator(new RecursiveArrayIterator($input_array), RecursiveIteratorIterator::SELF_FIRST);
        // loop over the iterator
        foreach ($iter as $key => $value) {
            // if the value matches our search
            if ($$type === $search_key) {
                // add the current key
                $keys = array($key);
                // loop up the recursive chain
                for ($i = $iter->getDepth() - 1; $i >= 0; $i--) {
                    // add each parent key
                    array_unshift($keys, $iter->getSubIterator($i)->key());
                }
                // return our output array
                $return = array('path' => $keys, 'value' => $value);
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return : ' . print_r($return, true), '');
                return $return;
            }
        }
        // return false if not found
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return false;
    }

    /**
     * method to unset array node from given array
     *
     * @author sagar.salunkhe
     * @param array $path path in sequence
     * @param array &$arr source array
     * @return array method will return array path and value
     * @since Jan 06, 2016
     */
    public function unsetArrayNode($path, &$arr)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'path : ' . print_r($path, true) . GCM_GL_LOG_VAR_SEPARATOR . 'arr : ' . print_r($arr, true), 'method to unset array node from given array');
        if (count($path) > 1) {
            return self::unsetArrayNode(array_slice($path, 1), $arr[$path[0]]);
        }
        unset($arr[$path[0]]);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr : ' . print_r($arr, true), '');
    }

    /**
     * get team members from team name
     *
     * @param string $team_name
     * @param string $only_active_users
     * @param number $explicit_flag
     * @return array $user_list
     * @author sagar.salunkhe
     * @since Jun 08, 2016
     */
    public function getTeamMembers($team_id, $only_active_users, $explicit_flag)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'team_id : ' . $team_id . GCM_GL_LOG_VAR_SEPARATOR . 'only_active_users : ' . $only_active_users . GCM_GL_LOG_VAR_SEPARATOR . 'explicit_flag : ' . $explicit_flag, 'get team members from team name');
        // Get the list of members
        $member = new TeamMembership();
        if ($explicit_flag == 1)
            $member_list = $member->get_full_list("", "team_id='$team_id' and explicit_assign=1");
        else
            $member_list = $member->get_full_list("", "team_id='$team_id'");

        $user_list = array();
        // make sure we do not try and iterate over an empty list.
        if ($member_list == null) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'user_list . ' . print_r($user_list, true), '');
            return $user_list;
        }

        // create user object and call retrieve with the user_id
        foreach ($member_list as $current_member) {
            $user = new User();
            $user->retrieve($current_member->user_id);

            // if the flag $only_active_users is set to true, then we only want to return
            // active users. This was defined as part of workflow to not send out notifications
            // to inactive users
            if ($only_active_users) {
                if ($user->status == 'Active')
                    $user_list[] = $user;
            } else
                $user_list[] = $user;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'user_list . ' . print_r($user_list, true), '');
        return $user_list;
    }

    /**
     * retrieve team id from team name
     *
     * @param string $team_name
     * @return string uuid of team module
     * @author sagar.salunkhe
     * @since Jun 08, 2016
     */
    public function retrieveTeamId($team_name)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'team_name : ' . print_r($team_name, true), 'retrieve team id from team name');
        global $db;
        $query = "SELECT id FROM teams WHERE (name='" . $db->quote($team_name) . "' OR name_2 = '" . $db->quote($team_name) . "') ";
        // Private teams seem to have the name split up, so we need to check for this
        $split_val = explode(' ', $team_name, 2);
        if (isset($split_val[1]) && !empty($split_val[1])) {
            $query .= " OR ( name = '" . $db->quote($split_val[0]) . "' AND name_2 = '" . $db->quote($split_val[1]) . "' ) ";
        }
        $query .= "AND deleted = 0";

        $result = $db->query($query, false, "Error retrieving user ID: ");
        $row = $db->fetchByAssoc($result);
        if (!$row) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
            return false;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'row id : ' . print_r($row['id'], true), '');
        return $row['id'];
    }

    /**
     * log entry in sugarcrm application log file
     *
     * @param string $message_code type of log message
     * @param string $log_level sugarcrm log level
     * @param string $file_path name and path of the file
     * @param string $method_name name of the method
     * @param string $message message that need to be logged
     * @param string $description description that need to be logged
     * @author sagar.salunkhe
     * @since Jun 08, 2016
     */
    public static function buildServiceLogMessage($message_code, $log_level, $file_path, $method_name, $message, $description)
    {
        $log_text = '';
        switch ($message_code) {
            case 'begin':
                $log_text = "BEGIN: " . $file_path . ' |~| ' . $method_name . " |~| " . $message;
                break;
            case 'trace':
                $log_text = "TRACER: " . $file_path . ' |~| ' . $method_name . " |~| " . $message;
                break;
            case 'end':
                $log_text = "END: " . $file_path . ' |~| ' . $method_name . " |~| " . $message;
                break;
            default:
                $log_text = $file_path . ' |~| ' . $method_name . " |~| " . $message;
                break;
        }
        if ($description != '') {
            $log_text = $log_text . " |~| " . $description;
        }
        if (!empty($log_text))
            $GLOBALS['log']->$log_level($log_text);
    }

    /**
     * build response parameter for service files
     *
     * @param string $api_ver
     * @param int $response_code tiny int service response code. 1 - success | 0 - failure
     * @param string $service_response_code global service response code
     * @param array $additional_params additional params to be passed in response
     * @param string $dynamic_msg
     * @return array return response in array format
     * @author sagar.salunkhe
     * @since Jun 04, 2016
     */
    public function buildServiceResponse($api_ver, $response_code, $service_response_code, $additional_params, $dynamic_msg)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'api_ver : ' . $api_ver . GCM_GL_LOG_VAR_SEPARATOR . 'response_code ' . $response_code . GCM_GL_LOG_VAR_SEPARATOR . 'service_response_code : ' . $service_response_code . GCM_GL_LOG_VAR_SEPARATOR . 'additional_params : ' . print_r($additional_params, true) . GCM_GL_LOG_VAR_SEPARATOR . 'dynamic_msg : ' . $dynamic_msg, 'build response parameter for service files');
        require_once ('custom/service/v4_1_custom/APIHelper/ClassServiceHelper.php');
        $ClassServiceHelper = new ClassServiceHelper();
        $response_text = $ClassServiceHelper->getServiceCodeDetails($api_ver, $service_response_code, $dynamic_msg);
        $response_text = !empty($response_text) ? $response_text : "";
        $res_param = array('ResponseCode' => (($response_code == 1) ? $response_code == (string) '1' : (string) '0'), 'ServiceResponseCode' => $service_response_code, 'ResponseMsg' => (empty($response_text) ? '(empty)' : $response_text));

        if (is_array($additional_params)) {
            foreach ($additional_params as $key => $val)
                $res_param[$key] = $val;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'res_param : ' . print_r($res_param, true), '');
        return $res_param;
    }

    /**
     * method to prepare an array for processing api post response
     *
     * @param array $request_array data array
     * @param array $response_array response array
     * @param object $bean module bean object
     * @author sagar.salunkhe
     * @since Jun 17, 2016
     */
    public function generatePostXmlResponseData($request_array, &$response_array, $bean)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'request_array : ' . print_r($request_array, true) . GCM_GL_LOG_VAR_SEPARATOR . 'response_array : ' . print_r($response_array, true), '');
        $field_from_xml_suffix = '_xml_id';
        $field_uuid_suffix = '_uuid';
        foreach ($request_array as $key => $val) {
            $uuid_field_name = str_replace($field_from_xml_suffix, $field_uuid_suffix, $key);
            if ((substr($key, -(strlen($field_from_xml_suffix))) === $field_from_xml_suffix) && !empty($uuid_field_name))
                $response_array[$val] = $bean->{$uuid_field_name};
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * method to validate email address
     *
     * @param array $emailStr data string
     * @return boolean true|false
     * @author rajul.mondal
     * @since Aug 02, 2016
     */
    public function email_validation($emailStr)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'emailStr : ' . print_r($emailStr, true), 'method to validate email address');
        global $sugar_config;
        $emailStr = trim($emailStr);
        // chech length & null
        if ($emailStr === null || strlen($emailStr) == 0 || strlen($emailStr) < $sugar_config['gcm_cstm_const']['email_max_length']) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'false');
            return false;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'true');
        return true;
    }

    /**
     * method to sort two arrays
     *
     * @param array $array1
     * @param array $array2
     * @return array sorted array
     * @author rajul.mondal
     * @since Mar 27, 2018
     */
    public function price_block_desc_sort($array1, $array2)
    {
        if ($array1['name'] == $array2['name']) return 0;
        return ($array1['name'] < $array2['name']) ? 1 : -1;
    }

    /**
     * method to compare two arrays and through the changes in the left side of array
     *
     * @param array $array1
     * @param array $array2
     * @param string $line_item_key line item array index
     * @return array changes in the left side array
     * @author mohamed.siddiq
     * @since Aug 04, 2016
     */
    public function array_diff_assoc_recursive($array1, $array2, $line_item_key = '')
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array1 : ' . print_r($array1, true) . GCM_GL_LOG_VAR_SEPARATOR . 'array2 : ' . print_r($array2, true), 'method to compare two arrays and through the changes in the left side of array');
        $difference = array();
        if($line_item_key === 'ContractLineProductPrice') {
            usort($array1, ['ModUtils', 'price_block_desc_sort']);
            usort($array2, ['ModUtils', 'price_block_desc_sort']);
        }
        foreach ($array1 as $key => $value) {
            if (is_array($value)) {
                if (!isset($array2[$key]) || !is_array($array2[$key])) {
                    $difference[$key] = $value;
                } else {
                    $new_diff = $this->array_diff_assoc_recursive($value, $array2[$key], $key);
                    if (!empty($new_diff))
                        $difference[$key] = $new_diff;
                }
            } else if (!array_key_exists($key, $array2)) {
                $difference[$key] = $value;
            } else {
                if (is_numeric($array2[$key])) {
                    if (floatval($array2[$key]) != floatval($value)) {
                        $difference[$key] = $value;
                    }
                } else {
                    if ($array2[$key] != $value) {
                        $difference[$key] = $value;
                    }
                }
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'difference : ' . print_r($difference, true), '');
        return $difference;
    }

    /**
     * method to compare two arrays and through the changes in the left side of array
     *
     * @param array $array1
     * @param array $array2
     * @return array changes in the left side array
     * @author sathishkumar.ganesan
     * @since May 11, 2017
     */
    public function array_diff_multi($array1, $array2)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array1 : ' . print_r($array1, true) . GCM_GL_LOG_VAR_SEPARATOR . 'array2 : ' . print_r($array2, true), 'method to compare two arrays and through the changes in the left side of array');
        $difference = array();
        foreach ($array1 as $key => $value) {
            if (is_array($value)) {
                if (!isset($array2[$key]) || !is_array($array2[$key])) {
                    $difference[$key] = $value;
                } else {
                    $new_diff = $this->array_diff_multi($value, $array2[$key]);
                    if (!empty($new_diff))
                        $difference[$key] = $new_diff;
                }
            }else if(!in_array($value,$array2)){
                $difference[$key] = $value;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'difference : ' . print_r($difference, true), '');
        return $difference;
    }

    /**
     * function to check if string is in uuid format
     *
     * @param string $string uuid string
     * @return boolean true|false
     * @author sagar.salunkhe
     * @since Aug 09, 2016
     */
    public function validateUUID($string)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'string : ' . $string, 'function to check if string is in uuid format');
        if (empty($string)) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
            return false;
        }

        if (preg_match('/^\{?[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}\}?$/', $string)) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
            return true;
        } else {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
            return false;
        }
    }

    /**
     * copy bean
     *
     * @param string $module_name
     * @param string $record_id parent record id
     * @param array $additional_dtls
     * @return object bean
     * @author sagar.salunkhe
     * @since Aug 17, 2016
     */
    public function duplicateBeanRecord($module_name, $record_id, $additional_dtls)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'module_name : ' . $module_name . GCM_GL_LOG_VAR_SEPARATOR . 'record_id : ' . $record_id . GCM_GL_LOG_VAR_SEPARATOR . 'additional_dtls : ' . print_r($additional_dtls, true), 'copy bean');
        $source_bean = BeanFactory::getBean($module_name);
        $source_bean->retrieve($record_id);

        $new_bean = BeanFactory::getBean($module_name);

        if (empty($record_id) || (empty($source_bean->id) && isset($source_bean->id))) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
            return $new_bean;
        }

        $module_field_defs = $source_bean->getFieldDefinitions();

        if (!empty($module_field_defs)) {
            foreach ($module_field_defs as $field_name => $field_def)
                $new_bean->$field_name = $source_bean->$field_name;
        }

        if (!empty($additional_dtls) && is_array($additional_dtls)) {
            // save additional data
            foreach ($additional_dtls as $field_name => $field_val)
                $new_bean->$field_name = $field_val;
        }

        $unset_fields = array('id', 'date_entered', 'created_by', 'modified_user_id');

        foreach ($unset_fields as $u_field)
            unset($new_bean->$u_field);

        $new_bean->save();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return $new_bean;
    }

    /**
     * get key from dropdown display member
     *
     * @param string $display_text display value of drodpdown
     * @param string $list_name dropdown list name of app string list
     * @author sagar.salunkhe
     * @since Aug 24, 2016
     */
    public function getAppListStringValueMember($display_text, $list_name)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'display_text : ' . $display_text . GCM_GL_LOG_VAR_SEPARATOR . 'list_name : ' . $list_name, 'get key from dropdown display member');
        global $app_list_strings;

        if (array_key_exists($list_name, $app_list_strings)) {
            $key = array_search($display_text, $app_list_strings[$list_name]);
            if ($key !== false) {
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'key : ' . $key, '');
                return $key;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return '';
    }

    /**
     * set default timezone if the timezone is empty
     *
     * @param string $timezone
     * @return string $timezone
     * @author mohamed.siddiq
     * @since Sep 09, 2016
     */
    public function checkAndSetDefaultTimezone($timezone)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'timezone : ' . $timezone, 'set default timezone if the timezone is empty');
        global $sugar_config;
        if (trim($timezone) == str_replace('+', '', $sugar_config['gcm_cstm_const']['timezone']['gmt']) || trim($timezone) == '') {
            $timezone = $sugar_config['gcm_cstm_const']['timezone']['gmt'];
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'timezone : ' . $timezone, '');
        return $timezone;
    }

    /**
     * function to check if string is in yyyy-mm-dd format
     *
     * @param string $date date string
     * @return boolean true|false
     * @author mohamed.siddiq
     * @since Nov 10, 2016
     */
    public function validateDateFormat($date)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'date : ' . $date, 'function to check if string is in yyyy-mm-dd format');
        if (empty($date)) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
            return false;
        }

        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $date)) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
            return true;
        } else {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
            return false;
        }
    }

    /**
     * convert object to array
     *
     * @param object $obj
     * @return object
     * @author rajul.mondal
     * @since Oct 04, 2016
     */
    public static function object_to_array($obj)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'obj : ' . print_r($obj, true), 'convert object to array');
        $new = array();
        if (is_object($obj)) {
            $obj = (array) $obj;
        }

        if (is_array($obj)) {
            foreach ($obj as $key => $val) {
                $new[$key] = ModUtils::object_to_array($val);
            }
        } else {
            $new = $obj;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'new : ' . print_r($new, true), '');
        return $new;
    }

    /**
     * function to check if string is a valid date
     *
     * @param string $date date string
     * @return boolean true|false
     * @author mohamed.siddiq
     * @since Nov 10, 2016
     */
    public function validateDate($date)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'date : ' . $date, 'function to check if string is a valid date');
        $validate_format = $this->validateDateFormat($date);
        if ($validate_format && is_bool($validate_format)) {
            list($y, $m, $d) = explode('-', $date);
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'date : ' . $date, '');
            return checkdate($m, $d, $y);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return false;
    }

    /**
     * method to generate csv file contents from an array
     *
     * @param array &$fields array to be converted
     * @param string $delimiter delimeter for creating csv
     * @param string $enclosure enclosure of column
     * @param boolean $encloseAll true|false
     * @param boolean $nullToMysqlNull true|false
     * @return string
     * @author mohamed.siddiq
     * @since Dec 04, 2016
     */
    public static function processArrayToCsv(array &$fields, $delimiter = ',', $enclosure = '"', $encloseAll = true, $nullToMysqlNull = false)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'fields : ' . print_r($fields, true) . GCM_GL_LOG_VAR_SEPARATOR . 'delimiter : ' . $delimiter . GCM_GL_LOG_VAR_SEPARATOR . 'enclosure : ' . $enclosure . GCM_GL_LOG_VAR_SEPARATOR . 'encloseAll : ' . $encloseAll, 'method to generate csv file contents from an array');
        array_walk_recursive($fields, "self::convertHtmlCharsToText");

        $delimiter_esc = preg_quote($delimiter, '/');
        $enclosure_esc = preg_quote($enclosure, '/');

        $output = array();
        $final_output = array();
        foreach ($fields as $key => $columns) {
            foreach ($columns as $key1 => $field) {
                if ($field === null && $nullToMysqlNull) {
                    $output[$key][] = 'NULL';
                    continue;
                }
                // Enclose fields containing $delimiter, $enclosure or whitespace
                if ($encloseAll || preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field)) {
                    $output[$key][] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
                } else {
                    $output[$key][] = $field;
                }
            }
            $final_output[] = implode($delimiter, $output[$key]); // implode each field with delimiter
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'final_output : ' . print_r($final_output, true), '');
        return implode(PHP_EOL, $final_output) . PHP_EOL;
    }

    /**
     * convert html characters to text
     *
     * @param string $value
     * @author sagar.salunkhe
     * @since Nov 25, 2016
     */
    public static function convertHtmlCharsToText(&$value)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'value : ' . $value, 'convert html characters to text');
        $value = html_entity_decode($value, ENT_QUOTES, 'UTF-8');
        $value = htmlspecialchars_decode($value, ENT_QUOTES);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'value : ' . $value, '');
    }

    public static function saveAuditRecordEntry(&$bean, $field_name, $data_type, $before, $after)
    {
        $audit_entry = array();
        $audit_entry['field_name'] = $field_name;
        $audit_entry['data_type'] = $data_type;
        $audit_entry['before'] = $before;
        $audit_entry['after'] = $after;
        $bean->db->save_audit_records($bean, $audit_entry);
    }

    /**
     * method to convert datetime with timezone into UTC datetime
     *
     * @param string $date (yyyy-mm-ddTH:i:s+00:00)
     * @return array
     * @author rajul.mondal
     * @since Nov 25, 2016
     */
    public function ConvertDatetoGMTDate($date)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'date : ' . $date, 'method to convert datetime with timezone into UTC datetime');
        global $sugar_config;
        $ret_arr = array('result' => false, 'error_type' => 1, 'date' => null);
        if ($date == null || $date == '') {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'ret_arr : ' . print_r($ret_arr, true), '');
            $ret_arr['error_type'] = 0;
            return $ret_arr;
        }
        $date = str_ireplace("Z", $sugar_config['gcm_cstm_const']['timezone']['gmt'], $date);
        $date_regx = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])T([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/";
        $date_regx_plus = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])T([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])[\+](0[0-9]|1[0-4]):(0[0]|3[0]|4[0-5])$/";
        $date_regx_minus = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])T([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])[\-](0[0-9]|1[0-2]):(0[0]|3[0]|4[0-5])$/";
        // if not satisfying both the condition then return false
        if (!preg_match($date_regx, $date) && !preg_match($date_regx_plus, $date) && !preg_match($date_regx_minus, $date)) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'ret_arr : ' . print_r($ret_arr, true), '');
            $ret_arr['error_type'] = 1;
            return $ret_arr;
        } else {
            $reqTimezone = substr($date, -6);
            if (preg_match('/(\+|\-|z|Z)/', $reqTimezone)) {
                $timezones = array();
                $timezone_bean = $obj_timezone = BeanFactory::getBean('gc_TimeZone');
                $timezone_list = $timezone_bean->get_full_list('', "c_country_id_c = ''");
                if ($timezone_list != null) {
                    foreach ($timezone_list as $timezone) {
                        $timezones[] = $timezone->utcoffset;
                    }
                }
                if (!in_array($reqTimezone, $timezones)) {
                    $ret_arr['error_type'] = 2;
                    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'ret_arr : ' . print_r($ret_arr, true), '');
                    return $ret_arr;
                }
            }
        }
        $date = new DateTime($date);
        $date->setTimezone(new DateTimeZone('GMT'));
        $ret_arr = array('result' => true, 'date' => $date->format('Y-m-d H:i:s'));
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'ret_arr : ' . print_r($ret_arr, true), '');
        return $ret_arr;
    }

    /**
     * method to convert UTC datetime into local datetime with timezone
     *
     * @param string $datetime (yyyy-mm-dd H:i:s)
     * @param string $timezone (+00:00)
     * @return array
     * @author rajul.mondal
     * @since Mar 17, 2017
     */
    public function ConvertGMTDateToLocalDate($datetime, $timezone)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'datetime : ' . $datetime . 'timezone : ' . $timezone, 'method to convert UTC datetime into local datetime with ');
        global $sugar_config;
        // default response for the method
        $response = array('result' => false, 'error_type' => 1, 'datetime' => null);
        // datetime empty validation
        if ($datetime == null || $datetime == '') {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response : ' . print_r($response, true), '');
            return $response;
        }
        // if timezone is null or empty set default value
        if ($timezone == null || $timezone == '') {
            $timezone = $sugar_config['gcm_cstm_const']['timezone']['gmt'];
        }
        // expression for datetime
        $datetime_regx = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])T([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/";
        // expression for +ve timezone
        $timezone_regx_plus = "/^[\+](0[0-9]|1[0-4]):(0[0]|3[0]|4[0-5])$/";
        // expression for -ve timezone
        $timezone_regx_minus = "/^[\-](0[0-9]|1[0-2]):(0[0]|3[0]|4[0-5])$/";

        // if not satisfying both the condition then return false
        if (!preg_match($datetime_regx, $datetime) && !preg_match($timezone_regx_plus, $timezone) && !preg_match($timezone_regx_minus, $timezone)) {
            $response['error_type'] = 2;
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response : ' . print_r($response, true), '');
            return $response;
        } else {
            // check timezone is avilable in db if timezone != +00:00
            if (!preg_match('/^[\+\-](0[0]):(0[0])$/', $timezone)) {
                $timezones = array();
                $timezone_bean = $obj_timezone = BeanFactory::getBean('gc_TimeZone');
                $timezone_list = $timezone_bean->get_full_list('', "c_country_id_c = ''");
                if ($timezone_list != null) {
                    foreach ($timezone_list as $db_timezone) {
                        $timezones[] = $db_timezone->utcoffset;
                    }
                }
                if (!in_array($timezone, $timezones)) {
                    $response['error_type'] = 3;
                    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response : ' . print_r($response, true), '');
                    return $response;
                }
            }
        }
        // convertion of UTC datetime into given timezone
        $datetime = new DateTime($datetime, new DateTimeZone('UTC'));
        $datetime->setTimezone(new DateTimeZone($timezone));
        $response = array('result' => true, 'datetime' => $datetime->format('Y-m-d H:i:s'));
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response : ' . print_r($response, true), '');
        return $response;
    }

    /**
     * method to validate the PMS ID pattern
     *
     * @param string $pmsid (Ex:00000000-0000-0000-0000-000000000946)
     * @return boolean true|false
     * @author mohamed.siddiq
     * @since Nov 29, 2016
     */
    public function validatePMSIdPattern($pmsid)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'pmsid : ' . $pmsid, 'method to validate the PMS ID pattern');
        if ($pmsid != '') {
            $date_regx = "/^" . "[0-9]{8}" . "-" . "[0-9]{4}" . "-" . "[0-9]{4}" . "-" . "[0-9]{4}" . "-" . "[0-9]{12}" . "$/";
            if (!preg_match($date_regx, $pmsid)) {
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
                return false;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return true;
    }

    /**
     * Method to date as user format
     *
     * @param string $dt db date time format
     * @return string userdate
     * @author Kamal.Thiyagarajan
     * @since Aug 1, 2016
     */
    public function getUserDate($dt)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'dt : ' . $dt, 'Method to date as user format');
        $userdate = '';
        if ($dt == '') {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'userdate : ' . $userdate, '');
            return $userdate;
        }
        $dt_arr = explode(' ', $dt);
        $dateFormat = $GLOBALS['current_user']->getPreference('datef');
        $newDate = date($dateFormat, strtotime($dt_arr[0]));
        $userdate = $newDate . ' ' . $dt_arr[1];
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'userdate : ' . $userdate, '');
        return $userdate;
    }

    /**
     * Method to date as user format
     *
     * @param string $dt db date time format
     * @return string
     * @author Shrikant.Gaware
     * @since 31-Dec-2015
     */
    public function getDbDate($dt)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'dt : ' . $dt, 'Method to date as user format');

        global $current_user;

        $dbdate = '';
        if ($dt == '') {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'dbdate : ' . $dbdate, '');
            return $dbdate;
        }
        $timedate = new TimeDate();
        $obj_date = DateTime::createFromFormat($current_user->getPreference('datef').' H:i:s', $dt);

        $dbdate = date($timedate->get_db_date_time_format(), $obj_date->getTimestamp());
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'dbdate : ' . $dbdate, '');
        return $dbdate;
    }

    /**
     * method getProductNameByID to get product name by product id.
     *
     * @param string $product_id product id
     * @return string $product_name product name
     * @author dinesh.itkar
     * @since Jan 07, 2016
     */
    public function getProductNameByID($product_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_id : ' . $product_id, 'method getProductNameByID to get product name by product id.');
        $product_name = "";
        $product_item_bean = BeanFactory::getBean('pi_product_item');
        $product_name_bean = $product_item_bean->retrieve($product_id);
        if ($product_name_bean != null) {
            $product_name = $product_name_bean->name;
        }

        unset($product_item_bean, $product_name_bean);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_name : ' . $product_name, '');
        return $product_name;
    }
    /**
     * method to validate database date format
     *
     * @param string $date date which needs to be validated
     * @return bool
     * @author sagar.salunkhe
     * @since April 24, 2018
     */
    public function validateDbDateFormat($date)
    {
        $format = 'Y-m-d H:i:s';
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * method to validate email address
     *
     * @param string $email_id Email Address
     * @return bool $valid valid email id or not
     * @author kamal.thiyagarajan
     * @since May 03, 2017
     */
    public function isValidEmailId()
    {
    	$email_address = isset($_REQUEST['email_id']) ? $_REQUEST['email_id'] : '';
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'email_address : ' . $email_address, 'method to validate email id.');

    	$valid = false;
    	require_once 'include/SugarEmailAddress/SugarEmailAddress.php';
    	$valid = SugarEmailAddress::isValidEmail($email_address);
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'valid : ' . $valid, '');
    	$result = array();
    	$result['valid'] = $valid;
    	echo json_encode($result);
    	exit;
    }

    /**
     * function to store workflow related product offerings in session
     *
     * @author sagar.salunkhe
     * @since Apr 27, 2017
     */
    public static function storeWorkflowOfferingsInSession()
    {
        $arr_json_enterprise = $data_json_gmone = array();

        // get config data from AppSettings class
        $res_gmone = AppSettings::load('AccessControlExt_GMOne');
        $res_enterprise = AppSettings::load('AccessControlExt_EnterpriseMail');

        // process json data to array
        $arr_json_gmone = json_decode($res_gmone, TRUE);
        if (!is_null($arr_json_gmone))
            $data_json_gmone = $arr_json_gmone;

        $arr_json_enterprise = json_decode($res_enterprise, TRUE);
        if (!is_null($arr_json_enterprise))
            $data_json_enterprise = $arr_json_enterprise;

        unset($_SESSION['wf_config_list']['workflow_flag'], $_SESSION['wf_config_list']['enterprise_mail_po']);

        // store processed data into session
        if (isset($data_json_gmone['product_offering']) && is_array($data_json_gmone['product_offering'])) {
            foreach ($data_json_gmone['product_offering'] as $key => $value) {
                $_SESSION['wf_config_list']['workflow_flag'][$key] = $value;
            }
        }

        if (isset($data_json_enterprise['product_offering']) && is_array($data_json_enterprise['product_offering'])) {
            foreach ($data_json_enterprise['product_offering'] as $key => $value)
                $_SESSION['wf_config_list']['enterprise_mail_po'][$key] = $value;
        }

        return true;
    }

    /**
     * function to set workflow product offerings from sessiont to sugar_config global variable
     *
     * @author sagar.salunkhe
     * @since Apr 27, 2017
     */
    public static function setGlobalConfigWorkflowOfferings()
    {
        global $sugar_config;

        // if session variable is not present call function storeWorkflowOfferingsInSession
        if (!isset($_SESSION['wf_config_list'])) {
            self::storeWorkflowOfferingsInSession();
        }

        if (is_object($_SESSION['wf_config_list']))
            $wf_config_list = self::object_to_array($_SESSION['wf_config_list']);
        else
            $wf_config_list = $_SESSION['wf_config_list'];

        unset($sugar_config['workflow_flag'], $sugar_config['enterprise_mail_po']);

        // GMOne product offerings
        if (isset($wf_config_list['workflow_flag']) && is_array($wf_config_list['workflow_flag'])) {
            foreach ($wf_config_list['workflow_flag'] as $key => $offerings_id) {
                $sugar_config['workflow_flag'][$offerings_id] = 'Yes';
            }
        }

        // Enterprise Mail product offerings
        if (isset($wf_config_list['enterprise_mail_po']) && is_array($wf_config_list['enterprise_mail_po'])) {
            foreach ($wf_config_list['enterprise_mail_po'] as $key => $offerings_id) {
                $sugar_config['workflow_flag'][$offerings_id] = 'Yes';
                $sugar_config['enterprise_mail_po'][$key] = $offerings_id;
            }
        }

        return true;
    }
}
?>
