<?php
/**
 * Key-Value store for application settings.
 * @author Satoshi Mamada
 */
class AppSettings {

	/** [Const] Table name to store data. */
	const TABLE_NAME        = '`app_settings`';
	/** [Const] Column name of Key */
	const COLUMN_NAME_KEY   = '`key`';
	/** [Const] Column name of Value */
	const COLUMN_NAME_VALUE = '`value`';

	/**
	 * Load value using Key.
	 * @param string $key Key
	 * @return string Value (NULL: Couldn't get value caused by not exist the key in app_settings table, or error occured.)
	 */
	public static function load($key) {
		global $db;
		$result = null;

		// Check argument
		if(empty($key)) return $result;

		try {
			// Build SQL statement
			$sql = sprintf("SELECT %s FROM %s WHERE %s='%s'"
			             , self::COLUMN_NAME_VALUE, self::TABLE_NAME, self::COLUMN_NAME_KEY, $db->quote($key));
			// Execute query
			$queryResult = $db->query($sql);
			// Read response
			if (($row = $db->fetchByAssoc($queryResult, false)) != null) {
				$result = $row['value'];
			}
		} catch(Exception $e) {
			$GLOBALS['log']->error($e->getMessage());
		}
		return $result;
	}

	/**
	 * Save value using Key.
	 * @param string $key Key (*If Key is empty, then do nothing.)
	 * @param string $value Value to save
	 * @return boolean Error occured (true: error occured / false: no error(success))
	 */
	public static function save($key, $value) {
		global $db;
		$result = false;

		// Check argument
		if(empty($key)) return $result;

		try {
			// Writes(Updates) value to app_settings table.
			$updSql = sprintf("UPDATE %s SET %s='%s' WHERE %s='%s'"
			                , self::TABLE_NAME
			                , self::COLUMN_NAME_VALUE, $db->quote($value)
			                , self::COLUMN_NAME_KEY,   $db->quote($key));
			$updResult = $db->query($updSql);
			if($db->getAffectedRowCount($updResult) < 1) {
				// If updated record was not exist, then try to insert.
				$insSql = sprintf("INSERT INTO %s (%s, %s) VALUES ('%s', '%s')"
			                    , self::TABLE_NAME
			                    , self::COLUMN_NAME_KEY, self::COLUMN_NAME_VALUE
			                    , $db->quote($key), $db->quote($value));
				$insResult = $db->query($insSql);
			}
		} catch(Exception $e) {
			$GLOBALS['log']->error($e->getMessage());
			$result = true;
		}
		return $result;
	}
}