<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
/**
 * XMLUtils Class
 * File System Unix UTF-8
 */
class XMLUtils
{

    /**
     * Method for get value by tag
     *
     * @param xml $xml xml data
     * @param string $prefix
     * @param string $tag_name
     * @return string $tag_val
     * @author Shrikant.Gaware
     * @since Oct 21, 2015
     */
    protected function getValueByTag($xml, $prefix, $tag_name)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'xml : ' . print_r($xml, true) . GCM_GL_LOG_VAR_SEPARATOR . 'prefix : ' . $prefix . GCM_GL_LOG_VAR_SEPARATOR . 'tag_name : ' . $tag_name, 'Method for get value by tag');
        $tag_val = '';
        foreach ($xml->xpath($prefix . $tag_name) as $item) {
            $row = simplexml_load_string($item->asXML());
            $prefix_name = preg_replace('/[^A-Za-z0-9\-]/', '', $prefix);
            if (isset($row->{$prefix_name . ':characteristicValue'}->{$prefix_name . ':' . $tag_name}) && !empty($row->{$prefix_name . ':characteristicValue'}->{$prefix_name . ':' . $tag_name})) {
                $tag_val = $row->{$prefix_name . ':characteristicValue'}->{$prefix_name . ':' . $tag_name};
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'tag_val : ' . print_r($tag_val, true), '');
        return (string) $tag_val;
    }

    /**
     * Method to get date time zone
     *
     * @param string $str_date_time_zone
     * @return array $result_date_time_zone
     */
    protected function fnGetDateTimeZone($str_date_time_zone)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'str_date_time_zone : ' . $str_date_time_zone, 'Method to get date time zone');
        global $sugar_config;
        if (!empty($str_date_time_zone)) {
            $result_date_time_zone = array();
            $fmt_date_time_zone = new DateTime($str_date_time_zone);
            $result_date_time_zone['date_time'] = $fmt_date_time_zone->format("Y-m-d H:i:s");
            $tz = $fmt_date_time_zone->getTimezone();
            if ($tz->getName() == 'Z' || date_default_timezone_get() == $tz->getName()) {
                $result_date_time_zone['time_zone'] = $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            } else {
                $result_date_time_zone['time_zone'] = $tz->getName();
            }
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'result_date_time_zone : ' . print_r($result_date_time_zone, true), '');
            return $result_date_time_zone;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * Exception handler function for API
     *
     * @param array $arr_error_code
     * @param string $str_err_list
     * @param array $get_error_array
     * @return xml $error_response
     */
    public function fnGetErrorXMLData($arr_error_code, $str_err_list, $get_error_array)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_error_code : ' . print_r($arr_error_code, true) . GCM_GL_LOG_VAR_SEPARATOR . 'str_err_list : ' . $str_err_list . GCM_GL_LOG_VAR_SEPARATOR . 'get_error_array : ' . print_r($get_error_array, true), 'Exception handler function for API');
        global $sugar_config;
        require_once ($sugar_config['root_dir_path'] . '/custom/service/v4_1_custom/APIHelper/ClassServiceHelper.php');
        $ClassServiceHelper = new ClassServiceHelper();

        $custom_err_msg = $str_err_list;
        $api_ver = 1;

        if ($get_error_array) {
            $error_list = array();
            foreach ($arr_error_code as $error_code) {
                $error_message = $ClassServiceHelper->getServiceCodeDetails($api_ver, $error_code, '');
                $error_message = (!empty($error_message) && isset($error_message)) ? $error_message : '';
                $error_list[] = "$error_code : " . $error_message;
            }
            $error_response = array(
                                    'error' => $error_list
            );
            unset($error_list);
        } else {
            $error_list = '';
            $schema_url = $GLOBALS['sugar_config']['site_url'] . 'custom/XMLSchema';
            $error_response = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<exp-v0:exception
    xsi:schemaLocation="$schema_url
                                    file:/var/www/html/custom/XMLSchema/Operation/CommonException-v0_1.xsd"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:exp-v0="$schema_url/common-exception"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <exp-v0:exceptionDetails>
XML;
            foreach ($arr_error_code as $error_code) {
                $dynamic_err_msg = ($error_code == '3004') ? $custom_err_msg : '';
                $err_desc_data = $ClassServiceHelper->getServiceCodeDetails($api_ver, $error_code, $dynamic_err_msg);
                $err_desc_data = (!empty($err_desc_data) && isset($err_desc_data)) ? $err_desc_data : '';
                $err_desc_data_result = $this->fnHtmlEnityDecode($err_desc_data);
                $err_desc_data_result = (!empty($err_desc_data_result) && isset($err_desc_data_result)) ? $err_desc_data_result : '';
                $error_list .= "<exp-v0:item>
            <exp-v0:messageCode>$error_code</exp-v0:messageCode>
            <exp-v0:message>$err_desc_data_result</exp-v0:message>
            </exp-v0:item>";
            }
            $error_response .= <<<XML
    $error_list
XML;
            $error_response .= <<<XML

    </exp-v0:exceptionDetails>
</exp-v0:exception>
XML;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'error_response : ' . print_r($error_response, true), '');
        return $error_response;
    }

    /**
     * Exception handler function for API 2.0
     *
     * @param array $arr_error_code Error Code
     * @param array $arr_error_details Error Details
     * @return xml $error_response Error XML
     */
    public function fnGetErrorXMLDataV2($errors)
    {
        global $sugar_config;
        require_once ($sugar_config['root_dir_path'] . '/custom/service/v4_1_custom/APIHelper/ClassServiceHelper.php');
        $ClassServiceHelper = new ClassServiceHelper();

        $error_response = $err_desc_data = $err_code_desc = $err_detail_desc = $error_list = '';
        $api_ver = 2;

        $schema_url = $GLOBALS['sugar_config']['site_url'] . 'api/xml-schema/v2';
        $error_response = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<exp:exceptions xsi:schemaLocation="https://gcm.ntt.com/api/xml-schema/v2/exception $schema_url/exception/exception.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:exp="https://gcm.ntt.com/api/xml-schema/v2/exception" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
XML;
        foreach ($errors['codes'] as $error_code) {
            $error_messages = !empty($errors['messages'][$error_code]) ? $errors['messages'][$error_code] : array();
            $error_details  = !empty($errors['details'][$error_code]) ? $errors['details'][$error_code] : array();
            $utc_time_stamp = date("Y-m-d\TH:i:s\Z");

            foreach ($error_messages as $error_index => $message) {
                $error_list .= "
    <exp:exception>
        <exp:error-code>$error_code</exp:error-code>
        <exp:error-date-time>$utc_time_stamp</exp:error-date-time>
        <exp:error-message>$message</exp:error-message>";

                    if(!empty($error_details[$error_index])) {
                    $error_list .= "
        <exp:error-details>";
                        if(is_array($error_details[$error_index])) {
                            foreach ($error_details[$error_index] as $details) {
                                $details = $this->fnHtmlEnityDecode($details);
                                $error_list .= "
            <exp:error-detail>$details</exp:error-detail>";
                            }
                        } else {
                            $details = $this->fnHtmlEnityDecode($error_details[$error_index]);
                            $error_list .= "
            <exp:error-detail>$details</exp:error-detail>";
                        }
                    $error_list .= "
        </exp:error-details>";
                    }

                $error_list .= "
    </exp:exception>";
            }
        }
        $error_response .= <<<XML
    $error_list
XML;
        $error_response .= <<<XML

</exp:exceptions>
XML;
        return $error_response;
    }

    /**
     * Handle html entity decode
     *
     * @param string $input_message
     * @param array $arr_error_details Error Details
     * @return XML (String) html entity decoded data
     */
    private function fnHtmlEnityDecode($input_message)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'input_message : ' . $input_message, 'Handle html entity decode');
        $return_input_message = $input_message_html_entity_decode = '';
        if (!empty($input_message)) {
            $input_message_html_entity_decode = html_entity_decode($input_message, ENT_QUOTES | 'ENT_XML1');
            $return_input_message = htmlspecialchars($input_message_html_entity_decode, ENT_QUOTES | 'ENT_XML1');
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_input_message : ' . print_r($return_input_message, true), '');
        return $return_input_message;
    }

    /**
     * get XML error
     *
     * @param xml $error
     * @return string $return Formatted error string
     * @author shrikant.gaware
     * @since Oct 28, 2015
     */
    private function libxmlDisplayError($error)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'error : ' . print_r($error, true), '');
        $return = '';
        if ($error->level != LIBXML_ERR_WARNING) {
            switch ($error->level) {
                /* case LIBXML_ERR_WARNING: $return .= "Warning $error->code: "; break; */
                case LIBXML_ERR_ERROR:
                    $return .= "Error $error->code: ";
                    break;
                case LIBXML_ERR_FATAL:
                    $return .= "Fatal Error $error->code: ";
                    break;
            }
            $return .= trim($error->message);
            if ($error->file) {
                $return .= " in $error->file";
            }
            $return .= " on line $error->line";
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return : ' . print_r($return, true), '');
        return $return;
    }

    /**
     * get Formatted XML error
     *
     * @return array $error_list Formatted error array
     * @author shrikant.gaware
     * @since Oct 28, 2015
     */
    public function libxmlDisplayErrors()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'get Formatted XML error');
        $errors = libxml_get_errors();
        $error_list = array();
        foreach ($errors as $error) {

            $err = $this->libxmlDisplayError($error);
            $err = (!empty($err) && isset($err)) ? $err : '';
            if ($err != '')
                $error_list[] = $err;
        }
        libxml_clear_errors();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'error_list : ' . print_r($error_list, true), '');
        return $error_list;
    }

    /**
     * method to check other than Japan, US and Canada location.
     *
     * @param string $location Location Code
     * @author dinesh.itkar
     * @return boolean true|false
     * @since Dec 12, 2015
     */
    protected function checkLocation($location)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'location : ' . $location, 'method to check other than Japan, US and Canada location');
        global $sugar_config;
        $location_list = array(
                            $sugar_config['gcm_cstm_const']['country_code']['JPN'],
                            $sugar_config['gcm_cstm_const']['country_code']['USA'],
                            $sugar_config['gcm_cstm_const']['country_code']['CAN']
        );
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        if (in_array($location, $location_list))
            return true;
        else
            return false;
    }

    /**
     * Method to get country tag name
     *
     * @param string $var1 start string
     * @param string $var2 end string
     * @param string $pool whole string
     * @return string
     */
    protected function get_country_tag_name($var1, $var2, $pool)
    {
        $temp1 = strpos($pool, $var1) + strlen($var1);
        $result = substr($pool, $temp1, strlen($pool));
        $dd = strpos($result, $var2);
        if ($dd == 0) {
            $dd = strlen($result);
        }
        $country_name = substr($result, 0, $dd);

        if ($country_name == 'american-property') {
            $country_name = 'american';
        } elseif ($country_name == 'canadian-property') {
            $country_name = 'canadian';
        } elseif ($country_name == 'japanese-property') {
            $country_name = 'japanese';
        }
        return $country_name;
    }

    /**
     * Method to parse xml tags
     *
     * @param xml $post_request xml data
     * @param string $xpath_query xpath query
     * @return array parse xml data in array format
     */
    protected function getTagElements($post_request, $xpath_query)
    {
        $result_xml_to_arr = array();
        if (!empty($post_request) && !empty($xpath_query)) {
            $post_request_xml_obj = simplexml_load_string($post_request);
            if (isset($post_request_xml_obj)) {
                $xml_obj_list = $post_request_xml_obj->xpath($xpath_query);
                if (!empty($xml_obj_list)) {
                    foreach ($xml_obj_list as $item) {
                        $result = simplexml_load_string($item->asXML());
                        $xmlarray = array();
                        $this->XMLToArrayFlat($result, $xmlarray, '', true);
                        $result_xml_to_arr[] = $xmlarray;
                    }
                }
            }
        }
        return $result_xml_to_arr;
    }

    /**
     * Method to convert xml data to array
     *
     * @param xml $xml xml string obj
     * @param array &$return output after converting xml to array
     * @param string $path tag name for generating path in array key
     * @param string $root set root tag name in array key
     */
    private function XMLToArrayFlat($xml, &$return, $path, $root)
    {
        $children = array();
        if ($xml instanceof SimpleXMLElement) {
            $children = $xml->children();
            if ($root) { // we're at root
                $root_name = $xml->getName();
                $root_name_arr = explode(':', $root_name);
                $path .= '/' . $root_name_arr[sizeof($root_name_arr) - 1];
            }
        }

        if (count($children) == 0) {
            $return[$path] = (string) $xml;
            return;
        }
        $seen = array();
        foreach ($children as $child => $value) {
            $child_name = $xml->getName();
            $child_name_arr = explode(':', $child_name);
            $child_att = explode(':', $child);

            $childname = ($child instanceof SimpleXMLElement) ? $child_name_arr[sizeof($child_name_arr) - 1] : $child_att[sizeof($child_att) - 1];
            if (!isset($seen[$childname])) {
                $seen[$childname] = 0;
            }
            $seen[$childname]++;
            $this->XMLToArrayFlat($value, $return, $path . '/' . $child_att[sizeof($child_att) - 1] . '[' . $seen[$childname] . ']', false);
        }
    }

    /**
     * Method to remove leading zeros from UUIDs
     *
     * @param string $str_uuid UUID with leading zeros
     * @param string $arr_uuid value after removing leading zeros from UUIDs
     * @return string
     */
    public function uuidRemoveFormat($str_uuid, $retZeroIfEmpty = false)
    {
        $arr_uuid = explode("-", $str_uuid);
        $res = ltrim($arr_uuid[count($arr_uuid) - 1], '0');
        if ($retZeroIfEmpty && $res == '')
            return '0';
            return $res;
    }
}

?>
