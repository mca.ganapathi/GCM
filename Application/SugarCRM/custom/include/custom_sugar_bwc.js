var sugar_sidecar_app = window.parent.SUGAR.App;
$(document).ready(function(){
    $('input[type="button"][id^="CANCEL"]').each(function(index) {

    var btn_cancel = $(this); //cancel button

    clickhandler = btn_cancel.attr("onclick"); // original click handler

    click_attr_body = "sugar_sidecar_app.alert.show('leave_confirmation',{level:'confirmation',messages:sugar_sidecar_app.lang.get('LBL_WARN_UNSAVED_CHANGES',app.module),onConfirm:function(){"+String(clickhandler)+"},onCancel:function(){return!1}}); return false;";

    btn_cancel.attr("onclick", click_attr_body);
    });
});