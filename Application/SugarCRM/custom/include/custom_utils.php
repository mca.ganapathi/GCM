<?php
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
/**
 * Check if file exist if yes then include file
 * 
 * @author Shrikant.Gaware
 * @since Sep 30, 2015
 */
if (file_exists('custom/include/ModUtils.php')) {
    include_once ('custom/include/ModUtils.php');
}

// Initialize NTT Communications' UUID class
if (file_exists('custom/include/nttcomUUID/nttcomUUID.php')) {
    include_once ('custom/include/nttcomUUID/nttcomUUID.php');
    if (!NTTComUUID::isValid()) {
        define('GCM_SYSTEM_ID','');
        NTTComUUID::setSystemId(GCM_SYSTEM_ID);
    }
}

// Require PMS-Client class definication
if (file_exists('custom/include/pmsClient.php')) {
    include_once ('custom/include/pmsClient.php');
}

// Require Key-Value store class for application settings.
if (file_exists('custom/include/AppSettings.php')) {
    include_once ('custom/include/AppSettings.php');
}

// Require to extend the sugar configuration
if (file_exists('custom/include/gcm_config.php')) {
    include_once ('custom/include/gcm_config.php');
}

//Global constants stored file
if (file_exists('custom/include/gcm_constants.php')) {
	include_once ('custom/include/gcm_constants.php');
}

?>