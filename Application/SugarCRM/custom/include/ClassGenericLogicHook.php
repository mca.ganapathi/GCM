<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * class Generic Logic hook
 *
 * @author sagar.salunkhe
 *         @date 01-June-2016
 */
class ClassGenericLogicHook
{

    /* field name suffix for which uuid will get auto updated */
    const STR_UUID_SUFFIX = '_uuid';

    public function __construct()
    {}

    /**
     * generic function to set uuid to the fields having field name suffix as _uuid
     *
     * @author sagar.salunkhe
     * @param object $bean - record sugar object [Sugar default]
     * @param object $event - hook event [Sugar default]
     * @param array $arguments - required parameters passed by framework [Sugar default]
     * @return void @date 01-Jun-2016
     */
    public function setModuleFieldUUIDs(&$bean, $event, $arguments)
    {
        $module_field_defs = $bean->getFieldDefinitions(); // fetch module fields from module bean

        // 1. copy UUIDs from old version in case of contract modification DF-155
        if (empty($bean->fetched_row['id']) && (isset($_POST['version_upgrade']) && $_POST['version_upgrade'] == 'true') && ! empty($bean->previous_version_id)) {
            $bean_prev_ver = BeanFactory::getBean($bean->object_name);
            $bean_prev_ver->retrieve($bean->previous_version_id);

            if (! empty($bean_prev_ver->id)) {
                foreach ($module_field_defs as $field_name => $field_def) {
                    // if field name has defined suffix & uuid value is empty
                    if ((substr($field_name, - (strlen(self::STR_UUID_SUFFIX))) === self::STR_UUID_SUFFIX) && empty($bean->$field_name)) $bean->$field_name = $bean_prev_ver->$field_name;
                }
            }
        } else {
            // 2. auto-generate UUIDs for normal save
            foreach ($module_field_defs as $field_name => $field_def) {
                // if field name has defined suffix & uuid value is empty
                if ((substr($field_name, - (strlen(self::STR_UUID_SUFFIX))) === self::STR_UUID_SUFFIX) && empty($bean->$field_name)) $bean->$field_name = create_guid();
            }
        }
    }
}
?>