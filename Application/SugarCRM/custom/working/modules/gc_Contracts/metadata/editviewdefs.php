<?php
$module_name = 'gc_Contracts';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'hidden' => 
        array (
          0 => '<input type="hidden" name="sugar_request" id="sugar_request" value="1">',
          1 => '<input type="hidden" name="contacts_country_id_c" id="contacts_country_id_c" value="{$CONTACTS_COUNTRY_ID_C}">',
          2 => '<input type="hidden" name="approver_user" id="approver_user" value="{$APPROVER_USER}">',
        ),
        'buttons' => 
        array (
          0 => 'SAVE',
          1 => 'CANCEL',
          2 => 
          array (
            'customCode' => '<input type="button" id="btn_custom_view_change_log" name="btn_custom_view_change_log"  onclick="auditLog( \'gc_Contracts\', 900, 900, \'{$fields.id.value}\');" value="View Change Log"/>',
          ),
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'cache/include/javascript/sugar_grp_yui_widgets.js',
        ),
        1 => 
        array (
          'file' => 'custom/modules/gc_Contracts/include/javascript/LineItemUtils.js',
        ),
        2 => 
        array (
          'file' => 'custom/modules/gc_Contracts/include/javascript/EditView.js',
        ),
        3 => 
        array (
          'file' => 'custom/modules/gc_Contracts/include/javascript/ProductPoupulate.js',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '20',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '20',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'contract_version',
            'label' => 'LBL_CONTRACT_VERSION',
            'type' => 'readonly',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'contract_status',
            'studio' => 'visible',
            'label' => 'LBL_CONTRACT_STATUS',
          ),
          1 => 
          array (
            'name' => 'loi_contract',
            'studio' => 'visible',
            'label' => 'LBL_LOI_CONTRACT',
          ),
        ),
        2 => 
        array (
          0 => 'description',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'team_name',
            'displayParams' => 
            array (
              'display' => true,
            ),
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'pm_products_gc_contracts_1_name',
            'displayParams' => 
            array (
              'initial_filter' => '&product_status_advanced=Approved',
            ),
          ),
          1 => 
          array (
            'name' => 'ndb_solution_id',
            'studio' => 'visible',
            'customLabel' => '<span style="font-weight:lighter">{$MOD.LBL_NDB_SOLUTION_ID}</span>&nbsp;<img title=\'{$fields.ndb_solution_id.help}\' src=\'themes/Sugar/images/info_inline.png?v=03QxjiIC-nlXIK1nio3fhA\'/>:',
            'type' => 'readonly',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'contract_type',
            'studio' => 'visible',
            'label' => 'LBL_CONTRACT_TYPE',
          ),
          1 => 
          array (
            'name' => 'contract_scheme',
            'studio' => 'visible',
            'label' => 'LBL_CONTRACT_SCHEME',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'product_offering',
            'label' => 'LBL_PRODUCT_OFFERING',
            'customCode' => '{include file=$CUSTOMDROPDOWNTPL fieldname=$ID_PRODUCT_OFFERING fieldlist= $LIST_PRODUCT_OFFERINGS }',
            'displayParams' => 
            array (
              'required' => true,
            ),
          ),
          1 => 
          array (
            'name' => 'billing_type',
            'studio' => 'visible',
            'label' => 'LBL_BILLING_TYPE',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'accounts_gc_contracts_1_name',
            'label' => 'LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_ACCOUNTS_TITLE',
            'displayParams' => 
            array (
              'required' => true,
            ),
          ),
          1 => 
          array (
            'name' => 'contacts_gc_contracts_1_name',
            'label' => 'LBL_CONTACTS_GC_CONTRACTS_1_FROM_CONTACTS_TITLE',
            'displayParams' => 
            array (
              'field_to_name_array' => 
              array (
                'id' => 'contacts_gc_contracts_1contacts_ida',
                'name' => 'contacts_gc_contracts_1_name',
              ),
              'additionalFields' => 
              array (
                'c_country_id_c' => 'contacts_country_id_c',
              ),
            ),
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'contract_startdate',
            'label' => 'LBL_CONTRACT_STARTDATE',
            'customCode' => '<div id="div_contract_startdate">{include file="custom/include/tpl/CustomDateTime.tpl" c_fld="contract_startdate" c_view="EditView" c_app = "" c_calss = "" c_dependentfield = ""}<input name="contract_startdate_hidden" id="contract_startdate_hidden" type="hidden" class="datehidden" value="{$FIELD_CONTRACT_STARTDATE_VAL}"></div>',
          ),
          1 => 
          array (
            'name' => 'contract_startdate_timezone',
            'label' => 'LBL_CONTRACT_STARTDATE_TIMEZONE',
            'customCode' => '{include file=$TZ_CUSTOMDROPDOWNTPL fieldname=$ID_CONTRACT_START_DATE fieldlist= $LIST_TIMEZONES }<input type="hidden" value="{$fields.contract_startdate_timezone.value}" id="contract_startdate_timezone" name="contract_startdate_timezone">',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'contract_period',
            'label' => 'LBL_CONTRACT_PERIOD',
          ),
          1 => 
          array (
            'name' => 'contract_period_unit',
            'studio' => 'visible',
            'label' => 'LBL_CONTRACT_PERIOD_UNIT',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'ndb_quote_id',
            'studio' => 'visible',
            'customLabel' => '<span style="font-weight:lighter">{$MOD.LBL_NDB_QUOTE_ID}</span>&nbsp;<img title=\'{$fields.ndb_quote_id.help}\' src=\'themes/Sugar/images/info_inline.png?v=03QxjiIC-nlXIK1nio3fhA\'/>:',
            'type' => 'readonly',
          ),
          1 => 
          array (
            'name' => 'ndb_quote_name',
            'studio' => 'visible',
            'customLabel' => '<span style="font-weight:lighter">{$MOD.LBL_NDB_QUOTE_NAME}</span>&nbsp;<img title=\'{$fields.ndb_quote_name.help}\' src=\'themes/Sugar/images/info_inline.png?v=03QxjiIC-nlXIK1nio3fhA\'/>:',
            'type' => 'readonly',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'factory_reference_no',
            'label' => 'LBL_FACTORY_REFERENCE_NO',
          ),
          1 => '',
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'ext_sys_created_by',
            'label' => 'LBL_EXT_SYS_CREATED_BY',
          ),
          1 => 
          array (
            'name' => 'ext_sys_modified_by',
            'label' => 'LBL_EXT_SYS_MODIFIED_BY',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'ndb_sales_representative_tpl',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_SALES_REPRESENTATIVE_TPL',
            'customCode' => '{include file=$SALESREPRESENTATIVETPL}',
            'hideLabel' => true,
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'ndb_line_item_details_tpl',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_LINE_ITEM_DETAILS_TPL',
            'customCode' => '{include file=$LINEITEMTPL}',
            'hideLabel' => true,
          ),
        ),
      ),
    ),
  ),
);
?>