<?php
$module_name = 'gc_Contracts';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'global_contract_id' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_GLOBAL_CONTRACT_ID',
        'width' => '10%',
        'default' => true,
        'name' => 'global_contract_id',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
      'favorites_only' => 
      array (
        'name' => 'favorites_only',
        'label' => 'LBL_FAVORITES_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'global_contract_id' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_GLOBAL_CONTRACT_ID',
        'width' => '10%',
        'default' => true,
        'name' => 'global_contract_id',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
    'contract_status' =>   array(
    				'name' => 'contract_status',
    				'studio' => 'visible',
    				'label' => 'LBL_CONTRACT_STATUS'
    		),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'accounts_gc_contracts_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_ACCOUNTS_TITLE',
        'id' => 'ACCOUNTS_GC_CONTRACTS_1ACCOUNTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'accounts_gc_contracts_1_name',
      ),
      'product_offering' => 
       array (
        'type' => 'enum',
        'label' => 'LBL_PRODUCT_OFFERING',
        'width' => '10%',
        'default' => true,
        'name' => 'product_offering',
        'displayParams' => array(
          'size' => 5
        ),
        'function' => array(
          'name' => 'getProductOfferingList',
          'params' => array(
              false
           ),
         ) ,         
      ),
      'favorites_only' => 
      array (
        'name' => 'favorites_only',
        'label' => 'LBL_FAVORITES_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>