<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * class ClassDirectDepositBeforeSave
 */
class ClassDirectDepositBeforeSave
{

    /**
     * method setLineItemAuditEntry to Save Audit Logs under Line Item Module
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author dinesh.itkar
     * @since Jan 02, 2016
     */
    
    function setLineItemAuditEntry($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'method to Save Audit Logs under Line Item Module');
        
        // Avoid audit log for new record.
        if (empty($bean->fetched_row['id'])) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
            return;
        }
        
        if (!empty($bean->contract_history_id)) {
            $obj_line_Item_contract_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
            $obj_line_Item_contract_history_bean->retrieve($bean->contract_history_id);
            $obj_line_item_bean = BeanFactory::getBean('gc_LineItem');
            $obj_line_item_bean->retrieve($obj_line_Item_contract_history_bean->line_item_id);
        }
        
        if (!empty($obj_line_item_bean)) {
            // Audit Bank Code
            if ($bean->bank_code != $bean->fetched_row['bank_code']) {
                $this->insertDirectDepositAudit($obj_line_item_bean, 'Direct Deposit - Bank Code', 'text', $bean->fetched_row['bank_code'], $bean->bank_code);
            }
            
            // Audit Branch Code
            if ($bean->branch_code != $bean->fetched_row['branch_code']) {
                $this->insertDirectDepositAudit($obj_line_item_bean, 'Direct Deposit - Branch Code', 'text', $bean->fetched_row['branch_code'], $bean->branch_code);
            }
            
            // Audit Deposit Type
            if ($bean->deposit_type != $bean->fetched_row['deposit_type']) {
                global $app_list_strings;
                $deposit_type_value_before = $app_list_strings['deposit_type_list'][$bean->fetched_row['deposit_type']];
                $deposit_type_value_after = $app_list_strings['deposit_type_list'][$bean->deposit_type];
                $this->insertDirectDepositAudit($obj_line_item_bean, 'Direct Deposit - Deposit Type', 'text', $deposit_type_value_before, $deposit_type_value_after);
            }
            
            // Audit Account Number
            if ($bean->account_no != $bean->fetched_row['account_no']) {
                $this->insertDirectDepositAudit($obj_line_item_bean, 'Direct Deposit - Account Number', 'text', $bean->fetched_row['account_no'], $bean->account_no);
            }
            
            // Audit Payee Contact Person Name (Latin Character)
            if ($bean->payee_contact_person_name_1 != $bean->fetched_row['payee_contact_person_name_1']) {
                $this->insertDirectDepositAudit($obj_line_item_bean, 'Direct Deposit - Payee Contact Person Name (Latin Character)', 'text', $bean->fetched_row['payee_contact_person_name_1'], $bean->payee_contact_person_name_1);
            }
            
            // Audit Payee Contact Person Name (Local Character)
            if ($bean->payee_contact_person_name_2 != $bean->fetched_row['payee_contact_person_name_2']) {
                $this->insertDirectDepositAudit($obj_line_item_bean, 'Direct Deposit - Payee Contact Person Name', 'text', $bean->fetched_row['payee_contact_person_name_2'], $bean->payee_contact_person_name_2);
            }
            
            // Audit Payee Telephone Number
            if ($bean->payee_tel_no != $bean->fetched_row['payee_tel_no']) {
                $this->insertDirectDepositAudit($obj_line_item_bean, 'Direct Deposit - Payee Telephone Number', 'text', $bean->fetched_row['payee_tel_no'], $bean->payee_tel_no);
            }
            
            // Audit Payee E-mail Address
            if ($bean->payee_email_address != $bean->fetched_row['payee_email_address']) {
                $this->insertDirectDepositAudit($obj_line_item_bean, 'Direct Deposit - Payee E-mail Address', 'text', $bean->fetched_row['payee_email_address'], $bean->payee_email_address);
            }
            
            // Audit Remarks
            if ($bean->remarks != $bean->fetched_row['remarks']) {
                $this->insertDirectDepositAudit($obj_line_item_bean, 'Direct Deposit - Remarks', 'text', $bean->fetched_row['remarks'], $bean->remarks);
            }            
        }
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * method insertDirectDepositAudit to save Direct Deposit Audit record.
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param string $field_name field name to audit
     * @param string $data_type date type of field
     * @param string $before value before update
     * @param string $after value after update
     * @author dinesh.itkar
     * @since Jan 02, 2016
     */
    private function insertDirectDepositAudit($bean, $field_name, $data_type, $before, $after)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'field_name : '.$field_name.GCM_GL_LOG_VAR_SEPARATOR.'data_type : '.$data_type.GCM_GL_LOG_VAR_SEPARATOR.'before : '.$before.GCM_GL_LOG_VAR_SEPARATOR.'after : '.$after, 'to save Direct Deposit Audit record');
        $audit_entry = array();
        $audit_entry['field_name'] = $field_name;
        $audit_entry['data_type'] = $data_type;
        $audit_entry['before'] = $before;
        $audit_entry['after'] = $after;
        $bean->db->save_audit_records($bean, $audit_entry);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}

?>