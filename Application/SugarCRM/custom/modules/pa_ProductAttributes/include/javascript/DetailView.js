/*
 * @file : custom/modules/pa_ProductAttributes/include/javascript/EditView.js 
 * @author : Sagar.Salunkhe 
 * @date : 22 Sept 2015 
 * @desc : All DetailView related JavaScript methods comes under this file
 */

/*
 * @Note : All element related triggers/events are written here and not in metadata; as
 * if we include/exclude field from layout will end into losing javascript array param
 */

RecordId = $('input[name="record"]').val();

$(document).ready(function() {
	PageLoad();
});

/*
 * @desc : method to get called once complete page is loaded
 * @author : Sagar.Salunkhe 
 * @params : NULL
 * @return : (void method) 
 */
function PageLoad() {

	if($('#attribute_type').val()!='AttributeValue') $('#detailpanel_2').hide();
	
	if($('#attribute_type').val() == 'EntryField') $('#parent_attribute').parent().parent().hide();	//hide parent attribute <tr> if attribute type is "Entry Field"

	if($('#delete_button').length==1){
		document.getElementById('delete_button').onclick = function(){
			
			var _form = document.getElementById('formDetailView'); 
			_form.return_module.value='pa_ProductAttributes'; 
			_form.return_action.value='ListView'; 
			_form.action.value='Delete';

			var AjaxUrl = "index.php?module=pa_ProductAttributes&action=AjaxProductAttributes&to_pdf=1";
		if($('#attribute_type').val()=='AttributeValue'){
 		SUGAR.ajaxUI.showLoadingPanel();
		$.ajax({
				type: "POST",
				url: AjaxUrl,
				//data: {parent_entity_attributes:parent_entity_attributes,eId:eId,moduleType:moduleType,hdRecordId:hdRecordId,execute:'getDetails'},
				data: {RecordId:RecordId,command:'getCountAttributeValues'},
				dataType: "json",
				async: false ,
				success: function(result){
					if(result.ResponseCode == 1){
						SUGAR.ajaxUI.hideLoadingPanel();
						if(confirm('Are you sure you want to delete this record? It will delete '+result.CountAttributeValues+ ' related Attribute Value(s) Record(s).')) SUGAR.ajaxUI.submitForm(_form);
					}
					else{
						SUGAR.ajaxUI.hideLoadingPanel();
						return false;
					}
				}
			});
		}
		else
		{
			if(confirm('Are you sure you want to delete this record?')) SUGAR.ajaxUI.submitForm(_form);
		}	
		};
	}
}