/*
 * @file : custom/modules/pa_ProductAttributes/include/javascript/DetailView.js 
 * @author : Sagar.Salunkhe 
 * @date : 22 Sept 2015 
 * @desc : All EditView related JavaScript methods comes under this file
 */

/*
 * @Note : All element related triggers/events are written here and not in metadata; as
 * if we include/exclude field from layout will end into losing javascript array param
 */

RecordId = $('input[name="record"]').val();

$(document).ready(function() {
	$('#save_and_continue').closest('table').hide();   //hide "Save and Continue" button as well as pagination buttons as it doesnt load this file
	PageLoad();
});

/*
 * @desc : method to get called once complete page is loaded
 * @author : Sagar.Salunkhe 
 * @params : NULL
 * @return : (void method) 
 */
function PageLoad() {
	AssignElementEvents();
	OnChangeAttributeType(); //toggle the view as per the selected attribute type on page load
	$(document).on('change','div.DivProductAttrDep>select',function (e){ onChangeProductAttribute($(this),e);});

	if(RecordId != ''){
		setEditViewForm();
	}
}

function ShowAttrHierarachyEdit(){
	if(typeof JsonAttributeHierarchy !== 'undefined' && JsonAttributeHierarchy !== null){
		if(JsonAttributeHierarchy.length == 0) return;
		$.each( JsonAttributeHierarchy , function( j , i ){
			$('#ProductAttribute_'+j+'').find('option[value="'+i+'"]').prop('selected', 'selected').change() ;
		} );
	}
}

function setEditViewForm() {
	$('#hiddenLineItemCounter').val(tmp_hiddenLineItemCounter);
	$("#attribute_type option").filter(":selected").siblings('option').prop('disabled', true);		//restrict use from changing attribute type
	ShowAttrHierarachyEdit();
}

function onChangeProductAttribute(objElement,objEvent){

	var AjaxUrl = "index.php?module=pa_ProductAttributes&action=AjaxProductAttributes&to_pdf=1";
	var CurrentDiv = objElement.parent();
	var ElementAttrRecordId = $.trim(objElement.val());
	var DependencyDepth = parseInt( objElement.data('dependency-depth') + 1 );
	var asyncStatus = (RecordId != 'undefined' && RecordId != '') ? false : true;

	if( $.trim(ElementAttrRecordId) != '' && $.trim(ElementAttrRecordId) != null ) {
		SUGAR.ajaxUI.showLoadingPanel();
		$.ajax({
				type: "POST",
				url: AjaxUrl,
				//data: {parent_entity_attributes:parent_entity_attributes,eId:eId,moduleType:moduleType,hdRecordId:hdRecordId,execute:'getDetails'},
				data: {ElementAttrRecordId:ElementAttrRecordId,RecordId:RecordId,command:'getChildAttributes'},
				/* dataType: "json", */
				async: asyncStatus ,
				success: function(result){
					
					result = JSON.parse(result);
					
					if(result.ResponseCode == 1){
						var subAttributeHTML = '' ;
						subAttributeHTML += '<div class="DivProductAttrDep" style="float:left;margin-left:5px;margin-top:5px;"> \
						<select data-dependency-depth='+DependencyDepth+' \
						id="ProductAttribute_'+DependencyDepth+'" name="ProductAttribute_'+DependencyDepth+'"> \
						<option value="">-- Select Attribute Name --</option>';
						$.each( result.SubAttributesList , function( j , i ){
							if( j !== RecordId) 
								subAttributeHTML += '<option value="'+j+'">'+i+'</option>';
							else
								subAttributeHTML+='<option disabled="disabled" value="'+j+'">'+i+'</option>';
						} );
						subAttributeHTML += '</select></div>';

						if( typeof $('select[data-dependency-depth='+DependencyDepth+']') === "undefined" ){
							$(subAttributeHTML).insertAfter(CurrentDiv);
						}else{
							$.fn.removeExistingDiv(objElement) ;
							$(subAttributeHTML).insertAfter(CurrentDiv);
						}
						$('input[name="hiddenDependencyDepth"]').val(DependencyDepth);
						SUGAR.ajaxUI.hideLoadingPanel();
					}
					else{
						$.fn.removeExistingDiv(objElement) ;
						SUGAR.ajaxUI.hideLoadingPanel();
						return false;
					}
				},
	            error:function() {
	                console.log('Error processing Ajax Request');
	            }
			});
	
	}
	else {
		$.fn.removeExistingDiv(objElement) ;	
		SUGAR.ajaxUI.hideLoadingPanel();
		return false;
	}
}

$.fn.removeExistingDiv = function(objElement){
	var dependency_depth = parseInt( objElement.data('dependency-depth') );
	$('div.DivProductAttrDep>select').each(function(){
		if( parseInt( $(this).data('dependency-depth') ) > parseInt( objElement.data('dependency-depth') ) ){
			$(this).parent().remove() ;
		}
	});
	$('input[name="dependency_depth"]').val(dependency_depth) ;
};

function AddValueLineItems() {
 	hiddenLineItemCounter = parseInt($('#hiddenLineItemCounter').val());
	//CurrentDiv = $('#DivLineItem'+hiddenLineItemCounter);
	hiddenLineItemCounter = parseInt(hiddenLineItemCounter + 1);
	
 	newLineItemHtml = GenerateValueRow(hiddenLineItemCounter);
	
	//$(newLineItemHtml).insertAfter(CurrentDiv);
	$( ".MainDivLineItems" ).append( newLineItemHtml );
	$('#hiddenLineItemCounter').val(hiddenLineItemCounter); 
}

function validateValueLineItems(row_no){
	
	if ($('#attribute_type').val() != 'AttributeValue') return true;
	
	row_no = row_no || '';
	
	flag = true;
	
	if(row_no==''){
		$('[name="AttrVal_Name[]"]').each(function(index) {
			
			elementValue = $.trim($(this).val());

			if(elementValue=='') {
				msg = 'Attribute '+SUGAR.language.get('pa_ProductAttributes', 'LBL_LINEVALUE_NAME')+' cannot be empty for any of the Line Items.';
				sugarMessageAlert(msg,'Sugar Validation');
				flag = false;
				return;
			}
			
		});
		
		//check duplicate attribute and uom combination
		for(i=0; i<($('[name="AttrVal_Name[]"]').length); i++){
			elementValue = $.trim($('[name="AttrVal_Name[]"]:eq('+i+')').val());
			elementUoM = $.trim($('[name="AttrVal_UoM[]"]:eq('+i+')').val());
			ElementCombination = elementValue+elementUoM;

			if(ElementCombination=='') continue;

			for(j=(parseInt(i)+1); j<($('[name="AttrVal_Name[]"]').length); j++){
				LvL2Combination = $.trim($('[name="AttrVal_Name[]"]:eq('+j+')').val())+$.trim($('[name="AttrVal_UoM[]"]:eq('+j+')').val());
				console.log(ElementCombination+' : '+LvL2Combination);
				if(flag==false) break;
				if(ElementCombination==LvL2Combination){
					sugarMessageAlert('Attribute Value and UoM Combinations cannot be duplicate in same Attribute!','Sugar Validation');
					flag = false;
					break;
				}
			}
			
		}
		
	}
	else{
		if($.trim($('#AttrVal_Name'+row_no).val())=='') {
			msg = 'Attribute '+SUGAR.language.get('pa_ProductAttributes', 'LBL_LINEVALUE_NAME')+' cannot be empty for any of the Line Items.';
			sugarMessageAlert(msg,'Sugar Validation');
			flag = false;
		}
	}
	
	return flag;
	
}


function GenerateValueRow(RowNum) {
	
 	var LineHTML = '<div id="DivLineItem'+RowNum+'" name="DivLineItem[]"> \
      <span>\
         <input type="hidden" id="hiddenAttributeValueId'+RowNum+'" name="hiddenAttributeValueId[]" value="">\
	  </span>\
      <table width="100%" cellspacing="1" cellpadding="0" border="0" class="yui3-skin-sam edit view panelContainer" id="LBL_EDITVIEW_PANEL2"> \
         <tr>\
            <td width="10%" valign="top" scope="col">\
               '+SUGAR.language.get('pa_ProductAttributes', 'LBL_LINEVALUE_NAME')+'\
               <span class="required">*</span>\
            </td>\
            <td width="35%" valign="top">\
               <input type="text" title="" value="" maxlength="100" size="30" id="AttrVal_Name'+RowNum+'" name="AttrVal_Name[]">\
            </td>\
            <td width="10%" valign="top" scope="col">\
			   ' + SUGAR.language.get('pa_ProductAttributes', 'LBL_LINEVALUE_UOM') + '\
            </td>\
            <td width="35%" valign="top">\
               <input type="text" title="" value="" maxlength="10" size="30" id="AttrVal_UoM'+RowNum+'" name="AttrVal_UoM[]">\
            </td>\
            <td width="10" valign="top">\
               <div style="padding-top:5px;" data-line-item="0"><input type="button" value="Remove" name="btnRemoveLineItem"\ style="margin-left:5px;margin-bottom:5px;float:right;" class="clsRemoveLineItem" onclick="RemoveLineItem(this,'+RowNum+');"></div>\
            </td>\
         </tr>\
      </table>\
   </div>';
	return LineHTML;
}

function RemoveLineItem(objButton,row_no) {
	if($('[name="AttrVal_Name[]"]').length == 1){
		sugarMessageAlert('At lease one line item is required!','Sugar Validation');
	}
	else{
		if($.trim($('#hiddenAttributeValueId'+row_no).val())=='')
			$('#DivLineItem'+row_no).remove();
		else{
			
			if(typeof flagCanDeleteAttr !== 'undefined' && flagCanDeleteAttr==0){
				sugarMessageAlert('Cannot delete existing Line Values as Attribute Record is used in Product Items Module!','Sugar ValidateSave');
				return;
			}
			
			YAHOO.SUGAR.MessageBox.show({msg: 'Are you sure you want to delete this Record?', title: 'Delete '+ SUGAR.language.get('pa_ProductAttributes', 'LBL_LINEVALUE_NAME') +' Row', type: 'confirm', 
				fn: function(confirm) {
					if (confirm == 'yes') {
						$('#DivLineItem'+row_no).remove();
					}
					if (confirm == 'no') {
						//do something if 'No' was clicked
					}
				}      
			});
		}
	}
}

/*
 * @desc : method to assign javascript events to dom elements
 * @author : Sagar.Salunkhe 
 * @params : NULL
 * @return : (void method) 
 */
function AssignElementEvents() {
	$('#attribute_type').on('change', function (e) {
		e.preventDefault();
		OnChangeAttributeType();
    });

	document.getElementById('SAVE_HEADER').onclick = function(){
		var _form = document.getElementById('EditView');
		_form.action.value = 'Save';
		if (check_form('EditView') && ValidateSave())
			SUGAR.ajaxUI.submitForm(_form);
		return false;
	};
	
	document.getElementById('SAVE_FOOTER').onclick = function(){
		var _form = document.getElementById('EditView');
		_form.action.value = 'Save';
		if (check_form('EditView') && ValidateSave())
			SUGAR.ajaxUI.submitForm(_form);
		return false;
	};
}

/*
 * @desc : method to manage actions on change of Attribute Type Dropdown
 * @author : Sagar.Salunkhe 
 * @params : NULL
 * @return : (void method)
 */
function OnChangeAttributeType() {
	
	objElement = $("#attribute_type");
	
	if (objElement.length == 0) {
		console.log('Error : OnChangeAttributeType() - objElement does not Exist!');
		return;
	}

	if (objElement.val() == 'AttributeValue') {
		//$('#value_data_type').val('Text');					//set default value as text for element "Value Data Type" 
		//$('#value_data_type').parent().parent().hide();		//hide table row of element "Value Data Type"
		$('#detailpanel_2').show();							//display Attribute Value Panel
		$('#ProductAttribute_0').parent().parent().parent().show();	//display parent attribute dropdown (relate field in db)
		
	} else if (objElement.val() == 'ParentAttribute') {
		//$('#value_data_type').val('Text');					//set default value as text for element "Value Data Type" 
		//$('#value_data_type').parent().parent().hide();		//hide table row of element "Value Data Type"
		$('#detailpanel_2').hide();							//hide Attribute Value Panel
		$('#ProductAttribute_0').parent().parent().parent().show();	//display parent attribute dropdown (relate field in db)
		
	} else if (objElement.val() == 'EntryField') {
		//$('#value_data_type').val('Text');					//set default value as text for element "Value Data Type" 
		//$('#value_data_type').parent().parent().show();		//display table row of element "Value Data Type"
		$('#detailpanel_2').hide();							//hide Attribute Value Panel
		$('#ProductAttribute_0').parent().parent().parent().hide();	//display parent attribute dropdown (relate field in db)
		
	} else {
		//$('#value_data_type').val('Text');						//set default value as text for element "Value Data Type" 
		//$('#value_data_type').parent().parent().hide('slow');	//hide table row of element "Value Data Type"
		$('#detailpanel_2').hide('slow');						//hide Attribute Value Panel
		$('#ProductAttribute_0').parent().parent().parent().hide('slow');	//hide parent attribute dropdown (relate field in db)
	}

	return;
}

/*
 * @desc : method to perform javascript validations before submitting/saving the form
 * @author : Sagar.Salunkhe
 * @params : NULL
 * @return : (void method)
 */
function ValidateSave() {
	clear_all_errors(); //clear all sugar error messages in order to set our own
	//if(validateAttributeType()==false) return false;	//perform attribute type level validations here
	if(checkDuplicateAttributeName()==false) return false;
	if(validateValueLineItems()==false) return false;
	return true;
}

/**
 * @desc   : method to perform duplicate name validation before submitting/saving the form
 * @author : Sagar.Salunkhe
 * @param  : NULL
 * @return : (boolean)
 **/
function checkDuplicateAttributeName(){
	AttributeName = $('#name').val();
	
	if($.trim(AttributeName)=='') return true;
	var returnFlag = true;
	var AjaxUrl = 'index.php?module=pa_ProductAttributes&action=AjaxProductAttributes&to_pdf=1';
	SUGAR.ajaxUI.showLoadingPanel();
	$.ajax({
			type: "POST",
			url: AjaxUrl,
			data: {RecordId:RecordId,AttributeName:AttributeName,command:'checkDuplicateAtributeName'},
			dataType: "json",
			async: false ,
			success: function(result){
				if(result!=null && result.ResponseCode == 1){
					if(result.CountAttributeNames > 0){
						add_error_style('EditView','name',SUGAR.language.get('pa_ProductAttributes', 'ERR_DUPLICATE')+' '+SUGAR.language.get('pa_ProductAttributes', 'LBL_NAME'),true);
						returnFlag = false;
					}
				}
				else{
					console.log('Error processing Ajax Request');
					returnFlag = false;
				}
			},
			error:function() {
				console.log('Error processing Ajax Request');
				returnFlag = false;
			}
		});
	SUGAR.ajaxUI.hideLoadingPanel();
	return returnFlag;
}

function validateAttributeType(){
	//attribute type validation comes here
	return true;
}

function sugarMessageAlert(text_msg,alert_title){
	YAHOO.SUGAR.MessageBox.show({msg: text_msg, title: alert_title} );
}