<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

require_once ('custom/modules/pa_ProductAttributes/include/ClassProductAttributes.php');

class ClassProdAttrBeforeDelete extends ClassProductAttributes
{

    /*
     * @desc : method to check validation before deleting the product attribute record
     * @author : Sagar.Salunkhe
     * @params :
     * (object)$bean - record sugar object [Sugar default]
     * (object)$event - hook event [Sugar default]
     * (array)$arguments - required parameters passed by framework [Sugar default]
     * @return : (void method)
     * @date : 30-Sep-2015
     */
    function checkProdAttrBeforeDelete($bean, $event, $arguments)
    {
        global $current_user;
        
        // call module class function to check if record satisfies the deletion conditions
        $res_before_delete_check = $this->checkBeforeAttributeDelete($bean->id);
        
        // if response code is 1 record dont satisfy the condition
        if ($res_before_delete_check['ResponseCode'] === 0) {
            
            // create session variable and access the same on DetailView to display message details.
            $_SESSION[$bean->object_name . '_DeleteCheck_' . $current_user->id] = $res_before_delete_check;
            $redirect_action = (isset($_REQUEST['record'])) ? 'DetailView' : 'index';
            $query_params = array(
                'module' => $bean->module_dir,
                'action' => $redirect_action,
                'record' => $bean->id
            );
            
            // finally redirect to detailview
            SugarApplication::redirect('index.php?' . http_build_query($query_params));
        }
    }
}
?>