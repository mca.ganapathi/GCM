<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (�MSA�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
class ClassProductAttributes
{

    /**
     * Class Contructor
     */
    function __construct()
    {
        global $db;
        $this->db = $db;
    }

    /*
     * @desc : method to get child attributes of respective parent attributes
     * @author : Sagar.Salunkhe
     * @params :
     * (string)$attribute_id(optional) - attribute id of which childs need to be fetched
     * (boolean)$only_parent_attrs(optional) - boolean flag to check whether to fetch attributes of type (parent attribute) or (parent attr and attribute value)
     * @return :
     * (array) $array_parent_attributes - attribute module objects array
     * @date : 30-Sep-2015
     */
    function getChildAttributes($attribute_id = '', $only_parent_attrs = false)
    {
        
        $obj_product_attributes = new pa_ProductAttributes(); // product attributes bean object
        
        $where = '';
        
        // query condition
        if ($only_parent_attrs)
            $where .= "pa_productattributes.attribute_type = 'ParentAttribute' AND ";
        else $where .= ' (pa_productattributes.attribute_type = "ParentAttribute" OR pa_productattributes.attribute_type= "AttributeValue") AND ';
        
        if (empty($attribute_id))
            $where .= "(pa_productattributes.pa_productattributes_id_c IS NULL OR pa_productattributes.pa_productattributes_id_c = '') ";
        else $where .= " pa_productattributes.pa_productattributes_id_c = '$attribute_id' ";
        
        // sugar bean function to fetch list of product attributes on provided condition
        $array_parent_attributes = $obj_product_attributes->get_full_list('', $where);
        
        return $array_parent_attributes;
    }

    /*
     * @desc : method to fetch parent attribute of an attribute
     * @author : Sagar.Salunkhe
     * @params :
     * (string)$attribute_id - attribute id of which parent attribute need to be fetched
     * @return :
     * (array) - array consist an id and name of parent attribute
     * @date : 30-Sep-2015
     */
    function getParentAttributeId($attribute_id)
    {
        
        $obj_product_attributes = new pa_ProductAttributes();
        $obj_product_attributes->retrieve($attribute_id);
        
        return array(
            'id' => $obj_product_attributes->pa_productattributes_id_c,
            'name' => $obj_product_attributes->parent_attribute
        );
    }

    /*
     * @desc : method to fetch parent attribute of an attribute
     * @author : Sagar.Salunkhe
     * @params :
     * (string)$attribute_id - attribute id of which parent attribute need to be fetched
     * @return :
     * (array) - array consist an id and name of parent attribute
     * @date : 30-Sep-2015
     */
    function getAttributeParentHierarchy($attribute_id)
    {
        
        $arr_attribute_hierarchy = array();
        
        if (isset($attribute_id) && ! empty($attribute_id)) {
            
            $obj_product_attributes = new pa_ProductAttributes();
            $obj_product_attributes->retrieve($attribute_id);
            
            $arr_attribute_hierarchy[$obj_product_attributes->id] = $obj_product_attributes->name;
            
            $product_attribute_id = $attribute_id;
            
            for ($i = 0; $i >= 0; $i ++) {
                $parent_attr_id = $this->getParentAttributeId($product_attribute_id);
                
                if (empty($parent_attr_id['id']))
                    break;
                else $arr_attribute_hierarchy[$parent_attr_id['id']] = $parent_attr_id['name'];
                
                $product_attribute_id = $parent_attr_id['id'];
            }
            $arr_attribute_hierarchy = array_reverse($arr_attribute_hierarchy);
        }
        return $arr_attribute_hierarchy;
    }

    /*
     * @desc : method to save product attribute values line items
     * @author : Sagar.Salunkhe
     * @params :
     * (object)$bean - attribute record bean object
     * (array)$arr_attribute_value_items - attribute value details
     * @return : void method
     * @date : 30-Sep-2015
     */
    function saveAttributeValueLineItems($bean, $arr_attribute_value_items)
    {
        
        $value_lines_count = count($arr_attribute_value_items['hiddenAttributeValueId']);
        $existing_attribute_value_line_items = $this->getAttributeValueLineItems($bean->id);
        
        foreach ($existing_attribute_value_line_items as $key => $attribute_line_item)
            $existing_attribute_value_line_items[$key] = $attribute_line_item->id;
            
            // compare posted line values with existing and delete the records which are removed from UI
        $arr_delete_value_line_items = array_diff($existing_attribute_value_line_items, $arr_attribute_value_items['hiddenAttributeValueId']);
        
        // Insert or Update Atribute Value Line Items
        for ($i = 0; $i < $value_lines_count; $i ++) {
            $obj_product_attribute_values = new pa_ProductAttributeValues();
            $obj_product_attribute_values->retrieve($arr_attribute_value_items['hiddenAttributeValueId'][$i]);
            $obj_product_attribute_values->pa_productattributes_id_c = $bean->id;
            $obj_product_attribute_values->name = $arr_attribute_value_items['AttributeValueName'][$i];
            $obj_product_attribute_values->uom = $arr_attribute_value_items['AttributeValueUoM'][$i];
            $obj_product_attribute_values->save();
        }
        
        // Soft Delete removed Line Items from Attribute Value Module
        foreach ($arr_delete_value_line_items as $key => $record_id) {
            $obj_product_attribute_values = new pa_ProductAttributeValues();
            $obj_product_attribute_values->retrieve($record_id);
            $obj_product_attribute_values->deleted = 1; // mark deleted as true
            $obj_product_attribute_values->save();
        }
    }

    /*
     * @desc : method to get list of attribute values of respective attribute record
     * @author : Sagar.Salunkhe
     * @params :
     * (string)$attribute_id - attribute id of which attribute values need to be fetched
     * @return :
     * (array) $array_attribute_values - attribute values module objects array
     * @date : 30-Sep-2015
     */
    function getAttributeValueLineItems($attribute_id)
    {
        
        if (empty($attribute_id)) return array();
        
        $obj_product_attribute_values = new pa_ProductAttributeValues();
        
        $where = "pa_productattributevalues.pa_productattributes_id_c = '$attribute_id'";
        
        $array_attribute_values = $obj_product_attribute_values->get_full_list('', $where);
        return $array_attribute_values;
    }

    /*
     * @desc : method to check conditions/validations before deleting the product attribute
     * @author : Sagar.Salunkhe
     * @params :
     * (string)$attribute_id - attribute id of which validations need to be checked
     * @return :
     * (array) $res - response array with response code(0:cannot delete, 1:can delete), response message,
     * count of module related records, array of module name
     * and respective count of records where atribute id is used
     * @date : 30-Sep-2015
     */
    function checkBeforeAttributeDelete($attribute_id)
    {
        
        $res_attribute_related_products = $this->checkProductBeforeAttrDelete($attribute_id);
        if ($res_attribute_related_products['ResponseCode'] === 0) return $res_attribute_related_products; // return if attribute is used in any of the product items
        
        $response_code = 1;
        $response_msg = 'Success';
        
        // check if it is used in any Atribute module hierarchy
        $array_child_attributes = $this->getChildAttributes($attribute_id, false); // get child attributes of product attribute
        
        $array_count_modules = array();
        
        // if atribute has child attributes, gather required information
        if (count($array_child_attributes) > 0) {
            $array_count_modules[$array_child_attributes[0]->object_name] = count($array_child_attributes);
            
            $obj_product_attributes = new pa_ProductAttributes();
            $obj_product_attributes->retrieve($attribute_id);
            
            $response_code = 0;
            $response_msg = 'Sorry! "' . $obj_product_attributes->name . '" is related to listed module(s). To delete "' . $obj_product_attributes->name . '" you have to remove the following relations!!';
        }
        
        // prepare and return an array with required details
        $res = array(
            'ResponseCode' => $response_code,
            'ResponseMsg' => $response_msg,
            'CountRelatedModules' => count($array_child_attributes),
            'ArrayRelatedModuleRecords' => $array_count_modules
        );
        return $res;
    }

    function checkProductBeforeAttrDelete($attribute_id)
    {
        
        if (empty($attribute_id)) return array(
            'ResponseCode' => 1,
            'ResponseMsg' => 'Attribute record id is blank!'
        );
        
        // get highest parent of attribute
        $arr_parent_attribute_hierarchy = $this->getAttributeParentHierarchy($attribute_id);
        $parent_attribute_id = '';
        foreach ($arr_parent_attribute_hierarchy as $key => $val) {
            $parent_attribute_id = $key;
            break;
        }
        
        if (! empty($parent_attribute_id)) {
            require_once ('custom/modules/pi_product_item/include/ClassProductItems.php'); // include product items class file
            $obj_class_product_items = new ClassProductItems();
            $res_attribute_related_products = $obj_class_product_items->getAttributeRelatedProducts($parent_attribute_id); // get attribute related products
            return $res_attribute_related_products;
        }
        
        // prepare and return an array with required details and return
        return array(
            'ResponseCode' => 1,
            'ResponseMsg' => 'No Parent Attribute Found!'
        );
    }
}
?>