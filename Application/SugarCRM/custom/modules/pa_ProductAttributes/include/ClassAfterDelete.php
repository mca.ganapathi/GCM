<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

require_once ('custom/modules/pa_ProductAttributes/include/ClassProductAttributes.php');

class ClassProdAttrAfterDelete extends ClassProductAttributes
{

    /*
     * @desc : method to soft delete all attribute values related to the respective product attribute
     * @author : Sagar.Salunkhe
     * @params :
     * (object)$bean - record sugar object [Sugar default]
     * (object)$event - hook event [Sugar default]
     * (array)$arguments - required parameters passed by framework [Sugar default]
     * @return : (void method)
     * @date : 30-Sep-2015
     */
    function deleteRelatedAttributeValues($bean, $event, $arguments)
    {
        
        global $current_user;
        $res_attribute_value_line_items = $this->getAttributeValueLineItems($bean->id); // retrieve attribute values
                                                                                    
        // soft delete the attribute values
        foreach ($res_attribute_value_line_items as $key => $obj_attribute_value) {
            $obj_product_attribute_values = new pa_ProductAttributeValues();
            $obj_product_attribute_values->retrieve($obj_attribute_value->id);
            $obj_product_attribute_values->deleted = 1;
            $obj_product_attribute_values->save();
        }
    }
}
?>