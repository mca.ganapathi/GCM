<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

require_once ('custom/modules/pa_ProductAttributes/include/ClassProductAttributes.php');

class ClassProdAttrAfterSave extends ClassProductAttributes
{

    /*
     * @desc method to save the attribute value line items if attribute type is "Attribute Value"
     * @author Sagar.Salunkhe
     * @params
     * (object)$bean - record sugar object [Sugar default]
     * (object)$event - hook event [Sugar default]
     * (array)$arguments - required parameters passed by framework [Sugar default]
     * @return (void method)
     * @date 30-Sep-2015
     */
    function processProdAttrPostSave($bean, $event, $arguments)
    {
        // Save Line Items only if Attribute Type is "Attribute Value"
        if ($bean->attribute_type == 'AttributeValue') {
            $arr_attribute_value_items = array(
                'hiddenAttributeValueId' => $_POST['hiddenAttributeValueId'],
                'AttributeValueName' => $_POST['AttrVal_Name'],
                'AttributeValueUoM' => $_POST['AttrVal_UoM']
            );
            
            $this->saveAttributeValueLineItems($bean, $arr_attribute_value_items); // pass params to module class file
        }
    }
}
?>