{*
/*********************************************************************************
* By installing or using this file, you are confirming on behalf of the entity
* subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
* the SugarCRM Inc. Master Subscription Agreement (�MSA�), which is viewable at:
* http://www.sugarcrm.com/master-subscription-agreement
*
* If Company is not bound by the MSA, then by installing or using this file
* you are agreeing unconditionally that Company will be bound by the MSA and
* certifying that you have authority to bind Company accordingly.
*
* Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
********************************************************************************/
*}
<div class="MainDivLineItems" style="float: left; margin-top: 5px; width: 100%;">
   {if count($ArrayAttributeLineItems) == 0}
   <div id="DivLineItem0" name="DivLineItem[]">
      <span>
      <input type="hidden" id="hiddenAttributeValueId0" name="hiddenAttributeValueId[]" value="{$hiddenAttributeValueId0}">
      </span>
      <table width="100%" cellspacing="1" cellpadding="0" border="0" class="yui3-skin-sam edit view panelContainer" id="LBL_EDITVIEW_PANEL2">
         <tr>
            <td width="10%" valign="top" scope="col">
               {sugar_translate label='LBL_NAME' module='pa_ProductAttributeValues'}:
               <span class="required">*</span>
            </td>
            <td width="35%" valign="top">
               <input type="text" title="" value="" maxlength="100" size="30" id="AttrVal_Name0" name="AttrVal_Name[]">
            </td>
            <td width="10%" valign="top" scope="col">
               {sugar_translate label='LBL_UOM' module='pa_ProductAttributeValues'}:
               <!--<span class="required">*</span>-->
            </td>
            <td width="35%" valign="top">
               <input type="text" title="" value="" maxlength="10" size="30" id="AttrVal_UoM0" name="AttrVal_UoM[]">
            </td>
            <td width="10" valign="top">
               <div style="padding-top:5px;" data-line-item="0"><input type="button" value="Remove" name="btnRemoveLineItem" style="margin-left:5px;margin-bottom:5px;float:right;" class="clsRemoveLineItem" onclick="RemoveLineItem(this,0);"></div>
            </td>
         </tr>
      </table>
   </div>
   {else}
   {counter start=0 print=false name="LineItemsCounter" assign="offset"}
   {foreach from=$ArrayAttributeLineItems key=ValueIndex item=ValueLineItem}
   <div id="DivLineItem{$offset}" name="DivLineItem[]">
      <span>
      <input type="hidden" id="hiddenAttributeValueId{$offset}" name="hiddenAttributeValueId[]" value="{$ValueLineItem.id}">
      </span>
      <table width="100%" cellspacing="1" cellpadding="0" border="0" class="yui3-skin-sam edit view panelContainer" id="LBL_EDITVIEW_PANEL2">
         <tr>
            <td width="10%" valign="top" scope="col">
               {sugar_translate label='LBL_NAME' module='pa_ProductAttributeValues'}:
               <span class="required">*</span>
            </td>
            <td width="35%" valign="top">
               <input type="text" title="" value="{$ValueLineItem.name}" maxlength="100" size="30" id="AttrVal_Name{$offset}" name="AttrVal_Name[]">
            </td>
            <td width="10%" valign="top" scope="col">
               {sugar_translate label='LBL_UOM' module='pa_ProductAttributeValues'}:
               <!--<span class="required">*</span>-->
            </td>
            <td width="35%" valign="top">
               <input type="text" title="" value="{$ValueLineItem.uom}" maxlength="10" size="30" id="AttrVal_UoM{$offset}" name="AttrVal_UoM[]">
            </td>
            <td width="10" valign="top">
               <div style="padding-top:5px;" data-line-item="0"><input type="button" value="Remove" name="btnRemoveLineItem" style="margin-left:5px;margin-bottom:5px;float:right;" class="clsRemoveLineItem" onclick="RemoveLineItem(this,{$offset});"></div>
            </td>
         </tr>
      </table>
   </div>
   {counter print=false name="LineItemsCounter" assign="offset"}
   {/foreach}
   {/if}
</div>
<div style="padding-top:5px;padding-bottom:5px;" id="divAddLines"><input type="button" onclick="AddValueLineItems();" id="btnAddLineItem" value="Add Values" name="btnAddLineItem"></div>