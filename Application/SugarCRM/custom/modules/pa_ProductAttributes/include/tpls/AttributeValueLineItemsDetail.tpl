{*
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (�MSA�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

*}
{if count($ArrayAttributeLineItems) == 0}
<table cellspacing="0" id="LBL_DETAILVIEW_LINEITEMS" class="panelContainer">
   <tbody>
      <tr>
         <td>
            <p class="msg">
               {$APP.LBL_NO_DATA}
            </p>
         </td>
      </tr>
   </tbody>
</table>
{else}
<table cellspacing="0" id="LBL_DETAILVIEW_LINEITEMS" class="panelContainer">
   <tbody>
      {counter start=0 print=false name="LineItemsCounter" assign="offset"}
      {foreach from=$ArrayAttributeLineItems key=ValueIndex item=ValueLineItem}
      <tr>
         <td width="12.5%" scope="col">
            {sugar_translate label='LBL_NAME' module='pa_ProductAttributeValues'}:&nbsp;
         </td>
         <td width="37.5%">
            <span class="sugar_field">
            {$ValueLineItem.name}
            </span>
         </td>
         <td width="12.5%" scope="col">
            {sugar_translate label='LBL_UOM' module='pa_ProductAttributeValues'}:&nbsp;
         </td>
         <td width="37.5%">
            <span class="sugar_field">
            {$ValueLineItem.uom}
            </span>
         </td>
      </tr>
      {counter print=false name="LineItemsCounter" assign="offset"}
      {/foreach}
   </tbody>
</table>
<script>
   {literal}
   parentDivId = $("#LBL_DETAILVIEW_LINEITEMS").closest("div").attr("id"); //get parent div id
   $("#LBL_DETAILVIEW_LINEITEMS").appendTo('#'+(parentDivId)); //append html table to parent div
   $("#ndb_attribute_values").closest("table").hide();
   {/literal}	
</script>
{/if}