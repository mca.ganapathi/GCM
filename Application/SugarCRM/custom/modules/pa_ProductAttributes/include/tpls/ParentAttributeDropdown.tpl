{*
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (�MSA�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

*}
<div class="DivProductAttrDep" style="float:left;margin-top:5px;">
<select id="ProductAttribute_0" data-dependency-depth="0" name="ProductAttribute_0">
	{foreach from=$ParentAttributeArray key=k item=v}
		<option value="{$k}" {if !empty($RecordId) && $k == $RecordId} disabled="disabled" {/if}>{$v}</option>
	{/foreach}
</select>
</div>

