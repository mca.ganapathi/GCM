<?php

if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

class ClassProdAttrBeforeSave
{

    /*
     * @desc : method to assign parent atribute (last node of product attributes hierarchy) Id
     * @author : Sagar.Salunkhe
     * @params :
     * (object)$bean - record sugar object [Sugar default]
     * (object)$event - hook event [Sugar default]
     * (array)$arguments - required parameters passed by framework [Sugar default]
     * @return : (void method)
     * @date : 30-Sep-2015
     */
    function setProductSubAttributeId($bean, $event, $arguments)
    {
        
        // save parent attribute id only if attribute type is "Parent Atribute or Attribute Value"
        if ($bean->attribute_type == 'ParentAttribute' || $bean->attribute_type == 'AttributeValue') {
            $parent_product_attribute_id = '';
            
            // system will post hierarchy of parent attributes. we have to store the last selected record.
            for ($i = 0; $i <= ($_POST['hiddenDependencyDepth']); $i ++) {
                if (! empty($_POST['ProductAttribute_' . $i])) $parent_product_attribute_id = $_POST['ProductAttribute_' . $i];
            }
            
            $bean->pa_productattributes_id_c = $parent_product_attribute_id;
        }
    }
}

?>