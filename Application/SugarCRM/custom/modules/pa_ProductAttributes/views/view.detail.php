<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (�MSA�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

require_once ('include/MVC/View/views/view.detail.php');
require_once ('custom/modules/pa_ProductAttributes/include/ClassProductAttributes.php');

class pa_ProductAttributesViewDetail extends ViewDetail
{

    function pa_ProductAttributesViewDetail()
    {
        // call parent ViewDetail method
        parent::__construct();
    
    }

    function preDisplay()
    {
        $this->showParentAttributeTrail();
        $this->populateValueLineItems();
        parent::preDisplay();
    }

    function display()
    {
        global $current_user;
        if (isset($_SESSION[$this->bean->object_name . '_DeleteCheck_' . $current_user->id])) {
            $this->populateRelatedModulesDiv($_SESSION[$this->bean->object_name . '_DeleteCheck_' . $current_user->id]);
            unset($_SESSION[$this->bean->object_name . '_DeleteCheck_' . $current_user->id]);
        }
        
        /**
         * * START : Hide unused buttons from View **
         */
        unset($this->dv->defs['templateMeta']['form']['buttons'][1]); // Hide Duplicate button
        unset($this->dv->defs['templateMeta']['form']['buttons'][3]); // Hide Find Duplicate button
        unset($this->dv->defs['templateMeta']['form']['buttons'][4]); // Hide Get Data button
        /**
         * * END : Hide unused buttons from View **
         */
        
        // call parent display method
        parent::display();
    }

    function populateValueLineItems()
    {
        $this->ss->assign('LINE_ITEM_VALUES', 'custom/modules/pa_ProductAttributes/include/tpls/AttributeValueLineItemsDetail.tpl');
        
        $obj_class_product_attributes = new ClassProductAttributes();
        $obj_array_attribute_line_items = $obj_class_product_attributes->getAttributeValueLineItems($this->bean->id);
        
        foreach ($obj_array_attribute_line_items as $key => $attribute_line_item)
            $obj_array_attribute_line_items[$key] = (array) $attribute_line_item;
        
        $this->ss->assign('ArrayAttributeLineItems', $obj_array_attribute_line_items);
        
        return count($obj_array_attribute_line_items);
    }

    function showParentAttributeTrail()
    {
        $obj_class_product_attributes = new ClassProductAttributes();
        $obj_attribute_parent_hierarchy = $obj_class_product_attributes->getAttributeParentHierarchy($this->bean->id);
        
        $str = '';
        
        foreach ($obj_attribute_parent_hierarchy as $key => $attribute_parent) {
            
            if ($key == $this->bean->id) continue;
            
            $params = array(
                'module' => $this->module,
                'action' => $this->action,
                'record' => $key
            );
            $str .= "<a href='index.php?" . http_build_query($params) . "'><span data-id-value='$key' class='sugar_field' id='pa_productattributes_id_c'>$attribute_parent</span></a>&nbsp;&rarr;&nbsp;";
        }
        
        $str = trim($str, '&nbsp;&rarr;&nbsp;');
        
        $this->ss->assign('PARENT_ATTRIBUTE_TRAIL', $str);
    }

    function populateRelatedModulesDiv($array_data)
    {
        $html = '';
        
        $label = translate('LBL_MODULE_NAME', 'Accounts');
        
        if ($array_data['CountRelatedModules'] > 0) {
            $html .= '<div id="div_related_values"> <div style="margin-top:20px;"><span style="color:red;">' . $array_data['ResponseMsg'] . '</span></div> <div> <div class="detail view  detail508 expanded">';
            
            $html .= '<table cellspacing="0" class="panelContainer" id="DEFAULT"> <tbody>';
            foreach ($array_data['ArrayRelatedModuleRecords'] as $module_bean => $record_count) {
                $html .= '<tr>
                  <td width="12.5%" scope="col">Module Name :</td>
                  <td width="37.5%"><span class="sugar_field">' . translate('LBL_MODULE_NAME', $module_bean) . '</span></td>
                  <td width="12.5%" scope="col">Related Record(s) :</td>
                  <td width="37.5%"><span class="sugar_field">' . $record_count . '</span></td>
               </tr> ';
            }
            $html .= '</tbody> </table>';
            $html .= '</div> </div> </div>';
        }
        echo $html;
    }
}