<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (�MSA�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

require_once ('include/MVC/View/views/view.edit.php');
require_once ('custom/modules/pa_ProductAttributes/include/ClassProductAttributes.php');

class pa_ProductAttributesViewEdit extends ViewEdit
{

    function pa_ProductAttributesViewEdit()
    {
        // call parent ViewEdit method
        parent::__construct();
    }

    function preDisplay()
    {
        $this->populateParentAttrEle();
        $value_count = $this->populateValueLineItems();
        $flag_can_delete_attr = $this->checkProductBeforeAttrDelete();
        
        // to adjust row indexing in value line items
        if ($value_count > 0) $value_count = $value_count - 1;
        
        echo <<< EOQ
		<script language='javascript'>
		var tmp_hiddenLineItemCounter = $value_count;
		var flagCanDeleteAttr = $flag_can_delete_attr;
		</script>
EOQ;
        // call parent preDisplay method
        parent::preDisplay();
    }

    function display()
    {   
        $this->getProductAttributeHierarchy();
        // call parent display method
        parent::display();
    }

    /*
     * @desc : method to replace Parent Attribute relate field with dropdown
     * @author : Sagar.Salunkhe
     * @params : NULL
     * @return : (void method)
     */
    function populateParentAttrEle()
    {
        $obj_class_product_attributes = new ClassProductAttributes();
        $obj_array_parent_attributes = $obj_class_product_attributes->getChildAttributes('', true);
        
        $parent_attribute_array = array(
            '' => '-- Select Attribute Name --'
        );
        
        if ($obj_array_parent_attributes != null) {
            foreach ($obj_array_parent_attributes as $obj_product_attribute) {
                $parent_attribute_array[$obj_product_attribute->id] = $obj_product_attribute->name;
            }
        }
        
        $this->ss->assign('PARENT_ATTRIBUTE', 'custom/modules/pa_ProductAttributes/include/tpls/ParentAttributeDropdown.tpl');
        $this->ss->assign('ParentAttributeArray', $parent_attribute_array);
        $this->ss->assign('RecordId', $this->bean->id);
        
        unset($parent_attribute_array, $obj_array_parent_attributes);
    }

    function populateValueLineItems()
    {
        $this->ss->assign('LINE_ITEM_VALUES', 'custom/modules/pa_ProductAttributes/include/tpls/AttributeValueLineItemsEdit.tpl');
        
        $obj_class_product_attributes = new ClassProductAttributes();
        $obj_array_attribute_line_items = $obj_class_product_attributes->getAttributeValueLineItems($this->bean->id);
        
        foreach ($obj_array_attribute_line_items as $key => $AttributeLineItem)
            $obj_array_attribute_line_items[$key] = (array) $AttributeLineItem;
        
        $this->ss->assign('ArrayAttributeLineItems', $obj_array_attribute_line_items);
        
        return count($obj_array_attribute_line_items);
    }

    function getProductAttributeHierarchy()
    {
        $arr_attribute_hierarchy = array();
        if ((isset($this->bean->id) && ! empty($this->bean->id)) && ($this->bean->attribute_type == 'AttributeValue' || $this->bean->attribute_type == 'ParentAttribute')) {
            // $arr_attribute_hierarchy[] = $this->bean->id; //removed as it is causing issue while rendering dropdown trail in editview
            $product_attribute_id = $this->bean->id;
            
            for ($i = 0; $i >= 0; $i ++) {
                $obj_class_product_attributes = new ClassProductAttributes();
                $parent_attr_id = $obj_class_product_attributes->getParentAttributeId($product_attribute_id);
                if (empty($parent_attr_id['id']))
                    break;
                else $arr_attribute_hierarchy[] = $parent_attr_id['id'];
                $product_attribute_id = $parent_attr_id['id'];
            }
            $arr_attribute_hierarchy = array_reverse($arr_attribute_hierarchy);
        }
        $json_str = json_encode($arr_attribute_hierarchy);
        
        echo <<< EOQ
		<script language='javascript'>
		var JsonAttributeHierarchy = JSON.parse('{$json_str}');
		</script>
EOQ;
    }

    function checkProductBeforeAttrDelete()
    {
        $obj_class_product_attributes = new ClassProductAttributes();
        $array_product_before_attr_delete = $obj_class_product_attributes->checkProductBeforeAttrDelete($this->bean->id);
        return ($array_product_before_attr_delete['ResponseCode'] == 1) ? 1 : 0;
    }
}