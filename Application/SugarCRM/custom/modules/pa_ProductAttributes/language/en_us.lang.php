<?php
// created: 2015-10-05 07:15:25
$mod_strings = array (
  'LBL_EDITVIEW_PANEL1' => 'Attribute Values',
  'LBL_DETAILVIEW_PANEL2' => 'Other',
  'LBL_ATTRIBUTE_TYPE' => 'Attribute Type',
  'LBL_VALUE_DATA_TYPE' => 'Value Data Type',
  'LBL_PARENT_ATTRIBUTE' => 'Parent Attribute',
);