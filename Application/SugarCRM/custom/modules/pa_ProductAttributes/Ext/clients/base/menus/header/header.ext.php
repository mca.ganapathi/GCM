<?php
// WARNING: The contents of this file are auto-generated.

 
$viewdefs['pa_ProductAttributes']['base']['menu']['header'][] = array (
  'label' => 'Create Product Attributes',
  'acl_action' => 'edit',
  'acl_module' => 'pa_ProductAttributes',
  'icon' => 'fa fa-plus',
  'route' => '#bwc/index.php?module=pa_ProductAttributes&action=EditView&return_module=pa_ProductAttributes&return_action=index',
);
$viewdefs['pa_ProductAttributes']['base']['menu']['header'][] = array (
  'label' => 'View Product Attributes',
  'acl_action' => 'list',
  'acl_module' => 'pa_ProductAttributes',
  'route' => '#bwc/index.php?module=pa_ProductAttributes&action=index&return_module=pa_ProductAttributes&return_action=DetailView',
);
