<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/pa_ProductAttributes/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['pa_ProductAttributes']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/pa_ProductAttributes/Ext/Vardefs/ndb_CustomFields.php

// ProductAttributes Module : [attribute_values]
$dictionary['pa_ProductAttributes']['fields']['ndb_attribute_values'] = array (
	'name' => 'ndb_attribute_values',
	'vname' => 'LBL_NDB_ATTRIBUTE_VALUES',
	'type' => 'varchar',
	'module' => 'pa_ProductAttributes',
	'enforced' => '',
	'dependency' => '',
	'required' => false,
	'massupdate' => '0',
	'default_value' => '',
	'no_default' => false,
	'help' => '',
	'comment' => '',
	'importable' => false,
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'reportable' => false,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'calculated' => false,
	'len' => '255',
	'max_size' => '255',
	'source' => 'non-db',
	'dbType' => 'non-db',
	'studio' => 'visible',
);

?>
<?php
// Merged from custom/Extension/modules/pa_ProductAttributes/Ext/Vardefs/sugarfield_attribute_type.php

 // created: 2016-09-29 04:48:38
$dictionary['pa_ProductAttributes']['fields']['attribute_type']['default'] = 'ParentAttribute';
$dictionary['pa_ProductAttributes']['fields']['attribute_type']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/pa_ProductAttributes/Ext/Vardefs/sugarfield_value_data_type.php

 // created: 2016-09-29 04:48:38
$dictionary['pa_ProductAttributes']['fields']['value_data_type']['default'] = '';
$dictionary['pa_ProductAttributes']['fields']['value_data_type']['full_text_search']['boost'] = 1;


?>
