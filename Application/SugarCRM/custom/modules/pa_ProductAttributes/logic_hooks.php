<?php
$hook_version = 1;
$hook_array = Array();

$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(
    1,
    'Process Generic After Save',
    'custom/modules/pa_ProductAttributes/include/ClassAfterSave.php',
    'ClassProdAttrAfterSave',
    'processProdAttrPostSave'
);

$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(
    1,
    'Process Generic Before Save',
    'custom/modules/pa_ProductAttributes/include/ClassBeforeSave.php',
    'ClassProdAttrBeforeSave',
    'setProductSubAttributeId'
);

$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(
    1,
    'Before Delete Check',
    'custom/modules/pa_ProductAttributes/include/ClassBeforeDelete.php',
    'ClassProdAttrBeforeDelete',
    'checkProdAttrBeforeDelete'
);

$hook_array['after_delete'] = Array();
$hook_array['after_delete'][] = Array(
    1,
    'Delete Attribute values after record is deleted',
    'custom/modules/pa_ProductAttributes/include/ClassAfterDelete.php',
    'ClassProdAttrAfterDelete',
    'deleteRelatedAttributeValues'
);

?>