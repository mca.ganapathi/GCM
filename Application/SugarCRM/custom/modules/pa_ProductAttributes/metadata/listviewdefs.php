<?php
// created: 2018-03-07 11:45:26
$listViewDefs['pa_ProductAttributes'] = array (
  'NAME' => 
  array (
    'width' => '32',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'ATTRIBUTE_TYPE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_ATTRIBUTE_TYPE',
    'width' => '10',
    'default' => true,
  ),
  'PARENT_ATTRIBUTE' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_PARENT_ATTRIBUTE',
    'id' => 'PA_PRODUCTATTRIBUTES_ID_C',
    'link' => true,
    'width' => '10',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10',
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10',
    'default' => true,
  ),
);