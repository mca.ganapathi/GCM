<?php
$popupMeta = array (
    'moduleMain' => 'pa_ProductAttributes',
    'varName' => 'pa_ProductAttributes',
    'orderBy' => 'pa_productattributes.name',
    'whereClauses' => array (
  'name' => 'pa_productattributes.name',
  'value_data_type' => 'pa_productattributes.value_data_type',
  'parent_attribute' => 'pa_productattributes.parent_attribute',
  'favorites_only' => 'pa_productattributes.favorites_only',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'value_data_type',
  5 => 'parent_attribute',
  6 => 'favorites_only',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'value_data_type' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_VALUE_DATA_TYPE',
    'width' => '10%',
    'name' => 'value_data_type',
  ),
  'parent_attribute' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_PARENT_ATTRIBUTE',
    'id' => 'PA_PRODUCTATTRIBUTES_ID_C',
    'link' => true,
    'width' => '10%',
    'name' => 'parent_attribute',
  ),
  'favorites_only' => 
  array (
    'name' => 'favorites_only',
    'label' => 'LBL_FAVORITES_FILTER',
    'type' => 'bool',
    'width' => '10%',
  ),
),
);
