<?php
$module_name = 'pa_ProductAttributes';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'attribute_type',
            'studio' => 'visible',
            'label' => 'LBL_ATTRIBUTE_TYPE',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'value_data_type',
            'studio' => 'visible',
            'label' => 'LBL_VALUE_DATA_TYPE',
          ),
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'parent_attribute',
            'studio' => 'visible',
            'label' => 'LBL_PARENT_ATTRIBUTE',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 'name',
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'ndb_attribute_values',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_ATTRIBUTE_VALUES',
          ),
        ),
      ),
    ),
  ),
);
?>