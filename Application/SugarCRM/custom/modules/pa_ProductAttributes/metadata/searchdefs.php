<?php
$module_name = 'pa_ProductAttributes';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'parent_attribute' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_PARENT_ATTRIBUTE',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'id' => 'PA_PRODUCTATTRIBUTES_ID_C',
        'name' => 'parent_attribute',
      ),
      'favorites_only' => 
      array (
        'name' => 'favorites_only',
        'label' => 'LBL_FAVORITES_FILTER',
        'type' => 'bool',
        'width' => '10%',
        'default' => true,
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'parent_attribute' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_PARENT_ATTRIBUTE',
        'id' => 'PA_PRODUCTATTRIBUTES_ID_C',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'name' => 'parent_attribute',
      ),
      'favorites_only' => 
      array (
        'name' => 'favorites_only',
        'label' => 'LBL_FAVORITES_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
      'attribute_type' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_ATTRIBUTE_TYPE',
        'width' => '10%',
        'default' => true,
        'name' => 'attribute_type',
      ),
      'value_data_type' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VALUE_DATA_TYPE',
        'width' => '10%',
        'name' => 'value_data_type',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>