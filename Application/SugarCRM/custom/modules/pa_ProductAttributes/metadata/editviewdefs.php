<?php
$module_name = 'pa_ProductAttributes';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'form'=> array(
				      'hidden' => array('<input type="hidden" id="hiddenDependencyDepth" name="hiddenDependencyDepth" value="0">',
										'<input type="hidden" id="hiddenLineItemCounter" name="hiddenLineItemCounter" value="0">',
					  ),
      ),
      'includes'=> array(
				      0 => array('file' => 'custom/modules/pa_ProductAttributes/include/javascript/EditView.js'),
					  1 => array('file' => 'cache/include/javascript/sugar_grp_yui_widgets.js'),
					  
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'attribute_type',
            'studio' => 'visible',
            'label' => 'LBL_ATTRIBUTE_TYPE',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'parent_attribute',
            'studio' => 'visible',
            'label' => 'LBL_PARENT_ATTRIBUTE',
            'customCode' => '{include file=$PARENT_ATTRIBUTE}',
          ),
        ),
        2 => 
        array (
          0 => 'name',
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'ndb_attribute_values',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_ATTRIBUTE_VALUES',
			'hideLabel' => 'true',
			'customCode' => '{include file=$LINE_ITEM_VALUES}',
          ),
        ),
      ),
    ),
  ),
);
?>