<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');
require_once ('custom/modules/pa_ProductAttributes/include/ClassProductAttributes.php');

class AjaxProductAttributes extends ClassProductAttributes
{

    function __construct()
    {
        // get method name from request params
        $method = $_REQUEST['command'];
        
        // check if method exists else respond error message
        if (method_exists($this, $method)) {
            $this->$method();
            return;
        } else {
            echo json_encode(array(
                'ResponseCode' => 0,
                'ResponseMsg' => 'Invalid Ajax Action!',
                'Response' => ''
            ));
            return;
        }
    }

    /*
     * @desc : method to get child attributes of respective parent attributes
     * @author : Sagar.Salunkhe
     * @params :
     * (string)$_REQUEST['ElementAttrRecordId'](optional) - attribute id of which childs need to be fetched (passed in request parameters of ajax)
     * @return :
     * (array) $res - response array with response code(0:couldnt processed successfuly,
     * 1:processed successfuly), response message, SubAttributesList array of child attributes
     * @date : 30-Sep-2015
     */
    function getChildAttributes()
    {
        
        $response_code = 0;
        $response_msg = '';
        $parent_attribute_array = array();
        
        // fetch child attributes from module parent class file
        $obj_array_parent_attribute = parent::getChildAttributes($_REQUEST['ElementAttrRecordId'], true);
        
        // process required details if child attributes are available
        if ($obj_array_parent_attribute != null) {
            foreach ($obj_array_parent_attribute as $obj_product_attribute) {
                $parent_attribute_array[$obj_product_attribute->id] = $obj_product_attribute->name;
            }
            $response_code = 1;
            $response_msg = 'Success!';
        }
        
        // finally return json data
        echo json_encode(array(
            'ResponseCode' => $response_code,
            'ResponseMsg' => $response_msg,
            'SubAttributesList' => $parent_attribute_array
        ));
        return;
    }

    /*
     * @desc : method to get count of attribute values
     * @author : Sagar.Salunkhe
     * @params :
     * (string)$_REQUEST['RecordId'] - attribute record id
     * @return :
     * (array) $res - response array with response code(0:couldnt processed successfuly,
     * 1:processed successfuly), response message, Count of Attribute Values
     * @date : 30-Sep-2015
     */
    function getCountAttributeValues()
    {
        
        // fetch attribute values using method declared in parent class
        $obj_array_parent_attribute = $this->getAttributeValueLineItems($_REQUEST['RecordId']);
        
        // finally return json data
        echo json_encode(array(
            'ResponseCode' => 1,
            'ResponseMsg' => 'Success!',
            'CountAttributeValues' => count($obj_array_parent_attribute)
        ));
        return;
    }

    function checkDuplicateAtributeName($attribute_id, $attribute_name)
    {
        
        $attribute_id = $_POST['RecordId'];
        $attribute_name = $_POST['AttributeName'];
        
        // query condition
        $attribute_name = trim($attribute_name);
        $where = " pa_productattributes.name  = '$attribute_name' ";
        
        if (! empty($attribute_id)) $where .= " and pa_productattributes.id <> '$attribute_id' ";
        
        // sugar bean function to fetch list of product attributes on provided condition
        $obj_product_attributes = new pa_ProductAttributes(); // product attributes bean object
        $array_attribute_names = $obj_product_attributes->get_full_list('', $where);
        
        echo json_encode(array(
            'ResponseCode' => 1,
            'ResponseMsg' => 'Success!',
            'CountAttributeNames' => count($array_attribute_names)
        ));
        return;
    }
}

// create an instance of an object to process the ajax requests
$obj_ajax_product_attributes = new AjaxProductAttributes();

?>