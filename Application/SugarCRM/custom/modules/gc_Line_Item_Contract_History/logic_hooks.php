<?php
// Do not store anything in this file that is not part of the array or the hook version. This file will
// be automatically rebuilt in the future.
$hook_version = 1;
$hook_array = Array();
// position, file, function
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(
    1,
    'process fields before saving',
    'custom/modules/gc_Line_Item_Contract_History/include/ClassBeforeSave.php',
    'ClassLineItemHistoryBeforeSave',
    'processLineItemHistoryFields');
$hook_array['before_save'][] = Array(
    2,
    'Create Audit Logs under Line Item Module',
    'custom/modules/gc_Line_Item_Contract_History/include/ClassBeforeSave.php',
    'ClassLineItemHistoryBeforeSave',
    'setLineItemAuditEntry');
$hook_array['before_save'][] = Array(
    3,
    'Set UUIDs in field specific uuid columns',
    'custom/include/ClassGenericLogicHook.php',
    'ClassGenericLogicHook',
    'setModuleFieldUUIDs');

$hook_array['process_record'] = array();
$hook_array['process_record'][] = array(
    1,
    'Convert DB date into local',
    'custom/modules/gc_Contracts/include/ClassAfterRetrieve.php',
    'ClassGcContractAfterRetrieve',
    'processContractLineItemFields'
);
$hook_array['after_retrieve'] = array();
$hook_array['after_retrieve'][] = array(
    1,
    'Convert DB date into local',
    'custom/modules/gc_Contracts/include/ClassAfterRetrieve.php',
    'ClassGcContractAfterRetrieve',
    'processContractLineItemFields'
);
