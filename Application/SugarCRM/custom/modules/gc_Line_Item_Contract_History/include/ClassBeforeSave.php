<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * class ClassLineItemHistoryBeforeSave
 */
class ClassLineItemHistoryBeforeSave
{

    public function processLineItemHistoryFields($bean, $event, $arguments)
    {

        $is_sugar_request = false;

        if(isset($_POST['sugar_request']) && $_POST['sugar_request'])
            $is_sugar_request = true;
        else
            return;
        
        $modUtils = new ModUtils();

        if(isset($bean->process_record_event) && $bean->process_record_event) {
            $bean->fetched_row['service_start_date'] = $bean->preserve_fr_svc_strt_dt;
            $bean->fetched_row['service_end_date'] = $bean->preserve_fr_svc_end_dt;
            $bean->fetched_row['billing_start_date'] = $bean->preserve_fr_bill_strt_dt;
            $bean->fetched_row['billing_end_date'] = $bean->preserve_fr_bill_end_dt;
        }
        
        // service start date
        if (!empty($bean->gc_timezone_id_c) && !empty($bean->service_start_date)) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone');
            $obj_timezone->retrieve($bean->gc_timezone_id_c);

            if (!empty($obj_timezone->utcoffset)) {
                $input_service_startdate = str_replace(' ', 'T', $bean->service_start_date);
                $input_service_startdate = $input_service_startdate . $obj_timezone->utcoffset;
                $arr_res = $modUtils->ConvertDatetoGMTDate($input_service_startdate);

                if ($arr_res['result'] && !empty($arr_res['date'])){
                    $bean->service_start_date_local = $bean->service_start_date;
                    $bean->service_start_date = $arr_res['date'];
                }
                    
            }
            unset($obj_timezone, $input_service_startdate, $arr_res);
        }
        if (!empty($bean->fetched_row['gc_timezone_id_c']) && !empty($bean->fetched_row['service_start_date'])) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone');
            $obj_timezone->retrieve($bean->fetched_row['gc_timezone_id_c']);

            if (!empty($obj_timezone->utcoffset)) {
                $input_startdate = str_replace(' ', 'T', $bean->fetched_row['service_start_date']);
                $input_startdate = $input_startdate . $obj_timezone->utcoffset;
                $arr_res = $modUtils->ConvertDatetoGMTDate($input_startdate);

                if ($arr_res['result'] && !empty($arr_res['date'])){
                    $bean->fetched_row['service_start_date_local'] = $bean->fetched_row['service_start_date'];
                    $bean->fetched_row['service_start_date'] = $arr_res['date'];
                }
                    
            }
            unset($obj_timezone, $input_startdate, $arr_res);
        }

        // service end date
        if (!empty($bean->gc_timezone_id1_c) && !empty($bean->service_end_date)) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone');
            $obj_timezone->retrieve($bean->gc_timezone_id1_c);

            if (!empty($obj_timezone->utcoffset)) {
                $input_service_enddate = str_replace(' ', 'T', $bean->service_end_date);
                $input_service_enddate = $input_service_enddate . $obj_timezone->utcoffset;
                $arr_res = $modUtils->ConvertDatetoGMTDate($input_service_enddate);

                if ($arr_res['result'] && !empty($arr_res['date'])){
                    $bean->service_end_date_local = $bean->service_end_date;
                    $bean->service_end_date = $arr_res['date'];
                }
                    
            }
            unset($obj_timezone, $input_service_enddate, $arr_res);
        }
        if (!empty($bean->fetched_row['gc_timezone_id1_c']) && !empty($bean->fetched_row['service_end_date'])) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone');
            $obj_timezone->retrieve($bean->fetched_row['gc_timezone_id1_c']);

            if (!empty($obj_timezone->utcoffset)) {
                $input_enddate = str_replace(' ', 'T', $bean->fetched_row['service_end_date']);
                $input_enddate = $input_enddate . $obj_timezone->utcoffset;
                $arr_res = $modUtils->ConvertDatetoGMTDate($input_enddate);

                if ($arr_res['result'] && !empty($arr_res['date'])){
                    $bean->fetched_row['service_end_date_local'] = $bean->fetched_row['service_end_date'];
                    $bean->fetched_row['service_end_date'] = $arr_res['date'];
                }
                    
            }
            unset($obj_timezone, $input_enddate, $arr_res);
        }

        // billing start date
        if (!empty($bean->gc_timezone_id2_c) && !empty($bean->billing_start_date)) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone');
            $obj_timezone->retrieve($bean->gc_timezone_id2_c);

            if (!empty($obj_timezone->utcoffset)) {
                $input_billing_startdate = str_replace(' ', 'T', $bean->billing_start_date);
                $input_billing_startdate = $input_billing_startdate . $obj_timezone->utcoffset;
                $arr_res = $modUtils->ConvertDatetoGMTDate($input_billing_startdate);

                if ($arr_res['result'] && !empty($arr_res['date'])){
                    $bean->billing_start_date_local = $bean->billing_start_date;
                    $bean->billing_start_date = $arr_res['date'];
                }
            }
            unset($obj_timezone, $input_billing_startdate, $arr_res);
        }
        if (!empty($bean->fetched_row['gc_timezone_id2_c']) && !empty($bean->fetched_row['billing_start_date'])) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone');
            $obj_timezone->retrieve($bean->fetched_row['gc_timezone_id2_c']);

            if (!empty($obj_timezone->utcoffset)) {
                $input_startdate = str_replace(' ', 'T', $bean->fetched_row['billing_start_date']);
                $input_startdate = $input_startdate . $obj_timezone->utcoffset;
                $arr_res = $modUtils->ConvertDatetoGMTDate($input_startdate);

                if ($arr_res['result'] && !empty($arr_res['date'])){
                    $bean->fetched_row['billing_start_date_local'] = $bean->fetched_row['billing_start_date'];
                    $bean->fetched_row['billing_start_date'] = $arr_res['date'];
                }
                    
            }
            unset($obj_timezone, $input_startdate, $arr_res);
        }

        // billing end date
        if (!empty($bean->gc_timezone_id3_c) && !empty($bean->billing_end_date)) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone');
            $obj_timezone->retrieve($bean->gc_timezone_id3_c);

            if (!empty($obj_timezone->utcoffset)) {
                $input_billing_enddate = str_replace(' ', 'T', $bean->billing_end_date);
                $input_billing_enddate = $input_billing_enddate . $obj_timezone->utcoffset;
                $arr_res = $modUtils->ConvertDatetoGMTDate($input_billing_enddate);

                if ($arr_res['result'] && !empty($arr_res['date'])){
                    $bean->billing_end_date_local = $bean->billing_end_date;
                    $bean->billing_end_date = $arr_res['date'];
                }
            }
            unset($obj_timezone, $input_billing_enddate, $arr_res);
        }
        if (!empty($bean->fetched_row['gc_timezone_id3_c']) && !empty($bean->fetched_row['billing_end_date'])) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone');
            $obj_timezone->retrieve($bean->fetched_row['gc_timezone_id3_c']);

            if (!empty($obj_timezone->utcoffset)) {
                $input_enddate = str_replace(' ', 'T', $bean->fetched_row['billing_end_date']);
                $input_enddate = $input_enddate . $obj_timezone->utcoffset;
                $arr_res = $modUtils->ConvertDatetoGMTDate($input_enddate);

                if ($arr_res['result'] && !empty($arr_res['date'])){
                    $bean->fetched_row['billing_end_date_local'] = $bean->fetched_row['billing_end_date'];
                    $bean->fetched_row['billing_end_date'] = $arr_res['date'];
                }
                    
            }
            unset($obj_timezone, $input_enddate, $arr_res);
        }
    }

    /**
     * method setLineItemAuditEntry to Save Audit Logs under Line Item Module
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author dinesh.itkar
     * @since Jan 05, 2016
     */
    function setLineItemAuditEntry($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'method to Save Audit Logs under Line Item Module');

        // Avoid audit log for new record.
        if (empty($bean->fetched_row['id'])) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
            return;
        }

        if (!empty($bean->line_item_id)) {
            $obj_line_item_bean = BeanFactory::getBean('gc_LineItem');
            $obj_line_item_bean->retrieve($bean->line_item_id);

            if (!empty($obj_line_item_bean)) {
                $service_start_date      = !empty($bean->fetched_row['service_start_date']) ? $bean->fetched_row['service_start_date'] : '';
                $service_end_date        = !empty($bean->fetched_row['service_end_date']) ? $bean->fetched_row['service_end_date'] : '';
                $billing_start_date      = !empty($bean->fetched_row['billing_start_date']) ? $bean->fetched_row['billing_start_date'] : '';
                $billing_end_date        = !empty($bean->fetched_row['billing_end_date']) ? $bean->fetched_row['billing_end_date'] : '';
                $gc_timezone_id_c        = !empty($bean->fetched_row['gc_timezone_id_c']) ? $bean->fetched_row['gc_timezone_id_c'] : '';
                $gc_timezone_id2_c       = !empty($bean->fetched_row['gc_timezone_id2_c']) ? $bean->fetched_row['gc_timezone_id2_c'] : '';
                $gc_timezone_id1_c       = !empty($bean->fetched_row['gc_timezone_id1_c']) ? $bean->fetched_row['gc_timezone_id1_c'] : '';
                $gc_timezone_id3_c       = !empty($bean->fetched_row['gc_timezone_id3_c']) ? $bean->fetched_row['gc_timezone_id3_c'] : '';
                $factory_reference_no    = !empty($bean->fetched_row['factory_reference_no']) ? $bean->fetched_row['factory_reference_no'] : '';
                $tech_account_id         = !empty($bean->fetched_row['tech_account_id']) ? $bean->fetched_row['tech_account_id'] : '';
                $bill_account_id         = !empty($bean->fetched_row['bill_account_id']) ? $bean->fetched_row['bill_account_id'] : '';
                $payment_type            = !empty($bean->fetched_row['payment_type']) ? $bean->fetched_row['payment_type'] : '';
                $product_specification   = !empty($bean->fetched_row['product_specification']) ? $bean->fetched_row['product_specification'] : '';
                $cust_contract_currency  = !empty($bean->fetched_row['cust_contract_currency']) ? $bean->fetched_row['cust_contract_currency'] : '';
                $cust_billing_currency   = !empty($bean->fetched_row['cust_billing_currency']) ? $bean->fetched_row['cust_billing_currency'] : '';
                $igc_contract_currency   = !empty($bean->fetched_row['igc_contract_currency']) ? $bean->fetched_row['igc_contract_currency'] : '';
                $igc_billing_currency    = !empty($bean->fetched_row['igc_billing_currency']) ? $bean->fetched_row['igc_billing_currency'] : '';
                $customer_billing_method = !empty($bean->fetched_row['customer_billing_method']) ? $bean->fetched_row['customer_billing_method'] : '';
                $igc_settlement_method   = !empty($bean->fetched_row['igc_settlement_method']) ? $bean->fetched_row['igc_settlement_method'] : '';
                $description             = !empty($bean->fetched_row['description']) ? $bean->fetched_row['description'] : '';
                $contract_line_item_type = !empty($bean->fetched_row['contract_line_item_type']) ? $bean->fetched_row['contract_line_item_type'] : '';
                $gc_organization_id_c    = !empty($bean->fetched_row['gc_organization_id_c']) ? $bean->fetched_row['gc_organization_id_c'] : '';

                $bean_service_start_date      = !empty($bean->service_start_date) ? $bean->service_start_date : '';
                $bean_service_end_date        = !empty($bean->service_end_date) ? $bean->service_end_date : '';
                $bean_billing_start_date      = !empty($bean->billing_start_date) ? $bean->billing_start_date : '';
                $bean_billing_end_date        = !empty($bean->billing_end_date) ? $bean->billing_end_date : '';
                $bean_gc_timezone_id_c        = !empty($bean->gc_timezone_id_c) ? $bean->gc_timezone_id_c : '';
                $bean_gc_timezone_id2_c       = !empty($bean->gc_timezone_id2_c) ? $bean->gc_timezone_id2_c : '';
                $bean_gc_timezone_id1_c       = !empty($bean->gc_timezone_id1_c) ? $bean->gc_timezone_id1_c : '';
                $bean_gc_timezone_id3_c       = !empty($bean->gc_timezone_id3_c) ? $bean->gc_timezone_id3_c : '';
                $bean_factory_reference_no    = !empty($bean->factory_reference_no) ? $bean->factory_reference_no : '';
                $bean_tech_account_id         = !empty($bean->tech_account_id) ? $bean->tech_account_id : '';
                $bean_bill_account_id         = !empty($bean->bill_account_id) ? $bean->bill_account_id : '';
                $bean_payment_type            = !empty($bean->payment_type) ? $bean->payment_type : '';
                $bean_product_specification   = !empty($bean->product_specification) ? $bean->product_specification : '';
                $bean_cust_contract_currency  = !empty($bean->cust_contract_currency) ? $bean->cust_contract_currency : '';
                $bean_cust_billing_currency   = !empty($bean->cust_billing_currency) ? $bean->cust_billing_currency : '';
                $bean_igc_contract_currency   = !empty($bean->igc_contract_currency) ? $bean->igc_contract_currency : '';
                $bean_igc_billing_currency    = !empty($bean->igc_billing_currency) ? $bean->igc_billing_currency : '';
                $bean_customer_billing_method = !empty($bean->customer_billing_method) ? $bean->customer_billing_method : '';
                $bean_igc_settlement_method   = !empty($bean->igc_settlement_method) ? $bean->igc_settlement_method : '';
                $bean_description             = !empty($bean->description) ? $bean->description : '';
                $bean_contract_line_item_type = !empty($bean->contract_line_item_type) ? $bean->contract_line_item_type : '';
                $bean_gc_organization_id_c    = !empty($bean->gc_organization_id_c) ? $bean->gc_organization_id_c : '';

                $bean_service_start_date_timezone = !empty($bean->service_start_date_timezone) ? $bean->service_start_date_timezone : '';
                $bean_service_end_date_timezone   = !empty($bean->service_end_date_timezone) ? $bean->service_end_date_timezone : '';
                $bean_billing_start_date_timezone = !empty($bean->billing_start_date_timezone) ? $bean->billing_start_date_timezone : '';
                $bean_billing_end_date_timezone   = !empty($bean->billing_end_date_timezone) ? $bean->billing_end_date_timezone : '';

                // Audit Service Start Date
                if ($bean->fetched_row['service_start_date_local'] != $bean->service_start_date_local) {
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Service Start Date', 'datetime', $bean->fetched_row['service_start_date_local'], $bean->service_start_date_local);
                }

                // Audit Service End Date
                if ($bean->fetched_row['service_end_date_local'] != $bean->service_end_date_local) {
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Service End Date', 'datetime', $bean->fetched_row['service_end_date_local'], $bean->service_end_date_local);
                }

                // Audit Billing Start Date
                if ($bean->fetched_row['billing_start_date_local'] != $bean->billing_start_date_local) {
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Billing Start Date', 'datetime', $bean->fetched_row['billing_start_date_local'], $bean->billing_start_date_local);
                }

                // Audit Billing End Date
                if ($bean->fetched_row['billing_end_date_local'] != $bean->billing_end_date_local) {
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Billing End Date', 'datetime', $bean->fetched_row['billing_end_date_local'], $bean->billing_end_date_local);
                }

                // Audit Service Start Date Time Zone
                if ($gc_timezone_id_c != $bean_gc_timezone_id_c) {
                    $obj_timezone = BeanFactory::getBean('gc_TimeZone');
                    $bean_obj_timezone = BeanFactory::getBean('gc_TimeZone');

                    if(!empty($gc_timezone_id_c)) $obj_timezone->retrieve($gc_timezone_id_c);
                    if(!empty($bean_gc_timezone_id_c)) $bean_obj_timezone->retrieve($bean_gc_timezone_id_c);
                    
                    $prev_timezone_name = (!empty($obj_timezone->name)) ? $obj_timezone->name . ' ( '.$obj_timezone->utcoffset.' ) ' : '';
                    $new_timezone_name = (!empty($bean_obj_timezone->name)) ? $bean_obj_timezone->name . ' ( '.$bean_obj_timezone->utcoffset.' ) ' : '';

                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Service Start Date Time Zone', 'text', $prev_timezone_name, $new_timezone_name);
                    unset($prev_timezone_name , $new_timezone_name);
                }

                // Audit Service End Date Time Zone
                if ($gc_timezone_id1_c != $bean_gc_timezone_id1_c) {
                    $obj_timezone1 = BeanFactory::getBean('gc_TimeZone');
                    $bean_obj_timezone1 = BeanFactory::getBean('gc_TimeZone');

                    if(!empty($gc_timezone_id1_c)) $obj_timezone1->retrieve($gc_timezone_id1_c);
                    if(!empty($bean_gc_timezone_id1_c)) $bean_obj_timezone1->retrieve($bean_gc_timezone_id1_c);

                    $prev_timezone_name = (!empty($obj_timezone1->name)) ? $obj_timezone1->name . ' ( '.$obj_timezone1->utcoffset.' ) ' : '';
                    $new_timezone_name = (!empty($bean_obj_timezone1->name)) ? $bean_obj_timezone1->name . ' ( '.$bean_obj_timezone1->utcoffset.' ) ' : '';
                    
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Service End Date Time Zone', 'text', $prev_timezone_name , $new_timezone_name);
                    unset($prev_timezone_name , $new_timezone_name);
                }

                // Audit Billing Start Date Time Zone
                if ($gc_timezone_id2_c != $bean_gc_timezone_id2_c) {
                    $obj_timezone2 = BeanFactory::getBean('gc_TimeZone');
                    $bean_obj_timezone2 = BeanFactory::getBean('gc_TimeZone');

                    if(!empty($gc_timezone_id2_c)) $obj_timezone2->retrieve($gc_timezone_id2_c);
                    if(!empty($bean_gc_timezone_id2_c)) $bean_obj_timezone2->retrieve($bean_gc_timezone_id2_c);

                    $prev_timezone_name = (!empty($obj_timezone2->name)) ? $obj_timezone2->name . ' ( '.$obj_timezone2->utcoffset.' ) ' : '';
                    $new_timezone_name = (!empty($bean_obj_timezone2->name)) ? $bean_obj_timezone2->name . ' ( '.$bean_obj_timezone2->utcoffset.' ) ' : '';

                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Billing Start Date Time Zone', 'text', $prev_timezone_name , $new_timezone_name);
                    unset($prev_timezone_name , $new_timezone_name);
                }

                // Audit Billing End Date Time Zone
                if ($gc_timezone_id3_c != $bean_gc_timezone_id3_c) {
                    $obj_timezone3 = BeanFactory::getBean('gc_TimeZone');
                    $bean_obj_timezone3 = BeanFactory::getBean('gc_TimeZone');

                    if(!empty($gc_timezone_id3_c)) $obj_timezone3->retrieve($gc_timezone_id3_c);
                    if(!empty($bean_gc_timezone_id3_c)) $bean_obj_timezone3->retrieve($bean_gc_timezone_id3_c);

                    $prev_timezone_name = (!empty($obj_timezone3->name)) ? $obj_timezone3->name . ' ( '.$obj_timezone3->utcoffset.' ) ' : '';
                    $new_timezone_name = (!empty($bean_obj_timezone3->name)) ? $bean_obj_timezone3->name . ' ( '.$bean_obj_timezone3->utcoffset.' ) ' : '';

                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Billing End Date Time Zone', 'text', $prev_timezone_name , $new_timezone_name);
                    unset($prev_timezone_name , $new_timezone_name);
                }

                // Audit Factory Reference Number
                if ($factory_reference_no != $bean_factory_reference_no) {
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Factory Reference Number', 'text', $factory_reference_no, $bean_factory_reference_no);
                }

                // Audit Tech Account ID
                if ($tech_account_id != $bean_tech_account_id) {
                    $before_tech_acc_name = $this->getAccountNameByRecordID($tech_account_id);
                    $after_tech_acc_name  = $this->getAccountNameByRecordID($bean_tech_account_id);
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Tech Account ID', 'text', $before_tech_acc_name, $after_tech_acc_name);
                }

                // Audit Billing Account ID
                if ($bill_account_id != $bean_bill_account_id) {
                    $before_billing_acc_name = $this->getAccountNameByRecordID($bill_account_id);
                    $after_billing_acc_name  = $this->getAccountNameByRecordID($bean_bill_account_id);
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Billing Account ID', 'text', $before_billing_acc_name, $after_billing_acc_name);
                }

                // Audit Payment Type
                if ($payment_type != $bean_payment_type) {
                    global $app_list_strings;
                    $before_payment_type = $app_list_strings['payment_type_list'][$payment_type];
                    $after_payment_type  = $app_list_strings['payment_type_list'][$bean_payment_type];
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Payment Type', 'text', $before_payment_type, $after_payment_type);
                }

                // Audit Product Specification
                if ($product_specification != $bean_product_specification) {
                    include_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';
                    $obj_product_config = new ClassProductConfiguration();

                    $gc_Contracts_bean        = BeanFactory::getBean('gc_Contracts');
                    $gc_Contracts_bean_result = $gc_Contracts_bean->retrieve($bean->contracts_id);

                    $obj_product_config_result = $obj_product_config->getProductOfferingDetails($gc_Contracts_bean_result->product_offering);
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Product Specification', 'text', $obj_product_config_result['respose_array']['specification_details'][$product_specification]['specname'], $obj_product_config_result['respose_array']['specification_details'][$bean_product_specification]['specname']);
                }

                include_once 'custom/modules/gc_Contracts/include/ClassGcContracts.php';
                $obj_gc_contracts  = new ClassGcContracts();
                $config_currencies = $obj_gc_contracts->getconfig_currencies();

                // Audit Customer Contract Currency
                if ($cust_contract_currency != $bean_cust_contract_currency && !empty($obj_line_item_bean)) {
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Customer Contract Currency', 'text', $config_currencies[$cust_contract_currency], $config_currencies[$bean_cust_contract_currency]);
                }

                // Audit Customer Billing Currency
                if ($cust_billing_currency != $bean_cust_billing_currency && !empty($obj_line_item_bean)) {
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Customer Billing Currency', 'text', $config_currencies[$cust_billing_currency], $config_currencies[$bean_cust_billing_currency]);
                }

                // Audit Inter-Group-Company Contract Currency
                if ($igc_contract_currency != $bean_igc_contract_currency && !empty($obj_line_item_bean)) {
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Inter-Group-Company Contract Currency', 'text', $config_currencies[$igc_contract_currency], $config_currencies[$bean_igc_contract_currency]);
                }

                // Audit Inter-Group-Company Billing Currency
                if ($igc_billing_currency != $bean_igc_billing_currency && !empty($obj_line_item_bean)) {
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Inter-Group-Company Billing Currency', 'text', $config_currencies[$igc_billing_currency], $config_currencies[$bean_igc_billing_currency]);
                }

                // Audit Customer Billing Method
                if ($customer_billing_method != $bean_customer_billing_method) {
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Customer Billing Method', 'text', $customer_billing_method, $bean_customer_billing_method);
                }

                // Audit Inter-Group-Company Settlement Method
                if ($igc_settlement_method != $bean_igc_settlement_method) {
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Inter-Group-Company Settlement Method', 'text', $igc_settlement_method, $bean_igc_settlement_method);
                }

                // Audit Remarks
                if ($description != $bean_description) {
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Remarks', 'text', $description, $bean_description);
                }

                // Audit Contract Line Item Type
                if ($contract_line_item_type != $bean_contract_line_item_type) {
                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Contract Line Item Type', 'text', $contract_line_item_type, $bean_contract_line_item_type);
                }

                // Audit Billing Affiliate
                if ($gc_organization_id_c != $bean_gc_organization_id_c) {
                    $before_organization_name = $this->getOrganizationByID($gc_organization_id_c);
                    $after_organization_name  = $this->getOrganizationByID($bean_gc_organization_id_c);

                    $this->insertLineItemHistoryAudit($obj_line_item_bean, 'Billing Affiliate', 'text', $before_organization_name, $after_organization_name);
                }
            }
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

     /**
     * method insertLineItemHistoryAudit to save Line Item Contract History Audit record.
     *
     * @param object $bean record sugar object [Sugar default]
     * @param string $field_name field name to audit
     * @param string $data_type date type of field
     * @param string $before value before update
     * @param string $after value after update
     * @author dinesh.itkar
     * @since Jan 05, 2016
     */
    function insertLineItemHistoryAudit($bean, $field_name, $data_type, $before, $after)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'field_name : '.$field_name.GCM_GL_LOG_VAR_SEPARATOR.'data_type : '.$data_type.GCM_GL_LOG_VAR_SEPARATOR.'before : '.$before.GCM_GL_LOG_VAR_SEPARATOR.'after : '.$after, 'to save Line Item Contract History Audit record');
        $audit_entry = array();
        $audit_entry['field_name'] = $field_name;
        $audit_entry['data_type'] = $data_type;
        $audit_entry['before'] = $before;
        $audit_entry['after'] = $after;
        $bean->db->save_audit_records($bean, $audit_entry);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

     /**
     * method getAccountNameByRecordID to get account name by account id.
     *
     * @param string $account_id account id
     * @return string $account_name account name
     * @author dinesh.itkar
	 * @since Jan 07, 2016
     */
    private function getAccountNameByRecordID($account_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'account_id : '.$account_id, 'to get account name by account id');
        $account_name = "";
        if (!empty($account_id)) {
            $account_bean = BeanFactory::getBean('Contacts');
            $account_name_bean = $account_bean->retrieve($account_id);

            if (!empty($account_name_bean->name)) {
                $account_name = trim($account_name_bean->name);
            } elseif (!empty($account_name_bean->name_2_c)) {
                    $account_name = $account_name_bean->name_2_c;
            }

            unset($account_bean, $account_name_bean);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'account_name : '.$account_name, '');
        return $account_name;
    }

    /**
     * method getOrganizationByID to get organization name by organization id.
     *
     * @param string $organization_id organization id
     * @return string $organization_name organization name
     * @author dinesh.itkar
	 * @since Nov 08, 2016
     */
    private function getOrganizationByID($organization_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'organization_id : '.$organization_id, 'to get organization name by organization id');
        $organization_name = "";
        if (!empty($organization_id)) {
            $gc_organization_bean = BeanFactory::getBean('gc_organization');
            $gc_organization_bean_result = $gc_organization_bean->retrieve($organization_id);

            $org_name_1 = "";
            $org_name_2 = "";
            $org_name_3 = "";
            $org_name_4 = "";
            if (!empty($gc_organization_bean_result->name))
                $org_name_1 = $gc_organization_bean_result->name;
            if (!empty($gc_organization_bean_result->organization_name_2_local))
                $org_name_2 = $gc_organization_bean_result->organization_name_2_local;
            if (!empty($gc_organization_bean_result->organization_name_3_local))
                $org_name_3 = $gc_organization_bean_result->organization_name_3_local;
            if (!empty($gc_organization_bean_result->organization_name_4_local))
                $org_name_4 = $gc_organization_bean_result->organization_name_4_local;

            $org_name_ar = array($org_name_1,$org_name_2,$org_name_3,$org_name_4);
            $organization_name = implode(" ",$org_name_ar);
            unset($gc_organization_bean, $gc_organization_bean_result);
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'organization_name : '.$organization_name, '');
        return $organization_name;
    }
}

?>