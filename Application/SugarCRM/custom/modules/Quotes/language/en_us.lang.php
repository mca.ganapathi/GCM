<?php
// created: 2015-09-08 12:57:43
$mod_strings = array (
  'LBL_SHIPPING_ACCOUNT_NAME' => 'Shipping Corporate Name:',
  'LBL_SHIPPING_CONTACT_NAME' => 'Shipping Account Name:',
  'LBL_SHIPPING_CONTACT_ID' => 'Shipping Account Id:',
  'LBL_ACCOUNT_NAME' => 'Corporate Name:',
  'LBL_BILLING_ACCOUNT_NAME' => 'Billing Corporate Name:',
  'LBL_BILLING_CONTACT_NAME' => 'Billing Account Name:',
  'LBL_BILLING_CONTACT_ID' => 'Billing Account Id:',
  'LBL_SHIP_TO_ACCOUNT' => 'Ship to Corporate',
  'LBL_BILL_TO_ACCOUNT' => 'Bill to Corporate',
  'LBL_SHIP_TO_CONTACT' => 'Ship to Account',
  'LBL_BILL_TO_CONTACT' => 'Bill to Account',
  'LBL_ACCOUNT_ID' => 'Corporate Id',
  'LBL_LIST_ACCOUNT_NAME' => 'Corporate Name',
);