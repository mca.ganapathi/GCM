<?php
// created: 2015-09-15 11:26:01
$mapping = array (
  'beans' => 
  array (
    'Accounts' => 
    array (
      'name' => 'name',
      'id' => 'id',
    ),
    'Contacts' => 
    array (
      'name' => 'full_name',
      'id' => 'id',
    ),
    'Leads' => 
    array (
      'name' => 'account_name',
      'id' => 'id',
    ),
    'Prospects' => 
    array (
      'name' => 'account_name',
      'id' => 'id',
    ),
  ),
);