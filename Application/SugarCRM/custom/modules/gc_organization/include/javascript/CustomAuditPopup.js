/** 
    @desc : Functions related to open view change log popup
    @file  : custom/modules/gc_Contracts/include/javascript/CustomAuditPopup.js 
    @Author : Mohamed Abubakkar Siddiq.L
*/
var myWindow;
var myTimeOut;
var mylineitemId;
var mySoLineNo;
var mydocTypFlg;

 
 /**
  * : Function to open Organization Custom Audit Log
  * 
  * @param :
  *            <string> module_name - Sugar Module Name <string> width - width
  *            for the popup window <string> height - height for the popup window
  * 
  * @return : <object> win Pop-up window object
  * @author : Mohamed.Siddiq
  * @date : 27-Dec-2016
  */
 function auditLog(module_name, width, height, contract_id) {
 	if (typeof (popupAuditCount) == "undefined" || popupAuditCount == 0)
 		popupAuditCount = 1;
 	window.document.close_popup = true;
 	width = (width == 600) ? 900 : width;
 	height = (height == 400) ? 900 : height;
 	URL = 'index.php?' + 'module=Audit' + '&module_name=' + module_name
 			+ '&action=Custom_Popup_Picker&query=true&record=' + contract_id + '&create=false&sugar_body_only=true';
 	windowName = module_name + '_popup_window' + popupAuditCount;
 	windowFeatures = 'width=' + width + ',height=' + height
 			+ ',resizable=1,scrollbars=1';
 	popup_mode = 'single';
 	URL += '&mode=' + popup_mode;
 	win = SUGAR.util.openWindow(URL, windowName, windowFeatures);
 	if (window.focus) {
 		win.focus();
 	}
 	win.popupAuditCount = popupAuditCount;
 	return win;
 }