<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

class ClassOrganizationBeforeSave
{

    /**
     * method to save Organization logs 
     * @author Kamal.Thiyagarajan
     * @param
     *        <object>$bean - record sugar object [Sugar default]
     * @param
     *        <object>$event - hook event [Sugar default]
     * @param
     *        <array>$arguments - required parameters passed by framework [Sugar default]
     * @return (void method)
     *         @date 07-Dec-2016
     */
    
    function setOrganizationAuditEntry($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'method to save Organization logs');
        
        // Avoid audit log for new record.
        if (empty($bean->fetched_row['id'])) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
            return;
        }
        $bean_sales_org_flag = ((!empty($bean->sales_org_flag))?$bean->sales_org_flag:"0");
        $bean_fetched_sales_org_flag = ((!empty($bean->fetched_row['sales_org_flag']))?$bean->fetched_row['sales_org_flag']:"0");
        $bean_billing_org_flag = ((!empty($bean->billing_org_flag))?$bean->billing_org_flag:"0");
        $bean_fetched_billing_org_flag = ((!empty($bean->fetched_row['billing_org_flag']))?$bean->fetched_row['billing_org_flag']:"0");
        $bean_service_org_flag = ((!empty($bean->service_org_flag))?$bean->service_org_flag:"0");
        $bean_fetched_service_org_flag = ((!empty($bean->fetched_row['service_org_flag']))?$bean->fetched_row['service_org_flag']:"0");        
        
        if ($bean_sales_org_flag != $bean_fetched_sales_org_flag) {
        	$after_sales_value = (trim($bean_sales_org_flag) == "")?"0":$bean_sales_org_flag;
        	$this->insertOrganizationAudit($bean, 'Sales Org Flag', 'text', $bean_fetched_sales_org_flag, $after_sales_value);	
        }
        
        if ($bean_billing_org_flag != $bean_fetched_billing_org_flag) {
        	$after_billing_value = (trim($bean_billing_org_flag) == "")?"0":$bean_billing_org_flag;
        	$this->insertOrganizationAudit($bean, 'Billing Org Flag', 'text', $bean_fetched_billing_org_flag, $after_billing_value);    	
        } 
        
        if ($bean_service_org_flag != $bean_fetched_service_org_flag) {
        	$after_service_value =  (trim($bean_service_org_flag) == "")?"0":$bean_service_org_flag;
        	$this->insertOrganizationAudit($bean, 'Service Org Flag', 'text', $bean_fetched_service_org_flag, $after_service_value);   	
        }
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
    
    /**
     * method to Update Organization Audit record.
     * @author Kamal
     * @param
     *        (object)$bean - record sugar object [Sugar default]
     *        (string)$field_name - field name to audit
     *        (string)$data_type - date type of field
     *        (string)$before - value before update
     *        (string)$after - value after update
     * @return (void method)
     *         @date : 07-Dec-2016
     */
    private function insertOrganizationAudit($bean, $field_name, $data_type, $before, $after)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'field_name : '.$field_name.GCM_GL_LOG_VAR_SEPARATOR.'data_type : '.$data_type.GCM_GL_LOG_VAR_SEPARATOR.'before : '.$before.GCM_GL_LOG_VAR_SEPARATOR.'after : '.$after, 'to Update Organization Audit record');
    	$audit_entry = array();
    	$audit_entry['field_name'] = $field_name;
    	$audit_entry['data_type'] = $data_type;
    	$audit_entry['before'] = $before;
    	$audit_entry['after'] = $after;
    	$bean->db->save_audit_records($bean, $audit_entry);
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    
}

?>
