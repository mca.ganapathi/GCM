<?php
$popupMeta = array (
    'moduleMain' => 'gc_organization',
    'varName' => 'gc_organization',
    'orderBy' => 'gc_organization.name',
    'whereClauses' => array (
  'name' => 'gc_organization.name',
),
    'searchInputs' => array (
  0 => 'gc_organization_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'ORGANIZATION_NAME_2_LOCAL' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ORGANIZATION_NAME_2_LOCAL',
    'width' => '10%',
    'default' => true,
    'name' => 'organization_name_2_local',
  ),
  'ORGANIZATION_NAME_3_LOCAL' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ORGANIZATION_NAME_3_LOCAL',
    'width' => '10%',
    'default' => true,
    'name' => 'organization_name_3_local',
  ),
  'ORGANIZATION_NAME_4_LOCAL' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ORGANIZATION_NAME_4_LOCAL',
    'width' => '10%',
    'default' => true,
    'name' => 'organization_name_4_local',
  ),
  'ORGANIZATION_CODE_1' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ORGANIZATION_CODE_1',
    'width' => '10%',
    'default' => true,
    'name' => 'organization_code_1',
  ),
  'ORGANIZATION_CODE_2' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ORGANIZATION_CODE_2',
    'width' => '10%',
    'default' => true,
    'name' => 'organization_code_2',
  ),
  'SALES_ORG_FLAG' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_SALES_ORG_FLAG',
    'width' => '10%',
    'name' => 'sales_org_flag',
  ),
  'BILLING_ORG_FLAG' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_BILLING_ORG_FLAG',
    'width' => '10%',
    'name' => 'billing_org_flag',
  ),
  'SERVICE_ORG_FLAG' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_SERVICE_ORG_FLAG',
    'width' => '10%',
    'name' => 'service_org_flag',
  ),
),
);
