<?php
$module_name = 'gc_organization';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          2 => 'DELETE',
          3 => array (
            'customCode' => '<input type="button" id="btn_custom_view_change_log" name="btn_custom_view_change_log"  onclick="auditLog( \'gc_organization\', 900, 900, \'{$fields.id.value}\');" value="View Change Log"/>',
          ),
        ),
        'hideAudit' => true,
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' =>array (
        0 =>array (
          'file' => 'custom/modules/gc_organization/include/javascript/CustomAuditPopup.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'organization_name_1_latin',
            'label' => 'LBL_ORGANIZATION_NAME_1_LATIN',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'organization_name_2_local',
            'label' => 'LBL_ORGANIZATION_NAME_2_LOCAL',
          ),
          1 => 
          array (
            'name' => 'organization_name_2_latin',
            'label' => 'LBL_ORGANIZATION_NAME_2_LATIN',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'organization_name_3_local',
            'label' => 'LBL_ORGANIZATION_NAME_3_LOCAL',
          ),
          1 => 
          array (
            'name' => 'organization_name_3_latin',
            'label' => 'LBL_ORGANIZATION_NAME_3_LATIN',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'organization_name_4_local',
            'label' => 'LBL_ORGANIZATION_NAME_4_LOCAL',
          ),
          1 => 
          array (
            'name' => 'organization_name_4_latin',
            'label' => 'LBL_ORGANIZATION_NAME_4_LATIN',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'organization_code_1',
            'label' => 'LBL_ORGANIZATION_CODE_1',
          ),
          1 => 
          array (
            'name' => 'organization_code_2',
            'label' => 'LBL_ORGANIZATION_CODE_2',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'sales_org_flag',
            'label' => 'LBL_SALES_ORG_FLAG',
          ),
          1 => 
          array (
            'name' => 'billing_org_flag',
            'label' => 'LBL_BILLING_ORG_FLAG',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'service_org_flag',
            'label' => 'LBL_SERVICE_ORG_FLAG',
          ),
          1 => 
          array (
            'name' => 'billing_affiliate_code',
            'label' => 'LBL_BILLING_AFFILIATE_CODE',
          ),
        ),
      ),
    ),
  ),
);
?>