<?php
$module_name = 'gc_organization';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'organization_name_2_local' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ORGANIZATION_NAME_2_LOCAL',
        'width' => '10%',
        'default' => true,
        'name' => 'organization_name_2_local',
      ),
      'organization_name_3_local' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ORGANIZATION_NAME_3_LOCAL',
        'width' => '10%',
        'default' => true,
        'name' => 'organization_name_3_local',
      ),
      'organization_name_4_local' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ORGANIZATION_NAME_4_LOCAL',
        'width' => '10%',
        'default' => true,
        'name' => 'organization_name_4_local',
      ),
      'sales_org_flag' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_SALES_ORG_FLAG',
        'width' => '10%',
        'name' => 'sales_org_flag',
      ),
      'billing_org_flag' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_BILLING_ORG_FLAG',
        'width' => '10%',
        'name' => 'billing_org_flag',
      ),
      'service_org_flag' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_SERVICE_ORG_FLAG',
        'width' => '10%',
        'name' => 'service_org_flag',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'organization_name_2_local' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ORGANIZATION_NAME_2_LOCAL',
        'width' => '10%',
        'default' => true,
        'name' => 'organization_name_2_local',
      ),
      'organization_name_3_local' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ORGANIZATION_NAME_3_LOCAL',
        'width' => '10%',
        'default' => true,
        'name' => 'organization_name_3_local',
      ),
      'organization_name_4_local' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ORGANIZATION_NAME_4_LOCAL',
        'width' => '10%',
        'default' => true,
        'name' => 'organization_name_4_local',
      ),
      'organization_code_1' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ORGANIZATION_CODE_1',
        'width' => '10%',
        'default' => true,
        'name' => 'organization_code_1',
      ),
      'organization_code_2' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ORGANIZATION_CODE_2',
        'width' => '10%',
        'default' => true,
        'name' => 'organization_code_2',
      ),
      'sales_org_flag' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_SALES_ORG_FLAG',
        'width' => '10%',
        'name' => 'sales_org_flag',
      ),
      'billing_org_flag' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_BILLING_ORG_FLAG',
        'width' => '10%',
        'name' => 'billing_org_flag',
      ),
      'service_org_flag' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_SERVICE_ORG_FLAG',
        'width' => '10%',
        'name' => 'service_org_flag',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>