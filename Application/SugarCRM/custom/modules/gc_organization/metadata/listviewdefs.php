<?php
// created: 2018-03-07 11:45:26
$listViewDefs['gc_organization'] = array (
  'NAME' => 
  array (
    'width' => '32',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'ORGANIZATION_NAME_2_LOCAL' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ORGANIZATION_NAME_2_LOCAL',
    'width' => '10',
    'default' => true,
  ),
  'ORGANIZATION_NAME_3_LOCAL' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ORGANIZATION_NAME_3_LOCAL',
    'width' => '10',
    'default' => true,
  ),
  'ORGANIZATION_NAME_4_LOCAL' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ORGANIZATION_NAME_4_LOCAL',
    'width' => '10',
    'default' => true,
  ),
  'ORGANIZATION_CODE_1' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ORGANIZATION_CODE_1',
    'width' => '10',
    'default' => true,
  ),
  'ORGANIZATION_CODE_2' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ORGANIZATION_CODE_2',
    'width' => '10',
    'default' => true,
  ),
  'SALES_ORG_FLAG' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_SALES_ORG_FLAG',
    'width' => '10',
  ),
  'BILLING_ORG_FLAG' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_BILLING_ORG_FLAG',
    'width' => '10',
  ),
  'SERVICE_ORG_FLAG' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_SERVICE_ORG_FLAG',
    'width' => '10',
  ),
  'TEAM_NAME' => 
  array (
    'width' => '9',
    'label' => 'LBL_TEAM',
    'default' => false,
  ),
);