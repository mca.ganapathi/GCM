<?php
// Do not store anything in this file that is not part of the array or the hook version. This file will
// be automatically rebuilt in the future.
$hook_version = 1;
$hook_array = Array();
// position, file, function
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(
    1,
    'Create Audit Logs under Line Item Module',
    'custom/modules/gc_ProductRule/include/ClassBeforeSave.php',
    'ClassProductRuleBeforeSave',
    'setProductRuleAuditEntry');
$hook_array['before_save'][] = Array(
    2,
    'Set UUIDs in field specific uuid columns',
    'custom/include/ClassGenericLogicHook.php',
    'ClassGenericLogicHook',
    'setModuleFieldUUIDs');
?>