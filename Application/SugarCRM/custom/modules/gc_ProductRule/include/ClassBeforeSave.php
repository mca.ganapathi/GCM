<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * class ClassProductRuleBeforeSave
 */
class ClassProductRuleBeforeSave
{

    /**
     * Method setProductRuleAuditEntry to Save Audit Logs under Line Item Module
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author dinesh.itkar
     * @since Apr 16, 2016
     */
    function setProductRuleAuditEntry($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'Method to Save Audit Logs under Line Item Module');
        
        // Avoid audit log for new record.
        if (empty($bean->fetched_row['id'])) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'', '');
            return;
        }
        
        // Audit Product Rule Value
        if ($bean->description != $bean->fetched_row['description']) {
            
            $obj_line_item_bean = BeanFactory::getBean('gc_LineItem');
            $obj_line_item_bean->retrieve($bean->line_item_id);
            
            if (!empty($obj_line_item_bean)) {
                $obj_line_item_contract_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
                $obj_line_item_contract_history_bean->retrieve_by_string_fields(array(
                    'line_item_id' => $bean->line_item_id
                ));
                
                $obj_gc_contracts_bean = BeanFactory::getBean('gc_Contracts');
                $obj_gc_contracts_bean->retrieve($obj_line_item_contract_history_bean->contracts_id);
                
                include_once ('custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php');
                $obj_product_config = new ClassProductConfiguration();
                $obj_product_config_result = $obj_product_config->getProductOfferingDetails($obj_gc_contracts_bean->product_offering);
                
                $rule_value = $obj_product_config_result['respose_array']['specification_details'][$obj_line_item_contract_history_bean->product_specification]['rules'][$bean->name]['rulename'];
                $this->insertProductRuleAudit($obj_line_item_bean, $rule_value, 'text', $bean->fetched_row['description'], $bean->description);                
            }
        }
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'', '');
    }

    /**
     * Method insertProductRuleAudit to save Line Item Product Rule Audit record.
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param string $field_name field name to audit
     * @param string $data_type data type of field
     * @param string $before value before update
     * @param string $after value after update
     * @author dinesh.itkar
     * @since Apr 16, 2016
     */
    private function insertProductRuleAudit($bean, $field_name, $data_type, $before, $after)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'field_name : '.$field_name.GCM_GL_LOG_VAR_SEPARATOR.'data_type : '.$data_type.GCM_GL_LOG_VAR_SEPARATOR.'before : '.$before.GCM_GL_LOG_VAR_SEPARATOR.'after : '.$after, 'Method to save Line Item Product Rule Audit record');
        $audit_entry = array();
        $audit_entry['field_name'] = $field_name;
        $audit_entry['data_type'] = $data_type;
        $audit_entry['before'] = $before;
        $audit_entry['after'] = $after;
        $bean->db->save_audit_records($bean, $audit_entry);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'', '');
    }
}

?>