<?php
$dashletData['gc_ContractsDashlet']['searchFields'] = array (
  'product_offering' => 
  array (
    'default' => '',
  ),
  'accounts_gc_contracts_1_name' => 
  array (
    'default' => '',
  ),
  'contract_status' => 
  array (
    'default' => '',
  ),
  'workflow_flag' => 
  array (
    'default' => '',
  ),
  'team_name' => 
  array (
    'default' => '',
  ),
);
$dashletData['gc_ContractsDashlet']['columns'] = array (
  'global_contract_id' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_GLOBAL_CONTRACT_ID',
    'width' => '10%',
    'default' => true,
    'name' => 'global_contract_id',
  ),
  'name' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'product_offering' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PRODUCT_OFFERING',
    'width' => '10%',
    'default' => true,
    'name' => 'product_offering',
  ),
  'accounts_gc_contracts_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_GC_CONTRACTS_1ACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'accounts_gc_contracts_1_name',
  ),
  'contract_version' => 
  array (
    'type' => 'decimal',
    'default' => false,
    'label' => 'LBL_CONTRACT_VERSION',
    'width' => '10%',
  ),
  'contract_status' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_CONTRACT_STATUS',
    'width' => '10%',
  ),
  'workflow_flag' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_WORKFLOW_FLAG',
    'width' => '10%',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => false,
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'modified_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'team_name' => 
  array (
    'width' => '15%',
    'label' => 'LBL_LIST_TEAM',
    'name' => 'team_name',
    'default' => false,
  ),
);
