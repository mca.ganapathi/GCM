<?php
// created: 2016-08-04 08:42:16
$subpanel_layout['list_fields'] = array (
  'global_contract_id' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_GLOBAL_CONTRACT_ID',
    'width' => '16%',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '28%',
    'default' => true,
  ),
  'contract_version' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_CONTRACT_VERSION',
    'width' => '11%',
  ),
  'contract_status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_CONTRACT_STATUS',
    'width' => '16%',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '14%',
    'default' => true,
  ),
  'ext_sys_created_by' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_EXT_SYS_CREATED_BY',
    'width' => '15%',
    'default' => true,
  ),
);
