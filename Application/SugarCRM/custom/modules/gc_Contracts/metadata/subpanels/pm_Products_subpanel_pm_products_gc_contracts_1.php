<?php
// created: 2015-12-21 12:12:00
$subpanel_layout['list_fields'] = array (
  'global_contract_id' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_GLOBAL_CONTRACT_ID',
    'width' => '15%',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '25%',
    'default' => true,
  ),
  'contract_version' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_CONTRACT_VERSION',
    'width' => '10%',
    'default' => true,
  ),
  'contract_status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_CONTRACT_STATUS',
    'width' => '10%',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '20%',
    'default' => true,
  ),
  'ext_sys_created_by' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_EXT_SYS_CREATED_BY',
    'width' => '20%',
    'default' => true,
  ),
);