<?php
$module_name = 'gc_Contracts';
$viewdefs [$module_name] =
array (
  'DetailView' =>
  array (
    'templateMeta' =>
    array (
      'form' =>
      array (
        'hidden' =>
        array (
          0 => '<input type="hidden" name="comment" id="comment" value=""/>',
          1 => '<input type="hidden" name="sugar_request" id="sugar_request" value="1">',
        ),
        'buttons' =>
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
          5 =>
          array (
            'customCode' => '<input type="button" value="Send For Approval" name="button" onclick="performAction(\'SendForApproval\')" class="button" accesskey="S" title="Send for Approval [Alt+S]" id="sendforapproval" />',
          ),
		  16 =>
			array (
			'customCode' => '<input type="button" value="Withdraw Approval Request" name="button" onclick="performAction(\'WithdrawApprovalRequest\')" class="button" accesskey="A" title="Withdraw Approval Request [Alt+W]" id="withdrawapprovalrequest" />',
		  ),	
		  6 =>
          array (
            'customCode' => '<input type="button" value="Approve" name="button" onclick="performAction(\'ApproveContract\')" class="button" accesskey="A" title="Approve Contract [Alt+A]" id="approvecontract" />',
          ),
          7 =>
          array (
            'customCode' => '<input type="button" value="Reject" name="button" onclick="performAction(\'RejectContract\')" class="button" accesskey="R" title="Reject Contract [Alt+A]" id="rejectContract" />',
          ),
          10 =>
          array (
            'customCode' => '<input type="button" id="activate_contract" name="button"  onclick="performActivation( \'ActivateContract\')" class="button" value="Activate"/>',
          ),
          11 =>
          array (
            'customCode' => '<input type="button" id="modification_contract" name="button"  onclick="performModification( \'{$fields.id.value}\')" class="button" value="Modification"/>',
          ),
          12 =>
          array (
            'customCode' => '<input type="button" value="Termination Approve" name="button" onclick="performAction(\'ApproveTerminationApproval\')" class="button" accesskey="A" title="Approve [Alt+A]" id="termination_approval" />',
          ),
          13 =>
          array (
            'customCode' => '<input type="button" value="Termination Reject" name="button" onclick="performAction(\'RejectTerminationApproval\')" class="button" accesskey="X" title="Reject [Alt+X]" id="termination_reject" />',
          ),
          14 =>
          array (
             'customCode' => '<input type="button" value="Deactivate" name="button" onclick="performDeactivation(\'Deactivate\')" class="button" accesskey="D" title="Deactivate [Alt+D]" id="deactivate" />',
          ),
          15 =>
          array (
             'customCode' => '<input type="button" value="Termination" name="button" onclick="performAction(\'SendTerminationApproval\')" class="button" accesskey="T" title="Termination [Alt+T]" id="send_termination_approval" />',
          ),
		  17 =>
		  array (
			'customCode' => '<input type="button" value="Withdraw Termination Approval" name="button" onclick="performAction(\'WithdrawTerminationApproval\')" class="button" accesskey="T" title="Withdraw Termination [Alt+T]" id="withdraw_termination_approval" />',
		  ),
		  8 =>
          array (
            'customCode' => '<input type="button" onclick="commentsLog( \'gc_Contracts\', 900, 900, \'{$fields.id.value}\');" value="View Approvals Log"/>',
          ),
          9 =>
          array (
            'customCode' => '<input type="button" id="btn_custom_view_change_log" name="btn_custom_view_change_log"  onclick="auditLog( \'gc_Contracts\', 900, 900, \'{$fields.id.value}\');" value="View Change Log"/>',
          ),
        ),
      ),
      'maxColumns' => '2',
      'widths' =>
      array (
        0 =>
        array (
          'label' => '20',
          'field' => '30',
        ),
        1 =>
        array (
          'label' => '20',
          'field' => '30',
        ),
      ),
      'includes' =>
      array (
        0 =>
        array (
          'file' => 'cache/include/javascript/sugar_grp_yui_widgets.js',
        ),
        1 =>
        array (
          'file' => 'custom/include/assests/js/bootstrap.min.js',
        ),
        2 =>
        array (
          'file' => 'custom/include/assests/js/CustomJS.js',
        ),
        3 =>
        array (
          'file' => 'custom/modules/gc_Contracts/include/javascript/DetailView.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' =>
      array (
        'DEFAULT' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL3' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL1' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL2' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' =>
    array (
      'default' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'global_contract_id',
            'label' => 'LBL_GLOBAL_CONTRACT_ID',
          ),
          1 => 'global_contract_id_uuid',
        ),
        1 =>
        array (
          0 => 'name',
          1 =>
          array (
            'name' => 'contract_version',
            'label' => 'LBL_CONTRACT_VERSION',
          ),
        ),
        2 =>
        array (
          0 =>
          array (
            'name' => 'contract_status',
            'studio' => 'visible',
            'label' => 'LBL_CONTRACT_STATUS',
          ),
          1 =>
          array (
            'name' => 'loi_contract',
            'studio' => 'visible',
            'label' => 'LBL_LOI_CONTRACT',
          ),
        ),
        3 =>
        array (
          0 => 'description',
        ),
        4 =>
        array (
          0 =>
          array (
            'name' => 'workflow_flag',
            'studio' => 'visible',
            'label' => 'LBL_WORKFLOW_FLAG',
          ),
          1 => 'team_name',
        ),
        5 =>
        array (
          0 =>
          array (
            'name' => 'contract_type',
            'studio' => 'visible',
            'label' => 'LBL_CONTRACT_TYPE',
          ),
          1 =>
          array (
            'name' => 'contract_scheme',
            'studio' => 'visible',
            'label' => 'LBL_CONTRACT_SCHEME',
          ),
        ),
        6 =>
        array (
          0 =>
          array (
            'name' => 'product_offering',
            'label' => 'LBL_PRODUCT_OFFERING',
          ),
          1 =>
          array (
            'name' => 'billing_type',
            'studio' => 'visible',
            'label' => 'LBL_BILLING_TYPE',
          ),
        ),
        7 =>
        array (
          0 =>
          array (
            'name' => 'accounts_gc_contracts_1_name',
            'label' => 'LBL_CONTRACT_CUST_COMPANY',
          ),
          1 =>
          array (
            'name' => 'contacts_gc_contracts_1_name',
            'label' => 'LBL_CONTRACT_ACCOUNT_ID',
          ),
        ),
        8 =>
        array (
          0 =>
          array (
            'name' => 'contract_startdate',
            'customCode' => '{$fields.contract_startdate.value} {$fields.contract_startdate_timezone.value}',
            'label' => 'LBL_CONTRACT_STARTDATE',
          ),
          1 =>
          array (
            'name' => 'contract_enddate',
            'customCode' => '{$fields.contract_enddate.value} {$fields.contract_enddate_timezone.value}',
            'label' => 'LBL_CONTRACT_ENDDATE',
          ),
        ),
        9 =>
        array (
          0 =>
          array (
            'name' => 'factory_reference_no',
            'label' => 'LBL_FACTORY_REFERENCE_NO',
          ),
          1 => 
          array (
            'name' => 'vat_no',
            'label' => 'LBL_VAT_NO',
          ),
        ),
        10 =>
        array (
          0 =>
          array (
            'name' => 'date_entered',
            'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 =>
          array (
            'name' => 'date_modified',
            'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
            'label' => 'LBL_DATE_MODIFIED',
          ),
        ),
        11 =>
        array (
          0 =>
          array (
            'name' => 'contract_request_id',
            'label' => 'LBL_CONTRACT_REQUEST_ID',
          ),
        ),
        12 =>
        array (
          0 =>
          array (
            'name' => 'ext_sys_created_by',
            'label' => 'LBL_EXT_SYS_CREATED_BY',
          ),
          1 =>
          array (
            'name' => 'ext_sys_modified_by',
            'label' => 'LBL_EXT_SYS_MODIFIED_BY',
          ),
        ),
      ),
      'lbl_detailview_panel3' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'ndb_contract_document_details_tpl',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_CONTRACT_DOCUMENT_DETAILS_TPL',
            'customCode' => '{include file=$CONTRACTDOCUMENTTPL}',
            'hideLabel' => true,
          ),
        ),
      ),
      'lbl_detailview_panel1' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'ndb_sales_representative_tpl',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_SALES_REPRESENTATIVE_TPL',
            'customCode' => '{include file=$SALESREPRESENTATIVETPL}',
            'hideLabel' => true,
          ),
        ),
      ),
      'lbl_detailview_panel2' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'ndb_line_item_details_tpl',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_LINE_ITEM_DETAILS_TPL',
            'customCode' => '{include file=$LINEITEMTPL}',
            'hideLabel' => true,
          ),
        ),
      ),
    ),
  ),
);
?>
