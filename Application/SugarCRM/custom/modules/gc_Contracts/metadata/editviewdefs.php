<?php
$module_name = 'gc_Contracts';
$viewdefs [$module_name] =
array (
  'EditView' =>
  array (
    'templateMeta' =>
    array (
      'form' =>
      array (
        'hidden' =>
        array (
          0 => '<input type="hidden" name="sugar_request" id="sugar_request" value="1">',
          1 => '<input type="hidden" name="contacts_country_id_c" id="contacts_country_id_c" value="{$CONTACTS_COUNTRY_ID_C}">',
          2 => '<input type="hidden" name="approver_user" id="approver_user" value="{$APPROVER_USER}">',
          3 => '<input type="hidden" name="contract_version" id="contract_version" value="{$fields.contract_version.value}">',
          4 => '<input type="hidden" name="parent_contract_id" id="parent_contract_id" value="{$fields.id.value}">',
        ),
        'buttons' =>
        array (
          'SAVE',
          'CANCEL',
          'CUSTOM_CHANGE_LOG' => array (
            'customCode' => '<input type="button" id="btn_custom_view_change_log" name="btn_custom_view_change_log"  onclick="auditLog( \'gc_Contracts\', 900, 900, \'{$fields.id.value}\');" value="View Change Log"/>',
          ),
        ),
      ),
      'includes' =>
      array (
        0 =>
        array (
          'file' => 'cache/include/javascript/sugar_grp_yui_widgets.js',
        ),
        1 =>
        array (
          'file' => 'custom/modules/gc_Contracts/include/javascript/LineItemUtils.js',
        ),
        2 =>
        array (
          'file' => 'custom/modules/gc_Contracts/include/javascript/EditView.js',
        ),
        3 =>
        array (
          'file' => 'custom/modules/gc_Contracts/include/javascript/ProductPoupulate.js',
        ),
      ),
      'maxColumns' => '2',
      'widths' =>
      array (
        0 =>
        array (
          'label' => '20',
          'field' => '30',
        ),
        1 =>
        array (
          'label' => '20',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' =>
      array (
        'LBL_EDITVIEW_PANEL4' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' =>
    array (
      'lbl_editview_panel4' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'global_contract_id',
            'label' => 'LBL_GLOBAL_CONTRACT_ID',
            'type' => 'readonly',
            'displayParams' =>
            array (
                'required' => false,
            ),
          ),
        1 =>
          array (
            'name' => 'global_contract_id_uuid',
            'type' => 'readonly',
          ),
        ),
        1 =>
        array (
          0 => 'name',
          1 =>
          array (
            'name' => 'contract_version',
            'label' => 'LBL_CONTRACT_VERSION',
            'type' => 'readonly',
          ),
        ),
        2 =>
        array (
          0 =>
          array (
            'name' => 'contract_status',
            'studio' => 'visible',
            'label' => 'LBL_CONTRACT_STATUS',
          ),
          1 =>
          array (
            'name' => 'loi_contract',
            'studio' => 'visible',
            'label' => 'LBL_LOI_CONTRACT',
          ),
        ),
        3 =>
        array (
          0 => 'description',
        ),
        4 =>
        array (
          0 =>
          array (
            'name' => 'team_name',
            'displayParams' =>
            array (
              'display' => true,
            ),
          ),
        ),
        5 =>
        array (
          0 =>
          array (
            'name' => 'contract_type',
            'studio' => 'visible',
            'label' => 'LBL_CONTRACT_TYPE',
          ),
          1 =>
          array (
            'name' => 'contract_scheme',
            'studio' => 'visible',
            'label' => 'LBL_CONTRACT_SCHEME',
          ),
        ),
        6 =>
        array (
          0 =>
          array (
            'name' => 'product_offering',
            'label' => 'LBL_PRODUCT_OFFERING',
          ),
          1 =>
          array (
            'name' => 'billing_type',
            'studio' => 'visible',
            'label' => 'LBL_BILLING_TYPE',

          ),
        ),
        7 =>
        array (
          0 =>
          array (
            'name' => 'accounts_gc_contracts_1_name',
            'label' => 'LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_ACCOUNTS_TITLE',
            'displayParams' =>
            array (
                'required' => true,
            ),
          ),
          1 =>
          array (
            'name' => 'contacts_gc_contracts_1_name',
            'label' => 'LBL_CONTACTS_GC_CONTRACTS_1_FROM_CONTACTS_TITLE',
            'displayParams' =>
            array (
              'required' => true,
              'field_to_name_array' =>
              array (
                'id' => 'contacts_gc_contracts_1contacts_ida',
                'name' => 'contacts_gc_contracts_1_name',
              ),
              'additionalFields' =>
              array (
                'c_country_id_c' => 'contacts_country_id_c',
              ),
            ),
          ),
        ),
        8 =>
        array (
          0 =>
          array (
            'name' => 'contract_startdate',
            'label' => 'LBL_CONTRACT_STARTDATE',
            'customCode' => '<div id="div_contract_startdate">{include file="custom/include/tpl/CustomDateTime.tpl" c_fld="contract_startdate" c_view="EditView" c_app = "" c_calss = "" c_dependentfield = ""}<input name="contract_startdate_hidden" id="contract_startdate_hidden" type="hidden" class="datehidden" value="{$FIELD_CONTRACT_STARTDATE_VAL}"></div>',
          ),
          1 =>
          array (
            'name' => 'contract_startdate_timezone',
            'label' => 'LBL_CONTRACT_STARTDATE_TIMEZONE',
            'customCode' => '{include file=$TZ_CUSTOMDROPDOWNTPL fieldname=$ID_CONTRACT_START_DATE fieldlist= $LIST_TIMEZONES action_event="onchange=\'setEndDateTimezone(this,\"gc_timezone_id1_c\");\'"}<input type="hidden" value="{$fields.contract_startdate_timezone.value}" id="contract_startdate_timezone" name="contract_startdate_timezone">',
          ),
        ),
        9 =>
        array (
          0 =>
          array (
            'name' => 'contract_enddate',
            'label' => 'LBL_CONTRACT_ENDDATE',
            'customCode' => '<div id="div_contract_enddate">{include file="custom/include/tpl/CustomDateTime.tpl" c_fld="contract_enddate" c_view="EditView" c_app = "" c_calss = "" c_dependentfield = ""}<input name="contract_enddate_hidden" id="contract_enddate_hidden" type="hidden" class="datehidden" value="{$FIELD_CONTRACT_ENDDATE_VAL}"></div>',
          ),
          1 =>
          array (
            'name' => 'contract_enddate_timezone',
            'studio' => 'visible',
            'label' => 'LBL_CONTRACT_ENDDATE_TIMEZONE',
            'customCode' => '{include file=$TZ_CUSTOMDROPDOWNTPL fieldname=$ID_CONTRACT_END_DATE fieldlist= $LIST_TIMEZONES }<input type="hidden" value="{$fields.contract_enddate_timezone.value}" id="contract_enddate_timezone" name="contract_enddate_timezone">',
          ),
        ),
        10 =>
        array (
          0 =>
          array (
            'name' => 'factory_reference_no',
            'label' => 'LBL_FACTORY_REFERENCE_NO',
          ),
          1 =>
          array (
            'name' => 'vat_no',
            'label' => 'LBL_VAT_NO',
          ),
        ),
      ),
      'lbl_editview_panel2' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'ndb_sales_representative_tpl',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_SALES_REPRESENTATIVE_TPL',
            'customCode' => '{include file=$SALESREPRESENTATIVETPL}',
            'hideLabel' => true,
          ),
        ),
      ),
      'lbl_editview_panel3' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'ndb_line_item_details_tpl',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_LINE_ITEM_DETAILS_TPL',
            'customCode' => '{include file=$LINEITEMTPL}',
            'hideLabel' => true,
          ),
        ),
      ),
    ),
  ),
);
?>