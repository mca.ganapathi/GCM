<?php
// created: 2018-03-07 11:45:26
$listViewDefs['gc_Contracts'] = array (
  'GLOBAL_CONTRACT_ID' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_GLOBAL_CONTRACT_ID',
    'width' => '10',
    'default' => true,
  ),
  'NAME' => 
  array (
    'width' => '32',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'ACCOUNTS_GC_CONTRACTS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_GC_CONTRACTS_1ACCOUNTS_IDA',
    'width' => '10',
    'default' => true,
  ),
  'PRODUCT_OFFERING' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_PRODUCT_OFFERING',
    'width' => '10',
    'default' => true,
  ),
  'CONTRACT_VERSION' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_CONTRACT_VERSION',
    'width' => '10',
    'default' => true,
  ),
  'CONTRACT_STATUS' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_CONTRACT_STATUS',
    'width' => '10',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10',
    'default' => true,
  ),
  'EXT_SYS_CREATED_BY' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_EXT_SYS_CREATED_BY',
    'width' => '10',
    'default' => true,
  ),
);