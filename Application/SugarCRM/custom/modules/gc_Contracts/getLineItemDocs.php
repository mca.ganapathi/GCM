<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/**
* File to get or delete the document details of a contract
*/

const GETLIDOCS = 'getLiDocs';
const DELLIDOCS = 'delLiDocs';

/* require this file for contract document audit*/
require_once ('custom/modules/Documents/include/ClassAfterRelationshipSave.php');

$command = trim($_POST['command']);

if($command == GETLIDOCS) {
	$liId = trim($_POST['liId']);
	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'command : ' . $command . GCM_GL_LOG_VAR_SEPARATOR . 'liId : ' . $liId, 'File to get the document details of a line item');
	if(!empty($liId)) {
		$line_item = new gc_LineItem();
        $line_item->retrieve($liId); 
        $line_item_doc_bean = $line_item->get_linked_beans( 'gc_lineitem_documents_1', 'Documents');
        // echo "<pre>";print_r($line_item_doc_bean);echo "</pre>";
        $document_template_types = $GLOBALS['app_list_strings']['document_template_type_dom'];
        $document_categorys = $GLOBALS['app_list_strings']['document_category_dom'];
        $line_item_doc_arr = array();
        if ($line_item_doc_bean != null) {
            foreach($line_item_doc_bean as $line_item_doc){
                $line_item_doc_arr[$line_item_doc->id]['doc_id'] = $line_item_doc->id;
                $line_item_doc_arr[$line_item_doc->id]['doc_name'] = $line_item_doc->document_name;
                $line_item_doc_arr[$line_item_doc->id]['filename'] = $line_item_doc->filename;
                $line_item_doc_arr[$line_item_doc->id]['revision'] = $line_item_doc->revision;
                $template = $line_item_doc->template_type;
                $cat = $line_item_doc->category_id;
                $line_item_doc_arr[$line_item_doc->id]['template_type'] = isset($document_template_types[$template]) ? $document_template_types[$template] : '';
                $line_item_doc_arr[$line_item_doc->id]['category_id'] = isset($document_categorys[$cat]) ? $document_categorys[$cat] :'';
            }
        }
		$respCode = 1; $respMsg = (count($line_item_doc_arr) > 0) ? $line_item_doc_arr : '';
	} else {
		$respCode = 1; $respMsg = '';
	}

} else if($command == DELLIDOCS) {
	$arrDocId = trim($_POST['record']);
	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'command : ' . $command . GCM_GL_LOG_VAR_SEPARATOR . 'arrDocId : ' . $arrDocId, 'File to delete the document details of a line item');
	if(!empty($arrDocId)) {
		$docBean = new Document();
        $docBean->retrieve($arrDocId); 
        $docBean->deleted = 1;
        $docBean->Save();
		$respCode = 1; $respMsg = '';
		
		// audit Contract Lineitem Document
		$GLOBALS['log']->debug("Start: Contract Lineitem document deleted audit");
		$lineitem_id = trim($_POST['lineitemId']);
		$document_name	=	$docBean->document_name;		
		$lineitem_bean = new gc_LineItem();
		$lineitem_bean->retrieve($lineitem_id);		
		$docAfterSaveObj 	=	new ClassAfterRelationshipSave();
		$docAfterSaveObj->insertContractAudit($lineitem_bean, 'Contract Lineitem Document', 'relate', $document_name, '');        	
		$GLOBALS['log']->debug("End: Contract Lineitem document deleted audit");		
	} else {
		$respCode = 2; $respMsg = '';
	}
}
$response = array('code' => $respCode, 'msg' => $respMsg);
ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response : ' . print_r($response, true), 'File to get or delete the document details of a contract');
echo json_encode($response);
