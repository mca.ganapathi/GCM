<div class="panel panel-default">
    
    <ul class="list-group" style="margin-left:0px">
        <li class="list-group-item" style="margin-left: 0px;">
			
            <div class="row toggle" id="dropdown-doc-rowid" data-toggle="doc-rowid">
               <div class="col-xs-10">
                  <strong>Documents</strong>	  
               </div>
               <div class="col-xs-2"><i id="arrow-doc-rowid" class="fa fa-chevron-up pull-right"></i>
               </div>
            </div>
		
        <div id="doc-rowid">
        <div style="display:" id="subpanel_gc_lineitem_documents_rowid" class="linedocsubpanel">
            <div id="list_subpanel_gc_lineitem_documents_rowid" >
            
            <table class="list view" border="0" cellpadding="0" cellspacing="0" width="100%">
            <thead>
                
                {if $DOCUMENT_EDIT_MODE == 'true'}
                <tr role="presentation">
                    <td colspan="8"  >
                        <div class="action_buttons"  style="padding: 6px">
                            <ul class="clickMenu">
                                <li class="sugar_action_button" onclick="createContractDocument('{$CONTRACTID}','')"> 
                                    <a href="javascript:void(0)"  >Create</a>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </td>
                </tr>
                {/if}
                <tr height="20">
                    <th scope="col" width="1%" style="border-radius: 0px;"><span style="white-space:normal;">&nbsp;</span></th>
                    <th scope="col" width="23%"><span style="white-space:normal;">{sugar_translate label='LBL_NAME' module='Documents'}</span></th>
                    <th scope="col" width="20%"><span style="white-space:normal;">{sugar_translate label='LBL_FILENAME' module='Documents'}</span></th>
                    <th scope="col" width="10%"><span style="white-space:normal;">{sugar_translate label='LBL_DOC_VERSION' module='Documents'}</span></th>
                    <th scope="col" width="22%"><span style="white-space:normal;">{sugar_translate label='LBL_TEMPLATE_TYPE' module='Documents'}</span></th> 
                    <th scope="col" width="18%"><span style="white-space:normal;">{sugar_translate label='LBL_SF_CATEGORY' module='Documents'}</span></th>
                    <th scope="col" width="3%"><span style="white-space:normal;">&nbsp;</span></th>
                    <th scope="col" width="3%"><span style="white-space:normal;">&nbsp;</span></th>
                </tr>
            </thead>
                {if empty($CONTRACTDOCUMENTLIST)}
                    <tbody id="contract_doc_list_items">
                        <tr class="evenListRowS1" height="20">
                            <td scope="row" valign="top" colspan=10 style="text-align:center"> No data </td>
                        </tr>
                    </tbody>
                {else}
                {assign var=rowcss value='even'}
                {assign var=row value=0}
                 <tbody id="contract_doc_list_items">
                {foreach from=$CONTRACTDOCUMENTLIST key=dk item=dv}
                    {if $rowcss == 'even'}
                        {assign var=rowcss value='odd'}
                    {else}
                        {assign var=rowcss value='even'}
                    {/if}
                    {assign var=row value=$row++}
               
                <tr class="{$rowcss}ListRowS1" height="20">
                    <td scope="row" valign="top">
                       <a href="index.php?entryPoint=download&amp;id={$dv.doc_id}&amp;type=Documents">
                        <img src="themes/default/images/attachment.gif" alt="attachment" border="0"></a>
                    </td>
                    <td scope="row" valign="top">
                        <a href="index.php?module=Documents&amp;action=DetailView&amp;record={$dv.doc_id}">{$dv.doc_name}</a>
                    </td>
                    <td scope="row" valign="top">
                        <a href="index.php?entryPoint=download&amp;id={$dv.doc_id}&amp;type=Documents" class="tabDetailViewDFLink" target="_blank">{$dv.filename}</a>
                    </td>
                    <td scope="row" valign="top">{$dv.revision}</td>
                    <td scope="row" valign="top">{$dv.template_type}</td>
                    <td scope="row" valign="top">{$dv.category_id}</td>
                    <td scope="row" valign="top" style="padding:3px">
                    {if $DOCUMENT_EDIT_MODE == 'true'}
                        <ul  class="clickMenu">
                            <li class="sugar_action_button">
                                <a href="javascript:void(0)" onclick="createContractDocument('{$dv.contract_id}','{$dv.doc_id}')">Edit</a>
                            </li></ul>
                    {else}
                        &nbsp;
                    {/if}        
                    </td>
                    <td scope="row" valign="top" style="padding:3px">
                    {if $DOCUMENT_EDIT_MODE == 'true'}
                         <ul class="clickMenu ">
                            <li class="sugar_action_button">
                                <a href="javascript:void(0)" style="padding:3px" onclick="removeContractDocument('{$dv.contract_id}','{$dv.doc_id}')">Remove</a>
                            </li>
                        </ul>
                    {else}
                        &nbsp;
                    {/if}
                    </td>
                </tr> 
                
                {/foreach}
                </tbody>
                {/if}
            </tbody>
            </table>
            </div>
           </div>
          </div>
         </li>                     								 
                  </ul>
	</div>