{*
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (â€œMSAâ€�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

*}
<link rel="stylesheet" href="custom/include/assests/css/bootstrap.min.css" />
<link rel="stylesheet" href="custom/include/assests/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="custom/include/assests/css/CustomCSS.css" />
<link rel="stylesheet" href="custom/include/assests/css/font-awesome.min.css">
<style>
{literal}
#dcmenuSugarCube .notifCount{
height: 27px !important;
}
#glblSearchBtn{
top: 0px !important;
width: 45px !important;
}	
.add-cell-border-right {
    border-right: 1px solid rgb(221, 221, 221) !important;
}

.method [class^="row"], .method [class*=" row"] {
    border-bottom: 1px solid #ddd !important;
    /*border-top: 1px solid #ddd !important;*/
}

.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
    position: relative;
    min-height: 42px !important;
    padding-right: 15px;
    padding-left: 15px;
}

.toggle {
     margin-right: 0px !important; 
     margin-left:  0px !important; 
}
{/literal}
</style>
	   <div class="panel panel-default">
		  <ul class="list-group" style="margin-left:0px">			  
				<li class="list-group-item" style="margin-left: 0px;">			  
				<div id="sr-1">
				   <input  id="sales_rep_id" name="sales_rep_id" type="hidden" value="">
				   <div class="method">
					  <div class="row margin-0">				 
						 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell" >
							   <span class="cellfont">Sales Channel Code</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right add-cell-border-top">
							<div class="cell">		
							   <input class="cellfont" id="sales_channel_code" name="sales_channel_code" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.sales_channel_code.len}" value="" type="text">
							</div>
						 </div>
						 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell">
							   <span class="cellfont">Sales Company</span>	
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right add-cell-border-top">
							<div class="cell">
							   <span class="cellfont">							
								  <select id="sales_company_id" name="sales_company_id" style="width:73%">
                                     <option value=""></option>
                                  {foreach from=$SALES_COMPANIES_LIST key=ckey item=cval}
                                     <option value="{$ckey}">{$cval}</option>
                                  {/foreach}
                                  </select>
								</span> 
							</div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">								
							   	<span class="cellfont">Sales Representative  Name (Local Character)</span>				   
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">	
                               <input class="cellfont" id="sales_rep_name_1" name="sales_rep_name_1" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.sales_rep_name_1.len}" value="" type="text">
							</div>
						 </div>
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">
							  <span class="cellfont">Sales Representative Name (Latin Character)</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">		
                               <input class="cellfont" id="sales_rep_name" name="sales_rep_name" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.name.len}" value="" type="text"> 
							</div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">								
							   <span class="cellfont">Division (Local Character)</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">	
                               <input class="cellfont" id="division_2" name="division_2" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.division_2.len}" value="" type="text">
							</div>
						 </div>					  
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">Division (Latin Character)</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">	
                                <input class="cellfont" id="division_1" name="division_1" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.division_1.len}" value="" type="text">
							</div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">								
							   <span class="cellfont">Title</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">		
                                 <input class="cellfont" id="title_2" name="title_2" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.title_2.len}" value="" type="text"> 
							</div>
						 </div>
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">Telephone number</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">
                                <input class="cellfont" id="tel_no" name="tel_no" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.tel_no.len}" value="" type="text"> 
							</div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">								
							   <span class="cellfont">Extension number</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">	
                               <input class="cellfont" id="ext_no" name="ext_no" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.ext_no.len}" value="" type="text">
							</div>
						 </div>
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">FAX number</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">	
                                <input class="cellfont" id="fax_no" name="fax_no" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.fax_no.len}" value="" type="text"> 
							</div>
						 </div>
					  </div>
					   <div class="row margin-0">
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">Email address</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">		
                                <input class="cellfont" id="email1" name="email1" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.email1.len}" value="" type="text">
							</div>
						 </div>    
						<div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">								
								<span class="cellfont">&nbsp;</span>								
							</div>
						 </div>  
						<div class="col-md-3 add-cell-border-right">
							<div class="cell">								
								<span class="cellfont">&nbsp;</span>								
							</div>
						 </div>  					 
					  </div>
				   </div>
				</div>
			 </li>
			 </ul>
	   </div>