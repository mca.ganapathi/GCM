{*
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (â€œMSAâ€�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

*}
<link rel="stylesheet" href="custom/include/assests/css/bootstrap.min.css" />
<link rel="stylesheet" href="custom/include/assests/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="custom/include/assests/css/CustomCSS.css" />
<link rel="stylesheet" href="custom/include/assests/css/font-awesome.min.css">
<style>
{literal}
#dcmenuSugarCube .notifCount{
height: 27px !important;
}
#glblSearchBtn{
top: 0px !important;
width: 45px !important;
}	
{/literal}
</style>
<form id="sales_rep_form" name="sales_rep_form" method="POST" action="index.php">
<input name="module" value="gc_Contracts" type="hidden">
<input name="action" value="UpdateSalesRep" type="hidden">
<input name="return_module" value="gc_Contracts" type="hidden">
<input name="return_action" value="DetailView" type="hidden">
<input name="return_id" value="{$CONTRACTID}" type="hidden">	  
{if empty($SALESREPRESENTATIVET_LIST)}
<input id="sales_rep_id" name="sales_rep_id" type="hidden" value="">
<div class="action_buttons pull-right" >
 <span style="{$SALESREPRESENTATIVEEDIT}"><input id="btn_create" type="button" value="Create" onclick="createSalesRep()"/></span>
</div>
     <span id="no_record_found">{sugar_translate label='LBL_NO_RECORD_FOUND' module='gc_Contracts'}</span>
     <div id="create_sales_rep" style="display:none">
				
				   <hr style="margin-top: 12px;border:0px">
				   </hr>
				   <div class="method">
					  <div class="row margin-0">				 
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell" >
							   <span class="cellfont">Sales Channel Code</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
							<div class="cell">								
							  
							   <span class="cellfont"> 
							       <input id="sales_channel_code" class="cellfont" name="sales_channel_code" tabindex="0" size="30" maxlength="100" value="" type="text">							
							   </span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell">
							   <span class="cellfont">Sales Company</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
							<div class="cell">		
                              
                              <span class="cellfont"> 
                              <select id="sales_company_id" name="sales_company_id" style="width:73%">
                                    <option value=""></option>
                                  {foreach from=$SALES_COMPANIES_LIST key=ckey item=cval}
                                    <option value="{$ckey}" >{$cval}</option>
                                  {/foreach}
                                 </select> 
                                 </span>
                            </div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">								
							  	<span class="cellfont">Sales Representative  Name (Local Character)</span>				   
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">				
								
								 <span class="cellfont">
								     <input class="cellfont" id="sales_rep_name_1" name="sales_rep_name_1" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.sales_rep_name_1.len}" value="" type="text">
								 </span>				   
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">Sales Representative Name (Latin Character)</span> 
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">								
							  	
							  	<span class="cellfont"> 
							  	   <input class="cellfont" id="sales_rep_name" name="sales_rep_name" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.name.len}" value="" type="text">
							  	</span>					
							</div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">								
							   <span class="cellfont">Division (Local Character)</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">							
							  
							   <span class="cellfont">
							       <input class="cellfont" id="division_2" name="division_2" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.division_2.len}" value="" type="text">
							   </span>						  
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">Division (Latin Character)</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">								
								
								<span class="cellfont">
						            <input class="cellfont" id="division_1" name="division_1" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.division_1.len}" value="" type="text">
								</span>								
							</div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">								
							   <span class="cellfont">Title</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">							
								
								 <span class="cellfont">
								     <input class="cellfont" id="title_2" name="title_2" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.title_2.len}" type="text">
								 </span>							  
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">Telephone number</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">								
								
								<span class="cellfont"> 
								    <input class="cellfont" id="tel_no" name="tel_no" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.tel_no.len}" type="text">
								</span>								
							</div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">								
							   <span class="cellfont">Extension number</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">							
							   
							   <span class="cellfont">
							       <input class="cellfont" id="ext_no" name="ext_no" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.ext_no.len}" type="text"> 
							   </span>							  
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">FAX number</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">								
								
								<span class="cellfont">
								    <input class="cellfont" id="fax_no" name="fax_no" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.fax_no.len}"  type="text">
								</span>								
							</div>
						 </div>
					  </div>
					   <div class="row margin-0">
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">Email address</span>
							</div>
						 </div>
						 <div class="col-md-9 col-sm-9 col-xs-9 add-cell-border-right">
							<div class="cell">								
								
								<span class="cellfont">
								    <input class="cellfont" id="email1" name="email1" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.email1.len}" value="" type="text">
								</span>								
							</div>
						 </div>    
						 					 
					  </div>
					 <div style="margin: 7px 0 0 47%;" class="sales_rep_edit">
					      <input type="submit" value="Save" id="sales_rep_save" onclick="var _form = document.getElementById('sales_rep_form'); if(check_form('sales_rep_form'))SUGAR.ajaxUI.submitForm(_form);return false;"/>
					      <input type="button" value="Cancel" onclick="cancelSalesRep()"/>
					  </div>
				</form>
				   </div>
				</div>
{else}	
   
	{assign var=i value=1}
	   <div class="panel panel-default">
		  <ul class="list-group" style="margin-left:0px;">
			  {foreach from=$SALESREPRESENTATIVET_LIST key=k item=v}
			   {assign var=rowid value=$i++}		  
				<li class="list-group-item" style="margin-left: 0px;">
				
				
				<input id="sales_rep_id" name="sales_rep_id" type="hidden" value="{$v.id}">
				
				<div class="row toggle" id="dropdown-sr-{$rowid}" data-toggle="sr-{$rowid}">
				   <div class="col-xs-10">
					  <strong>{$v.sales_channel_code}</strong>  
				   </div>
				   
				       
				  
				   <div class="col-xs-2">
				       <i id="arrow-sr-{$rowid}" class="fa fa-chevron-down pull-right"></i>
				   </div>
				   
				</div>
				
				<div id="sr-{$rowid}" style="display:none">
				<div class="action_buttons pull-right" >
				           <span style="{$SALESREPRESENTATIVEEDIT}"><input id="btn_edit" type="button" value="Edit" onclick="editSalesRep()"/></span>
				           <input id="btn_view_change_log" title="View Change Log"
						   class="button"
						   onclick="open_popup(&quot;Audit&quot;, &quot;600&quot;, &quot;400&quot;, &quot;&amp;record={$k}&amp;module_name=gc_SalesRepresentative&quot;, true, false,  {ldelim} &quot;call_back_function&quot;:&quot;set_return&quot;,&quot;form_name&quot;:&quot;EditView&quot;,&quot;field_to_name_array&quot;:[] {rdelim} ); return false;"
						   value="View Change Log" type="button">
				</div>
				   <hr style="margin-top: 12px;border:0px">
				   </hr>
				   <div class="method">
					  <div class="row margin-0">				 
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell" >
							   <span class="cellfont">Sales Channel Code</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
							<div class="cell">								
							   <span class="cellfont sales_rep_text">{$v.sales_channel_code}</span>
							   <span class="cellfont sales_rep_edit"> 
							       <input id="sales_channel_code" class="cellfont" name="sales_channel_code" tabindex="0" size="30" maxlength="100" value="{$SALESREPRESENTATIVE_FRESH_LIST.$k.sales_channel_code}" type="text">							
							   </span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell">
							   <span class="cellfont">Sales Company</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
							<div class="cell">		
                              <span class="cellfont sales_rep_text">{$v.sales_company_name}</span>
                              <span class="cellfont sales_rep_edit"> 
                              <select id="sales_company_id" name="sales_company_id" style="width:73%">
                                    <option value=""></option>
                                  {foreach from=$SALES_COMPANIES_LIST key=ckey item=cval}
                                    <option value="{$ckey}" {if $v.sales_company_id == $ckey} selected {/if} >{$cval}</option>
                                  {/foreach}
                                 </select> 
                                 </span>
                            </div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">								
							  	<span class="cellfont">Sales Representative  Name (Local Character)</span>				   
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">				
								 <span class="cellfont sales_rep_text" style="display: block; width: auto; word-break: break-all;">{$v.sales_rep_name_1}</span>	
								 <span class="cellfont sales_rep_edit">
								     <input class="cellfont" id="sales_rep_name_1" name="sales_rep_name_1" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.sales_rep_name_1.len}" value="{$SALESREPRESENTATIVE_FRESH_LIST.$k.sales_rep_name_1}" type="text">
								 </span>				   
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">Sales Representative Name (Latin Character)</span> 
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">								
							  	<span class="cellfont sales_rep_text" style="display: block; width: auto; word-break: break-all;">{$v.name}</span>	
							  	<span class="cellfont sales_rep_edit"> 
							  	   <input class="cellfont" id="sales_rep_name" name="sales_rep_name" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.name.len}" value="{$SALESREPRESENTATIVE_FRESH_LIST.$k.name}" type="text">
							  	</span>					
							</div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">								
							   <span class="cellfont">Division (Local Character)</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">							
							   <span class="cellfont sales_rep_text">{$v.division_2}</span>	
							   <span class="cellfont sales_rep_edit">
							       <input class="cellfont" id="division_2" name="division_2" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.division_2.len}" value="{$SALESREPRESENTATIVE_FRESH_LIST.$k.division_2}" type="text">
							   </span>						  
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">Division (Latin Character)</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">								
								<span class="cellfont sales_rep_text">{$v.division_1}</span>
								<span class="cellfont sales_rep_edit">
						            <input class="cellfont" id="division_1" name="division_1" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.division_1.len}" value="{$SALESREPRESENTATIVE_FRESH_LIST.$k.division_1}" type="text">
								</span>								
							</div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">								
							   <span class="cellfont">Title</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">							
								 <span class="cellfont sales_rep_text">{$v.title_2}</span>
								 <span class="cellfont sales_rep_edit">
								     <input class="cellfont" id="title_2" name="title_2" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.title_2.len}" value="{$SALESREPRESENTATIVE_FRESH_LIST.$k.title_2}" type="text">
								 </span>							  
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">Telephone number</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">								
								<span class="cellfont sales_rep_text">{$v.tel_no}</span>
								<span class="cellfont sales_rep_edit"> 
								    <input class="cellfont" id="tel_no" name="tel_no" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.tel_no.len}" value="{$SALESREPRESENTATIVE_FRESH_LIST.$k.tel_no}" type="text">
								</span>								
							</div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">								
							   <span class="cellfont">Extension number</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">							
							   <span class="cellfont sales_rep_text">{$v.ext_no}</span>
							   <span class="cellfont sales_rep_edit">
							       <input class="cellfont" id="ext_no" name="ext_no" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.ext_no.len}" value="{$SALESREPRESENTATIVE_FRESH_LIST.$k.ext_no}" type="text"> 
							   </span>							  
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">FAX number</span>
							</div>
						 </div>
						 <div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">								
								<span class="cellfont sales_rep_text">{$v.fax_no}</span>
								<span class="cellfont sales_rep_edit">
								    <input class="cellfont" id="fax_no" name="fax_no" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.fax_no.len}" value="{$SALESREPRESENTATIVE_FRESH_LIST.$k.fax_no}" type="text">
								</span>								
							</div>
						 </div>
					  </div>
					   <div class="row margin-0">
						 <div class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">Email address</span>
							</div>
						 </div>
						 <div class="col-md-9 col-sm-9 col-xs-9 add-cell-border-right">
							<div class="cell">								
								<span class="cellfont sales_rep_text">{$v.email1}</span>
								<span class="cellfont sales_rep_edit">
								    <input class="cellfont" id="email1" name="email1" tabindex="0" size="30" maxlength="{$SALES_REP_FIELDS_META.email1.len}" value="{$SALESREPRESENTATIVE_FRESH_LIST.$k.email1}" type="text">
								</span>								
							</div>
						 </div> 					 
					  </div>
					 <div style="margin: 7px 0 0 47%;" class="sales_rep_edit">
					      <input type="submit" value="Save" id="sales_rep_save" onclick="var _form = document.getElementById('sales_rep_form'); if(check_form('sales_rep_form'))SUGAR.ajaxUI.submitForm(_form);return false;"/>
					      <input type="button" value="Cancel" onclick="cancelSalesRep()"/>
					  </div>
				</form>
				   </div>
				</div>
				 
			 </li>				  	  
			  {/foreach}
		  </ul>
	   </div>	
{/if}
