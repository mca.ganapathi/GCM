{if empty($LINEITEMHEADERLIST)} {sugar_translate
label='LBL_NO_RECORD_FOUND' module='gc_Contracts'} {else} {assign var=i
value=1} {assign var=j value=1}

<div class="" style="text-align: right; margin-bottom: 4px;">
	<div class="action_buttons">
		<button type="button" class="button btn-open-all">Open All</button>
		<button type="button" class="button primary btn-close-all">Close All</button>
	</div>
</div>

<div class="panel panel-default">
	<ul class="list-group" style="margin-left: 0px">
		{foreach from=$LINEITEMHEADERLIST key=k item=v} {assign var=rowid
		value=$i++}
		<li class="list-group-item" style="margin-left: 0px;">
			<div class="row toggle" id="dropdown-li-{$rowid}"
				data-toggle="li-{$rowid}">
				<div class="col-xs-2">
					<strong>{$v.global_contract_item_id}</strong>
				</div>
				<div class="col-xs-4">
					<strong>{$v.product_specification} {if
						!empty($v.product_specification_type_name)}(
						{$v.product_specification_type_name} ) {/if}</strong>
				</div>
				<div class="col-xs-3">
					<strong>{$v.group_name}</strong>
				</div>
				<div class="col-xs-2">
					<strong>{$v.contract_line_item_type}</strong>
				</div>
				<div class="col-xs-1">
					<i id="arrow-li-{$rowid}" class="fa fa-chevron-down pull-right"></i>
				</div>
			</div>
			<div id="li-{$rowid}" style="display: none">
				<div class="pull-right">
					<input id="btn_view_change_log" title="View Change Log"
						class="button" style="margin-top: 3px; margin-right: 3px;"
						onclick = "auditLog( &quot;gc_LineItem&quot;, 600, 400, &quot;{$v.line_item_id}&quot;); return false;"
						value="View Change Log" type="button">
				</div>
				<hr style="margin-top: 9px; border: 0px;"></hr>
				<div class="method">
					<div class="row margin-0">
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">Global Contract Item ID</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">{$v.global_contract_item_id}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">Object ID</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3  add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont {if $v.contract_line_item_type == 'Change'}upgrade-change{/if}">{$v.line_item_id}</span> <input
									type="hidden" name="line_itemid_{$rowid}"
									id="line_itemid_{$rowid}" value="{$v.line_item_id}"
									data-row="{$rowid}">
							</div>
						</div>
					</div>
					<div class="row margin-0">
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont" id="label_payment_type_{$rowid}">{sugar_translate
									label='LBL_PRODUCT_SPECIFICATION'
									module='gc_Line_Item_Contract_History'}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont" id="div_product_spec_{$rowid}">
									{$v.product_specification} {if
									!empty($v.product_specification_type_name)}(
									{$v.product_specification_type_name} ) {/if} </span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
								<span class="cellfont">{sugar_translate
									label='LBL_INVOICE_SUBTOT_GROUP' module='gc_Contracts'}</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">
								<span class="cellfont"><a
									href="?module=gc_InvoiceSubtotalGroup&action=DetailView&record={$v.group_id}">{$v.group_name}</a></span>
							</div>
						</div>
						<!--DF-38-->
						<!-- <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">								
                        <span class="cellfont">Quantity</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">							
                        <span class="cellfont">{$v.product_quantity}</span>	 					  
                     </div>
                  </div> -->
						<!--DF-38-->
					</div>
					<div class="row margin-0">
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">{sugar_translate
									label='LBL_CUST_CONTRACT_CURRENCY'
									module='gc_Line_Item_Contract_History'}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">{$v.cust_contract_currency}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">{sugar_translate
									label='LBL_CUST_BILLING_CURRENCY'
									module='gc_Line_Item_Contract_History'}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">{$v.cust_billing_currency}</span>
							</div>
						</div>
					</div>
					<div class="row margin-0">
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">{sugar_translate
									label='LBL_IGC_CONTRACT_CURRENCY'
									module='gc_Line_Item_Contract_History'}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">{$v.igc_contract_currency}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">{sugar_translate
									label='LBL_IGC_BILLING_CURRENCY'
									module='gc_Line_Item_Contract_History'}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">{$v.igc_billing_currency}</span>
							</div>
						</div>
					</div>
					<div class="row margin-0">
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">{sugar_translate
									label='LBL_CUSTOMER_BILLING_METHOD'
									module='gc_Line_Item_Contract_History'}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">

							<div class="cell">
								<span class="cellfont">{$v.customer_billing_method}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">{sugar_translate
									label='LBL_IGC_SETTLEMENT_METHOD'
									module='gc_Line_Item_Contract_History'}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
							<div class="cell">
								<span class="cellfont">{$v.igc_settlement_method}</span>
							</div>
						</div>
					</div>
					<!-- <div class="row margin-0">
                  <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell" >
                        <span class="cellfont">NRC</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">								
                        <span class="cellfont">0.00</span>							
                     </div>
                  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">								
                        <span class="cellfont">MRC</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">							
                        <span class="cellfont">0.00</span>					  
                     </div>
                  </div>
               </div> -->
					<div class="row margin-0">
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
								<span class="cellfont">Technology Customer Account</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">
								<span class="cellfont"><a
									href="?module=Contacts&action=DetailView&record={$v.tech_account_id}">{if trim($v.tech_account_name) == '' && $v.tech_account_id}Click Here{else}{$v.tech_account_name}{/if}</a></span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
								<span class="cellfont">Billing Customer Account</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">
								<span class="cellfont"><a
									href="?module=Contacts&action=DetailView&record={$v.bill_account_id}">{if trim($v.bill_account_name) == '' && $v.bill_account_id}Click Here{else}{$v.bill_account_name}{/if}</a></span>
							</div>
						</div>
					</div>
					<div class="row margin-0">
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
								<span class="cellfont">Service Start Date</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">
								<span class="cellfont">{$v.service_start_date}
									{$v.service_start_date_timezone}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
								<span class="cellfont">Service End Date</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">
								<span class="cellfont">{$v.service_end_date}
									{$v.service_end_date_timezone}</span>
							</div>
						</div>
					</div>
					<div class="row margin-0">
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
								<span class="cellfont">Billing Start Date</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">
								<span class="cellfont">{$v.billing_start_date}
									{$v.billing_start_date_timezone}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
								<span class="cellfont">Billing End Date</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">
								<span class="cellfont">{$v.billing_end_date}
									{$v.billing_end_date_timezone}</span>
							</div>
						</div>
					</div>
					<div class="row margin-0">
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
								<span class="cellfont">Payment Type</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">
								<span class="cellfont">{$v.payment_type_value}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
								<span class="cellfont">{sugar_translate
									label='LBL_FACTORY_REFERENCE_NO'
									module='gc_Line_Item_Contract_History'}</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">
								<span class="cellfont">{$v.factory_reference_no}</span>
							</div>
						</div>
					</div>
					<div class="row margin-0">
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
								<span class="cellfont">{sugar_translate
									label='LBL_CONTRACT_LINE_ITEM_TYPE'
									module='gc_Line_Item_Contract_History'}</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">
								<span class="cellfont {if $v.contract_line_item_type == 'Change'}upgrade-change{/if}">{$v.contract_line_item_type}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
								<span class="cellfont">
								   {sugar_translate label='LBL_BILLING_AFFILIATE' module='gc_Line_Item_Contract_History'}
								</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">
								<span class="cellfont">
								   <a href="?module=gc_organization&action=DetailView&record={$v.gc_organization_id_c}">{$v.org_name_combined}</a>
								</span>
							</div>
						</div>
					</div>
					<div class="row margin-0">
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
								<span class="cellfont">{sugar_translate label='LBL_REMARKS'
									module='gc_Line_Item_Contract_History'}</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">
								<span class="cellfont">{$v.description}</span>
							</div>
						</div>
						<div
							class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
							<div class="cell">
								<!--<span class="cellfont">{sugar_translate label='LBL_MODULE_TITLE'
									module='pi_product_item'}</span>-->
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
							<div class="cell">
								<!--<span class="cellfont"><a
									href="?module=pi_product_item&action=DetailView&record={$v.products_id}">{$v.products_name}</a></span>-->
							</div>
						</div>
					</div>

				</div>
				{if $v.payment_type_id|count_characters == 2 && $v.payment_type_id == 00}
				<div class="panel panel-default" style="margin-top: 15px">
					<ul class="list-group" style="margin-left: 0px">
						<li class="list-group-item" style="margin-left: 0px;">
							<div class="row toggle" id="dropdown-dd-{$rowid}"
								data-toggle="dd-{$rowid}">
								<div class="col-xs-10">
									<strong>Direct Deposit Details</strong>
								</div>
								<div class="col-xs-2">
									<i id="arrow-dd-{$rowid}" class="fa fa-chevron-down pull-right"></i>
								</div>
							</div>
							<div id="dd-{$rowid}" style="display: none">
								<hr style="margin-top: 2px; border: 0px;">
								</hr>
								<div class="method">
									<div class="row margin-0">
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">Bank Code</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">{$v.deposit_deposit_details.bank_code}</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">Branch Code</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">{$v.deposit_deposit_details.branch_code}</span>
											</div>
										</div>
									</div>
									<div class="row margin-0">
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
											<div class="cell">
												<span class="cellfont">Deposit Type</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
											<div class="cell">
												<span class="cellfont">{$v.deposit_deposit_details.deposit_type}</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
											<div class="cell">
												<span class="cellfont">Account Number</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
											<div class="cell">
												<span class="cellfont">{$v.deposit_deposit_details.account_no}</span>
											</div>
										</div>
									</div>
									<div class="row margin-0">
										<!-- <div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
											<div class="cell">
												<span class="cellfont">Payee Contact Person Name1 (Latin
													Character)</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
											<div class="cell">
												<span class="cellfont">{$v.deposit_deposit_details.person_name_1}</span>
											</div>
										</div> -->
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
											<div class="cell">
												<span class="cellfont">Payee Contact Person Name</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
											<div class="cell">
												<span class="cellfont">{$v.deposit_deposit_details.person_name_2}</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
											<div class="cell">
												<span class="cellfont">Payee Telephone Number</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
											<div class="cell">
												<span class="cellfont">{$v.deposit_deposit_details.payee_tel_no}</span>
											</div>
										</div>
									</div>
									<div class="row margin-0">
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
											<div class="cell">
												<span class="cellfont">Payee E-mail Address</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right" style="word-wrap: break-word;">
											<div class="cell">
												<span class="cellfont">{$v.deposit_deposit_details.payee_email_address}</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
											<div class="cell">
												<span class="cellfont">Remarks</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right"
											style="word-wrap: break-word;">
											<div class="cell">
												<span class="cellfont">{$v.deposit_deposit_details.remarks}</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				{/if} {if $v.payment_type_id == 10}
				<div class="panel panel-default" style="margin-top: 15px">
					<ul class="list-group" style="margin-left: 0px">
						<li class="list-group-item" style="margin-left: 0px;">
							<div class="row toggle" id="dropdown-dd-{$rowid}"
								data-toggle="dd-{$rowid}">
								<div class="col-xs-10">
									<strong>Account Transfer Details</strong>
								</div>
								<div class="col-xs-2">
									<i id="arrow-dd-{$rowid}" class="fa fa-chevron-down pull-right"></i>
								</div>
							</div>
							<div id="dd-{$rowid}" style="display: none">
								<hr style="margin-top: 2px; border: 0px;">
								</hr>
								<div class="method">
									<div class="row margin-0">
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">Account Holder's Name</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">{$v.acc_trans_details.name}</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">Deposit Type</span>

											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">{$v.acc_trans_details.deposit_type}</span>
											</div>
										</div>
									</div>
									<div class="row margin-0">
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
											<div class="cell">
												<span class="cellfont">Bank Code</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
											<div class="cell">
												<span class="cellfont">{$v.acc_trans_details.bank_code}</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
											<div class="cell">
												<span class="cellfont">Branch Code</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
											<div class="cell">
												<span class="cellfont">{$v.acc_trans_details.branch_code}</span>
											</div>
										</div>
									</div>
									<div class="row margin-0">
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
											<div class="cell">
												<span class="cellfont">Account Number</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
											<div class="cell">
												<span class="cellfont">{$v.acc_trans_details.account_no}</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
											<div class="cell">
												<span class="cellfont">Account Number Display</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
											<div class="cell">
												<span class="cellfont">{$v.acc_trans_details.account_no_display}</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				{/if} {if $v.payment_type_id == 30}
				<div class="panel panel-default" style="margin-top: 15px">
					<ul class="list-group" style="margin-left: 0px">
						<li class="list-group-item" style="margin-left: 0px;">
							<div class="row toggle" id="dropdown-dd-{$rowid}"
								data-toggle="dd-{$rowid}">
								<div class="col-xs-10">
									<strong>Postal Transfer Details</strong>
								</div>
								<div class="col-xs-2">
									<i id="arrow-dd-{$rowid}" class="fa fa-chevron-down pull-right"></i>
								</div>
							</div>
							<div id="dd-{$rowid}" style="display: none">
								<hr style="margin-top: 2px; border: 0px;">
								</hr>
								<div class="method">
									<div class="row margin-0">
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">Account Holder's Name</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">{$v.postal_trans_details.name}</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">Postal Passbook Mark</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">{$v.postal_trans_details.postal_passbook_mark}</span>
											</div>
										</div>

									</div>
									<div class="row margin-0">
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
											<div class="cell">
												<span class="cellfont">Postal Passbook Number</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
											<div class="cell">
												<span class="cellfont">{$v.postal_trans_details.postal_passbook}</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right">
											<div class="cell">
												<span class="cellfont">Postal Passbook Number Display</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right">
											<div class="cell">
												<span class="cellfont">{$v.postal_trans_details.postal_passbook_no_display}</span>
											</div>
										</div>

									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				{/if} {if $v.payment_type_id == 60}
				<div class="panel panel-default" style="margin-top: 15px">
					<ul class="list-group" style="margin-left: 0px">
						<li class="list-group-item" style="margin-left: 0px;">
							<div class="row toggle" id="dropdown-dd-{$rowid}"
								data-toggle="dd-{$rowid}">
								<div class="col-xs-10">
									<strong>Credit Card Details</strong>
								</div>
								<div class="col-xs-2">
									<i id="arrow-dd-{$rowid}" class="fa fa-chevron-down pull-right"></i>
								</div>
							</div>
							<div id="dd-{$rowid}" style="display: none">
								<hr style="margin-top: 2px; border: 0px;">
								</hr>
								<div class="method">
									<div class="row margin-0">
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">Credit-card Number</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">{$v.credit_card_details.credit_card_no}</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">Expiration Date</span>
											</div>
										</div>
										<div
											class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">{$v.credit_card_details.expiration_date}</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				{/if} {if count($v.product_config) > 0}
				<div class="panel panel-default" style="margin-top: 15px">
					<ul class="list-group" style="margin-left: 0px">
						<li class="list-group-item" style="margin-left: 0px;">
							<div class="row toggle" id="dropdown-pc-{$rowid}"
								data-toggle="pc-{$rowid}">
								<div class="col-xs-10">
									<strong>{sugar_translate label='LBL_MODULE_NAME'
										module='gc_ProductConfiguration'}</strong>
								</div>
								<div class="col-xs-2">
									<i id="arrow-pc-{$rowid}" class="fa fa-chevron-down pull-right"></i>
								</div>
							</div>
							<div id="pc-{$rowid}" style="display: none">
								<hr style="margin-top: 2px; border: 0px;">
								</hr>
								<div class="method">
									{if !empty($v.product_config) }
									{assign var=k value=1} {foreach from=$v.product_config key=key
									item=val} {assign var=prowid value=$k++} 
										{if $val.lbl != ''}
											{if $k%2 == 0}
												<div class="row margin-0">
													{/if}
													<div
														class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top">
														<div class="cell">
															<span class="cellfont">{$val.lbl}</span>
														</div>
													</div>
													<div
														class="col-md-3 col-sm-3 col-xs-3 add-cell-border-right add-cell-border-top">
														<div class="cell">
															<span class="cellfont"> {if is_array($val.charval)}
																{$val.charval.displaytext} {else} {$val.charval} {/if} </span>
														</div>
													</div>
													{if $k%2 != 0}
												</div>
											{/if} 
									{else}
										{assign var=k value=1}
									{/if} 
									{/foreach} 
									{else}
										<span class="cellfont">{sugar_translate label='LBL_NO_RECORD_FOUND' module='gc_Contracts'}</span>
									{/if}
									{if $k%2 == 0}
									<div
										class="col-md-6 col-sm-6 col-xs-6 add-cell-border-right add-cell-border-top">
										<div class="cell">&nbsp;</div>
									</div>
									{/if}

								</div>
							</div>
						</li>
					</ul>
				</div>
				{else}
					<div class="panel panel-default" style="margin-top: 15px">
					 <ul class="list-group" style="margin-left: 0px">
					  <li class="list-group-item" style="margin-left: 0px;">
					   <div class="row toggle" id="dropdown-pc-{$rowid}"
						data-toggle="pc-{$rowid}">
						<div class="col-xs-10">
						 <strong>{sugar_translate label='LBL_MODULE_NAME'
						  module='gc_ProductConfiguration'}</strong>
						</div>
						<div class="col-xs-2">
						 <i id="arrow-pc-{$rowid}" class="fa fa-chevron-down pull-right"></i>
						</div>
					   </div>
					   <div id="pc-{$rowid}" style="display: none">
						<div class="cell" style="padding-top: 10px;">
						 <span class="cellfont">{sugar_translate label='LBL_NO_RECORD_FOUND' module='gc_Contracts'}</span>
						</div>         
					   </div>
					  </li>
					 </ul>
					</div>
				{/if}

				<!-- Rules Panel Start -->
				{if count($v.rules) > 0}
				<div class="panel panel-default" style="margin-top: 15px">
					<ul class="list-group" style="margin-left: 0px">
						<li class="list-group-item" style="margin-left: 0px;">
							<div class="row toggle" id="dropdown-rule-{$rowid}"
								data-toggle="rule-{$rowid}">
								<div class="col-xs-10">
									<strong>{sugar_translate label='LBL_PRODUCT_INFORMATION'
										module='gc_Contracts'}</strong>
								</div>
								<div class="col-xs-2">
									<i id="arrow-rule-{$rowid}"
										class="fa fa-chevron-down pull-right"></i>
								</div>
							</div>
							<div id="rule-{$rowid}" style="display: none">
								<hr style="margin-top: 2px; border: 0px;">
								</hr>
								<div class="method">
									{foreach from=$v.rules key=k1 item=rv}
									<div class="row margin-0">
										<div
											class="col-md-3 col-sm-3 col-xs-3 cellgrey add-cell-border-right add-cell-border-top add-cell-border-left">
											<div class="cell">
												<span class="cellfont">{$rv.rulename}</span>
											</div>
										</div>
										<div
											class="col-md-9 col-sm-9 col-xs-9 add-cell-border-right add-cell-border-top">
											<div class="cell">
												<span class="cellfont">{$rv.rule_value}</span>
											</div>
										</div>
									</div>
									{/foreach}
								</div>
							</div>
						</li>
					</ul>
				</div>
				{/if}
				<!-- Rules Panel End -->


				<div class="panel panel-default" style="margin-top: 15px">
					<ul class="list-group" style="margin-left: 0px">
						<li class="list-group-item" style="margin-left: 0px;">
							<div class="row toggle" id="dropdown-pp-{$rowid}"
								data-toggle="pp-{$rowid}">
								<div class="col-xs-10">
									<strong>{sugar_translate label='LBL_MODULE_NAME'
										module='gc_ProductPrice'}</strong>
								</div>
								<div class="col-xs-2">
									<i id="arrow-pp-{$rowid}" class="fa fa-chevron-down pull-right"></i>
								</div>
							</div>
							{if count($v.product_price) > 0}
							<div id="pp-{$rowid}" style="display: none">
								<hr style="margin-top: 2px; border: 0px;">
								</hr>
								<div class="method">
									{if count($v.product_price) > 0}
									<div class="row "
										style="margin-left: 0px; margin-right: 1px; border: 0px;">
										{assign var=k value=1} {assign var=r_incr value=1} {foreach
										from=$v.product_price key=key item=val} {assign var=prowid
										value=$k++} {if $r_incr > 4} {assign var=r_incr value=1}</div>
									<div class="margin-0">&nbsp;</div>

									{/if}
									{if $val.pricelineid > 0}
									{if $prowid%3 == 1}
									<div class="row "
										style="margin-left: 0px; margin-right: 1px; border: 0px;">
										{/if}
										<div class="col-md-4 col-sm-4 col-xs-4"
											id="product_price_{$rowid}_{$val.pricelineid}"
											style="margin-bottom: 7px">
											<table class="table table-bordered product_price"
												style="table-layout: fixed !important;">
												<tr>
													<td colspan="2" class="cellgrey header"
														title="{$val.chargedesc}"><div class="cellfont"
															style="overflow: hidden; max-height: 37px; font-weight: bold;">{$val.chargedesc}</div></td>
												</tr>
												<tr>
													<td class="cellgrey"><span class="cellfont">{sugar_translate
															label='LBL_CHARGE_TYPE' module='gc_Contracts'}</span></td>
													<td class=""><span class="cellfont"> {$val.chargetype} </span></td>
												</tr>
												<tr>
													<td class="cellgrey"><span class="cellfont">{sugar_translate
															label='LBL_CHARGE_PERIOD' module='gc_Contracts'}</span></td>
													<td class=""><span class="cellfont">
															{$val.chargeperioddesc} </span></td>
												</tr>
												<tr>
													<td class="cellgrey"><span class="cellfont">{sugar_translate
															label='LBL_CURRENCY' module='gc_Contracts'}</span></td>
													<td class=""><span class="cellfont"> {$val.currencyid} </span></td>
												</tr>
												<tr>
													<td class="cellgrey"><span class="cellfont">{sugar_translate
															label='LBL_LIST_PRICE' module='gc_Contracts'}</span></td>
													<td class=""><span class="cellfont">
													{if (($val.list_price neq "ICB") && ($val.list_price gt 999))}
													    {$val.list_price|number_format:3}
													{else}
														{$val.list_price}
													{/if}													
													</span></td>
												</tr>
												<tr>
													<td class="cellgrey"><span class="cellfont">{sugar_translate
															label='LBL_DISCOUNT' module='gc_Contracts'}</span></td>
													<td class=""><span class="cellfont"> 
													{if (($val.discount neq "-") && ($val.discount gt 999))}
													    {$val.discount|number_format:3}
													{else}
														{$val.discount}
													{/if}													
													{if $val.percent_discount_flag == 1}%{/if}</span>
													</td>
												</tr>
												<tr>
													<td class="cellgrey"><span class="cellfont">{sugar_translate
															label='LBL_CONTRACT_PRICE' module='gc_Contracts'}</span></td>
													<td class=""><span class="cellfont">
													{if ($val.charge_price gt 999)}
													    {$val.charge_price|number_format:3}
													{else}
														{$val.charge_price}
													{/if}													
													</span></td>
												</tr>
												<tr>
													<td class="cellgrey"><span class="cellfont">{sugar_translate
															label='LBL_IGC_SETTLEMENT_PRICE' module='gc_Contracts'}</span></td>
													<td class=""><span class="cellfont">
													{if ($val.igc_settlement_price gt 999)}
													    {$val.igc_settlement_price|number_format:3}
													{else}
														{$val.igc_settlement_price}
													{/if}													
													</span></td>
												</tr>
											</table>

										</div>
										{if $prowid%3 == 0}
									</div>
									{/if}
									{else}
									<div class="row margin-0"><div class="col-md-12 add-cell-border-right add-cell-border-left add-cell-border-bottom add-cell-border-top center"><div class="cell" ><span class="cellfont">No charge is available for this case</span></div></div></div>
									{/if} 
									{assign var=r_incr value=++$r_incr} {/foreach} {else}
									{sugar_translate label='LBL_NO_PRICE_DETAILS'
									module='gc_Contracts'} {/if}
								</div>
							</div>
							{else}
							<div id="pp-{$rowid}" style="display: none">
								<hr style="margin-top: 2px; border: 0px;">
								</hr>
								<div class="method">
							<div class="row margin-0"><div class="col-md-12 add-cell-border-right add-cell-border-left add-cell-border-bottom add-cell-border-top center"><div class="cell" ><span class="cellfont">No charge is available for this case</span></div></div></div>
							</div>
							</div>
							{/if}
						</li>
					</ul>
				</div>
				{assign var=line_item_docs value=$v.line_item_doc_arr} {include
				file='custom/modules/gc_Contracts/tpl/LineItemDocument.tpl'}
			</div>
		</li> {/foreach}
	</ul>
</div>
{/if}
<input type="hidden" name="team_difference" id="team_difference" value="{$TEAM_DIFFERENCE}">
<input type="hidden" name="description_changed" id="description_changed" value="{$DESCRIPTION_CHANGED}">
<input type="hidden" name="loi_changed" id="loi_changed" value="{$LOI_CHANGED}">
<input type="hidden" name="contract_type_changed" id="contract_type_changed" value="{$CONTRACT_TYPE_CHANGED}">
<input type="hidden" name="contract_scheme_changed" id="contract_scheme_changed" value="{$CONTRACT_SCHEME_CHANGED}">
<input type="hidden" name="billing_type_changed" id="billing_type_changed" value="{$BILLING_TYPE_CHANGED}">
<input type="hidden" name="contract_startdate_timezone_changed" id="contract_startdate_timezone_changed" value="{$CONTRACT_STARTDATE_TIMEZONE_CHANGED}">
<input type="hidden" name="contract_enddate_timezone_changed" id="contract_enddate_timezone_changed" value="{$CONTRACT_ENDDATE_TIMEZONE_CHANGED}">
<input type="hidden" name="contract_workflow_flag_changed" id="contract_workflow_flag_changed" value="{$CONTRACT_WORKFLOW_FLAG_CHANGED}">
<input type="hidden" name="document_edit_mode" id="document_edit_mode"
	value="{$DOCUMENT_EDIT_MODE}" />
<link rel="stylesheet"
	href="custom/modules/gc_Contracts/include/css/DetailView.css" />
