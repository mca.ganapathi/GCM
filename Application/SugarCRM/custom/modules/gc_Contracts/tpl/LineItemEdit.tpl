{assign var=i value=1}
{assign var=j value=1}
<link rel="stylesheet" href="custom/modules/gc_Contracts/include/css/EditView.css"/>
<div class="panel panel-default parentDiv" style="margin-bottom: 0px;">
	<input type="hidden" name="is_admin_flag" id="is_admin_flag" value="{$IS_ADMIN_FLAG}">
	{if $SIZEOFLINEITEM == 0 || $SIZEOFLINEITEM == ''} {assign var="SIZEOFLINEITEM" value=1} {/if}

    <input type="hidden" name="version_upgrade" id="version_upgrade" value="{$VERSION_UPGRADE}">
	<input type="hidden" name="nolineitem" id="nolineitem" class="nolineitem" value="{$SIZEOFLINEITEM}">
	<input type="hidden" name="deletedItem" id="deletedItem" class="deletedItem" value="">
	<input type="hidden" name="deletedItemHistory" id="deletedItemHistory" class="deletedItemHistory" value="">
	<input type="hidden" name="paymentItem" id="paymentItem" class="paymentItem" value="">
	<input type="hidden" name="product_specification_base" id="product_specification_base" value="">
    <input type="hidden" name="invoice_group_details_json" id="invoice_group_details_json" value='{$LIST_INVOICE_GROUPS}' />

    <div id="invoice_group_dialog" title="{sugar_translate label='LBL_INVOICE_SUBTOT_GROUP_NEW' module='gc_Contracts'}" style="display:none">
        <input type="hidden" id="invoice_rowid" name="invoice_rowid" >
        <div class="row col-md-12">
          <div class="col-md-6 cellgrey add-cell-border-right add-cell-border-top add-cell-border-left" style="padding:6px;height: 53px;">
             <div class="cell">
                <span class="cellfont" >{sugar_translate label='LBL_INVOICE_SUBTOT_GROUP_NAME' module='gc_Contracts'}<font color="red">*</font></span>
             </div>
          </div>
         <div class="col-md-6 add-cell-border-right add-cell-border-top "  style="padding:6px;">
            <div class="cell">
                 <input type="text" id="invoice_groupname" name="invoice_groupname" value="" class="cellfont" style="width:90%;padding:2px;" maxlength=100>
                 <span id="error_invoice_groupname" class="required" style="display:none" >
                    Missing Required field : {sugar_translate label='LBL_INVOICE_SUBTOT_GROUP_NAME' module='gc_Contracts'}
                 </span>
								 <span id="error_invoice_groupname_unique" class="required validation-message " style="display:none" >
                    Field must be unique : {sugar_translate label='LBL_INVOICE_SUBTOT_GROUP_NAME' module='gc_Contracts'}
                 </span>
            </div>
         </div>
        </div>
        <div class="row col-md-12" >
             <div class="col-md-6 cellgrey add-cell-border" style="padding:6px;">
               <div class="cell">
                  <span class="cellfont" >{sugar_translate label='LBL_INVOICE_SUBTOT_GROUP_DESC' module='gc_Contracts'}</span>
               </div>
             </div>
          <div class="col-md-6 add-cell-border " style="padding:6px;">
            <div class="cell">
                 <input type="text" id="invoice_groupdesc" name="invoice_groupdesc" value="" class="cellfont" style="width:90%;">
            </div>
         </div>
        </div>
    </div>
	{*<!-- Line Item Row Copy Start-->*}
	<ul class="list-group LineItemCopy hide" style="margin-left:0px"  id="container_lineitem_0">
	  <li class="list-group-item" style="margin-left: 0px;">
		<div class="row" id="dropdown-sr_0">
		   <div class="col-xs-10">
			  <strong><span class="lineno" id="lineno_0">{sugar_translate label='LBL_LINEITEM_DETAILS' module='gc_Contracts'}</span></strong>
			  <input type="hidden" id="line_item_id_0" name="line_item_id_0"  value="" class ="line_item_id">
			  <input type="hidden" id="line_item_history_id_0" name="line_item_history_id_0"  value="" class="line_item_history_id">
			  <input type="hidden" id="line_item_number" name="line_item_number"  value="" class ="line_item_number">
			  <input type="hidden" id="li_deleted_0" name="li_deleted_0"  value="0">
		   </div>
		   <div class="col-xs-2">
           {if empty($APPROVER_USER)}
			  <button type="button" class="close"  id="btn_li_remove_0" data-dismiss="modal" aria-hidden="true" onclick="removeLineItem(this);">&times;</button>
		   {/if}
           </div>
		</div>
		 <div id="li_0">
			<div class="method">
			   <div class="row margin-0">
                    <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                     <div class="cell">
                        <span class="cellfont" id="label_product_specification_0">{sugar_translate label='LBL_PRODUCT_SPECIFICATION' module='gc_Line_Item_Contract_History'}<font color="red">*</font></span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right add-cell-border-top">
                     <div class="cell">
                        <span class="cellfont" id="div_product_spec_0">
							<select class="req" style="width:100%" name="product_specification_0" id="product_specification_0">
								<option value="">Please select product offering</option>
							</select>
						</span>
                     </div>
                  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell">
						<span class="cellfont" id="label_product_name_0">{sugar_translate label='LBL_INVOICE_SUBTOT_GROUP' module='gc_Contracts'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
                    <div class="col-md-8" style="padding:0px; min-height:40px!important;">
                        <div class="cell">
                        <span class="cellfont">
							<select  style="width:100%" name="li_invoice_subgroup_0" id="li_invoice_subgroup_0" style="width: 80%!important">
								<option value=""></option>
							</select>
						</span>
                        </div>
                     </div>
                     <div class="col-md-4" style= "padding:0px;  min-height:40px!important;">
                      <div class="cell">
                        <span class="cellfont" >
                        <input type="button" id="add_invoice_subgroup_0" value="Add" name="button" class="button primary" title="Add" onclick="createIGDialog(this)">
                        </span>
                      </div>
                     </div>
				  </div>
				 <!-- DF 38<div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont">{sugar_translate label='LBL_PRODUCT_QUANTITY' module='gc_LineItem'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<input id="product_quantity_0" name="product_quantity_0" tabindex="0" size="30" value="1" type="text" readonly style="background-color: rgb(220, 220, 220);">
						</span>
					 </div>
				  </div> -->

			   </div>
               <div class="row margin-0">
                <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_cust_contract_currency_0">{sugar_translate label='LBL_CUST_CONTRACT_CURRENCY' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select id="cust_contract_currency_0" name="cust_contract_currency_0" style="width:100%"
                                    onchange='product_currency_change(this.id); changeLineItemBTypeFieldValue(this);'>
                                <option value=""></option>
                            {foreach from=$DEFAULT_CURRENCIES key=ckey item=cval}
                                <option value="{$ckey}">{$cval}</option>
                            {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_cust_billing_currency_0">{sugar_translate label='LBL_CUST_BILLING_CURRENCY' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select id="cust_billing_currency_0" name="cust_billing_currency_0" style="width:100%"
                                    onchange='changeLineItemBTypeFieldValue(this);'>
                                <option value=""></option>
                            {foreach from=$DEFAULT_CURRENCIES key=key item=val}
                                <option value="{$key}">{$val}</option>
                            {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
               </div>
               <div class="row margin-0">
                <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_igc_contract_currency_0">{sugar_translate label='LBL_IGC_CONTRACT_CURRENCY' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select id="igc_contract_currency_0" name="igc_contract_currency_0" style="width:100%"
                                        onchange='changeLineItemBTypeFieldValue(this);'>
                                <option value=""></option>
                            {foreach from=$DEFAULT_CURRENCIES key=key item=val}
                                <option value="{$key}">{$val}</option>
                            {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont"  id="label_igc_billing_currency_0">{sugar_translate label='LBL_IGC_BILLING_CURRENCY' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select id="igc_billing_currency_0" name="igc_billing_currency_0" style="width:100%"
                                            onchange='changeLineItemBTypeFieldValue(this);'>
                                <option value=""></option>
                            {foreach from=$DEFAULT_CURRENCIES key=key item=val}
                                <option value="{$key}">{$val}</option>
                            {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
               </div>
               <div class="row margin-0">
					<div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_customer_billing_method_0">{sugar_translate label='LBL_CUSTOMER_BILLING_METHOD' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select id="customer_billing_method_0" name="customer_billing_method_0"  style="width:100%"
                                            onchange='changeLineItemBTypeFieldValue(this);'>
                            {foreach from=$CUSTOMER_BILLING_METHODS key=key item=val}
                                <option value="{$key}">{$val}</option>
                            {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_igc_settlement_method_0">{sugar_translate label='LBL_IGC_SETTLEMENT_METHOD' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select id="igc_settlement_method_0" name="igc_settlement_method_0" style="width:100%"
                                    onchange='changeLineItemBTypeFieldValue(this);'>
                            {foreach from=$IGC_SETTLEMENT_METHODS key=key item=val}
                                <option value="{$key}">{$val}</option>
                            {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
               </div>
               <div class="row margin-0">
				  <div class="col-md-3 cellgrey add-cell-border-right">
					 <div class="cell">
						<span class="cellfont" id="label_tech_account_name_0">{sugar_translate label='LBL_TECH_ACCOUNT_ID' module='gc_Contracts'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right">
					 <div class="cell">
						<span class="cellfont">
							{include file="custom/include/tpl/CustomRelateField.tpl"
                            c_fld=","|explode:"tech_account_id_0,tech_account_name_0,Contacts, Accounts,0"
                            c_id="" c_name="" c_valid = '0' c_parent = '' c_filter = '' c_return = 'set_contacts_popup_return'}
						</span>
                        <input type="hidden" name="tech_account_countryid_0" id="tech_account_countryid_0" value="">
					 </div>
				  </div>
				  <div class="col-md-3 cellgrey add-cell-border-right">
					 <div class="cell">
						<span class="cellfont" id="label_bill_account_name_0">{sugar_translate label='LBL_BILL_ACOOUNT_ID' module='gc_Contracts'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right">
					 <div class="cell">
						<span class="cellfont">
							{include file="custom/include/tpl/CustomRelateField.tpl"
                            c_fld=","|explode:"bill_account_id_0,bill_account_name_0,Contacts, Accounts,0"
                            c_id="" c_name="" c_valid = '0' c_parent = '' c_filter = '' c_return = 'set_contacts_popup_return'}
						</span>
                        <input type="hidden" name="bill_account_countryid_0" id="bill_account_countryid_0" value="">
					 </div>
				  </div>
			   </div>
			   <div class="row margin-0">
				  <div class="col-md-3 cellgrey add-cell-border-right">
					 <div class="cell" >
						<span class="cellfont" id="label_service_start_date_0">{sugar_translate label='LBL_SERVICE_START_DATE' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right">
					 <div class="cell">
						<span class="cellfont dateInput">
							{include file="custom/include/tpl/CustomDateTime.tpl" c_fld="service_start_date" c_view="EditView" c_app = "_0" c_calss = "primary" c_dependentfield = ""}
							<span>
							<input id="service_start_date_hidden_0" name="service_start_date_hidden_0" class="datehidden" tabindex="0" size="30" value="" type="text" dependentfield="" style="display:none">
							</span>
						</span>
					 </div>
				  </div>
                   <div class="col-md-3 cellgrey add-cell-border-right">
					 <div class="cell" >
						<span class="cellfont" id="label_service_start_date_timezone_0">{sugar_translate label='LBL_SERVICE_START_DATE_TIMEZONE' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right">
					 <div class="cell">
						<span class="cellfont">
                            <input type="hidden" id="service_start_date_timezone_name_0" name="service_start_date_timezone_name_0" >
							<select id="service_start_date_timezone_0" name="service_start_date_timezone_0" tabindex="0" style="width:100%"
                                onchange="litz_change(this.id, 'service_start_date_timezone_name_', 'true')">
                                <option value=""></option>
                                {foreach from=$LIST_TIME_ZONE_LIST key=key item=val}
                                <option data-lbl="{$val.label}" data-cde="{$val.code}" value="{$key}">{$val.name}</option>
                                {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
			   </div>
			   <div class="row margin-0">
                    <div class="col-md-3 cellgrey add-cell-border-right">
					 <div class="cell">
						<span class="cellfont" id="label_service_end_date_0">{sugar_translate label='LBL_SERVICE_END_DATE' module='gc_Line_Item_Contract_History'}</span>
					 </div>
                    </div>
                    <div class="col-md-3 add-cell-border-right">
					 <div class="cell">
						<span class="cellfont dateInput">
							{include file="custom/include/tpl/CustomDateTime.tpl" c_fld="service_end_date" c_view="EditView" c_app = "_0" c_calss = "secondary" c_dependentfield = "service_start_date_0"}
							<span>
							<input id="service_end_date_hidden_0" name="service_end_date_hidden_0" class="datehidden" tabindex="0" size="30" value="" type="text" dependentfield="service_start_date_hidden_0" style="display:none">
							</span>
						</span>
					 </div>
                    </div>
                    <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_service_end_date_timezone_0">{sugar_translate label='LBL_SERVICE_END_DATE_TIMEZONE' module='gc_Line_Item_Contract_History'}</span>
                     </div>
                    </div>
                    <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont">
                            <input type="hidden" id="service_end_date_timezone_name_0" name="service_end_date_timezone_name_0" >
                            <select id="service_end_date_timezone_0" name="service_end_date_timezone_0" tabindex="0" style="width:100%"
                            onchange="litz_change(this.id, 'service_end_date_timezone_name_', 'true')">
                                <option value=""></option>
                                {foreach from=$LIST_TIME_ZONE_LIST key=key item=val}
                                <option data-lbl="{$val.label}" data-cde="{$val.code}" value="{$key}">{$val.name}</option>
                                {/foreach}
                            </select>
                        </span>
                     </div>
                    </div>
			   </div>
			   <div class="row margin-0">
                    <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_billing_start_date_0">{sugar_translate label='LBL_BILLING_START_DATE' module='gc_Line_Item_Contract_History' c_app = "_0"}</span>
                     </div>
                    </div>
                    <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont dateInput">
                            {include file="custom/include/tpl/CustomDateTime.tpl" c_fld="billing_start_date" c_view="EditView" c_app = "_0" c_calss = "primary" c_dependentfield = ""}
                            <span>
                                <input id="billing_start_date_hidden_0" name="billing_start_date_hidden_0" class="datehidden" tabindex="0" size="30" value="" type="text" dependentfield="" style="display:none">
                            </span>
                        </span>
                     </div>
                    </div>
                    <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_billing_start_date_timezone_0">{sugar_translate label='LBL_BILLING_START_DATE_TIMEZONE' module='gc_Line_Item_Contract_History'}</span>
                     </div>
                    </div>
                    <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont">
                            <input type="hidden" id="billing_start_date_timezone_name_0" name="billing_start_date_timezone_name_0" >
                            <select id="billing_start_date_timezone_0" name="billing_start_date_timezone_0" tabindex="0" style="width:100%"
                                onchange="litz_change(this.id, 'billing_start_date_timezone_name_', 'true')">
                                <option value=""></option>
                                {foreach from=$LIST_TIME_ZONE_LIST key=key item=val}
                                <option data-lbl="{$val.label}" data-cde="{$val.code}" value="{$key}">{$val.name}</option>
                                {/foreach}
                            </select>
                        </span>
                     </div>
                    </div>
			   </div>
			   <div class="row margin-0">
                    <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_billing_end_date_0">{sugar_translate label='LBL_BILLING_END_DATE' module='gc_Line_Item_Contract_History'}</span>
                     </div>
                    </div>
                    <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                       <span class="cellfont dateInput">
                            {include file="custom/include/tpl/CustomDateTime.tpl" c_fld="billing_end_date" c_view="EditView" c_app = "_0" c_calss = "secondary" c_dependentfield = "billing_start_date_0"}
                            <span>
                                <input id="billing_end_date_hidden_0" name="billing_end_date_hidden_0" class="datehidden" tabindex="0" size="30" value="" type="text" dependentfield="billing_start_date_hidden_0" style="display:none">
                            </span>
                        </span>
                     </div>
                    </div>
				    <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_billing_end_date_timezone_0">{sugar_translate label='LBL_BILLING_END_DATE_TIMEZONE' module='gc_Line_Item_Contract_History'}</span>
                     </div>
                    </div>
                    <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                       <span class="cellfont">
                            <input type="hidden" id="billing_end_date_timezone_name_0" name="billing_end_date_timezone_name_0" >
                            <select id="billing_end_date_timezone_0" name="billing_end_date_timezone_0" tabindex="0" style="width:100%"
                                onchange="litz_change(this.id, 'billing_end_date_timezone_name_', 'true')">
                                <option value=""></option>
                                {foreach from=$LIST_TIME_ZONE_LIST key=key item=val}
                                <option data-lbl="{$val.label}" data-cde="{$val.code}" value="{$key}">{$val.name}</option>
                                {/foreach}
                            </select>
                        </span>
                     </div>
                    </div>
			   </div>
               <div class="row margin-0">
				  <div class="col-md-3 cellgrey add-cell-border-right">
					 <div class="cell">
						<span class="cellfont" id="label_payment_type_0">{sugar_translate label='LBL_PAYMENT_TYPE' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right">
					 <div class="cell">
						<span class="cellfont">
							<select class="payment_type " id="payment_type_0" name="payment_type_0" tabindex="0"
                                onChange="togglePayemntType(this);changeLineItemBTypeFieldValue(this);" data ="enum">
							{foreach from=$PAYMENTTYPE key=key item=val}
								<option value="{$key}">{$val}</option>
							{/foreach}
							</select>
						</span>
					 </div>
				  </div>
				  <div class="col-md-3 cellgrey add-cell-border-right">
					 <div class="cell" >
						<span class="cellfont">{sugar_translate label='LBL_FACTORY_REFERENCE_NO' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right">
					<div class="cell">
						<span class="cellfont">
							<input id="factory_reference_no_0" name="factory_reference_no_0" tabindex="0" maxlength="{$FIELD_LENGTH.gc_Line_Item_Contract_History.factory_reference_no.len}" size="30" value="" type="text">
						</span>
					 </div>
				  </div>
			   </div>
				<div class="row margin-0">
                <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_contract_line_item_type_0">{sugar_translate label='LBL_CONTRACT_LINE_ITEM_TYPE' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select class="contract_line_item_type" id="contract_line_item_type_0" name="contract_line_item_type_0">
                            {foreach from=$CONTRACT_LINE_ITEM_TYPE key=ckey item=cval}
                                <option value="{$ckey}">{$cval}</option>
                            {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
					     <span class="cellfont" class="cellfont" id="label_gc_organization_0">{sugar_translate label='LBL_BILLING_AFFILIATE' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">{include file="custom/include/tpl/CustomRelateField.tpl" c_fld=","|explode:"gc_organization_id_0,gc_organization_name_0,gc_organization, Organization,0" c_id="" c_name="" c_valid = '0' c_parent = '' c_filter = $BILLAFFILIATEFILTER c_return = 'set_organization_popup_return'}</span>
					 </div>
				  </div>
               </div>
               <div class="row margin-0">
				  <div class="col-md-3 cellgrey add-cell-border-right" style=" height: 57px;">
					 <div class="cell">
						<span class="cellfont" id="label_remark_0">{sugar_translate label='LBL_DESCRIPTION' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right" style=" height: 57px;">
					 <div class="cell">
						<span class="cellfont">
							<textarea id="description_0" name="description_0" tabindex="0" cols="40" rows="1" ></textarea>
						</span>
					 </div>
				  </div>
				  <div class="col-md-3 cellgrey add-cell-border-right " style=" height: 57px;">
					 <!-- <div class="cell">
						<span class="cellfont" id="label_product_name_0">{sugar_translate label='LBL_MODULE_TITLE' module='pi_product_item'}</span>
					 </div>-->
				  </div>
				  <div class="col-md-3 add-cell-border-right " style=" height: 57px;">
					 <div class="cell">
						 <!--<span class="cellfont pm_products_gc_contracts_1pm_products_ida">
						 {include file="custom/include/tpl/CustomRelateField.tpl"
                         c_fld=","|explode:"product_id_0,product_name_0,pi_product_item, Product Item, 0"
                         c_id='' c_name='' c_valid = '0' c_parent = 'pm_products_gc_contracts_1pm_products_ida' c_filter = $INITFILTER
                         c_return = 'set_return' }
						</span>-->
					 </div>
				  </div>
                </div>

			   <div class="panel panel-default subLineItem hide" style="margin-top:15px">
				  <ul class="list-group" style="margin-left:0px">
					 <li class="list-group-item PaymentType DirectDeposit hide" style="margin-left: 0px;">
						<div id="dd_0">
						   <div class="method">
						      <input class="payment_id " id="gc_DirectDeposit_id_0" name="gc_DirectDeposit_id_0" tabindex="0" size="30" value="" type="hidden" data="gc_DirectDeposit">
							  <div class="row margin-0">
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell" >
									   <span class="cellfont" id= "label_dd_bank_code_0">{sugar_translate label='LBL_BANK_CODE' module='gc_DirectDeposit'}</span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
									   <span class="cellfont">
											<input class = "felement" id="dd_bank_code_0" name="dd_bank_code_0" tabindex="0" maxlength="{$FIELD_LENGTH.gc_DirectDeposit.bank_code.len}"
                                                    size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
									   </span>
									</div>
								 </div>
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell">
									   <span class="cellfont" id= "label_dd_branch_code_0">{sugar_translate label='LBL_BRANCH_CODE' module='gc_DirectDeposit'}</span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input class = "" id="dd_branch_code_0" name="dd_branch_code_0"
                                                tabindex="0" maxlength="{$FIELD_LENGTH.gc_DirectDeposit.branch_code.len}"
                                                size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
							  </div>
							  <div class="row margin-0">
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell" >
									   <span class="cellfont" id="label_dd_deposit_type_0">{sugar_translate label='LBL_DEPOSIT_TYPE' module='gc_DirectDeposit'}</span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<select class="dd_deposit_type" id="dd_deposit_type_0" name="dd_deposit_type_0"
                                                tabindex="0" data ="enum" onchange='changeLineItemBTypeFieldValue(this);'>
											{foreach from=$DEPOSITTYPE key=key item=val}
												<option value="{$key}">{$val}</option>
											{/foreach}
											</select>
										</span>
									</div>
								 </div>
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell">
									   <span class="cellfont" id="label_dd_account_no_0">{sugar_translate label='LBL_ACCOUNT_NO' module='gc_Contracts'}</span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input class="" id="dd_account_no_0" name="dd_account_no_0" maxlength="{$FIELD_LENGTH.gc_DirectDeposit.account_no.len}"
                                                    tabindex="0" size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
							  </div>
							  <div class="row margin-0">
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell">
									   <span class="cellfont">{sugar_translate label='LBL_PAYEE_CONTACT_PERSON_NAME_2' module='gc_DirectDeposit'}</span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input id="dd_person_name_2_0" name="dd_person_name_2_0" maxlength="20" tabindex="0" size="30" value="" type="text"
                                                onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
								 <!-- <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell" >
									   <span class="cellfont">{sugar_translate label='LBL_PAYEE_CONTACT_PERSON_NAME_1' module='gc_DirectDeposit'}</span> 
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input id="dd_person_name_1_0" name="dd_person_name_1_0" maxlength="{$FIELD_LENGTH.gc_DirectDeposit.payee_contact_person_name_1.len}"
                                                    tabindex="0" size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>-->
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell" >
									   <span class="cellfont">{sugar_translate label='LBL_PAYEE_TEL_NO' module='gc_DirectDeposit'}</span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input id="dd_payee_tel_no_0" name="dd_payee_tel_no_0" maxlength="{$FIELD_LENGTH.gc_DirectDeposit.payee_tel_no.len}"
                                                    tabindex="0" size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
							  </div>
							  <div class="row margin-0">
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell">
									   <span class="cellfont" id="label_dd_payee_email_address_0">{sugar_translate label='LBL_PAYEE_EMAIL_ADDRESS' module='gc_DirectDeposit'}</span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input id="dd_payee_email_address_0" name="dd_payee_email_address_0" class = "emailadd" maxlength="{$FIELD_LENGTH.gc_DirectDeposit.payee_email_address.len}"
                                                    tabindex="0" size="30" value="" type="text" data="email" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell" >
									   <span class="cellfont">{sugar_translate label='LBL_REMARKS' module='gc_DirectDeposit'}</span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input id="dd_remarks_0" name="dd_remarks_0" maxlength="40" tabindex="0"
                                                    size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
							  </div>
						   </div>
						</div>
					 </li>
					 <li class="list-group-item PaymentType CreditCard hide" style="margin-left: 0px;">
						<div id="dd_0">
						   <div class="method">
							  <input class="payment_id" id="gc_CreditCard_id_0" name="gc_CreditCard_id_0" tabindex="0" size="30" value="" type="hidden" data="gc_CreditCard">
							  <div class="row margin-0">
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell" >
									   <span class="cellfont" id= "label_cc_credit_card_no_0">{sugar_translate label='LBL_CREDIT_CARD_NO' module='gc_CreditCard'}<font color="red">*</font></span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input class = "felement req" id="cc_credit_card_no_0" name="cc_credit_card_no_0" maxlength="{$FIELD_LENGTH.gc_CreditCard.credit_card_no.len}"
                                                    tabindex="0" size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell">
									   <span class="cellfont" id= "label_cc_expiration_date_0">{sugar_translate label='LBL_EXPIRATION_DATE' module='gc_Contracts'}<font color="red">*</font></span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input class = "req" id="cc_expiration_date_0" name="cc_expiration_date_0" maxlength="{$FIELD_LENGTH.gc_CreditCard.expiration_date.len}"
                                                tabindex="0" size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
							  </div>
						   </div>
						</div>
					 </li>
					 <li class="list-group-item PaymentType PostalTransfer hide" style="margin-left: 0px;">
						<div id="dd_0">
						   <div class="method">
							  <input class="payment_id" id="gc_PostalTransfer_id_0" name="gc_PostalTransfer_id_0" tabindex="0" size="30" value="" type="hidden" data="gc_PostalTransfer">
							  <div class="row margin-0">
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell" >
									   <span class="cellfont" id="label_pt_name_0">{sugar_translate label='LBL_NAME' module='gc_PostalTransfer'}<font color="red">*</font></span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input class = "felement req" id="pt_name_0" name="pt_name_0" maxlength="30"
                                                    tabindex="0" size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell">
									   <span class="cellfont" id="label_pt_postal_passbook_mark_0">{sugar_translate label='LBL_POSTAL_PASSBOOK_MARK' module='gc_PostalTransfer'}<font color="red">*</font></span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input class= "req" id="pt_postal_passbook_mark_0" name="pt_postal_passbook_mark_0" maxlength="{$FIELD_LENGTH.gc_PostalTransfer.postal_passbook_mark.len}"
                                                    tabindex="0" size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
							  </div>
							  <div class="row margin-0">
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell" >
									   <span class="cellfont" id="label_pt_postal_passbook_0">{sugar_translate label='LBL_POSTAL_PASSBOOK' module='gc_PostalTransfer'}<font color="red">*</font></span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input class= "req" id="pt_postal_passbook_0" name="pt_postal_passbook_0" maxlength="{$FIELD_LENGTH.gc_PostalTransfer.postal_passbook.len}"
                                                    tabindex="0" size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell">
									   <span class="cellfont" id="label_pt_postal_passbook_no_display_0">{sugar_translate label='LBL_POSTAL_PASSBOOK_NO_DISPLAY' module='gc_PostalTransfer'}<font color="red">*</font></span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<select class="pt_postal_passbook_no_display req"
                                                id="pt_postal_passbook_no_display_0" name="pt_postal_passbook_no_display_0" tabindex="0"
                                                onchange='changeLineItemBTypeFieldValue(this);'>
											{foreach from=$POSTALPASSBOOKDISPLAYLIST key=key item=val}
												<option value="{$key}">{$val}</option>
											{/foreach}
											</select>
										</span>
									</div>
								 </div>
							  </div>
						   </div>
						</div>
					 </li>
					 <li class="list-group-item PaymentType AccountTransfer hide" style="margin-left: 0px;">
						<div id="dd_0">
						   <div class="method">
							  <input class="payment_id" id="gc_AccountTransfer_id_0" name="gc_AccountTransfer_id_0" tabindex="0" size="30" value="" type="hidden" data="gc_AccountTransfer">
							  <div class="row margin-0">
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell" >
									   <span class="cellfont" id="label_at_name_0">{sugar_translate label='LBL_NAME' module='gc_AccountTransfer'}<font color="red">*</font></span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input class = "felement req" id="at_name_0" name="at_name_0" maxlength="30"
                                                tabindex="0" size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell">
									   <span class="cellfont" id="label_at_deposit_type_0">{sugar_translate label='LBL_DEPOSIT_TYPE' module='gc_AccountTransfer'}<font color="red">*</font></span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<select class="at_deposit_type req" id="at_deposit_type_0" name="at_deposit_type_0"
                                                    tabindex="0" data ="enum" onchange='changeLineItemBTypeFieldValue(this);'>
											{foreach from=$DEPOSITTYPE key=key item=val}
												<option value="{$key}">{$val}</option>
											{/foreach}
											</select>
										</span>
									</div>
								 </div>
							  </div>
							  <div class="row margin-0">
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell">
									   <span class="cellfont" id="label_at_bank_code_0">{sugar_translate label='LBL_BANK_CODE' module='gc_AccountTransfer'}<font color="red">*</font></span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input class="req" id="at_bank_code_0" name="at_bank_code_0" maxlength="{$FIELD_LENGTH.gc_AccountTransfer.bank_code.len}"
                                                tabindex="0" size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell" >
									   <span class="cellfont" id="label_at_branch_code_0">{sugar_translate label='LBL_BRANCH_CODE' module='gc_AccountTransfer'}<font color="red">*</font></span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input class="req" id="at_branch_code_0" name="at_branch_code_0" maxlength="{$FIELD_LENGTH.gc_AccountTransfer.branch_code.len}"
                                                tabindex="0" size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
							  </div>
							  <div class="row margin-0">
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell" >
									   <span class="cellfont" id="label_at_account_no_0">{sugar_translate label='LBL_ACCOUNT_NO' module='gc_Contracts'}<font color="red">*</font></span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<input class="req" id="at_account_no_0" name="at_account_no_0" maxlength="{$FIELD_LENGTH.gc_AccountTransfer.account_no.len}"
                                                tabindex="0" size="30" value="" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
									</div>
								 </div>
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
									<div class="cell">
									   <span class="cellfont" id="label_at_account_no_display_0">{sugar_translate label='LBL_ACCOUNT_NO_DISPLAY' module='gc_AccountTransfer'}<font color="red">*</font></span>
									</div>
								 </div>
								 <div class="col-md-3 add-cell-border-right add-cell-border-top">
									<div class="cell">
										<span class="cellfont">
											<select class="at_account_no_display req" id="at_account_no_display_0" name="at_account_no_display_0"
                                                    tabindex="0" onchange='changeLineItemBTypeFieldValue(this);'>
											{foreach from=$ACCOUNTNODISPLAYLIST key=key item=val}
												<option value="{$key}">{$val}</option>
											{/foreach}
											</select>
										</span>
									</div>
								 </div>
							  </div>
						   </div>
						</div>
					 </li>
				  </ul>
			   </div>
			   {assign var=rowid value=0}
               {include file='custom/modules/gc_Contracts/tpl/ProductSpecificationEdit.tpl'}
			</div>
		 </div>
	  </li>
	</ul>
	{*<!-- Line Item Row Copy Start-->*}
	{if $LINEITEM_LIST|@sizeof > 0}
	{foreach from=$LINEITEM_LIST key=k item=v}
    {assign var=rowid value=$i++}
    <input type="hidden" name="li_deleted_{$rowid}" id="li_deleted_{$rowid}" value="0">
    <input type="hidden" name="li_prod_specification_changed_{$rowid}" id="li_prod_specification_changed_{$rowid}" value="0">
    <input type="hidden" name="li_prod_configuration_changed_{$rowid}" id="li_prod_configuration_changed_{$rowid}" value="0">
    <ul class="list-group LineItem" style="margin-left:0px" id="container_lineitem_{$rowid}" >
      <li class="list-group-item" style="margin-left: 0px;">
		 <div class="row" id="dropdown-sr_{$rowid}">
		   <div class="col-xs-10">
			  <strong><span class="lineno" id="lineno_{$rowid}">{sugar_translate label='LBL_LINEITEM_DETAILS' module='gc_Contracts'}</span></strong>
			  <input class ="line_item_id" id="line_item_id_{$rowid}" name="line_item_id_{$rowid}" type="hidden" value="{$v.line_item_id}">
			  <input class ="line_item_history_id" id="line_item_history_id_{$rowid}" name="line_item_history_id_{$rowid}" type="hidden" value="{$k}">
              <input id="line_item_parent_id_{$rowid}" name="line_item_parent_id_{$rowid}" type="hidden" value="{$v.line_item_id}">
			  <input id="line_item_history_parent_id_{$rowid}" name="line_item_history_parent_id_{$rowid}" type="hidden" value="{$k}">
			  <input class ="line_item_number" id="line_item_number_{$rowid}" name="line_item_number_{$rowid}" type="hidden" value="{$rowid}">
		   </div>

		   <div class="col-xs-2">
               {if $IS_CHANGE_AVAILABLE }
		   			<button type="button" id="change_contract_{$rowid}" class="button" style="display:{if $v.contract_line_item_type != $LI_STATUS_TO_DISP_CHANGE} none {/if}" title="Change" onclick="makeLineItemToChange('{$rowid}')">Change</button>
		   			&nbsp;
		   			<input type="button" onclick="cancelChangeLineItem('{$rowid}')" title="Cancel Changes" class="button" name="button" value="Cancel" id="cancel_change_line_item_{$rowid}" style="display:{if $v.contract_line_item_type != 'Change'}none{/if}">
		   		    &nbsp;
		   		    <input type="button" onclick="terminateLineItem('{$rowid}')" title="Terminate" class="button" name="button" value="Terminate" id="terminate_line_item_{$rowid}" style="display:{if $v.contract_line_item_type == $LI_STATUS_TO_DISP_TERMINATE OR $v.contract_line_item_type == 'New' OR $v.contract_line_item_type == 'Change'}none{/if}">
		   			&nbsp;
		   			<input type="button" onclick="cancelTerminateLineItem('{$rowid}')" title="Cancel Termination" class="button" name="button" value="Cancel" id="cancel_terminate_line_item_{$rowid}" style="display:{if $v.contract_line_item_type == $LI_STATUS_TO_DISP_TERMINATE}{else}none{/if}">
				{/if}
           {if empty($APPROVER_USER) and (($IS_CHANGE_AVAILABLE and $v.contract_line_item_type == 'New') || $IS_CHANGE_AVAILABLE !='1')}
			  <button type="button" class="close " id="btn_li_remove_{$rowid}" data-dismiss="modal" aria-hidden="true" onclick="removeLineItem(this);">&times;</button>
		   {/if}
           </div>
		 </div>
         <div id="li_{$rowid}">
            <div class="method">
			   <div class="row margin-0 hide">
                  <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                     <div class="cell">
                        <span class="cellfont">{sugar_translate label='LBL_GLOBAL_CONTRACT_ITEM_ID' module='gc_Contracts'}</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right add-cell-border-top">
                     <div class="cell">
                         <span class="cellfont">
							<span class="sugar_field" id="id">{$v.global_contract_item_id}</span>
						</span>
                     </div>
                  </div>
				  <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                     <div class="cell" >
                        <span class="cellfont">{sugar_translate label='LBL_OBJECT_ID' module='gc_Contracts'}</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right add-cell-border-top">
                    <div class="cell">
                        <span class="cellfont">
							<span class="sugar_field line_item_objid" id="line_item_{$rowid}">{$v.line_item_id}</span>
						</span>
                     </div>
                  </div>
               </div>
			   <div class="row margin-0">
                  <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                     <div class="cell">
                        <span class="cellfont" id="label_product_specification_{$rowid}">{sugar_translate label='LBL_PRODUCT_SPECIFICATION' module='gc_Line_Item_Contract_History'}<font color="red">*</font></span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right add-cell-border-top">
                     <div class="cell">
                        <span class="cellfont" id="div_product_spec_{$rowid}">
                            <select style="width:100%" name="product_specification_{$rowid}" id="product_specification_{$rowid}">
								<option value="{$v.product_specification_id}">{$v.product_specification}</option>
							</select>
						</span>
                     </div>
                  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell">
						<span class="cellfont" id="label_product_name_{$rowid}">{sugar_translate label='LBL_INVOICE_SUBTOT_GROUP' module='gc_Contracts'}</span>
					 </div>
				  </div>
                  <div class="col-md-3 add-cell-border-right add-cell-border-top">
                    <div class="col-md-8" style="padding:0px; min-height:40px!important;">
                        <div class="cell">
                        <span class="cellfont">
							<select  style="width:100%" name="li_invoice_subgroup_{$rowid}" id="li_invoice_subgroup_{$rowid}" style="width: 80%!important">
								<option value=""></option>
							</select>
                            <input type="hidden" name="hid_li_invoice_subgroup_{$rowid}" id="hid_li_invoice_subgroup_{$rowid}" value="{$v.group_id}">
						</span>
                        </div>
                     </div>
                     <div class="col-md-4" style= "padding:0px;  min-height:40px!important;">
                      <div class="cell">
                        <span class="cellfont" >
                        <input type="button" id="add_invoice_subgroup_{$rowid}" value="Add" name="button" class="button primary" title="Add" onclick="createIGDialog(this)">
                        </span>
                      </div>
                     </div>
				  </div>
               </div>
               <div class="row margin-0">
                <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_cust_contract_currency_{$rowid}">{sugar_translate label='LBL_CUST_CONTRACT_CURRENCY' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select id="cust_contract_currency_{$rowid}" name="cust_contract_currency_{$rowid}" style="width:100%"
                                    onchange='product_currency_change(this.id);changeLineItemBTypeFieldValue(this);'>
                                <option value=""></option>
                                    {foreach from=$DEFAULT_CURRENCIES key=key item=val}
                                        <option value="{$key}" {if $val == $v.cust_contract_currency} selected {/if}>{$val}</option>
                                    {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_cust_billing_currency_{$rowid}">{sugar_translate label='LBL_CUST_BILLING_CURRENCY' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select id="cust_billing_currency_{$rowid}" name="cust_billing_currency_{$rowid}" style="width:100%"
                                    onchange='changeLineItemBTypeFieldValue(this);' >
                                <option value=""></option>
                                    {foreach from=$DEFAULT_CURRENCIES key=key item=val}
                                        <option value="{$key}" {if $val == $v.cust_billing_currency} selected {/if}>{$val}</option>
                                    {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
               </div>
               <div class="row margin-0">
               <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_igc_contract_currency_{$rowid}">{sugar_translate label='LBL_IGC_CONTRACT_CURRENCY' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select id="igc_contract_currency_{$rowid}" name="igc_contract_currency_{$rowid}"  style="width:100%"
                                        onchange='changeLineItemBTypeFieldValue(this);'>
                                <option value=""></option>
                                    {foreach from=$DEFAULT_CURRENCIES key=key item=val}
                                        <option value="{$key}" {if $val == $v.igc_contract_currency} selected {/if} >{$val}</option>
                                    {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_igc_billing_currency_{$rowid}">{sugar_translate label='LBL_IGC_BILLING_CURRENCY' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select id="igc_billing_currency_{$rowid}" name="igc_billing_currency_{$rowid}"  style="width:100%"
                                    onchange='changeLineItemBTypeFieldValue(this);'>
                                <option value=""></option>
                                    {foreach from=$DEFAULT_CURRENCIES key=key item=val}
                                        <option value="{$key}" {if $val == $v.igc_billing_currency} selected {/if} >{$val}</option>
                                    {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
               </div>
               <div class="row margin-0">
                <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_customer_billing_method_{$rowid}">{sugar_translate label='LBL_CUSTOMER_BILLING_METHOD' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select id="customer_billing_method_{$rowid}" name="customer_billing_method_{$rowid}" style="width:100%"
                                        onchange='changeLineItemBTypeFieldValue(this);'>
                            {foreach from=$CUSTOMER_BILLING_METHODS key=key item=val}
                                <option value="{$key}" {if $val == $v.customer_billing_method} selected {/if}>{$val}</option>
                            {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_igc_settlement_method_{$rowid}">{sugar_translate label='LBL_IGC_SETTLEMENT_METHOD' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select id="igc_settlement_method_{$rowid}" name="igc_settlement_method_{$rowid}" style="width:100%"
                                    onchange='changeLineItemBTypeFieldValue(this);'>
                            {foreach from=$IGC_SETTLEMENT_METHODS key=key item=val}
                                <option value="{$key}" {if $val == $v.igc_settlement_method} selected {/if}>{$val}</option>
                            {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
               </div>
               <div class="row margin-0">
                  <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_tech_account_name_{$rowid}">{sugar_translate label='LBL_TECH_ACCOUNT_ID' module='gc_Contracts'}</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont">
							{include file="custom/include/tpl/CustomRelateField.tpl" c_fld=","|explode:"tech_account_id_$rowid,tech_account_name_$rowid,Contacts, Accounts,0" c_id=$v.tech_account_id c_name=$v.tech_account_name c_valid = '0' c_parent = '' c_filter = '' c_return = 'set_contacts_popup_return'
                            c_clear = 'clear_custom_popup'}
						</span>
                        <input type="hidden" name="tech_account_countryid_{$rowid}" id="tech_account_countryid_{$rowid}" value="{$v.tech_account_country}">
                     </div>
                  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_bill_account_name_{$rowid}">{sugar_translate label='LBL_BILL_ACOOUNT_ID' module='gc_Contracts'}</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont">
							{include file="custom/include/tpl/CustomRelateField.tpl" c_fld=","|explode:"bill_account_id_$rowid,bill_account_name_$rowid,Contacts, Accounts,0" c_id=$v.bill_account_id c_name=$v.bill_account_name c_valid = '0' c_parent = '' c_filter = '' c_return = 'set_contacts_popup_return'}
						</span>
                        <input type="hidden" name="bill_account_countryid_{$rowid}" id="bill_account_countryid_{$rowid}" value="{$v.bill_account_country}">
                     </div>
                  </div>
               </div>
               <div class="row margin-0">
                  <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell" >
                        <span class="cellfont" id="label_service_start_date_{$rowid}">{sugar_translate label='LBL_SERVICE_START_DATE' module='gc_Line_Item_Contract_History'}</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont dateInput">
							{include file="custom/include/tpl/CustomDateTime.tpl" c_fld="service_start_date" c_view="EditView" c_app = "_$rowid" c_calss = "primary" c_dependentfield = ""}
							<span>
								<input id="service_start_date_hidden_{$rowid}" name="service_start_date_hidden_{$rowid}" class="datehidden" tabindex="0" size="30" value="{$v.service_start_date}" type="text" dependentfield="" style="display:none">
							</span>
						</span>
                     </div>
                  </div>
                   <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell" >
                        <span class="cellfont" id="label_service_start_date_timezone_{$rowid}">{sugar_translate label='LBL_SERVICE_START_DATE_TIMEZONE' module='gc_Line_Item_Contract_History'}</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont">
                            <input type="hidden" id="service_start_date_timezone_name_{$rowid}" name="service_start_date_timezone_name_{$rowid}" >
							<select id="service_start_date_timezone_{$rowid}" name="service_start_date_timezone_{$rowid}" tabindex="0" style="width:100%"
                            onchange="litz_change(this.id, 'service_start_date_timezone_name_', 'true')">
                                <option value=""></option>
                                {foreach from=$LIST_TIME_ZONE_LIST key=key item=val}
                                <option data-lbl="{$val.label}" data-cde="{$val.code}" value="{$key}" {if $key == $v.service_start_date_timezone_id} selected {/if}>{$val.name}</option>
                                {/foreach}
                            </select>
						</span>
                     </div>
                  </div>
               </div>
               <div class="row margin-0">
                    <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_service_end_date_{$rowid}">{sugar_translate label='LBL_SERVICE_END_DATE' module='gc_Line_Item_Contract_History'}</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont dateInput">
							{include file="custom/include/tpl/CustomDateTime.tpl" c_fld="service_end_date" c_view="EditView" c_app = "_$rowid" c_calss = "secondary" c_dependentfield = "service_start_date_$rowid"}
							<span>
								<input id="service_end_date_hidden_{$rowid}" name="service_end_date_hidden_{$rowid}" class="datehidden" tabindex="0" size="30" value="{$v.service_end_date}" type="text" dependentfield="service_start_date_hidden_{$rowid}" style="display:none">
							</span>
						</span>
                     </div>
                  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_service_end_date_timezone_{$rowid}">{sugar_translate label='LBL_SERVICE_END_DATE_TIMEZONE' module='gc_Line_Item_Contract_History'}</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont">
                            <input type="hidden" id="service_end_date_timezone_name_{$rowid}" name="service_end_date_timezone_name_{$rowid}" >
							<select id="service_end_date_timezone_{$rowid}" name="service_end_date_timezone_{$rowid}" tabindex="0" style="width:100%"
                                    onchange="litz_change(this.id, 'service_end_date_timezone_name_' , 'true')">
                                <option value=""></option>
                                {foreach from=$LIST_TIME_ZONE_LIST key=key item=val}
                                <option data-lbl="{$val.label}" data-cde="{$val.code}" value="{$key}" {if $key == $v.service_end_date_timezone_id} selected {/if}>{$val.name}</option>
                                {/foreach}
                            </select>
						</span>
                     </div>
                  </div>
               </div>
			   <div class="row margin-0">
                <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_billing_start_date_{$rowid}">{sugar_translate label='LBL_BILLING_START_DATE' module='gc_Line_Item_Contract_History'}</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont dateInput">
							{include file="custom/include/tpl/CustomDateTime.tpl" c_fld="billing_start_date" c_view="EditView" c_app = "_$rowid" c_calss = "primary" c_dependentfield = ""}
							<span>
								<input id="billing_start_date_hidden_{$rowid}" name="billing_start_date_hidden_{$rowid}" class="datehidden" tabindex="0" size="30" value="{$v.billing_start_date}" type="text" dependentfield="" style="display:none">
							</span>
						</span>
                     </div>
                    </div>
                    <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_billing_start_date_timezone_{$rowid}">{sugar_translate label='LBL_BILLING_START_DATE_TIMEZONE' module='gc_Line_Item_Contract_History'}</span>
                     </div>
                    </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont">
                             <input type="hidden" id="billing_start_date_timezone_name_{$rowid}" name="billing_start_date_timezone_name_{$rowid}" >
                            <select id="billing_start_date_timezone_{$rowid}" name="billing_start_date_timezone_{$rowid}" tabindex="0" style="width:100%"
                                    onchange="litz_change(this.id, 'billing_start_date_timezone_name_', 'true')">
                                <option value=""></option>
                                {foreach from=$LIST_TIME_ZONE_LIST key=key item=val}
                                <option data-lbl="{$val.label}" data-cde="{$val.code}" value="{$key}" {if $key == $v.billing_start_date_timezone_id} selected {/if}>{$val.name}</option>
                                {/foreach}
                            </select>
						</span>
                     </div>
                  </div>
               </div>
               <div class="row margin-0">
                     <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_billing_end_date_{$rowid}">{sugar_translate label='LBL_BILLING_END_DATE' module='gc_Line_Item_Contract_History'}</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                       <span class="cellfont dateInput">
							{include file="custom/include/tpl/CustomDateTime.tpl" c_fld="billing_end_date" c_view="EditView" c_app = "_$rowid" c_calss = "secondary" c_dependentfield = "billing_start_date_$rowid"}
							<span>
                                <input id="billing_end_date_hidden_{$rowid}" name="billing_end_date_hidden_{$rowid}" class="datehidden" tabindex="0" size="30" value="{$v.billing_end_date}" type="text" dependentfield="billing_start_date_hidden_{$rowid}" style="display:none">
							</span>
						</span>
                     </div>
                  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_billing_end_date_timezone_{$rowid}">{sugar_translate label='LBL_BILLING_END_DATE_TIMEZONE' module='gc_Line_Item_Contract_History'}</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                       <span class="cellfont">
                            <input type="hidden" id="billing_end_date_timezone_name_{$rowid}" name="billing_end_date_timezone_name_{$rowid}" >
                            <select id="billing_end_date_timezone_{$rowid}" name="billing_end_date_timezone_{$rowid}" tabindex="0" style="width:100%"
                                onchange="litz_change(this.id, 'billing_end_date_timezone_name_', 'true')">
                                <option value=""></option>
                                {foreach from=$LIST_TIME_ZONE_LIST key=key item=val}
                                <option data-lbl="{$val.label}" data-cde="{$val.code}" value="{$key}" {if $key == $v.billing_end_date_timezone_id} selected {/if}>{$val.name}</option>
                                {/foreach}
                            </select>
						</span>
                     </div>
                  </div>
               </div>
			   <div class="row margin-0">
                  <div class="col-md-3 cellgrey add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont" id="label_payment_type_{$rowid}">{sugar_translate label='LBL_PAYMENT_TYPE' module='gc_Line_Item_Contract_History'}</span>
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right">
                     <div class="cell">
                        <span class="cellfont">
							<select class="payment_type" id="payment_type_{$rowid}" name="payment_type_{$rowid}" tabindex="0"
                                    onChange="togglePayemntType(this);changeLineItemBTypeFieldValue(this);" data ="enum">
							{foreach from=$PAYMENTTYPE key=key item=val}
								<option value="{$key}" {if $val == $v.payment_type_value} selected {/if}>{$val}</option>
							{/foreach}
							</select>
						</span>
                     </div>
                  </div>
				  <div class="col-md-3 cellgrey add-cell-border-right">
					 <div class="cell" >
						<span class="cellfont">{sugar_translate label='LBL_FACTORY_REFERENCE_NO' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right">
					<div class="cell">
						<span class="cellfont">
							<input id="factory_reference_no_{$rowid}" name="factory_reference_no_{$rowid}" tabindex="0" maxlength="{$FIELD_LENGTH.gc_Line_Item_Contract_History.factory_reference_no.len}" size="30" value="{$v.factory_reference_no}" type="text">
						</span>
					 </div>
				  </div>
               </div>
				<div class="row margin-0">
                <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_contract_line_item_type_0">{sugar_translate label='LBL_CONTRACT_LINE_ITEM_TYPE' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">
							<select class="contract_line_item_type" id="contract_line_item_type_{$rowid}" name="contract_line_item_type_{$rowid}">
                            {foreach from=$CONTRACT_LINE_ITEM_TYPE key=ckey item=cval}
                                <option value="{$ckey}" {if $ckey == $v.contract_line_item_type} selected {/if}>{$cval}</option>
                            {/foreach}
                            </select>
						</span>
					 </div>
				  </div>
                  <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
					 <div class="cell" >
						<span class="cellfont" id="label_organization_{$rowid}">{sugar_translate label='LBL_BILLING_AFFILIATE' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right add-cell-border-top">
					<div class="cell">
						<span class="cellfont">{include file="custom/include/tpl/CustomRelateField.tpl" c_fld=","|explode:"gc_organization_id_$rowid,gc_organization_name_$rowid,gc_organization, Organization,0" c_id=$v.gc_organization_id c_name=$v.gc_organization_name c_valid = '0' c_parent = '' c_filter = $BILLAFFILIATEFILTER c_return = 'set_organization_popup_return'}</span>
					 </div>
				  </div>
               </div>
               <div class="row margin-0">
				  <div class="col-md-3 cellgrey add-cell-border-right" style=" height: 57px;">
					 <div class="cell">
						<span class="cellfont" id="label_remark_{$rowid}">{sugar_translate label='LBL_DESCRIPTION' module='gc_Line_Item_Contract_History'}</span>
					 </div>
				  </div>
				  <div class="col-md-3 add-cell-border-right">
					 <div class="cell">
						<span class="cellfont">
							<textarea id="description_{$rowid}" name="description_{$rowid}" tabindex="0" cols="40" rows="1" >{$v.description}</textarea>
						</span>
					 </div>
				  </div>
                   <div class="col-md-3 cellgrey add-cell-border-right " style=" height: 57px;">
                     <div class="cell">
                        <!--<span class="cellfont" id="label_product_name_{$rowid}">{sugar_translate label='LBL_MODULE_TITLE' module='pi_product_item'}</span>-->
                     </div>
                  </div>
                  <div class="col-md-3 add-cell-border-right ">
                     <div class="cell">
                         <!--<span class="cellfont pm_products_gc_contracts_1pm_products_ida">
						 {include file="custom/include/tpl/CustomRelateField.tpl"
                         c_fld=","|explode:"product_id_$rowid,product_name_$rowid,pi_product_item, Product Item,0"
                         c_id=$v.products_id c_name=$v.products_name c_valid = '1' c_parent = 'pm_products_gc_contracts_1pm_products_ida'
                         c_filter = $INITFILTER c_return = 'set_return' }
						</span>-->
                     </div>
                  </div>
                </div>

               <div class="panel panel-default subLineItem hide" style="margin-top:15px">
                  <ul class="list-group" style="margin-left:0px">
					 {assign var=did value= "deposit_deposit_details"}
                     <li class="list-group-item PaymentType DirectDeposit hide" style="margin-left: 0px;">
                        <div id="dd_{$rowid}">
                           <div class="method">
                              <div class="row margin-0">
								 <input class="payment_id gc_DirectDeposit_id" id="gc_DirectDeposit_id_{$rowid}" name="gc_DirectDeposit_id_{$rowid}" tabindex="0" size="30" value="{$v.$did.id}" type="hidden" data="gc_DirectDeposit">
                                 <input class="payment_id " id="gc_DirectDeposit_parent_id_{$rowid}" name="gc_DirectDeposit_parent_id_{$rowid}" tabindex="0" size="30" value="{$v.$did.id}" type="hidden" data="gc_DirectDeposit">
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell" >
                                       <span class="cellfont" id="label_dd_bank_code_{$rowid}">{sugar_translate label='LBL_BANK_CODE' module='gc_DirectDeposit'}</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
                                       <span class="cellfont">
											<input class = "felement" id="dd_bank_code_{$rowid}" name="dd_bank_code_{$rowid}" maxlength="{$FIELD_LENGTH.gc_DirectDeposit.bank_code.len}"
                                                    tabindex="0" size="30" value="{$v.$did.bank_code}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
									   </span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell">
                                       <span class="cellfont" id="label_dd_branch_code_{$rowid}">{sugar_translate label='LBL_BRANCH_CODE' module='gc_DirectDeposit'}</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input class="" id="dd_branch_code_{$rowid}" name="dd_branch_code_{$rowid}" maxlength="{$FIELD_LENGTH.gc_DirectDeposit.branch_code.len}"
                                                    tabindex="0" size="30" value="{$v.$did.branch_code}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                              </div>
                              <div class="row margin-0">
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell" >
                                       <span class="cellfont" id="label_dd_deposit_type_{$rowid}">{sugar_translate label='LBL_DEPOSIT_TYPE' module='gc_DirectDeposit'}</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<select class="dd_deposit_type" id="dd_deposit_type_{$rowid}" name="dd_deposit_type_{$rowid}"
                                                    tabindex="0" data ="enum" onchange='changeLineItemBTypeFieldValue(this);'>
											{foreach from=$DEPOSITTYPE key=key item=val}
												<option value="{$key}" {if $val == $v.$did.deposit_type} selected {/if}>{$val}</option>
											{/foreach}
											</select>
										</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell">
                                       <span class="cellfont" id="label_dd_account_no_{$rowid}">{sugar_translate label='LBL_ACCOUNT_NO' module='gc_Contracts'}</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input class="" id="dd_account_no_{$rowid}" name="dd_account_no_{$rowid}" maxlength="{$FIELD_LENGTH.gc_DirectDeposit.account_no.len}"
                                                    tabindex="0" size="30" value="{$v.$did.account_no}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                              </div>
                              <div class="row margin-0">
                                 <!-- <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell" >
                                       <span class="cellfont">{sugar_translate label='LBL_PAYEE_CONTACT_PERSON_NAME_1' module='gc_DirectDeposit'}</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input id="dd_person_name_1_{$rowid}" name="dd_person_name_1_{$rowid}" maxlength="{$FIELD_LENGTH.gc_DirectDeposit.payee_contact_person_name_1.len}"
                                                    tabindex="0" size="30" value="{$v.$did.person_name_1}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>-->
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell">
                                       <span class="cellfont">{sugar_translate label='LBL_PAYEE_CONTACT_PERSON_NAME_2' module='gc_DirectDeposit'}</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input id="dd_person_name_2_{$rowid}" name="dd_person_name_2_{$rowid}" maxlength="20"
                                                    tabindex="0" size="30" value="{$v.$did.person_name_2}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell" >
                                       <span class="cellfont">{sugar_translate label='LBL_PAYEE_TEL_NO' module='gc_DirectDeposit'}</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input id="dd_payee_tel_no_{$rowid}" name="dd_payee_tel_no_{$rowid}" maxlength="{$FIELD_LENGTH.gc_DirectDeposit.payee_tel_no.len}"
                                                    tabindex="0" size="30" value="{$v.$did.payee_tel_no}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                              </div>
                              <div class="row margin-0">

                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell">
                                       <span class="cellfont" id="label_dd_payee_email_address_{$rowid}">{sugar_translate label='LBL_PAYEE_EMAIL_ADDRESS' module='gc_DirectDeposit'}</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input id="dd_payee_email_address_{$rowid}" name="dd_payee_email_address_{$rowid}" class = "emailadd" maxlength="{$FIELD_LENGTH.gc_DirectDeposit.payee_email_address.len}"
                                                    tabindex="0" size="30" value="{$v.$did.payee_email_address}" type="text" data="email" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell" >
                                       <span class="cellfont">{sugar_translate label='LBL_REMARKS' module='gc_DirectDeposit'}</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input id="dd_remarks_{$rowid}" name="dd_remarks_{$rowid}" maxlength="40" tabindex="0" size="30"
                                                value="{$v.$did.remarks}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                              </div>

                           </div>
                        </div>
                     </li>
					 {assign var=did value= "credit_card_details"}
					 <li class="list-group-item PaymentType CreditCard hide" style="margin-left: 0px;">
                        <div id="dd-{$rowid}">
                           <div class="method">
							  <input class="payment_id gc_CreditCard_id" id="gc_CreditCard_id_{$rowid}" name="gc_CreditCard_id_{$rowid}" maxlength="{$FIELD_LENGTH.gc_CreditCard.credit_card_no.len}" tabindex="0" size="30" value="{$v.$did.id}" type="hidden" data="gc_CreditCard">
                              <input class="payment_id" id="gc_CreditCard_parent_id_{$rowid}" name="gc_CreditCard_parent_id_{$rowid}" maxlength="{$FIELD_LENGTH.gc_CreditCard.credit_card_no.len}" tabindex="0" size="30" value="{$v.$did.id}" type="hidden" data="gc_CreditCard">
                              <div class="row margin-0">
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell" >
                                       <span class="cellfont" id="label_cc_credit_card_no_{$rowid}">{sugar_translate label='LBL_CREDIT_CARD_NO' module='gc_CreditCard'}<font color="red">*</font></span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input class = "felement req" id="cc_credit_card_no_{$rowid}" name="cc_credit_card_no_{$rowid}" maxlength="{$FIELD_LENGTH.gc_CreditCard.credit_card_no.len}"
                                                    tabindex="0" size="30" value="{$v.$did.credit_card_no}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell">
                                       <span class="cellfont" id="label_cc_expiration_date_{$rowid}">{sugar_translate label='LBL_EXPIRATION_DATE' module='gc_Contracts'}<font color="red">*</font></span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input class="req" id="cc_expiration_date_{$rowid}" name="cc_expiration_date_{$rowid}" tabindex="0" size="30" value="{$v.$did.expiration_date}"
                                                    maxlength="{$FIELD_LENGTH.gc_CreditCard.expiration_date.len}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </li>
					 {assign var=did value= "postal_trans_details"}
					 <li class="list-group-item PaymentType PostalTransfer hide" style="margin-left: 0px;">
                        <div id="dd-{$rowid}">
                           <div class="method">
							  <input class="payment_id gc_PostalTransfer_id" id="gc_PostalTransfer_id_{$rowid}" name="gc_PostalTransfer_id_{$rowid}" tabindex="0" size="30" value="{$v.$did.id}" type="hidden" data="gc_PostalTransfer">
                              <input class="payment_id " id="gc_PostalTransfer_parent_id_{$rowid}" name="gc_PostalTransfer_parent_id_{$rowid}" tabindex="0" size="30" value="{$v.$did.id}" type="hidden" data="gc_PostalTransfer">
                              <div class="row margin-0">
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell" >
                                       <span class="cellfont" id="label_pt_name_{$rowid}">{sugar_translate label='LBL_NAME' module='gc_PostalTransfer'}<font color="red">*</font></span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input class = "felement req" id="pt_name_{$rowid}" name="pt_name_{$rowid}" maxlength="30" tabindex="0" size="30" value="{$v.$did.name}" type="text"
                                                    onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell">
                                       <span class="cellfont" id="label_pt_postal_passbook_mark_{$rowid}">{sugar_translate label='LBL_POSTAL_PASSBOOK_MARK' module='gc_PostalTransfer'}<font color="red">*</font></span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input class="req" id="pt_postal_passbook_mark_{$rowid}" name="pt_postal_passbook_mark_{$rowid}" maxlength="{$FIELD_LENGTH.gc_PostalTransfer.postal_passbook_mark.len}"
                                                    tabindex="0" size="30" value="{$v.$did.postal_passbook_mark}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                              </div>
							  <div class="row margin-0">
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell" >
                                       <span class="cellfont" id="label_pt_postal_passbook_{$rowid}">{sugar_translate label='LBL_POSTAL_PASSBOOK' module='gc_PostalTransfer'}<font color="red">*</font></span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input class="req" id="pt_postal_passbook_{$rowid}" name="pt_postal_passbook_{$rowid}" maxlength="{$FIELD_LENGTH.gc_PostalTransfer.postal_passbook.len}"
                                                    tabindex="0" size="30" value="{$v.$did.postal_passbook}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell">
                                       <span class="cellfont" id="label_pt_postal_passbook_no_display_{$rowid}">{sugar_translate label='LBL_POSTAL_PASSBOOK_NO_DISPLAY' module='gc_PostalTransfer'}<font color="red">*</font></span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<select class="pt_postal_passbook_no_display req" id="pt_postal_passbook_no_display_{$rowid}" name="pt_postal_passbook_no_display_{$rowid}"
                                                    onchange='changeLineItemBTypeFieldValue(this);' tabindex="0">
											{foreach from=$POSTALPASSBOOKDISPLAYLIST key=key item=val}
												<option value="{$key}" {if $v.$did.postal_passbook_no_display == $val} selected {/if} >{$val}</option>
											{/foreach}
											</select>
										</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </li>
					 {assign var=did value= "acc_trans_details"}
					 <li class="list-group-item PaymentType AccountTransfer hide" style="margin-left: 0px;">
                        <div id="dd-{$rowid}">
                           <div class="method">
							  <input class="payment_id gc_AccountTransfer_id" id="gc_AccountTransfer_id_{$rowid}" name="gc_AccountTransfer_id_{$rowid}" tabindex="0" size="30" value="{$v.$did.id}" type="hidden" data="gc_AccountTransfer">
                              <input class="payment_id" id="gc_AccountTransfer_parent_id_{$rowid}" name="gc_AccountTransfer_parent_id_{$rowid}" tabindex="0" size="30" value="{$v.$did.id}" type="hidden" data="gc_AccountTransfer">
                              <div class="row margin-0">
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell" >
                                       <span class="cellfont" id="label_at_name_{$rowid}">{sugar_translate label='LBL_NAME' module='gc_AccountTransfer'}<font color="red">*</font></span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input class = "felement req" id="at_name_{$rowid}" name="at_name_{$rowid}" maxlength="30" tabindex="0"
                                                    size="30" value="{$v.$did.name}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell">
                                       <span class="cellfont" id="label_at_deposit_type_{$rowid}">{sugar_translate label='LBL_DEPOSIT_TYPE' module='gc_AccountTransfer'}<font color="red">*</font></span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<select class="at_deposit_type req" id="at_deposit_type_{$rowid}" name="at_deposit_type_{$rowid}"
                                                    tabindex="0" data ="enum" onchange='changeLineItemBTypeFieldValue(this);'>
											{foreach from=$DEPOSITTYPE key=key item=val}
												<option value="{$key}" {if $val == $v.$did.deposit_type} selected {/if}>{$val}</option>
											{/foreach}
											</select>
										</span>
                                    </div>
                                 </div>
                              </div>
							  <div class="row margin-0">
								 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell">
                                       <span class="cellfont" id="label_at_bank_code_{$rowid}">{sugar_translate label='LBL_BANK_CODE' module='gc_AccountTransfer'}<font color="red">*</font></span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input class="req" id="at_bank_code_{$rowid}" name="at_bank_code_{$rowid}" maxlength="{$FIELD_LENGTH.gc_AccountTransfer.bank_code.len}"
                                                    tabindex="0" size="30" value="{$v.$did.bank_code}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell" >
                                       <span class="cellfont" id="label_at_branch_code_{$rowid}">{sugar_translate label='LBL_BRANCH_CODE' module='gc_AccountTransfer'}<font color="red">*</font></span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input class="req" id="at_branch_code_{$rowid}" name="at_branch_code_{$rowid}" maxlength="{$FIELD_LENGTH.gc_AccountTransfer.branch_code.len}"
                                                    tabindex="0" size="30" value="{$v.$did.branch_code}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                              </div>
							  <div class="row margin-0">
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell" >
                                       <span class="cellfont" id="label_at_account_no_{$rowid}">{sugar_translate label='LBL_ACCOUNT_NO' module='gc_Contracts'}<font color="red">*</font></span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<input class="req" id="at_account_no_{$rowid}" name="at_account_no_{$rowid}" maxlength="{$FIELD_LENGTH.gc_AccountTransfer.account_no.len}"
                                                    tabindex="0" size="30" value="{$v.$did.account_no}" type="text" onblur='changeLineItemBTypeFieldValue(this);'>
										</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
                                    <div class="cell">
                                       <span class="cellfont" id="label_at_account_no_display_{$rowid}">{sugar_translate label='LBL_ACCOUNT_NO_DISPLAY' module='gc_AccountTransfer'}<font color="red">*</font></span>
                                    </div>
                                 </div>
                                 <div class="col-md-3 add-cell-border-right add-cell-border-top">
                                    <div class="cell">
										<span class="cellfont">
											<select class="at_account_no_display req" id="at_account_no_display_{$rowid}" name="at_account_no_display_{$rowid}"
                                                        tabindex="0" onchange='changeLineItemBTypeFieldValue(this);'>
											{foreach from=$ACCOUNTNODISPLAYLIST key=key item=val}
												<option value="{$key}" {if $v.$did.account_no_display == $val} selected {/if} >{$val}</option>
											{/foreach}
											</select>
										</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </li>
                  </ul>
               </div>
			   {include file='custom/modules/gc_Contracts/tpl/ProductSpecificationEdit.tpl'}
            </div>
         </div>
      </li>
    </ul>
   {/foreach}
   {/if}
</div>
<div class="panel">
    {if empty($APPROVER_USER)}
	  <button type="button" class="button pull-right" id="add_line_item" style="padding:3px 9px" onclick="addLineItem('1')"><i class="glyphicon-plus"></i> ADD</button>
    {/if}
</div>
