<div class="panel panel-default product_spec_subpanels " id="product_spec_subpanels_{$rowid}" style="margin-top:15px">
<ul class="list-group" style="margin-left:0px">
    <!-- product_config sub panel start -->
    <li class="list-group-item product_config "  style="margin-left: 0px;">
    <div id="container_product_config">
       <div class="method">
          <div class="row margin-0">
            <div class="col-md-12  cellgrey cellheader add-cell-border-right add-cell-border-top ">
                <div class="cell" >
                   <span class="cellfont">Product Configuration</span>
                </div>
            </div>
          </div>
          <span id="div_product_config_{$rowid}">
           <div class="row margin-0 add-cell-border-right">
                <div class="col-md-12 add-cell-border-right add-cell-border-left add-cell-border-bottom center">
                    <div class="cell" >
                    <span class="cellfont">No records found</span></div>
                </div>
           </div>
          </span>
        </div>
    </div>
    </li>
    <!-- product_config sub panel end -->
    <!-- Product information sub panel start -->
    <li class="list-group-item product_informat "  style="margin-left: 0px;">
    <div id="container_product_informat_{$rowid}">
        <div class="method">
          <div class="row margin-0">
            <div class="col-md-12  cellgrey cellheader add-cell-border-right add-cell-border-top ">
                <div class="cell" >
                   <span class="cellfont">Product Information</span>
                </div>
            </div>
          </div>
          <span id="div_product_informat_{$rowid}">
            <div class="row margin-0 add-cell-border-right">
                <div class="col-md-12 add-cell-border-right add-cell-border-left add-cell-border-bottom center">
                    <div class="cell" >
                    <span class="cellfont">No records found</span></div>
                </div>
            </div>
          </span>
        </div>
    </div>
    </li>
    <!-- Product information sub panel end -->
    <!-- Product price details sub panel start -->
    <li class="list-group-item price_details " style="margin-left: 0px;">    
    <div id="container_price_details " class="add-cell-border-right">
       <div class="method">
          <div class="row margin-0">
            <div class="col-md-12  cellgrey cellheader add-cell-border-right add-cell-border-top ">
                <div class="cell" >
                   <span class="cellfont">Product Price</span>
                </div>
            </div>
          </div>
          <div id="div_product_price_{$rowid}" class="add-cell-border-left"> 
            <div class="row margin-0 add-cell-border-right">
                <div class="col-md-12 add-cell-border-right add-cell-border-left add-cell-border-bottom center">
                    <div class="cell" >
                    <span class="cellfont">{sugar_translate label='LBL_NO_PRICE_DETAILS' module='gc_Contracts'}</span></div>
                </div>
           </div>
          </div>
        </div>
    </div>
    </li>
    <!-- Product price details sub panel end -->
</ul>
</div>