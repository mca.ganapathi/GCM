<?php

use Sugarcrm\Sugarcrm\Util\Arrays\ArrayFunctions\ArrayFunctions;
include_once("include/workflow/alert_utils.php");
include_once("include/workflow/action_utils.php");
include_once("include/workflow/time_utils.php");
include_once("include/workflow/trigger_utils.php");
//BEGIN WFLOW PLUGINS
include_once("include/workflow/custom_utils.php");
//END WFLOW PLUGINS
	class gc_Contracts_workflow {
	function process_wflow_triggers(& $focus){
		include("custom/modules/gc_Contracts/workflow/triggers_array.php");
		include("custom/modules/gc_Contracts/workflow/alerts_array.php");
		include("custom/modules/gc_Contracts/workflow/actions_array.php");
		include("custom/modules/gc_Contracts/workflow/plugins_array.php");
		if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('0'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('1'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9b94a9ae_43b8_11e8_9b2f_5cb9017d175a'])){
		$triggeredWorkflows['9b94a9ae_43b8_11e8_9b2f_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "66fef16f-0b23-4fbf-90fa-b4dfd03e963e"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts0_alert0'], $alertshell_array, false); 
 	 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "923424e3-0b23-4fbc-b608-60603610c971"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts0_alert1'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('1'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('2'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9b96304e_43b8_11e8_9116_5cb9017d175a'])){
		$triggeredWorkflows['9b96304e_43b8_11e8_9116_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "923424e3-0b23-4fbc-b608-60603610c971"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts1_alert0'], $alertshell_array, false); 
 	 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "46a712f5-0b23-4fb3-9878-c896795871dc"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts1_alert1'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('2'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('3'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9b975546_43b8_11e8_b276_5cb9017d175a'])){
		$triggeredWorkflows['9b975546_43b8_11e8_b276_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "46a712f5-0b23-4fb3-9878-c896795871dc"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts2_alert0'], $alertshell_array, false); 
 	 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "923424e3-0b23-4fbc-b608-60603610c971"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts2_alert1'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('3'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('4'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9b987a3e_43b8_11e8_b5ef_5cb9017d175a'])){
		$triggeredWorkflows['9b987a3e_43b8_11e8_b5ef_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "46a712f5-0b23-4fb3-9878-c896795871dc"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts3_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		 $action_meta_array['gc_Contracts3_action0']['trigger_id'] = '9b987a3e_43b8_11e8_b5ef_5cb9017d175a'; 
 	 $action_meta_array['gc_Contracts3_action0']['action_id'] = 'feaf7759-0b23-4fb1-affa-d2cb4274c196'; 
 	 process_workflow_actions($focus, $action_meta_array['gc_Contracts3_action0']); 
 	}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('1'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('0'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9b9a6290_43b8_11e8_9dfa_5cb9017d175a'])){
		$triggeredWorkflows['9b9a6290_43b8_11e8_9dfa_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "b74da531-0b23-4fb1-8652-d6cac54ffa00"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts4_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('2'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('0'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9b9b25e0_43b8_11e8_ab6c_5cb9017d175a'])){
		$triggeredWorkflows['9b9b25e0_43b8_11e8_ab6c_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "b74da531-0b23-4fb1-8652-d6cac54ffa00"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts5_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('3'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('0'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9b9be930_43b8_11e8_baa8_5cb9017d175a'])){
		$triggeredWorkflows['9b9be930_43b8_11e8_baa8_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "b74da531-0b23-4fb1-8652-d6cac54ffa00"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts6_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('4'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('5'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9b9d6fd0_43b8_11e8_894f_5cb9017d175a'])){
		$triggeredWorkflows['9b9d6fd0_43b8_11e8_894f_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "f62390c5-0b23-4fbe-b734-a53ec7368300"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts7_alert0'], $alertshell_array, false); 
 	 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "e644f05d-0b23-4fba-9448-37de77ca3631"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts7_alert1'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('5'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('6'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9b9e94c8_43b8_11e8_b97d_5cb9017d175a'])){
		$triggeredWorkflows['9b9e94c8_43b8_11e8_b97d_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "e644f05d-0b23-4fba-9448-37de77ca3631"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts8_alert0'], $alertshell_array, false); 
 	 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "f801e4ab-0b23-4fbb-9039-7b298ef9b5e9"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts8_alert1'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('6'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('7'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9b9fb9c0_43b8_11e8_94e1_5cb9017d175a'])){
		$triggeredWorkflows['9b9fb9c0_43b8_11e8_94e1_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "f801e4ab-0b23-4fbb-9039-7b298ef9b5e9"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts9_alert0'], $alertshell_array, false); 
 	 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "e644f05d-0b23-4fba-9448-37de77ca3631"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts9_alert1'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('7'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('8'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9ba0deb8_43b8_11e8_9b99_5cb9017d175a'])){
		$triggeredWorkflows['9ba0deb8_43b8_11e8_9b99_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "f801e4ab-0b23-4fbb-9039-7b298ef9b5e9"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts10_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('5'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('4'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9ba26558_43b8_11e8_accc_5cb9017d175a'])){
		$triggeredWorkflows['9ba26558_43b8_11e8_accc_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "fb1e5625-0b23-4fb5-b280-f66fc5749b7b"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts11_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('6'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('4'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9ba328a8_43b8_11e8_a279_5cb9017d175a'])){
		$triggeredWorkflows['9ba328a8_43b8_11e8_a279_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "fb1e5625-0b23-4fb5-b280-f66fc5749b7b"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts12_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ($focus->fetched_row['workflow_flag'] ==  stripslashes('7'))&& 
 (isset($focus->workflow_flag) && $focus->workflow_flag ==  stripslashes('4'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9ba44daa_43b8_11e8_8ec0_5cb9017d175a'])){
		$triggeredWorkflows['9ba44daa_43b8_11e8_8ec0_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "fb1e5625-0b23-4fb5-b280-f66fc5749b7b"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts13_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( (isset($focus->workflow_flag) && $focus->workflow_flag ==  '9')){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9ba572a2_43b8_11e8_b63f_5cb9017d175a'])){
		$triggeredWorkflows['9ba572a2_43b8_11e8_b63f_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "7300aac2-0b23-4fb0-aaa0-df0d3936e617"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts14_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		 $action_meta_array['gc_Contracts14_action0']['trigger_id'] = '9ba572a2_43b8_11e8_b63f_5cb9017d175a'; 
 	 $action_meta_array['gc_Contracts14_action0']['action_id'] = 'dde88e4a-0b23-4fbb-a5df-84afb5bf5a41'; 
 	 process_workflow_actions($focus, $action_meta_array['gc_Contracts14_action0']); 
 	}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 

if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( (isset($focus->workflow_flag) && $focus->workflow_flag ==  '10')){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['9ba75aea_43b8_11e8_9303_5cb9017d175a'])){
		$triggeredWorkflows['9ba75aea_43b8_11e8_9303_5cb9017d175a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "5863c637-0b23-4fb3-8433-38670a89469c"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['gc_Contracts15_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		 $action_meta_array['gc_Contracts15_action0']['trigger_id'] = '9ba75aea_43b8_11e8_9303_5cb9017d175a'; 
 	 $action_meta_array['gc_Contracts15_action0']['action_id'] = '0c6d8091-0b23-4fb1-a152-e104fdb0ea3a'; 
 	 process_workflow_actions($focus, $action_meta_array['gc_Contracts15_action0']); 
 	}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 


	//end function process_wflow_triggers
	}

	//end class
	}

?>