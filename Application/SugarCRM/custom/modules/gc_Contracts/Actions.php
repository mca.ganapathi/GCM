<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once ('custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php');
include_once ('custom/include/ModUtils.php');
include_once ('custom/modules/gc_Contracts/include/ClassGcContracts.php');

/**
 * Ajax handler for the product populate process
 *
 * @author arunsakthivel.sambandam
 * @since Mar 18, 2016
 */
class Actions extends ModUtils
{
    const KEY_RESPONSE_CODE = 'ResponseCode';

    const KEY_RESPONSE_MSG = 'ResponseMsg';

    private $response_array = array();

    function __construct()
    {
        $this->response_array[self::KEY_RESPONSE_CODE] = 0;
        $this->response_array[self::KEY_RESPONSE_MSG] = '';
    }

    /**
     * function to return the OfferingDetails
     *
     * @author arunsakthivel.sambandam
     * @since Mar 18, 2016
     */
    public function getOfferingDetails()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'Offering_id : ' . $_REQUEST['off_id'] , 'Return product offering details in json format');
        $offering_id = $_REQUEST['off_id'];
        $obj_product = new ClassProductConfiguration();
        $offeringDetails = $obj_product->getProductOfferingDetails($offering_id);
        $offeringDetails = ((is_array($offeringDetails) && count($offeringDetails) > 0) ? $offeringDetails : array());
        $this->outputData($offeringDetails);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'offeringDetails : ' . print_r($offeringDetails, true), "Return product offering details in json format");
    }

    /**
     * Function to return the Offerings
     *
     * @author arunsakthivel.sambandam
     * @since Mar 18, 2016
     */
    public function getOfferings()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', "Return product offerings");
        $obj_product = new ClassProductConfiguration();
        $offerings = $obj_product->getProductOfferings(false);
        $offerings = ((is_array($offerings) && count($offerings) > 0) ? $offerings : array());
        $this->outputData($offerings);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'offerings : ' . print_r($offerings, true), "Return product offerings");
    }

    /**
     * Function to get the existing pms product related details
     *
     * @author arunsakthivel.sambandam
     * @since Mar 18, 2016
     */
    public function getEditviewDetails()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_bean_id : ' . $_REQUEST['c_b_id'] . GCM_GL_LOG_VAR_SEPARATOR . 'product_offering : ' . $_REQUEST['off_id'], 'get the existing pms product related details');
        $obj_gc_contracts = new ClassGcContracts();
        $contract_bean_id = $_REQUEST['c_b_id'];
        $product_offering = $_REQUEST['off_id'];
        $line_item_header_list = $obj_gc_contracts->getLineItemHeader($contract_bean_id, 0, $product_offering, true);
        $arr_pms_det = array();
        if (is_array($line_item_header_list) && (count($line_item_header_list) > 0)) {
            $arr_line_item_order = $line_item_header_list['order'];
            if (is_array($arr_line_item_order) && (count($arr_line_item_order) > 0)) {
                ksort($arr_line_item_order);
                
                foreach ($arr_line_item_order as $key => $value_line_item_uuid) {
                    $tmp[$value_line_item_uuid] = $line_item_header_list[$value_line_item_uuid];
                    $line_item_id = $line_item_header_list[$value_line_item_uuid]['line_item_id'];
                    $arr_pms_det[$line_item_id]['product_specification_id'] = !empty($tmp[$value_line_item_uuid]['product_specification_id']) ? $tmp[$value_line_item_uuid]['product_specification_id'] : '';
                    $arr_pms_det[$line_item_id]['product_config'] = !empty($tmp[$value_line_item_uuid]['product_config']) ? $tmp[$value_line_item_uuid]['product_config'] : array();
                    $arr_pms_det[$line_item_id]['product_price'] = !empty($tmp[$value_line_item_uuid]['product_price']) ? $tmp[$value_line_item_uuid]['product_price'] : array();
                    $arr_pms_det[$line_item_id]['rules'] = !empty($tmp[$value_line_item_uuid]['rules']) ? $tmp[$value_line_item_uuid]['rules'] : array();
                }                
            }            
        }

        $response = array(
                        'response_code' => 1,
                        'respose_array' => $arr_pms_det
        );
        $this->outputData($response);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response : ' . print_r($response, true), 'get the existing pms product related details');
    }

    /**
     * function to set relationship between line item history and invoice group
     *
     * @author sagar.salunkhe
     * @since May 07, 2016
     */
    public function setInvoiceGroupLineItemRel()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'invoice_group_id : ' . $_REQUEST['invoice_group_id'] . GCM_GL_LOG_VAR_SEPARATOR . 'line_item_id : ' . $_REQUEST['line_item_id'] . GCM_GL_LOG_VAR_SEPARATOR . 'contracts_id : ' . $_REQUEST['contract_id'], 'Function to set relationship between line item history and invoice group');

        $invoice_group_id = $_REQUEST['invoice_group_id'];
        $line_item_id = $_REQUEST['line_item_id'];
        $contracts_id = $_REQUEST['contract_id'];

        if (!isset($invoice_group_id) || empty($invoice_group_id)) {
            $this->response_array[self::KEY_RESPONSE_MSG] = 'Invoice group ID cannot be empty!';
        } elseif (!isset($line_item_id) || empty($line_item_id)) {
            $this->response_array[self::KEY_RESPONSE_MSG] = 'Line Item ID cannot be empty!';
            $this->outputData($this->response_array);
        } elseif (!isset($contracts_id) || empty($contracts_id)) {
            $this->response_array[self::KEY_RESPONSE_MSG] = 'Contracts ID cannot be empty!';
            $this->outputData($this->response_array);
        }

        $obj_array_line_contract_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
        $obj_array_line_contract_history_bean->retrieve_by_string_fields(array(
                                                                                'line_item_id' => $line_item_id,
                                                                                'contracts_id' => $contracts_id
        ));

        if (empty($obj_array_line_contract_history_bean->id)) {
            $this->response_array[self::KEY_RESPONSE_MSG] = 'Unable to fetch Line Item History ID!';
            $this->outputData($this->response_array);
        }

        $obj_invoice_group = BeanFactory::getBean('gc_InvoiceSubtotalGroup', $invoice_group_id);
        if (empty($obj_invoice_group->id)) {
            $this->response_array[self::KEY_RESPONSE_MSG] = 'Provided invoice group ID is not valid!';
            $this->outputData($this->response_array);
        }

        $obj_invoice_group->load_relationship('gc_invoicesubtotalgroup_gc_line_item_contract_history_1');
        $obj_invoice_group->gc_invoicesubtotalgroup_gc_line_item_contract_history_1->add($obj_array_line_contract_history_bean->id);
        
        $this->response_array[self::KEY_RESPONSE_CODE] = 1;
        $this->response_array[self::KEY_RESPONSE_MSG] = 'Relationship added successfully!';
        $this->outputData($this->response_array);
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response_array : ' . $this->response_array , 'Function to set relationship between line item history and invoice group');
    }

    /**
     * function to unset relationship between line item history and invoice group
     *
     * @author sagar.salunkhe
     * @since 07-May-2016
     */
    public function unsetInvoiceGroupLineItemRel()
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'invoice_group_id : ' . $_REQUEST['invoice_group_id'] . GCM_GL_LOG_VAR_SEPARATOR . 'line_item_history_id : ' . $_REQUEST['line_item_history_id'], 'Function to unset relationship between line item history and invoice group');
    	
        $invoice_group_id = $_REQUEST['invoice_group_id'];
        $line_item_history_id = $_REQUEST['line_item_history_id'];

        if (!isset($invoice_group_id) || empty($invoice_group_id)) {
            $this->response_array[self::KEY_RESPONSE_MSG] = 'Invoice group ID cannot be empty!';
            $this->outputData($this->response_array);
        } elseif (!isset($line_item_history_id) || empty($line_item_history_id)) {
            $this->response_array[self::KEY_RESPONSE_MSG] = 'Line Item History ID cannot be empty!';
            $this->outputData($this->response_array);
        }

        $obj_array_line_contract_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History', $line_item_history_id);
        if (empty($obj_array_line_contract_history_bean->id)) {
            $this->response_array[self::KEY_RESPONSE_MSG] = 'Provided Line Item History ID is not valid!';
            $this->outputData($this->response_array);
        }

        $obj_invoice_group = BeanFactory::getBean('gc_InvoiceSubtotalGroup', $invoice_group_id);
        if (empty($obj_invoice_group->id)) {
            $this->response_array[self::KEY_RESPONSE_MSG] = 'Provided invoice group ID is not valid!';
            $this->outputData($this->response_array);
        }

        $obj_invoice_group->load_relationship('gc_invoicesubtotalgroup_gc_line_item_contract_history_1');
        $obj_invoice_group->gc_invoicesubtotalgroup_gc_line_item_contract_history_1->delete($obj_invoice_group->id, $obj_array_line_contract_history_bean->id);
        
        $obj_line_item_bean = BeanFactory::getBean('gc_LineItem');
        $obj_line_item_bean->retrieve($obj_array_line_contract_history_bean->line_item_id);
        $audit_entry = array();
        $audit_entry['field_name'] = 'Invoice Subtotal Group';
        $audit_entry['data_type'] = 'text';
        $audit_entry['before'] = $obj_invoice_group->name;
        $audit_entry['after'] = '';
        $obj_line_item_bean->db->save_audit_records($obj_line_item_bean, $audit_entry);
        
        $this->response_array[self::KEY_RESPONSE_CODE] = 1;
        $this->response_array[self::KEY_RESPONSE_MSG] = 'Relationship deleted successfully!';
        $this->outputData($this->response_array);
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response_array : ' . $this->response_array , 'Function to unset relationship between line item history and invoice group');
        
    }

    /**
     * function to get max_input_val value from php ini
     *
     * @author rajul.mondal
     * @since Jun 17, 2016
     */
    public function getMaxInputVar()
    {
        $max_input_vars = (!empty(ini_get('max_input_vars'))?ini_get('max_input_vars'):'');
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'max_input_vars : ' . print_r($max_input_vars, true), 'Function to get max_input_val value from php ini');
        $this->outputData($max_input_vars);
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'max_input_vars : ' . print_r($max_input_vars, true), 'Function to get max_input_val value from php ini');
    }

    /**
     * function to check current user has approver permission
     *
     * @author rajul.mondal
     * @since Jul 08, 2016
     */
    public function isApproverUser()
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $_REQUEST['record'], 'Function to check current user has approver permission');
    	if (!isset($_REQUEST['record']) || $_REQUEST['record'] == '') {
            return false;
        }

        $contract_id = $_REQUEST['record'];
        $isApprover = false;

        $bean = BeanFactory::getBean('gc_Contracts', $contract_id);
        $bean->retrieve('id');
        $status = $bean->contract_status;

        $obj_gc_contracts = new ClassGcContracts();
        $approver = $obj_gc_contracts->getRolePermissions(SETTING_KEY_ACE_CONTRACTS);
        $roles = $obj_gc_contracts->getUserRoleIds();

        if (($GLOBALS['current_user']->is_admin || count(array_intersect($roles, $approver)) != 0) && $status === '6') {
            $isApprover = true;
        }
        $this->outputData($isApprover);
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'isApprover : ' . $isApprover, 'Function to check current user has approver permission');
    }

    /**
     * function to get line item details
     *
     * @param line item id and line item history id
     * @return Json of line item details
     * @author Kamal.Thiyagarajan
     * @since Jul 28, 2016
     */
    public function getLineItemDetails()
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $_REQUEST['record'], 'Function to check current user has approver permission');
        $line_history_id = $_REQUEST['line_history_id'];
        $product_offering = $_REQUEST['product_offering'];
        $record_id = $_REQUEST['record_id'];
        require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';
        include_once ('custom/modules/gc_Contracts/include/ClassGcContracts.php');
        $obj_gc_contracts = new ClassGcContracts();
        $config_currencies = $obj_gc_contracts->getconfig_currencies();
        $config_currencies = ((is_array($config_currencies) && (count($config_currencies)>0))?$config_currencies:array());
        $product_config = new ClassProductConfiguration();
        $line_item_his_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
        $product_config_bean = BeanFactory::getBean('gc_ProductConfiguration');
        $product_price_bean = BeanFactory::getBean('gc_ProductPrice');
        
        /* Fixed to avaoid the uncessary call to PMS system and currl error Further code review should be done on this - Arunsakthivel */
        $off_details = $sp_details = $pcharge_details = array();
        if (!empty($product_offering)) {
            $off_details = $product_config->getProductOfferingDetails($product_offering);
            if (is_array($off_details) && (count($off_details)>0)) {
                $sp_details = $off_details['respose_array']['specification_details'];
                $pcharge_details = $off_details['respose_array']['charge_details'];                
            }
        }
        unset($off_details);
        unset($product_config);
        $line_item_his_bean_arr = $line_item_his_bean->get_full_list('', "gc_line_item_contract_history.id = '$line_history_id'");
        if ($record_id != "") {
            $contract_obj = BeanFactory::getBean('gc_Contracts');
            $contract_obj->retrieve($record_id);
            $global_contract_id = $contract_obj->global_contract_id;
            $contract_version = $contract_obj->contract_version;
            $contr_version = $contract_version - 1;
            $contr_version = ($contr_version > 1) ? $contr_version : 1;
            $where = " global_contract_id = '" . $global_contract_id . "' AND contract_version = '" . $contr_version . "' ";
            $contract_object = BeanFactory::getBean('gc_Contracts');
            $result_beans = $contract_object->get_full_list('', $where);
            if (!empty($result_beans) && is_array($result_beans) && isset($result_beans[0]->id) && $result_beans[0]->id != '') {
                $contract_object->retrieve($result_beans[0]->id);
                $line_item_id_uuid = ((!empty($line_item_his_bean_arr[0]->line_item_id_uuid))?$line_item_his_bean_arr[0]->line_item_id_uuid:'');
                $where = "  gc_line_item_contract_history.line_item_id_uuid = '" . $line_item_id_uuid . "' AND gc_line_item_contract_history.contracts_id = '" . $result_beans[0]->id . "' ";
                $line_item_his_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
                $line_item_his_bean_arr = $line_item_his_bean->get_full_list('', $where);
            }
        }
        
        $line_item_his_bean_arr_1st_index = ((!empty($line_item_his_bean_arr[0]))?$line_item_his_bean_arr[0]:'');
        $line_item_details = $obj_gc_contracts->getLineItem($line_item_his_bean_arr_1st_index, $product_config_bean, $product_price_bean, $config_currencies, $sp_details, $pcharge_details, $product_offering);
        if (is_array($line_item_details) && (count($line_item_details)>0)) {
            $service_start_date = ((!empty($line_item_details['service_start_date']))?$line_item_details['service_start_date']:'');
            $line_item_details['service_start_date'] = ModUtils::getUserDate($service_start_date);
            
            $service_end_date = ((!empty($line_item_details['service_end_date']))?$line_item_details['service_end_date']:'');
            $line_item_details['service_end_date'] = ModUtils::getUserDate($service_end_date);
            
            $billing_start_date = ((!empty($line_item_details['billing_start_date']))?$line_item_details['billing_start_date']:'');
            $line_item_details['billing_start_date'] = ModUtils::getUserDate($billing_start_date);
            
            $billing_end_date = ((!empty($line_item_details['billing_end_date']))?$line_item_details['billing_end_date']:'');
            $line_item_details['billing_end_date'] = ModUtils::getUserDate($billing_end_date);
            echo json_encode($line_item_details);            
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_item_details : ' . print_r($line_item_details, true), 'Function to check current user has approver permission');
        exit();
    }
}

 

$a = new Actions();
$a->process();
