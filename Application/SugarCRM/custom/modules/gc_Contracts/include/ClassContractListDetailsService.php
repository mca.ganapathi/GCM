<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once 'custom/include/XMLUtils.php';

/**
 * Module API - Retrieve (GET) Contract Details Class File
 *
 * @author debasish.gupta
 * @since Nov 22, 2016
 */
/**
 * @type unix-file system UTF-8
 */
class ClassContractListDetailsService
{

    private $db;
    private $obj_xml_utils;

    /**
     *
     * @author debasish.gupta
     * @since Nov 22, 2016
     */
    public function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        global $db;
        $this->db = $db;

        $this->obj_xml_utils = new XMLUtils();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * generic function to retrieve (GET) Contract Details
     *
     * @author debasish.gupta
     * @param array $params
     * @return array result data
     * @since Nov 22, 2016
     */
    public function getContractList($params)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'params: '.print_r($params,true), 'Generic function to retrieve (GET) Contract Details');
        // declare the search parameter variables
        $contract_customer_id = "";
        $contract_product_id  = "";
        $updated_after        = "";
        $contract_status      = "";
        $all_versions         = "";
        $result_array         = array();

        // initialize the search parameter variables
        if (!empty($params['search_filter']['contract-customer-id'])) {
            $contract_customer_id = $params['search_filter']['contract-customer-id'];
        }

        if (!empty($params['search_filter']['contract-product-id'])) {
            $contract_product_id = self::parseContractProductId($params['search_filter']['contract-product-id']);
        }

        if (!empty($params['search_filter']['updated-after'])) {
            $updated_after = $params['search_filter']['updated-after'];
        }

        if (!empty($params['search_filter']['status'])) {
            $contract_status_array = self::getContractStatusIdByName($params['search_filter']['status']);
            $contract_status       = implode(',', $contract_status_array);
        }

        if (!empty($params['search_filter']['all-versions'])) {
            $all_versions = $params['search_filter']['all-versions'];
        }

        // query based on the search parameter variables
        $search_query = "SELECT
                contracts.global_contract_id_uuid,contracts.contract_version
            FROM
                gc_contracts AS contracts
            LEFT JOIN accounts_gc_contracts_1_c AS agcontracts ON agcontracts.accounts_gc_contracts_1gc_contracts_idb = contracts.id AND agcontracts.deleted = 0
            LEFT JOIN accounts AS accnts ON accnts.id = agcontracts.accounts_gc_contracts_1accounts_ida AND accnts.deleted = 0
            LEFT JOIN accounts_cstm AS acstm ON acstm.id_c = accnts.id
            LEFT JOIN contacts_gc_contracts_1_c AS cgcontracts ON cgcontracts.contacts_gc_contracts_1gc_contracts_idb = contracts.id AND cgcontracts.deleted = 0
            LEFT JOIN contacts AS cntcts ON cntcts.id = cgcontracts.contacts_gc_contracts_1contacts_ida AND cntcts.deleted = 0
            " . (($all_versions != "1") ? " INNER JOIN
                      (SELECT global_contract_id ,MAX(contract_version) max_version
                        FROM `gc_contracts`
                        WHERE deleted = 0 AND
                        global_contract_id_uuid IS NOT NULL AND
                        contract_version IS NOT NULL
                        GROUP BY `global_contract_id`
                        ORDER BY global_contract_id) AS gcc_max
                    ON gcc_max.global_contract_id = contracts.global_contract_id AND contracts.contract_version = gcc_max.max_version" : "") . "
            WHERE
                contracts.deleted = 0  AND
                contracts.global_contract_id_uuid IS NOT NULL AND
                contracts.contract_version IS NOT NULL
                " . (($contract_customer_id != "") ? " AND acstm.common_customer_id_c = '" . $contract_customer_id . "'" : "") . "
                " . (($contract_product_id != "") ? " AND contracts.product_offering = '" . $contract_product_id . "'" : "") . "
                " . (($contract_status != "") ? " AND contracts.contract_status IN(" . $contract_status . ")" : "") . "
                " . (($updated_after != "") ? " AND (contracts.date_modified >= '" . $updated_after . "' OR accnts.date_modified >= '" . $updated_after . "' OR
                cntcts.date_modified >= '" . $updated_after . "')" : "") . " ORDER BY contracts.global_contract_id ASC,contracts.contract_version ASC";
        $query_results = $this->db->query($search_query, true);
        $k             = 0;
        while ($row = $this->db->fetchByAssoc($query_results)) {
            $result_array[$k]['product_contract_uuid'] = $row['global_contract_id_uuid'];
            $result_array[$k]['version']               = (int) $row['contract_version'];
            $k++;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'result_array: '.print_r($result_array,true), '');
        return $result_array;
    }

    /**
     * generic function to parse contract product id
     *
     * @author debasish.gupta
     * @param string $contract_product_id
     * @return string
     * @since Nov 23, 2016
     */
    private function parseContractProductId($contract_product_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'contract_product_id: '.$contract_product_id, 'Generic function to parse contract product id');
        $contract_product_arr = explode(":", $contract_product_id);
        $contract_product_id  = $this->obj_xml_utils->uuidRemoveFormat($contract_product_arr[0], true);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'contract_product_id: '.$contract_product_id, '');
        return $contract_product_id;
    }

    /**
     * generic function to get contract status id array by giving status name array
     *
     * @author debasish.gupta
     * @param array $status_array
     * @return array contract status id
     * @since Nov 24, 2016
     */
    private function getContractStatusIdByName($status_array)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'status_array: '.print_r($status_array, true), 'Generic function to get contract status id array by giving status name array');
        $return_status_id_array = array();
        if (is_array($status_array) && count($status_array) > 0) {
            foreach ($status_array as $status_name) {
                if ($status_name != "") {
                    foreach ($GLOBALS['app_list_strings']['contract_status_list'] as $key => $value) {
                        if ($value == $status_name) {
                            $return_status_id_array[] = $key;
                        }

                    }
                }
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'return_status_id_array: '.print_r($return_status_id_array, true), '');
        return $return_status_id_array;
    }
}
