<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class ClassGcContractAfterRetrieve
{

    /**
     * method to retrieve converted datetime of contract header.
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author Rajul.Mondal
     * @since 20-Mar-2017
     */
    public function processContractHeaderFields($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'method to retrieve converted datetime of contract');
        global $sugar_config;
        $modUtils      = new ModUtils();
        $timezone_bean = BeanFactory::getBean('gc_TimeZone');

        global $is_api_call;
        if($is_api_call)
            return;

        // contract start date
        $contract_startdate = $bean->fetched_row['contract_startdate'];
        if (!empty($contract_startdate)) {
            $timezone_list               = $timezone_bean->retrieve($bean->gc_timezone_id_c);
            $contract_startdate_timezone = !empty($timezone_list->utcoffset) ? $timezone_list->utcoffset : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $contract_startdate          = $modUtils->ConvertGMTDateToLocalDate($contract_startdate, $contract_startdate_timezone);
            $bean->contract_startdate    = $bean->fetched_row['contract_startdate']    = $contract_startdate['datetime'];
        }

        // contract end date
        $contract_enddate = $bean->fetched_row['contract_enddate'];
        if (!empty($contract_enddate)) {
            $timezone_list             = $timezone_bean->retrieve($bean->gc_timezone_id1_c);
            $contract_enddate_timezone = !empty($timezone_list->utcoffset) ? $timezone_list->utcoffset : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $contract_enddate          = $modUtils->ConvertGMTDateToLocalDate($contract_enddate, $contract_enddate_timezone);
            $bean->contract_enddate    = $bean->fetched_row['contract_enddate']    = $contract_enddate['datetime'];
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * method to retrieve converted datetime of contract line item.
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author Rajul.Mondal
     * @since 20-Mar-2017
     */
    public function processContractLineItemFields($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'method to retrieve converted datetime of contract line item');
        global $sugar_config,$is_api_call;

        $modUtils      = new ModUtils();
        $timezone_bean = BeanFactory::getBean('gc_TimeZone');

        $bean->process_record_event = false;

        if($event == 'process_record')
            $bean->process_record_event = true;

        if($is_api_call)
            return;

        // line item service start date
        if(!isset($bean->fetched_row['service_start_date'])) $bean->fetched_row['service_start_date'] = $bean->service_start_date;
        $service_start_date = $bean->fetched_row['service_start_date']; //$bean->service_start_date;
        if (!empty($service_start_date)) {
            $timezone_list               = $timezone_bean->retrieve($bean->gc_timezone_id_c);
            $service_start_date_timezone = !empty($timezone_list->utcoffset) ? $timezone_list->utcoffset : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $service_start_date          = $modUtils->ConvertGMTDateToLocalDate($service_start_date, $service_start_date_timezone);
            $bean->service_start_date    = $bean->fetched_row['service_start_date']    = $bean->preserve_fr_svc_strt_dt = $service_start_date['datetime'];
        }

        // line item service end date
        if(!isset($bean->fetched_row['service_end_date'])) $bean->fetched_row['service_end_date'] = $bean->service_end_date;
        $service_end_date = $bean->fetched_row['service_end_date']; //$bean->service_end_date;
        if (!empty($service_end_date)) {
            $timezone_list             = $timezone_bean->retrieve($bean->gc_timezone_id1_c);
            $service_end_date_timezone = !empty($timezone_list->utcoffset) ? $timezone_list->utcoffset : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $service_end_date          = $modUtils->ConvertGMTDateToLocalDate($service_end_date, $service_end_date_timezone);
            $bean->service_end_date    = $bean->fetched_row['service_end_date']    = $bean->preserve_fr_svc_end_dt = $service_end_date['datetime'];
        }

        // line item billing start date
        if(!isset($bean->fetched_row['billing_start_date'])) $bean->fetched_row['billing_start_date'] = $bean->billing_start_date;
        $billing_start_date = $bean->fetched_row['billing_start_date'];
        if (!empty($billing_start_date)) {
            $timezone_list               = $timezone_bean->retrieve($bean->gc_timezone_id2_c);
            $billing_start_date_timezone = !empty($timezone_list->utcoffset) ? $timezone_list->utcoffset : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $billing_start_date          = $modUtils->ConvertGMTDateToLocalDate($billing_start_date, $billing_start_date_timezone);
            $bean->billing_start_date    = $bean->fetched_row['billing_start_date']    = $bean->preserve_fr_bill_strt_dt = $billing_start_date['datetime'];
        }

        // line item billing end date
        if(!isset($bean->fetched_row['billing_end_date'])) $bean->fetched_row['billing_end_date'] = $bean->billing_end_date;
        $billing_end_date = $bean->fetched_row['billing_end_date'];
        if (!empty($billing_end_date)) {
            $timezone_list             = $timezone_bean->retrieve($bean->gc_timezone_id3_c);
            $billing_end_date_timezone = !empty($timezone_list->utcoffset) ? $timezone_list->utcoffset : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $billing_end_date          = $modUtils->ConvertGMTDateToLocalDate($billing_end_date, $billing_end_date_timezone);
            $bean->billing_end_date    = $bean->fetched_row['billing_end_date']    = $bean->preserve_fr_bill_end_dt = $billing_end_date['datetime'];
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }
}
