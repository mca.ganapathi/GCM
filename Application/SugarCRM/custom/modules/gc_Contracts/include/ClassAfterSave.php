<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once 'custom/modules/gc_Contracts/include/ClassGcContracts.php';

class ClassGcContractAfterSave extends ClassGcContracts
{

    /**
     * method to Save contract subpanel record.
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author Dinesh.Itkar
     * @since 25-Nov-2015
     */
    public function saveContractSubPanel($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method to Save contract subpanel record');
        $modUtils                   = new ModUtils();
        $is_sugar_request           = true; // true if request is from sugar
        $flg_contract_update        = true; // Edit Mode
        $invoice_group_details_json = '';

        // if value is not posted from sugar, stop function execution
        if (!isset($_POST['sugar_request']) || empty($_POST['sugar_request'])) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'If value is not posted from sugar, stop function execution.', 'Debug');
            return;
        }

        if ($bean->fetched_row['id'] == '') {
            $flg_contract_update = false; // Create Mode
        }

        if (isset($_POST['invoice_group_details_json'])) {
            $invoice_group_details_json = $_POST['invoice_group_details_json'];
        }

        // decode json data
        $invoice_group_details = json_decode(html_entity_decode($invoice_group_details_json, ENT_QUOTES), true);

        // if json data is not valid or empty, pass empty array ahead
        if (json_last_error() !== JSON_ERROR_NONE || !is_array($invoice_group_details)) {
            $invoice_group_details = array();
        }

        $array_sales_representative_new                               = array();
        $array_sales_representative_new['sales_channel_code']         = isset($_POST['sales_channel_code']) ? trim($_POST['sales_channel_code']) : '';
        $array_sales_representative_new['name']                       = isset($_POST['sales_rep_name']) ? trim($_POST['sales_rep_name']) : '';
        $array_sales_representative_new['sales_rep_name_1']           = isset($_POST['sales_rep_name_1']) ? trim($_POST['sales_rep_name_1']) : '';
        $array_sales_representative_new['division_1']                 = isset($_POST['division_1']) ? trim($_POST['division_1']) : '';
        $array_sales_representative_new['division_2']                 = isset($_POST['division_2']) ? trim($_POST['division_2']) : '';
        $array_sales_representative_new['title_2']                    = isset($_POST['title_2']) ? trim($_POST['title_2']) : '';
        $array_sales_representative_new['tel_no']                     = isset($_POST['tel_no']) ? trim($_POST['tel_no']) : '';
        $array_sales_representative_new['ext_no']                     = isset($_POST['ext_no']) ? trim($_POST['ext_no']) : '';
        $array_sales_representative_new['fax_no']                     = isset($_POST['fax_no']) ? trim($_POST['fax_no']) : '';
        $array_sales_representative_new['email1']                     = isset($_POST['email1']) ? trim($_POST['email1']) : '';
        $array_sales_representative_new['gc_nttcomgroupcompany_id_c'] = isset($_POST['sales_company_id']) ? trim($_POST['sales_company_id']) : '';
        $array_sales_representative_new['previous_version_id']        = isset($_POST['sales_rep_parent_id']) ? trim($_POST['sales_rep_parent_id']) : '';

        // Create request for sales representative.
        if (empty($_POST['sales_rep_id'])) {
            // Check array with non empty values
            if (!empty(array_diff(array_map('trim', $array_sales_representative_new), array('', NULL, FALSE)))) {
                $this->setContractSalesRepEntryService($array_sales_representative_new, $bean->id, false, $is_sugar_request);
            }
        } else {
            // Update request for sales representative.

            // Populate old field value of sales representative into array
            $array_sales_representative_old = array();
            $obj_sales_representative_bean  = BeanFactory::getBean('gc_SalesRepresentative');
            $obj_sales_representative_bean->retrieve($_POST['sales_rep_id']);
            $array_sales_representative_old['sales_channel_code']         = $obj_sales_representative_bean->sales_channel_code;
            $array_sales_representative_old['name']                       = $obj_sales_representative_bean->name;
            $array_sales_representative_old['sales_rep_name_1']           = $obj_sales_representative_bean->sales_rep_name_1;
            $array_sales_representative_old['division_1']                 = $obj_sales_representative_bean->division_1;
            $array_sales_representative_old['division_2']                 = $obj_sales_representative_bean->division_2;
            $array_sales_representative_old['title_2']                    = $obj_sales_representative_bean->title_2;
            $array_sales_representative_old['tel_no']                     = $obj_sales_representative_bean->tel_no;
            $array_sales_representative_old['ext_no']                     = $obj_sales_representative_bean->ext_no;
            $array_sales_representative_old['fax_no']                     = $obj_sales_representative_bean->fax_no;
            $array_sales_representative_old['email1']                     = $obj_sales_representative_bean->email1;
            $array_sales_representative_old['gc_nttcomgroupcompany_id_c'] = $obj_sales_representative_bean->gc_nttcomgroupcompany_id_c;
            $array_sales_representative_old['previous_version_id']        = $obj_sales_representative_bean->previous_version_id;

            // If New field values is not same as Old fields values then update sales representative record.
            if (array_values($array_sales_representative_old) !== array_values($array_sales_representative_new)) {
                // Update sales representative record.
                $this->setContractSalesRepEntryService($array_sales_representative_new, $bean->id, true, $is_sugar_request);
            }
        }

        $arr_line_item               = array();
        $deleted_item                = isset($_POST['deletedItem']) ? array_values(array_filter(explode(',', $_POST['deletedItem']))) : array();
        $deleted_item_history        = isset($_POST['deletedItemHistory']) ? array_values(array_filter(explode(',', $_POST['deletedItemHistory']))) : array();
        $deleted_payment_item        = isset($_POST['paymentItem']) ? array_values(array_filter(explode(',', $_POST['paymentItem']))) : array();
        $deleted_payment_item_id     = array();
        $deleted_payment_item_module = array();

        // do not delete payment type when changed while saving new version (DF-155 regression)
        if (isset($_POST['version_upgrade']) && $_POST['version_upgrade'] != 'true') {
            foreach ($deleted_payment_item as $value) {
                if ($value != '') {
                    $temp                          = explode('|', $value);
                    $deleted_payment_item_id[]     = $temp[1];
                    $deleted_payment_item_module[] = $temp[0];
                }
            }
        }

        $tech_account_id = '';
        $bill_account_id = '';
        $nolineitem      = isset($_POST['nolineitem']) ? $_POST['nolineitem'] : '';
        for ($i = 0; $i < $nolineitem; $i++) {

            $line_item_order     = $i + 1;
            $line_item_id        = isset($_POST['line_item_id_' . $line_item_order]) ? $_POST['line_item_id_' . $line_item_order] : '';
            $flag_prod_configuration_change = $flg_lineitem_update = $flg_lineitem_delete = $flag_prod_specification_change = false;

            $product_configuration_details = $product_price_details = $product_rule_details = array();

            // if line item delete flag is 1 (true) then delete the line item
            if (isset($_POST['li_deleted_' . $line_item_order]) && $_POST['li_deleted_' . $line_item_order] == 1) {
                $flg_lineitem_delete = true;
            }

            if (isset($_POST['li_prod_specification_changed_' . $line_item_order]) && $_POST['li_prod_specification_changed_' . $line_item_order] == 1) {
                $flag_prod_specification_change = true;
            }

            if (isset($_POST['li_prod_configuration_changed_' . $line_item_order]) && $_POST['li_prod_configuration_changed_' . $line_item_order] == 1) {
                $flag_prod_configuration_change = true;
            }

            if ($flg_lineitem_delete && !empty($line_item_id)) {
                $this->removeLineItemEntryService($line_item_id, $bean);
                continue;
            } elseif ($flg_lineitem_delete && empty($line_item_id)) {
                continue;
            }
            // if line item delete flag is true and new line item is included, ignore the line item data

            // if line item id is present, update line item flag is set to true
            if (!empty($line_item_id)) {
                $flg_lineitem_update = true;
            }

            // if product specification field is selected then gather product specs related data otherwise ignore
            if (!empty($_POST['product_specification_' . $line_item_order])) {
                $pc_characteristic_name = isset($_POST['pc_characteristic_name_' . $line_item_order]) ? $_POST['pc_characteristic_name_' . $line_item_order] : '';
                // product configuration fields array
                for ($cnt = 0; $cnt < count($pc_characteristic_name); $cnt++) {
                    $product_configuration_details[] = array(
                        'id'                   => isset($_POST['pc_characteristic_record_id_' . $line_item_order][$cnt]) ? trim($_POST['pc_characteristic_record_id_' . $line_item_order][$cnt]) : '',
                        'name'                 => isset($_POST['pc_characteristic_name_' . $line_item_order][$cnt]) ? trim($_POST['pc_characteristic_name_' . $line_item_order][$cnt]) : '',
                        'characteristic_value' => isset($_POST['pc_characteristic_value_' . $line_item_order][$cnt]) ? trim($_POST['pc_characteristic_value_' . $line_item_order][$cnt]) : '',
                        'previous_version_id'  => isset($_POST['pc_characteristic_record_parent_id_' . $line_item_order][$cnt]) ? trim($_POST['pc_characteristic_record_parent_id_' . $line_item_order][$cnt]) : '',
                    );
                }

                $pp_charge_name = isset($_POST['pp_charge_name_' . $line_item_order]) ? $_POST['pp_charge_name_' . $line_item_order] : '';

                if(!empty($pp_charge_name)){
                // product price fields array
                for ($cnt = 0; $cnt < count($pp_charge_name); $cnt++) {
                    $product_price_details[] = array(
                        'id'                      => isset($_POST['pp_charge_record_id_' . $line_item_order][$cnt]) ? trim($_POST['pp_charge_record_id_' . $line_item_order][$cnt]) : '',
                        'name'                    => isset($_POST['pp_charge_name_' . $line_item_order][$cnt]) ? trim($_POST['pp_charge_name_' . $line_item_order][$cnt]) : '',
                        'charge_type'             => isset($_POST['pp_charge_type_' . $line_item_order][$cnt]) ? trim($_POST['pp_charge_type_' . $line_item_order][$cnt]) : '',
                        'charge_period'           => isset($_POST['pp_charge_period_' . $line_item_order][$cnt]) ? trim($_POST['pp_charge_period_' . $line_item_order][$cnt]) : '',
                        'list_price'              => isset($_POST['pp_list_price_' . $line_item_order][$cnt]) ? trim($_POST['pp_list_price_' . $line_item_order][$cnt]) : '',
                        'discount'                => isset($_POST['pp_discount_' . $line_item_order][$cnt]) ? trim($_POST['pp_discount_' . $line_item_order][$cnt]) : '',
                        'percent_discount_flag'   => isset($_POST['pp_percent_discount_flag_' . $line_item_order][$cnt]) ? trim($_POST['pp_percent_discount_flag_' . $line_item_order][$cnt]) : '',
                        'icb_flag'                => isset($_POST['pp_icb_flag_' . $line_item_order][$cnt]) ? trim($_POST['pp_icb_flag_' . $line_item_order][$cnt]) : '',
                        'customer_contract_price' => isset($_POST['pp_customer_contract_price_' . $line_item_order][$cnt]) ? trim($_POST['pp_customer_contract_price_' . $line_item_order][$cnt]) : '',
                        'igc_settlement_price'    => isset($_POST['pp_igc_settlement_price_' . $line_item_order][$cnt]) ? trim($_POST['pp_igc_settlement_price_' . $line_item_order][$cnt]) : '',
                        'currencyid'              => isset($_POST['pp_charge_currency_' . $line_item_order][$cnt]) ? trim($_POST['pp_charge_currency_' . $line_item_order][$cnt]) : '',
                        'previous_version_id'     => isset($_POST['pp_charge_record_parent_id_' . $line_item_order][$cnt]) ? trim($_POST['pp_charge_record_parent_id_' . $line_item_order][$cnt]) : '',
                    );
                }
                }

                if (isset($_POST['pr_rule_id_' . $line_item_order]) && is_array($_POST['pr_rule_id_' . $line_item_order])) {
                    // product rule fields array
                    for ($cnt = 0; $cnt < count($_POST['pr_rule_id_' . $line_item_order]); $cnt++) {
                        $product_rule_details[] = array(
                            'id'                  => $_POST['pr_rule_record_id_' . $line_item_order][$cnt],
                            'name'                => $_POST['pr_rule_id_' . $line_item_order][$cnt],
                            'description'         => $_POST['pr_description_' . $line_item_order][$cnt],
                            'previous_version_id' => $_POST['pr_rule_record_parent_id_' . $line_item_order][$cnt],
                        );
                    }
                }
            }

            $arr_line_type_action_mapping = array(
                'New'                 => 'add',
                'Change'              => 'change',
                'Termination'         => 'remove',
                'Service_Termination' => 'remove',
                'Activated'           => 'no_change',
            );

            $arr_line_item[$i] = array(
                'line_item_id'                     => $line_item_id,
                'ContractLineItemHeader'           => array(
                    'service_start_date'          => isset($_POST['service_start_date_hidden_' . $line_item_order]) ? $modUtils->getDbDate(trim($_POST['service_start_date_hidden_' . $line_item_order])) : '',
                    'service_end_date'            => isset($_POST['service_end_date_hidden_' . $line_item_order]) ? $modUtils->getDbDate(trim($_POST['service_end_date_hidden_' . $line_item_order])) : '',
                    'billing_start_date'          => isset($_POST['billing_start_date_hidden_' . $line_item_order]) ? $modUtils->getDbDate(trim($_POST['billing_start_date_hidden_' . $line_item_order])) : '',
                    'billing_end_date'            => isset($_POST['billing_end_date_hidden_' . $line_item_order]) ? $modUtils->getDbDate(trim($_POST['billing_end_date_hidden_' . $line_item_order])) : '',

                    'gc_timezone_id_c'            => isset($_POST['service_start_date_timezone_' . $line_item_order]) ? trim($_POST['service_start_date_timezone_' . $line_item_order]) : '',
                    'gc_timezone_id1_c'           => isset($_POST['service_end_date_timezone_' . $line_item_order]) ? trim($_POST['service_end_date_timezone_' . $line_item_order]) : '',
                    'gc_timezone_id2_c'           => isset($_POST['billing_start_date_timezone_' . $line_item_order]) ? trim($_POST['billing_start_date_timezone_' . $line_item_order]) : '',
                    'gc_timezone_id3_c'           => isset($_POST['billing_end_date_timezone_' . $line_item_order]) ? trim($_POST['billing_end_date_timezone_' . $line_item_order]) : '',

                    'service_start_date_timezone' => isset($_POST['service_start_date_timezone_name_' . $line_item_order]) ? trim($_POST['service_start_date_timezone_name_' . $line_item_order]) : '',
                    'service_end_date_timezone'   => isset($_POST['service_end_date_timezone_name_' . $line_item_order]) ? trim($_POST['service_end_date_timezone_name_' . $line_item_order]) : '',
                    'billing_start_date_timezone' => isset($_POST['billing_start_date_timezone_name_' . $line_item_order]) ? trim($_POST['billing_start_date_timezone_name_' . $line_item_order]) : '',
                    'billing_end_date_timezone'   => isset($_POST['billing_end_date_timezone_name_' . $line_item_order]) ? trim($_POST['billing_end_date_timezone_name_' . $line_item_order]) : '',

                    'factory_reference_no'        => isset($_POST['factory_reference_no_' . $line_item_order]) ? trim($_POST['factory_reference_no_' . $line_item_order]) : '',
                    'payment_type'                => isset($_POST['payment_type_' . $line_item_order]) ? trim($_POST['payment_type_' . $line_item_order]) : '',
                    'tech_account_id'             => isset($_POST['tech_account_id_' . $line_item_order]) ? trim($_POST['tech_account_id_' . $line_item_order]) : '',
                    'bill_account_id'             => isset($_POST['bill_account_id_' . $line_item_order]) ? trim($_POST['bill_account_id_' . $line_item_order]) : '',
                    'line_item_history_id'        => isset($_POST['line_item_history_id_' . $line_item_order]) ? trim($_POST['line_item_history_id_' . $line_item_order]) : '',
                    'previous_version_id'         => isset($_POST['line_item_history_parent_id_' . $line_item_order]) ? trim($_POST['line_item_history_parent_id_' . $line_item_order]) : '',
                    'product_quantity'            => isset($_POST['product_quantity_' . $line_item_order]) ? trim($_POST['product_quantity_' . $line_item_order]) : '',
                    'global_contract_item_id'     => isset($_POST['global_contract_item_id_' . $line_item_order]) ? trim($_POST['global_contract_item_id_' . $line_item_order]) : '',

                    'product_specification'       => isset($_POST['product_specification_' . $line_item_order]) ? trim($_POST['product_specification_' . $line_item_order]) : '',
                    'cust_contract_currency'      => isset($_POST['cust_contract_currency_' . $line_item_order]) ? trim($_POST['cust_contract_currency_' . $line_item_order]) : '',
                    'cust_billing_currency'       => isset($_POST['cust_billing_currency_' . $line_item_order]) ? trim($_POST['cust_billing_currency_' . $line_item_order]) : '',
                    'igc_contract_currency'       => isset($_POST['igc_contract_currency_' . $line_item_order]) ? trim($_POST['igc_contract_currency_' . $line_item_order]) : '',
                    'igc_billing_currency'        => isset($_POST['igc_billing_currency_' . $line_item_order]) ? trim($_POST['igc_billing_currency_' . $line_item_order]) : '',
                    'customer_billing_method'     => isset($_POST['customer_billing_method_' . $line_item_order]) ? trim($_POST['customer_billing_method_' . $line_item_order]) : '',
                    'igc_settlement_method'       => isset($_POST['igc_settlement_method_' . $line_item_order]) ? trim($_POST['igc_settlement_method_' . $line_item_order]) : '',
                    'description'                 => isset($_POST['description_' . $line_item_order]) ? trim($_POST['description_' . $line_item_order]) : '',
                    'contract_line_item_type'     => isset($_POST['contract_line_item_type_' . $line_item_order]) ? trim($_POST['contract_line_item_type_' . $line_item_order]) : '',
                    'bi_action'                   => isset($_POST['contract_line_item_type_' . $line_item_order]) ? $arr_line_type_action_mapping[$_POST['contract_line_item_type_' . $line_item_order]] : '',
                    'gc_organization_id_c'        => isset($_POST['gc_organization_id_' . $line_item_order]) ? trim($_POST['gc_organization_id_' . $line_item_order]) : '',
                ),
                'ContractLineItems'                => array(
                    /* 'product_id'          => isset($_POST['product_id_' . $line_item_order]) ? trim($_POST['product_id_' . $line_item_order]) : '', */
                    'previous_version_id' => isset($_POST['line_item_parent_id_' . $line_item_order]) ? trim($_POST['line_item_parent_id_' . $line_item_order]) : '',
                ),
                'ContractLineProductConfiguration' => $product_configuration_details,
                'ContractLineProductPrice'         => $product_price_details,
                'ContractLineProductRule'          => $product_rule_details,
            );

            // duplicate billing and technology accounts when new version of contract is created
            if (isset($_POST['version_upgrade']) && $_POST['version_upgrade'] == 'true') {
                $create_accounts = true;
                if ($bean->billing_type == 'OneStop' && $i != 0) {
                    $create_accounts                                                = false;
                    $arr_line_item[$i]['ContractLineItemHeader']['tech_account_id'] = $tech_account_id;
                    $arr_line_item[$i]['ContractLineItemHeader']['bill_account_id'] = $bill_account_id;
                }

                if ($create_accounts == true) {
                    if (isset($arr_line_item[$i]['ContractLineItemHeader']['tech_account_id']) && !empty($arr_line_item[$i]['ContractLineItemHeader']['tech_account_id'])) {
                        $new_bean = $modUtils->duplicateBeanRecord('Contacts', $arr_line_item[$i]['ContractLineItemHeader']['tech_account_id'], array());

                        // save accounts (japan specific)
                        $obj_accountsjs_bean = BeanFactory::getBean('js_Accounts_js');
                        $obj_accountsjs_bean->retrieve_by_string_fields(array(
                            'contact_id_c' => $arr_line_item[$i]['ContractLineItemHeader']['tech_account_id'],
                        ));
                        $modUtils->duplicateBeanRecord('js_Accounts_js', $obj_accountsjs_bean->id, array(
                            'contact_id_c' => $new_bean->id,
                        ));

                        if (!empty($new_bean->id)) {
                            $arr_line_item[$i]['ContractLineItemHeader']['tech_account_id'] = $new_bean->id;
                        }

                        $tech_account_id = $new_bean->id;
                        unset($new_bean, $obj_accountsjs_bean);
                    }

                    if (isset($arr_line_item[$i]['ContractLineItemHeader']['bill_account_id']) && !empty($arr_line_item[$i]['ContractLineItemHeader']['bill_account_id'])) {
                        $new_bean = $modUtils->duplicateBeanRecord('Contacts', $arr_line_item[$i]['ContractLineItemHeader']['bill_account_id'], array());

                        // save accounts (japan specific)
                        $obj_accountsjs_bean = BeanFactory::getBean('js_Accounts_js');
                        $obj_accountsjs_bean->retrieve_by_string_fields(array(
                            'contact_id_c' => $arr_line_item[$i]['ContractLineItemHeader']['bill_account_id'],
                        ));
                        $modUtils->duplicateBeanRecord('js_Accounts_js', $obj_accountsjs_bean->id, array(
                            'contact_id_c' => $new_bean->id,
                        ));

                        if (!empty($new_bean->id)) {
                            $arr_line_item[$i]['ContractLineItemHeader']['bill_account_id'] = $new_bean->id;
                        }

                        $bill_account_id = $new_bean->id;
                        unset($new_bean, $obj_accountsjs_bean);
                    }
                }
            }

            if (isset($_POST['payment_type_' . $line_item_order]) && trim($_POST['payment_type_' . $line_item_order]) == '00') {
                $payment_id = isset($_POST['gc_DirectDeposit_id_' . $line_item_order]) ? trim($_POST['gc_DirectDeposit_id_' . $line_item_order]) : '';
                if (in_array($payment_id, $deleted_payment_item_id)) {
                    $payment_id = '';
                }
                $arr_line_item[$i]['ContractDirectDeposit'] = array(
                    'id'                          => $payment_id,
                    'previous_version_id'         => isset($_POST['gc_DirectDeposit_parent_id_' . $line_item_order]) ? trim($_POST['gc_DirectDeposit_parent_id_' . $line_item_order]) : '',
                    'bank_code'                   => isset($_POST['dd_bank_code_' . $line_item_order]) ? trim($_POST['dd_bank_code_' . $line_item_order]) : '',
                    'branch_code'                 => isset($_POST['dd_branch_code_' . $line_item_order]) ? trim($_POST['dd_branch_code_' . $line_item_order]) : '',
                    'deposit_type'                => isset($_POST['dd_deposit_type_' . $line_item_order]) ? trim($_POST['dd_deposit_type_' . $line_item_order]) : '',
                    'account_no'                  => isset($_POST['dd_account_no_' . $line_item_order]) ? trim($_POST['dd_account_no_' . $line_item_order]) : '',
                    'payee_contact_person_name_1' => isset($_POST['dd_person_name_1_' . $line_item_order]) ? trim($_POST['dd_person_name_1_' . $line_item_order]) : '',
                    'payee_contact_person_name_2' => isset($_POST['dd_person_name_2_' . $line_item_order]) ? trim($_POST['dd_person_name_2_' . $line_item_order]) : '',
                    'payee_tel_no'                => isset($_POST['dd_payee_tel_no_' . $line_item_order]) ? trim($_POST['dd_payee_tel_no_' . $line_item_order]) : '',
                    'payee_email_address'         => isset($_POST['dd_payee_email_address_' . $line_item_order]) ? trim($_POST['dd_payee_email_address_' . $line_item_order]) : '',
                    'remarks'                     => isset($_POST['dd_remarks_' . $line_item_order]) ? trim($_POST['dd_remarks_' . $line_item_order]) : '',
                );
            }
            if (isset($_POST['payment_type_' . $line_item_order]) && trim($_POST['payment_type_' . $line_item_order]) == '10') {
                $payment_id = isset($_POST['gc_AccountTransfer_id_' . $line_item_order]) ? trim($_POST['gc_AccountTransfer_id_' . $line_item_order]) : '';
                if (in_array($payment_id, $deleted_payment_item_id)) {
                    $payment_id = '';
                }
                $arr_line_item[$i]['ContractAccountTransfer'] = array(
                    'id'                  => $payment_id,
                    'previous_version_id' => isset($_POST['gc_AccountTransfer_parent_id_' . $line_item_order]) ? trim($_POST['gc_AccountTransfer_parent_id_' . $line_item_order]) : '',
                    'name'                => isset($_POST['at_name_' . $line_item_order]) ? trim($_POST['at_name_' . $line_item_order]) : '',
                    'bank_code'           => isset($_POST['at_bank_code_' . $line_item_order]) ? trim($_POST['at_bank_code_' . $line_item_order]) : '',
                    'branch_code'         => isset($_POST['at_branch_code_' . $line_item_order]) ? trim($_POST['at_branch_code_' . $line_item_order]) : '',
                    'deposit_type'        => isset($_POST['at_deposit_type_' . $line_item_order]) ? trim($_POST['at_deposit_type_' . $line_item_order]) : '',
                    'account_no'          => isset($_POST['at_account_no_' . $line_item_order]) ? trim($_POST['at_account_no_' . $line_item_order]) : '',
                    'account_no_display'  => isset($_POST['at_account_no_display_' . $line_item_order]) ? trim($_POST['at_account_no_display_' . $line_item_order]) : '',
                );
            }
            if (isset($_POST['payment_type_' . $line_item_order]) && trim($_POST['payment_type_' . $line_item_order]) == '30') {
                $payment_id = isset($_POST['gc_PostalTransfer_id_' . $line_item_order]) ? $_POST['gc_PostalTransfer_id_' . $line_item_order] : '';
                if (in_array($payment_id, $deleted_payment_item_id)) {
                    $payment_id = '';
                }
                $arr_line_item[$i]['ContractPostalTransfer'] = array(
                    'id'                         => $payment_id,
                    'previous_version_id'        => isset($_POST['gc_PostalTransfer_parent_id_' . $line_item_order]) ? trim($_POST['gc_PostalTransfer_parent_id_' . $line_item_order]) : '',
                    'name'                       => isset($_POST['pt_name_' . $line_item_order]) ? trim($_POST['pt_name_' . $line_item_order]) : '',
                    'postal_passbook_mark'       => isset($_POST['pt_postal_passbook_mark_' . $line_item_order]) ? trim($_POST['pt_postal_passbook_mark_' . $line_item_order]) : '',
                    'postal_passbook'            => isset($_POST['pt_postal_passbook_' . $line_item_order]) ? trim($_POST['pt_postal_passbook_' . $line_item_order]) : '',
                    'postal_passbook_no_display' => isset($_POST['pt_postal_passbook_no_display_' . $line_item_order]) ? trim($_POST['pt_postal_passbook_no_display_' . $line_item_order]) : '',
                );
            }
            if (isset($_POST['payment_type_' . $line_item_order]) && trim($_POST['payment_type_' . $line_item_order]) == '60') {
                $payment_id = isset($_POST['gc_CreditCard_id_' . $line_item_order]) ? trim($_POST['gc_CreditCard_id_' . $line_item_order]) : '';
                if (in_array($payment_id, $deleted_payment_item_id)) {
                    $payment_id = '';
                }
                $arr_line_item[$i]['ContractCreditCard'] = array(
                    'id'                  => $payment_id,
                    'previous_version_id' => isset($_POST['gc_CreditCard_parent_id_' . $line_item_order]) ? trim($_POST['gc_CreditCard_parent_id_' . $line_item_order]) : '',
                    'credit_card_no'      => isset($_POST['cc_credit_card_no_' . $line_item_order]) ? trim($_POST['cc_credit_card_no_' . $line_item_order]) : '',
                    'expiration_date'     => isset($_POST['cc_expiration_date_' . $line_item_order]) ? trim($_POST['cc_expiration_date_' . $line_item_order]) : '',
                );
            }

            // if product specification is changed, remove old related data
            if ($flag_prod_specification_change && !empty($line_item_id)) {
                self::removeOldProductSpecificationData($line_item_id);
            }

            // if product configuration is changed, remove old related data
            if ($flag_prod_configuration_change && !empty($line_item_id)) {
                self::removeOldProductConfigurationData($line_item_id);
            }

            $line_item_save_response = array();
            $obj_line_item           = $this->setContractLineItemEntryService($arr_line_item[$i], $bean->id, $arr_line_item[$i]['ContractLineItemHeader']['tech_account_id'], $arr_line_item[$i]['ContractLineItemHeader']['bill_account_id'], $flg_lineitem_update, $line_item_save_response);
            unset($line_item_save_response);

            // map line item history id with group
            $li_invoice_subgroup = isset($_POST['li_invoice_subgroup_' . $line_item_order]) ? trim($_POST['li_invoice_subgroup_' . $line_item_order]) : '';
            if (array_key_exists($li_invoice_subgroup, $invoice_group_details)) {
                $invoice_group_details[$_POST['li_invoice_subgroup_' . $line_item_order]]['line_item_history_id'][] = $obj_line_item->line_item_contract_history_id;
            }

            // get the new group information
            $new_group_name = ((!empty($invoice_group_details[$li_invoice_subgroup]['name'])) ? $invoice_group_details[$li_invoice_subgroup]['name'] : '');
            $new_group_id   = ((!empty($invoice_group_details[$li_invoice_subgroup]['uuid'])) ? $invoice_group_details[$li_invoice_subgroup]['uuid'] : '');

            // get the old group information
            $old_group_name = "";
            $old_group_id   = "";
            if (isset($_POST['line_item_history_id_' . $line_item_order]) && !empty($_POST['line_item_history_id_' . $line_item_order])) {
                $line_item_history_id         = $_POST['line_item_history_id_' . $line_item_order];
                $invoicesubtotal_group_detail = CustomFunctionGcContract::getInvoiceGroupByLineItem($line_item_history_id);
                if (is_array($invoicesubtotal_group_detail) && count($invoicesubtotal_group_detail) > 0) {
                    $old_group_id   = (!empty($invoicesubtotal_group_detail['id'])) ? $invoicesubtotal_group_detail['id'] : "";
                    $old_group_name = (!empty($invoicesubtotal_group_detail['group_name'])) ? $invoicesubtotal_group_detail['group_name'] : "";
                }
            }

            // checking for new values and send to audit log
            if ($old_group_id != $new_group_id) {
                $obj_line_item_bean = BeanFactory::getBean('gc_LineItem');
                $obj_line_item_bean->retrieve($line_item_id);
                $obj_lineitem_history_bean = new ClassLineItemHistoryBeforeSave();
                $obj_lineitem_history_bean->insertLineItemHistoryAudit($obj_line_item_bean, 'Invoice Subtotal Group', 'dropdown', $old_group_name, $new_group_name);
            }
        }

        if (!empty($invoice_group_details) && is_array($invoice_group_details)) {
            // perform invoice group entry service
            foreach ($invoice_group_details as $key => $val) {
                $val['contract_id'] = $bean->id;
                if (!isset($val['line_item_history_id'])) {
                    $val['line_item_history_id'] = array();
                }
                $val['description']         = !empty($val['desc']) ? trim($val['desc']) : '';
                $val['previous_version_id'] = !empty($val['parent_uuid']) ? trim($val['parent_uuid']) : '';
                $this->setInvoiceGroupEntryService($val, true);
            }
        }

        if (!empty($deleted_item)) {
            $this->delLineItem($deleted_item);
        }
        if (!empty($deleted_item_history)) {
            $this->delLineItemHistory($deleted_item_history);
        }
        if (!empty($deleted_payment_item_id)) {
            $this->delPaymentItem($deleted_payment_item_id, $deleted_payment_item_module);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * method to delete line item.
     *
     * @param array $arr_delete_line_item list of line item for delete
     * @author Dinesh.Itkar
     * @since 05-Jan-2016
     */
    public function delLineItem($arr_delete_line_item)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'arr_delete_line_item: '.print_r($arr_delete_line_item,true), 'Method to delete line item');
        foreach ($arr_delete_line_item as $delete_line_item) {
            $focus = new gc_LineItem();
            $focus->retrieve($delete_line_item);
            $focus->deleted = '1';
            $focus->save();
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * method to delete line item history table.
     *
     * @param array $arr_delete_line_item_history list of line item history table for delete
     * @author Dinesh.Itkar
     * @since 05-Jan-2016
     */
    public function delLineItemHistory($arr_delete_line_item_history)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'arr_delete_line_item_history: '.print_r($arr_delete_line_item_history,true), 'Method to delete line item history table');
        foreach ($arr_delete_line_item_history as $delete_line_item_history) {
            $focus = new gc_Line_Item_Contract_History();
            $focus->retrieve($delete_line_item_history);
            $focus->deleted = '1';
            $focus->save();
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * method to delete Payment line item .
     *
     * @param array $arr_delete_line_item_history list of line item history table for delete
     * @author Dinesh.Itkar
     * @since 05-Jan-2016
     */
    public function delPaymentItem($ids, $modules)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'ids: '.print_r($ids,true).GCM_GL_LOG_VAR_SEPARATOR.'modules: '.print_r($modules,true), 'Method to delete Payment line item');
        
        foreach ($ids as $key => $id) {
            $focus = BeanFactory::getBean($modules[$key]);
            $focus->retrieve($id);
            $focus->deleted = '1';
            $focus->save();
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * method to delete Product Specification related module record
     *
     * @param string $line_item_id line item id
     * @return boolean
     * @author sagar.salunkhe
     * @since 16-Apr-2016
     */
    public function removeOldProductSpecificationData($line_item_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'line_item_id: '.$line_item_id, 'Method to delete Product Specification related module record');
        
        // fetch and soft delete product configuration data
        $obj_product_configuration_bean = BeanFactory::getBean('gc_ProductConfiguration')->get_list('', "line_item_id = '$line_item_id'", null);
        foreach ($obj_product_configuration_bean['list'] as $focus) {
            $focus->deleted = 1;
            $focus->save();
        }
        // fetch and soft delete product price data
        $obj_product_price_bean = BeanFactory::getBean('gc_ProductPrice')->get_list('', "line_item_id = '$line_item_id'", null);
        foreach ($obj_product_price_bean['list'] as $focus) {
            $focus->deleted = 1;
            $focus->save();
        }
        // fetch and soft delete product rule data
        $obj_product_rule_bean = BeanFactory::getBean('gc_ProductRule')->get_list('', "line_item_id = '$line_item_id'", null);
        foreach ($obj_product_rule_bean['list'] as $focus) {
            $focus->deleted = 1;
            $focus->save();
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return true;
    }

    /**
     * method to delete Product Configuration related module record
     *
     * @param string $line_item_id line item id
     * @return boolean
     * @author dinesh.itkar
     * @since 22-Apr-2016
     */
    public function removeOldProductConfigurationData($line_item_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'line_item_id: '.$line_item_id, 'Method to delete Product Configuration related module record');
        
        // fetch and soft delete product price data
        $obj_product_price_bean = BeanFactory::getBean('gc_ProductPrice')->get_list('', "line_item_id = '$line_item_id'", null);
        foreach ($obj_product_price_bean['list'] as $focus) {
            $focus->deleted = 1;
            $focus->save();
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return true;
    }

    /**
     * function is triggered when upgraded version of contract is saved
     *
     * @param object $bean
     * @param array $event
     * @param array $arguments
     * @author sagar.salunkhe
     * @since Aug 16, 2016
     */
    public function processVersionUpgradeSave(&$bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method will be triggered when upgraded version of contract is saved');
        $modUtils = new ModUtils();
        if (isset($_POST['version_upgrade']) && $_POST['version_upgrade'] == 'true') {
            // save contracting corporate
            if (!empty($bean->accounts_gc_contracts_1accounts_ida)) {
                $contracting_corporate = BeanFactory::getBean('Accounts');
                $contracting_corporate->retrieve($bean->accounts_gc_contracts_1accounts_ida);

                if (empty($contracting_corporate->common_customer_id_c)) {
                    $new_bean = $modUtils->duplicateBeanRecord('Accounts', $contracting_corporate->id, array());
                    $bean->load_relationship('accounts_gc_contracts_1');
                    $bean->accounts_gc_contracts_1->add(array(
                        $new_bean->id,
                    ));
                }
            }

            // save contracting account
            if (!empty($bean->contacts_gc_contracts_1contacts_ida)) {
                $new_bean = $modUtils->duplicateBeanRecord('Contacts', $bean->contacts_gc_contracts_1contacts_ida, array());
                $bean->load_relationship('contacts_gc_contracts_1');
                $bean->contacts_gc_contracts_1->add(array(
                    $new_bean->id,
                ));

                // save accounts (japan specific)
                $obj_accountsjs_bean = BeanFactory::getBean('js_Accounts_js');
                $obj_accountsjs_bean->retrieve_by_string_fields(array(
                    'contact_id_c' => $bean->contacts_gc_contracts_1contacts_ida,
                ));

                $modUtils->duplicateBeanRecord('js_Accounts_js', $obj_accountsjs_bean->id, array(
                    'contact_id_c' => $new_bean->id,
                ));
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}
