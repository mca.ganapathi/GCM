<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once ('custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php');

/**
 * Module API - Service Class File
 *
 * @author sagar.salunkhe
 * @since Nov 09, 2016
 */
class ClassReportServiceGcContracts
{

    private $db;

    private $export_key;

    private $arr_report_headers = array();

    private $arr_prdct_offrng_dtls = array();

    /**
     *
     * @author sagar.salunkhe
     * @since Nov 09, 2016
     */
    function __construct()
    {
        global $db;
        $this->db = $db;
    }

    /**
     * generic function to serve all report queries
     *
     * @param array $params
     * @return array return array of result data
     *
     * @author sagar.salunkhe
     * @since Nov 10, 2016
     */
    public function generateReport($params)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'generic function to serve all report queries');

        $this->export_key = $params['export_key'];
        $csv_data = array();

        // 1. Check if key is valid
        $arr_report_keys = self::getValidExportKeys();
        if (!array_key_exists($this->export_key, $arr_report_keys)) {
            return false;
        }

        $function_key = $arr_report_keys[$this->export_key];

        // 2. build report header columns
        $this->arr_report_headers = self::getExportReportHeaders($this->export_key);
        foreach ($this->arr_report_headers as $db_field => $csv_header) {
            $csv_data[0][] = $csv_header;
        }

        // 3. fetch report data from db
        self::generateExportReportResult($csv_data, $function_key, $params);


        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'csv_data : ' . print_r($csv_data, true), 'generic function to serve all report queries');

        // 4. convert data array to csv and return
        return ModUtils::processArrayToCsv($csv_data, ',', '"', true, false);
    }

    /**
     * method to generate report in csv format
     *
     * @param array $csv_data
     * @param string $function_key
     * @param array $params
     *
     * @author sagar.salunkhe
     * @since Dec 30, 2016
     */
    private function generateExportReportResult(&$csv_data, $function_key, $params)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'csv_data : ' . print_r($csv_data, true) . GCM_GL_LOG_VAR_SEPARATOR . 'function_key : ' . print_r($function_key, true), 'method to generate report in csv format');
        global $sugar_config;

        if (isset($sugar_config['gbs_api_po']) && count($sugar_config['gbs_api_po']) > 0) {
            $params['filter']['product_offering'] = array_filter($sugar_config['gbs_api_po']);
        }

        if (isset($sugar_config['gcm_cstm_const']['gbs_api_exclude']['billing_affiliate_code']) && count($sugar_config['gcm_cstm_const']['gbs_api_exclude']['billing_affiliate_code']) > 0) {
            $params['filter']['billing_affiliate_code'] = array_filter($sugar_config['gcm_cstm_const']['gbs_api_exclude']['billing_affiliate_code']);
        }

        $arr_record_type = array(
                                'New',
                                'Mod'
        );

        foreach ($arr_record_type as $key => $rec_type) {
            $query = '';
            $function_select_query = "get{$function_key}{$rec_type}SelectQuery";
            self::$function_select_query($query);

            $function_filter_query = "get{$function_key}{$rec_type}FilterQuery";
            self::$function_filter_query($query, $params['filter']);

            $function_report_result = "get{$function_key}QueryResult";
            self::$function_report_result($query, $params, $csv_data);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'csv_data : ' . print_r($csv_data, true), 'method to generate report in csv format');
        return;
    }

    /**
     * method to return valid export keys
     *
     * @return array export keys array
     *
     * @author sagar.salunkhe
     * @since Nov 10, 2016
     */
    private function getValidExportKeys()
    {
    	$export_keys = array(
    			'export-billing-accounts-for-gbs' => 'BillingAccountsGBS',
    			'export-contract-accounts-for-gbs' => 'ContractAccountsGBS'
    	);
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'export_keys : ' . print_r($export_keys, true), 'method to return valid export keys');
        return $export_keys;
    }

    /**
     * method to build billing accounts new records query
     *
     * @param array $filters
     * @return string billing accounts sql query
     *
     * @author sagar.salunkhe
     * @since Nov 10, 2016
     */
    private function getBillingAccountsGBSNewSelectQuery(&$query)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'query : ' . $query, 'method to build billing accounts new records query');
        $query .= <<<EOF
SELECT
    cntracts.global_contract_id
    ,cntracts.product_offering
    ,1 AS order_type
    ,1 AS data_type
    ,org.billing_affiliate_code AS billing_affiliate
    ,NULL AS affiliate
    ,NULL AS service_type
    ,IF(bill_acnt.name IS NULL OR bill_acnt.name = '', bill_acnt_cstm.name_2_c , bill_acnt.name) AS billing_account_name
    ,bill_cntry.country_code_alphabet AS billing_country
    ,IF(bill_cnts_cust.addr_2_c IS NULL OR bill_cnts_cust.addr_2_c = '', bill_cnts_cust.addr_1_c, bill_cnts_cust.addr_2_c) AS billing_address_any_1
    ,bill_cnts_cust.addr_general_c AS billing_address_us_canada_1
    ,NULL AS billing_address_any_2
    ,CONCAT(bill_cnts_cust.city_general_c, ", ", bill_state.name, ", ", bill_cnts_cust.postal_no_c) AS billing_address_us_canada_2
    ,NULL AS billing_address_3
    ,bill_cnts_cust.attention_line_c AS attention
    ,bill_acnt_cstm.common_customer_id_c AS billing_cid
    ,IF(sales.sales_rep_name_1 IS NULL OR sales.sales_rep_name_1 = '', sales.name, sales.sales_rep_name_1) AS account_manager
    ,IF(bill_cnts_cust.name_2_c IS NULL OR bill_cnts_cust.name_2_c = '', bill_cnts.last_name, bill_cnts_cust.name_2_c) AS contact_name
    ,IF(bill_cnts.phone_work IS NULL OR bill_cnts.phone_work = '', bill_cnts.phone_mobile, bill_cnts.phone_work) AS phone_number
    ,email.email_address AS email_address
    ,NULL AS bill_cycle
    ,tbl_line_item.cust_billing_currency AS billing_currency

FROM  `gc_contracts` AS cntracts
    LEFT JOIN (
        SELECT
            line_contract_no,
            contracts_id,
            bill_account_id,
            gc_organization_id_c,
            cust_billing_currency,
			customer_billing_method
        FROM (
            SELECT
                gc_lineitem.name AS line_contract_no
                ,gc_line_item_contract_history.contracts_id AS contracts_id
                ,gc_line_item_contract_history.bill_account_id AS bill_account_id
                ,gc_line_item_contract_history.gc_organization_id_c AS gc_organization_id_c
                ,gc_line_item_contract_history.cust_billing_currency AS cust_billing_currency
				,gc_line_item_contract_history.customer_billing_method AS customer_billing_method
            FROM gc_line_item_contract_history
            LEFT JOIN gc_lineitem ON gc_line_item_contract_history.line_item_id = gc_lineitem.id
            WHERE
                gc_lineitem.deleted = 0 AND
                gc_line_item_contract_history.deleted = 0
            ORDER BY gc_lineitem.name
            ) AS tbl_cust_line_item
        GROUP BY tbl_cust_line_item.contracts_id
        HAVING tbl_cust_line_item.line_contract_no IS NOT NULL
        ) tbl_line_item ON tbl_line_item.contracts_id = cntracts.id
    LEFT JOIN  `gc_organization` AS org ON org.id = tbl_line_item.gc_organization_id_c AND org.deleted = 0
    LEFT JOIN  `gc_salesrepresentative` AS sales ON sales.contracts_id = cntracts.id AND sales.deleted = 0

    LEFT JOIN  `contacts` AS bill_cnts ON bill_cnts.id = tbl_line_item.bill_account_id AND bill_cnts.deleted = 0
    LEFT JOIN  `contacts_cstm` AS bill_cnts_cust ON bill_cnts_cust.id_c = bill_cnts.id
    LEFT JOIN  `c_country` AS bill_cntry ON bill_cntry.id = bill_cnts_cust.c_country_id_c AND bill_cntry.deleted = 0
    LEFT JOIN  `c_state` AS bill_state ON bill_state.id = bill_cnts_cust.c_state_id_c AND bill_state.deleted = 0
    LEFT JOIN  `accounts_contacts` AS bill_acnt_bill_cnts ON bill_acnt_bill_cnts.contact_id = bill_cnts.id AND bill_acnt_bill_cnts.deleted = 0
    LEFT JOIN  `accounts` AS bill_acnt ON bill_acnt.id = bill_acnt_bill_cnts.account_id AND bill_acnt.deleted = 0
    LEFT JOIN  `accounts_cstm` AS bill_acnt_cstm ON bill_acnt_cstm.id_c = bill_acnt.id
    LEFT JOIN  `email_addr_bean_rel` AS bill_acnt_email ON bill_acnt_email.bean_id = bill_cnts.id AND bill_acnt_email.deleted = 0 AND bill_acnt_email.primary_address = 1
    LEFT JOIN  `email_addresses` AS email ON email.id = bill_acnt_email.email_address_id AND email.deleted = 0

EOF;
        return;
    }

    /**
     * method returns sql select columns and join table statements for billing account modified records report
     *
     * @param string $query
     * @return string
     *
     * @author sagar.salunkhe
     * @since Dec 30, 2016
     */
    private function getBillingAccountsGBSModSelectQuery(&$query)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'query : ' . $query, 'method returns sql select columns and join table statements for billing account modified records report');
        $query .= <<<EOF
SELECT
    cntracts.global_contract_id
    ,cntracts.product_offering
    ,2 AS order_type
    ,1 AS data_type
    ,NULL AS billing_affiliate
    ,NULL AS affiliate
    ,NULL AS service_type
    ,NULL AS billing_account_name
    ,NULL AS billing_country
    ,NULL AS billing_address_any_1
    ,NULL AS billing_address_us_canada_1
    ,NULL AS billing_address_any_2
    ,NULL AS billing_address_us_canada_2
    ,NULL AS billing_address_3
    ,NULL AS attention
    ,bill_acnt_cstm.common_customer_id_c AS billing_cid
    ,IF(sales.sales_rep_name_1 IS NULL OR sales.sales_rep_name_1 = '', sales.name, sales.sales_rep_name_1) AS account_manager
    ,IF(bill_cnts_cust.name_2_c IS NULL OR bill_cnts_cust.name_2_c = '', bill_cnts.last_name, bill_cnts_cust.name_2_c) AS contact_name
    ,IF(bill_cnts.phone_work IS NULL OR bill_cnts.phone_work = '', bill_cnts.phone_mobile, bill_cnts.phone_work) AS phone_number
    ,bill_acnt_email.email_address AS email_address
    ,NULL AS bill_cycle
    ,NULL AS billing_currency

FROM  `gc_contracts` AS cntracts
    LEFT JOIN (
        SELECT
            line_contract_no,
            contracts_id,
            bill_account_id,
            gc_organization_id_c,
            cust_billing_currency,
            customer_billing_method
        FROM (
            SELECT
                gc_lineitem.name as line_contract_no
                ,gc_line_item_contract_history.contracts_id as contracts_id
                ,gc_line_item_contract_history.bill_account_id as bill_account_id
                ,gc_line_item_contract_history.gc_organization_id_c as gc_organization_id_c
                ,gc_line_item_contract_history.cust_billing_currency as cust_billing_currency
                ,gc_line_item_contract_history.customer_billing_method as customer_billing_method
            FROM gc_line_item_contract_history
            LEFT JOIN gc_lineitem ON gc_line_item_contract_history.line_item_id = gc_lineitem.id
            WHERE
                gc_lineitem.deleted = 0 AND
                gc_line_item_contract_history.deleted = 0
            ORDER BY gc_lineitem.name
            ) as tbl_cust_line_item
        GROUP BY tbl_cust_line_item.contracts_id
        HAVING tbl_cust_line_item.line_contract_no IS NOT NULL
        ) tbl_line_item ON tbl_line_item.contracts_id = cntracts.id
    LEFT JOIN  `gc_salesrepresentative` AS sales ON sales.contracts_id = cntracts.id AND sales.deleted = 0
    LEFT JOIN  `contacts` AS bill_cnts ON bill_cnts.id = tbl_line_item.bill_account_id AND bill_cnts.deleted = 0
    LEFT JOIN  `contacts_cstm` AS bill_cnts_cust ON bill_cnts_cust.id_c = bill_cnts.id
    LEFT JOIN  `gc_organization` AS org ON org.id = tbl_line_item.gc_organization_id_c AND org.deleted = 0
    LEFT JOIN  `c_country` AS bill_cntry ON bill_cntry.id = bill_cnts_cust.c_country_id_c AND bill_cntry.deleted = 0
    LEFT JOIN  `accounts_contacts` AS bill_acnt_bill_cnts ON bill_acnt_bill_cnts.contact_id = bill_cnts.id AND bill_acnt_bill_cnts.deleted = 0
    LEFT JOIN  `accounts` AS bill_acnt ON bill_acnt.id = bill_acnt_bill_cnts.account_id AND bill_acnt.deleted = 0
    LEFT JOIN  `accounts_cstm` AS bill_acnt_cstm ON bill_acnt_cstm.id_c = bill_acnt.id
    LEFT JOIN  `email_addr_bean_rel` AS acnt_email ON acnt_email.bean_id = bill_cnts.id AND acnt_email.deleted = 0 AND acnt_email.primary_address = 1
    LEFT JOIN  `email_addresses` AS bill_acnt_email ON bill_acnt_email.id = acnt_email.email_address_id AND bill_acnt_email.deleted = 0

EOF;
        return;
    }

    /**
     * method returns sql query filter statements for billing account new records report
     *
     * @param string $query
     * @param array $filters
     *
     * @author sagar.salunkhe
     * @since Dec 30, 2016
     */
    private function getBillingAccountsGBSNewFilterQuery(&$query, $filters)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'query : ' . $query . GCM_GL_LOG_VAR_SEPARATOR . 'filters' . print_r($filters, true), 'method returns sql select columns and join table statements for billing account modified records report');
        $fltr_product_offering = "''";
        if (isset($filters['product_offering']) && count($filters['product_offering']) > 0) {
            $fltr_product_offering = implode(',', array_map('add_quotes', $filters['product_offering']));
        }

        $fltr_billing_affiliate_code = "''";
        if (isset($filters['billing_affiliate_code']) && count($filters['billing_affiliate_code']) > 0) {
            $fltr_billing_affiliate_code = implode(',', array_map('add_quotes', $filters['billing_affiliate_code']));
        }

        $query .= <<<EOF
WHERE
    (DATE_FORMAT(cntracts.date_entered,'%Y-%m-%d') BETWEEN '{$filters['date-after']}' AND '{$filters['date-before']}')
     AND cntracts.product_offering IN ({$fltr_product_offering})
     AND(
        (tbl_line_item.customer_billing_method = 'GBS_Billing')
        OR
        (
          (bill_cnts_cust.contact_type_c = 'Billing')
          AND
          (bill_cnts_cust.c_country_id_c <> '' AND bill_cnts_cust.c_country_id_c IS NOT NULL AND bill_cntry.country_code_numeric <> '392')
        )
    )
    AND (org.billing_affiliate_code NOT IN ({$fltr_billing_affiliate_code}) OR org.billing_affiliate_code IS NULL)
    ORDER BY cntracts.date_entered DESC
EOF;
        return;
    }

    /**
     * method returns sql query filter statements for billing account modified records report
     *
     * @param string $query
     * @param array $filters
     *
     * @author sagar.salunkhe
     * @since Dec 30, 2016
     */
    private function getBillingAccountsGBSModFilterQuery(&$query, $filters)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'query : ' . $query . GCM_GL_LOG_VAR_SEPARATOR . 'filters' . print_r($filters, true), 'method returns sql query filter statements for billing account modified records report');
        $fltr_product_offering = "''";
        if (isset($filters['product_offering']) && count($filters['product_offering']) > 0) {
            $fltr_product_offering = implode(',', array_map('add_quotes', $filters['product_offering']));
        }

        $fltr_billing_affiliate_code = "''";
        if (isset($filters['billing_affiliate_code']) && count($filters['billing_affiliate_code']) > 0) {
            $fltr_billing_affiliate_code = implode(',', array_map('add_quotes', $filters['billing_affiliate_code']));
        }

        $query .= <<<EOF
WHERE

    (
     (tbl_line_item.customer_billing_method = 'GBS_Billing')
      OR
      (
          (bill_cnts_cust.contact_type_c = 'Billing')
           AND
          (bill_cnts_cust.c_country_id_c <> '' AND bill_cnts_cust.c_country_id_c IS NOT NULL AND bill_cntry.country_code_numeric <> '392')
       )
    )
    AND (org.billing_affiliate_code NOT IN ({$fltr_billing_affiliate_code}) OR org.billing_affiliate_code IS NULL)
    AND cntracts.product_offering IN ({$fltr_product_offering})
    AND
    (
        (
            DATE_FORMAT(sales.date_entered,'%Y-%m-%d') < '{$filters['date-after']}' AND
            (DATE_FORMAT(sales.date_modified,'%Y-%m-%d') BETWEEN '{$filters['date-after']}' AND '{$filters['date-before']}')
        ) OR (
            DATE_FORMAT(bill_cnts.date_entered,'%Y-%m-%d') < '{$filters['date-after']}' AND
            (DATE_FORMAT(bill_cnts.date_modified,'%Y-%m-%d') BETWEEN '{$filters['date-after']}' AND '{$filters['date-before']}')
        )
    )
ORDER BY cntracts.date_entered DESC
EOF;
        return;
    }

    /**
     * method to build sql statement result of billing accounts report
     *
     * @param string $query
     * @param array $params
     * @param array $csv_data
     *
     * @author sagar.salunkhe
     * @since Dec 30, 2016
     */
    private function getBillingAccountsGBSQueryResult($query, $params, &$csv_data)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'query : ' . $query . GCM_GL_LOG_VAR_SEPARATOR . 'params' . print_r($params, true) . GCM_GL_LOG_VAR_SEPARATOR . 'csv_data : ' . print_r($csv_data, true), 'method to build sql statement result of billing accounts report');
        $query_results = $this->db->query($query, true);

        $this->arr_prdct_offrng_dtls = $params['product_offering_dtls'];

        while ($row = $this->db->fetchByAssoc($query_results)) {
            $csv_row = array();
            if (($row['billing_country'] == 'USA' || $row['billing_country'] == 'CAN')) {
                $row['billing_address_1'] = $row['billing_address_us_canada_1'];
                $row['billing_address_2'] = $row['billing_address_us_canada_2'];
            } else {
                $row['billing_address_1'] = $row['billing_address_any_1'];
                $row['billing_address_2'] = $row['billing_address_any_2'];
            }

            // if product offering dtls are available and key is present, assign bill cycle value else keep null
            if (isset($this->arr_prdct_offrng_dtls[$row['product_offering']]) && isset($this->arr_prdct_offrng_dtls[$row['product_offering']]['listofferingoption']['gbs_billing_cycle'])) {
                $row['bill_cycle'] = $this->arr_prdct_offrng_dtls[$row['product_offering']]['listofferingoption']['gbs_billing_cycle'];
            }

            // if this is modified result then set the bill_cycle as null
            if (isset($row['order_type']) && $row['order_type'] == '2') {
                $row['bill_cycle'] = null;
            }

            foreach ($this->arr_report_headers as $db_field => $csv_header) {
                $csv_row[] = $row[$db_field];
            }

            $csv_data[] = $csv_row;
        }
        return;
    }

    /**
     * method to build contracting accounts query
     *
     * @param array $filters
     * @return string contracting accounts sql query
     *
     * @author sagar.salunkhe
     * @since Nov 10, 2016
     */
    private function getContractAccountsGBSNewSelectQuery(&$query)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'query : ' . $query, 'method to build contracting accounts query');
        $query .= <<<EOF
SELECT
    cntract_acnt_cstm.common_customer_id_c
    ,1 as order_type
    ,IF(cntract_acnt.name IS NULL OR cntract_acnt.name = '', cntract_acnt_cstm.name_2_c, cntract_acnt.name) AS contract_account_name
    ,cntract_cntry.country_code_alphabet AS corporate_country
    ,IF(cntract_cntry.country_code_numeric ='840' OR cntract_cntry.country_code_numeric ='124',
    concat(cntract_cnts_cust.addr_general_c,' ',cntract_cnts_cust.city_general_c,' ',cntract_state.name,' ',cntract_cnts_cust.postal_no_c),
    IF(cntract_cnts_cust.addr_2_c IS NULL OR cntract_cnts_cust.addr_2_c = '', cntract_cnts_cust.addr_1_c, cntract_cnts_cust.addr_2_c)) as corporate_address
    ,cntracts.global_contract_id
    ,cntracts.vat_no AS contract_vat_no

FROM  `gc_contracts` AS cntracts
    LEFT JOIN (
        SELECT
            line_contract_no,
            contracts_id,
            bill_account_id,
            gc_organization_id_c,
            cust_billing_currency,
            customer_billing_method
        FROM (
            SELECT
                gc_lineitem.name as line_contract_no
                ,gc_line_item_contract_history.contracts_id as contracts_id
                ,gc_line_item_contract_history.bill_account_id as bill_account_id
                ,gc_line_item_contract_history.gc_organization_id_c as gc_organization_id_c
                ,gc_line_item_contract_history.cust_billing_currency as cust_billing_currency
                ,gc_line_item_contract_history.customer_billing_method as customer_billing_method
            FROM gc_line_item_contract_history
            LEFT JOIN gc_lineitem ON gc_line_item_contract_history.line_item_id = gc_lineitem.id
            WHERE
                gc_lineitem.deleted = 0 AND
                gc_line_item_contract_history.deleted = 0
            ORDER BY gc_lineitem.name
            ) as tbl_cust_line_item
        GROUP BY tbl_cust_line_item.contracts_id
        HAVING tbl_cust_line_item.line_contract_no IS NOT NULL
        ) tbl_line_item ON tbl_line_item.contracts_id = cntracts.id

    LEFT JOIN  `gc_organization` AS org ON org.id = tbl_line_item.gc_organization_id_c AND org.deleted = 0

    LEFT JOIN  `contacts` AS bill_cnts ON bill_cnts.id = tbl_line_item.bill_account_id AND bill_cnts.deleted = 0
    LEFT JOIN  `contacts_cstm` AS bill_cnts_cust ON bill_cnts_cust.id_c = bill_cnts.id
    LEFT JOIN  `c_country` AS bill_cntry ON bill_cntry.id = bill_cnts_cust.c_country_id_c AND bill_cntry.deleted = 0

    LEFT JOIN  `contacts_gc_contracts_1_c` cntract_cnts_cntracts ON cntract_cnts_cntracts.contacts_gc_contracts_1gc_contracts_idb = cntracts.id and cntract_cnts_cntracts.deleted = 0
    LEFT JOIN  `contacts` cntract_cnts ON cntract_cnts.id = cntract_cnts_cntracts.contacts_gc_contracts_1contacts_ida and cntract_cnts.deleted = 0
    LEFT JOIN  `contacts_cstm` cntract_cnts_cust ON cntract_cnts_cust.id_c = cntract_cnts.id
    LEFT JOIN  `accounts_gc_contracts_1_c` cntract_acnt_cntracts ON cntract_acnt_cntracts.accounts_gc_contracts_1gc_contracts_idb = cntracts.id AND cntract_acnt_cntracts.deleted = 0
    LEFT JOIN  `accounts` cntract_acnt ON cntract_acnt.id = cntract_acnt_cntracts.accounts_gc_contracts_1accounts_ida AND cntract_acnt.deleted = 0
    LEFT JOIN  `accounts_cstm` cntract_acnt_cstm ON cntract_acnt_cstm.id_c = cntract_acnt.id
    LEFT JOIN  `c_country` cntract_cntry ON cntract_cntry.id = cntract_cnts_cust.c_country_id_c and cntract_cntry.deleted = 0
    LEFT JOIN  `c_state` cntract_state ON cntract_state.id = cntract_cnts_cust.c_state_id_c and cntract_state.deleted = 0

EOF;
        return;
    }

    /**
     * method returns sql select columns and join table statements for contract account modified records report
     *
     * @param string $query
     *
     * @author sagar.salunkhe
     * @since Dec 30, 2016
     */
    private function getContractAccountsGBSModSelectQuery(&$query)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'query : ' . $query, 'method returns sql select columns and join table statements for contract account modified records report');
        $query .= <<<EOF
SELECT
    cntract_acnt_cstm.common_customer_id_c
    ,2 as order_type
    ,IF(cntract_acnt.name IS NULL OR cntract_acnt.name = '', cntract_acnt_cstm.name_2_c, cntract_acnt.name) AS contract_account_name
    ,NULL AS corporate_country
    ,IF(cntract_cntry.country_code_numeric ='840' OR cntract_cntry.country_code_numeric ='124',
    concat(cntract_cnts_cust.addr_general_c,' ',cntract_cnts_cust.city_general_c,' ',cntract_state.name,' ',cntract_cnts_cust.postal_no_c),
    IF(cntract_cnts_cust.addr_2_c IS NULL OR cntract_cnts_cust.addr_2_c = '', cntract_cnts_cust.addr_1_c, cntract_cnts_cust.addr_2_c)) as corporate_address
    ,cntracts.global_contract_id
    ,cntracts.vat_no AS contract_vat_no

FROM  `gc_contracts` AS cntracts
    LEFT JOIN (
        SELECT
            line_contract_no,
            contracts_id,
            bill_account_id,
            gc_organization_id_c,
            cust_billing_currency,
            customer_billing_method
        FROM (
            SELECT
                gc_lineitem.name as line_contract_no
                ,gc_line_item_contract_history.contracts_id as contracts_id
                ,gc_line_item_contract_history.bill_account_id as bill_account_id
                ,gc_line_item_contract_history.gc_organization_id_c as gc_organization_id_c
                ,gc_line_item_contract_history.cust_billing_currency as cust_billing_currency
                ,gc_line_item_contract_history.customer_billing_method AS customer_billing_method
            FROM gc_line_item_contract_history
            LEFT JOIN gc_lineitem ON gc_line_item_contract_history.line_item_id = gc_lineitem.id
            WHERE
                gc_lineitem.deleted = 0 AND
                gc_line_item_contract_history.deleted = 0
            ORDER BY gc_lineitem.name
            ) as tbl_cust_line_item
        GROUP BY tbl_cust_line_item.contracts_id
        HAVING tbl_cust_line_item.line_contract_no IS NOT NULL
        ) tbl_line_item ON tbl_line_item.contracts_id = cntracts.id
    LEFT JOIN  `gc_organization` AS org ON org.id = tbl_line_item.gc_organization_id_c AND org.deleted = 0

    LEFT JOIN  `contacts` AS bill_cnts ON bill_cnts.id = tbl_line_item.bill_account_id AND bill_cnts.deleted = 0
    LEFT JOIN  `contacts_cstm` AS bill_cnts_cust ON bill_cnts_cust.id_c = bill_cnts.id
    LEFT JOIN  `c_country` AS bill_cntry ON bill_cntry.id = bill_cnts_cust.c_country_id_c AND bill_cntry.deleted = 0

    LEFT JOIN  `contacts_gc_contracts_1_c` cntract_cnts_cntracts ON cntract_cnts_cntracts.contacts_gc_contracts_1gc_contracts_idb = cntracts.id and cntract_cnts_cntracts.deleted = 0
    LEFT JOIN  `contacts` cntract_cnts ON cntract_cnts.id = cntract_cnts_cntracts.contacts_gc_contracts_1contacts_ida and cntract_cnts.deleted = 0
    LEFT JOIN  `contacts_cstm` cntract_cnts_cust ON cntract_cnts_cust.id_c = cntract_cnts.id
    LEFT JOIN  `accounts_gc_contracts_1_c` cntract_acnt_cntracts ON cntract_acnt_cntracts.accounts_gc_contracts_1gc_contracts_idb = cntracts.id AND cntract_acnt_cntracts.deleted = 0
    LEFT JOIN  `accounts` cntract_acnt ON cntract_acnt.id = cntract_acnt_cntracts.accounts_gc_contracts_1accounts_ida AND cntract_acnt.deleted = 0
    LEFT JOIN  `accounts_cstm` cntract_acnt_cstm ON cntract_acnt_cstm.id_c = cntract_acnt.id
    LEFT JOIN  `c_country` cntract_cntry ON cntract_cntry.id = cntract_cnts_cust.c_country_id_c and cntract_cntry.deleted = 0
    LEFT JOIN  `c_state` cntract_state ON cntract_state.id = cntract_cnts_cust.c_state_id_c and cntract_state.deleted = 0

EOF;
        return;
    }

    /**
     * method returns sql query filter statements for contract account new records report
     *
     * @param string $query
     * @param array $filters
     *
     * @author sagar.salunkhe
     * @since Dec 30, 2016
     */
    private function getContractAccountsGBSNewFilterQuery(&$query, $filters)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'query : ' . $query . GCM_GL_LOG_VAR_SEPARATOR . 'filters' . print_r($filters, true), 'method returns sql query filter statements for contract account new records report');
        $fltr_product_offering = "''";
        if (isset($filters['product_offering']) && count($filters['product_offering']) > 0) {
            $fltr_product_offering = implode(', ', array_map('add_quotes', $filters['product_offering']));
        }

        $fltr_billing_affiliate_code = "''";
        if (isset($filters['billing_affiliate_code']) && count($filters['billing_affiliate_code']) > 0) {
            $fltr_billing_affiliate_code = implode(',', array_map('add_quotes', $filters['billing_affiliate_code']));
        }

        $query .= <<<EOF
WHERE
    (DATE_FORMAT(cntracts.date_entered,'%Y-%m-%d') BETWEEN '{$filters['date-after']}' AND '{$filters['date-before']}')
     AND cntracts.product_offering IN ({$fltr_product_offering})
     AND(
        (tbl_line_item.customer_billing_method = 'GBS_Billing')
        OR
        (
          (bill_cnts_cust.contact_type_c = 'Billing')
          AND
          (bill_cnts_cust.c_country_id_c <> '' AND bill_cnts_cust.c_country_id_c IS NOT NULL AND bill_cntry.country_code_numeric <> '392')
        )
    )
    AND (org.billing_affiliate_code NOT IN ({$fltr_billing_affiliate_code}) OR org.billing_affiliate_code IS NULL)
    ORDER BY cntracts.date_entered DESC
EOF;
        return;
    }

    /**
     * method returns sql query filter statements for contract account modified records report
     *
     * @param string $query
     * @param array $filters
     *
     * @author sagar.salunkhe
     * @since Dec 30, 2016
     */
    private function getContractAccountsGBSModFilterQuery(&$query, $filters)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'query : ' . $query . GCM_GL_LOG_VAR_SEPARATOR . 'filters' . print_r($filters, true), 'method returns sql query filter statements for contract account modified records report');
        $fltr_product_offering = "''";
        if (isset($filters['product_offering']) && count($filters['product_offering']) > 0) {
            $fltr_product_offering = implode(', ', array_map('add_quotes', $filters['product_offering']));
        }

        $fltr_billing_affiliate_code = "''";
        if (isset($filters['billing_affiliate_code']) && count($filters['billing_affiliate_code']) > 0) {
            $fltr_billing_affiliate_code = implode(',', array_map('add_quotes', $filters['billing_affiliate_code']));
        }

        $query .= <<<EOF
WHERE
    (
     (tbl_line_item.customer_billing_method = 'GBS_Billing')
      OR
      (
          (bill_cnts_cust.contact_type_c = 'Billing')
           AND
          (bill_cnts_cust.c_country_id_c <> '' AND bill_cnts_cust.c_country_id_c IS NOT NULL AND bill_cntry.country_code_numeric <> '392')
       )
    )
    AND (org.billing_affiliate_code NOT IN ({$fltr_billing_affiliate_code}) OR org.billing_affiliate_code IS NULL)
    AND cntracts.product_offering IN ({$fltr_product_offering})
    AND
    (
        (
            DATE_FORMAT(cntracts.date_entered,'%Y-%m-%d') < '{$filters['date-after']}' AND
            (DATE_FORMAT(cntracts.date_modified,'%Y-%m-%d') BETWEEN '{$filters['date-after']}' AND '{$filters['date-before']}')
        ) OR (
            DATE_FORMAT(cntract_acnt.date_entered,'%Y-%m-%d') < '{$filters['date-after']}' AND
            (DATE_FORMAT(cntract_acnt.date_modified,'%Y-%m-%d') BETWEEN '{$filters['date-after']}' AND '{$filters['date-before']}')
        ) OR (
            cntract_cnts_cust.contact_type_c = 'Contract' AND
            DATE_FORMAT(cntract_cnts.date_entered,'%Y-%m-%d') < '{$filters['date-after']}' AND
            (DATE_FORMAT(cntract_cnts.date_modified,'%Y-%m-%d') BETWEEN '{$filters['date-after']}' AND '{$filters['date-before']}')
        )
    )
ORDER BY cntracts.date_entered DESC
EOF;
        return;
    }

    /**
     * method to build sql statement result of contract accounts report
     *
     * @param string $query
     * @param array $params
     * @param array $csv_data
     *
     * @author sagar.salunkhe
     * @since Dec 30, 2016
     */
    private function getContractAccountsGBSQueryResult($query, $params, &$csv_data)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'query : ' . $query . GCM_GL_LOG_VAR_SEPARATOR . 'params' . print_r($params, true) . GCM_GL_LOG_VAR_SEPARATOR . 'csv_data : ' . print_r($csv_data, true), 'method to build sql statement result of contract accounts report');
        $query_results = $this->db->query($query, true);
        while ($row = $this->db->fetchByAssoc($query_results)) {
            $csv_row = array();
            foreach ($this->arr_report_headers as $db_field => $csv_header) {
                $csv_row[] = $row[$db_field];
            }
            $csv_data[] = $csv_row;
        }
        return;
    }

    /**
     * method to return headers of respective export key
     *
     * @param string $export_key
     * @return array respective headers from export key
     *
     * @author sagar.salunkhe
     * @since Nov 10, 2016
     */
    private function getExportReportHeaders($export_key)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'export_key : ' . $export_key, 'method to return headers of respective export key');
        $arr_export_headers = self::exportHeadersMaster();
        return $arr_export_headers[$export_key];
    }

    /**
     * method to return db field and csv headers mapping
     *
     * @return array array will contain db field and csv headers mapping
     * @author sagar.salunkhe
     * @since Nov 10, 2016
     */
    private function exportHeadersMaster()
    {
        $arr_export_headers = array(
                                    'export-billing-accounts-for-gbs' => array(
                                                                            'global_contract_id' => 'Account IF ID',
                                                                            'order_type' => 'Order Type',
                                                                            'data_type' => 'Data Type',
                                                                            'billing_affiliate' => 'Billing Affiliate',
                                                                            'affiliate' => 'Affiliate',
                                                                            'service_type' => 'Service Type',
                                                                            'billing_account_name' => 'Billing Account Name',
                                                                            'billing_country' => 'Billing Country',
                                                                            'billing_address_1' => 'Billing Address 1',
                                                                            'billing_address_2' => 'Billing Address 2',
                                                                            'billing_address_3' => 'Billing Address 3',
                                                                            'attention' => 'Attention',
                                                                            'billing_cid' => 'Billing CID',
                                                                            'account_manager' => 'Account Manager',
                                                                            'contact_name' => 'Contact Name',
                                                                            'phone_number' => 'Phone Number',
                                                                            'email_address' => 'Email Address',
                                                                            'bill_cycle' => 'Bill Cycle',
                                                                            'billing_currency' => 'Billing Currency'
                                    ),
                                    'export-contract-accounts-for-gbs' => array(
                                                                                'global_contract_id' => 'Contract ID',
                                                                                'order_type' => 'Order Type',
                                                                                'contract_account_name' => 'Contract Account Name',
                                                                                'common_customer_id_c' => 'CID',
                                                                                'corporate_country' => 'Country',
                                                                                'corporate_address' => 'Address',
                                                                                'contract_vat_no' => 'VAT No'
                                    )
        );
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_export_headers : ' . print_r($arr_export_headers, true), 'method to return db field and csv headers mapping');
        return $arr_export_headers;
    }
}
