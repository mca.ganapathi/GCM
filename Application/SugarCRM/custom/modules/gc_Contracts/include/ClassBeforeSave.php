<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once 'custom/modules/gc_Contracts/include/ClassGcContracts.php';

class ClassGcContractBeforeSave extends ClassGcContracts
{

    /**
     * method to set before save params and general logic
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author Sagar.Salunkhe
     * @since 13-Jan-2016
     */
    public function setGeneralParams($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'event: ' . print_r($event, true) . GCM_GL_LOG_VAR_SEPARATOR . 'arguments: ' . print_r($arguments, true), 'Method to set before save params and general logic');
        global $sugar_config;
        $modUtils = new ModUtils();
        // If assigned to user is empty then assigning the record to the user created by
        if (empty($bean->assigned_user_id)) {
            $bean->assigned_user_id = $bean->created_by;
        }

        $is_sugar_request = false;
        
        if(isset($_POST['sugar_request']) && $_POST['sugar_request'])
            $is_sugar_request = true;
        else
            return;

        /**
         * * START : DF-1441 Convert Local date to GMT before saving **
         */

        $bean->previous_version_id = isset($_POST['parent_contract_id']) ? $_POST['parent_contract_id'] : '';

        // contract start date custom field save
        if (isset($_POST['contract_startdate_hidden']) && !empty($_POST['contract_startdate_hidden'])) {
            $bean->contract_startdate = $modUtils->getDbDate($_POST['contract_startdate_hidden']);
        }

        if (isset($_POST['contract_enddate_hidden']) && !empty($_POST['contract_enddate_hidden'])) {
            $bean->contract_enddate = $modUtils->getDbDate($_POST['contract_enddate_hidden']);
        }

        // Contract Start Date
        if (!empty($bean->contract_startdate) && empty($bean->gc_timezone_id_c) && $is_sugar_request) {
            $bean->gc_timezone_id_c = $sugar_config['cstm_default_timezone'][0];
        }
        if (!empty($bean->gc_timezone_id_c) && !empty($bean->contract_startdate)) {

            $obj_timezone = BeanFactory::getBean('gc_TimeZone');
            $obj_timezone->retrieve($bean->gc_timezone_id_c);

            if (!empty($obj_timezone->utcoffset)) {
                $input_startdate = str_replace(' ', 'T', $bean->contract_startdate);
                $input_startdate = $input_startdate . $obj_timezone->utcoffset;
                $arr_res = $modUtils->ConvertDatetoGMTDate($input_startdate);

                if ($arr_res['result'] && !empty($arr_res['date'])){
                    $bean->contract_startdate_local = $bean->contract_startdate;
                    $bean->contract_startdate = $arr_res['date'];
                }
            }
            unset($obj_timezone, $input_startdate, $arr_res);
        }

        if (!empty($bean->fetched_row['gc_timezone_id_c']) && !empty($bean->fetched_row['contract_startdate'])) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone');
            $obj_timezone->retrieve($bean->fetched_row['gc_timezone_id_c']);

            if (!empty($obj_timezone->utcoffset)) {
                $input_startdate = str_replace(' ', 'T', $bean->fetched_row['contract_startdate']);
                $input_startdate = $input_startdate . $obj_timezone->utcoffset;
                $arr_res = $modUtils->ConvertDatetoGMTDate($input_startdate);

                if ($arr_res['result'] && !empty($arr_res['date'])){
                    $bean->fetched_row['contract_startdate_local'] = $bean->fetched_row['contract_startdate'];
                    $bean->fetched_row['contract_startdate'] = $arr_res['date'];
                }
            }
            unset($obj_timezone, $input_startdate, $arr_res);
        }

        // Contract End Date
        if (!empty($bean->contract_enddate) && empty($bean->gc_timezone_id1_c) && $is_sugar_request) {
            $bean->gc_timezone_id1_c = $sugar_config['cstm_default_timezone'][0];
        }
        if (!empty($bean->gc_timezone_id1_c) && !empty($bean->contract_enddate)) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone');
            $obj_timezone->retrieve($bean->gc_timezone_id1_c);

            if (!empty($obj_timezone->utcoffset)) {
                $input_startdate = str_replace(' ', 'T', $bean->contract_enddate);
                $input_startdate = $input_startdate . $obj_timezone->utcoffset;
                $arr_res = $modUtils->ConvertDatetoGMTDate($input_startdate);

                if ($arr_res['result'] && !empty($arr_res['date'])){
                    $bean->contract_enddate_local = $bean->contract_enddate;
                    $bean->contract_enddate = $arr_res['date'];
                }
                    
            }
            unset($obj_timezone, $input_startdate, $arr_res);
        }
        
        if (!empty($bean->fetched_row['gc_timezone_id1_c']) && !empty($bean->fetched_row['contract_enddate'])) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone');
            $obj_timezone->retrieve($bean->fetched_row['gc_timezone_id1_c']);

            if (!empty($obj_timezone->utcoffset)) {
                $input_startdate = str_replace(' ', 'T', $bean->fetched_row['contract_enddate']);
                $input_startdate = $input_startdate . $obj_timezone->utcoffset;
                $arr_res = $modUtils->ConvertDatetoGMTDate($input_startdate);

                if ($arr_res['result'] && !empty($arr_res['date'])){
                    $bean->fetched_row['contract_enddate_local'] = $bean->fetched_row['contract_enddate'];
                    $bean->fetched_row['contract_enddate'] = $arr_res['date'];
                }
                    
            }
            unset($obj_timezone, $input_startdate, $arr_res);
        }
       
        
        /**
         * * END : DF-1441 Convert Local date to GMT before saving **
         */
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');

    }

    /**
     * method to set incremental value to Global Contract ID
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author Sagar.Salunkhe
     * @since 10-Oct-2015
     */
    public function setGlobalContractId($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'event: ' . print_r($event, true) . GCM_GL_LOG_VAR_SEPARATOR . 'arguments: ' . print_r($arguments, true), 'Method to set incremental value to Global Contract ID');
        if (!empty($bean->global_contract_id)) {
            return;
        }
        // return if Global Contract Id is already present

        // DF-155 keep global contract id same as previous version, in case of version upgrade
        if (!empty($_POST['parent_contract_id'])) {
            $obj_contracts_bean = BeanFactory::getBean('gc_Contracts');
            $obj_contracts_bean->retrieve($_POST['parent_contract_id']);

            $bean->global_contract_id = (!empty($obj_contracts_bean->global_contract_id)) ? $obj_contracts_bean->global_contract_id : $this->generateGlobalContractId();
        } else {
            $bean->global_contract_id = $this->generateGlobalContractId();
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * Method to check availability of creating new version
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author Debasish.Gupta
     * @since 18-July-2016
     */
    public function checkContractModificationAvailability($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'event: ' . print_r($event, true) . GCM_GL_LOG_VAR_SEPARATOR . 'arguments: ' . print_r($arguments, true), 'Method to check availability of creating new version');
        global $mod_strings;
        if (!empty($_POST['parent_contract_id']) && $_REQUEST['version_upgrade']) {
            $contract_id = !empty($_POST['parent_contract_id']) ? $_POST['parent_contract_id'] : '';
            $obj_contracts_bean = BeanFactory::getBean('gc_Contracts');
            $obj_contracts_bean->retrieve($contract_id);

            $response = $this->checkContractVersionUpgrade($obj_contracts_bean);
            if ($response) {
                $params = array('module' => 'gc_Contracts', 'action' => 'DetailView', 'record' => $contract_id);
                $err_msg = isset($mod_strings['LBL_CONTRACT_VERSION_ALREADY_CREATED']) ? $mod_strings['LBL_CONTRACT_VERSION_ALREADY_CREATED'] : '';
                SugarApplication::appendErrorMessage($err_msg);
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
                SugarApplication::redirect('index.php?' . http_build_query($params));
                exit();
            } else if (isset($obj_contracts_bean->contract_status) && $obj_contracts_bean->contract_status != "4") {
                $params = array('module' => 'gc_Contracts', 'action' => 'DetailView', 'record' => $contract_id);
                $err_msg = isset($mod_strings['LBL_INVALID_STAGE']) ? $mod_strings['LBL_INVALID_STAGE'] : '';
                SugarApplication::appendErrorMessage($err_msg);
                SugarApplication::redirect('index.php?' . http_build_query($params));
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
                exit();
            }
        }
    }

    /**
     * Method to set Lock Version DF-675 - Lock version control on GUI
     *
     * @param object $bean record sugar object [Sugar default]
     * @author Amol.Sananse
     * @since 02-Aug-2016
     */
    public function setLockVersion($bean)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to set Lock Version DF-675 - Lock version control on GUI');
        global $mod_strings;

        if (isset($_POST['action']) && $_POST['action'] == 'Save') {
            if (!empty($bean) && $bean->lock_version > 0 && $_POST['version_upgrade'] == '') {
                $_SESSION['current_lock_version'] = (empty($bean->fetched_row['id']) ? 1 : $_SESSION['current_lock_version']);
                if (!empty($_SESSION['current_lock_version']) && ((int) $_SESSION['current_lock_version'] == $bean->lock_version)) {
                    if (empty($bean->fetched_row['id'])) {
                        $bean->lock_version = 1; // lock version set for new contract
                    } else {
                        $bean->lock_version = ($bean->lock_version > 0 ? ($_SESSION['current_lock_version'] + 1) : 1); // lock version set for existing contract
                    }

                    $_SESSION['current_lock_version'] = 0;
                } else {
                    ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'current_lock_version: ' . print_r($_SESSION['current_lock_version'], true), 'To check current lock version');
                    $err_msg = isset($mod_strings['LBL_DUPLICATED_VERSION_UP']) ? $mod_strings['LBL_DUPLICATED_VERSION_UP'] : '';
                    SugarApplication::appendErrorMessage($err_msg);
                    SugarApplication::redirect('index.php?module=gc_Contracts&action=DetailView&record=' . $bean->id);
                    exit();
                }
            } else {
                $bean->lock_version = 1; // lock version set for new contract
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }
    
    public function setContractHeaderAuditEntry($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        // Avoid audit log for new record.
        if (empty($bean->fetched_row['id'])) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
            return;
        }

        global $sugar_config;
        $modUtils      = new ModUtils();
        $timezone_bean = BeanFactory::getBean('gc_TimeZone');

        // Convert contract start date
        $contract_startdate = '';
        if (!empty($bean->contract_startdate) && !empty($bean->gc_timezone_id_c)) {
            $timezone_list               = $timezone_bean->retrieve($bean->gc_timezone_id_c);
            $contract_startdate_timezone = !empty($timezone_list->utcoffset) ? $timezone_list->utcoffset : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $contract_startdate          = $modUtils->ConvertGMTDateToLocalDate($bean->contract_startdate, $contract_startdate_timezone);
            $contract_startdate          = $contract_startdate['datetime'];
        }
        // Convert contract end date
        $contract_enddate = '';
        if (!empty($bean->contract_enddate) && !empty($bean->gc_timezone_id1_c)) {
            $timezone_list             = $timezone_bean->retrieve($bean->gc_timezone_id1_c);
            $contract_enddate_timezone = !empty($timezone_list->utcoffset) ? $timezone_list->utcoffset : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $contract_enddate          = $modUtils->ConvertGMTDateToLocalDate($bean->contract_enddate, $contract_enddate_timezone);
            $contract_enddate          = $contract_enddate['datetime'];
        }
        // Audit contract start date
        if ($bean->contract_startdate_local != $bean->fetched_row['contract_startdate_local']) {
            ModUtils::saveAuditRecordEntry($bean, 'contract_startdate', 'text', $bean->fetched_row['contract_startdate_local'], $bean->contract_startdate_local);
        }
        // Audit contract end date
        if ($bean->contract_enddate_local != $bean->fetched_row['contract_enddate_local']) {
            ModUtils::saveAuditRecordEntry($bean, 'contract_enddate', 'text', $bean->fetched_row['contract_enddate_local'], $bean->contract_enddate_local);
        }
        // Audit contract start date timezone
        if ($bean->gc_timezone_id_c != $bean->fetched_row['gc_timezone_id_c']) {
            // before save value for contract start date timezone
            $contract_startdate_timezone_before = '';
            if (!empty($bean->gc_timezone_id_c)) {
                $obj_timezone                       = BeanFactory::getBean('gc_TimeZone', $bean->gc_timezone_id_c);
                $contract_startdate_timezone_before = $obj_timezone->name . ' (' . $obj_timezone->utcoffset . ') ';
            }
            // after save value for contract start date timezone
            $contract_startdate_timezone_after = '';
            if (!empty($bean->fetched_row['gc_timezone_id_c'])) {
                $obj_timezone                      = BeanFactory::getBean('gc_TimeZone', $bean->fetched_row['gc_timezone_id_c']);
                $contract_startdate_timezone_after = $obj_timezone->name . ' (' . $obj_timezone->utcoffset . ') ';
            }
            // adding audti log
            ModUtils::saveAuditRecordEntry($bean, 'contract_startdate_timezone', 'relate', $contract_startdate_timezone_after, $contract_startdate_timezone_before);
        }
        // Audit contract end date timezone
        if ($bean->gc_timezone_id1_c != $bean->fetched_row['gc_timezone_id1_c']) {
            // before save value for contract end date timezone
            $contract_enddate_timezone_before = '';
            if (!empty($bean->gc_timezone_id1_c)) {
                $obj_timezone                     = BeanFactory::getBean('gc_TimeZone', $bean->gc_timezone_id1_c);
                $contract_enddate_timezone_before = $obj_timezone->name . ' (' . $obj_timezone->utcoffset . ') ';
            }
            // after save value for contract end date timezone
            $contract_enddate_timezone_after = '';
            if (!empty($bean->fetched_row['gc_timezone_id1_c'])) {
                $obj_timezone                    = BeanFactory::getBean('gc_TimeZone', $bean->fetched_row['gc_timezone_id1_c']);
                $contract_enddate_timezone_after = $obj_timezone->name . ' (' . $obj_timezone->utcoffset . ') ';
            }
            // adding audti log
            ModUtils::saveAuditRecordEntry($bean, 'contract_enddate_timezone', 'relate', $contract_enddate_timezone_after, $contract_enddate_timezone_before);
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }
}
