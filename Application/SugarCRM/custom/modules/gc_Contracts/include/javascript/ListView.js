  /*
   * Function to check PMS is working
   * 
   * @author : Kamal.Thiayagarajan
   */
  $( document ).ready(function() {
      $.ajax({
          type: "POST",
          url: "index.php?module=gc_Contracts",
          dataType: "json",
          async: true,
          data: {
              "method": "getOfferings",
              'sugar_body_only': 1,
              'action': 'Actions'
          },
          success: function(response) {
          	if (response.respose_xml == "") {
          		//alert(SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_PMS_ERROR'));
          		YAHOO.SUGAR.MessageBox.show({
          			close: false,
                      width: 300,
                      msg: SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_PMS_ERROR'),
                      title: SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_PMS_ERROR_TITLE'),
                      type: 'alert',
                      buttons:[
                               { text: 'OK', handler: handleOK ,isDefault:true},
                      ]
                  });
          		
          	} 
             
          }
      });
  });

  var handleOK = function() {
      this.hide();
      window.location.href = 'index.php';
  	  return false;
  };

