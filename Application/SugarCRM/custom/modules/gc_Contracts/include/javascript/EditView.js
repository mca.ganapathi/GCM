/**
 * @file   : custom/modules/gc_Contracts/include/javascript/EditView.js
 * @author : Dinesh.Itkar & Arunsakthivel.S
 * @date   : 25 Nov 2015
 * @desc   : All EditView file related JavaScript methods except product popilation using JS.
 * @Modified : 15 May 2016
 */
RecordId = $('input[name="record"]').val();
var invoice_groups = {};
var invoice_groups_nxtelem = 1;
var invoice_group_dialog;
var from_contacts_popup_return = false;
var one_stop_billing_reference_row = 1;
var EmailAddressMinLength = 3;
var GlobalMaxPostVar = 25000;
var version_upgrade;
var is_versionedup_contract;
window.GlobalMaxPostVar = 25000;
var description_max_length = 1024;
/*
 * @desc : method to add list of fields for validation
 * @author : Dinesh.Itkar
 * @params : NULL
 * @return : (void method)
 */
function addToValidateList() {
    return;
}
/**
 * @param : Function will return the no.of line item present in the current contract create / edit screen
 * @return : < Integer> No.of line item
 * @author : Arunsakthivel.s
 * @date : 19-May-2016
 */
function getLineItemsRowLenth() {
    return parseInt(parentNode.find('.LineItem').length);
    //$("[id^='container_lineitem_']" ).length; <- This is the good one we have to check and change
}
/*
 * @desc : method to perform javascript validations before submitting/saving the form
 * @author : Dinesh.Itkar
 * @params : NULL
 * @return : (void method)
 */
function CustomValidateSave() {
    if (salesRepValidation() == false) return false;
    return true;
}

function markFieldsReadOnly(element_id) {
    node_type = $('#' + element_id).prop('nodeName');
    if (node_type == 'INPUT') {
        $('#' + element_id).prop('readonly', true);
    } else if (node_type == 'SELECT') {
        $('#' + element_id + ' option').filter(":selected").siblings('option').prop('disabled', true);
    }
}
/*
 * @desc : method to perform javascript validations before submitting/saving the
 * form for Sales Representation fields
 * @author : Dinesh.Itkar
 * @params : NULL
 * @return : (boolean)
 */
function salesRepValidation() {
    return checkValidEmail($('#email1'));
}

function checkValidEmail(e) 
{
	var email_address = e.val();
	if (email_address != "") {
        if(email_address.trim().length < EmailAddressMinLength)
        {
            e.after('<div class="error">Invalid Value: Email address</div>');
            e.css('backgroundColor', "#FF0000");
            e.animate({backgroundColor: ''}, 2000);
            return false;
        }
    }
	return true;
}

function prodConfigValidation(){
	var return_val = true;	
	var temp_ret_val = true;
	var id = '';
	$('[name^="pc_characteristic_value_"]').each(function() {
		if($(this).data('valtypeid') == 2){
			temp_ret_val = product_config_validate(this);
			if(temp_ret_val === false && return_val === true){
				return_val = temp_ret_val;
				id = this.id;
			}
		}
    });
	/*
    if(id!=''){
	    $('html, body').animate({
	        scrollTop: $("#"+id).offset().top
	    }, 400);
	}
    */
    if (return_val == true) return 0;
    return 1;
}

function customContractHeaderFields() {
    contract_startdate_hidden = $('#contract_startdate_hidden');
    div_contract_startdate = $('#div_contract_startdate');
    if (contract_startdate_hidden.val() != '' && contract_startdate_hidden.val() != undefined) {
        var dt = contract_startdate_hidden.val().split(" ");
        div_contract_startdate.find('.date_input').val(dt[0]);
        var time = dt[1].split(":");
        div_contract_startdate.find('.datetimecombo_hr').val(time[0]);
        div_contract_startdate.find('.datetimecombo_min').val(time[1]);
        div_contract_startdate.find('.datetimecombo_sec').val(time[2]);
    }
    contract_enddate_hidden = $('#contract_enddate_hidden');
    div_contract_enddate = $('#div_contract_enddate');
    if (contract_enddate_hidden.val() != '' && contract_enddate_hidden.val() != undefined) {
        var dt = contract_enddate_hidden.val().split(" ");
        div_contract_enddate.find('.date_input').val(dt[0]);
        var time = dt[1].split(":");
        div_contract_enddate.find('.datetimecombo_hr').val(time[0]);
        div_contract_enddate.find('.datetimecombo_min').val(time[1]);
        div_contract_enddate.find('.datetimecombo_sec').val(time[2]);
    }
    //customize contracting customer account popup select button
    document.getElementById('btn_contacts_gc_contracts_1_name').onclick = function() {
        open_popup("Contacts", 600, 400, "", true, false, {
            "call_back_function": "set_contacts_popup_return",
            "form_name": "EditView",
            "field_to_name_array": {
                "id": "contacts_gc_contracts_1contacts_ida",
                "name": "contacts_gc_contracts_1_name",
                "c_country_id_c": "contacts_country_id_c"
            }
        }, "single", true);
    };
    //customize contracting customer account popup clear button
    document.getElementById('btn_clr_contacts_gc_contracts_1_name').onclick = function() {
        SUGAR.clearRelateField(this.form, 'contacts_gc_contracts_1_name', 'contacts_gc_contracts_1contacts_ida', 'contacts_country_id_c');
        $('#contacts_country_id_c').val(''); //clear contacts country hidden field
        populateTimeZoneDropdownList('gc_timezone_id_c', '', 0);
        populateTimeZoneDropdownList('gc_timezone_id1_c', '', 0);
    };
}

function contractDateTimezoneValidation()
{
	var end_full_date = "";
    var start_full_date = "";
    var start_time_zone = "";
    var end_time_zone = "";
    var parent_startdate = $('#div_contract_startdate').closest('td');
   
    var date_fld_startdate = parent_startdate.find('.date_input');
    removeFromValidate('EditView', 'contract_startdate');
    removeFromValidate('EditView', 'contract_enddate');
    removeFromValidate('EditView', 'gc_timezone_id_c');
    removeFromValidate('EditView', 'gc_timezone_id1_c');
    
    if ((parseInt(parent_startdate.find('.datetimecombo_hr').val()) > 0 || parseInt(parent_startdate.find('.datetimecombo_min').val()) >0 || parseInt(parent_startdate.find('.datetimecombo_sec').val()) > 0) &&  (date_fld_startdate.val() == '' || date_fld_startdate.val() == 'undefined')) {
        addToValidate('EditView', 'contract_startdate', 'date', true, SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_STARTDATE')); //contract start date
    } else {
        addToValidate('EditView', 'contract_startdate', 'date', false, SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_STARTDATE')); //contract start date
    }
    if (date_fld_startdate.val() != '' && date_fld_startdate.val() != 'undefined') {
    	start_full_date = date_fld_startdate.val() + ' ' + parent_startdate.find('.datetimecombo_hr').val() + ':' + parent_startdate.find('.datetimecombo_min').val() + ':' + parent_startdate.find('.datetimecombo_sec').val();
        
    }
    if ((parseInt(parent_startdate.find('.datetimecombo_hr').val()) > 0 || parseInt(parent_startdate.find('.datetimecombo_min').val()) >0 || parseInt(parent_startdate.find('.datetimecombo_sec').val()) > 0) ||  (date_fld_startdate.val() != '' && date_fld_startdate.val() != 'undefined')) {
        addToValidate('EditView', 'gc_timezone_id_c', 'text', true, SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_STARTDATE_TIMEZONE')); //contract start date
    }
    var parent_enddate = $('#div_contract_enddate').closest('td');
   
    var date_fld_enddate = parent_enddate.find('.date_input');

    if ((parseInt(parent_enddate.find('.datetimecombo_hr').val()) > 0 || parseInt(parent_enddate.find('.datetimecombo_min').val()) >0 || parseInt(parent_enddate.find('.datetimecombo_sec').val()) > 0) &&  (date_fld_enddate.val() == '' || date_fld_enddate.val() == 'undefined')) {
        addToValidate('EditView', 'contract_enddate', 'date', true, SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_ENDDATE')); //contract end date
    } else {
        addToValidate('EditView', 'contract_enddate', 'date', false, SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_ENDDATE')); //contract end date
    }
    
    if (date_fld_enddate.val() != '' && date_fld_enddate.val() != 'undefined') {
    	end_full_date = date_fld_enddate.val() + ' ' + parent_enddate.find('.datetimecombo_hr').val() + ':' + parent_enddate.find('.datetimecombo_min').val() + ':' + parent_enddate.find('.datetimecombo_sec').val();
    }
    if ((parseInt(parent_enddate.find('.datetimecombo_hr').val()) > 0 || parseInt(parent_enddate.find('.datetimecombo_min').val()) >0 || parseInt(parent_enddate.find('.datetimecombo_sec').val()) > 0) ||  (date_fld_enddate.val() != '' && date_fld_enddate.val() != 'undefined')) {
        addToValidate('EditView', 'gc_timezone_id1_c', 'text', true, SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_ENDDATE_TIMEZONE')); //contract start date
        addToValidate('EditView', 'contract_startdate', 'date', true, SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_STARTDATE')); //contract start date
    }
    var contract_startdate_timezone = $('#gc_timezone_id_c :selected').attr('label');
    if (contract_startdate_timezone != undefined) {
    	start_time_zone = contract_startdate_timezone.substring(contract_startdate_timezone.indexOf('(')+1, contract_startdate_timezone.indexOf(')'));
    	contract_startdate_timezone = contract_startdate_timezone.substring(0, contract_startdate_timezone.indexOf('('));
    	$('#contract_startdate_timezone').val(contract_startdate_timezone);
    } else {
    	$('#contract_startdate_timezone').val("");
    }
    var contract_enddate_timezone = $('#gc_timezone_id1_c :selected').attr('label');
    if (contract_enddate_timezone != undefined) {
    	end_time_zone = contract_enddate_timezone.substring(contract_enddate_timezone.indexOf('(')+1, contract_enddate_timezone.indexOf(')'));;
    	contract_enddate_timezone = contract_enddate_timezone.substring(0, contract_enddate_timezone.indexOf('('));
        $('#contract_enddate_timezone').val(contract_enddate_timezone);
    } else {
    	$('#contract_enddate_timezone').val("");
    }
    
    if (end_full_date != "" && start_full_date != "") {
    	var start_date = addUTCToDate(start_full_date,start_time_zone);
    	var end_date = addUTCToDate(end_full_date,end_time_zone);
    	if (start_date > end_date) {
        	$("#contract_startdate").parent().append('<div class="error-message">' + SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_STARTDATE_ERROR_MSG') + '</div>');
        	return false;
    	}
    	
    }
    
    return true;   
}

function contractHeaderActions() {
	var end_full_date = "";
    var start_full_date = "";
    //set contract start date field
    var parent_startdate = $('#div_contract_startdate').closest('td');
    var date_hidden_startdate = parent_startdate.find('.datehidden');
    var date_fld_startdate = parent_startdate.find('.date_input');
    if (date_fld_startdate.val() == '') date_hidden_startdate.val('');
    if (date_fld_startdate.val() != '' && date_fld_startdate.val() != 'undefined') {
        start_full_date = date_fld_startdate.val() + ' ' + parent_startdate.find('.datetimecombo_hr').val() + ':' + parent_startdate.find('.datetimecombo_min').val() + ':' + parent_startdate.find('.datetimecombo_sec').val();
    	date_hidden_startdate.val(start_full_date);    
	}
    var parent_enddate = $('#div_contract_enddate').closest('td');
    var date_hidden_enddate = parent_enddate.find('.datehidden');
    var date_fld_enddate = parent_enddate.find('.date_input');
    if (date_fld_enddate.val() == '') date_hidden_enddate.val('');
    if (date_fld_enddate.val() != '' && date_fld_enddate.val() != 'undefined') {
       	end_full_date = date_fld_enddate.val() + ' ' + parent_enddate.find('.datetimecombo_hr').val() + ':' + parent_enddate.find('.datetimecombo_min').val() + ':' + parent_enddate.find('.datetimecombo_sec').val();
    	date_hidden_enddate.val(end_full_date);  
    }
    var contract_startdate_timezone = $('#gc_timezone_id_c :selected').attr('label');
    if (contract_startdate_timezone != undefined) {
    	contract_startdate_timezone = contract_startdate_timezone.substring(0, contract_startdate_timezone.indexOf('('));
        $('#contract_startdate_timezone').val(contract_startdate_timezone);
    } else {
    	$('#contract_startdate_timezone').val("");
    }
    var contract_enddate_timezone = $('#gc_timezone_id1_c :selected').attr('label');
    if (contract_enddate_timezone != undefined) {
    	contract_enddate_timezone = contract_enddate_timezone.substring(0, contract_enddate_timezone.indexOf('('));
        $('#contract_enddate_timezone').val(contract_enddate_timezone);
    } else {
    	$('#contract_enddate_timezone').val("");
    }
    
}
/**
 * : Function to open Contract Custom Audit Log
 *
 * @param : <string> module_name - Sugar Module Name <string> width - width
 *            for the popup window <string> height - height for the popup window
 * @return : <object> win Pop-up window object
 * @author : Dinesh.Itkar
 * @date : 20-Apr-2016
 */
function auditLog(module_name, width, height, contract_id) {
    if (typeof(popupAuditCount) == "undefined" || popupAuditCount == 0) popupAuditCount = 1;
    window.document.close_popup = true;
    width = (width == 600) ? 900 : width;
    height = (height == 400) ? 900 : height;
    URL = 'index.php?' + 'module=Audit' + '&module_name=' + module_name + '&action=Custom_Popup_Picker&query=true&record=' + contract_id + '&create=false&sugar_body_only=true';
    windowName = module_name + '_popup_window' + popupAuditCount;
    windowFeatures = 'width=' + width + ',height=' + height + ',resizable=1,scrollbars=1';
    popup_mode = 'single';
    URL += '&mode=' + popup_mode;
    win = SUGAR.util.openWindow(URL, windowName, windowFeatures);
    if (window.focus) {
        win.focus();
    }
    win.popupAuditCount = popupAuditCount;
    return win;
}
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// BEGIN : TIMEZONE related functions
/**
 * @param : Common function for popup return handler for opening contacts popup
 * @return :
 * @author : Sagar Salunkhe & Arunsakthivel.s
 * @date : 02-May-2016
 */
function set_contacts_popup_return(popup_reply_data) {
    from_contacts_popup_return = true;
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    var retkey = '';
    $.each(name_to_value_array, function(the_key, dValue) {
        if (the_key != 'toJSON') {
            if (retkey == '') retkey = the_key;
            var displayValue = dValue.replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');;
            if (window.document.forms[form_name] && window.document.forms[form_name].elements[the_key]) {
                window.document.forms[form_name].elements[the_key].value = displayValue;
                SUGAR.util.callOnChangeListers(window.document.forms[form_name].elements[the_key]);
            }
        }
    });
    if (retkey == 'contacts_gc_contracts_1contacts_ida') {
        $("#gc_timezone_id_c").val("");
    	$("#gc_timezone_id1_c").val("");
    	$("#contract_startdate_timezone").val("");
    	$("#contract_enddate_timezone").val("");
        populateTimeZoneDropdownList('gc_timezone_id_c', popup_reply_data.name_to_value_array.contacts_country_id_c, 0);
        populateTimeZoneDropdownList('gc_timezone_id1_c', popup_reply_data.name_to_value_array.contacts_country_id_c, 0);
        disableStartDateTimeZone();
        disableEndDateTimeZone();
    } else {
        var rid = retkey.substr(retkey.lastIndexOf('_') + 1);
        retkey = $.trim(retkey.substr(0, 15));
        var con_country_id = popup_reply_data.name_to_value_array.con_country_id;
        if (retkey == "bill_account_id") {
            $('#bill_account_countryid_' + rid).val(con_country_id);
            populateTimeZoneDropdownList('billing_start_date_timezone_', con_country_id, rid);
            populateTimeZoneDropdownList('billing_end_date_timezone_', con_country_id, rid);
            changeLineItemBTypeBillingAccountRelated(rid);
        } else if (retkey == "tech_account_id") {
            $('#tech_account_countryid_' + rid).val(con_country_id);
            populateTimeZoneDropdownList('service_start_date_timezone_', con_country_id, rid);
            populateTimeZoneDropdownList('service_end_date_timezone_', con_country_id, rid);
        }
    }
}
var changeLineItemBillingOrganizationRelated = function(rowid) {
    var billing_type = $.trim($("#billing_type").val());
    if (billing_type == 'OneStop') {
        if (rowid == one_stop_billing_reference_row) {
            var gc_organization_name = $('#gc_organization_name_' + one_stop_billing_reference_row).val();
            var gc_organization_id = $('#gc_organization_id_' + one_stop_billing_reference_row).val();
            var lineItemRows = getLineItemsRowLenth();
            for (var li_rowid = one_stop_billing_reference_row + 1; li_rowid <= lineItemRows; li_rowid++) {
                if ($("#li_deleted_" + li_rowid).val() != '1') {
                    $('#gc_organization_name_' + li_rowid).val(gc_organization_name);
                    $('#gc_organization_id_' + li_rowid).val(gc_organization_id);
                }
            }
        }
    }
};

function set_organization_popup_return(popup_reply_data) {
    from_organization_popup_return = true;
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    var retkey = '';
    $.each(name_to_value_array, function(the_key, dValue) {
        if (the_key != 'toJSON') {
            if (retkey == '') retkey = the_key;
            var displayValue = dValue.replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            if (window.document.forms[form_name] && window.document.forms[form_name].elements[the_key]) {
                window.document.forms[form_name].elements[the_key].value = displayValue;
                SUGAR.util.callOnChangeListers(window.document.forms[form_name].elements[the_key]);
            }
        }
    });
    //your custom code goes here
    var rid = retkey.substr(retkey.lastIndexOf('_') + 1);
    retkey = $.trim(retkey.substr(0, 15));
    if (retkey == "gc_organization") {
        changeLineItemBillingOrganizationRelated(rid);
    }
}

function clear_custom_popup(f_key_v) {
    var rid = f_key_v.substr(f_key_v.lastIndexOf('_') + 1);
    f_key = f_key_v.substr(0, 12);
    if (f_key == "btn_clr_tech") {
        $('#tech_account_countryid_' + rid).val('');
        populateTimeZoneDropdownList('service_start_date_timezone_', '', rid);
        populateTimeZoneDropdownList('service_end_date_timezone_', '', rid);
    } else if (f_key == "btn_clr_bill") {
        $('#bill_account_countryid_' + rid).val('');
        populateTimeZoneDropdownList('billing_start_date_timezone_', '', rid);
        populateTimeZoneDropdownList('billing_end_date_timezone_', '', rid);
        changeLineItemBTypeBillingAccountRelated(rid);
    } else if (f_key == "btn_clr_gc_o") {
        changeLineItemBillingOrganizationRelated(rid);
    }
}
/**
 * @param : Function will show the popup and get the details of InvoiceGroup and add it then update the other line item boxes too
 * @return :
 * @author : Sagar Salunkhe & Arunsakthivel.s
 * @date : 02-May-2016
 */
function populateTimeZoneDropdownList(timezoneObj, contacts_country, rid) {
    //console.log('populateTimeZoneDropdownList : timezoneObj - ' + timezoneObj + ' contacts_country '+ contacts_country);
    //create json object from json string
    var obj = jQuery.parseJSON(json_timezone_list);
    if (rid > 0) {
        // Reset the name hidden used for backend storage only the fields inside the Line items
        $('#' + timezoneObj + 'name_' + rid).val('');
        $('#' + timezoneObj + 'name_' + rid).val('');
        timezoneObj = timezoneObj + rid;
    }
    //remove dropdown options except the first one
    $('option', '#' + timezoneObj).not(':eq(0)').remove();
    // append options to timezone dropdown
    $.each(obj, function(i, value) {
        if ($.trim(contacts_country) == '' || $.trim(contacts_country) == value.code || value.code == '') {
            $('#' + timezoneObj).append($('<option>').text(value.name).attr('value', i).attr('label', value.label).attr('data', value.code).attr('data-lbl', value.label).attr('data-cde', value.code));
        }
    });
    if (timezoneObj == "gc_timezone_id_c" ) disableStartDateTimeZone();
    else if (timezoneObj == "gc_timezone_id1_c") disableEndDateTimeZone();
    else disableLineItemTimeZone(timezoneObj);
    
}
/**
 * @param : On changing timezone set the values to the name field and if it is start TZ then set same to the end TZ
 * @return :
 * @author : Sagar Salunkhe & Arunsakthivel.s
 * @date : 10-May-2016
 */
function litz_change(elemntid, dest, start) {
    var rid = elemntid.substr(elemntid.lastIndexOf("_") + 1);
    var tmp = $("#" + elemntid + " option:selected").data('lbl');
    $("#" + dest + rid).val(tmp);
    if (start == 'true') {
        var tval = $("#" + elemntid + " option:selected").val();
        var newele = elemntid.replace("start", "end");
        $("#" + newele).val(tval);
        var end_dest = dest.replace("start", "end");
        $("#" + end_dest + rid).val(tmp);
        
    }
}
//  END - TimeZone Related functions
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// BEGIN :  InvoiceGroup related functions
/**
 * @param : Function will show the popup and get the details of InvoiceGroup and add it then update the other line item boxes too
 * @return :
 * @author : Arunsakthivel.s
 * @date : 02-May-2016
 */
function createIGDialog(element) {
    $("#invoice_groupname").val('');
    $("#invoice_groupdesc").val('');
    rowid = element.id; // add_invoice_subgroup_0
    rowid = rowid.substr(rowid.lastIndexOf('_') + 1);
    $("#invoice_rowid").val(rowid);
    invoice_group_dialog.dialog("open");
}
/**
 * @param : Function will add the invoice sub group
 * @return :
 * @author : Arunsakthivel.s
 * @update : Rajul.Mondal
 * @date : 02-May-2016
 * @update_date : 05-Oct-2016
 */
function addInvoiceGroups() {
    var invoice_groupname = trim($("#invoice_groupname").val());
    invoice_groupname_l = invoice_groupname.toLowerCase();
    var invoice_groupdesc = trim($("#invoice_groupdesc").val());
    var invoice_rowid = $("#invoice_rowid").val();
    var options = $('[name="li_invoice_subgroup_' + invoice_rowid + '"] option').map(function() {
        return $(this).text();
    }).get();
    if (invoice_groupname == "") {
        $("#invoice_groupname").addClass("form_error");
        $("#error_invoice_groupname").show();
        $("#error_invoice_groupname_unique").hide();
        setTimeout(function() {
            $("#invoice_groupname").removeClass("form_error", 9000);
            $("#error_invoice_groupname").hide();
        }, 9000);
    } else {
        if (jQuery.inArray(invoice_groupname_l, options) >= 0) {
            $("#invoice_groupname").addClass("form_error");
            $("#error_invoice_groupname").hide();
            $("#error_invoice_groupname_unique").show();
            setTimeout(function() {
                $("#invoice_groupname").removeClass("form_error", 9000);
                $("#error_invoice_groupname_unique").hide();
            }, 9000);
        } else {
            var elekey = 'grp_' + invoice_groups_nxtelem;
            invoice_groups_nxtelem++;
            invoice_groups[elekey] = {
                'uuid': '',
                'name': invoice_groupname,
                'desc': invoice_groupdesc
            };
            var lineItemRows = getLineItemsRowLenth();
            for (var rowid = 1; rowid <= lineItemRows; rowid++) {
                $('<option>').val(elekey).text(invoice_groupname).appendTo('#li_invoice_subgroup_' + rowid);
                if (is_change_available == '1' && $('#contract_line_item_type_' + rowid).val() == li_status_to_disp_change || $('#contract_line_item_type_' + rowid).val() == li_status_to_disp_terminate) {
                    disableSelectboxOptions('li_invoice_subgroup_' + rowid);
                }
            }
            $('#li_invoice_subgroup_' + invoice_rowid).val(elekey);
            $('#li_invoice_subgroup_' + invoice_rowid).val(elekey);
            $('#invoice_group_details_json').val(JSON.stringify(invoice_groups));
            invoice_group_dialog.dialog("close");
        }
    }
    return false;
}
/**
 * @param : Function will show invoice groups in other Line Item rows also
 * @return :
 * @author : Arunsakthivel.s
 * @date : 02-May-2016
 */
function showInvoiceGroups(rowid) {
    $.each(invoice_groups, function(elekey, invoice_group) {
        $('<option>').val(elekey).text(invoice_group.name).appendTo('#li_invoice_subgroup_' + rowid);
    });
}
// END :  Invoice sub Group related functions
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// BEGIN : Functionality related to DF-150 One-Stop Billing Dependency
var bType_LI_Fields = new Array('bill_account_name_', 'bill_account_id_', 'bill_account_countryid_', 'payment_type_', 'cust_contract_currency_', 'cust_billing_currency_', 'igc_contract_currency_', 'igc_billing_currency_', 'customer_billing_method_', 'igc_settlement_method_', 'gc_organization_name_', 'gc_organization_id_');
var bType_DD_Fields = new Array('dd_bank_code_', 'dd_branch_code_', 'dd_deposit_type_', 'dd_account_no_', 'dd_person_name_2_', 'dd_payee_tel_no_', 'dd_payee_email_address_', 'dd_remarks_');
var bType_AT_Fields = new Array('at_name_', 'at_bank_code_', 'at_branch_code_', 'at_deposit_type_', 'at_account_no_', 'at_account_no_display_');
var bType_PT_Fields = new Array('pt_name_', 'pt_postal_passbook_', 'pt_postal_passbook_mark_', 'pt_postal_passbook_no_display_');
var bType_CC_Fields = new Array('cc_credit_card_no_', 'cc_expiration_date_');
/**
 * @param : Function change LineItem Fields By Billing_type on the contract header is equal to onestop
 * @return :
 * @author : Arunsakthivel.s
 * @date : 18-May-2016
 */
changeLineItemFieldsByBilling_type = function(single, li_rowid) {
    var billing_type = $.trim($("#billing_type").val()); //OneStop / Separate
    if (billing_type == 'OneStop') {
        if (single == true && li_rowid > 1) {
            changeLineItemBTypeFields(li_rowid);
        } else {
            var lineItemRows = getLineItemsRowLenth();
            for (var li_rowid = one_stop_billing_reference_row + 1; li_rowid <= lineItemRows; li_rowid++) {
                if ($("#li_deleted_" + li_rowid).val() != '1') {
                    changeLineItemBTypeFields(li_rowid);
                    changeLineItemBTypeBillingAccountRelated(li_rowid);
                }
            }
        }
        resetLineItemBTypeFields(true);
    } else {
        var lineItemRows = getLineItemsRowLenth();
        for (var li_rowid = one_stop_billing_reference_row + 1; li_rowid <= lineItemRows; li_rowid++) {
            if ($("#li_deleted_" + li_rowid).val() != '1') {
                $('#btn_bill_account_name_' + li_rowid).show();
                $('#btn_clr_bill_account_name_' + li_rowid).show();
                $('#btn_gc_organization_name_' + li_rowid).show();
                $('#btn_clr_gc_organization_name_' + li_rowid).show();
            }
        }
        resetLineItemBTypeFields(false);
    }
};
/**
 * @param : Function to change LineItem Fields By Billing_type on the contract header is equal to onestop
 * @return :
 * @author : Arunsakthivel.s
 * @date : 18-May-2016
 */
changeLineItemBTypeFields = function(li_rowid) {
    $.each(bType_LI_Fields, function(speckey, billTypeField) {
        var tmp = $('#' + billTypeField + one_stop_billing_reference_row).val();
        $('#' + billTypeField + li_rowid).val(tmp);
    });
    // Timezone selection should done based on billing account change
    var bill_country = $('#bill_account_countryid_' + li_rowid).val();
    var bill_st_tz_val = $('#billing_start_date_timezone_' + li_rowid).val();
    var bill_ed_tz_val = $('#billing_end_date_timezone_' + li_rowid).val();
    populateTimeZoneDropdownList('billing_start_date_timezone_', bill_country, li_rowid);
    populateTimeZoneDropdownList('billing_end_date_timezone_', bill_country, li_rowid);
    //Set the billing timezone details after populated it
    $('#billing_start_date_timezone_'+li_rowid).val(bill_st_tz_val);
    $('#billing_end_date_timezone_'+li_rowid).val(bill_ed_tz_val);
    // Relate field buttons hide
    $('#btn_bill_account_name_' + li_rowid).hide();
    $('#btn_clr_bill_account_name_' + li_rowid).hide();
    $('#btn_gc_organization_name_' + li_rowid).hide();
    $('#btn_clr_gc_organization_name_' + li_rowid).hide();
    // Payment sub panel display
    togglePayemntType($("#payment_type_" + li_rowid), '');
    var bType_pay_Fields = new Array();
    bType_pay_Fields = $.merge($.merge($.merge(bType_DD_Fields, bType_AT_Fields), bType_PT_Fields), bType_CC_Fields);
    $.each(bType_pay_Fields, function(speckey, billTypeField) {
        var tmp = $('#' + billTypeField + one_stop_billing_reference_row).val();
        $('#' + billTypeField + li_rowid).val(tmp);
    });
    resetLineItemBTypeFields(true);
};
/**
 * @param : Function to change the particular field value in other line item rows when the elemnet on change
 * @return :
 * @author : Arunsakthivel.s
 * @date : 18-May-2016
 */
changeLineItemBTypeFieldValue = function(element) {
    var billing_type = $.trim($("#billing_type").val());
    if (billing_type == 'OneStop') {
        var eleid = $.trim(element.id);
        var lim_rowid = eleid.substr(eleid.lastIndexOf('_') + 1);
        var namekey = eleid.substr(0, eleid.lastIndexOf('_') + 1);
        if (lim_rowid == one_stop_billing_reference_row) {
            var tmp = $('#' + eleid).val();
            var lineItemRows = getLineItemsRowLenth();
            for (var li_rowid = one_stop_billing_reference_row + 1; li_rowid <= lineItemRows; li_rowid++) {
                if ($("#li_deleted_" + li_rowid).val() != '1') {
                    $('#' + namekey + li_rowid).val(tmp);
                    $('#' + namekey + li_rowid).prop('disabled', true);
                    if (namekey == 'payment_type_') {
                    	$('#' + namekey + li_rowid+' option').prop('disabled', false);
                        togglePayemntType($("#payment_type_" + li_rowid), '');
                        $('#' + namekey + li_rowid+' option:not(:selected)').prop('disabled', true);
                    } else if(namekey == 'dd_deposit_type_' || namekey == 'at_deposit_type_' || namekey == 'at_account_no_display_' 
                    	|| namekey == 'pt_postal_passbook_no_display_') {
                    	$('#' + namekey + li_rowid+' option:selected').prop('disabled', false);
                    }
					
                    if (namekey == "cust_contract_currency_") {
                        product_currency_change(namekey + li_rowid);
                    }
                }
            }
        }
    }
};
/**
 * @param : Function to handle specialy the billing account change it should handle the timezone change tooo
 * @return :
 * @author : Arunsakthivel.s
 * @date : 20-May-2016
 */
changeLineItemBTypeBillingAccountRelated = function(rowid) {
    var billing_type = $.trim($("#billing_type").val());
    if (billing_type == 'OneStop') {
        if (rowid == one_stop_billing_reference_row) {
            var bill_account_name = $('#bill_account_name_' + one_stop_billing_reference_row).val();
            var bill_account_id = $('#bill_account_id_' + one_stop_billing_reference_row).val();
            var con_country_id = $('#bill_account_countryid_' + one_stop_billing_reference_row).val();
            var lineItemRows = getLineItemsRowLenth();
            for (var li_rowid = one_stop_billing_reference_row + 1; li_rowid <= lineItemRows; li_rowid++) {
                if ($("#li_deleted_" + li_rowid).val() != '1') {
                    $('#bill_account_name_' + li_rowid).val(bill_account_name);
                    $('#bill_account_id_' + li_rowid).val(bill_account_id);
                    $('#bill_account_countryid_' + li_rowid).val(con_country_id);
                }
            }
        }
    }
};
/**
 * @param : Reset the field disable stage
 * @return :
 * @author : Arunsakthivel.s
 * @date : 18-May-2016
 */
resetLineItemBTypeFields = function(disabled) {
    var lineItemRows = getLineItemsRowLenth();
    $.each(bType_LI_Fields, function(speckey, billTypeField) {
        $('#' + billTypeField + one_stop_billing_reference_row).prop('disabled', false);
    });
    var bType_pay_Fields = new Array();
    bType_pay_Fields = $.merge($.merge($.merge(bType_DD_Fields, bType_AT_Fields), bType_PT_Fields), bType_CC_Fields);
    $.each(bType_pay_Fields, function(speckey, billTypeField) {
        $('#' + billTypeField + one_stop_billing_reference_row).prop('disabled', false);
    });
    if (version_upgrade == true && is_change_available == '1') {
        $('#btn_bill_account_name_' + one_stop_billing_reference_row).show();
        $('#btn_clr_bill_account_name_' + one_stop_billing_reference_row).show();
        $('#btn_gc_organization_name_' + one_stop_billing_reference_row).show();
        $('#btn_clr_gc_organization_name_' + one_stop_billing_reference_row).show();
    }
    for (var li_rowid = one_stop_billing_reference_row + 1; li_rowid <= lineItemRows; li_rowid++) {
        if ($("#li_deleted_" + li_rowid).val() != '1') {
            $.each(bType_LI_Fields, function(speckey, billTypeField) {
                $('#' + billTypeField + li_rowid).prop('disabled', disabled);
            });
            var bType_pay_Fields = new Array();
            bType_pay_Fields = $.merge($.merge($.merge(bType_DD_Fields, bType_AT_Fields), bType_PT_Fields), bType_CC_Fields);
            $.each(bType_pay_Fields, function(speckey, billTypeField) {
                $('#' + billTypeField + li_rowid).prop('disabled', disabled);
            });
        }
    }
};
//END : DF-150 One-Stop Billing Dependency
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
saveContractForm = function() {
    console.log(1);
    if (checkMaxPostVar()) {
        console.log(2);
		$( ".error-message" ).remove();
		$('.error').remove();
    	var error_count = 0;
        var _form = document.getElementById('EditView');
        _form.action.value = 'Save';
        var billing_type = $.trim($("#billing_type").val()); //OneStop / Separate
        if (billing_type == 'OneStop') resetLineItemBTypeFields(false);
        lineItemActions();
        contractHeaderActions();
        addToValidateList();
        error_count = (!contractDateTimezoneValidation()) ? error_count+1 : error_count;
        error_count = (!addDateTimezoneValidation()) ? error_count+1 : error_count;
        error_count = (!addPaymentValidation()) ? error_count+1 : error_count;
        error_count = (!CustomValidateSave()) ? error_count+1 : error_count;

        //Contracting Customer Company: validation
        removeFromValidate('EditView', 'accounts_gc_contracts_1_name');
        removeFromValidate('EditView', 'accounts_gc_contracts_1accounts_ida');
        if ($.trim($("#accounts_gc_contracts_1accounts_ida").val()) == '') {
            addToValidate('EditView', 'accounts_gc_contracts_1_name', 'text', true, SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_CUST_COMPANY'));
        } else {
            removeFromValidate('EditView', 'accounts_gc_contracts_1_name');
        }
        //Contracting Customer Account: validation
        removeFromValidate('EditView', 'contacts_gc_contracts_1_name');
        removeFromValidate('EditView', 'contacts_gc_contracts_1contacts_ida');
        if ($.trim($("#contacts_gc_contracts_1contacts_ida").val()) == '') {
            addToValidate('EditView', 'contacts_gc_contracts_1_name', 'text', true, SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_ACCOUNT_ID'));
        } else {
            removeFromValidate('EditView', 'contacts_gc_contracts_1_name');
        }
        error_count += validateSaleRep();
        error_count += prodConfigValidation();
        if (check_form('EditView') && addRequiredBaseValidation() && error_count == 0) {
            console.log(3);
            if (is_change_available == '1') {
                if (checkAllLineItemsAreTerminate() && checkAllBaseAreInTermination() && error_count == 0) {
                    SUGAR.ajaxUI.submitForm(_form);
                }
                if (billing_type == 'OneStop') resetLineItemBTypeFields(true);
            } else if (error_count == 0){
                SUGAR.ajaxUI.submitForm(_form);
            }
        } else {
            console.log(4);
            if (billing_type == 'OneStop') resetLineItemBTypeFields(true);
        }
    } else {
        console.log(5);
        YAHOO.SUGAR.MessageBox.show({
            width: 250,
            msg: SUGAR.language.get('gc_Contracts', 'LBL_MAX_INPUT_VARS_EXCEEDS_MSG') + '(' + GlobalMaxPostVar + ')',
            title: SUGAR.language.get('gc_Contracts', 'LBL_MAX_INPUT_VARS_EXCEEDS_TITLE'),
            type: 'alert',
        });
    }
    return false;
};
$(document).ready(function() {
	YAHOO.util.Event.onDOMReady(function(){
		YAHOO.util.Event.addListener("contract_startdate","change",onSelectDateTime);
		YAHOO.util.Event.addListener("contract_enddate","change",onSelectDateTime);
	});
	pmsValidation();
    version_upgrade = $('#version_upgrade').val();
    if (($("#approver_user").val() != "") && ($("#contract_status").val() == "1") && (version_upgrade != 'true')) {
        restrictApproverFields();
    }
    document.getElementById('SAVE_HEADER').onclick = function() {
        return saveContractForm();
    };
    document.getElementById('SAVE_FOOTER').onclick = function() {
        return saveContractForm();
    };
    markFieldsReadOnly('contacts_gc_contracts_1_name');
    markFieldsReadOnly('accounts_gc_contracts_1_name');
    customContractHeaderFields();
    // restrict user from changing contract status from GUI
    $("#contract_status option").filter(":selected").siblings('option').prop('disabled', true);
    // restrict user from changing contract line item type status from GUI
    $(".contract_line_item_type option").filter(":selected").siblings('option').prop('disabled', true);
    //OneStop / Separate
    $("#billing_type").on("change", function() {
        changeLineItemFieldsByBilling_type(false, 0);
    });
    // BEGIN :  InvoiceGroup related functions
    invoice_group_dialog = $("#invoice_group_dialog").dialog({
        autoOpen: false,
        height: 250,
        width: 500,
        modal: true,
        buttons: {
            "Save": addInvoiceGroups
        },
    });
    if (typeof record === 'undefined' || !record) record = '';
    // END :  Invoice sub Group related functions
    if (record != "") { // Edit page
        // BEGIN :  InvoiceGroup related functions
        var tmp = trim($('#invoice_group_details_json').val());
        if (tmp != "") {
            invoice_groups = jQuery.parseJSON(tmp);
            var lineItemRows = getLineItemsRowLenth();
            for (var rowid = 1; rowid <= lineItemRows; rowid++) {
                var sel_val = '';
                var hidden_igid = $('#hid_li_invoice_subgroup_' + rowid).val();
                $.each(invoice_groups, function(elekey, invoice_group) {
                    if (rowid == 1) invoice_groups_nxtelem++;
                    if (hidden_igid == invoice_group.uuid) {
                        $('<option>').val(elekey).text(invoice_group.name).attr("selected", 'true').appendTo('#li_invoice_subgroup_' + rowid);
                    } else {
                        $('<option>').val(elekey).text(invoice_group.name).appendTo('#li_invoice_subgroup_' + rowid);
                    }
                });
            }
        }
        // END :  Invoice sub Group related functions
        // BEGIN : For Time Zone filtering
        for (var rowid = 1; rowid <= lineItemRows; rowid++) {
            var contacts_country = $('#tech_account_countryid_' + rowid).val();
            if (contacts_country != "") {
                var tmp = $('#service_start_date_timezone_' + rowid).val();
                populateTimeZoneDropdownList('service_start_date_timezone_', contacts_country, rowid);
                $('#service_start_date_timezone_' + rowid).val(tmp);
                var tmp = $('#service_end_date_timezone_' + rowid).val();
                populateTimeZoneDropdownList('service_end_date_timezone_', contacts_country, rowid);
                $('#service_end_date_timezone_' + rowid).val(tmp);
            }
            var contacts_country = $('#bill_account_countryid_' + rowid).val();
            if (contacts_country != "") {
                var tmp = $('#billing_start_date_timezone_' + rowid).val();
                populateTimeZoneDropdownList('billing_start_date_timezone_', contacts_country, rowid);
                $('#billing_start_date_timezone_' + rowid).val(tmp);
                var tmp = $('#billing_end_date_timezone_' + rowid).val();
                populateTimeZoneDropdownList('billing_end_date_timezone_', contacts_country, rowid);
                $('#billing_end_date_timezone_' + rowid).val(tmp);
            }
        }
        // END :  For Time Zone filtering
        changeLineItemFieldsByBilling_type(false, 0);
    }
    getMaxInputVar();
    if (version_upgrade == 'true') {
        $('input[name="record"]').val('');
        $('.line_item_history_id').val('');
        $('.line_item_objid').html('');
        $('#contract_status').val(0);
        $("#contract_status option").filter(":selected").siblings('option').prop('disabled', true);
        markFieldsReadOnly('gc_timezone_id_c');
        markFieldsReadOnly('product_offering');
        markFieldsReadOnly('ext_sys_modified_by');
        markFieldsReadOnly('ext_sys_created_by');
        $('#ext_sys_modified_by').val('');
        $('#ext_sys_created_by').val('');
        var isg_tmp = invoice_groups;
        $.each(isg_tmp, function(elekey, invoice_group) {
            isg_tmp[elekey].parent_uuid = isg_tmp[elekey].uuid;
            isg_tmp[elekey].uuid = '';
        });
        console.log(isg_tmp);
        invoice_groups = isg_tmp;
        $('#invoice_group_details_json').val(JSON.stringify(invoice_groups));
    }
    // OM-341
    var is_user_admin = $('#is_admin_flag').val();
    if (version_upgrade == 'true' && is_user_admin != 'true') {
        $('#contract_startdate_trigger').hide();
        markFieldsReadOnly('contract_startdate');
        markFieldsReadOnly('contract_startdate_hours');
        markFieldsReadOnly('contract_startdate_minutes');
        markFieldsReadOnly('contract_startdate_seconds');
    }
    //if the version is upgraded then product offering, contract start date, start time zone should be disabled
    if (is_versionedup_contract == '1' && is_user_admin != 'true') {
        $('#contract_startdate_trigger').hide();
        markFieldsReadOnly('product_offering');
        markFieldsReadOnly('gc_timezone_id_c');
        markFieldsReadOnly('product_offering');
        markFieldsReadOnly('contract_startdate');
        markFieldsReadOnly('contract_startdate_hours');
        markFieldsReadOnly('contract_startdate_minutes');
        markFieldsReadOnly('contract_startdate_seconds');
        markFieldsReadOnly('ext_sys_modified_by');
        markFieldsReadOnly('ext_sys_created_by');
    }
    
    $("#contract_startdate_hours, #contract_startdate_minutes, #contract_startdate_seconds").change(function(){
    	disableStartDateTimeZone();
    });
    $("#contract_enddate_hours, #contract_enddate_minutes, #contract_enddate_seconds").change(function(){
    	disableEndDateTimeZone();
    });
   
    disableStartDateTimeZone();
    disableEndDateTimeZone();
    $('#description,#invoice_groupdesc').attr('maxLength', description_max_length);
});
/**
 * @param : Function to disable particular field while approver editing in contract activate status
 * @return :
 * @author : Kamal.Thiyagarajan
 * @date : 21-May-2016
 */
function restrictApproverFields() {
    $("#btn_accounts_gc_contracts_1_name, #btn_clr_accounts_gc_contracts_1_name, #btn_pm_products_gc_contracts_1_name, #btn_clr_pm_products_gc_contracts_1_name, #teamSelect, #teamAdd").remove();
    $("#accounts_gc_contracts_1_name, #pm_products_gc_contracts_1_name").prop('readonly', true);
    markFieldsReadOnly("contract_scheme");
    markFieldsReadOnly("contract_type");
    markFieldsReadOnly("billing_type");
    markFieldsReadOnly("product_offering");
    $("input[name^='team_name_collection_']").prop('readonly', true);
    $("[id^='remove_team_name_collection_']").remove();
    $("input[name='primary_team_name_collection']").attr("onclick", "return false");
    /* $("input[name^='product_name_']").prop('readonly', true);
    $("[id^='btn_product_name_']").remove();
    $("[id^='btn_clr_product_name_']").remove(); */
    $('[id^="product_config_"]').prop('readonly', true);
    $('.LineItem').each(function() {
        var line_item_id = $(this).attr('id');
        var line_id_array = line_item_id.split('_');
        var line_item_seq = line_id_array[2];
        $('#cust_billing_currency_' + line_item_seq + ' option').filter(":selected").siblings('option').prop('disabled', true);
        $('#cust_contract_currency_' + line_item_seq + ' option').filter(":selected").siblings('option').prop('disabled', true);
        $('#igc_contract_currency_' + line_item_seq + ' option').filter(":selected").siblings('option').prop('disabled', true);
        $('#igc_billing_currency_' + line_item_seq + ' option').filter(":selected").siblings('option').prop('disabled', true);
    });
}
/*
 * Function to get the max input var from php ini using ajax
 * DF-513
 * @author : Rajul.Mondal
 */
function getMaxInputVar() {
    $.ajax({
        type: "POST",
        url: "index.php?module=gc_Contracts",
        dataType: "json",
        async: false,
        data: {
            "method": "getMaxInputVar",
            'sugar_body_only': 1,
            'action': 'Actions'
        },
        success: function(response) {
            if (response != '') {
                window.GlobalMaxPostVar = parseInt(response);
            } else {
                window.GlobalMaxPostVar = 25000;
            }
            return window.GlobalMaxPostVar;
        }
    });
}
/*
 * Function to check whetehr the inpur count exceeds the allowed limit
 * DF-513
 * @author : Rajul.Mondal
 */
function checkMaxPostVar() {
    var postLength = $('form').find('input:not([disabled]), select:not([disabled]), textarea:not([disabled])').length;
    var max_input_var = window.GlobalMaxPostVar;
    if (parseInt(postLength) < parseInt(max_input_var)) return true;
    return false;
}

function setEndDateTimezone(objElement, second_element_id) {
    var secondElementObject = $('#' + second_element_id);
    if (typeof objElement === 'object' && typeof secondElementObject === 'object') secondElementObject.val(objElement.value);
}
/*
 * Function to enable fields in the EditView form
 * DF-156
 * @author : Rajul.Mondal
 * @date : 08-July-2016
 */
var enableFieldsOnTerminationApproved = (function(fields) {
    if (fields.length > 0) {
        $.each(fields, function(i, field) {
            $('form#EditView').find('input[name^="' + field + '"],select[name^="' + field + '"],textarea[name^="' + field + '"]').prop('readonly', false);
            $('form#EditView').find('select[name^="' + field + '"] option').attr('disabled', false);
            $('form#EditView').find('[id^="btn_' + field + '"]').parent().parent().find('.id-ff,.id-ff-remove,[id^="add_invoice_subgroup_"],#add_line_item').show();
        });
    }
});
/*
 * Function to disable fields in the EditView form
 * DF-156
 * @author : Rajul.Mondal
 * @date : 08-July-2016
 */
var disableFieldsOnTerminationApproved = (function() {
    $('form#EditView').find('input,textarea').prop('readonly', true);
    $('form#EditView').find('select option:not(:selected)').attr('disabled', true);
    $('.id-ff,.id-ff-remove,[id^="add_invoice_subgroup_"],#add_line_item').hide();
    $('[id^="billing_start_date_trigger_"],[id^="service_start_date_trigger_"],[id^="pp_discountflag_"],#contract_startdate_trigger').hide();
    var terminateRecordId = $('[name="record"]').val();
    //provide name attribute of the field
    var enableFields = ['name', 'description', 'contract_enddate', 'contract_enddate_hours', 'contract_enddate_minutes', 'contract_enddate_seconds', 'gc_timezone_id1_c', 'service_end_date_', 'billing_end_date_', 'description_', 'gc_organization_name_', 'gc_organization_id_'];
    $.ajax({
        type: "POST",
        url: "index.php?module=gc_Contracts",
        dataType: "json",
        async: false,
        data: {
            "method": "isApproverUser",
            'sugar_body_only': 1,
            'action': 'Actions',
            'record': terminateRecordId
        },
        success: function(response) {
            if (response === true) {
                enableFieldsOnTerminationApproved(enableFields);
            }
        }
    });
});
/*
 * On document ready check crontract terminated or not
 * DF-156
 * @author : Rajul.Mondal
 * @date : 08-July-2016
 */
var isContractStatusTerminated = (function() {
    var contractStatus = $('[name="contract_status"]').val();
    var terminateRecordId = $('[name="record"]').val();
    var is_admin_flag = $('#is_admin_flag').val();
    if (terminateRecordId.length !== 0 && contractStatus === '6' && is_admin_flag === '') {
        disableFieldsOnTerminationApproved();
    }
});
$(document).ready(function() {
    isContractStatusTerminated();
});
/**
 * @desc : Function to revert changes on line item.
 * DF-155
 * @params : Line item sequence number
 * @return :
 * @author : Kamal Thiyagarajan
 * @date : 28-JUL-2016
 */
function cancelChangeLineItem(index) {
    var parent_line_item_id = $('#line_item_parent_id_' + index).val();
    var parent_line_item_history_id = $('#line_item_history_parent_id_' + index).val();
    var RecordId = $('input[name="record"]').val();
    var product_offering = $("#product_offering").val();
    $.ajax({
        type: "POST",
        url: "index.php?module=gc_Contracts",
        dataType: "json",
        async: false,
        data: {
            'method': 'getLineItemDetails',
            'record_id': RecordId,
            'line_history_id': parent_line_item_history_id,
            'product_offering': product_offering,
            'sugar_body_only': 1,
            'action': 'Actions'
        },
        success: function(result) {
            var product_specification = decodeHtml(result.product_specification);
            var product_specification_type_name = decodeHtml(result.product_specification_type_name);
            var cust_contract_currency = decodeHtml(result.cust_contract_currency);
            var cust_billing_currency = decodeHtml(result.cust_billing_currency);
            var igc_contract_currency = decodeHtml(result.igc_contract_currency);
            var igc_billing_currency = decodeHtml(result.igc_billing_currency);
            var customer_billing_method = decodeHtml(result.customer_billing_method);
            var igc_settlement_method = decodeHtml(result.igc_settlement_method);
            var li_invoice_subgroup = decodeHtml(result.group_name);
            var description = decodeHtml(result.description);
            var payment_type = decodeHtml(result.payment_type_id);
            var factory_reference_no = decodeHtml(result.factory_reference_no);
            var service_start_date_timezone = decodeHtml(result.service_start_date_timezone_id);
            var service_end_date_timezone = decodeHtml(result.service_end_date_timezone_id);
            var billing_start_date_timezone = decodeHtml(result.billing_start_date_timezone_id);
            var billing_end_date_timezone = decodeHtml(result.billing_end_date_timezone_id);
            var tech_account_name = decodeHtml(result.tech_account_name);
            var tech_account_id = decodeHtml(result.tech_account_id);
            var tech_account_countryid = decodeHtml(result.tech_account_country);
            var bill_account_name = decodeHtml(result.bill_account_name);
            var bill_account_id = decodeHtml(result.bill_account_id);
            var bill_account_countryid = decodeHtml(result.bill_account_countryid);
            var product_name = decodeHtml(result.products_name);
            var product_id = decodeHtml(result.products_id);
            var gc_organization_name = decodeHtml(result.gc_organization_name);
            var gc_organization_id = decodeHtml(result.gc_organization_id);
            if (result.service_start_date) {
                var service_start_date_time = decodeHtml(result.service_start_date);
                var service_start_date_array = service_start_date_time.split(" ");
                var service_start_date = service_start_date_array[0];
                var service_start_time_array = service_start_date_array[1].split(":");
                $("#service_start_date_hidden_" + index).val(service_start_date_time);
                $("#service_start_date_" + index).val(service_start_date);
                $("#service_start_date_hours_" + index).val(service_start_time_array[0]);
                $("#service_start_date_minutes_" + index).val(service_start_time_array[1]);
                $("#service_start_date_seconds_" + index).val(service_start_time_array[2]);
            } else {
                $("#service_start_date_hidden_" + index).val("");
                $("#service_start_date_" + index).val("");
                $("#service_start_date_hours_" + index).val("");
                $("#service_start_date_minutes_" + index).val("");
                $("#service_start_date_seconds_" + index).val("");
            }
            if (result.service_end_date) {
                var service_end_date_time = decodeHtml(result.service_end_date);
                var service_end_date_array = service_end_date_time.split(" ");
                var service_end_date = service_end_date_array[0];
                var service_end_time_array = service_end_date_array[1].split(":");
                $("#service_end_date_hidden_" + index).val(service_end_date_time);
                $("#service_end_date_" + index).val(service_end_date);
                $("#service_end_date_hours_" + index).val(service_end_time_array[0]);
                $("#service_end_date_minutes_" + index).val(service_end_time_array[1]);
                $("#service_end_date_seconds_" + index).val(service_end_time_array[2]);
            } else {
                $("#service_end_date_hidden_" + index).val("");
                $("#service_end_date_" + index).val("");
                $("#service_end_date_hours_" + index).val("");
                $("#service_end_date_minutes_" + index).val("");
                $("#service_end_date_seconds_" + index).val("");
            }
            if (result.billing_start_date) {
                var billing_start_date_time = decodeHtml(result.billing_start_date);
                var billing_start_date_array = billing_start_date_time.split(" ");
                var billing_start_date = billing_start_date_array[0];
                var billing_start_time_array = billing_start_date_array[1].split(":");
                $("#billing_start_date_hidden_" + index).val(billing_start_date_time);
                $("#billing_start_date_" + index).val(billing_start_date);
                $("#billing_start_date_hours_" + index).val(billing_start_time_array[0]);
                $("#billing_start_date_minutes_" + index).val(billing_start_time_array[1]);
                $("#billing_start_date_seconds_" + index).val(billing_start_time_array[2]);
            } else {
                $("#billing_start_date_hidden_" + index).val("");
                $("#billing_start_date_" + index).val("");
                $("#billing_start_date_hours_" + index).val("");
                $("#billing_start_date_minutes_" + index).val("");
                $("#billing_start_date_seconds_" + index).val("");
            }
            if (result.billing_end_date) {
                var billing_end_date_time = decodeHtml(result.billing_end_date);
                var billing_end_date_array = billing_end_date_time.split(" ");
                var billing_end_date = billing_end_date_array[0];
                var billing_end_time_array = billing_end_date_array[1].split(":");
                $("#billing_end_date_hidden_" + index).val(billing_end_date_time);
                $("#billing_end_date_" + index).val(billing_end_date);
                $("#billing_end_date_hours_" + index).val(billing_end_time_array[0]);
                $("#billing_end_date_minutes_" + index).val(billing_end_time_array[1]);
                $("#billing_end_date_seconds_" + index).val(billing_end_time_array[2]);
            } else {
                $("#billing_end_date_hidden_" + index).val("");
                $("#billing_end_date_" + index).val("");
                $("#billing_end_date_hours_" + index).val("");
                $("#billing_end_date_minutes_" + index).val("");
                $("#billing_end_date_seconds_" + index).val("");
            }
            //service_start_date_hidden_1
            //service_start_date
            // product_specification_id, product_specification_uuid, product_specification, product_specification_type_name
            console.log(product_specification + ' ('+ product_specification_type_name +')');
            selectByText("product_specification_" + index, product_specification + ' ('+ product_specification_type_name +')');
            $("#product_specification_" + index).trigger("change");
            selectByText("cust_contract_currency_" + index, cust_contract_currency);
            $("#cust_contract_currency_" + index).trigger("change");
            selectByText("cust_billing_currency_" + index, cust_billing_currency);
            selectByText("igc_contract_currency_" + index, igc_contract_currency);
            selectByText("igc_billing_currency_" + index, igc_billing_currency);
            selectByText("customer_billing_method_" + index, customer_billing_method);
            selectByText("igc_settlement_method_" + index, igc_settlement_method);
            selectByText("li_invoice_subgroup_" + index, li_invoice_subgroup);
            $("#payment_type_" + index).val(payment_type);
            $("#payment_type_" + index).trigger("change");
            $("#factory_reference_no_" + index).val(factory_reference_no);
            $("#description_" + index).val(description);
            $("#contract_line_item_type_" + index + ' option').filter(":selected").siblings('option').prop('disabled', false);
            $("#contract_line_item_type_" + index).val("Activated");
            $("#contract_line_item_type_" + index + ' option').filter(":selected").siblings('option').prop('disabled', true);
            $("#service_start_date_timezone_" + index).val(service_start_date_timezone);
            $("#service_end_date_timezone_" + index).val(service_end_date_timezone);
            $("#billing_start_date_timezone_" + index).val(billing_start_date_timezone);
            $("#billing_end_date_timezone_" + index).val(billing_end_date_timezone);
            $("#tech_account_name_" + index).val(tech_account_name);
            $("#tech_account_id_" + index).val(tech_account_id);
            $("#tech_account_countryid_" + index).val(tech_account_countryid);
            $("#bill_account_name_" + index).val(bill_account_name);
            $("#bill_account_id_" + index).val(bill_account_id);
            $("#bill_account_countryid_" + index).val(bill_account_countryid);
            //$("#product_name_" + index).val(product_name);
            //$("#product_id_" + index).val(product_id);
            $("#gc_organization_name_" + index).val(gc_organization_name);
            $("#gc_organization_id_" + index).val(gc_organization_id);
            switch (result.payment_type_id) {
                case '00':
                    if (result.deposit_deposit_details != undefined) {
                        $("#dd_bank_code_" + index).val(decodeHtml(result.deposit_deposit_details.bank_code));
                        $("#dd_branch_code_" + index).val(decodeHtml(result.deposit_deposit_details.branch_code));
                        selectByText("dd_deposit_type_" + index, result.deposit_deposit_details.deposit_type);
                        $("#dd_account_no_" + index).val(decodeHtml(result.deposit_deposit_details.account_no));
                        // $("#dd_person_name_1_" + index).val(decodeHtml(result.deposit_deposit_details.person_name_1));
                        $("#dd_person_name_2_" + index).val(decodeHtml(result.deposit_deposit_details.person_name_2));
                        $("#dd_payee_tel_no_" + index).val(decodeHtml(result.deposit_deposit_details.payee_tel_no));
                        $("#dd_payee_email_address_" + index).val(decodeHtml(result.deposit_deposit_details.payee_email_address));
                        $("#dd_remarks_" + index).val(decodeHtml(result.deposit_deposit_details.remarks));
                    }
                    break;
                case '10':
                    if (result.acc_trans_details != undefined) {
                        var acc_holder_name = decodeHtml(result.acc_trans_details.name);
                        $("#at_name_" + index).val(acc_holder_name);
                        var bank_code = decodeHtml(result.acc_trans_details.bank_code);
                        $("#at_bank_code_" + index).val(bank_code);
                        var deposit_type = decodeHtml(result.acc_trans_details.deposit_type);
                        selectByText("at_deposit_type_" + index, deposit_type);
                        var branch_code = decodeHtml(result.acc_trans_details.branch_code);
                        $("#at_branch_code_" + index).val(branch_code);
                        var account_no = decodeHtml(result.acc_trans_details.account_no);
                        $("#at_account_no_" + index).val(account_no);
                        var account_no_display = result.acc_trans_details.account_no_display;
                        selectByText("at_account_no_display_" + index, account_no_display);
                    }
                    break;
                case '30':
                    if (result.postal_trans_details != undefined) {
                        $("#pt_name_" + index).val(decodeHtml(result.postal_trans_details.name));
                        $("#pt_postal_passbook_mark_" + index).val(decodeHtml(result.postal_trans_details.postal_passbook_mark));
                        $("#pt_postal_passbook_" + index).val(decodeHtml(result.postal_trans_details.postal_passbook));
                        selectByText("pt_postal_passbook_no_display_" + index, result.postal_trans_details.postal_passbook_no_display);
                    }
                    break;
                case '60':
                    if (result.credit_card_details != undefined) {
                        $("#cc_credit_card_no_" + index).val(decodeHtml(result.credit_card_details.credit_card_no));
                        $("#cc_expiration_date_" + index).val(decodeHtml(result.credit_card_details.expiration_date));
                    }
                    break;
            }
            if (result.product_config) {
                $.each(result.product_config, function(ch_key, characteristic) {
                    var characteristicvalue = characteristic.characteristic_value;
                    $("#product_config_" + index + "_" + ch_key).val(characteristicvalue);
                    $("#product_config_" + index + "_" + ch_key).trigger("change");
                });
            }
            if (result.rules) {
                $.each(result.rules, function(ch_key, rule) {
                    $("#pi_role_value_" + index + "_" + ch_key).val(rule.rule_value);
                });
            }
            if (result.product_price) {
                $.each(result.product_price, function(ch_key, price) {
                    $("#pp_discount_" + index + "_" + ch_key).val(price.discount);
                    $("#pp_list_price_" + index + "_" + ch_key).val(price.list_price);
                    $("#pp_igc_settlement_price_" + index + "_" + ch_key).val(price.igc_settlement_price);
                    $("#spn_customer_contract_price_" + index + "_" + ch_key).html(price.charge_price);
                    $("#pp_customer_contract_price_" + index + "_" + ch_key).val(price.charge_price);
                    $("#pp_icb_flag_" + index + "_" + ch_key).val(price.icb_flag);
                    if (price.percent_discount_flag != "0") {
                        $("#pp_discountflag_" + index + "_" + ch_key).attr('checked', true);
                        $("#pp_percent_discount_flag_" + index + "_" + ch_key).val(1);
                    } else {
                        $("#pp_discountflag_" + index + "_" + ch_key).attr('checked', false);
                        $("#pp_percent_discount_flag_" + index + "_" + ch_key).val(0);
                    }
                });
            }
        }
    });
    $('#cancel_change_line_item_' + index).hide();
    $('#terminate_line_item_' + index).show();
    $('#change_contract_' + index).show();
    disableAllFieldsInALineItem(index);
}

function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

function selectByText(id, txt) {
    $('#' + id + ' option').filter(function() {
        return $.trim($(this).text()) == txt;
    }).attr('selected', true);
}

/*
 * Function to check PMS is working
 * 
 * @author : Kamal.Thiayagarajan
 */
function pmsValidation() {
    $.ajax({
        type: "POST",
        url: "index.php?module=gc_Contracts",
        dataType: "json",
        async: false,
        data: {
            "method": "getOfferings",
            'sugar_body_only': 1,
            'action': 'Actions'
        },
        success: function(response) {
        	if (response.respose_xml == "") {
        		//alert(SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_PMS_ERROR'));
        		YAHOO.SUGAR.MessageBox.show({
        			close: false,
                    width: 300,
                    msg: SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_PMS_ERROR'),
                    title: SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_PMS_ERROR_TITLE'),
                    type: 'alert',
                    buttons:[
                             { text: 'OK', handler: handleOK ,isDefault:true},
                    ]
                });
        		
        	} else {
        		return true;
        	}
           
        }
    });
}

var handleOK = function() {
    this.hide();
    window.location.href = 'index.php';
	return false;
};

function addUTCToDate(date,timezone)
{
    // 23.12.2010 | 12.23.2010 | 2010.12.23
    date = date.replace(/\./g, "\/");
    // 23-12-2010 | 12-23-2010 | 2010-12-23
    date = date.replace(/\-/g, "\/");
    
    date = processDateFormat(date);

	var d1 = new Date(date);
	var d2 = new Date (d1);
	if (timezone != "") {
		var arHrMin = timezone.split(':');
	    var Hrs = parseInt(arHrMin[0]) ;
	    var Mins = parseInt(arHrMin[1]);
	    var actualsecs = Math.abs(Hrs) * 3600 + Mins * 60 ;
	    if ( parseInt(Hrs) >= 0 ) {
			actualsecs *= -1;
		}
		d2.setSeconds(d1.getSeconds() + actualsecs);
	}
	var result = (d2.getUTCDate() +'/' + (d2.getUTCMonth()+1) +'/'+ d2.getUTCFullYear() + ' ' + d2.getHours() +':' + d2.getMinutes() +':'+d2.getSeconds());
    //return result;
	return d2;
}

function processDateFormat(date) {
    date_format = SUGAR.expressions.userPrefs.datef;
    if(date_format.slice(0, 1) == 'd') {
        arr_dt = date.split("/");

        var dt = arr_dt[0];
        var mn = arr_dt[1];

        arr_dt[0] = mn;
        arr_dt[1] = dt;
        return arr_dt.join('/');
    }
    else
        return date;
}

function checkLatinCharacter(latinchar) 
{
    var regexpr = /^[\x00-\xFF]+$/;
    if(latinchar.val() != '') {
        if(!latinchar.val().match(regexpr)) {
        	latinchar.after('<div class="error">'+SUGAR.language.get('gc_Contracts', 'LBL_ONLY_LATIN_ERROR')+'</div>');
        	latinchar.css('backgroundColor', "#FF0000");
        	latinchar.animate({backgroundColor: ''}, 2000);
            return true;
        }
    }
    return false;
}

function checkPhoneNumber(m) 
{
    var phoneNo = /^[+]?(\d+[-]?)+\d+$/;
 
    if(m.val() != '') {
        if(!m.val().match(phoneNo)) {
        	m.after('<div class="error">'+SUGAR.language.get('gc_Contracts', 'LBL_TEL_NO_ERROR')+'</div>');
        	m.css('backgroundColor', "#FF0000");
        	m.animate({backgroundColor: ''}, 2000);
        	//setTimeout(function(){ m.css('backgroundColor', ""); }, 2000);
            return true;
        }
    }
    return false;
}

function checkExtensionNumber(e) 
{
    var extNo = /^[\d]{1,5}$/;
 
    if(e.val() != '') {
        if(!e.val().match(extNo)) {
        	e.after('<div class="error">'+SUGAR.language.get('gc_Contracts', 'LBL_EXT_NO_ERROR')+'</div>');
        	e.css('backgroundColor', "#FF0000");
        	e.animate({backgroundColor: ''}, 2000);
            return true;
        }
    }
    return false;
}

function checkFaxNumber(f) 
{
    var faxNo = /^[+]?(\d+[-]?)+\d+$/;
 
    if(f.val() != '') {
        if(!f.val().match(faxNo)) {
        	f.after('<div class="error">'+SUGAR.language.get('gc_Contracts', 'LBL_FAX_NO_ERROR')+'</div>');
        	f.css('backgroundColor', "#FF0000");
        	f.animate({backgroundColor: ''}, 2000);
            return true;
        }
    }
    return false;
}

function salesChannelCode(s)
{
	  var channelCode = /^[0-9a-zA-Z]{8}$/;
	   
    if(s.val() != '') {
        if(!s.val().match(channelCode)) {
        	s.after('<div class="error">'+SUGAR.language.get('gc_Contracts', 'LBL_SALES_CHANNEL_CODE_ERROR')+'</div>');
        	s.css('backgroundColor', "#FF0000");
        	s.animate({backgroundColor: ''}, 2000);
            return true;
        }
    }
    return false;
}

function validateSaleRep()
{
	var error_count = 0;
	
	if(salesChannelCode($('#sales_channel_code'))) {
		error_count = error_count+1;
		$('#sales_channel_code').focus();
    }
	if(checkPhoneNumber($('#tel_no'))) {
		error_count = error_count+1;
		$('#tel_no').focus();
    }
	if(checkExtensionNumber($('#ext_no'))) {
		error_count = error_count+1;
		$('#ext_no').focus();
    }
	if(checkFaxNumber($('#fax_no'))) {
		error_count = error_count+1;
		$('#fax_no').focus();
    }
	if(checkLatinCharacter($('#division_1'))) {
		error_count = error_count+1;
		$('#division_1').focus();
    }
	if(checkLatinCharacter($('#sales_rep_name'))) {
		error_count = error_count+1;
		$('#sales_rep_name').focus();
    }
	return error_count;
}

function disableStartDateTimeZone()
{
	var parent_startdate = $('#div_contract_startdate').closest('td');
	var date_fld_startdate = parent_startdate.find('.date_input');
    if ((parseInt(parent_startdate.find('.datetimecombo_hr').val()) > 0 || parseInt(parent_startdate.find('.datetimecombo_min').val()) >0 || parseInt(parent_startdate.find('.datetimecombo_sec').val()) > 0) ||  (date_fld_startdate.val() != '' && date_fld_startdate.val() != 'undefined')) {
    	$('#gc_timezone_id_c option').prop('disabled', false);
    	if ($("#gc_timezone_id_c").val() == "") {
    		$("#gc_timezone_id_c option[label='UTC (+00:00)']").attr('selected', 'selected');
    	}
    } else {
    	$("#gc_timezone_id_c").val("");
    	markFieldsReadOnly("gc_timezone_id_c");
    }	   
}

function disableEndDateTimeZone()
{
	
    var parent_enddate = $('#div_contract_enddate').closest('td');
	var date_fld_enddate = parent_enddate.find('.date_input');
    if ((parseInt(parent_enddate.find('.datetimecombo_hr').val()) > 0 || parseInt(parent_enddate.find('.datetimecombo_min').val()) >0 || parseInt(parent_enddate.find('.datetimecombo_sec').val()) > 0) ||  (date_fld_enddate.val() != '' && date_fld_enddate.val() != 'undefined')) {
    	$('#gc_timezone_id1_c option').prop('disabled', false);
    	if ($("#gc_timezone_id1_c").val() == "") {
    		$("#gc_timezone_id1_c option[label='UTC (+00:00)']").attr('selected', 'selected');
    	}
    } else {
    	$("#gc_timezone_id1_c").val("");
    	markFieldsReadOnly("gc_timezone_id1_c");
    }
	   
}

function onSelectDateTime()
{
	var id = $(this).attr("id");
	if (id == "contract_startdate") {
		disableStartDateTimeZone();
	} else if (id == "contract_enddate") {
		disableEndDateTimeZone();
	} else {
		 var res = id.split("_");
		 var field_index = res[3]; 
		 var element = id.replace(field_index,"hours_"+field_index);
		 
		 disableLineItemTimeZone(element);
	}
}
// End of file
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++