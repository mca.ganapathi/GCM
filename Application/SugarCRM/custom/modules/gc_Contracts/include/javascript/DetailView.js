/** 
    @desc : Functions related to customize Detail view
    @file  : custom/modules/gc_Contracts/include/javascript/DetailView.js 
    @Author : Arunsakthivel.S
*/
var myWindow;
var myTimeOut;
var mylineitemId;
var mySoLineNo;
var mydocTypFlg;
var EmailAddressMinLength = 3;

function createLineitemDocument(lineitemId, documentId, rowid)
{
    var docTypFlg = ($.trim(lineitemId) == '') ? 1 : 0;
    var w = window.outerWidth/2;
    var h = window.outerHeight;
    if (myWindow && !myWindow.closed && mylineitemId != '') {
        myWindow.close();
        checkWin(mylineitemId,mySoLineNo,mydocTypFlg);
    }
    mylineitemId = lineitemId;
    mySoLineNo = rowid;
    mydocTypFlg = docTypFlg;
    myWindow = window.open("index.php?module=Documents&action=EditView&return_module=Documents&return_action=DetailView&liId="+lineitemId+"&record="+documentId, "_blank", "toolbar=no, scrollbars=yes, resizable=yes, top=0, left=0, menubar=no, status=no, titlebar=no, width="+w+", height="+h+"");
    myTimeOut = setTimeout(function(){ 
        checkWin(lineitemId,rowid,mydocTypFlg); 
    }, 5000);
}

function removeLineitemDocument(lineitemId, documentId, rowid)
{
    if(confirm("Attention! You want to remove the selected document(s) ?")) { 
        SUGAR.ajaxUI.showLoadingPanel();
        $.ajax({
            type: "POST",
            url: "index.php?module=gc_Contracts&action=getLineItemDocs&sugar_body_only=1",
            data: "record="+documentId+"&command=delLiDocs&lineitemId="+lineitemId,
            dataType: "json",
            async: false,
            success: function(result){
                if(result.code == '1' ) {
                    refreshLineitemDocument(lineitemId,rowid);
                } 
            }
        });
        SUGAR.ajaxUI.hideLoadingPanel();
    }
}

function refreshLineitemDocument(lineitemId, rowid)
{
    $.ajax({
        type: "POST",
        url: "index.php?module=gc_Contracts&action=getLineItemDocs&sugar_body_only=1",
        data: "liId="+lineitemId+"&command=getLiDocs",
        dataType: "json",
        async: false,
        success: function(result){
            if(result.code == '1' ) {
                var str_resp = '';
                var rowCss = 'even';
                $("#li_doc_list_items_"+rowid).html('');
                if(result.msg == ''){
                    str_resp = '<tr class="evenListRowS1" height="20"><td scope="row" valign="top" colspan=10 style="text-align:center"> No data</td></tr>';
                }else{
                    $.each(result.msg, function (key, dv) {
                        if(rowCss == 'even') rowCss = 'odd'; else rowCss = 'even';
                        str_resp += '<tr class="'+rowCss+'ListRowS1" height="20">';
                        str_resp += '<td scope="row" valign="top">';
                        str_resp += '<a href="index.php?entryPoint=download&amp;id='+dv.doc_id+'&amp;type=Documents">';
                        str_resp += '<img src="themes/default/images/attachment.gif" alt="attachment" border="0"></a></td>';
                        str_resp += '<td scope="row" valign="top">';
                        str_resp += '<a href="index.php?module=Documents&amp;action=DetailView&amp;record='+dv.doc_id+'">'+dv.doc_name+'</a></td>';
                        str_resp += '<td scope="row" valign="top">';
                        str_resp += '<a href="index.php?entryPoint=download&amp;id='+dv.doc_id+'&amp;type=Documents" class="tabDetailViewDFLink" ';
                        str_resp += 'target="_blank">'+dv.filename+'</a></td>';
                        str_resp += '<td scope="row" valign="top">'+dv.revision+'</td>';
                        str_resp += '<td scope="row" valign="top">'+dv.template_type+'</td>';
                        str_resp += '<td scope="row" valign="top">'+dv.category_id+'</td>';
                        str_resp += '<td scope="row" valign="top" style="padding:3px"><ul class="clickMenu">';
                        str_resp += '<li class="sugar_action_button"><a href="javascript:void(0)" onclick=\'createLineitemDocument( ';
                        str_resp += '"'+lineitemId+'", "'+dv.doc_id +'", "'+ rowid +'")\'>Edit</a></li></ul></td>'; 
                        str_resp += '<td scope="row" valign="top" style="padding:3px"><ul class="clickMenu ">';
                        str_resp += '<li class="sugar_action_button"><a href="javascript:void(0)" onclick=\'removeLineitemDocument(';
                        str_resp += '"'+lineitemId+'", "'+ dv.doc_id +'", "'+ rowid +'")\'>Remove</a></li></ul></td> </tr>';
                    });
                }
                $("#li_doc_list_items_"+rowid).html(str_resp);
            } 
        }
    });
}


function createContractDocument(conId, documentId)
{
    var docTypFlg = ($.trim(conId) == '') ? 1 : 0;
    var w = window.outerWidth/2;
    var h = window.outerHeight;
    if (myWindow && !myWindow.closed && mylineitemId != '') {
        myWindow.close();
        checkWin(mylineitemId,mySoLineNo);
    }
    mylineitemId = conId;
    mySoLineNo = '';
    
    myWindow = window.open("index.php?module=Documents&action=EditView&return_module=Documents&return_action=DetailView&CnId="+conId+"&record="+documentId, "_blank", "toolbar=no, scrollbars=yes, resizable=yes, top=0, left=0, menubar=no, status=no, titlebar=no, width="+w+", height="+h+"");
    myTimeOut = setTimeout(function(){ 
        checkWin(conId,''); 
    }, 5000);
}

function removeContractDocument(conId, documentId)
{
    if(confirm("Attention! You want to remove the selected document(s) ?")) { 
        SUGAR.ajaxUI.showLoadingPanel();
        $.ajax({
            type: "POST",
            url: "index.php?module=gc_Contracts&action=getContractDocs&sugar_body_only=1",
            data: "record="+documentId+"&command=delCnDocs&conId="+conId,
            dataType: "json",
            async: false,
            success: function(result){
                if(result.code == '1' ) {
                    refreshContractDocument(conId);
                } 
            }
        });
        SUGAR.ajaxUI.hideLoadingPanel();
    }
}

function refreshContractDocument(conId)
{
    $.ajax({
        type: "POST",
        url: "index.php?module=gc_Contracts&action=getContractDocs&sugar_body_only=1",
        data: "CnId="+conId+"&command=getCnDocs",
        dataType: "json",
        async: false,
        success: function(result){
            if(result.code == '1' ) {
                var str_resp = '';
                var rowCss = 'even';
                $("#contract_doc_list_items").html('');
                if(result.msg == ''){
                    str_resp = '<tr class="evenListRowS1" height="20"><td scope="row" valign="top" colspan=10 style="text-align:center"> No data</td></tr>';
                }else{
                    $.each(result.msg, function (key, dv) {
                        if(rowCss == 'even') rowCss = 'odd'; else rowCss = 'even';
                        str_resp += '<tr class="'+rowCss+'ListRowS1" height="20">';
                        str_resp += '<td scope="row" valign="top">';
                        str_resp += '<a href="index.php?entryPoint=download&amp;id='+dv.doc_id+'&amp;type=Documents">';
                        str_resp += '<img src="themes/default/images/attachment.gif" alt="attachment" border="0"></a></td>';
                        str_resp += '<td scope="row" valign="top">';
                        str_resp += '<a href="index.php?module=Documents&amp;action=DetailView&amp;record='+dv.doc_id+'">'+dv.doc_name+'</a></td>';
                        str_resp += '<td scope="row" valign="top">';
                        str_resp += '<a href="index.php?entryPoint=download&amp;id='+dv.doc_id+'&amp;type=Documents" class="tabDetailViewDFLink" ';
                        str_resp += 'target="_blank">'+dv.filename+'</a></td>';
                        str_resp += '<td scope="row" valign="top">'+dv.revision+'</td>';
                        str_resp += '<td scope="row" valign="top">'+dv.template_type+'</td>';
                        str_resp += '<td scope="row" valign="top">'+dv.category_id+'</td>';
                        str_resp += '<td scope="row" valign="top" style="padding:3px"><ul class="clickMenu">';
                        str_resp += '<li class="sugar_action_button"><a href="javascript:void(0)" onclick=\'createContractDocument( ';
                        str_resp += '"'+conId+'", "'+dv.doc_id +'")\'>Edit</a></li></ul></td>'; 
                        str_resp += '<td scope="row" valign="top" style="padding:3px"><ul class="clickMenu ">';
                        str_resp += '<li class="sugar_action_button"><a href="javascript:void(0)" onclick=\'removeContractDocument(';
                        str_resp += '"'+conId+'", "'+ dv.doc_id +'")\'>Remove</a></li></ul></td> </tr>';
                    });
                }
                $("#contract_doc_list_items").html(str_resp);
            } 
        }
    });
}

function checkWin(lineitemId,rowid)
{
    if (myWindow.closed) { 
        clearTimeout(myTimeOut);
        SUGAR.ajaxUI.showLoadingPanel();
        if(rowid == '') {
            refreshContractDocument(lineitemId);
        }else{
            refreshLineitemDocument(lineitemId,rowid);
        }
        SUGAR.ajaxUI.hideLoadingPanel();
    } else {
        myTimeOut = setTimeout(function(){ 
        checkWin(lineitemId,rowid); 
    }, 1000);
  }
}

/* 
    End : Functions related to customize the Document sub panel loader
*/

/**
 * Function to init DOM objects on JQuery Document Load
 */
$( document ).ready(function() {
	pmsValidation();
	enableButton();
    var contract_status = parseInt($("#contract_status").val());
	if (parseInt($("#contract_version").html()) > 1 && (contract_status == 0 || contract_status == 1 || contract_status == 3)) {
		if ($("#team_difference").val() != "") {
			var team_diff = $("#team_difference").val();
			var teams = $('table[id^=DEFAULT] tr:nth(4) td:nth-child(4)').html();
			var teams_array = teams.split(",")
			var diff_array = team_diff.split(","); 
			var cleanArry = new Array();
			$.each(teams_array, function (idx, val) {
				cleanArry.push($.trim(this));
			});
			var team = "";
			for (var i=0; i<(cleanArry.length); i++) {
				if(team != "") {
					team += ", "; 
				}
				if (diff_array.indexOf(cleanArry[i]) >= 0) {
					team += "<span class='upgrade-change'>"+cleanArry[i]+"</span>";
					 
				} else {
					team += cleanArry[i];
				}
			}
			
			$('table[id^=DEFAULT] tr:nth(4) td:nth-child(4)').html(team);
			
		}	
		$("#id").addClass("upgrade-change");
		$("#contract_version").addClass("upgrade-change");
		$("#contract_status").parent("td").addClass("upgrade-change");
		if ($("#description_changed").val() == "1") $("#description").addClass("upgrade-change");
		if ($("#loi_changed").val() == "1") $("#loi_contract").parent("td").addClass("upgrade-change");
		if ($("#contract_type_changed").val() == "1") $("#contract_type").parent("td").addClass("upgrade-change");
		if ($("#contract_scheme_changed").val() == "1") $("#contract_scheme").parent("td").addClass("upgrade-change");
		if ($("#billing_type_changed").val() == "1") $("#billing_type").parent("td").addClass("upgrade-change");
		if ($("#contract_startdate_timezone_changed").val() == "1") $("#contract_startdate_timezone").addClass("upgrade-change");
		if ($("#contract_enddate_timezone_changed").val() == "1") $("#contract_enddate_timezone").addClass("upgrade-change");
		if (contract_status != 3 && $("#contract_workflow_flag_changed").val() == "1") $("#workflow_flag").parent("td").addClass("upgrade-change");
		$(".discount-change").parent("span").addClass("upgrade-change");	
	}
	document.getElementById('sales_rep_save').onclick = function() {
		return validateSalesRepForm();
	};
});


/**
 * @desc : Function to enable the contract approval buttons based on logged in user role
 * @params : 
 * @return : 
 * @author : Shrikant.Gaware
 * @date : 19-Feb-2016
 */
function enableButton()
{
	var display = $('#displaybuttonflag');
	var display1 = $('#displaybuttonflag1');
	var record = $('[name="record"]');
	var div = '';
	var sa_button = '';
	var a_button = '';
	var r_button = '';
	var cmt = '';
	if(display1.val() == 1 && record.val() != '')
		sa_button = '<input type="submit" name="sendforapproval" id="sendforapproval" class="clickMenu" '+
							'onclick="performAction(\'SendForApproval\')" value="'+SUGAR.language.get('gc_Contracts', 'LBL_SENDFORAPPROVAL')+'">';
	if(display.val() == 1 && record.val() != '')
	{
		a_button = '<input type="submit" name="approvecontract" id="approvecontract" class="clickMenu" '+
							'onclick="performAction(\'ApproveContract\')" value="'+SUGAR.language.get('gc_Contracts', 'LBL_APPROVE')+'">';
		r_button = '<input type="submit" name="rejectContract" id="rejectContract" class="clickMenu" '+
							'onclick="performAction(\'RejectContract\')" value="'+SUGAR.language.get('gc_Contracts', 'LBL_REJECT')+'">';					
		cmt = '&nbsp;&nbsp;&nbsp;&nbsp;<input type ="text" name="comment" id="comment" class="comment">';					
	}
	var div = sa_button+a_button+r_button+cmt;
	if(div != ''){
		$(div).insertBefore( $( '.action_buttons' ).find('.clear') );
	}
}

/**
 * @desc : Function to excuted supplied argumented Action parameter
 * @params : form <object> DOM obejct of the form
 *			 id <string> contract record id
 *			 action <string> controller action
 * @return : <bool> true/false form submit
 * @author : Shrikant.Gaware
 * @date : 19-Feb-2016
 */
 function performAction(action) {
	 var form = document.getElementById('formDetailView');
	 var id = $('[name="record"]');
	 form.return_module.value='gc_Contracts'; 
	 form.return_action.value='DetailView';
	 form.return_id.value=id; 
	 form.action.value=action;
	 if(action == 'RejectContract'){
		 
	 }
	 form.submit();
 }
 
 /**
 * : Function to open Contract Comments Log
 * 
 * @param :
 *            <string> module_name - Sugar Module Name <string> width - width
 *            for the popup window <string> height - height for the popup window
 * 
 * @return : <object> win Pop-up window object
 * @author : Dinesh.Itkar
 * @date : 22-Feb-2016
 */
function commentsLog(module_name, width, height, contract_id) {
	if (typeof (popupCount) == "undefined" || popupCount == 0)
		popupCount = 1;
	window.document.close_popup = true;
	width = (width == 600) ? 900 : width;
	height = (height == 400) ? 900 : height;
	URL = 'index.php?' + 'module=' + module_name
			+ '&action=Popup_CommentsLog&sugar_body_only=true&contract_id=' + contract_id;
	windowName = module_name + '_popup_window' + popupCount;
	windowFeatures = 'width=' + width + ',height=' + height
			+ ',resizable=1,scrollbars=1';
	popup_mode = 'single';
	URL += '&mode=' + popup_mode;
	win = SUGAR.util.openWindow(URL, windowName, windowFeatures);
	if (window.focus) {
		win.focus();
	}
	win.popupCount = popupCount;
	return win;
}

/**
 * @desc : Function to check message box is empty or not and if empty then show error msg
 * @params : 
 * @return : 
 * @author : Debasish.Gupta
 * @date : 29-August-2016
 */

function checkMsgEmptyOrNot() {
	msg = $.trim($("#sugar-message-prompt").val());
	if(msg != "")
		$("#errorMsgSpan").html('');
	else
		$("#errorMsgSpan").html(SUGAR.language.get('gc_Contracts', 'LBL_COMMENT_MUST_FILLED'));
}

/**
 * @desc : Function to excuted supplied argumented Action parameter
 * @params : action <string> controller action
 * @return : <bool> true/false form submit
 * @author : Shrikant.Gaware
 * @date : 19-Feb-2016
 */
 function performAction(action) {
	 var flag = 0;
	 YAHOO.SUGAR.MessageBox.promptTemplate = "{body}:<textarea id='sugar-message-prompt' class='sugar-message-prompt' name='sugar-message-prompt' rows='10' style='resize:vertical; max-height:400px; min-height:200px; width:95%;'"; 	 
	 if(action == 'RejectContract' || action == 'RejectTerminationApproval')	 
		 YAHOO.SUGAR.MessageBox.promptTemplate += " onkeyup='checkMsgEmptyOrNot();' ";	 
	 YAHOO.SUGAR.MessageBox.promptTemplate += "></textarea><span id='errorMsgSpan' style='color:red;'></span>";
	 YAHOO.SUGAR.MessageBox.show(
	 {
		msg: SUGAR.language.get('gc_Contracts', 'LBL_COMMENTS'), 
		title: SUGAR.language.get('gc_Contracts', 'LBL_COMMENTS_TITLE'),
		width: 300,
		type: 'prompt', 
		fn: function(cmnt) {
			if($.trim(cmnt) != '')
			{
				YAHOO.SUGAR.MessageBox.hide();
				flag = 1;
			}
			else
			{	
				if(action == 'RejectContract' || action == 'RejectTerminationApproval')
				{
					$("#errorMsgSpan").html(SUGAR.language.get('gc_Contracts', 'LBL_COMMENT_MUST_FILLED'));
					this.show();
					flag = 0;
				}	
				else
				{
					flag = 1;
				}
			}
			if (flag)
			{
				 submitDetailViewForm(action,cmnt);
			}
		}      
	});
 }
 
 /**
 * @desc : Function to submit detail view form
 * @params : action <string> controller action 
 * 			 cmnt <string> comment entered by user
 * @return :
 * @author : Shrikant.Gaware
 * @date : 23-Feb-2016
 */
 function submitDetailViewForm(action,cmnt) {
	 SUGAR.ajaxUI.showLoadingPanel();
	 var form = document.getElementById('formDetailView');
	 var id = $('[name="record"]');
	 form.return_module.value='gc_Contracts'; 
	 form.return_action.value='DetailView';
	 form.return_id.value=id; 
	 form.action.value=action;
	 form.comment.value=cmnt;
	 form.submit();
 }
 
 /**
  * : Function to open Contract Custom Audit Log
  * 
  * @param :
  *            <string> module_name - Sugar Module Name <string> width - width
  *            for the popup window <string> height - height for the popup window
  * 
  * @return : <object> win Pop-up window object
  * @author : Dinesh.Itkar
  * @date : 20-Apr-2016
  */
 function auditLog(module_name, width, height, contract_id) {
 	if (typeof (popupAuditCount) == "undefined" || popupAuditCount == 0)
 		popupAuditCount = 1;
 	window.document.close_popup = true;
 	width = (width == 600) ? 900 : width;
 	height = (height == 400) ? 900 : height;
 	URL = 'index.php?' + 'module=Audit' + '&module_name=' + module_name
 			+ '&action=Custom_Popup_Picker&query=true&record=' + contract_id + '&create=false&sugar_body_only=true';
 	windowName = module_name + '_popup_window' + popupAuditCount;
 	windowFeatures = 'width=' + width + ',height=' + height
 			+ ',resizable=1,scrollbars=1';
 	popup_mode = 'single';
 	URL += '&mode=' + popup_mode;
 	win = SUGAR.util.openWindow(URL, windowName, windowFeatures);
 	if (window.focus) {
 		win.focus();
 	}
 	win.popupAuditCount = popupAuditCount;
 	return win;
 }
 
 /**
  * @desc : Function to submit activate action
  * @params : action <string> controller action
  * @return : <bool> true/false form submit
  * @author : Kamal.Thiyagarajan
  * @date : 20-May-2016
  */
  function performActivation(action) {
	  YAHOO.SUGAR.MessageBox.show({
          msg: SUGAR.language.get('gc_Contracts','LBL_CLICK_ACTIVATION'),
          title: 'Attention!',
          type: 'confirm',
          fn: function(confirm) {
              if (confirm == 'yes') {
            	  submitDetailViewForm(action, "");   
              } else {
                  return false;
              }
          }      
      });  
  }
  
  /**
   * @desc : Function to submit deactivate action
   * @params : action <string> controller action
   * @return : <bool> true/false form submit
   * @author : Rajul.Mondal
   * @date : 12-July-2016
   */
   function performDeactivation(action) {
 	  YAHOO.SUGAR.MessageBox.show({
           msg: SUGAR.language.get('gc_Contracts','LBL_CLICK_DEACTIVATION'),
           title: 'Attention!',
           type: 'confirm',
           fn: function(confirm) {
               if (confirm == 'yes') {
             	  submitDetailViewForm(action, "");   
               } else {
                   return false;
               }
           }      
       });  
   }

  /**
  * @desc : Function to redirect to upgrade contract creation page
  * @params : id <string> of contract
  * @author : Mohamed.Siddiq
  * @date : 05-Jul-2016
  */
  function performModification(contract_id){
    window.location.href = 'index.php?module=gc_Contracts&action=CheckModificationAvailability&contract_id='+contract_id;
    //var URL = 'index.php?module=gc_Contracts&action=EditView&VerUp=1&record='+contract_id+'';
    //window.location.href=URL;
  }
  
  /**
   * @desc : Function to Show edit sales rep
   * @params : 
   * @author : Kamal.Thiyagarajan
   * @date : 02-Nov-2016
   */
  function editSalesRep()
  { 
  	  $(".sales_rep_text").hide();
  	  $(".sales_rep_edit").show()
  	  $("#btn_edit").hide();
  }
  
  /**
   * @desc : Function to clear sales rep form
   * @params : 
   * @author : Kamal.Thiyagarajan
   * @date : 02-Nov-2016
   */
  function cancelSalesRep()
  {
	  document.getElementById("sales_rep_form").reset();
	  $(".validation-message").remove();
	  $('.error').remove();
	  $('.sales_rep_edit').hide();
	  if ($("#sales_rep_id").val() == "") {
		  $("#create_sales_rep").hide();
		  $("#btn_create").show();
		  $("#no_record_found").show();
	  } else {
		  $('.sales_rep_text').show();
		  $("#btn_edit").show();  
	  }
	  
  }
  
  /**
   * @desc : Function to validate sales rep form
   * @params : 
   * @author : Kamal.Thiyagarajan
   * @date : 02-Nov-2016
   */
  function validateSalesRepForm()
  {
	  var _form = document.getElementById('sales_rep_form');
      var error_count = validateSaleRep();
	  if (check_form('sales_rep_form') && error_count == 0) {
		  SUGAR.ajaxUI.submitForm(_form);
	  }
	  return false;
  }
  
  /**
   * @desc : Function to create sales rep
   * @params : 
   * @author : Kamal.Thiyagarajan
   * @date : 02-Nov-2016
   */
  function createSalesRep()
  {
	  $("#create_sales_rep").show();
	  $(".sales_rep_edit").show();
	  $("#btn_create").hide();
	  $("#no_record_found").hide();
  }
  
  /*
   * Function to check PMS is working
   * 
   * @author : Kamal.Thiayagarajan
   */
  function pmsValidation() {
      $.ajax({
          type: "POST",
          url: "index.php?module=gc_Contracts",
          dataType: "json",
          async: true,
          data: {
              "method": "getOfferings",
              'sugar_body_only': 1,
              'action': 'Actions'
          },
          success: function(response) {
          	if (response.respose_xml == "") {
          		//alert(SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_PMS_ERROR'));
          		YAHOO.SUGAR.MessageBox.show({
          			close: false,
                      width: 300,
                      msg: SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_PMS_ERROR'),
                      title: SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_PMS_ERROR_TITLE'),
                      type: 'alert',
                      buttons:[
                               { text: 'OK', handler: handleOK ,isDefault:true},
                      ]
                  });
          		
          	} else {
          		return true;
          	}
             
          }
      });
  }

  var handleOK = function() {
      this.hide();
      window.location.href = 'index.php';
  	return false;
  };
  
  
  function checkLatinCharacter(latinchar) 
  {
      var regexpr = /^[\x00-\xFF]+$/;
      if(latinchar.val() != '') {
          if(!latinchar.val().match(regexpr)) {
          	latinchar.after('<div class="error">'+SUGAR.language.get('gc_Contracts', 'LBL_ONLY_LATIN_ERROR')+'</div>');
          	latinchar.css('backgroundColor', "#FF0000");
          	latinchar.animate({backgroundColor: ''}, 2000);
              return true;
          }
      }
      return false;
  }

  function checkPhoneNumber(m) 
  {
      var phoneNo = /^[+]?(\d+[-]?)+\d+$/;
   
      if(m.val() != '') {
          if(!m.val().match(phoneNo)) {
          	m.after('<div class="error">'+SUGAR.language.get('gc_Contracts', 'LBL_TEL_NO_ERROR')+'</div>');
          	m.css('backgroundColor', "#FF0000");
          	m.animate({backgroundColor: ''}, 2000);
          	//setTimeout(function(){ m.css('backgroundColor', ""); }, 2000);
              return true;
          }
      }
      return false;
  }

  function checkExtensionNumber(e) 
  {
      var extNo = /^[\d]{1,5}$/;
   
      if(e.val() != '') {
          if(!e.val().match(extNo)) {
          	e.after('<div class="error">'+SUGAR.language.get('gc_Contracts', 'LBL_EXT_NO_ERROR')+'</div>');
          	e.css('backgroundColor', "#FF0000");
          	e.animate({backgroundColor: ''}, 2000);
              return true;
          }
      }
      return false;
  }

  function checkFaxNumber(f) 
  {
      var faxNo = /^[+]?(\d+[-]?)+\d+$/;
   
      if(f.val() != '') {
          if(!f.val().match(faxNo)) {
          	f.after('<div class="error">'+SUGAR.language.get('gc_Contracts', 'LBL_FAX_NO_ERROR')+'</div>');
          	f.css('backgroundColor', "#FF0000");
          	f.animate({backgroundColor: ''}, 2000);
              return true;
          }
      }
      return false;
  }

  function salesChannelCode(s)
  {
  	  var channelCode = /^[0-9a-zA-Z]{8}$/;
  	   
      if(s.val() != '') {
          if(!s.val().match(channelCode)) {
          	s.after('<div class="error">'+SUGAR.language.get('gc_Contracts', 'LBL_SALES_CHANNEL_CODE_ERROR')+'</div>');
          	s.css('backgroundColor', "#FF0000");
          	s.animate({backgroundColor: ''}, 2000);
              return true;
          }
      }
      return false;
  }
  
  function checkValidEmail(e) 
{
	var email_address = e.val();	
	if (email_address != "") {
		if(email_address.trim().length < EmailAddressMinLength) 
        {
            e.after('<div class="error">Invalid Value: Email address</div>');
            e.css('backgroundColor', "#FF0000");
            e.animate({backgroundColor: ''}, 2000);
            return false;
        }
    }
	return true;
}

  function validateSaleRep()
  {
  	var error_count = 0;
  	$('.error').remove();
  	if(salesChannelCode($('#sales_channel_code'))) {
  		error_count = error_count+1;
  		$('#sales_channel_code').focus();
      }
  	if(checkPhoneNumber($('#tel_no'))) {
  		error_count = error_count+1;
  		$('#tel_no').focus();
      }
  	if(checkExtensionNumber($('#ext_no'))) {
  		error_count = error_count+1;
  		$('#ext_no').focus();
      }
  	if(checkFaxNumber($('#fax_no'))) {
  		error_count = error_count+1;
  		$('#fax_no').focus();
      }
  	if(checkLatinCharacter($('#division_1'))) {
  		error_count = error_count+1;
  		$('#division_1').focus();
      }
  	if(checkLatinCharacter($('#sales_rep_name'))) {
  		error_count = error_count+1;
  		$('#sales_rep_name').focus();
      }
      if(!checkValidEmail($('#email1'))){
          error_count = error_count+1;
          $('#email1').focus();
      }
  	
  	return error_count;
  }