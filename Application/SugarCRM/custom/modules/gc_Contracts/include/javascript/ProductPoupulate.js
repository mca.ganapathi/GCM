/**
 * @desc : Class file for the all the functionalities related to product
 *       populate using Json data from PMS system ( DF 38)
 * @author : Arunsakthivel
 * @date : 09-03-2016
 */
product_populate = new function() {
    var spec_details = "";
    var offer_details = "";
    var edit_details = "";
    /**
     * @desc : Function to intialize the json contents
     * @author : Arunsakthivel
     */
    this.product_intialize = function() {
        // Get the json details and store it
        this.offer_details = "";
        this.spec_details = "";
        this.edit_details = "";
        $(".product_spec_subpanels").hide();
    };
    /**
     * @desc : Function to receive the specication details for the selected the
     *       offering id
     * @author : Arunsakthivel
     */
    this.product_spec_getDetails = function() {
        // console.log(('product_spec_getDetails ==> ');
        offering_id = $("#product_offering").val();
        if (offering_id > 0) {
            SUGAR.ajaxUI.showLoadingPanel();
            $.ajax({
                type: "POST",
                url: "index.php?module=gc_Contracts",
                dataType: "json",
                async: false,
                data: {
                    "method": "getOfferingDetails",
                    "off_id": offering_id,
                    'sugar_body_only': 1,
                    'action': 'Actions'
                },
                success: function(result) {
                    if (result) {
                        if (result.response_code == 1) {
                            product_populate.offer_details = result.respose_array;
                            product_populate.spec_details = product_populate.offer_details.specification_details;
                            product_populate.edit_details = result.respose_array;
                        } else {
                            YAHOO.SUGAR.MessageBox.show({
                                msg: SUGAR.language.get('gc_Contracts', 'LBL_PMS_CONNECTION_ERROR'),
                                title: 'Error',
                                type: 'alert',
                                fn: function(confirm) {
                                    window.location.reload();
                                }
                            });
                            window.setTimeout('window.location.reload();', 10000);
                        }
                    }
                }
            });
            SUGAR.ajaxUI.hideLoadingPanel();
        } else {
            this.product_intialize();
        }
    };
    /**
     * @desc : Function to receive the line item details related with the PMS
     *       details used in edit view
     * @author : Arunsakthivel
     */
    this.product_spec_getEditDetails = function() {
        // console.log(('product_spec_getEditDetails ==> ');
        offering_id = $("#product_offering").val();
        if (offering_id > 0 && record != "") {
            SUGAR.ajaxUI.showLoadingPanel();
            $.ajax({
                type: "POST",
                url: "index.php?module=gc_Contracts",
                dataType: "json",
                async: false,
                data: {
                    "method": "getEditviewDetails",
                    "off_id": offering_id,
                    "c_b_id": record,
                    'sugar_body_only': 1,
                    'action': 'Actions'
                },
                success: function(result) {
                    if (result.response_code == 1) {
                        product_populate.edit_details = result.respose_array;
                    }
                }
            });
            SUGAR.ajaxUI.hideLoadingPanel();
        } else {
            this.product_intialize();
        }
    };
    /**
     * @desc : Function to populate the product specication with selected
     *       product offering
     * @author : Arunsakthivel
     */
    this.product_spec_populate = function() {
        // console.log('product_spec_populate ==> ');
        SUGAR.ajaxUI.showLoadingPanel();
        var base_options = "";
        var is_admin_flag = $('#is_admin_flag').val();
        // Loop include to take all the line item specifications
        var lineItemRows = parseInt(parentNode.find('.LineItem').length);
        for (var rowid = 1; rowid <= lineItemRows; rowid++) {
            if (product_populate.spec_details.length == 0) {
                $("#div_product_spec_" + rowid).html('');
                $("#product_spec_subpanels_" + rowid).hide();
            } else {
                var spec_id = '-';
                if ($("#li_deleted_" + rowid).val() != '1') {
                    if (editInit == true) {
                        var li_id = $("#line_item_id_" + rowid).val();
                        spec_id = product_populate.edit_details[li_id]['product_specification_id'];
                    }
                    $("#product_spec_subpanels_" + rowid).hide();
                    var ele = "<select name='product_specification_" + rowid + "' class='req product_specification' id='product_specification_" + rowid + "' onchange='product_spec_change(this)' style='width:100%' data ='enum'>";
                    if ($("#approver_user").val() != "" && $("#contract_status").val() == "1") ele += "<option value='' disabled=''></option>";
                    else if ($("#contract_status").val() == "6" && is_admin_flag == '') ele += "<option value='' disabled=''></option>";
                    else ele += "<option value=''></option>";
                    $.each(product_populate.spec_details, function(speckey, spec_detail_row) {
                        ele += "<option value='" + speckey + "' ";
                        if (spec_id == speckey) {
                            ele += " selected='true' ";
                        } else if ($("#approver_user").val() != "" && $("#contract_status").val() == "1") {
                            ele += " disabled='' ";
                        } else if ($("#contract_status").val() == "6" && is_admin_flag == '') {
                            ele += " disabled='' ";
                        }
                        ele += " >" + spec_detail_row.specname + " (" + spec_detail_row.spectypename + ")</option>";
                        if (spec_detail_row.spectype_mandatory == "Yes") {
                            base_options = (base_options == "") ? speckey : ',' + speckey;
                        }
                    });
                    ele += '</select>';
                    $("#div_product_spec_" + rowid).html(ele);
                    if (editInit == true) {
                        $("#product_spec_subpanels_" + rowid).show();
                        product_populate.product_config_populate(rowid, spec_id);
                    }
                }
            }
        }
        $("#product_specification_base").val(base_options);
        SUGAR.ajaxUI.hideLoadingPanel();
        editInit = false;
        // show all save btn in edit view when page load completed (Hide Code -> LineItemUtils.js:25)
        $('#SAVE_HEADER,#save_and_continue,#SAVE_FOOTER').show();
    };
    /**
     * @desc : Function to populate the product specication with selected
     *       product offering for the single row
     * @author : Arunsakthivel
     */
    this.product_spec_populate_single_row = function(rowid) {
        // console.log(('product_spec_populate_single_row ==> rowid ' + rowid );
        SUGAR.ajaxUI.showLoadingPanel();
        $("#product_spec_subpanels_" + rowid).hide();
        if (product_populate.spec_details.length == 0) {
            $("#product_spec_subpanels_" + rowid).hide();
        } else {
            var options = "<option value=''></option>";
            $.each(product_populate.spec_details, function(speckey, spec_detail_row) {
                options += "<option value='" + speckey + "'>" + spec_detail_row.specname + " (" + spec_detail_row.spectypename + ")</option>";
            });
            var ele = "<select name='product_specification_" + rowid + "' class='req ' id='product_specification_" + rowid + "' onchange='product_spec_change(this)' style='width:100%' data ='enum'>";
            ele += options;
            ele += '</select>';
            $("#div_product_spec_" + rowid).html(ele);
        }
        SUGAR.ajaxUI.hideLoadingPanel();
    };
    /**
     * @desc : Function to populate the product configuraion with selected
     *       product specication
     * @author : Arunsakthivel
     */
    this.product_config_populate = function(rowid, spec_id) {
        var is_admin_flag = $('#is_admin_flag').val();
        // console.log(('product_config_populate ==> rowid ' + rowid + ' spec_id
        // '+ spec_id);
        if (spec_id == '' || rowid < 0) {
            return false;
        }
        SUGAR.ajaxUI.showLoadingPanel();
        var configuration = product_populate.spec_details[spec_id].characteristics;
        if (editInit == true) {
            var li_id = $("#line_item_id_" + rowid).val();
            config_edit_details = product_populate.edit_details[li_id]['product_config'];
        }
        $("#div_product_config_" + rowid).html('');
        if (configuration != undefined) {
        if (Object.keys(configuration).length > 0 && Object.keys(configuration)[0] != '') {
            var ele_str = '<div class="row margin-0">';
            var row_inc = 1;
            $.each(configuration, function(ch_key, characteristic) {
                var characteristicvalue = characteristic.characteristicvalue;
                var record_id = '';
                var characteristic_value = '';
                if (editInit == true && config_edit_details.hasOwnProperty(ch_key)) {
                    record_id = config_edit_details[ch_key]['id'];
                    characteristic_value = config_edit_details[ch_key]['characteristic_value'];
                }
                if (row_inc > 2) {
                    row_inc = 1;
                    ele_str += '</div><div class="row margin-0">';
                }
                ele_str += '<div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">';
                ele_str += '<div class="cell" >';
                ele_str += '<span class="cellfont" id="label_product_config_' + rowid + '_' + ch_key + '">' + characteristic.charname + '';
                if (characteristic.ismandatory == 'true') {
                    ele_str += '<font color="red">*</font>';
                }
                ele_str += '</span>';
                ele_str += '</div></div><div class="col-md-3 add-cell-border-right add-cell-border-top">';
                ele_str += '<div class="cell">';
                ele_str += '<span class="cellfont">';
                ele_str += '<input type="hidden" name="pc_characteristic_name_' + rowid + '[]" value="' + ch_key + '" >';
                ele_str += '<input type="hidden" name="pc_characteristic_record_id_' + rowid + '[]" value="' + record_id + '" class="pc_characteristic_record_id">';
                ele_str += '<input type="hidden" name="pc_characteristic_record_parent_id_' + rowid + '[]" value="' + record_id + '">';
                
                if(characteristic.valtypeid == 2){
                    var lv = parseInt(characteristic.lowervalue);
                    if(isNaN(lv)){
                    	lv = 0;
                    }
                    var uv = parseInt(characteristic.uppervalue);
                    var inc = parseInt(characteristic.interval);
                    ele_str += '<input type="input" value="'+characteristic_value+'" maxlength="100" name="pc_characteristic_value_' + rowid + '[]" id="product_config_' + rowid + '_' + ch_key + '" class="';
                    if (characteristic.ismandatory == 'true') {
	                    ele_str += ' req ';
	                }
	                ele_str += '  product_configs " data-valtypeid="' + characteristic.valtypeid + '" data-charvalid="' + characteristic.charvalid + '" data-lv="' + lv + '" data-uv="' + uv + '" data-inc="' + inc + '"';
	                ele_str += ' style="width:100%" onblur="product_config_validate(this);" onchange="product_config_change(this.id, false);"/><br/>';
	                ele_str += '<span class="helptxt10px">(Input Range: Minimum='+lv+', Maximum='+uv+' & Interval='+inc+')</span><br/>';
	                ele_str += '<span id="error_product_config_' + rowid + '_' + ch_key + '" class="error"></span>';
                }else{
	                ele_str += '<select name="pc_characteristic_value_' + rowid + '[]" id="product_config_' + rowid + '_' + ch_key + '" class="';
	                if (characteristic.ismandatory == 'true') {
	                    ele_str += ' req ';
	                }
	                ele_str += '  product_configs " data-valtypeid="' + characteristic.valtypeid + '" data-charvalid="' + characteristic.charvalid + '" ';
	                ele_str += ' style="width:100%" onchange="product_config_change(this.id, false)">';
	                if ($("#approver_user").val() != "" && $("#contract_status").val() == "1") ele_str += '<option value="" disabled=""></option>';
	                else if ($("#contract_status").val() == "6" && is_admin_flag == '') ele_str += "<option value='' disabled=''></option>";
	                else ele_str += '<option value=""></option>';
	               /* if (characteristic.valtypeid == 2) {
	                    var lv = parseInt(characteristic.lowervalue);
	                    var uv = parseInt(characteristic.uppervalue);
	                    var inc = parseInt(characteristic.interval);
	                    for (i = lv; i <= uv; i = parseInt(i + inc)) {
	                        sel_str = '';
	                        if (characteristic_value == i) {
	                            sel_str += ' selected="selected" ';
	                        } else if ($("#approver_user").val() != "" && $("#contract_status").val() == "1") {
	                            sel_str += " disabled='' ";
	                        } else if ($("#contract_status").val() == "6" && is_admin_flag == '') {
	                            sel_str += " disabled='' ";
	                        }
	                        ele_str += '<option value="' + i + '" ' + sel_str + '>' + i + '</option>';
	                    }
	                } else {*/
                    if (Object.keys(characteristicvalue).length > 0) {
                        $.each(characteristicvalue, function(k, cv) {
                            sel_str = '';
                            if (editInit == true) {
                                if (characteristic_value == cv.id) {
                                    sel_str += ' selected="selected" ';
                                } else if ($("#approver_user").val() != "" && $("#contract_status").val() == "1") {
                                    sel_str += " disabled='' ";
                                } else if ($("#contract_status").val() == "6" && is_admin_flag == '') {
                                    sel_str += " disabled='' ";
                                }
                            } else {
                                if (cv.isdefault == 'true') {
                                    sel_str += ' selected="selected" ';
                                } else if ($("#approver_user").val() != "" && $("#contract_status").val() == "1") {
                                    sel_str += " disabled='' ";
                                } else if ($("#contract_status").val() == "6" && is_admin_flag == '') {
                                    sel_str += " disabled='' ";
                                }
                            }
                            ele_str += '<option value="' + cv.id + '" ' + sel_str + '>' + cv.displaytext + '</option>';
                        });
                    }
	                //}
	                ele_str += '</select>';
                }	
                ele_str += '</span></div></div>';
                row_inc++;
            });
            if (row_inc == 2) {
                ele_str += '<div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top"></div>';
                ele_str += '<div class="col-md-3 add-cell-border-right add-cell-border-top"></div>';
            }
        } else {
            var ele_str = '<div class="row margin-0"><div class="col-md-12 add-cell-border-right add-cell-border-left add-cell-border-bottom center"><div class="cell" ><span class="cellfont">No records found</span></div></div></div>';
        }
        } else {
            var ele_str = '<div class="row margin-0"><div class="col-md-12 add-cell-border-right add-cell-border-left add-cell-border-bottom center"><div class="cell" ><span class="cellfont">No records found</span></div></div></div>';
        }
        $("#div_product_config_" + rowid).html(ele_str);
        SUGAR.ajaxUI.hideLoadingPanel();
        // Initiate the Rules part display
        product_populate.product_info_populate(rowid, spec_id);
        // Initiate price display part by invoke on change handler function
        product_config_change("product_config_" + rowid + "_1", editInit);
        disable_enter_form_submit();
    };
    /**
     * @desc : Function to populate the product information details with
     *       selected product specication
     * @author : Arunsakthivel
     */
    this.product_info_populate = function(rowid, spec_id) {
        var is_admin_flag = $('#is_admin_flag').val();
        // SUGAR.ajaxUI.showLoadingPanel();
        // console.log(('product_info_populate ==> rowid ' + rowid + ' spec_id
        // '+ spec_id);
        var rules = product_populate.spec_details[spec_id].rules;
        if (editInit == true) {
            var li_id = $("#line_item_id_" + rowid).val();
            var info_edit_details = product_populate.edit_details[li_id]['rules'];
        }
        var ele_str = '';
        if (rules.length == 0) {
            $("#container_product_informat_" + rowid).hide();
            $("#div_product_informat_" + rowid).html('');
        } else {
            ele_str = '';
            $.each(rules, function(ch_key, rule) {
                var mandatory = rule.ismandatory;
                var info_value = '';
                var record_id = '';
                if (editInit == true && info_edit_details.hasOwnProperty(ch_key)) {
                    info_value = info_edit_details[ch_key]['rule_value'];
                    record_id = info_edit_details[ch_key]['rule_objectid'];
                }
                ele_str += '<div class="row margin-0">';
                ele_str += '<div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top" style=" height: 57px;">';
                ele_str += '<div class="cell" >';
                ele_str += '<span class="cellfont" id="label_pi_role_value_' + rowid + '_' + ch_key + '">' + rule.rulename + '';
                if (mandatory == 'true') {
                    ele_str += '<font color="red">*</font>';
                }
                ele_str += '</span>';
                ele_str += '</div></div><div class="col-md-9 add-cell-border-right add-cell-border-top">';
                ele_str += '<div class="cell">';
                ele_str += '<span class="cellfont">';
                ele_str += '<input type="hidden" name="pr_rule_id_' + rowid + '[]" value="' + rule.id + '">';
                ele_str += '<input type="hidden" name="pr_rule_record_id_' + rowid + '[]" value="' + record_id + '" class="pr_rule_record_id">';
                ele_str += '<input type="hidden" name="pr_rule_record_parent_id_' + rowid + '[]" value="' + record_id + '">';
                ele_str += '<textarea name="pr_description_' + rowid + '[]" id="pi_role_value_' + rowid + '_' + ch_key + '" cols="40" rows="1" class="product_informat_e ';
                if (mandatory == 'true') {
                    ele_str += ' req ';
                }
                ele_str += ' " ';
                if ($("#approver_user").val() != "" && $("#contract_status").val() == "1") {
                    ele_str += 'readonly=""';
                } else if ($("#contract_status").val() == "6" && is_admin_flag == '') {
                    ele_str += 'readonly=""';
                }
                if (rule.placeholder != undefined) {
                	ele_str += ' placeholder="' + rule.placeholder + '"';
                }
                ele_str += '>' + info_value + '</textarea>';
                if (rule.ruledesc != undefined && $.trim(rule.ruledesc) != "") {
                	ele_str += '<span class="info_desc">(' + rule.ruledesc + ')</span>';
                }
                ele_str += '</div></div></div>';
            });
            $("#container_product_informat_" + rowid).show();
            $("#div_product_informat_" + rowid).html(ele_str);
        }
        // console.log(('product_info_populate ==> End ');
        SUGAR.ajaxUI.hideLoadingPanel();
    };
    /**
     * @desc : Function to populate the product price/charge details with
     *       selected product configuraion
     * @author : Arunsakthivel
     */
    this.product_price_populate = function(specid, input_selA, rowid) {
        //console.log('product_price_populate ==> rowid ' + rowid + ' spec_id '+ specid);
        SUGAR.ajaxUI.showLoadingPanel();
        $("#div_product_price_" + rowid).html('');
        // Filtering using the specification and charectorstic value (a) config
        var result_charge = [];
        var row_inc = 0;
        if (editInit == true) {
            var li_id = $("#line_item_id_" + rowid).val();
            var price_edit_details = product_populate.edit_details[li_id]['product_price'];
        }
        if (typeof product_populate.offer_details.charge_mapping[specid] != 'undefined') {
            var charge_mapping = product_populate.offer_details.charge_mapping[specid];
            $.each(charge_mapping, function(key, charge_mapping_row) {
                charvalid = charge_mapping_row.charvalid;
                pricelineid = charge_mapping_row.pricelineid;
                // Below condition work for whether charvalid array in
                // input_selA array
                if(charvalid.length == 0) {
                    result_charge.push(pricelineid);
                }
                else if(charvalid.length == 1 && $(charvalid).not(input_selA).length == 0) {
                    result_charge.push(pricelineid);
                }
                else if(charvalid.length == input_selA.length && $(charvalid).not(input_selA).length == 0) {
                    result_charge.push(pricelineid);
                }
            });
        }
        // Filtering By currency start
        var curr = $("#cust_contract_currency_" + rowid).val();
        var result_charge1 = result_charge.slice();
        var charge_details = product_populate.offer_details.charge_details;
        $.each(result_charge1, function(k, pricelineid) {
            charge_detail = charge_details[pricelineid];
            if (curr != '' && charge_detail.currencyid != curr && charge_detail.isCustomPrice == 'false') {
                var i = result_charge.indexOf(pricelineid);
                result_charge.splice(i, 1);
            }
        });
        // Filtering By currency end
        // Display part
        if (result_charge.length == 0) {
            var ele_str = '<div class="row margin-0"><div class="col-md-12 add-cell-border-right add-cell-border-left add-cell-border-bottom center"><div class="cell" ><span class="cellfont">' + SUGAR.language.get('gc_Contracts', 'LBL_NO_PRICE_DETAILS') + '</span></div></div></div>';
        } else {
            var ele_str = '<div class="row " style= "margin-left: 0px; margin-right: 1px; border: 0px;"><div class="margin-0">&nbsp;</div>';
            var row_inc = 1;
            $.each(result_charge, function(prc_key, pricelineid) {
                var charge_detail = charge_details[pricelineid];
                var icbFlag = '0';
                var record_id = '';
                var currencyid = charge_detail.currencyid;
                if (charge_detail.isCustomPrice == 'true') {
                    icbFlag = '1';
                    offeringprice = charge_detail.offeringprice;
                } else {
                    offeringprice = charge_detail.offeringprice;
                }
                var discount = '0.000';
                var contract_price = offeringprice;
                var percent_discount_flag = '0';
                var igc_settle_price = '0.000';
                if (editInit == true && price_edit_details.hasOwnProperty(pricelineid)) {
                    price_edit_detail = price_edit_details[pricelineid];
                    record_id = price_edit_detail['id'];
                    offeringprice = price_edit_detail['list_price'];
                    if (price_edit_detail['charge_price'] != "") {
                        contract_price = price_edit_detail['charge_price'];
                    }
                    igc_settle_price = price_edit_detail['igc_settlement_price'];
                    discount = price_edit_detail['discount'];
                    percent_discount_flag = price_edit_detail['percent_discount_flag'];
                    currencyid = price_edit_detail['currencyid'];
                }
                offeringprice = parseFloat(offeringprice).toFixed(3);
                contract_price = parseFloat(contract_price).toFixed(3);
                igc_settle_price = parseFloat(igc_settle_price).toFixed(3);
                if (row_inc > 3) {
                    row_inc = 1;
                    ele_str += '</div><div class="margin-0">&nbsp;</div>';
                    ele_str += '<div class="row" style= "margin-left: 0px; margin-right: 1px; border: 0px;">';
                }
                ele_str += '<div class="col-md-4 col-sm-4 col-xs-4 " id="product_price_' + rowid + '_' + pricelineid + '" style="margin-bottom: 7px">';
                ele_str += '<table class="table table-bordered product_price" style="table-layout: fixed !important;border: 1px solid #ddd !important;">';
                ele_str += '<tr>';
                ele_str += '<td style="border: 1px solid #ddd !important;" colspan="2" class="cellgrey header" title="' + charge_detail.chargedesc + '"><div class="cellfont" style="overflow: hidden; max-height: 37px; font-weight: bold;">' + charge_detail.chargedesc + '</div></td>';
                ele_str += '</tr>';
                ele_str += '<tr>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class="cellgrey"><span class="cellfont">' + SUGAR.language.get('gc_Contracts', 'LBL_CHARGE_TYPE') + '</span></td>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class=""><span class="cellfont"> ' + charge_detail.chargetype + ' </span></td>';
                ele_str += '</tr>';
                ele_str += '<tr>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class="cellgrey"><span class="cellfont">' + SUGAR.language.get('gc_Contracts', 'LBL_CHARGE_PERIOD') + '</span></td>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class=""><span class="cellfont"> ' + charge_detail.chargeperioddesc + ' </span></td>';
                ele_str += '</tr>';
                ele_str += '<tr>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class="cellgrey"><span class="cellfont">' + SUGAR.language.get('gc_Contracts', 'LBL_CURRENCY') + '</span></td>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class=""><span class="cellfont"> ' + currencyid + ' </span></td>';
                ele_str += '</tr>';
                ele_str += '<tr>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class="cellgrey"><span class="cellfont">' + SUGAR.language.get('gc_Contracts', 'LBL_LIST_PRICE') + '</span></td>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class=""><span class="cellfont">';
                ele_str += '<input type="hidden" name="pp_charge_record_id_' + rowid + '[]" value="' + record_id + '" class="pp_charge_record_id">';
                ele_str += '<input type="hidden" name="pp_charge_record_parent_id_' + rowid + '[]" value="' + record_id + '">';
                ele_str += '<input type="hidden" name="pp_icb_flag_' + rowid + '[]" id="pp_icb_flag_' + rowid + '_' + pricelineid + '" value="' + icbFlag + '">';
                ele_str += '<input type="hidden" name="pp_list_price_' + rowid + '[]" id="pp_list_price_' + rowid + '_' + pricelineid + '"  value="' + offeringprice + '">';
                if (icbFlag == '1') {
                    ele_str += 'ICB';
                } else {
                    ele_str += offeringprice;
                }
                ele_str += '</span></td>';
                ele_str += '</tr>';
                ele_str += '<tr>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class="cellgrey"><span class="cellfont">' + SUGAR.language.get('gc_Contracts', 'LBL_DISCOUNT') + '</span></td>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class=""><span class="cellfont">';
                if (icbFlag != '1') {
                    ele_str += '<input type="text" id="pp_discount_' + rowid + '_' + pricelineid + '" maxlength="16"';
                    ele_str += ' class="pms-pp-act-ronly" style="width:80%" name="pp_discount_' + rowid + '[]"  ';
                    if ($("#approver_user").val() != "" && $("#contract_status").val() == "1") {
                        ele_str += ' readonly="" ';
                    }
                    ele_str += ' onblur="validateDecimal(this);changeDiscount(' + rowid + ',' + pricelineid + ')"  ';
                    ele_str += ' value="' + discount + '" >';
                    ele_str += '<input class="pms-pp-act-ronlychk" style="margin-left: 4px;" type="checkbox" id="pp_discountflag_' + rowid + '_' + pricelineid + '" value="1" title="Discount in percentage" ';
                    if ($("#approver_user").val() != "" && $("#contract_status").val() == "1") {
                        ele_str += ' onclick="return false" ';
                    } else {
                        ele_str += 'onclick="changeDiscountFlag(' + rowid + ',' + pricelineid + ')"';
                    }
                    if (percent_discount_flag == '1') {
                        ele_str += ' checked=true';
                    }
                    ele_str += '><input type="hidden" id="pp_percent_discount_flag_' + rowid + '_' + pricelineid + '" name="pp_percent_discount_flag_' + rowid + '[]" value="' + percent_discount_flag + '" >';
                    ele_str += ' </span><span id="error_discount_' + rowid + '_' + pricelineid + '" class=" pms-pp-act-ronly required validation-message"></span></td>';
                } else {
                    ele_str += '-';
                }
                ele_str += '</tr>';
                ele_str += '<tr>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class="cellgrey"><span class="cellfont">' + SUGAR.language.get('gc_Contracts', 'LBL_CONTRACT_PRICE') + '</span></td>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class=""><span class="cellfont"> <span class="cellfont"';
                if (icbFlag == '1') {
                    ele_str += ' ><input class="pms-pp-act-ronly" type="text" value="' + contract_price + '" maxlength="16" ';
                    ele_str += ' onblur="validateDecimal(this);"  ';
                    ele_str += ' style="width:100%" name="pp_customer_contract_price_' + rowid + '[]"  ';
                    if ($("#approver_user").val() != "" && $("#contract_status").val() == "1") {
                        ele_str += ' readonly="" ';
                    }
                    ele_str += 'id="pp_customer_contract_price_' + rowid + '_' + pricelineid + '" /> </span>';
                } else {
                    ele_str += ' id="spn_customer_contract_price_' + rowid + '_' + pricelineid + '" >' + contract_price + '</span>';
                    ele_str += '<input type="hidden" value="' + contract_price + '" name="pp_customer_contract_price_' + rowid + '[]" ';
                    ele_str += ' id="pp_customer_contract_price_' + rowid + '_' + pricelineid + '" /> ';
                }
                ele_str += ' </span></td></tr>';
                ele_str += '<tr>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class="cellgrey"><span class="cellfont">' + SUGAR.language.get('gc_Contracts', 'LBL_IGC_SETTLEMENT_PRICE') + '</span></td>';
                ele_str += '<td style="border: 1px solid #ddd !important;" class=""><span class="cellfont">';
                ele_str += '<input type="text" value="' + igc_settle_price + '"';
                ele_str += ' class="pms-pp-act-ronly" style="width:100%" name="pp_igc_settlement_price_' + rowid + '[]" maxlength="16"';
                if ($("#approver_user").val() != "" && $("#contract_status").val() == "1") {
                    ele_str += ' readonly="" ';
                }
                ele_str += ' id="pp_igc_settlement_price_' + rowid + '_' + pricelineid + '"/>';
                ele_str += ' </span>';
                ele_str += '<input type="hidden" name="pp_charge_name_' + rowid + '[]" value="' + pricelineid + '">';
                ele_str += '<input type="hidden" name="pp_charge_type_' + rowid + '[]" value="' + charge_detail.chargeid + '">';
                ele_str += '<input type="hidden" name="pp_charge_period_' + rowid + '[]" value="' + charge_detail.chargeperiodid + '">';
                ele_str += '<input type="hidden" name="pp_charge_currency_' + rowid + '[]" value="' + charge_detail.currencyid + '">';
                ele_str += '</td>';
                ele_str += '</tr>';
                ele_str += '</table>';
                ele_str += '</div>';
                row_inc++;
                // changeDiscount(rowid, pricelineid);
            });
        }
        $("#div_product_price_" + rowid).html(ele_str);
        // disable list of fields when contract status is Approved
        if (($("#approver_user").val() != "") && ($("#contract_status").val() == "1") && ($('#version_upgrade').val() != 'true')) {
            restrictApproverFields();
        }
        // disable list of fields when contract status is Termination Approved
        isContractStatusTerminated();
        SUGAR.ajaxUI.hideLoadingPanel();
    };
};
var editInit = false;
var product_offering = '';
var product_offering_changed = false;
$(document).ready(function() {
    // Initialize the variables
    product_populate.product_intialize();
    /**
     * @desc : Linking the onchange event to product_offering
     * @author : Arunsakthivel
     */
    $("#product_offering").on("change", function() {
        if (record != '') {
            YAHOO.SUGAR.MessageBox.show({
                msg: SUGAR.language.get('gc_Contracts', 'LBL_CHANGE_OFFER_ALERT'),
                title: 'Attention!',
                type: 'confirm',
                close: false,
                fn: function(confirm) {
                    if (confirm == 'yes') {
                        lineItemRows = $("[id^='container_lineitem_']").length;
                        for (var rowid = 1; rowid <= lineItemRows; rowid++) {
                            if ($("#line_item_id_" + rowid).val() != "") {
                                $("#container_lineitem_" + rowid).hide();
                                $("#container_lineitem_" + rowid + " :input").attr("disabled", true);
                                $("#line_item_id_" + rowid).attr("disabled", false);
                                $("#li_deleted_" + rowid).attr("disabled", false);
                                $("#li_deleted_" + rowid).val('1');
                            } else { // DF149
                                // kamal
                                // added
                                $("#container_lineitem_" + rowid).remove();
                            }
                        }
                        addLineItem("0");
                        //Change the first line item in use.
                        var lineItemRows1 = getLineItemsRowLenth()
                        for (var rowid1 = one_stop_billing_reference_row + 1; rowid1 <= lineItemRows1; rowid1++) {
                            if ($("#li_deleted_" + rowid1).val() == '0') {
                                one_stop_billing_reference_row = rowid1;
                                break;
                            }
                        }
                        if (product_offering_changed == false) {
                            product_offering_changed = true;
                        }
                        product_populate.product_spec_getDetails();
                        product_populate.product_spec_populate();
                    } else {
                        $("#product_offering").val(product_offering);
                        return false;
                    }
                }
            });
        } else {
            editInit = false;
            product_populate.product_spec_getDetails();
            product_populate.product_spec_populate();
        }
    });
    /**
     * @desc : function to handle the onchange on the
     *       specifications
     * @author : Arunsakthivel
     * @param :
     *            html element
     */
    product_spec_change = function(ele) {
        editInit = false;
        var id_name = $(ele).attr("id");
        var spec_id = $(ele).val();
        var rowid = id_name.substr(id_name.lastIndexOf('_') + 1);
        if (spec_id > 0) {
            if (record != '') {
                $("#li_prod_specification_changed_" + rowid).val('1');
            }
            product_populate.product_config_populate(rowid, spec_id);
            $("#product_spec_subpanels_" + rowid).show();
            disable_enter_form_submit();
        } else {
            $("#product_spec_subpanels_" + rowid).hide();
        }
    };
    
    product_config_validate = function(thisobj){
        disable_enter_form_submit();
    	var id = thisobj.id;
    	var lv = $(thisobj).data('lv');
    	var uv = $(thisobj).data('uv');
    	var inc = $(thisobj).data('inc');
    	var value = $('#'+id).val();
    	var error = false;
    	var numbers = /^[0-9]+$/;
    	$('#error_'+id).html('');
    	if($.trim(value)!=''){    		
	    	if(value.match(numbers) === null){
	    		error = true;
	    	}else{
		    	value = parseInt(value);
		    	if(isNaN(value)==true){
		    		error = true;
		    	}else if(value < lv || value > uv || (value % inc !== 0)){
		    		error = true;
		    	}
	    	}
	    	if(error){
	    		$('#'+id).parent().find('.error').html('Invalid range value');
                if ($('#'+id).parent().find('.error').length == 0 ) {
                    $('#'+id).parent().append('<span id="'+ id +'" class="error">Invalid range value</span>');
                }
	    		return false;	    		
	    	}	    	
    	}
        $('#'+id).parent().find('.error').remove();
    	return true;
    }
    /**
     * @desc : function to handle the onchange on the
     *       configurations
     * @author : Arunsakthivel
     * @param :
     *            Name of the element
     */
    product_config_change = function(id_name, editInit) {
        disable_enter_form_submit();
        editInit = editInit;
        // Format product_config_1_1
        var rowid = id_name.substr(15, id_name.lastIndexOf('_') - 15);
        var spec_id = $("#product_specification_" + rowid).val();
        if (spec_id > 0) {
            // populate configA details
            var configA = [];
            if (record != '' && editInit == false) {
                $("#li_prod_configuration_changed_" + rowid).val('1');
            }
            $("[id^='product_config_" + rowid + "_']").each(function() {
                var valtypeid = $(this).data('valtypeid');
                var config_selected = $(this).val();
                if (valtypeid == 2) {
                    // For valtypeid == 2 and range selection
                    charvalid = $(this).data('charvalid');
                    config_selected = charvalid;
                }
                if (config_selected > 0 && valtypeid != 2) {
                    configA.push(config_selected);
                }
            });
            product_populate.product_price_populate(spec_id, configA, rowid);
        } else {
            var ele_str = '<div class="row margin-0"><div class="col-md-12 add-cell-border-right add-cell-border-left add-cell-border-bottom center"><div class="cell" ><span class="cellfont">No charge is available for this case</span></div></div></div>';
            $("#div_product_price_" + rowid).html(ele_str);
        }
    };
    /**
     * @desc : Fired when discount chck box is cliked
     * @author : Arunsakthivel
     * @param :
     *            row id & charge price line id value
     */
    product_currency_change = function(id_name) {
        editInit = false;
        // Format cust_contract_currency_0
        var rowid = id_name.substr(id_name.lastIndexOf('_') + 1);
        product_config_change("product_config_" + rowid + "_1", editInit);
    };
    /**
     * @desc : Fired when discount chck box is cliked
     * @author : Arunsakthivel
     * @param :
     *            row id & charge price line id value
     */
    changeDiscountFlag = function(rowid, pricelineid) {
        // var lprc = $("#pp_list_price_" + rowid + '_'+pricelineid).val();
        // $("#pp_discount_" + rowid + '_'+pricelineid).val('');
        // $("#spn_customer_contract_price_" + rowid + '_'+pricelineid).html(lprc);
        // $("#pp_customer_contract_price_" + rowid + '_'+pricelineid).val(lprc);
        if ($("#pp_discountflag_" + rowid + '_' + pricelineid).is(':checked')) {
            $("#pp_percent_discount_flag_" + rowid + '_' + pricelineid).val(1);
        } else {
            $("#pp_percent_discount_flag_" + rowid + '_' + pricelineid).val(0);
        }
        changeDiscount(rowid, pricelineid);
    };
    /**
     * @desc : Fired when value changed and after blur in the
     *       discount box
     * @author : Arunsakthivel
     * @param :
     *            row id & charge price line id value
     */
    changeDiscount = function(rowid, pricelineid) {
        // console.log('changeDiscount rowid ' + rowid + '
        // pricelineid ' + pricelineid);
        var vl_dis_s = $("#pp_discount_" + rowid + '_' + pricelineid).val();
        var lprc = $("#pp_list_price_" + rowid + '_' + pricelineid).val();
        lprc = parseFloat(lprc);
        vl_dis = parseFloat(vl_dis_s);
        var dprc = lprc;
        var error = false;
        if (isNaN(vl_dis)) {
            vl_disn = '';
            error = true;
        } else {
            if ($("#pp_discountflag_" + rowid + '_' + pricelineid).is(':checked')) {
                // Discount is in Percentage
                if (vl_dis > 100 || vl_dis < 0) {
                    vl_disn = '';
                    error = true;
                } else {
                    dprc = lprc - (lprc * vl_dis / 100);
                }
            } else {
                // Discount is in direct amount
                var dec_rx = /^\d{1,12}\.{1}\d{0,3}$/;
                if (vl_dis > lprc || vl_dis < 0 || !vl_dis_s.match(dec_rx)) {
                    vl_disn = '';
                    error = true;
                } else {
                    dprc = lprc - vl_dis;
                }
            }
        }
        dprc = parseFloat(dprc).toFixed(3);
        // vl_dis = vl_dis.toFixed(2);
        if (error == true) {
            $("#pp_discount_" + rowid + '_' + pricelineid).val(vl_disn);
            YAHOO.SUGAR.MessageBox.show({
                msg: SUGAR.language.get('gc_Contracts', 'LBL_PRODUCT_PRICE_DEC_TITLE'),
                title: 'Error',
                type: 'alert',
                fn: function(confirm) {
                    $("#pp_discount_" + rowid + '_' + pricelineid).val('0.000');
                }
            });
        }
        $("#spn_customer_contract_price_" + rowid + '_' + pricelineid).html(dprc);
        $("#pp_customer_contract_price_" + rowid + '_' + pricelineid).val(dprc);
    };
    /*
     * To make the line item as 'Change' from 'Activate' if the 'Change' button is clicked
     * $param: rowid
     * @author: Mohamed.Siddiq
     * @date: 06-Jul-2016
     */
    makeLineItemToChange = function(rowid) {
        $('#terminate_line_item_' + rowid).hide();
        enableAllFieldsInALineItem(rowid, true);
        $('#contract_line_item_type_' + rowid).val('Change');
        disableSelectboxOptions('contract_line_item_type_' + rowid);
        $('#change_contract_' + rowid).css('display', 'none');
        $('#cancel_change_line_item_' + rowid).css('display', '');
    };
    /*
     * Function to disable all the options inside a dropdown
     * @Param: element id
     * @author: Mohamed.Siddiq
     * @Date: 08-Jul-2016
     */
    disableSelectboxOptions = function(elem_id) {
            $('#' + elem_id + ' option').filter(":selected").siblings('option').prop('disabled', true);
        }
        /*
         * Function to enable all the fields in a particular line item
         * DF-155
         * @Param: rowid
         * @param: enableAllFieldsInALineItem //if set to true, then specification field will be in disabled
         * @author: Mohamed.Siddiq
         * @Date: 06-Jul-2016
         */
    enableAllFieldsInALineItem = function(rowid, isdisablespecification) {
        isdisablespecification = isdisablespecification || true;
        var type = null;
        $("#li_" + rowid + " :input").each(function(input) {
            type = $(this).prop('type');
            switch (type) {
                case 'text':
                case 'textarea':
                    var elid = $(this).attr('id');
                    var expectedids = ['tech_account_name_' + rowid, 'bill_account_name_' + rowid, 'gc_organization_name_' + rowid];
                    if ($.inArray(elid, expectedids) !== -1) {
                        $(this).attr('readonly', 'readonly');
                    } else {
                        $(this).removeAttr('readonly');
                    }
                    break;
                case 'select-one':
                    var elem_id = $(this).attr('id');
                    var specificatinofieldid = 'product_specification_' + rowid;
                    if (isdisablespecification && (elem_id == specificatinofieldid)) {
                        disableSelectboxOptions(elem_id);
                    } else {
                        $('#' + elem_id + ' option').filter(":selected").siblings('option').prop('disabled', false);
                    }
                    // OM-341
                    var is_user_admin = $('#is_admin_flag').val();
                    if(is_user_admin == 'true' && (elem_id == specificatinofieldid)) {
                        $('#' + elem_id + ' option').filter(":selected").siblings('option').prop('disabled', false);
                    }
                    break;
                case 'button':
                    var elid = $(this).attr('id');
                    var expectedids = ['add_invoice_subgroup_' + rowid, 'btn_bill_account_name_' + rowid, 'btn_clr_bill_account_name_' + rowid, 'btn_tech_account_name_' + rowid, 'btn_clr_tech_account_name_' + rowid, 'btn_gc_organization_name_' + rowid, 'btn_clr_gc_organization_name_' + rowid];
                    if ($.inArray(elid, expectedids) !== -1) {
                        $(this).show();
                    }
                    break;
                case 'checkbox':
                    var actualfunctions = $(this).attr('onclick');
                    actualfunctions = actualfunctions.replace('return false;', '');
                    $(this).attr('onclick', actualfunctions);
                    break;
                default:
                    //console.log($(this).attr('id') + 'type '+ $(this).prop('type') + ' do nothing');
                    break;
            }
        });
        //hide the calendar selection images
        $("#service_start_date_trigger_" + rowid).show();
        $("#service_end_date_trigger_" + rowid).show();
        $("#billing_start_date_trigger_" + rowid).show();
        $("#billing_end_date_trigger_" + rowid).show();
    };
    /*
     * Function to disable all the fields in a particular line item
     * DF-155
     * @Param : rowid
     * @author: Mohamed.Siddiq
     * @date: 06-Jul-2016
     */
    disableAllFieldsInALineItem = function(rowid) {
        var type = null;
        $("#li_" + rowid + " :input").each(function(input) {
            type = $(this).prop('type');
            switch (type) {
                case 'text':
                case 'textarea':
                    $(this).attr('readonly', 'readonly');
                    break;
                case 'select-one':
                    var elem_id = $(this).attr('id');
                    disableSelectboxOptions(elem_id);
                    break;
                case 'button':
                    var elid = $(this).attr('id');
                    var expectedids = ['add_invoice_subgroup_' + rowid, 'btn_bill_account_name_' + rowid, 'btn_clr_bill_account_name_' + rowid, 'btn_tech_account_name_' + rowid, 'btn_clr_tech_account_name_' + rowid, 'btn_gc_organization_name_' + rowid, 'btn_clr_gc_organization_name_' + rowid];
                    if ($.inArray(elid, expectedids) !== -1) {
                        $(this).hide();
                    }
                    break;
                case 'checkbox':
                    var actualfunctions = $(this).attr('onclick');
                    $(this).attr('onclick', 'return false;' + actualfunctions);
                    break;
                default:
                    //console.log($(this).attr('id') + 'type '+ $(this).prop('type') + ' do nothing');
                    break;
            }
        });
        //hide the calendar selection images
        $("#service_start_date_trigger_" + rowid).hide();
        $("#service_end_date_trigger_" + rowid).hide();
        $("#billing_start_date_trigger_" + rowid).hide();
        $("#billing_end_date_trigger_" + rowid).hide();
    };
    /**
     * To disable all the existing line item until the user clicks on change button
     * @author : Mohamed.Siddiq
     * @date: 06-jul-2016
     */
    disableAllExistingLineItem = function() {
            var lineItemRows = getLineItemsRowLenth();
            for (var rowid = 1; rowid <= lineItemRows; rowid++) {
                var li_status = $('#contract_line_item_type_' + rowid).val();
                if (li_status == li_status_to_disp_change || li_status == li_status_to_disp_terminate) {
                    disableAllFieldsInALineItem(rowid);
                }
                //disable product specification options if the current status is change
                if (li_change_status == li_status) {
                    // OM-341
                    var is_user_admin = $('#is_admin_flag').val();
                    if(is_user_admin != 'true') {
                        disableSelectboxOptions('product_specification_' + rowid);
                    }
                }
            }
        }
        /**
         * Code part for the Edit page, Initialization and seting values
         */
    if (record != "") {
        editInit = true;
        product_populate.product_spec_getDetails();
        product_populate.product_spec_getEditDetails();
        product_populate.product_spec_populate();
        product_offering = $("#product_offering").val();
        if (version_upgrade == 'true') {
            $('.line_item_id').val('');
            $('.line_item_history_id').val('');
            $('.sales_rep_id').val('');
            $('.gc_CreditCard_id').val('');
            $('.gc_DirectDeposit_id').val('');
            $('.gc_AccountTransfer_id').val('');
            $('.gc_PostalTransfer_id').val('');
            $('.pc_characteristic_record_id').val('');
            $('.pr_rule_record_id').val('');
            $('.pp_charge_record_id').val('');
        }
        if (is_change_available >= 1) {
            disableAllExistingLineItem();
        }
        disable_enter_form_submit();
    }
});

function validateDecimal(elem) {
    var dec_rx = /^\d{1,12}\.{1}\d{0,3}$/;
    var int_rx = /^\d{1,12}$/;
    var vl = elem.value;
    if (!vl.match(dec_rx)) {
        if (vl.match(int_rx)) {
            elem.value = vl + '.000';
        } else {
            YAHOO.SUGAR.MessageBox.show({
                msg: SUGAR.language.get('gc_Contracts', 'LBL_PRODUCT_PRICE_DEC_TITLE'),
                title: 'Error',
                type: 'alert',
                fn: function(confirm) {
                    elem.value = '0.000';
                }
            });
            return false;
        }
    }
    return true;
}

function disable_enter_form_submit() {
    var disable_enter_fields = ['[id^="pp_discount_"]'];
    disable_enter_fields = disable_enter_fields.join(", ");
    
    $(disable_enter_fields).on('focusin', function() {
        console.log('focusin');
        $(this).off('keydown').keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });
}