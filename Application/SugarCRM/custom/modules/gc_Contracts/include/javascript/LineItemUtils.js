/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/
/**
 * @desc : Global variables
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
var parentNode = null;
var record = '';
var delLineItem = [];
var delLineItemHistory = [];
var delPaymentType = [];
//hide all save btn in edit view until page load complete (Show Code -> ProductPopulate.js:156)
if ($('[name="record"]').val().length != 0) $('#SAVE_HEADER,#save_and_continue,#SAVE_FOOTER').hide();
/**
 * @desc : Function to init DOM objects on JQuery Document Load
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
$(document).ready(function() {
    record = $('[name="record"]').val();
    parentNode = $('.parentDiv');
    initLineItem();
    initLineItemType();
    initLineItemDateTime();
    // clear accounts relate if corporate relate browse clicked
    /* $('#btn_pm_products_gc_contracts_1_name, #btn_clr_pm_products_gc_contracts_1_name').click(function() {
        var form = this.form;
        parentNode.find('.pm_products_gc_contracts_1pm_products_ida').each(function() {
            $this = $(this);
            SUGAR.clearRelateField(form, $this.find('.sqsEnabled').attr('id'), $this.find('.idhidden').attr('id'));
        });
    }); */
    if (record == '') {
        parentNode.find('.close').removeClass('hide');
    } else {
        parentNode.find('.LineItem').each(function() {
            $this = $(this);
            node = $this.find('.line_item_id');
            if (node.val() == '') {
                $this.find('.close').removeClass('hide');
            }
        });
    }
    
    $(document).on('change', ".datetimecombo_hr, .datetimecombo_min, .datetimecombo_sec", function(){
    	disableLineItemTimeZone($(this).attr("id"));
    });
    disableTimeZoneOnLoad();
});
/**
 * @desc : Function to init line item for create view
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function initLineItem() {
    if (record == '') {
        clear_all_errors();
        var node = parentNode.find('.LineItemCopy').clone();
        var cal_setup = node.find('script');
        node.find('script').remove();
        var lineitem = null;
        lineitem = updateLineItemSequenceNumber(node, 1);
        lineitem.removeClass('hide LineItemCopy').addClass('LineItem');
        lineitem.appendTo(parentNode);
        lineitem.attr('id', 'container_lineitem_1');
        setupCalendarFields(lineitem, cal_setup);
    }
}
/**
 * @desc : Function to add a new line item row on add button click
 * @params : element <object> DOM object of add button
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function addLineItem(change_lineitem_by_billing) {
    clear_all_errors();
    var rowno = parseInt(parentNode.find('.LineItem').length) + 1;
    var node = parentNode.find('.LineItemCopy').clone();
    var cal_setup = node.find('script');
    node.find('script').remove();
    var lineitem = null;
    lineitem = updateLineItemSequenceNumber(node, rowno);
    lineitem.removeClass('hide LineItemCopy').addClass('LineItem');
    lineitem.appendTo(parentNode);
    lineitem.attr('id', 'container_lineitem_' + rowno);
    updateLineItemElementByOrder();
    setupCalendarFields(lineitem, cal_setup);
    product_populate.product_spec_populate_single_row(rowno); // In file productPopulate.js
    showInvoiceGroups(rowno); // In file EditView.js
    if (change_lineitem_by_billing == "1") {
        changeLineItemFieldsByBilling_type(true, rowno); // In file EditView.js
    }
    disableTimeZoneOnLoad();
}
/**
 * @desc : Function to update the order sequence of line item order
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function updateLineItemElementByOrder() {
    var node = parentNode.find('.LineItem');
    var i = 1;
    node.each(function() {
        $this = $(this);
        updateLineItemSequenceNumber($this, i, $this.find('.line_item_number').val());
        setupCalendarFields($this, getCalSetups());
        i = i + 1;
    });
}
/**
 * @desc : Function to init LineItem Date and Time
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function initLineItemDateTime() {
    parentNode.find('.dateInput').each(function() {
        $this = $(this);
        var date_time = $this.find('.datehidden').val();
        if (date_time != '' && date_time != 'undefined') {
            var dt = date_time.split(" ");
            $this.find('.date_input').val(dt[0]);
            var time = dt[1].split(":");
            $this.find('.datetimecombo_hr').val(time[0]);
            $this.find('.datetimecombo_min').val(time[1]);
            $this.find('.datetimecombo_sec').val(time[2]);
        }
    });
}
/**
 * @desc : Function to get Cal Setup Script
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function getCalSetups() {
    return parentNode.find('.LineItemCopy').find('script');
}
/**
 * @desc : Function to update the sequence number of DOM element
 *       attribute{Name/Id}
 * @params : lineItemRow <object> JQuery object line item div expseq <string>
 *         the sequence number to be replaced. currseq <string> the current
 *         sequence number in the line item div
 * @return : lineItemRow <object> JQuery object line item div after replacing
 *         the sequence number
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function updateLineItemSequenceNumber(lineItemRow, expseq, currseq) {
    if (typeof currseq == 'undefined') {
        // by default there will zero current sequence number
        currseq = 0;
    }
    var btn = new Array('btn_tech_account_name_', 'btn_clr_tech_account_name_', 'btn_bill_account_name_', 'btn_clr_bill_account_name_', 'btn_gc_organization_name_', 'btn_clr_gc_organization_name_');
    var date_fld = new Array('service_end_date_', 'billing_end_date_', 'service_end_date_hidden_', 'billing_end_date_hidden_');
    var fields = new Array('dropdown-sr_', 'lineno_', 'line_item_id_', 'line_item_number_', 'li_', 'dd_', 'is_deleted_', 'product_quantity_', 'additional_fee_', 'total_nrc_', 'total_mrc_', 'service_start_date_hidden_', 'service_start_date_', 'service_start_date_trigger_', 'service_start_date_hours_', 'service_start_date_minutes_', 'service_start_date_seconds_', 'service_end_date_hidden_', 'service_end_date_', 'service_end_date_trigger_', 'service_end_date_hours_', 'service_end_date_minutes_', 'service_end_date_seconds_', 'gc_organization_id_', 'gc_organization_name_', 'btn_gc_organization_name_', 'btn_clr_gc_organization_name_', 'billing_start_date_hidden_', 'billing_start_date_', 'billing_start_date_trigger_', 'billing_start_date_hours_', 'billing_start_date_minutes_', 'billing_start_date_seconds_', 'billing_end_date_hidden_', 'billing_end_date_', 'billing_end_date_trigger_', 'billing_end_date_hours_', 'billing_end_date_minutes_', 'billing_end_date_seconds_', 'service_start_date_timezone_', 'service_start_date_timezone_name_', 'service_end_date_timezone_', 'service_end_date_timezone_name_', 'billing_start_date_timezone_', 'billing_start_date_timezone_name_', 'billing_end_date_timezone_', 'billing_end_date_timezone_name_', 'tech_account_countryid_', 'bill_account_countryid_', 'tech_account_name_', 'tech_account_id_', 'btn_tech_account_name_', 'btn_clr_tech_account_name_', 'bill_account_name_', 'bill_account_id_', 'btn_bill_account_name_', 'btn_clr_bill_account_name_', 'payment_type_', 'dd_bank_code_', 'dd_branch_code_', 'dd_deposit_type_', 'dd_account_no_', 'dd_person_name_1_', 'dd_person_name_2_', 'dd_payee_tel_no_', 'dd_payee_email_address_', 'dd_remarks_', 'cc_credit_card_no_', 'cc_expiration_date_', 'pt_name_', 'pt_postal_passbook_mark_', 'pt_postal_passbook_', 'pt_postal_passbook_no_display_', 'at_name_', 'at_bank_code_', 'at_branch_code_', 'at_deposit_type_', 'at_account_no_', 'at_account_no_display_', 'gc_DirectDeposit_id_', 'gc_CreditCard_id_', 'gc_PostalTransfer_id_', 'gc_AccountTransfer_id_', 'label_product_name_', 'label_gc_organization_name_', 'label_tech_account_name_', 'label_bill_account_name_', 'label_payment_type_', 'label_total_nrc_', 'label_total_mrc_', 'label_billing_start_date_', 'label_billing_end_date_', 'label_service_start_date_', 'label_service_end_date_', 'label_dd_payee_email_address_', 'line_item_history_id_', 'label_dd_bank_code_', 'label_dd_branch_code_', 'label_dd_deposit_type_', 'label_dd_account_no_', 'label_cc_credit_card_no_', 'label_cc_expiration_date_', 'label_pt_name_', 'label_pt_postal_passbook_mark_', 'label_pt_postal_passbook_', 'label_pt_postal_passbook_no_display_', 'label_at_name_', 'label_at_bank_code_', 'label_at_branch_code_', 'label_at_deposit_type_', 'label_at_account_no_', 'label_at_account_no_display_', 'factory_reference_no_', 'product_specification_', 'cust_contract_currency_', 'cust_billing_currency_', 'igc_billing_currency_', 'igc_contract_currency_', 'customer_billing_method_', 'igc_settlement_method_', 'div_product_config_', 'div_product_price_', 'product_spec_subpanels_', 'div_product_spec_', 'label_product_specification_', 'label_cust_contract_currency_', 'label_cust_billing_currency_', 'label_igc_contract_currency_', 'label_igc_billing_currency_', 'label_customer_billing_method_', 'label_igc_settlement_method_', 'description_', 'container_product_informat_', 'div_product_informat_', 'li_deleted_', 'li_invoice_subgroup_', 'add_invoice_subgroup_', 'btn_li_remove_', 'contract_line_item_type_', 'label_billing_end_date_timezone_', 'label_billing_start_date_timezone_','label_service_start_date_timezone_', 'label_service_end_date_timezone_');
    for (var fieldindex in fields) {
        var elementId = fields[fieldindex];
        var actualElementId = elementId + currseq;
        var expectedElementId = elementId + expseq;
        lineItemRow.find('#' + actualElementId).attr('id', expectedElementId).filter('[name="' + actualElementId + '"]').attr('name', expectedElementId);
        var re = new RegExp('_' + currseq, "g");
        for (var btn_val in btn) {
            if (elementId == btn[btn_val]) {
                var oldattr = lineItemRow.find('#' + expectedElementId).attr('onclick');
                var newattr = oldattr.replace(re, '_' + expseq);
                lineItemRow.find('#' + expectedElementId).attr('onclick', newattr);
            }
        }
        for (var fld in date_fld) {
            if (elementId == date_fld[fld]) {
                var oldattr = lineItemRow.find('#' + expectedElementId).attr('dependentField');
                var newattr = oldattr.replace(re, '_' + expseq);
                lineItemRow.find('#' + expectedElementId).attr('dependentField', newattr);
            }
        }
    }
    //lineItemRow.find('.lineno').html('L' + expseq);
    lineItemRow.find('.line_item_number').val(expseq);
    return lineItemRow;
}
/**
 * @desc : Function to total MRC and total NRC
 * @params : fld <string> class name of the field updatefld <string> id name of field to set Total Value
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function calculateTotal(fld, updatefld) {
    var node = parentNode.find('.LineItem');
    var amt = 0;
    node.find('.' + fld).each(function() {
        $this = $(this);
        var val = parseFloat($this.val());
        if (isNaN(val)) val = 0;
        amt = parseFloat(val) + parseFloat(amt);
    });
    if (fld == 'mrc') {
        var cnt_period = $('#contractperiod').val();
        if (isNaN(cnt_period)) {
            cnt_period = 1;
            amt = parseFloat(amt) * parseFloat(cnt_period);
        }
    }
    if (isNaN(amt)) amt = 0;
    amt = parseFloat(amt).toFixed(2);
    $('#' + updatefld).val(amt);
    calculateGrandTotal();
}
/**
 * @desc : Function to calculate grand total and set into grand total field
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function calculateGrandTotal() {
    var total_nrc = $('#total_nrc').val();
    var total_mrc = $('#total_mrc').val();
    var amt = parseFloat(total_nrc) + parseFloat(total_mrc);
    if (isNaN(amt)) amt = 0;
    amt = parseFloat(amt).toFixed(2);
    $('#grand_total').val(amt);
}
/**
 * @desc : Function to get the field label
 * @params : label <string> name of the field
 * @return : lbl <string> Field label
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function getLabel(label) {
    var lbl = parentNode.find('#' + label).text();
    lbl = lbl.replace(/[^\w\s]/gi, ' ');
    return lbl;
}
/**
 * @desc : Function to remove duplicated array element
 * @params : list <Array> array list from which duplicate elements to be removed
 * @return : result <Array> Unique list of array elements
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function unique(list) {
    var result = [];
    $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
}
/**
 * @desc : Function to check Parent exist then only allow to open pop up for  relate field
 * @params : node <object> DOM object of current relate select button flag <int>
 *         flag to enable disable checking p_name <string> name of the parent
 *         relate field
 * @return : result <bool> true / false
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function checkParent(node, flag, p_name) {
    if (flag == '0' || flag == '') return true;
    if (p_name == '') return false;
    var parent = $('#' + p_name);
    if (parent.val() == '') {
        var lbl = parent.closest('td').prev().text();
        lbl = lbl.replace(/[^\w\s]/gi, '');
        var alert_message = SUGAR.language.get('gc_Contracts', 'LBL_SELECT_PARENT_ALERT');
        alert_message = alert_message.replace('~~', lbl);
        var title_message = SUGAR.language.get('gc_Contracts', 'LBL_SELECT_PARENT_TITLE');
        YAHOO.SUGAR.MessageBox.show({
            msg: alert_message,
            title: title_message,
            type: 'alert'
        });
        return false;
    }
    return true;
}
/**
 * @desc : Function to to be called on save button click
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function lineItemActions() {
    addRequiredValidation();
    addPayemntTypeRequiredValidation();
    addNumericValidation();
    addDateValidation();
    addEmailValidation();
    calculateTotal('nrc', 'total_nrc');
    calculateTotal('mrc', 'total_mrc');
    $('#deletedItem').val(delLineItem.join());
    $('#deletedItemHistory').val(delLineItemHistory.join());
    $('#nolineitem').val(parentNode.find('.LineItem').length);
    removeUnwantedValidation();
    parentNode.find('.LineItem').find('.payment_id').each(function() {
        $n = $(this);
        var par = $n.closest('li');
        if ($n.val() != '' && par.hasClass('hide')) delPaymentType.push($n.attr('data') + '|' + $n.val());
    });
    delPaymentType = unique(delPaymentType);
    $('#paymentItem').val(delPaymentType.join());
}
/**
 * @desc : Function to remove the validation on the fields that are no longer required
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function removeUnwantedValidation(node) {
    var node = parentNode.find('.LineItem').find('.hide').find('.req, .emailadd');
    node.each(function() {
        $this = $(this);
        var name = $this.attr('name');
        removeFromValidate('EditView', name);
    });
    node.parent().find('.validation-message').remove();
}
/**
 * @desc : Function to add a email field validation for the line item fields
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function addEmailValidation() {
    return;
/*     var node = parentNode.find('.LineItem');
    node.find('.emailadd').each(function() {
        $this = $(this);
        var name = $this.attr('id');
        var type = $this.attr('data');
        var lbl = getLabel('label_' + name);
        console.log('field_type : '+type);
        addToValidate('EditView', name, type, false, lbl);
    }); */
}
/**
 * @desc : Function to add a date field validation for the line item fields
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function addDateValidation() {
    var node = parentNode.find('.LineItem');
    var count = 0;
    node.find('.date_input').each(function() {
        $this = $(this);
        var name = $this.attr('id');
        var type = $this.attr('data');
        var lbl = getLabel('label_' + name);
        addToValidate('EditView', name, type, false, lbl);
        makeDateTime($this);
    });
   
}
/**
 * @desc : Function to add a date timezone validation for the line item fields
 * @params :
 * @return :
 * @author : Kamal
 * @date : 3-Mar-2017
 */
function addDateTimezoneValidation() 
{
    var node = parentNode.find('.LineItem');
    var count = 0;
    node.find('.date_input').each(function() {
        $this = $(this);
        if (!lineItemDateTimezoneValidation($this)) {
        	count = count+1;
        }
    });
   if(count > 0) {
	   return false;
   }
   return true;
}
/**
 * @desc : Function to concat date and time
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function makeDateTime(node) {
    var parent = node.closest('span');
    var date_hidden = parent.find('.datehidden');
    var date_fld = parent.find('.date_input');
    if (date_fld.val() == '') date_hidden.val('');
    if (date_fld.val() != '' && date_fld.val() != 'undefined') {
        date_hidden.val(date_fld.val() + ' ' + parent.find('.datetimecombo_hr').val() + ':' + parent.find('.datetimecombo_min').val() + ':' + parent.find('.datetimecombo_sec').val());
    }
        
}
/**
 * @desc : Function to concat date and time
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function lineItemDateTimezoneValidation(node) {
    var parent = node.closest('span');
    
    var date_fld = parent.find('.date_input');
    var date_field_id = date_fld.attr("id");
    removeFromValidate('EditView', date_field_id);
    if ((parseInt(parent.find('.datetimecombo_hr').val()) > 0 || parseInt(parent.find('.datetimecombo_min').val()) >0 || parseInt(parent.find('.datetimecombo_sec').val()) > 0) &&  (date_fld.val() == '' || date_fld.val() == 'undefined')) {
    	var lbl = getLabel('label_' + date_field_id);
    	addToValidate('EditView', date_field_id, 'date', true, lbl); 
    } else {
    	var lbl = getLabel('label_' + date_field_id);
        addToValidate('EditView', date_field_id, 'date', false, lbl); 
    }
    var res = date_field_id.split("_");
    var field_index = res[3]; 
    var timezone_id = date_field_id.replace(field_index,"timezone_"+field_index);
    
    var msg = getLabel('label_' + timezone_id);
    removeFromValidate('EditView', timezone_id);
    if ((parseInt(parent.find('.datetimecombo_hr').val()) > 0 || parseInt(parent.find('.datetimecombo_min').val()) >0 || parseInt(parent.find('.datetimecombo_sec').val()) > 0) ||  (date_fld.val() != '' && date_fld.val() != 'undefined')) {
    	addToValidate('EditView', timezone_id, 'text', true, msg);
    }
    if (date_fld.val() != '' && date_fld.val() != 'undefined') {
        if (date_fld.attr('dependentfield') != '') {
        	var dep_fld = date_fld.attr('dependentfield');
        	if ($("#"+dep_fld).val() != '' && $("#"+dep_fld).val() != undefined) {
        		var res = dep_fld.split("_");
        	    var field_index = res[3];
        	
        	    var start_timezone = "";
        	    var end_timezone = "";
        	    var end_date = date_fld.val() + ' ' + parent.find('.datetimecombo_hr').val() + ':' + parent.find('.datetimecombo_min').val() + ':' + parent.find('.datetimecombo_sec').val();
        	
        	    var endate_id = date_fld.attr("id");
        	
        	    var end_timezone_id = endate_id.replace(field_index,"timezone_"+field_index);
        	
        	    var end_timezone_label = $("#" + end_timezone_id + " :selected").attr('data-lbl');
        	    var start_timezone_id = dep_fld.replace(field_index,"timezone_"+field_index);
        	
        	    var start_timezone_label = $("#"+start_timezone_id + " :selected").attr('data-lbl');
        	    var dep_parent = $("#"+dep_fld).closest('span');
        	    var start_date =  $("#"+dep_fld).val() + ' ' + dep_parent.find('.datetimecombo_hr').val() + ':' + dep_parent.find('.datetimecombo_min').val() + ':' + dep_parent.find('.datetimecombo_sec').val();
        	    if (start_timezone_label != undefined) {
        	    	start_timezone  = start_timezone_label.substring(start_timezone_label.indexOf('(')+1, start_timezone_label.indexOf(')'));
        	    }
        	    if (end_timezone_label != undefined) {
        	    	end_timezone = end_timezone_label.substring(end_timezone_label.indexOf('(')+1, end_timezone_label.indexOf(')'));
        	    }
        	    var lineitem_start_date = addUTCToDate(start_date,start_timezone);
            	var lineitem_end_date = addUTCToDate(end_date,end_timezone);
            	
                if (lineitem_start_date > lineitem_end_date) {
	            	$("#" + dep_fld).parent().append('<div class="error-message">' + SUGAR.language.get('gc_Contracts', 'LBL_LINEITEM_STARTDATE_ERROR_MSG') + '</div>');
	            	return false;
                }
        	   
        	} else {
        		//message = (dep_fld.indexOf('service') !== -1)? SUGAR.language.get('gc_Contracts', 'LBL_SERVICE_STARTDATE') : SUGAR.language.get('gc_Contracts', 'LBL_BILLING_STARTDATE');;
        		var lbl = getLabel('label_' + dep_fld);
        		removeFromValidate('EditView', dep_fld);
                addToValidate('EditView', dep_fld, 'date', true, lbl); 

        	}
        	
            
            
        }
    } else {
    	 if (date_fld.attr('dependentfield') != '') {
    		 var dep_fld = date_fld.attr('dependentfield');
    		 var lbl = getLabel('label_' + dep_fld);
    		 var dep_parent = $("#"+dep_fld).closest('span');
    		 if (parseInt(parent.find('.datetimecombo_hr').val()) > 0 || parseInt(parent.find('.datetimecombo_min').val()) >0 || parseInt(parent.find('.datetimecombo_sec').val()) > 0) {
    			 removeFromValidate('EditView', dep_fld);
      		     addToValidate('EditView', dep_fld, 'date', true, lbl);
    		 }
    		 else if (parseInt(dep_parent.find('.datetimecombo_hr').val()) == 0 && parseInt(dep_parent.find('.datetimecombo_min').val()) == 0 && parseInt(dep_parent.find('.datetimecombo_sec').val()) == 0) {
               removeFromValidate('EditView', dep_fld);
    		   addToValidate('EditView', dep_fld, 'date', false, lbl); 
    		 }
    	 }
    }
    return true;
}
/**
 * @desc : Function to add a numeric field validation for the line item fields
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function addNumericValidation() {
    var node = parentNode.find('.LineItem');
    node.find('.curr').each(function() {
        $this = $(this);
        var name = $this.attr('id');
        var type = $this.attr('data');
        var lbl = getLabel('label_' + name);
        addToValidate('EditView', name, type, false, lbl);
    });
}
/**
 * @desc : Function to add a required field validation for the line item fields
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function addRequiredValidation(node) {
    if (node == undefined) node = parentNode.find('.LineItem');
    node.find('.req').each(function() {
        if (!$(this).is(':hidden')) {
            $this = $(this);
            var name = $this.attr('id');
            var type = $this.attr('data');
            var lbl = getLabel('label_' + name);
            addToValidate('EditView', name, type, true, lbl);
        } else {
            $this = $(this);
            var name = $this.attr('id');
            removeFromValidate('EditView', name);
        }
    });
}
/**
 * @desc : Function to add a required field validation for the Payment line item
 *       fields
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function addPayemntTypeRequiredValidation() {
    var node = parentNode.find('.PaymentType');
    if (!node.hasClass('hide')) {
        addRequiredValidation(node);
    }
}
/**
 * @desc : Function to setup date field calendar popup after line item is added
 *       to line item panel
 * @params : lineitem <obejct> JQuery object of line item row cloned cal_setup
 *         <object> JQuery object contains calendar setup code to be executed
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function setupCalendarFields(lineitem, cal_setup) {
    var l_no = lineitem.find('#line_item_number').val();
    cal_setup.each(function() {
        $this = $(this);
        var str = $this.html();
        str = str.replace(/_0/g, '_' + l_no);
        eval(str);
    });
}
/**
 * @desc : Function to enable/hide the respective payment information div based
 *       selected payment type on page load
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function initLineItemType() {
    parentNode.find('.payment_type').each(function() {
        togglePayemntType(this, 1);
    });
}
/**
 * @desc : Function to enable/hide the respective payment information div based
 *       selected payment type
 * @params : node <object> DOM Object of select dropdown flag <int> flag to
 *         prevent
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function togglePayemntType(node, flag) {
    clear_all_errors();
    $this = $(node);
    var parent = $this.closest("li");
    var lineitem = parent.find('.subLineItem');
    if ($this.val() == '') lineitem.addClass('hide');
    else lineitem.removeClass('hide');
    lineitem.find('.PaymentType').addClass('hide');
    if ($this.val() == '00') parent.find('.DirectDeposit').removeClass('hide');
    else if ($this.val() == '10') parent.find('.AccountTransfer').removeClass('hide');
    else if ($this.val() == '30') parent.find('.PostalTransfer').removeClass('hide');
    else if ($this.val() == '60') parent.find('.CreditCard').removeClass('hide');
    lineitem.find('.PaymentType').each(function() {
        $this = $(this);
        if (!$this.hasClass("hide")) {
            if (typeof flag == "undefined") $this.find('.felement').focus();
            var ind = delPaymentType.indexOf($this.find('.payment_id').val());
            if (ind != -1) delPaymentType.splice(ind, 1);
        }
    });
}
/**
 * @desc : Function to record the old value of the dropdown
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 31-Dec-2015
 */
function recordOldVlaue(node) {
    $(node).attr('oldval', $(node).val());
}
/**
 * @desc : Function to remove the the line item row on X button click
 * @params : node <object> DOM object of the clicked X button
 * @return :
 * @author : Arunsakthivel.sambandam
 * @date : 18-May-2015
 */
function removeLineItem(node) {
    clear_all_errors();
    $this = $(node);
    var parent = $this.closest("ul");
    if (parentNode.find('.LineItem:visible').length > 1) {
        var alert_message = SUGAR.language.get('gc_Contracts', 'LBL_LINEITEM_REMOVE_ALERT');
        var title_message = SUGAR.language.get('gc_Contracts', 'LBL_LINEITEM_REMOVE_TITLE');
        YAHOO.SUGAR.MessageBox.show({
            msg: alert_message,
            title: title_message,
            type: 'confirm',
            fn: function(confirm) {
                if (confirm == 'yes') {
                    /*if (parent.find('.line_item_id').val() != '') {
                        delLineItem.push(parent.find('.line_item_id').val());
						delLineItemHistory.push(parent.find('.line_item_history_id').val());
						parent.find('.payment_id').each(function() {
                            $n = $(this);
                            if ($n.val() != '')
                                delPaymentType.push($n.attr('data')+ '|' + $n.val());
                        });
					}
					//parent.remove();
					//updateLineItemElementByOrder();
					*/
                    // BEGIN : DF-346 for Removing the exisitng line item even in edit this code combined work for create and edit
                    var rowid = $this.attr("id");
                    rowid = rowid.substr(rowid.lastIndexOf('_') + 1);
                    $("#container_lineitem_" + rowid).hide();
                    $("#li_deleted_" + rowid).val('1');
                    $("#container_lineitem_" + rowid + " :input").attr("disabled", true);
                    $("#line_item_id_" + rowid).attr("disabled", false);
                    $("#li_deleted_" + rowid).attr("disabled", false);
                    // End : DF-346 for Removing the exisitng line item
                    // BEGIN : DF-150 one stop billing 
                    if (one_stop_billing_reference_row == rowid) {
                        var lineItemRows = getLineItemsRowLenth()
                        for (var rowid = one_stop_billing_reference_row + 1; rowid <= lineItemRows; rowid++) {
                            if ($("#li_deleted_" + rowid).val() == '0') {
                                one_stop_billing_reference_row = rowid;
                                break;
                            }
                        }
                        // TO Reinitiate the One stop billing
                        changeLineItemFieldsByBilling_type(false, 0);
                    }
                    // END : DF-150 one stop billing 
                }
            }
        });
    }
}
/**
 * @desc : Function to check base is choosed in product specification
 * @params : 
 * @return : Validation True or False
 * @author : Kamal.Thiyagarajan & Arunsakthivel
 * @date : 12-APR-2016
 */
function addRequiredBaseValidation() {
    if ($("#product_specification_base").val() == "") {
        return true;
    }
    var base_ids = $("#product_specification_base").val();
    var base_id_array = base_ids.split(',');
    var lineItemRows = getLineItemsRowLenth();
    for (var li_rowid = 1; li_rowid <= lineItemRows; li_rowid++) {
        if ($("#li_deleted_" + li_rowid).val() != '1') {
            var product_speci = $("#product_specification_" + li_rowid).val();
            if (base_id_array.indexOf(product_speci) >= 0) {
                return true;
            }
        }
    }
    YAHOO.SUGAR.MessageBox.show({
        msg: SUGAR.language.get('gc_Contracts', 'LBL_BASE_REQUIRED_ALERT'),
        title: 'Error',
        type: 'alert'
    });
    return false;
}
/**
 * @desc : Function to change Contract Line Item Type to Terminate
 * @params : 
 * @return : 
 * @author : Amol Sananse
 * @date : 4-JUL-2016
 */
function terminateLineItem(index) {
    $('#terminate_line_item_' + index).hide();
    $('#cancel_terminate_line_item_' + index).show();
    $('#change_contract_' + index).hide();
    $('#contract_line_item_type_' + index + ' option[value="' + li_status_to_disp_terminate + '"]').removeAttr('disabled');
    $('#contract_line_item_type_' + index + ' option[value="' + li_status_to_disp_change + '"]').attr('disabled', 'disabled');
    $('#contract_line_item_type_' + index).val(li_status_to_disp_terminate);
}
/**
 * @desc : Function to change Contract Line Item Type to Previous stage
 * @params : 
 * @return : 
 * @author : Amol Sananse
 * @date : 4-JUL-2016
 */
function cancelTerminateLineItem(index) {
    $('#cancel_terminate_line_item_' + index).hide();
    $('#terminate_line_item_' + index).show();
    $('#change_contract_' + index).show();
    $('#contract_line_item_type_' + index + ' option[value="' + li_status_to_disp_terminate + '"]').attr('disabled', 'disabled');
    $('#contract_line_item_type_' + index + ' option[value="' + li_status_to_disp_change + '"]').removeAttr('disabled');
    $('#contract_line_item_type_' + index).val(li_status_to_disp_change);
}
/**
 * @desc : Function to check all line items are marked as Terminate
 * @params : 
 * @return : Validation True or False
 * @author : Amol Sananse
 * @date : 5-JUL-2016
 */
function checkAllLineItemsAreTerminate() {
    var lineItemRows = getLineItemsRowLenth();
    var lineItemTerminationRows = 0;
    var deleteditems = 0;
    for (var li_rowid = 1; li_rowid <= lineItemRows; li_rowid++) {
        var is_deleted = $("#li_deleted_" + li_rowid).val();
        if (is_deleted == '1') {
            deleteditems++;
        }
    }
    if (parseInt(deleteditems) > 0) {
        lineItemRows = parseInt(lineItemRows) - parseInt(deleteditems);
    }
    for (var li_rowid = 1; li_rowid <= lineItemRows; li_rowid++) {
        if ($('#contract_line_item_type_' + li_rowid).val() == li_status_to_disp_terminate) {
            lineItemTerminationRows++;
        }
    }
    if (lineItemRows == lineItemTerminationRows) {
        YAHOO.SUGAR.MessageBox.show({
            msg: SUGAR.language.get('gc_Contracts', 'LBL_ALL_LINE_ITEMS_TERMINATION_ALERT'),
            title: 'Error',
            type: 'alert'
        });
        return false;
    }
    return true;
}
/**
 * @desc : Function to check all base product are marked as service_termination
 * @params : 
 * @return : Validation True or False
 * @author : Amol.Sananse
 * @date : 5-JUL-2016
 */
function checkAllBaseAreInTermination() {
    var base_ids = $("#product_specification_base").val();
    var base_id_array = base_ids.split(',');
    var lineItemRows = getLineItemsRowLenth();
    for (var li_rowid = 1; li_rowid <= lineItemRows; li_rowid++) {
        if ($("#li_deleted_" + li_rowid).val() != '1') {
            var product_speci = $("#product_specification_" + li_rowid).val();
            if (base_id_array.indexOf(product_speci) >= 0 && $("#contract_line_item_type_" + li_rowid).val() != li_status_to_disp_terminate) {
                return true;
            }
        }
    }
    YAHOO.SUGAR.MessageBox.show({
        msg: SUGAR.language.get('gc_Contracts', 'LBL_BASE_REQUIRED_ALERT'),
        title: 'Error',
        type: 'alert'
    });
    return false;
}

function disableLineItemTimeZone(element)
{
	var id = element;
	if (id.indexOf('service') !== -1 || id.indexOf('billing') !== -1) {
		 var res = id.split("_");
		 var field_index = res[3]; 
		 var timezone_id = id.replace(field_index,"timezone");
		 if(parseInt($("#"+element).val())>0) {
			 $('#'+timezone_id+' option').prop('disabled', false);
			 if ($("#"+timezone_id).val() == "") {
				 $("#"+timezone_id+" option[data-lbl='UTC (+00:00)']").attr('selected', 'selected');
			 }
		 } else {
			 var hours_id = id.replace(field_index,"hours");
			 
			 var mins_id = id.replace(field_index,"minutes");
			 var secs_id = id.replace(field_index,"seconds");
			 var date_fld = id.replace(field_index,"");
			 date_fld = date_fld.replace("__","_");
			 if (parseInt($("#"+hours_id).val()) == 0 && parseInt($("#"+mins_id).val()) == 0 && parseInt($("#"+secs_id).val()) == 0 && $("#"+date_fld).val() == "") {
				 $("#"+timezone_id).val("");
				 markFieldsReadOnly(timezone_id);
			 } else {
				 $('#'+timezone_id+' option').prop('disabled', false);
				 if ($("#"+timezone_id).val() == "") {
					 $("#"+timezone_id+" option[data-lbl='UTC (+00:00)']").attr('selected', 'selected');
				 }
			 }
		 }
	}
}

function disableTimeZoneOnLoad()
{
	var node = parentNode.find('.LineItem');
    node.find('.date_input').each(function() {
    	$this = $(this);
        var name = $this.attr('id');
        var type = $this.attr('data');
        var lbl = getLabel('label_' + name);
        var node =  $this;
        var parent = node.closest('span');
        var date_fld = parent.find('.date_input');
        var res = name.split("_");
		var field_index = res[3]; 
		YAHOO.util.Event.onDOMReady(function(){
			YAHOO.util.Event.removeListener(name,"change",onSelectDateTime);
			YAHOO.util.Event.addListener(name,"change",onSelectDateTime);
			
		});
		var timezone_id = name.replace(field_index,"timezone_"+field_index);
        if ((parseInt(parent.find('.datetimecombo_hr').val())>0 || parseInt(parent.find('.datetimecombo_min').val())>0 || parseInt(parent.find('.datetimecombo_sec').val())>0) || (date_fld.val() != '' && date_fld.val() != 'undefined')) {
        	
   		    $('#'+timezone_id+' option').prop('disabled', false);
   		    if ($("#"+timezone_id).val() == "") {
   		    	$("#"+timezone_id+" option[data-lbl='UTC (+00:00)']").attr('selected', 'selected');
		    }
        } else {
        	$("#"+timezone_id).val("");
			 markFieldsReadOnly(timezone_id);
        }
    });
}

function addPaymentValidation()
{
	//alert($(".payment_type").find('option:selected').val());
	var error_count = true;
	var node = parentNode.find('.LineItem');
    node.find('.payment_type').each(function() {
    	$this = $(this);
    	switch ($this.val()) {
    	case '00' :
    		error_count = directDepositValidation($this.attr("id"));
    		break;
    	case '10' :
    		error_count = accountTransferValidation($this.attr("id"));
    		break;
    	case '30' :
    		error_count = postalTransferValidation($this.attr("id"));
    		break;
    	case '60' :
    		error_count = creditCardValidation($this.attr("id"));
    		break;
    	}
    });
    return error_count;
}

function directDepositValidation(element)
{
	var res = element.split("_");
	var field_index = res[2]; 
	var mandatory = false;
	var dd_fields = new Array('dd_bank_code_', 'dd_branch_code_', 'dd_deposit_type_', 'dd_account_no_', 'dd_person_name_2_', 'dd_payee_tel_no_', 'dd_payee_email_address_', 'dd_remarks_');
	var error_count = 0;
	for (var key in dd_fields) {
		var field_id = dd_fields[key]+field_index;
		var field_value = $("#"+field_id).val();
		if ($.trim(field_value) != "") {
			mandatory = true;
		}
		if (dd_fields[key] == 'dd_bank_code_' || dd_fields[key] == 'dd_branch_code_' || dd_fields[key] == 'dd_account_no_' || dd_fields[key] == 'dd_payee_tel_no_' || 'dd_payee_email_address_') {
			
			if (field_value != "") {
				var regexp = "";
				var msg = "";
				if (dd_fields[key] == 'dd_bank_code_') {
					regexp = /^[0-8a-zA-Z]{2}[0-9a-zA-Z]{2}$/;
				    msg = SUGAR.language.get('gc_Contracts', 'LBL_BANK_CODE_ERROR');
				}
				else if (dd_fields[key] == 'dd_branch_code_') {
					regexp = /^[0-9a-zA-Z]{3}$/;
				    msg = SUGAR.language.get('gc_Contracts', 'LBL_BRANCH_CODE_ERROR');
				}
				else if (dd_fields[key] == 'dd_account_no_') {
					regexp = /^\d{7}$/;
				    msg = SUGAR.language.get('gc_Contracts', 'LBL_ACCOUNT_NO_ERROR');
				}
				else if (dd_fields[key] == 'dd_payee_tel_no_') {
					regexp = /^[+]?(\d+[-]?)+\d+$/;
				    msg = SUGAR.language.get('gc_Contracts', 'LBL_TEL_NO_ERROR');
				}
                else if(dd_fields[key] == 'dd_payee_email_address_' && !checkValidEmail($("#"+field_id)))
                {
                    error_count = error_count+1;
                }

				if(!field_value.match(regexp)) {
					$("#"+field_id).after('<div class="error">'+msg+'</div>');
					$("#"+field_id).css('backgroundColor', "#FF0000");
					$("#"+field_id).animate({backgroundColor: ''}, 2000);
					error_count = error_count+1;
				}   
			}
		}
	 }
	if (mandatory) {
		bank_code_lbl = getLabel('label_' + 'dd_bank_code_'+field_index);
        addToValidate('EditView', 'dd_bank_code_'+field_index, 'text', true, bank_code_lbl);
        branch_code_lbl = getLabel('label_' + 'dd_branch_code_'+field_index);
        addToValidate('EditView', 'dd_branch_code_'+field_index, 'text', true, branch_code_lbl);
        deposit_type_lbl = getLabel('label_' + 'dd_deposit_type_'+field_index);
        addToValidate('EditView', 'dd_deposit_type_'+field_index, 'text', true, deposit_type_lbl);
        account_no_lbl = getLabel('label_' + 'dd_account_no_'+field_index);
        addToValidate('EditView', 'dd_account_no_'+field_index, 'text', true, account_no_lbl);
	} else {
		removeFromValidate('EditView', 'dd_bank_code_'+field_index);
		removeFromValidate('EditView', 'dd_branch_code_'+field_index);
		removeFromValidate('EditView', 'dd_deposit_type_'+field_index);
		removeFromValidate('EditView', 'dd_account_no_'+field_index);	
	}
	if (error_count > 0) return false;
	return true;
}

function accountTransferValidation(element)
{
	var res = element.split("_");
	var field_index = res[2]; 
	var dd_fields = new Array('at_bank_code_', 'at_branch_code_', 'at_account_no_');
	var error_count = 0;
	for (var key in dd_fields) {
		var field_id = dd_fields[key]+field_index;
		var field_value = $("#"+field_id).val();
		if (field_value != "") {
			var regexp = "";
			var msg = "";
			if (dd_fields[key] == 'at_bank_code_') {
				regexp = /^[0-8a-zA-Z]{2}[0-9a-zA-Z]{2}$/;
				msg = SUGAR.language.get('gc_Contracts', 'LBL_BANK_CODE_ERROR');
			}
			if (dd_fields[key] == 'at_branch_code_') {
				regexp = /^[0-9a-zA-Z]{3}$/;
				msg = SUGAR.language.get('gc_Contracts', 'LBL_BRANCH_CODE_ERROR');
			}
			if (dd_fields[key] == 'at_account_no_') {
				regexp = /^\d{7}$/;
				msg = SUGAR.language.get('gc_Contracts', 'LBL_ACCOUNT_NO_ERROR');
			}
			if(!field_value.match(regexp)) {
				$("#"+field_id).after('<div class="error">'+msg+'</div>');
				$("#"+field_id).css('backgroundColor', "#FF0000");
				$("#"+field_id).animate({backgroundColor: ''}, 2000);
				error_count = error_count+1;
			}
		}
	 }
	if (error_count > 0) return false;
	return true;
}

function postalTransferValidation(element)
{
	var res = element.split("_");
	var field_index = res[2]; 
	var dd_fields = new Array('pt_postal_passbook_mark_', 'pt_postal_passbook_');
	var error_count = 0;
	for (var key in dd_fields) {
		var field_id = dd_fields[key]+field_index;
		var field_value = $("#"+field_id).val();
		if (field_value != "") {
			var regexp = "";
			var msg = "";
			if (dd_fields[key] == 'pt_postal_passbook_mark_') {
				regexp = /^\d{5}$/;
				msg = SUGAR.language.get('gc_Contracts', 'LBL_POSTAL_PASSBOOK_ERROR');
			}
			if (dd_fields[key] == 'pt_postal_passbook_') {
				regexp = /^\d{7}[1-39]$/;
				msg = SUGAR.language.get('gc_Contracts', 'LBL_POSTAL_NO_ERROR');
			}
			if(!field_value.match(regexp)) {
				$("#"+field_id).after('<div class="error">'+msg+'</div>');
				$("#"+field_id).css('backgroundColor', "#FF0000");
				$("#"+field_id).animate({backgroundColor: ''}, 2000);
				error_count = error_count+1;
			}
		}
	 }
	if (error_count > 0) return false;
	return true;
}

function creditCardValidation(element)
{
	var res = element.split("_");
	var field_index = res[2]; 
	var dd_fields = new Array('cc_credit_card_no_', 'cc_expiration_date_');
	var error_count = 0;
	for (var key in dd_fields) {
		var field_id = dd_fields[key]+field_index;
		var field_value = $("#"+field_id).val();
		if (field_value != "") {
			var regexp = "";
			var msg = "";
			if (dd_fields[key] == 'cc_credit_card_no_') {
				regexp = /^\d{16}$/;
				msg = SUGAR.language.get('gc_Contracts', 'LBL_CREDIT_NO_ERROR');
			}
			if (dd_fields[key] == 'cc_expiration_date_') {
				regexp = /^\d{4}(1[0-2]|0[1-9])$/;
				msg = SUGAR.language.get('gc_Contracts', 'LBL_CREDIT_EXP_ERROR');
			}
			if(!field_value.match(regexp)) {
				$("#"+field_id).after('<div class="error">'+msg+'</div>');
				$("#"+field_id).css('backgroundColor', "#FF0000");
				$("#"+field_id).animate({backgroundColor: ''}, 2000);
				error_count = error_count+1;
			}
		}
	 }
	if (error_count > 0) return false;
	return true;
}