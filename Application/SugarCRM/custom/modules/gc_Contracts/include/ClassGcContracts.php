<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once ('custom/modules/gc_Contracts/CustomFunctionGcContract.php');
require_once ('custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php');

/**
 * class contains all required methods for contract related activities
 *
 * @author sagar.salunkhe
 */
class ClassGcContracts extends CustomFunctionGcContract
{

    private $api_strings;

    protected $default_api_utc = '+00:00';

    protected $flag_api_request = false;

    private $invoice_group_list = array();

    function __construct()
    {
        // module class file constructor
        global $app_strings;
        $this->api_strings = $app_strings['custom_api_service'];
    }

    /**
     * method generates unique global contract id
     *
     * @throws Exception
     */
    protected function generateGlobalContractId()
    {
        global $db;
        global $sugar_config;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'default_sequence_start : ' . $sugar_config['default_sequence_start']['gc_Contracts'], 'method generates unique global contract id');
        if (empty($sugar_config['default_sequence_start']['gc_Contracts'])) {
            throw new Exception("Configuration value is not defined. - default_sequence_start");
        }
        $prefix = substr($sugar_config['default_sequence_start']['gc_Contracts'], 0, 2);

        $insSql = "INSERT INTO gc_contracts_sequence(dummy) VALUES ('')";
        $db->query($insSql);
        $getSql = "SELECT LAST_INSERT_ID() AS SEQ FROM gc_contracts_sequence";
        $result = $db->query($getSql);
        if (($row = $db->fetchByAssoc($result, false)) != null) {
            $seq = $row['SEQ'];
        } else {
            throw new Exception("Can't get new GlobalContractId value from gc_contracts_sequence.");
        }

        if ($seq == 0) {
            throw new Exception("Can't get new GlobalContractId value from gc_contracts_sequence.");
        }

        return sprintf("%02s%013d", $prefix, $seq); // Convert from number to string.
    }

    /**
     * generate line item sequence number for each contract sequence (global contract id will remain same)
     *
     * @param string $contract_id contract record id
     * @param string $line_item_id_uuid line item uuid
     * @return string incremented line item number
     *
     * @author sagar.salunkhe
     * @since Oct 10, 2015
     */
    protected function generateLineItemId($contract_id, $line_item_id_uuid)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $contract_id . GCM_GL_LOG_VAR_SEPARATOR . 'line_item_id_uuid : ' . $line_item_id_uuid, 'generate line item sequence number for each contract sequence (global contract id will remain same)');

        global $sugar_config;
        $contract_versions_id_list = array();

        if (!isset($sugar_config['default_sequence_start']['gc_LineItem'])) {
            $sugar_config['default_sequence_start']['gc_LineItem'] = 100001;
        }

        $res_line_item_id = $sugar_config['default_sequence_start']['gc_LineItem'];

        $obj_contracts_header_bean = BeanFactory::getBean('gc_Contracts');

        if (!empty($line_item_id_uuid)) {
            $prev_line_item_history_version = BeanFactory::getBean('gc_Line_Item_Contract_History');
            $prev_line_item_history_version->retrieve_by_string_fields(array(
                                                                            'line_item_id_uuid' => $line_item_id_uuid
            ));
            if ($prev_line_item_history_version != null && isset($prev_line_item_history_version->line_item_id)) {
            	$line_item_id = $prev_line_item_history_version->line_item_id;
            	$prev_line_item_version = BeanFactory::getBean('gc_LineItem')->retrieve($line_item_id);
            	if (!empty($prev_line_item_version->name)) {
            		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, ' name : ' . $prev_line_item_version->name, 'generate line item sequence number for each contract sequence (global contract id will remain same)');
            		return $prev_line_item_version->name;
            	}
            }
        }

        // if line item uuid not found in db (scenario : when field_xml_id is passed as xml tag id)
        if (!empty($contract_id)) {

            $obj_contracts_header_bean->retrieve($contract_id);
            // get all versions of contracts
            $where_cond = "global_contract_id='" . $obj_contracts_header_bean->global_contract_id . "'";
            $contract_list = BeanFactory::getBean('gc_Contracts')->get_full_list('contract_version desc', $where_cond);
            foreach ($contract_list as $i => $contract_object)
                $contract_versions_id_list[] = $contract_object->id;

            if (count($contract_versions_id_list) > 0) {

                $arr_item_history_beans = $contract_item_id_list = $line_numbers = array();

                // get all line item contract history records
                $obj_line_contract_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
                $quotes_contract_ids = implode(',', array_map('add_quotes', $contract_versions_id_list));
                $line_history_where = "contracts_id IN ($quotes_contract_ids)";

                $arr_item_history_live = $obj_line_contract_history_bean->get_full_list('', $line_history_where);
                $arr_item_history_deleted = $obj_line_contract_history_bean->get_full_list('', $line_history_where, false, 1);

                if (!is_array($arr_item_history_live)) {
                    $arr_item_history_live = array();
                }
                if (!is_array($arr_item_history_deleted)) {
                    $arr_item_history_deleted = array();
                }

                $arr_item_history_beans = array_merge($arr_item_history_live, $arr_item_history_deleted);
                foreach ($arr_item_history_beans as $i => $contract_hist_object)
                    $contract_item_id_list[] = $contract_hist_object->line_item_id;

                if (count($contract_item_id_list) > 0) {
                    // get all line item ids with global contract line item ids
                    $obj_contract_line_item_bean = BeanFactory::getBean('gc_LineItem');
                    $quotes_line_item_ids = implode(',', array_map('add_quotes', $contract_item_id_list));
                    $line_item_where = "gc_lineitem.id IN ($quotes_line_item_ids)";

                    $arr_line_item_beans_live = $arr_line_item_beans_deleted = array();
                    $arr_line_item_beans_live = $obj_contract_line_item_bean->get_full_list('', $line_item_where);
                    $arr_line_item_beans_deleted = $obj_contract_line_item_bean->get_full_list('', $line_item_where, false, 1);

                    if (!is_array($arr_line_item_beans_live)) {
                        $arr_line_item_beans_live = array();
                    }

                    if (!is_array($arr_line_item_beans_deleted)) {
                        $arr_line_item_beans_deleted = array();
                    }

                    $arr_line_item_beans = array_merge($arr_line_item_beans_live, $arr_line_item_beans_deleted);

                    // get max number of line item id from all versions
                    foreach ($arr_line_item_beans as $i => $obj_line_bean) {
                        $line_numbers[] = $obj_line_bean->name;
                    }

                    if (count($line_numbers) > 0) {
                        $res_line_item_id = max($line_numbers) + 1;
                    }
                }
            }
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'res_line_item_id : ' . $res_line_item_id, 'generate line item sequence number for each contract sequence (global contract id will remain same)');
        return $res_line_item_id;
    }

    /**
     * method will return master list of product offerings
     *
     * @param string $status_filter
     * @return array
     * @author arun.sakthivel
     */
    public function getProductOfferings($status_filter)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'status_filter : ' . $status_filter, 'method will return master list of product offerings');
        $obj_product = new ClassProductConfiguration();
        $offering_list = $obj_product->getProductOfferings($status_filter);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'offering_list : ' . print_r($offering_list, true), 'method will return master list of product offerings');
        return $offering_list['response_code'] == '1' ? $offering_list['respose_array'] : array();
    }

    /**
     * method will return list of currencies from gcm custom currency array list
     */
    public function getconfig_currencies()
    {
        include ('custom/modules/Currencies/CustomCurrencyList.php');

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'config_currencies : ' . print_r($gcm_currency_list, true), 'method will return list of currencies from gcm custom currency array list');
        $config_currencies = $gcm_currency_list;
        $response_currencies = array();

        foreach ($config_currencies as $k => $currency) {
            $unicode_display = '';
            foreach ($currency['unicode'] as $char) {
                $char = trim($char);
                $char = html_entity_decode('&#' . $char . ';', ENT_QUOTES);
                $unicode_display .= $char;
            }
            $response_currencies[$currency['code']] = $currency['code'] . ' (' . $unicode_display . ')';
        }
        asort($response_currencies);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response_currencies : ' . print_r($response_currencies, true), 'method will return list of currencies from gcm custom currency array list');
        return $response_currencies;
    }

    /**
     * Function to return the product offering details
     *
     * @param string $product_offering_id
     *
     * @author kamal.thiyagarajan
     */
    public function getProductOfferingDetails($product_offering_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_offering_id : ' . $product_offering_id, 'Function to return the product offering details');
        $obj_product = new ClassProductConfiguration();
        return $obj_product->getProductOfferingDetails($product_offering_id);
    }

    /**
     * method to Save Contract Sales Representative Details
     *
     * @param array $array_contract_sales_rep_params array with required details to save contract sales represetative record
     * @param string $contract_id Contract Object Record Id
     * @return object method will return Sales Representative module sugar bean object
     *
     * @author sagar.salunkhe
     * @since Oct 08, 2015
     */
    protected function setContractSalesRepEntryService($array_contract_sales_rep_params, $contract_id, $flag_update, $is_sugar_request)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array_contract_sales_rep_params : ' . print_r($array_contract_sales_rep_params, true) . GCM_GL_LOG_VAR_SEPARATOR . 'contract_id : ' . $contract_id . GCM_GL_LOG_VAR_SEPARATOR . 'flag_update : ' . $flag_update . GCM_GL_LOG_VAR_SEPARATOR . 'is_sugar_request : ' . $is_sugar_request, 'method to Save Contract Sales Representative Details');

        $obj_sales_representative_bean = BeanFactory::getBean('gc_SalesRepresentative');

        if (!isset($array_contract_sales_rep_params) || empty($array_contract_sales_rep_params) || count($array_contract_sales_rep_params) == 0) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', $this->api_strings['ERR_EMPTY_REQ_PARAMS'] . $this->api_strings['MSG_NO_RECORD_UPDATE']);

            return $obj_sales_representative_bean;
        }

        // retrieve sales representative record from Contract id
        if ($flag_update) {
            $obj_sales_representative_bean->retrieve_by_string_fields(array(
                                                                            'contracts_id' => $contract_id
            ));
            $obj_sales_representative_bean->retrieve($obj_sales_representative_bean->id);

            ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'Retrieved Record Id : ' . $obj_sales_representative_bean->id, '');
        }

        // fetch ntt sales company id when request is from api
        if (!isset($array_contract_sales_rep_params['gc_nttcomgroupcompany_id_c']) && isset($array_contract_sales_rep_params['sales_company'])) {
            if (trim($array_contract_sales_rep_params['sales_company']) == '')
                $array_contract_sales_rep_params['gc_nttcomgroupcompany_id_c'] = '';
            else {
                $obj_ntt_sales_company = BeanFactory::getBean('gc_NTTComGroupCompany');
                $obj_ntt_sales_company->retrieve_by_string_fields(array(
                                                                        'name' => $array_contract_sales_rep_params['sales_company']
                ));
                $array_contract_sales_rep_params['gc_nttcomgroupcompany_id_c'] = $obj_ntt_sales_company->id;
                unset($obj_ntt_sales_company);
            }
        }

        // set array fields into Sales Representative bean object
        foreach ($array_contract_sales_rep_params as $field => $value)
            $obj_sales_representative_bean->$field = $value;

            // Contracts CRM Record Id
        $obj_sales_representative_bean->contracts_id = $contract_id;
        $obj_sales_representative_bean->team_id = 1;
        $obj_sales_representative_bean->team_set_id = 1;
        $obj_sales_representative_bean->save();

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'sales_rep_id : ' . $obj_sales_representative_bean->id, $this->api_strings['MSG_RECORD_UPDATE_SUCCESS']);

        return $obj_sales_representative_bean;
    }

    /**
     * method to set Contract Line Item Details
     *
     * @param array $array_contract_line_params array with required details to save contract line item record
     * @param string $contract_id line item related contract record id
     * @param string $tech_account_id Technical Account CRM record Id
     * @param string $billing_account_id Billing Account CRM record Id
     * @param boolean $flag_update flag to check whether to update or insert the record
     * @return object Contract Line Item Header record object/bean
     *
     * @author sagar.salunkhe
     * @since Oct 08, 2015
     */
    protected function setContractLineItemEntryService($array_contract_line_params, $contract_id, $tech_account_id, $billing_account_id, $flag_update, &$api_response)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array_contract_line_params : ' . print_r($array_contract_line_params, true) . GCM_GL_LOG_VAR_SEPARATOR . 'contract_id : ' . $contract_id . GCM_GL_LOG_VAR_SEPARATOR . 'tech_account_id : ' . $tech_account_id . 'flag_update : ' . $flag_update . GCM_GL_LOG_VAR_SEPARATOR . 'api_response : ' . print_r($api_response, true), 'method to set Contract Line Item Details');
        $obj_modutils = new ModUtils();
        $obj_contract_line_item_bean = BeanFactory::getBean('gc_LineItem');

        $line_item_id_uuid = '';

        if ($flag_update && isset($array_contract_line_params['line_item_id'])) {
            $obj_contract_line_item_bean->retrieve($array_contract_line_params['line_item_id']);
        } else {

            if (array_key_exists('previous_version_id', $array_contract_line_params['ContractLineItemHeader']) && !empty($array_contract_line_params['ContractLineItemHeader']['previous_version_id'])) {
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'setContractLineItemEntryService_previous version id:  ' . $array_contract_line_params['ContractLineItemHeader']['previous_version_id'], '');
                $ob_prev_version_line_history = BeanFactory::getBean('gc_Line_Item_Contract_History');
                $ob_prev_version_line_history->retrieve($array_contract_line_params['ContractLineItemHeader']['previous_version_id']);
                $line_item_id_uuid = $ob_prev_version_line_history->line_item_id_uuid;
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'setContractLineItemEntryService_$line_item_id_uuid:  ' . $line_item_id_uuid, '');
            } elseif (array_key_exists('line_item_id_xml_id', $array_contract_line_params['ContractLineItemHeader'])) {
                $line_item_id_uuid = $array_contract_line_params['ContractLineItemHeader']['line_item_id_xml_id'];
            }
        }
        if ($obj_contract_line_item_bean->name == '')
            $obj_contract_line_item_bean->name = $this->generateLineItemId($contract_id, $line_item_id_uuid); // auto-generated id

        if (isset($array_contract_line_params['ContractLineItems']['product_id']))
            $obj_contract_line_item_bean->products_id = $array_contract_line_params['ContractLineItems']['product_id']; // Products CRM Record Id

        $obj_contract_line_item_bean->product_quantity = 1; // Product Quantity
        $obj_contract_line_item_bean->team_id = 1;
        $obj_contract_line_item_bean->team_set_id = 1;
        $obj_contract_line_item_bean->save();
        
        //DF-1485 delete old payment type
        $obj_line_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
        if ($flag_update && isset($array_contract_line_params['ContractLineItemHeader']['line_item_history_id']) && (isset($array_contract_line_params['ContractLineItemHeader']['payment_type_flag']) && $array_contract_line_params['ContractLineItemHeader']['payment_type_flag'] == 1)) {
        	$obj_line_history_bean_object = $obj_line_history_bean->retrieve($array_contract_line_params['ContractLineItemHeader']['line_item_history_id']);
        	if(is_object($obj_line_history_bean_object)) {
        		switch ((string) $obj_line_history_bean_object->payment_type) {
        			case '00':
        				$payment_mode_bean = BeanFactory::getBean('gc_DirectDeposit');
        				break;
        			case '10':
        			    $payment_mode_bean = BeanFactory::getBean('gc_AccountTransfer');
        			    break;
        		    case '30':
        			    $payment_mode_bean = BeanFactory::getBean('gc_PostalTransfer');
        			    break;
        		    case '60':
        			    $payment_mode_bean = BeanFactory::getBean('gc_CreditCard');
        			    break;
        		    default:
        			   $payment_mode_bean = '';
        			   break;
        		}
        		if (is_object($payment_mode_bean)) {
        			$payment_mode_arr = $payment_mode_bean->get_full_list('', "contract_history_id = '{$array_contract_line_params['ContractLineItemHeader']['line_item_history_id']}'");
        			if ($payment_mode_arr != null) {
        				foreach ($payment_mode_arr as $payment_mode_list) {
        					$payment_mode_list->deleted = 1;
        				    $payment_mode_list->save();
        				}
        			}
        		}
        	}
        }
        $contract_line_contract_history = $this->setContractLineContractHistoryService($array_contract_line_params['ContractLineItemHeader'], $contract_id, $obj_contract_line_item_bean->id, $tech_account_id, $billing_account_id, $flag_update);

        if (isset($array_contract_line_params['InvoiceGroupDtls']) && count($array_contract_line_params['InvoiceGroupDtls']) > 0) {

            foreach ($this->invoice_group_list as $invoice_gp_name => $invoice_gp_id) {
                if ($array_contract_line_params['InvoiceGroupDtls']['name'] == $invoice_gp_name) {
                    $array_contract_line_params['InvoiceGroupDtls']['uuid'] = $invoice_gp_id;
                }
            }

            $array_contract_line_params['InvoiceGroupDtls']['contract_id'] = array(
                                                                                $contract_id
            );
            $obj_invoice_group = $this->setInvoiceGroupEntryService($array_contract_line_params['InvoiceGroupDtls'], $flag_update);
            $obj_invoice_group->load_relationship('gc_invoicesubtotalgroup_gc_line_item_contract_history_1');
            $obj_invoice_group->gc_invoicesubtotalgroup_gc_line_item_contract_history_1->add(array(
                                                                                                    $contract_line_contract_history->id
            ));

            $obj_modutils->generatePostXmlResponseData($array_contract_line_params['InvoiceGroupDtls'], $api_response['InvoiceGroupDtls']['local_uuid'], $obj_invoice_group);
        }

        $obj_contract_line_item_bean->line_item_contract_history_id = $contract_line_contract_history->id; // pass respective line item contract history id in line item object
        //DF-1485 delete old price block
        if ($flag_update && (isset($array_contract_line_params['ContractLineItemHeader']['cust_contract_currency_flag']) && $array_contract_line_params['ContractLineItemHeader']['cust_contract_currency_flag'] == 1)) {
        	$product_price_bean_obj = BeanFactory::getBean('gc_ProductPrice');
        	$price_block_arr = $product_price_bean_obj->get_full_list('', "line_item_id = '{$obj_contract_line_item_bean->id}'");
        	if ($price_block_arr != null) {
        		foreach ($price_block_arr as $price_block_list) {
        			$price_block_list->deleted = 1;
        			$price_block_list->save();
        		}
        	}
        }
        
        // jira #df-149 @desc insert contract line Product Specification and Product Price only when product specification field is selected by user in line items
        if (isset($array_contract_line_params['ContractLineItemHeader']['product_specification']) && !empty($array_contract_line_params['ContractLineItemHeader']['product_specification'])) {

            // Insert contract line product configuration, if array is passed and not empty
            if (isset($array_contract_line_params['ContractLineProductConfiguration']) && !empty($array_contract_line_params['ContractLineProductConfiguration'])) {
                foreach ($array_contract_line_params['ContractLineProductConfiguration'] as $index => $prod_conf) {
                    $obj_product_configuration_bean = $this->setProductConfigurationEntryService($prod_conf, $obj_contract_line_item_bean->id);
                    $obj_modutils->generatePostXmlResponseData($prod_conf, $api_response['ContractLineProductConfiguration'][$index]['local_uuid'], $obj_product_configuration_bean);
                }
            }
            
            
            // Insert contract line product price, if array is passed and not empty
            if (isset($array_contract_line_params['ContractLineProductPrice']) && !empty($array_contract_line_params['ContractLineProductPrice'])) {
                foreach ($array_contract_line_params['ContractLineProductPrice'] as $index => $prod_price) {
                    $obj_product_price_bean = $this->setProductPriceEntryService($prod_price, $obj_contract_line_item_bean->id);
                    $obj_modutils->generatePostXmlResponseData($prod_price, $api_response['ContractLineProductPrice'][$index]['local_uuid'], $obj_product_price_bean);
                }
            }
            // Insert contract line product rule, if array is passed and not empty
            if (isset($array_contract_line_params['ContractLineProductRule']) && !empty($array_contract_line_params['ContractLineProductRule'])) {
                foreach ($array_contract_line_params['ContractLineProductRule'] as $index => $prod_rule) {
                    $obj_product_rule_bean = $this->setProductRuleEntryService($prod_rule, $obj_contract_line_item_bean->id);
                    $obj_modutils->generatePostXmlResponseData($prod_rule, $api_response['ContractLineProductRule'][$index]['local_uuid'], $obj_product_rule_bean);
                }
            }
        }  else if ($flag_update && isset($array_contract_line_params['ContractLineProductPrice']) && !empty($array_contract_line_params['ContractLineProductPrice'])) { // Insert contract line product price, if array is passed, not empty and if it is patch.
        	foreach ($array_contract_line_params['ContractLineProductPrice'] as $index => $prod_price) {
        		$obj_product_price_bean = $this->setProductPriceEntryService($prod_price, $obj_contract_line_item_bean->id);
        	}
        }

        if (isset($array_contract_line_params['ContractDirectDeposit']) && count($array_contract_line_params['ContractDirectDeposit']) > 0) {
            if (isset($array_contract_line_params['ContractDirectDeposit']['id']) && $array_contract_line_params['ContractDirectDeposit']['id'] == '')
                $flag_update = false;
            $this->setDirectDepositService($array_contract_line_params['ContractDirectDeposit'], $contract_line_contract_history->id, $flag_update);
        } elseif (isset($array_contract_line_params['ContractAccountTransfer']) && count($array_contract_line_params['ContractAccountTransfer']) > 0) {
            if (isset($array_contract_line_params['ContractAccountTransfer']['id']) && $array_contract_line_params['ContractAccountTransfer']['id'] == '')
                $flag_update = false;
            $this->setAccountTransferService($array_contract_line_params['ContractAccountTransfer'], $contract_line_contract_history->id, $flag_update);
        } elseif (isset($array_contract_line_params['ContractPostalTransfer']) && count($array_contract_line_params['ContractPostalTransfer']) > 0) {
            if (isset($array_contract_line_params['ContractPostalTransfer']['id']) && $array_contract_line_params['ContractPostalTransfer']['id'] == '')
                $flag_update = false;
            $this->setPostalTransferService($array_contract_line_params['ContractPostalTransfer'], $contract_line_contract_history->id, $flag_update);
        } elseif (isset($array_contract_line_params['ContractCreditCard']) && count($array_contract_line_params['ContractCreditCard']) > 0) {
            if (isset($array_contract_line_params['ContractCreditCard']['id']) && $array_contract_line_params['ContractCreditCard']['id'] == '')
                $flag_update = false;
            $this->setCreditCardService($array_contract_line_params['ContractCreditCard'], $contract_line_contract_history->id, $flag_update);
        }

        $obj_modutils->generatePostXmlResponseData($array_contract_line_params['ContractLineItemHeader'], $api_response['ContractLineItems']['local_uuid'], $contract_line_contract_history);

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, $this->api_strings['MSG_RECORD_UPDATE_SUCCESS'] . $obj_contract_line_item_bean->id, '');
        return $obj_contract_line_item_bean;
    }

    /**
     * method to Save Contract Line Item history Details
     *
     * @param array $array_line_contract_history_params array with required details to save contract line item history record
     * @param string $contract_id related contract record id
     * @param string $contract_line_item_id related contract line item record id
     * @param string $tech_account_id Technical Account CRM record Id
     * @param string $billing_account_id Billing Account CRM record Id
     * @param boolean $flag_update flag to check if it is update operation
     * @return object Contract Line Item history record object/bean
     *
     * @author sagar.salunkhe
     * @since Oct 08, 2015
     */
    protected function setContractLineContractHistoryService($array_line_contract_history_params, $contract_id, $contract_line_item_id, $tech_account_id, $billing_account_id, $flag_update)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array_line_contract_history_params : ' . print_r($array_line_contract_history_params, true) . GCM_GL_LOG_VAR_SEPARATOR . 'contract_id : ' . $contract_id . GCM_GL_LOG_VAR_SEPARATOR . 'contract_line_item_id : ' . $contract_line_item_id . GCM_GL_LOG_VAR_SEPARATOR . 'tech_account_id : ' . $tech_account_id . GCM_GL_LOG_VAR_SEPARATOR . 'billing_account_id : ' . $billing_account_id . GCM_GL_LOG_VAR_SEPARATOR . 'flag_update : ' . $flag_update, 'method to Save Contract Line Item history Details');
        $obj_line_contract_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');

        if ($flag_update && isset($array_line_contract_history_params['line_item_history_id']))
            $obj_line_contract_history_bean->retrieve($array_line_contract_history_params['line_item_history_id']);

        if (isset($array_line_contract_history_params['service_start_date_timezone']) && !isset($array_line_contract_history_params['gc_timezone_id_c'])){
            if($flag_update || (!$flag_update && isset($array_line_contract_history_params['service_start_date'])  && !empty($array_line_contract_history_params['service_start_date'])))
            $array_line_contract_history_params['gc_timezone_id_c'] = $this->getTimezoneIdFromDtls(array(
                'name' => $array_line_contract_history_params['service_start_date_timezone']
            ));
        }

        if (isset($array_line_contract_history_params['service_end_date_timezone']) && !isset($array_line_contract_history_params['gc_timezone_id1_c'])){
            if($flag_update || (!$flag_update && isset($array_line_contract_history_params['service_end_date'])  && !empty($array_line_contract_history_params['service_end_date'])))
            $array_line_contract_history_params['gc_timezone_id1_c'] = $this->getTimezoneIdFromDtls(array(
                'name' => $array_line_contract_history_params['service_end_date_timezone']
            ));
        }

        if (isset($array_line_contract_history_params['billing_start_date_timezone']) && !isset($array_line_contract_history_params['gc_timezone_id2_c'])){
            if($flag_update || (!$flag_update && isset($array_line_contract_history_params['billing_start_date'])  && !empty($array_line_contract_history_params['billing_start_date'])))
            $array_line_contract_history_params['gc_timezone_id2_c'] = $this->getTimezoneIdFromDtls(array(
                'name' => $array_line_contract_history_params['billing_start_date_timezone']
            ));
        }

        if (isset($array_line_contract_history_params['billing_end_date_timezone']) && !isset($array_line_contract_history_params['gc_timezone_id3_c'])){
            if($flag_update || (!$flag_update && isset($array_line_contract_history_params['billing_end_date'])  && !empty($array_line_contract_history_params['billing_end_date'])))
            $array_line_contract_history_params['gc_timezone_id3_c'] = $this->getTimezoneIdFromDtls(array(
                'name' => $array_line_contract_history_params['billing_end_date_timezone']
            ));
        }

        foreach ($array_line_contract_history_params as $field => $value)
            $obj_line_contract_history_bean->$field = $value;

        if (!empty($contract_line_item_id))
            $obj_line_contract_history_bean->line_item_id = $contract_line_item_id;
        if (!empty($tech_account_id))
            $obj_line_contract_history_bean->tech_account_id = $tech_account_id;
        if (!empty($billing_account_id))
            $obj_line_contract_history_bean->bill_account_id = $billing_account_id;
        if (!empty($contract_id))
            $obj_line_contract_history_bean->contracts_id = $contract_id;

        $obj_line_contract_history_bean->team_id = 1;
        $obj_line_contract_history_bean->team_set_id = 1;
        $obj_line_contract_history_bean->save();

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_item_history_id : ' . $obj_line_contract_history_bean->id, $this->api_strings['MSG_RECORD_UPDATE_SUCCESS']);
        return $obj_line_contract_history_bean;
    }

    /**
     * method to save Contract Line Item -> Product Configuration Module record
     *
     * @param array $array_product_configuration_dtls - array with required details to save Product Configuration Module record
     * @param string $line_item_id (optional) - related Line Item Module Object Id
     * @return object method will return Product Configuration module sugar bean object
     *
     * @author sagar.salunkhe
     * @since Mar 19, 2015
     */
    protected function setProductConfigurationEntryService($array_product_configuration_dtls, $line_item_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array_product_configuration_dtls : ' . print_r($array_product_configuration_dtls, true) . GCM_GL_LOG_VAR_SEPARATOR . 'line_item_id : ' . $line_item_id, 'method to save Contract Line Item -> Product Configuration Module record');
        $obj_product_configuration_bean = BeanFactory::getBean('gc_ProductConfiguration');

        if (isset($array_product_configuration_dtls['id']))
            $obj_product_configuration_bean->retrieve($array_product_configuration_dtls['id']);

        foreach ($array_product_configuration_dtls as $field => $value)
            $obj_product_configuration_bean->$field = $value;

        $obj_product_configuration_bean->line_item_id = $line_item_id;
        $obj_product_configuration_bean->save();

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_configuration_id : ' . $obj_product_configuration_bean->id, $this->api_strings['MSG_RECORD_UPDATE_SUCCESS']);
        return $obj_product_configuration_bean;
    }

    /**
     * method to save Contract Line Item -> Product Price Module record
     *
     * @param array $array_product_price_dtls array with required details to save Product Price Module record
     * @param string $line_item_id related Line Item Module Object Id
     * @return object method will return Product Price module sugar bean object
     *
     * @author sagar.salunkhe
     * @since Mar 19, 2015
     */
    protected function setProductPriceEntryService($array_product_price_dtls, $line_item_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array_product_price_dtls : ' . print_r($array_product_price_dtls, true) . GCM_GL_LOG_VAR_SEPARATOR . 'line_item_id : ' . $line_item_id, 'method to save Contract Line Item -> Product Price Module record');
        $obj_product_price_bean = BeanFactory::getBean('gc_ProductPrice');

        if (isset($array_product_price_dtls['id']))
            $obj_product_price_bean->retrieve($array_product_price_dtls['id']);

        foreach ($array_product_price_dtls as $field => $value)
            $obj_product_price_bean->$field = $value;

        $obj_product_price_bean->line_item_id = $line_item_id;
        $obj_product_price_bean->save();

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_price_id : ' . $obj_product_price_bean->id, $this->api_strings['MSG_RECORD_UPDATE_SUCCESS']);
        return $obj_product_price_bean;
    }

    /**
     * get latest available version number from global contract id
     *
     * @param string $global_contract_id
     *
     * @author sagar.salunkhe
     * @since Aug 3, 2016
     */
    protected function getLatestContractVersion($global_contract_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'global_contract_id : ' . $global_contract_id, 'get latest available version number from global contract id');
        $where = " global_contract_id = '" . $global_contract_id . "'";
        $result_bean = BeanFactory::getBean('gc_Contracts')->get_full_list('', $where);

        $version = 1;

        if (!empty($result_bean) && is_array($result_bean) && count($result_bean) > 0) {
            foreach ($result_bean as $i => $contract_bean)
                $version = ($contract_bean->contract_version > $version) ? $contract_bean->contract_version : $version;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'version : ' . $version, 'get latest available version number from global contract id');
        return $version;
    }

    /**
     * method to save Contract Line Item -> Product Rule Module record
     *
     * @param array $array_product_rule_dtls - array with required details to save Product Rule Module record
     * @param string $line_item_id - related Line Item Module Object Id
     * @return object method will return Product Rule module sugar bean object
     *
     * @author sagar.salunkhe
     * @since Apr 05, 2015
     */
    protected function setProductRuleEntryService($array_product_rule_dtls, $line_item_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array_product_rule_dtls : ' . print_r($array_product_rule_dtls, true) . GCM_GL_LOG_VAR_SEPARATOR . 'line_item_id : ' . $line_item_id, 'method to save Contract Line Item -> Product Rule Module record');
        $obj_product_rule_bean = BeanFactory::getBean('gc_ProductRule');

        if (isset($array_product_rule_dtls['id']))
            $obj_product_rule_bean->retrieve($array_product_rule_dtls['id']);

        foreach ($array_product_rule_dtls as $field => $value)
            $obj_product_rule_bean->$field = $value;

        $obj_product_rule_bean->line_item_id = $line_item_id;
        $obj_product_rule_bean->save();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_rule_id : ' . $obj_product_rule_bean->id, $this->api_strings['MSG_RECORD_UPDATE_SUCCESS']);
        return $obj_product_rule_bean;
    }

    /**
     * method to set Direct Deposit Details
     *
     * @param array $array_direct_deposit_params array with required details to save Direct deposit record
     * @param string $line_contract_history_id related contract line item history record id
     * @return object Contract Direct Deposit record object/bean
     *
     * @author sagar.salunkhe
     * @since Oct 08, 2015
     */
    protected function setDirectDepositService($array_direct_deposit_params, $line_contract_history_id, $flag_update)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array_direct_deposit_params : ' . print_r($array_direct_deposit_params, true) . GCM_GL_LOG_VAR_SEPARATOR . 'line_contract_history_id : ' . $line_contract_history_id . GCM_GL_LOG_VAR_SEPARATOR . 'flag_update : ' . $flag_update, 'method to set Direct Deposit Details');
        $obj_direct_deposit_bean = BeanFactory::getBean('gc_DirectDeposit');

        if ($flag_update)
            $obj_direct_deposit_bean->retrieve($array_direct_deposit_params['id']);

        foreach ($array_direct_deposit_params as $field => $value)
            $obj_direct_deposit_bean->$field = $value;

        $obj_direct_deposit_bean->contract_history_id = $line_contract_history_id;
        $obj_direct_deposit_bean->team_id = 1;
        $obj_direct_deposit_bean->team_set_id = 1;
        $obj_direct_deposit_bean->save();

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'direct_deposit_id : ' . $obj_direct_deposit_bean->id, $this->api_strings['MSG_RECORD_UPDATE_SUCCESS']);
        return $obj_direct_deposit_bean;
    }

    /**
     * method to set Account Transfer Details
     *
     * @param array $array_acc_transfer_params array with required details to save Account Transfer record
     * @param string $line_contract_history_id related contract line item history record id
     * @return object Contract Account Transfer record object/bean
     *
     * @author Dinesh.Itkar
     * @since Dec 31, 2015
     */
    protected function setAccountTransferService($array_acc_transfer_params, $line_contract_history_id, $flag_update)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array_acc_transfer_params : ' . print_r($array_acc_transfer_params, true) . GCM_GL_LOG_VAR_SEPARATOR . 'line_contract_history_id : ' . $line_contract_history_id . GCM_GL_LOG_VAR_SEPARATOR . 'flag_update : ' . $flag_update, 'method to set Account Transfer Details');
        $obj_acc_transfer_bean = BeanFactory::getBean('gc_AccountTransfer');
        if ($flag_update)
            $obj_acc_transfer_bean->retrieve($array_acc_transfer_params['id']);

        foreach ($array_acc_transfer_params as $field => $value)
            $obj_acc_transfer_bean->$field = $value;

        $obj_acc_transfer_bean->contract_history_id = $line_contract_history_id;
        $obj_acc_transfer_bean->team_id = 1;
        $obj_acc_transfer_bean->team_set_id = 1;
        $obj_acc_transfer_bean->save();

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'acc_transfer_id : ' . $obj_acc_transfer_bean->id, $this->api_strings['MSG_RECORD_UPDATE_SUCCESS']);
        return $obj_acc_transfer_bean;
    }

    /**
     * method to set Postal Transfer Details
     *
     * @param array $array_postal_transfer_params array with required details to save Account Transfer record
     * @param string $line_contract_history_id related contract line item history record id
     * @return object Contract Postal Transfer record object/bean
     *
     * @author Dinesh.Itkar
     * @since Dec 31, 2015
     */
    protected function setPostalTransferService($array_postal_transfer_params, $line_contract_history_id, $flag_update)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array_postal_transfer_params : ' . print_r($array_postal_transfer_params, true) . GCM_GL_LOG_VAR_SEPARATOR . 'line_contract_history_id : ' . $line_contract_history_id . GCM_GL_LOG_VAR_SEPARATOR . 'flag_update : ' . $flag_update, 'method to set Postal Transfer Details');

        $obj_postal_transfer_bean = BeanFactory::getBean('gc_PostalTransfer');
        if ($flag_update)
            $obj_postal_transfer_bean->retrieve($array_postal_transfer_params['id']);

        foreach ($array_postal_transfer_params as $field => $value)
            $obj_postal_transfer_bean->$field = $value;

        $obj_postal_transfer_bean->contract_history_id = $line_contract_history_id;
        $obj_postal_transfer_bean->team_id = 1;
        $obj_postal_transfer_bean->team_set_id = 1;
        $obj_postal_transfer_bean->save();

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'postal_transfer_id : ' . $obj_postal_transfer_bean->id, $this->api_strings['MSG_RECORD_UPDATE_SUCCESS']);
        return $obj_postal_transfer_bean;
    }

    /**
     * method to set Credit Card Details
     *
     * @param array $array_credit_card_params array with required details to save Credit Card record
     * @param string $line_contract_history_id related contract line item history record id
     * @return object Contract Credit Card record object/bean
     *
     * @author Dinesh.Itkar
     * @since Dec 31, 2015
     */
    protected function setCreditCardService($array_credit_card_params, $line_contract_history_id, $flag_update)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array_credit_card_params : ' . print_r($array_credit_card_params, true) . GCM_GL_LOG_VAR_SEPARATOR . 'line_contract_history_id : ' . $line_contract_history_id . GCM_GL_LOG_VAR_SEPARATOR . 'flag_update : ' . $flag_update, 'method to set Credit Card Details');
        $obj_credit_card_bean = BeanFactory::getBean('gc_CreditCard');
        if ($flag_update)
            $obj_credit_card_bean->retrieve($array_credit_card_params['id']);

        foreach ($array_credit_card_params as $field => $value)
            $obj_credit_card_bean->$field = $value;

        $obj_credit_card_bean->contract_history_id = $line_contract_history_id;
        $obj_credit_card_bean->team_id = 1;
        $obj_credit_card_bean->team_set_id = 1;
        $obj_credit_card_bean->save();

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'credit_card_id : ' . $obj_credit_card_bean->id, $this->api_strings['MSG_RECORD_UPDATE_SUCCESS']);
        return $obj_credit_card_bean;
    }

    /**
     * method to save invoice group module record
     *
     * @param array $invoice_group_data
     * @return object Invoice group record object/bean
     *
     * @author sagar.salunkhe
     * @since Jun 9, 2016
     */
    protected function setInvoiceGroupEntryService($invoice_group_data, $flag_update)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'invoice_group_data : ' . print_r($invoice_group_data, true) . GCM_GL_LOG_VAR_SEPARATOR . '$flag_update : ' . $flag_update, 'method to save invoice group module record');

        $obj_invoice_group = BeanFactory::getBean('gc_InvoiceSubtotalGroup');

        $invoice_id = isset($invoice_group_data['uuid']) ? $invoice_group_data['uuid'] : '';

        $obj_invoice_group->retrieve($invoice_id);

        if (empty($obj_invoice_group->id) && trim($invoice_group_data['name']) == "")
            return $obj_invoice_group;

        foreach ($invoice_group_data as $field => $value)
            $obj_invoice_group->$field = $value;

        $obj_invoice_group->team_id = 1;
        $obj_invoice_group->team_set_id = 1;
        $obj_invoice_group->save();

        $this->invoice_group_list[$obj_invoice_group->name] = $obj_invoice_group->id;

        if (isset($invoice_group_data['line_item_history_id']) && !empty($invoice_group_data['line_item_history_id'])) {
            $obj_invoice_group->load_relationship('gc_invoicesubtotalgroup_gc_line_item_contract_history_1');
            $obj_invoice_group->gc_invoicesubtotalgroup_gc_line_item_contract_history_1->add($invoice_group_data['line_item_history_id']);
        }

        if ($flag_update) {
            $line_item_history_list = $obj_invoice_group->get_linked_beans('gc_invoicesubtotalgroup_gc_line_item_contract_history_1', 'gc_Line_Item_Contract_History');
            $arr_invoice_line_rel = array();
            foreach ($line_item_history_list as $line_item_history) {
                $arr_invoice_line_rel[] = $line_item_history->id;
            }

            $result = array_diff($arr_invoice_line_rel, $invoice_group_data['line_item_history_id']);

            foreach ($result as $obj_history_id)
                $obj_invoice_group->gc_invoicesubtotalgroup_gc_line_item_contract_history_1->delete($obj_invoice_group->id, $obj_history_id);
        }

        $obj_invoice_group->load_relationship('gc_contracts_gc_invoicesubtotalgroup_1');
        $obj_invoice_group->gc_contracts_gc_invoicesubtotalgroup_1->add($invoice_group_data['contract_id']);

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'invoice_group_id : ' . $obj_invoice_group->id, $this->api_strings['MSG_RECORD_UPDATE_SUCCESS']);
        return $obj_invoice_group;
    }

    /**
     * method to soft delete line item and related module records
     *
     * @param string $line_item_id line item record id
     * @param object $contract_bean contract_bean
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Apr 16, 2016
     */
    protected function removeLineItemEntryService($line_item_id, $contract_bean)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_item_id : ' . $line_item_id, 'method to soft delete line item and related module records');

        $obj_contract_line_item_bean = BeanFactory::getBean('gc_LineItem');
        $obj_contract_line_item_bean->retrieve($line_item_id);

        if (empty($obj_contract_line_item_bean->id))
            return false; // contract line item record not present or already deleted

        // delete line item documents
        $line_item_doc_bean = $obj_contract_line_item_bean->get_linked_beans('gc_lineitem_documents_1', 'Documents');
        if ($line_item_doc_bean != null) {
            foreach ($line_item_doc_bean as $contract_doc) {
                $contract_doc->deleted = 1;
                $contract_doc->save();
                // deleting document revision
                $doc_id = $contract_doc->id;
                $doc_rev_bean = new DocumentRevision();
                $doc_rev_bean_arr = $doc_rev_bean->get_full_list('', "document_id = '$doc_id'");
                if ($doc_rev_bean_arr != null) {
                    foreach ($doc_rev_bean_arr as $doc_rev) {
                        $doc_rev->deleted = 1;
                        $doc_rev->save();
                    }
                }

                $obj_contract_line_item_bean->load_relationship('gc_lineitem_documents_1');
                $obj_contract_line_item_bean->gc_lineitem_documents_1->delete($line_item_id, $doc_id);
            }
        }

        $line_itemname = $obj_contract_line_item_bean->name;

        $obj_contract_line_item_bean->deleted = 1;
        $obj_contract_line_item_bean->save();
        // Audit log entry DF346
        $this->insertLineItemAudit($contract_bean, $line_itemname);

        $obj_line_contract_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History')->get_list('', "line_item_id = '$line_item_id'", null);
        foreach ($obj_line_contract_history_bean['list'] as $focus) {

            $focus->deleted = 1;
            $focus->save();

            $arr_payment_modules = array(
                                        'gc_DirectDeposit',
                                        'gc_AccountTransfer',
                                        'gc_PostalTransfer',
                                        'gc_CreditCard'
            );
            foreach ($arr_payment_modules as $module_bean_name) {
                $obj_module_bean = BeanFactory::getBean($module_bean_name)->get_list('', "contract_history_id = '" . $focus->id . "'", null);
                foreach ($obj_module_bean['list'] as $p_focus) {
                    $p_focus->deleted = 1;
                    $p_focus->save();
                }
            }
        }

        $arr_related_modules = array(
                                    'gc_ProductConfiguration',
                                    'gc_ProductPrice',
                                    'gc_ProductRule'
        );
        foreach ($arr_related_modules as $module_bean_name) {
            $obj_module_bean = BeanFactory::getBean($module_bean_name)->get_list('', "line_item_id = '$line_item_id'", null);
            foreach ($obj_module_bean['list'] as $focus) {
                $focus->deleted = 1;
                $focus->save();
            }
        }

        return true;
    }

    /**
     * make entry into contract module audit log while deleting the line item
     *
     * @param object Contract bean to make entry into contract module audit log
     * @param string $line_itemname global line item name like 100001
     *
     * @author Arunsakthivel.Sambandam
     * @since 18-May-2016
     * @internal method was previously present into SugarCRM/custom/modules/gc_Contracts/include/ClassServiceGcContracts.php
     */
    private function insertLineItemAudit($bean, $line_itemname)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_itemname : ' . $line_itemname, 'make entry into contract module audit log while deleting the line item');
        $audit_entry = array();
        $audit_entry['field_name'] = "Contract Line Item";
        $audit_entry['data_type'] = "text";
        $audit_entry['before'] = $line_itemname;
        $audit_entry['after'] = "";
        $bean->db->save_audit_records($bean, $audit_entry);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'make entry into contract module audit log while deleting the line item');
    }

    /**
     * when deleting contract delete all related records
     *
     * @param object $bean Contract bean
     *
     * @author Kamal.Thiyagarajan
     * @since 26-May-2016
     * @internal method was previously present into SugarCRM/custom/modules/gc_Contracts/include/ClassServiceGcContracts.php
     */
    protected function deleteContract($bean)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'when deleting contract delete all related records');
        // Check is it valid bean
        if (empty($bean)) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Empty bean');
            return;
        }
        $contract_id = $bean->id;
        // Delete Sales Represantative
        $sales_rep_bean = BeanFactory::getBean('gc_SalesRepresentative');
        $sales_rep_bean_arr = $sales_rep_bean->get_full_list('', "contracts_id = '$contract_id'");
        if ($sales_rep_bean_arr != null) {
            $sales_rep = $sales_rep_bean_arr[0];
            $sales_rep->deleted = '1';
            $sales_rep->save();
        }
        // Delete all contract document and related tables
        $contract_doc_bean = $bean->get_linked_beans('gc_contracts_documents_1', 'Documents');
        if ($contract_doc_bean != null) {
            foreach ($contract_doc_bean as $contract_doc) {
                $contract_doc->deleted = 1;
                $contract_doc->save();
                // deleting document revision
                $doc_id = $contract_doc->id;
                $doc_rev_bean = new DocumentRevision();
                $doc_rev_bean_arr = $doc_rev_bean->get_full_list('', "document_id = '$doc_id'");
                if ($doc_rev_bean_arr != null) {
                    foreach ($doc_rev_bean_arr as $doc_rev) {
                        $doc_rev->deleted = 1;
                        $doc_rev->save();
                    }
                }
            }
        }
        // deleting all line item's of contract
        $line_item_his_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
        $line_item_his_bean_arr = $line_item_his_bean->get_full_list('', "contracts_id = '$contract_id'");
        if ($line_item_his_bean_arr != null) {
            foreach ($line_item_his_bean_arr as $line_item_header) {
                $this->removeLineItemEntryService($line_item_header->line_item_id, $bean);
                // deleting linked groups
                $line_item_grp_bean = $line_item_header->get_linked_beans('gc_invoicesubtotalgroup_gc_line_item_contract_history_1', 'gc_InvoiceSubtotalGroup');
                if ($line_item_grp_bean != null) {
                    foreach ($line_item_grp_bean as $group) {
                        $obj_invoice_group = BeanFactory::getBean('gc_InvoiceSubtotalGroup', $group->id);
                        $obj_invoice_group->load_relationship('gc_invoicesubtotalgroup_gc_line_item_contract_history_1');
                        $obj_invoice_group->gc_invoicesubtotalgroup_gc_line_item_contract_history_1->delete($group->id, $line_item_header->id);
                    }
                }
            }
        }
        // deleting all groups
        $contract_grp_bean = $bean->get_linked_beans('gc_contracts_gc_invoicesubtotalgroup_1', 'gc_InvoiceSubtotalGroup');
        if ($contract_grp_bean != null) {
            foreach ($contract_grp_bean as $group) {
                $group->deleted = '1';
                $group->save();
            }
        }
        // deleting comments and approval related to contracts
        $arr_related_modules = array(
                                    'gc_comments',
                                    'gc_MyApprovals'
        );
        foreach ($arr_related_modules as $module_bean_name) {
            $obj_module_bean = BeanFactory::getBean($module_bean_name)->get_list('', "gc_contracts_id_c = '$contract_id'", null);
            if ($obj_module_bean != null) {
                foreach ($obj_module_bean['list'] as $focus) {
                    $focus->deleted = 1;
                    $focus->save();
                }
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'when deleting contract delete all related records');
    }

    /**
     * get record id from timezone module on basis of utc offset
     *
     * @param array $search_params field_name => value
     * @return string record object id
     *
     * @author sagar.salunkhe
     * @since Jun 11, 2016
     */
    protected function getTimezoneIdFromDtls($search_params)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '$search_params : ' . print_r($search_params, true), 'get record id from timezone module on basis of utc offset');

        $obj_timezone = BeanFactory::getBean('gc_TimeZone');

        $filter = array();

        foreach ($search_params as $field => $value) {
            if (!empty($value))
                $filter[$field] = $value;
        }

        if (count($filter) < 1) {
            if ($this->flag_api_request)
                $filter['name'] = $this->default_api_utc;
            else {
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'returned empty value.');
                return '';
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'flag_api_request : ' . (string) $this->flag_api_request, '');

        $obj_timezone->retrieve_by_string_fields($filter);

        if (empty($obj_timezone->id) && $this->flag_api_request) {
            $filter['name'] = $this->default_api_utc;
            $obj_timezone->retrieve_by_string_fields($filter);
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'retrieved record id : ' . (string) $obj_timezone->id, 'get record id from timezone module on basis of utc offset');
            return $obj_timezone->id;
        } else {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'retrieved record id : ' . (string) $obj_timezone->id, 'get record id from timezone module on basis of utc offset');
            return $obj_timezone->id;
        }
    }

    /**
     * check contract version upgrade
     *
     * @param object contract bean
     * @return boolean
     *
     * @author Debasish.Gupta
     * @since Jul 09, 2016
     */
    public function checkContractVersionUpgrade($contract_bean)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'check contract version upgrade');
        $contract_version = $contract_bean->contract_version;
        $global_contract_id = $contract_bean->global_contract_id;
        $where = " global_contract_id = '" . $global_contract_id . "' AND
                        contract_version > '" . $contract_version . "' ";
        $result_bean = BeanFactory::getBean('gc_Contracts')->get_full_list('', $where);
        if (!empty($result_bean) && is_array($result_bean) && count($result_bean) > 0) {
            return true;
        }

        return false;
    }

    /**
     * get respective line item type for bi action
     *
     * @param string $bi_action
     *
     * @author sagar.salunkhe
     * @since Aug 24, 2016
     */
    protected function getLineItemTypeFromBiAction($bi_action)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'bi_action : ' . print_r($bi_action, true), 'get respective line item type for bi action');
        $bi_action_line_type_mapping = array(
                                            'add' => 'New',
                                            'change' => 'Change',
                                            'no_change' => 'Activated',
                                            'remove' => 'Service_Termination'
        );
        return (array_key_exists($bi_action, $bi_action_line_type_mapping)) ? $bi_action_line_type_mapping[$bi_action] : 'New';
    }

    /**
     * get contract visibility teams from admin panel
     *
     * @author sagar.salunkhe
     * @since Aug 25, 2016
     */
    protected function getContractApproverTeams()
    {
        $approver = CustomFunctionGcContract::getRolePermissions(SETTING_KEY_ACE_CONTRACTS);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'approver : ' . print_r($approver, true), 'get contract visibility teams from admin panel');
        $approver_teams = $approver['approver_team'];
        $approver_team_array = array();
        if (!empty($approver_teams) && is_array($approver_teams)) {
            foreach ($approver_teams as $key => $approver_team) {
                $team = BeanFactory::getBean('Teams');
                $team->retrieve($approver_team);
                $approver_team_array[] = $team->name;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'approver_team_array : ' . print_r($approver_team_array, true), 'get contract visibility teams from admin panel');
        return $approver_team_array;
    }

    /**
     * array intersect multi dimension
     *
     * @param array $roles currect user's roles (single dimension)
     * @param array $approver approver user's roles (multi dimension)
     * @return array intersect array will return (single dimension)
     * @author rajul.mondal
     * @since Jan 19, 2017
     */
    public function arrayIntersectMulti($roles, $approver)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '$roles : ' . print_r($roles, true) . GCM_GL_LOG_VAR_SEPARATOR . 'approver' . print_r($approver, true), 'array intersect multi dimension');
        $array_intersect = array();
        if (is_array($roles) && !empty($roles) && is_array($approver) && !empty($roles)) {
            $serialized_roles = $this->serialize_array_values($roles);
            $serialized_approver = $this->serialize_array_values($approver);
            $array_intersect = array_map("unserialize", array_intersect($serialized_roles, $serialized_approver));
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array_intersect : ' . print_r($array_intersect, true), 'array intersect multi dimension');
        return $array_intersect;
    }

    /**
     * To searialize array
     *
     * @return array
     * @author Kamal.Thiyagarajan
     * @since 12-Jan-2017
     */
    public function serialize_array_values($arr)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr : ' . print_r($arr, true), 'To searialize array');
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                sort($val);
            }

            $arr[$key] = serialize($val);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr : ' . print_r($arr, true), 'To searialize array');
        return $arr;
    }
}