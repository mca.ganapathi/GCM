<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * Method to populate ListView Product Offerings Field from PMS API
 *
 * @return array product offering list
 * @author Dinesh.Itkar
 * @since 06-May-2016
 */
function getProductOfferingList()
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    include_once 'custom/modules/gc_Contracts/include/ClassGcContracts.php';
    $obj_gc_contract             = new ClassGcContracts();
    $produt_offering_list        = $obj_gc_contract->getProductOfferings(true);
    $array_product_offerings     = array();
    $array_product_offerings[''] = '';
    if(!empty($produt_offering_list)){
        foreach ($produt_offering_list as $key => $val) {
            $array_product_offerings[$key] = $val;
        }    
    }
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'array_product_offerings: '.print_r($array_product_offerings,true), '');
    return $array_product_offerings;
}
