<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
require_once ('custom/modules/gc_Contracts/include/ClassGcContracts.php');

class ClassGcContractBeforeDelete extends ClassGcContracts
{

    /**
     * method to delete contract related records.
     *
     * @author Kamal.Thiyagarajan
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @since 26-May-2016
     */
    function deleteContractRelatedRecords($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event: '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments: '.print_r($arguments,true), 'Method to delete contract related records');
        if(isset($bean->fetched_row['id']) && !empty($bean->fetched_row['id'])){
            $this->deleteContract($bean);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}
?>