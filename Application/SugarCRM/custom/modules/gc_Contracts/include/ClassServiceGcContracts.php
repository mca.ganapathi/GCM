<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once ('custom/modules/gc_Contracts/include/ClassGcContracts.php');

/**
 * Module API - Service Class File
 *
 * @author sagar.salunkhe
 * @since Oct 07, 2015
 */
class ClassServiceGcContracts extends ClassGcContracts
{

    const NULL_CHAR = '';

    private $api_strings;

    private $sugar_config;

    private $obj_current_user;

    private $api_version = 1;

    private $response_data = array();

    private $contract_upgrade = false;

    protected $timezone_offset;

    protected $time;

    protected $contract_action = 'Insert';

    private $modUtils;

    /**
     * class constructor
     *
     * @param string $version contract api version number
     * @author sagar.salunkhe
     * @since Jun 8, 2016
     */
    function __construct($version)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'version: '.print_r($version,true), '');
        // write code here to get executed when object is initialized
        global $current_user, $app_strings, $sugar_config;

        $this->api_strings = $app_strings['custom_api_service'];
        $this->sugar_config = $sugar_config;
        $this->obj_current_user = $current_user;

        $this->flag_api_request = true;
        $this->api_version = $version;

        // get server timezone difference in hours with respect to UTC
        $this->time = new DateTime('now', new DateTimeZone(date_default_timezone_get()));
        $this->timezone_offset = $this->time->format('P');
        $this->timezone_offset = ($this->timezone_offset == 0 ? '00:00' : $this->timezone_offset); // if timezone diff is zero set "Z"

        $this->modUtils = new ModUtils();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * method to Save Contract details into SugarCRM Application (method will be invoked from sugar service impl file)
     *
     * @param object $current_user_details sugarcrm logged in user object
     * @param array $array_contracts_params array with required details to save contract
     * @return array will contain required data (need to finalize the return required params)
     * @author sagar.salunkhe
     * @since Oct 7, 2015
     */
    public function setContractEntryService($current_user_details, $array_contracts_params)
    {
        global $sugar_config;

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'current_user_details: '.print_r($current_user_details->id,true).GCM_GL_LOG_VAR_SEPARATOR.'array_contracts_params: '.print_r($array_contracts_params,true), 'Method to Save Contract details into SugarCRM Application (method will be invoked from sugar service impl file)');
        $this->obj_current_user = $current_user_details;

        $billing_affiliate_id = NULL;
        if (isset($array_contracts_params['ContractDetails']['BillingAffiliate'][0])) {
            $billing_affiliate_id = self::getBillingAffiliateRecord($array_contracts_params['ContractDetails']['BillingAffiliate'][0]);
        }

        // identify if contract is for version upgrade if global contract id or global contract uuid is passed
        $global_contract_id = isset($array_contracts_params['ContractDetails']['ContractsHeader']['global_contract_id']) ? $array_contracts_params['ContractDetails']['ContractsHeader']['global_contract_id'] : '';
        $global_contract_id_uuid = isset($array_contracts_params['ContractDetails']['ContractsHeader']['global_contract_id_xml_id']) ? $array_contracts_params['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'] : '';

        if (!empty($global_contract_id) || (!empty($global_contract_id_uuid) && $this->modUtils->validateUUID($global_contract_id_uuid))) {
            $this->contract_upgrade = true;
        }

        // validate provided array data
        if ($this->contract_upgrade) {
            self::processContractVersionUpgradeData($array_contracts_params);
        }

        // save Contract Type Account
        $obj_contract_accounts = $this->setAccountEntryService($array_contracts_params['Accounts']['Contract'], '');
        $obj_corporates = BeanFactory::getBean('Accounts');
        $obj_corporates->retrieve($obj_contract_accounts->account_id);

        if(!isset($array_contracts_params['Accounts']['Technology'])) {
            $array_contracts_params['Accounts']['Technology'] = array();
        }
        // save Technology Type Account
        $obj_technology_accounts = $this->setAccountEntryService($array_contracts_params['Accounts']['Technology'], '');

        if(!isset($array_contracts_params['Accounts']['Billing'])) {
            $array_contracts_params['Accounts']['Billing'] = array();
        }
        // save Billing Type Account
        $obj_billing_accounts = $this->setAccountEntryService($array_contracts_params['Accounts']['Billing'], '');

        // save contract header details
        $obj_contract_header = BeanFactory::getBean('gc_Contracts');
        $this->setContractHeaderEntryService($array_contracts_params['ContractDetails']['ContractsHeader'], $obj_contract_accounts, $obj_corporates, $obj_contract_header);

        if($this->contract_upgrade && in_array($obj_contract_header->product_offering, $sugar_config['enterprise_mail_po'])) {
            $this->makePreviousContractAsUpgraded($obj_contract_header);
        }

        $this->response_data['ContractDetails']['ContractHeader']['ContractObjectId'] = $obj_contract_header->id;
        $this->response_data['ContractDetails']['ContractHeader']['GlobalContractId'] = $obj_contract_header->global_contract_id;
        $this->response_data['ContractDetails']['ContractHeader']['ContractVersion'] = $obj_contract_header->contract_version;
        $this->response_data['ContractDetails']['ContractHeader']['ContractUpgrade'] = $this->contract_upgrade;

        if(isset($array_contracts_params['ContractDetails']['ContractsHeader']['surrogate_teams_xml_id'])) unset($array_contracts_params['ContractDetails']['ContractsHeader']['surrogate_teams_xml_id']);

        $this->modUtils->generatePostXmlResponseData($array_contracts_params['ContractDetails']['ContractsHeader'], $this->response_data['ContractDetails']['ContractHeader']['local_uuid'], $obj_contract_header);

        // save contract sales representative details (one contract can have many sales representative)
        $array_obj_sales_rep = array();
        if(isset($array_contracts_params['ContractDetails']['ContractSalesRepresentative']) && is_array($array_contracts_params['ContractDetails']['ContractSalesRepresentative'])) {
            foreach ($array_contracts_params['ContractDetails']['ContractSalesRepresentative'] as $array_sales_representative) {
                $array_obj_sales_rep[] = $this->setContractSalesRepEntryService($array_sales_representative, $obj_contract_header->id, false, false);
            }
        }

        // save contract line items
        $array_obj_contract_line_items = array();
        foreach ($array_contracts_params['ContractDetails']['ContractLineItemDetails'] as $key => $array_contract_line_details) {

            if (array_key_exists('bi_action', $array_contract_line_details['ContractLineItemHeader'])) {
                $array_contract_line_details['ContractLineItemHeader']['bi_action'] = $this->modUtils->getAppListStringValueMember($array_contract_line_details['ContractLineItemHeader']['bi_action'], 'bi_action_list');
            }

            if($obj_contract_header->contract_status == '4' && $this->contract_upgrade && in_array($obj_contract_header->product_offering, $sugar_config['enterprise_mail_po']) && $array_contract_line_details['ContractLineItemHeader']['bi_action'] == 'remove') {
                    $array_contract_line_details['ContractLineItemHeader']['contract_line_item_type'] = $this->getLineItemTypeFromBiAction($array_contract_line_details['ContractLineItemHeader']['bi_action']);
            }
            elseif($obj_contract_header->contract_status == '4') {
                // if contract is activated, activate the line item
                $array_contract_line_details['ContractLineItemHeader']['contract_line_item_type'] = 'Activated';
            }
            else {
                $array_contract_line_details['ContractLineItemHeader']['contract_line_item_type'] = $this->getLineItemTypeFromBiAction($array_contract_line_details['ContractLineItemHeader']['bi_action']);
            }

            if (!is_null($billing_affiliate_id)) {
                $array_contract_line_details['ContractLineItemHeader']['gc_organization_id_c'] = $billing_affiliate_id;
            }

            $array_obj_contract_line_items[$key] = $this->setContractLineItemEntryService($array_contract_line_details, $obj_contract_header->id, $obj_technology_accounts->id, $obj_billing_accounts->id, false, $this->response_data['ContractDetails']['ContractLineItemDetails'][$key]);
        }

        // prepare array of Line Item object Ids to return
        foreach ($array_obj_contract_line_items as $key => $array_obj_contract_line_item) {
            $this->response_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineItems']['LineItemObjectId'] = $array_obj_contract_line_item->id;
        }
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return $this->modUtils->buildServiceResponse($this->api_version, '1', '1001', $this->response_data, $dynamic_msg);
    }

    /**
     * method to Update Contract details into SugarCRM GCM Application (method will be invoked from service file)
     *
     * @param object $current_user_details object of current user details
     * @param array $array_contracts_params array with required details to save contract
     * @return array array will contain required data with api response code and success flag
     * @author sagar.salunkhe
     * @since Oct 27, 2015
     */
    public function updateContractEntryService($current_user_details, $array_contracts_params)
    {
        global $sugar_config;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'current_user_details: '.print_r($current_user_details,true).GCM_GL_LOG_VAR_SEPARATOR.'array_contracts_params: '.print_r($array_contracts_params,true), 'Method to Update Contract details into SugarCRM GCM Application (method will be invoked from service file)');

        $this->obj_current_user = $current_user_details;
        $this->contract_action = 'Update';

		$billing_affiliate_id = NULL;
        if (isset($array_contracts_params['ContractDetails']['BillingAffiliate'][0])) {
            $billing_affiliate_id = self::getBillingAffiliateRecord($array_contracts_params['ContractDetails']['BillingAffiliate'][0]);
        }

        // 2. retrieve contract record Sugar bean object
        $obj_contracts_bean = BeanFactory::getBean('gc_Contracts');
        if (isset($array_contracts_params['ContractObjectId']) && !empty($array_contracts_params['ContractObjectId'])) {
            $obj_contracts_bean->retrieve($array_contracts_params['ContractObjectId']);

        } elseif (isset($array_contracts_params['GlobalContractId']) && !empty($array_contracts_params['GlobalContractId'])) {
            $latest_version = $this->getLatestContractVersion($array_contracts_params['GlobalContractId']);
            $obj_contracts_bean->retrieve_by_string_fields(array(
                                                                'global_contract_id' => $array_contracts_params['GlobalContractId'],
                                                                'contract_version' => $latest_version
            ));
            $obj_contracts_bean->retrieve($obj_contracts_bean->id);

        } elseif (isset($array_contracts_params['ContractDetails']['ContractsHeader']['global_contract_id_xml_id']) && !empty($array_contracts_params['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'])) {
            // get latest version of the contract of respective global contract id uuid
            $obj_contracts_uuid_bean = BeanFactory::getBean('gc_Contracts');
            $obj_contracts_uuid_bean->retrieve_by_string_fields(array(
                                                                    'global_contract_id_uuid' => $array_contracts_params['ContractDetails']['ContractsHeader']['global_contract_id_xml_id']
            ));

            if (!empty($obj_contracts_uuid_bean->id)) {
                $latest_version = $this->getLatestContractVersion($obj_contracts_uuid_bean->global_contract_id);
                $obj_contracts_bean->retrieve_by_string_fields(array(
                                                                    'global_contract_id' => $obj_contracts_uuid_bean->global_contract_id,
                                                                    'contract_version' => $latest_version
                ));
                $obj_contracts_bean->retrieve($obj_contracts_bean->id);
            }
        }

        // 3. return if contract record details are invalid
        //api v1.0 related deprecated code removed

        // 4. Process Update Account (type : Contract)
        if (isset($array_contracts_params['Accounts']['Contract']) && count($array_contracts_params['Accounts']['Contract']) > 0) {
            $obj_contract_account_bean = BeanFactory::getBean('Contacts');
            $obj_contract_account_bean->retrieve($obj_contracts_bean->contacts_gc_contracts_1contacts_ida); // retrieve linked contracting account

            $array_contracts_params['Accounts']['Contract']['contact_type_c'] = 'Contract'; // defined contact type as it is not being passed from XML for PATCH method
            $res_contract_account = $this->setAccountEntryService($array_contracts_params['Accounts']['Contract'], $obj_contract_account_bean->id);

            // JIRA # OM-28_OM-33 if contracting person corporate updates, update the same at Contract header
            if ($res_contract_account->account_id != $obj_contracts_bean->accounts_gc_contracts_1accounts_ida) {
                $obj_contracts_bean->accounts_gc_contracts_1accounts_ida = $res_contract_account->account_id;
            }

            if ($res_contract_account->id != $obj_contract_account_bean->id) {
                $obj_contracts_bean->contacts_gc_contracts_1contacts_ida = $res_contract_account->id;
            }
        }

        // 5. fetch Billing and Technology Account Ids from Line Items
        $where_condition_line_items = " gc_line_item_contract_history.contracts_id = '" . $obj_contracts_bean->id . "' ";
        $obj_array_line_contract_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History')->get_list('gc_line_item_contract_history.id', $where_condition_line_items, null, 1);

        foreach ($obj_array_line_contract_history_bean['list'] as $line_contract_bean) {
            $tech_account_id = $line_contract_bean->tech_account_id;
            $bill_account_id = $line_contract_bean->bill_account_id;
            continue;
        }

        // 6. Process Update Account (type : Technology)
        if (isset($array_contracts_params['Accounts']['Technology']) && count($array_contracts_params['Accounts']['Technology']) > 0) {
            $array_contracts_params['Accounts']['Contract']['contact_type_c'] = 'Technology'; // defined contact type as it is not being passed from XML for PATCH method
            $obj_technology_account = $this->setAccountEntryService($array_contracts_params['Accounts']['Technology'], $tech_account_id);
            $tech_account_id = $obj_technology_account->id;
        }

        // 7. Process Update Account (type : Billing)
        if (isset($array_contracts_params['Accounts']['Billing']) && count($array_contracts_params['Accounts']['Billing']) > 0) {
            $array_contracts_params['Accounts']['Contract']['contact_type_c'] = 'Billing'; // defined contact type as it is not being passed from XML for PATCH method
            $obj_billing_account = $this->setAccountEntryService($array_contracts_params['Accounts']['Billing'], $bill_account_id);
            $bill_account_id = $obj_billing_account->id;
        }

        // 8. update contract sales representative details (one contract can have many sales representative)
        $array_obj_sales_rep = array();

        // 9. only one sales represatative details are considered in pj1 release
        $array_sales_representative = isset($array_contracts_params['ContractDetails']['ContractSalesRepresentative'][0]) ? $array_contracts_params['ContractDetails']['ContractSalesRepresentative'][0] : array();

        if (!empty($array_sales_representative)) {
            $array_obj_sales_rep[] = $this->setContractSalesRepEntryService($array_sales_representative, $obj_contracts_bean->id, true, false);
        }

        // 10. UPDATE TECHNICAL AND BILLING ACCOUNT FOREIGN KEY IN CONTRACT LINE ITEM, DF-534 UPDATE LINE ITEM TYPE WITH RESPECTIVE CONTRACT STATUS
        $obj_line_item_history_bean_arr = BeanFactory::getBean('gc_Line_Item_Contract_History');
        $obj_line_history_bean_arr = $obj_line_item_history_bean_arr->get_full_list('', "contracts_id = '" . $obj_contracts_bean->id . "'");
        if ($obj_line_history_bean_arr != null) {

            $contract_line_item_type = '';
            $contract_status = '';

            if(isset($array_contracts_params['ContractDetails']['ContractsHeader']['contract_status'])){
                $contract_status = $array_contracts_params['ContractDetails']['ContractsHeader']['contract_status'];
            }

            if ($contract_status == '2' || $contract_status == '5') {

                //DF-1291 : [APIv2 PATCH] Set Current Datetime When Contract Information is Deactivated
                if($contract_status == '2'){
                	if (!isset($array_contracts_params['ContractDetails']['ContractsHeader']['contract_enddate']) || empty($array_contracts_params['ContractDetails']['ContractsHeader']['contract_enddate']))
                    $array_contracts_params['ContractDetails']['ContractsHeader']['contract_enddate'] = date('Y-m-d H:i:s');

                	if (!isset($array_contracts_params['ContractDetails']['ContractsHeader']['contract_enddate_timezone']) || empty($array_contracts_params['ContractDetails']['ContractsHeader']['contract_enddate_timezone']))
                    $array_contracts_params['ContractDetails']['ContractsHeader']['contract_enddate_timezone'] = $this->default_api_utc;
                }

                $contract_line_item_type = 'Termination';
                if ($sugar_config['workflow_flag'][$obj_contracts_bean->product_offering] == 'Yes' && !in_array($obj_contracts_bean->product_offering, $sugar_config['enterprise_mail_po'])) {
                    $approver_teams = $this->getContractApproverTeams(); // get approver teams from admin configuration
                    $obj_contracts_bean->load_relationship('teams');
                    $contract_teams = array();
                    foreach ($approver_teams as $i => $team_name) {
                        $team_id = $this->modUtils->retrieveTeamId($team_name);
                        if ($team_id) {
                            $contract_teams[] = $team_id;
                        }
                    }
                    if (count($contract_teams) > 0) {
                        $obj_contracts_bean->teams->add($contract_teams);
                    }
                    unset($contract_teams);
                }
            }

            foreach ($obj_line_history_bean_arr as $obj_line_history_bean) {
                $update_line = false;

                if (!empty($contract_line_item_type)) {
                    $obj_line_history_bean->contract_line_item_type = $contract_line_item_type;
                    $update_line = true;
                }

                if ($update_line)
                  $obj_line_history_bean->save();

            }
        }

        $array_obj_contract_line_items = array();
        $obj_line_history_bean_retreive_arr = BeanFactory::getBean('gc_Line_Item_Contract_History');
        $obj_group_invoice_bean = BeanFactory::getBean('gc_InvoiceSubtotalGroup');
        foreach ($array_contracts_params['ContractDetails']['ContractLineItemDetails'] as $key => $array_contract_line_details) {

            $obj_line_history_bean_arr = $obj_line_item_history_bean_arr->get_full_list('', "contracts_id = '" . $obj_contracts_bean->id . "' AND line_item_id_uuid = '". $array_contract_line_details['ContractLineItemHeader']['line_item_id_xml_id'] ."'");

            if ($obj_line_history_bean_arr != null) {
                $obj_line_history_retreive_bean = $obj_line_history_bean_arr[0];
            }

        	if (!empty($obj_line_history_retreive_bean->id)) {
        		//DF-1291 : [APIv2 PATCH] Set Current Datetime When Contract Information is Deactivated
        		if ($contract_status == '2') {
        			$datetime_now = date('Y-m-d H:i:s');
        			$update_date = false;
        			if (!isset($array_contract_line_details['ContractLineItemHeader']['service_end_date']) && (empty($obj_line_history_retreive_bean->service_end_date) || (!empty($obj_line_history_retreive_bean->service_end_date) && (strtotime($obj_line_history_retreive_bean->service_end_date) > strtotime($datetime_now))))) {
        				$obj_line_history_retreive_bean->service_end_date = $datetime_now;
        				$obj_line_history_retreive_bean->gc_timezone_id1_c = $this->getTimezoneIdFromDtls(array('name' => $this->default_api_utc));
        				$update_date = true;
        			}
        			if (!isset($array_contract_line_details['ContractLineItemHeader']['billing_end_date']) && (empty($obj_line_history_retreive_bean->billing_end_date) || (!empty($obj_line_history_retreive_bean->billing_end_date) && (strtotime($obj_line_history_retreive_bean->billing_end_date) > strtotime($datetime_now))))) {
        				$obj_line_history_retreive_bean->billing_end_date = $datetime_now;
        				$obj_line_history_retreive_bean->gc_timezone_id3_c = $this->getTimezoneIdFromDtls(array('name' => $this->default_api_utc));
        				$update_date = true;
        			}

        			if ($update_date) $obj_line_history_retreive_bean->save();
        		}
        		if (!is_null($billing_affiliate_id)) {
        			$array_contract_line_details['ContractLineItemHeader']['gc_organization_id_c'] = $billing_affiliate_id;
        		}
        		if (isset($array_contract_line_details['InvoiceGroupDtls']['name_xml_id']) || isset($array_contract_line_details['InvoiceGroupDtls']['description_xml_id'])) {
        			if (isset($array_contract_line_details['InvoiceGroupDtls']['name_xml_id'])) {
        				$fields['name_uuid'] = $array_contract_line_details['InvoiceGroupDtls']['name_xml_id'];
        			}
        			if (isset($array_contract_line_details['InvoiceGroupDtls']['description_xml_id'])) {
        				$fields['description_uuid'] = $array_contract_line_details['InvoiceGroupDtls']['description_xml_id'];
        			}
        			$obj_group_invoice_bean_retreive = $obj_group_invoice_bean->retrieve_by_string_fields($fields);
        			if (!empty($obj_group_invoice_bean_retreive->id)) {
        				if (isset($array_contract_line_details['InvoiceGroupDtls']['name_xml_id'])) {
        					$obj_group_invoice_bean_retreive->name = $array_contract_line_details['InvoiceGroupDtls']['name'];
        				}
        				if (isset($array_contract_line_details['InvoiceGroupDtls']['description_xml_id'])) {
        					$obj_group_invoice_bean_retreive->description = $array_contract_line_details['InvoiceGroupDtls']['description'];
        				}
        				$obj_group_invoice_bean_retreive->save();
        			}

        			unset($array_contract_line_details['InvoiceGroupDtls']);
        		}

        		if (isset($array_contract_line_details['ContractLineItemHeader']['payment_type_flag']) && isset($array_contract_line_details['ContractLineItemHeader']['payment_type']) && $array_contract_line_details['ContractLineItemHeader']['payment_type_flag'] == 0) {
        			switch ((string) $array_contract_line_details['ContractLineItemHeader']['payment_type']) {
        				case '00':
        					$payment_mode_bean = BeanFactory::getBean('gc_DirectDeposit');
        					$payment_detail = $payment_mode_bean->retrieve_by_string_fields(array('contract_history_id'=>$obj_line_history_retreive_bean->id));
        					if (!empty($payment_detail->id)) $array_contract_line_details['ContractDirectDeposit']['id'] = $payment_detail->id;
        					break;
        				case '10':
        					$payment_mode_bean = BeanFactory::getBean('gc_AccountTransfer');
        					$payment_detail = $payment_mode_bean->retrieve_by_string_fields(array('contract_history_id'=>$obj_line_history_retreive_bean->id));
        					if (!empty($payment_detail->id)) $array_contract_line_details['ContractAccountTransfer']['id'] = $payment_detail->id;
        					break;
        				case '30':
        					$payment_mode_bean = BeanFactory::getBean('gc_PostalTransfer');
        					$payment_detail = $payment_mode_bean->retrieve_by_string_fields(array('contract_history_id'=>$obj_line_history_retreive_bean->id));
        					if (!empty($payment_detail->id)) $array_contract_line_details['ContractPostalTransfer']['id'] = $payment_detail->id;
        					break;
        				case '60':
        					$payment_mode_bean = BeanFactory::getBean('gc_CreditCard');
        					$payment_detail = $payment_mode_bean->retrieve_by_string_fields(array('contract_history_id'=>$obj_line_history_retreive_bean->id));
        					if (!empty($payment_detail->id)) $array_contract_line_details['ContractCreditCard']['id'] = $payment_detail->id;
        					break;
        			}
        		}
        		$response = array();
        		$array_contract_line_details['line_item_id'] = $obj_line_history_retreive_bean->line_item_id;
        		$array_contract_line_details['ContractLineItemHeader']['line_item_history_id'] = $obj_line_history_retreive_bean->id;
        		$this->setContractLineItemEntryService($array_contract_line_details, $obj_contracts_bean->id, $tech_account_id, $bill_account_id, true, $response);
           }
        }

        // 11. update contract header module params
        $this->setContractHeaderEntryService($array_contracts_params['ContractDetails']['ContractsHeader'], '', '', $obj_contracts_bean);

        // 12. return service resonse after successful update
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return $this->modUtils->buildServiceResponse($this->api_version, '1', '1003', '', '');
    }

    /**
     * save SugarCRM Corporate module
     *
     * @param array $array_corporates_params corporate details array(db_field_name=>value)
     * @param string $corporate_id Corporate object Id
     * @return object method will return corporate module sugar bean object
     * @author sagar.salunkhe
     * @since Oct 08, 2015
     */
    private function setCorporateEntryService($array_corporates_params, $corporate_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'array_corporates_params: '.print_r($array_corporates_params,true).GCM_GL_LOG_VAR_SEPARATOR.'corporate_id: '.print_r($corporate_id,true), 'Method to save SugarCRM Corporate module');
        $execute_save = false;
        $patch_request = false;

        $obj_corporates_bean = BeanFactory::getBean('Accounts');
        if (!empty($corporate_id)) {
            $obj_corporates_bean->retrieve($corporate_id);
            $patch_request = true;
        }

        // 1. Validate : If corporate array is not set, return empty accounts object
        if (!isset($array_corporates_params) || empty($array_corporates_params)) {
            return $obj_corporates_bean;
        }

        // 2. Condition : Divide the process on CID
        if (isset($array_corporates_params['common_customer_id_c']) && !empty($array_corporates_params['common_customer_id_c'])) { // 2-1. CID is present in API Request

            if ($patch_request && !empty($obj_corporates_bean->id) && ($obj_corporates_bean->common_customer_id_c != $array_corporates_params['common_customer_id_c'])) {
                // 2-1.(1) CID in Request API and Existing Contract Corporate CID does not match
                $obj_corporates_bean = BeanFactory::getBean('Accounts'); // reinitiate the same object
                $obj_corporates_bean->retrieve_by_string_fields(array(
                                                                    'common_customer_id_c' => $array_corporates_params['common_customer_id_c']
                ));
            } else {
                $obj_corporates_bean->retrieve_by_string_fields(array(
                                                                    'common_customer_id_c' => $array_corporates_params['common_customer_id_c']
                )); // 2-1.(2) retrieve GCM corporate record
            }

            if (empty($obj_corporates_bean->id) && (isset($array_corporates_params['cidas_data']) && $array_corporates_params['cidas_data'] == 'Yes')) {
                $execute_save = true; // Record not found in gcm db but in CIDAS
            }
        } else { // 2-2. CID is not present in API Request
            $execute_save = true;

            // 2-2.(1) If CID is present in linked corporate [patch]
            if ($patch_request && !empty($obj_corporates_bean->common_customer_id_c)) {
                $obj_corporates_bean = BeanFactory::getBean('Accounts'); // reinitiate the same object
            }
        }

        // 3. If CID is empty, set null
        /* if (empty($array_corporates_params['common_customer_id_c'])) $array_corporates_params['common_customer_id_c'] = self::NULL_CHAR; */

        // 4. Update corporate array object with params
        foreach ($array_corporates_params as $field => $value) {
            $obj_corporates_bean->$field = $value;
        }

        if (empty($obj_corporates_bean->id)) {
            $obj_corporates_bean->assigned_user_id = $this->obj_current_user->id; // current session user id
        }

        // 5. Execute save
        if ($execute_save) {
            $obj_corporates_bean->save();
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return $obj_corporates_bean;
    }

    /**
     * method to Save Accounts (Contacts module of Sugar)
     *
     * @param array $array_accounts_params array with required details to save Accounts record
     * @param string $account_id Accounts Module Object Id
     * @return object return Accounts module sugar bean object
     * @author sagar.salunkhe
     * @since Oct 08, 2015
     */
    private function setAccountEntryService($array_accounts_params, $account_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'array_accounts_params: '.print_r($array_accounts_params,true).GCM_GL_LOG_VAR_SEPARATOR.'account_id: '.print_r($account_id,true), 'Method to Save Accounts (Contacts module of Sugar)');
        $obj_accounts_bean = BeanFactory::getBean('Contacts');

        // do not create record if params are blank
        if (empty($array_accounts_params)) {
            return $obj_accounts_bean;
        }

        $flag_update = false;
        if (!empty($account_id)) {
            $obj_accounts_bean->retrieve($account_id);
            $flag_update = true;
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'flag_update: '.print_r(
            $flag_update,true), 'Debug');
        $arr_corporate_dtls = isset($array_accounts_params['CorporateDetails']) ? $array_accounts_params['CorporateDetails'] : '';

        if (!empty($arr_corporate_dtls)) {
            $arr_corporate_dtls['contact_type_c'] = $array_accounts_params['contact_type_c']; // set related account type
        }

        // check if corporate record exists on the basis of Common cutomer id or corporate name
        if ($flag_update) {
            $obj_corporate_bean = $this->setCorporateEntryService($arr_corporate_dtls, $obj_accounts_bean->account_id);
        } else {
            $obj_corporate_bean = $this->setCorporateEntryService($arr_corporate_dtls, '');
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'obj_corporate_bean->id: '.print_r($obj_corporate_bean->id,true), 'Related Corporate Id');
        foreach ($array_accounts_params as $field => $value) {
            if (!is_array($value)) {
                $obj_accounts_bean->$field = $value;
            }
        }

        // JIRA # OM-28_OM-33 update corporate relation is possible in POST and PATCH contract method
        $obj_accounts_bean->account_id = $obj_corporate_bean->id; // Corporate CRM Record Id

        $country_code_numeric = isset($array_accounts_params['country_code_numeric']) ? $array_accounts_params['country_code_numeric'] : '';

        // set country module relate field id if country numberic code is present in array
        if (isset($array_accounts_params['country_code_numeric'])) {
            // fetch country object bean id on the basis of numeric country code
            $obj_country_bean = BeanFactory::getBean('c_country');

            if(!empty($country_code_numeric)) {
                $obj_country_bean->retrieve_by_string_fields(array(
                                                                    'country_code_numeric' => $array_accounts_params['country_code_numeric']
                ));
            }

            // if country changed in patch then empty all country related fields
            if ($flag_update) {
                $prev_country_bean = BeanFactory::getBean('c_country', $obj_accounts_bean->c_country_id_c);
                if ($country_code_numeric != $prev_country_bean->country_code_numeric) {
                    $obj_accounts_bean->postal_no_c = (isset($array_accounts_params['postal_no_c'])) ? $array_accounts_params['postal_no_c'] : '';
                    $obj_accounts_bean->c_state_id_c = '';
                    $obj_accounts_bean->addr_1_c = '';
                    $obj_accounts_bean->addr_2_c = '';
                    $obj_accounts_bean->city_general_c = ''; // general city
                    $obj_accounts_bean->addr_general_c = ''; // general address

                    $obj_accounts_bean->attention_line_c = '';
                }
            }

            $obj_accounts_bean->c_country_id_c = $obj_country_bean->id; // Country CRM Record Id
        } elseif ($flag_update) {
            $obj_country_bean = BeanFactory::getBean('c_country', $obj_accounts_bean->c_country_id_c);
            $array_accounts_params['country_code_numeric'] = $country_code_numeric = $obj_country_bean->country_code_numeric;
        }

        if (isset($array_accounts_params['attention_line_c'])) {
            $obj_accounts_bean->attention_line_c = $array_accounts_params['attention_line_c'];
        }

        if ($country_code_numeric == '840' || $country_code_numeric == '124') {
            $general_address = isset($array_accounts_params['general_address']) ? $array_accounts_params['general_address'] : array();
            // if general/global address array key is defined then save fields
            if (count($general_address) > 0) {
                if (isset($general_address['city_general_c'])) {
                    $obj_accounts_bean->city_general_c = $general_address['city_general_c'];
                }
                if (isset($general_address['addr_general_c'])) {
                    $obj_accounts_bean->addr_general_c = $general_address['addr_general_c'];
                }
                if (isset($general_address['state_general_code'])) {
                    $obj_state_bean = BeanFactory::getBean('c_State');
                    $obj_state_bean->retrieve_by_string_fields(array(
                                                                    'state_code' => $general_address['state_general_code']
                    ));
                    $obj_accounts_bean->c_state_id_c = $obj_state_bean->id;
                }
            }
        } else {
            // fill latin address finally
            if (isset($array_accounts_params['addr_1_c'])) {
                $obj_accounts_bean->addr_1_c = $array_accounts_params['addr_1_c']; // Address (Latin Character)
            }
            if (isset($array_accounts_params['addr_2_c'])) {
                $obj_accounts_bean->addr_2_c = $array_accounts_params['addr_2_c']; // Address (Local Character)
            }
        }

        $obj_accounts_bean->save();
        /* END : insert record into Main Accounts module */

        // if japan specific address key is defined then create record in accounts_js module
        if ($country_code_numeric == '392') {
            // Country Japan
            if (isset($array_accounts_params['Accounts_js'])) {
                $this->setAccountsJapanSpecEntryService($array_accounts_params, $obj_accounts_bean->id, $flag_update);
            }
        } elseif ($flag_update) {
            // in case of other countries, clear japan specific address fields
            $array_accounts_params['Accounts_js']['addr1'] = '';
            $array_accounts_params['Accounts_js']['addr2'] = '';
            $this->setAccountsJapanSpecEntryService($array_accounts_params, $obj_accounts_bean->id, $flag_update);
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return $obj_accounts_bean;
    }

    /**
     * method to Save Accounts (Japan Specific)
     *
     * @param array $array_accounts_params array with required details to save Accounts record
     * @param string $account_id Accounts module object/bean record id
     * @param boolean $flag_update flag to define whether to insert or update the record based on Account Id
     * @author sagar.salunkhe
     * @since Oct 27, 2015
     */
    private function setAccountsJapanSpecEntryService($array_accounts_params, $account_id, $flag_update)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'array_accounts_params: '.print_r($array_accounts_params,true).GCM_GL_LOG_VAR_SEPARATOR.'account_id: '.print_r($account_id,true).GCM_GL_LOG_VAR_SEPARATOR.'flag_update: '.print_r($flag_update,true), 'Method to Save Accounts (Japan Specific)');

        $obj_accountsjs_bean = BeanFactory::getBean('js_Accounts_js');

        if ($flag_update) {
            $obj_accountsjs_bean->retrieve_by_string_fields(array(
                                                                'contact_id_c' => $account_id
            ));
            $obj_accountsjs_bean->retrieve($obj_accountsjs_bean->id);
            if (isset($array_accounts_params['Accounts_js']['addr1'])) {
                $obj_accountsjs_bean->addr1 = $array_accounts_params['Accounts_js']['addr1']; // Address 1 [Japan specific]
            }
            if (isset($array_accounts_params['Accounts_js']['addr2'])) {
                $obj_accountsjs_bean->addr2 = $array_accounts_params['Accounts_js']['addr2']; // Address 2 [Japan specific]
            }
        } else {
            $obj_accountsjs_bean->addr1 = $array_accounts_params['Accounts_js']['addr1']; // Address 1 [Japan specific]
            $obj_accountsjs_bean->addr2 = $array_accounts_params['Accounts_js']['addr2']; // Address 2 [Japan specific]
            $obj_accountsjs_bean->assigned_user_id = $this->obj_current_user->id; // session user crm record id
        }

        $obj_accountsjs_bean->contact_id_c = $account_id; // Account CRM Record Id
        $obj_accountsjs_bean->save();

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * method to Save Contract Header level details
     *
     * @author sagar.salunkhe
     * @param array $array_contract_header_params array with required details to save contract header record
     * @param object $obj_accounts_bean Accounts module sugar bean object
     * @param object $obj_corporates Corporates module sugar bean object
     * @param object $obj_contracts_bean reference object of gc_contracts module sugar bean
     * @return object method will return gc_Contracts module sugar bean object
     * @since Oct 08, 2015
     */
    private function setContractHeaderEntryService($array_contract_header_params, $obj_accounts_bean, $obj_corporates, &$obj_contracts_bean)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'array_contract_header_params: '.print_r($array_contract_header_params,true).GCM_GL_LOG_VAR_SEPARATOR.'obj_contracts_bean: '.$obj_contracts_bean->id, 'Method to Save Contract Header level details');

        global $sugar_config;
        $object_id = $obj_contracts_bean->id;

        if (isset($array_contract_header_params['product_offering']) && !empty($array_contract_header_params['product_offering'])) {
            if (isset($sugar_config['workflow_flag'][$array_contract_header_params['product_offering']]) && $sugar_config['workflow_flag'][$array_contract_header_params['product_offering']] == 'Yes') {
                // DF-1398 : New workflow for Enterprise Mail
                if (in_array($array_contract_header_params['product_offering'], $sugar_config['enterprise_mail_po']))
                    $array_contract_header_params['contract_status'] = '4';
                else
                    $array_contract_header_params['contract_status'] = '0';
            } else {
                $array_contract_header_params['contract_status'] = '4';
            }
        }

        $arr_status_workflag_comb = array(
                                        '5' => '5',
                                        '2' => '8'
        );
        if (isset($array_contract_header_params['contract_status']) && array_key_exists($array_contract_header_params['contract_status'], $arr_status_workflag_comb) && !in_array($obj_contracts_bean->product_offering, $sugar_config['enterprise_mail_po'])) {
            $array_contract_header_params['workflow_flag'] = $arr_status_workflag_comb[$array_contract_header_params['contract_status']];
            // log entry into approval logs (gc_comments) module
            if ($array_contract_header_params['workflow_flag'] == '5') {
                $this->saveContractComments($obj_contracts_bean->id, '', '4', 'SendTerminationApproval');
            }
        }

        if (isset($array_contract_header_params['contract_startdate_timezone'])) {
            if((!empty($obj_contracts_bean->id) && !empty($obj_contracts_bean->contract_startdate)) || (empty($obj_contracts_bean->id) && isset($array_contract_header_params['contract_startdate']) && !empty($array_contract_header_params['contract_startdate']))) {
                $array_contract_header_params['gc_timezone_id_c'] = $this->getTimezoneIdFromDtls(array('name' => $array_contract_header_params['contract_startdate_timezone']));
            }
        }

        if (isset($array_contract_header_params['contract_enddate_timezone'])) {
            if((!empty($obj_contracts_bean->id) && (!empty($obj_contracts_bean->contract_enddate) || !empty($array_contract_header_params['contract_enddate']))) || (empty($obj_contracts_bean->id) && isset($array_contract_header_params['contract_enddate']) && !empty($array_contract_header_params['contract_enddate']))) {
                $array_contract_header_params['gc_timezone_id1_c'] = $this->getTimezoneIdFromDtls(array(
                                                                                                    'name' => $array_contract_header_params['contract_enddate_timezone']
                ));
            }
        }

        foreach ($array_contract_header_params as $field => $value) {
            $obj_contracts_bean->$field = $value;
        }

        // update the fields which are not passed through XML (only when action is insert)
        if (empty($object_id)) {
            $obj_contracts_bean->date_entered_timezone = $this->timezone_offset; // date entered timezone
            $obj_contracts_bean->assigned_user_id = $this->obj_current_user->id; // session user crm record id
            $obj_contracts_bean->accounts_gc_contracts_1accounts_ida = $obj_corporates->id; // Corporate CRM Record Id related to contract type account(contact)
            $obj_contracts_bean->contacts_gc_contracts_1contacts_ida = $obj_accounts_bean->id; // Contract type Accounts CRM Record Id
        } else {
            $obj_contracts_bean->lock_version += 1;
        }

        $obj_contracts_bean->date_modified_timezone = $this->timezone_offset; // date modified timezone
        $obj_contracts_bean->save();

        if(isset($array_contract_header_params['surrogate_teams'])){
            $obj_contracts_bean->retrieve($obj_contracts_bean->id); // retrieve all relationships
            $obj_contracts_bean->load_relationship('teams');

            $contract_teams = array();
            foreach ($array_contract_header_params['surrogate_teams'] as $i => $team_name) {
                $team_id = $this->modUtils->retrieveTeamId($team_name);
                if ($team_id) {
                    $contract_teams[] = $team_id;
                    $this->response_data['ContractDetails']['ContractHeader']['local_uuid'][$array_contract_header_params['surrogate_teams_xml_id'][$i]] = $team_id;
                }
            }
            if (count($contract_teams) > 0) {
                $obj_contracts_bean->teams->add($contract_teams);
            }
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return $obj_contracts_bean;
    }

    /**
     * method to Save Contract Line Item config summary Details
     *
     * @param array $array_line_config_summary_params array with required details to save contract line item config summary record
     * @param string $contract_line_item_id related line item record id
     * @return object Contract Line Item config summary record object/bean
     * @author sagar.salunkhe
     * @since Oct 08, 2015
     */
    private function setContractLineConfigSummaryService($array_line_config_summary_params, $contract_line_item_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'array_contract_header_params: '.print_r($array_line_config_summary_params,true).GCM_GL_LOG_VAR_SEPARATOR.'contract_line_item_id: '.print_r($contract_line_item_id,true), 'Method to Save Contract Line Item config summary Details');
        // NOTE : this function is not in scope of pj1
        $obj_line_config_summary_bean = BeanFactory::getBean('gc_Line_Item_Config_Summary');

        foreach ($array_line_config_summary_params as $field => $value) {
            $obj_line_config_summary_bean->$field = $value;
        }

        $obj_line_config_summary_bean->save();

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return $obj_line_config_summary_bean;
    }

    /**
     * method to Deactivate SugarCRM Contract Record
     *
     * @param string contract_record_id SugarCRM Contract Record CRM Id
     * @return boolean true/false
     * @author sagar.salunkhe
     * @since Oct 21, 2015
     * @deprecated api v1 function hence deprecated
     */
    public function deactivateContractEntryService($contract_record_id)
    {
        $svc_response['ResponseCode'] = 0;
        $svc_response['ServiceResponseCode'] = 0;
        $svc_response['ResponseMsg'] = __METHOD__ . " is deprecated since api v2.0!" ;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'contract_record_id: '.print_r($contract_record_id,true), print_r($svc_response,true));
        return $svc_response;
    }

    /**
     * manipulate contract data before processing for Save
     *
     * @param array $contract_data
     * @author sagar.salunkhe
     * @since Aug 2, 2016
     */
    private function processContractVersionUpgradeData(&$contract_data)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'contract_data: '.print_r($contract_data,true), 'Manipulate contract data before processing for Save');
        $global_contract_id_xml_id = $global_contract_id = '';

        if(isset($contract_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id']))
            $global_contract_id_xml_id = $contract_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];

        if(isset($contract_data['ContractDetails']['ContractsHeader']['global_contract_id']))
            $global_contract_id = $contract_data['ContractDetails']['ContractsHeader']['global_contract_id'];

        $key_new_rec = 'is_new_record';
        $obj_contracts_bean = BeanFactory::getBean('gc_Contracts');

        if (empty($contract_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id']) && !empty($contract_data['ContractDetails']['ContractsHeader']['global_contract_id'])) {
            $obj_contracts_bean->retrieve_by_string_fields(array(
                                                                'global_contract_id' => $contract_data['ContractDetails']['ContractsHeader']['global_contract_id']
            ));
            $contract_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'] = $obj_contracts_bean->global_contract_id_uuid;
        } elseif (!empty($contract_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id']) && empty($contract_data['ContractDetails']['ContractsHeader']['global_contract_id'])) {
                $obj_contracts_bean->retrieve_by_string_fields(array(
                                                                    'global_contract_id_uuid' => $contract_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id']
                ));
                $contract_data['ContractDetails']['ContractsHeader']['global_contract_id'] = $obj_contracts_bean->global_contract_id;
            }

        self::getFieldLocalIdsFromXmlData($contract_data['ContractDetails']['ContractsHeader']);
        self::getRowLocalIdsFromDb('gc_Contracts', 'global_contract_id_uuid', $contract_data['ContractDetails']['ContractsHeader']);

        // increment contract version by 1
        $latest_contract_version = $this->getLatestContractVersion($contract_data['ContractDetails']['ContractsHeader']['global_contract_id']);
        $latest_contract_version += 1;
        $contract_data['ContractDetails']['ContractsHeader']['contract_version'] = $latest_contract_version;

        foreach ($contract_data['ContractDetails']['ContractLineItemDetails'] as $key => $array_contract_line_details) {
            if (isset($array_contract_line_details[$key_new_rec]) && $array_contract_line_details[$key_new_rec] == true){
                continue; // do not copy local uuids if line item record is new
            }

            self::getFieldLocalIdsFromXmlData($contract_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineItemHeader']);
            self::getRowLocalIdsFromDb('gc_Line_Item_Contract_History', 'line_item_id_uuid', $contract_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineItemHeader']);

            if(isset($contract_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineProductConfiguration'])) {
                foreach ($contract_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineProductConfiguration'] as $i => $val) {
                        self::getFieldLocalIdsFromXmlData($contract_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineProductConfiguration'][$i]);
                }
            }

            if(isset($contract_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineProductRule'])) {
                foreach ($contract_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineProductRule'] as $i => $val) {
                        self::getFieldLocalIdsFromXmlData($contract_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineProductRule'][$i]);
                }
            }

            if(isset($contract_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineProductPrice'])) {
                foreach ($contract_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineProductPrice'] as $i => $val) {
                    if (isset($array_contract_line_details['ContractLineProductPrice'][$i][$key_new_rec]) && $array_contract_line_details['ContractLineProductPrice'][$i][$key_new_rec] != true){
                        self::getFieldLocalIdsFromXmlData($contract_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineProductPrice'][$i]);
                    }
                }
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return;
    }

    /**
     * get uuids of fields from xml array (data is available in fields with suffix _xml_id)
     *
     * @param array $data_array (by reference)
     * @author sagar.salunkhe
     * @since Aug 3, 2016
     */
    private function getFieldLocalIdsFromXmlData(&$data_array)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'data_array: '.print_r($data_array,true), 'Get uuids of fields from xml array (data is available in fields with suffix _xml_id)');
        $suffix = '_xml_id';
        $uuid_suffix = '_uuid';

        foreach ($data_array as $key => $val) {
            if (substr($key, -(strlen($suffix))) === $suffix) {
                $new_key = str_replace($suffix, $uuid_suffix, $key);
                $data_array[$new_key] = $val;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return true;
    }

    /**
     * fetch uuids from previous version record, if it is blank in provided data array
     *
     * @param string $module_bean_name module object name
     * @param string $field_id uuid field name
     * @param array $data_array data array
     * @return boolean
     * @author sagar.salunkhe
     * @since Aug 4, 2016
     */
    private function getRowLocalIdsFromDb($module_bean_name, $field_id, &$data_array)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'field_id: '.print_r($field_id,true).GCM_GL_LOG_VAR_SEPARATOR.'data_array: '.print_r($data_array,true), 'Fetch uuids from previous version record, if it is blank in provided data array');
        $bean = BeanFactory::getBean($module_bean_name);
        $module_field_defs = $bean->getFieldDefinitions(); // fetch module fields from module bean

        $bean->retrieve_by_string_fields(array(
                                                $field_id => $data_array[$field_id]
        ));

        foreach ($module_field_defs as $field_name => $field_def) {
            // if field name has defined suffix & uuid value is empty
            if ((substr($field_name, -(strlen('_uuid'))) === '_uuid') && (!isset($data_array[$field_name]) || empty($data_array[$field_name]) || !($this->modUtils->validateUUID($data_array[$field_name]))))
                $data_array[$field_name] = $bean->$field_name;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return true;
    }

    /**
     * get billing affiliate record
     *
     * @param array $params
     * @return string $record_id record object id
     * @author sagar.salunkhe
     * @since Nov 2, 2016
     * @internal handled only for post requests and not patch
     */
    private function getBillingAffiliateRecord($params)
    {
         ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'params: '.print_r($params,true), 'Get billing affiliate record');
        $record_id = '';
        if (count($params) > 0) {
            $obj_gc_organization = BeanFactory::getBean('gc_organization');
            $where_org_cond  = "(organization_code_1 = '" . $params['organization_code_1'] . "'";
            if($params['organization_code_1'] == '')
            	$where_org_cond  .= " or organization_code_1 is null";
            $where_org_cond .= " )and (organization_code_2 = '" . $params['organization_code_2'] . "'";
            if($params['organization_code_2'] == '')
            		$where_org_cond  .= " or organization_code_2 is null";
            $where_org_cond .= ")";
            $org_search_list = $obj_gc_organization->get_full_list('', $where_org_cond);
            $org_id = "";
            if ($org_search_list != null) {
            	foreach ($org_search_list as $org) {
            		$org_id = $org->id;
            	}
            }
            $record_id = $org_id;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'record_id: '.$record_id, '');
        return $record_id;
    }
}
?>