<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

require_once('include/Dashlets/DashletGeneric.php');

class CustomContractsDashletView extends DashletGeneric { 
    function CustomContractsDashletView($id, $def = null) {
        global $current_user, $app_strings, $app_list_strings;
		
		include_once('custom/modules/gc_Contracts/include/ClassGcContracts.php');
		$obj_gc_contracts = new ClassGcContracts();
        $app_list_strings['product_offering_list'] = $obj_gc_contracts->getProductOfferings(false);

		require('custom/modules/gc_Contracts/Dashlets/CustomContractsDashletView/CustomContractsDashletView.data.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_CUSTOM_CONTRACTS_DASHLET_VIEW', 'gc_Contracts');

        $this->searchFields = $dashletData['CustomContractsDashletView']['searchFields'];
        $this->columns = $dashletData['CustomContractsDashletView']['columns'];

        $this->seedBean = new gc_Contracts();        
    }
   
}

?>