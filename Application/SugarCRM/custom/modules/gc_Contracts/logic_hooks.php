<?php
$hook_array = Array();
// position, file, function
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(
    1,
    'Set general params before saving record',
    'custom/modules/gc_Contracts/include/ClassBeforeSave.php',
    'ClassGcContractBeforeSave',
    'setGeneralParams'
);
$hook_array['before_save'][] = Array(
    2,
    'check Contract Modification Availability',
    'custom/modules/gc_Contracts/include/ClassBeforeSave.php',
    'ClassGcContractBeforeSave',
    'checkContractModificationAvailability'
);
$hook_array['before_save'][] = Array(
    3,
    'workflow',
    'include/workflow/WorkFlowHandler.php',
    'WorkFlowHandler',
    'WorkFlowHandler'
);
$hook_array['before_save'][] = Array(
    4,
    'Set Global Contract Id',
    'custom/modules/gc_Contracts/include/ClassBeforeSave.php',
    'ClassGcContractBeforeSave',
    'setGlobalContractId'
);
$hook_array['before_save'][] = Array(
    5,
    'Set UUIDs in field specific uuid columns',
    'custom/include/ClassGenericLogicHook.php',
    'ClassGenericLogicHook',
    'setModuleFieldUUIDs'
);

$hook_array['before_save'][] = Array(
    6,
    'Lock version control on GUI',
    'custom/modules/gc_Contracts/include/ClassBeforeSave.php',
    'ClassGcContractBeforeSave',
    'setLockVersion'
);
$hook_array['before_save'][] = Array(
    7,
    'custom fields audit entry',
    'custom/modules/gc_Contracts/include/ClassBeforeSave.php',
    'ClassGcContractBeforeSave',
    'setContractHeaderAuditEntry'
);

$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(
    1,
    'Update Contract',
    'custom/modules/gc_Contracts/include/ClassAfterSave.php',
    'ClassGcContractAfterSave',
    'saveContractSubPanel'
);
$hook_array['after_save'][] = Array(
    2,
    'version upgrade save logic goes here',
    'custom/modules/gc_Contracts/include/ClassAfterSave.php',
    'ClassGcContractAfterSave',
    'processVersionUpgradeSave'
);

$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(
    1,
    'Delete Contract',
    'custom/modules/gc_Contracts/include/ClassBeforeDelete.php',
    'ClassGcContractBeforeDelete',
    'deleteContractRelatedRecords'
);

$hook_array['after_retrieve'] = array();
$hook_array['after_retrieve'][] = array(
    1,
    'Convert DB Contract date into local',
    'custom/modules/gc_Contracts/include/ClassAfterRetrieve.php',
    'ClassGcContractAfterRetrieve',
    'processContractHeaderFields'
);