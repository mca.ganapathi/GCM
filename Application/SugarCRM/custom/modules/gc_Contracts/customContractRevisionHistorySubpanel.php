<?php

/**
 * Function to retrieve contract revision history
 *
 * @return array
 * @since Dec 29, 2016
 */
function get_all_versions_of_contract($params)
{
    $args = func_get_args();
    $global_contract_id = $args[0]['global_contract_id'];
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'global_contract_id : ' . $global_contract_id, 'Function to retrieve contract revision history');
    $return_array['select'] = " SELECT global_contract_id,name,contract_status,contract_version,ext_sys_created_by";
    $return_array['from'] = " FROM gc_contracts  ";
    $return_array['where'] = " WHERE global_contract_id = '" . $global_contract_id . "'";
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_array : ' . print_r($return_array, true), 'Function to retrieve contract revision history');
    return $return_array;
}
