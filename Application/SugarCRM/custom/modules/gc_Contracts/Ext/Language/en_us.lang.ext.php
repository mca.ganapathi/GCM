<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Language/en_us.contract-revision-history.php
 
 // created: 2015-10-05 06:39:32
$mod_strings['LBL_CONTRACT_REVISION_HISTORY'] = 'Contract Revision History';

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Language/en_us.ContractDeploy061015.php
 
 // created: 2015-10-06 05:18:39
$mod_strings['LBL_CONTRACT_CUST_COMPANY_ACCOUNT_ID'] = 'Contracting Customer Company (related Corporate ID)';
$mod_strings['LBL_CONTRACT_CUST_COMPANY'] = 'Contracting Customer Company';
$mod_strings['LBL_NDB_QUOTE_ID'] = 'Quote ID';
$mod_strings['LBL_NDB_QUOTE_NAME'] = 'Quote Name';
$mod_strings['LBL_NDB_SOLUTION_ID'] = 'Solution ID';
$mod_strings['LBL_NO_RECORD_FOUND'] = 'No record found.';
$mod_strings['LBL_DETAILVIEW_PANEL2'] = 'Line Item Details';
$mod_strings['LBL_CREATED'] = 'Sugar Created By';
$mod_strings['LBL_DATE_ENTERED'] = 'Sugar Date Created';
$mod_strings['LBL_MODIFIED_NAME'] = 'Sugar Modified By Name';
$mod_strings['LBL_DATE_MODIFIED'] = 'Sugar Date Modified';


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Language/en_us.ContractLineItemPanel.php
 
 // created: 2015-10-05 06:39:32
$mod_strings['LBL_CONTRACT_CUST_COMPANY_ACCOUNT_ID'] = 'Contracting Customer Company (related Corporate ID)';
$mod_strings['LBL_CONTRACT_CUST_COMPANY'] = 'Contracting Customer Company';
$mod_strings['LBL_NDB_QUOTE_ID'] = 'Quote ID';
$mod_strings['LBL_NDB_QUOTE_NAME'] = 'Quote Name';
$mod_strings['LBL_NDB_SOLUTION_ID'] = 'Solution ID';
$mod_strings['LBL_NO_RECORD_FOUND'] = 'No record found.';
$mod_strings['LBL_DETAILVIEW_PANEL2'] = 'Line Item Details';
$mod_strings['LBL_CREATED'] = 'Created By';
$mod_strings['LBL_DATE_ENTERED'] = 'Date Created';
$mod_strings['LBL_MODIFIED_NAME'] = 'Modified By Name';
$mod_strings['LBL_DATE_MODIFIED'] = 'Date Modified';


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Language/en_us.customaccounts_gc_contracts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contracting Customer Company';

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Language/en_us.customcontacts_gc_contracts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contracting Customer Company';
$mod_strings['LBL_CONTACTS_GC_CONTRACTS_1_FROM_CONTACTS_TITLE'] = 'Contracting Customer Account';

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Language/en_us.customgc_contracts_documents_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contracting Customer Company';
$mod_strings['LBL_CONTACTS_GC_CONTRACTS_1_FROM_CONTACTS_TITLE'] = 'Contracting Customer Account';
$mod_strings['LBL_PM_PRODUCTS_GC_CONTRACTS_1_FROM_PM_PRODUCTS_TITLE'] = 'Product';
$mod_strings['LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Documents';

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Language/en_us.customgc_contracts_gc_invoicesubtotalgroup_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_ACCOUNTS_TITLE'] = 'Corporates';
$mod_strings['LBL_CONTACTS_GC_CONTRACTS_1_FROM_CONTACTS_TITLE'] = 'Accounts';
$mod_strings['LBL_PM_PRODUCTS_GC_CONTRACTS_1_FROM_PM_PRODUCTS_TITLE'] = 'Products';
$mod_strings['LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Documents';
$mod_strings['LBL_GC_CONTRACTS_GC_INVOICESUBTOTALGROUP_1_FROM_GC_INVOICESUBTOTALGROUP_TITLE'] = 'Invoice Subtotal Group';

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Language/en_us.custompm_products_gc_contracts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contracting Customer Company';
$mod_strings['LBL_CONTACTS_GC_CONTRACTS_1_FROM_CONTACTS_TITLE'] = 'Contracting Customer Account';
$mod_strings['LBL_PM_PRODUCTS_GC_CONTRACTS_1_FROM_PM_PRODUCTS_TITLE'] = 'Product';

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Language/en_us.StudioContractModule.php
 
 // created: 2015-09-30 06:48:19
$mod_strings['LBL_CONTRACT_CUST_COMPANY_ACCOUNT_ID'] = 'Contracting Customer Company (related Corporate ID)';
$mod_strings['LBL_CONTRACT_CUST_COMPANY'] = 'Contracting Customer Company';
$mod_strings['LBL_NDB_QUOTE_ID'] = 'Quote ID';
$mod_strings['LBL_NDB_QUOTE_NAME'] = 'Quote Name';
$mod_strings['LBL_NDB_SOLUTION_ID'] = 'Solution ID';
$mod_strings['LBL_DETAILVIEW_PANEL1'] = 'Sales Representative';


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Language/en_us.StudioContractModuleAddField.php
 
 // created: 2015-10-01 13:16:07
$mod_strings['LBL_CONTRACT_CUST_COMPANY_ACCOUNT_ID'] = 'Contracting Customer Company (related Corporate ID)';
$mod_strings['LBL_CONTRACT_CUST_COMPANY'] = 'Contracting Customer Company';
$mod_strings['LBL_NDB_QUOTE_ID'] = 'Quote ID';
$mod_strings['LBL_NDB_QUOTE_NAME'] = 'Quote Name';
$mod_strings['LBL_NDB_SOLUTION_ID'] = 'Solution ID';
$mod_strings['LBL_NO_RECORD_FOUND'] = 'No record found.';
$mod_strings['LBL_DETAILVIEW_PANEL2'] = 'Line Item Details';


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Language/en_us.StudioGcContract_6-2-1_060616.php
 
 // created: 2016-06-06 14:27:49
$mod_strings['LBL_CONTRACT_CUST_COMPANY_ACCOUNT_ID'] = 'Contracting Customer Company (related Corporate ID)';
$mod_strings['LBL_CONTRACT_CUST_COMPANY'] = 'Contracting Customer Company';
$mod_strings['LBL_CONTRACT_ACCOUNT_ID_CONTACT_ID'] = 'Contracting Customer Account (related Account ID)';
$mod_strings['LBL_CONTRACT_ACCOUNT_ID'] = 'Contracting Customer Account';
$mod_strings['LBL_NDB_QUOTE_ID'] = 'Quote ID';
$mod_strings['LBL_NDB_QUOTE_NAME'] = 'Quote Name';
$mod_strings['LBL_NDB_SOLUTION_ID'] = 'Solution ID';
$mod_strings['LBL_NO_RECORD_FOUND'] = 'No record found.';
$mod_strings['LBL_DETAILVIEW_PANEL2'] = 'Line Item Details';
$mod_strings['LBL_CREATED'] = 'Sugar Created By';
$mod_strings['LBL_DATE_ENTERED'] = 'Sugar Date Created';
$mod_strings['LBL_MODIFIED_NAME'] = 'Sugar Modified By Name';
$mod_strings['LBL_DATE_MODIFIED'] = 'Sugar Date Modified';
$mod_strings['LBL_EDITVIEW_PANEL1'] = 'New Panel 1';
$mod_strings['LBL_EDITVIEW_PANEL2'] = 'Sales Representative';
$mod_strings['LBL_DETAILVIEW_PANEL3'] = 'Contract Header Documents';
$mod_strings['LBL_ID'] = 'Object ID';
$mod_strings['LBL_EDITVIEW_PANEL3'] = 'Contract Line Item';
$mod_strings['LBL_NAME'] = 'Contract Name';
$mod_strings['LBL_CONTRACT_VERSION'] = 'Contract Version';
$mod_strings['LBL_TOTAL_NRC'] = 'Total NRC';
$mod_strings['LBL_TOTAL_MRC'] = 'Total MRC';
$mod_strings['LBL_GRAND_TOTAL'] = 'Grand Total';
$mod_strings['LBL_PAYMENT_TYPE_CHANGE_ALERT'] = 'Existing values of Payment Sub-Panel type will be cleared!<br/>Are you Sure?';
$mod_strings['LBL_PAYMENT_TYPE_CHANGE_TITLE'] = 'Payment Type Change Confirm';
$mod_strings['LBL_LINEITEM_REMOVE_ALERT'] = 'Are you sure you want to remove line item?';
$mod_strings['LBL_LINEITEM_REMOVE_TITLE'] = 'Remove Line Item Confirm';
$mod_strings['LBL_ACCOUNT_NO'] = 'Account Number';
$mod_strings['LBL_EXPIRATION_DATE'] = 'Expiration Date';
$mod_strings['LBL_GLOBAL_CONTRACT_ID'] = 'Global Contract ID';
$mod_strings['LBL_SELECT_PARENT_ALERT'] = 'Please select ~~ from the Header';
$mod_strings['LBL_SELECT_PARENT_TITLE'] = 'Select Parent Alert';
$mod_strings['LBL_DESCRIPTION'] = 'Contract Description';
$mod_strings['LBL_EDITVIEW_PANEL4'] = 'Contract Overview';
$mod_strings['LBL_TECH_ACCOUNT_ID'] = 'Technology Customer Account';
$mod_strings['LBL_BILL_ACOOUNT_ID'] = 'Billing Customer Account';
$mod_strings['LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Documents';
$mod_strings['LBL_LINEITEM_DETAILS'] = 'Line Item Details';
$mod_strings['LBL_GLOBAL_CONTRACT_ITEM_ID'] = 'Global Contract Item ID';
$mod_strings['LBL_OBJECT_ID'] = 'Object ID';
$mod_strings['LBL_CONTRACT_REQUEST_ID'] = 'External System Contract Request ID';
$mod_strings['LBL_SENDFORAPPROVAL'] = 'Send For Approval';
$mod_strings['LBL_APPROVE'] = 'Approve';
$mod_strings['LBL_REJECT'] = 'Reject';
$mod_strings['LBL_COMMENTS_TITLE'] = 'Comments Dialogue';
$mod_strings['LBL_COMMENTS'] = 'Please enter your comments';
$mod_strings['LBL_INVALID_STAGE'] = 'Invalid Contract Action. Please contact Administrator';
$mod_strings['LBL_PMS_CONNECTION_ERROR'] = 'Connection to PMS failed, Please try after some time or contact admin';
$mod_strings['LBL_CHARGE_TYPE'] = 'Charge Type';
$mod_strings['LBL_CHARGE_PERIOD'] = 'Charge Period';
$mod_strings['LBL_CURRENCY'] = 'Currency';
$mod_strings['LBL_LIST_PRICE'] = 'List Price';
$mod_strings['LBL_DISCOUNT'] = 'Discount';
$mod_strings['LBL_CONTRACT_PRICE'] = 'Contract Price';
$mod_strings['LBL_IGC_SETTLEMENT_PRICE'] = 'IGC Settlement Price';
$mod_strings['LBL_REMARKS'] = 'Remarks';
$mod_strings['LBL_NO_PRICE_DETAILS'] = 'No charge is available for this case';
$mod_strings['LBL_NO_RULE_API_VALUE'] = 'Reference label not exist in PMS API';
$mod_strings['LBL_PRODUCT_INFORMATION'] = 'Product Information';
$mod_strings['LBL_BASE_REQUIRED_ALERT'] = 'One Base specification to be included in contract';
$mod_strings['LBL_CHANGE_OFFER_ALERT'] = 'You are attempting to change the Product Offering. It will remove all the line item(s) and cannot be reverted. Are you sure want to proceed?';
$mod_strings['LBL_PRODUCT_PRICE_DEC_TITLE'] = 'Invalid Data';
$mod_strings['LBL_INVOICE_SUBTOT_GROUP'] = 'Invoice Subtotal Group';
$mod_strings['LBL_INVOICE_SUBTOT_GROUP_NAME'] = 'Name';
$mod_strings['LBL_INVOICE_SUBTOT_GROUP_DESC'] = 'Description';
$mod_strings['LBL_INVOICE_SUBTOT_GROUP_NEW'] = 'New Group Details';
$mod_strings['LBL_CLICK_ACTIVATION'] = 'Are you sure you want to activate the contract?';
$mod_strings['LBL_CONTRACT_PERIOD'] = 'Contract Period';
$mod_strings['LBL_CONTRACT_PERIOD_UNIT'] = 'Contract Period Unit';
$mod_strings['LBL_EXT_SYS_MODIFIED_BY'] = 'External System Modified By';
$mod_strings['LBL_EXT_SYS_CREATED_BY'] = 'External System Created By';
$mod_strings['LBL_CUSTOM_CONTRACTS_DASHLET_VIEW'] = 'My Contracts';


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_BILLING_TYPE'] = 'Billing Scheme';
$mod_strings['LBL_DATE_ENTERED'] = 'GCM Date Created';
$mod_strings['LBL_DATE_MODIFIED'] = 'GCM Date Modified';
$mod_strings['LBL_GLOBAL_CONTRACT_ID_UUID'] = 'Object ID';

?>
