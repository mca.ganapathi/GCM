<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/accounts_gc_contracts_1_gc_Contracts.php

 // created: 2016-09-29 04:39:38
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1']['name'] = 'accounts_gc_contracts_1';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1']['type'] = 'link';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1']['relationship'] = 'accounts_gc_contracts_1';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1']['source'] = 'non-db';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1']['module'] = 'Accounts';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1']['bean_name'] = 'Account';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1']['vname'] = 'LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_ACCOUNTS_TITLE';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1']['id_name'] = 'accounts_gc_contracts_1accounts_ida';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1_name']['name'] = 'accounts_gc_contracts_1_name';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1_name']['type'] = 'relate';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1_name']['source'] = 'non-db';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1_name']['vname'] = 'LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_ACCOUNTS_TITLE';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1_name']['save'] = true;
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1_name']['id_name'] = 'accounts_gc_contracts_1accounts_ida';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1_name']['link'] = 'accounts_gc_contracts_1';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1_name']['table'] = 'accounts';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1_name']['module'] = 'Accounts';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1_name']['rname'] = 'name';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1_name']['audited'] = true;
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1accounts_ida']['name'] = 'accounts_gc_contracts_1accounts_ida';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1accounts_ida']['type'] = 'id';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1accounts_ida']['relationship'] = 'accounts_gc_contracts_1';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1accounts_ida']['source'] = 'non-db';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1accounts_ida']['reportable'] = false;
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1accounts_ida']['side'] = 'right';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1accounts_ida']['vname'] = 'LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1accounts_ida']['link'] = 'accounts_gc_contracts_1';
$dictionary['gc_Contracts']['fields']['accounts_gc_contracts_1accounts_ida']['rname'] = 'id';

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/contacts_gc_contracts_1_gc_Contracts.php


$dictionary["gc_Contracts"]["fields"]["contacts_gc_contracts_1"] = array (
  'name' => 'contacts_gc_contracts_1',
  'type' => 'link',
  'relationship' => 'contacts_gc_contracts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_GC_CONTRACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_gc_contracts_1contacts_ida',
);
$dictionary["gc_Contracts"]["fields"]["contacts_gc_contracts_1_name"] = array (
  'name' => 'contacts_gc_contracts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_GC_CONTRACTS_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_gc_contracts_1contacts_ida',
  'link' => 'contacts_gc_contracts_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
  'audited' => true,
);
$dictionary["gc_Contracts"]["fields"]["contacts_gc_contracts_1contacts_ida"] = array (
  'name' => 'contacts_gc_contracts_1contacts_ida',
  'type' => 'id',
  'relationship' => 'contacts_gc_contracts_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
  'link' => 'contacts_gc_contracts_1',
  'rname' => 'id',
);

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/gc_contracts_documents_1_gc_Contracts.php

// created: 2016-01-07 11:52:10
$dictionary["gc_Contracts"]["fields"]["gc_contracts_documents_1"] = array (
  'name' => 'gc_contracts_documents_1',
  'type' => 'link',
  'relationship' => 'gc_contracts_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/gc_contracts_gc_invoicesubtotalgroup_1_gc_Contracts.php

// created: 2016-05-04 05:33:03
$dictionary["gc_Contracts"]["fields"]["gc_contracts_gc_invoicesubtotalgroup_1"] = array (
  'name' => 'gc_contracts_gc_invoicesubtotalgroup_1',
  'type' => 'link',
  'relationship' => 'gc_contracts_gc_invoicesubtotalgroup_1',
  'source' => 'non-db',
  'module' => 'gc_InvoiceSubtotalGroup',
  'bean_name' => 'gc_InvoiceSubtotalGroup',
  'side' => 'right',
  'vname' => 'LBL_GC_CONTRACTS_GC_INVOICESUBTOTALGROUP_1_FROM_GC_INVOICESUBTOTALGROUP_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/pm_products_gc_contracts_1_gc_Contracts.php

 // created: 2016-09-29 04:39:38
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1']['name'] = 'pm_products_gc_contracts_1';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1']['type'] = 'link';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1']['relationship'] = 'pm_products_gc_contracts_1';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1']['source'] = 'non-db';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1']['module'] = 'pm_Products';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1']['bean_name'] = 'pm_Products';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1']['vname'] = 'LBL_PM_PRODUCTS_GC_CONTRACTS_1_FROM_PM_PRODUCTS_TITLE';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1']['id_name'] = 'pm_products_gc_contracts_1pm_products_ida';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1_name']['name'] = 'pm_products_gc_contracts_1_name';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1_name']['type'] = 'relate';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1_name']['source'] = 'non-db';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1_name']['vname'] = 'LBL_PM_PRODUCTS_GC_CONTRACTS_1_FROM_PM_PRODUCTS_TITLE';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1_name']['save'] = true;
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1_name']['id_name'] = 'pm_products_gc_contracts_1pm_products_ida';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1_name']['link'] = 'pm_products_gc_contracts_1';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1_name']['table'] = 'pm_products';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1_name']['module'] = 'pm_Products';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1_name']['rname'] = 'name';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1_name']['audited'] = true;
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1pm_products_ida']['name'] = 'pm_products_gc_contracts_1pm_products_ida';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1pm_products_ida']['type'] = 'id';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1pm_products_ida']['relationship'] = 'pm_products_gc_contracts_1';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1pm_products_ida']['source'] = 'non-db';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1pm_products_ida']['reportable'] = false;
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1pm_products_ida']['side'] = 'right';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1pm_products_ida']['vname'] = 'LBL_PM_PRODUCTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1pm_products_ida']['link'] = 'pm_products_gc_contracts_1';
$dictionary['gc_Contracts']['fields']['pm_products_gc_contracts_1pm_products_ida']['rname'] = 'id';

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_billing_type.php

 // created: 2016-12-13 08:39:19
$dictionary['gc_Contracts']['fields']['billing_type']['audited']=true;
$dictionary['gc_Contracts']['fields']['billing_type']['full_text_search']['boost'] = 1;
$dictionary['gc_Contracts']['fields']['billing_type']['default']='OneStop';
$dictionary['gc_Contracts']['fields']['billing_type']['massupdate']=false;
 
?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_contract_enddate.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['contract_enddate']['audited'] = false;
$dictionary['gc_Contracts']['fields']['contract_enddate']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_contract_enddate_timezone.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['contract_enddate_timezone']['audited'] = false;

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_contract_period.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['contract_period']['required'] = false;
$dictionary['gc_Contracts']['fields']['contract_period']['audited'] = true;
$dictionary['gc_Contracts']['fields']['contract_period']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_contract_period_unit.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['contract_period_unit']['required'] = false;
$dictionary['gc_Contracts']['fields']['contract_period_unit']['audited'] = true;
$dictionary['gc_Contracts']['fields']['contract_period_unit']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_contract_startdate.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['contract_startdate']['audited'] = false;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_contract_startdate_timezone.php

 // created: 2017-03-24 09:24:12
$dictionary['gc_Contracts']['fields']['contract_startdate_timezone']['audited']=false;

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_contract_status.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['contract_status']['required'] = true;
$dictionary['gc_Contracts']['fields']['contract_status']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_contract_type.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['contract_type']['audited'] = true;
$dictionary['gc_Contracts']['fields']['contract_type']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_contract_version.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['contract_version']['required'] = true;
$dictionary['gc_Contracts']['fields']['contract_version']['default'] = '1';


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_description.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['description']['audited'] = true;
$dictionary['gc_Contracts']['fields']['description']['cols'] = '40';
$dictionary['gc_Contracts']['fields']['description']['rows'] = '4';
$dictionary['gc_Contracts']['fields']['description']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_ext_sys_created_by.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['ext_sys_created_by']['audited'] = true;
$dictionary['gc_Contracts']['fields']['ext_sys_created_by']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_ext_sys_modified_by.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['ext_sys_modified_by']['audited'] = true;
$dictionary['gc_Contracts']['fields']['ext_sys_modified_by']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_global_contract_id.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['global_contract_id']['required'] = true;
$dictionary['gc_Contracts']['fields']['global_contract_id']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_global_contract_id_uuid.php

 // created: 2017-05-26 09:47:49
$dictionary['gc_Contracts']['fields']['global_contract_id_uuid']['massupdate']=false;
$dictionary['gc_Contracts']['fields']['global_contract_id_uuid']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);

 
?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_grand_total.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['grand_total']['default'] = '0.00';


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_lock_version.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['lock_version']['default'] = '1';
$dictionary['gc_Contracts']['fields']['lock_version']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_loi_contract.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['loi_contract']['required'] = true;
$dictionary['gc_Contracts']['fields']['loi_contract']['audited'] = true;
$dictionary['gc_Contracts']['fields']['loi_contract']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_name.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['name']['audited'] = true;
$dictionary['gc_Contracts']['fields']['name']['full_text_search']['boost'] = 3;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_product_offering.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['product_offering']['type'] = 'enum';
$dictionary['gc_Contracts']['fields']['product_offering']['options'] = 'product_offering_list';
$dictionary['gc_Contracts']['fields']['product_offering']['required'] = true;
$dictionary['gc_Contracts']['fields']['product_offering']['dependency'] = false;
$dictionary['gc_Contracts']['fields']['product_offering']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_total_mrc.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['total_mrc']['default'] = '0.00';


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/sugarfield_total_nrc.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['total_nrc']['default'] = '0.00';


?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Vardefs/vardefs.ext.php


// created: 2016-08-02 11:54 AM @auther: Amol.Sananse
$dictionary["gc_Contracts"]['optimistic_locking'] = false;

// Accounts Module : gc_Contracts
$dictionary['gc_Contracts']['fields']['ndb_quote_id'] = array (
	'name' => 'ndb_quote_id',
	'vname' => 'LBL_NDB_QUOTE_ID',
	'type' => 'varchar',
	'module' => 'gc_Contracts',
	'enforced' => '',
	'dependency' => '',
	'required' => false,
	'massupdate' => '0',
	'default_value' => '',
	'no_default' => false,
	'help' => 'Non-DB field for the PJ1',
	'comment' => 'Non-DB field for the PJ1',
	'importable' => false,
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'reportable' => false,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'calculated' => false,
	'len' => '255',
	'max_size' => '100',
	'source' => 'non-db',
	'dbType' => 'non-db',
	'studio' => 'visible',
);

// Accounts Module : gc_Contracts
$dictionary['gc_Contracts']['fields']['ndb_quote_name'] = array (
	'name' => 'ndb_quote_name',
	'vname' => 'LBL_NDB_QUOTE_NAME',
	'type' => 'varchar',
	'module' => 'gc_Contracts',
	'enforced' => '',
	'dependency' => '',
	'required' => false,
	'massupdate' => '0',
	'default_value' => '',
	'no_default' => false,
	'help' => 'Non-DB field for the PJ1',
	'comment' => 'Non-DB field for the PJ1',
	'importable' => false,
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'reportable' => false,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'calculated' => false,
	'len' => '255',
	'max_size' => '100',
	'source' => 'non-db',
	'dbType' => 'non-db',
	'studio' => 'visible',
);

// Accounts Module : gc_Contracts
$dictionary['gc_Contracts']['fields']['ndb_solution_id'] = array (
	'name' => 'ndb_solution_id',
	'vname' => 'LBL_NDB_SOLUTION_ID',
	'type' => 'varchar',
	'module' => 'gc_Contracts',
	'enforced' => '',
	'dependency' => '',
	'required' => false,
	'massupdate' => '0',
	'default_value' => '',
	'no_default' => false,
	'help' => 'Non-DB field for the PJ1',
	'comment' => 'Non-DB field for the PJ1',
	'importable' => false,
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'reportable' => false,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'calculated' => false,
	'len' => '255',
	'max_size' => '100',
	'source' => 'non-db',
	'dbType' => 'non-db',
	'studio' => 'visible',
);

// Accounts Module : gc_Contracts
$dictionary['gc_Contracts']['fields']['ndb_sales_representative_tpl'] = array (
	'name' => 'ndb_sales_representative_tpl',
	'vname' => 'LBL_NDB_SALES_REPRESENTATIVE_TPL',
	'type' => 'varchar',
	'module' => 'gc_Contracts',
	'enforced' => '',
	'dependency' => '',
	'required' => false,
	'massupdate' => '0',
	'default_value' => '',
	'no_default' => false,
	'help' => '',
	'comment' => '',
	'importable' => false,
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'reportable' => false,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'calculated' => false,
	'len' => '255',
	'max_size' => '100',
	'source' => 'non-db',
	'dbType' => 'non-db',
	'studio' => 'visible',
);

// Accounts Module : gc_Contracts
$dictionary['gc_Contracts']['fields']['ndb_line_item_details_tpl'] = array (
	'name' => 'ndb_line_item_details_tpl',
	'vname' => 'LBL_NDB_LINE_ITEM_DETAILS_TPL',
	'type' => 'varchar',
	'module' => 'gc_Contracts',
	'enforced' => '',
	'dependency' => '',
	'required' => false,
	'massupdate' => '0',
	'default_value' => '',
	'no_default' => false,
	'help' => '',
	'comment' => '',
	'importable' => false,
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'reportable' => false,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'calculated' => false,
	'len' => '255',
	'max_size' => '100',
	'source' => 'non-db',
	'dbType' => 'non-db',
	'studio' => 'visible',
);

// Accounts Module : gc_Contracts
$dictionary['gc_Contracts']['fields']['ndb_contract_document_details_tpl'] = array (
	'name' => 'ndb_contract_document_details_tpl',
	'vname' => 'LBL_NDB_CONTRACT_DOCUMENT_DETAILS_TPL',
	'type' => 'varchar',
	'module' => 'gc_Contracts',
	'enforced' => '',
	'dependency' => '',
	'required' => false,
	'massupdate' => '0',
	'default_value' => '',
	'no_default' => false,
	'help' => '',
	'comment' => '',
	'importable' => false,
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'reportable' => false,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'calculated' => false,
	'len' => '255',
	'max_size' => '100',
	'source' => 'non-db',
	'dbType' => 'non-db',
	'studio' => 'visible',
);


?>
