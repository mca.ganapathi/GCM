<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Layoutdefs/contract-revision-history.php
 
// Call the function to get contract revision history
	$layout_defs['gc_Contracts']['subpanel_setup']['contract_version_history'] =
        array('order' => 99,
            'module' => 'gc_Contracts',
            'subpanel_name' => 'default',
			'sort_order' => 'desc', 
			'sort_by' => 'contract_version', 
            'get_subpanel_data' => 'function:get_all_versions_of_contract',
            'generate_select' => true,
            'title_key' => 'LBL_CONTRACT_REVISION_HISTORY',
            'top_buttons' => array(),
            'function_parameters' => array(
                'import_function_file' => 'custom/modules/gc_Contracts/customContractRevisionHistorySubpanel.php',
                'global_contract_id' => $this->_focus->global_contract_id,
                'return_as_array' => 'true'
            ),
);

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Layoutdefs/gc_contracts_documents_1_gc_Contracts.php

 // created: 2016-01-07 11:52:07
$layout_defs["gc_Contracts"]["subpanel_setup"]['gc_contracts_documents_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'gc_contracts_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Layoutdefs/gc_contracts_gc_invoicesubtotalgroup_1_gc_Contracts.php

 // created: 2016-05-04 05:33:01
$layout_defs["gc_Contracts"]["subpanel_setup"]['gc_contracts_gc_invoicesubtotalgroup_1'] = array (
  'order' => 100,
  'module' => 'gc_InvoiceSubtotalGroup',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_GC_CONTRACTS_GC_INVOICESUBTOTALGROUP_1_FROM_GC_INVOICESUBTOTALGROUP_TITLE',
  'get_subpanel_data' => 'gc_contracts_gc_invoicesubtotalgroup_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Layoutdefs/removeSubpanel.php

/*** START : Remove unnecessary subpanels that we can't hide from Admin Panel ***/

unset($layout_defs["gc_Contracts"]["subpanel_setup"]['gc_contracts_documents_1']);
unset($layout_defs["gc_Contracts"]["subpanel_setup"]['gc_contracts_gc_invoicesubtotalgroup_1']);
//unset($layout_defs["gc_Contracts"]["subpanel_setup"]['gc_contracts_documents_1']['top_buttons'][1]);
?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Layoutdefs/_overridegc_Contracts_subpanel_contract_version_history.php

//auto-generated file DO NOT EDIT
$layout_defs['gc_Contracts']['subpanel_setup']['contract_version_history']['override_subpanel_name'] = 'gc_Contracts_subpanel_contract_version_history';

?>
<?php
// Merged from custom/Extension/modules/gc_Contracts/Ext/Layoutdefs/_overridegc_Contracts_subpanel_gc_contracts_documents_1.php

//auto-generated file DO NOT EDIT
$layout_defs['gc_Contracts']['subpanel_setup']['gc_contracts_documents_1']['override_subpanel_name'] = 'gc_Contracts_subpanel_gc_contracts_documents_1';

?>
