<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

/**
 * Code to display Contract Comments Log
 *
 * @author : Dinesh.Itkar
 * @since : 19-Feb-2016
 */
ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $_REQUEST['contract_id'], 'Code to display Contract Comments Log');
$obj_contracts_comments_bean = BeanFactory::getBean('gc_comments');
$obj_contracts_comments_lists = $obj_contracts_comments_bean->get_full_list('date_entered desc', "gc_contracts_id_c = '" . $_REQUEST['contract_id'] . "'");

$html_str = '
<style>
.CSSTableGenerator {
    width:100%;
    border:1px solid #000000;
}.CSSTableGenerator table{
    border-collapse: collapse;
    width:100%;
}
.CSSTableGenerator td{
    vertical-align:top;
    border:1px solid #000000;
    border-width:0px 1px 1px 0px;
    text-align:left;
    padding:7px;
    font-size:12px;
    font-family:Arial;
    font-weight:normal;
    color:#000000;
}
.CSSTableGenerator tr:last-child td{
    border-width:0px 1px 0px 0px;
}.CSSTableGenerator tr td:last-child{
    border-width:0px 0px 1px 0px;
}.CSSTableGenerator tr:last-child td:last-child{
    border-width:0px 0px 0px 0px;
}
.CSSTableGenerator tr:first-child td{
    background-color:#2e587e;
    border:0px solid #000000;
    text-align:center;
    border-width:0px 0px 1px 1px;
    font-size:14px;
    font-family:Arial;
    color:#ffffff;
}
.CSSTableGenerator tr:first-child:hover td{
    background-color:#2e587e;
}
.CSSTableGenerator tr:first-child td:first-child{
    border-width:0px 0px 1px 0px;
}
.CSSTableGenerator tr:first-child td:last-child{
    border-width:0px 0px 1px 1px;
}

</style>
<div class="CSSTableGenerator" >
                <table >
                    <tr>
                        <td> Workflow Action </td>
                        <td> User </td>
                        <td> Team </td>
                        <td> Date time </td>
                        <td> Comment </td>
                    </tr>';

require_once ('modules/Teams/Team.php');
$team_bean = new Team();
if ($obj_contracts_comments_lists != null) {

    foreach ($obj_contracts_comments_lists as $obj_contracts_comments_list) {
        $user = new User();
        if(isset($obj_contracts_comments_list->assigned_user_id) && !empty($obj_contracts_comments_list->assigned_user_id)){
            
            $user->retrieve($obj_contracts_comments_list->assigned_user_id);
        }
        else{
            $user->name = '';
        }

        $team_name = '';
        if(isset($obj_contracts_comments_list->created_by) && !empty($obj_contracts_comments_list->created_by)){
            $teams = $team_bean->get_teams_for_user($obj_contracts_comments_list->created_by);
            $team_set_array = array();
            if($teams!=null && !empty($teams)){
                foreach ($teams as $k => $v) {
                    if ($v->private == 0 && !in_array($v->id, array(
                                                                    1
                    )))
                        $team_set_array[] = $v->name . ' ' . $v->name_2;
                }    
            }
            $team_name = implode(', ', $team_set_array);    
        }
        $comment_list_name = isset($obj_contracts_comments_list->name)?$obj_contracts_comments_list->name:'';
        $user_name = isset($user->name)?$user->name:'';
        $date_entered = isset($obj_contracts_comments_list->date_entered)?$obj_contracts_comments_list->date_entered:'';
        $desc = isset($obj_contracts_comments_list->description)?$obj_contracts_comments_list->description:'';

        $html_str .= '<tr>
                <td style="width: 20%;">' . $comment_list_name . '</td>
                <td style="width: 15%;">' . $user_name . '</td>
                <td style="width: 20%;">' . $team_name . '</td>
                <td style="width: 15%;">' . $date_entered . '</td>
                <td>' . $desc . '</td>
                </tr>';
    }
} else {
    $html_str .= '<tr> <td colspan="5" style="text-align: center;"> No Record Found </td> </tr>';
}

$html_str .= '</table> </div>';
ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'html_str : ' . $html_str, 'Code to display Contract Comments Log');
echo $html_str;
?>