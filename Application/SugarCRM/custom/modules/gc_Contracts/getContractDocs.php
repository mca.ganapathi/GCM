<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/**
* File to get or delete the document details of a contract
*/

const GETCNDOCS = 'getCnDocs';
const DELCNDOCS = 'delCnDocs';

/*require this file for contract document audit*/
require_once ('custom/modules/Documents/include/ClassAfterRelationshipSave.php');

$command = trim($_POST['command']);

if($command == GETCNDOCS) {
	$CnId = trim($_POST['CnId']); 
	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'command : ' . $command . GCM_GL_LOG_VAR_SEPARATOR . 'CnId : ' . $CnId, 'File to get the document details of a contract');
	if(!empty($CnId)) {
		$contract = new gc_Contracts();
        $contract->retrieve($CnId); 
        $contract_doc_bean = $contract->get_linked_beans( 'gc_contracts_documents_1', 'Documents');
        // echo "<pre>";print_r($contract_doc_bean);echo "</pre>";exit;
        $document_template_types = $GLOBALS['app_list_strings']['document_template_type_dom'];
        $document_categorys = $GLOBALS['app_list_strings']['document_category_dom'];
        $contract_doc_arr = array();
        if ($contract_doc_bean != null) {
            foreach($contract_doc_bean as $line_item_doc){
                $contract_doc_arr[$line_item_doc->id]['contract_id'] = $CnId;
                $contract_doc_arr[$line_item_doc->id]['doc_id'] = $line_item_doc->id;
                $contract_doc_arr[$line_item_doc->id]['doc_name'] = $line_item_doc->document_name;
                $contract_doc_arr[$line_item_doc->id]['filename'] = $line_item_doc->filename;
                $contract_doc_arr[$line_item_doc->id]['revision'] = $line_item_doc->revision;
                $template = $line_item_doc->template_type;
                $cat = $line_item_doc->category_id;
                $contract_doc_arr[$line_item_doc->id]['template_type'] = isset($document_template_types[$template]) ? $document_template_types[$template] : '';
                $contract_doc_arr[$line_item_doc->id]['category_id'] = isset($document_categorys[$cat]) ? $document_categorys[$cat] :'';
            }
        }
		$respCode = 1; $respMsg = (count($contract_doc_arr) > 0) ? $contract_doc_arr : '';
	} else {
		$respCode = 1; $respMsg = '';
	}

} else if($command == DELCNDOCS) {
	$arrDocId = trim($_POST['record']);
	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'command : ' . $command . GCM_GL_LOG_VAR_SEPARATOR . 'arrDocId : ' . $arrDocId, 'File to delete the document details of a contract');
	if(!empty($arrDocId)) {
		$docBean = new Document();
        $docBean->retrieve($arrDocId); 
		$docBean->deleted = 1;
        $docBean->Save();
		$respCode = 1; $respMsg = '';

		// audit Contract Document
		$GLOBALS['log']->debug("Start: Contract document deleted audit");
		$contract_id = trim($_POST['conId']);
		$document_name	=	$docBean->document_name;
		$contract_bean = new gc_Contracts();
		$contract_bean->retrieve($contract_id);			
		$docAfterSaveObj 	=	new ClassAfterRelationshipSave();
		$docAfterSaveObj->insertContractAudit($contract_bean, 'Contract Document', 'relate', $document_name, '');        	
		$GLOBALS['log']->debug("End: Contract document deleted audit");			
	} else {
		$respCode = 2; $respMsg = '';
	}
}
$response = array('code' => $respCode, 'msg' => $respMsg);
ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response : ' . print_r($response, true), 'File to get or delete the document details of a contract');
echo json_encode($response);
