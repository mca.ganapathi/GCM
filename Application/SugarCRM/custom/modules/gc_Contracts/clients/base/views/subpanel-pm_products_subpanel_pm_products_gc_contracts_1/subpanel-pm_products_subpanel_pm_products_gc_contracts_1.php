<?php
// created: 2016-09-29 04:41:43
$viewdefs['gc_Contracts']['base']['view']['subpanel-pm_products_subpanel_pm_products_gc_contracts_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'type' => 'varchar',
          'default' => true,
          'label' => 'LBL_GLOBAL_CONTRACT_ID',
          'enabled' => true,
          'name' => 'global_contract_id',
        ),
        1 => 
        array (
          'default' => true,
          'label' => 'LBL_NAME',
          'enabled' => true,
          'name' => 'name',
          'link' => true,
          'type' => 'name',
        ),
        2 => 
        array (
          'type' => 'decimal',
          'default' => true,
          'label' => 'LBL_CONTRACT_VERSION',
          'enabled' => true,
          'name' => 'contract_version',
        ),
        3 => 
        array (
          'type' => 'enum',
          'default' => true,
          'label' => 'LBL_CONTRACT_STATUS',
          'enabled' => true,
          'name' => 'contract_status',
        ),
        4 => 
        array (
          'type' => 'datetime',
          'default' => true,
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'name' => 'date_entered',
        ),
        5 => 
        array (
          'type' => 'varchar',
          'default' => true,
          'label' => 'LBL_EXT_SYS_CREATED_BY',
          'enabled' => true,
          'name' => 'ext_sys_created_by',
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);