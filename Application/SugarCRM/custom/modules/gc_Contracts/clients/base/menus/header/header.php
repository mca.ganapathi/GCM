<?php
/* Created by SugarUpgrader for module gc_Contracts */
$viewdefs['gc_Contracts']['base']['menu']['header'] =array (
  0 => 
  array (
    'route' => '#gc_Contracts/create',
    'label' => 'LNK_NEW_RECORD',
    'acl_action' => 'create',
    'acl_module' => 'gc_Contracts',
    'icon' => 'fa-plus',
  ),
  1 => 
  array (
    'route' => '#gc_Contracts',
    'label' => 'LNK_LIST',
    'acl_action' => 'list',
    'acl_module' => 'gc_Contracts',
    'icon' => 'fa-bars',
  ),
);
