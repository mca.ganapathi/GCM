<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
include_once ('data/BeanFactory.php');
include_once ('modules/ACLRoles/ACLRole.php');
/**
 * To fetch the contract and its related module details from API or GUI
 */
class CustomFunctionGcContract
{

    const STATUS_SERVICE_TERMINATION = 'Service_Termination';
    const STATUS_TERMINATION = 'Termination';
    const STATUS_ACTIVATED = 'Activated';

    //const SETTING_KEY_ACE_CONTRACTS = 'AccessControlExt_GMOne';

    /**
     * Method to get Sales Representative Details
     *
     * @param string $ids contract id for which the sales representative details needed
     * @return array list of sales representative details for the given contract id
     */
    public function getSalesRepresentativeList($ids)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'ids : ' . $ids, 'Method to get Sales Representative Details');
    	$list = array();

        $sales_rep_bean = BeanFactory::getBean('gc_SalesRepresentative');
        $sales_rep_bean_arr = $sales_rep_bean->get_full_list('', "contracts_id = '$ids'");

        if ($sales_rep_bean_arr != null) {
            foreach ($sales_rep_bean_arr as $sales_rep) {
                $list[$sales_rep->id]['id'] = $sales_rep->id;
                $list[$sales_rep->id]['name'] = $sales_rep->name;
                $list[$sales_rep->id]['sales_channel_code'] = $sales_rep->sales_channel_code;
                $list[$sales_rep->id]['sales_rep_name_1'] = $sales_rep->sales_rep_name_1;
                $list[$sales_rep->id]['division_1'] = $sales_rep->division_1;
                $list[$sales_rep->id]['division_2'] = $sales_rep->division_2;
                $list[$sales_rep->id]['title_1'] = $sales_rep->title_1;
                $list[$sales_rep->id]['title_2'] = $sales_rep->title_2;
                $list[$sales_rep->id]['tel_no'] = $sales_rep->tel_no;
                $list[$sales_rep->id]['ext_no'] = $sales_rep->ext_no;
                $list[$sales_rep->id]['fax_no'] = $sales_rep->fax_no;
                $list[$sales_rep->id]['mob_no'] = $sales_rep->mob_no;
                $list[$sales_rep->id]['email1'] = $sales_rep->email1;
                $list[$sales_rep->id]['corporate_id'] = $sales_rep->corporate_id;
                $list[$sales_rep->id]['sales_company_id'] = $sales_rep->gc_nttcomgroupcompany_id_c;

                $sales_company = $this->getSalesCompany($sales_rep->gc_nttcomgroupcompany_id_c);
                $sales_company = is_array($sales_company)?$sales_company:array();
                $list[$sales_rep->id]['sales_company_name'] = (isset($sales_company[$sales_rep->gc_nttcomgroupcompany_id_c]) && ($sales_company[$sales_rep->gc_nttcomgroupcompany_id_c]))  ? $sales_company[$sales_rep->gc_nttcomgroupcompany_id_c] : "";

                $list[$sales_rep->id]['contracts_id'] = $sales_rep->contracts_id;
            }
        }

        unset($sales_rep_bean, $sales_rep_bean_arr);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'list : ' . print_r($list, true), 'Method to get Sales Representative Details');

        return $list;
    }

    /**
     * Method to to get Line Item Details
     *
     * @param string $ids contract id for which the line item details needed
     * @param boolean $show_deleted if 1 then display the deleted record also
     * @param string $product_offering product offering id
     * @param boolean $show_terminate if true then fetch the terminated contract also
     * @return array list of line item header details for the given contract id
     */
    public function getLineItemHeader($ids, $show_deleted, $product_offering, $show_terminate)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'ids : ' . $ids . GCM_GL_LOG_VAR_SEPARATOR . 'show_deleted : ' . $show_deleted . GCM_GL_LOG_VAR_SEPARATOR . 'product_offering : ' . print_r($product_offering, true) . GCM_GL_LOG_VAR_SEPARATOR . 'show_terminate : ' . $show_terminate, 'Method to to get Line Item Details');
    	$list = array();
        require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';
        include_once ('custom/modules/gc_Contracts/include/ClassGcContracts.php');
        $obj_gc_contracts = new ClassGcContracts();
        $config_currencies = $obj_gc_contracts->getconfig_currencies();
        $config_currencies = is_array($config_currencies)?$config_currencies:array();
        $product_config = new ClassProductConfiguration();
        $line_item_his_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
        $product_config_bean = BeanFactory::getBean('gc_ProductConfiguration');
        $product_price_bean = BeanFactory::getBean('gc_ProductPrice');
        /*
         * Fixed to avaoid the uncessary call to PMS system and currl error
         * Further code review should be done on this - Arunsakthivel
         */
        if (! empty($product_offering)) {
            $off_details = $product_config->getProductOfferingDetails($product_offering);
            $sp_details = isset($off_details['respose_array']['specification_details'])?$off_details['respose_array']['specification_details']:array();
            $pcharge_details = isset($off_details['respose_array']['charge_details'])?$off_details['respose_array']['charge_details']:array();
        } else {
            $off_details = $sp_details = $pcharge_details = array();
        }
        unset($off_details);
        unset($product_config);

        $li_qry = "contracts_id = '$ids' ";
        if (! $show_terminate) $li_qry .= " and contract_line_item_type !='".self::STATUS_SERVICE_TERMINATION."'";

        if ($show_deleted)
            $line_item_his_bean_arr = $line_item_his_bean->get_full_list('', $li_qry, false, 1);
        else $line_item_his_bean_arr = $line_item_his_bean->get_full_list('', $li_qry);

        if ($line_item_his_bean_arr != null) {
            foreach ($line_item_his_bean_arr as $line_item_header) {
                $line_item_detail = $this->getLineItem($line_item_header, $product_config_bean, $product_price_bean, $config_currencies, $sp_details, $pcharge_details, $product_offering);
                $line_item_detail['global_contract_item_id'] = isset($line_item_detail['global_contract_item_id'])?$line_item_detail['global_contract_item_id']:'';
                $list['order'][$line_item_detail['global_contract_item_id']] = isset($line_item_header->id)?$line_item_header->id:'';
                $list[$line_item_header->id] = $line_item_detail;
            }
        }
        unset($line_item_his_bean, $line_item_his_bean_arr);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'list : ' . print_r($list, true), 'Method to to get Line Item Details');
        return $list;
    }

    /**
     * Method to get Accounts record by ID
     *
     * @param string $account_id account id for which the account name is needed
     * @param boolean $ret_array
     * @return string account name
     */
    private function getAccountNameByID($account_id, $ret_array)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'account_id : ' . $account_id . GCM_GL_LOG_VAR_SEPARATOR . 'ret_array : ' . print_r($ret_array, true), 'Method to get Accounts record by ID');

        $account_bean = BeanFactory::getBean('Contacts');
        $account_name_bean = $account_bean->retrieve($account_id);
        $c_country_id = '';
        $account_name='';
        if($account_name_bean!=null){
            // If Latin name is not present, display local name instead for Account - Sagar : 26-10-2015
            $account_name = (isset($account_name_bean->name) && $account_name_bean->name!='')?trim($account_name_bean->name):'';

            if (strlen(trim($account_name)) == 0){
              $account_name = isset($account_name_bean->name_2_c) ? trim($account_name_bean->name_2_c) : '';
            }
            $c_country_id = isset($account_name_bean->c_country_id_c) ? $account_name_bean->c_country_id_c : '';
        }

        // if account name is empty and account id is not empty, set text as "Click Here" - Sagar.Salunkhe 11-02-2015 | Jira Ticket # OM-15
        if (strlen(trim($account_name)) == 0 && !empty($account_id)) $account_name = 'Click Here';

        $result = array(
            'account_name' => $account_name,
            'country_id' => $c_country_id
        );

        unset($account_bean, $account_name_bean);

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'result : ' . print_r($result, true), 'Method to get Accounts record by ID');
        if ($ret_array) {
            return $result;
        } else {
            return $account_name;
        }

    }

    /**
     * Method to get Organization record by ID
     *
     * @param string $orgization_id orgization id for which the orgization name is needed
     * @return string orgization name
     */
    private function getOrganizationNameByID($orgization_id)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'orgization_id : ' . $orgization_id, 'Method to get Organization record by ID');

        $organization_bean = BeanFactory::getBean('gc_organization');
        $organization_bean = $organization_bean->retrieve($orgization_id);
        $organization_name = isset($organization_bean->name)?$organization_bean->name:'';

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'organization_name : ' . $organization_name, 'Method to get Organization record by ID');
        unset($organization_bean);
        return $organization_name;
    }

    /**
     * Method to retrieve the documents relate to contract
     *
     * @param string $contract_id contract id for which the document details are needed
     * @return array document details
     * @author :Arunsakthivel.S
     * @since Jun 2016
     */
    public function getContractDocuments($contract_id)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $contract_id, 'Method to retrieve the documents relate to contract');
    	$contract = new gc_Contracts();
        $contract->retrieve($contract_id);
        $contract_doc_bean = $contract->get_linked_beans('gc_contracts_documents_1', 'Documents');
        $document_template_type_dom = $GLOBALS['app_list_strings']['document_template_type_dom'];
        $document_category_dom = $GLOBALS['app_list_strings']['document_category_dom'];
        $contract_doc_arr = array();
        if ($contract_doc_bean != null) {
            foreach ($contract_doc_bean as $line_item_doc) {
                if(!isset($line_item_doc->id) || $line_item_doc->id == null){
                    continue;
                }
                $contract_doc_arr[$line_item_doc->id]['contract_id'] = $contract_id;
                $contract_doc_arr[$line_item_doc->id]['doc_id'] = isset($line_item_doc->id)?$line_item_doc->id:'';
                $contract_doc_arr[$line_item_doc->id]['doc_name'] = isset($line_item_doc->document_name)?$line_item_doc->document_name:'';
                $contract_doc_arr[$line_item_doc->id]['filename'] = isset($line_item_doc->filename)?$line_item_doc->filename:'';
                $contract_doc_arr[$line_item_doc->id]['revision'] = isset($line_item_doc->revision)?$line_item_doc->revision:'';
                $contract_doc_arr[$line_item_doc->id]['template_type'] = isset($document_template_type_dom[$line_item_doc->template_type])?$document_template_type_dom[$line_item_doc->template_type]:'';
                $contract_doc_arr[$line_item_doc->id]['category_id'] = isset($document_category_dom[$line_item_doc->category_id])?$document_category_dom[$line_item_doc->category_id]:'';
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_doc_arr : ' . print_r($contract_doc_arr, true), 'Method to retrieve the documents relate to contract');
        return $contract_doc_arr;
    }

    /**
     * Method to get role permission in order to display contract workflow buttons
     * @return array permission details
     * @author Shrikant.Gaware
     * @since Feb 22, 2016
     */
    public function getRolePermissions($workflow_settings_key)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to get role permission in order to display contract workflow buttons');
    	$data = array();
        // [DF-672] Modify json reading process from file to database.
        // $json_url = 'custom/modules/Administration/include/AccessControlExt/gc_Contracts.json';
        // if (file_exists($json_url)) {
        // $json = file_get_contents($json_url);
        // $data = json_decode($json, TRUE);
        // }
        $aryJson = json_decode(AppSettings::load($workflow_settings_key), TRUE);
        if (! is_null($aryJson)) $data = $aryJson;

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'data : ' . print_r($data, true), 'Method to get role permission in order to display contract workflow buttons');
        return $data;
    }

    /**
     * Method to get sales company.
     * @param string $sales_company_id sales company id
     * @return array list of NTT Group Company details
     * @author Kamal.Thiyagarajan
     * @since May 05, 2016
     */
    public function getSalesCompany($sales_company_id)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'sales_company_id : ' . $sales_company_id, 'Method to get sales company.');
    	$list = array();

        $sales_company_bean = BeanFactory::getBean('gc_NTTComGroupCompany');

        if (! empty($sales_company_id))
            $sales_company_bean_arr = $sales_company_bean->get_full_list('', "gc_nttcomgroupcompany.id = '$sales_company_id'");
        else $sales_company_bean_arr = $sales_company_bean->get_full_list('');

        if ($sales_company_bean_arr != null) {
            foreach ($sales_company_bean_arr as $sales_company) {
                $list[$sales_company->id] = $sales_company->name;
            }
        }

        unset($sales_company_bean, $sales_company_bean_arr);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'list : ' . print_r($list, true), 'Method to get sales company.');
        return $list;
    }

    /**
     * Method to get invoice group related to lineitem.
     * @param string $line_item_id line item id for which invoice group details are needed
     * @return array invoice group details array
     * @author Kamal.Thiyagarajan
     * @since May 04, 2016
     */
    public function getInvoiceGroupByLineItem($line_item_id)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_item_id : ' . $line_item_id, 'Method to get invoice group related to lineitem.');
    	if (empty($line_item_id)) return array();
        $list = array();
        $line_item = BeanFactory::getBean('gc_Line_Item_Contract_History');
        $line_item->retrieve($line_item_id);
        $line_item_grp_bean = $line_item->get_linked_beans('gc_invoicesubtotalgroup_gc_line_item_contract_history_1', 'gc_InvoiceSubtotalGroup');
        if ($line_item_grp_bean != null) {
            foreach ($line_item_grp_bean as $group) {
                if(!isset($group->id) || $group->id == null){
                    continue;
                }
                $list['id'] = $group->id;
                $list['group_name'] = isset($group->name)?$group->name:'';
                $list['group_name_uuid'] = isset($group->name_uuid)?$group->name_uuid:'';
                $list['description'] = isset($group->description)?:'';
                $list['description_uuid'] = isset($group->description_uuid)?$group->description_uuid:'';
            }
        }
        unset($line_item_grp_bean, $line_item);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'list : ' . print_r($list, true), 'Method to get invoice group related to lineitem.');
        return $list;
    }

    /**
     * Method to retrieve the Invoice Groups By Contract id
     * @param string $contract_id contract id of which the invoice group is needed
     * @return array invoice group details array
     * @author Arunsakthivel.S
     * @since May 2016
     */
    public function getInvoiceGroupByContract($contract_id)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $contract_id, 'Method to retrieve the Invoice Groups By Contract id');
    	$contract = new gc_Contracts();
        $contract->retrieve($contract_id);
        $contract_ig_bean = $contract->get_linked_beans('gc_contracts_gc_invoicesubtotalgroup_1', '`gc_InvoiceSubtotalGroup');

        $contract_ig_arr = array();
        if ($contract_ig_bean != null) {
            foreach ($contract_ig_bean as $contract_ig) {
                if(!isset($contract_ig->id) || $contract_ig->id == null){
                    continue;
                }
                $contract_ig_arr[$contract_ig->id]['ig_id'] = $contract_ig->id;
                $contract_ig_arr[$contract_ig->id]['ig_name'] = isset($contract_ig->name)?$contract_ig->name:'';
                $contract_ig_arr[$contract_ig->id]['ig_desc'] = isset($contract_ig->description)?$contract_ig->description:'';
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_ig_arr : ' . print_r($contract_ig_arr, true), 'Method to retrieve the Invoice Groups By Contract id');
        return $contract_ig_arr;
    }

    /**
     * Method to Get Current User Role IDs
     * @return $role_ids array will contain current user role ids
     * @author Kamal.Thiyagarajan
     * @since Jun 10, 2016
     */
    public function getUserRoleIds()
    {
    	$role_object = new ACLRole();
        $roles = $role_object->getUserRoles($GLOBALS['current_user']->id, false);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'roles : ' . print_r($roles,true), 'Method to Get Current User Role IDs');
        $role_ids = array();
        if(!empty($roles)){
            foreach ($roles as $role) {
                $role_ids[] = $role->id;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'role_ids : ' . print_r($role_ids, true), 'Method to Get Current User Role IDs');
        return $role_ids;
    }

    /**
     * Method to set contract line item status
     *
     * @param $contract_id
     * @param $status
     * @return void
     * @author shamsusjaman.mondal
     * @since July 08, 2016
     */
    public function setLineItemStatus($contract_id, $status)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $contract_id . GCM_GL_LOG_VAR_SEPARATOR . 'status : ' . $status, 'Method to set contract line item status');
        if ($status === '' || empty($contract_id)) return false;

        // Fetching the Line_Item_Contract_History details
        $obj_line_contract_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
        $line_items = $obj_line_contract_history_bean->get_full_list('', "contracts_id = '" . $contract_id . "' AND contract_line_item_type !='".self::STATUS_SERVICE_TERMINATION."'");

        $li_action_val = '';
        if (! empty($line_items)) {
            switch ($status) {
                case self::STATUS_TERMINATION:
                    $li_action_val = 'remove';
                    break;
                case self::STATUS_ACTIVATED:
                    $li_action_val = 'no_change';
                    break;
            }
            if (! empty($li_action_val)) {
                foreach ($line_items as $rowitem) {
                    $rowitem->contract_line_item_type = $status;
                    $rowitem->bi_action = $li_action_val;
                    $rowitem->save();
                }
            }
        }

    }

    /**
     * Method to get line item details
     * @param object $line_item_header line item history object
     * @param object $product_config_bean product configuration bean object
     * @param object $product_price_bean Product price bean object
     * @param array $config_currencies currency array
     * @param array $sp_details product specification details array
     * @param array $pcharge_details product sppecification price detail array
     * @param int $product_offering prodcut offering id
     * @return array $list line item details
     * @author Kamal.Thiyagarajan
     * @since Aug 2016
     */
    public function getLineItem($line_item_header, $product_config_bean, $product_price_bean, $config_currencies, $sp_details, $pcharge_details, $product_offering)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_item_header : ' . print_r($line_item_header, true) . GCM_GL_LOG_VAR_SEPARATOR . ' config_currencies : ' . print_r($config_currencies, true) . GCM_GL_LOG_VAR_SEPARATOR . 'sp_details : ' . print_r($sp_details, true) . GCM_GL_LOG_VAR_SEPARATOR . 'pcharge_details : ' . print_r($pcharge_details, true) . GCM_GL_LOG_VAR_SEPARATOR . 'product_offering: ' . print_r($product_offering, true), 'Method to get line item details');
        $list = array();
        $modUtils = new ModUtils();
        $list['service_start_date'] = isset($line_item_header->service_start_date)?$line_item_header->service_start_date:'';
        $list['service_end_date'] = isset($line_item_header->service_end_date)?$line_item_header->service_end_date:'';
        $list['billing_start_date'] = isset($line_item_header->billing_start_date)?$line_item_header->billing_start_date:'';
        $list['billing_end_date'] = isset($line_item_header->billing_end_date)?$line_item_header->billing_end_date:'';

        $list['service_start_date_uuid'] = isset($line_item_header->service_start_date_uuid)?$line_item_header->service_start_date_uuid:'';
        $list['service_end_date_uuid'] = isset($line_item_header->service_end_date_uuid)?$line_item_header->service_end_date_uuid:'';
        $list['billing_start_date_uuid'] = isset($line_item_header->billing_start_date_uuid)?$line_item_header->billing_start_date_uuid:'';
        $list['billing_end_date_uuid'] = isset($line_item_header->billing_end_date_uuid)?$line_item_header->billing_end_date_uuid:'';

        $list['service_start_date_timezone_id'] = isset($line_item_header->gc_timezone_id_c)?$line_item_header->gc_timezone_id_c:'';
        $list['service_start_date_timezone_actual'] = isset($line_item_header->service_start_date_timezone)?$line_item_header->service_start_date_timezone:'';
        if (! empty($line_item_header->gc_timezone_id_c)) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone', $line_item_header->gc_timezone_id_c);
            $st_tz = isset($line_item_header->service_start_date_timezone)? $line_item_header->service_start_date_timezone:'';
            $st_tz_offset = isset($obj_timezone->utcoffset)?$obj_timezone->utcoffset:'';
            $list['service_start_date_timezone'] = $st_tz . ' (' . $st_tz_offset . ')';
            $list['service_start_date_timezone_actual'] = $st_tz_offset;
        } else
            $list['service_start_date_timezone'] = isset($line_item_header->service_start_date_timezone)?$line_item_header->service_start_date_timezone:'';
        $list['service_end_date_timezone_id'] = isset($line_item_header->gc_timezone_id1_c)?$line_item_header->gc_timezone_id1_c:'';
        $list['service_end_date_timezone_actual'] = isset($line_item_header->service_end_date_timezone)?$line_item_header->service_end_date_timezone:'';
        if (! empty($line_item_header->gc_timezone_id1_c)) {
            $obj_timezone1 = BeanFactory::getBean('gc_TimeZone', $line_item_header->gc_timezone_id1_c);
            $ed_tz = isset($line_item_header->service_end_date_timezone)?$line_item_header->service_end_date_timezone:'';
            $ed_tz_offset = isset($obj_timezone1->utcoffset)?$obj_timezone1->utcoffset:'';
            $list['service_end_date_timezone'] = $ed_tz . ' (' . $ed_tz_offset . ')';
            $list['service_end_date_timezone_actual'] = $ed_tz_offset;
        } else
            $list['service_end_date_timezone'] = isset($line_item_header->service_end_date_timezone)?$line_item_header->service_end_date_timezone:'';
        $list['billing_start_date_timezone_id'] = isset($line_item_header->gc_timezone_id2_c)?$line_item_header->gc_timezone_id2_c:'';
        $list['billing_start_date_timezone_actual'] = isset($line_item_header->billing_start_date_timezone)?$line_item_header->billing_start_date_timezone:'';
        if (! empty($line_item_header->gc_timezone_id2_c)) {
            $obj_timezone2 = BeanFactory::getBean('gc_TimeZone', $line_item_header->gc_timezone_id2_c);
            $bill_st_tz = isset($line_item_header->billing_start_date_timezone)? $line_item_header->billing_start_date_timezone:'';
            $bill_st_tz_offset = isset($obj_timezone2->utcoffset)?$obj_timezone2->utcoffset:'';
            $list['billing_start_date_timezone'] = $bill_st_tz . ' (' . $bill_st_tz_offset . ')';
            $list['billing_start_date_timezone_actual'] = $bill_st_tz_offset;
        } else
            $list['billing_start_date_timezone'] = isset($line_item_header->billing_start_date_timezone)?$line_item_header->billing_start_date_timezone:'';
        $list['billing_end_date_timezone_id'] = isset($line_item_header->gc_timezone_id3_c)?$line_item_header->gc_timezone_id3_c:'';
        $list['billing_end_date_timezone_actual'] = isset($line_item_header->billing_end_date_timezone)?$line_item_header->billing_end_date_timezone:'';
        if (! empty($line_item_header->gc_timezone_id3_c)) {
            $obj_timezone3 = BeanFactory::getBean('gc_TimeZone', $line_item_header->gc_timezone_id3_c);
            $bill_ed_tz = isset($line_item_header->billing_end_date_timezone)? $line_item_header->billing_end_date_timezone:'';
            $bill_ed_tz_offset = isset($obj_timezone3->utcoffset)?$obj_timezone3->utcoffset:'';
            $list['billing_end_date_timezone'] = $bill_ed_tz . ' (' . $bill_ed_tz_offset . ')';
            $list['billing_end_date_timezone_actual'] = $bill_ed_tz_offset;
        } else
            $list['billing_end_date_timezone'] = isset($line_item_header->billing_end_date_timezone)?$line_item_header->billing_end_date_timezone:'';
        $list['factory_reference_no'] = isset($line_item_header->factory_reference_no)?$line_item_header->factory_reference_no:'';
        $list['factory_reference_no_uuid'] = isset($line_item_header->factory_reference_no_uuid)?$line_item_header->factory_reference_no_uuid:'';
        /* Start : DF-38 */
        $list['product_specification'] = isset($line_item_header->product_specification)?$line_item_header->product_specification:'';
        $list['product_specification_uuid'] = isset($line_item_header->product_specification_uuid)?$line_item_header->product_specification_uuid:'';
        $list['cust_contract_currency'] = isset($config_currencies[$line_item_header->cust_contract_currency])?$config_currencies[$line_item_header->cust_contract_currency]:'';
        $list['cust_contract_currency_code'] = isset($line_item_header->cust_contract_currency)?$line_item_header->cust_contract_currency:'';
        $list['cust_contract_currency_uuid'] = isset($line_item_header->cust_contract_currency_uuid)?$line_item_header->cust_contract_currency_uuid:'';
        $list['cust_billing_currency'] = isset($config_currencies[$line_item_header->cust_billing_currency])?$config_currencies[$line_item_header->cust_billing_currency]:'';
        $list['cust_billing_currency_code'] = isset($line_item_header->cust_billing_currency)?$line_item_header->cust_billing_currency:'';
        $list['cust_billing_currency_uuid'] = isset($line_item_header->cust_billing_currency_uuid)?$line_item_header->cust_billing_currency_uuid:'';
        $list['igc_contract_currency'] = isset($config_currencies[$line_item_header->igc_contract_currency])?$config_currencies[$line_item_header->igc_contract_currency]:'';
        $list['igc_contract_currency_code'] = isset($line_item_header->igc_contract_currency)?$line_item_header->igc_contract_currency:'';
        $list['igc_contract_currency_uuid'] = isset($line_item_header->igc_contract_currency_uuid)?$line_item_header->igc_contract_currency_uuid:'';
        $list['igc_billing_currency'] = isset($config_currencies[$line_item_header->igc_billing_currency])?$config_currencies[$line_item_header->igc_billing_currency]:'';
        $list['igc_billing_currency_code'] = isset($line_item_header->igc_billing_currency)?$line_item_header->igc_billing_currency:'';
        $list['igc_billing_currency_uuid'] = isset($line_item_header->igc_billing_currency_uuid)?$line_item_header->igc_billing_currency_uuid:'';
        $list['description'] = isset($line_item_header->description)?$line_item_header->description:'';
        $list['description_uuid'] = isset($line_item_header->description_uuid)?$line_item_header->description_uuid:'';
        $list['customer_billing_method'] = isset($GLOBALS['app_list_strings']['customer_billing_method_list'][$line_item_header->customer_billing_method])?$GLOBALS['app_list_strings']['customer_billing_method_list'][$line_item_header->customer_billing_method]:'';
        $list['customer_billing_method_value'] = isset($line_item_header->customer_billing_method)?$line_item_header->customer_billing_method:'';
        $list['customer_billing_method_uuid'] = isset($line_item_header->customer_billing_method_uuid)?$line_item_header->customer_billing_method_uuid:'';
        $list['igc_settlement_method'] = isset($GLOBALS['app_list_strings']['igc_settlement_method_list'][$line_item_header->igc_settlement_method])?$GLOBALS['app_list_strings']['igc_settlement_method_list'][$line_item_header->igc_settlement_method]:'';
        $list['igc_settlement_method_value'] = isset($line_item_header->igc_settlement_method)?$line_item_header->igc_settlement_method:'';
        $list['igc_settlement_method_uuid'] = isset($line_item_header->igc_settlement_method_uuid)?$line_item_header->igc_settlement_method_uuid:'';
        $product_offering = 1;
        if ($product_offering != '') {
            $prod_sp = $line_item_header->product_specification;
            $list['product_specification'] = isset($sp_details[$prod_sp]['specname'])?$sp_details[$prod_sp]['specname']:'';
            $list['product_specification_type_name'] = isset($sp_details[$prod_sp]['spectypename'])?$sp_details[$prod_sp]['spectypename']:'';
            $list['product_specification_id'] = isset($prod_sp)?$prod_sp:'';
            $l_id = isset($line_item_header->line_item_id)?$line_item_header->line_item_id:'';
            $temp = @clone ($product_config_bean);
            $temp_list = $temp->get_full_list('', "line_item_id = '$l_id'");
            $i = 0;
            if(!empty($temp_list)){
                foreach ($temp_list as $v) {
                    $c_id = isset($v->name)?$v->name:'';
                    $cv_id = isset($v->characteristic_value)?$v->characteristic_value:'';
                    $list['product_config'][$c_id]['id'] = isset($v->id)?$v->id:'';
                    $list['product_config'][$c_id]['name'] = $c_id;
                    $list['product_config'][$c_id]['lbl'] = isset($sp_details[$prod_sp]['characteristics'][$c_id]['charname'])?$sp_details[$prod_sp]['characteristics'][$c_id]['charname']:'';
                    $list['product_config'][$c_id]['characteristic_value'] = $cv_id;
                    $list['product_config'][$c_id]['characteristic_uuid'] = isset($v->characteristic_uuid)?$v->characteristic_uuid:'';
                    $list['product_config'][$c_id]['charval'] = (isset($sp_details[$prod_sp]['characteristics'][$c_id]['valtypeid']) && $sp_details[$prod_sp]['characteristics'][$c_id]['valtypeid'] == 1) ? $sp_details[$prod_sp]['characteristics'][$c_id]['characteristicvalue'][$cv_id] : $cv_id;
                    $i ++;
                }
            }

            $temp = @clone ($product_price_bean);
            $temp_list = $temp->get_full_list('', "line_item_id = '$l_id'");
            $i = 0;
            if(!empty($temp_list)){
                foreach ($temp_list as $v) {
                    $pc_id = isset($v->name)?$v->name:'';
                    $list['product_price'][$pc_id]['id'] = isset($v->id)?$v->id:'';
                    $list['product_price'][$pc_id]['pricelineid'] = isset($v->name)?$v->name:'';
                    $list['product_price'][$pc_id]['charge_type'] = isset($v->charge_type)?$v->charge_type:'';
                    $list['product_price'][$pc_id]['charge_period'] = isset($v->charge_period)?$v->charge_period:'';

                    $list['product_price'][$pc_id]['chargedesc'] = isset($pcharge_details[$pc_id]['chargedesc'])?$pcharge_details[$pc_id]['chargedesc']:'';
                    $list['product_price'][$pc_id]['chargetype'] = isset($pcharge_details[$pc_id]['chargetype'])?$pcharge_details[$pc_id]['chargetype']:'';
                    $list['product_price'][$pc_id]['chargeperioddesc'] = isset($pcharge_details[$pc_id]['chargeperioddesc'])?$pcharge_details[$pc_id]['chargeperioddesc']:'';
                    $list['product_price'][$pc_id]['currencyid'] = isset($v->currencyid)?$v->currencyid:'';
                    $list['product_price'][$pc_id]['list_price'] = (isset($v->icb_flag) && $v->icb_flag == 1) ? 'ICB' : $v->list_price;
                    $list['product_price'][$pc_id]['discount'] = (isset($v->icb_flag) && $v->icb_flag == 1) ? '-' : $v->discount;
                    $list['product_price'][$pc_id]['percent_discount_flag'] = isset($v->percent_discount_flag)?$v->percent_discount_flag:'';
                    $list['product_price'][$pc_id]['charge_price'] = isset($v->customer_contract_price)?$v->customer_contract_price:'';
                    $list['product_price'][$pc_id]['igc_settlement_price'] = isset($v->igc_settlement_price)?$v->igc_settlement_price:'';
                    $list['product_price'][$pc_id]['icb_flag'] = isset($v->icb_flag)?$v->icb_flag:'';
                    $list['product_price'][$pc_id]['icb_flag_boolean'] = ($v->icb_flag) ? 'true' : 'false';

                    $list['product_price'][$pc_id]['charge_uuid'] = isset($v->charge_uuid)?$v->charge_uuid:'';
                    $list['product_price'][$pc_id]['currencyid_uuid'] = isset($v->currencyid_uuid)?$v->currencyid_uuid:'';
                    $list['product_price'][$pc_id]['charge_type_uuid'] = isset($v->charge_type_uuid)?$v->charge_type_uuid:'';
                    $list['product_price'][$pc_id]['charge_period_uuid'] = isset($v->charge_period_uuid)?$v->charge_period_uuid:'';
                    $list['product_price'][$pc_id]['list_price_uuid'] = isset($v->list_price_uuid)?$v->list_price_uuid:'';
                    $list['product_price'][$pc_id]['icb_flag_uuid'] = isset($v->icb_flag_uuid)?$v->icb_flag_uuid:'';
                    $list['product_price'][$pc_id]['discount_amount_uuid'] = isset($v->discount_amount_uuid)?$v->discount_amount_uuid:'';
                    $list['product_price'][$pc_id]['discount_rate_uuid'] = isset($v->discount_rate_uuid)?$v->discount_rate_uuid:'';
                    $list['product_price'][$pc_id]['customer_contract_price_uuid'] = isset($v->customer_contract_price_uuid)?$v->customer_contract_price_uuid:'';
                    $list['product_price'][$pc_id]['igc_settlement_price_uuid'] = isset($v->igc_settlement_price_uuid)?$v->igc_settlement_price_uuid:'';

                    $i ++;
                }
            }

            /* Populate line item rules start */
            $product_rules_bean = BeanFactory::getBean('gc_ProductRule');
            $product_rules_result = $product_rules_bean->get_full_list('name', "line_item_id = '$line_item_header->line_item_id'");
            if ($product_rules_result != null) {
                foreach ($product_rules_result as $product_rule) {
                    $list['rules'][$product_rule->name]['ruleid'] = isset($product_rule->name)?$product_rule->name:'';
                    $list['rules'][$product_rule->name]['rulename'] = isset($sp_details[$prod_sp]['rules'][$product_rule->name]['rulename'])?$sp_details[$prod_sp]['rules'][$product_rule->name]['rulename']:'';
                    $list['rules'][$product_rule->name]['rule_objectid'] = isset($product_rule->id)?$product_rule->id:'';
                    $list['rules'][$product_rule->name]['rule_value'] = isset($product_rule->description)?$product_rule->description:'';
                    $list['rules'][$product_rule->name]['is_mandatory'] = isset($sp_details[$prod_sp]['rules'][$product_rule->name]['ismandatory'])?$sp_details[$prod_sp]['rules'][$product_rule->name]['ismandatory']:'';
                    $list['rules'][$product_rule->name]['rule_uuid'] = isset($product_rule->rule_uuid)?$product_rule->rule_uuid:'';

                }
            }
            /* Populate line item rules end */
        } else
            $list['product_specification'] = '';

            /* End : DF-38 */
        $group_detail = $this->getInvoiceGroupByLineItem($line_item_header->id);
        $list['group_id'] = (! empty($group_detail['id'])) ? $group_detail['id'] : "";
        $list['group_name'] = (isset($group_detail['group_name'])) ? $group_detail['group_name'] : "";
        $list['group_description'] = (isset($group_detail['description'])) ? $group_detail['description'] : "";
        $list['group_name_uuid'] = (! empty($group_detail['group_name_uuid'])) ? $group_detail['group_name_uuid'] : "";
        $list['group_description_uuid'] = (! empty($group_detail['description_uuid'])) ? $group_detail['description_uuid'] : "";
        $tech_account_details = self::getAccountNameByID($line_item_header->tech_account_id, true);
        $list['tech_account_id'] = isset($line_item_header->tech_account_id)?$line_item_header->tech_account_id:'';
        $list['tech_account_name'] = isset($tech_account_details['account_name'])?$tech_account_details['account_name']:'';
        $list['tech_account_country'] = isset($tech_account_details['country_id'])?$tech_account_details['country_id']:'';
        if(!empty($list['tech_account_country'])){
            $tech_acc_country_bean_obj = BeanFactory::getBean('c_country');
            $tech_acc_country_result = $tech_acc_country_bean_obj->retrieve($list['tech_account_country']);
            if(isset($tech_acc_country_result->name) && !empty($tech_acc_country_result->name)){
                $list['tech_account_country_code_alphabet'] = $tech_acc_country_result->country_code_alphabet;
            }
        }
        $bill_account_name = self::getAccountNameByID($line_item_header->bill_account_id, true);
        $list['bill_account_id'] = isset($line_item_header->bill_account_id)?$line_item_header->bill_account_id:'';
        $list['bill_account_name'] = isset($bill_account_name['account_name'])?$bill_account_name['account_name']:'';
        $list['bill_account_country'] = isset($bill_account_name['country_id'])?$bill_account_name['country_id']:'';
        if(!empty($list['bill_account_country'])){
            $bill_acc_country_bean_obj = BeanFactory::getBean('c_country');
            $bill_acc_country_result = $bill_acc_country_bean_obj->retrieve($list['bill_account_country']);
            if(isset($bill_acc_country_result->name) && !empty($bill_acc_country_result->name)){
                $list['bill_account_country_code_alphabet'] = $bill_acc_country_result->country_code_alphabet;
            }
        }
        $list['line_item_id'] = isset($line_item_header->line_item_id)?$line_item_header->line_item_id:'';
        $list['line_item_id_uuid'] = isset($line_item_header->line_item_id_uuid)?$line_item_header->line_item_id_uuid:'';
        $line_item_bean = BeanFactory::getBean('gc_LineItem');
        $line_item_bean_arr = $line_item_bean->retrieve($line_item_header->line_item_id);
        $list['product_quantity'] = isset($line_item_bean_arr->product_quantity)?$line_item_bean_arr->product_quantity:'';
        $list['global_contract_item_id'] = isset($line_item_bean_arr->name)?$line_item_bean_arr->name:'';
        $product_name = $modUtils->getProductNameByID($line_item_bean_arr->products_id);
        $list['products_id'] = isset($line_item_bean_arr->products_id)?$line_item_bean_arr->products_id:'';
        $list['products_name'] = $product_name;

        $gc_organization = self::getOrganizationNameByID($line_item_header->gc_organization_id_c);
        $list['gc_organization_id_c'] = isset($line_item_header->gc_organization_id_c)?$line_item_header->gc_organization_id_c:'';
        $list['gc_organization_id'] = isset($line_item_header->gc_organization_id_c)?$line_item_header->gc_organization_id_c:'';
        $list['gc_organization_name'] = $gc_organization;

        /* DF-373 new field contract line item type */
        $list['contract_line_item_type'] = isset($line_item_header->contract_line_item_type)?$line_item_header->contract_line_item_type:'';
        global $app_list_strings;
        $payment_type_value = isset($app_list_strings['payment_type_list'][$line_item_header->payment_type])?$app_list_strings['payment_type_list'][$line_item_header->payment_type]:'';
        $list['payment_type_id'] = isset($line_item_header->payment_type)?$line_item_header->payment_type:'';
        $list['payment_type_value'] = $payment_type_value;
        if ($line_item_header->payment_type == 00) {
            $direct_deposit_bean = BeanFactory::getBean('gc_DirectDeposit');
            $direct_deposit_arr = $direct_deposit_bean->get_full_list('', "contract_history_id = '$line_item_header->id'");
            if ($direct_deposit_arr != null) {
                foreach ($direct_deposit_arr as $direct_deposit_list) {
                    $list['deposit_deposit_details']['id'] = isset($direct_deposit_list->id)?$direct_deposit_list->id:'';
                    $list['deposit_deposit_details']['bank_code'] = isset($direct_deposit_list->bank_code)?$direct_deposit_list->bank_code:'';
                    $list['deposit_deposit_details']['branch_code'] = isset($direct_deposit_list->branch_code)?$direct_deposit_list->branch_code:'';
                    $deposittype = isset($GLOBALS['app_list_strings']['deposit_type_list'][$direct_deposit_list->deposit_type])?$GLOBALS['app_list_strings']['deposit_type_list'][$direct_deposit_list->deposit_type]:'';
                    $list['deposit_deposit_details']['deposit_type'] = $deposittype;
                    $list['deposit_deposit_details']['deposit_type_value'] = isset($direct_deposit_list->deposit_type)?$direct_deposit_list->deposit_type:'';
                    $list['deposit_deposit_details']['person_name_1'] = isset($direct_deposit_list->payee_contact_person_name_1)?$direct_deposit_list->payee_contact_person_name_1:'';
                    $list['deposit_deposit_details']['person_name_2'] = isset($direct_deposit_list->payee_contact_person_name_2)?$direct_deposit_list->payee_contact_person_name_2:'';
                    $list['deposit_deposit_details']['payee_email_address'] = isset($direct_deposit_list->payee_email_address)?$direct_deposit_list->payee_email_address:'';
                    $list['deposit_deposit_details']['remarks'] = isset($direct_deposit_list->remarks)?$direct_deposit_list->remarks:'';
                    $list['deposit_deposit_details']['payee_tel_no'] = isset($direct_deposit_list->payee_tel_no)?$direct_deposit_list->payee_tel_no:'';
                    $list['deposit_deposit_details']['account_no'] = isset($direct_deposit_list->account_no)?$direct_deposit_list->account_no:'';
                }
            }
        }
        if ($line_item_header->payment_type == 10) {
            $account_transfer_bean = BeanFactory::getBean('gc_AccountTransfer');
            $acc_transfer_arr = $account_transfer_bean->get_full_list('', "contract_history_id = '$line_item_header->id'");
            if ($acc_transfer_arr != null) {
                foreach ($acc_transfer_arr as $acc_transfer_list) {
                    $list['acc_trans_details']['id'] = isset($acc_transfer_list->id)?$acc_transfer_list->id:'';
                    $list['acc_trans_details']['name'] = isset($acc_transfer_list->name)?$acc_transfer_list->name:'';
                    $list['acc_trans_details']['bank_code'] = isset($acc_transfer_list->bank_code)?$acc_transfer_list->bank_code:'';
                    $list['acc_trans_details']['branch_code'] = isset($acc_transfer_list->branch_code)?$acc_transfer_list->branch_code:'';
                    $deposittype = isset($GLOBALS['app_list_strings']['deposit_type_list'][$acc_transfer_list->deposit_type])?$GLOBALS['app_list_strings']['deposit_type_list'][$acc_transfer_list->deposit_type]:'';
                    $list['acc_trans_details']['deposit_type'] = $deposittype;
                    $list['acc_trans_details']['deposit_type_value'] = isset($acc_transfer_list->deposit_type)?$acc_transfer_list->deposit_type:'';
                    $list['acc_trans_details']['account_no'] = isset($acc_transfer_list->account_no)?$acc_transfer_list->account_no:'';
                    $account_no_display_list = isset($GLOBALS['app_list_strings']['account_no_display_list'][$acc_transfer_list->account_no_display])?$GLOBALS['app_list_strings']['account_no_display_list'][$acc_transfer_list->account_no_display]:'';
                    $list['acc_trans_details']['account_no_display'] = $account_no_display_list;
                    $list['acc_trans_details']['account_no_display_value'] = isset($acc_transfer_list->account_no_display)?$acc_transfer_list->account_no_display:'';
                }
            }
        }
        if ($line_item_header->payment_type == 30) {
            $postal_transfer_bean = BeanFactory::getBean('gc_PostalTransfer');
            $postal_transfer_arr = $postal_transfer_bean->get_full_list('', "contract_history_id = '$line_item_header->id'");
            if ($postal_transfer_arr != null) {
                foreach ($postal_transfer_arr as $postal_transfer_list) {
                    $list['postal_trans_details']['id'] = isset($postal_transfer_list->id)?$postal_transfer_list->id:'';
                    $list['postal_trans_details']['name'] = isset($postal_transfer_list->name)?$postal_transfer_list->name:'';
                    $list['postal_trans_details']['postal_passbook_mark'] = isset($postal_transfer_list->postal_passbook_mark)?$postal_transfer_list->postal_passbook_mark:'';
                    $list['postal_trans_details']['postal_passbook'] = isset($postal_transfer_list->postal_passbook)?$postal_transfer_list->postal_passbook:'';
                    $postal_passbook_no_display = isset($GLOBALS['app_list_strings']['postal_passbook_no_display_list'][$postal_transfer_list->postal_passbook_no_display])?$GLOBALS['app_list_strings']['postal_passbook_no_display_list'][$postal_transfer_list->postal_passbook_no_display]:'';
                    $list['postal_trans_details']['postal_passbook_no_display'] = $postal_passbook_no_display;
                    $list['postal_trans_details']['postal_passbook_no_display_value'] = isset($postal_transfer_list->postal_passbook_no_display)?$postal_transfer_list->postal_passbook_no_display:'';
                }
            }
        }
        if ($line_item_header->payment_type == 60) {
            $credit_card_bean = BeanFactory::getBean('gc_CreditCard');
            $credit_card_arr = $credit_card_bean->get_full_list('', "contract_history_id = '$line_item_header->id'");
            if ($credit_card_arr != null) {
                foreach ($credit_card_arr as $credit_card_list) {
                    $list['credit_card_details']['id'] = isset($credit_card_list->id)?$credit_card_list->id:'';
                    $list['credit_card_details']['credit_card_no'] = isset($credit_card_list->credit_card_no)?$credit_card_list->credit_card_no:'';
                    $list['credit_card_details']['expiration_date'] = isset($credit_card_list->expiration_date)?$credit_card_list->expiration_date:'';
                }
            }
        }
        $line_item = new gc_LineItem();
        $line_item->retrieve($line_item_header->line_item_id);
        $line_item_doc_bean = $line_item->get_linked_beans('gc_lineitem_documents_1', 'Documents');
        $document_template_type_dom = $GLOBALS['app_list_strings']['document_template_type_dom'];
        $document_category_dom = $GLOBALS['app_list_strings']['document_category_dom'];
        $line_item_doc_arr = array();
        if ($line_item_doc_bean != null) {
            foreach ($line_item_doc_bean as $line_item_doc) {
                $line_item_doc_arr[$line_item_doc->id]['doc_id'] = isset($line_item_doc->id)?$line_item_doc->id:'';
                $line_item_doc_arr[$line_item_doc->id]['doc_name'] = isset($line_item_doc->document_name)?$line_item_doc->document_name:'';
                $line_item_doc_arr[$line_item_doc->id]['filename'] = isset($line_item_doc->filename)?$line_item_doc->filename:'';
                $line_item_doc_arr[$line_item_doc->id]['revision'] = isset($line_item_doc->revision)?$line_item_doc->revision:'';
                $line_item_doc_arr[$line_item_doc->id]['template_type'] = isset($document_template_type_dom[$line_item_doc->template_type])?$document_template_type_dom[$line_item_doc->template_type]:'';
                $line_item_doc_arr[$line_item_doc->id]['category_id'] = isset($document_category_dom[$line_item_doc->category_id])?$document_category_dom[$line_item_doc->category_id]:'';
            }
        }
        $list['line_item_doc_arr'] = $line_item_doc_arr;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'list : ' . print_r($list, true), 'Method to get line item details');
        unset($direct_deposit_bean, $direct_deposit_arr, $line_item_bean, $line_item_bean_arr, $line_item, $line_item_doc_bean);
        return $list;
    }

    /**
     * Method to get contract header for given global_contract_id or global_contract_id_uuid
     * @param string $global_contract_id global contract id
     * @param string $global_contract_uuid global contract id uui
     * @return array contract header details
     * @author mohamed.siddiq
     * @since July 29, 2016
     */
    public function getContractHeader($global_contract_id, $global_contract_uuid, $version_number, $contract_interaction_status)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'global_contract_id : ' . $global_contract_id . GCM_GL_LOG_VAR_SEPARATOR . ' global_contract_uuid : ' . $global_contract_uuid, 'Method to get contract header for given global_contract_id or global_contract_id_uuid');
        $where_cond = '';
        $list = array();

        if (trim($global_contract_id) != '' || trim($global_contract_uuid) != '') {
            $where_cond = "(global_contract_id='" . $global_contract_id . "' or global_contract_id_uuid = '" . $global_contract_uuid . "') ";
            if(trim($version_number) === '0') {
                return $list;
            }
            elseif (trim($version_number) != '') {
                $version_number = sprintf('%.1f', $version_number);
                $where_cond .= " AND contract_version = '" . $version_number . "'";
            }
            if (trim($contract_interaction_status) != '') {
                require_once 'custom/include/globalUUIDConfig.php';
                $arr_interaction_status = explode(',', $contract_interaction_status);
                $arr_contract_status    = array();
                foreach ($arr_interaction_status as $status) {
                    $interaction_status_index = array_search(trim($status), $interaction_status);
                    if ($interaction_status_index !== false) {
                        $arr_contract_status = array_merge($arr_contract_status, array_keys($contract_interaction_status_mapping, $interaction_status_index));
                    }
                }
                if (!empty($arr_contract_status)) {
                    $where_cond .= " AND contract_status IN (" . implode(',', $arr_contract_status) . ")";
                } else {
                    return $list;
                }
            }

            if (! isset($respone_error_code_arr['respone_error_code'])) {
                // Get the latest version of the contract
                $contract = BeanFactory::getBean('gc_Contracts');
                $contract_list = $contract->get_full_list('contract_version desc', $where_cond);

                if (! empty($contract_list)) {
                    $contract = $contract->retrieve($contract_list[0]->id);
                    if ($contract->name != '' && $contract->product_offering != '') {
                        $list = $contract;
                    }
                }
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'list : ' . print_r($list, true), 'Method to get contract header for given global_contract_id or global_contract_id_uuid');
        return $list;
    }

    /**
     * Method to save contract comments
     * @param string $contract_id contract id
     * @param string $comments contract comments
     * @param string $workflow_id workflow id
     * @param string $workflow_action workflow action
     * @author Dinesh.Itkar
     * @since Feb 22, 2016
     */
    public function saveContractComments($contract_id, $comments, $workflow_id, $workflow_action)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $contract_id . GCM_GL_LOG_VAR_SEPARATOR . 'comments : ' . $comments . GCM_GL_LOG_VAR_SEPARATOR . 'workflow_id : ' . $workflow_id .GCM_GL_LOG_VAR_SEPARATOR . 'workflow_action : ' . $workflow_action, 'Method to save contract comments');
        $contract_comments_bean = BeanFactory::getBean('gc_comments');
        $contract_comments_bean->gc_contracts_id_c = $contract_id;
        $contract_comments_bean->description = $comments;
        $contract_comments_bean->assigned_user_id = $GLOBALS['current_user']->id;
        $contract_comments_bean->team_id = 1;
        $contract_comments_bean->team_set_id = 1;

        if ($workflow_id != "" && $workflow_action != "") {
            // get workflow action name
            $workflow_action_name = $this->getWorkflowActionName($workflow_id, $workflow_action);
            $contract_comments_bean->name = $workflow_action_name;
        }

        require_once ('modules/Teams/TeamSet.php');
        $teamSetBean = new TeamSet();
        $teams = $teamSetBean->getTeams($GLOBALS['current_user']->team_set_id);
        $team_ids = array();
        foreach ($teams as $key => $val) {
            $team_ids[] = $key;
        }
        $contract_comments_bean->load_relationship('teams');
        // Add the teams
        $contract_comments_bean->teams->add($team_ids);
        $contract_comments_bean->save();

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_comments_bean : ' . $contract_comments_bean->id, 'Method to save contract comments');
    }

    /**
     * Method to get workflow action name
     * @param string $workflow workflow id
     * @param string $workflow workflow action
     * @return string workflow action name
     * @author Debasish.Gupta
     * @since Aug 24, 2016
     */
    private function getWorkflowActionName($workflow_id, $workflow_action)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'workflow_id : ' . $workflow_id . GCM_GL_LOG_VAR_SEPARATOR . 'workflow_action : ' . $workflow_action, 'Method to get workflow action name');
        if ($workflow_id != "" && $workflow_action != "") {
            $action_name = "";
            if ($workflow_id == "0" && $workflow_action == "SendForApproval") {
                $action_name = "Contract submitted to workflow";
            } elseif ($workflow_id == "1") {
                if ($workflow_action == "Approve") {
                    $action_name = "Approved by 1st level";
                } elseif ($workflow_action == "Reject") {
                    $action_name = "Rejected by 1st level";
                } elseif ($workflow_action == "Withdraw") {
                    $action_name = "Withdrawn";
                }
            } elseif ($workflow_id == "2") {
                if ($workflow_action == "Approve") {
                    $action_name = "Approved by 2nd level";
                } elseif ($workflow_action == "Reject") {
                    $action_name = "Rejected by 2nd level";
                } elseif ($workflow_action == "Withdraw") {
                    $action_name = "Withdrawn";
                }
            } elseif ($workflow_id == "3") {
                if ($workflow_action == "Approve") {
                    $action_name = "Approved by 3rd level";
                } elseif ($workflow_action == "Reject") {
                    $action_name = "Rejected by 3rd level";
                } elseif ($workflow_action == "Withdraw") {
                    $action_name = "Withdrawn";
                }
            } elseif ($workflow_id == "4" && $workflow_action == "SendTerminationApproval") {
                $action_name = "Send for Termination Approval";
            } elseif ($workflow_id == "5") {
                if ($workflow_action == "Approve") {
                    $action_name = "Termination Approved by 1st level";
                } elseif ($workflow_action == "Reject") {
                    $action_name = "Termination Rejected by 1st level";
                } elseif ($workflow_action == "Withdraw") {
                    $action_name = "Termination Withdrawn";
                }
            } elseif ($workflow_id == "6") {
                if ($workflow_action == "Approve") {
                    $action_name = "Termination Approved by 2nd level";
                } elseif ($workflow_action == "Reject") {
                    $action_name = "Termination Rejected by 2nd level";
                } elseif ($workflow_action == "Withdraw") {
                    $action_name = "Termination Withdrawn";
                }
            } elseif ($workflow_id == "7") {
                if ($workflow_action == "Approve") {
                    $action_name = "Termination Approved by 3rd level";
                } elseif ($workflow_action == "Reject") {
                    $action_name = "Termination Rejected by 3rd level";
                } elseif ($workflow_action == "Withdraw") {
                    $action_name = "Termination Withdrawn";
                }
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'action_name : ' . $action_name, 'Method to get workflow action name');
        return $action_name;
    }

    /**
     * To check is there any previous of contract available.
     * And if available change the status as "Upgraded"
     *
     * @author Mohamed.Siddiq
     * @since Jul 09, 2016
     */
    public function makePreviousContractAsUpgraded($bean)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'global_contract_id : ' . $bean->global_contract_id . GCM_GL_LOG_VAR_SEPARATOR . 'contr_version : '. $bean->contract_version . GCM_GL_LOG_VAR_SEPARATOR . 'contract_status : ' . $bean->contract_status, 'To check is there any previous of contract available');
        if (intval($bean->contract_version) > 1 && $bean->contract_status == '4') {
            $global_contract_id = $bean->global_contract_id;
            $contr_version = intval($bean->contract_version);
            $contr_version = $contr_version - 1;
            $contr_version = ($contr_version > 1) ? $contr_version : 1;
            $where = " global_contract_id = '" . $global_contract_id . "' AND contract_version = '" . $contr_version . "' ";
            $contract_obj = BeanFactory::getBean('gc_Contracts');
            $result_beans = $contract_obj->get_full_list('', $where);
            if (!empty($result_beans) && is_array($result_beans)) {
                if (isset($result_beans[0]->id) && $result_beans[0]->id != '') {
                    $contract_obj->retrieve($result_beans[0]->id);
                    $contract_obj->contract_status = '7';
                    $contract_obj->contract_startdate = $contract_obj->fetched_row['contract_startdate'];
                    $contract_obj->contract_enddate = $contract_obj->fetched_row['contract_enddate'];
                    $contract_obj->save();
                }
            }
        }
    }

    /**
     * GUI Beluga validation for billing account, billing start date, billing end date and payment type.
     *
     * @author Dinesh Itkar
     * @since Feb 02, 2018
     */
    public function validateBillingAccForActivation($contract_id,$global_contract_id_uuid){
        $billing_acc_validation_arr = array();
        $gc_contract_bean_obj = BeanFactory::getBean('gc_Contracts');

        if(!empty($contract_id)){
            $contract_bean_result = $gc_contract_bean_obj->retrieve($contract_id);
        }elseif(!empty($global_contract_id_uuid)){
            $contract_bean_result = $gc_contract_bean_obj->retrieve_by_string_fields(array(
                'global_contract_id_uuid' => $global_contract_id_uuid,
            ));
            $contract_id = $gc_contract_bean_obj->id;
        }

        if(!empty($contract_bean_result->product_offering)){
            require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';
            $product_config_obj = new ClassProductConfiguration();
            $product_offering_detail_arr = $product_config_obj->getProductOfferingDetails($contract_bean_result->product_offering);
            if(
                !empty($product_offering_detail_arr) &&
                isset($product_offering_detail_arr['response_code']) &&
                $product_offering_detail_arr['response_code'] == 1 &&
                isset($product_offering_detail_arr['respose_array']['listofferingoption']['beluga_product_type']) &&
                isset($product_offering_detail_arr['respose_array']['listofferingoption']['beluga_service_type']) &&
                !empty($product_offering_detail_arr['respose_array']['listofferingoption']['beluga_product_type']) &&
                !empty($product_offering_detail_arr['respose_array']['listofferingoption']['beluga_service_type'])
                ){
                    $line_item_details_arr = $this->getLineItemHeader($contract_id, false, $contract_bean_result->product_offering, false);

                    if(!empty($line_item_details_arr)){
                        foreach($line_item_details_arr['order'] as $key => $value){

                            if(empty($line_item_details_arr[$value]['bill_account_id'])){
                                $billing_acc_validation_arr[$key] = array();
                            } else if($line_item_details_arr[$value]['bill_account_country_code_alphabet']== 'JPN' && $line_item_details_arr[$value]['customer_billing_method_value'] != 'GBS_Billing'){
                                if(empty($line_item_details_arr[$value]['billing_start_date'])){
                                    $billing_acc_validation_arr[$key][] = 'Billing Start Date';
                                }

                                if(empty($line_item_details_arr[$value]['billing_end_date'])){
                                    $billing_acc_validation_arr[$key][] = 'Billing End Date';
                                }

                                if(isset($line_item_details_arr[$value]['payment_type_id']) && $line_item_details_arr[$value]['payment_type_id'] == ''){
                                    $billing_acc_validation_arr[$key][] = 'Payment Type';
                                }
                            }
                        }
                    }
                }
        }
        ksort($billing_acc_validation_arr);
        return $billing_acc_validation_arr;
    }
  }
?>