<?php
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

/**
 * gc_ContractsController Class to Detail View Actions
 *
 * @author Shrikant.Gaware
 * @since Sep 24, 2015
 */
include_once 'custom/modules/gc_Contracts/CustomFunctionGcContract.php';
include_once 'custom/modules/gc_Contracts/include/ClassGcContracts.php';
include_once('custom/modules/Administration/DBActions.php');

class gc_ContractsController extends SugarController
{

    private $obj_gc_contracts;

    private $approver;

    public function __construct()
    {
        parent::__construct();
        $this->obj_gc_contracts        = new ClassGcContracts();
        $this->obj_custom_gc_contracts = new CustomFunctionGcContract();
        $this->approver                = $this->obj_gc_contracts->getRolePermissions(SETTING_KEY_ACE_CONTRACTS);
    }

    /**
     * Method to send contract for approval
     *
     * @author Shrikant.Gaware
     * @since Feb 19, 2016
     */
    public function action_SendForApproval()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $this->bean->id . GCM_GL_LOG_VAR_SEPARATOR . 'workflow_flag : ' . $this->bean->workflow_flag, 'Method to send contract for approval');
        global $mod_strings;
        $contract_id   = $this->bean->id;
        $workflow_flag = $this->bean->workflow_flag;
        $bean          = @clone ($this->bean);

        if ($this->bean->workflow_flag == '0' && (ACLController::checkAccess($this->bean->object_name, 'edit', true) || $GLOBALS['current_user']->is_admin)) {
            $bean->workflow_flag = $this->bean->workflow_flag = '1';

            // set contract status to send for approval as decribed in DF-373
            $bean->contract_status    = $this->bean->contract_status    = '3';
            $bean->contract_startdate = $this->bean->contract_startdate = $this->bean->fetched_row['contract_startdate'];
            $bean->contract_enddate   = $this->bean->contract_enddate   = $this->bean->fetched_row['contract_enddate'];
            $bean->save();

            $approver = $this->obj_custom_gc_contracts->getRolePermissions(SETTING_KEY_ACE_CONTRACTS);
            if (isset($approver['approver_team'])) {
                $team_ids = $approver['approver_team'];
                $this->bean->load_relationship('teams');
                $this->bean->teams->add($team_ids);
            }
            $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "SendForApproval");
        } else {
            SugarApplication::appendErrorMessage($mod_strings['LBL_INVALID_STAGE']);
        }
        $params = array(
            'module' => 'gc_Contracts',
            'action' => 'DetailView',
            'record' => $contract_id,
        );
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'Method to send contract for approval');
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }

    /**
     * Method to Approve the Contract
     *
     * @author Shrikant.Gaware
     * @since Feb 19, 2016
     */
    public function action_ApproveContract()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $this->bean->id, 'Method to Approve the Contract');
        global $mod_strings;
        $contract_id = $this->bean->id;
        if (self::checkVaildRole()) {
            $flag = 0;
        } else {
            $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $this->bean->workflow_flag, "Approve");
            $flag = self::saveContractAction('Approved');
        }
        if (!$flag) {
            SugarApplication::appendErrorMessage($mod_strings['LBL_INVALID_STAGE']);
        }
        $params = array(
            'module' => 'gc_Contracts',
            'action' => 'DetailView',
            'record' => $contract_id,
        );
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'Method to Approve the Contract');
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }

    /**
     * Method to Reject the Contract
     *
     * @author Shrikant.Gaware
     * @since Feb 19, 2016
     */
    public function action_RejectContract()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $this->bean->id, 'Method to Reject the Contract');
        global $mod_strings;
        $contract_id = $this->bean->id;
        if (self::checkVaildRole()) {
            $flag = 0;
        } else {
            if (!empty($_REQUEST['comment'])) {
                $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $this->bean->workflow_flag, "Reject");
            }
            $flag = self::saveContractAction('Rejected');
        }
        if (!$flag) {
            SugarApplication::appendErrorMessage($mod_strings['LBL_INVALID_STAGE']);
        }
        $params = array(
            'module' => 'gc_Contracts',
            'action' => 'DetailView',
            'record' => $contract_id,
        );
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'Method to Reject the Contract');
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }

    /**
     * Method to save contract action
     *
     * @param string $action Approved/Rejected contract
     * @return bool
     * @author Shrikant.Gaware
     * @since Feb 22, 2016
     */
    private function saveContractAction($action)
    {
        global $sugar_config;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'initial_stage : ' . $this->bean->workflow_flag, 'Method to save contract action');
        $flag          = 0;
        $final_stage   = '';
        $initial_stage = $this->bean->workflow_flag;
        $bean          = @clone ($this->bean);
        if ($action == 'Approved') {
            $final_stage = $initial_stage + 1;
            if ($final_stage < 4) {
                $bean->workflow_flag   = $this->bean->workflow_flag   = $final_stage;
                $bean->contract_status = $this->bean->contract_status = '3';
                $flag                  = 1;
            } elseif ($final_stage == 4) {
                $bean->workflow_flag   = $this->bean->workflow_flag   = $final_stage;
                $bean->contract_status = $this->bean->contract_status = '1';
                $flag                  = 1;
            } else {
                $flag = 0;
            }
        } elseif ($action == "Activate") {
            if ($this->bean->contract_status == '1' || ($this->bean->contract_status == '0' && in_array($this->bean->product_offering, $sugar_config['enterprise_mail_po']))) {
                $bean->contract_status = $this->bean->contract_status = '4';

                $this->obj_custom_gc_contracts->makePreviousContractAsUpgraded($this->bean);
                $line_item_his_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
                $contracts_id = $this->bean->id;

                $line_item_his_bean_arr = $line_item_his_bean->get_full_list('', "contracts_id = '$contracts_id'");

                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_item_his_bean_arr : ' . print_r($line_item_his_bean_arr, true), 'log all line item');

                if ($line_item_his_bean_arr != null) {
                    foreach ($line_item_his_bean_arr as $line_item_header) {
                        $line_item_header->contract_line_item_type = ($line_item_header->contract_line_item_type != 'Service_Termination') ? "Activated" : "Service_Termination";
                        $line_item_header->save();
                    }
                }
                $flag = 1;
            } else {
                $flag = 0;
                SugarApplication::appendErrorMessage($mod_strings['LBL_INVALID_STAGE']);
            }
        } elseif ($action == "Withdraw_Approval") {
            $bean->contract_status = $this->bean->contract_status = '0';
            $bean->workflow_flag   = $this->bean->workflow_flag   = '9';
            $flag                  = 1;
            $final_stage           = $bean->workflow_flag;
        } elseif ($action == "Withdraw_Termination") {
            $bean->contract_status = $this->bean->contract_status = '4';
            $bean->workflow_flag   = $this->bean->workflow_flag   = '10';
            $final_stage           = $bean->workflow_flag;
            $flag                  = 1;
        } else {
            $final_stage           = 0;
            $bean->workflow_flag   = $this->bean->workflow_flag   = $final_stage;
            $bean->contract_status = $this->bean->contract_status = '0';
            $flag                  = 1;
        }

        $bean->contract_startdate = $this->bean->contract_startdate = $this->bean->fetched_row['contract_startdate'];
        $bean->contract_enddate   = $this->bean->contract_enddate   = $this->bean->fetched_row['contract_enddate'];
        $bean->save();

        $this->saveContractApprovals($this->bean->id, $this->bean->global_contract_id, $action, $initial_stage, $final_stage);

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'flag : ' . $flag, 'Method to save contract action');
        return $flag;
    }

    /**
     * Method to save contract Actions ( Approve / Reject )
     *
     * @param string $contract_id contract object id
     * @param string $global_contract_id global contract id
     * @param string $action contract action ( Approve / Reject )
     * @param string $from_stage Current stage of contract
     * @param string $to_stage Next stage of contract
     * @author Dinesh.Itkar
     * @since Feb 24, 2016
     */
    private function saveContractApprovals($contract_id, $global_contract_id, $action, $from_stage, $to_stage)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $contract_id . GCM_GL_LOG_VAR_SEPARATOR . 'global_contract_id : ' . $global_contract_id . GCM_GL_LOG_VAR_SEPARATOR . 'action : ' . $action . GCM_GL_LOG_VAR_SEPARATOR . 'from_stage : ' . $from_stage . GCM_GL_LOG_VAR_SEPARATOR . 'to_stage : ' . $to_stage, 'Method to save contract Actions ( Approve / Reject )');
        $contract_approval_bean                    = BeanFactory::getBean('gc_MyApprovals');
        $contract_approval_bean->name              = $global_contract_id;
        $contract_approval_bean->gc_contracts_id_c = $contract_id;
        $contract_approval_bean->contract_action   = $action;
        $contract_approval_bean->from_stage        = $from_stage;
        $contract_approval_bean->to_stage          = $to_stage;
        $contract_approval_bean->assigned_user_id  = $GLOBALS['current_user']->id;

        require_once 'modules/Teams/TeamSet.php';
        $teamSetBean = new TeamSet();
        $teams       = $teamSetBean->getTeams($GLOBALS['current_user']->team_set_id);
        $team_ids    = array();
        foreach ($teams as $key => $val) {
            $team_ids[] = $key;
        }
        $contract_approval_bean->load_relationship('teams');
        // Add the teams
        $contract_approval_bean->teams->add($team_ids);

        $contract_approval_bean->save();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_approval_bean : ' . $contract_approval_bean->id, 'Method to save contract Actions ( Approve / Reject )');
    }

    /**
     * Method to check if user belong to proper Approver team
     *
     * @return bool
     * @author Shrikant.Gaware
     * @since Mar 04, 2016
     */
    private function checkVaildRole()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'initial_stage : ' . $this->bean->workflow_flag . GCM_GL_LOG_VAR_SEPARATOR . 'roles : ' . print_r($this->obj_custom_gc_contracts->getUserRoleIds(), true) . GCM_GL_LOG_VAR_SEPARATOR . 'approver : ' . print_r($this->obj_custom_gc_contracts->getRolePermissions(SETTING_KEY_ACE_CONTRACTS), true), 'Method to check if user belong to proper Approver team');
        $initial_stage = $this->bean->workflow_flag;
        $roles         = $this->obj_custom_gc_contracts->getUserRoleIds();
        $approver      = $this->obj_custom_gc_contracts->getRolePermissions(SETTING_KEY_ACE_CONTRACTS);
        $approver      = $approver[$initial_stage];
        $flag          = 0;
        if ((!in_array($approver, $roles) && !$GLOBALS['current_user']->is_admin) || $initial_stage == 0) {
            $flag = 1;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'flag : ' . $flag, 'Method to check if user belong to proper Approver team');
        return $flag;
    }

    /**
     * Method to Activate the Contract
     *
     * @author Kamal.Thiyagarajan
     * @since May 20, 2016
     */
    public function action_ActivateContract()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to Activate the Contract');

        global $mod_strings, $sugar_config;
        $contract_id         = $this->bean->id;
        $roles               = $this->obj_custom_gc_contracts->getUserRoleIds();
        $workflow_config_key = SETTING_KEY_ACE_CONTRACTS;

        if (in_array($this->bean->product_offering, $sugar_config['enterprise_mail_po'])) {
            $workflow_config_key = 'AccessControlExt_EnterpriseMail';
        }

        $this->approver = $this->obj_custom_gc_contracts->getRolePermissions($workflow_config_key);
        $role_intersect = $this->obj_gc_contracts->arrayIntersectMulti($roles, $this->approver);

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'Contract_id : ' . $this->bean->id . GCM_GL_LOG_VAR_SEPARATOR . ' Roles : ' . print_r($roles, true) . GCM_GL_LOG_VAR_SEPARATOR . ' Approver : ' . print_r($this->approver, true), 'Log Variables');


         // DF-1939 - Billing account validation starts
        require_once ('custom/modules/gc_Contracts/CustomFunctionGcContract.php');
        $CustomFunctionGcContract_obj = new CustomFunctionGcContract();
        $billing_acc_activation_err_lists = $CustomFunctionGcContract_obj->validateBillingAccForActivation($contract_id,'');
        if(!empty($billing_acc_activation_err_lists)){
            $bill_acc_validate_error_msg = $mod_strings['LBL_BILL_ACC_ERROR'];
            foreach($billing_acc_activation_err_lists as $key => $value){
                if(empty($value)){
                    $bill_acc_err_list = '';
                    $replace_string_with = '"Billing Customer Account"';
                    $bill_acc_err_list = str_replace('field_list',$replace_string_with,$bill_acc_validate_error_msg);
                    $bill_acc_err_list = str_replace('line_item_id',$key,$bill_acc_err_list);
                    SugarApplication::appendErrorMessage($bill_acc_err_list);
                }else{
                    $field_list_string = '';
                    $field_list_err_list = '';
                    $field_list_string = implode('", "', $value);
                    $field_list_err_list = str_replace('field_list','"'.$field_list_string.'"',$bill_acc_validate_error_msg);
                    $field_list_err_list = str_replace('line_item_id',$key,$field_list_err_list);
                    if(count($value)>1){
                        $field_list_err_list = str_replace('is','are',$field_list_err_list);
                    }
                    SugarApplication::appendErrorMessage($field_list_err_list);
                }
            }
        $params = array(
            'module' => 'gc_Contracts',
            'action' => 'DetailView',
            'record' => $contract_id,
        );
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'Method to Activate the Contract');
        SugarApplication::redirect('index.php?' . http_build_query($params));
        }
         // DF-1939 - Billing account validation ends

        // admin and approver only can able to change to activate contract
        if($this->bean->fetched_row['contract_startdate'] == '' || $this->bean->fetched_row['gc_timezone_id_c']==''){
            SugarApplication::appendErrorMessage($mod_strings['LBL_CONTRACT_STARTDATE_ACIVATE_ERR']);
        }
        else{
            if ($GLOBALS['current_user']->is_admin || count($role_intersect) != 0) {
                $flag = self::saveContractAction('Activate');
            } else {
                $flag = 0;
            }
            if (!$flag) {
                SugarApplication::appendErrorMessage($mod_strings['LBL_INVALID_STAGE']);
            }
        }

        $params = array(
            'module' => 'gc_Contracts',
            'action' => 'DetailView',
            'record' => $contract_id,
        );
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'Method to Activate the Contract');
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }

    /**
     * Method to Send Contract termination approval request
     *
     * @author Rajul.Mondal
     * @since Jun 10, 2016
     */
    public function action_SendTerminationApproval()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $this->bean->id . GCM_GL_LOG_VAR_SEPARATOR . 'status : ' . $this->bean->contract_status . GCM_GL_LOG_VAR_SEPARATOR . 'roles : ' . print_r($this->obj_custom_gc_contracts->getUserRoleIds(), true) . GCM_GL_LOG_VAR_SEPARATOR . 'workflow_flag : ' . $this->bean->workflow_flag . GCM_GL_LOG_VAR_SEPARATOR . 'version_upgrade : ' . $this->obj_gc_contracts->checkContractVersionUpgrade($this->bean), 'Method to Send Contract termination approval request');
        global $mod_strings;
        $contract_id     = $this->bean->id;
        $status          = $this->bean->contract_status;
        $roles           = $this->obj_gc_contracts->getUserRoleIds();
        $workflow_flag   = $this->bean->workflow_flag;
        $version_upgrade = $this->obj_gc_contracts->checkContractVersionUpgrade($this->bean);
        if ($version_upgrade === true) {
            SugarApplication::appendErrorMessage($mod_strings['LBL_CONTRACT_VERSION_ALREADY_CREATED']);
        } elseif ($status === '4' && $workflow_flag === '4') {
            $this->bean->contract_startdate = $this->bean->fetched_row['contract_startdate'];
            $this->bean->contract_enddate   = $this->bean->fetched_row['contract_enddate'];
            $this->bean->workflow_flag      = '5';
            $this->bean->contract_status    = '5';
            $this->bean->save();
            $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "SendTerminationApproval");
            $this->obj_custom_gc_contracts->setLineItemStatus($contract_id, 'Termination');
        } else {
            SugarApplication::appendErrorMessage($mod_strings['LBL_INVALID_STAGE']);
        }

        $params = array(
            'module' => 'gc_Contracts',
            'action' => 'DetailView',
            'record' => $contract_id,
        );

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'Method to Send Contract termination approval request');
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }

    /**
     * Method to Approve Contract termination approval request
     *
     * @author Rajul.Mondal
     * @since Jun 17, 2016
     */
    public function action_ApproveTerminationApproval()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $this->bean->id . GCM_GL_LOG_VAR_SEPARATOR . 'status : ' . $this->bean->contract_status . GCM_GL_LOG_VAR_SEPARATOR . 'roles : ' . print_r($this->obj_custom_gc_contracts->getUserRoleIds(), true) . GCM_GL_LOG_VAR_SEPARATOR . 'workflow_flag : ' . $this->bean->workflow_flag . GCM_GL_LOG_VAR_SEPARATOR . 'version_upgrade : ' . $this->obj_gc_contracts->checkContractVersionUpgrade($this->bean), 'Method to Approve Contract termination approval request');
        global $mod_strings;
        $contract_id     = $this->bean->id;
        $status          = $this->bean->contract_status;
        $roles           = $this->obj_gc_contracts->getUserRoleIds();
        $workflow_flag   = $this->bean->workflow_flag;
        $version_upgrade = $this->obj_gc_contracts->checkContractVersionUpgrade($this->bean);
        $role_intersect  = $this->obj_gc_contracts->arrayIntersectMulti($roles, $this->approver);
        // store true|false if valid action
        $has_invalid_action             = array();
        $this->bean->contract_startdate = $this->bean->fetched_row['contract_startdate'];
        $this->bean->contract_enddate   = $this->bean->fetched_row['contract_enddate'];

        if ($version_upgrade === true) {
            SugarApplication::appendErrorMessage($mod_strings['LBL_CONTRACT_VERSION_ALREADY_CREATED']);
        } elseif ($GLOBALS['current_user']->is_admin && $status === '5') {
            if ($workflow_flag === '5') {
                $this->bean->workflow_flag = '6';
                $this->bean->save();
                $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Approve");
                $has_invalid_action[] = false;
            } elseif ($workflow_flag === '6') {
                $this->bean->workflow_flag = '7';
                $this->bean->save();
                $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Approve");
                $has_invalid_action[] = false;
            } elseif ($workflow_flag === '7') {
                $this->bean->contract_status = '6';
                $this->bean->workflow_flag   = '8';
                $this->bean->save();
                $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Approve");
                $has_invalid_action[] = false;
            } else {
                $has_invalid_action[] = true;
            }
        } elseif (count($role_intersect) > 0 && $status === '5') {
            foreach ($role_intersect as $role) {
                $approvalPosition = array_search($role, $this->approver);
                if ($approvalPosition == 1 && $workflow_flag === '5') {
                    $this->bean->workflow_flag = '6';
                    $this->bean->save();
                    $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Approve");
                    $has_invalid_action[] = false;
                } elseif ($approvalPosition == 2 && $workflow_flag === '6') {
                    $this->bean->workflow_flag = '7';
                    $this->bean->save();
                    $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Approve");
                    $has_invalid_action[] = false;
                } elseif ($approvalPosition == 3 && $workflow_flag === '7') {
                    $this->bean->contract_status = '6';
                    $this->bean->workflow_flag   = '8';
                    $this->bean->save();
                    $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Approve");
                    $has_invalid_action[] = false;
                } else {
                    $has_invalid_action[] = true;
                }
            }
        } else {
            SugarApplication::appendErrorMessage($mod_strings['LBL_INVALID_STAGE']);
        }

        // if $has_invalid_action has invalid action, show error message
        if (!in_array(false, $has_invalid_action)) {
            SugarApplication::appendErrorMessage($mod_strings['LBL_INVALID_STAGE']);
        }

        $params = array(
            'module' => 'gc_Contracts',
            'action' => 'DetailView',
            'record' => $contract_id,
        );

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'Method to Approve Contract termination approval request');
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }

    /**
     * Method to Reject Contract termination approval request
     *
     * @author Rajul.Mondal
     * @since Jun 11, 2016
     */
    public function action_RejectTerminationApproval()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $this->bean->id . GCM_GL_LOG_VAR_SEPARATOR . 'status : ' . $this->bean->contract_status . GCM_GL_LOG_VAR_SEPARATOR . 'roles : ' . print_r($this->obj_custom_gc_contracts->getUserRoleIds(), true) . GCM_GL_LOG_VAR_SEPARATOR . 'workflow_flag : ' . $this->bean->workflow_flag, 'Method to Reject Contract termination approval request');
        global $mod_strings;
        $contract_id    = $this->bean->id;
        $status         = $this->bean->contract_status;
        $roles          = $this->obj_gc_contracts->getUserRoleIds();
        $workflow_flag  = $this->bean->workflow_flag;
        $role_intersect = $this->obj_gc_contracts->arrayIntersectMulti($roles, $this->approver);
        // store true|false if valid action
        $has_invalid_action             = array();
        $this->bean->contract_startdate = $this->bean->fetched_row['contract_startdate'];
        $this->bean->contract_enddate   = $this->bean->fetched_row['contract_enddate'];

        if ($GLOBALS['current_user']->is_admin && $status === '5' && in_array($workflow_flag, array(5, 6, 7))) {
            if ($workflow_flag === '5') {
                $this->obj_custom_gc_contracts->setLineItemStatus($contract_id, 'Activated');
                $this->bean->contract_status = '4';
                $this->bean->workflow_flag   = '4';
                $this->bean->save();
                if (!empty($_REQUEST['comment'])) {
                    $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Reject");
                }
                $has_invalid_action[] = false;
            } elseif ($workflow_flag === '6') {
                $this->obj_custom_gc_contracts->setLineItemStatus($contract_id, 'Activated');
                $this->bean->contract_status = '4';
                $this->bean->workflow_flag   = '4';
                $this->bean->save();
                if (!empty($_REQUEST['comment'])) {
                    $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Reject");
                }
                $has_invalid_action[] = false;
            } elseif ($workflow_flag === '7') {
                $this->obj_custom_gc_contracts->setLineItemStatus($contract_id, 'Activated');
                $this->bean->contract_status = '4';
                $this->bean->workflow_flag   = '4';
                $this->bean->save();
                if (!empty($_REQUEST['comment'])) {
                    $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Reject");
                }
                $has_invalid_action[] = false;
            } else {
                $has_invalid_action[] = true;
            }
        } elseif (count($role_intersect) > 0 && $status === '5' && in_array($workflow_flag, array(5, 6, 7))) {
            foreach ($role_intersect as $role) {
                if ($role == $this->approver[1] && $workflow_flag === '5') {
                    $this->obj_custom_gc_contracts->setLineItemStatus($contract_id, 'Activated');
                    $this->bean->contract_status = '4';
                    $this->bean->workflow_flag   = '4';
                    $this->bean->save();
                    if (!empty($_REQUEST['comment'])) {
                        $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Reject");
                    }
                    $has_invalid_action[] = false;
                } elseif ($role == $this->approver[2] && $workflow_flag === '6') {
                    $this->obj_custom_gc_contracts->setLineItemStatus($contract_id, 'Activated');
                    $this->bean->contract_status = '4';
                    $this->bean->workflow_flag   = '4';
                    $this->bean->save();
                    if (!empty($_REQUEST['comment'])) {
                        $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Reject");
                    }
                    $has_invalid_action[] = false;
                } elseif ($role == $this->approver[3] && $workflow_flag === '7') {
                    $this->obj_custom_gc_contracts->setLineItemStatus($contract_id, 'Activated');
                    $this->bean->contract_status = '4';
                    $this->bean->workflow_flag   = '4';
                    $this->bean->save();
                    if (!empty($_REQUEST['comment'])) {
                        $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Reject");
                    }
                    $has_invalid_action[] = false;
                } else {
                    $has_invalid_action[] = true;
                }
            }
        } else {
            SugarApplication::appendErrorMessage($mod_strings['LBL_INVALID_STAGE']);
        }

        // if $has_invalid_action has invalid action, show error message
        if (!in_array(false, $has_invalid_action)) {
            SugarApplication::appendErrorMessage($mod_strings['LBL_INVALID_STAGE']);
        }

        $params = array(
            'module' => 'gc_Contracts',
            'action' => 'DetailView',
            'record' => $contract_id,
        );

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'Method to Reject Contract termination approval request');
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }

    /**
     * Method to deactivate Contract
     *
     * @author Rajul.Mondal
     * @since Jun 11, 2016
     */
    public function action_Deactivate()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $this->bean->id . GCM_GL_LOG_VAR_SEPARATOR . 'status : ' . $this->bean->contract_status . GCM_GL_LOG_VAR_SEPARATOR . 'roles : ' . print_r($this->obj_custom_gc_contracts->getUserRoleIds(), true) . GCM_GL_LOG_VAR_SEPARATOR . 'workflow_flag : ' . $this->bean->workflow_flag, 'Method to deactivate Contract');
        global $mod_strings, $sugar_config;
        $contract_id   = $this->bean->id;
        $status        = $this->bean->contract_status;
        $roles         = $this->obj_gc_contracts->getUserRoleIds();
        $workflow_flag = $this->bean->workflow_flag;
        // DF-1398 New workflow for Enterprise Mail -- fetch workflow settings conditionally on basis of product offering
        if (in_array($this->bean->product_offering, $sugar_config['enterprise_mail_po'])) {
            $this->approver = $this->obj_gc_contracts->getRolePermissions('AccessControlExt_EnterpriseMail');
        }
        $role_intersect = $this->obj_gc_contracts->arrayIntersectMulti($roles, $this->approver);

        $version_upgrade = $this->obj_gc_contracts->checkContractVersionUpgrade($this->bean);
        if ($version_upgrade === true) {
            SugarApplication::appendErrorMessage($mod_strings['LBL_CONTRACT_VERSION_ALREADY_CREATED']);
        } elseif (($GLOBALS['current_user']->is_admin || count($role_intersect) > 0) && (($status === '6' && $workflow_flag === '8') || ($status === '4' && in_array($this->bean->product_offering, $sugar_config['enterprise_mail_po'])))) {
            if (in_array($this->bean->product_offering, $sugar_config['enterprise_mail_po'])) {
                $this->obj_custom_gc_contracts->setLineItemStatus($contract_id, 'Termination');
            }
            $this->bean->contract_startdate = $this->bean->fetched_row['contract_startdate'];
            $this->bean->contract_enddate   = $this->bean->fetched_row['contract_enddate'];
            $this->bean->contract_status    = '2';
            $this->bean->save();
        } else {
            SugarApplication::appendErrorMessage($mod_strings['LBL_INVALID_STAGE']);
        }

        if (!empty($_REQUEST['comment'])) {
            $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], "", "");
        }

        $params = array(
            'module' => 'gc_Contracts',
            'action' => 'DetailView',
            'record' => $contract_id,
        );

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'Method to deactivate Contract');
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }

    /**
     * Method to check availability of creating new version
     *
     * @author Debasish.Gupta
     * @since Jul 08, 2016
     */
    public function action_CheckModificationAvailability()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $this->bean->id, 'Method to check availability of creating new version');
        $contract_id = $_REQUEST['contract_id'];
        global $mod_strings;
        $params = array(
            'module' => 'gc_Contracts',
            'action' => 'DetailView',
            'record' => $contract_id,
        );
        if ($contract_id != "") {
            $contract_bean = BeanFactory::getBean('gc_Contracts');
            $contract_bean->retrieve($contract_id);
            if (!empty($contract_bean->contract_status) && ($contract_bean->contract_status == "4")) {
                if (!empty($contract_bean->global_contract_id)) {
                    $response = $this->obj_gc_contracts->checkContractVersionUpgrade($contract_bean);
                    if ($response) {
                        SugarApplication::appendErrorMessage($mod_strings['LBL_CONTRACT_VERSION_ALREADY_CREATED']);
                    } else {
                        $params = array(
                            'module' => 'gc_Contracts',
                            'action' => 'EditView',
                            'record' => $contract_id,
                            'VerUp'  => '1',
                        );
                    }
                }
            } else {
                SugarApplication::appendErrorMessage($mod_strings['LBL_CONTRACT_NOT_ACTIVATED']);
            }
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'Method to check availability of creating new version');
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }

    /**
     * DF-479 Method to Withdraw Contract Approval Request After a user enter some comments and click "OK", GCM changes the Contract Status into "Draft".
     * Also, the Workflow Flag is changed into "0" Send one notification email
     *
     * @author Amol.Sananse
     * @since Jul 29, 2016
     */
    public function action_WithdrawApprovalRequest()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $this->bean->id . GCM_GL_LOG_VAR_SEPARATOR . 'status : ' . $this->bean->contract_status . GCM_GL_LOG_VAR_SEPARATOR . 'roles : ' . print_r($this->obj_custom_gc_contracts->getUserRoleIds(), true) . GCM_GL_LOG_VAR_SEPARATOR . 'workflow_flag : ' . $this->bean->workflow_flag, 'DF-479 Withdraw Contract Approval Request After a user enter some comments and click OK, GCM changes the Contract Status into Draft');
        global $mod_strings;
        $contract_id    = $this->bean->id;
        $status         = $this->bean->contract_status;
        $roles          = $this->obj_gc_contracts->getUserRoleIds();
        $workflow_flag  = $this->bean->workflow_flag;
        $role_intersect = $this->obj_gc_contracts->arrayIntersectMulti($roles, $this->approver);

        if (($GLOBALS['current_user']->is_admin || count($role_intersect) != 0 || (ACLController::checkAccess($this->bean->object_name, 'edit', true))) && $status === '3') {
            $flag = self::saveContractAction('Withdraw_Approval');
            if ($flag) {
                $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Withdraw");
            }
        } else {
            SugarApplication::appendErrorMessage($mod_strings['LBL_INVALID_STAGE']);
        }
        $params = array(
            'module' => 'gc_Contracts',
            'action' => 'DetailView',
            'record' => $contract_id,
        );
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'Method to Approve Contract termination approval request');
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }

    /**
     * DF-479 Withdraw Contract Termination Approval Request After a user enter some comments and click "OK", GCM changes the Contract Status into "Draft".
     * Also, the Workflow Flag is changed into Status into "Activated". Also, the Workflow Flag is changed into "4" Send one notification email
     *
     * @author Amol.Sananse
     * @since Aug 01, 2016
     */
    public function action_WithdrawTerminationApproval()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $this->bean->id . GCM_GL_LOG_VAR_SEPARATOR . 'status : ' . $this->bean->contract_status . GCM_GL_LOG_VAR_SEPARATOR . 'roles : ' . print_r($this->obj_custom_gc_contracts->getUserRoleIds(), true) . GCM_GL_LOG_VAR_SEPARATOR . 'workflow_flag : ' . $this->bean->workflow_flag, 'DF-479 Withdraw Contract Approval Request After a user enter some comments and click OK, GCM changes the Contract Status into Draft');
        global $mod_strings;
        $contract_id    = $this->bean->id;
        $status         = $this->bean->contract_status;
        $roles          = $this->obj_gc_contracts->getUserRoleIds();
        $workflow_flag  = $this->bean->workflow_flag;
        $role_intersect = $this->obj_gc_contracts->arrayIntersectMulti($roles, $this->approver);

        if (($GLOBALS['current_user']->is_admin || count($role_intersect) != 0 || (ACLController::checkAccess($this->bean->object_name, 'edit', true))) && $status === '5') {
            $flag = self::saveContractAction('Withdraw_Termination');
            $this->obj_custom_gc_contracts->setLineItemStatus($contract_id, 'Activated');
            if ($flag) {
                $this->obj_custom_gc_contracts->saveContractComments($contract_id, $_REQUEST['comment'], $workflow_flag, "Withdraw");
            }
        } else {
            SugarApplication::appendErrorMessage($mod_strings['LBL_INVALID_STAGE']);
        }
        $params = array(
            'module' => 'gc_Contracts',
            'action' => 'DetailView',
            'record' => $contract_id,
        );
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'DF-479 Withdraw Contract Termination Approval Request After a user enter some comments and click OK, GCM changes the Contract Status into Draft');
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }

    /**
     * To update sales represantative in detail view.
     *
     * @author Kamal.Thiyagarajan
     * @since Nov 02, 2016
     */
    public function action_UpdateSalesRep()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id : ' . $this->bean->id, 'To update sales represantative in detail view');
        global $mod_strings;
        $contract_id               = $_POST['return_id'];
        $sales_representative_bean = BeanFactory::getBean('gc_SalesRepresentative');
        if ($GLOBALS['current_user']->is_admin != '1' && !$sales_representative_bean->aclAccess('edit')) {
            SugarApplication::appendErrorMessage($mod_strings['LBL_UNAUTHORIZED_USER']);

            $params = array(
                'module' => 'gc_Contracts',
                'action' => 'DetailView',
                'record' => $contract_id,
            );

            SugarApplication::redirect('index.php?' . http_build_query($params));
            exit();
        }
        $result                                                        = array();
        $array_contract_sales_rep_params['sales_channel_code']         = trim($_POST['sales_channel_code']);
        $array_contract_sales_rep_params['name']                       = trim($_POST['sales_rep_name']);
        $array_contract_sales_rep_params['sales_rep_name_1']           = trim($_POST['sales_rep_name_1']);
        $array_contract_sales_rep_params['division_1']                 = trim($_POST['division_1']);
        $array_contract_sales_rep_params['division_2']                 = trim($_POST['division_2']);
        $array_contract_sales_rep_params['title_2']                    = trim($_POST['title_2']);
        $array_contract_sales_rep_params['tel_no']                     = trim($_POST['tel_no']);
        $array_contract_sales_rep_params['ext_no']                     = trim($_POST['ext_no']);
        $array_contract_sales_rep_params['fax_no']                     = trim($_POST['fax_no']);
        $array_contract_sales_rep_params['email1']                     = trim($_POST['email1']);
        $array_contract_sales_rep_params['gc_nttcomgroupcompany_id_c'] = trim($_POST['sales_company_id']);
        if (empty($contract_id)) {
            SugarApplication::appendErrorMessage($mod_strings['LBL_EMPTY_REQUEST']);
        } else {

            $obj_sales_representative_bean = BeanFactory::getBean('gc_SalesRepresentative');
            $obj_sales_representative_bean->retrieve_by_string_fields(array(
                'contracts_id' => $contract_id,
            ));
            if ($sales_representative_bean->id) {
                $obj_sales_representative_bean->retrieve($obj_sales_representative_bean->id);
            }
            foreach ($array_contract_sales_rep_params as $field => $value) {
                $obj_sales_representative_bean->$field = $value;
            }

            // Contracts CRM Record Id
            $obj_sales_representative_bean->contracts_id = $contract_id;
            $obj_sales_representative_bean->team_id      = 1;
            $obj_sales_representative_bean->team_set_id  = 1;
            $obj_sales_representative_bean->save();
            $obj_contracts_bean = BeanFactory::getBean('gc_Contracts');
            $obj_contracts_bean->retrieve($contract_id);
            $obj_contracts_bean->contract_startdate = $obj_contracts_bean->fetched_row['contract_startdate'];
            $obj_contracts_bean->contract_enddate   = $obj_contracts_bean->fetched_row['contract_enddate'];
            $obj_contracts_bean->save();
        }
        $params = array(
            'module' => 'gc_Contracts',
            'action' => 'DetailView',
            'record' => $contract_id,
        );
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params : ' . print_r($params, true), 'To update sales represantative in detail view');
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }

    public function action_requestBodyXML()
	{
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'seq : ' . $_REQUEST['seq'], 'Method generate XML file');
    	$seq = isset($_REQUEST['seq']) ? $_REQUEST['seq'] : '';
    	$file_name = "requestbody".$seq.".xml";
    	header('Content-type: text/xml');
    	header('Content-Disposition: attachment; filename="'.$file_name.'"');	$dbactions = new DBActions();
		$table_name = isset($_REQUEST['interface']) ? $_REQUEST['interface'] : '';
		$xml_contents = $dbactions->getXMLContent($table_name,$seq, "request_body");
		echo $xml_contents[0];
		exit();
	}

	public function action_responseBodyXML()
	{
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'seq : ' . $_REQUEST['seq'], 'Method generate XML file');
    	$seq = isset($_REQUEST['seq']) ? $_REQUEST['seq'] : '';
    	$table_name = isset($_REQUEST['interface']) ? $_REQUEST['interface'] : '';
    	$dbactions = new DBActions();
    	$content = $dbactions->getXMLContent($table_name, $seq, "response_body");
    	if ($table_name == "export-csv-for-gbs" && $content[1] == "200") {
    		$file_name = "responsecsv".$seq.".csv";
    		header("Content-type: text/csv");
    		header("Content-Disposition: attachment; filename=".$file_name);
    		header("Pragma: no-cache");
    		header("Expires: 0");
    		echo $content[0];
    		exit();
    	} else {
    		$file_name = "responsebody".$seq.".xml";
		    header('Content-type: text/xml');
		    header('Content-Disposition: attachment; filename="'.$file_name.'"');
		    echo $content[0];
		    exit();
    	}
	}
}
