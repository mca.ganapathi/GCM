<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

require_once ('custom/include/MVC/View/views/view.list.php');
include_once ('custom/modules/gc_Contracts/include/ListViewProductOfferingList.php');
include_once ('custom/modules/gc_Contracts/include/ClassGcContracts.php');

class Gc_ContractsViewList extends CustomViewList
{

    public function preDisplay()
    {
    	echo '<script type="text/javascript" src="custom/modules/gc_Contracts/include/javascript/ListView.js"></script>';
    	echo '<script type="text/javascript" src="cache/include/javascript/sugar_grp_yui_widgets.js"></script>';
        parent::preDisplay();

        // Hide Mass Update action
        $this->lv->showMassupdateFields = false;

        // Hide Merge action
        $this->lv->mergeduplicates = false;

        // Hide Add To Target List action
        $this->lv->targetList = false;

        // Hide Quick Edit Pencil
        $this->lv->quickViewLinks = false;

        // Hide Export action
        $this->lv->export = false;

        // Hide Mail action
        $this->lv->email = false;

        // Hide Delete action
        $this->lv->delete = false;

        /**
         * Custom code to display product offering name in list.
         *
         * @author : Dinesh.Itkar
         * @since : 10-June-2016
         */
        global $app_list_strings;
        $obj_gc_contracts = new ClassGcContracts();
        $product_offering_list = $obj_gc_contracts->getProductOfferings(false);
        $product_offering_list = is_array($product_offering_list)?$product_offering_list:array();
        $app_list_strings['product_offering_list'] = $product_offering_list;
    }

    function processSearchForm()
    {
        parent::processSearchForm();

        /**
         * Custom code to make the product_offering select wider for both Basic and Advanced panels.
         *
         * @author : Dinesh.Itkar
         * @since : 06-May-2016
         */
        if ((isset($_REQUEST['search_form_view'])) and ($_REQUEST['search_form_view'] = 'advanced_search')) {
            $field = 'product_offering_advanced[]';
        } else {
            $field = 'product_offering_basic[]';
        }
        echo '<script type="text/javascript">
               var j = document.getElementsByName("' . $field . '");
               if(j.length > 0)
                  j[0].setAttribute("style","width: 250px !important");
            </script>';
    }
}