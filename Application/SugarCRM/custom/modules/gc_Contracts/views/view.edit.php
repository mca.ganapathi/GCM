<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (â€œMSAâ€�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
include_once ('custom/modules/gc_Contracts/include/ClassGcContracts.php');
include_once ('custom/modules/gc_TimeZone/include/ClassGcTimeZone.php');

class Gc_ContractsViewEdit extends ViewEdit
{

    // extended module class object
    private $obj_gc_contracts;

    // array product offerings list
    private $list_product_offerings;

    // list of status to enable to change button in edit upgraded version
    private $available_change_status = array(
                                            0,
                                            3,
                                            1
    );
    // Draft,Sent For Approval, Approved

    // Line Item Status to display the "Change" button while upgrading contract
    private $li_status_to_disp_change = 'Activated';

    private $li_status_to_disp_terminate = 'Service_Termination';

    private $li_change_status = 'Change';
	
	private $modUtils;

    function __construct()
    {
        parent::__construct();
        $this->obj_gc_contracts = new ClassGcContracts();
		$this->modUtils = new ModUtils();
    }

    function preDisplay()
    {
        // call parent preDisplay method
        // populate product offerings list
        $this->list_product_offerings = $this->obj_gc_contracts->getProductOfferings(true);

        // Only show workflow flag yes product offering for create contract page
        if (empty($this->bean->id) && is_array($this->list_product_offerings) && (count($this->list_product_offerings) > 0)) {
            $list_product_offerings_new = array();
            global $sugar_config;
            foreach ($this->list_product_offerings as $key => $val) {
                if (isset($sugar_config['workflow_flag'][$key]) && strtolower($sugar_config['workflow_flag'][$key]) == 'yes')
                    $list_product_offerings_new[$key] = $val;
            }
            $this->list_product_offerings = $list_product_offerings_new;
        }

        parent::preDisplay();
    }

    function display()
    {
        global $current_user, $app_list_strings, $sugar_config;
        $version_upgrade = false;
        $version_up = isset($_REQUEST['VerUp']) ? $_REQUEST['VerUp'] : '';
        // Hide edit button for regular user when product offering workflow is no at activated state of contract even user have edit role
        if (!$GLOBALS['current_user']->is_admin && isset($this->bean->contract_status) && $this->bean->contract_status == '4' && empty($version_up) && isset($sugar_config['enterprise_mail_po']) && isset($this->bean->product_offering)) {
        	if (in_array($this->bean->product_offering, $sugar_config['enterprise_mail_po'])) {
        		$this->restrictCreateAction();
        		return;
        	}
        }
        // Remove view change log button for new record.
        if (empty($this->bean->id)) {
            unset($this->ev->defs['templateMeta']['form']['buttons']['CUSTOM_CHANGE_LOG']['customCode']); // Hide view change log button
        }

        $contract_start_date = isset($this->bean->fetched_row['contract_startdate'])?$this->modUtils->getUserDate($this->bean->fetched_row['contract_startdate']):'';
        $contract_end_date = isset($this->bean->fetched_row['contract_enddate'])?$this->modUtils->getUserDate($this->bean->fetched_row['contract_enddate']):'';
        $this->ss->assign('FIELD_CONTRACT_STARTDATE_VAL', $contract_start_date);
        $this->ss->assign('FIELD_CONTRACT_ENDDATE_VAL', $contract_end_date);

        if (isset($current_user->is_admin) && $current_user->is_admin)
            $this->ss->assign('IS_ADMIN_FLAG', 'true');

            // edit action is restricted for non admin user when status is Deactivated
        if (isset($current_user->is_admin) && ($current_user->is_admin != "1") && isset($this->bean->contract_status) && ($this->bean->contract_status == '2')) {
            $this->restrictCreateAction();
            return;
        }

        // These fields should be available for admin user in edit form only
        if (empty($this->bean->id) || (isset($current_user->is_admin) && $current_user->is_admin != "1")) {
            $this->unsetFieldsFromView('ext_sys_created_by');
            $this->unsetFieldsFromView('ext_sys_modified_by');
        }

        $show_terminate = true;
        if (isset($this->bean->contract_status) && $this->bean->contract_status == 4) {
            // Version upgrade coding
            if ($_REQUEST['VerUp'] > 0 && !empty($this->bean->id)) {
                $version_upgrade = true;
            }
        }
        // hide service_termination line item in contract_status Deactivated(2),Termination Approved(6),Sent for Termination Approval(5),Activated(4),Upgraded(7)
        if (isset($this->bean->contract_status) && in_array($this->bean->contract_status, array(
                                                        2,
                                                        4,
                                                        5,
                                                        6,
                                                        7
        ))) {
            $show_terminate = false;
        }

        $roles = $this->obj_gc_contracts->getUserRoleIds();
        $approver = $this->obj_gc_contracts->getRolePermissions(SETTING_KEY_ACE_CONTRACTS);
        $GLOBALS['log']->info('End: Gc_ContractsViewEdit->display() roles: '.print_r($roles,1));
        $GLOBALS['log']->info('End: Gc_ContractsViewEdit->display() approver: '.print_r($approver,1));
        $role_intersect = $this->obj_gc_contracts->arrayIntersectMulti($roles, $approver);
        
        $approver_user = (count($role_intersect) > 0 && !($GLOBALS['current_user']->is_admin)) ? "approver" : "";
        if (isset($this->bean->contract_status) && $this->bean->contract_status == '1') {
            $this->ss->assign('APPROVER_USER', $approver_user);
        }
        
        if (empty($version_up) && $this->bean->contract_status != 4) {
        	if (!$GLOBALS['current_user']->is_admin && $this->bean->contract_status != 0) {
        		if (!(!empty($approver_user) && ($this->bean->contract_status == '1' || $this->bean->contract_status == '6') && (isset($sugar_config['workflow_flag'][$this->bean->product_offering]) && (strtolower($sugar_config['workflow_flag'][$this->bean->product_offering]) == 'yes')) )) {
        			$this->restrictCreateAction();
        			return;
        		}
        	}
        }
       
        
        /**
         * ******** Assinging Values to TPL Variables for Sales representative panel ************
         */

        // in create view, unset unnecessary fields
        if (empty($this->bean->id)) {
            $this->unsetFieldsFromView('id');
            $this->unsetFieldsFromView('date_entered');
            $this->unsetFieldsFromView('date_modified');
        }

        $line_item_list = $this->obj_gc_contracts->getLineItemHeader($this->bean->id, 0, $this->bean->product_offering, $show_terminate);
        $arr_line_item_order = isset($line_item_list['order'])?$line_item_list['order']:array();
        ksort($arr_line_item_order);
        $arr_line_item_header_list = array();
        if(!empty($arr_line_item_order)){
            foreach ($arr_line_item_order as $key => $value_line_item_uuid) {
                $arr_line_item_header_list[$value_line_item_uuid] = isset($line_item_list[$value_line_item_uuid])?$line_item_list[$value_line_item_uuid]:array();
            }    
        }
        $line_item_list = $arr_line_item_header_list;
        unset($arr_line_item_header_list);
        if(!empty($line_item_list)){
            foreach ($line_item_list as $key => $value) {
                $line_item_list[$key]['service_start_date'] = isset($value['service_start_date'])?$this->modUtils->getUserDate($value['service_start_date']):'';
                $line_item_list[$key]['service_end_date'] = isset($value['service_end_date'])?$this->modUtils->getUserDate($value['service_end_date']):'';
                $line_item_list[$key]['billing_start_date'] = isset($value['billing_start_date'])?$this->modUtils->getUserDate($value['billing_start_date']):'';
                $line_item_list[$key]['billing_end_date'] = isset($value['billing_end_date'])?$this->modUtils->getUserDate($value['billing_end_date']):'';
            }    
        }
        
        $no_line_item = sizeof($line_item_list);
        $config_currencies = $this->obj_gc_contracts->getconfig_currencies();

        $this->ss->assign('LINEITEM_LIST', $line_item_list);
        $this->ss->assign('FIELD_LENGTH', $this->getFieldLengths());
        $this->ss->assign('PAYMENTTYPE', isset($GLOBALS['app_list_strings']['payment_type_list'])?$GLOBALS['app_list_strings']['payment_type_list']:'');
        $this->ss->assign('POSTALPASSBOOKDISPLAYLIST', isset($GLOBALS['app_list_strings']['postal_passbook_no_display_list'])?$GLOBALS['app_list_strings']['postal_passbook_no_display_list']:'');
        $this->ss->assign('ACCOUNTNODISPLAYLIST', isset($GLOBALS['app_list_strings']['account_no_display_list'])?$GLOBALS['app_list_strings']['account_no_display_list']:'');
        $this->ss->assign('DEPOSITTYPE', isset($GLOBALS['app_list_strings']['deposit_type_list'])?$GLOBALS['app_list_strings']['deposit_type_list']:'');
        $this->ss->assign('SIZEOFLINEITEM', $no_line_item);
        $this->ss->assign('LINEITEMTPL', 'custom/modules/gc_Contracts/tpl/LineItemEdit.tpl');
        //$this->ss->assign('INITFILTER', '&pm_products_pi_product_itempm_products_ida_advanced="+encodeURIComponent($("#pm_products_gc_contracts_1pm_products_ida").val())+"&source_module=' . $this->bean->module_name);

        $this->ss->assign('BILLAFFILIATEFILTER', '&billing_org_flag_advanced=1&source_module=' . $this->bean->module_name);
        $this->ss->assign('CUSTOMER_BILLING_METHODS', isset($GLOBALS['app_list_strings']['customer_billing_method_list'])?$GLOBALS['app_list_strings']['customer_billing_method_list']:'');
        $this->ss->assign('IGC_SETTLEMENT_METHODS', isset($GLOBALS['app_list_strings']['igc_settlement_method_list'])?$GLOBALS['app_list_strings']['igc_settlement_method_list']:'');
        $this->ss->assign('DEFAULT_CURRENCIES', $config_currencies);
        $this->ss->assign('CONTRACT_LINE_ITEM_TYPE', isset($GLOBALS['app_list_strings']['contract_line_item_type_list'])?$GLOBALS['app_list_strings']['contract_line_item_type_list']:'');
        

        $bean_id = (isset($this->bean->id) && !empty($this->bean->id)) ? $this->bean->id : null;
        $sale_rep_list = $this->obj_gc_contracts->getSalesRepresentativeList($bean_id);
        $sale_rep_list = is_array($sale_rep_list)?$sale_rep_list:array();
        // populate sales company list
        $sales_companies_list = $this->obj_gc_contracts->getSalesCompany('');
        $sales_companies_list = is_array($sales_companies_list)?$sales_companies_list:array();
        $this->ss->assign('SALES_COMPANIES_LIST', $sales_companies_list);

        if (!empty($sale_rep_list)) {
            $this->ss->assign('SALESREPRESENTATIVET_LIST', $sale_rep_list);
            $this->ss->assign('SALESREPRESENTATIVETPL', 'custom/modules/gc_Contracts/tpl/SalesRepresentativeEdit.tpl');
        } else {
            $this->ss->assign('SALESREPRESENTATIVETPL', 'custom/modules/gc_Contracts/tpl/SalesRepresentativeCreate.tpl');
        }

        // Assinging field length in Sales representative panel
        $sales_rep_bean = BeanFactory::getBean('gc_SalesRepresentative');
        $sales_rep_fields_def = $sales_rep_bean->getFieldDefinitions();

        /* BEGIN : Replace product offering textfield with dropdown */
        // prepare product offerings array for dropdown tpl
        if (isset($this->bean->product_offering) && !empty($this->bean->product_offering) && !array_key_exists($this->bean->product_offering, $this->list_product_offerings)) {
            $product_offering_details = $this->obj_gc_contracts->getProductOfferingDetails($this->bean->product_offering);
            if (isset($product_offering_details['respose_array']['offering_details'])) {
                $offering_id = $product_offering_details['respose_array']['offering_details']['offering_id'];
                $offering_name = isset($product_offering_details['respose_array']['offering_details']['offering_name'])?$product_offering_details['respose_array']['offering_details']['offering_name']:'';
                $this->list_product_offerings[$offering_id] = $offering_name;
                ksort($this->list_product_offerings);
            }
        }

        $app_list_strings['product_offering_list'][''] = '';
        if(!empty($this->list_product_offerings)){
            foreach ($this->list_product_offerings as $key => $val) {
                $app_list_strings['product_offering_list'][$key] = $val;
            }    
        }
        /* END : Replace product offering textfield with dropdown */

        /* BEGIN get group details related to the contract */
        $invoice_groups = $this->obj_gc_contracts->getInvoiceGroupByContract($this->bean->id);
        $res_igroups_arr = array();
        $i = 1;
        if(is_array($invoice_groups) && !empty($invoice_groups)){
            foreach ($invoice_groups as $invoice_group) {
                $elekey = 'grp_' . $i;
                $i++;
                $res_igroups_arr[$elekey]['uuid'] = $invoice_group['ig_id'];
                $res_igroups_arr[$elekey]['name'] = $invoice_group['ig_name'];
                $res_igroups_arr[$elekey]['desc'] = $invoice_group['ig_desc'];
            }    
        }
        $res_igroups_str = '';
        if (!empty($res_igroups_arr)) {
            $res_igroups_str = json_encode($res_igroups_arr);
        }
        /* END get group details related to the contract */

        /* BEGIN : Replace Contract Start Date Time Zone textfield with dropdown */
        $contract_account_id = isset($this->bean->contacts_gc_contracts_1contacts_ida) ? $this->bean->contacts_gc_contracts_1contacts_ida : "";
        $obj_company_accounts = BeanFactory::getBean('Contacts')->retrieve($contract_account_id);
        $arr_timezones = array();
        $c_country_id = isset($obj_company_accounts->c_country_id_c) ? $obj_company_accounts->c_country_id_c : '';
        $arr_timezones = $this->getFormattedTimeZoneList($c_country_id);
        $arr_timezones = is_array($arr_timezones) ? $arr_timezones : array();
        /* END : Replace Contract Start Date Time Zone textfield with dropdown */

        if (isset($version_upgrade) && $version_upgrade == true) {
            // $this->bean->id = '';
            $this->bean->contract_version = isset($this->bean->contract_version)?$this->bean->contract_version:1;
            $this->bean->contract_version = $this->bean->contract_version + 1;
            $this->ss->assign('VERSION_UPGRADE', 'true');
        }
		$company_accounts_id =  isset($obj_company_accounts->id) ? $obj_company_accounts->id : "";
        $this->ss->assign('SALES_REP_FIELDS_META', $sales_rep_fields_def);
        $this->ss->assign('LIST_INVOICE_GROUPS', $res_igroups_str);
        $this->ss->assign('CONTACTS_COUNTRY_ID_C', isset($obj_company_accounts->id)?$obj_company_accounts->id:'');
        $this->ss->assign('LIST_TIMEZONES', $arr_timezones);
        $this->ss->assign('ID_CONTRACT_START_DATE', 'gc_timezone_id_c');
        $this->ss->assign('ID_CONTRACT_END_DATE', 'gc_timezone_id1_c');
        $this->ss->assign('TZ_CUSTOMDROPDOWNTPL', 'custom/include/tpl/CustomDropDown.tpl');
        $this->ev->defs['templateMeta']['form']['hideAudit'] = true; // Hide View Change Log button

        // get list of all timezones and store json object into js variable
        $res_timezones = $this->getFormattedTimeZoneList('');
        $res_timezones = is_array($res_timezones)?$res_timezones:array();
        $this->ss->assign('LIST_TIME_ZONE_LIST', $res_timezones);

        // check if change buton should be displayed or not
        $is_change_available = $this->checkIsChangeAvailable();
        $is_versionedup_contract = $this->checkIsVersionedUpContract();

        $this->ss->assign('IS_CHANGE_AVAILABLE', $is_change_available);
        $this->ss->assign('LI_STATUS_TO_DISP_CHANGE', $this->li_status_to_disp_change);
        $this->ss->assign('LI_STATUS_TO_DISP_TERMINATE', $this->li_status_to_disp_terminate);

        // echo $_REQUEST['VerUp']; exit;
        $json_timezones_list = json_encode($res_timezones);
        echo <<<EOQ
<script> var json_timezone_list = '$json_timezones_list'; var is_change_available = '$is_change_available';
var li_status_to_disp_change = '$this->li_status_to_disp_change';
var li_status_to_disp_terminate = '$this->li_status_to_disp_terminate';
var li_change_status = '$this->li_change_status';
var is_versionedup_contract = '$is_versionedup_contract';</script>
EOQ;
        parent::display();
        $GLOBALS['log']->info('End: Gc_ContractsViewEdit->display()');
    }
    
    /**
     * method to fetch timezones with UTC Timezone displayed as the first option in Dropdown
     *
     * @param string $country_id object id of country module
     * @return array list of timezone
     * @author sagar.salunkhe
     * @since 06-May-2016
     */
    private function getFormattedTimeZoneList($country_id)
    {
        global $sugar_config;
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'country_id : ' . $country_id, 'method to fetch timezones with UTC Timezone displayed as the first option in Dropdown');
        $arr_timezones = array();
        $default_timezones = array();
        $timezones = array();
        $gc_timeZone_obj  = new ClassGcTimeZone();
        $res_timezones = $gc_timeZone_obj->getTimeZoneList($country_id);
        $cstm_default_timezone = (isset($sugar_config['cstm_default_timezone']) && is_array($sugar_config['cstm_default_timezone']))?$sugar_config['cstm_default_timezone']:array();
        if($res_timezones !=null && !empty($res_timezones)){
            foreach ($res_timezones as $i => $obj_timezone) {
                if(!isset($obj_timezone->id)){
                    continue;
                }

                if (isset($obj_timezone->id) && in_array($obj_timezone->id, $cstm_default_timezone)) {
                    $tz_name = isset($obj_timezone->name)?$obj_timezone->name:'';
                    $tz_offset = isset($obj_timezone->utcoffset)?$obj_timezone->utcoffset:'';
                    $default_timezones[$obj_timezone->id] = array(
                                                                'name' => $tz_name . ' (' . $tz_offset . ')',
                                                                'label' => $tz_name . ' (' . $tz_offset . ')',
                                                                'code' => isset($obj_timezone->c_country_id_c)?$obj_timezone->c_country_id_c:''
                    );
                } else {
                    $tz_name = isset($obj_timezone->name)?$obj_timezone->name:'';
                    $tz_offset = isset($obj_timezone->utcoffset)?$obj_timezone->utcoffset:'';
                    $arr_timezones[$obj_timezone->id] = array(
                                                            'name' => $obj_timezone->name . ' (' . $obj_timezone->utcoffset . ')',
                                                            'label' => $obj_timezone->name . ' (' . $obj_timezone->utcoffset . ')',
                                                            'code' => isset($obj_timezone->c_country_id_c)?$obj_timezone->c_country_id_c:''
                    );
                }
            }
        }
        asort($default_timezones);
        asort($arr_timezones);
        $timezones = array_merge($default_timezones, $arr_timezones);
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'timezones : ' . print_r($timezones, true), 'method to fetch timezones with UTC Timezone displayed as the first option in Dropdown');
        return $timezones;
    }

    /**
     * Method to get field lengths from modules
     *
     * @return array list of fields with lenght from module
     */
    public function getFieldLengths()
    {
    	$data = '';
        $mod = array(
                    'gc_Line_Item_Contract_History',
                    'gc_AccountTransfer',
                    'gc_CreditCard',
                    'gc_DirectDeposit',
                    'gc_PostalTransfer'
        );
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'mod : ' . print_r($mod, true), 'Method to get field lengths from modules');
        foreach ($mod as $m) {
            $m_bean = BeanFactory::getBean($m);
            $m_bean_def = $m_bean->getFieldDefinitions();
            if(!empty($m_bean_def)){
                foreach ($m_bean_def as $key => $value) {
                    $data[$m][$key]['len'] = isset($value['len']) ? $value['len'] : '';
                }    
            }
        }
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'data : ' . print_r($data, true), 'Method to get field lengths from modules');
        return $data;
    }

    /**
     * method to unset array node from given array
     *
     * @author Sagar.Salunkhe
     * @param string $field_name element name
     * @since 06-Jan-2016
     */
    private function unsetFieldsFromView($field_name)
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'field_name : ' . print_r($field_name, true), 'method to unset array node from given array');
        $arr_element = $this->modUtils->searchArrayElement($this->ev->defs['panels'], 'value', $field_name);

        if (isset($arr_element['path'])) {

            // if node has name element (refer sugar defs structure), unset the parent array
            if (!is_numeric($arr_element['path'][(count($arr_element['path'])) - 1])) {
                unset($arr_element['path'][(count($arr_element['path'])) - 1]);
            }

            $this->modUtils->unsetArrayNode($arr_element['path'], $this->ev->defs['panels']);
        }
    }

    /**
     * restrict create action on Contracts EditView
     *
     * @author Sagar Salunkhe
     * @since 08-Jan-2016
     */
    private function restrictCreateAction()
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'restrict create action on Contracts EditView');
    	unset($this->ev->defs['templateMeta']['form']['buttons']['CUSTOM_CHANGE_LOG']['customCode']);
        // display error message
        $display_txt = '<br><div id="div_related_values"><div><span style="color:red;">Action Prohibited: You are restricted to Edit this Record from Sugar Application.</span></h4></div><div><br>';
        echo $display_txt;
        echo $this->ev->display($this->showTitle);
        echo '<script>$("#SAVE_HEADER").hide(); $( ".action_buttons" ).last().css( "display", "none" ); $(".moduleTitle").hide(); $("#CANCEL_HEADER").val("Back");$("#EditView_tabs,#detailpanel_30,#btn_view_change_log").hide();</script>';
    }

    /**
     * To check if the change button should exist or not
     * If the contract vestino is greater than one and if the contract status is in 'draft' or 'sent for approval' or 'approved'
     * the button will display
     *
     * @return boolean
     * @author Mohamed.Siddiq
     * @since 06-Jul-2016
     */
    private function checkIsChangeAvailable()
    {
        $verup = isset($_REQUEST['VerUp'])?$_REQUEST['VerUp']:'';
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'VerUp : ' . $verup . GCM_GL_LOG_VAR_SEPARATOR . 'contract_version : ' . $this->bean->contract_version . GCM_GL_LOG_VAR_SEPARATOR . 'contract_status : ' . $this->bean->contract_status . GCM_GL_LOG_VAR_SEPARATOR . 'available_change_status : ' . print_r($this->available_change_status, true), 'To check if the change button should exist or not');
        $return  = false;
        if((isset($_REQUEST['VerUp']) && $_REQUEST['VerUp'] >= 1 ) || (isset($this->bean->contract_version) && floatval($this->bean->contract_version) > 1 && isset($this->bean->contract_status) &&  in_array($this->bean->contract_status,$this->available_change_status))){
            $return = true;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return : ' . $return, 'To check if the change button should exist or not');
        return $return;
    }

    /**
     * To check if the contract is upgraded contract or base contract
     *
     * @return boolean
     * @author Mohamed.Siddiq
     * @since 12-Jul-2016
     */
    private function checkIsVersionedUpContract()
    {
    	ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_version : ' . $this->bean->contract_version, 'To check if the contract is upgraded contract or base contract');
        return (isset($this->bean->contract_version) && floatval($this->bean->contract_version) > 1);
    }
	
	 /**
     * To searialize array
     *
     * @return array
     * @author Kamal.Thiyagarajan
     * @since 12-Jan-2017
     */
	private function serialize_array_values($arr)
	{
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr : ' . print_r($arr, true), 'To searialize array');
		foreach ($arr as $key=>$val) {
			if (is_array($val))
				sort($val);
		
			$arr[$key]=serialize($val);
		}
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr : ' . print_r($arr, true), 'To searialize array');
		return $arr;
    }

}
