<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (?MSA?), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

include_once 'custom/modules/gc_Contracts/include/ClassGcContracts.php';

class Gc_ContractsViewDetail extends ViewDetail
{
    // extended module class object
    private $obj_gc_contracts;

    // array product offerings list
    private $list_product_offerings;

    // Contrat approver list
    private $approver;

    private $modUtils;

    /**
     * string no account name
     */
    const STR_NO_ACCOUNT_NAME = 'Click Here';

    /**
     * string no available
     */
    const STR_NO_AVAILABLE = 'Not Available';

    /**
     * string data not found in CIDAS
     */
    const STR_NO_DATA_CIDAS = 'Data Not Found in CIDAS';

    public function __construct()
    {
        parent::__construct();
        $this->obj_gc_contracts = new ClassGcContracts();
        $this->modUtils = new ModUtils();
    }

    public function preDisplay()
    {
        global $app_list_strings, $sugar_config;

        // DF-1398 New workflow for Enterprise Mail -- fetch workflow settings conditionally on basis of product offering
        if (in_array($this->bean->product_offering, $sugar_config['enterprise_mail_po'])) {
            $this->approver = $this->obj_gc_contracts->getRolePermissions('AccessControlExt_EnterpriseMail');
        } else {
            $this->approver = $this->obj_gc_contracts->getRolePermissions(SETTING_KEY_ACE_CONTRACTS);
        }

        // Fetching the accounts details
        $account_details = new Account();
        $account_details->retrieve_by_string_fields(array('id' => $this->bean->accounts_gc_contracts_1accounts_ida));
        $this->bean->accounts_gc_contracts_1_name = $account_details->name;

        $array_corpname_filter = array('', SELF::STR_NO_AVAILABLE, SELF::STR_NO_DATA_CIDAS);
        $corp_name_search = array_search(strtolower($this->bean->accounts_gc_contracts_1_name), array_map('strtolower', $array_corpname_filter));

        if ((empty($account_details->name) && !strlen($account_details->name)) && (!empty($account_details->name_2_c) || strlen(trim($account_details->name_2_c)))) {
            $this->bean->accounts_gc_contracts_1_name = $account_details->name_2_c;
        } elseif ((empty($account_details->name) && !strlen($account_details->name)) && $corp_name_search !== false) {
            $this->bean->accounts_gc_contracts_1_name = SELF::STR_NO_ACCOUNT_NAME;
        }

        // If Latin name is not present, display local name instead for Contract Account - Sagar : 26-10-2015
        if (empty($this->bean->contacts_gc_contracts_1_name)) {
            $obj_accounts_bean = BeanFactory::getBean('Contacts');
            $obj_accounts_bean->retrieve($this->bean->contacts_gc_contracts_1contacts_ida);
            if(strlen($obj_accounts_bean->name) == 0)
                $this->bean->contacts_gc_contracts_1_name = $obj_accounts_bean->name_2_c;  // replace name with local name
            else
                $this->bean->contacts_gc_contracts_1_name = $obj_accounts_bean->name;
        }

        $this->bean->contract_startdate = $this->modUtils->getUserDate($this->bean->fetched_row['contract_startdate']);
        $this->bean->contract_enddate = $this->modUtils->getUserDate($this->bean->fetched_row['contract_enddate']);

        $app_list_strings['product_offering_list'] = array();
        // populate product offerings display value instead of ID
        if (!empty($this->bean->product_offering)) {
            $app_list_strings['product_offering_list'] = $this->obj_gc_contracts->getProductOfferings(false);
        }
        // call parent preDisplay method
        parent::preDisplay();
    }

    public function display()
    {
        global $layout_defs, $sugar_config;
        $roles = $this->obj_gc_contracts->getUserRoleIds();
        $workflow_flag = $this->bean->workflow_flag;
        $workflow_access = $this->workflowAcess($roles, $workflow_flag);
        $show_terminate = true;
        $role_intersect = $this->obj_gc_contracts->arrayIntersectMulti($roles, $this->approver);

        // hide modification and termination button at activated status if product offering workflow is not yes
        $is_visible_modification_termination_btn_at_activated_status = false;
        if (($this->bean->contract_status == "4") && isset($this->bean->product_offering) && $this->bean->product_offering > 0 && isset($sugar_config['workflow_flag'][$this->bean->product_offering]) && strtolower($sugar_config['workflow_flag'][$this->bean->product_offering]) == 'yes') {
            $is_visible_modification_termination_btn_at_activated_status = true;
        }

        $obj_sales_representative_bean = BeanFactory::getBean('gc_SalesRepresentative');
        $sales_rep_edit = "";
        // Hide create/edit sales representative option
        if ($GLOBALS['current_user']->is_admin != '1' && !$obj_sales_representative_bean->aclAccess('edit')) {
            $sales_rep_edit = "display:none;";
        }

        // hide service_termination line item in contract_status Deactivated(2),Termination Approved(6),Sent for Termination Approval(5),Activated(4),Upgraded(7)
        if (in_array($this->bean->contract_status, array(2, 4, 5, 6, 7))) {
            $show_terminate = false;
        }

        // if contract_status is Draft(0),Approved(1),Deactivated(2),Sent for Approval(3),Upgraded(7)
        if (in_array($this->bean->contract_status, array(0, 1, 2, 3, 7))) {
            unset($this->dv->defs['templateMeta']['form']['buttons'][15]); // Hide SendTerminationApproval button
            unset($this->dv->defs['templateMeta']['form']['buttons'][12]); // Hide termination_approval button
            unset($this->dv->defs['templateMeta']['form']['buttons'][13]); // Hide RejectTerminationApproval button
            unset($this->dv->defs['templateMeta']['form']['buttons'][14]); // Hide Deactivate button
        } elseif(in_array($this->bean->product_offering, $sugar_config['enterprise_mail_po']) && (count($role_intersect) > 0 || $GLOBALS['current_user']->is_admin == 1)) {
            unset($this->dv->defs['templateMeta']['form']['buttons'][15]); // Hide SendTerminationApproval button
            unset($this->dv->defs['templateMeta']['form']['buttons'][12]); // Hide termination_approval button
            unset($this->dv->defs['templateMeta']['form']['buttons'][13]); // Hide RejectTerminationApproval button
        } elseif($GLOBALS['current_user']->is_admin != '1' && $workflow_access == false) {
            unset($this->dv->defs['templateMeta']['form']['buttons'][15]); // Hide SendTerminationApproval button
            unset($this->dv->defs['templateMeta']['form']['buttons'][12]); // Hide termination_approval button
            unset($this->dv->defs['templateMeta']['form']['buttons'][13]); // Hide RejectTerminationApproval button
            unset($this->dv->defs['templateMeta']['form']['buttons'][14]); // Hide Deactivate button
        }

        $CheckWrokflowFlag = 0;
        $CheckWrokflowFlag = $this->checkProductType($this->bean->fetched_row['product_offering']);
        if (($CheckWrokflowFlag > 0) || $this->bean->contract_status != 3 || (!ACLController::checkAccess($this->bean->object_name, 'edit', true))) {
            if (($CheckWrokflowFlag > 0) || ($GLOBALS['current_user']->is_admin != '1' && count($role_intersect) == 0) || (in_array($this->bean->workflow_flag, array(0, 3, 6, 7, 4, 5, 8)) || (!ACLController::checkAccess($this->bean->object_name, 'edit', true)))) {
                unset($this->dv->defs['templateMeta']['form']['buttons'][16]); // Hide Withdraw Approval Request button
            }
        }

        if (($CheckWrokflowFlag > 0) || ($this->checkProductType($this->bean->fetched_row['product_offering']) > 0) || ($this->bean->contract_status != 5 || !ACLController::checkAccess($this->bean->object_name, 'edit', true))) {

            if (($CheckWrokflowFlag > 0) || ($GLOBALS['current_user']->is_admin != '1' && count($role_intersect) == 0) || (in_array($this->bean->workflow_flag, array(0, 1, 2, 3, 4, 8)) || (!ACLController::checkAccess($this->bean->object_name, 'edit', true)))) {
                unset($this->dv->defs['templateMeta']['form']['buttons'][17]); // Hide Termination Withdraw Approval Request button
            }
        }

        // if contract_status is Termination Approved(6)
        if ($this->bean->contract_status == '6' && $this->bean->workflow_flag == '8') {
            unset($this->dv->defs['templateMeta']['form']['buttons'][15]); // Hide SendTerminationApproval button
            unset($this->dv->defs['templateMeta']['form']['buttons'][12]); // Hide termination_approval button
            unset($this->dv->defs['templateMeta']['form']['buttons'][13]); // Hide RejectTerminationApproval button
            if ($GLOBALS['current_user']->is_admin != '1' && count($role_intersect) == 0) {
                unset($this->dv->defs['templateMeta']['form']['buttons'][14]); // Hide Deactivate button
            }
        } elseif (in_array($this->bean->contract_status, array(4, 5))) { //if contract_status is Sent for Termination Approval(5),Activated(4)
            switch ($workflow_flag) {
                case "5":
                    {
                        unset($this->dv->defs['templateMeta']['form']['buttons'][15]); // Hide SendTerminationApproval button
                        unset($this->dv->defs['templateMeta']['form']['buttons'][14]); // Hide Deactivate button
                        if ($GLOBALS['current_user']->is_admin != '1' && count($role_intersect) == 0) {
                            unset($this->dv->defs['templateMeta']['form']['buttons'][12]); // Hide termination_approval button
                            unset($this->dv->defs['templateMeta']['form']['buttons'][13]); // Hide RejectTerminationApproval button
                        }
                        break;
                    }
                case "6":
                case "7":
                    {
                        unset($this->dv->defs['templateMeta']['form']['buttons'][15]); // Hide SendTerminationApproval button
                        unset($this->dv->defs['templateMeta']['form']['buttons'][14]); // Hide Deactivate button
                        break;
                    }
                default:
                    {

                        unset($this->dv->defs['templateMeta']['form']['buttons'][12]); // Hide termination_approval button
                        unset($this->dv->defs['templateMeta']['form']['buttons'][13]); // Hide RejectTerminationApproval button

                        if ($this->bean->contract_status == '5' && $workflow_flag != 0 || !in_array($this->bean->product_offering, $sugar_config['enterprise_mail_po']))
                            unset($this->dv->defs['templateMeta']['form']['buttons'][14]); // Hide Deactivate button

                        if (!(ACLController::checkAccess($this->bean->object_name, 'edit', true) && ($is_visible_modification_termination_btn_at_activated_status))) {
                            unset($this->dv->defs['templateMeta']['form']['buttons'][15]); // Hide SendTerminationApproval button
                        }
                    }
            }
        }

        // No restrication to admin to edit contract on any stage of workflow(DF-373)
        if (!$GLOBALS['current_user']->is_admin) {
            // approver can edit approved contract(DF-373)
            if (!(count($role_intersect) != 0 && ($this->bean->contract_status == '1' || $this->bean->contract_status == '6'))) {
                // Check contract status deactivate
                if ($this->bean->contract_status == '2' || $this->bean->workflow_flag != '0') {
                    unset($this->dv->defs['templateMeta']['form']['buttons'][0]); // Hide Edit button
                }
            }

            // Hide edit button for approver when product offering workflow is no
            if (($this->bean->contract_status == '1') && ($this->bean->aclAccess('edit'))) {
                global $sugar_config;
                if (!(isset($sugar_config['workflow_flag'][$this->bean->product_offering]) && (strtolower($sugar_config['workflow_flag'][$this->bean->product_offering]) == 'yes'))) {
                    unset($this->dv->defs['templateMeta']['form']['buttons'][0]); // Hide Edit button
                    unset($this->dv->defs['templateMeta']['form']['buttons'][10]); // Hide Activate button
                }
            }

            // Hide edit button for regular user when product offering workflow is no at activated state of contract even user have edit role
            if (($this->bean->contract_status == '4' || $this->bean->contract_status == '7') && ($this->bean->aclAccess('edit'))) {
                global $sugar_config;
                if (!(isset($sugar_config['workflow_flag'][$this->bean->product_offering]) && (strtolower($sugar_config['workflow_flag'][$this->bean->product_offering]) == 'yes')) || in_array($this->bean->product_offering, $sugar_config['enterprise_mail_po'])) {
                    unset($this->dv->defs['templateMeta']['form']['buttons'][0]); // Hide Edit button
                }
            }
        }

        // Admin and approver can activate contract at approve stage of contract.
        if (!((($this->bean->contract_status == '1' && $this->bean->workflow_flag == '4') || ($this->bean->contract_status == '0' && in_array($this->bean->product_offering, $sugar_config['enterprise_mail_po']))) && (($GLOBALS['current_user']->is_admin) || count($role_intersect) != 0) && ($this->bean->product_offering != ""))) {
            unset($this->dv->defs['templateMeta']['form']['buttons'][10]); // Hide Activate button
        }

        unset($this->dv->defs['templateMeta']['form']['buttons'][1]); // Hide Duplicate button
                                                                      // Delete in draft and admin can delete in any stage
        if (!($GLOBALS['current_user']->is_admin || $this->bean->contract_status == '0')) {
            unset($this->dv->defs['templateMeta']['form']['buttons'][2]); // Hide Delete button
        }

        if (!($this->bean->contract_status == '4' && $this->bean->aclAccess('edit') && !empty($this->bean->product_offering) && $is_visible_modification_termination_btn_at_activated_status)) {
            unset($this->dv->defs['templateMeta']['form']['buttons'][11]); // Hide Modification Button
        }

        unset($this->dv->defs['templateMeta']['form']['buttons'][3]); // Hide Find Duplicate button
        unset($this->dv->defs['templateMeta']['form']['buttons'][4]); // Hide Get Data button

        //DF-1398 : New workflow for Enterprise Mail (TC_DF-1398_003)
        if(in_array($this->bean->product_offering, $sugar_config['enterprise_mail_po']))
            unset($this->dv->defs['templateMeta']['form']['buttons'][8]); // Hide "View Approvals Log" button

        $this->dv->defs['templateMeta']['form']['hideAudit'] = true; // Hide View Change Log button

        /**
         * ******** Assinging Values to TPL Variables for Sales representative panel ************
         */
        $bean_id = (isset($this->bean->id) && !empty($this->bean->id)) ? $this->bean->id : null;
        $sale_rep_list = $this->obj_gc_contracts->getSalesRepresentativeList($bean_id);
        $sales_companies_list = $this->obj_gc_contracts->getSalesCompany('');
        $this->ss->assign('SALES_COMPANIES_LIST', $sales_companies_list);
        $this->ss->assign('SALESREPRESENTATIVE_FRESH_LIST', $sale_rep_list);
        $sales_rep_bean = BeanFactory::getBean('gc_SalesRepresentative');
        $sales_rep_fields_def = $sales_rep_bean->getFieldDefinitions();
        $this->ss->assign('SALES_REP_FIELDS_META', $sales_rep_fields_def);
        $diff_team = "";
        $contract_startdate_timezone_changed = $contract_enddate_timezone_changed = $contract_type_changed = $description_changed = $loi_changed = $contract_scheme_changed = $billing_type_changed = $contract_workflow_flag_changed = 0;
        $contract_version = intval($this->bean->contract_version);
        if ($contract_version > 1 && in_array($this->bean->contract_status, array(0, 3, 1))) {
            // team
            require_once 'modules/Teams/TeamSet.php';
            $teamSetBean = new TeamSet();
            $teams = $teamSetBean->getTeams($this->bean->team_set_id);
            $new_teams = array();
            foreach ($teams as $team) {
                $new_teams[] = $team->name;
            }
            require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';
            $product_config_bean = BeanFactory::getBean('gc_ProductConfiguration');
            $product_price_bean = BeanFactory::getBean('gc_ProductPrice');
            $config_currencies = $this->obj_gc_contracts->getconfig_currencies();
            $product_config = new ClassProductConfiguration();
            if (!empty($this->bean->product_offering)) {
                $off_details = $product_config->getProductOfferingDetails($this->bean->product_offering);
                $sp_details = $off_details['respose_array']['specification_details'];
                $pcharge_details = $off_details['respose_array']['charge_details'];
            } else {
                $off_details = $sp_details = $pcharge_details = array();
            }
            unset($off_details);
            unset($product_config);

            $global_contract_id = $this->bean->global_contract_id;
            $contr_version = $contract_version - 1;
            $contr_version = ($contr_version > 1) ? $contr_version : 1;
            $where = " global_contract_id = '" . $global_contract_id . "' AND contract_version = '" . $contr_version . "' ";
            $contract_obj = BeanFactory::getBean('gc_Contracts');
            $result_beans = $contract_obj->get_full_list('', $where);
            if (!empty($result_beans) && is_array($result_beans) && isset($result_beans[0]->id) && $result_beans[0]->id != '') {
                $contract_obj->retrieve($result_beans[0]->id);
                $teams = $teamSetBean->getTeams($contract_obj->team_set_id);
                $old_teams = array();
                foreach ($teams as $team) {
                    $old_teams[] = $team->name;
                }
                $result = array_diff($new_teams, $old_teams);

                if (count($result)) {
                    $diff_team = implode(",", $result);
                }
                $contract_obj->contract_startdate = $this->modUtils->getUserDate($contract_obj->fetched_row['contract_startdate']);
                $contract_obj->contract_enddate = $this->modUtils->getUserDate($contract_obj->fetched_row['contract_enddate']);
                $header_fields = array("factory_reference_no", "date_entered", "date_modified", "contract_startdate", "contract_enddate", "product_offering", "pm_products_gc_contracts_1_name", "accounts_gc_contracts_1_name", "contacts_gc_contracts_1_name", "team_name", "created_by_name", "modified_by_name", "vat_no");
                foreach ($header_fields as $field) {
                    if (trim($contract_obj->$field) != trim($this->bean->$field)) {
                        $this->bean->$field = "<span class='upgrade-change'>" . $this->bean->$field . "</span>";
                    }
                }
                if (trim($contract_obj->name) != trim($this->bean->name)) {
                    $this->bean->name = '<span class="name-change">' . $this->bean->name . '</span>';
                }

                if (trim($contract_obj->description) != trim($this->bean->description)) {
                    $description_changed = 1;
                }

                if ($contract_obj->loi_contract != $this->bean->loi_contract) {
                    $loi_changed = 1;
                }

                if ($contract_obj->contract_type != $this->bean->contract_type) {
                    $contract_type_changed = 1;
                }

                if ($contract_obj->contract_scheme != $this->bean->contract_scheme) {
                    $contract_scheme_changed = 1;
                }

                if ($contract_obj->billing_type != $this->bean->billing_type) {
                    $billing_type_changed = 1;
                }

                if ($contract_obj->contract_startdate_timezone != $this->bean->contract_startdate_timezone) {
                    $contract_startdate_timezone_changed = 1;
                }

                if ($contract_obj->contract_enddate_timezone != $this->bean->contract_enddate_timezone) {
                    $contract_enddate_timezone_changed = 1;
                }

                if ($contract_obj->workflow_flag != $this->bean->workflow_flag) {
                	$contract_workflow_flag_changed = 1;
                }

                $contract_bean_id = (isset($contract_obj->id) && !empty($contract_obj->id)) ? $contract_obj->id : null;
                $prev_sale_rep_list = $this->obj_gc_contracts->getSalesRepresentativeList($contract_bean_id);
                if ($sale_rep_list != null) {
                    $sale_rep_values = array_values($sale_rep_list);
                    $sales_rep = $prev_sale_rep = array();
                    if (isset($sale_rep_values[0])) {
                        $sales_rep = $sale_rep_values[0];
                    }

                    $prev_sale_rep_values = array_values($prev_sale_rep_list);
                    if (isset($prev_sale_rep_values[0])) {
                        $prev_sale_rep = $prev_sale_rep_values[0];
                    }

                    $sales_rep_diff = array_diff($sales_rep, $prev_sale_rep);
                    $array_keys = array_keys($sale_rep_list);
                    if (isset($array_keys[0]) && count($sales_rep_diff) > 0) {
                        foreach ($sales_rep_diff as $key => $value) {
                            if ($key == "sales_channel_code") {
                                $sale_rep_list[$array_keys[0]][$key] = "<span class='color-change'>" . $value . "</span>";
                            } else if ($key != "sales_company_id" && $key != "id") {
                                $sale_rep_list[$array_keys[0]][$key] = "<span class='upgrade-change'>" . $value . "</span>";
                            }
                        }
                    }
                }
            }
        }

        $this->ss->assign('SALESREPRESENTATIVEEDIT', $sales_rep_edit);
        $this->ss->assign('TEAM_DIFFERENCE', $diff_team);
        $this->ss->assign('DESCRIPTION_CHANGED', $description_changed);
        $this->ss->assign('LOI_CHANGED', $loi_changed);
        $this->ss->assign('CONTRACT_TYPE_CHANGED', $contract_type_changed);
        $this->ss->assign('CONTRACT_SCHEME_CHANGED', $contract_scheme_changed);
        $this->ss->assign('BILLING_TYPE_CHANGED', $billing_type_changed);
        $this->ss->assign('CONTRACT_STARTDATE_TIMEZONE_CHANGED', $contract_startdate_timezone_changed);
        $this->ss->assign('CONTRACT_ENDDATE_TIMEZONE_CHANGED', $contract_enddate_timezone_changed);
        $this->ss->assign('CONTRACT_WORKFLOW_FLAG_CHANGED', $contract_workflow_flag_changed);
        $this->ss->assign('SALESREPRESENTATIVET_LIST', $sale_rep_list);
        $this->ss->assign('SALESREPRESENTATIVETPL', 'custom/modules/gc_Contracts/tpl/SalesRepresentative.tpl');

        /**
         * ******** Assinging Values to TPL Variables for Contract Line Item panel ************
         */
        $line_item_header_list = $this->obj_gc_contracts->getLineItemHeader($this->bean->id, 0, $this->bean->fetched_row['product_offering'], $show_terminate);
        $arr_line_item_order = !empty($line_item_header_list['order']) ? $line_item_header_list['order'] : array();
        ksort($arr_line_item_order);

        $arr_line_item_header_list = array();
        if (is_array($arr_line_item_order) && count($arr_line_item_order) > 0) {
            foreach ($arr_line_item_order as $key => $value_line_item_uuid) {
                $arr_line_item_header_list[$value_line_item_uuid] = $line_item_header_list[$value_line_item_uuid];
                // get lineitem type name
                if (isset($GLOBALS['app_list_strings']['contract_line_item_type_list'][$arr_line_item_header_list[$value_line_item_uuid]['contract_line_item_type']]) && $GLOBALS['app_list_strings']['contract_line_item_type_list'][$arr_line_item_header_list[$value_line_item_uuid]['contract_line_item_type']] != "") {
                    $arr_line_item_header_list[$value_line_item_uuid]['contract_line_item_type'] = $GLOBALS['app_list_strings']['contract_line_item_type_list'][$arr_line_item_header_list[$value_line_item_uuid]['contract_line_item_type']];
                }

                // Prepare Billing Affiliate details
                $obj_gc_organization = BeanFactory::getBean('gc_organization');
                $org_name_combined = '';
                if (!empty($arr_line_item_header_list[$value_line_item_uuid]['gc_organization_id_c'])) {
                    $organisation_arr = $obj_gc_organization->retrieve($arr_line_item_header_list[$value_line_item_uuid]['gc_organization_id_c']);
                    $org_name_1 = $organisation_arr->name;
                    $org_name_2 = $organisation_arr->organization_name_2_local;
                    $org_name_3 = $organisation_arr->organization_name_3_local;
                    $org_name_4 = $organisation_arr->organization_name_4_local;
                    $org_name_ar = array($org_name_1, $org_name_2, $org_name_3, $org_name_4);
                    $org_name_combined = implode(" ", $org_name_ar);
                }

                $arr_format_date_fields = array('service_start_date', 'service_end_date', 'billing_start_date', 'billing_end_date');

                foreach($arr_format_date_fields as $line_dt_field){
                    if (!empty($arr_line_item_header_list[$value_line_item_uuid][$line_dt_field])) {

                        $record_dt = $arr_line_item_header_list[$value_line_item_uuid][$line_dt_field];

                        if($this->modUtils->validateDbDateFormat($record_dt)){
                            $record_dt_arr = explode(" ", $record_dt);

                            $datetime = new datetime($record_dt);
                            $timedate = new TimeDate();
                            $format_dt = $timedate->asUser($datetime, $GLOBALS['current_user']);
                            $format_dt_arr = explode(" ", $format_dt);

                            $arr_line_item_header_list[$value_line_item_uuid][$line_dt_field] = $format_dt_arr[0].' '.$record_dt_arr[1];
                        }
                    }
                }

                // Add Billing Affiliate details value to tpl assigned array
                if (!empty($org_name_combined)) {
                    $arr_line_item_header_list[$value_line_item_uuid]['org_name_combined'] = $org_name_combined;
                }

                if (!empty($arr_line_item_header_list[$value_line_item_uuid]['line_item_id_uuid']) && isset($result_beans[0]->id) && $result_beans[0]->id != '' && !empty($result_beans) && is_array($result_beans) && $contract_version > 1 && in_array($this->bean->contract_status, array(0, 3, 1)) && $arr_line_item_header_list[$value_line_item_uuid]['contract_line_item_type'] == "Change") {
                    $where = "  gc_line_item_contract_history.line_item_id_uuid = '" . $arr_line_item_header_list[$value_line_item_uuid]['line_item_id_uuid'] . "' AND gc_line_item_contract_history.contracts_id = '" . $result_beans[0]->id . "' ";
                    $line_item_his_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
                    $line_item_his_bean_arr = $line_item_his_bean->get_full_list('', $where);
                    $line_item_details = $this->obj_gc_contracts->getLineItem($line_item_his_bean_arr[0], $product_config_bean, $product_price_bean, $config_currencies, $sp_details, $pcharge_details, $this->bean->product_offering);
                    $line_item_fields = array('global_contract_item_id', 'service_start_date', 'service_end_date', 'billing_start_date', 'billing_end_date', 'service_start_date_timezone', 'service_end_date_timezone', 'billing_start_date_timezone', 'billing_end_date_timezone', 'factory_reference_no', 'cust_contract_currency', 'cust_billing_currency', 'igc_contract_currency', 'igc_billing_currency', 'description', 'customer_billing_method', 'igc_settlement_method', 'group_name', 'tech_account_id', 'bill_account_id', 'products_id', 'payment_type_id', 'gc_organization_id_c');

                    foreach ($line_item_fields as $line_item_field) {
                        if ($line_item_details[$line_item_field] != $arr_line_item_header_list[$value_line_item_uuid][$line_item_field]) {
                            if ($line_item_field == "tech_account_id") {
                                $arr_line_item_header_list[$value_line_item_uuid]["tech_account_name"] = "<span class='upgrade-change'>" . $line_item_header_list[$value_line_item_uuid]["tech_account_name"] . "</span>";
                            } elseif ($line_item_field == "bill_account_id") {
                                $arr_line_item_header_list[$value_line_item_uuid]["bill_account_name"] = "<span class='upgrade-change'>" . $line_item_header_list[$value_line_item_uuid]["bill_account_name"] . "</span>";
                            } elseif ($line_item_field == "products_id") {
                                $arr_line_item_header_list[$value_line_item_uuid]["products_name"] = "<span class='upgrade-change'>" . $line_item_header_list[$value_line_item_uuid]["products_name"] . "</span>";
                            } elseif ($line_item_field == "payment_type_id") {
                                $arr_line_item_header_list[$value_line_item_uuid]["payment_type_value"] = "<span class='upgrade-change'>" . $line_item_header_list[$value_line_item_uuid]["payment_type_value"] . "</span>";
                            } elseif ($line_item_field == "group_name" || $line_item_field == "global_contract_item_id") {
                                $arr_line_item_header_list[$value_line_item_uuid][$line_item_field] = "<span class='color-change'>" . $line_item_header_list[$value_line_item_uuid][$line_item_field] . "</span>";
                            } elseif ($line_item_field == "gc_organization_id_c") {
                                $arr_line_item_header_list[$value_line_item_uuid]['org_name_combined'] = "<span class='upgrade-change'>" . $org_name_combined . "</span>";
                            } else {
                                $arr_line_item_header_list[$value_line_item_uuid][$line_item_field] = "<span class='upgrade-change'>" . $line_item_header_list[$value_line_item_uuid][$line_item_field] . "</span>";
                            }
                        }
                    }
                    if (!empty($arr_line_item_header_list[$value_line_item_uuid]["payment_type_id"])) {
                        switch ($arr_line_item_header_list[$value_line_item_uuid]["payment_type_id"]) {
                            case "00":
                                if ($arr_line_item_header_list[$value_line_item_uuid]["payment_type_id"] != $line_item_details['payment_type_id']) {
                                    foreach ($arr_line_item_header_list[$value_line_item_uuid]['deposit_deposit_details'] as $key => $value) {
                                        $arr_line_item_header_list[$value_line_item_uuid]['deposit_deposit_details'][$key] = "<span class='upgrade-change'>" . $value . "</span>";
                                    }
                                } else {
                                    foreach ($arr_line_item_header_list[$value_line_item_uuid]['deposit_deposit_details'] as $key => $value) {
                                        if ($arr_line_item_header_list[$value_line_item_uuid]['deposit_deposit_details'][$key] != $line_item_details['deposit_deposit_details'][$key]) {
                                            $arr_line_item_header_list[$value_line_item_uuid]['deposit_deposit_details'][$key] = "<span class='upgrade-change'>" . $value . "</span>";
                                        }
                                    }
                                }
                                break;
                            case "10":
                                if ($arr_line_item_header_list[$value_line_item_uuid]["payment_type_id"] != $line_item_details['payment_type_id']) {
                                    foreach ($arr_line_item_header_list[$value_line_item_uuid]['acc_trans_details'] as $key => $value) {
                                        $arr_line_item_header_list[$value_line_item_uuid]['acc_trans_details'][$key] = "<span class='upgrade-change'>" . $value . "</span>";
                                    }
                                } else {
                                    foreach ($arr_line_item_header_list[$value_line_item_uuid]['acc_trans_details'] as $key => $value) {
                                        if ($arr_line_item_header_list[$value_line_item_uuid]['acc_trans_details'][$key] != $line_item_details['acc_trans_details'][$key]) {
                                            $arr_line_item_header_list[$value_line_item_uuid]['acc_trans_details'][$key] = "<span class='upgrade-change'>" . $value . "</span>";
                                        }
                                    }
                                }
                                break;
                            case "30":
                                if ($arr_line_item_header_list[$value_line_item_uuid]["payment_type_id"] != $line_item_details['payment_type_id']) {
                                    foreach ($arr_line_item_header_list[$value_line_item_uuid]['postal_trans_details'] as $key => $value) {
                                        $arr_line_item_header_list[$value_line_item_uuid]['postal_trans_details'][$key] = "<span class='upgrade-change'>" . $value . "</span>";
                                    }
                                } else {
                                    foreach ($arr_line_item_header_list[$value_line_item_uuid]['postal_trans_details'] as $key => $value) {
                                        if ($arr_line_item_header_list[$value_line_item_uuid]['postal_trans_details'][$key] != $line_item_details['postal_trans_details'][$key]) {
                                            $arr_line_item_header_list[$value_line_item_uuid]['postal_trans_details'][$key] = "<span class='upgrade-change'>" . $value . "</span>";
                                        }
                                    }
                                }
                                break;
                            case "60":
                                if ($arr_line_item_header_list[$value_line_item_uuid]["payment_type_id"] != $line_item_details['payment_type_id']) {
                                    foreach ($arr_line_item_header_list[$value_line_item_uuid]['credit_card_details'] as $key => $value) {
                                        $arr_line_item_header_list[$value_line_item_uuid]['credit_card_details'][$key] = "<span class='upgrade-change'>" . $value . "</span>";
                                    }
                                } else {
                                    foreach ($arr_line_item_header_list[$value_line_item_uuid]['credit_card_details'] as $key => $value) {
                                        if ($arr_line_item_header_list[$value_line_item_uuid]['credit_card_details'][$key] != $line_item_details['credit_card_details'][$key]) {
                                            $arr_line_item_header_list[$value_line_item_uuid]['credit_card_details'][$key] = "<span class='upgrade-change'>" . $value . "</span>";
                                        }
                                    }
                                }
                                break;
                        }
                    }
                    if (isset($arr_line_item_header_list[$value_line_item_uuid]['product_config']) && is_array($arr_line_item_header_list[$value_line_item_uuid]['product_config'])) {
                        $configurations = $arr_line_item_header_list[$value_line_item_uuid]['product_config'];
                        foreach ($configurations as $key => $value) {
                            if ($value['characteristic_value'] != $line_item_details['product_config'][$key]['characteristic_value']) {
                                if (is_array($value['charval'])) {
                                    $arr_line_item_header_list[$value_line_item_uuid]['product_config'][$key]['charval']['displaytext'] = "<span class='upgrade-change'>" . $value['charval']['displaytext'] . "</span>";
                                } else {
                                    $arr_line_item_header_list[$value_line_item_uuid]['product_config'][$key]['charval'] = "<span class='upgrade-change'>" . $value['charval'] . "</span>";
                                }
                            }
                        }
                    }
                    if (isset($arr_line_item_header_list[$value_line_item_uuid]['rules']) && is_array($arr_line_item_header_list[$value_line_item_uuid]['rules'])) {
                        $rules = $arr_line_item_header_list[$value_line_item_uuid]['rules'];
                        foreach ($rules as $key => $value) {
                            if ($value['rule_value'] != $line_item_details['rules'][$key]['rule_value']) {
                                $arr_line_item_header_list[$value_line_item_uuid]['rules'][$key]['rule_value'] = "<span class='upgrade-change'>" . $value['rule_value'] . "</span>";
                            }
                        }
                    }
                    if (isset($arr_line_item_header_list[$value_line_item_uuid]['product_price']) && is_array($arr_line_item_header_list[$value_line_item_uuid]['product_price'])) {
                        $price = $arr_line_item_header_list[$value_line_item_uuid]['product_price'];
                        $price_fields = array("chargetype", "chargeperioddesc", "currencyid", "list_price", "charge_price", "igc_settlement_price");

                        foreach ($price as $key => $value) {
                            if (!isset($line_item_details['product_price'][$key])) {
                                $arr_line_item_header_list[$value_line_item_uuid]['product_price'][$key]['discount'] = "<span class='discount-change'>" . $arr_line_item_header_list[$value_line_item_uuid]['product_price'][$key]['discount'] . "</span>";
                                foreach ($price_fields as $price_field) {
                                    $arr_line_item_header_list[$value_line_item_uuid]['product_price'][$key][$price_field] = "<span class='upgrade-change'>" . $value[$price_field] . "</span>";
                                }
                            } else {
                                foreach ($price_fields as $price_field) {
                                    if ($value[$price_field] != $line_item_details['product_price'][$key][$price_field]) {
                                        $arr_line_item_header_list[$value_line_item_uuid]['product_price'][$key][$price_field] = "<span class='upgrade-change'>" . $value[$price_field] . "</span>";
                                    }
                                }
                                if ($arr_line_item_header_list[$value_line_item_uuid]['product_price'][$key]['discount'] != $line_item_details['product_price'][$key]['discount'] || $arr_line_item_header_list[$value_line_item_uuid]['product_price'][$key]['percent_discount_flag'] != $line_item_details['product_price'][$key]['percent_discount_flag']) {
                                    $arr_line_item_header_list[$value_line_item_uuid]['product_price'][$key]['discount'] = "<span class='discount-change'>" . $arr_line_item_header_list[$value_line_item_uuid]['product_price'][$key]['discount'] . "</span>";
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->ss->assign('LINEITEMHEADERLIST', $arr_line_item_header_list);
        $this->ss->assign('LINEITEMTPL', 'custom/modules/gc_Contracts/tpl/LineItemDetails.tpl');

        /**
         * ******** Assinging Values to TPL Variables for Contract Document panel ************
         */
        $contract_document_list = $this->obj_gc_contracts->getContractDocuments($this->bean->id);
        $this->ss->assign('CONTRACTID', $this->bean->id);
        $this->ss->assign('CONTRACTDOCUMENTLIST', $contract_document_list);
        $this->ss->assign('CONTRACTDOCUMENTTPL', 'custom/modules/gc_Contracts/tpl/ContractDocument.tpl');

        // Checking the contract status and change edit status flag true
        $edit = 'false';
        if (($GLOBALS['current_user']->is_admin) || (($this->bean->contract_status == '0' && ACLController::checkAccess($this->bean->object_name, 'edit', true)) || (count($role_intersect) != 0 && in_array($this->bean->contract_status, array(1, 6))))) {
            $edit = 'true';
        }
        $this->ss->assign('DOCUMENT_EDIT_MODE', $edit);
        // End
        $validateProducts = 0;
        $validateProducts = $this->checkProductType($this->bean->fetched_row['product_offering']);
        if (($this->displayButtons(1) || $validateProducts) || in_array($this->bean->product_offering, $sugar_config['enterprise_mail_po'])) {
            unset($this->dv->defs['templateMeta']['form']['buttons'][5]);
        }
        if ($this->displayButtons(0) || $validateProducts || in_array($this->bean->contract_status, array(5, 6, 2))) {
            unset($this->dv->defs['templateMeta']['form']['buttons'][6]);
            unset($this->dv->defs['templateMeta']['form']['buttons'][7]);
        }
        if (!empty($this->bean->contract_startdate_timezone)) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone', $this->bean->gc_timezone_id_c);
            $this->bean->contract_startdate_timezone .= ' (' . $obj_timezone->utcoffset . ')';
        }
        if (!empty($this->bean->contract_enddate_timezone)) {
            $obj_timezone = BeanFactory::getBean('gc_TimeZone', $this->bean->gc_timezone_id1_c);
            $this->bean->contract_enddate_timezone .= ' (' . $obj_timezone->utcoffset . ')';
        }
        $contact_record_id = $this->bean->contacts_gc_contracts_1contacts_ida;
        // Assign "Click Here" link if tere is no contracting customer account name
        if (empty($this->bean->contacts_gc_contracts_1_name) && !strlen($this->bean->contacts_gc_contracts_1_name) && $contact_record_id) {
            $this->bean->contacts_gc_contracts_1_name = "<a href='?module=Contacts&action=DetailView&record=" . $contact_record_id . "'>" . SELF::STR_NO_ACCOUNT_NAME . "</a>";
        }

        unset($line_item_header_list, $arr_line_item_order, $arr_line_item_header_list);
        parent::display();

        if (!empty($this->bean)) {
            $_SESSION['current_lock_version'] = $this->bean->lock_version;
        }

        $GLOBALS['log']->info('End: Gc_ContractsViewDetail->display()');
    }

    /*
     * To hide the create option if Contract in not in Draft mode
     */
    public function _displaySubPanels()
    {
        require_once 'custom/include/SubPanel/SubPanelTiles.php';
        $subpanel = new CustomSubPanelTiles($this->bean, $this->module);
        if ($this->bean->contract_status != '0') {
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['gc_contracts_documents_1']['top_buttons'][0]); // hiding create
        }
        echo $subpanel->display();
    }

    /**
     * Method to check to display contract workflow buttons
     *
     * @param boolean $flag
     * @return boolean
     * @author Shrikant.Gaware
     * @since 22-Feb-2016
     *        @modified : Sagar.Salunkhe DF-401 26-05-2016
     */
    private function displayButtons($flag)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'flag : ' . $flag, 'Method to check to display contract workflow buttons');
        if ($flag) {
            if ($this->bean->workflow_flag == '0' && ($GLOBALS['current_user']->is_admin || ACLController::checkAccess($this->bean->object_name, 'edit', true))) {
                if ($this->bean->contract_status == '1') {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 1;
            }
        }
        $roles = $this->obj_gc_contracts->getUserRoleIds();
        $current_approver = !empty($this->approver[$this->bean->workflow_flag]) ? $this->approver[$this->bean->workflow_flag] : '';

        if ($this->bean->contract_status == '1' || $this->bean->workflow_flag == '0' || $this->bean->workflow_flag == '4') {
            return 1;
        }

        if (!in_array($current_approver, $roles) && !$GLOBALS['current_user']->is_admin) {
            return 1;
        }

        return 0;
    }

    /**
     * Method to product type in order to display contract workflow buttons
     *
     * @param string $product_offering_id
     * @return boolean
     * @author Shrikant.Gaware
     * @since 22-Feb-2016
     */
    private function checkProductType($product_offering_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_offering_id : ' . $product_offering_id, 'Method to product type in order to display contract workflow buttons');
        global $sugar_config;
        $flag = 0;
        if (isset($sugar_config['workflow_flag'][$product_offering_id]) && strtolower($sugar_config['workflow_flag'][$product_offering_id]) == 'no') {
            $flag = 1;
        }

        if (!isset($sugar_config['workflow_flag'][$product_offering_id])) {
            $flag = 1;
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'flag : ' . $flag, 'Method to product type in order to display contract workflow buttons');
        return $flag;
    }

    /*
     * Check contact workflow
     * @param array $roles current user role
     * @param int $workflow_flag contract workflow flag
     * @return boolean
     * @author Rajul.Mondal
     * @since 05-July-2016
     */
    private function workflowAcess($roles, $workflow_flag)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'roles : ' . print_r($roles, true) . GCM_GL_LOG_VAR_SEPARATOR . 'workflow_flag : ' . $workflow_flag, 'Check contact workflow');
        // if admin return true as workflow access
        if ($GLOBALS['current_user']->is_admin) {
            return true;
        }

        $role_intersect = $this->obj_gc_contracts->arrayIntersectMulti($roles, $this->approver);
        if (count($role_intersect) > 0) {
            foreach ($role_intersect as $role) {

                $approvalPosition = array_search($role, $this->approver);

                // 1st approval && (Contract submitted to workflow || Contract submitted to Termination workflow)
                if ($approvalPosition == 1 && ($workflow_flag == 1 || $workflow_flag == 5)) {
                    return true;
                }

                // 2nd approval && (Approved by 1st level || Termination Approved by 1st level)
                elseif ($approvalPosition == 2 && ($workflow_flag == 2 || $workflow_flag == 6)) {
                    return true;
                }

                // 3rd approval && (Approved by 2nd level || Termination Approved by 2nd level)
                elseif ($approvalPosition == 3 && ($workflow_flag == 3 || $workflow_flag == 7)) {
                    return true;
                }

                // 1st,2nd,3rd approval && (Approved by 3rd level || Termination Approved by 3rd level)
                elseif ($workflow_flag == 4 || $workflow_flag == 8) {
                    return true;
                }
            }
        }

        // normal user && (Approved by 3rd level || Termination Approved by 3rd level)
        elseif ($workflow_flag == 4 || $workflow_flag == 8) {
            return true;
        }

        return false;
    }
}