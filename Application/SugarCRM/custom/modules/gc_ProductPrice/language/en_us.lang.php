<?php
// created: 2016-06-07 10:14:57
$mod_strings = array (
  'LBL_LIST_PRICE' => 'List Price',
  'LBL_DISCOUNT' => 'Discount',
  'LBL_IGC_SETTLEMENT_PRICE' => 'Inter-Group-Company Settlement Price',
  'LBL_CUSTOMER_CONTRACT_PRICE' => 'Customer Contract Price',
  'LBL_CURRENCYID' => 'Currency ID',
);