<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/gc_ProductPrice/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_ProductPrice']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/gc_ProductPrice/Ext/Vardefs/sugarfield_currencyid.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_ProductPrice']['fields']['currencyid']['audited'] = true;
$dictionary['gc_ProductPrice']['fields']['currencyid']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_ProductPrice/Ext/Vardefs/sugarfield_customer_contract_price.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_ProductPrice']['fields']['customer_contract_price']['len'] = '15';
$dictionary['gc_ProductPrice']['fields']['customer_contract_price']['precision'] = '3';


?>
<?php
// Merged from custom/Extension/modules/gc_ProductPrice/Ext/Vardefs/sugarfield_discount.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_ProductPrice']['fields']['discount']['len'] = '15';
$dictionary['gc_ProductPrice']['fields']['discount']['precision'] = '3';


?>
<?php
// Merged from custom/Extension/modules/gc_ProductPrice/Ext/Vardefs/sugarfield_igc_settlement_price.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_ProductPrice']['fields']['igc_settlement_price']['len'] = '15';
$dictionary['gc_ProductPrice']['fields']['igc_settlement_price']['precision'] = '3';


?>
<?php
// Merged from custom/Extension/modules/gc_ProductPrice/Ext/Vardefs/sugarfield_list_price.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_ProductPrice']['fields']['list_price']['len'] = '15';
$dictionary['gc_ProductPrice']['fields']['list_price']['precision'] = '3';


?>
