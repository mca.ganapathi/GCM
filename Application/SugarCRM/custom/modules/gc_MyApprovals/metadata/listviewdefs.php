<?php
// created: 2018-03-07 11:45:26
$listViewDefs['gc_MyApprovals'] = array (
  'GLOBAL_CONTRACT_ID' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_GLOBAL_CONTRACT_ID',
    'id' => 'GC_CONTRACTS_ID_C',
    'link' => true,
    'width' => '10',
    'default' => true,
  ),
  'CONTRACT_ACTION' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_CONTRACT_ACTION',
    'width' => '10',
  ),
  'FROM_STAGE' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_FROM_STAGE',
    'width' => '10',
  ),
  'TO_STAGE' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_TO_STAGE',
    'width' => '10',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'TEAM_NAME' => 
  array (
    'width' => '9',
    'label' => 'LBL_TEAM',
    'default' => true,
  ),
);