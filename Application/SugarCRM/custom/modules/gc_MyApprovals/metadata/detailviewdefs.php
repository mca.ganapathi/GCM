<?php
$module_name = 'gc_MyApprovals';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'global_contract_id',
            'studio' => 'visible',
            'label' => 'LBL_GLOBAL_CONTRACT_ID',
          ),
          1 => 
          array (
            'name' => 'contract_action',
            'studio' => 'visible',
            'label' => 'LBL_CONTRACT_ACTION',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'from_stage',
            'studio' => 'visible',
            'label' => 'LBL_FROM_STAGE',
          ),
          1 => 
          array (
            'name' => 'to_stage',
            'studio' => 'visible',
            'label' => 'LBL_TO_STAGE',
          ),
        ),
        2 => 
        array (
          0 => 'assigned_user_name',
          1 => 'team_name',
        ),
      ),
    ),
  ),
);
?>
