<?php
$dashletData['gc_MyApprovalsDashlet']['searchFields'] = array (
  'global_contract_id' => 
  array (
    'default' => '',
  ),
  'contract_action' => 
  array (
    'default' => '',
  ),
  'from_stage' => 
  array (
    'default' => '',
  ),
  'to_stage' => 
  array (
    'default' => '',
  ),
  'assigned_user_name' => 
  array (
    'default' => '',
  ),
  'team_id' => 
  array (
    'default' => '',
  ),
);
$dashletData['gc_MyApprovalsDashlet']['columns'] = array (
  'name' => 
  array (
    'type' => 'name',
    'link' => true,
    'width' => '10%',
    'label' => 'LBL_NAME',
    'default' => true,
    'name' => 'name',
  ),
  'contract_action' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_CONTRACT_ACTION',
    'width' => '10%',
    'name' => 'contract_action',
  ),
  'from_stage' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_FROM_STAGE',
    'width' => '10%',
    'name' => 'from_stage',
  ),
  'to_stage' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_TO_STAGE',
    'width' => '10%',
    'name' => 'to_stage',
  ),
  'team_name' => 
  array (
    'width' => '15%',
    'label' => 'LBL_LIST_TEAM',
    'name' => 'team_name',
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => true,
  ),
  'global_contract_id' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_GLOBAL_CONTRACT_ID',
    'id' => 'GC_CONTRACTS_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => false,
    'name' => 'global_contract_id',
  ),
);
