<?php
$popupMeta = array (
    'moduleMain' => 'gc_MyApprovals',
    'varName' => 'gc_MyApprovals',
    'orderBy' => 'gc_myapprovals.name',
    'whereClauses' => array (
  'global_contract_id' => 'gc_myapprovals.global_contract_id',
  'contract_action' => 'gc_myapprovals.contract_action',
  'from_stage' => 'gc_myapprovals.from_stage',
  'to_stage' => 'gc_myapprovals.to_stage',
  'assigned_user_name' => 'gc_myapprovals.assigned_user_name',
  'team_name' => 'gc_myapprovals.team_name',
),
    'searchInputs' => array (
  4 => 'global_contract_id',
  5 => 'contract_action',
  6 => 'from_stage',
  7 => 'to_stage',
  8 => 'assigned_user_name',
  9 => 'team_name',
),
    'searchdefs' => array (
  'global_contract_id' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_GLOBAL_CONTRACT_ID',
    'id' => 'GC_CONTRACTS_ID_C',
    'link' => true,
    'width' => '10%',
    'name' => 'global_contract_id',
  ),
  'contract_action' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_CONTRACT_ACTION',
    'width' => '10%',
    'name' => 'contract_action',
  ),
  'from_stage' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_FROM_STAGE',
    'width' => '10%',
    'name' => 'from_stage',
  ),
  'to_stage' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_TO_STAGE',
    'width' => '10%',
    'name' => 'to_stage',
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'name' => 'assigned_user_name',
  ),
  'team_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_TEAMS',
    'id' => 'TEAM_ID',
    'width' => '10%',
    'name' => 'team_name',
  ),
),
);
