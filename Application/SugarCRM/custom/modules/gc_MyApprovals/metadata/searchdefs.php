<?php
$module_name = 'gc_MyApprovals';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'global_contract_id' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_GLOBAL_CONTRACT_ID',
        'id' => 'GC_CONTRACTS_ID_C',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'name' => 'global_contract_id',
      ),
      'contract_action' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_CONTRACT_ACTION',
        'width' => '10%',
        'name' => 'contract_action',
      ),
      'from_stage' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_FROM_STAGE',
        'width' => '10%',
        'name' => 'from_stage',
      ),
      'to_stage' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_TO_STAGE',
        'width' => '10%',
        'name' => 'to_stage',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'width' => '10%',
        'default' => true,
      ),
      'team_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_TEAM',
        'id' => 'TEAM_ID',
        'width' => '10%',
        'default' => true,
        'name' => 'team_name',
      ),
    ),
    'advanced_search' => 
    array (
      'global_contract_id' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_GLOBAL_CONTRACT_ID',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'id' => 'GC_CONTRACTS_ID_C',
        'name' => 'global_contract_id',
      ),
      'contract_action' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_CONTRACT_ACTION',
        'width' => '10%',
        'name' => 'contract_action',
      ),
      'from_stage' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_FROM_STAGE',
        'width' => '10%',
        'name' => 'from_stage',
      ),
      'to_stage' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_TO_STAGE',
        'width' => '10%',
        'name' => 'to_stage',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'team_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_TEAMS',
        'width' => '10%',
        'default' => true,
        'id' => 'TEAM_ID',
        'name' => 'team_name',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
