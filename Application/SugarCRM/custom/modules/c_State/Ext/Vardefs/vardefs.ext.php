<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/c_State/Ext/Vardefs/c_country_c_state_1_c_State.php

 // created: 2016-09-29 04:39:40
$dictionary['c_State']['fields']['c_country_c_state_1']['name'] = 'c_country_c_state_1';
$dictionary['c_State']['fields']['c_country_c_state_1']['type'] = 'link';
$dictionary['c_State']['fields']['c_country_c_state_1']['relationship'] = 'c_country_c_state_1';
$dictionary['c_State']['fields']['c_country_c_state_1']['source'] = 'non-db';
$dictionary['c_State']['fields']['c_country_c_state_1']['module'] = 'c_country';
$dictionary['c_State']['fields']['c_country_c_state_1']['bean_name'] = 'c_country';
$dictionary['c_State']['fields']['c_country_c_state_1']['vname'] = 'LBL_C_COUNTRY_C_STATE_1_FROM_C_COUNTRY_TITLE';
$dictionary['c_State']['fields']['c_country_c_state_1']['id_name'] = 'c_country_c_state_1c_country_ida';
$dictionary['c_State']['fields']['c_country_c_state_1_name']['name'] = 'c_country_c_state_1_name';
$dictionary['c_State']['fields']['c_country_c_state_1_name']['type'] = 'relate';
$dictionary['c_State']['fields']['c_country_c_state_1_name']['source'] = 'non-db';
$dictionary['c_State']['fields']['c_country_c_state_1_name']['vname'] = 'LBL_C_COUNTRY_C_STATE_1_FROM_C_COUNTRY_TITLE';
$dictionary['c_State']['fields']['c_country_c_state_1_name']['save'] = true;
$dictionary['c_State']['fields']['c_country_c_state_1_name']['id_name'] = 'c_country_c_state_1c_country_ida';
$dictionary['c_State']['fields']['c_country_c_state_1_name']['link'] = 'c_country_c_state_1';
$dictionary['c_State']['fields']['c_country_c_state_1_name']['table'] = 'c_country';
$dictionary['c_State']['fields']['c_country_c_state_1_name']['module'] = 'c_country';
$dictionary['c_State']['fields']['c_country_c_state_1_name']['rname'] = 'name';
$dictionary['c_State']['fields']['c_country_c_state_1_name']['audited'] = true;
$dictionary['c_State']['fields']['c_country_c_state_1c_country_ida']['name'] = 'c_country_c_state_1c_country_ida';
$dictionary['c_State']['fields']['c_country_c_state_1c_country_ida']['type'] = 'id';
$dictionary['c_State']['fields']['c_country_c_state_1c_country_ida']['relationship'] = 'c_country_c_state_1';
$dictionary['c_State']['fields']['c_country_c_state_1c_country_ida']['source'] = 'non-db';
$dictionary['c_State']['fields']['c_country_c_state_1c_country_ida']['reportable'] = false;
$dictionary['c_State']['fields']['c_country_c_state_1c_country_ida']['side'] = 'right';
$dictionary['c_State']['fields']['c_country_c_state_1c_country_ida']['vname'] = 'LBL_C_COUNTRY_C_STATE_1_FROM_C_STATE_TITLE';
$dictionary['c_State']['fields']['c_country_c_state_1c_country_ida']['link'] = 'c_country_c_state_1';
$dictionary['c_State']['fields']['c_country_c_state_1c_country_ida']['rname'] = 'id';

?>
<?php
// Merged from custom/Extension/modules/c_State/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['c_State']['full_text_search']=false;

?>
