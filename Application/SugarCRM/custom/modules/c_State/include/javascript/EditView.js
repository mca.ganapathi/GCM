/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$( document ).ready(function() {
	document.getElementById('SAVE_HEADER').onclick = function() {
		if (ValidateSave()) {
			var _form = document.getElementById('EditView');
			_form.action.value = 'Save';
			SUGAR.ajaxUI.submitForm(_form);
		}
		return false;
		
	};
	document.getElementById('SAVE_FOOTER').onclick = function() {
		if (ValidateSave()) {
			var _form = document.getElementById('EditView');
			_form.action.value = 'Save';
			SUGAR.ajaxUI.submitForm(_form);
		}
		return false;
	};
});

function ValidateSave() {
		clear_all_errors(); // clear sugar validation msgs
		removeFromValidate('EditView','c_country_c_state_1_name');
		removeFromValidate('EditView','c_country_c_state_1c_country_ida');
		
		if($.trim($("#c_country_c_state_1c_country_ida").val()) == '') {
		addToValidate('EditView', 'c_country_c_state_1_name', 'text', true, SUGAR.language.get('c_State', 'LBL_C_COUNTRY_C_STATE_1_FROM_C_COUNTRY_TITLE'));
		} else {
		removeFromValidate('EditView','c_country_c_state_1_name');
		}
	return check_form('EditView');
}