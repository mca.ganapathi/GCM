<?php
// created: 2016-09-29 04:41:43
$viewdefs['c_State']['base']['view']['subpanel-c_country_subpanel_c_country_c_state_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'default' => true,
          'label' => 'LBL_NAME',
          'enabled' => true,
          'name' => 'name',
          'link' => true,
          'type' => 'name',
        ),
        1 => 
        array (
          'type' => 'varchar',
          'default' => true,
          'label' => 'LBL_STATE_TYPE',
          'enabled' => true,
          'name' => 'state_type',
        ),
        2 => 
        array (
          'type' => 'varchar',
          'default' => true,
          'label' => 'LBL_STATE_CODE',
          'enabled' => true,
          'name' => 'state_code',
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);