<?php

$moduleName = 'c_State';

$viewdefs[$moduleName]['base']['menu']['header'] = array(
    array (
        'route' => '#c_State/create',
        'label' => 'LNK_NEW_RECORD',
        'acl_action' => 'create',
        'acl_module' => 'c_State',
        'icon' => 'fa-plus',
    ),
    array(
        'route' => "#bwc/index.php?module=$moduleName&action=ListView",
        'label' => 'LNK_LIST',
        'acl_action' => 'list',
        'acl_module' => $moduleName,
        'icon' => 'fa fa-bars',
    ),
    array(
        'route' => "#bwc/index.php?module=Import&action=Step1&import_module=$moduleName&return_module=$moduleName&return_action=index",
        'label' => 'LBL_IMPORT',
        'acl_action' => 'import',
        'acl_module' => $moduleName,
        'icon' => 'fa-arrow-circle-o-up',
    ),
);