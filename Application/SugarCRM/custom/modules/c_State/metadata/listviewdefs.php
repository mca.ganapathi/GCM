<?php
// created: 2018-03-07 11:45:26
$listViewDefs['c_State'] = array (
  'NAME' => 
  array (
    'width' => '30',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'C_COUNTRY_C_STATE_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_C_COUNTRY_C_STATE_1_FROM_C_COUNTRY_TITLE',
    'id' => 'C_COUNTRY_C_STATE_1C_COUNTRY_IDA',
    'width' => '30',
    'default' => true,
  ),
  'STATE_TYPE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_STATE_TYPE',
    'width' => '20',
    'default' => true,
  ),
  'STATE_CODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_STATE_CODE',
    'width' => '20',
    'default' => true,
  ),
);