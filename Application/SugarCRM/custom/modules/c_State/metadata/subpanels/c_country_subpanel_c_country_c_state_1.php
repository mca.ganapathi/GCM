<?php
// created: 2016-01-19 07:09:46
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '40%',
    'default' => true,
  ),
  'state_type' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_STATE_TYPE',
    'width' => '30%',
    'default' => true,
  ),
  'state_code' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_STATE_CODE',
    'width' => '30%',
    'default' => true,
  ),
);