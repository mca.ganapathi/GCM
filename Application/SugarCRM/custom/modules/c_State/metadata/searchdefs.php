<?php
$module_name = 'c_State';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'state_type' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_STATE_TYPE',
        'width' => '10%',
        'default' => true,
        'name' => 'state_type',
      ),
      'state_code' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_STATE_CODE',
        'width' => '10%',
        'default' => true,
        'name' => 'state_code',
      ),
      'c_country_c_state_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_C_COUNTRY_C_STATE_1_FROM_C_COUNTRY_TITLE',
        'id' => 'C_COUNTRY_C_STATE_1C_COUNTRY_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'c_country_c_state_1_name',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'state_type' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_STATE_TYPE',
        'width' => '10%',
        'default' => true,
        'name' => 'state_type',
      ),
      'state_code' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_STATE_CODE',
        'width' => '10%',
        'default' => true,
        'name' => 'state_code',
      ),
      'c_country_c_state_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_C_COUNTRY_C_STATE_1_FROM_C_COUNTRY_TITLE',
        'width' => '10%',
        'default' => true,
        'id' => 'C_COUNTRY_C_STATE_1C_COUNTRY_IDA',
        'name' => 'c_country_c_state_1_name',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>