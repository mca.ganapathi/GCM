<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

global $sugar_config;
require_once "custom/modules/Users/login_functions.php";

$nonce = 'n-' . rand();
$_SESSION['OP_nonce'] = $nonce;
$openIdUrl = $sugar_config['openidconnect']['iss'] . "/as/authorization.oauth2?client_id=" . $sugar_config['openidconnect']['aud'] . "&scope=openid email profile address phone&state=ping&redirect_uri=&" . $sugar_config['site_url'] . "index.php?action=Login&module=Users&response_type=id_token token&response_mode=form_post&nonce=" . $nonce;
?>
<link href="modules/Users/login.css" media="all" type="text/css"
	rel="stylesheet">
<script type="text/javascript" src="modules/Users/login.js"></script>


<table style="width: 460px;" cellspacing="2" cellpadding="0" border="0"
	align="center">
	<tr>
		<td style="text-align: center;">
			<div style="margin: 50px 0px 20px 0px; text-align: center;">
				<img border="0" id="logo" class="logo"
					src="custom/themes/default/images/company_logo.png?v=hYMCzuoZDyB6l66W7cDeng&amp;logo_md5=230187f9f6630edf1bccc635a4abad2b">
			</div>
			<div style="margin: 10px 0px 20px 0px; text-align: center;">
				<h2>Hi, Welcome to GCM !</h2>
			</div>
		</td>
	</tr>
	<tr>
		<td style="text-align: center;"><span class='error'
			style="text-align: center;">
                                <?php echo $_SESSION['login_error']; ?>
                         </span></td>
	</tr>

	<tr>
		<td style="text-align: center;">
			<fieldset>
				<div style="width: 460px;" class="loginBoxShadow">
					<div class="loginBox">
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							align="center">

							<tr>
								<td align="left"><b>Login With COE account</b><br></td>
							</tr>
							<tr>
								<td>
									<div class="login">

										<table width="100%" cellspacing="2" cellpadding="0" border="0"
											align="center">
											<tbody>
												<tr>
													<td width="1%" scope="row"></td>
													<td style="text-align: center;"><a
														href="<?php echo $openIdUrl; ?>"
														style="text-decoration: none;"> <input type="button"
															value="Login" id="coe_login" name="coe_login">
													</a></td>
												</tr>

											</tbody>
										</table>
									</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</fieldset> <br>
			<div style="text-align: center; font-weight: bold;">Or</div> <br>

			<fieldset>
				<div style="width: 460px;" class="loginBoxShadow">
					<div class="loginBox">
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							align="center">
							<tbody>
								<tr>
									<td align="left"><b>Login With GCM account</b><br></td>
								</tr>

								<tr>
									<td>
										<div class="login">
											<form action="index.php" method="post" name="DetailView"
												id="form">
												<table width="100%" cellspacing="2" cellpadding="0"
													border="0" align="center">
													<tbody>
														<tr>
															<td colspan="2" scope="row"><span style="display: none"
																id="browser_warning" class="error"> <b>Warning:</b> Your
																	browser version is no longer supported or you are using
																	an unsupported browser.
																	<p></p>The following browser versions are recommended:
																	<p></p>
																	<ul>
																		<li>Internet Explorer 10 (compatibility view not
																			supported)</li>
																		<li>Firefox 39.0</li>
																		<li>Safari 6.0</li>
																		<li>Chrome 43</li>
																	</ul>
															</span> <span style="display: none"
																id="ie_compatibility_mode_warning" class="error"> <b>Warning:</b>
																	Your browser is in IE compatibility view which is not
																	supported.
															</span></td>
														</tr>
														<tr>
															<td width="1%" scope="row"></td>
															<td scope="row"><span class="error" id="post_error"></span></td>
														</tr>
														<tr>
															<td width="100%"
																style="font-size: 12px; font-weight: normal; padding-bottom: 4px;"
																colspan="2" scope="row"><input name="module"
																value="Users" type="hidden"> <input name="action"
																value="Authenticate" type="hidden"> <input
																name="return_module" value="Users" type="hidden"> <input
																name="return_action" value="Login" type="hidden"> <input
																id="cant_login" name="cant_login" value="" type="hidden">
																<input name="login_module" value="" type="hidden"> <input
																name="login_action" value="" type="hidden"> <input
																name="login_record" value="" type="hidden"> <input
																name="login_token" value="" type="hidden"> <input
																name="login_oauth_token" value="" type="hidden"> <input
																name="login_mobile" value="" type="hidden"></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td scope="row">Language:</td>
															<td><select onchange="switchLanguage(this.value)"
																name="login_language" style="width: 152px">
																	<option value="en_us" selected="">English (US)</option>
																	<option value="ja_JP">日本語</option>
															</select></td>
														</tr>
														<tr>
															<td width="30%" scope="row"><label for="user_name">User
																	Name:</label></td>
															<td width="70%"><input type="text" value=""
																name="user_name" id="user_name" tabindex="1" size="35"></td>
														</tr>
														<tr>
															<td scope="row"><label for="user_password">Password:</label></td>
															<td width="30%"><input type="password" value=""
																name="user_password" id="user_password" tabindex="2"
																size="26"></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td><input type="submit" value="Log In" name="Login"
																id="login_button" tabindex="3" class="button primary"
																title="Log In"><br>&nbsp;</td>
														</tr>
													</tbody>
												</table>
											</form>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</fieldset>
		</td>
	</tr>
	<table>
<?php $_SESSION['login_error'] = ''; exit; ?>