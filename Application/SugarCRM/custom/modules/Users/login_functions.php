<?php
if (isset($_REQUEST['error']) && !empty($_REQUEST['error'])) {
    $_SESSION['login_error'] = 'Access Denied ! Please contact Admin';
} else 
    if (isset($_REQUEST['id_token']) && !empty($_REQUEST['id_token']) && !empty($_REQUEST['access_token']) && !empty($_SESSION['OP_nonce'])) {
        
        $idToken = $_REQUEST['id_token'];
        $accessToken = $_REQUEST['access_token'];
        $nonce = $_SESSION['OP_nonce'];
        $issuedAt = time();
        $validationResponse = validateIDToken($idToken, $issuedAt, $nonce);
        
        if (!empty($validationResponse['userEmail']) && is_array($validationResponse) && $validationResponse['errFlag'] == 0) {
            
            if (!defined('SUGAR_PHPUNIT_RUNNER')) {
                session_regenerate_id(false);
            }
            
            $arrLoginUserDetails = array();
            global $mod_strings, $db, $arrLoginUserDetails;
            
            $res = $GLOBALS['sugar_config']['passwordsetting'];
            $login_vars = $GLOBALS['app']->getLoginVars(false);
            
            $user_name = isset($validationResponse['userEmail']) ? trim($validationResponse['userEmail']) : '';
            $arrLoginUserDetails['userEmail'] = $user_name;
            
            if (!empty($user_name)) {
                $password = mt_rand();
                $newPassword = FnLoginPasswordHash($password);
                $updatePass = "UPDATE users SET user_hash = '" . $newPassword . "' WHERE user_name = '" . $user_name . "'";
                $db->query($updatePass);
            }
            
            $authController->login($user_name, $password);
            
            // authController will set the authenticated_user_id session variable
            if (isset($_SESSION['authenticated_user_id'])) { // Login is successful
                
                $_SESSION['login_error'] = '';
                
                if ($_SESSION['hasExpiredPassword'] == '1' && $_REQUEST['action'] != 'Save') {
                    $GLOBALS['module'] = 'Users';
                    $GLOBALS['action'] = 'ChangePassword';
                    ob_clean();
                    header("Location: index.php?module=Users&action=ChangePassword");
                    sugar_cleanup(true);
                }
                
                global $record;
                global $current_user;
                global $sugar_config;
                
                if (isset($_SESSION['isMobile']) && (empty($_REQUEST['login_module']) || $_REQUEST['login_module'] == 'Users') && (empty($_REQUEST['login_action']) || $_REQUEST['login_action'] == 'wirelessmain')) {
                    $last_module = $current_user->getPreference('wireless_last_module');
                    if (!empty($last_module)) {
                        $login_vars['login_module'] = $_REQUEST['login_module'] = $last_module;
                        $login_vars['login_action'] = $_REQUEST['login_action'] = 'wirelessmodule';
                    }
                }
                
                global $current_user;
                
                if (isset($current_user) && empty($login_vars)) {
                    if (!empty($GLOBALS['sugar_config']['default_module']) && !empty($GLOBALS['sugar_config']['default_action'])) {
                        $url = "index.php?module={$GLOBALS['sugar_config']['default_module']}&action={$GLOBALS['sugar_config']['default_action']}";
                    } else {
                        $modListHeader = query_module_access_list($current_user);
                        // try to get the user's tabs
                        $tempList = $modListHeader;
                        $idx = array_shift($tempList);
                        if (!empty($modListHeader[$idx])) {
                            $url = "index.php?module={$modListHeader[$idx]}&action=index";
                        }
                    }
                } else {
                    $url = $GLOBALS['app']->getLoginRedirect();
                }
            } else {
                
                $_SESSION['login_error'] = "Access Denied ! Please contact Admin";
                
                $url = "index.php?module=Users&action=Login";
                if (!empty($login_vars)) {
                    $url .= '&' . http_build_query($login_vars);
                }
            }
            
            if (empty($_SESSION['login_error'])) {
                $url = 'Location: ' . $url; // construct redirect url
                if (isset($_SESSION['isMobile'])) {
                    $url = $url . '&mobile=1';
                } // check for presence of a mobile device, redirect accordingly
            }
            
            // adding this for bug: 21712.
            if (!empty($GLOBALS['app'])) {
                $GLOBALS['app']->headerDisplayed = true;
            }
            if (!defined('SUGAR_PHPUNIT_RUNNER')) {
                sugar_cleanup();
                header($url);
            }
        } else {
            $_SESSION['login_error'] = ($validationResponse['errFlag'] == 1) ? "ID token authentication failed! Please contact Admin" : "Authentication Failed! Please contact Admin";
        }
    }

/**
 * Method FnLoginPasswordHash to encrypt string $password
 * @param string $password
 * @return string 
 */
function FnLoginPasswordHash($password)
{
    if (!defined('CRYPT_MD5') || !constant('CRYPT_MD5')) {
        // does not support MD5 crypt - leave as is
        if (defined('CRYPT_EXT_DES') && constant('CRYPT_EXT_DES')) {
            return crypt(strtolower(md5($password)), "_.012" . substr(str_shuffle('./ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'), -4));
        }
        // plain crypt cuts password to 8 chars, which is not enough
        // fall back to old md5
        return strtolower(md5($password));
    }
    return crypt(strtolower(md5($password)));
}

/**
 * Method urlsafeB64Decode
 * @param string $input
 * @return string
 */
function urlsafeB64Decode($input)
{
    $GLOBALS['log']->info("Begin: urlsafeB64Decode($input)");
    $remainder = strlen($input) % 4;
    if ($remainder) {
        $padlen = 4 - $remainder;
        $input .= str_repeat('=', $padlen);
    }
    $GLOBALS['log']->info("End: urlsafeB64Decode($input)");
    return base64_decode(strtr($input, '-_', '+/'));
}

/**
 * Method jsonDecode
 * @param object $input
 * @return array $obj
 */
function jsonDecode($input)
{
    $obj = json_decode($input, true);
    return $obj;
}

/**
 * Method sign
 * @param string $msg
 * @param string $key
 * @param string $method
 * @return string
 */
function sign($msg, $key, $method = 'HS256')
{
    $GLOBALS['log']->info("End: sign($msg, $key, $method)");
    $methods = array(
                    'HS256' => 'sha256', 
                    'HS384' => 'sha384', 
                    'HS512' => 'sha512'
    );
    if (empty($methods[$method])) {
        $GLOBALS['log']->info("End: sign($msg, $key, $method)");
        return 0;
    }
    $GLOBALS['log']->info("End: sign($msg, $key, $method)");
    return hash_hmac($methods[$method], $msg, $key, true);
}

/**
 * Method validateIDToken
 * @param string $datab64
 * @param string $time
 * @param string $nonce
 * @return array $arrReturn
 */
function validateIDToken($datab64, $time, $nonce)
{
    $GLOBALS['log']->info('Begin: validateIDToken(' . print_r($datab64, true) . ",$time,$nonce)");
    global $sugar_config;
    $error = $arrReturn = array();
    
    $tks = explode('.', $datab64);
    list($headb64, $payloadb64, $cryptob64) = $tks;
    $signature = urlsafeB64Decode($cryptob64);
    $algorithm = urlsafeB64Decode($headb64);
    $data = urlsafeB64Decode($payloadb64);
    $dataArr = jsonDecode($data);
    $algArr = jsonDecode($algorithm);
    $payloadArr = array_merge($dataArr, $algArr);
    
    $key = $sugar_config['openidconnect']['client_secret'];
    
    /* Start: Custom Time for IDF Validation */
    $custom_time = $time + 1;
    /* End: Custom Time for IDF Validation */
    if (is_array($payloadArr) && !empty($payloadArr)) {
        if ($nonce != trim($payloadArr['nonce'])) {
            $error[] = 'Nounce mismatch';
        }
        if ($sugar_config['openidconnect']['aud'] != trim($payloadArr['aud'])) {
            $error[] = 'Audiance mismatch';
        }
        if ($sugar_config['openidconnect']['iss'] != trim($payloadArr['iss'])) {
            $error[] = 'OP mismatch';
        }
        if ($sugar_config['openidconnect']['alg'] != trim($payloadArr['alg'])) {
            $error[] = 'Algorithm mismatch';
        }
        /* Start: Custom Time for IDF Validation */
        if ($custom_time < $payloadArr['iat']) {
            $error[] = 'Too Early';
        }
        /* End: Custom Time for IDF Validation */
        if ($time > $payloadArr['exp']) {
            $error[] = 'Time Expired';
        }
        if ($signature != sign("$headb64.$payloadb64", $key, $payloadArr['alg'])) {
            $error[] = 'Signature verification failed';
        }
        if (!empty($error)) {
            $GLOBALS['log']->info('IDToken contains Errors: ' . print_r($error, true));
            $GLOBALS['log']->info('End: validateIDToken(' . print_r($datab64, true) . ",$time,$nonce,$custom_time)");
            $arrReturn['errFlag'] = 1;
            return $arrReturn;
        }
        if (empty($error)) {
            $GLOBALS['log']->info('IDToken Validated Successfully: ');
            $GLOBALS['log']->info('End: validateIDToken(' . print_r($datab64, true) . ",$time,$nonce,$custom_time)");
            $arrReturn['errFlag'] = 0;
            $arrReturn['userEmail'] = $payloadArr['email'];
            return $arrReturn;
        }
    }
    
    $error[] = 'Replay Attacks';
    $GLOBALS['log']->info('IDToken Replay Attacks');
    $GLOBALS['log']->info('End: validateIDToken(' . print_r($datab64, true) . ",$time,$nonce)");
    $arrReturn['errFlag'] = 1;
    unset($error);
    return $arrReturn;
}