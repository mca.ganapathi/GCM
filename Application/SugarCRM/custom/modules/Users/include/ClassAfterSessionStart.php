<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class ClassUsersAfterSessionStart
{
    
    function setGlobalConfigWorkflowOfferings($bean, $event, $arguments=array())
    {
        ModUtils::setGlobalConfigWorkflowOfferings();
        return true;
    }
}