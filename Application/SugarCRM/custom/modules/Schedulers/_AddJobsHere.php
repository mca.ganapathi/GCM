<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * Set up an array of Jobs with the appropriate metadata
 * 'jobName' => array (
 * 'X' => 'name',
 * )
 * 'X' should be an increment of 1
 * 'name' should be the EXACT name of your function
 *
 * Your function should not be passed any parameters
 * Always return a Boolean. If it does not the Job will not terminate itself
 * after completion, and the webserver will be forced to time-out that Job instance.
 * DO NOT USE sugar_cleanup(); in your function flow or includes. this will
 * break Schedulers. That function is called at the foot of cron.php
 */

/**
 * This array provides the Schedulers admin interface with values for its "Job"
 * dropdown menu.
 */

array_push($job_strings, 'contractApprovalReminder', 'contractTerminationApprovalReminder');
$mod_strings['LBL_CONTRACTAPPROVALREMINDER'] = 'Contract Approval Reminder Email';
$mod_strings['LBL_CONTRACTTERMINATIONAPPROVALREMINDER'] = 'Email for Pending Contract Termination Approval';

/**
 * method to send contract approval reminder mailer to Approver team members
 * @author Sagar.Salunkhe
 * @param        
 *
 * @return <boolean> value will be updated in Scheduler modules log in scheduler is executed successfully or not.
 *         @date 01-Mar-2016
 */

function contractApprovalReminder()
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'method to send contract approval reminder mailer to Approver team members');    
    require_once ('custom/include/SugarMagicMailer.php');
    require_once('custom/modules/gc_Contracts/CustomFunctionGcContract.php');
    $obj_custom_gc_contracts = new CustomFunctionGcContract();
    $approver = $obj_custom_gc_contracts->getRolePermissions(SETTING_KEY_ACE_CONTRACTS);
    $array_approval_stage = array();
    /* contract stage list and respective team to send notification/reminder mailer */
    if(isset($approver['approver_team1']) && isset($approver['approver_team2']) && isset($approver['approver_team3'])) {
        $array_approval_stage = array(
            '1' => $approver['approver_team1'],
            '2' => $approver['approver_team2'],
            '3' => $approver['approver_team3']
        );
    } // workflow stage list (except final stage i.e. approved by 3rd approver)
    
    $template_name = 'GCM_Reminder_Email'; // reminder mail template name
    
    /* build where condition to get list of contracts */
    $where = '';
    $workflow_flag_condition = '';
    if (! empty($array_approval_stage)) {
        
        foreach ($array_approval_stage as $stage => $team)
            $workflow_flag_condition .= " workflow_flag = '$stage' OR";
        
        $workflow_flag_condition = trim($workflow_flag_condition, 'OR');
    }
    $where .= $workflow_flag_condition;
    
    /* fetch contracts list */
    $obj_contracts_bean = BeanFactory::getBean('gc_Contracts');
    $arr_contract_bean_list = $obj_contracts_bean->get_full_list('', " $where ");
    
    if (!empty($arr_contract_bean_list)) {
        foreach ($arr_contract_bean_list as $contract_record) {
            // get reminder team name of respective stage
            $reminder_team_id = $array_approval_stage[$contract_record->workflow_flag];
            if (empty($reminder_team_id) || ! isset($reminder_team_id)) continue;
        
            // get list of team members from team name
            $obj_utils = ModUtils::getInstance('', '');
            $arr_reminder_team_members = $obj_utils->getTeamMembers($reminder_team_id, false, 1);
            // collect list of email addresses of respective users
            $arr_user_emails = array();
            foreach ($arr_reminder_team_members as $obj_user)
                $arr_user_emails[] = $obj_user->email1;
        
                $obj_SugarMagicMailer = new SugarMagicMailer();
                $params = array(
                    'to_addr' => $arr_user_emails,
                    'template_name' => $template_name,
                    'template_type' => 'workflow'
                );
                $obj_SugarMagicMailer->sendSugarMail($params, $contract_record->object_name, $contract_record->id);
        }
    }

    unset($array_approval_stage, $obj_contracts_bean, $arr_contract_bean_list);
    
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    return true;
} // end of function

/**
 * method to send contract termination approval reminder mailer to Approver team members
 * @author Rajul.Mondal
 * @param        
 *
 * @return <boolean> value will be updated in Scheduler modules log in scheduler is executed successfully or not.
 *         @date 28-July-2016
 */

function contractTerminationApprovalReminder()
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'method to send contract termination approval reminder mailer to Approver team members');
    
    require_once ('custom/include/SugarMagicMailer.php');
    require_once('custom/modules/gc_Contracts/CustomFunctionGcContract.php');
    $obj_custom_gc_contracts = new CustomFunctionGcContract();
    $approver = $obj_custom_gc_contracts->getRolePermissions(SETTING_KEY_ACE_CONTRACTS);
    $array_approval_stage = array();
    /* contract stage list and respective team to send notification/reminder mailer */
    if(isset($approver['approver_team1']) && isset($approver['approver_team2']) && isset($approver['approver_team3'])) {
        $array_approval_stage = array(
            '5' => $approver['approver_team1'],
            '6' => $approver['approver_team2'],
            '7' => $approver['approver_team3']
        );
    } // workflow stage list (except final stage i.e. approved by 3rd approver)
    
    $template_name = 'GCM_Reminder_Email_Termination'; // reminder mail template name
    
    /* build where condition to get list of contracts */
    $where = '';
    $workflow_flag_condition = '';
    if (! empty($array_approval_stage)) {
        foreach ($array_approval_stage as $stage => $team)
            $workflow_flag_condition .= " workflow_flag = '$stage' OR";
        $workflow_flag_condition = trim($workflow_flag_condition, 'OR');
    }
    $where .= $workflow_flag_condition;
    
    /* fetch contracts list */
    $obj_contracts_bean = BeanFactory::getBean('gc_Contracts');
    $arr_contract_bean_list = $obj_contracts_bean->get_full_list('', " $where ");
    
    if (!empty($arr_contract_bean_list)) {
        foreach ($arr_contract_bean_list as $contract_record) {
            // get reminder team name of respective stage
            $reminder_team_id = $array_approval_stage[$contract_record->workflow_flag];
            if (empty($reminder_team_id) || ! isset($reminder_team_id)) continue;
        
            // get list of team members from team name
            $obj_utils = ModUtils::getInstance('', '');
            $arr_reminder_team_members = $obj_utils->getTeamMembers($reminder_team_id, false, 1);
            // collect list of email addresses of respective users
            $arr_user_emails = array();
            foreach ($arr_reminder_team_members as $obj_user){
                $arr_user_emails[] = $obj_user->email1;
            }
            $obj_SugarMagicMailer = new SugarMagicMailer();
            $params = array(
                'to_addr' => $arr_user_emails,
                'template_name' => $template_name,
                'template_type' => 'workflow'
            );
            $sendSugarMail = $obj_SugarMagicMailer->sendSugarMail($params, $contract_record->object_name, $contract_record->id);
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'sendSugarMail : '.$sendSugarMail, 'to check mail sent or not');           
        }        
    }

    unset($array_approval_stage, $obj_contracts_bean, $arr_contract_bean_list);    
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');    
    return true;
} // end of function

?>