/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/
/**
 * Function to add an empty row
 *
 * @author Shrikant.Gaware
 * @since Dec 17, 2015
 */
function addEmptyRow() {
    var parentTab = $('.list');
    var parentHeader = parentTab.find('.trHeader');
    newRow = $('<tr><td colspan = "5" align="center">No Records Found !!!<td></tr>');
    parentTab.empty();
    parentHeader.appendTo(parentTab);
    newRow.appendTo(parentTab);
}
/**
 * Function to add the row to search result table
 * 
 * @param array result CIDAS AJAX Data
 * @author Shrikant.Gaware
 * @since Dec 17, 2015
 */
function addRow(result) {
    var parentTab = $('.list');
    var parentHeader = parentTab.find('.trHeader');
    if (result.length > 0) {
        parentTab.empty();
        parentHeader.appendTo(parentTab);
        for (i = 0; i < result.length; i++) {
            var newRow = $('.listRowCopy').clone(true, true);
            newRow.removeClass('listRowCopy hide');
            var res = result[i];
            var onclickData = "";
            for (var key in res) {
                if (res[key] != '') {
                    newRow.find('.' + key).html(res[key]);
                    // newRow.find('.'+key).attr('onclick', result[i].text);
                } else {
                    var parent = newRow.find('.' + key).parent();
                    parent.empty();
                    parent.html('&nbsp;');
                }
            }
            onclickData = "sendBackData('" + JSON.stringify(result[i]) + "');";
            newRow.find('.CidasData').attr('onclick', onclickData);
            newRow.appendTo(parentTab);
        }
    } else {
        addEmptyRow();
    }
}
/**
 * Fucntion to remove all falsy values: undefined, null, 0, false, NaN and ""
 * (empty string) from an array
 * 
 * @param array actual
 * @return array
 * @author Dinesh.Itkar
 * @since Dec 32, 2015
 */
function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}
/**
 * AJAX Function to search CIDAS records based on search text
 *
 * @author Shrikant.Gaware
 * @since Dec 17, 2015
 */
function searchRecords() {
    if (typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function() {
            return this.replace(/^\s+|\s+$/g, '');
        }
    }
    var commonCustomerId = $('#commonCustomerId').val();
    var companyNameEng = $('#companyNameEng').val();
    var companyName = $('#companyName').val();
    commonCustomerId = commonCustomerId.trim();
    companyNameEng = companyNameEng.trim();
    companyName = companyName.trim();
    var input_fields = [commonCustomerId, companyNameEng, companyName];
    var input_fields_filter = cleanArray(input_fields);
    if (input_fields_filter.length == 0) {
        YAHOO.SUGAR.MessageBox.show({
            msg: 'Please enter a key to search record(s) from CIDAS.',
            title: 'CIDAS Search',
            type: 'alert'
        });
        return false;
    }
    if (input_fields_filter.length > 1) {
        YAHOO.SUGAR.MessageBox.show({
            msg: 'Please enter any one key to search record(s) from CIDAS.',
            title: 'CIDAS Search',
            type: 'alert'
        });
        return false;
    }
    SUGAR.ajaxUI.showLoadingPanel();
    $.ajax({
        type: "POST",
        url: "index.php",
        data: {
            module: 'Accounts',
            action: 'Actions',
            method: 'getCidasData',
            sugar_body_only: '1',
            commonCustomerId: commonCustomerId,
            companyNameEng: companyNameEng,
            companyName: companyName,
        },
        async: false,
        success: function(data) {
            var result = $.parseJSON(data);
            if (result.hasOwnProperty('error') || $.isEmptyObject(result)) {
                SUGAR.ajaxUI.hideLoadingPanel();
                addEmptyRow();
                var error_str = '';
                for (var key in result.error) {
                    var res = result.error[key].split(":");
                    if ($.trim(res[0]) == "3005") error_str += 'No record found from CIDAS.<br />';
                    else error_str += result.error[key] + '<br />';
                }
                YAHOO.SUGAR.MessageBox.show({
                    msg: error_str,
                    title: 'CIDAS Search',
                    type: 'alert'
                });
                return;
            } else if (result.cidas_failed == 1) {
                SUGAR.ajaxUI.hideLoadingPanel();
                addEmptyRow();
                YAHOO.SUGAR.MessageBox.show({
                    msg: 'No record found from CIDAS.',
                    title: 'CIDAS Search',
                    type: 'alert'
                });
                return;
            } else if (result.length > 0 && result[0].cidas_data == 'No') {
                SUGAR.ajaxUI.hideLoadingPanel();
                addEmptyRow();
                YAHOO.SUGAR.MessageBox.show({
                    msg: 'No record found from CIDAS.',
                    title: 'CIDAS Search',
                    type: 'alert'
                });
                return;
            } 
            addRow(result);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
    SUGAR.ajaxUI.hideLoadingPanel();
}
/**
 * Function to set the values on edit view of selected CIDAS data
 * 
 * @param object data CIDAS JSON Data
 * @author Shrikant.Gaware
 * @since Dec 17, 2015
 */
function sendBackData(data) {
    var result = $.parseJSON(data);
    for (var key in result) {
        var node = eval("window.opener.document.EditView." + key);
        if (typeof(node) != 'undefined') {
            if (key == 'customer_status_code_c') {
                node[0].value = result[key];
                if (result[key] == 1) {
                    node[1].checked = false;
                    node[2].checked = true;
                } else {
                    node[1].checked = true;
                    node[2].checked = false;
                }
                $('[name="customer_status_code_c"]:not(:checked)').each(function(){
                	$(this).attr('disabled', true);
                });
            } else {
                node.value = result[key];
                node.readOnly = true;
            }
        }
    }
    closePopup();
}
/**
 * Function to hide search form and load CIDAS result in edit corporate
 * 
 * @author Kamal.Thiyagarajan
 * @since May 17, 2016
 */
$(document).ready(function() {
    var cid = $('#existingCId').val();
    if (cid != "") {
        $('.edit').hide();
        $('#commonCustomerId').val(cid);
        searchRecords();
    }
});