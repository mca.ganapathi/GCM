/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

/**
 * Function to open CIDAS Popup
 * 
 * @param string module_name Sugar Module Name <string> width - width
 *            for the popup window <string> height - height for the popup window
 * 
 * @return object win Pop-up window object
 * @author Shrikant.Gaware
 * @date Dec 17, 2015
 */
function cidasInput(module_name, width, height) {
    if (typeof(popupCount) == "undefined" || popupCount == 0) popupCount = 1;
    window.document.close_popup = true;
    width = (width == 600) ? 900 : width;
    height = (height == 400) ? 900 : height;
    var record = $('[name="record"]').val();
    var common_customer_id_c = $('#common_customer_id_c').val();
    URL = 'index.php?' + 'module=' + module_name + '&action=PopupCidas&sugar_body_only=true&record=' + record + "&common_customer_id=" + common_customer_id_c;
    windowName = module_name + '_popup_window' + popupCount;
    windowFeatures = 'width=' + width + ',height=' + height + ',resizable=1,scrollbars=1';
    popup_mode = 'single';
    URL += '&mode=' + popup_mode;
    win = SUGAR.util.openWindow(URL, windowName, windowFeatures);
    if (window.focus) {
        win.focus();
    }
    win.popupCount = popupCount;
    return win;
}
/**
 * Function to check duplicate product name
 *
 * @return boolean true/false
 * @author Shrikant.Gaware
 * @date Sep 30, 2015
 */
function checkDuplicateValidation() {
    SUGAR.ajaxUI.showLoadingPanel();
    var valid = false;
    var record = $('[name="record"]').val();
    var common_customer_id_c = $('#common_customer_id_c').val();
    $.ajax({
        type: "POST",
        url: "index.php",
        data: {
            module: 'Accounts',
            action: 'Actions',
            method: 'validateCorporateId',
            sugar_body_only: '1',
            record: record,
            common_customer_id_c: common_customer_id_c,
        },
        async: false,
        success: function(data) {
            var result = $.parseJSON(data);
            if (result.hasOwnProperty('error') || $.isEmptyObject(result)) valid = true;
            else if (result.exist == '1') valid = true;
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
    SUGAR.ajaxUI.hideLoadingPanel();
    return valid;
}
/**
 * Function to check added custom validations
 *
 * @return boolean true/false
 * @author Shrikant.Gaware
 * @date Sep 30, 2015
 */
function cstmValidation() {
    clear_all_errors();
    var error_count = 0;
    //Check atleast Corporate Name (Latin Character) or Corporate Name (Local Character 1) is present.
    if ($.trim($("#name").val()) == "" && $.trim($("#name_2_c").val()) == "") {
        add_error_style('EditView', 'name', SUGAR.language.get('Accounts', 'LBL_LATIN_LOCAL'), true);
        add_error_style('EditView', 'name_2_c', SUGAR.language.get('Accounts', 'LBL_LATIN_LOCAL'), true);
        error_count += 1;
    }
    if (checkDuplicateValidation()) {
        add_error_style('EditView', 'common_customer_id_c', SUGAR.language.get('Accounts', 'LBL_RECORD_EXIST'), true);
        error_count += 1;
    }
    //if any error return false
    if (error_count > 0) {
        return false;
    }
    return check_form('EditView');
}
/**
 * To make all fields readonly when cid is present OM#81
 *
 * @author kamal.thiyagarajan
 * @date May 17, 2016
 */
$(document).ready(function() {
    var cid = $.trim($("#common_customer_id_c").val());
    //common customer id shoould be readonly in create/edit form
    $("#common_customer_id_c").attr('readonly', true);
    //make inputbox,texarea and radio button readonly if cid is not empty
    if (cid != "") {
        $("#EditView input, textarea").attr('readonly', true);
        $('[name="customer_status_code_c"]:not(:checked)').each(function(){
        	$(this).attr('disabled', true);
        });
    }
});