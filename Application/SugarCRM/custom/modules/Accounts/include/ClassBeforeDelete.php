<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * Accounts before delete 
 */
class ClassAccountsBeforeSave
{

    /**
     * method to Check Relationships before deleting corporate record.
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author Dinesh.Itkar
     * @since Dec 23, 2015
     */
    public function checkCorporateRelationship($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'method to Check Relationships before deleting corporate record');

        $account = new Account();
        $account->retrieve($bean->id);
        $contacts     = $account->get_linked_beans('contacts', 'Contact');
        $gc_contracts = $account->get_linked_beans('accounts_gc_contracts_1', 'gc_Contracts');

        if (count($contacts) > 0 || count($gc_contracts) > 0) {
            $redirect_action = (isset($_REQUEST['record'])) ? 'DetailView' : 'ListView';
            $query_params    = array(
                'module'         => 'Accounts',
                'action'         => $redirect_action,
                'record'         => $arguments['id'],
                'offset'         => $_REQUEST['offset'],
                'stamp'          => $_REQUEST['stamp'],
                'return_module'  => $_REQUEST['return_module'],
                'account_count'  => count($contacts),
                'contract_count' => count($gc_contracts),
            );
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
            SugarApplication::redirect('index.php?' . http_build_query($query_params));
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}
