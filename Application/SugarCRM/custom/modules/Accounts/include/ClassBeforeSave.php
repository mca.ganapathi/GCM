<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * sample class file created for demo purpose
 */
class ClassAccountsBeforeSave
{
    /**
     * method to Set Corporate team to global.
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook . event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author Debasish.Gupta
     * @since Jun 08, 2016
     */
    public function saveCorporateTeam($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'method to Set Corporate team to global');
        $bean->team_id     = 1;
        $bean->team_set_id = 1;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}
