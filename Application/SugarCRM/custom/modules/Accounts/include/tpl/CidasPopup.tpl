{*
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

*}
<!-- BEGIN: main -->
<style>
    #subjectfield { height: 1.6em; }
</style>
<script src="cache/include/javascript/sugar_grp1_jquery.js?v=M0HsFp6bgQkCQSZUFi7QcA" type="text/javascript"></script>
<script src="cache/include/javascript/sugar_grp1_yui.js?v=M0HsFp6bgQkCQSZUFi7QcA" type="text/javascript"></script>
<script src="cache/include/javascript/sugar_grp1.js?v=M0HsFp6bgQkCQSZUFi7QcA" type="text/javascript"></script>
<script src="include/javascript/calendar.js?v=M0HsFp6bgQkCQSZUFi7QcA" type="text/javascript"></script>
<script src="include/javascript/sugar_3.js" type="text/javascript"></script>
<script src="include/javascript/popup_helper.js" type="text/javascript"></script>
<script src="include/javascript/sugarwidgets/SugarYUIWidgets.js" type="text/javascript"></script>
{JAVASCRIPT}
<script language="Javascript" src="include/javascript/tiny_mce/tiny_mce.js" type="text/javascript"></script>
<script language="Javascript" src="custom/modules/Accounts/include/js/CidasPopup.js" type="text/javascript"></script>
<script type="text/javascript">
    SUGAR.themes.loading_image      = 'themes/default/images/img_loading.gif';
</script>
<input id="existingCId" name="existingCId" type="hidden" value="{EXISTING_CID}">
    {SEARCH_FORM_HEADER}
    <table border="0" cellpadding="0" cellspacing="0" class="edit view" width="100%">
        <tr>
            <td>
                <form action="index.php" id="popup_query_form" method="post" name="popup_query_form">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td nowrap="nowrap" scope="row" width="10%">
                                            <label for="commonCustomerId">
                                                {MOD.LBL_COMMON_CUSTOMER_ID}
                                            </label>
                                        </td>
                                        <td nowrap="nowrap" width="30%">
                                            <input id="commonCustomerId" name="commonCustomerId" size="30" title="" type="text" value="">
                                            </input>
                                        </td>
                                        <td nowrap="nowrap" scope="row" width="10%">
                                            <label for="companyNameEng">
                                                {MOD.LBL_COMPANY_NAME_ENG}
                                            </label>
                                        </td>
                                        <td nowrap="nowrap" width="30%">
                                            <input id="companyNameEng" name="companyNameEng" size="30" title="" type="text" value="">
                                            </input>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap" scope="row" width="10%">
                                            <label for="companyName">
                                                {MOD.LBL_COMPANY_NAME_JP}
                                            </label>
                                        </td>
                                        <td nowrap="nowrap" width="30%">
                                            <input id="companyName" name="companyName" size="30" title="" type="text" value="">
                                            </input>
                                        </td>
                                    </tr>
                                </table>
                                <script>
                                    if (typeof(loadSSL_Scripts) == 'function') {
                                    loadSSL_Scripts();
                                }
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input class="button" id="search_form_submit" name="button" onclick="searchRecords()" title="{APP.LBL_SAVE_BUTTON_TITLE}" type="button" value="{APP.LBL_SEARCH}"/>
                                <input class="button" id="search_form_clear" onclick="SUGAR.searchForm.clear_form(this.form); return false;" title="{APP.LBL_CLEAR_BUTTON_TITLE}" type="reset" value="{APP.LBL_CLEAR_BUTTON_LABEL}"/>
                            </td>
                            <td align="right">
                            </td>
                        </tr>
                    </table>
                </form>
            </td>
        </tr>
    </table>
    {CIDAS_HEADER}
    <table border="0" cellpadding="0" cellspacing="0" class="list view" width="100%">
        <tr class="trHeader" height="20">
            <th nowrap="nowrap" scope="col">
                <div align="left" style="white-space: normal;" width="100%">
                    {MOD.LBL_COMMON_CUSTOMER_ID}
                </div>
            </th>
            <th nowrap="nowrap" scope="col">
                <div align="left" style="white-space: normal;" width="100%">
                    {MOD.LBL_COMPANY_NAME_ENG}
                </div>
            </th>
            <th nowrap="nowrap" scope="col">
                <div align="left" style="white-space: normal;" width="100%">
                    {MOD.LBL_COMPANY_NAME_JP}
                </div>
            </th>
            <th nowrap="nowrap" scope="col">
                <div align="left" style="white-space: normal;" width="100%">
                    {MOD.LBL_COUNTRY_NAME}
                </div>
            </th>
            <th nowrap="nowrap" scope="col">
                <div align="left" style="white-space: normal;" width="100%">
                    {MOD.LBL_HQ_ADD_ENG}
                </div>
            </th>
            <th nowrap="nowrap" scope="col">
                <div align="left" style="white-space: normal;" width="100%">
                    {MOD.LBL_HQ_ADD_JP}
                </div>
            </th>
        </tr>
        <tr class="oddListRowS1" height="20">
            <td align="center" colspan="6">
                {MOD.LBL_NO_RECORDS}
            </td>
        </tr>
    </table>
    <table class="hide">
        <tr class="oddListRowS1 listRowCopy hide" height="20">
            <td align="left" bgcolor="" class="oddListRowS1" scope="row" valign="top">
                <a class="CidasData common_customer_id_c" href="javascript:void(0)" onclick="send_back();">
                </a>
            </td>
            <td align="left" bgcolor="" class="oddListRowS1" scope="row" valign="top">
                <a class="CidasData name" href="javascript:void(0)" onclick="send_back();">
                </a>
            </td>
            <td align="left" bgcolor="" class="oddListRowS1" scope="row" valign="top">
                <a class="CidasData name_2_c" href="javascript:void(0)" onclick="send_back();">
                </a>
            </td>
            <td align="left" bgcolor="" class="oddListRowS1" scope="row" valign="top">
                <a class="CidasData country_name_c" href="javascript:void(0)" onclick="send_back();">
                </a>
            </td>
            <td align="left" bgcolor="" class="oddListRowS1" scope="row" valign="top">
                <a class="CidasData hq_addr_1_c" href="javascript:void(0)" onclick="send_back();">
                </a>
            </td>
            <td align="left" bgcolor="" class="oddListRowS1" scope="row" valign="top">
                <a class="CidasData hq_addr_2_c" href="javascript:void(0)" onclick="send_back();">
                </a>
            </td>
        </tr>
    </table>
    {jsLang}
{tinyjs}
    <!-- END: main -->
</input>