<?php
$viewdefs['Accounts']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'common_customer_id_c',
          'default' => true,
          'enabled' => true,
          'type' => 'varchar',
          'label' => 'LBL_COMMON_CUSTOMER_ID',
          'width' => '10%',
        ),
        1 => 
        array (
          'name' => 'name',
          'link' => true,
          'label' => 'LBL_LIST_ACCOUNT_NAME',
          'enabled' => true,
          'default' => true,
          'width' => '20%',
        ),
        2 => 
        array (
          'name' => 'name_2_c',
          'default' => true,
          'enabled' => true,
          'type' => 'varchar',
          'label' => 'LBL_NAME_2',
          'link' => true,
          'width' => '10%',
        ),
        3 => 
        array (
          'name' => 'name_3_c',
          'default' => true,
          'enabled' => true,
          'type' => 'varchar',
          'label' => 'LBL_NAME_3',
          'width' => '10%',
        ),
        4 => 
        array (
          'name' => 'date_entered',
          'type' => 'datetime',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'default' => true,
          'readonly' => true,
          'width' => '5%',
        ),
        5 => 
        array (
          'name' => 'created_by_name',
          'default' => true,
          'enabled' => true,
          'width' => '10%',
          'label' => 'LBL_CREATED',
        ),
        6 => 
        array (
          'name' => 'date_modified',
          'enabled' => true,
          'default' => true,
          'width' => '5%',
          'label' => 'LBL_DATE_MODIFIED',
        ),
        7 => 
        array (
          'name' => 'modified_by_name',
          'default' => true,
          'enabled' => true,
          'width' => '10%',
          'label' => 'LBL_MODIFIED',
        ),
        8 => 
        array (
          'name' => 'email',
          'label' => 'LBL_EMAIL_ADDRESS',
          'enabled' => true,
          'default' => false,
          'width' => '15%',
          'sortable' => false,
          'link' => true,
          'customCode' => '{$EMAIL_LINK}{$EMAIL}</a>',
        ),
      ),
    ),
  ),
);
