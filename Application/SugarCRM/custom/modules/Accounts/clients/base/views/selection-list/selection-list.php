<?php
$viewdefs['Accounts'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'link' => true,
                'label' => 'LBL_LIST_ACCOUNT_NAME',
                'enabled' => true,
                'default' => true,
              ),
              1 => 
              array (
                'name' => 'billing_address_city',
                'label' => 'LBL_LIST_CITY',
                'enabled' => true,
                'default' => false,
              ),
              2 => 
              array (
                'name' => 'billing_address_country',
                'label' => 'LBL_BILLING_ADDRESS_COUNTRY',
                'enabled' => true,
                'default' => false,
              ),
              3 => 
              array (
                'name' => 'phone_office',
                'label' => 'LBL_LIST_PHONE',
                'enabled' => true,
                'default' => false,
              ),
              4 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_LIST_ASSIGNED_USER',
                'id' => 'ASSIGNED_USER_ID',
                'enabled' => true,
                'default' => false,
              ),
              5 => 
              array (
                'name' => 'email',
                'label' => 'LBL_EMAIL_ADDRESS',
                'enabled' => true,
                'default' => false,
              ),
              6 => 
              array (
                'name' => 'date_entered',
                'type' => 'datetime',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'default' => true,
                'readonly' => true,
              ),
              7 => 
              array (
                'type' => 'varchar',
                'default' => true,
                'label' => 'LBL_COMMON_CUSTOMER_ID',
                'enabled' => true,
                'name' => 'common_customer_id_c',
              ),
              8 => 
              array (
                'type' => 'varchar',
                'default' => true,
                'label' => 'LBL_NAME_2',
                'enabled' => true,
                'name' => 'name_2_c',
              ),
              9 => 
              array (
                'type' => 'varchar',
                'default' => true,
                'label' => 'LBL_NAME_3',
                'enabled' => true,
                'name' => 'name_3_c',
              ),
              10 => 
              array (
                'type' => 'relate',
                'link' => 'created_by_link',
                'readonly' => true,
                'label' => 'LBL_CREATED',
                'default' => true,
                'enabled' => true,
                'name' => 'created_by_name',
              ),
              11 => 
              array (
                'type' => 'datetime',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_MODIFIED',
                'default' => true,
                'enabled' => true,
                'name' => 'date_modified',
              ),
              12 => 
              array (
                'type' => 'relate',
                'link' => 'modified_user_link',
                'readonly' => true,
                'label' => 'LBL_MODIFIED',
                'default' => true,
                'enabled' => true,
                'name' => 'modified_by_name',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
