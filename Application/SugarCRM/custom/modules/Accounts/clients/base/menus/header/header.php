<?php

$moduleName = 'Accounts';

$viewdefs[$moduleName]['base']['menu']['header'] = array(
    array(
        'route' => "#bwc/index.php?module=$moduleName&action=EditView",
        'label' => 'LNK_NEW_ACCOUNT',
        'acl_action' => 'create',
        'acl_module' => $moduleName,
        'icon' => 'fa fa-plus',
    ),
    array(
        'route' => "#bwc/index.php?module=$moduleName&action=ListView",
        'label' => 'LNK_ACCOUNT_LIST',
        'acl_action' => 'list',
        'acl_module' => $moduleName,
        'icon' => 'fa fa-bars',
    )
);

?>