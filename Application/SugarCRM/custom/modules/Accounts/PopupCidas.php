<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

/**
 * Code to display Popup for CIDAS records search
 *
 * @author Shrikant.Gaware
 * @since Dec 17, 2015
 */
require_once 'include/SugarTinyMCE.php';
global $app_strings;
global $app_list_strings;
global $curent_language;

$mod_strings = return_module_language($current_language, $currentModule);
// Generic Header for the Popup
echo insert_popup_header();
echo getClassicModuleTitle($mod_strings['LBL_MODULE_NAME'], array(), true);
// Initialization for new Tpl Template class
$xtpl = new XTemplate('custom/modules/Accounts/include/tpl/CidasPopup.tpl');
$xtpl->assign('MOD', $mod_strings);
$xtpl->assign('APP', $app_strings);

$xtpl->assign('CANCEL_SCRIPT', 'window.close()');
// Assigning tpl variables
if (isset($_REQUEST['return_module'])) {
    $xtpl->assign('RETURN_MODULE', $_REQUEST['return_module']);
}

if (isset($_REQUEST['return_action'])) {
    $xtpl->assign('RETURN_ACTION', $_REQUEST['return_action']);
}

if (isset($_REQUEST['return_id'])) {
    $xtpl->assign('RETURN_ID', $_REQUEST['return_id']);
}

if (empty($_REQUEST['return_id'])) {
    $xtpl->assign('RETURN_ACTION', 'index');
}
$xtpl->assign('INPOPUPWINDOW', 'true');
$xtpl->assign('PRINT_URL', 'index.php?' . $GLOBALS['request_string']);
$xtpl->assign('JAVASCRIPT', get_set_focus_js());

if (isset($_REQUEST['the_user_id'])) {
    $xtpl->assign('THE_USER_ID', $_REQUEST['the_user_id']);
}

if (empty($_REQUEST['record']) || empty($_REQUEST['common_customer_id']) || $_REQUEST['common_customer_id'] == "-") {
    $xtpl->assign('SEARCH_FORM_HEADER', get_form_header($mod_strings['CIDAS_SEARCH'], '', false));
} else {
    $xtpl->assign('EXISTING_CID', $_REQUEST['common_customer_id']);
}
$xtpl->assign('CIDAS_HEADER', get_form_header($mod_strings['LBL_CIDAS_INFO'], '', false));
$jsLang = getVersionedScript("cache/jsLanguage/{$GLOBALS['current_language']}.js", $GLOBALS['sugar_config']['js_lang_version']);
$xtpl->assign('jsLang', $jsLang);

$tiny = new SugarTinyMCE();
$xtpl->assign("tinyjs", $tiny->getInstance('sigText'));
// Displaying the output
$xtpl->parse('main.textarea');
$xtpl->parse('main');
$xtpl->out('main');
