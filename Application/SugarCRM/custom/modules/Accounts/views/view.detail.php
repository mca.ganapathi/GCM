<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

require_once 'include/MVC/View/views/view.detail.php';

/**
 * Accounts module details view
 */
class AccountsViewDetail extends ViewDetail
{

    /**
     * accounts details view
     */
    public function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'constructor');
        parent::__construct();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * accounts display method
     */
    public function display()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'accounts display method');
        unset($this->dv->defs['templateMeta']['form']['buttons'][1]); // Hide Duplicate button
        unset($this->dv->defs['templateMeta']['form']['buttons'][3]); // Hide Find Duplicate button
        unset($this->dv->defs['templateMeta']['form']['buttons'][4]); // Hide Get Data button

        if ((isset($_REQUEST['account_count']) && $_REQUEST['account_count'] > 0) || (isset($_REQUEST['contract_count']) && $_REQUEST['contract_count'] > 0)) {
            $err_str = "";

            $err_str .= "<p class='error'> Sorry! '" . $this->bean->name . "' linked with the ";
            if ($_REQUEST['account_count'] > 0) {
                $err_str .= "Accounts ( " . $_REQUEST['account_count'] . " Record(s) )";
            }
            if ($_REQUEST['account_count'] > 0 && $_REQUEST['contract_count'] > 0) {
                $err_str .= " and ";
            }

            if ($_REQUEST['contract_count'] > 0) {
                $err_str .= "Contract ( " . $_REQUEST['contract_count'] . " Record(s) )";
            }

            $err_str .= ". So we are unable to procees this request.</p>";
            echo $err_str;
        }
        parent::display();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Override the _displaySubPanels method to support subpanel customizations *
     */
    public function _displaySubPanels()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Override the _displaySubPanels method to support subpanel customizations');
        require_once 'custom/include/SubPanel/SubPanelTiles.php';
        $subpanel = new CustomSubPanelTiles($this->bean, $this->module);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts']['top_buttons'][0]); // hiding create
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts']['top_buttons'][1]); // hiding select
        echo $subpanel->display();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}
