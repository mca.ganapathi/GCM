<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (â€œMSAâ€�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/
require_once 'include/MVC/View/views/view.popup.php';

/**
 * Accounts module popup view
 */
class AccountsViewPopup extends ViewPopup
{

    /**
     * accounts custom popup view method
     */
    public function CustomViewPopup()
    {
        parent::ViewPopup();
    }

    /**
     * accounts pre display method
     */
    public function preDisplay()
    {
        parent::preDisplay();
    }
    
    /**
     * accounts display method
     */
    public function display()
    {
        global $popupMeta, $mod_strings;

        parent::display();

        //hide 'Create Corporate button from Corporates Popup Screen' -- Sagar.Salunkhe 11-1-2016
        $js = <<<EOQ
<script type="text/javascript">
$("#addformlink").addClass('hide');
</script>
EOQ;
        echo $js;
    }
}
