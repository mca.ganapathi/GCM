<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

require_once 'custom/include/MVC/View/views/view.list.php';

/**
 * Accounts module list view
 */
class AccountsViewList extends CustomViewList
{

    /**
     * accounts pre display method
     */
    public function preDisplay()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'accounts pre display method');
        parent::preDisplay();

        // Hide Mass Update action
        $this->lv->showMassupdateFields = false;

        // Hide Merge action
        $this->lv->mergeduplicates = false;

        // Hide Add To Target List action
        $this->lv->targetList = false;

        // Hide Quick Edit Pencil
        $this->lv->quickViewLinks = false;

        // Hide Export action
        $this->lv->export = false;

        // Hide Mail action
        $this->lv->email = false;

        if ((isset($_REQUEST['account_count']) && $_REQUEST['account_count'] > 0) || (isset($_REQUEST['contract_count']) && $_REQUEST['contract_count'] > 0)) {
            $err_str = "";

            $err_str .= "<p class='error'> Sorry! '" . $this->bean->name . "' linked with the ";
            if ($_REQUEST['account_count'] > 0) {
                $err_str .= "Accounts ( " . $_REQUEST['account_count'] . " Record(s) )";
            }
            if ($_REQUEST['account_count'] > 0 && $_REQUEST['contract_count'] > 0) {
                $err_str .= " and ";
            }

            if ($_REQUEST['contract_count'] > 0) {
                $err_str .= "Contract ( " . $_REQUEST['contract_count'] . " Record(s) )";
            }

            $err_str .= ". So we are unable to procees this request.</p>";
            echo $err_str;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * accounts list view process method
     */
    public function listViewProcess()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'accounts list view process method');
        // Hide Delete action
        $this->lv->delete = false;

        $this->processSearchForm();
        $this->lv->searchColumns = $this->searchForm->searchColumns;
        //Filter corporate record which CID have empty and -
        $this->params['custom_where'] = ' AND accounts_cstm.common_customer_id_c != "-" AND  accounts_cstm.common_customer_id_c != "" ';
        if (!$this->headers) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
            return;
        }

        if (empty($_REQUEST['search_form_only']) || $_REQUEST['search_form_only'] == false) {
            $this->lv->ss->assign("SEARCH", true);
            $this->lv->setup($this->seed, 'custom/include/ListView/ListViewGeneric.tpl', $this->where, $this->params);
            $savedSearchName = empty($_REQUEST['saved_search_select_name']) ? '' : (' - ' . $_REQUEST['saved_search_select_name']);
            echo $this->lv->display();
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}
