<?php

$hook_version                  = 1;
$hook_array                    = array();
$hook_array['before_delete']   = array();
$hook_array['before_delete'][] = array(
    1,
    'Check Account and Contract relation',
    'custom/modules/Accounts/include/ClassBeforeDelete.php',
    'ClassAccountsBeforeSave',
    'checkCorporateRelationship',
);
$hook_array['before_save']   = array();
$hook_array['before_save'][] = array(
    1,
    'Update Corporate Teams',
    'custom/modules/Accounts/include/ClassBeforeSave.php',
    'ClassAccountsBeforeSave',
    'saveCorporateTeam',
);
