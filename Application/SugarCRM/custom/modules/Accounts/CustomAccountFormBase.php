<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
require_once 'modules/Accounts/AccountFormBase.php';

/**
 * CustomAccountFormBase extended from AccountFormBase for handling duplicate records
 */
class CustomAccountFormBase extends AccountFormBase
{

    /**
     * Method to handle duplicate records validations
     *
     * @param string $prefix
     * @param boolean $redirect
     * @param boolean $useRequired
     * @author Shrikant.Gaware
     * @since Dec 22, 2015
     */
    public function handleSave($prefix, $redirect = true, $useRequired = false)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'prefix : '.$prefix.GCM_GL_LOG_VAR_SEPARATOR.'redirect : '.$redirect.GCM_GL_LOG_VAR_SEPARATOR.'useRequired : '.$useRequired, 'Method to handle duplicate records validations');
        require_once 'include/formbase.php';
        global $mod_strings;
        $focus = new Account();

        if ($useRequired && !checkRequired($prefix, array_keys($focus->required_fields))) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'prefix : '.$prefix.GCM_GL_LOG_VAR_SEPARATOR.'redirect : '.$redirect.GCM_GL_LOG_VAR_SEPARATOR.'useRequired : '.$useRequired, '');
            return null;
        }
        $focus = populateFromPost($prefix, $focus);

        if (isset($GLOBALS['check_notify'])) {
            $check_notify = $GLOBALS['check_notify'];
        } else {
            $check_notify = false;
        }

        if (empty($_REQUEST['dup_checked']) && $focus->common_customer_id_c != '') {
            $record        = $_POST['record'];
            $obj_corp_bean = BeanFactory::getBean('Accounts');
            $result        = $obj_corp_bean->get_full_list('', "common_customer_id_c = '$focus->common_customer_id_c'");

            if ($result != null) {
                $ids = array();
                foreach ($result as $key => $value) {
                    $ids[] = $value->id;
                }

                $key = array_search($record, $ids);
                if (array_key_exists($key, $ids)) {
                    unset($ids[$key]);
                    $ids = array_values($ids);
                }
                if (sizeof($ids) > 0) {
                    $this->redirectAction($record, $focus->common_customer_id_c);
                }
            }
        }
        if (!$focus->ACLAccess('Save')) {
            ACLController::displayNoAccess(true);
            sugar_cleanup(true);
        }

        $focus->save($check_notify);
        $return_id = $focus->id;

        $GLOBALS['log']->debug("Saved record with id of " . $return_id);

        if (!empty($_POST['is_ajax_call']) && $_POST['is_ajax_call'] == '1') {
            $json = getJSONobj();
            echo $json->encode(array(
                'status' => 'success',
                'get'    => '',
            ));
            $trackerManager = TrackerManager::getInstance();
            $timeStamp      = TimeDate::getInstance()->nowDb();
            if ($monitor = $trackerManager->getMonitor('tracker')) {
                $monitor->setValue('team_id', $GLOBALS['current_user']->getPrivateTeamID());
                $monitor->setValue('action', 'detailview');
                $monitor->setValue('user_id', $GLOBALS['current_user']->id);
                $monitor->setValue('module_name', 'Accounts');
                $monitor->setValue('date_modified', $timeStamp);
                $monitor->setValue('visible', 1);

                if (!empty($this->bean->id)) {
                    $monitor->setValue('item_id', $return_id);
                    $monitor->setValue('item_summary', $focus->get_summary_text());
                }
                $trackerManager->saveMonitor($monitor, true, true);
            }
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'prefix : '.$prefix.GCM_GL_LOG_VAR_SEPARATOR.'redirect : '.$redirect.GCM_GL_LOG_VAR_SEPARATOR.'useRequired : '.$useRequired, '');
            return null;
        }

        if (isset($_POST['popup']) && $_POST['popup'] == 'true') {
            $urlData = array(
                "query"  => true,
                "name"   => $focus->name,
                "module" => 'Accounts',
                'action' => 'Popup',
            );
            if (!empty($_POST['return_module'])) {
                $urlData['module'] = $_POST['return_module'];
            }
            if (!empty($_POST['return_action'])) {
                $urlData['action'] = $_POST['return_action'];
            }
            foreach (array('return_id', 'popup', 'create', 'to_pdf') as $var) {
                if (!empty($_POST[$var])) {
                    $urlData[$var] = $_POST[$var];
                }
            }
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'prefix : '.$prefix.GCM_GL_LOG_VAR_SEPARATOR.'redirect : '.$redirect.GCM_GL_LOG_VAR_SEPARATOR.'useRequired : '.$useRequired, '');
            header("Location: index.php?" . http_build_query($urlData));
            return;
        }
        if ($redirect) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'prefix : '.$prefix.GCM_GL_LOG_VAR_SEPARATOR.'redirect : '.$redirect.GCM_GL_LOG_VAR_SEPARATOR.'useRequired : '.$useRequired, '');
            handleRedirect($return_id, 'Accounts');
        } else {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'prefix : '.$prefix.GCM_GL_LOG_VAR_SEPARATOR.'redirect : '.$redirect.GCM_GL_LOG_VAR_SEPARATOR.'useRequired : '.$useRequired, '');
            return $focus;
        }
    }

    /**
     * Method for page redirection
     *
     * @param string $record
     * @param string $common_customer_id_c
     * @author Shrikant.Gaware
     * @since Dec 22, 2015
     */
    public function redirectAction($record, $common_customer_id_c)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'record : '.$record.GCM_GL_LOG_VAR_SEPARATOR.'common_customer_id_c : '.$common_customer_id_c, 'Method for page redirection');
        global $mod_strings;
        if ($record) {
            $queryParams = array(
                'module' => 'Accounts',
                'action' => 'EditView',
                'record' => $record,
            );
        } else {
            $queryParams = array(
                'module' => 'Accounts',
                'action' => 'EditView',
            );

        }
        SugarApplication::appendErrorMessage('<pre><span style="text-decoration:bold;color:red">"' . $common_customer_id_c . '" ' . $mod_strings['LBL_RECORD_EXIST'] . '</span></pre>');
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        SugarApplication::redirect('index.php?' . http_build_query($queryParams));
    }
}
