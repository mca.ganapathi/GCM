<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
include_once 'custom/include/ModUtils.php';

/**
 * Actions Class to Handle Ajax Request
 */
class Actions extends ModUtils
{

    /**
     * Method to get CIDAS data based on entered search string
     *
     * @return array ('exist' => '0|1')
     * @author Shrikant.Gaware
     * @since Sep 24, 2015
     */
    public function getCidasData()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method to get CIDAS data based on entered search string');
        $corporate_id         = isset($_REQUEST['commonCustomerId']) ? trim($_REQUEST['commonCustomerId']) : '';
        $corporate_name_latin = isset($_REQUEST['companyNameEng']) ? trim($_REQUEST['companyNameEng']) : '';
        $corporate_name       = isset($_REQUEST['companyName']) ? trim($_REQUEST['companyName']) : '';
        $data                 = array();

        if (!empty($corporate_id)) {
            require_once 'custom/service/v4_1_custom/APIHelper/ClassCIDASConnection.php';
            $obj_cidas = new ClassCIDASConnection();
            $data      = $obj_cidas->getCorporateDetailsFromCIDASByCID($corporate_id, true, '2');
        }

        if (!empty($corporate_name) || !empty($corporate_name_latin)) {
            require_once 'custom/service/v4_1_custom/APIHelper/ClassCIDASConnection.php';
            $obj_cidas = new ClassCIDASConnection();
            $data      = $obj_cidas->getCorporateDetailsFromCIDASByName($corporate_name, $corporate_name_latin, true);
        }

        $this->outputData($data);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method to check for duplicate Corporate Id
     *
     * @return array ('exist' => '0|1')
     * @author Shrikant.Gaware
     * @since Sep 24, 2015
     */
    public function validateCorporateId()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method to check for duplicate Corporate Id');
        $flag  = 0;
        $check = isset($_REQUEST['common_customer_id_c']) ? trim($_REQUEST['common_customer_id_c']) : '';
        if ($check != '') {
            $record = isset($_REQUEST['record']) ? trim($_REQUEST['record']) : '';
            $where  = " accounts_cstm.common_customer_id_c = '$check' AND accounts.id != '$record' ";
            $bean   = BeanFactory::getBean('Accounts')->get_list('accounts.name', $where, null, 1);
            if (sizeof($bean['list']) > 0) {
                $flag = 1;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        $this->outputData(array(
            'exist' => $flag,
        ));
    }
}

// Initialize action class object
$a = new Actions();
$a->process();
