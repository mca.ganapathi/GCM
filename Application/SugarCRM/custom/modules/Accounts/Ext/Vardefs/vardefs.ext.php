<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_gc_contracts_1_Accounts.php

// created: 2015-09-28 09:38:20
$dictionary["Account"]["fields"]["accounts_gc_contracts_1"] = array (
  'name' => 'accounts_gc_contracts_1',
  'type' => 'link',
  'relationship' => 'accounts_gc_contracts_1',
  'source' => 'non-db',
  'module' => 'gc_Contracts',
  'bean_name' => 'gc_Contracts',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_common_customer_id_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['common_customer_id_c']['labelValue'] = 'Common Customer ID';
$dictionary['Account']['fields']['common_customer_id_c']['enforced'] = '';
$dictionary['Account']['fields']['common_customer_id_c']['dependency'] = '';
$dictionary['Account']['fields']['common_customer_id_c']['required'] = false;
$dictionary['Account']['fields']['common_customer_id_c']['audited'] = true;
$dictionary['Account']['fields']['common_customer_id_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_country_code_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['country_code_c']['labelValue'] = 'Country Name Code';
$dictionary['Account']['fields']['country_code_c']['enforced'] = '';
$dictionary['Account']['fields']['country_code_c']['dependency'] = '';
$dictionary['Account']['fields']['country_code_c']['required'] = false;
$dictionary['Account']['fields']['country_code_c']['audited'] = true;
$dictionary['Account']['fields']['country_code_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_country_name_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['country_name_c']['labelValue'] = 'Country Name';
$dictionary['Account']['fields']['country_name_c']['enforced'] = '';
$dictionary['Account']['fields']['country_name_c']['dependency'] = '';
$dictionary['Account']['fields']['country_name_c']['required'] = false;
$dictionary['Account']['fields']['country_name_c']['audited'] = true;
$dictionary['Account']['fields']['country_name_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_customer_status_code_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['customer_status_code_c']['labelValue'] = 'Customer Status Code';
$dictionary['Account']['fields']['customer_status_code_c']['dependency'] = '';
$dictionary['Account']['fields']['customer_status_code_c']['visibility_grid'] = '';
$dictionary['Account']['fields']['customer_status_code_c']['required'] = true;
$dictionary['Account']['fields']['customer_status_code_c']['audited'] = true;


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_hq_addr_1_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['hq_addr_1_c']['labelValue'] = 'Headquarter Address (Latin Character)';
$dictionary['Account']['fields']['hq_addr_1_c']['enforced'] = '';
$dictionary['Account']['fields']['hq_addr_1_c']['dependency'] = '';
$dictionary['Account']['fields']['hq_addr_1_c']['required'] = false;
$dictionary['Account']['fields']['hq_addr_1_c']['audited'] = true;
$dictionary['Account']['fields']['hq_addr_1_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_hq_addr_2_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['hq_addr_2_c']['labelValue'] = 'Headquarter Address (Local Character)';
$dictionary['Account']['fields']['hq_addr_2_c']['enforced'] = '';
$dictionary['Account']['fields']['hq_addr_2_c']['dependency'] = '';
$dictionary['Account']['fields']['hq_addr_2_c']['required'] = false;
$dictionary['Account']['fields']['hq_addr_2_c']['audited'] = true;
$dictionary['Account']['fields']['hq_addr_2_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_hq_postal_no_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['hq_postal_no_c']['labelValue'] = 'Headquarter Postal Number';
$dictionary['Account']['fields']['hq_postal_no_c']['enforced'] = '';
$dictionary['Account']['fields']['hq_postal_no_c']['dependency'] = '';
$dictionary['Account']['fields']['hq_postal_no_c']['required'] = false;
$dictionary['Account']['fields']['hq_postal_no_c']['audited'] = true;
$dictionary['Account']['fields']['hq_postal_no_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_name.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['name']['len'] = '100';
$dictionary['Account']['fields']['name']['help'] = 'Corporate name in Latin character (English)';
$dictionary['Account']['fields']['name']['comments'] = 'Corporate name in Latin character (English)';
$dictionary['Account']['fields']['name']['merge_filter'] = 'disabled';
$dictionary['Account']['fields']['name']['calculated'] = false;
$dictionary['Account']['fields']['name']['reportable'] = false;
$dictionary['Account']['fields']['name']['required'] = false;
$dictionary['Account']['fields']['name']['audited'] = true;
$dictionary['Account']['fields']['name']['full_text_search']['enabled'] = true;
$dictionary['Account']['fields']['name']['full_text_search']['searchable'] = true;
$dictionary['Account']['fields']['name']['full_text_search']['boost'] = 1.9099999999999999200639422269887290894985198974609375;


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_name_2_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['name_2_c']['labelValue'] = 'Corporate  Name (Local Character 1)';
$dictionary['Account']['fields']['name_2_c']['enforced'] = '';
$dictionary['Account']['fields']['name_2_c']['dependency'] = '';
$dictionary['Account']['fields']['name_2_c']['required'] = false;
$dictionary['Account']['fields']['name_2_c']['audited'] = true;
$dictionary['Account']['fields']['name_2_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_name_3_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['name_3_c']['labelValue'] = 'Corporate  Name (Local Character 2)';
$dictionary['Account']['fields']['name_3_c']['enforced'] = '';
$dictionary['Account']['fields']['name_3_c']['dependency'] = '';
$dictionary['Account']['fields']['name_3_c']['required'] = false;
$dictionary['Account']['fields']['name_3_c']['audited'] = true;
$dictionary['Account']['fields']['name_3_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_reg_addr_1_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['reg_addr_1_c']['labelValue'] = 'Registered Address (Latin Character)';
$dictionary['Account']['fields']['reg_addr_1_c']['enforced'] = '';
$dictionary['Account']['fields']['reg_addr_1_c']['dependency'] = '';
$dictionary['Account']['fields']['reg_addr_1_c']['required'] = false;
$dictionary['Account']['fields']['reg_addr_1_c']['audited'] = true;
$dictionary['Account']['fields']['reg_addr_1_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_reg_addr_2_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['reg_addr_2_c']['labelValue'] = 'Registered Address (Local Character)';
$dictionary['Account']['fields']['reg_addr_2_c']['enforced'] = '';
$dictionary['Account']['fields']['reg_addr_2_c']['dependency'] = '';
$dictionary['Account']['fields']['reg_addr_2_c']['required'] = false;
$dictionary['Account']['fields']['reg_addr_2_c']['audited'] = true;
$dictionary['Account']['fields']['reg_addr_2_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_reg_postal_no_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['reg_postal_no_c']['labelValue'] = 'Registered Postal Number';
$dictionary['Account']['fields']['reg_postal_no_c']['enforced'] = '';
$dictionary['Account']['fields']['reg_postal_no_c']['dependency'] = '';
$dictionary['Account']['fields']['reg_postal_no_c']['required'] = false;
$dictionary['Account']['fields']['reg_postal_no_c']['audited'] = true;
$dictionary['Account']['fields']['reg_postal_no_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_vat_no_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['vat_no_c']['duplicate_merge_dom_value'] = 0;
$dictionary['Account']['fields']['vat_no_c']['labelValue'] = 'VAT Number';
$dictionary['Account']['fields']['vat_no_c']['enforced'] = '';
$dictionary['Account']['fields']['vat_no_c']['dependency'] = '';
$dictionary['Account']['fields']['vat_no_c']['audited'] = false;
$dictionary['Account']['fields']['vat_no_c']['full_text_search']['boost'] = 1;


?>
