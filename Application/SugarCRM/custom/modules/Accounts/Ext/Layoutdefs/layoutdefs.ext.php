<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Layoutdefs/accounts_gc_contracts_1_Accounts.php

 // created: 2015-09-28 09:38:17
$layout_defs["Accounts"]["subpanel_setup"]['accounts_gc_contracts_1'] = array (
  'order' => 100,
  'module' => 'gc_Contracts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
  'get_subpanel_data' => 'accounts_gc_contracts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Layoutdefs/custom_remove_subpanel.php


//Disable unnecessary subpanel from corporate detailview

unset($layout_defs["Accounts"]["subpanel_setup"]['insideview']);    
unset($layout_defs["Accounts"]["subpanel_setup"]['activities']);
unset($layout_defs["Accounts"]["subpanel_setup"]['history']); 
unset($layout_defs["Accounts"]["subpanel_setup"]['documents']); 
unset($layout_defs["Accounts"]["subpanel_setup"]['opportunities']); 
unset($layout_defs["Accounts"]["subpanel_setup"]['quotes']); 
unset($layout_defs["Accounts"]["subpanel_setup"]['campaigns']);
unset($layout_defs["Accounts"]["subpanel_setup"]['accounts']);
unset($layout_defs["Accounts"]["subpanel_setup"]['leads']);
unset($layout_defs["Accounts"]["subpanel_setup"]['cases']);
unset($layout_defs["Accounts"]["subpanel_setup"]['accounts_gc_contracts_1']);


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Layoutdefs/_overrideAccount_subpanel_account_subpanel.php

//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['account_subpanel']['override_subpanel_name'] = 'Account_subpanel_account_subpanel';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Layoutdefs/_overrideAccount_subpanel_contacts.php

//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['contacts']['override_subpanel_name'] = 'Account_subpanel_contacts';

?>
