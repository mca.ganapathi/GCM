<?php
// WARNING: The contents of this file are auto-generated.


// created: 2016-09-29 04:41:43
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][] = array (
  'label' => 'LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
  'context' => 
  array (
    'link' => 'accounts_gc_contracts_1',
  ),
  'layout' => 'subpanel',
);

// created: 2016-09-29 04:41:43
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][] = array (
  'override_subpanel_list_view' => 
  array (
    'view' => 'subpanel-for-accounts',
    'link' => 'contacts',
  ),
);