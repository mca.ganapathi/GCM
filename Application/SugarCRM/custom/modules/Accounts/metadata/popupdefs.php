<?php
// get the value of 'date entered' custom field
$date_entered_custom_advanced = "";
if (isset($_REQUEST['date_entered_custom_advanced']) && $_REQUEST['date_entered_custom_advanced'] != "") {
    $date_entered_custom_advanced = $_REQUEST['date_entered_custom_advanced'];
}

// get the value of 'date_modified' custom field
$date_modified_custom_advanced = "";
if (isset($_REQUEST['date_modified_custom_advanced']) && $_REQUEST['date_modified_custom_advanced'] != "") {
    $date_modified_custom_advanced = $_REQUEST['date_modified_custom_advanced'];
}

$popupMeta = array(
    'moduleMain'   => 'Account',
    'varName'      => 'ACCOUNT',
    'orderBy'      => 'name',
    'whereClauses' => array(
        'name'                 => 'accounts.name',
        'common_customer_id_c' => 'accounts_cstm.common_customer_id_c',
        'name_2_c'             => 'accounts_cstm.name_2_c',
        'name_3_c'             => 'accounts_cstm.name_3_c',
        'date_entered'         => 'accounts.date_entered',
        'created_by_name'      => 'accounts.created_by_name',
        'date_modified'        => 'accounts.date_modified',
        'modified_by_name'     => 'accounts.modified_by_name',
    ),
    'searchInputs' => array(
        0 => 'name',
        3 => 'common_customer_id_c',
        4 => 'name_2_c',
        5 => 'name_3_c',
        6 => 'date_entered',
        7 => 'created_by_name',
        8 => 'date_modified',
        9 => 'modified_by_name',
    ),
    'create'       => array(
        'formBase'          => 'AccountFormBase.php',
        'formBaseClass'     => 'AccountFormBase',
        'getFormBodyParams' => array(
            0 => '',
            1 => '',
            2 => 'AccountSave',
        ),
        'createButton'      => 'LNK_NEW_ACCOUNT',
    ),
    'searchdefs'   => array(
        'common_customer_id_c' => array(
            'type'  => 'varchar',
            'label' => 'LBL_COMMON_CUSTOMER_ID',
            'width' => '10%',
            'name'  => 'common_customer_id_c',
        ),
        'name'                 => array(
            'name'  => 'name',
            'width' => '10%',
        ),
        'name_2_c'             => array(
            'type'  => 'varchar',
            'label' => 'LBL_NAME_2',
            'width' => '10%',
            'name'  => 'name_2_c',
        ),
        'name_3_c'             => array(
            'type'  => 'varchar',
            'label' => 'LBL_NAME_3',
            'width' => '10%',
            'name'  => 'name_3_c',
        ),
        'date_entered_custom'  => array(
            'type'  => 'datetime',
            'label' => 'LBL_DATE_ENTERED',
            'width' => '10%',
            'name'  => 'date_entered_custom',
            'value' => $date_entered_custom_advanced,
        ),
        'date_modified_custom' => array(
            'type'  => 'datetime',
            'label' => 'LBL_DATE_MODIFIED',
            'width' => '10%',
            'name'  => 'date_modified_custom',
            'value' => $date_modified_custom_advanced,
        ),
        'created_by'           => array(
            'type'  => 'assigned_user_name',
            'label' => 'LBL_CREATED',
            'id'    => 'CREATED_BY',
            'width' => '10%',
            'name'  => 'created_by',
        ),
        'modified_user_id'     => array(
            'type'    => 'assigned_user_name',
            'label'   => 'LBL_MODIFIED_NAME',
            'width'   => '10%',
            'default' => true,
            'name'    => 'modified_user_id',
        ),
    ),
    'listviewdefs' => array(
        'COMMON_CUSTOMER_ID_C' => array(
            'type'    => 'varchar',
            'default' => true,
            'label'   => 'LBL_COMMON_CUSTOMER_ID',
            'width'   => '10%',
        ),
        'NAME'                 => array(
            'width'   => '40%',
            'label'   => 'LBL_LIST_ACCOUNT_NAME',
            'link'    => true,
            'default' => true,
            'name'    => 'name',
        ),
        'NAME_2_C'             => array(
            'type'    => 'varchar',
            'default' => true,
            'label'   => 'LBL_NAME_2',
            'width'   => '10%',
            'link'    => true,
        ),
        'NAME_3_C'             => array(
            'type'    => 'varchar',
            'default' => true,
            'label'   => 'LBL_NAME_3',
            'width'   => '10%',
        ),
        'DATE_ENTERED'         => array(
            'type'    => 'datetime',
            'label'   => 'LBL_DATE_ENTERED',
            'width'   => '10%',
            'default' => true,
        ),
        'CREATED_BY_NAME'      => array(
            'type'    => 'relate',
            'link'    => true,
            'label'   => 'LBL_CREATED',
            'id'      => 'id_c',
            'width'   => '10%',
            'default' => true,
        ),
        'DATE_MODIFIED'        => array(
            'type'    => 'datetime',
            'label'   => 'LBL_DATE_MODIFIED',
            'width'   => '10%',
            'default' => true,
        ),
        'MODIFIED_BY_NAME'     => array(
            'type'    => 'relate',
            'link'    => true,
            'label'   => 'LBL_MODIFIED_NAME',
            'id'      => 'id_c',
            'width'   => '10%',
            'default' => true,
        ),
    ),
);

/*
 * Natarajan Muthusamy
 * OM-81 : Do not Display the records in which Common Sucstomer ID does not exit
 * 17 May, 2016
 *
 */
// add 'date entered' and 'date modified' custom fields with the where statement to filter the result.
$popupMeta['whereStatement'] = ' accounts_cstm.common_customer_id_c != "-" AND accounts_cstm.common_customer_id_c != "" AND accounts_cstm.common_customer_id_c IS NOT NULL ' . (($date_entered_custom_advanced != "") ? ' AND DATE_FORMAT(accounts.date_entered, "%Y-%m-%d") = "' . date("Y-m-d", strtotime($date_entered_custom_advanced)) . '" ' : '') . (($date_modified_custom_advanced != "") ? ' AND DATE_FORMAT(accounts.date_modified, "%Y-%m-%d") = "' . date("Y-m-d", strtotime($date_modified_custom_advanced)) . '" ' : '');
