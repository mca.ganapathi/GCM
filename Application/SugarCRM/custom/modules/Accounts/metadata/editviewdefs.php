<?php
$viewdefs['Accounts'] = array(
    'EditView' => array(
        'templateMeta' => array(
            'form'                => array(
                'hidden'  => array(
                    0 => '<input type="hidden" name="customer_status_code_c" id="customer_status_code_c_radio" value="0"/>',
                ),
                'buttons' => array(
                    0 => array(
                        'customCode' => '<input type="submit" name="save" id="save" onClick="this.form.return_action.value=\'DetailView\';
                this.form.action.value=\'Save\'; return cstmValidation();" value="Save">',
                    ),
                    1 => 'CANCEL',
                    2 => array(
                        'customCode' => '<input type="button" onclick="cidasInput( \'Accounts\', 900, 900);" value="CIDAS Search"/>',
                    ),
                ),
            ),
            'maxColumns'          => '2',
            'useTabs'             => false,
            'widths'              => array(
                0 => array(
                    'label' => '20',
                    'field' => '30',
                ),
                1 => array(
                    'label' => '20',
                    'field' => '30',
                ),
            ),
            'includes'            => array(
                0 => array(
                    'file' => 'modules/Accounts/Account.js',
                ),
                1 => array(
                    'file' => 'custom/modules/Accounts/include/js/EditView.js',
                ),
            ),
            'tabDefs'             => array(
                'LBL_PANEL_ADVANCED'  => array(
                    'newTab'       => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL2' => array(
                    'newTab'       => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL3' => array(
                    'newTab'       => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL4' => array(
                    'newTab'       => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => false,
        ),
        'panels'       => array(
            'LBL_PANEL_ADVANCED'  => array(
                0 => array(
                    0 => array(
                        'name'  => 'common_customer_id_c',
                        'label' => 'LBL_COMMON_CUSTOMER_ID',
                    ),
                    1 => array(
                        'name'   => 'customer_status_code_c',
                        'studio' => 'visible',
                        'label'  => 'LBL_CUSTOMER_STATUS_CODE',
                    ),
                ),
            ),
            'lbl_editview_panel2' => array(
                0 => array(
                    0 => array(
                        'name'  => 'name',
                        'label' => 'LBL_NAME',
                    ),
                    1 => '',
                ),
                1 => array(
                    0 => array(
                        'name'  => 'name_2_c',
                        'label' => 'LBL_NAME_2',
                    ),
                    1 => array(
                        'name'  => 'name_3_c',
                        'label' => 'LBL_NAME_3',
                    ),
                ),
            ),
            'lbl_editview_panel3' => array(
                0 => array(
                    0 => array(
                        'name'          => 'hq_addr_1_c',
                        'studio'        => 'visible',
                        'label'         => 'LBL_HQ_ADDR_1',
                        'displayParams' => array(
                            'rows'  => 4,
                            'cols'  => 50,
                            'field' => array(
                                'maxlength' => '400',
                            ),
                        ),
                    ),
                    1 => array(
                        'name'          => 'hq_addr_2_c',
                        'studio'        => 'visible',
                        'label'         => 'LBL_HQ_ADDR_2',
                        'displayParams' => array(
                            'rows'  => 4,
                            'cols'  => 50,
                            'field' => array(
                                'maxlength' => '160',
                            ),
                        ),
                    ),
                ),
                1 => array(
                    0 => array(
                        'name'  => 'hq_postal_no_c',
                        'label' => 'LBL_HQ_POSTAL_NO',
                    ),
                    1 => '',
                ),
                2 => array(
                    0 => array(
                        'name'          => 'reg_addr_1_c',
                        'studio'        => 'visible',
                        'label'         => 'LBL_REG_ADDR_1',
                        'displayParams' => array(
                            'rows'  => 4,
                            'cols'  => 50,
                            'field' => array(
                                'maxlength' => '400',
                            ),
                        ),
                    ),
                    1 => array(
                        'name'          => 'reg_addr_2_c',
                        'studio'        => 'visible',
                        'label'         => 'LBL_REG_ADDR_2',
                        'displayParams' => array(
                            'rows'  => 4,
                            'cols'  => 50,
                            'field' => array(
                                'maxlength' => '160',
                            ),
                        ),
                    ),
                ),
                3 => array(
                    0 => array(
                        'name'  => 'reg_postal_no_c',
                        'label' => 'LBL_REG_POSTAL_NO',
                    ),
                    1 => '',
                ),
                4 => array(
                    0 => array(
                        'name'  => 'country_name_c',
                        'label' => 'LBL_COUNTRY_NAME',
                    ),
                    1 => array(
                        'name'  => 'country_code_c',
                        'label' => 'LBL_COUNTRY_CODE',
                    ),
                ),
            ),
        ),
    ),
);
