<?php

$searchdefs['Accounts'] = array(
    'templateMeta' => array(
        'maxColumns'      => '3',
        'maxColumnsBasic' => '4',
        'widths'          => array(
            'label' => '10',
            'field' => '30',
        ),
    ),
    'layout'       => array(
        'basic_search'    => array(
            0 => array(
                'type'    => 'varchar',
                'default' => true,
                'label'   => 'LBL_COMMON_CUSTOMER_ID',
                'width'   => '10%',
                'name'    => 'common_customer_id_c',
            ),
            1 => array(
                'name'    => 'name',
                'default' => true,
                'width'   => '10%',
            ),
        ),
        'advanced_search' => array(
            0 => array(
                'type'    => 'varchar',
                'default' => true,
                'label'   => 'LBL_COMMON_CUSTOMER_ID',
                'width'   => '10%',
                'name'    => 'common_customer_id_c',
            ),
            1 => array(
                'name'    => 'name',
                'default' => true,
                'width'   => '10%',
            ),
            2 => array(
                'type'    => 'varchar',
                'default' => true,
                'label'   => 'LBL_NAME_2',
                'width'   => '10%',
                'name'    => 'name_2_c',
            ),
            3 => array(
                'type'    => 'varchar',
                'default' => true,
                'label'   => 'LBL_NAME_3',
                'width'   => '10%',
                'name'    => 'name_3_c',
            ),
            4 => array(
                'type'    => 'datetime',
                'label'   => 'LBL_DATE_ENTERED',
                'width'   => '10%',
                'default' => true,
                'name'    => 'date_entered',
            ),
            5 => array(
                'type'    => 'assigned_user_name',
                'label'   => 'LBL_CREATED',
                'width'   => '10%',
                'default' => true,
                'name'    => 'created_by',
            ),
            6 => array(
                'type'    => 'datetime',
                'label'   => 'LBL_DATE_MODIFIED',
                'width'   => '10%',
                'default' => true,
                'name'    => 'date_modified',
            ),
            7 => array(
                'type'    => 'assigned_user_name',
                'label'   => 'LBL_MODIFIED',
                'width'   => '10%',
                'default' => true,
                'name'    => 'modified_user_id',
            ),
        ),
    ),
);
