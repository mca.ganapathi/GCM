<?php
// created: 2016-09-29 04:39:05
$listViewDefs['Accounts'] = array(
    'COMMON_CUSTOMER_ID_C' => array(
        'type'    => 'varchar',
        'default' => true,
        'label'   => 'LBL_COMMON_CUSTOMER_ID',
        'width'   => '10%',
    ),
    'NAME'                 => array(
        'width'   => '20%',
        'label'   => 'LBL_LIST_ACCOUNT_NAME',
        'link'    => true,
        'default' => true,
    ),
    'NAME_2_C'             => array(
        'type'    => 'varchar',
        'default' => true,
        'label'   => 'LBL_NAME_2',
        'link'    => true,
        'width'   => '10%',
    ),
    'NAME_3_C'             => array(
        'type'    => 'varchar',
        'default' => true,
        'label'   => 'LBL_NAME_3',
        'width'   => '10%',
    ),
    'DATE_ENTERED'         => array(
        'width'   => '5%',
        'label'   => 'LBL_DATE_ENTERED',
        'default' => true,
    ),
    'CREATED_BY_NAME'      => array(
        'width'   => '10%',
        'label'   => 'LBL_CREATED',
        'default' => true,
    ),
    'DATE_MODIFIED'        => array(
        'width'   => '5%',
        'label'   => 'LBL_DATE_MODIFIED',
        'default' => true,
    ),
    'MODIFIED_BY_NAME'     => array(
        'width'   => '10%',
        'label'   => 'LBL_MODIFIED',
        'default' => true,
    ),
    'EMAIL'                => array(
        'width'      => '15%',
        'label'      => 'LBL_EMAIL_ADDRESS',
        'sortable'   => false,
        'link'       => true,
        'customCode' => '{$EMAIL_LINK}{$EMAIL}</a>',
        'default'    => false,
    ),
);
