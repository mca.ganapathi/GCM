<?php
$viewdefs['Accounts'] = array(
    'DetailView' => array(
        'templateMeta' => array(
            'form'                => array(
                'buttons' => array(
                    0 => 'EDIT',
                    1 => 'DUPLICATE',
                    2 => 'DELETE',
                    3 => 'FIND_DUPLICATES',
                ),
            ),
            'maxColumns'          => '2',
            'useTabs'             => false,
            'widths'              => array(
                0 => array(
                    'label' => '20',
                    'field' => '30',
                ),
                1 => array(
                    'label' => '20',
                    'field' => '30',
                ),
            ),
            'includes'            => array(
                0 => array(
                    'file' => 'modules/Accounts/Account.js',
                ),
            ),
            'tabDefs'             => array(
                'LBL_PANEL_ADVANCED'  => array(
                    'newTab'       => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL2' => array(
                    'newTab'       => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL3' => array(
                    'newTab'       => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL1' => array(
                    'newTab'       => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels'       => array(
            'LBL_PANEL_ADVANCED'  => array(
                0 => array(
                    0 => array(
                        'name'  => 'common_customer_id_c',
                        'label' => 'LBL_COMMON_CUSTOMER_ID',
                    ),
                    1 => array(
                        'name'   => 'customer_status_code_c',
                        'studio' => 'visible',
                        'label'  => 'LBL_CUSTOMER_STATUS_CODE',
                    ),
                ),
                1 => array(
                    0 => 'id',
                    1 => '',
                ),
            ),
            'lbl_editview_panel2' => array(
                0 => array(
                    0 => array(
                        'name'          => 'name',
                        'comment'       => 'Name of the Company',
                        'label'         => '{$MOD.LBL_NAME} <img title="{$fields.name.help}" src="themes/Sugar/images/info_inline.png?v=03QxjiIC-nlXIK1nio3fhA">',
                        'displayParams' => array(
                        ),
                    ),
                    1 => '',
                ),
                1 => array(
                    0 => array(
                        'name'  => 'name_2_c',
                        'label' => '{$MOD.LBL_NAME_2} <img title="{$fields.name_2_c.help}" src="themes/Sugar/images/info_inline.png?v=03QxjiIC-nlXIK1nio3fhA">',
                    ),
                    1 => array(
                        'name'  => 'name_3_c',
                        'label' => '{$MOD.LBL_NAME_3} <img title="{$fields.name_3_c.help}" src="themes/Sugar/images/info_inline.png?v=03QxjiIC-nlXIK1nio3fhA">',
                    ),
                ),
            ),
            'lbl_editview_panel3' => array(
                0 => array(
                    0 => array(
                        'name'   => 'hq_addr_1_c',
                        'studio' => 'visible',
                        'label'  => 'LBL_HQ_ADDR_1',
                    ),
                    1 => array(
                        'name'   => 'hq_addr_2_c',
                        'studio' => 'visible',
                        'label'  => 'LBL_HQ_ADDR_2',
                    ),
                ),
                1 => array(
                    0 => array(
                        'name'  => 'hq_postal_no_c',
                        'label' => 'LBL_HQ_POSTAL_NO',
                    ),
                    1 => '',
                ),
                2 => array(
                    0 => array(
                        'name'   => 'reg_addr_1_c',
                        'studio' => 'visible',
                        'label'  => 'LBL_REG_ADDR_1',
                    ),
                    1 => array(
                        'name'   => 'reg_addr_2_c',
                        'studio' => 'visible',
                        'label'  => 'LBL_REG_ADDR_2',
                    ),
                ),
                3 => array(
                    0 => array(
                        'name'  => 'reg_postal_no_c',
                        'label' => 'LBL_REG_POSTAL_NO',
                    ),
                    1 => '',
                ),
                4 => array(
                    0 => array(
                        'name'  => 'country_name_c',
                        'label' => 'LBL_COUNTRY_NAME',
                    ),
                    1 => array(
                        'name'  => 'country_code_c',
                        'label' => 'LBL_COUNTRY_CODE',
                    ),
                ),
            ),
            'lbl_editview_panel1' => array(
                0 => array(
                    0 => '',
                    1 => '',
                ),
                1 => array(
                    0 => array(
                        'name'       => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                    ),
                    1 => array(
                        'name'       => 'date_modified',
                        'label'      => 'LBL_DATE_MODIFIED',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                    ),
                ),
            ),
        ),
    ),
);
