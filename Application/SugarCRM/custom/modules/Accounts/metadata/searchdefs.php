<?php
$searchdefs['Accounts'] = array(
    'layout'       => array(
        'basic_search'    => array(
            'common_customer_id_c' => array(
                'type'    => 'varchar',
                'default' => true,
                'label'   => 'LBL_COMMON_CUSTOMER_ID',
                'width'   => '10%',
                'name'    => 'common_customer_id_c',
            ),
            'name'                 => array(
                'name'    => 'name',
                'default' => true,
                'width'   => '10%',
            ),
        ),
        'advanced_search' => array(
            'common_customer_id_c' => array(
                'type'    => 'varchar',
                'default' => true,
                'label'   => 'LBL_COMMON_CUSTOMER_ID',
                'width'   => '10%',
                'name'    => 'common_customer_id_c',
            ),
            'name'                 => array(
                'name'    => 'name',
                'default' => true,
                'width'   => '10%',
            ),
            'name_2_c'             => array(
                'type'    => 'varchar',
                'default' => true,
                'label'   => 'LBL_NAME_2',
                'width'   => '10%',
                'name'    => 'name_2_c',
            ),
            'name_3_c'             => array(
                'type'    => 'varchar',
                'default' => true,
                'label'   => 'LBL_NAME_3',
                'width'   => '10%',
                'name'    => 'name_3_c',
            ),
            'date_entered'         => array(
                'type'    => 'datetime',
                'label'   => 'LBL_DATE_ENTERED',
                'width'   => '10%',
                'default' => true,
                'name'    => 'date_entered',
            ),
            'created_by'           => array(
                'type'    => 'assigned_user_name',
                'label'   => 'LBL_CREATED',
                'width'   => '10%',
                'default' => true,
                'name'    => 'created_by',
            ),
            'date_modified'        => array(
                'type'    => 'datetime',
                'label'   => 'LBL_DATE_MODIFIED',
                'width'   => '10%',
                'default' => true,
                'name'    => 'date_modified',
            ),
            'modified_user_id'     => array(
                'type'    => 'assigned_user_name',
                'label'   => 'LBL_MODIFIED',
                'width'   => '10%',
                'default' => true,
                'name'    => 'modified_user_id',
            ),
        ),
    ),
    'templateMeta' => array(
        'maxColumns'      => '3',
        'maxColumnsBasic' => '4',
        'widths'          => array(
            'label' => '10',
            'field' => '30',
        ),
    ),
);
