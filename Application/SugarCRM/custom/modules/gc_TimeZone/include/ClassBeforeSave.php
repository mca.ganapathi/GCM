<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * class ClassGcTimeZoneBeforeSave
 */
class ClassGcTimeZoneBeforeSave
{

    /**
     * Method setGcTimeZoneAuditEntry to log Time Zone record.
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author dinesh.itkar
     * @since May 12, 2016
     */
    function setGcTimeZoneAuditEntry($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'to log Time Zone record');
        
        if (empty($bean->fetched_row['id'])) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
            return;
        }
        
        // Audit Country
        if ($bean->c_country_id_c != $bean->fetched_row['c_country_id_c']) {
            $obj_time_zone_bean = BeanFactory::getBean('c_country');
            $obj_time_zone_bean->retrieve($bean->fetched_row['c_country_id_c']);
            $before_country_name = ((!empty($obj_time_zone_bean->name))?$obj_time_zone_bean->name:'');
            $after_country_name = ((!empty($bean->country))?$bean->country:'');
            $this->insertGcTimeZoneAudit($bean, 'Country', 'text', $before_country_name, $after_country_name);
        }
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method insertGcTimeZoneAudit to Update Time Zone Audit record.
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param string $field_name field name to audit
     * @param string $data_type data type of field
     * @param string $before value before update
     * @param string $after value after update
     * @author dinesh.itkar
     * @since May 12, 2016
     */
    private function insertGcTimeZoneAudit($bean, $field_name, $data_type, $before, $after)
    {        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'field_name : '.$field_name.GCM_GL_LOG_VAR_SEPARATOR.'data_type : '.$data_type.GCM_GL_LOG_VAR_SEPARATOR.'before : '.$before.GCM_GL_LOG_VAR_SEPARATOR.'after : '.$after, 'to Update Time Zone Audit record');
        $audit_entry = array();
        $audit_entry['field_name'] = $field_name;
        $audit_entry['data_type'] = $data_type;
        $audit_entry['before'] = $before;
        $audit_entry['after'] = $after;
        $bean->db->save_audit_records($bean, $audit_entry);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}

?>