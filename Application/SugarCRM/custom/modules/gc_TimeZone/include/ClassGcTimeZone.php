<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * class ClassGcTimeZone
 */
class ClassGcTimeZone
{

    /**
     * Method getTimeZoneList to get list of timezones from database, based on provided country id(if country id is empty, it'll provide complete list timezones) 
     * 
     * @param string $country_id country object id
     * @return object $arr_timezones array of timezone objects
     * @author sagar.salunkhe
     * @since May 03, 2016
     */
    public function getTimeZoneList($country_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'country_id : '.$country_id, 'to get list of timezones from database, based on provided country id');
        $arr_timezones = array();
        
        if (!empty($country_id))
            $arr_timezones = BeanFactory::getBean('gc_TimeZone')->get_full_list('gc_timezone.name', " (gc_timezone.c_country_id_c = '$country_id') OR (gc_timezone.c_country_id_c = '' OR gc_timezone.c_country_id_c IS NULL) OR (gc_timezone.c_country_id_c IN (SELECT id FROM c_country WHERE deleted = 1))");
        else
            $arr_timezones = BeanFactory::getBean('gc_TimeZone')->get_full_list('gc_timezone.name');
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'country_id : '.$country_id, '');
        return $arr_timezones;
    }
}