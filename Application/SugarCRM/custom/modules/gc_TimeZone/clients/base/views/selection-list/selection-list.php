<?php
$viewdefs['gc_TimeZone'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
              2 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
              3 => 
              array (
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'default' => true,
                'name' => 'date_modified',
                'readonly' => true,
              ),
              4 => 
              array (
                'type' => 'relate',
                'studio' => 'visible',
                'label' => 'LBL_COUNTRY',
                'default' => true,
                'enabled' => true,
                'name' => 'country',
              ),
              5 => 
              array (
                'type' => 'varchar',
                'label' => 'LBL_UTCOFFSET',
                'default' => true,
                'enabled' => true,
                'name' => 'utcoffset',
              ),
              6 => 
              array (
                'type' => 'datetime',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
                'default' => true,
                'enabled' => true,
                'name' => 'date_entered',
              ),
              7 => 
              array (
                'type' => 'relate',
                'link' => 'created_by_link',
                'readonly' => true,
                'label' => 'LBL_CREATED',
                'default' => true,
                'enabled' => true,
                'name' => 'created_by_name',
              ),
              8 => 
              array (
                'type' => 'relate',
                'link' => 'modified_user_link',
                'readonly' => true,
                'label' => 'LBL_MODIFIED',
                'default' => true,
                'enabled' => true,
                'name' => 'modified_by_name',
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
