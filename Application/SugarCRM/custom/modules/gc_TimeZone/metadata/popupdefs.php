<?php
$popupMeta = array (
    'moduleMain' => 'gc_TimeZone',
    'varName' => 'gc_TimeZone',
    'orderBy' => 'gc_timezone.name',
    'whereClauses' => array (
  'name' => 'gc_timezone.name',
),
    'searchInputs' => array (
  0 => 'gc_timezone_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'COUNTRY' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_COUNTRY',
    'id' => 'C_COUNTRY_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
    'name' => 'country',
  ),
  'UTCOFFSET' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_UTCOFFSET',
    'width' => '10%',
    'default' => true,
    'name' => 'utcoffset',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
    'name' => 'date_entered',
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => true,
    'name' => 'created_by_name',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
    'name' => 'date_modified',
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => true,
    'name' => 'modified_by_name',
  ),
),
);
