<?php
// created: 2015-09-08 12:57:43
$mod_strings = array (
  'LBL_CONTACT_NAME' => 'Account Name:',
  'LBL_ACCOUNT_NAME' => 'Corporate Name:',
  'LBL_ACCOUNT' => 'Corporate',
  'LBL_CONTACT' => 'Account',
  'LBL_CONTACT_ID' => 'Account ID',
  'LBL_ACCOUNT_ID' => 'Corporate ID',
  'LBL_LIST_ACCOUNT_NAME' => 'Corporate Name',
);