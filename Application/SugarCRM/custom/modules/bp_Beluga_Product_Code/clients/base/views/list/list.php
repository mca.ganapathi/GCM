<?php
$viewdefs['bp_Beluga_Product_Code']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'gcm_product_item_id',
          'default' => true,
          'enabled' => true,
          'type' => 'relate',
          'studio' => 'visible',
          'ACLTag' => 'PI_PRODUCT_ITEM',
          'label' => 'LBL_GCM_PRODUCT_ITEM_ID',
          'id' => 'PI_PRODUCT_ITEM_ID_C',
          'link' => true,
          'width' => '10%',
          'related_fields' => 
          array (
            0 => 'pi_product_item_id_c',
          ),
        ),
        1 => 
        array (
          'name' => 'beluga_srv_type',
          'default' => true,
          'enabled' => true,
          'type' => 'varchar',
          'label' => 'LBL_BELUGA_SRV_TYPE',
          'width' => '10%',
        ),
        2 => 
        array (
          'name' => 'beluga_prd_type',
          'default' => true,
          'enabled' => true,
          'type' => 'varchar',
          'label' => 'LBL_BELUGA_PRD_TYPE',
          'width' => '10%',
        ),
        3 => 
        array (
          'name' => 'team_name',
          'label' => 'LBL_TEAM',
          'default' => false,
          'enabled' => true,
          'width' => '9%',
        ),
      ),
    ),
  ),
);
