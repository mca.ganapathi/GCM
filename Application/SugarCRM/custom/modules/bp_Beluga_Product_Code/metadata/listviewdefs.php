<?php
// created: 2018-03-07 11:45:26
$listViewDefs['bp_Beluga_Product_Code'] = array (
  'GCM_PRODUCT_ITEM_ID' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'module' => 'pi_product_item',
    'ACLTag' => 'PI_PRODUCT_ITEM',
    'label' => 'LBL_GCM_PRODUCT_ITEM_ID',
    'id' => 'PI_PRODUCT_ITEM_ID_C',
    'link' => true,
    'width' => '10',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'pi_product_item_id_c',
    ),
  ),
  'BELUGA_SRV_TYPE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BELUGA_SRV_TYPE',
    'width' => '10',
    'default' => true,
  ),
  'BELUGA_PRD_TYPE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BELUGA_PRD_TYPE',
    'width' => '10',
    'default' => true,
  ),
  'TEAM_NAME' => 
  array (
    'width' => '9',
    'label' => 'LBL_TEAM',
    'default' => false,
  ),
);