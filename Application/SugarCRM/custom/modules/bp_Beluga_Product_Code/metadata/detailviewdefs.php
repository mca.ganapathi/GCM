<?php
$module_name = 'bp_Beluga_Product_Code';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'gcm_product_item_id',
            'studio' => 'visible',
            'label' => 'LBL_GCM_PRODUCT_ITEM_ID',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'beluga_prd_type',
            'label' => 'LBL_BELUGA_PRD_TYPE',
          ),
          1 => 
          array (
            'name' => 'beluga_srv_type',
            'label' => 'LBL_BELUGA_SRV_TYPE',
          ),
        ),
      ),
    ),
  ),
);
?>
