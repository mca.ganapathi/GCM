<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/contacts_gc_contracts_1_Contacts.php

 // created: 2015-09-28 09:40:14
$layout_defs["Contacts"]["subpanel_setup"]['contacts_gc_contracts_1'] = array (
  'order' => 100,
  'module' => 'gc_Contracts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
  'get_subpanel_data' => 'contacts_gc_contracts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/customRemoveSubpanel.php

/*** START : Remove unnecessary subpanels that we can't hide from Admin Panel ***/

unset($layout_defs["Contacts"]["subpanel_setup"]['activities']);		//unset subpanel Activities
unset($layout_defs["Contacts"]["subpanel_setup"]['history']);			//unset subpanel History
unset($layout_defs["Contacts"]["subpanel_setup"]['contacts']);			//unset subpanel Direct Reports
unset($layout_defs["Contacts"]["subpanel_setup"]['documents']);			//unset subpanel Documents
unset($layout_defs["Contacts"]["subpanel_setup"]['opportunities']);		//unset subpanel opportunities
unset($layout_defs["Contacts"]["subpanel_setup"]['quotes']);			//unset subpanel quotes
unset($layout_defs["Contacts"]["subpanel_setup"]['products']);			//unset subpanel products
unset($layout_defs["Contacts"]["subpanel_setup"]['leads']);				//unset subpanel leads
unset($layout_defs["Contacts"]["subpanel_setup"]['campaigns']);			//unset subpanel campaigns
unset($layout_defs["Contacts"]["subpanel_setup"]['cases']);				//unset subpanel cases
unset($layout_defs["Contacts"]["subpanel_setup"]['bugs']);				//unset subpanel bugs
unset($layout_defs["Contacts"]["subpanel_setup"]['project']);			//unset subpanel project
unset($layout_defs["Contacts"]["subpanel_setup"]['contracts']);			//unset subpanel contracts
unset($layout_defs["Contacts"]["subpanel_setup"]['contacts_gc_contracts_1']['top_buttons'][0]);			//unset create button of contracts subpanel
unset($layout_defs["Contacts"]["subpanel_setup"]['contacts_gc_contracts_1']['top_buttons'][1]);			//unset select button of contracts subpanel

/*** END : Remove unnecessary subpanels that we can't hide from Admin Panel ***/
 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/_overrideContact_subpanel_contacts_gc_contracts_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['contacts_gc_contracts_1']['override_subpanel_name'] = 'Contact_subpanel_contacts_gc_contracts_1';

?>
