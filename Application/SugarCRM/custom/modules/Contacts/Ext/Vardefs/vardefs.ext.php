<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/contacts_gc_contracts_1_Contacts.php

// created: 2015-09-28 09:40:15
$dictionary["Contact"]["fields"]["contacts_gc_contracts_1"] = array (
  'name' => 'contacts_gc_contracts_1',
  'type' => 'link',
  'relationship' => 'contacts_gc_contracts_1',
  'source' => 'non-db',
  'module' => 'gc_Contracts',
  'bean_name' => 'gc_Contracts',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/ndb_CustomFields.php

// Accounts Module : Address 1 [Japan specific]
$dictionary['Contact']['fields']['ndb_addr1'] = array (
	'name' => 'ndb_addr1',
	'vname' => 'LBL_NDB_ADDR_1',
	'type' => 'text',
	'module' => 'Contacts',
	'enforced' => '',
	'dependency' => '',
	'required' => false,
	'massupdate' => '0',
	'default_value' => '',
	'no_default' => false,
	'help' => '',
	'comment' => '',
	'importable' => false,
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'reportable' => false,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'calculated' => false,
    'rows' => '4',
    'cols' => '60',
	'source' => 'non-db',
	'dbType' => 'non-db',
	'studio' => 'visible',
);

// Accounts Module : Address 2 [Japan specific]
$dictionary['Contact']['fields']['ndb_addr2'] = array (
	'name' => 'ndb_addr2',
	'vname' => 'LBL_NDB_ADDR_2',
	'type' => 'text',
	'module' => 'Contacts',
	'enforced' => '',
	'dependency' => '',
	'required' => false,
	'massupdate' => '0',
	'default_value' => '',
	'no_default' => false,
	'help' => '',
	'comment' => '',
	'importable' => false,
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'reportable' => false,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'calculated' => false,
    'rows' => '4',
    'cols' => '60',
	'source' => 'non-db',
	'dbType' => 'non-db',
	'studio' => 'visible',
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_addr_1_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['addr_1_c']['labelValue'] = 'Address (Latin Character)';
$dictionary['Contact']['fields']['addr_1_c']['enforced'] = '';
$dictionary['Contact']['fields']['addr_1_c']['dependency'] = '';
$dictionary['Contact']['fields']['addr_1_c']['audited'] = true;
$dictionary['Contact']['fields']['addr_1_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_addr_2_c.php

 // created: 2017-04-26 14:33:27
$dictionary['Contact']['fields']['addr_2_c']['labelValue']='Address (Local Character)';
$dictionary['Contact']['fields']['addr_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['addr_2_c']['enforced']='';
$dictionary['Contact']['fields']['addr_2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_addr_general_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['addr_general_c']['labelValue'] = 'Address [US/Canada Specific]';
$dictionary['Contact']['fields']['addr_general_c']['enforced'] = '';
$dictionary['Contact']['fields']['addr_general_c']['dependency'] = '';
$dictionary['Contact']['fields']['addr_general_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_attention_line_c.php

 // created: 2016-12-02 09:45:22
$dictionary['Contact']['fields']['attention_line_c']['labelValue']='Attention Line';
$dictionary['Contact']['fields']['attention_line_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['attention_line_c']['enforced']='';
$dictionary['Contact']['fields']['attention_line_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_city_general_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['city_general_c']['labelValue'] = 'City [US/Canada Specific]';
$dictionary['Contact']['fields']['city_general_c']['enforced'] = '';
$dictionary['Contact']['fields']['city_general_c']['dependency'] = '';
$dictionary['Contact']['fields']['city_general_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_contact_type_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['contact_type_c']['labelValue'] = 'Contact Type';
$dictionary['Contact']['fields']['contact_type_c']['dependency'] = '';
$dictionary['Contact']['fields']['contact_type_c']['visibility_grid'] = '';
$dictionary['Contact']['fields']['contact_type_c']['required'] = true;
$dictionary['Contact']['fields']['contact_type_c']['audited'] = true;
$dictionary['Contact']['fields']['contact_type_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_country_code_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['country_code_c']['labelValue'] = 'Country';
$dictionary['Contact']['fields']['country_code_c']['dependency'] = '';
$dictionary['Contact']['fields']['country_code_c']['audited'] = true;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_c_country_id_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['c_country_id_c']['duplicate_merge_dom_value'] = 0;
$dictionary['Contact']['fields']['c_country_id_c']['calculated'] = false;
$dictionary['Contact']['fields']['c_country_id_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_c_state_id_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['c_state_id_c']['duplicate_merge_dom_value'] = 0;
$dictionary['Contact']['fields']['c_state_id_c']['calculated'] = false;
$dictionary['Contact']['fields']['c_state_id_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_division_1_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['division_1_c']['labelValue'] = 'Division (Latin Character)';
$dictionary['Contact']['fields']['division_1_c']['enforced'] = '';
$dictionary['Contact']['fields']['division_1_c']['dependency'] = '';
$dictionary['Contact']['fields']['division_1_c']['audited'] = true;
$dictionary['Contact']['fields']['division_1_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_division_2_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['division_2_c']['labelValue'] = 'Division (Local Character)';
$dictionary['Contact']['fields']['division_2_c']['enforced'] = '';
$dictionary['Contact']['fields']['division_2_c']['dependency'] = '';
$dictionary['Contact']['fields']['division_2_c']['audited'] = true;
$dictionary['Contact']['fields']['division_2_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_do_not_call.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['do_not_call']['audited'] = false;
$dictionary['Contact']['fields']['do_not_call']['comments'] = 'An indicator of whether contact can be called';
$dictionary['Contact']['fields']['do_not_call']['merge_filter'] = 'disabled';
$dictionary['Contact']['fields']['do_not_call']['calculated'] = false;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_email1.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['email1']['len'] = '140';
$dictionary['Contact']['fields']['email1']['merge_filter'] = 'disabled';
$dictionary['Contact']['fields']['email1']['calculated'] = false;
$dictionary['Contact']['fields']['email1']['audited'] = true;
$dictionary['Contact']['fields']['email1']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_ext_no_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['ext_no_c']['labelValue'] = 'Extension number';
$dictionary['Contact']['fields']['ext_no_c']['enforced'] = '';
$dictionary['Contact']['fields']['ext_no_c']['dependency'] = '';
$dictionary['Contact']['fields']['ext_no_c']['audited'] = true;
$dictionary['Contact']['fields']['ext_no_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_first_name.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['first_name']['len'] = '60';
$dictionary['Contact']['fields']['first_name']['help'] = 'Contact Person in Latin character (English)';
$dictionary['Contact']['fields']['first_name']['comments'] = 'Contact Person in Latin character (English)';
$dictionary['Contact']['fields']['first_name']['merge_filter'] = 'disabled';
$dictionary['Contact']['fields']['first_name']['calculated'] = false;
$dictionary['Contact']['fields']['first_name']['full_text_search']['enabled'] = true;
$dictionary['Contact']['fields']['first_name']['full_text_search']['searchable'] = true;
$dictionary['Contact']['fields']['first_name']['full_text_search']['boost'] = 1.9899999999999999911182158029987476766109466552734375;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_last_name.php

 // created: 2017-04-19 10:17:11
$dictionary['Contact']['fields']['last_name']['len']='100';
$dictionary['Contact']['fields']['last_name']['help']='Contact Person in Latin character (English)';
$dictionary['Contact']['fields']['last_name']['comments']='Contact Person in Latin character (English)';
$dictionary['Contact']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Contact']['fields']['last_name']['calculated']=false;
$dictionary['Contact']['fields']['last_name']['required']=false;
$dictionary['Contact']['fields']['last_name']['audited']=true;
$dictionary['Contact']['fields']['last_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.97',
  'searchable' => true,
);
$dictionary['Contact']['fields']['last_name']['massupdate']=false;
$dictionary['Contact']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['last_name']['duplicate_merge_dom_value']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_name_2_c.php

 // created: 2017-04-19 10:18:37
$dictionary['Contact']['fields']['name_2_c']['labelValue']='Contact Person Name (Local Character)';
$dictionary['Contact']['fields']['name_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['name_2_c']['enforced']='';
$dictionary['Contact']['fields']['name_2_c']['dependency']='';
 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_fax.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['phone_fax']['len'] = '20';
$dictionary['Contact']['fields']['phone_fax']['comments'] = 'Contact fax number';
$dictionary['Contact']['fields']['phone_fax']['importable'] = 'false';
$dictionary['Contact']['fields']['phone_fax']['merge_filter'] = 'disabled';
$dictionary['Contact']['fields']['phone_fax']['reportable'] = false;
$dictionary['Contact']['fields']['phone_fax']['calculated'] = false;
$dictionary['Contact']['fields']['phone_fax']['audited'] = true;
$dictionary['Contact']['fields']['phone_fax']['full_text_search']['enabled'] = true;
$dictionary['Contact']['fields']['phone_fax']['full_text_search']['searchable'] = true;
$dictionary['Contact']['fields']['phone_fax']['full_text_search']['boost'] = 1.060000000000000053290705182007513940334320068359375;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_mobile.php

 // created: 2017-04-26 09:41:36
$dictionary['Contact']['fields']['phone_mobile']['len']='20';
$dictionary['Contact']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Contact']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_mobile']['calculated']=false;
$dictionary['Contact']['fields']['phone_mobile']['audited']=false;
$dictionary['Contact']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.09',
  'searchable' => true,
);
$dictionary['Contact']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge_dom_value']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_work.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['phone_work']['len'] = '20';
$dictionary['Contact']['fields']['phone_work']['comments'] = 'Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['merge_filter'] = 'disabled';
$dictionary['Contact']['fields']['phone_work']['calculated'] = false;
$dictionary['Contact']['fields']['phone_work']['audited'] = true;
$dictionary['Contact']['fields']['phone_work']['full_text_search']['enabled'] = true;
$dictionary['Contact']['fields']['phone_work']['full_text_search']['searchable'] = true;
$dictionary['Contact']['fields']['phone_work']['full_text_search']['boost'] = 1.0800000000000000710542735760100185871124267578125;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_postal_no_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['postal_no_c']['labelValue'] = 'Postal Number';
$dictionary['Contact']['fields']['postal_no_c']['enforced'] = '';
$dictionary['Contact']['fields']['postal_no_c']['dependency'] = '';
$dictionary['Contact']['fields']['postal_no_c']['audited'] = true;
$dictionary['Contact']['fields']['postal_no_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_state_general_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['state_general_c']['labelValue'] = 'State [US/Canada Specific]';
$dictionary['Contact']['fields']['state_general_c']['dependency'] = '';
$dictionary['Contact']['fields']['state_general_c']['audited'] = true;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_title.php

 // created: 2017-04-26 09:39:26
$dictionary['Contact']['fields']['title']['comments']='The title of the Contact in Latin character (English)';
$dictionary['Contact']['fields']['title']['merge_filter']='disabled';
$dictionary['Contact']['fields']['title']['calculated']=false;
$dictionary['Contact']['fields']['title']['audited']=false;
$dictionary['Contact']['fields']['title']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['title']['massupdate']=false;
$dictionary['Contact']['fields']['title']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['title']['duplicate_merge_dom_value']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_title_2_c.php

 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['title_2_c']['labelValue'] = 'Title (Local Character)';
$dictionary['Contact']['fields']['title_2_c']['enforced'] = '';
$dictionary['Contact']['fields']['title_2_c']['dependency'] = '';
$dictionary['Contact']['fields']['title_2_c']['audited'] = true;
$dictionary['Contact']['fields']['title_2_c']['full_text_search']['boost'] = 1;


?>
