<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/en_us.customcontacts_gc_contracts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CONTACTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE'] = 'Contracts';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/en_us.customLanguagePack.php

$mod_strings['LBL_NDB_ADDR_1'] = 'Address 1 [Japan specific]';
$mod_strings['LBL_NDB_ADDR_2'] = 'Address 2 [Japan specific]';
$mod_strings['ERR_PERSON_NAME_VALIDATION'] = 'either Latin or Local Characters';
$mod_strings['ERR_REQUIRED'] = 'Please enter';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CREATED_ACCOUNT'] = 'Created a new Corporate';
$mod_strings['LBL_EXISTING_ACCOUNT'] = 'Used an existing Corporate';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Select Corporate';
$mod_strings['NTC_OPPORTUNITY_REQUIRES_ACCOUNT'] = 'Creating an Opportunity requires an Corporate.\\n Please either create a new Corporate or select an existing one.';
$mod_strings['LBL_ATTENTION_LINE_C'] = 'Attention Line';
$mod_strings['LBL_ANY_EMAIL'] = 'Any Email:';
$mod_strings['LBL_LAST_NAME'] = 'Contact Person Name (Latin Character)';
$mod_strings['LBL_TITLE_2'] = 'Title';
$mod_strings['LBL_ADDR_1'] = 'Address [Any]';
$mod_strings['LBL_NAME_2'] = 'Contact Person Name (Local Character)';
$mod_strings['LBL_ADDR_2'] = 'Address (Local Character)';

?>
