<?php
// created: 2016-10-14 12:16:42
$subpanel_layout['list_fields'] = array(
    'name'           => array(
        'name'         => 'name',
        'vname'        => 'LBL_LIST_NAME',
        'sort_by'      => 'last_name',
        'sort_order'   => 'asc',
        'widget_class' => 'SubPanelDetailViewLink',
        'module'       => 'Contacts',
        'width'        => '20%',
        'default'      => true,
    ),
    'name_2_c'       => array(
        'type'    => 'varchar',
        'widget_class' => 'SubPanelDetailViewLink',
        'default' => true,
        'vname'   => 'LBL_NAME_2',
        'width'   => '13%',
    ),
    'account_name'   => array(
        'name'              => 'account_name',
        'module'            => 'Accounts',
        'target_record_key' => 'account_id',
        'target_module'     => 'Accounts',
        'widget_class'      => 'SubPanelDetailViewLink',
        'vname'             => 'LBL_ACCOUNT_NAME',
        'width'             => '13%',
        'sortable'          => true,
        'default'           => true,
    ),
    'contact_type_c' => array(
        'type'    => 'enum',
        'default' => true,
        'vname'   => 'LBL_CONTACT_TYPE',
        'width'   => '13%',
    ),
    'email'          => array(
        'name'         => 'email',
        'vname'        => 'LBL_LIST_EMAIL',
        'widget_class' => 'SubPanelEmailLink',
        'width'        => '11%',
        'sortable'     => false,
        'default'      => true,
    ),
    'date_entered'   => array(
        'type'     => 'datetime',
        'studio'   => array(
            'portaleditview' => false,
        ),
        'readonly' => true,
        'vname'    => 'LBL_DATE_ENTERED',
        'width'    => '15%',
        'default'  => true,
    ),
    'date_modified'  => array(
        'type'     => 'datetime',
        'studio'   => array(
            'portaleditview' => false,
        ),
        'readonly' => true,
        'vname'    => 'LBL_DATE_MODIFIED',
        'width'    => '15%',
        'default'  => true,
    ),
    'first_name'     => array(
        'name'  => 'first_name',
        'usage' => 'query_only',
    ),
    'last_name'      => array(
        'name'  => 'last_name',
        'usage' => 'query_only',
    ),
    'salutation'     => array(
        'name'  => 'salutation',
        'usage' => 'query_only',
    ),
    'account_id'     => array(
        'usage' => 'query_only',
    ),
);
