<?php
// created: 2015-09-29 04:16:36
$subpanel_layout['list_fields'] = array(
    'name'           => array(
        'name'         => 'name',
        'vname'        => 'LBL_LIST_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'module'       => 'Contacts',
        'width'        => '20%',
        'default'      => true,
    ),
    'name_2_c'       => array(
        'type'         => 'varchar',
        'widget_class' => 'SubPanelDetailViewLink',
        'default'      => true,
        'vname'        => 'LBL_NAME_2',
        'width'        => '16%',
        'link'         => true,
    ),
    'account_name'   => array(
        'type'              => 'relate',
        'link'              => true,
        'vname'             => 'LBL_ACCOUNT_NAME',
        'id'                => 'ACCOUNT_ID',
        'width'             => '13%',
        'default'           => true,
        'widget_class'      => 'SubPanelDetailViewLink',
        'target_module'     => 'Accounts',
        'target_record_key' => 'account_id',
    ),
    'contact_type_c' => array(
        'type'    => 'enum',
        'default' => true,
        'studio'  => 'visible',
        'vname'   => 'LBL_CONTACT_TYPE',
        'width'   => '6%',
    ),
    'email1'         => array(
        'name'         => 'email1',
        'vname'        => 'LBL_LIST_EMAIL',
        'widget_class' => 'SubPanelEmailLink',
        'width'        => '11%',
        'sortable'     => false,
        'default'      => true,
    ),
    'date_entered'   => array(
        'type'    => 'datetime',
        'vname'   => 'LBL_DATE_ENTERED',
        'width'   => '17%',
        'default' => true,
    ),
    'date_modified'  => array(
        'type'    => 'datetime',
        'vname'   => 'LBL_DATE_MODIFIED',
        'width'   => '17%',
        'default' => true,
    ),
    'first_name'     => array(
        'name'  => 'first_name',
        'usage' => 'query_only',
    ),
    'last_name'      => array(
        'name'  => 'last_name',
        'usage' => 'query_only',
    ),
    'salutation'     => array(
        'name'  => 'salutation',
        'usage' => 'query_only',
    ),
);
