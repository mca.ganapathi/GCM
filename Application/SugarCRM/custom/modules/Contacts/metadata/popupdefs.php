<?php
$popupMeta = array(
                'moduleMain' => 'Contact', 
                'varName' => 'CONTACT', 
                'orderBy' => 'contacts.first_name, contacts.last_name', 
                'whereClauses' => array(
                                        'last_name' => 'contacts.last_name', 
                                        'account_name' => 'accounts.name', 
                                        'name_2_c' => 'contacts_cstm.name_2_c', 
                                        'contact_type_c' => 'contacts_cstm.contact_type_c', 
                                        'email' => 'contacts.email'
                ), 
                'searchInputs' => array(
                                        1 => 'last_name', 
                                        2 => 'account_name', 
                                        3 => 'email', 
                                        4 => 'name_2_c', 
                                        5 => 'contact_type_c'
                ), 
                'create' => array(
                                'formBase' => 'ContactFormBase.php', 
                                'formBaseClass' => 'ContactFormBase', 
                                'getFormBodyParams' => array(
                                                            0 => '', 
                                                            1 => '', 
                                                            2 => 'ContactSave'
                                ), 
                                'createButton' => 'LNK_NEW_CONTACT'
                ), 
                'searchdefs' => array(
                                    'last_name' => array(
                                                        'name' => 'last_name', 
                                                        'width' => '10%'
                                    ), 
                                    'name_2_c' => array(
                                                        'type' => 'varchar', 
                                                        'label' => 'LBL_NAME_2', 
                                                        'width' => '10%', 
                                                        'name' => 'name_2_c'
                                    ), 
                                    'contact_type_c' => array(
                                                            'type' => 'enum', 
                                                            'studio' => 'visible', 
                                                            'label' => 'LBL_CONTACT_TYPE', 
                                                            'width' => '10%', 
                                                            'name' => 'contact_type_c'
                                    ), 
                                    'account_name' => array(
                                                            'name' => 'account_name', 
                                                            'type' => 'varchar', 
                                                            'width' => '10%'
                                    ), 
                                    'email' => array(
                                                    'name' => 'email', 
                                                    'width' => '10%'
                                    )
                ), 
                'listviewdefs' => array(
                                        'NAME' => array(
                                                        'width' => '20%', 
                                                        'label' => 'LBL_LIST_NAME', 
                                                        'link' => true, 
                                                        'default' => true, 
                                                        'related_fields' => array(
                                                                                0 => 'first_name', 
                                                                                1 => 'last_name', 
                                                                                2 => 'salutation', 
                                                                                3 => 'account_name', 
                                                                                4 => 'account_id'
                                                        ), 
                                                        'name' => 'name'
                                        ), 
                                        'NAME_2_C' => array(
                                                            'type' => 'varchar', 
                                                            'default' => true, 
                                                            'label' => 'LBL_NAME_2', 
                                                            'width' => '10%', 
                                                            'link' => true
                                        ), 
                                        'CONTACT_TYPE_C' => array(
                                                                'type' => 'enum', 
                                                                'default' => true, 
                                                                'studio' => 'visible', 
                                                                'label' => 'LBL_CONTACT_TYPE', 
                                                                'width' => '10%'
                                        ), 
                                        'ACCOUNT_NAME' => array(
                                                                'width' => '25%', 
                                                                'label' => 'LBL_LIST_ACCOUNT_NAME', 
                                                                'module' => 'Accounts', 
                                                                'id' => 'ACCOUNT_ID', 
                                                                'default' => true, 
                                                                'sortable' => true, 
                                                                'link' => false, 
                                                                'ACLTag' => 'ACCOUNT', 
                                                                'related_fields' => array(
                                                                                        0 => 'account_id'
                                                                ), 
                                                                'name' => 'account_name'
                                        ), 
                                        'TITLE_2_C' => array(
                                                            'type' => 'varchar', 
                                                            'default' => true, 
                                                            'label' => 'LBL_TITLE_2', 
                                                            'width' => '10%'
                                        )
                )
);
