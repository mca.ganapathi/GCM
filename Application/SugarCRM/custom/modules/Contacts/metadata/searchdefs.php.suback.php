<?php
// created: 2016-09-29 04:48:11
$searchdefs['Contacts'] = array(
    'templateMeta' => array(
        'maxColumns'      => '3',
        'maxColumnsBasic' => '4',
        'widths'          => array(
            'label' => '10',
            'field' => '30',
        ),
    ),
    'layout'       => array(
        'basic_search'    => array(
            0 => array(
                'name'    => 'last_name',
                'default' => true,
                'width'   => '10%',
            ),
            1 => array(
                'name'    => 'account_name',
                'default' => true,
                'width'   => '10%',
            ),
            2 => array(
                'name'    => 'current_user_only',
                'label'   => 'LBL_CURRENT_USER_FILTER',
                'type'    => 'bool',
                'default' => true,
                'width'   => '10%',
            ),
            3 => array(
                'name'    => 'favorites_only',
                'label'   => 'LBL_FAVORITES_FILTER',
                'type'    => 'bool',
                'default' => true,
                'width'   => '10%',
            ),
            4 => array(
                'type'    => 'varchar',
                'default' => true,
                'label'   => 'LBL_NAME_2',
                'width'   => '10%',
                'name'    => 'name_2_c',
            ),
        ),
        'advanced_search' => array(
            0 => array(
                'name'    => 'last_name',
                'default' => true,
                'width'   => '10%',
            ),
            1 => array(
                'type'    => 'enum',
                'default' => true,
                'studio'  => 'visible',
                'label'   => 'LBL_CONTACT_TYPE',
                'width'   => '10%',
                'name'    => 'contact_type_c',
            ),
            2 => array(
                'type'    => 'varchar',
                'default' => true,
                'label'   => 'LBL_NAME_2',
                'width'   => '10%',
                'name'    => 'name_2_c',
            ),
            3 => array(
                'type'    => 'phone',
                'label'   => 'LBL_MOBILE_PHONE',
                'width'   => '10%',
                'default' => true,
                'name'    => 'phone_mobile',
            ),
            4 => array(
                'type'    => 'varchar',
                'label'   => 'LBL_TITLE',
                'width'   => '10%',
                'default' => true,
                'name'    => 'title',
            ),
            5 => array(
                'type'    => 'varchar',
                'default' => true,
                'label'   => 'LBL_TITLE_2',
                'width'   => '10%',
                'name'    => 'title_2_c',
            ),
            6 => array(
                'type'    => 'varchar',
                'default' => true,
                'label'   => 'LBL_DIVISION_2',
                'width'   => '10%',
                'name'    => 'division_2_c',
            ),
            7 => array(
                'type'    => 'varchar',
                'default' => true,
                'label'   => 'LBL_DIVISION_1',
                'width'   => '10%',
                'name'    => 'division_1_c',
            ),
            8 => array(
                'name'     => 'assigned_user_id',
                'type'     => 'enum',
                'label'    => 'LBL_ASSIGNED_TO',
                'function' => array(
                    'name'   => 'get_user_array',
                    'params' => array(
                        0 => false,
                    ),
                ),
                'default'  => true,
                'width'    => '10%',
            ),
        ),
    ),
);
