<?php
$viewdefs['Contacts'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'hidden' => 
        array (
          0 => '<input type="hidden" name="opportunity_id" value="{$smarty.request.opportunity_id}">',
          1 => '<input type="hidden" name="case_id" value="{$smarty.request.case_id}">',
          2 => '<input type="hidden" name="bug_id" value="{$smarty.request.bug_id}">',
          3 => '<input type="hidden" name="email_id" value="{$smarty.request.email_id}">',
          4 => '<input type="hidden" name="inbound_email_id" value="{$smarty.request.inbound_email_id}">',
          5 => '<input type="hidden" id="country_code_numeric" name="country_code_numeric" value="{$COUNTRY_CODE_NUMERIC}">',
          6 => '<input type="hidden" id="stateidhidden" name="stateidhidden" value="{$STATE_ID}">',
          7 => '<input type="hidden" id="postal_no_c" name="postal_no_c" value="{$POSTALNUMBER}">',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/modules/Contacts/include/javascript/EditView.js',
        ),
      ),
      'maxColumns' => '2',
      'useTabs' => false,
      'widths' => 
      array (
        0 => 
        array (
          'label' => '20',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '20',
          'field' => '30',
        ),
      ),
      'tabDefs' => 
      array (
        'LBL_CONTACT_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'lbl_contact_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'contact_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CONTACT_TYPE',
          ),
          1 => 
          array (
            'name' => 'account_name',
            'displayParams' => 
            array (
              'required' => true,
            ),
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'name_2_c',
            'label' => 'LBL_NAME_2',
          ),
          1 => 
          array (
            'name' => 'last_name',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'division_2_c',
            'label' => 'LBL_DIVISION_2',
          ),
          1 => 
          array (
            'name' => 'division_1_c',
            'label' => 'LBL_DIVISION_1',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'title_2_c',
            'label' => 'LBL_TITLE_2',
          ),
          1 => 
          array (
            'name' => 'phone_work',
            'comment' => 'Work phone number of the contact',
            'label' => 'LBL_OFFICE_PHONE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'ext_no_c',
            'label' => 'LBL_EXT_NO',
          ),
          1 => 
          array (
            'name' => 'phone_fax',
            'comment' => 'Contact fax number',
            'label' => 'LBL_FAX_PHONE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'email1',
            'studio' => 'false',
            'label' => 'LBL_EMAIL_ADDRESS',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'country_code_c',
            'studio' => 'visible',
            'label' => 'LBL_COUNTRY_CODE',
            // 'customCode' => '{include file=$CUSTOMDROPDOWNTPL fieldname=$COUNTRYFILDNAME fieldlist= $COUNTRYLIST action_event = "onchange=\'onCountryChange();\'" }',
            'displayParams' => 
            array (
            ),
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'ndb_addr1',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_ADDR_1',
            'displayParams' => 
            array (
            ),
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'ndb_addr2',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_ADDR_2',
            'displayParams' => 
            array (
            ),
          ),
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'addr_general_c',
            'studio' => 'visible',
            'label' => 'LBL_ADDR_GENERAL',
            'displayParams' => 
            array (
            ),
          ),
          1 => 
          array (
            'name' => 'city_general_c',
            'label' => 'LBL_CITY_GENERAL',
            'displayParams' => 
            array (
            ),
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'state_general_c',
            'studio' => 'visible',
            'label' => 'LBL_STATE_GENERAL',
            'displayParams' => 
            array (
            ),
           // 'customCode' => '{include file=$CUSTOMDROPDOWNTPL fieldname=$STATEFILDNAME fieldlist=$STATELIST action_event=""}<td  id="postal_no_c_label" scope="col" width="30%" valign="top">Postal Number: </td><td id="USCanadaPostal" width="30%" valign="top"></td>',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'attention_line_c',
            'studio' => 'visible',
            'label' => 'LBL_ATTENTION_LINE',
          ),
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'addr_1_c',
            'studio' => 'visible',
            'label' => 'LBL_ADDR_1',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'attention_line_c',
            'studio' => 'visible',
            'label' => 'LBL_ATTENTION_LINE',
          ),
        ),
      ),
    ),
  ),
);
