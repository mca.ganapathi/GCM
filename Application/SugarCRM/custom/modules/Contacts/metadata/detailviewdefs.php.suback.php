<?php
$viewdefs['Contacts'] =
array (
  'DetailView' =>
  array (
    'templateMeta' =>
    array (
      'form' =>
      array (
        'buttons' =>
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
          4 =>
          array (
            'customCode' => '<input type="submit" class="button" title="{$APP.LBL_MANAGE_SUBSCRIPTIONS}" onclick="this.form.return_module.value=\'Contacts\'; this.form.return_action.value=\'DetailView\'; this.form.return_id.value=\'{$fields.id.value}\'; this.form.action.value=\'Subscriptions\'; this.form.module.value=\'Campaigns\'; this.form.module_tab.value=\'Contacts\';" name="Manage Subscriptions" value="{$APP.LBL_MANAGE_SUBSCRIPTIONS}"/>',
            'sugar_html' =>
            array (
              'type' => 'submit',
              'value' => '{$APP.LBL_MANAGE_SUBSCRIPTIONS}',
              'htmlOptions' =>
              array (
                'class' => 'button',
                'id' => 'manage_subscriptions_button',
                'title' => '{$APP.LBL_MANAGE_SUBSCRIPTIONS}',
                'onclick' => 'this.form.return_module.value=\'Contacts\'; this.form.return_action.value=\'DetailView\'; this.form.return_id.value=\'{$fields.id.value}\'; this.form.action.value=\'Subscriptions\'; this.form.module.value=\'Campaigns\'; this.form.module_tab.value=\'Contacts\';',
                'name' => 'Manage Subscriptions',
              ),
            ),
          ),
        ),
      ),
      'maxColumns' => '2',
      'useTabs' => false,
      'widths' =>
      array (
        0 =>
        array (
          'label' => '22',
          'field' => '30',
        ),
        1 =>
        array (
          'label' => '22',
          'field' => '30',
        ),
      ),
      'includes' =>
      array (
        0 =>
        array (
          'file' => 'modules/Leads/Lead.js',
        ),
          1 =>
          array (
              'file' => 'custom/modules/Contacts/include/javascript/DetailView.js',
          ),
      ),
      'tabDefs' =>
      array (
        'LBL_CONTACT_INFORMATION' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL4' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL5' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL6' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' =>
    array (
      'lbl_contact_information' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'contact_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CONTACT_TYPE',
          ),
          1 =>
          array (
            'name' => 'account_name',
            'label' => 'LBL_ACCOUNT_NAME',
            'displayParams' =>
            array (
            ),
          ),
        ),
        1 =>
        array (
          0 =>
          array (
            'name' => 'name_2_c',
            'label' => '{$MOD.LBL_NAME_2}&nbsp;<img title=\'{$fields.name_2_c.help}\' src=\'themes/Sugar/images/info_inline.png?v=03QxjiIC-nlXIK1nio3fhA\'/>',
          ),
          1 =>
          array (
            'name' => 'last_name',
            'comment' => 'Contact Person in Latin character (English)',
            'label' => '{$MOD.LBL_LAST_NAME}&nbsp;<img title=\'{$fields.last_name.help}\' src=\'themes/Sugar/images/info_inline.png?v=03QxjiIC-nlXIK1nio3fhA\'/>',
          ),
        ),
      ),
      'lbl_editview_panel1' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'division_2_c',
            'label' => 'LBL_DIVISION_2',
          ),
          1 =>
          array (
            'name' => 'division_1_c',
            'label' => 'LBL_DIVISION_1',
          ),
        ),
        1 =>
        array (
          0 =>
          array (
            'name' => 'title_2_c',
            'label' => 'LBL_TITLE_2',
          ),
          1 =>
          array (
            'name' => 'phone_work',
            'label' => 'LBL_OFFICE_PHONE',
          ),
        ),
        2 =>
        array (
          0 =>
          array (
            'name' => 'ext_no_c',
            'label' => 'LBL_EXT_NO',
          ),
          1 =>
          array (
            'name' => 'phone_fax',
            'label' => 'LBL_FAX_PHONE',
          ),
        ),
        3 =>
        array (
          0 =>
          array (
            'name' => 'email1',
            'studio' => 'false',
            'label' => 'LBL_EMAIL_ADDRESS',
          ),
        ),
      ),
      'lbl_editview_panel2' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'country_code_c',
            'studio' => 'visible',
            'label' => 'LBL_COUNTRY_CODE',
          ),
        ),
      ),
      'lbl_detailview_panel4' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'postal_no_c',
            'label' => 'LBL_POSTAL_NO',
          ),
        ),
        1 =>
        array (
          0 =>
          array (
            'name' => 'ndb_addr1',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_ADDR_1',
          ),
        ),
        2 =>
        array (
          0 =>
          array (
            'name' => 'ndb_addr2',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_ADDR_2',
          ),
        ),
      ),
      'lbl_detailview_panel5' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'addr_general_c',
            'studio' => 'visible',
            'label' => 'LBL_ADDR_GENERAL',
          ),
          1 =>
          array (
            'name' => 'city_general_c',
            'label' => 'LBL_CITY_GENERAL',
          ),
        ),
        1 =>
        array (
          0 =>
          array (
            'name' => 'state_general_c',
            'studio' => 'visible',
            'label' => 'LBL_STATE_GENERAL',
          ),
          1 =>
          array (
            'name' => 'postal_no_c',
            'label' => 'LBL_POSTAL_NO',
          ),
        ),
        2 =>
        array (
          0 =>
          array (
            'name' => 'attention_line_c',
            'studio' => 'visible',
            'label' => 'LBL_ATTENTION_LINE',
          ),
        ),
      ),
      'lbl_detailview_panel6' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'addr_1_c',
            'studio' => 'visible',
            'label' => 'LBL_ADDR_1',
          ),
        ),
        1 =>
        array (
          0 =>
          array (
            'name' => 'attention_line_c',
            'studio' => 'visible',
            'label' => 'LBL_ATTENTION_LINE',
          ),
        ),
      ),
      'lbl_editview_panel3' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
          ),
          1 => 'team_name',
        ),
        1 =>
        array (
          0 =>
          array (
            'name' => 'date_entered',
            'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 =>
          array (
            'name' => 'date_modified',
            'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
            'label' => 'LBL_DATE_MODIFIED',
          ),
        ),
      ),
    ),
  ),
);
