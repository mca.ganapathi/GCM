<?php

$hook_array['before_save'][]  = array(
	2, 
	'Custom Audit Fields', 
	'custom/modules/Contacts/include/ClassBeforeSave.php', 
	'ClassContactBeforeSave', 
	'setCustomAuditEntry'
);
$hook_array['before_save'][]  = array(
	3, 
	'Save Accounts (Japan Specific) non-db fields', 
	'custom/modules/Contacts/include/ClassBeforeSave.php', 
	'ClassContactBeforeSave', 
	'setAcountJSEntry'
);
$hook_array['before_save'][]  = array(
	4, 
	'Update Account Teams', 
	'custom/modules/Contacts/include/ClassBeforeSave.php', 
	'ClassContactBeforeSave', 
	'saveAccountsTeam'
);
$hook_array['after_save'][]   = array(
	5, 
	'Account Page Load', 
	'custom/modules/Contacts/include/ClassAfterSave.php', 
	'ClassContactAfterSave', 
	'saveAccountandPageLoad'
);
$hook_array['after_delete'][] = array(
    1,
    'Delete respective record from js_accounts_js custom table',
    'custom/modules/Contacts/include/ClassAfterDelete.php',
    'ClassContactAfterDelete',
    'deleteAccountJsRecord'
);
