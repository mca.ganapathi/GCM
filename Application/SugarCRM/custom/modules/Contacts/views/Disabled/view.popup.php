<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/
require_once 'include/MVC/View/views/view.popup.php';

/**
 * Class to extend view popup
 */
class CustomViewPopup extends ViewPopup
{
    /**
     * Method to handle display
     */
    public function display()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'display');
        global $popupMeta, $mod_strings;

        // Redmine Bug #9345 : On selecting account in contract create/edit page by search, instead of timezone pertain to particular country all timezones are getting displayed
        if(!isset($_REQUEST['field_to_name'])) $_REQUEST['field_to_name'] = array('c_country_id_c');

        parent::display();

        $util         = ModUtils::getInstance('Contacts', 'ClassContacts');
        $country_list = $util->getCountryIdNamePair();
        $country_list = ((is_array($country_list) && (count($country_list)>0))?$country_list:array());
        $ddl_country = $this->generateDropdownElement('c_country_id_c', 'c_country_id_c', $country_list, 'onCountryChange');
        $ddl_country = ((!empty($ddl_country))?$ddl_country:'');
        $ddl_state   = $this->generateDropdownElement('c_state_id_c', 'c_state_id_c', array(), '');
        $ddl_state = ((!empty($ddl_state))?$ddl_state:'');
        echo <<<EOQ
        <script type="text/javascript">
        $(document).ready(function() {
            $("#country_code_c").closest('td').html('$ddl_country');
            $("#state_general_c").closest('td').html('$ddl_state');
            PageLoad();
});
        </script>
EOQ;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method to generate dropdown element
     *
     * @param string $element_id element id
     * @param string $element_name element name
     * @param array $option_list option list
     * @param string $onchange_event call back
     * @return string HTML element
     */
    private function generateDropdownElement($element_id, $element_name, $option_list = array(), $onchange_event = '')
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'element_id : '.$element_id.GCM_GL_LOG_VAR_SEPARATOR.'element_name : '.$element_name.GCM_GL_LOG_VAR_SEPARATOR.'option_list : '.print_r($option_list,true).GCM_GL_LOG_VAR_SEPARATOR.'onchange_event : '.$onchange_event, 'Method to generate dropdown element');
        $ddl_html = '<select ';
        if (!empty($onchange_event)) {
            $ddl_html .= 'onchange="' . $onchange_event . '();"';
        }
        $ddl_html .= 'id="' . $element_id . '" name="' . $element_name . '">';
        $ddl_html .= '<option data="" value=""></option>';
        foreach ($option_list as $key => $val) {
            $ddl_html .= '<option ';
            if (isset($val['code'])) {
                $ddl_html .= 'data="' . $val['code'] . '"';
            }
            $ddl_html .= 'value="' . $key . '">' . $val['name'] . '</option>';
        }
        $ddl_html .= '</select>';
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'ddl_html : '.$ddl_html, '');
        return $ddl_html;
    }
}
