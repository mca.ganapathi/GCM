<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

require_once 'modules/Contacts/views/view.edit.php';

/**
 * Class to extend custom contracts edit view
 */
class CustomContactsViewEdit extends ContactsViewEdit
{

    /**
     * boolean variable to determine whether view can be used for subpanel creates
     *
     * @var boolean
     */
    public $useForSubpanel = true;

    /**
     * boolean variable to determine whether or not SubpanelQuickCreate has a separate display function
     *
     * @var boolean
     */
    public $useModuleQuickCreateTemplate = true;

    /**
     * boolean variable to determine whether or not title shoudl be display or not
     *
     * @var boolean
     */
    public $showTitle = true;

    public function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'constructor');
        parent::__construct();
        $this->useForSubpanel = true; // call parent ViewEdit method
        parent::ViewEdit();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method to handle pre display
     */
    public function preDisplay()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'preDisplay');
        if (!empty($this->bean->id)) {
            $util = ModUtils::getInstance('Contacts', 'ClassContacts');
            // fetch sub-module fields from Accounts (Japan Specific Module)
            $util->fetchAccountJPFields($this->bean);
            $obj_country = new c_country();
            $c_country_id_c = ((!empty($this->bean->c_country_id_c))?$this->bean->c_country_id_c:'');
            $obj_country->retrieve($c_country_id_c);
            $country_code_numeric = ((!empty($obj_country->country_code_numeric))?$obj_country->country_code_numeric:'');
            $this->ss->assign('COUNTRY_CODE_NUMERIC', $country_code_numeric);
            $c_state_id_c = ((!empty($this->bean->c_state_id_c))?$this->bean->c_state_id_c:'');
            $this->ss->assign('STATE_ID', $c_state_id_c);
            $this->ss->assign('POSTALNUMBER', $this->bean->postal_no_c);
        }
        // call parent preDisplay method
        parent::preDisplay();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method to handle display
     */
    public function display()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method to handle display');
        // call parent display method if record is in edit view else show error message
        $util         = ModUtils::getInstance('Contacts', 'ClassContacts');
        $country_list = $util->getCountryIdNamePair();
        $state_list=array();

        $country_list = ((is_array($country_list) && (count($country_list)>0))?$country_list:array());
        $this->ss->assign('COUNTRYLIST', $country_list);
        $this->ss->assign('STATELIST', $state_list);
        $this->ss->assign('COUNTRYFILDNAME', 'c_country_id_c');
        $this->ss->assign('STATEFILDNAME', 'c_state_id_c');
        $this->ss->assign('CUSTOMDROPDOWNTPL', 'custom/include/tpl/CustomDropDown.tpl');
        parent::display();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

}
