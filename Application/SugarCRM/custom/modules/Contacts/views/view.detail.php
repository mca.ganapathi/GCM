<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

require_once 'modules/Contacts/views/view.detail.php';

/**
 * Class to extend custom contracts details view
 */
class CustomContactsViewDetail extends ContactsViewDetail
{

    /**
     * set default value of empty latin|local name
     *
     * @var string
     */
    public $text_click_here;

    public function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'constructor');
        parent::__construct();
        $this->text_click_here = 'Click Here';
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method to handle pre display
     */
    public function preDisplay()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'preDisplay');
        $util = ModUtils::getInstance('Contacts', 'ClassContacts');
        // fetch sub-module fields from Accounts (Japan Specific Module)
        $util->fetchAccountJPFields($this->bean);
        // Fetching the accounts details
        $account_details = new Account();
        $account_details->retrieve_by_string_fields(array(
            'id' => $this->bean->account_id,
        ));
        // if corporate name is empty, set text as "Click Here" - Sagar.Salunkhe 15-02-2015 | Jira Ticket # OM-15
        $this->bean->account_name = $account_details->name;
        $array_corpname_filter    = array(
            '',
            'Not Available',
            'Data Not Found in CIDAS',
        );
        $corp_name_search     = array_search(strtolower(trim($this->bean->account_name)), array_map('strtolower', $array_corpname_filter));
        $corp_name_2_c_search = array_search(strtolower(trim($account_details->name_2_c)), array_map('strtolower', $array_corpname_filter));
        if ($corp_name_search !== false && $corp_name_2_c_search === false) {
            $this->bean->account_name = $account_details->name_2_c;
        } elseif ($corp_name_search !== false && $corp_name_2_c_search !== false) {
            $this->bean->account_name = $this->text_click_here;
        }
        // call parent preDisplay method
        parent::preDisplay();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method to handle display
     */
    public function display()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'display');
        // Hide unused buttons from View
        // enable edit button - ticket #OD-69
        // unset($this->dv->defs['templateMeta']['form']['buttons'][0]); //Hide Edit button
        unset($this->dv->defs['templateMeta']['form']['buttons'][1]); // Hide Duplicate button
        unset($this->dv->defs['templateMeta']['form']['buttons'][2]); // Hide Delete button
        unset($this->dv->defs['templateMeta']['form']['buttons'][3]); // Hide Find Duplicate button
        unset($this->dv->defs['templateMeta']['form']['buttons'][4]); // Hide Get Data button

        $obj_state_bean = BeanFactory::getBean('c_State');

        // BEGIN : Unset address fields accoding to selected country
        $obj_country_bean = BeanFactory::getBean('c_country');
        $obj_country_bean->retrieve($this->bean->c_country_id_c);
        $unset_fields = array();
        if (empty($obj_country_bean->country_code_numeric)) {
            $unset_fields = array(
                'lbl_detailview_panel4',
                'lbl_detailview_panel5',
                'lbl_detailview_panel6',
            );
        } elseif ($obj_country_bean->country_code_numeric == 392) {
            $unset_fields = array(
                'lbl_detailview_panel5',
                'lbl_detailview_panel6',
            );
        } elseif ($obj_country_bean->country_code_numeric == 840 || $obj_country_bean->country_code_numeric == 124) {
            $unset_fields = array(
                'lbl_detailview_panel4',
                'lbl_detailview_panel6',
            );
        } else {
            $unset_fields = array(
                'lbl_detailview_panel4',
                'lbl_detailview_panel5',
            );
        }

        foreach ($unset_fields as $field) {
            $this->unsetFieldsFromView($field);
        }

        if (!$obj_country_bean->aclAccess('view')) {
        	$this->bean->c_country_id_c = "";
        }

        if (!$obj_state_bean->aclAccess('view')) {
        	$this->bean->c_state_id_c = "";
        }
        // call parent display method
        parent::display();
        // manage sugar layout issue using jquery
        $js = <<<EOQ
        <script type="text/javascript">
            // japan specific address details panel
        $("#ndb_addr1").closest("table").find("tr").find("td:eq(3)").remove();
        $("#ndb_addr1").closest("table").find("tr").find("td:eq(2)").remove();
        $("#ndb_addr1").closest("table").find("tr").find("td:eq(1)").attr('width', '');

           // other address details panel
        $("#addr_1_c").closest("table").find("tr").find("td:eq(3)").remove();
        $("#addr_1_c").closest("table").find("tr").find("td:eq(2)").remove();
        $("#addr_1_c").closest("table").find("tr").find("td:eq(1)").attr('width', '');
        </script>
EOQ;
        echo $js;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * method to unset array node from given array
     *
     * @param string $field_name element name
     * @author Sagar.Salunkhe
     * @since Jan 22, 2016
     */
    private function unsetFieldsFromView($field_name)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'field_name : '.$field_name, 'method to unset array node from given array');
		$modUtils = new ModUtils();
        $arr_element = $modUtils->searchArrayElement($this->dv->defs['panels'], 'key', $field_name);
        if (isset($arr_element['path'])) {
            // if node has name element (refer sugar defs structure), unset the parent array
            if (isset($arr_element['path'][count($arr_element['path'])]) && !is_numeric($arr_element['path'][count($arr_element['path'])])) {
                unset($arr_element['path'][count($arr_element['path'])]);
            }
            $modUtils->unsetArrayNode($arr_element['path'], $this->dv->defs['panels']);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * method to unset array node from given array
     */
    public function _displaySubPanels()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'method to unset array node from given array');
        require_once 'custom/include/SubPanel/SubPanelTiles.php';
        $subpanel = new CustomSubPanelTiles($this->bean, $this->module);
        //do not show contracts subpanel for contacts of type Technology and Billing
        if (!empty($this->bean->contact_type_c) && ($this->bean->contact_type_c == 'Technology' || $this->bean->contact_type_c == 'Billing')) {
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts_gc_contracts_1']);
        }
        echo $subpanel->display();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}
