<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * Class to handle after save
 */
class ClassContactAfterSave
{

    /**
     * method to save Accounts - DF 521
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @return (void method)
     * @author Natarajan
     * @since Jun 14, 2016
     */
    public function saveAccountandPageLoad($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'method to save Accounts - DF 521');
        // logic hook should get called only when user is editing record from Sugar screen edit view
        if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'Popup') {
            $urlData = array("id_advanced" => $bean->id, "name_2_c_advanced" => $_POST['name_2_c'], "last_name_advanced" => $_POST['last_name'],
                "module"                       => 'Contacts', 'action'           => 'Popup', 'mode'                          => 'single', 'field_to_name[]' => 'name', 'field_to_name[]' => 'c_country_id_c');
            if (isset($_REQUEST['return_module']) && !empty($_POST['return_module'])) {
                $urlData['module'] = $_POST['return_module'];
            }
            if (isset($_POST['return_action']) && !empty($_POST['return_action'])) {
                $urlData['action'] = $_POST['return_action'];
            }
            foreach (array('return_id', 'create') as $var) {
                if (isset($_POST[$var]) && !empty($_POST[$var])) {
                    $urlData[$var] = $_POST[$var];
                }
            }
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'urlData : '.print_r($urlData,true), '');
            header("Location: index.php?" . http_build_query($urlData));
            sugar_upgrade_exit();
        }
    }
}
