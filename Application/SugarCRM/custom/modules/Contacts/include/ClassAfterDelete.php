<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * Class to handle after delete
 */
class ClassContactAfterDelete
{

    /**
     * method to delete respective record from js_accounts_js custom table
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author Dinesh.Itkar
     * @since Nov 15, 2016
     */
    public function deleteAccountJsRecord($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'method to delete respective record from js_accounts_js custom table');
        // Account_js module object
        $obj_accounts_js_bean = BeanFactory::getBean('js_Accounts_js');
        $obj_accounts_js_bean->retrieve_by_string_fields(array(
            'contact_id_c' => $bean->fetched_row['id'],
        ));
        $obj_accounts_js_bean->retrieve($obj_accounts_js_bean->id);
        if (!empty($obj_accounts_js_bean->id)) {
            // flag deleted 1
            $obj_accounts_js_bean->deleted = '1';
            $obj_accounts_js_bean->save();
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

}
