/*
 * @author Dinesh.Itkar
 * @date 08 Jan 2018
 * @desc : All Detail View related JavaScript methods come under this file
 */
/*
 * @Note : All element related triggers/events are written here and not in metadata; as
 * if we include/exclude field from layout will end into losing javascript array param
 */

SetAddressFieldStyle = 'display: block; width: auto; word-break: break-all;';
$(document).ready(function() {
	     // Any Address (Latin Character) set style
        $('#addr_1_c').attr('style', SetAddressFieldStyle);
		
		 // [US/Canada specific] Address set style
        $('#addr_general_c').attr('style', SetAddressFieldStyle);
		
        //Address 1[Japan specific] set style
        $('#ndb_addr1').attr('style', SetAddressFieldStyle);
		
		//Address 2[Japan specific] set style
        $('#ndb_addr2').attr('style', SetAddressFieldStyle);		
});


