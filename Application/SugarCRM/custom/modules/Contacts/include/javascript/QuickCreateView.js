/*
 * @author Sagar.Salunkhe
 * @date 24 Nov 2015
 * @desc : All EditView related JavaScript methods come under this file
 */
/*
 * @Note : All element related triggers/events are written here and not in metadata; as
 * if we include/exclude field from layout will end into losing javascript array param
 */
EmailAddressMaxLength = 140;
EmailAddressMinLength = 3;
AnyAddressFieldMaxLength = 150;
USCAAddressFieldMaxLength = 100;
JapanAAddressFieldMaxLength = 100;
USCityMaxLength = 30;
var countryField = null;
var record = null;
var japanSpecific = null;
var uscanadaSpecific = null;
var othersSpecific = null;
var fieldsId = JSON.parse('{"japan":["ndb_addr1","ndb_addr2"],"uscanada":["c_state_id_c","city_general_c","addr_general_c"]}');
var countryCodeKey = null;
// set length when complete document is loaded
window.onload = function() {
	$('#account_name').prop('readonly', true);
    // email address length validation
    $('#ContactsemailAddressesTable0 input[type=text]').each(function() {
        $(this).attr('maxLength', EmailAddressMaxLength);
    });  
	
	// Any Address (Latin Character) length validation
	$('#addr_1_c').attr('maxLength', AnyAddressFieldMaxLength);
	
	 // [US/Canada specific] Address length validation
	$('#addr_general_c').attr('maxLength', USCAAddressFieldMaxLength);
	
	//Address 1[Japan specific] length validation
	$('#ndb_addr1').attr('maxLength', JapanAAddressFieldMaxLength);
	
	//Address 2[Japan specific] length validation
	$('#ndb_addr2').attr('maxLength', JapanAAddressFieldMaxLength);
		
    //City [US/Canada Specific] length validation
    $('#city_general_c').attr('maxLength', USCityMaxLength);
};
/*
 * @desc method to get called once page load event is initiated of jquery
 * @author Sagar.Salunkhe @params @return
 */
function PageLoad() {
	var custom_result = true;
    // set email address max legnth
    $("#Contacts0_email_widget_add").click(function() {
        $('#ContactsemailAddressesTable0 input[type=text]').each(function() {
            $(this).attr('maxLength', EmailAddressMaxLength);
        });
    });
    //$('#country_code_c').attr('readonly', true);  //make country code textbox readonly
    countryCode = $('#country_code_numeric').val().trim();
    /*** begin : Sugar default validation issue ***/
    removeFromValidate('form_QuickCreate_Contacts', 'account_id');
    removeFromValidate('form_QuickCreate_Contacts', 'account_name');
    addToValidate('form_QuickCreate_Contacts', 'account_name', 'relate', true, 'Company Name');
    addToValidate('form_QuickCreate_Contacts', 'account_id', 'relate', false, 'Corporate ID');
    /*** end : Sugar default validation issue ***/
    var save_button = document.getElementsByName('Contacts_popupcreate_save_button');
    save_button[0].onclick = function() {
        var _form = document.getElementById('form_QuickCreate_Contacts');
        _form.action.value = 'Popup';
        custom_result = customValidate();
        return (ValidateSave() && check_form('form_QuickCreate_Contacts') && custom_result);
    };
    save_button[1].onclick = function() {
        var _form = document.getElementById('form_QuickCreate_Contacts');
        _form.action.value = 'Popup';
        custom_result = customValidate();
        return (ValidateSave() && check_form('form_QuickCreate_Contacts') && custom_result);
    };
    japanSpecific = $('#ndb_addr1').closest('div');
    uscanadaSpecific = $('#addr_general_c').closest('div');
    othersSpecific = $('#addr_1_c').closest('div');
    record = $('input[name="record"]').val();
    countryField = $('#c_country_id_c');
    if (countryField.val() == '') {
        toggleNode(japanSpecific, 1);
        toggleNode(uscanadaSpecific, 1);
        toggleNode(othersSpecific, 1);
        fillStates(1, 0);
    } else {
        toggleFields();
        fillStates(0, 1);
    }
    $("input[name='attention_line_c']").each(function() {
        $(this).val($('#attention_line_c').val());
        $(this).change(function() {
            setAttentionLineValue();
        });
    });
    /* start : manage sugar layout issue using jquery */
    // japan specific address details panel
    $("#detailpanel_4 tr").find("td:eq(3)").remove();
    $("#detailpanel_4 tr").find("td:eq(2)").remove();
    $("#detailpanel_4 tr").find("td:eq(1)").attr('width', '');
    // other address details panel
    $("#detailpanel_6 tr").find("td:eq(3)").remove();
    $("#detailpanel_6 tr").find("td:eq(2)").remove();
    $("#detailpanel_6 tr").find("td:eq(1)").attr('width', '');
    /* end : manage sugar layout issue using jquery */
}

function toggleFields() {
    countryCode = $('#country_code_numeric').val().trim();
    if (countryCode == '392') {
        countryCodeKey = 'japan';
        toggleNode(japanSpecific, 0);
        toggleNode(uscanadaSpecific, 1);
        toggleNode(othersSpecific, 1);
    } else if (countryCode == '840' || countryCode == '124') {
        countryCodeKey = 'uscanada';
        toggleNode(uscanadaSpecific, 0);
        toggleNode(japanSpecific, 1);
        toggleNode(othersSpecific, 1);
    } else if (countryCodeKey != '') {
        countryCodeKey = 'other';
        toggleNode(othersSpecific, 0);
        toggleNode(japanSpecific, 1);
        toggleNode(uscanadaSpecific, 1);
    }
}

function toggleNode(node, flag) {
    if (flag) {
        node.addClass('hide');
        clearFields(node);
    } else {
        node.removeClass('hide');
    }
}

function clearFields(node) {
    node.find('input, select, textarea').each(function(index) {
        var node_id = $(this).attr('id');
        if (node_id != 'attention_line_c') $(this).val('');
    });
}

function onCountryChange() {
    clear_all_errors();
    var countryCode = $('#country_code_numeric');
    if (countryField.val() == '') {
        toggleNode(japanSpecific, 1);
        toggleNode(uscanadaSpecific, 1);
        toggleNode(othersSpecific, 1);
        countryCode.val('');
        fillStates(1, 0);
    } else {
        countryCode.val(countryField.find(":selected").attr('data'));
        toggleFields();
        fillStates(0, 0);
    }
    $('#addr_1_c').val('');
    $('#addr_2_c').val('');
    $("input[name='attention_line_c']").each(function() {
        $(this).val('');
    });
    addPostalNumberField();
    clearFields(othersSpecific);
    clearFields(japanSpecific);
    clearFields(uscanadaSpecific);
}

function fillStates(flag, flag1) {
    var statefld = $('#c_state_id_c');
    statefld.find("option:gt(0)").remove();
    if (flag) {
        return;
    } else {
        if (countryCodeKey == 'uscanada') {
            SUGAR.ajaxUI.showLoadingPanel();
            $.ajax({
                type: "POST",
                url: "index.php",
                data: {
                    module: 'Contacts',
                    action: 'Actions',
                    method: 'getStates',
                    sugar_body_only: '1',
                    c_id: countryField.val(),
                },
                async: true,
                success: function(data) {
                    SUGAR.ajaxUI.hideLoadingPanel();
                    var result = $.parseJSON(data);
                    if (result.hasOwnProperty('error') || $.isEmptyObject(result)) return;
                    else {
                        $.each(result, function(i, value) {
                            statefld.append($('<option>').text(value).attr('value', i));
                        });
                    }
                    if (flag1) {
                        if (record != '') {
                            $('#c_state_id_c').val($('#stateidhidden').val());
                        }
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    clearFields(othersSpecific);
                    clearFields(japanSpecific);
                    clearFields(uscanadaSpecific);
                    SUGAR.ajaxUI.hideLoadingPanel();
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }
    }
}

function removeUnwantedValidation() {
    var node = $('#EditView_tabs').find('.hide');
    node.each(function() {
        var trnode = $(this);
        trnode.find('input, select, textarea').each(function(index) {
            var fld = $(this);
            var name = fld.attr('name');
            removeFromValidate('form_QuickCreate_Contacts', name);
        });
    });
}

function addValidations(node) {
    var name = node.attr('name');
    var lbl = node.closest('td').prev().text();
    lbl = lbl.replace('*', '');
    //list of fields not to validate 
    var notToValidate = ['account_name', 'account_id', 'c_country_id_c', 'c_state_id_c', 'city_general_c', 'addr_general_c', 'ndb_addr1', 'ndb_addr2'];
    if ($.inArray(name, notToValidate) < 0) {
        addToValidate('form_QuickCreate_Contacts', name, 'text', true, lbl);
    }
}

function ValidateSave() {
    clear_all_errors();
    if (typeof fieldsId[countryCodeKey] != 'undefined') {
        for (var i = 0; i < fieldsId[countryCodeKey].length; i++) {
            addValidations($('#' + fieldsId[countryCodeKey][i]));
        }
    }
    setAttentionLineValue();
    removeUnwantedValidation();
    removeFromValidate('form_QuickCreate_Contacts', 'postal_no_japan');
    removeFromValidate('form_QuickCreate_Contacts', 'ndb_addr1');
    removeFromValidate('form_QuickCreate_Contacts', 'ndb_addr2');
    removeFromValidate('form_QuickCreate_Contacts', 'postal_no_us');
    removeFromValidate('form_QuickCreate_Contacts', 'addr_general_c');
    removeFromValidate('form_QuickCreate_Contacts', 'city_general_c');
    removeFromValidate('form_QuickCreate_Contacts', 'c_state_id_c');
    removeFromValidate('form_QuickCreate_Contacts', 'addr_1_c');
    countryCodeNumeric = $('#country_code_numeric').val().trim();
    if (countryCodeNumeric == '392') {
    var postal_number =	$("#postal_no_japan").val();
    $("#postal_no_c").val(postal_number);	
  
    var address1 = $("#ndb_addr1").val();
    var address2 = $("#ndb_addr2").val();
    if ($.trim(postal_number) != '' || $.trim(address1) != '' || $.trim(address2) != '') {
    	addToValidate('form_QuickCreate_Contacts', 'postal_no_japan', 'text', true, 'Postal Number');
    	addToValidate('form_QuickCreate_Contacts', 'ndb_addr1', 'text', true, 'Address 1 [Japan specific]');
    	addToValidate('form_QuickCreate_Contacts', 'ndb_addr2', 'text', true, 'Address 2 [Japan specific]');
    }
  } else if (countryCodeNumeric == '840') {
	    var postal_number =	$("#postal_no_us").val();
    	$("#postal_no_c").val(postal_number);	
   
    
    var address = $("#addr_general_c").val();
    var city = $("#city_general_c").val();
    var state = $("#c_state_id_c").val();
    var attention = document.getElementsByName('attention_line_c')[0].value;
    if ($.trim(postal_number) != '' || $.trim(address) != '' || $.trim(city) != '' || $.trim(state) != '' || $.trim(attention) != '') {
    	addToValidate('form_QuickCreate_Contacts', 'postal_no_us', 'text', true, 'Postal Number');
    	addToValidate('form_QuickCreate_Contacts', 'addr_general_c', 'text', true, 'Address [US/Canada Specific]');
    	addToValidate('form_QuickCreate_Contacts', 'city_general_c', 'text', true, 'City [US/Canada Specific]');
    	addToValidate('form_QuickCreate_Contacts', 'c_state_id_c', 'select', true, 'State [US/Canada Specific]');
    }
    
    }
    else if (countryCodeNumeric == '124') {
    	var postal_number =	$("#postal_no_us").val();
    	$("#postal_no_c").val(postal_number);	
    	
    
    
    var address = $("#addr_general_c").val();
    var city = $("#city_general_c").val();
    var state = $("#c_state_id_c").val();
    var attention = document.getElementsByName('attention_line_c')[0].value;
    if ($.trim(postal_number) != '' || $.trim(address) != '' || $.trim(city) != '' || $.trim(state) != '' || $.trim(attention) != '') {
    	addToValidate('form_QuickCreate_Contacts', 'postal_no_us', 'text', true, 'Postal Number');
    	addToValidate('form_QuickCreate_Contacts', 'addr_general_c', 'text', true, 'Address [US/Canada Specific]');
    	addToValidate('form_QuickCreate_Contacts', 'city_general_c', 'text', true, 'City [US/Canada Specific]');
    	addToValidate('form_QuickCreate_Contacts', 'c_state_id_c', 'select', true, 'State [US/Canada Specific]');
    }
    } else {
    	$("#postal_no_c").val("");
    	if (countryCodeNumeric != "") {
    	    var attention = document.getElementsByName('attention_line_c')[1].value;
    	    if ($.trim(attention) != '') {
    	    	addToValidate('form_QuickCreate_Contacts', 'addr_1_c', 'text', true, 'Address [Any]');
    	    }
    	}
    }
    
    //if ($.trim($("#last_name").val()) == '' && $.trim($("#name_2_c").val()) == '') addToValidate('form_QuickCreate_Contacts', 'last_name', 'text', true, 'Please enter Contact Person Name (Latin Character) or Contact Person Name (Local Character)');
    //else removeFromValidate('form_QuickCreate_Contacts', 'last_name');
    //add country relate field in mandatory validation
    //addToValidate('form_QuickCreate_Contacts', 'country_code_c', 'relate', true,'Country Name Code');
    //if selected country code is Japan then make Japan Specific Addresses Mandatory
    /* 	if($.trim($("#country_code_numeric").val())=='392') {
    		addToValidate('form_QuickCreate_Contacts', 'ndb_addr1', 'text', true,'Address 1 [Japan specific]');
    		addToValidate('form_QuickCreate_Contacts', 'ndb_addr2', 'text', true,'Address 2 [Japan specific]');
    	}
    	else {
    		removeFromValidate('form_QuickCreate_Contacts','ndb_addr1');
    		removeFromValidate('form_QuickCreate_Contacts','ndb_addr2');
    	} */
    removeFromValidate('form_QuickCreate_Contacts', 'account_id');
    //removeFromValidate('form_QuickCreate_Contacts','account_name');
    //addToValidate('form_QuickCreate_Contacts', 'account_id', 'text', true, SUGAR.language.get('Contacts', 'LBL_ACCOUNT_NAME'));
    if ($.trim($("#account_id").val()) == '') {
        addToValidate('form_QuickCreate_Contacts', 'account_id', 'text', true, SUGAR.language.get('Contacts', 'LBL_ACCOUNT_NAME'));
    } else {
        removeFromValidate('form_QuickCreate_Contacts', 'account_name');
    }
    return true;
}

function setAttentionLineValue() {
    countryCode = $('#country_code_numeric').val().trim();
    if (countryCode == '392' || $('#c_country_id_c').val().trim() == '') {
        $("input[name='attention_line_c']").each(function() {
            $(this).val('');
        });
    } else if (countryCode == '840' || countryCode == '124') {
        $("input[name='attention_line_c']").each(function() {
            $(this).val(document.getElementsByName('attention_line_c')[0].value);
        });
    } else {
        $("input[name='attention_line_c']").each(function() {
            $(this).val(document.getElementsByName('attention_line_c')[1].value);
        });
    }
}

function checkLatinCharacter(latinchar) 
{
    var regexpr = /^[\x00-\xFF]+$/;
    if(latinchar.val() != '') {
        if(!latinchar.val().match(regexpr)) {
        	latinchar.after('<div class="error">Only Latin characters are available</div>');
        	latinchar.css('backgroundColor', "#FF0000");
        	latinchar.animate({backgroundColor: ''}, 2000);
            return true;
        }
    }
    return false;
}

function checkPhoneNumber(m) 
{
    var phoneNo = /^[+]?(\d+[-]?)+\d+$/;
 
    if(m.val() != '') {
        if(!m.val().match(phoneNo)) {
        	m.after('<div class="error">Invalid Telephone number</div>');
        	m.css('backgroundColor', "#FF0000");
        	m.animate({backgroundColor: ''}, 2000);
        	//setTimeout(function(){ m.css('backgroundColor', ""); }, 2000);
            return true;
        }
    }
    return false;
}

function checkExtensionNumber(e) 
{
    var extNo = /^[\d]{1,5}$/;
 
    if(e.val() != '') {
        if(!e.val().match(extNo)) {
        	e.after('<div class="error">Invalid Extension number</div>');
        	e.css('backgroundColor', "#FF0000");
        	e.animate({backgroundColor: ''}, 2000);
            return true;
        }
    }
    return false;
}

function checkFaxNumber(f) 
{
    var faxNo = /^[+]?(\d+[-]?)+\d+$/;
 
    if(f.val() != '') {
        if(!f.val().match(faxNo)) {
        	f.after('<div class="error">Invalid FAX number</div>');
        	f.css('backgroundColor', "#FF0000");
        	f.animate({backgroundColor: ''}, 2000);
            return true;
        }
    }
    return false;
}

function checkJapanPostal(p) 
{
    var postalNo = /^\d{7}$/;
    if(p.val() != '') {
        if(!p.val().match(postalNo)) {
        	p.after('<div class="error">Invalid Postal Number</div>');
        	p.css('backgroundColor', "#FF0000");
        	p.animate({backgroundColor: ''}, 2000);
            return true;
        }
    }
    return false;
}

function checkUSPostal(p) 
{
    var postalNo = /^[0-9]{5}([0-9]{4})?$/;
 
    if(p.val() != '') {
        if(!p.val().match(postalNo)) {
        	p.after('<div class="error">Invalid Postal Number</div>');
        	p.css('backgroundColor', "#FF0000");
        	p.animate({backgroundColor: ''}, 2000);
            return true;
        }
    }
    return false;
}

function checkCanadaPostal(p) 
{
    var postalNo = /^[0-9A-Z]{3} [0-9A-Z]{3}$/;
 
    if(p.val() != '') {
        if(!p.val().match(postalNo)) {
        	p.after('<div class="error">Invalid Postal Number</div>');
        	p.css('backgroundColor', "#FF0000");
        	p.animate({backgroundColor: ''}, 2000);
            return true;
        }
    }
    return false;
}

function addPostalNumberField()
{
	$(".postalnumber").remove();
	countryCodeNumeric = $('#country_code_numeric').val().trim();
	if (countryCodeNumeric == '392') {
		var japanPostalHtml = '<tr class="postalnumber"><td id="postal_no_c_label" scope="col" width="20%" valign="top">Postal Number: </td><td colspan="3" width="" valign="top"><input id="postal_no_japan" name="postal_no_japan" size="30" maxlength="9"  title="" type="text"></td></tr>';
		$("#ndb_addr1").parent().parent().parent().prepend(japanPostalHtml);
	}
	if (countryCodeNumeric == '840' || countryCodeNumeric == '124') {
		$("#c_state_id_c").parent().attr("colspan",1);
		var USPostalHtml = '<td class="postalnumber"  id="postal_no_c_label" scope="col" width="30%" valign="top">Postal Number: </td><td class="postalnumber" id="USCanadaPostal" width="30%" valign="top"><input id="postal_no_us" name="postal_no_us" size="30" maxlength="9" title="" type="text"></td>';
		$("#state_general_c_label").parent().append(USPostalHtml);
	}
}

function checkValidEmail(e) 
{
	var email_address_val = e.val();
	var email_address = trim(email_address_val);

	if (email_address != "") {
        if(email_address.length < EmailAddressMinLength)
        {
            e.after('<div class="error">Invalid Value: Email address</div>');
            e.css('backgroundColor', "#FF0000");
            e.animate({backgroundColor: ''}, 2000);
            return true;
        }
    }
    
	return false;
	
}

function customValidate()
{
	var error_count = 0;
	$('.error').remove();
	$('#ContactsemailAddressesTable0 input[type=text]').each(function() {
		 var fld = $(this);
		 var id = fld.attr('id');
		 var name = fld.attr('name');
	     // remove sugar email validation on email addresses 
	     removeFromValidate('form_QuickCreate_Contacts', name);
	     // add email validation on email addresses
	     
	     var trid = $(this).closest('tr').attr('id');
	     
	     if (checkValidEmail($('#'+id))) {
	    	 error_count = error_count+1;
	     }
		 
	});
	if (checkLatinCharacter($('#last_name'))) {
		error_count = error_count+1;
    }
	if (checkLatinCharacter($('#division_1_c'))) {
		error_count = error_count+1;
    }
	if (checkPhoneNumber($('#phone_work'))) {
    	error_count = error_count+1;
    }
	if (checkExtensionNumber($('#ext_no_c'))) {
		error_count = error_count+1;
    }
	if (checkFaxNumber($('#phone_fax'))) {
		error_count = error_count+1;
    }
	var countryCodeNumeric = $('#country_code_numeric').val().trim();
    if (countryCodeNumeric == '392') {
    	if (checkJapanPostal($('#postal_no_japan'))) {
    		error_count = error_count+1;
    	}
    } else if (countryCodeNumeric == '840') {
    	 if(checkUSPostal($('#postal_no_us'))) {
    		 error_count = error_count+1;
         }
    } else if (countryCodeNumeric == '124') {
    	if(checkCanadaPostal($('#postal_no_us'))) {
    		error_count = error_count+1;
        }
    }
    
    if (error_count > 0) return false;
    
	return true;
}