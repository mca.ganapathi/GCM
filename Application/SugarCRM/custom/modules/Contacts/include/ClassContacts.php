<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (?MSA?), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

/**
 * ClassContacts Utils Class
 */
class ClassContacts extends ModUtils
{

    /**
     * Class Variable
     *
     * @var object
     */
    public $db;

    public function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'constructor');
        global $db;
        $this->db = $db;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method to get Country in Id=>array (Name & Country Code) pair
     *
     * @return array $list array('id'=>'name')
     * @author Shrikant.Gaware
     * @since Jan 20, 2016
     */
    public function getCountryIdNamePair()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method to get Country in Id=>array (Name & Country Code) pair');
        $list      = array();
        $bean      = BeanFactory::getBean('c_country');
        $bean_list = $bean->get_full_list('name');
        if (is_array($bean_list) && (count($bean_list)>0)) {
            foreach ($bean_list as $modBean) {
                $list[$modBean->id] = array('name' => $modBean->name, 'code' => $modBean->country_code_numeric);
            }            
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'list : '.print_r($list,true), '');
        return $list;
    }

    /**
     * Method to get states in Id=>Name pair for requested country
     *
     * @param string $cid UUID of Country record
     * @return array $list array('id'=>'name')
     * @author Shrikant.Gaware
     * @since Jan 21, 2016
     */
    public function getStateIdNamePair($cid)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'cid : '.$cid, 'Method to get states in Id=>Name pair for requested country');
        $list = array();
        $bean = BeanFactory::getBean('c_country');
        $bean->retrieve($cid);
        $bean_list = $bean->get_linked_beans('c_country_c_state_1', 'c_State', array(0 => 'name'));
        if (is_array($bean_list) && (count($bean_list)>0)) {
            foreach ($bean_list as $modBean) {
                $list[$modBean->id] = $modBean->name;
            }
            asort($list);
        }        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'list : '.print_r($list,true), '');
        return $list;
    }
    
    /**
     * Fetch fields from Accounts (Japan Specific) Module of relative record
     *
     * @author : Sagar Salunkhe
     * @since Nov 24, 2015
     */
    public function fetchAccountJPFields(&$bean)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Fetch fields from Accounts (Japan Specific) Module of relative record');
        $obj_accounts_js = new js_Accounts_js();
        $obj_accounts_js->retrieve_by_string_fields(array(
            'contact_id_c' => $bean->id,
        ));
        $bean->ndb_addr1 = $obj_accounts_js->addr1;
        $bean->ndb_addr2 = $obj_accounts_js->addr2;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return;
    }   
    
}
