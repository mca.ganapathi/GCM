<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * Class to handle before save
 */
class ClassContactBeforeSave
{

    /**
     * method to save Accounts (japan specific) fields present on Accounts edit screen
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author Sagar.Salunkhe
     * @since 10-Oct-2015
     */
    public function setAcountJSEntry($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'method to save Accounts (japan specific) fields present on Accounts edit screen');
        // logic hook should get called only when user is editing record from Sugar screen edit view
        if(isset($_REQUEST['module']) && $_REQUEST['module'] == 'Contacts'){
            // Account_js module object
            $obj_accounts_js_bean = BeanFactory::getBean('js_Accounts_js');
            $obj_accounts_js_bean->retrieve_by_string_fields(array(
                'contact_id_c' => $bean->id,
            ));
            $obj_accounts_js_bean->retrieve($obj_accounts_js_bean->id);
            // update if record exists in module or create new record
            $obj_accounts_js_bean->contact_id_c = $bean->id;
            $obj_country_before = BeanFactory::getBean('c_country');
            $obj_country_before->retrieve($bean->fetched_row['c_country_id_c']);
            $obj_country_after = BeanFactory::getBean('c_country');
            $obj_country_after->retrieve($bean->c_country_id_c);
            $add_one = '';
            $add_two = '';
            if ($_POST['country_code_numeric'] == '392') {
                $add_one = $_POST['ndb_addr1'];
                $add_two = $_POST['ndb_addr2'];
            }
            $obj_accounts_js_bean->addr1 = $add_one; // Address 1 [Japan specific]
            $obj_accounts_js_bean->addr2 = $add_two; // Address 2 [Japan specific]
            $obj_accounts_js_bean->save();
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * method to set custom audit entry
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     */
    public function setCustomAuditEntry($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'method to set custom audit entry');
        if ((isset($_POST['record']) && $_POST['record'] != '') || !empty($bean->fetched_row)) {
            if ($bean->account_name != $bean->fetched_rel_row['account_name']) {
                // create an array to audit changes in the calls module�s audit table
                $audit_entry               = array();
                $audit_entry['field_name'] = 'account_name';
                $audit_entry['data_type']  = 'text';
                $audit_entry['before']     = $bean->fetched_rel_row['account_name'];
                $audit_entry['after']      = $bean->account_name;
                // save audit entry
                $bean->db->save_audit_records($bean, $audit_entry);
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'audit_entry : '.print_r($audit_entry,true), 'to check audit fields');
            }

            if ($bean->c_country_id_c != $bean->fetched_row['c_country_id_c']) {
                $obj_country_before = BeanFactory::getBean('c_country');
                $obj_country_before->retrieve($bean->fetched_row['c_country_id_c']);
                $obj_country_after = BeanFactory::getBean('c_country');
                $obj_country_after->retrieve($bean->c_country_id_c);
                // create an array to audit changes in the calls module�s audit table
                $audit_entry               = array();
                $audit_entry['field_name'] = 'country_code_c';
                $audit_entry['data_type']  = 'text';
                $audit_entry['before']     = $obj_country_before->name;
                $audit_entry['after']      = $obj_country_after->name;
                // save audit entry
                $bean->db->save_audit_records($bean, $audit_entry);
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'audit_entry : '.print_r($audit_entry,true), 'to check audit fields');
            }

            if ($bean->c_state_id_c != $bean->fetched_row['c_state_id_c']) {
                $obj_state_before = BeanFactory::getBean('c_State');
                $obj_state_before->retrieve($bean->fetched_row['c_state_id_c']);
                $obj_state_after = BeanFactory::getBean('c_State');
                $obj_state_after->retrieve($bean->c_state_id_c);
                // create an array to audit changes in the calls module�s audit table
                $audit_entry               = array();
                $audit_entry['field_name'] = 'state_general_c';
                $audit_entry['data_type']  = 'text';
                $audit_entry['before']     = $obj_state_before->name;
                $audit_entry['after']      = $obj_state_after->name;
                // save audit entry
                $bean->db->save_audit_records($bean, $audit_entry);
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'audit_entry : '.print_r($audit_entry,true), 'to check audit fields');
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * method to Set Accounts team to global.
     *
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook . 0event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author Debasish.Gupta
     * @since Jun 08, 2016
     */
    public function saveAccountsTeam($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'method to Set Accounts team to global');
        $bean->team_id     = 1;
        $bean->team_set_id = 1;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}
