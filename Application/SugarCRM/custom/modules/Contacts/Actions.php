<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
include_once 'custom/modules/Contacts/include/ClassContacts.php';

/**
 * Actions Class to Handle Ajax Request
 */
class Actions extends ClassContacts
{

    /**
     * Method to get all related states
     *
     * @author Shrikant.Gaware
     * @since Jan 21, 2016
     */
    public function getStates()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method to get all related states');       
        $cid = ((!empty($_REQUEST['c_id']))?$_REQUEST['c_id']:null);
        $data = $this->getStateIdNamePair($cid);
        $data = ((is_array($data) && (count($data)>0))?$data:array());
        $this->outputData($data);       
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');      
    }
}

// Initialize action class object
$a = new Actions();
$a->process();
