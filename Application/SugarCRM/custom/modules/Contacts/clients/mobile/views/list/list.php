<?php

$viewdefs['Contacts']['mobile']['view']['list'] = array(
    'panels' => array(
        0 => array(
            'label'  => 'LBL_PANEL_DEFAULT',
            'fields' => array(
                0  => array(
                    'name'           => 'name',
                    'label'          => 'LBL_NAME',
                    'default'        => true,
                    'enabled'        => true,
                    'link'           => true,
                    'related_fields' => array(
                        0 => 'first_name',
                        1 => 'last_name',
                        2 => 'salutation',
                    ),
                    'width'          => '20%',
                    'orderBy'        => 'last_name',
                ),
                1  => array(
                    'name'    => 'title',
                    'label'   => 'LBL_TITLE',
                    'enabled' => true,
                    'width'   => '15%',
                    'default' => true,
                ),
                2  => array(
                    'name'    => 'phone_work',
                    'label'   => 'LBL_OFFICE_PHONE',
                    'enabled' => true,
                    'width'   => '15%',
                    'default' => true,
                ),
                3  => array(
                    'name'    => 'do_not_call',
                    'default' => false,
                    'enabled' => true,
                    'width'   => '10',
                    'label'   => 'LBL_DO_NOT_CALL',
                ),
                4  => array(
                    'name'    => 'phone_home',
                    'enabled' => true,
                    'width'   => '10',
                    'default' => false,
                    'label'   => 'LBL_HOME_PHONE',
                ),
                5  => array(
                    'name'    => 'phone_mobile',
                    'enabled' => true,
                    'width'   => '10',
                    'default' => false,
                    'label'   => 'LBL_MOBILE_PHONE',
                ),
                6  => array(
                    'name'    => 'phone_other',
                    'default' => false,
                    'enabled' => true,
                    'width'   => '10',
                    'label'   => 'LBL_WORK_PHONE',
                ),
                7  => array(
                    'name'    => 'phone_fax',
                    'default' => false,
                    'enabled' => true,
                    'width'   => '10',
                    'label'   => 'LBL_FAX_PHONE',
                ),
                8  => array(
                    'name'    => 'date_entered',
                    'default' => false,
                    'enabled' => true,
                    'width'   => '10',
                    'label'   => 'LBL_DATE_ENTERED',
                ),
                9  => array(
                    'name'    => 'created_by_name',
                    'default' => false,
                    'enabled' => true,
                    'width'   => '10',
                    'label'   => 'LBL_CREATED',
                ),
                10 => array(
                    'name'    => 'team_name',
                    'default' => true,
                    'enabled' => true,
                    'width'   => '10',
                    'label'   => 'LBL_TEAM',
                ),
            ),
        ),
    ),
);
