<?php

$viewdefs['Contacts']['base']['view']['subpanel-for-accounts'] = array(
    'panels' => array(
        0 => array(
            'name'   => 'panel_header',
            'label'  => 'LBL_PANEL_1',
            'fields' => array(
                0 => array(
                    'name'    => 'name',
                    'default' => true,
                    'label'   => 'LBL_LIST_NAME',
                    'enabled' => true,
                    'link'    => true,
                    'fields'  => array(
                        0 => 'first_name',
                        1 => 'last_name',
                        2 => 'salutation',
                        3 => 'title',
                    ),
                    'type'    => 'fullname',
                ),
                1 => array(
                    'type'    => 'varchar',
                    'default' => true,
                    'link'    => true,
                    'label'   => 'LBL_NAME_2',
                    'enabled' => true,
                    'name'    => 'name_2_c',
                ),
                2 => array(
                    'type'              => 'relate',
                    'link'              => true,
                    'default'           => true,
                    'target_module'     => 'Accounts',
                    'target_record_key' => 'account_id',
                    'label'             => 'LBL_ACCOUNT_NAME',
                    'enabled'           => true,
                    'name'              => 'account_name',
                ),
                3 => array(
                    'type'    => 'enum',
                    'default' => true,
                    'label'   => 'LBL_CONTACT_TYPE',
                    'enabled' => true,
                    'name'    => 'contact_type_c',
                ),
                4 => array(
                    'name'     => 'email',
                    'sortable' => false,
                    'default'  => true,
                    'label'    => 'LBL_LIST_EMAIL',
                    'enabled'  => true,
                    'type'     => 'email',
                ),
                5 => array(
                    'type'    => 'datetime',
                    'default' => true,
                    'label'   => 'LBL_DATE_ENTERED',
                    'enabled' => true,
                    'name'    => 'date_entered',
                ),
                6 => array(
                    'type'    => 'datetime',
                    'default' => true,
                    'label'   => 'LBL_DATE_MODIFIED',
                    'enabled' => true,
                    'name'    => 'date_modified',
                ),
            ),
        ),
    ),
    'type'   => 'subpanel-list',
);
