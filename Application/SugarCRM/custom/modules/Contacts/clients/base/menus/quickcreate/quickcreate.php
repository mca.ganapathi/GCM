<?php

$viewdefs['Contacts']['base']['menu']['quickcreate'] = array(
    'layout'  => 'create',
    'label'   => 'LNK_NEW_CONTACT',
    'visible' => false,
    'icon'    => 'fa-plus',
    'related' => array(
        0 => array(
            'module' => 'Accounts',
            'link'   => 'contacts',
        ),
        1 => array(
            'module' => 'Opportunities',
            'link'   => 'contacts',
        ),
    ),
    'order'   => 5,
);
