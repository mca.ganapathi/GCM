<?php

$viewdefs['Contacts']['base']['filter']['default'] = array(
    'default_filter' => 'all_records',
    'fields'         => array(
        'last_name'          => array(
        ),
        'account_name'       => array(
            'dbFields' => array(
            ),
        ),
        'name_2_c'           => array(
            'type'    => 'varchar',
            'default' => true,
            'width'   => '10%',
            'name'    => 'name_2_c',
            'vname'   => 'LBL_NAME_2',
        ),
        'contact_type_c'     => array(
            'type'    => 'enum',
            'default' => true,
            'studio'  => 'visible',
            'width'   => '10%',
            'name'    => 'contact_type_c',
            'vname'   => 'LBL_CONTACT_TYPE',
        ),
        'phone_mobile'       => array(
        ),
        'title'              => array(
        ),
        'title_2_c'          => array(
            'type'    => 'varchar',
            'default' => true,
            'width'   => '10%',
            'name'    => 'title_2_c',
            'vname'   => 'LBL_TITLE_2',
        ),
        'division_2_c'       => array(
            'type'    => 'varchar',
            'default' => true,
            'width'   => '10%',
            'name'    => 'division_2_c',
            'vname'   => 'LBL_DIVISION_2',
        ),
        'division_1_c'       => array(
            'type'    => 'varchar',
            'default' => true,
            'width'   => '10%',
            'name'    => 'division_1_c',
            'vname'   => 'LBL_DIVISION_1',
        ),
        'assigned_user_name' => array(
        ),
        '$owner'             => array(
            'predefined_filter' => true,
            'vname'             => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite'          => array(
            'predefined_filter' => true,
            'vname'             => 'LBL_FAVORITES_FILTER',
        ),
    ),
);
