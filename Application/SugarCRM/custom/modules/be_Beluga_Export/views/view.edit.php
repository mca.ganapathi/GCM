<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ('MSA'), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

require_once ('include/MVC/View/views/view.edit.php');

class be_Beluga_ExportViewEdit extends ViewEdit
{

    public function be_Beluga_ExportViewEdit()
    {
        // call parent ViewEdit method
        parent::__construct();
    }

    public function preDisplay()
    {
        // call parent preDisplay method
        parent::preDisplay();
    }

    public function display()
    {
        // call parent display method
        global $current_user;
        // Instantiate the TimeDate Class
        $timeDate = new TimeDate();
        $firstDay = date('Y-m-d', strtotime('first day of last month'));
        $lastDay = date('Y-m-d', strtotime('last day of last month'));
        
        // Call the function
        $localStartDate = $timeDate->to_display_date($firstDay, true, true, $current_user);
        $localEndDate = $timeDate->to_display_date($lastDay, true, true, $current_user);
        $this->bean->start_date = $localStartDate;
        $this->bean->end_date = $localEndDate;
        parent::display();
    }
}