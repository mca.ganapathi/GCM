<?php
$module_name = 'be_Beluga_Export';
$viewdefs [$module_name] = array (
		'EditView' => array (
				'templateMeta' => array (
						'form' => array (
								'buttons' => array (
										0 => array (
												'customCode' => '<input type="submit" value="{sugar_translate label=\'LBL_EXPORT_CSV\' module=\'be_Beluga_Export\'}"
			 name="button" onclick="this.form.return_module.value=\'be_Beluga_Export\'; this.form.return_action.value=\'EditView\'; this.form.action.value=\'GCMExportCSV\'; window.onbeforeunload = null; return  exportCSV();" class="button" accesskey="A" title="Export CSV [Alt+A]" id="btn_export_csv" />'
										),										
								) 
						),
						'maxColumns' => '2',
						'widths' => array (
								0 => array (
										'label' => '10',
										'field' => '30' 
								),
								1 => array (
										'label' => '10',
										'field' => '30' 
								) 
						),
						 'includes' => 
				  array (
						 0 => 
						array (
						  'file' => 'cache/include/javascript/sugar_grp_yui_widgets.js',
						),
						1 => 
						array (
						  'file' =>  'custom/modules/be_Beluga_Export/include/js/EditView.js',
						),      
					  ),
						'useTabs' => false,
						'tabDefs' => array (
								'DEFAULT' => array (
										'newTab' => false,
										'panelDefault' => 'expanded' 
								) 
						),
						'syncDetailEditViews' => true 
				),
				'panels' => array (
						'default' => array (
								0 => array (
										0 => array (
												'name' => 'start_date',
												'label' => 'LBL_START_DATE' 
										) 
								),
								1 => array (
										0 => array (
												'name' => 'end_date',
												'label' => 'LBL_END_DATE' 
										) 
								) 
						) 
				) 
		) 
);
?>