<?php
require_once ('BelugaConvConst.php');
require_once ('BelugaCharConst.php');

/** */
class BelugaCsvConverter {

	// -------- --------
	static $IS_LOG = true;
	// -------- --------
	const CHAR_SPACE = '　';
	const CHAR_EMPTY = '';

	// -------- encoding --------
	const ENCODING_UTF8 = "UTF-8";
	const ENCODING_CP932 = "sjis-win";

	// -------- column types --------
	/** em character */
	const COL_TYPE_EM = 1;
	/** en character */
	const COL_TYPE_EN = 2;
	/** en numbers */
	const COL_TYPE_EN_NUM = 3;
	/** en letters and numbers */
	const COL_TYPE_EN_ALPHANUM = 4;
	/** em katakana characters */
	const COL_TYPE_EM_KATAKANA = 5;
	/** characters for Bank VAN */
	const COL_TYPE_BANK_VAN = 6;
	/** characters for Bank account name */
	const COL_TYPE_BANK_ACNT_NAME = 7;
	/** code value */
	const COL_TYPE_CODE_VALUE = 8;

	// -------- settings per line --------
	private $history_id = '';
	private $contract_id = '';
	private $global_contract_id = '';
	private $lineNo = '';

	/** Constructor.
	 *
	 * @param string $history_id
	 * @param string $contract_id
	 * @param string $lineNo */
	function __construct($history_id = '', $contract_id = '', $global_contract_id = '', $lineNo = '') {
		$this->history_id = $history_id;
		$this->contract_id = $contract_id;
		$this->global_contract_id = $global_contract_id;
		$this->lineNo = $lineNo;
	}

	// ---------------- public function ----------------
	public function convertEm($param, $idx) {
		$str = $param;
		// マニュアル文字寄せ
		$str = self::unify ( $str, BelugaConvConst::$CHAR_HYPHEN_ORG_FOR_EM, BelugaConvConst::CHAR_HYPHEN_DEST );
		$str = self::unify ( $str, BelugaConvConst::$CHAR_TILDE_ORG, BelugaConvConst::CHAR_TILDE_DEST );
		// 半角制御文字削除
		$str = BelugaCharConst::removeControllCharacter ( $str );
		// 半角→全角変換
		$str = strtr ( $str, BelugaConvConst::$CHAR_EN_TO_EM_ALL );
		// 代替漢字変換
		$str = strtr ( $str, BelugaConvConst::$CHAR_ALT_KANJI );
		// 利用禁止文字変換
		$str = self::replaceUnavailables ( BelugaCharConst::$CHAR_LIST_EM, $str, $unavailables, self::CHAR_SPACE );
		// log if changed
		$this->logIfChanged ( $idx, $param, $str );
		// 文字コード変換
		$str = mb_convert_encoding ( $str, self::ENCODING_CP932, self::ENCODING_UTF8 );
		return $str;
	}
	public function convertEn($param, $idx) {
		$str = $param;
		// 利用禁止文字変換
		$str = self::replaceUnavailables ( BelugaCharConst::$CHAR_LIST_EN, $str, $unavailables, self::CHAR_EMPTY );
		// log if changed
		$this->logIfChanged ( $idx, $param, $str );
		// 文字コード変換
		$str = mb_convert_encoding ( $str, self::ENCODING_CP932, self::ENCODING_UTF8 );

		return $str;
	}
	public function convertEnNum($param, $idx) {
		$str = $param;
		// 利用禁止文字変換
		$str = self::replaceUnavailables ( BelugaCharConst::$CHAR_LIST_EN_NUM, $str, $unavailables, self::CHAR_EMPTY );
		// log if changed
		$this->logIfChanged ( $idx, $param, $str );

		return $str;
	}
	public function convertEnAlphanum($param, $idx) {
		$str = $param;
		// 利用禁止文字変換
		$str = self::replaceUnavailables ( BelugaCharConst::$CHAR_LIST_EN_ALPHANUM, $str, $unavailables, self::CHAR_EMPTY );
		// log if changed
		$this->logIfChanged ( $idx, $param, $str );

		return $str;
	}
	public function convertEmKatakana($param, $idx) {
		$str = $param;
		// マニュアル文字寄せ
		$str = self::unify ( $str, BelugaConvConst::$CHAR_HYPHEN_ORG, BelugaConvConst::CHAR_HYPHEN_DEST );
		// 半角制御文字削除
		$str = BelugaCharConst::removeControllCharacter ( $str );
		// 半角カナ→全角カナ変換
		$str = strtr ( $str, BelugaConvConst::$CHAR_EN_TO_EM_KATAKANA );
		// 利用禁止文字変換
		$str = self::replaceUnavailables ( BelugaCharConst::$CHAR_LIST_EM_KATAKANA, $str, $unavailables, self::CHAR_SPACE );
		// log if changed
		$this->logIfChanged ( $idx, $param, $str );

		// 文字コード変換
		$str = mb_convert_encoding ( $str, self::ENCODING_CP932, self::ENCODING_UTF8 );

		return $str;
	}
	public function convertBankVan($param, $idx) {
		$str = $param;
		// 利用禁止文字変換
		$str = self::replaceUnavailables ( BelugaCharConst::$CHAR_LIST_BANK_VAN, $str, $unavailables, self::CHAR_EMPTY );
		// log if changed
		$this->logIfChanged ( $idx, $param, $str );

		return $str;
	}
	public function convertBankAcntName($param, $idx) {
		$str = $param;
		// マニュアル文字寄せ
		$str = self::unify ( $str, BelugaConvConst::$CHAR_HYPHEN_ORG, BelugaConvConst::CHAR_HYPHEN_DEST );
		// 半角制御文字削除
		$str = BelugaCharConst::removeControllCharacter ( $str );
		// 半角カナ→全角カナ変換
		$str = strtr ( $str, BelugaConvConst::$CHAR_EN_TO_EM_BANK_ACNT_NAME );
		// 利用禁止文字変換
		$str = self::replaceUnavailables ( BelugaCharConst::$CHAR_LIST_BANK_ACNT_NAME, $str, $unavailables, self::CHAR_SPACE );
		// log if changed
		$this->logIfChanged ( $idx, $param, $str );
		// 文字コード変換
		$str = mb_convert_encoding ( $str, self::ENCODING_CP932, self::ENCODING_UTF8 );

		return $str;
	}

	// ---------------- private function ----------------

	/** Unify the similar character.
	 *
	 * @param $param before
	 * @param $search convert from
	 * @param $repl convert to
	 * @return after */
	private static function unify($param, $search, $repl) {
		$ret = "";
		$strs = BelugaCharConst::mb_str_split ( $param );
		foreach ( $strs as $ch ) {
			$hex = self::stringToHex ( $ch );
			if (in_array ( $hex, $search )) {
				$ret .= self::hexToString ( $repl );
			} else {
				$ret .= $ch;
			}
		}
		return $ret;
	}

	/** replace the unavailable character.
	 *
	 * @param $abailables array of available characters
	 * @param $param character to be converted
	 * @param $unavailables if there is unavailable character
	 * @param $replchar replacement character
	 * @return character converted */
	private static function replaceUnavailables($availables, $param, &$unavailables, $replchar) {
		$strs = BelugaCharConst::mb_str_split ( $param );
		$ret = "";
		foreach ( $strs as $ch ) {
			if (in_array ( $ch, $availables )) {
				$ret .= $ch;
			} else {
				$ret .= $replchar;
			}
		}
		return $ret;
	}

	/** encode string to hex
	 *
	 * @param $param string
	 * @return parmeter encoded */
	private static function stringToHex($string) {
		$hex = '';
		for($i = 0; $i < strlen ( $string ); $i ++) {
			$hex .= dechex ( ord ( $string [$i] ) );
		}
		return $hex;
	}

	/** decode hex to string
	 *
	 * @param $param hex
	 * @return parmeter decoded */
	private static function hexToString($hex) {
		$string = '';
		for($i = 0; $i < strlen ( $hex ) - 1; $i += 2) {
			$string .= chr ( hexdec ( $hex [$i] . $hex [$i + 1] ) );
		}
		return $string;
	}
	public function logIfChanged($idx, $org, $dest) {
		$colNo = $idx + 1;
		if (self::$IS_LOG && $org != $dest) {
			$GLOBALS ['log']->warn ( "Value has been converted for BelugaCSV output. [gc_line_item_contract_history.id={$this->history_id}, gc_contracts.id={$this->history_id}, global_contract_id={$this->global_contract_id}, Line No={$this->lineNo}, Column No={$colNo}, From={$org}, To={$dest}]" );
		}
	}
}