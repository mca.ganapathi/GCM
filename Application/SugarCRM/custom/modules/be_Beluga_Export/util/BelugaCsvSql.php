<?php
class BelugaCsvSql {

	/** 登録区分「新規」を抽出するSQLの生成 */
	static function selectNewSql($start_date, $end_date, $offering_id_list) {
		$result  = self::selectBaseSql(1, $offering_id_list);  // 1 = 登録区分「新規」
		$result .= <<<EOF
WHERE
     v_gc_line_item_contract_history.deleted = 0
 AND (IFNULL(v_gc_line_item_contract_history.customer_billing_method,'')!="GBS_Billing" AND contacts_B.c_country_id_c=(SELECT id FROM c_country WHERE country_code_numeric='392'))	 
 AND v_gc_line_item_contract_history.billing_start_date_jst >= str_to_date('{$start_date}', '%Y%m%d')
 AND v_gc_line_item_contract_history.billing_start_date_jst < date_add(str_to_date('{$end_date}', '%Y%m%d'), INTERVAL 1 DAY)
 AND v_gc_line_item_contract_history.billing_start_date <= v_gc_line_item_contract_history.billing_end_date

ORDER BY v_gc_contracts.date_modified ASC
EOF;
		return $result;
	}


	/** 登録区分「変更」を抽出するSQLの生成 */
	static function selectModSql($start_date, $end_date, $offering_id_list) {
		$result  = self::selectBaseSql(2, $offering_id_list);  // 2 = 登録区分「変更」
		$result .= <<<EOF
WHERE
      v_gc_line_item_contract_history.deleted = 0
 AND (IFNULL(v_gc_line_item_contract_history.customer_billing_method,'')!="GBS_Billing" AND contacts_B.c_country_id_c=(SELECT id FROM c_country WHERE country_code_numeric='392'))	  
 AND  (
       (
        v_gc_contracts.date_modified_jst >= str_to_date('{$start_date}', '%Y%m%d') AND
        v_gc_contracts.date_modified_jst < date_add(str_to_date('{$end_date}', '%Y%m%d'), INTERVAL 1 DAY)
       ) OR (
        accounts_C.date_modified_jst >= str_to_date('{$start_date}', '%Y%m%d') AND
        accounts_C.date_modified_jst < date_add(str_to_date('{$end_date}', '%Y%m%d'), INTERVAL 1 DAY)
       )
      )
 AND  v_gc_line_item_contract_history.billing_start_date_jst < str_to_date('{$start_date}', '%Y%m%d')
 AND  v_gc_contracts.contract_status = 4

ORDER BY v_gc_contracts.date_modified ASC
EOF;
		return $result;
	}


	/** 登録区分「廃止」を抽出するSQLの生成 */
	static function selectDelSql($start_date, $end_date, $offering_id_list) {
		$result  = self::selectBaseSql(3, $offering_id_list);  // 3 = 登録区分「廃止」
		$result .= <<<EOF
WHERE
     v_gc_line_item_contract_history.deleted = 0
 AND (IFNULL(v_gc_line_item_contract_history.customer_billing_method,'')!="GBS_Billing" AND contacts_B.c_country_id_c=(SELECT id FROM c_country WHERE country_code_numeric='392'))	 
 AND v_gc_contracts.date_modified_jst >= str_to_date('{$start_date}', '%Y%m%d')
 AND v_gc_contracts.date_modified_jst < date_add(str_to_date('{$end_date}', '%Y%m%d'), INTERVAL 1 DAY)
 AND v_gc_line_item_contract_history.billing_start_date <= v_gc_line_item_contract_history.billing_end_date
 AND v_gc_contracts.contract_status = 2

ORDER BY v_gc_contracts.date_modified ASC
EOF;
		return $result;
	}


	private static function selectBaseSql($regist_type, $offering_id_list) {
		return <<<EOF
SELECT
     v_gc_line_item_contract_history.id as history_id
    ,v_gc_contracts.id as contract_id
    ,'{$regist_type}' AS regist_type
    ,v_gc_contracts.global_contract_id AS global_contract_id
    ,DATE_FORMAT(v_gc_line_item_contract_history.billing_start_date_jst,'%Y%m%d') AS billing_start_date
    ,DATE_FORMAT(v_gc_line_item_contract_history.billing_end_date_jst,  '%Y%m%d') AS billing_end_date
    ,accounts_cstm_C.common_customer_id_c AS common_customer_id_c
    ,v_gc_line_item_contract_history.payment_type
    ,gc_accounttransfer.name AS at_name
    ,gc_accounttransfer.bank_code AS at_bank_code
    ,gc_accounttransfer.branch_code AS at_branch_code
    ,gc_accounttransfer.deposit_type AS at_deposit_type
    ,gc_accounttransfer.account_no AS at_account_no
    ,gc_accounttransfer.account_no_display AS at_account_no_display
    ,gc_postaltransfer.name AS pt_name
    ,gc_postaltransfer.postal_passbook_mark AS pt_postal_passbook_mark
    ,gc_postaltransfer.postal_passbook AS pt_postal_passbook
    ,gc_postaltransfer.postal_passbook_no_display AS pt_postal_passbook_no_display
    ,gc_creditcard.credit_card_no AS credit_card_no
    ,gc_creditcard.expiration_date AS expiration_date
    ,gc_directdeposit.bank_code AS dd_bank_code
    ,gc_directdeposit.branch_code AS dd_branch_code
    ,gc_directdeposit.deposit_type AS dd_deposit_type
    ,gc_directdeposit.account_no AS dd_account_no
    ,gc_directdeposit.payee_contact_person_name_2 AS dd_payee_contact_person_name_2
    ,gc_directdeposit.payee_tel_no AS dd_payee_tel_no
    ,gc_directdeposit.payee_email_address AS dd_payee_email_address
    ,gc_directdeposit.remarks AS dd_remarks
    ,accounts_cstm_C.name_3_c AS contract_name_kana
    ,accounts_cstm_C.name_2_c AS contract_name_knj
    ,accounts_cstm_C.hq_postal_no_c AS contract_postal_no
    ,accounts_cstm_C.hq_addr_2_c AS contract_addr
    ,contacts_C.division_2_c AS contract_contact_division
    ,contacts_C.name_2_c AS contract_contact_name
    ,contacts_C.phone_work AS contract_contact_tel_no
    ,contacts_C.ext_no_c AS contract_contact_ext_no
    ,email_addresses_C.email_address AS contract_email
    ,contacts_C.phone_fax AS contract_phone_fax_no
    ,accounts_cstm_B.name_3_c AS billing_name_kana
    ,accounts_cstm_B.name_2_c AS billing_name_knj
    ,contacts_B.postal_no_c AS billing_poistal_no
    ,js_accounts_js.addr1 AS billing_addr_1
    ,js_accounts_js.addr2 AS billing_addr_2
    ,contacts_B.division_2_c AS billing_contact_division
    ,contacts_B.name_2_c AS billing_contact_name
    ,contacts_B.phone_work AS billing_contact_tel_no
    ,contacts_B.ext_no_c AS billing_contact_ext_no
    ,email_addresses_B.email_address AS billing_email
    ,contacts_B.phone_fax AS billing_phone_fax_no
    ,gc_salesrepresentative.sales_channel_code AS sales_channel_code
    ,gc_salesrepresentative.sales_rep_name_1 AS sales_rep_name
    ,gc_salesrepresentative.division_2 AS sales_rep_division
    ,gc_salesrepresentative.tel_no AS sales_rep_tel_no
    ,gc_salesrepresentative.ext_no AS sales_rep_ext_no
    ,gc_salesrepresentative.fax_no AS sales_rep_fax_no
    ,gc_salesrepresentative.email1 AS sales_email
    ,v_gc_contracts.product_offering AS product_offering
FROM
    (SELECT *
          , CONVERT_TZ(billing_start_date, '+00:00', '+09:00') AS billing_start_date_jst
          , CONVERT_TZ(billing_end_date  , '+00:00', '+09:00') AS billing_end_date_jst
     FROM gc_line_item_contract_history
    )  AS v_gc_line_item_contract_history
INNER JOIN
    (SELECT *
          , CONVERT_TZ(date_modified  , '+00:00', '+09:00') AS date_modified_jst
     FROM gc_contracts
    )  AS v_gc_contracts
 ON v_gc_line_item_contract_history.contracts_id = v_gc_contracts.id
AND v_gc_contracts.deleted = 0
AND v_gc_contracts.product_offering IN ({$offering_id_list})
LEFT JOIN
    gc_accounttransfer
 ON v_gc_line_item_contract_history.id = gc_accounttransfer.contract_history_id
AND gc_accounttransfer.deleted = 0
LEFT JOIN
    gc_postaltransfer
 ON v_gc_line_item_contract_history.id = gc_postaltransfer.contract_history_id
AND gc_postaltransfer.deleted = 0
LEFT JOIN
    gc_creditcard
 ON v_gc_line_item_contract_history.id = gc_creditcard.contract_history_id
AND gc_creditcard.deleted = 0
LEFT JOIN
    gc_directdeposit
 ON v_gc_line_item_contract_history.id = gc_directdeposit.contract_history_id
AND gc_directdeposit.deleted = 0
LEFT JOIN
    gc_salesrepresentative
 ON v_gc_contracts.id = gc_salesrepresentative.contracts_id
AND gc_salesrepresentative.deleted = 0

-- Contract
INNER JOIN (
  SELECT
    contacts.id as id
   ,contacts_gc_contracts_1_c.contacts_gc_contracts_1gc_contracts_idb as contracts_id
   ,contacts_cstm.postal_no_c
   ,contacts_cstm.division_2_c
   ,contacts_cstm.name_2_c
   ,contacts_cstm.ext_no_c
   ,contacts.phone_work
   ,contacts.phone_fax
  FROM
    contacts_gc_contracts_1_c
  INNER JOIN contacts_cstm
    ON contacts_gc_contracts_1_c.contacts_gc_contracts_1contacts_ida = contacts_cstm.id_c
  INNER JOIN contacts
    ON contacts_cstm.id_c = contacts.id
   AND contacts.deleted = 0
  WHERE
      contacts_cstm.contact_type_c = 'Contract'
  AND contacts_gc_contracts_1_c.deleted = 0
) contacts_C
 ON contacts_C.contracts_id = v_gc_contracts.id

INNER JOIN
    accounts_gc_contracts_1_c
 ON v_gc_contracts.id = accounts_gc_contracts_1_c.accounts_gc_contracts_1gc_contracts_idb
AND accounts_gc_contracts_1_c.deleted = 0
INNER JOIN
    accounts_cstm accounts_cstm_C
 ON accounts_gc_contracts_1_c.accounts_gc_contracts_1accounts_ida = accounts_cstm_C.id_c
INNER JOIN
    (SELECT *
          , CONVERT_TZ(date_modified  , '+00:00', '+09:00') AS date_modified_jst
     FROM accounts
    )  AS accounts_C
 ON accounts_cstm_C.id_c = accounts_C.id
AND accounts_C.deleted = 0

LEFT JOIN
    email_addr_bean_rel AS email_addr_bean_rel_C
 ON contacts_C.id = email_addr_bean_rel_C.bean_id
AND email_addr_bean_rel_C.deleted = 0
LEFT JOIN
    email_addresses AS email_addresses_C
 ON email_addr_bean_rel_C.email_address_id = email_addresses_C.id
AND email_addresses_C.deleted = 0

-- Billing

INNER JOIN (
  SELECT
    contacts.id as id
   ,contacts_cstm.postal_no_c
   ,contacts_cstm.division_2_c
   ,contacts_cstm.name_2_c
   ,contacts_cstm.ext_no_c
   ,contacts.phone_work
   ,contacts.phone_fax
   ,contacts_cstm.c_country_id_c
  FROM
    contacts
  INNER JOIN contacts_cstm
    ON contacts.id = contacts_cstm.id_c
  WHERE
      contacts_cstm.contact_type_c = 'Billing'  
  AND contacts.deleted = 0
) contacts_B
 ON contacts_B.id = v_gc_line_item_contract_history.bill_account_id

INNER JOIN
    accounts_contacts accounts_contacts_B
 ON contacts_B.id = accounts_contacts_B.contact_id
AND accounts_contacts_B.deleted = 0
INNER JOIN
    accounts_cstm accounts_cstm_B
 ON accounts_contacts_B.account_id = accounts_cstm_B.id_c
INNER JOIN
    accounts accounts_B
 ON accounts_cstm_B.id_c = accounts_B.id
AND accounts_B.deleted = 0

LEFT JOIN
    email_addr_bean_rel AS email_addr_bean_rel_B
 ON contacts_B.id = email_addr_bean_rel_B.bean_id
AND email_addr_bean_rel_B.deleted = 0
LEFT JOIN
    email_addresses AS email_addresses_B
 ON email_addr_bean_rel_B.email_address_id = email_addresses_B.id
AND email_addresses_B.deleted = 0
LEFT JOIN
    js_accounts_js
 ON contacts_B.id = js_accounts_js.contact_id_c
AND js_accounts_js.deleted = 0

EOF;
	}
}

