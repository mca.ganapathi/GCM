<?php
require_once 'util/BelugaCsvSql.php';
require_once 'util/BelugaCsvConverter.php';

/** */
class BelugaExport {

	/** payment type : account transfer */
	const PAYMENT_TYPE_ACNT_TRANSFER = '10';
	/** payment type : postal transfer */
	const PAYMENT_TYPE_POSTAL_TRANSFER = '30';
	/** payment type : credit card */
	const PAYMENT_TYPE_CREDIT_CARD = '60';
	/** payment type : direct deposit */
	const PAYMENT_TYPE_DIRECT_DEPOSIT = '00';

	/** customer type : indivisual */
	const CUSTOMER_TYPE_INDIVISUAL = '01';
	/** customer type : corporate */
	const CUSTOMER_TYPE_CORPORATE = '02';

	/** address input type : get from external definition */
	const ADDR_INPUT_TYPE_FROM_EXT_DEF = '1';
	/** address input type : Direct input (destination 2 lines) */
	const ADDR_INPUT_TYPE_DIRECT_2LINES = '2';
	/** address input type : Direct input (destination 3 lines) */
	const ADDR_INPUT_TYPE_DIRECT_3LINES = '3';

	/** Address delimiter */
	const DELIM_ADDR = '　';


	/** AbsolutePathRoot for PMS-API */
	const PMS_ABSOLUTE_PATH_ROOT = '/pms/api';

	/** Option Key: Beluga Product Type */
	const PMS_OPTION_KEY_BELUGA_PRODUCT_TYPE = 'beluga_product_type';

	/** Option Key: Beluga Service Type */
	const PMS_OPTION_KEY_BELUGA_SERVICE_TYPE = 'beluga_service_type';

	private $productInfoForBeluga;
	private $strOfferingIdList;


	/** initialize environment */
	public function initialize() {
		// set encode for this converter
		mb_internal_encoding ( 'UTF-8' );
		mb_regex_encoding ( 'UTF-8' );
	}

	/**
	 *
	 * @param string $startDate yyyyMMdd
	 * @param string $endDate   yyyyMMdd
	 * @param string $outputDir Path to output directory
	 * @return Download file name(FullPath)
	 */
	public function GCMExportCSV($startDate, $endDate, $outputDir) {

		// Log - begin message
		$GLOBALS['log']->info('Begin: BelugaExport - From=' . $startDate . ' To=' . $endDate);

		// init
		$this->initialize();

		// Get ProductInfo that exports to beluga from PMS
		$this->productInfoForBeluga = $this->requestBelugaProductsFromPMS();

		// Log - ProductInfo that exports to beluga
		$GLOBALS['log']->info('BelugaExport(PMS Defined): ' . print_r($this->productInfoForBeluga, true));

		$offeringIds = array();
		foreach($this->productInfoForBeluga as $key => $value) {
			$offeringIds[] = "'" . $key . "'";  // Encloses OfferingID by single quotation.
		}
		$this->strOfferingIdList = join(',' , $offeringIds);

		// TimeStamp(for apply to export files)
		$ts = date ( "YmdHis" );

		// 「新規」のエクスポート処理
		$fileNew = $this->exportNew($startDate, $endDate, $outputDir, $ts);

		// 「変更」「廃止」のエクスポート処理
		$fileChange = $this->exportChange($startDate, $endDate, $outputDir, $ts);

		// CSVファイルのアーカイブ処理(⇒クライアントにダウンロードさせるZIPファイル)
		$fileResult = $this->archiveFiles($outputDir, $ts, array($fileNew, $fileChange));

		// Log - finish message
		$GLOBALS['log']->info('End: BelugaExport - FilePath=' . $fileResult . ' HostName=' . gethostname());

		return $fileResult;
	}

	// ---------------- private ----------------

	/**
	 * BELUGAに出力する「OfferingIDとbeluga_product_type,beluga_service_typeの対応情報」をPMSから取得する
	 * @return array(OfferingID => array('beluga_product_type'=>prdType, 'beluga_service_type'=>srvType))
	 */
	private function requestBelugaProductsFromPMS() {
		global $sugar_config;
		$belugaProducts = array();

		$pmsClient = new PmsClient($sugar_config['pms_product']['base_url'],
		                           self::PMS_ABSOLUTE_PATH_ROOT,
		                           $sugar_config['pms_product']['public_key'],
		                           $sugar_config['pms_product']['secret_key']);

		// Requests Offering(List)
		$offeringXml = $pmsClient->getOffering($sugar_config['pms_product']['catalog_id']);
		// Extracts ProductOfferingIDs(array) from $offeringXml
		$offeringIDs = self::extractOfferingIdList($offeringXml);

		// Requests OfferingDetail(for each ProductOfferingId)
		foreach($offeringIDs as $offeringId) {
			$offeringDetailXml = $pmsClient->getOfferingDetail($sugar_config['pms_product']['catalog_id'], $offeringId);
			// Extracts BelugaInformation(i.e.beluga_product_type,beluga_service_type) from $offeringDetailXml
			$optionTagValues = self::extractOfferingOptionValues($offeringDetailXml, $sugar_config['pms_product']['catalog_id'], $offeringId);
			// Checks BelugaInformation was defined or not by option tag
			if(array_key_exists(self::PMS_OPTION_KEY_BELUGA_PRODUCT_TYPE, $optionTagValues) &&
			   array_key_exists(self::PMS_OPTION_KEY_BELUGA_SERVICE_TYPE, $optionTagValues)) {
				$belugaProducts[$offeringId] = array(self::PMS_OPTION_KEY_BELUGA_PRODUCT_TYPE => $optionTagValues[self::PMS_OPTION_KEY_BELUGA_PRODUCT_TYPE],
				                                     self::PMS_OPTION_KEY_BELUGA_SERVICE_TYPE => $optionTagValues[self::PMS_OPTION_KEY_BELUGA_SERVICE_TYPE]);
			}
		}
		return $belugaProducts;
	}

	/**
	 * Extracts /listcatalogofferingresponse/offerings//offering/id Elements value
	 * @param string $offeringXml XML string of offering
	 * @return array
	 */
	private static function extractOfferingIdList($offeringXml) {
		$result = array();
		$sxe = new SimpleXMLElement($offeringXml);

		$offeringElements = $sxe->xpath('/listcatalogofferingresponse/offerings/offering');
		foreach($offeringElements as $offeringElement) {
			$offeringId = self::getElementValueByXpath($offeringElement, 'id');
			if(!empty($offeringId)) {
				$result[] = $offeringId;
			}
		}
		return $result;
	}

	/**
	 * Extracts /listcatalogofferingdetailresponse/listofferingoption/options//option/key=>value Element value
	 * @param string $offeringDetailXml XML string of offering-detail
	 * @param string $catalogId   CatalogID  - for log(Warning message)
	 * @param string $offeringId  OfferingID - for log(Warning message)
	 * @return associate array(key => value)
	 */
	private static function extractOfferingOptionValues($offeringDetailXml, $catalogId, $offeringId) {
		$result = array();
		$sxe = new SimpleXMLElement($offeringDetailXml);

		$optionElements = $sxe->xpath('/listcatalogofferingdetailresponse/listofferingoption/options/option');
		foreach($optionElements as $optionElement) {
			$keyElementValue = self::getElementValueByXpath($optionElement, 'key');
			if(empty($keyElementValue)) {	// If 'key' is empty,
				continue;                	//     then ignore the option tag.
			}
			$valueElementValue = self::getElementValueByXpath($optionElement, 'value');
			if(empty($valueElementValue)) {	// If 'value' is empty even 'key' is exist, then output log as warning.
				$GLOBALS ['log']->warn ('(PMS) OptionValue was empty. CatalogID=' . $catalogId . ' OfferingID=' . $offeringId . ' OptionKey=' . $keyElementValue );
				continue;
			}
			$result[$keyElementValue] = $valueElementValue;   // key => value
		}
		return $result;
	}

	/**
	 * Get Element's value by XPath from XML(SimpleXMLElement)
	 * @param SimpleXMLElement $sxe
	 * @param string $xpath
	 * @return NULL|string
	 */
	private static function getElementValueByXpath($sxe, $xpath) {
		$elements = $sxe->xpath($xpath);
		if(empty($elements)) {
			return null;
		}
		return (string)$elements[0];
	}

	private function exportNew($startDate, $endDate, $outputDir, $ts) {
		// $db = DBManagerFactory::getInstance ();
		global $db;

		// open file
		$filePath = $this->filePath ( $outputDir, $ts, '_new.csv' );
		$fp = fopen ( $filePath, 'a' );

		// search data
		if(!empty($this->productInfoForBeluga)) {
			$sql = BelugaCsvSql::selectNewSql ( $startDate, $endDate, $this->strOfferingIdList );
			$result = $db->query ( $sql, true /*, "An error occurred during CSV output data acquisition for Beluga." */);

			$GLOBALS ['log']->debug ( print_r ( $result, true ) );

			$lineIdx = 0;
			while ( ($row = $db->fetchByAssoc ( $result, false )) != null ) {
				// echo print_r ( $row, true );

				// convert
				$cols = $this->convertRowForAdd($row, ++ $lineIdx);
				// write
				$this->_fputcsv ( $fp, $cols );
			}
		}

		// close file
		fclose ( $fp );

		return $filePath;
	}


	private function exportChange($startDate, $endDate, $outputDir, $ts) {
		global $db;

		// open file
		$filePath = $this->filePath ( $outputDir, $ts, '_change.csv' );
		$fp = fopen ( $filePath, 'a' );

		$lineIdx = 0;

		if(!empty($this->productInfoForBeluga)) {
			// search data(Modify)
			$sql = BelugaCsvSql::selectModSql ( $startDate, $endDate, $this->strOfferingIdList );
			$result = $db->query ( $sql, true /*, "An error occurred during CSV output data acquisition for Beluga." */);

			$GLOBALS ['log']->debug ( print_r ( $result, true ) );


			while ( ($row = $db->fetchByAssoc ( $result, false )) != null ) {
				// echo print_r ( $row, true );

				// convert
				$cols = $this->convertRowForMod($row, ++ $lineIdx);
				// write
				$this->_fputcsv ( $fp, $cols );
			}


			// search data(Deactivated)
			$sql = BelugaCsvSql::selectDelSql ( $startDate, $endDate, $this->strOfferingIdList );
			$result = $db->query ( $sql, true /*, "An error occurred during CSV output data acquisition for Beluga." */);

			$GLOBALS ['log']->debug ( print_r ( $result, true ) );


			while ( ($row = $db->fetchByAssoc ( $result, false )) != null ) {
				// echo print_r ( $row, true );

				// convert
				$cols = $this->convertRowForDel($row, ++ $lineIdx);
				// write
				$this->_fputcsv ( $fp, $cols );
			}
		}

		// close file
		fclose ( $fp );

		return $filePath;
	}

	/**
	 * アーカイブ(ZIP)生成処理
	 * @param string $outputDir ZIP生成先ディレクトリ
	 * @param string $ts        ZIPファイル名に付加するタイムスタンプ文字列
	 * @param array  $srcFiles  ZIPファイルに格納する実ファイル(フルパス)の配列
	 * @return string ZIPファイルパス(フルパス)
	 */
	private function archiveFiles($outputDir, $ts, $srcFiles) {
		$zip = new ZipArchive();
		// ZIPファイルをオープン
		// open file
		$filePath = $this->filePath ( $outputDir, $ts, '.zip' );
		$res = $zip->open($filePath, ZipArchive::CREATE);

		// zipファイルのオープンに成功した場合
		if ($res === true) {
			//  圧縮するファイルを指定
			foreach($srcFiles as $srcFile) {
				$zip->addFile($srcFile, basename($srcFile));
			}
			// ZIPファイルをクローズ(圧縮実行)
			$zip->close();
		}

		// ZIPファイルの生成が成功していたら、元ファイルを削除する
		if (file_exists($filePath)) {
			foreach($srcFiles as $srcFile) {
				unlink($srcFile);
			}
		}

		return $filePath;
	}


	private function convertRowForAdd($row, $lineNo) {
		// columns
		$cols = array ();
		// colum index
		$idx = 0;
		// gc_line_item_contract_history.id
		$history_id = $row ['history_id'];
		// gc_contracts.id
		$contract_id = $row ['contract_id'];
		// global_contract_id
		$global_contract_id = $row['global_contract_id'];
		// payment type
		$payType = $row ['payment_type'];

		$cv = new BelugaCsvConverter ( $history_id, $contract_id, $global_contract_id, $lineNo );

		// 001. 【必須】【】ファイル内通番 (※通番を設定)
		$cols [$idx ++] = $lineNo;
		// 002. 【必須】【半角数字】登録区分 [regist_type]
		$cols [$idx ++] = $cv->convertEnNum ( $row ['regist_type'], $idx );
		// 003. 【必須】【半角数字】契約識別番号 [global_contract_id]
		$cols [$idx ++] = $cv->convertEnNum ( $row ['global_contract_id'], $idx );
		// 004. 【必須】【半角数字】プロダクト種類 [beluga_prd_type]
		$cols [$idx ++] = $cv->convertEnNum ( $this->productInfoForBeluga[$row['product_offering']][self::PMS_OPTION_KEY_BELUGA_PRODUCT_TYPE], $idx );
		// 005. 【必須】【半角数字】サービス種類 [beluga_srv_type]
		$cols [$idx ++] = $cv->convertEnNum ( $this->productInfoForBeluga[$row['product_offering']][self::PMS_OPTION_KEY_BELUGA_SERVICE_TYPE], $idx );
		// 006. 【不要】【半角】ビリングＩＤ (※)
		$cols [$idx ++] = '';
		// 007. 【必須】【半角数字】利用開始日 [billing_start_date]
		$cols [$idx ++] = $cv->convertEnNum ( $row ['billing_start_date'], $idx );
		// 008. 【不要】【半角数字】利用廃止日 [billing_end_date]
		$cols [$idx ++] = '';
		// 009. 【必須】【半角数字】顧客区分 (※02を設定)
		$cols [$idx ++] = self::CUSTOMER_TYPE_CORPORATE;
		// 010. 【任意】【半角英数字】共通顧客ＩＤ [common_customer_id_c]
		$cols [$idx ++] = $cv->convertEnAlphanum ( $row ['common_customer_id_c'], $idx );
		// 011. 【任意】【半角英数字】内部顧客識別ＩＤ (※)
		$cols [$idx ++] = '';
		// 012. 【必須】【半角数字】支払区分 [payment_type]
		$cols [$idx ++] = $cv->convertEnNum ( $row ['payment_type'], $idx );

		if ($payType == self::PAYMENT_TYPE_ACNT_TRANSFER) {
			// 013. 【条件付※1】【口座名義文字】口座振替口座名義 [at_name]
			$cols [$idx ++] = $cv->convertBankAcntName ( $row ['at_name'], $idx );
			// 014. 【条件付※1】【半角英数字】口座振替銀行コード [at_bank_code]
			$cols [$idx ++] = $cv->convertEnAlphanum ( $row ['at_bank_code'], $idx );
			// 015. 【条件付※1】【半角英数字】口座振替銀行支店コード [at_branch_code]
			$cols [$idx ++] = $cv->convertEnAlphanum ( $row ['at_branch_code'], $idx );
			// 016. 【条件付※1】【半角数字】口座振替預金種別 [at_deposit_type]
			$cols [$idx ++] = $cv->convertEnNum ( $row ['at_deposit_type'], $idx );
			// 017. 【条件付※1】【半角数字】口座振替口座番号 [at_account_no]
			$cols [$idx ++] = $cv->convertEnNum ( $row ['at_account_no'], $idx );
			// 018. 【条件付※1】【半角数字】口座振替口座番号表示有無 [at_account_no_display]
			$cols [$idx ++] = $cv->convertEnNum ( $row ['at_account_no_display'], $idx );
		} else {
			$cols = $this->addEmptyCols ( $cols, $idx, 6 );
		}

		if ($payType == self::PAYMENT_TYPE_POSTAL_TRANSFER) {
			// 019. 【条件付※2】【口座名義文字】郵便振替口座名義 [pt_name]
			$cols [$idx ++] = $cv->convertBankAcntName ( $row ['pt_name'], $idx );
			// 020. 【条件付※2】【半角数字】郵便振替通帳記号 [pt_postal_passbook_mark]
			$cols [$idx ++] = $cv->convertEnNum ( $row ['pt_postal_passbook_mark'], $idx );
			// 021. 【条件付※2】【半角数字】郵便振替通帳番号 [pt_postal_passbook]
			$cols [$idx ++] = $cv->convertEnNum ( $row ['pt_postal_passbook'], $idx );
			// 022. 【条件付※2】【半角数字】郵便振替口座番号表示有無 [pt_postal_passbook_no_display]
			$cols [$idx ++] = $cv->convertEnNum ( $row ['pt_postal_passbook_no_display'], $idx );
		} else {
			$cols = $this->addEmptyCols ( $cols, $idx, 4 );
		}

		if ($payType == self::PAYMENT_TYPE_CREDIT_CARD) {
			// 023. 【条件付※3】【半角数字】クレジットカード番号 [credit_card_no]
			$cols [$idx ++] = $cv->convertEnNum ( $row ['credit_card_no'], $idx );
			// 024. 【条件付※3】【半角数字】クレジットカード有効期限 [expiration_date]
			$cols [$idx ++] = $cv->convertEnNum ( $row ['expiration_date'], $idx );
		} else {
			$cols = $this->addEmptyCols ( $cols, $idx, 2 );
		}

		if ($payType == self::PAYMENT_TYPE_DIRECT_DEPOSIT) {
			// 025. 【条件付※4】【半角英数字】銀行コード [dd_bank_code]
			$cols [$idx ++] = $cv->convertEnAlphanum ( $row ['dd_bank_code'], $idx );
			// 026. 【条件付※4】【半角英数字】銀行支店コード [dd_branch_code]
			$cols [$idx ++] = $cv->convertEnAlphanum ( $row ['dd_branch_code'], $idx );
			// 027. 【条件付※4】【半角数字】預金種別 [dd_deposit_type]
			$cols [$idx ++] = $cv->convertEnNum ( $row ['dd_deposit_type'], $idx );
			// 028. 【条件付※4】【半角数字】口座番号 [dd_account_no]
			$cols [$idx ++] = $cv->convertEnNum ( $row ['dd_account_no'], $idx );
		} else {
			$cols = $this->addEmptyCols ( $cols, $idx, 4 );
		}

		// 029. 【任意】【全角文字】振込担当者名 [dd_payee_contact_person_name_2]
		$cols [$idx ++] = $cv->convertEm ( $row ['dd_payee_contact_person_name_2'], $idx );
		// 030. 【任意】【銀行ＶＡＮ英数字】振込連絡番号 [dd_payee_tel_no]
		$cols [$idx ++] = $cv->convertBankVan ( $row ['dd_payee_tel_no'], $idx );
		// 031. 【任意】【半角※メアド】メールアドレス [dd_payee_email_address]
		$cols [$idx ++] = $cv->convertEn ( $row ['dd_payee_email_address'], $idx );
		// 032. 【任意】【全角文字】備考 [dd_remarks]
		$cols [$idx ++] = $cv->convertEm ( $row ['dd_remarks'], $idx );
		// 033. 【任意】【半角数字】型紙識別 (※)
		$cols [$idx ++] = '';
		// 034. 【任意】【半角数字】型紙契約ＩＤ (※)
		$cols [$idx ++] = '';
		// 035. 【任意】【半角英数字】型紙管理コード (※)
		$cols [$idx ++] = '';
		// 036. 【必須】【半角数字】契約者住所入力方法 (※"2" (宛名２段)を設定)
		$cols [$idx ++] = self::ADDR_INPUT_TYPE_DIRECT_2LINES;
		// 037. 【必須】【全角カタカナ】契約者カナ [contract_name_kana]
		$cols [$idx ++] = $cv->convertEmKatakana ( $row ['contract_name_kana'], $idx );
		// 038. 【必須】【全角文字】契約者名 [contract_name_knj]
		$cols [$idx ++] = $cv->convertEm ( $row ['contract_name_knj'], $idx );
		// 039. 【必須】【半角数字】契約者郵便番号 [contract_postal_no]
		$cols [$idx ++] = $cv->convertEnNum ( $row ['contract_postal_no'], $idx );
		// 040. 【条件付※9】【半角英数字】契約者住所コード (※)
		$cols [$idx ++] = '';

		// 契約者住所情報
		$addrs = $this->convertContractAddr ( $cv, $row ['contract_addr'], $idx );
		// 041. 【条件付※10】【全角文字】契約者住所 [contract_addr]
		$cols [$idx ++] = $addrs [0];
		// 042. 【任意】【全角文字】契約者番地等 (※同上)
		$cols [$idx ++] = $addrs [1];
		// 043. 【任意】【全角文字】契約者建物等 (※同上)
		$cols [$idx ++] = $addrs [2];
		// 044. 【任意】【全角文字】契約者様方等 (※同上)
		$cols [$idx ++] = $addrs [3];

		// 045. 【任意】【全角文字】契約者部課名 [contract_contact_division]
		$cols [$idx ++] = $cv->convertEm ( $row ['contract_contact_division'], $idx );
		// 046. 【任意】【全角文字】契約者担当者名 [contract_contact_name]
		$cols [$idx ++] = $cv->convertEm ( $row ['contract_contact_name'], $idx );
		// 047. 【任意】【銀行ＶＡＮ英数字】契約者電話番号 [contract_contact_tel_no]
		$cols [$idx ++] = $cv->convertBankVan ( $row ['contract_contact_tel_no'], $idx );
		// 048. 【任意】【銀行ＶＡＮ英数字】契約者内線番号 [contract_contact_ext_no]
		$cols [$idx ++] = $cv->convertBankVan ( $row ['contract_contact_ext_no'], $idx );
		// 049. 【任意】【半角※メアド】契約者メールアドレス [contract_email]
		$cols [$idx ++] = $cv->convertEn ( $row ['contract_email'], $idx );
		// 050. 【任意】【銀行ＶＡＮ英数字】契約者ＦＡＸ番号 [contract_phone_fax_no]
		$cols [$idx ++] = $cv->convertBankVan ( $row ['contract_phone_fax_no'], $idx );
		// 051. 【必須】【半角数字】送付先住所入力方法 (※"2" (直接住所入力 (宛名２段))を設定)
		$cols [$idx ++] = self::ADDR_INPUT_TYPE_DIRECT_2LINES;
		// 052. 【任意】【全角カタカナ】送付先カナ [billing_name_kana]
		$cols [$idx ++] = $cv->convertEmKatakana ( $row ['billing_name_kana'], $idx );
		// 053. 【必須】【全角文字】送付先名 [billing_name_knj]
		$cols [$idx ++] = $cv->convertEm ( $row ['billing_name_knj'], $idx );
		// 054. 【必須】【半角数字】送付先郵便番号 [billing_poistal_no]
		$cols [$idx ++] = $cv->convertEnNum ( $row ['billing_poistal_no'], $idx );

		// 055. 【条件付※12】【半角英数字】送付先住所コード (※)
		$cols [$idx ++] = '';

		$addrs = $this->convertBillingAddr ( $cv, $row ['billing_addr_1'], $row ['billing_addr_2'], $idx );
		// 056. 【条件付※13】【全角文字】送付先住所 [billing_addr_1]
		$cols [$idx ++] = $addrs [0];
		// 057. 【任意】【全角文字】送付先番地等 [billing_addr_2]
		$cols [$idx ++] = $addrs [1];
		// 058. 【任意】【全角文字】送付先建物等 (※同上)
		$cols [$idx ++] = $addrs [2];
		// 059. 【任意】【全角文字】送付先様方等 (※同上)
		$cols [$idx ++] = $addrs [3];

		// 060. 【任意】【全角文字】送付先部課名 [billing_contact_division]
		$cols [$idx ++] = $cv->convertEm ( $row ['billing_contact_division'], $idx );
		// 061. 【任意】【全角文字】送付先担当者名 [billing_contact_name]
		$cols [$idx ++] = $cv->convertEm ( $row ['billing_contact_name'], $idx );
		// 062. 【任意】【銀行ＶＡＮ英数字】送付先電話番号 [billing_contact_tel_no]
		$cols [$idx ++] = $cv->convertBankVan ( $row ['billing_contact_tel_no'], $idx );
		// 063. 【任意】【銀行ＶＡＮ英数字】送付先内線番号 [billing_contact_ext_no]
		$cols [$idx ++] = $cv->convertBankVan ( $row ['billing_contact_ext_no'], $idx );
		// 064. 【任意】【半角】送付先メールアドレス [billing_email]
		$cols [$idx ++] = $cv->convertEn ( $row ['billing_email'], $idx );
		// 065. 【任意】【銀行ＶＡＮ英数字】送付先ＦＡＸ番号 [billing_phone_fax_no]
		$cols [$idx ++] = $cv->convertBankVan ( $row ['billing_phone_fax_no'], $idx );
		// 066. 【任意】【半角英数字】販売チャネルコード [sales_channel_code]
		$cols [$idx ++] = $cv->convertEnAlphanum ( $row ['sales_channel_code'], $idx );
		// 067. 【任意】【全角文字】販売担当者名 [sales_rep_name]
		$cols [$idx ++] = $cv->convertEm ( $row ['sales_rep_name'], $idx );
		// 068. 【任意】【全角文字】販売担当者カナ名 (※)
		$cols [$idx ++] = '';
		// 069. 【任意】【全角文字】販売担当者所属部課名 [sales_rep_division]
		$cols [$idx ++] = $cv->convertEm ( $row ['sales_rep_division'], $idx );
		// 070. 【任意】【銀行ＶＡＮ英数字】販売担当者電話番号 [sales_rep_tel_no]
		$cols [$idx ++] = $cv->convertBankVan ( $row ['sales_rep_tel_no'], $idx );
		// 071. 【任意】【銀行ＶＡＮ英数字】販売担当者内線番号 [sales_rep_ext_no]
		$cols [$idx ++] = $cv->convertBankVan ( $row ['sales_rep_ext_no'], $idx );
		// 072. 【任意】【銀行ＶＡＮ英数字】販売担当者ＦＡＸ番号 [sales_rep_fax_no]
		$cols [$idx ++] = $cv->convertBankVan ( $row ['sales_rep_fax_no'], $idx );
		// 073. 【任意】【半角】販売担当者メールアドレス [sales_email]
		$cols [$idx ++] = $cv->convertEn ( $row ['sales_email'], $idx );
		// 074. 【任意】【全角文字】記事欄 (※)
		$cols [$idx ++] = '';

		return $cols;
	}
	private function convertRowForMod($row, $lineNo, $productInfoForBeluga) {
		// columns
		$cols = array ();
		// colum index
		$idx = 0;
		// gc_line_item_contract_history.id
		$history_id = $row ['history_id'];
		// gc_contracts.id
		$contract_id = $row ['contract_id'];
		// global_contract_id
		$global_contract_id = $row['global_contract_id'];
		// payment type
		$payTmype = $row ['payment_type'];

		$cv = new BelugaCsvConverter ( $history_id, $contract_id, $global_contract_id, $lineNo );

		// 001. 【必須】【】ファイル内通番 (※通番を設定)
		$cols [$idx ++] = $lineNo;
		// 002. 【必須】【半角数字】登録区分 [regist_type]
		$cols [$idx ++] = $cv->convertEnNum ( $row ['regist_type'], $idx );
		// 003. 【必須】【半角数字】契約識別番号 [global_contract_id]
		$cols [$idx ++] = $cv->convertEnNum ( $row ['global_contract_id'], $idx );
		// 004. 【必須】【半角数字】プロダクト種類 [beluga_prd_type]
		$cols [$idx ++] = $cv->convertEnNum ( $this->productInfoForBeluga[$row['product_offering']][self::PMS_OPTION_KEY_BELUGA_PRODUCT_TYPE], $idx );
		// 005. 【不要】【半角数字】サービス種類 [サービス種類]
		$cols [$idx ++] = '';
		// 006. 【不要】【半角】ビリングＩＤ (※)
		$cols [$idx ++] = '';
		// 007. 【任意】【半角数字】利用開始日 [billing_start_date]
		$cols [$idx ++] = '';
		// 008. 【不要】【半角数字】利用廃止日 [billing_end_date]
		$cols [$idx ++] = '';
		// 009. 【不要】【半角数字】顧客区分 (※02を設定)
		$cols [$idx ++] = '';
		// 010. 【不要】【半角英数字】共通顧客ＩＤ [common_customer_id_c]
		$cols [$idx ++] = '';
		// 011. 【不要】【半角英数字】内部顧客識別ＩＤ (※)
		$cols [$idx ++] = '';
		// 012. 【不要】【半角数字】支払区分 [payment_type]
		$cols [$idx ++] = '';
		// 013. 【不要】【口座名義文字】口座振替口座名義 [at_name]
		$cols [$idx ++] = '';
		// 014. 【不要】【半角英数字】口座振替銀行コード [at_bank_code]
		$cols [$idx ++] = '';
		// 015. 【不要】【半角英数字】口座振替銀行支店コード [at_branch_code]
		$cols [$idx ++] = '';
		// 016. 【不要】【半角数字】口座振替預金種別 [at_deposit_type]
		$cols [$idx ++] = '';
		// 017. 【不要】【半角数字】口座振替口座番号 [at_account_no]
		$cols [$idx ++] = '';
		// 018. 【不要】【半角数字】口座振替口座番号表示有無 [at_account_no_display]
		$cols [$idx ++] = '';
		// 019. 【不要】【口座名義文字】郵便振替口座名義 [pt_name]
		$cols [$idx ++] = '';
		// 020. 【不要】【半角数字】郵便振替通帳記号 [pt_postal_passbook_mark]
		$cols [$idx ++] = '';
		// 021. 【不要】【半角数字】郵便振替通帳番号 [pt_postal_passbook]
		$cols [$idx ++] = '';
		// 022. 【不要】【半角数字】郵便振替口座番号表示有無 [pt_postal_passbook_no_display]
		$cols [$idx ++] = '';
		// 023. 【不要】【半角数字】クレジットカード番号 [credit_card_no]
		$cols [$idx ++] = '';
		// 024. 【不要】【半角数字】クレジットカード有効期限 [expiration_date]
		$cols [$idx ++] = '';
		// 025. 【不要】【半角英数字】銀行コード [dd_bank_code]
		$cols [$idx ++] = '';
		// 026. 【不要】【半角英数字】銀行支店コード [dd_branch_code]
		$cols [$idx ++] = '';
		// 027. 【不要】【半角数字】預金種別 [dd_deposit_type]
		$cols [$idx ++] = '';
		// 028. 【不要】【半角数字】口座番号 [dd_account_no]
		$cols [$idx ++] = '';
		// 029. 【不要】【全角文字】振込担当者名 [dd_payee_contact_person_name_1]
		$cols [$idx ++] = '';
		// 030. 【不要】【銀行ＶＡＮ英数字】振込連絡番号 [dd_payee_contact_person_name_2]
		$cols [$idx ++] = '';
		// 031. 【不要】【半角※メアド】メールアドレス [dd_payee_email_address]
		$cols [$idx ++] = '';
		// 032. 【不要】【全角文字】備考 [dd_remarks]
		$cols [$idx ++] = '';
		// 033. 【条件付※5】【半角数字】型紙識別 (※)
		$cols [$idx ++] = '';
		// 034. 【条件付※5】【半角数字】型紙契約ＩＤ (※)
		$cols [$idx ++] = '';
		// 035. 【条件付※5】【半角英数字】型紙管理コード (※)
		$cols [$idx ++] = '';
		// 036. 【条件付※6】【半角数字】契約者住所入力方法 (※"2" (宛名２段)を設定)
		$cols [$idx ++] = self::ADDR_INPUT_TYPE_DIRECT_2LINES;
		// 037. 【条件付※7】【全角カタカナ】契約者カナ [contract_name_kana]
		$cols [$idx ++] = $cv->convertEmKatakana ( $row ['contract_name_kana'], $idx );
		// 038. 【条件付※7】【全角文字】契約者名 [contract_name_knj]
		$cols [$idx ++] = $cv->convertEm ( $row ['contract_name_knj'], $idx );
		// 039. 【条件付※8】【半角数字】契約者郵便番号 [contract_postal_no]
		$cols [$idx ++] = $cv->convertEnNum ( $row ['contract_postal_no'], $idx );
		// 040. 【条件付※9】【半角英数字】契約者住所コード (※)
		$cols [$idx ++] = '';

		// 契約者住所情報
		$addrs = $this->convertContractAddr ( $cv, $row ['contract_addr'], $idx );
		// 041. 【条件付※10】【全角文字】契約者住所 [contract_addr]
		$cols [$idx ++] = $addrs [0];
		// 042. 【条件付※11】【全角文字】契約者番地等 (※同上)
		$cols [$idx ++] = $addrs [1];
		// 043. 【条件付※11】【全角文字】契約者建物等 (※同上)
		$cols [$idx ++] = $addrs [2];
		// 044. 【条件付※11】【全角文字】契約者様方等 (※同上)
		$cols [$idx ++] = $addrs [3];

		// 045. 【条件付※11】【全角文字】契約者部課名 [contract_contact_division]
		$cols [$idx ++] = $cv->convertEm ( $row ['contract_contact_division'], $idx );
		// 046. 【条件付※11】【全角文字】契約者担当者名 [contract_contact_name]
		$cols [$idx ++] = $cv->convertEm ( $row ['contract_contact_name'], $idx );
		// 047. 【条件付※11】【銀行ＶＡＮ英数字】契約者電話番号 [contract_contact_tel_no]
		$cols [$idx ++] = $cv->convertBankVan ( $row ['contract_contact_tel_no'], $idx );
		// 048. 【条件付※11】【銀行ＶＡＮ英数字】契約者内線番号 [contract_contact_ext_no]
		$cols [$idx ++] = $cv->convertBankVan ( $row ['contract_contact_ext_no'], $idx );
		// 049. 【条件付※11】【半角※メアド】契約者メールアドレス [contract_email]
		$cols [$idx ++] = $cv->convertEn ( $row ['contract_email'], $idx );
		// 050. 【条件付※11】【銀行ＶＡＮ英数字】契約者ＦＡＸ番号 [contract_phone_fax_no]

		$cols [$idx ++] = $cv->convertBankVan ( $row ['contract_phone_fax_no'], $idx );
		// 051. 【不要】【半角数字】送付先住所入力方法 (※"2" (直接住所入力 (宛名２段))を設定)
		$cols [$idx ++] = '';
		// 052. 【不要】【全角カタカナ】送付先カナ [billing_name_kana]
		$cols [$idx ++] = '';
		// 053. 【不要】【全角文字】送付先名 [billing_name_knj]
		$cols [$idx ++] = '';
		// 054. 【不要】【半角数字】送付先郵便番号 [billing_poistal_no]
		$cols [$idx ++] = '';
		// 055. 【不要】【半角英数字】送付先住所コード (※)
		$cols [$idx ++] = '';
		// 056. 【不要】【全角文字】送付先住所 [billing_addr_1]
		$cols [$idx ++] = '';
		// 057. 【不要】【全角文字】送付先番地等 [billing_addr_2]
		$cols [$idx ++] = '';
		// 058. 【不要】【全角文字】送付先建物等 (※同上)
		$cols [$idx ++] = '';
		// 059. 【不要】【全角文字】送付先様方等 (※同上)
		$cols [$idx ++] = '';
		// 060. 【不要】【全角文字】送付先部課名 [billing_contact_division]
		$cols [$idx ++] = '';
		// 061. 【不要】【全角文字】送付先担当者名 [billing_contact_name]
		$cols [$idx ++] = '';
		// 062. 【不要】【銀行ＶＡＮ英数字】送付先電話番号 [billing_contact_tel_no]
		$cols [$idx ++] = '';
		// 063. 【不要】【銀行ＶＡＮ英数字】送付先内線番号 [billing_contact_ext_no]
		$cols [$idx ++] = '';
		// 064. 【不要】【半角】送付先メールアドレス [billing_email]
		$cols [$idx ++] = '';
		// 065. 【不要】【銀行ＶＡＮ英数字】送付先ＦＡＸ番号 [billing_phone_fax_no]
		$cols [$idx ++] = '';
		// 066. 【不要】【半角英数字】販売チャネルコード [sales_channel_code]
		$cols [$idx ++] = '';
		// 067. 【不要】【全角文字】販売担当者名 [sales_rep_name]
		$cols [$idx ++] = '';
		// 068. 【不要】【全角文字】販売担当者カナ名 (※)
		$cols [$idx ++] = '';
		// 069. 【不要】【全角文字】販売担当者所属部課名 [sales_rep_division]
		$cols [$idx ++] = '';
		// 070. 【不要】【銀行ＶＡＮ英数字】販売担当者電話番号 [sales_rep_tel_no]
		$cols [$idx ++] = '';
		// 071. 【不要】【銀行ＶＡＮ英数字】販売担当者内線番号 [sales_rep_ext_no]
		$cols [$idx ++] = '';
		// 072. 【不要】【銀行ＶＡＮ英数字】販売担当者ＦＡＸ番号 [sales_rep_fax_no]
		$cols [$idx ++] = '';
		// 073. 【不要】【半角】販売担当者メールアドレス [sales_email]
		$cols [$idx ++] = '';
		// 074. 【任意】【全角文字】記事欄 (※)
		$cols [$idx ++] = '';

		return $cols;
	}
	private function convertRowForDel($row, $lineNo) {
		// columns
		$cols = array ();
		// colum index
		$idx = 0;
		// gc_line_item_contract_history.id
		$history_id = $row ['history_id'];
		// gc_contracts.id
		$contract_id = $row ['contract_id'];
		// global_contract_id
		$global_contract_id = $row['global_contract_id'];
		// payment type
		$payType = $row ['payment_type'];

		$cv = new BelugaCsvConverter ( $history_id, $contract_id, $global_contract_id, $lineNo );

		// 001. 【必須】【】ファイル内通番 (※通番を設定)
		$cols [$idx ++] = $lineNo;
		// 002. 【必須】【半角数字】登録区分 [regist_type]
		$cols [$idx ++] = $cv->convertEnNum ( $row ['regist_type'], $idx );
		// 003. 【必須】【半角数字】契約識別番号 [global_contract_id]
		$cols [$idx ++] = $cv->convertEnNum ( $row ['global_contract_id'], $idx );
		// 004. 【必須】【半角数字】プロダクト種類 [beluga_prd_type]
		$cols [$idx ++] = $cv->convertEnNum ( $this->productInfoForBeluga[$row['product_offering']][self::PMS_OPTION_KEY_BELUGA_PRODUCT_TYPE], $idx );
		// 005. 【不要】【半角数字】サービス種類 [サービス種類]
		$cols [$idx ++] = '';
		// 006. 【不要】【半角】ビリングＩＤ (※)
		$cols [$idx ++] = '';
		// 007. 【不要】【半角数字】利用開始日 [billing_start_date]
		$cols [$idx ++] = '';
		// 008. 【必須】【半角数字】利用廃止日 [billing_end_date]
		$billingEndDate = $row ['billing_end_date'];
		$objDate = DateTime::createFromFormat('Ymd', $billingEndDate);
		if($objDate) {
			$billingEndDate = $objDate->add(new DateInterval('P1D'))->format('Ymd');  // 'P1D'は1日加算の意味
		}
		$cols [$idx ++] = $cv->convertEnNum ( $billingEndDate, $idx );
		// 009. 【不要】【半角数字】顧客区分 (※02を設定)
		$cols [$idx ++] = '';
		// 010. 【不要】【半角英数字】共通顧客ＩＤ [common_customer_id_c]
		$cols [$idx ++] = '';
		// 011. 【不要】【半角英数字】内部顧客識別ＩＤ (※)
		$cols [$idx ++] = '';
		// 012. 【不要】【半角数字】支払区分 [payment_type]
		$cols [$idx ++] = '';
		// 013. 【不要】【口座名義文字】口座振替口座名義 [at_name]
		$cols [$idx ++] = '';
		// 014. 【不要】【半角英数字】口座振替銀行コード [at_bank_code]
		$cols [$idx ++] = '';
		// 015. 【不要】【半角英数字】口座振替銀行支店コード [at_branch_code]
		$cols [$idx ++] = '';
		// 016. 【不要】【半角数字】口座振替預金種別 [at_deposit_type]
		$cols [$idx ++] = '';
		// 017. 【不要】【半角数字】口座振替口座番号 [at_account_no]
		$cols [$idx ++] = '';
		// 018. 【不要】【半角数字】口座振替口座番号表示有無 [at_account_no_display]
		$cols [$idx ++] = '';
		// 019. 【不要】【口座名義文字】郵便振替口座名義 [pt_name]
		$cols [$idx ++] = '';
		// 020. 【不要】【半角数字】郵便振替通帳記号 [pt_postal_passbook_mark]
		$cols [$idx ++] = '';
		// 021. 【不要】【半角数字】郵便振替通帳番号 [pt_postal_passbook]
		$cols [$idx ++] = '';
		// 022. 【不要】【半角数字】郵便振替口座番号表示有無 [pt_postal_passbook_no_display]
		$cols [$idx ++] = '';
		// 023. 【不要】【半角数字】クレジットカード番号 [credit_card_no]
		$cols [$idx ++] = '';
		// 024. 【不要】【半角数字】クレジットカード有効期限 [expiration_date]
		$cols [$idx ++] = '';
		// 025. 【不要】【半角英数字】銀行コード [dd_bank_code]
		$cols [$idx ++] = '';
		// 026. 【不要】【半角英数字】銀行支店コード [dd_branch_code]
		$cols [$idx ++] = '';
		// 027. 【不要】【半角数字】預金種別 [dd_deposit_type]
		$cols [$idx ++] = '';
		// 028. 【不要】【半角数字】口座番号 [dd_account_no]
		$cols [$idx ++] = '';
		// 029. 【不要】【全角文字】振込担当者名 [dd_payee_contact_person_name_1]
		$cols [$idx ++] = '';
		// 030. 【不要】【銀行ＶＡＮ英数字】振込連絡番号 [dd_payee_contact_person_name_2]
		$cols [$idx ++] = '';
		// 031. 【不要】【半角※メアド】メールアドレス [dd_payee_email_address]
		$cols [$idx ++] = '';
		// 032. 【不要】【全角文字】備考 [dd_remarks]
		$cols [$idx ++] = '';
		// 033. 【不要】【半角数字】型紙識別 (※)
		$cols [$idx ++] = '';
		// 034. 【不要】【半角数字】型紙契約ＩＤ (※)
		$cols [$idx ++] = '';
		// 035. 【不要】【半角英数字】型紙管理コード (※)
		$cols [$idx ++] = '';
		// 036. 【不要】【半角数字】契約者住所入力方法 (※"2" (宛名２段)を設定)
		$cols [$idx ++] = '';
		// 037. 【不要】【全角カタカナ】契約者カナ [contract_name_kana]
		$cols [$idx ++] = '';
		// 038. 【不要】【全角文字】契約者名 [contract_name_knj]
		$cols [$idx ++] = '';
		// 039. 【不要】【半角数字】契約者郵便番号 [contract_postal_no]
		$cols [$idx ++] = '';
		// 040. 【不要】【半角英数字】契約者住所コード (※)
		$cols [$idx ++] = '';
		// 041. 【不要】【全角文字】契約者住所 [contract_addr]
		$cols [$idx ++] = '';
		// 042. 【不要】【全角文字】契約者番地等 (※同上)
		$cols [$idx ++] = '';
		// 043. 【不要】【全角文字】契約者建物等 (※同上)
		$cols [$idx ++] = '';
		// 044. 【不要】【全角文字】契約者様方等 (※同上)
		$cols [$idx ++] = '';
		// 045. 【不要】【全角文字】契約者部課名 [contract_contact_division]
		$cols [$idx ++] = '';
		// 046. 【不要】【全角文字】契約者担当者名 [contract_contact_name]
		$cols [$idx ++] = '';
		// 047. 【不要】【銀行ＶＡＮ英数字】契約者電話番号 [contract_contact_tel_no]
		$cols [$idx ++] = '';
		// 048. 【不要】【銀行ＶＡＮ英数字】契約者内線番号 [contract_contact_ext_no]
		$cols [$idx ++] = '';
		// 049. 【不要】【半角※メアド】契約者メールアドレス [contract_email]
		$cols [$idx ++] = '';
		// 050. 【不要】【銀行ＶＡＮ英数字】契約者ＦＡＸ番号 [contract_phone_fax_no]
		$cols [$idx ++] = '';
		// 051. 【不要】【半角数字】送付先住所入力方法 (※"2" (直接住所入力 (宛名２段))を設定)
		$cols [$idx ++] = '';
		// 052. 【不要】【全角カタカナ】送付先カナ [billing_name_kana]
		$cols [$idx ++] = '';
		// 053. 【不要】【全角文字】送付先名 [billing_name_knj]
		$cols [$idx ++] = '';
		// 054. 【不要】【半角数字】送付先郵便番号 [billing_poistal_no]
		$cols [$idx ++] = '';
		// 055. 【不要】【半角英数字】送付先住所コード (※)
		$cols [$idx ++] = '';
		// 056. 【不要】【全角文字】送付先住所 [billing_addr_1]
		$cols [$idx ++] = '';
		// 057. 【不要】【全角文字】送付先番地等 [billing_addr_2]
		$cols [$idx ++] = '';
		// 058. 【不要】【全角文字】送付先建物等 (※同上)
		$cols [$idx ++] = '';
		// 059. 【不要】【全角文字】送付先様方等 (※同上)
		$cols [$idx ++] = '';
		// 060. 【不要】【全角文字】送付先部課名 [billing_contact_division]
		$cols [$idx ++] = '';
		// 061. 【不要】【全角文字】送付先担当者名 [billing_contact_name]
		$cols [$idx ++] = '';
		// 062. 【不要】【銀行ＶＡＮ英数字】送付先電話番号 [billing_contact_tel_no]
		$cols [$idx ++] = '';
		// 063. 【不要】【銀行ＶＡＮ英数字】送付先内線番号 [billing_contact_ext_no]
		$cols [$idx ++] = '';
		// 064. 【不要】【半角】送付先メールアドレス [billing_email]
		$cols [$idx ++] = '';
		// 065. 【不要】【銀行ＶＡＮ英数字】送付先ＦＡＸ番号 [billing_phone_fax_no]
		$cols [$idx ++] = '';
		// 066. 【不要】【半角英数字】販売チャネルコード [sales_channel_code]
		$cols [$idx ++] = '';
		// 067. 【不要】【全角文字】販売担当者名 [sales_rep_name]
		$cols [$idx ++] = '';
		// 068. 【不要】【全角文字】販売担当者カナ名 (※)
		$cols [$idx ++] = '';
		// 069. 【不要】【全角文字】販売担当者所属部課名 [sales_rep_division]
		$cols [$idx ++] = '';
		// 070. 【不要】【銀行ＶＡＮ英数字】販売担当者電話番号 [sales_rep_tel_no]
		$cols [$idx ++] = '';
		// 071. 【不要】【銀行ＶＡＮ英数字】販売担当者内線番号 [sales_rep_ext_no]
		$cols [$idx ++] = '';
		// 072. 【不要】【銀行ＶＡＮ英数字】販売担当者ＦＡＸ番号 [sales_rep_fax_no]
		$cols [$idx ++] = '';
		// 073. 【不要】【半角】販売担当者メールアドレス [sales_email]
		$cols [$idx ++] = '';
		// 074. 【任意】【全角文字】記事欄 (※)
		$cols [$idx ++] = '';
		return $cols;
	}

	/** split contract address string.
	 *
	 * @param BelugaCsvConverter $cv
	 * @param $param address string
	 * @param $idx columns number
	 * @return address array */
	function convertContractAddr(BelugaCsvConverter $cv, $param, $idx) {
		$lim_addr1 = 46;
		$lim_addr2 = 20;
		$lim_addr3 = 20;
		$lim_addr4 = 20;

		$addrs = array (
				'',
				'',
				'',
				''
		);
		// truncate addr str to specified length
		$addr = mb_substr ( $param, 0, ($lim_addr1 + $lim_addr2 + $lim_addr3 + $lim_addr4) );
		// log if changed
		$cv->logIfChanged ( $idx, $param, $addr );
		// convet em
		$converted = $cv->convertEm ( $addr, $idx );

		$len = mb_strlen ( $addr );

		$addr1 = mb_substr ( $addr, 0, $lim_addr1 );
		$addrs [0] = $cv->convertEm ( $addr1, $idx );
		if ($len > $lim_addr1) {
			$addr2 = mb_substr ( $addr, $lim_addr1, $lim_addr2 );
			$addrs [1] = $cv->convertEm ( $addr2, $idx );
		}
		if ($len > $lim_addr1 + $lim_addr2) {
			$addr3 = mb_substr ( $addr, $lim_addr1 + $lim_addr2, $lim_addr3 );
			$addrs [2] = $cv->convertEm ( $addr3, $idx );
		}
		if ($len > $lim_addr1 + $lim_addr2 + $lim_addr3) {
			$addr4 = mb_substr ( $addr, $lim_addr1 + $lim_addr2 + $lim_addr3, $lim_addr4 );
			$addrs [3] = $cv->convertEm ( $addr4, $idx );
		}
		return $addrs;
	}
	/** split billing address string
	 *
	 * @param BelugaCsvConverter $cv
	 * @param $addr_main
	 * @param $addr_sub
	 * @param $idx */
	function convertBillingAddr(BelugaCsvConverter $cv, $addr_main, $addr_sub, $idx) {
		$lim_addr_main = 40;
		$lim_addr_sub = 20;

		$addrs = array (
				'',
				'',
				'',
				''
		);

		// length of address1/2
		$len_addr_main = mb_strlen ( $addr_main );
		$len_addr_sub = mb_strlen ( $addr_sub );

		// If the address 1 is long, address 1 is cut and move to the address 2
		if ($len_addr_main > $lim_addr_main) {
			$extra = mb_substr ( $addr_main, $lim_addr_main );
			$addr_main = mb_substr ( $addr_main, 0, $lim_addr_main );
			$addr_sub = $extra . self::DELIM_ADDR . $addr_sub;
		}  // If the address 2 is long, move head of address 2 to tail of address 1
		else if ($len_addr_sub > $lim_addr_sub * 3) {

			$len_addr_main_free = $lim_addr_main - $len_addr_main - 1;
			if ($len_addr_main_free > 0) {
				$splitted = $this->splitByLenOrSpc ( $addr_sub, $len_addr_main_free );
				$addr_main .= self::DELIM_ADDR . $splitted [0];
				$addr_sub = mb_substr ( $addr_sub, mb_strlen ( $splitted [0] ) );
			}
		}

		// convert addr1
		$addrs [0] = $cv->convertEm ( $addr_main, $idx );

		// split addr_sub to addr2~4
		$splitted = $this->splitByLenOrSpc ( $addr_sub, $lim_addr_sub );

		// If address size is over,
		// 1. remove spaces
		// 2. simply concatenate and divide by fixed length.
		if (count ( $splitted ) > 3) {
			$splitted = $this->splitByLen($addr_sub, $lim_addr_sub);
			if ($len_addr_sub > $lim_addr_sub * 3) {
				$cv->logIfChanged ( $idx, $addr_sub, mb_substr ( $addr_sub, 0, $lim_addr_sub * 3 ) );
			}
		}

		$addrIdx = 1;
		foreach ( $splitted as $each ) {
			$addrs [$addrIdx ++] = $cv->convertEm ( $each, $idx );
		}

		return $addrs;
	}

	// ---------------- utility ----------------

	/** {$outputDir}/belugaExport_{$ts}{$suffix}'
	 *
	 * @param unknown $outputDir */
	private function filePath($outputDir, $ts, $suffix) {

		$name = "belugaExport_" . $ts . $suffix;
		$path = implode ( DIRECTORY_SEPARATOR, array (
				$outputDir,
				$name
		) );
		return $path;
	}
	private function _fputcsv($fp, $cols) {
		$line = '';
		foreach ( $cols as $col ) {
			if ($line) {
				$line .= ',';
			}
			$col = str_replace ( '"', '""', $col );
			$line .= '"' . $col . '"';
		}
		$line .= "\r\n";
		fwrite ( $fp, $line );
	}
	/** add empty element to array
	 *
	 * @param $cols target array
	 * @param $idx current index
	 * @param $times times to add */
	private function addEmptyCols($cols, &$idx, $times) {
		for($i = 0; $i < $times; $i ++) {
			$cols [$idx ++] = '';
		}
		return $cols;
	}
	/**
	 *
	 * @param $str
	 * @param $length */
	private function splitByLenOrSpc($str, $length) {

		// split by space
		$str = mb_convert_kana ( $str, 's', 'UTF-8' );
		$words = preg_split ( '/[\s]+/', $str );
		// split with length
		$splitted = array ();
		foreach ( $words as $w ) {
			$wlen = mb_strlen ( $w );
			if ($wlen > $length) {
				for($i = 0; $i < $wlen; $i += $length) {
					$splitted [] = mb_substr ( $w, $i, $length );
				}
			} else {
				$splitted [] = $w;
			}
		}

		// initialize return array
		$result = array (
				'',
				'',
				''
		);
		// connect elements if element fits into one line
		$currIdx = 0;
		$currElement = '';
		foreach ( $splitted as $each ) {
			$join = $currElement ? self::DELIM_ADDR : '';
			$nextElement = $currElement . $join . $each;
			if (mb_strlen ( $nextElement ) > $length) {
				$result [$currIdx ++] = $currElement;
				$currElement = $each;
			} else {
				$currElement = $nextElement;
			}
		}
		$result [$currIdx] = $currElement;

		return $result;
	}
	private function splitByLen($str, $length) {
		$result = array ();
		// remove space
		$str = str_replace ( array (
				" ",
				"　"
		), "", $str );
		// split with $length
		$strlen = mb_strlen ( $str );
		for($l = 0; $l < $strlen; $l += $length) {
			$result [] = mb_substr ( $str, $l, $length );
		}
		return $result;
	}
}

