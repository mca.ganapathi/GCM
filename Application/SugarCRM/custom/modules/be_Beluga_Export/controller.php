<?php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ('MSA'), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

/**
 * class be_Beluga_ExportController created for exporting
 */
class be_Beluga_ExportController extends SugarController
{
    
    /**
     * GCMExportCSV Function
     */
    function action_GCMExportCSV()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'GCMExportCSV Function');
        global $mod_strings, $sugar_config, $current_user;
        $modUtils = new ModUtils();
        $successFlag = 0;
        if (empty($successFlag)) {
            // SugarApplication::appendErrorMessage('Beluga Export Start Date : "'.$_POST['start_date']." <br/> Beluga Export End Date : " .$_POST['end_date']);
            require_once ("custom/modules/be_Beluga_Export/BelugaExport.php");
            $belugaExport = new BelugaExport();
            $outputDir = $sugar_config['beluga_export_path'];
            $start_date = date_create_from_format($current_user->getPreference('datef'), $_POST['start_date']);
            $start_date = date_format($start_date, 'Ymd');
            $end_date = date_create_from_format($current_user->getPreference('datef'), $_POST['end_date']);
            $end_date = date_format($end_date, 'Ymd');
            $filepath = $belugaExport->GCMExportCSV($start_date, $end_date, $outputDir);
            $filepath = str_replace('\\', '/', $filepath);
            // SugarApplication::appendErrorMessage(' <br/> Beluga Export CSV File is saved :'. $filepath);
            if (file_exists($filepath)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filepath));
                readfile($filepath);
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'filepath : '.$filepath, 'to check exported file path');
                unlink($filepath);
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
                exit();
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}

?>