/**
 * @desc   : dates range validation on beluga exports
 * @author : rajul.mondal
 * @since   : 15-June-2018
 */
function exportCSV() {

    var start_date = convertDateTime(document.getElementById("start_date").value);
    var end_date = convertDateTime(document.getElementById("end_date").value);

    if (date_diff_indays(start_date, end_date) < 0) {
        var msg = 'End Date should be greater than Start Date';
        add_error_style('EditView', 'end_date', msg, true);
        return false;
    }

    return check_form('EditView');
}

function processDateFormat(date) {
    date_format = SUGAR.expressions.userPrefs.datef;
    if(date_format.slice(0, 1) == 'd') {
        arr_dt = date.split("/");

        var dt = arr_dt[0];
        var mn = arr_dt[1];

        arr_dt[0] = mn;
        arr_dt[1] = dt;
        return arr_dt.join('/');
    }
    else
        return date;
}

function convertDateTime(date) {
    // 23.12.2010 | 12.23.2010 | 2010.12.23
    date = date.replace(/\./g, "\/");
    // 23-12-2010 | 12-23-2010 | 2010-12-23
    date = date.replace(/\-/g, "\/");
    
    date = processDateFormat(date);
    
    var d1 = new Date(date);
    var d2 = new Date(d1);

    //var result = (d2.getUTCDate() +'/' + (d2.getUTCMonth()+1) +'/'+ d2.getUTCFullYear() + ' ' + d2.getHours() +':' + d2.getMinutes() +':'+d2.getSeconds());
    //return result;
    return d2;
}



var date_diff_indays = function(date1, date2) {
    dt1 = new Date(date1);
    dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
}

$(document).ready(function() {
    $(document).find('.moduleTitle > h2').html('Beluga Export');
    $(document).find('title').html('Beluga Export » SugarCRM');
});