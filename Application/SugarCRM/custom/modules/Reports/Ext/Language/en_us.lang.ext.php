<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Reports/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Corporate';
$mod_strings['LBL_ACCOUNTS'] = 'Corporates';
$mod_strings['LBL_ACCOUNT'] = 'Corporate';
$mod_strings['LBL_ACCOUNT_REPORTS'] = 'Corporate Reports';
$mod_strings['LBL_MY_TEAM_ACCOUNT_REPORTS'] = 'My Team&#039;s Corporate Reports';
$mod_strings['LBL_MY_ACCOUNT_REPORTS'] = 'My Corporate Reports';
$mod_strings['LBL_PUBLISHED_ACCOUNT_REPORTS'] = 'Published Corporate Reports';
$mod_strings['DEFAULT_REPORT_TITLE_3'] = 'Partner Corporate List';
$mod_strings['DEFAULT_REPORT_TITLE_4'] = 'Customer Corporate List';
$mod_strings['DEFAULT_REPORT_TITLE_16'] = 'Corporates By Type By Industry';
$mod_strings['DEFAULT_REPORT_TITLE_43'] = 'Customer Corporate Owners';
$mod_strings['DEFAULT_REPORT_TITLE_44'] = 'My New Customer Corporates';

?>
