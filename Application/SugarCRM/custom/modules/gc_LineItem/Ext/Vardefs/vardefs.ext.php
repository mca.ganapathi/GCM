<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/gc_LineItem/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_LineItem']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/gc_LineItem/Ext/Vardefs/gc_lineitem_documents_1_gc_LineItem.php

// created: 2016-01-08 05:22:36
$dictionary["gc_LineItem"]["fields"]["gc_lineitem_documents_1"] = array (
  'name' => 'gc_lineitem_documents_1',
  'type' => 'link',
  'relationship' => 'gc_lineitem_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_GC_LINEITEM_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/gc_LineItem/Ext/Vardefs/sugarfield_products_id.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_LineItem']['fields']['products_id']['audited'] = false;
$dictionary['gc_LineItem']['fields']['products_id']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/gc_LineItem/Ext/Vardefs/sugarfield_product_quantity.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_LineItem']['fields']['product_quantity']['audited'] = true;
$dictionary['gc_LineItem']['fields']['product_quantity']['full_text_search']['boost'] = 1;


?>
