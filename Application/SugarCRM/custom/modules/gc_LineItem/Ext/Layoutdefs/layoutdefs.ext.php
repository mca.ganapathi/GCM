<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/gc_LineItem/Ext/Layoutdefs/gc_lineitem_documents_1_gc_LineItem.php

 // created: 2016-01-08 05:22:33
$layout_defs["gc_LineItem"]["subpanel_setup"]['gc_lineitem_documents_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_GC_LINEITEM_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'gc_lineitem_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/gc_LineItem/Ext/Layoutdefs/removeSubpanelButton.php

// Remove the create and select button here only view
//unset($layout_defs["gc_LineItem"]["subpanel_setup"]['gc_lineitem_documents_1']['top_buttons'][0]);
unset($layout_defs["gc_LineItem"]["subpanel_setup"]['gc_lineitem_documents_1']['top_buttons'][1]);
?>
