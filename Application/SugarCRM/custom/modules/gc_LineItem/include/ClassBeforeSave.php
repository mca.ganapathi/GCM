<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * class ClassLineItemBeforeSave
 */
class ClassLineItemBeforeSave
{

   /**
     * method setLineItemAuditEntry to Save Audit Logs under Line Item Module
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author dinesh.itkar
     * @since Jan 05, 2016
     */
    function setLineItemAuditEntry($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'to Save Audit Logs under Line Item Module');
        
        // Avoid audit log for new record.
        if (empty($bean->fetched_row['id'])) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'', '');
            return;
        }
        
        // Audit product id
        if ($bean->products_id != $bean->fetched_row['products_id']) {
            $modUtils = new ModUtils();
            $before_product_name = $modUtils->getProductNameByID($bean->fetched_row['products_id']);
            $after_product_name = $modUtils->getProductNameByID($bean->products_id);
            $this->insertLineItemAudit($bean, 'Product id', 'text', $before_product_name, $after_product_name);
        }
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'', '');
    }

     /**
     * method insertLineItemAudit to save Line Item Contract Audit record.
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param string $field_name field name to audit
     * @param string $data_type data type of field
     * @param string $before value before update
     * @param string $after value after update
     * @author dinesh.itkar
     * @since  Jan 05, 2016
     */
    private function insertLineItemAudit($bean, $field_name, $data_type, $before, $after)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'field_name : '.$field_name.GCM_GL_LOG_VAR_SEPARATOR.'data_type : '.$data_type.GCM_GL_LOG_VAR_SEPARATOR.'before : '.$before.GCM_GL_LOG_VAR_SEPARATOR.'after : '.$after, 'save Line Item Contract Audit record');
        $audit_entry = array();
        $audit_entry['field_name'] = $field_name;
        $audit_entry['data_type'] = $data_type;
        $audit_entry['before'] = $before;
        $audit_entry['after'] = $after;
        $bean->db->save_audit_records($bean, $audit_entry);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'', '');
    }
}

?>