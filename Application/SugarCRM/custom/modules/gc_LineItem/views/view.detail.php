<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

/**
 * class Gc_LineItemViewDetail
 */
class Gc_LineItemViewDetail extends ViewDetail
{

    /**
     * Method _displaySubPanels Overridden to support subpanel customizations
     */
    function _displaySubPanels()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method Overridden to support subpanel customizations');
        require_once ('custom/include/SubPanel/SubPanelTiles.php');
        $subpanel = new CustomSubPanelTiles($this->bean, $this->module);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts']['top_buttons'][0]); // hiding create
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts']['top_buttons'][1]); // hiding select
        echo $subpanel->display();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}

?>