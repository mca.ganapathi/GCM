<?php
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

require_once ('include/MVC/View/views/view.popup.php');

/**
 * class gc_lineItemViewPopup
 */
class gc_lineItemViewPopup extends ViewPopup
{

    /**
     * : Method to invoke parent viewpopup method
     * @param  :
     * @return :
     * @author : Arunsakthivel.S
     * @date : 06-05-2016
     */
    function CustomViewPopup()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method to invoke parent viewpopup method');
        parent::ViewPopup();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method display to display the processed data on the Html page & apply filter to the page
     * 
     * @author Arunsakthivel.S
     * @since May 06, 2016
     */
    function display()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'to display the processed data on the Html page & apply filter to the page');
        global $popupMeta, $mod_strings, $custom_filter_gcm;
        $contracts_id = ((!empty($_REQUEST['conId']))?$_REQUEST['conId']:'');
        $group_id = ((!empty($_REQUEST['liIgId']))?$_REQUEST['liIgId']:'');
        $js = '<script type="text/javascript">$(document).ready(function() { $(".edit").hide();  $(".formHeader").hide();   });</script>';
        echo $js;
        // $product_id = $_REQUEST['pm_products_pi_product_itempm_products_ida_advanced'];
        $custom_filter_gcm = " gc_lineitem.id in (SELECT line_item_id 
                            FROM gc_line_item_contract_history where contracts_id = '$contracts_id' AND deleted = '0' and                        gc_line_item_contract_history.id not in( 
                                    SELECT  b.gc_invoice53c9history_idb 
                                    FROM gc_invoicesubtotalgroup_gc_line_item_contract_history_1_c as b 
                                        JOIN gc_line_item_contract_history as a 
                                    WHERE a.id = b.gc_invoice53c9history_idb and a.contracts_id = '$contracts_id' AND b.deleted = 0 ))";
        
        parent::display(); // call parent display method
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}

