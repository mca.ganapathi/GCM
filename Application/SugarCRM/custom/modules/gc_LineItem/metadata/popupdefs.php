<?php
global $custom_filter_gcm;
$popupMeta = array (
    'moduleMain' => 'gc_lineItem',
    'varName' => 'gc_lineItem',
    'orderBy' => 'gc_lineItem.name',
    'whereClauses' => array (
  'name' => 'gc_lineItem.name',
),
    'searchInputs' => array (
  0 => 'gc_lineItem_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
),
);

if (isset($custom_filter_gcm) && ! empty($custom_filter_gcm)) $popupMeta['whereStatement'] = $custom_filter_gcm;
unset($custom_filter_gcm);