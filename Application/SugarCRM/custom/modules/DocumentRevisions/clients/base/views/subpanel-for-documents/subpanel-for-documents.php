<?php
// created: 2016-09-29 04:41:43
$viewdefs['DocumentRevisions']['base']['view']['subpanel-for-documents'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'default' => true,
          'label' => 'LBL_REV_LIST_FILENAME',
          'enabled' => true,
          'name' => 'filename',
          'link' => true,
          'type' => 'file',
        ),
        1 => 
        array (
          'default' => true,
          'label' => 'LBL_REV_LIST_REVISION',
          'enabled' => true,
          'name' => 'revision',
        ),
        2 => 
        array (
          'default' => true,
          'label' => 'LBL_REV_LIST_ENTERED',
          'enabled' => true,
          'name' => 'date_entered',
          'type' => 'datetime',
        ),
        3 => 
        array (
          'default' => true,
          'label' => 'LBL_REV_LIST_CREATED',
          'enabled' => true,
          'name' => 'created_by_name',
          'type' => 'relate',
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);