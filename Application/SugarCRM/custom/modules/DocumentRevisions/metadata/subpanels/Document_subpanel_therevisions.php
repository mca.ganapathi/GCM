<?php
// created: 2016-01-13 11:19:59
$subpanel_layout['list_fields'] = array (
  'filename' => 
  array (
    'vname' => 'LBL_REV_LIST_FILENAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '30%',
    'default' => true,
  ),
  'revision' => 
  array (
    'vname' => 'LBL_REV_LIST_REVISION',
    'width' => '10%',
    'default' => true,
  ),
  'date_entered' => 
  array (
    'vname' => 'LBL_REV_LIST_ENTERED',
    'width' => '25%',
    'default' => true,
  ),
  'created_by_name' => 
  array (
    'vname' => 'LBL_REV_LIST_CREATED',
    'width' => '25%',
    'default' => true,
  ),
  'del_button' => 
  array (
    'vname' => 'LBL_DELETE_BUTTON',
    'widget_class' => 'SubPanelRemoveButton',
    'width' => '10%',
    'default' => true,
  ),
  'document_id' => 
  array (
    'usage' => 'query_only',
  ),
);