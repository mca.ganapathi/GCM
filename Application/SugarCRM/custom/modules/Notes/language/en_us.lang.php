<?php
// created: 2015-09-08 12:57:43
$mod_strings = array (
  'LBL_CONTACT_NAME' => 'Account:',
  'LBL_LIST_CONTACT_NAME' => 'Account',
  'LBL_ACCOUNTS' => 'Corporates',
  'LBL_CONTACT_ID' => 'Account ID:',
  'LBL_LIST_CONTACT' => 'Account',
  'LBL_ACCOUNT_ID' => 'Corporate ID:',
);