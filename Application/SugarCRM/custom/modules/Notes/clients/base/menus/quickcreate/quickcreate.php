<?php
// created: 2016-09-29 04:41:37
$viewdefs['Notes']['base']['menu']['quickcreate'] = array (
  'layout' => 'create',
  'label' => 'LNK_NEW_NOTE',
  'visible' => false,
  'icon' => 'fa-plus',
  'related' => 
  array (
    0 => 
    array (
      'module' => 'Contacts',
      'link' => 'notes',
    ),
  ),
  'order' => 3,
);