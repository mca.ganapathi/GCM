<?php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

require_once('include/MVC/View/views/view.sidecar.php');

class HomeViewSidecar extends ViewSidecar
{
	
	public function preDisplay($params = array())
	{
		parent::preDisplay();
		global $sugar_config;
		$this->ss->assign('idf_error', "");
		$this->ss->assign('idf_user_name', "");
		$this->ss->assign('idf_password', "");
		session_start();
		if (isset ( $_REQUEST ['id_token'] ) && ! empty ( $_REQUEST ['id_token'] ) && ! empty ( $_REQUEST ['access_token'] ) && ! empty ( $_SESSION ['OP_nonce'] )) {
			if (!isset($_SESSION['OLD_token']) || $_SESSION['OLD_token'] != $_REQUEST ['id_token'] ) {
			   $this->COELogin();	
			}
		}
		$nonce = 'n-'.rand();
		$_SESSION['OP_nonce'] = $nonce;
		$redirect_url = urlencode ($sugar_config['site_url']) . "&response_type=id_token token&response_mode=form_post&nonce=" . $nonce;
        $openIdUrl = $sugar_config['openidconnect']['iss']."/as/authorization.oauth2?client_id=".$sugar_config['openidconnect']['aud']."&scope=openid email profile address phone&state=ping&redirect_uri=" . $redirect_url;
		$this->ss->assign('openIdUrl', $openIdUrl);
	}
	private function COELogin() {
		$idToken = $_REQUEST ['id_token'];
		$accessToken = $_REQUEST ['access_token'];
		$nonce = $_SESSION ['OP_nonce'];
		$issuedAt = time ();
		$validationResponse = $this->validateIDToken ( $idToken, $issuedAt, $nonce );
		$_SESSION['OLD_token'] = $idToken;
		// $curlResponse['email'] = 'Sudharsan.Ganesan@emeriocorp.com';
		if (! empty ( $validationResponse ['userEmail'] ) && is_array ( $validationResponse ) && $validationResponse ['errFlag'] == 0) {
			if (! defined ( 'SUGAR_PHPUNIT_RUNNER' )) {
				session_regenerate_id ( false );
			}
			$arrLoginUserDetails = array ();
			global $mod_strings, $db, $arrLoginUserDetails;
			$res = $GLOBALS ['sugar_config'] ['passwordsetting'];
			$login_vars = $GLOBALS ['app']->getLoginVars ( false );
			$user_name = isset ( $validationResponse ['userEmail'] ) ? trim ( $validationResponse ['userEmail'] ) : '';
			$arrLoginUserDetails ['userEmail'] = $user_name;
			if (! empty ( $user_name )) {
				$password = mt_rand ();
				$newPassword = $this->FnLoginPasswordHash ( $password );
				$updatePass = "UPDATE users SET user_hash = '" . $newPassword . "' WHERE user_name = '" . $user_name . "'";
				$db->query ( $updatePass );
				$this->ss->assign('idf_user_name', $user_name);
		        $this->ss->assign('idf_password', $password);
			}
           	
		} else {
			$_SESSION ['login_error'] = ($validationResponse ['errFlag'] == 1) ? "ID token authentication failed! Please contact Admin" : "Authentication Failed! Please contact Admin";
		    $this->ss->assign('idf_error', $_SESSION ['login_error']);
			
		}
	}
	private function FnLoginPasswordHash($password) {
		if (! defined ( 'CRYPT_MD5' ) || ! constant ( 'CRYPT_MD5' )) {
			// does not support MD5 crypt - leave as is
			if (defined ( 'CRYPT_EXT_DES' ) && constant ( 'CRYPT_EXT_DES' )) {
				return crypt ( strtolower ( md5 ( $password ) ), "_.012" . substr ( str_shuffle ( './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789' ), - 4 ) );
			}
			// plain crypt cuts password to 8 chars, which is not enough
			// fall back to old md5
			return strtolower ( md5 ( $password ) );
		}
		return crypt ( strtolower ( md5 ( $password ) ) );
	}
	private function validateIDToken($datab64, $time, $nonce) {
		$GLOBALS ['log']->info ( 'Begin: validateIDToken(' . print_r ( $datab64, true ) . ",$time,$nonce)" );
		global $sugar_config;
		$error = $arrReturn = array ();
		
		$tks = explode ( '.', $datab64 );
		list ( $headb64, $payloadb64, $cryptob64 ) = $tks;
		$signature = $this->urlsafeB64Decode ( $cryptob64 );
		$algorithm = $this->urlsafeB64Decode ( $headb64 );
		$data = $this->urlsafeB64Decode ( $payloadb64 );
		$dataArr = $this->jsonDecode ( $data );
		$algArr = $this->jsonDecode ( $algorithm );
		$payloadArr = array_merge ( $dataArr, $algArr );
		
		$key = $sugar_config ['openidconnect'] ['client_secret'];
		/* Start: Custom Time for IDF Validation */
		$custom_time = $time + 1;
		/* End: Custom Time for IDF Validation */
		if (is_array ( $payloadArr ) && ! empty ( $payloadArr )) {
			if ($nonce != trim ( $payloadArr ['nonce'] )) {
				$error [] = 'Nounce mismatch';
			}
			if ($sugar_config ['openidconnect'] ['aud'] != trim ( $payloadArr ['aud'] )) {
				$error [] = 'Audiance mismatch';
			}
			if ($sugar_config ['openidconnect'] ['iss'] != trim ( $payloadArr ['iss'] )) {
				$error [] = 'OP mismatch';
			}
			if ($sugar_config ['openidconnect'] ['alg'] != trim ( $payloadArr ['alg'] )) {
				$error [] = 'Algorithm mismatch';
			}
			/* Start: Custom Time for IDF Validation */
			if ($custom_time < $payloadArr ['iat']) {
				$error [] = 'Too Early';
			}
			/* End: Custom Time for IDF Validation */
			if ($time > $payloadArr ['exp']) {
				$error [] = 'Time Expired';
			}
			if ($signature != $this->sign ( "$headb64.$payloadb64", $key, $payloadArr ['alg'] )) {
				$error [] = 'Signature verification failed';
			}
			if (! empty ( $error )) {
				$GLOBALS ['log']->info ( 'IDToken contains Errors: ' . print_r ( $error, true ) );
				$GLOBALS ['log']->info ( 'End: validateIDToken(' . print_r ( $datab64, true ) . ",$time,$nonce,$custom_time)" );
				$arrReturn ['errFlag'] = 1;
				return $arrReturn;
			}
			if (empty ( $error )) {
				$GLOBALS ['log']->info ( 'IDToken Validated Successfully: ' );
				$GLOBALS ['log']->info ( 'End: validateIDToken(' . print_r ( $datab64, true ) . ",$time,$nonce,$custom_time)" );
				$arrReturn ['errFlag'] = 0;
				$arrReturn ['userEmail'] = $payloadArr ['email'];
				return $arrReturn;
			}
		}
		
		$error [] = 'Replay Attacks';
		$GLOBALS ['log']->info ( 'IDToken Replay Attacks' );
		$GLOBALS ['log']->info ( 'End: validateIDToken(' . print_r ( $datab64, true ) . ",$time,$nonce)" );
		$arrReturn ['errFlag'] = 1;
		unset ( $error );
		return $arrReturn;
	}
	private function urlsafeB64Decode($input) {
		$GLOBALS ['log']->info ( "Begin: urlsafeB64Decode($input)" );
		$remainder = strlen ( $input ) % 4;
		if ($remainder) {
			$padlen = 4 - $remainder;
			$input .= str_repeat ( '=', $padlen );
		}
		$GLOBALS ['log']->info ( "End: urlsafeB64Decode($input)" );
		return base64_decode ( strtr ( $input, '-_', '+/' ) );
	}
	private function jsonDecode($input) {
		$obj = json_decode ( $input, true );
		return $obj;
	}
	private function sign($msg, $key, $method = 'HS256') {
		$GLOBALS ['log']->info ( "End: sign($msg, $key, $method)" );
		$methods = array (
				'HS256' => 'sha256',
				'HS384' => 'sha384',
				'HS512' => 'sha512' 
		);
		if (empty ( $methods [$method] )) {
			$GLOBALS ['log']->info ( "End: sign($msg, $key, $method)" );
			return 0;
		}
		$GLOBALS ['log']->info ( "End: sign($msg, $key, $method)" );
		return hash_hmac ( $methods [$method], $msg, $key, true );
	}

}
