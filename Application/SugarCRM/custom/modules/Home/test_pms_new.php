<?php
/*
 *  Catalyst API 呼び出し
 */
// 固定値
define('BASE_URL',   'https://fl1-pms25bae-pub.securesites.com/ossb-pms-services');
define('PUBLIC_KEY', '68fe41a25c8c4d478d7a7b4cfbc5309b');
define('SECRET_KEY', '31ea1fa25c8c4d478d7a7b4cfbc53000');

// リクエスト情報
$timestamp = date('D, d M Y H:i:s O', time());   //echo $timestamp;
$httpVerb = 'GET';
$absolutePathRoot = '/pms/api';

//$relativeUri = '/catalog/173/offering'; // Product Offering
$relativeUri = '/catalog/173/offering/946/detail'; // Product Specification Details 
$query = '';


// Signature
$stringToSign = PUBLIC_KEY . '$' . $httpVerb . '$' . $absolutePathRoot . '$' . $relativeUri . '$' . $timestamp; //echo $stringToSign."\n";
$signature = base64_encode(hash_hmac('sha1', $stringToSign, SECRET_KEY, true));
// echo $signature;
$api_key = PUBLIC_KEY . ':' . $signature;    //echo $api_key;

// HTTP Request(Call API)
$httpClient = curl_init(BASE_URL . $absolutePathRoot . $relativeUri . (strlen($query)>0 ? '?' : '') . $query);
//curl_setopt($httpClient, CURLOPT_HEADER, true);  // If this flag is true, header information is included in response string.
curl_setopt($httpClient, CURLOPT_RETURNTRANSFER, true);
curl_setopt($httpClient, CURLOPT_HTTPHEADER,
	array('api-key: '   . $api_key,
	      'x-mp-date: ' . $timestamp

));


if (substr(PHP_OS, 0, 3) == 'WIN') {
	// Windows PHPが内包するCA情報は古いので、SSL無視のオプションを指定
	curl_setopt($httpClient, CURLOPT_SSL_VERIFYPEER, false);
}
// HTTP Proxy
// curl_setopt($httpClient, CURLOPT_PROXY, 'http://127.0.0.1');
// curl_setopt($httpClient, CURLOPT_PROXYPORT, '8000');

$responseContents = curl_exec($httpClient);
echo 'PMS Result:<br/>';
echo $responseContents;

//file_put_contents("Response_".time().".xml",$responseContents);

curl_close($httpClient);

?>