<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * class ClassPostalTransferBeforeSave
 */
class ClassPostalTransferBeforeSave
{

    /**
     * Method setLineItemAuditEntry to Save Audit Logs under Line Item Module
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author dinesh.itkar
     * @since Jan 02, 2016
     */
    function setLineItemAuditEntry($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'Method to Save Audit Logs under Line Item Module');
        
        // Avoid audit log for new record.
        if (empty($bean->fetched_row['id'])) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'', '');
            return;
        }
        
        if (!empty($bean->contract_history_id)) {
            $obj_line_Item_contract_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
            $obj_line_Item_contract_history_bean->retrieve($bean->contract_history_id);
            $obj_line_item_bean = BeanFactory::getBean('gc_LineItem');
            $obj_line_item_bean->retrieve($obj_line_Item_contract_history_bean->line_item_id);
        }
        
        if (!empty($obj_line_item_bean)) {
            $bean_name = ((!empty($bean->name))?$bean->name:'');
            $bean_fetched_name = ((!empty($bean->fetched_row['name']))?$bean->fetched_row['name']:'');            
            $bean_postal_passbook_mark = ((!empty($bean->postal_passbook_mark))?$bean->postal_passbook_mark:'');
            $bean_fetched_postal_passbook_mark = ((!empty($bean->fetched_row['postal_passbook_mark']))?$bean->fetched_row['postal_passbook_mark']:'');            
            $bean_postal_passbook = ((!empty($bean->postal_passbook))?$bean->postal_passbook:'');
            $bean_fetched_postal_passbook = ((!empty($bean->fetched_row['postal_passbook']))?$bean->fetched_row['postal_passbook']:'');            
            
            $bean_postal_passbook_no_display = ((!empty($bean->postal_passbook_no_display))?$bean->postal_passbook_no_display:'');
            $bean_fetched_postal_passbook_no_display = ((!empty($bean->fetched_row['postal_passbook_no_display']))?$bean->fetched_row['postal_passbook_no_display']:'');            
            
            // Audit Account Holder's Name
            if ($bean_name != $bean_fetched_name) {
                $this->insertPostalTransferAudit($obj_line_item_bean, 'Postal Transfer - Account Holder Name', 'text', $bean_fetched_name, $bean_name);
            }
            
            // Audit Postal Passbook Mark
            if ($bean_postal_passbook_mark != $bean_fetched_postal_passbook_mark) {
                $this->insertPostalTransferAudit($obj_line_item_bean, 'Postal Passbook Mark', 'text', $bean_fetched_postal_passbook_mark, $bean_postal_passbook_mark);
            }
            
            // Audit Postal Passbook Number
            if ($bean_postal_passbook != $bean_fetched_postal_passbook) {
                $this->insertPostalTransferAudit($obj_line_item_bean, 'Postal Passbook Number', 'text', $bean_fetched_postal_passbook, $bean_postal_passbook);
            }
            
            // Audit Passbook Number Display
            if ($bean_postal_passbook_no_display != $bean_fetched_postal_passbook_no_display) {
                global $app_list_strings;
                $postal_no_display_value_before = $app_list_strings['postal_passbook_no_display_list'][$bean_fetched_postal_passbook_no_display];
                $postal_no_display_value_after = $app_list_strings['postal_passbook_no_display_list'][$bean_postal_passbook_no_display];
                $this->insertPostalTransferAudit($obj_line_item_bean, 'Postal Passbook Number Display', 'text', $postal_no_display_value_before, $postal_no_display_value_after);
            }            
        }
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'', '');
    }

     /**
     * Method insertPostalTransferAudit to save Postal Transfer Audit record.
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param string $field_name field name to audit
     * @param string $data_type data type of field
     * @param string $before value before update
     * @param string $after value after update
     * @author dinesh.itkar
     * @since Jan 02, 2016
     */
    private function insertPostalTransferAudit($bean, $field_name, $data_type, $before, $after)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'field_name : '.$field_name.GCM_GL_LOG_VAR_SEPARATOR.'data_type : '.$data_type.GCM_GL_LOG_VAR_SEPARATOR.'before : '.$before.GCM_GL_LOG_VAR_SEPARATOR.'after : '.$after, 'to save Postal Transfer Audit record');
        $audit_entry = array();
        $audit_entry['field_name'] = $field_name;
        $audit_entry['data_type'] = $data_type;
        $audit_entry['before'] = $before;
        $audit_entry['after'] = $after;
        $bean->db->save_audit_records($bean, $audit_entry);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'', '');
    }
}

?>