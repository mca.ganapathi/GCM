/*
 * @author Sagar.Salunkhe
 * @date 17 Dec 2015
 * @desc : All EditView related JavaScript methods come under this file
 */

/*
 * @Note : All element related triggers/events are written here and not in metadata; as
 * if we include/exclude field from layout will end into losing javascript array param
 */

$(document).ready(function() {
	$('#save_and_continue').closest('table').hide(); // hide "Save and
														// Continue" button as
														// well as pagination
														// buttons as it doesnt
														// load this file
	PageLoad();
});

function PageLoad() {
	document.getElementById('SAVE_HEADER').onclick = function() {
		if (ValidateSave()) {
			var _form = document.getElementById('EditView');
			_form.action.value = 'Save';
			SUGAR.ajaxUI.submitForm(_form);
		}
		return false;
	};
	document.getElementById('SAVE_FOOTER').onclick = function() {
		if (ValidateSave()) {
			var _form = document.getElementById('EditView');
			_form.action.value = 'Save';
			SUGAR.ajaxUI.submitForm(_form);
		}
		return false;
	};
}

/**
 * @desc Function to override form save on click even and add custom validation
 * @author Sagar.Salunkhe
 * @date 18-Dec-2015
 * 
 */
function ValidateSave() {
	clear_all_errors(); // clear sugar validation msgs
	var startD = document.getElementById("product_introduction_date").value;
	var EndD = document.getElementById("product_sales_end_date").value;
	
		removeFromValidate('EditView','fm_factory_pm_products_name');
		removeFromValidate('EditView','fm_factory_pm_productsfm_factory_ida');
		if($.trim($("#fm_factory_pm_productsfm_factory_ida").val()) == '') {
		addToValidate('EditView', 'fm_factory_pm_products_name', 'text', true, SUGAR.language.get('pm_Products', 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_FM_FACTORY_TITLE'));
		} else {
		removeFromValidate('EditView','fm_factory_pm_products_name');
		}
		
	if (startD != '' && EndD != '') {
		if(isDate(startD) && isDate(EndD)) {
			var difference = diffDays(startD, EndD);
			if (difference < 0) {
				var msg = SUGAR.language.get('pm_Products',
						'LBL_PRODUCT_SALES_END_DATE')
						+ ' '
						+ SUGAR.language.get('pm_Products',
								'LBL_ALERT_DATE_VALIDATION')
						+ ' '
						+ SUGAR.language.get('pm_Products',
								'LBL_PRODUCT_INTRODUCTION_DATE');
				add_error_style('EditView', 'product_sales_end_date', msg, true);
				return false;
			}
		}
	}
	return check_form('EditView');
}

/**
 * @desc Function to calculate difference between two supplied dates
 * @params <string> - startD <string> - EndD
 * @return <int> - _diffDays
 * @author Sagar.Salunkhe
 * @date 30-Sep-2015
 */
function diffDays(startD, EndD) {
	startD = startD.replace(/-|\./gi, "/");
	EndD = EndD.replace(/-|\./gi, "/");
	var NstartD = DateFormat(startD);
	var NEndD = DateFormat(EndD);
	var date1 = Math.round(new Date(NEndD).getTime() / 1000);
	var date2 = Math.round(new Date(NstartD).getTime() / 1000);
	var daylen = 60 * 60 * 24;
	var _diffDays = ((date1) - (date2)) / daylen;
	return _diffDays;
}

/**
 * @desc Function to format date in Y-m-d
 * @params <string> - dt
 * @return <string> - yyyy+'-'+mm+'-'+dd
 * @author Sagar.Salunkhe
 * @date 18-12-2015
 */
function DateFormat(dt) {
	var ymd_date = '';
	$.ajax({
		type : "POST",
		url : "index.php",
		data : {
			module : 'pi_product_item',
			action : 'Actions',
			method : 'asDbDateFormat',
			sugar_body_only : '1',
			date : dt,
		},
		async : false,
		success : function(data) {
			var result = $.parseJSON(data);
			if (result.hasOwnProperty('error') || $.isEmptyObject(result))
				return;
			else if (result.date != '')
				ymd_date = result.date;
		},
		error : function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status);
			alert(thrownError);
		}
	});
	return ymd_date;
}