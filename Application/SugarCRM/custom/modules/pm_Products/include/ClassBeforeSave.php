<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');
require_once ('custom/modules/pm_Products/include/ClassPmProducts.php');
class ClassPmProductsBeforeSave extends ClassPmProducts
{

    /**
     * method to set incremental value to Autogen Product Id
     * @author Sagar.Salunkhe
     * @param
     *        (object)$bean - record sugar object [Sugar default]
     *        (object)$event - hook event [Sugar default]
     *        (array)$arguments - required parameters passed by framework [Sugar default]
     * @return (void method)
     *         @date 18-Dec-2015
     */
    function setAutogenProductId($bean, $event, $arguments)
    {
        if (! empty($bean->products_id)) return; // return if Global Contract Id is already present
        $bean->products_id = $this->generateAutogenProductId();
    }
}

?>