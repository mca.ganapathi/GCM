<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (?MSA?), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

class ClassPmProducts
{
    function __construct()
    {
        // module class file constructor
    }
	
    function generateAutogenProductId()
    {
        global $sugar_config;
        if (! isset($sugar_config['default_sequence_start']['pm_Products'])) $sugar_config['default_sequence_start']['pm_Products'] = 100001;
        
        $obj_products_bean = BeanFactory::getBean('pm_Products');
        $mod_table = $obj_products_bean->table_name;
        $where = "$mod_table.deleted IN(0,1,NULL)";
        $array_product_list = $obj_products_bean->get_full_list('', $where);

        $res_product_id = (floatval($sugar_config['default_sequence_start']['pm_Products'])) + sizeof($array_product_list);
        $res_product_id = number_format($res_product_id, 0, '', '');
        unset($array_product_list);
        return $res_product_id;
    }
	
}