<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
/**
 * : pi_product_itemController Controller Class
 * @author : Shrikant.Gaware
 *         @date : 24-Sep-2015
 */
class pm_ProductsController extends SugarController
{

    /**
     * @desc Method to update Product Status as Approved
     * @author Sagar.Salunkhe
     * @param
     * @return
     * @date 18-Dec-2015
     */
    function action_approve()
    {
        $GLOBALS['log']->info("Begin: pm_ProductsController->action_approve()");
        global $mod_strings;
        $product_id = $this->bean->id;
        $flag = 0;
        
        if ($this->bean->product_introduction_date != '' && $this->bean->product_sales_end_date != '') {
            $this->bean->product_status = 'Approved';
            $this->bean->save();
        } else {
            $flag = 1;
            $lbl = array();
            if ($this->bean->product_introduction_date == '') $lbl[] = $mod_strings['LBL_PRODUCT_INTRODUCTION_DATE'];
            if ($this->bean->product_sales_end_date == '') $lbl[] = $mod_strings['LBL_PRODUCT_SALES_END_DATE'];
        }
        
        if ($flag == 1) {
            $str = '';
            foreach ($lbl as $k => $v) {
                $str .= ($k + 1) . '. ' . $v . '<br />';
            }
            SugarApplication::appendErrorMessage($mod_strings['LBL_CANNOT_APPROVE'] . '<pre>' . $str . '</pre>');
        }
        $params = array(
            'module' => 'pm_Products',
            'action' => 'DetailView',
            'record' => $product_id
        );
        $GLOBALS['log']->info("Begin: pm_ProductsController->action_approve()");
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }
}
?>