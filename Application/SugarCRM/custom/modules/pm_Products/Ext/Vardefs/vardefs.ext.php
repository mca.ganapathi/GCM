<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/Vardefs/fm_factory_pm_products_pm_Products.php

 // created: 2016-09-29 04:39:35
$dictionary['pm_Products']['fields']['fm_factory_pm_products']['name'] = 'fm_factory_pm_products';
$dictionary['pm_Products']['fields']['fm_factory_pm_products']['type'] = 'link';
$dictionary['pm_Products']['fields']['fm_factory_pm_products']['relationship'] = 'fm_factory_pm_products';
$dictionary['pm_Products']['fields']['fm_factory_pm_products']['source'] = 'non-db';
$dictionary['pm_Products']['fields']['fm_factory_pm_products']['module'] = 'fm_Factory';
$dictionary['pm_Products']['fields']['fm_factory_pm_products']['bean_name'] = 'fm_Factory';
$dictionary['pm_Products']['fields']['fm_factory_pm_products']['vname'] = 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_FM_FACTORY_TITLE';
$dictionary['pm_Products']['fields']['fm_factory_pm_products']['id_name'] = 'fm_factory_pm_productsfm_factory_ida';
$dictionary['pm_Products']['fields']['fm_factory_pm_products_name']['name'] = 'fm_factory_pm_products_name';
$dictionary['pm_Products']['fields']['fm_factory_pm_products_name']['type'] = 'relate';
$dictionary['pm_Products']['fields']['fm_factory_pm_products_name']['source'] = 'non-db';
$dictionary['pm_Products']['fields']['fm_factory_pm_products_name']['vname'] = 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_FM_FACTORY_TITLE';
$dictionary['pm_Products']['fields']['fm_factory_pm_products_name']['save'] = true;
$dictionary['pm_Products']['fields']['fm_factory_pm_products_name']['id_name'] = 'fm_factory_pm_productsfm_factory_ida';
$dictionary['pm_Products']['fields']['fm_factory_pm_products_name']['link'] = 'fm_factory_pm_products';
$dictionary['pm_Products']['fields']['fm_factory_pm_products_name']['table'] = 'fm_factory';
$dictionary['pm_Products']['fields']['fm_factory_pm_products_name']['module'] = 'fm_Factory';
$dictionary['pm_Products']['fields']['fm_factory_pm_products_name']['rname'] = 'name';
$dictionary['pm_Products']['fields']['fm_factory_pm_products_name']['audited'] = true;
$dictionary['pm_Products']['fields']['fm_factory_pm_productsfm_factory_ida']['name'] = 'fm_factory_pm_productsfm_factory_ida';
$dictionary['pm_Products']['fields']['fm_factory_pm_productsfm_factory_ida']['type'] = 'id';
$dictionary['pm_Products']['fields']['fm_factory_pm_productsfm_factory_ida']['relationship'] = 'fm_factory_pm_products';
$dictionary['pm_Products']['fields']['fm_factory_pm_productsfm_factory_ida']['source'] = 'non-db';
$dictionary['pm_Products']['fields']['fm_factory_pm_productsfm_factory_ida']['reportable'] = false;
$dictionary['pm_Products']['fields']['fm_factory_pm_productsfm_factory_ida']['side'] = 'right';
$dictionary['pm_Products']['fields']['fm_factory_pm_productsfm_factory_ida']['vname'] = 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_PM_PRODUCTS_TITLE';
$dictionary['pm_Products']['fields']['fm_factory_pm_productsfm_factory_ida']['link'] = 'fm_factory_pm_products';
$dictionary['pm_Products']['fields']['fm_factory_pm_productsfm_factory_ida']['rname'] = 'id';

?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['pm_Products']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/Vardefs/pm_products_gc_contracts_1_pm_Products.php

// created: 2015-12-21 12:06:51
$dictionary["pm_Products"]["fields"]["pm_products_gc_contracts_1"] = array (
  'name' => 'pm_products_gc_contracts_1',
  'type' => 'link',
  'relationship' => 'pm_products_gc_contracts_1',
  'source' => 'non-db',
  'module' => 'gc_Contracts',
  'bean_name' => 'gc_Contracts',
  'side' => 'right',
  'vname' => 'LBL_PM_PRODUCTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/Vardefs/pm_products_pi_product_item_pm_Products.php

// created: 2016-02-18 07:06:47
$dictionary["pm_Products"]["fields"]["pm_products_pi_product_item"] = array (
  'name' => 'pm_products_pi_product_item',
  'type' => 'link',
  'relationship' => 'pm_products_pi_product_item',
  'source' => 'non-db',
  'module' => 'pi_product_item',
  'bean_name' => 'pi_product_item',
  'vname' => 'LBL_PM_PRODUCTS_PI_PRODUCT_ITEM_FROM_PI_PRODUCT_ITEM_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/Vardefs/sugarfield_product_introduction_date.php

 // created: 2015-12-18 09:25:48

 
?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/Vardefs/sugarfield_product_sales_end_date.php

 // created: 2015-12-18 09:25:58

 
?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/Vardefs/sugarfield_product_version.php

 // created: 2016-09-29 04:48:38
$dictionary['pm_Products']['fields']['product_version']['default'] = '1.0';
$dictionary['pm_Products']['fields']['product_version']['precision'] = '1';


?>
