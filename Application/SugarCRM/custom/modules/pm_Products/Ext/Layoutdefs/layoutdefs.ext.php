<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/Layoutdefs/pm_products_gc_contracts_1_pm_Products.php

 // created: 2015-12-21 12:06:40
$layout_defs["pm_Products"]["subpanel_setup"]['pm_products_gc_contracts_1'] = array (
  'order' => 100,
  'module' => 'gc_Contracts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PM_PRODUCTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
  'get_subpanel_data' => 'pm_products_gc_contracts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/Layoutdefs/pm_products_pi_product_item_pm_Products.php

 // created: 2016-02-18 07:06:47
$layout_defs["pm_Products"]["subpanel_setup"]['pm_products_pi_product_item'] = array (
  'order' => 100,
  'module' => 'pi_product_item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PM_PRODUCTS_PI_PRODUCT_ITEM_FROM_PI_PRODUCT_ITEM_TITLE',
  'get_subpanel_data' => 'pm_products_pi_product_item',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/Layoutdefs/removeSubpanelCustom.php

/*** START : Remove unnecessary subpanels that we can't hide from Admin Panel ***/
unset($layout_defs["pm_Products"]["subpanel_setup"]['pm_products_pi_product_item']['top_buttons'][0]);			//unset create button of Product Items subpanel
unset($layout_defs["pm_Products"]["subpanel_setup"]['pm_products_gc_contracts_1']['top_buttons'][0]);			//unset create button of contracts subpanel
unset($layout_defs["pm_Products"]["subpanel_setup"]['pm_products_gc_contracts_1']['top_buttons'][1]);			//unset select button of contracts subpanel
/*** END : Remove unnecessary subpanels that we can't hide from Admin Panel ***/
 
?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/Layoutdefs/_overridepm_Products_subpanel_pm_products_gc_contracts_1.php

//auto-generated file DO NOT EDIT
$layout_defs['pm_Products']['subpanel_setup']['pm_products_gc_contracts_1']['override_subpanel_name'] = 'pm_Products_subpanel_pm_products_gc_contracts_1';

?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/Layoutdefs/_overridepm_Products_subpanel_pm_products_pi_product_item.php

//auto-generated file DO NOT EDIT
$layout_defs['pm_Products']['subpanel_setup']['pm_products_pi_product_item']['override_subpanel_name'] = 'pm_Products_subpanel_pm_products_pi_product_item';

?>
