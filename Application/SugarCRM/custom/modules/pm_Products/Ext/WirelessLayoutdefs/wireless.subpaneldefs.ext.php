<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/WirelessLayoutdefs/pm_products_gc_contracts_1_pm_Products.php

 // created: 2015-12-21 12:06:51
$layout_defs["pm_Products"]["subpanel_setup"]['pm_products_gc_contracts_1'] = array (
  'order' => 100,
  'module' => 'gc_Contracts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_PM_PRODUCTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
  'get_subpanel_data' => 'pm_products_gc_contracts_1',
);

?>
<?php
// Merged from custom/Extension/modules/pm_Products/Ext/WirelessLayoutdefs/pm_products_pi_product_item_pm_Products.php

 // created: 2016-02-18 07:06:47
$layout_defs["pm_Products"]["subpanel_setup"]['pm_products_pi_product_item'] = array (
  'order' => 100,
  'module' => 'pi_product_item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_PM_PRODUCTS_PI_PRODUCT_ITEM_FROM_PI_PRODUCT_ITEM_TITLE',
  'get_subpanel_data' => 'pm_products_pi_product_item',
);

?>
