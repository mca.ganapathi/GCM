<?php
// WARNING: The contents of this file are auto-generated.


// created: 2016-09-29 04:41:43
$viewdefs['pm_Products']['mobile']['layout']['subpanels']['components'][] = array (
  'label' => 'LBL_PM_PRODUCTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
  'context' => 
  array (
    'link' => 'pm_products_gc_contracts_1',
  ),
  'layout' => 'subpanel',
);

// created: 2016-09-29 04:41:43
$viewdefs['pm_Products']['mobile']['layout']['subpanels']['components'][] = array (
  'label' => 'LBL_PM_PRODUCTS_PI_PRODUCT_ITEM_FROM_PI_PRODUCT_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'pm_products_pi_product_item',
  ),
  'layout' => 'subpanel',
);