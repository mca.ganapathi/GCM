<?php
// created: 2016-09-29 04:41:43
$viewdefs['pm_Products']['base']['view']['subpanel-pi_product_item_subpanel_pm_products_pi_product_item'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'type' => 'varchar',
          'default' => true,
          'label' => 'LBL_PRODUCTS_ID',
          'enabled' => true,
          'name' => 'products_id',
        ),
        1 => 
        array (
          'default' => true,
          'label' => 'LBL_NAME',
          'enabled' => true,
          'name' => 'name',
          'link' => true,
          'type' => 'name',
        ),
        2 => 
        array (
          'type' => 'enum',
          'default' => true,
          'label' => 'LBL_PRODUCT_STATUS',
          'enabled' => true,
          'name' => 'product_status',
        ),
        3 => 
        array (
          'type' => 'relate',
          'link' => true,
          'default' => true,
          'target_module' => 'fm_Factory',
          'target_record_key' => 'fm_factory_pm_productsfm_factory_ida',
          'label' => 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_FM_FACTORY_TITLE',
          'enabled' => true,
          'name' => 'fm_factory_pm_products_name',
        ),
        4 => 
        array (
          'default' => true,
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'name' => 'date_modified',
          'type' => 'datetime',
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);