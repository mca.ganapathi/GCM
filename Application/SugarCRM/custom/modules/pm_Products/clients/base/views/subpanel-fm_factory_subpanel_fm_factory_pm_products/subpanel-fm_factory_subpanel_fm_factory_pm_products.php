<?php
// created: 2016-09-29 04:41:43
$viewdefs['pm_Products']['base']['view']['subpanel-fm_factory_subpanel_fm_factory_pm_products'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'type' => 'varchar',
          'default' => true,
          'label' => 'LBL_PRODUCTS_ID',
          'enabled' => true,
          'name' => 'products_id',
        ),
        1 => 
        array (
          'default' => true,
          'label' => 'LBL_NAME',
          'enabled' => true,
          'name' => 'name',
          'link' => true,
          'type' => 'name',
        ),
        2 => 
        array (
          'type' => 'enum',
          'default' => true,
          'label' => 'LBL_PRODUCT_STATUS',
          'enabled' => true,
          'name' => 'product_status',
        ),
        3 => 
        array (
          'default' => true,
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'name' => 'date_modified',
          'type' => 'datetime',
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);