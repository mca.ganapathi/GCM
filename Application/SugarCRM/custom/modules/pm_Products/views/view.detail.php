<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

class pm_ProductsViewDetail extends ViewDetail
{

    function preDisplay()
    {
        // call parent preDisplay method
        parent::preDisplay();
    }

    function display()
    {
        unset($this->dv->defs['templateMeta']['form']['buttons'][1]); // Hide Duplicate button
        
        if($this->bean->product_status == 'Approved'){
            unset($this->dv->defs['templateMeta']['form']['buttons'][0]); // Hide Edit button
            unset($this->dv->defs['templateMeta']['form']['buttons'][2]); // Hide Delete button
            unset($this->dv->defs['templateMeta']['form']['buttons'][4]); // Hide Approve button
        }

        unset($this->dv->defs['templateMeta']['form']['buttons'][3]); // Hide Find Duplicate button
        parent::display();
    }
    
    /**
     * Override the _displaySubPanels method to support subpanel customizations *
     */
    function _displaySubPanels()
    {
    	require_once ('custom/include/SubPanel/SubPanelTiles.php');
    	$subpanel = new CustomSubPanelTiles($this->bean, $this->module);
    	unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts']['top_buttons'][0]); // hiding create
    	unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts']['top_buttons'][1]); // hiding select
    	echo $subpanel->display();
    }
}
