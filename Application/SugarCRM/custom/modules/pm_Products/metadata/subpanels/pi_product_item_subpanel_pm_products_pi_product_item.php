<?php
// created: 2015-12-18 09:35:21
$subpanel_layout['list_fields'] = array (
  'products_id' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_PRODUCTS_ID',
    'width' => '20%',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '30%',
    'default' => true,
  ),
  'product_status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_PRODUCT_STATUS',
    'width' => '10%',
  ),
  'fm_factory_pm_products_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_FM_FACTORY_TITLE',
    'id' => 'FM_FACTORY_PM_PRODUCTSFM_FACTORY_IDA',
    'width' => '20%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'fm_Factory',
    'target_record_key' => 'fm_factory_pm_productsfm_factory_ida',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '20%',
    'default' => true,
  ),
);