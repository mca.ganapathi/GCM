<?php
// created: 2015-12-21 09:40:11
$subpanel_layout['list_fields'] = array (
  'products_id' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_PRODUCTS_ID',
    'width' => '20%',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '40%',
    'default' => true,
  ),
  'product_status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_PRODUCT_STATUS',
    'width' => '20%',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '20%',
    'default' => true,
  ),
);