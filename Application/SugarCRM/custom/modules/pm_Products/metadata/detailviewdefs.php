<?php
$module_name = 'pm_Products';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
          4 => 
          array (
            'customCode' => '<input type="submit" value="{sugar_translate label=\'LBL_APPROVE\' module=\'pm_Products\'}"
	 name="button" onclick="this.form.return_module.value=\'pm_Products\'; this.form.return_action.value=\'DetailView\';this.form.return_id.value=\'{$fields.id.value}\'; this.form.action.value=\'approve\';" class="button" accesskey="A" title="Approve Product [Alt+A]" id="btn_approve_product" />',
          ),
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'products_id',
            'label' => 'LBL_PRODUCTS_ID',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'product_status',
            'studio' => 'visible',
            'label' => 'LBL_PRODUCT_STATUS',
          ),
          1 => 
          array (
            'name' => 'product_version',
            'label' => 'LBL_PRODUCT_VERSION',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'product_introduction_date',
            'label' => 'LBL_PRODUCT_INTRODUCTION_DATE',
          ),
          1 => 
          array (
            'name' => 'product_sales_end_date',
            'label' => 'LBL_PRODUCT_SALES_END_DATE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'fm_factory_pm_products_name',
          ),
          1 => 'id',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 => 
          array (
            'name' => 'date_modified',
            'comment' => 'Date record last modified',
            'label' => 'LBL_DATE_MODIFIED',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
    ),
  ),
);
?>