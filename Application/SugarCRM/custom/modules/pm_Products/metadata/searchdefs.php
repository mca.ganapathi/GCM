<?php
$module_name = 'pm_Products';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'products_id' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_PRODUCTS_ID',
        'width' => '10%',
        'default' => true,
        'name' => 'products_id',
      ),
      'favorites_only' => 
      array (
        'name' => 'favorites_only',
        'label' => 'LBL_FAVORITES_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'products_id' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_PRODUCTS_ID',
        'width' => '10%',
        'default' => true,
        'name' => 'products_id',
      ),
      'fm_factory_pm_products_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_FM_FACTORY_TITLE',
        'id' => 'FM_FACTORY_PM_PRODUCTSFM_FACTORY_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'fm_factory_pm_products_name',
      ),
      'product_introduction_date' => 
      array (
        'type' => 'date',
        'label' => 'LBL_PRODUCT_INTRODUCTION_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'product_introduction_date',
      ),
      'product_sales_end_date' => 
      array (
        'type' => 'date',
        'label' => 'LBL_PRODUCT_SALES_END_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'product_sales_end_date',
      ),
      'favorites_only' => 
      array (
        'name' => 'favorites_only',
        'label' => 'LBL_FAVORITES_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>