<?php
$module_name = 'pm_Products';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/modules/pm_Products/include/javascript/EditView.js',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'fm_factory_pm_products_name',
            'displayParams' => 
            array (
              'required' => true,
            ),
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'product_status',
            'studio' => 'visible',
            'label' => 'LBL_PRODUCT_STATUS',
            'type' => 'readonly',
          ),
          1 => 
          array (
            'name' => 'product_version',
            'label' => 'LBL_PRODUCT_VERSION',
            'type' => 'readonly',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'product_introduction_date',
            'label' => 'LBL_PRODUCT_INTRODUCTION_DATE',
          ),
          1 => 
          array (
            'name' => 'product_sales_end_date',
            'label' => 'LBL_PRODUCT_SALES_END_DATE',
          ),
        ),
      ),
    ),
  ),
);
?>