<?php
// created: 2016-06-21 06:23:01
$mod_strings = array (
  'LBL_PRODUCT_INTRODUCTION_DATE' => 'Product Introduction Date',
  'LBL_PRODUCT_SALES_END_DATE' => 'Product Sales End Date',
  'LBL_PM_PRODUCTS_PI_PRODUCT_ITEM_FROM_PI_PRODUCT_ITEM_TITLE' => 'Child Product Item',
  'LBL_ID' => 'Object ID',
  'LBL_PM_PRODUCTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE' => 'GCM Contracts',
  'LBL_PRODUCT_TYPE' => 'Product Type',
);