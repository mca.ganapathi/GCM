<?php
// created: 2016-09-29 04:41:37
$viewdefs['Cases']['base']['menu']['quickcreate'] = array (
  'layout' => 'create',
  'label' => 'LNK_NEW_CASE',
  'visible' => false,
  'icon' => 'fa-plus',
  'order' => 11,
);