<?php
// created: 2015-09-08 12:57:43
$mod_strings = array (
  'LBL_ACCOUNT_NAME' => 'Corporate Name:',
  'LBL_ACCOUNT_ID' => 'Corporate ID',
  'LBL_CONTACTS' => 'Accounts',
  'LBL_ACCOUNT' => 'Corporate',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Accounts',
  'LBL_LIST_ACCOUNT_NAME' => 'Corporate Name',
);