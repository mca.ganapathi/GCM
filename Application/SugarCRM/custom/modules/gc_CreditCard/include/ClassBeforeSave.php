<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * class ClassCreditCardBeforeSave
 */
class ClassCreditCardBeforeSave
{

    /**
     * method setLineItemAuditEntry to Save Audit Logs under Line Item Module
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author dinesh.itkar
     * @since Jan 02, 2016
     */
    function setLineItemAuditEntry($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'method to Save Audit Logs under Line Item Module');
        
        // Avoid audit log for new record.
        if (empty($bean->fetched_row['id'])) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
            return;
        }
        
        if (!empty($bean->contract_history_id)) {
            $obj_line_Item_contract_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History');
            $obj_line_Item_contract_history_bean->retrieve($bean->contract_history_id);
            $obj_line_item_bean = BeanFactory::getBean('gc_LineItem');
            $obj_line_item_bean->retrieve($obj_line_Item_contract_history_bean->line_item_id);
        }
        
        if (!empty($obj_line_item_bean)) {
            // Audit Credit Card Number
            $bean_credit_card_no = ((!empty($bean->credit_card_no))?$bean->credit_card_no:'');
            $bean_fetched_credit_card_no = ((!empty($bean->fetched_row['credit_card_no']))?$bean->fetched_row['credit_card_no']:'');
            if ($bean_credit_card_no != $bean_fetched_credit_card_no) {
                $this->insertCreditCardAudit($obj_line_item_bean, 'Credit Card Number', 'text', $bean_fetched_credit_card_no, $bean_credit_card_no);
            }
            
            // Audit Credit Card Expiration Date
            $bean_expiration_date = ((!empty($bean->expiration_date))?$bean->expiration_date:'');
            $bean_fetched_expiration_date = ((!empty($bean->fetched_row['expiration_date']))?$bean->fetched_row['expiration_date']:'');            
            if ($bean_expiration_date != $bean_fetched_expiration_date) {
                $this->insertCreditCardAudit($obj_line_item_bean, 'Credit Card Expiration Date', 'text', $bean_fetched_expiration_date, $bean_expiration_date);
            }
        }
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * method insertCreditCardAudit to save Credit Card Audit record.
     * 
     * @param (object $bean record sugar object [Sugar default]
     * @param string $field_name field name to audit
     * @param string $data_type date type of field
     * @param string $before value before update
     * @param string $after value after update
     * @author dinesh.itkar
     * @since Jan 02, 2016
     */
    private function insertCreditCardAudit($bean, $field_name, $data_type, $before, $after)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'field_name : '.$field_name.GCM_GL_LOG_VAR_SEPARATOR.'data_type : '.$data_type.GCM_GL_LOG_VAR_SEPARATOR.'before : '.$before.GCM_GL_LOG_VAR_SEPARATOR.'after : '.$after, 'to save Credit Card Audit record');
        $audit_entry = array();
        $audit_entry['field_name'] = $field_name;
        $audit_entry['data_type'] = $data_type;
        $audit_entry['before'] = $before;
        $audit_entry['after'] = $after;
        $bean->db->save_audit_records($bean, $audit_entry);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}

?>