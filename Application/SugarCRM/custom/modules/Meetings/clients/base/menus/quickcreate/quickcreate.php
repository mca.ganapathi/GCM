<?php
// created: 2016-09-29 04:41:37
$viewdefs['Meetings']['base']['menu']['quickcreate'] = array (
  'layout' => 'create',
  'label' => 'LNK_NEW_MEETING',
  'visible' => false,
  'icon' => 'fa-calendar',
  'order' => 1,
);