<?php
// created: 2015-09-08 12:57:43
$mod_strings = array (
  'LBL_CONTACT_NAME' => 'Account:',
  'LBL_CONTACTS' => 'Accounts',
  'LBL_ACCOUNT' => 'Corporate',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Accounts',
  'LBL_LIST_CONTACT' => 'Account',
);