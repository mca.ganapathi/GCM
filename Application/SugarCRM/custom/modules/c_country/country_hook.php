<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

/**
 * Module Logic Hook Class ClassBeforeSave for executing hooks
 */
class ClassBeforeSave
{

    /* Method checkBeforeSave for hook execution
     * @author Natarajan
     * @since 6-Oct-2015
     * 
     */
    function checkBeforeSave($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'Module Logic Hook Class ClassBeforeSave for executing hooks');        
        global $mod_strings;
        
        if (isset($_REQUEST['record'])) {
            $objCountry = BeanFactory::getBean('c_country');
            $where = " ( c_country.name LIKE  '" . $bean->name . "'
						   OR c_country.country_code_alphabet LIKE  '" . $bean->country_code_alphabet . "' 
						   OR c_country.country_code_numeric LIKE '" . $bean->country_code_numeric . "' ) ";
            
            if (isset($_REQUEST['record']))
                $where .= " AND  c_country.id <> '" . $bean->id . "'";
            
            $array_obj_country = $objCountry->get_full_list('c_country', $where);
            if (count($array_obj_country) >= 1) {
                $queryParams = array(
                                    'module' => 'c_country', 
                                    'action' => 'EditView'
                );
                $_SESSION['country'] = $_POST;
                SugarApplication::appendErrorMessage('"' . $bean->name . '" or  "' . $bean->country_code_alphabet . '" or  "' . $bean->country_code_numeric . '" "' . $mod_strings['LBL_DENY_SAVE']);
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), '');
                SugarApplication::redirect('index.php?' . http_build_query($queryParams));
            }
        }               
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), '');
    }
}