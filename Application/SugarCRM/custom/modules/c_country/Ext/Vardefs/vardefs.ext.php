<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/c_country/Ext/Vardefs/c_country_c_state_1_c_country.php

// created: 2016-01-19 07:00:34
$dictionary["c_country"]["fields"]["c_country_c_state_1"] = array (
  'name' => 'c_country_c_state_1',
  'type' => 'link',
  'relationship' => 'c_country_c_state_1',
  'source' => 'non-db',
  'module' => 'c_State',
  'bean_name' => 'c_State',
  'side' => 'right',
  'vname' => 'LBL_C_COUNTRY_C_STATE_1_FROM_C_STATE_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/c_country/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['c_country']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/c_country/Ext/Vardefs/sugarfield_country_code_alphabet.php

 // created: 2016-12-19 09:59:35
$dictionary['c_country']['fields']['country_code_alphabet']['audited']=true;
$dictionary['c_country']['fields']['country_code_alphabet']['massupdate']=false;
$dictionary['c_country']['fields']['country_code_alphabet']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);

 
?>
<?php
// Merged from custom/Extension/modules/c_country/Ext/Vardefs/sugarfield_country_code_c.php

 // created: 2015-10-06 07:39:57

 
?>
<?php
// Merged from custom/Extension/modules/c_country/Ext/Vardefs/sugarfield_country_code_numeric.php

 // created: 2016-12-19 10:00:16
$dictionary['c_country']['fields']['country_code_numeric']['audited']=true;
$dictionary['c_country']['fields']['country_code_numeric']['massupdate']=false;
$dictionary['c_country']['fields']['country_code_numeric']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);

 
?>
<?php
// Merged from custom/Extension/modules/c_country/Ext/Vardefs/sugarfield_name.php

 // created: 2016-12-19 09:58:33
$dictionary['c_country']['fields']['name']['audited']=true;
$dictionary['c_country']['fields']['name']['massupdate']=false;
$dictionary['c_country']['fields']['name']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '3',
  'searchable' => false,
);

 
?>
<?php
// Merged from custom/Extension/modules/c_country/Ext/Vardefs/sugarfield_region.php

 // created: 2016-12-19 10:00:57
$dictionary['c_country']['fields']['region']['audited']=true;
$dictionary['c_country']['fields']['region']['massupdate']=false;

 
?>
