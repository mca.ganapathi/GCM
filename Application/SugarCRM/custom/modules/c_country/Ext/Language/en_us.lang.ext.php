<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/c_country/Ext/Language/en_us.customc_country_c_state_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_C_COUNTRY_C_STATE_1_FROM_C_STATE_TITLE'] = 'States';

?>
<?php
// Merged from custom/Extension/modules/c_country/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Country Name';
$mod_strings['LBL_COUNTRY_CODE_ALPHABET'] = 'Country Code (alphabet)';
$mod_strings['LBL_COUNTRY_CODE_NUMERIC'] = 'Country Code (numeric)';
$mod_strings['LBL_REGION'] = 'Region';

?>
