<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/c_country/Ext/Layoutdefs/c_country_c_state_1_c_country.php

 // created: 2016-01-19 07:00:33
$layout_defs["c_country"]["subpanel_setup"]['c_country_c_state_1'] = array (
  'order' => 100,
  'module' => 'c_State',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_C_COUNTRY_C_STATE_1_FROM_C_STATE_TITLE',
  'get_subpanel_data' => 'c_country_c_state_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/c_country/Ext/Layoutdefs/removeSubpanel.php

/*** START : Remove unnecessary subpanels that we can't hide from Admin Panel ***/
unset($layout_defs["c_country"]["subpanel_setup"]['c_country_c_state_1']['top_buttons'][0]);   //unset state subpanel create button
unset($layout_defs["c_country"]["subpanel_setup"]['c_country_c_state_1']['top_buttons'][1]);   //unset state subpanel select button

?>
<?php
// Merged from custom/Extension/modules/c_country/Ext/Layoutdefs/_overridec_country_subpanel_c_country_c_state_1.php

//auto-generated file DO NOT EDIT
$layout_defs['c_country']['subpanel_setup']['c_country_c_state_1']['override_subpanel_name'] = 'c_country_subpanel_c_country_c_state_1';

?>
