<?php
// WARNING: The contents of this file are auto-generated.


// created: 2016-09-29 04:41:43
$viewdefs['c_country']['base']['layout']['subpanels']['components'][] = array (
  'label' => 'LBL_C_COUNTRY_C_STATE_1_FROM_C_STATE_TITLE',
  'context' => 
  array (
    'link' => 'c_country_c_state_1',
  ),
  'layout' => 'subpanel',
);

// created: 2016-09-29 04:41:43
$viewdefs['c_country']['base']['layout']['subpanels']['components'][] = array (
  'override_subpanel_list_view' => 
  array (
    'view' => 'subpanel-c_country_subpanel_c_country_c_state_1',
    'link' => 'c_country_c_state_1',
  ),
);