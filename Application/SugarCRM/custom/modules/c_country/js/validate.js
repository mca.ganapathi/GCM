//alert('kkk');
/**
 * Validation JS
 */
/**
 * @desc   : Function to check added custom validations
 * @params : 
 * @return : <boolean> true/false
 * @author : Natarajan
 * @date   : 06-O2015
 */
function cstmValidation(){
var flag = true;
	 flag = check_form('EditView');
	
	if(flag) {
		var flagCntry = true;
		cntry = $('#name').val();
		cntry_alphabet = $('#country_code_alphabet').val();
		cntry_numeric = $('#country_code_numeric').val();
		
		var regEx = /^[a-zA-Z\s]+$/;
		flagCntry = regEx.test(cntry);
		if( !flagCntry ){
		  var msg = ' Invalid Country ';
			YAHOO.SUGAR.MessageBox.show({msg: msg, type: 'plain'} );
			return false;
           }
        var flagCntryAlphabet = true;
        var regEx = /^[a-zA-Z]{3}$/;
        flagCntryAlphabet = regEx.test(cntry_alphabet);
		  if(!flagCntryAlphabet ){
		    var msg = 'Invalid Country Code  (alphabet) ';
			 YAHOO.SUGAR.MessageBox.show({msg: msg, type: 'plain'} );
			 return false;
         }        
		 var regExp = /^[0-9]{3}$/;
	        var flagCntryNum = true;
        var  flagCntryNum = regExp.test(cntry_numeric);
		  if(!flagCntryNum ){
		    var msg = 'Invalid Country Code (numeric)';
			 YAHOO.SUGAR.MessageBox.show({msg: msg, type: 'plain'} );
			 return false;
         }        
	}
	return flag ;
	
}