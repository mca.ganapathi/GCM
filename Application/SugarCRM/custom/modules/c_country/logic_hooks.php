<?php
$hook_version = 1;
$hook_array = Array();

$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(
                                    // Processing index. For sorting the array.
                                    1, 
                                    
                                    // Label. A string value to identify the hook.
                                    'before_save check entity', 
                                    
                                    // The PHP file where your class is located.
                                    'custom/modules/c_country/country_hook.php', 
                                    
                                    // The class the method is in.
                                    'ClassBeforeSave', 
                                    
                                    // The method to call.
                                    'checkBeforeSave'
);
?>