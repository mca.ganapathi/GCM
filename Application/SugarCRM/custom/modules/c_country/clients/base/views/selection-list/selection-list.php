<?php
$viewdefs['c_country'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
              2 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
              3 => 
              array (
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'default' => false,
                'name' => 'date_modified',
                'readonly' => true,
              ),
              4 => 
              array (
                'type' => 'enum',
                'default' => true,
                'studio' => 'visible',
                'label' => 'LBL_REGION',
                'enabled' => true,
                'name' => 'region',
              ),
              5 => 
              array (
                'type' => 'varchar',
                'label' => 'LBL_COUNTRY_CODE_ALPHABET',
                'default' => true,
                'enabled' => true,
                'name' => 'country_code_alphabet',
              ),
              6 => 
              array (
                'type' => 'varchar',
                'label' => 'LBL_COUNTRY_CODE_NUMERIC',
                'default' => true,
                'enabled' => true,
                'name' => 'country_code_numeric',
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
