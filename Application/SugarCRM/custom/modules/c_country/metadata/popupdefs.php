<?php
$popupMeta = array (
    'moduleMain' => 'c_country',
    'varName' => 'c_country',
    'orderBy' => 'c_country.name',
    'whereClauses' => array (
  'name' => 'c_country.name',
  'region' => 'c_country.region',
  'country_code_alphabet' => 'c_country.country_code_alphabet',
  'country_code_numeric' => 'c_country.country_code_numeric',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'region',
  5 => 'country_code_alphabet',
  6 => 'country_code_numeric',
),
    'searchdefs' => array (
  'region' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_REGION',
    'width' => '10%',
    'name' => 'region',
  ),
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'country_code_alphabet' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_COUNTRY_CODE_ALPHABET',
    'width' => '10%',
    'name' => 'country_code_alphabet',
  ),
  'country_code_numeric' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_COUNTRY_CODE_NUMERIC',
    'width' => '10%',
    'name' => 'country_code_numeric',
  ),
),
    'listviewdefs' => array (
  'REGION' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_REGION',
    'width' => '10%',
  ),
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'COUNTRY_CODE_ALPHABET' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_COUNTRY_CODE_ALPHABET',
    'width' => '10%',
    'default' => true,
  ),
  'COUNTRY_CODE_NUMERIC' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_COUNTRY_CODE_NUMERIC',
    'width' => '10%',
    'default' => true,
  ),
),
);
