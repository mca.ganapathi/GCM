<?php
$module_name = 'c_country';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
	   'form' => 
      array (
         'buttons' => 
        array (
          0 => 
          array (
            'customCode' => '<input type="submit" name="save" id="save" onClick="this.form.return_action.value=\'DetailView\'; 
								this.form.action.value=\'Save\'; return cstmValidation();" value="Save">',
          ),
          1 => 'CANCEL',
        ),
      ),
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
	   'includes' => 
      array (
	     0 => 
        array (
          'file' => 'cache/include/javascript/sugar_grp_yui_widgets.js',
        ),
        1 => 
        array (
          'file' =>  'custom/modules/c_country/js/validate.js',
        ),      
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'region',
            'studio' => 'visible',
            'label' => 'LBL_REGION',
          ),
        ),
        1 => 
        array (
          0 => 'name',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'country_code_alphabet',
            'label' => 'LBL_COUNTRY_CODE_ALPHABET',
          ),
          1 => 
          array (
            'name' => 'country_code_numeric',
            'label' => 'LBL_COUNTRY_CODE_NUMERIC',
          ),
        ),
      ),
    ),
  ),
);

?>