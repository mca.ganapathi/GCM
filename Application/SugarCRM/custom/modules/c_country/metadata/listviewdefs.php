<?php
// created: 2018-03-07 11:45:26
$listViewDefs['c_country'] = array (
  'REGION' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_REGION',
    'width' => '10',
  ),
  'NAME' => 
  array (
    'width' => '32',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'COUNTRY_CODE_ALPHABET' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_COUNTRY_CODE_ALPHABET',
    'width' => '10',
    'default' => true,
  ),
  'COUNTRY_CODE_NUMERIC' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_COUNTRY_CODE_NUMERIC',
    'width' => '10',
    'default' => true,
  ),
  'TEAM_NAME' => 
  array (
    'width' => '9',
    'label' => 'LBL_TEAM',
    'default' => false,
  ),
);