<?php
require_once('include/json_config.php');
require_once('custom/include/MVC/View/views/view.list.php');
class c_countryViewList extends CustomViewList
{
	function __construct()
	{
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'constructor');
		parent::__construct();
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
	}
	
	function display()
	{
	    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'display');
		$this->lv->quickViewLinks = false;
		$this->lv->mergeduplicates = false;
		$this->lv->showMassupdateFields = false;
		$this->lv->export = false;
		parent::Display();
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
	}
}
