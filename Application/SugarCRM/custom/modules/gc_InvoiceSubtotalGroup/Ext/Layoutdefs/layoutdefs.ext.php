<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/gc_InvoiceSubtotalGroup/Ext/Layoutdefs/gc_invoicesubtotalgroup_gc_line_item_contract_history_1_gc_InvoiceSubtotalGroup.php

 // created: 2016-05-04 05:46:26
$layout_defs["gc_InvoiceSubtotalGroup"]["subpanel_setup"]['gc_invoicesubtotalgroup_gc_line_item_contract_history_1'] = array (
  'order' => 100,
  'module' => 'gc_Line_Item_Contract_History',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_GC_INVOICESUBTOTALGROUP_GC_LINE_ITEM_CONTRACT_HISTORY_1_FROM_GC_LINE_ITEM_CONTRACT_HISTORY_TITLE',
  'get_subpanel_data' => 'gc_invoicesubtotalgroup_gc_line_item_contract_history_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/gc_InvoiceSubtotalGroup/Ext/Layoutdefs/removeSubpanel.php

/*** START : Remove unnecessary subpanels that we can't hide from Admin Panel ***/

unset($layout_defs["gc_InvoiceSubtotalGroup"]["subpanel_setup"]['gc_invoicesubtotalgroup_gc_line_item_contract_history_1']);

?>
