<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/gc_InvoiceSubtotalGroup/Ext/Language/en_us.customgc_contracts_gc_invoicesubtotalgroup_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GC_CONTRACTS_GC_INVOICESUBTOTALGROUP_1_FROM_GC_CONTRACTS_TITLE'] = 'Contracts';

?>
<?php
// Merged from custom/Extension/modules/gc_InvoiceSubtotalGroup/Ext/Language/en_us.customgc_invoicesubtotalgroup_gc_line_item_contract_history_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GC_CONTRACTS_GC_INVOICESUBTOTALGROUP_1_FROM_GC_CONTRACTS_TITLE'] = 'Contracts';
$mod_strings['LBL_GC_INVOICESUBTOTALGROUP_GC_LINE_ITEM_CONTRACT_HISTORY_1_FROM_GC_LINE_ITEM_CONTRACT_HISTORY_TITLE'] = 'LineItemContractHistory';

?>
<?php
// Merged from custom/Extension/modules/gc_InvoiceSubtotalGroup/Ext/Language/en_us.StudioInvoiceGroup_6-2-1_060616.php
 
 // created: 2016-06-06 13:49:43
$mod_strings['LBL_DETAILVIEW_PANEL1'] = 'Contract Overview';
$mod_strings['LBL_DETAILVIEW_PANEL2'] = 'Contract Line Items';
$mod_strings['LBL_GLOBAL_CONTRACT'] = 'Global Contract ID';
$mod_strings['LBL_VERSION'] = 'Version';
$mod_strings['LBL_CONTRACT_NAME'] = 'Name';
$mod_strings['LBL_CONTRACTING_COMPANY'] = 'Contracting Company';
$mod_strings['LBL_PRODUCT_OFFERING'] = 'Product Offering';
$mod_strings['LBL_LINE_ITEM'] = 'Global Contract Item ID';
$mod_strings['LBL_PRODUCT_SPECIFICATION'] = 'Product Specification';
$mod_strings['LBL_SERVICE_START_DATE'] = 'Service Start Date';
$mod_strings['LBL_SERVICE_END_DATE'] = 'Service End Date';


?>
