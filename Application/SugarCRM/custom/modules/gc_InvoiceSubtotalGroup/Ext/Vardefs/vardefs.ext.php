<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/gc_InvoiceSubtotalGroup/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_InvoiceSubtotalGroup']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/gc_InvoiceSubtotalGroup/Ext/Vardefs/gc_contracts_gc_invoicesubtotalgroup_1_gc_InvoiceSubtotalGroup.php

 // created: 2016-09-29 04:39:33
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1']['name'] = 'gc_contracts_gc_invoicesubtotalgroup_1';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1']['type'] = 'link';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1']['relationship'] = 'gc_contracts_gc_invoicesubtotalgroup_1';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1']['source'] = 'non-db';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1']['module'] = 'gc_Contracts';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1']['bean_name'] = 'gc_Contracts';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1']['vname'] = 'LBL_GC_CONTRACTS_GC_INVOICESUBTOTALGROUP_1_FROM_GC_CONTRACTS_TITLE';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1']['id_name'] = 'gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1_name']['name'] = 'gc_contracts_gc_invoicesubtotalgroup_1_name';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1_name']['type'] = 'relate';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1_name']['source'] = 'non-db';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1_name']['vname'] = 'LBL_GC_CONTRACTS_GC_INVOICESUBTOTALGROUP_1_FROM_GC_CONTRACTS_TITLE';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1_name']['save'] = true;
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1_name']['id_name'] = 'gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1_name']['link'] = 'gc_contracts_gc_invoicesubtotalgroup_1';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1_name']['table'] = 'gc_contracts';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1_name']['module'] = 'gc_Contracts';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1_name']['rname'] = 'name';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida']['name'] = 'gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida']['type'] = 'id';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida']['relationship'] = 'gc_contracts_gc_invoicesubtotalgroup_1';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida']['source'] = 'non-db';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida']['reportable'] = false;
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida']['side'] = 'right';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida']['vname'] = 'LBL_GC_CONTRACTS_GC_INVOICESUBTOTALGROUP_1_FROM_GC_INVOICESUBTOTALGROUP_TITLE';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida']['link'] = 'gc_contracts_gc_invoicesubtotalgroup_1';
$dictionary['gc_InvoiceSubtotalGroup']['fields']['gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida']['rname'] = 'id';

?>
<?php
// Merged from custom/Extension/modules/gc_InvoiceSubtotalGroup/Ext/Vardefs/gc_invoicesubtotalgroup_gc_line_item_contract_history_1_gc_InvoiceSubtotalGroup.php

// created: 2016-05-04 05:46:28
$dictionary["gc_InvoiceSubtotalGroup"]["fields"]["gc_invoicesubtotalgroup_gc_line_item_contract_history_1"] = array (
  'name' => 'gc_invoicesubtotalgroup_gc_line_item_contract_history_1',
  'type' => 'link',
  'relationship' => 'gc_invoicesubtotalgroup_gc_line_item_contract_history_1',
  'source' => 'non-db',
  'module' => 'gc_Line_Item_Contract_History',
  'bean_name' => 'gc_Line_Item_Contract_History',
  'side' => 'right',
  'vname' => 'LBL_GC_INVOICESUBTOTALGROUP_GC_LINE_ITEM_CONTRACT_HISTORY_1_FROM_GC_LINE_ITEM_CONTRACT_HISTORY_TITLE',
);


// Contract Module : gc_InvoiceSubtotalGroup
$dictionary['gc_InvoiceSubtotalGroup']['fields']['ndb_contract_details_tpl'] = array (
    'name' => 'ndb_contract_details_tpl',
    'vname' => 'LBL_NDB_CONTRACT_DETAILS_TPL',
    'type' => 'varchar',
    'module' => 'gc_InvoiceSubtotalGroup',
    'enforced' => '',
    'dependency' => '',
    'required' => false,
    'massupdate' => '0',
    'default_value' => '',
    'no_default' => false,
    'help' => '',
    'comment' => '',
    'importable' => false,
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => false,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'calculated' => false,
    'len' => '255',
    'max_size' => '100',
    'source' => 'non-db',
    'dbType' => 'non-db',
    'studio' => 'visible',
);
// Line Item Module : gc_InvoiceSubtotalGroup
$dictionary['gc_InvoiceSubtotalGroup']['fields']['ndb_line_item_details_tpl'] = array (
    'name' => 'ndb_line_item_details_tpl',
    'vname' => 'LBL_NDB_LINE_ITEM_DETAILS_TPL',
    'type' => 'varchar',
    'module' => 'gc_InvoiceSubtotalGroup',
    'enforced' => '',
    'dependency' => '',
    'required' => false,
    'massupdate' => '0',
    'default_value' => '',
    'no_default' => false,
    'help' => '',
    'comment' => '',
    'importable' => false,
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => false,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'calculated' => false,
    'len' => '255',
    'max_size' => '100',
    'source' => 'non-db',
    'dbType' => 'non-db',
    'studio' => 'visible',
);
?>
