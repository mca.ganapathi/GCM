<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
require_once ('custom/include/MVC/View/views/view.list.php');

/**
 * class Gc_InvoiceSubtotalGroupViewList
 */
class Gc_InvoiceSubtotalGroupViewList extends CustomViewList
{
    /**
     * Method display
     */
    function display()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'display');
        global $current_user;
        if (($current_user->is_admin != "1")) {
            $this->restrictViewAction();
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
            return;
        }
        parent::display(); // call parent display method
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method restrictViewAction
     */
    function restrictViewAction()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method restrictViewAction');
        // display error message
        $display_txt = '<br><div id="div_related_values"><div><span style="color:red;">Action Prohibited: You are restricted to view this Page.</span></h4></div><div><br>';
        echo $display_txt;
        echo $this->lv->display($this->showTitle);
        echo '<script>$("#SAVE_HEADER").hide(); $( ".action_buttons" ).last().css( "display", "none" ); $(".moduleTitle").hide(); $("#CANCEL_HEADER").val("Back");$("#EditView_tabs,#detailpanel_30,#btn_view_change_log").hide();</script>';
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}