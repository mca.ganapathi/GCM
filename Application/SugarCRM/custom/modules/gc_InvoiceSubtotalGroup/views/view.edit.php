<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

/**
 * class gc_InvoiceSubtotalGroupViewEdit
 */
class gc_InvoiceSubtotalGroupViewEdit extends ViewEdit
{

    /**
     * Method preDisplay 
     */
    function preDisplay()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'preDisplay');
        global $mod_strings;
        if (empty($this->bean->id)) {
            SugarApplication::appendErrorMessage($mod_strings['LBL_CREATE_ERROR']);
            $params = array(
                            'module' => 'gc_InvoiceSubtotalGroup', 
                            'action' => 'index'
            );
            SugarApplication::redirect('index.php?' . http_build_query($params));
        }
        parent::preDisplay(); // call parent preDisplay method
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}
