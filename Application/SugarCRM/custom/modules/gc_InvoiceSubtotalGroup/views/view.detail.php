<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

include_once ('custom/modules/gc_Contracts/include/ClassGcContracts.php');

/**
 * class gc_InvoiceSubtotalGroupViewDetail
 */
class gc_InvoiceSubtotalGroupViewDetail extends ViewDetail
{
    // extended module class object
    private $obj_gc_contracts;

    function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'constructor');
        $this->obj_gc_contracts = new ClassGcContracts();
        parent::__construct();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method display
     */
    function display()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'display');
        global $app_list_strings;
        
        $line_items = array();
        // to get contract details related to group.
        $contract_bean = $this->bean->get_linked_beans("gc_contracts_gc_invoicesubtotalgroup_1", "gc_Contract");
        $list_product_offerings = $this->obj_gc_contracts->getProductOfferings(false);
        $product_offerings = $list_product_offerings;
        
        if ($contract_bean != null) {
            $contract = $contract_bean[0];
            
            $contract_detail['invoice_group_id'] = $this->bean->id;
            
            $contract_detail['global_contract_id'] = $contract->global_contract_id;
            $contract_detail['contract_version'] = $contract->contract_version;
            $contract_detail['contract_status'] = $app_list_strings['contract_status_list'][$contract->contract_status];
            $contract_detail['contract_name'] = $contract->name;
            $contract_detail['contract_id'] = $contract->id;
            $contract_detail['contracting_company'] = $contract->accounts_gc_contracts_1_name;
            $contract_detail['company_id'] = $contract->accounts_gc_contracts_1accounts_ida;
            $contract_detail['product_offering_id'] = $contract->product_offering;
            if (!empty($contract->product_offering) && isset($product_offerings[$contract->product_offering])) {
                $contract_detail['product_offering'] = $product_offerings[$contract->product_offering];
            } else {
                $contract_detail['product_offering'] = "";
            }
            $contract_detail['date_entered'] = $contract->date_entered;
            $contract_detail['date_modified'] = $contract->date_modified;
            
            $off_details = $this->obj_gc_contracts->getProductOfferingDetails($contract_detail['product_offering_id']);
            $sp_details = $off_details['respose_array']['specification_details'];
        }
        // to get line items of group
        $line_item_his_bean_arr = $this->bean->get_linked_beans("gc_invoicesubtotalgroup_gc_line_item_contract_history_1", "gc_Line_Item_Contract_History");
        $line_item_bean = BeanFactory::getBean('gc_LineItem');
        if ($line_item_his_bean_arr != null) {
            foreach ($line_item_his_bean_arr as $line_item_header) {
                $line_items[$line_item_header->id]['service_start_date'] = $line_item_header->service_start_date;
                $line_items[$line_item_header->id]['service_end_date'] = $line_item_header->service_end_date;
                $prod_sp = $line_item_header->product_specification;
                $line_items[$line_item_header->id]['product_specification'] = $sp_details[$prod_sp]['specname'];
                $line_item_bean_arr = $line_item_bean->retrieve($line_item_header->line_item_id);
                $line_items[$line_item_header->id]['global_contract_item_id'] = $line_item_bean_arr->name;
                $line_items[$line_item_header->id]['lineitem_history_id'] = $line_item_header->id; // line_item_id
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'line_item_header->id : '.($line_item_header->id), 'to check Lineitem header id');
            }
        }
        /* Assigning contract details and line items list to tpl */
        $this->ss->assign('CONTRACT_DETAIL', $contract_detail);
        $this->ss->assign('LINE_ITEMS_LIST', $line_items);
        $this->ss->assign('CONTRACTDETAILSTPL', 'custom/modules/gc_InvoiceSubtotalGroup/tpl/ContractDetails.tpl');
        $this->ss->assign('LINEITEMSDETAILTPL', 'custom/modules/gc_InvoiceSubtotalGroup/tpl/LineItemsDetail.tpl');
        unset($off_details, $line_item_his_bean_arr, $line_item_bean, $contract_bean);
        unset($this->dv->defs['templateMeta']['form']['buttons'][1]); // Hide Duplicate button
        unset($this->dv->defs['templateMeta']['form']['buttons'][3]); // Hide Find Duplicate button
        $button_visible = "true";
        if ($contract->contract_status != "0" || $contract->workflow_flag != "0") { // If status is not Draft
            unset($this->dv->defs['templateMeta']['form']['buttons'][0]); // Hide Edit button
            unset($this->dv->defs['templateMeta']['form']['buttons'][2]); // Hide Delete button
            $button_visible = "false";
        }
        $this->ss->assign('BUTTON_VISIBLE', $button_visible);
        parent::display(); // call parent display method
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method _displaySubPanels Overridden to support subpanel customizations
     */
    function _displaySubPanels()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method _displaySubPanels Overridden to support subpanel customizations');
        require_once ('custom/include/SubPanel/SubPanelTiles.php');
        $subpanel = new CustomSubPanelTiles($this->bean, $this->module);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts']['top_buttons'][0]); // hiding create
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts']['top_buttons'][1]); // hiding select
        echo $subpanel->display();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}
