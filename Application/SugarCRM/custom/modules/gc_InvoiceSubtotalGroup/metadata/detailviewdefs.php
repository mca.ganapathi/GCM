<?php
$module_name = 'gc_InvoiceSubtotalGroup';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/modules/gc_InvoiceSubtotalGroup/include/js/DetailView.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'description',
        ),
      ),
      'lbl_detailview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'ndb_contract_details_tpl',
            'comment' => '',
            'studio' => 'visible',
            'customCode' => '{include file=$CONTRACTDETAILSTPL}',
            'hideLabel' => true,
          ),
        ),
      ),
      'lbl_detailview_panel2' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'ndb_line_item_details_tpl',
            'comment' => '',
            'studio' => 'visible',
            'label' => 'LBL_NDB_LINE_ITEM_DETAILS_TPL',
            'customCode' => '{include file=$LINEITEMSDETAILTPL}',
            'hideLabel' => true,
          ),
        ),
      ),
    ),
  ),
);
?>