<link rel="stylesheet" href="custom/modules/gc_Contracts/include/css/DetailView.css"/>
<div class="panel panel-default">
    <ul class="list-group" style="margin-left:0px">
        <li class="list-group-item" style="margin-left: 0px;">
            <div id="lidoc-0" >
            <div  id="subpanel_gc_lineitem" class="linedocsubpanel">
                <div id="list_subpanel_gc_lineitem" >
                <table class="list view" border="0" cellpadding="0" cellspacing="0" width="100%">
                <thead>
                    <tr role="presentation">
                        <td colspan="20" align="right" >
                        
                         {if ($BUTTON_VISIBLE == 'true')}
							<div class="action_buttons" style="padding: 6px">
                            <ul class="clickMenu">
                                <li class="sugar_action_button"> 
                                    <input type="button" value="Select" title="Select" class="button" 
                                    id="gc_invoicesubtotalgroup_gc_line_item_contract_history_1_select_button" 
                                    name="gc_invoicesubtotalgroup_gc_line_item_contract_history_1_select_button" 
                          onclick="open_popup('gc_LineItem', 600,400, '&conId={$CONTRACT_DETAIL.contract_id}&liIgId={$CONTRACT_DETAIL.invoice_group_id}', true,true,
                          {literal} {'call_back_function':'saveInvoiceGroupLink','form_name':'DetailView','field_to_name_array':{'id':'subpanel_id'},'passthru_data':{    'child_field':'gc_invoicesubtotalgroup_gc_line_item_contract_history_1', 'return_url':'', 
                          'link_field_name':'gc_invoicesubtotalgroup_gc_line_item_contract_history_1', 'module_name':'gc_invoicesubtotalgroup_gc_line_item_contract_history_1', 'refresh_page':true}
                          }, '',true);"    
                         {/literal}  />
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>
						{/if}
                        </td>
                    </tr>
                    <tr height="20">
                        
                        <th scope="col" width="10%"><span style="white-space:normal;">{sugar_translate label='LBL_LINE_ITEM' module='gc_InvoiceSubtotalGroup'}</span></th>
                        <th scope="col" width="15%"><span style="white-space:normal;">{sugar_translate label='LBL_PRODUCT_SPECIFICATION' module='gc_InvoiceSubtotalGroup'}</span></th>
                        <th scope="col" width="10%"><span style="white-space:normal;">{sugar_translate label='LBL_SERVICE_START_DATE' module='gc_InvoiceSubtotalGroup'}</span></th>
                        <th scope="col" width="10%"><span style="white-space:normal;">{sugar_translate label='LBL_SERVICE_END_DATE' module='gc_InvoiceSubtotalGroup'}</span></th> 
                        <th scope="col" width="30%"><span style="white-space:normal;">&nbsp;</span></th>
                       
                    </tr>
                </thead>
                    {if empty($LINE_ITEMS_LIST)}
                        <tbody id="li_list_items">
                            <tr class="evenListRowS1" height="20">
                                <td scope="row" valign="top" colspan=10 style="text-align:center"> No data</td>
                            </tr>
                        </tbody>
                    {else}
                    {assign var=rowcss value='even'}
                    {assign var=row value=0}
                     <tbody id="li_list_items">
                    {foreach from=$LINE_ITEMS_LIST key=k item=v}
                        {if $rowcss == 'even'}
                            {assign var=rowcss value='odd'}
                        {else}
                            {assign var=rowcss value='even'}
                        {/if}
                        {assign var=row value=$row++}
                   
                    <tr class="{$rowcss}ListRowS1" height="20">
					   
                        <td scope="row" valign="top">{$v.global_contract_item_id}</td>
                        <td scope="row" valign="top">{$v.product_specification}</td>
                        <td scope="row" valign="top">{$v.service_start_date}</td>
                        <td scope="row" valign="top">{$v.service_end_date}</td>
                        <td scope="row" valign="top" style="padding:3px;text-align:right;">
                           {if ($BUTTON_VISIBLE == 'true')}  <ul class="clickMenu ">
                                <li class="sugar_action_button"><a href="javascript:void(0)" onclick="removeInvoiceGroupLink('{$v.lineitem_history_id}' , '{$CONTRACT_DETAIL.invoice_group_id}')">Remove</a></li>
                            </ul>
                            {/if}
                        </td>
                    </tr> 
                    
                    {/foreach}
                    </tbody>
                    {/if}
                </tbody>
                </table>
            </div>
            </div>
            </div>
        </li>                     								 
    </ul>
</div>