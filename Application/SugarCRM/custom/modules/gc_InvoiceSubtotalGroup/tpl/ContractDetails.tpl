{*
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

*}
<link rel="stylesheet" href="custom/include/assests/css/bootstrap.min.css" />
<link rel="stylesheet" href="custom/include/assests/css/CustomCSS.css" />
<link rel="stylesheet" href="custom/modules/gc_Contracts/include/css/DetailView.css"/>

{if empty($CONTRACT_DETAIL)}
     {sugar_translate label='LBL_NO_RECORD_FOUND' module='gc_InvoiceSubtotalGroup'}
{else}	
<input type="hidden" value="{$CONTRACT_DETAIL.invoice_group_id}" name="group_record_id" id="group_record_id" >
<input type="hidden" value="{$CONTRACT_DETAIL.contract_id}" name="cotract_record_id" id="cotract_record_id" >   
	   <div class="panel panel-default">
		  <ul class="list-group" style="margin-left:0px">		  
				<li class="list-group-item" style="margin-left: 0px;">			  
				   <hr style="margin-top: 12px;border:0px">
				   </hr>
				   <div class="method">
					  <div class="row margin-0">				 
						 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell" >
							   <span class="cellfont">{sugar_translate label='LBL_GLOBAL_CONTRACT' module='gc_InvoiceSubtotalGroup'}</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right add-cell-border-top">
							<div class="cell">								
							   <span class="cellfont">{$CONTRACT_DETAIL.global_contract_id}</span>							
							</div>
						 </div>
						 <div class="col-md-3 cellgrey add-cell-border-right add-cell-border-top">
							<div class="cell">
							   <span class="cellfont">{sugar_translate label='LBL_VERSION' module='gc_InvoiceSubtotalGroup'}</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right add-cell-border-top">
							<div class="cell">		
                              <span class="cellfont">{$CONTRACT_DETAIL.contract_version}</span></div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">{sugar_translate label='LBL_STATUS' module='gc_InvoiceSubtotalGroup'}</span> 
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">								
							  	<span class="cellfont">{$CONTRACT_DETAIL.contract_status}</span>						
							</div>
						 </div>
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">								
							  	<span class="cellfont">{sugar_translate label='LBL_CONTRACT_NAME' module='gc_InvoiceSubtotalGroup'}</span>				   
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">				
								 <span class="cellfont"><a href="?module=gc_Contracts&action=DetailView&record={$CONTRACT_DETAIL.contract_id}">{$CONTRACT_DETAIL.contract_name}</a></span>					   
							</div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">{sugar_translate label='LBL_CONTRACTING_COMPANY' module='gc_InvoiceSubtotalGroup'}</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">								
								<span class="cellfont"><a href="?module=Accounts&action=DetailView&record={$CONTRACT_DETAIL.company_id}">{$CONTRACT_DETAIL.contracting_company}</a></span>								
							</div>
						 </div>
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">								
							   <span class="cellfont">{sugar_translate label='LBL_PRODUCT_OFFERING' module='gc_InvoiceSubtotalGroup'}</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">							
							   <span class="cellfont">{$CONTRACT_DETAIL.product_offering}</span>							  
							</div>
						 </div>
					  </div>
					  <div class="row margin-0">
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">
							   <span class="cellfont">{sugar_translate label='LBL_DATE_ENTERED' module='gc_InvoiceSubtotalGroup'}</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">								
								<span class="cellfont">{$CONTRACT_DETAIL.date_entered}</span>								
							</div>
						 </div>
						 <div class="col-md-3 cellgrey add-cell-border-right">
							<div class="cell">								
							   <span class="cellfont">{sugar_translate label='LBL_DATE_MODIFIED' module='gc_InvoiceSubtotalGroup'}</span>
							</div>
						 </div>
						 <div class="col-md-3 add-cell-border-right">
							<div class="cell">							
								 <span class="cellfont">{$CONTRACT_DETAIL.date_modified}</span>							  
							</div>
						 </div>
					  </div>
				   </div>
				
			 </li>				  	  
			  
		  </ul>
	   </div>	
{/if}