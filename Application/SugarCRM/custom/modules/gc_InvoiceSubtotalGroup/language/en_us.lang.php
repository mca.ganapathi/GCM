<?php
// created: 2016-06-21 06:23:01
$mod_strings = array (
  'LBL_DETAILVIEW_PANEL1' => 'Contract Overview',
  'LBL_DETAILVIEW_PANEL2' => 'Contract Line Items',
  'LBL_GLOBAL_CONTRACT' => 'Global Contract ID',
  'LBL_VERSION' => 'Version',
  'LBL_CONTRACT_NAME' => 'Name',
  'LBL_CONTRACTING_COMPANY' => 'Contracting Company',
  'LBL_PRODUCT_OFFERING' => 'Product Offering',
  'LBL_LINE_ITEM' => 'Global Contract Item ID',
  'LBL_PRODUCT_SPECIFICATION' => 'Product Specification',
  'LBL_SERVICE_START_DATE' => 'Service Start Date',
  'LBL_SERVICE_END_DATE' => 'Service End Date',
  'LBL_GC_CONTRACTS_GC_INVOICESUBTOTALGROUP_1_FROM_GC_CONTRACTS_TITLE' => 'GCM Contracts',
  'LBL_CREATE_ERROR' => 'Restriction on Invoice Subtotal Group creation. Please contact system administrator.',
);