/** 
    @desc : Functions related to customize Detail view
    @file  : custom/modules/gc_InvoiceSubtotalGroup/include/js/DetailView.js 
    @Author : Arunsakthivel.S
*/

function saveInvoiceGroupLink(data){
    var liId = data.name_to_value_array.subpanel_id;
    var igId = $("#group_record_id").val();
    var ciId = $("#cotract_record_id").val();
    SUGAR.ajaxUI.showLoadingPanel();
    $.ajax({
        type: "POST",
        url: "index.php?module=gc_Contracts&action=Actions&method=setInvoiceGroupLineItemRel",
        data : {"invoice_group_id" : igId, "line_item_id" : liId, "contract_id" : ciId, 'sugar_body_only' : 1},
        success: function(result){
            window.location.reload();
        }
    });
    SUGAR.ajaxUI.hideLoadingPanel();
}

function removeInvoiceGroupLink(lihId, igId){
    SUGAR.ajaxUI.showLoadingPanel();
    $.ajax({
        type: "POST",
        url: "index.php?module=gc_Contracts&action=Actions&method=unsetInvoiceGroupLineItemRel",
        data : {"invoice_group_id" :igId , "line_item_history_id" : lihId , 'sugar_body_only' : 1},
        success: function(result){
            window.location.reload();
        }
    });
    SUGAR.ajaxUI.hideLoadingPanel();

}


