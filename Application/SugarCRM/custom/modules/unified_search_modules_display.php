<?php
// created: 2016-06-06 11:50:13
$unified_search_modules_display = array (
  'Accounts' => 
  array (
    'visible' => true,
  ),
  'Bugs' => 
  array (
    'visible' => false,
  ),
  'Calls' => 
  array (
    'visible' => true,
  ),
  'Campaigns' => 
  array (
    'visible' => false,
  ),
  'Cases' => 
  array (
    'visible' => true,
  ),
  'Contacts' => 
  array (
    'visible' => true,
  ),
  'Contracts' => 
  array (
    'visible' => false,
  ),
  'Documents' => 
  array (
    'visible' => true,
  ),
  'KBDocuments' => 
  array (
    'visible' => false,
  ),
  'Leads' => 
  array (
    'visible' => true,
  ),
  'Meetings' => 
  array (
    'visible' => true,
  ),
  'Notes' => 
  array (
    'visible' => true,
  ),
  'Opportunities' => 
  array (
    'visible' => true,
  ),
  'Project' => 
  array (
    'visible' => false,
  ),
  'ProjectTask' => 
  array (
    'visible' => false,
  ),
  'ProspectLists' => 
  array (
    'visible' => false,
  ),
  'Prospects' => 
  array (
    'visible' => false,
  ),
  'Quotes' => 
  array (
    'visible' => false,
  ),
  'Tasks' => 
  array (
    'visible' => false,
  ),
  'bp_Beluga_Product_Code' => 
  array (
    'visible' => false,
  ),
  'gc_CreditCard' => 
  array (
    'visible' => false,
  ),
  'gc_DirectDeposit' => 
  array (
    'visible' => false,
  ),
  'gc_Line_Item_Config_Summary' => 
  array (
    'visible' => false,
  ),
  'gc_Line_Item_Contract_History' => 
  array (
    'visible' => false,
  ),
  'js_Accounts_js' => 
  array (
    'visible' => false,
  ),
  'pi_product_item_attribute' => 
  array (
    'visible' => false,
  ),
);