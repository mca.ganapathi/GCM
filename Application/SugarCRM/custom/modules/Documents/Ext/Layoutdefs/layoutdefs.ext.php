<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Layoutdefs/removeActionButton.php

/*** START : Remove unnecessary subpanels that we can't hide from Admin Panel ***/


unset($layout_defs["Documents"]["subpanel_setup"]['contracts']);    // Hide Contract subpanel
unset($layout_defs["Documents"]["subpanel_setup"]['accounts']);     // Hide accounts subpanel
unset($layout_defs["Documents"]["subpanel_setup"]['contacts']);     // Hide contacts subpanel


?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Layoutdefs/_overrideDocument_subpanel_therevisions.php

//auto-generated file DO NOT EDIT
$layout_defs['Documents']['subpanel_setup']['therevisions']['override_subpanel_name'] = 'Document_subpanel_therevisions';

?>
