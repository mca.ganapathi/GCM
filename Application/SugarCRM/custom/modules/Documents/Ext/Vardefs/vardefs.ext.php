<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['Document']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/gc_contracts_documents_1_Documents.php

// created: 2016-01-07 11:52:10
$dictionary["Document"]["fields"]["gc_contracts_documents_1"] = array (
  'name' => 'gc_contracts_documents_1',
  'type' => 'link',
  'relationship' => 'gc_contracts_documents_1',
  'source' => 'non-db',
  'module' => 'gc_Contracts',
  'bean_name' => 'gc_Contracts',
  'vname' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_GC_CONTRACTS_TITLE',
  'id_name' => 'gc_contracts_documents_1gc_contracts_ida',
);
$dictionary["Document"]["fields"]["gc_contracts_documents_1_name"]=array (
  'name' => 'gc_contracts_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_GC_CONTRACTS_TITLE',
  'save' => true,
  'id_name' => 'gc_contracts_documents_1gc_contracts_ida',
  'link' => 'gc_contracts_documents_1',
  'table' => 'gc_contracts',
  'module' => 'gc_Contracts',
  'rname' => 'name',
);

$dictionary["Document"]["fields"]["gc_contracts_documents_1gc_contracts_ida"]=array (
  'name' => 'gc_contracts_documents_1gc_contracts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'id_name' => 'gc_contracts_documents_1gc_contracts_ida',
  'link' => 'gc_contracts_documents_1',
  'table' => 'gc_contracts',
  'module' => 'gc_Contracts',
  'rname' => 'id',
  'reportable' => false,
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
$dictionary["Document"]["fields"]["gc_contracts_documents_1_right"]=array (
  'name' => 'gc_contracts_documents_1_right',
  'type' => 'link',
  'relationship' => 'gc_contracts_documents_1',
  'source' => 'non-db',
  'vname' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'id_name' => '_idb',
  'side' => 'right',
  'link-type' => 'many',
);


?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/gc_lineitem_documents_1_Documents.php

// created: 2016-01-08 05:22:36
$dictionary["Document"]["fields"]["gc_lineitem_documents_1"] = array (
  'name' => 'gc_lineitem_documents_1',
  'type' => 'link',
  'relationship' => 'gc_lineitem_documents_1',
  'source' => 'non-db',
  'module' => 'gc_LineItem',
  'bean_name' => 'gc_LineItem',
  'vname' => 'LBL_GC_LINEITEM_DOCUMENTS_1_FROM_GC_LINEITEM_TITLE',
  'id_name' => 'gc_lineitem_documents_1gc_lineitem_ida',
);
$dictionary["Document"]["fields"]["gc_lineitem_documents_1_name"]=array (
  'name' => 'gc_lineitem_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_GC_LINEITEM_DOCUMENTS_1_FROM_GC_LINEITEM_TITLE',
  'save' => true,
  'id_name' => 'gc_lineitem_documents_1gc_lineitem_ida',
  'link' => 'gc_lineitem_documents_1',
  'table' => 'gc_lineitem',
  'module' => 'gc_LineItem',
  'rname' => 'name',
);

$dictionary["Document"]["fields"]["gc_lineitem_documents_1gc_lineitem_ida"]=array (
  'name' => 'gc_lineitem_documents_1gc_lineitem_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_GC_LINEITEM_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'id_name' => 'gc_lineitem_documents_1gc_lineitem_ida',
  'link' => 'gc_lineitem_documents_1',
  'table' => 'gc_lineitem',
  'module' => 'gc_LineItem',
  'rname' => 'id',
  'reportable' => false,
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
$dictionary["Document"]["fields"]["gc_lineitem_documents_1_right"]=array (
  'name' => 'gc_lineitem_documents_1_right',
  'type' => 'link',
  'relationship' => 'gc_lineitem_documents_1',
  'source' => 'non-db',
  'vname' => 'LBL_GC_LINEITEM_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'id_name' => '_idb',
  'side' => 'right',
  'link-type' => 'many',
);


?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_category_id.php

 // created: 2016-09-29 04:48:38
$dictionary['Document']['fields']['category_id']['audited'] = true;
$dictionary['Document']['fields']['category_id']['merge_filter'] = 'disabled';
$dictionary['Document']['fields']['category_id']['calculated'] = false;
$dictionary['Document']['fields']['category_id']['dependency'] = false;
$dictionary['Document']['fields']['category_id']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_description.php

 // created: 2016-09-29 04:48:38
$dictionary['Document']['fields']['description']['audited'] = true;
$dictionary['Document']['fields']['description']['comments'] = 'Full text of the note';
$dictionary['Document']['fields']['description']['merge_filter'] = 'disabled';
$dictionary['Document']['fields']['description']['calculated'] = false;
$dictionary['Document']['fields']['description']['full_text_search']['enabled'] = true;
$dictionary['Document']['fields']['description']['full_text_search']['searchable'] = true;
$dictionary['Document']['fields']['description']['full_text_search']['boost'] = 0.60999999999999998667732370449812151491641998291015625;


?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_document_name.php

 // created: 2016-09-29 04:48:38
$dictionary['Document']['fields']['document_name']['audited'] = true;
$dictionary['Document']['fields']['document_name']['merge_filter'] = 'disabled';
$dictionary['Document']['fields']['document_name']['full_text_search']['enabled'] = true;
$dictionary['Document']['fields']['document_name']['full_text_search']['searchable'] = true;
$dictionary['Document']['fields']['document_name']['full_text_search']['boost'] = 0.81999999999999995115018691649311222136020660400390625;
$dictionary['Document']['fields']['document_name']['calculated'] = false;


?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_template_type.php

 // created: 2016-09-29 04:48:38
$dictionary['Document']['fields']['template_type']['audited'] = true;
$dictionary['Document']['fields']['template_type']['merge_filter'] = 'disabled';
$dictionary['Document']['fields']['template_type']['calculated'] = false;
$dictionary['Document']['fields']['template_type']['dependency'] = false;
$dictionary['Document']['fields']['template_type']['full_text_search']['boost'] = 1;


?>
