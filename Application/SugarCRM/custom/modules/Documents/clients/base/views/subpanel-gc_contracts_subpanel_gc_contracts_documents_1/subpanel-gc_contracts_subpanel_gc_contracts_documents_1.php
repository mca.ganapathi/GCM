<?php
// created: 2016-09-29 04:41:43
$viewdefs['Documents']['base']['view']['subpanel-gc_contracts_subpanel_gc_contracts_documents_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'document_name',
          'default' => true,
          'label' => 'LBL_LIST_DOCUMENT_NAME',
          'enabled' => true,
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'filename',
          'sortable' => false,
          'default' => true,
          'label' => 'LBL_LIST_FILENAME',
          'enabled' => true,
          'type' => 'file',
        ),
        2 => 
        array (
          'type' => 'enum',
          'default' => true,
          'label' => 'LBL_TEMPLATE_TYPE',
          'enabled' => true,
          'name' => 'template_type',
        ),
        3 => 
        array (
          'name' => 'category_id',
          'default' => true,
          'label' => 'LBL_LIST_CATEGORY',
          'enabled' => true,
          'type' => 'enum',
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);