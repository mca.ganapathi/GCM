<?php
// created: 2018-03-07 11:43:34
$viewdefs['Documents']['EditView'] = array (
  'templateMeta' => 
  array (
    'form' => 
    array (
      'enctype' => 'multipart/form-data',
      'hidden' => 
      array (
        0 => '<input type="hidden" name="old_id" value="{$fields.document_revision_id.value}">',
        1 => '<input type="hidden" name="contract_id" value="{$smarty.request.contract_id}">',
      ),
    ),
    'maxColumns' => '2',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
      1 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'javascript' => '{sugar_getscript file="include/javascript/popup_parent_helper.js"}
{sugar_getscript file="modules/Documents/documents.js"}',
    'includes' => 
    array (
      0 => 
      array (
        'file' => 'cache/include/javascript/sugar_grp_yui_widgets.js',
      ),
    ),
    'useTabs' => false,
    'tabDefs' => 
    array (
      'LBL_DOCUMENT_INFORMATION' => 
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
      ),
    ),
  ),
  'panels' => 
  array (
    'lbl_document_information' => 
    array (
      0 => 
      array (
        0 => 
        array (
          'name' => 'filename',
          'displayParams' => 
          array (
            'onchangeSetFileNameTo' => 'document_name',
          ),
        ),
      ),
      1 => 
      array (
        0 => 'document_name',
      ),
      2 => 
      array (
        0 => 
        array (
          'name' => 'revision',
          'customCode' => '<input name="revision" type="text" readonly value="{$fields.revision.value}" {$DISABLED}>',
          'type' => 'readonly',
        ),
      ),
      3 => 
      array (
        0 => 'category_id',
      ),
      4 => 
      array (
        0 => 
        array (
          'name' => 'template_type',
          'label' => 'LBL_DET_TEMPLATE_TYPE',
        ),
      ),
      5 => 
      array (
        0 => 
        array (
          'name' => 'description',
        ),
      ),
      6 => 
      array (
        0 => 
        array (
          'name' => 'gc_contracts_documents_1_name',
          'label' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_GC_CONTRACTS_TITLE',
        ),
        1 => 
        array (
          'name' => 'gc_lineitem_documents_1_name',
        ),
      ),
    ),
  ),
);