<?php
$hook_version = 1;
$hook_array = Array();

$hook_array['after_relationship_add'] = Array();
$hook_array['after_relationship_add'][] = Array(
    1,
    'Set changes log after adding documents for contract and contract lineitem',
    'custom/modules/Documents/include/ClassAfterRelationshipSave.php',
    'ClassAfterRelationshipSave',
    'saveDocumentsSubPanel'
);
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(
    1,
    'Set global team to contract and line item documents',
    'custom/modules/Documents/include/ClassBeforeSave.php',
    'ClassBeforeSave',
    'setTeamGlobal'
    );
?>
