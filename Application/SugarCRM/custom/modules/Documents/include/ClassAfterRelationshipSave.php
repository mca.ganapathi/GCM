<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * class ClassAfterRelationshipSave
 */
class ClassAfterRelationshipSave
{

    /**
     * method saveDocumentsSubPanel to Save contract document record.
     * 
     * @param object $bean record sugar object
     * @param object $event hook event
     * @param array $arguments required parameters passed by framework
     * @author debasish.gupta
     * @since May 25, 2016
     */
    public function saveDocumentsSubPanel($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'method to Save contract document record');
        if ($arguments['related_module'] == 'gc_Contracts') {
            $contract_id = $arguments['related_id'];
            $document_name = $bean->document_name;
            $contract_bean = new gc_Contracts();
            $contract_bean->retrieve($contract_id);
            $this->insertContractAudit($contract_bean, 'Contract Document', 'relate', '', $document_name);
        } elseif ($arguments['related_module'] == 'gc_LineItem') {
            $lineitem_id = $arguments['related_id'];
            $document_name = $bean->document_name;
            $lineitem_bean = new gc_LineItem();
            $lineitem_bean->retrieve($lineitem_id);
            $this->insertContractAudit($lineitem_bean, 'Contract Lineitem Document', 'relate', '', $document_name);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * method insertContractAudit to audit Contract Document and Lineitem Document Audit record.
     * 
     * @param object $bean - record sugar object [Sugar default]
     * @param string $field_name field name to audit
     * @param string $data_type date type of field
     * @param string $before value before update
     * @param string $after value after update
     * @author debasish.gupta
     * @since : May 26, 2016
     */
    public function insertContractAudit($bean, $field_name, $data_type, $before, $after)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'field_name : '.$field_name.GCM_GL_LOG_VAR_SEPARATOR.'data_type : '.$data_type.GCM_GL_LOG_VAR_SEPARATOR.'before : '.$before.GCM_GL_LOG_VAR_SEPARATOR.'after : '.$after, 'to audit Contract Document and Lineitem Document Audit record');
        $audit_entry = array();
        $audit_entry['field_name'] = $field_name;
        $audit_entry['data_type'] = $data_type;
        $audit_entry['before'] = $before;
        $audit_entry['after'] = $after;
        $bean->db->save_audit_records($bean, $audit_entry);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}

?>