<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * class ClassBeforeSave
 */
class ClassBeforeSave
{

    /**
     * method setTeamGlobal to Set document team to global.
     * 
     * @param object $bean  record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author Kamal.Thiyagarajan
     * @since May 31, 2016
     *         
     */
    public function setTeamGlobal($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Set document team to global');
        // For contract document or line item document set team as global.
        if (!empty($bean->gc_contracts_documents_1gc_contracts_ida) || !empty($bean->gc_lineitem_documents_1gc_lineitem_ida)) {
            $bean->team_id = 1;
            $bean->team_set_id = 1;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}

?>