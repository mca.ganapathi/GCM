<?php
// created: 2016-06-21 06:22:56
$mod_strings = array (
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Corporates',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Accounts',
  'LBL_NAME' => 'Document Name',
  'LBL_SF_CATEGORY' => 'Category',
  'LBL_TEMPLATE_TYPE' => 'Document Type',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DOC_REV_HEADER' => 'Document Revisions',
  'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_GC_CONTRACTS_TITLE' => 'GCM Contracts',
);