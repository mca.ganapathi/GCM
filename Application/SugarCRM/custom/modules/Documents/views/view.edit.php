<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ("MSA"), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

/**
 * class DocumentsViewEdit
 */
class DocumentsViewEdit extends ViewEdit
{

    /**
     * Method display
     */
    public function display()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'display');
        $documentId = ((!empty($_REQUEST['record']))?trim($_REQUEST['record']):'');
        $liId = ((!empty($_REQUEST['liId']))?trim($_REQUEST['liId']):'');
        $cnId = ((!empty($_REQUEST['CnId']))?trim($_REQUEST['CnId']):'');
        $docTypFlg = ((!empty($_REQUEST['docTypFlg']))?trim($_REQUEST['docTypFlg']):'');
        
        parent::display(); // call parent display method
        
        $max_file_size = $GLOBALS['sugar_config']['upload_maxsize'];
        $max_file_size_str = ($max_file_size / (1000 * 1000)) . 'MB';
        
        if (!empty($liId)) {
            $line_item = new gc_LineItem();
            $line_item->retrieve($liId);
            $liId = $line_item->id;
            $liName = $line_item->name;
            
            echo '<script>				
                    $( document ).ready(function() {	
                        $("#dcmenu,#footer,#CANCEL_HEADER,#CANCEL_FOOTER,#gc_contracts_documents_1_name_label, #btn_clr_gc_lineitem_documents_1_name, #btn_gc_lineitem_documents_1_name, #btn_clr_gc_contracts_documents_1_name, #btn_gc_contracts_documents_1_name, #gc_lineitem_documents_1_name_label, #gc_lineitem_documents_1_name, #gc_contracts_documents_1_name").hide(); 
                        $("#gc_lineitem_documents_1_name").val(\'' . $liName . '\');
                        $("#gc_lineitem_documents_1_name").prop("readonly", true);
                        $("#gc_lineitem_documents_1gc_lineitem_ida").val(\'' . $liId . '\');
                    });	
                    
                    $("#filename_file").bind("change", function() {
                        if(  this.files[0].size > ' . $max_file_size . '){
                            YAHOO.SUGAR.MessageBox.show({
                                msg : "Attachment size exceeds the allowable limit (' . $max_file_size_str . ')",
                                title : "File upload error",
                                type : "alert"
                            });
                            $("#filename_file").val("");
                            $("#document_name").val("");
                        }
                    });         
                    </script>';
        }
        
        if (!empty($cnId)) {
            $contract = new gc_Contracts();
            
            $contract->retrieve($cnId);
            $cnId = $contract->id;
            $cnName = $contract->name;
            echo '<script>				
                    $( document ).ready(function() {	
                        $("#dcmenu,#footer,#CANCEL_HEADER,#CANCEL_FOOTER,#gc_contracts_documents_1_name_label, #btn_clr_gc_lineitem_documents_1_name, #btn_gc_lineitem_documents_1_name, #btn_clr_gc_contracts_documents_1_name, #btn_gc_contracts_documents_1_name, #gc_lineitem_documents_1_name_label, #gc_lineitem_documents_1_name, #gc_contracts_documents_1_name").hide(); 
                        $("#gc_contracts_documents_1_name").val(\'' . $cnName . '\');
                        $("#gc_contracts_documents_1_name").prop("readonly", true);
                        $("#gc_contracts_documents_1gc_contracts_ida").val(\'' . $cnId . '\');
                    });
                    
                    $("#filename_file").bind("change", function() {
                        if(  this.files[0].size > ' . $max_file_size . '){
                            YAHOO.SUGAR.MessageBox.show({
                                msg : "Attachment size exceeds the allowable limit (' . $max_file_size_str . ')",
                                title : "File upload error",
                                type : "alert"
                            });
                            $("#filename_file").val("");
                            $("#document_name").val("");
                        }
                    });       
                    
            </script>';
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}
