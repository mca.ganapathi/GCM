<?php
if (!defined('sugarEntry') || !sugarEntry) {
	die('Not A Valid Entry Point');
}
require_once('custom/modules/Administration/DBActions.php');
class FetchAPILog
{
    function getAPILogDetails()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'constructor handling fetching api log by search parameter');
        $result_array = array();
        $output = array();
        $draw = isset ($_REQUEST['draw']) ? $_REQUEST['draw'] : "";
        if (!empty(trim($_REQUEST['http_status'])) && !is_numeric($_REQUEST['http_status'])) {
        	$result_array["draw"] = $draw;
        	$result_array["recordsTotal"] =  0;
        	$result_array["recordsFiltered"] = 0;
        	$result_array["data"] = $output;
        	echo json_encode($result_array);
        	exit;
        }
        $select_columns = array('seq', 'method', 'path', 'http_status', 'request_datetime', 'response_datetime', 'request_body', 'response_body');
		$order_column = (isset ($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['column'] >= 0) ? $select_columns[$_REQUEST['order'][0]['column']] : "";
		$order_dir = isset ($_REQUEST['order'][0]['dir']) ? $_REQUEST['order'][0]['dir'] : "ASC";
		$offset = isset ($_REQUEST['start']) ? (int)$_REQUEST['start'] : 0;
		$limit = isset ($_REQUEST['length']) ? (int)$_REQUEST['length'] : 5;
		$interface = isset ($_REQUEST['interface']) ? $_REQUEST['interface'] : "";
		$method_type = isset ($_REQUEST['method_type']) ? $_REQUEST['method_type'] : "";
		$http_status = isset ($_REQUEST['http_status']) ? (int)$_REQUEST['http_status'] : "";
		$from_date = isset ($_REQUEST['from_date']) ? $_REQUEST['from_date'] : "";
		$from_date_hours = isset ($_REQUEST['from_date_hours']) ? $_REQUEST['from_date_hours'] : "00";
		$from_date_minutes = isset ($_REQUEST['from_date_minutes']) ? $_REQUEST['from_date_minutes'] : "00";
		$from_date_seconds = isset ($_REQUEST['from_date_seconds']) ? $_REQUEST['from_date_seconds'] : "00";
		$to_date = isset ($_REQUEST['to_date']) ? $_REQUEST['to_date'] : "";
		$to_date_hours = isset ($_REQUEST['to_date_hours']) ? $_REQUEST['to_date_hours'] : "00";
		$to_date_minutes = isset ($_REQUEST['to_date_minutes']) ? $_REQUEST['to_date_minutes'] : "00";
		$to_date_seconds = isset ($_REQUEST['to_date_seconds']) ? $_REQUEST['to_date_seconds'] : "00";
		$requestDateFrom = "";
		$requestDateTo = "";
		if (!empty($from_date)) {
			$requestDateFrom = $from_date." ".$from_date_hours.":".$from_date_minutes.":".$from_date_seconds;
		}
		if (!empty($to_date)) {
			$requestDateTo = $to_date." ".$to_date_hours.":".$to_date_minutes.":".$to_date_seconds;
		}
		$dbactions = new DBActions();
		$logTables = $dbactions->getTableList();
		$key = array_search($interface, $logTables);
		$results = $dbactions->getApiAccessLog($interface, $method_type, $http_status, $requestDateFrom, $requestDateTo, $offset, $limit, $order_column, $order_dir);
		
		$totalRecord = $dbactions->totalRecord($interface, $method_type, $http_status, $requestDateFrom, $requestDateTo);
		
		if (is_array($results)) {
			$result_array["draw"] = $draw;
            $result_array["recordsTotal"] = isset($totalRecord) ? (int)$totalRecord : 0;
            $result_array["recordsFiltered"] = isset($totalRecord) ? (int)$totalRecord : 0;
           
            $i = 0;
            foreach ($results as $result) {
            	$output[$i] = $result;
            	if (!empty($output[$i][6])) {
            	  $output[$i][6] = "<a href='index.php?module=gc_Contracts&action=requestBodyXML&seq=".$result[0]."&interface=".$logTables[$key]."'>".htmlentities(substr($result[6], 0, 50))."</a>";
            	}
            	$pos = strpos($result[7], '</exp:error-message>');
            	if ($pos !== false) {
            		$len = $pos+(strlen("</exp:error-message>"));
            		$output[$i][7] = "<a href='index.php?module=gc_Contracts&action=responseBodyXML&seq=".$result[0]."&interface=".$logTables[$key]."'>".htmlentities(substr($result[7], 0, $len))."</a>";
            	} else {
            		if (!empty($output[$i][7])) {
            			$output[$i][7] = "<a href='index.php?module=gc_Contracts&action=responseBodyXML&seq=".$result[0]."&interface=".$logTables[$key]."'>".htmlentities(substr($result[7], 0, 50))."</a>";
            		}
            	}
            	
            	$i++;
            }
			$result_array["data"] = $output;
			echo json_encode($result_array);
		} 
	}
}

$object = new FetchAPILog();
if (isset($_REQUEST['method'])) {
	switch (strtolower($_REQUEST['method'])) {
		case 'getapilogdetails':
			$object->getAPILogDetails();
			break;
	}
}
?>