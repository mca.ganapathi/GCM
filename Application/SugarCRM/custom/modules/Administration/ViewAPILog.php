<?php
global $sugar_config;
$col_limit = !empty($sugar_config['gcm_cstm_const']['view_api_log']['row_limit']) ? $sugar_config['gcm_cstm_const']['view_api_log']['row_limit'] : '5';
$logTables = array();
require_once('custom/modules/Administration/DBActions.php');
$dbactions = new DBActions();
$logTables = $dbactions->getTableList();
?>
<style type="text/css">
	th.sorting:after {
		content: url('themes/default/images/arrow.gif');
	}
	th.sorting_asc:after {
		content: url('themes/default/images/arrow_up.gif');
	}
	th.sorting_desc:after {
		content: url('themes/default/images/arrow_down.gif');
	}
	@-webkit-keyframes spin {
	  0% { -webkit-transform: rotate(0deg); }
	  100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}
	.listViewBody {
		margin-top: 20px;
	}
	.dataTables_length, .dataTables_info, .dataTables_paginate{
    	background: #FAFAFA none repeat scroll 0 0;
	    color: #666666;
	    padding: 4px 0px 4px 10px !important;
	    background-color: white;
	}
	.dataTables_length {
	    border-right: 1px solid #afafae;
		border-left: 1px solid #afafae;
	}
	.footer-tool, .header-tool {
		position: relative;
		width: 99.85%;
		height: 35px;
		border-bottom: 1px solid #afafae;
	    border-right: 1px solid #afafae;
		border-left: 1px solid #afafae;
		border-top: 1px solid #afafae;
	}
	.dataTables_info, .dataTables_paginate{
		position: absolute;
		width: 50%;
		top: 7px;
	}
	.dataTables_paginate{
		position: absolute;
		text-align: right;
		left: 50%;
		margin-left: -20px;
		top: 3px;
	}
	table.dataTable th, a.paginate_button:not(.disabled){
		cursor: pointer;
	}
	table.dataTable th:hover{
		font-weight: bold;
    	text-decoration: underline;
	}
	td.dataTables_empty {
		text-align: center;
	}
	#div_from_date .validation-message {
		position: absolute;
    	top: 30px;
	}
	
	.search_form {
	   padding-bottom: 15px;
	}
	.error{
	   color:#FF0000;
	}
</style>
<div class="clear"></div>

<div class="listViewBody">
<?php if(empty($logTables)){?>
<div class="error">DB host is not reachable. Please contact Infra Team.</div></div>
<?php }else{?>
	<form name="search_form" id="search_form" class="search_form" method="post" action="index.php?module=Administration&amp;action=ViewAPILog" onsubmit="var _form = document.getElementById('search_form'); if(check_form('search_form'))return true;return false;">
		<input type="hidden" name="csrf_token" value="wr_WOYaBEC8QqMck6MjzRn0-zndhNwD_">
		<div class="edit view">
			<script>
				$(function() {
					var $dialog = $('<div></div>')
						.html(SUGAR.language.get('app_strings', 'LBL_SEARCH_HELP_TEXT'))
						.dialog({
							autoOpen: false,
							title: SUGAR.language.get('app_strings', 'LBL_SEARCH_HELP_TITLE'),
							width: 700
						});

						$('.help-search').click(function() {
						$dialog.dialog('open');
						// prevent the default action, e.g., following a link
					});
				});
			</script>
			<table width="100%" cellspacing="0" cellpadding="0" border="0" width="100%" class="yui3-skin-sam edit view panelContainer">
				<tbody>
					<tr>
						<td scope="col" width="8%" valign="top">
							Interface
							<span class="required">*</span>
						</td>
						<td width="30%" valign="top">
								<select id="interface" name="interface" >
                                    <?php foreach($logTables as $logTable) {?>
                                             <option value="<?php echo $logTable?>" <?php if(!empty($_POST['interface']) && $_POST['interface'] == $logTable) { ?>selected="selected"<?php } ?> ><?php echo $logTable?></option><?php }?>
								</select>
						</td>
						<td scope="col" valign="top" width="8%">
							Method
						</td>
						<td width="30%" valign="top">
							
								<select id="method_type" name="method_type" >
								<option label="" value=""></option>
								<option label="POST" value="POST" <?php if(!empty($_POST['method_type']) && $_POST['method_type'] == 'POST') { ?>selected="selected"<?php } ?> >POST</option>
								<option label="GET" value="GET" <?php if(!empty($_POST['method_type']) && $_POST['method_type'] == 'GET') { ?>selected="selected"<?php } ?> >GET</option>
								<option label="PATCH" value="PATCH" <?php if(!empty($_POST['method_type']) && $_POST['method_type'] == 'PATCH') { ?>selected="selected"<?php } ?> >PATCH</option>
								<option label="PUT" value="PUT" <?php if(!empty($_POST['method_type']) && $_POST['method_type'] == 'PUT') { ?>selected="selected"<?php } ?> >PUT</option>
								<option label="DELETE" value="DELETE" <?php if(!empty($_POST['method_type']) && $_POST['method_type'] == 'DELETE') { ?>selected="selected"<?php } ?> >DELETE</option>
								</select>
						</td>
						<td scope="col" valign="top" width="8%">
							HTTP Status
						</td>
						<td valign="top" width="30%">
							<input type="text" name="http_status" id="http_status" size="30" maxlength="100" value="<?php echo !empty($_POST['http_status']) ? $_POST['http_status'] : ''?>" title="">
						</td>
					</tr>
					<tr>
						<td scope="col" valign="top" width="8%">
							Request Date Time From
							<span class="required">*</span>
						</td>
						<td valign="top" width="30%">
<div id="div_from_date" style="position: relative;">
	<input style="float: left;" class="date_input " autocomplete="off" type="text" name="from_date" id="from_date" value="<?php echo !empty($_POST['from_date']) ? $_POST['from_date'] : ''?>" title="" tabindex="0" size="8" maxlength="10" data="date" dependentfield="">
	<img style="float: left;padding: 2px 4px 0px 4px;" src="themes/Sugar/images/jscalendar.png" alt="Enter Date" border="0" id="from_date_trigger">&nbsp;
	<div style="float: left;" id="date_start_time_section">
		<span>

			<select class="datetimecombo_hr" size="1" id="from_date_hours" name="from_date_hours" tabindex="0">
			<?php for($hours=0; $hours<=23;$hours++) {?>
			<option value="<?php echo sprintf("%02d", $hours);?>" <?php if(!empty($_POST['from_date_hours']) && $_POST['from_date_hours'] == sprintf("%02d", $hours)) { ?>selected="selected"<?php } ?> ><?php echo sprintf("%02d", $hours);?></option>
			<?php }?>
			</select>&nbsp;:&nbsp;<select class="datetimecombo_min" size="1" id="from_date_minutes" name="from_date_minutes" tabindex="0">
			<?php for($min=0; $min<=59;$min++) {?>
			<option value="<?php echo sprintf("%02d", $min);?>" <?php if(!empty($_POST['from_date_minutes']) && $_POST['from_date_minutes'] == sprintf("%02d", $min)) { ?>selected="selected"<?php } ?> ><?php echo sprintf("%02d", $min);?></option>
			<?php }?>
			</select>&nbsp;:&nbsp;<select class="datetimecombo_sec" size="1" id="from_date_seconds" name="from_date_seconds" tabindex="0">
			<?php for($sec=0; $sec<=59;$sec++) {?>
			<option value="<?php echo sprintf("%02d", $sec);?>" <?php if(!empty($_POST['from_date_seconds']) && $_POST['from_date_seconds'] == sprintf("%02d", $sec)) { ?>selected="selected"<?php } ?> ><?php echo sprintf("%02d", $sec);?></option>
			<?php }?>
			</select>

		</span>
	</div>


	<script type="text/javascript">
		Calendar.setup ({
			inputField : "from_date",
			form : "EditView",
			ifFormat : "%m/%d/%Y %I:%M%P",
			daFormat : "%m/%d/%Y %I:%M%P",
			button : "from_date_trigger",
			singleClick : true,
			dateStr : "",
			startWeekday: 0,
			step : 1,
			weekNumbers:false
		});
	</script>
	<input accesskey="" tabindex="0" name="from_date_hidden" id="to_date_hidden" type="hidden" class="datehidden" value="">
</div>
						</td>
						<td scope="col" valign="top" width="8%">
							<label for="name_3_c_advanced">Request Date Time To</label>
						</td>
						<td valign="top" width="30%">

<div id="div_to_date" style="position: relative;">
	<input style="float: left;" class="date_input " autocomplete="off" type="text" name="to_date" id="to_date" value="<?php echo !empty($_POST['to_date']) ? $_POST['to_date'] : ''?>" title="" tabindex="0" size="8" maxlength="10" data="date" dependentfield="">
	<img style="float: left;padding: 2px 4px 0px 4px;" src="themes/Sugar/images/jscalendar.png" alt="Enter Date" border="0" id="to_date_trigger">&nbsp;
	<div style="float: left;" id="date_start_time_section">
		<span>
		
		<select class="datetimecombo_hr" size="1" id="to_date_hours" name="to_date_hours" tabindex="0">
			<?php for($hours=0; $hours<=23;$hours++) {?>
			<option value="<?php echo sprintf("%02d", $hours);?>" <?php if(!empty($_POST['to_date_hours']) && $_POST['to_date_hours'] == sprintf("%02d", $hours)) { ?>selected="selected"<?php } ?> ><?php echo sprintf("%02d", $hours);?></option>
			<?php }?>
		</select>&nbsp;:&nbsp;<select class="datetimecombo_min" size="1" id="to_date_minutes" name="to_date_minutes" tabindex="0">
			<?php for($min=0; $min<=59;$min++) {?>
			<option value="<?php echo sprintf("%02d", $min);?>" <?php if(!empty($_POST['to_date_minutes']) && $_POST['to_date_minutes'] == sprintf("%02d", $min)) { ?>selected="selected"<?php } ?> ><?php echo sprintf("%02d", $min);?></option>
			<?php }?>
		</select>&nbsp;:&nbsp;<select class="datetimecombo_sec" size="1" id="to_date_seconds" name="to_date_seconds" tabindex="0">
			<?php for($sec=0; $sec<=59;$sec++) {?>
			<option value="<?php echo sprintf("%02d", $sec);?>" <?php if(!empty($_POST['to_date_seconds']) && $_POST['to_date_seconds'] == sprintf("%02d", $sec)) { ?>selected="selected"<?php } ?> ><?php echo sprintf("%02d", $sec);?></option>
			<?php }?>
		</select>

		</span>
	</div>

	<script type="text/javascript">
		Calendar.setup ({
			inputField : "to_date",
			form : "EditView",
			ifFormat : "%m/%d/%Y %I:%M%P",
			daFormat : "%m/%d/%Y %I:%M%P",
			button : "to_date_trigger",
			singleClick : true,
			dateStr : "",
			startWeekday: 0,
			step : 1,
			weekNumbers:false
		});
	</script>
	<input accesskey="" tabindex="0" name="to_date_hidden" id="to_date_hidden" type="hidden" class="datehidden" value="">
</div>
						</td>
					</tr>
					<tr>
					<tr>
						<td colspan="20">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5">
					        <input tabindex="2" title="Search" onclick="SUGAR.savedViews.setChooser()" class="button" type="submit" name="button" value="Search" id="search_form_submit_advanced">&nbsp;
					        <input tabindex="2" title="Clear" onclick="SUGAR.searchForm.clear_form(this.form); document.getElementById(&quot;saved_search_select&quot;).options[0].selected=true; return false;" class="button" type="button" name="clear" id="search_form_clear_advanced" value="Clear">
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</form>
	
	<?php if(empty($_REQUEST['from_date']) && empty($_REQUEST['data'])) { ?>
	<div class="header-tool">
		<div class="dataTables_info" id="ViewApiLog_dataTable_info" role="status" aria-live="polite">Showing 0 to 0 of 0 entries</div>
		<div class="dataTables_paginate paging_full" id="ViewApiLog_dataTable_paginate">
			<a class="paginate_button first disabled" aria-controls="ViewApiLog_dataTable" aria-label="First" data-dt-idx="0" tabindex="0" id="ViewApiLog_dataTable_first"><img src="themes/RacerX/images/start_off.png" align="absmiddle" border="0" alt="Start"></a>
			<a class="paginate_button previous disabled" aria-controls="ViewApiLog_dataTable" aria-label="Previous" data-dt-idx="1" tabindex="0" id="ViewApiLog_dataTable_previous"><img src="themes/RacerX/images/previous_off.png" align="absmiddle" border="0" alt="Previous"></a>
			<a class="paginate_button next disabled" aria-controls="ViewApiLog_dataTable" aria-label="Next" data-dt-idx="2" tabindex="0" id="ViewApiLog_dataTable_next"><img src="themes/RacerX/images/next_off.png" align="absmiddle" border="0" alt="Next"></a>
			<a class="paginate_button last disabled" aria-controls="ViewApiLog_dataTable" aria-label="Last" data-dt-idx="3" tabindex="0" id="ViewApiLog_dataTable_last"><img src="themes/RacerX/images/end_off.png" align="absmiddle" border="0" alt="End"></a>
		</div>
	</div>
	<?php }?>

	<table cellpadding="0" cellspacing="0" border="0" class="list view" id="ViewApiLog_dataTable" width="100%">
		<thead>
			<tr>
				<th height="20">Seq</th>
				<th height="20">Method</th>
				<th height="20">Path</th>
				<th height="20">HTTP Status</th>
				<th height="20">Request Date Time</th>
				<th height="20">ResponseDate Time</th>
				<th height="20">Request Body</th>
				<th height="20">Response Body</th>
			</tr>
		</thead>
		<?php if(empty($_REQUEST['from_date']) && empty($_REQUEST['data'])) { ?>
		<tbody><tr class="oddListRowS1"><td valign="top" colspan="8" class="dataTables_empty">No records found</td></tr></tbody>
		<?php }?>
	</table>

	<?php if(empty($_REQUEST['from_date']) && empty($_REQUEST['data'])) { ?>
	<div class="footer-tool">
		<div class="dataTables_info" id="ViewApiLog_dataTable_info" role="status" aria-live="polite">Showing 0 to 0 of 0 entries</div>
		<div class="dataTables_paginate paging_full" id="ViewApiLog_dataTable_paginate">
			<a class="paginate_button first disabled" aria-controls="ViewApiLog_dataTable" aria-label="First" data-dt-idx="0" tabindex="0" id="ViewApiLog_dataTable_first"><img src="themes/RacerX/images/start_off.png" align="absmiddle" border="0" alt="Start"></a>
			<a class="paginate_button previous disabled" aria-controls="ViewApiLog_dataTable" aria-label="Previous" data-dt-idx="1" tabindex="0" id="ViewApiLog_dataTable_previous"><img src="themes/RacerX/images/previous_off.png" align="absmiddle" border="0" alt="Previous"></a>
			<a class="paginate_button next disabled" aria-controls="ViewApiLog_dataTable" aria-label="Next" data-dt-idx="2" tabindex="0" id="ViewApiLog_dataTable_next"><img src="themes/RacerX/images/next_off.png" align="absmiddle" border="0" alt="Next"></a>
			<a class="paginate_button last disabled" aria-controls="ViewApiLog_dataTable" aria-label="Last" data-dt-idx="3" tabindex="0" id="ViewApiLog_dataTable_last"><img src="themes/RacerX/images/end_off.png" align="absmiddle" border="0" alt="End"></a>
		</div>
	</div>
	<?php }?>
</div>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" language="javascript" class="init"></script>
<script type="text/javascript">
	$(document).ready(function() {
		// CSRF
		if($('input[name="csrf_token"]').length == 0) {
            $('form[name="EditView"]').append('<input type="hidden" name="csrf_token" >')
        }
        $('input[name="csrf_token"]').val(SUGAR.csrf.form_token);

		addToValidate('search_form', 'from_date', 'text', true, 'Request Date Time');
		<?php if(!empty($_REQUEST['interface']) && !empty($_REQUEST['from_date'])) { ?>
		SUGAR.ajaxUI.showLoadingPanel();
		var URL = 'index.php?module=Administration&action=FetchAPILog';
		$('#ViewApiLog_dataTable').dataTable( {
			"dom": '<"header-tool"ip>t<"footer-tool"ip>',
			"pagingType": "full",
			"searching": false,
			"stripeClasses": [ 'oddListRowS1', 'evenListRowS1' ],
		 	"processing": true,
			"serverSide": true,
			"pageLength": <?php echo $col_limit;?>,
			"ajax":{
				url :URL, // json datasource
				type: "post",  // method  , by default get
				data : {
					"csrf_token":SUGAR.csrf.form_token,
					"sugar_body_only":1,
					"method": "getapilogdetails",
					"interface": '<?php echo $_REQUEST["interface"]?>',
					"method_type": '<?php echo $_REQUEST["method_type"]?>',
					"http_status": '<?php echo $_REQUEST["http_status"]?>',
					"from_date": '<?php echo $_REQUEST["from_date"]?>',
					"from_date_hours": '<?php echo $_REQUEST["from_date_hours"]?>',
					"from_date_minutes": '<?php echo $_REQUEST["from_date_minutes"]?>',
					"from_date_seconds": '<?php echo $_REQUEST["from_date_seconds"]?>',
					"to_date": '<?php echo $_REQUEST["to_date"]?>',
					"to_date_hours": '<?php echo $_REQUEST["to_date_hours"]?>',
					"to_date_minutes": '<?php echo $_REQUEST["to_date_minutes"]?>',
					"to_date_seconds": '<?php echo $_REQUEST["to_date_seconds"]?>'
				},
				error: function(){  // error handling
					$("#ViewApiLog_dataTable").append('<tbody><tr class="oddListRowS1"><td valign="top" colspan="8" class="dataTables_empty">No records found</td></tr></tbody>');
					setTimeout(function() {
						SUGAR.ajaxUI.hideLoadingPanel();
					}, 300);
					
				}
			},
			"language": {
				"emptyTable" : "No records found",
				"paginate": {
					"first":    '<img src="themes/RacerX/images/start.png?v=SpLBsqdUxmNsnM5bsI-YNA" align="absmiddle" border="0" alt="Start">',
					"previous": '<img src="themes/RacerX/images/previous.png?v=SpLBsqdUxmNsnM5bsI-YNA" align="absmiddle" border="0" alt="Previous">',
					"next":     '<img src="themes/RacerX/images/next.png?v=SpLBsqdUxmNsnM5bsI-YNA" align="absmiddle" border="0" alt="Next">',
					"last":     '<img src="themes/RacerX/images/end.png?v=SpLBsqdUxmNsnM5bsI-YNA" align="absmiddle" border="0" alt="End">'
				},
				"aria": {
					"paginate": {
						"first":    'First',
						"previous": 'Previous',
						"next":     'Next',
						"last":     'Last'
					}
				}
			},
			"fnDrawCallback": function( settings ) {
				$.each($('a.paginate_button.disabled img'), function(){
					var src = $(this).attr('src');
					src = src.replace(".png","_off.png");
					$(this).attr('src',src);
				});
				setTimeout(function() {
					SUGAR.ajaxUI.hideLoadingPanel();
				}, 300);
			},
			"preDrawCallback": function(settings, json) {
				SUGAR.ajaxUI.showLoadingPanel();
			}
		});
		<?php }?>
	});
</script>
<?php }?>