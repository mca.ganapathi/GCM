<?php
class DBActions
{
	/** Database Connection  */
	private $logPdo;
	/** Table name of writing log */
	private $logTables = array();
	
	public function __construct() {
		// Connect to Database(using SugarCRM's configuration)
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, "","Constructor to intialize variable and db");
		global $sugar_config;
		$this->logTables = array();
		$dbConfig = $sugar_config['dbconfig'];
		$dataSource = 'mysql:host=' . $dbConfig['db_slave_host_name'] . ';dbname=' . $sugar_config['gcm_cstm_const']['view_api_log']['log_db_name'];
		$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
		try{
    		$this->logPdo = new PDO($dataSource, $dbConfig['db_user_name'], $dbConfig['db_password'], $options);
            
    		$stmt = $this->logPdo->query('SHOW TABLES');
    		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    			$values = array_values($row);
    			if(count($values)>0) {
    				$this->logTables[] = $values[0];
    			}
    		}
    		$stmt->closeCursor();
		}catch (PDOException $e){
		    
		}
	}
	
	public function getTableList(){
	    return $this->logTables;
	}
	
	public function getApiAccessLog($tableName, $method, $http_status, $requestDateFrom, $requestDateTo, $offset, $limit, $sortby, $sortdir)
	{
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'tableName : '.$tableName.GCM_GL_LOG_VAR_SEPARATOR.'method : '.$method.GCM_GL_LOG_VAR_SEPARATOR.'http_status : '.$http_status.GCM_GL_LOG_VAR_SEPARATOR.'requestDateFrom : '.$requestDateFrom.GCM_GL_LOG_VAR_SEPARATOR.'requestDateTo : '.$requestDateTo.GCM_GL_LOG_VAR_SEPARATOR."offset : ".$offset.GCM_GL_LOG_VAR_SEPARATOR."limit : ".$limit, 'Method to get API Access log');
		$key = array_search($tableName, $this->logTables);
        if (isset($key) && $key >= 0) {
			$stmt = $this->getSelectStatement($this->logTables[$key], $method, $http_status, $requestDateFrom, $requestDateTo, $offset, $limit, $sortby, $sortdir);
		   
			$stmt->execute(); 
		    $selectResult = $stmt->fetchAll(PDO::FETCH_NUM);
		    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,  '', '');
		    return $selectResult;
		} else {
			ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,  '', '');
			$array = array();
			return $array;
		}		
	}
	private function getSelectStatement($tableName, $method, $http_status, $requestDateFrom, $requestDateTo, $offset, $limit, $sortby, $sortdir)
	{
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'tableName : '.$tableName.GCM_GL_LOG_VAR_SEPARATOR.'method : '.$method.GCM_GL_LOG_VAR_SEPARATOR.'http_status : '.$http_status.GCM_GL_LOG_VAR_SEPARATOR.'requestDateFrom : '.$requestDateFrom.GCM_GL_LOG_VAR_SEPARATOR.'requestDateTo : '.$requestDateTo.GCM_GL_LOG_VAR_SEPARATOR."offset : ".$offset.GCM_GL_LOG_VAR_SEPARATOR."limit : ".$limit.GCM_GL_LOG_VAR_SEPARATOR."sortby : ".$sortby.GCM_GL_LOG_VAR_SEPARATOR."sortdir : ".$sortdir, 'Method to create select statement');
		
        $sql = 'SELECT seq, method, path, http_status, DATE_FORMAT(request_datetime, "%Y/%m/%d %H:%i:%s") as request_datetime, DATE_FORMAT(response_datetime, "%Y/%m/%d %H:%i:%s") as response_datetime, request_body, response_body FROM `' . $tableName . '` WHERE request_datetime >= :request_datetimefrom ';
		if (!empty($method)) {
			$sql .= ' AND method = :method';
		}
		if ($http_status != "") {
			$sql .= ' AND http_status = :http_status';
		}
		if (!empty($requestDateTo)) {
			$sql .= ' AND request_datetime <= :request_datetimeto';
		}
        if (!empty($sortby) && !empty($sortdir)) {
			$sql .= ' ORDER BY ' . $sortby . ' ' . $sortdir;
		}	
        $sql .= ' LIMIT :offset, :limit';		
		$stmt = $this->logPdo->prepare($sql);
		if (!empty($method)) {
			$stmt->bindValue(':method',            $method,                       PDO::PARAM_STR);
		}
		$stmt->bindValue(':request_datetimefrom',  date("Y-m-d H:i:s", strtotime($requestDateFrom)),  PDO::PARAM_STR);
		if ($http_status != "") {
			$stmt->bindValue(':http_status',       $http_status,                  PDO::PARAM_INT);
		}
		if (!empty($requestDateTo)) {
			$stmt->bindValue(':request_datetimeto', date("Y-m-d H:i:s", strtotime($requestDateTo)), PDO::PARAM_STR);
		}
        $stmt->bindValue(':offset',            $offset,                       PDO::PARAM_INT);
		$stmt->bindValue(':limit',            $limit,                       PDO::PARAM_INT);
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,  '', '');
		return $stmt;
	}
	
	public function totalRecord($tableName, $method, $http_status, $requestDateFrom, $requestDateTo)
	{
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'tableName : '.$tableName.GCM_GL_LOG_VAR_SEPARATOR.'method : '.$method.GCM_GL_LOG_VAR_SEPARATOR.'http_status : '.$http_status.GCM_GL_LOG_VAR_SEPARATOR.'requestDateFrom : '.$requestDateFrom.GCM_GL_LOG_VAR_SEPARATOR.'requestDateTo : '.$requestDateTo, 'Method to create select statement');
        $key = array_search($tableName, $this->logTables);
        if (isset($key) && $key >= 0) {
		$sql = 'SELECT count(*) as total FROM  `' . $this->logTables[$key] . '` WHERE request_datetime >= :request_datetimefrom ';
		if (!empty($method)) {
			$sql .= ' AND method = :method';
		}
		if ($http_status != "") {
			$sql .= ' AND http_status = :http_status';
		}
		if (!empty($requestDateTo)) {
			$sql .= ' AND request_datetime <= :request_datetimeto';
		}

		$stmt = $this->logPdo->prepare($sql);
		if (!empty($method)) {
			$stmt->bindValue(':method',            $method,                       PDO::PARAM_STR);
		}
		$stmt->bindValue(':request_datetimefrom',  date("Y-m-d H:i:s", strtotime($requestDateFrom)),  PDO::PARAM_STR);
		if ($http_status != "") {
			$stmt->bindValue(':http_status',       $http_status,                  PDO::PARAM_INT);
		}
		if (!empty($requestDateTo)) {
			$stmt->bindValue(':request_datetimeto', date("Y-m-d H:i:s", strtotime($requestDateTo)), PDO::PARAM_STR);
		}
		$stmt->execute(); 
		$selectResult = $stmt->fetch(PDO::FETCH_ASSOC);
		return $selectResult['total'];
        } else {
			return 0;
		}
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,  '', '');
	}
	
	public function getXMLContent($tableName, $seq, $xmlField)
	{
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'tableName : '.$tableName.GCM_GL_LOG_VAR_SEPARATOR.'method : '.$method.GCM_GL_LOG_VAR_SEPARATOR.'http_status : '.$http_status.GCM_GL_LOG_VAR_SEPARATOR.'requestDateFrom : '.$requestDateFrom.GCM_GL_LOG_VAR_SEPARATOR.'requestDateTo : '.$requestDateTo, 'Method to create select statement');
		$key = array_search($tableName, $this->logTables);
		if (isset($key) && $key >= 0) {
			$sql = 'SELECT '.$xmlField.', http_status FROM  `' . $this->logTables[$key] . '` WHERE seq = :seq ';
			
			$stmt = $this->logPdo->prepare($sql);
			
			$stmt->bindValue(':seq',  $seq,  PDO::PARAM_INT);
			//$stmt->bindValue(':xmlField', $xmlField, PDO::PARAM_STR);
			
			$stmt->execute();
			$selectResult = $stmt->fetch(PDO::FETCH_NUM);
			return $selectResult;
		} else {
			return "";
		}
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,  '', '');
	}
}
