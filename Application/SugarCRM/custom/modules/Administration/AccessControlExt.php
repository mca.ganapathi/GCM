<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
include_once ('custom/modules/gc_Contracts/include/ClassGcContracts.php');

/**
 * This class is created for modifying access controls for modules
 */
class classAccessControlExt
{

    public $module_list;

    // [DF-672] Modify json reading process from file to database.(So do not need file-path define)
    // public $access_files_path;
    const SETTING_KEY_ACE_CONTRACTS = 'AccessControlExt_GMOne';
    const SETTING_KEY_EMP_CONTRACTS = 'AccessControlExt_EnterpriseMail';
    function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        if (!empty($_POST)) {
            if (isset($_POST['module_access']) && !empty($_POST['module_access'])) {
            	if (isset($_POST['enterprise_mail_approver'])) {
                    $enterprise["product_offering"] = $_POST['enterprise_product_offering'];
            		$enterprise["module_access"] = $_POST['module_access'];
            		$enterprise["1"] = $_POST['enterprise_mail_approver'];
            		unset($_POST['enterprise_mail_approver']);
            		self::setJsonData($enterprise, self::SETTING_KEY_EMP_CONTRACTS);
            	}
                $_POST["product_offering"] = $_POST['gmone_product_offering'];
                unset($_POST['gmone_product_offering']);
                unset($_POST['enterprise_product_offering']);

                self::setJsonData($_POST, self::SETTING_KEY_ACE_CONTRACTS);
                unset($_SESSION['wf_config_list']);
                ModUtils::storeWorkflowOfferingsInSession();
                ModUtils::setGlobalConfigWorkflowOfferings();
            }
        }

        global $app_list_strings;
        $module_list = $app_list_strings["moduleList"];

        $this->module_list = array(
                                'gc_Contracts'
        );

        if (!isset($_REQUEST['access']) || empty($_REQUEST['access'])) {
            echo <<<HTML
<div class="yui-content" style="overflow-y: auto; height: 241px;">
  <div class="bodywrapper">
    <br />
    <br />
   <div width="100%" class="wizard">
    <div>
     <center><b>Select a module to modify Access Controls.</b></center>
    </div>
    <div id="Buttons">
     <table width="90%" align="center" cellspacing="7">
      <tbody>
       <tr>
HTML;

            foreach ($this->module_list as $module) {
                echo <<<HTML
    <tr>
     <td align="center"><a href="index.php?module=Administration&action=AccessControlExt&access={$module}" class="studiolink"> <img width="32" height="32" align="bottom" border="0" alt="" src="custom/themes/default/images/{$module}.gif" /> </a></td>
    </tr>
    <tr>
     <td align="center"><a href="index.php?module=Administration&action=AccessControlExt&access={$module}" class="studiolink">{$module_list[$module]}</a></td>
    </tr>
HTML;
            }
            echo <<<HTML
       </tr>
      </tbody>
     </table>
    </div>
   </div>
  </div></div>
HTML;
        } else {
            self::displayModuleAccessControl($_REQUEST['access']);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * Method for displaying access control for $module module
     * @param string $module
     */
    function displayModuleAccessControl($module)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'module : '.$module, 'Method for displaying access control for $module module');
        $json_data = AppSettings::load(self::SETTING_KEY_ACE_CONTRACTS);
        $enterprise_mail_json = AppSettings::load(self::SETTING_KEY_EMP_CONTRACTS);
        $enterprise_data = array();
        if (self::isJson($enterprise_mail_json))
        	$enterprise_data = json_decode($enterprise_mail_json, true);

            $stage_data = array();
            if (self::isJson($json_data))
                $stage_data = json_decode($json_data, true);
            $roles = ACLRole::getAllRoles(true);
            $team_bean = new Team();
            $teams_arr = $team_bean->get_full_list('name asc', '');
            if ($module == 'gc_Contracts') {
                $html = <<<HTML
                <style>
                    select.product_offering option[disabled]{
                        color: rgba(200, 200, 200, 0.79) !important;
                    }
                </style>
                <div style="margin: 20px 0;"><b>Choose Workflow type to edit:</b>
                		<select name="" onchange="changeSettings(this.value)">
                		<option value="gmone">GMOne</option>
                		<option value="enterprise">EnterpriseMail</option>
                		</select>
                		<script type="text/javascript">
                		function changeSettings(workflow_type)
                		{
                		   if (workflow_type == "enterprise") {
                		          $(".gmone_type").hide();
                		          $(".enterprise_mail").show();
                		   } else {
                		          $(".gmone_type").show();
                		          $(".enterprise_mail").hide();
                		      }
                		}
                		</script>
                </div>

				<form action="index.php?module=Administration&action=AccessControlExt" name="EditView" method="POST">
                <div style="margin: 20px 0;" class="gmone_type"><b style="vertical-align: top;">Product Offering</b>
                    <select size="6" name="gmone_product_offering[]" class="product_offering" multiple="true">
HTML;
                    $productOfferingList = $this->productOfferingList();
                    foreach ($productOfferingList as $productOfferingList) {
                        $selectedProductOffering = '';
                        if(!empty($stage_data['product_offering']) && in_array($productOfferingList['id'], $stage_data['product_offering'])) {
                            $selectedProductOffering = ' selected="selected" ';
                        }
                        $disabledProductOffering = '';
                        if(!empty($enterprise_data['product_offering']) && in_array($productOfferingList['id'], $enterprise_data['product_offering'])) {
                            $disabledProductOffering = ' disabled="disabled"';
                        }
                        $html .= <<<HTML
                        <option {$selectedProductOffering} {$disabledProductOffering} value="{$productOfferingList['id']}">{$productOfferingList['name']}</option>
HTML;
                    }
                    $html .= <<<HTML
                    </select>
                </div>
                <div class="gmone_type">
				    <input type="hidden" name="module_access" value="gc_Contracts">
                    Approval Workflow Roles
				    <table class="other view">
                        <tbody>
                            <tr>
HTML;

                                $current_stage = '0';

                                $html .= <<<HTML
                                <td width="20%" scope="row">{$GLOBALS['app_list_strings']['workflow_flag_list'][$current_stage]}</td>
                                <td width="30%"><b>Record Owner (Assigned user)</b></td>
HTML;

                                $current_stage = '1';

                                $html .= <<<HTML

                                <td width="20%" scope="row">{$GLOBALS['app_list_strings']['workflow_flag_list'][$current_stage]}</td>
                                <td width="30%">
                                    <span id="span_stage_1">
                                        <select id="ddl_contract_stage1" name="{$current_stage}">
HTML;
                                            foreach ($roles as $role) {
                                                $selected = '';
                                                if (isset($stage_data[$current_stage]) && ($stage_data[$current_stage] == $role['id']))
                                                    $selected .= ' selected ';
                                                $html .= <<<HTML
                                                <option {$selected} value="{$role['id']}">{$role['name']}</option>
HTML;
                                            }
                                            $current_stage = '2';
                                            $html .= <<<HTML
                                        </select>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" scope="row">{$GLOBALS['app_list_strings']['workflow_flag_list'][$current_stage]}</td>
                                <td width="30%">
                                    <span id="span_stage_2">
                                        <select id="ddl_contract_stage2" name="{$current_stage}">
HTML;

                                            foreach ($roles as $role) {
                                                $selected = '';
                                                if (isset($stage_data[$current_stage]) && ($stage_data[$current_stage] == $role['id']))
                                                    $selected .= ' selected ';
                                                $html .= <<<HTML
                                                <option {$selected} value="{$role['id']}">{$role['name']}</option>
HTML;
                                            }
                                            $current_stage = '3';
                                            $html .= <<<HTML
                                        </select>
                                    </span>
                                </td>

                                <td width="20%" scope="row">{$GLOBALS['app_list_strings']['workflow_flag_list'][$current_stage]}</td>
                                <td width="30%">
                                    <span id="span_stage_3">
                                        <select id="ddl_contract_stage3" name="{$current_stage}">
HTML;
                                            foreach ($roles as $role) {
                                                $selected = '';
                                                if (isset($stage_data[$current_stage]) && ($stage_data[$current_stage] == $role['id']))
                                                    $selected .= ' selected ';
                                                $html .= <<<HTML
                                                <option {$selected} value="{$role['id']}">{$role['name']}</option>
HTML;
                                            }
                                            $html .= <<<HTML
                                        </select>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
HTML;
                $html .= <<<HTML
                <div class="gmone_type">
                    Approver Teams
                    <table class="other view">
                        <tbody>
                            <tr>
                                <td width="20%" scope="row">Approver Team1</td>
                                <td width="30%">
                                    <select id="approver_team1" name="approver_team1">
HTML;
                                        if ($teams_arr != null) {
                                            foreach ($teams_arr as $team) {
                                                $selected = '';
                                                if ($team->id == $stage_data['approver_team1'])
                                                    $selected = 'selected';
                                                $html .= <<<HTML
                                                <option value="{$team->id}" {$selected}>{$team->name}</option>
HTML;
                                            }
                                        }
                                        $html .= <<<HTML
                                    </select>
                                </td>
                                <td scope="row">Approver Team3</td>
                                <td>
                                    <select id="approver_team3" name="approver_team3">
HTML;
                                        if ($teams_arr != null) {
                                            foreach ($teams_arr as $team) {
                                                $selected = '';
                                                if ($team->id == $stage_data['approver_team3'])
                                                    $selected = 'selected';
                                                $html .= <<<HTML
                                                <option value="{$team->id}" {$selected}>{$team->name}</option>
HTML;
                                            }
                                        }
                                        $html .= <<<HTML
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">Approver Team2</td>
                                <td>
                                    <select id="approver_team2" name="approver_team2">
HTML;
                                        if ($teams_arr != null) {
                                            foreach ($teams_arr as $team) {
                                                $selected = '';
                                                if ($team->id == $stage_data['approver_team2'])
                                                    $selected = 'selected';
                                                $html .= <<<HTML
                                                <option value="{$team->id}" {$selected}>{$team->name}</option>
HTML;
                                            }
                                        }
                                        $html .= <<<HTML
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="gmone_type">
                    Contract Visibility Teams
                    <table class="other view">
                        <tbody>
                            <tr>
                                <td width="20%" scope="row">Contract Visibility Teams</td>
                                <td width="30%">
                                    <select id="approver_team" name="approver_team[]" multiple size="6">
HTML;
                                        if ($teams_arr != null) {
                                            foreach ($teams_arr as $team) {
                                                $selected = '';
                                                if (in_array($team->id, $stage_data['approver_team']))
                                                    $selected = 'selected';
                                                $html .= <<<HTML
                                                <option value="{$team->id}" {$selected}>{$team->name}</option>
HTML;
                                            }
                                        }
                                        $html .= <<<HTML
                                    </select>
                                </td>
                                <td scope="row">Selected Teams</td>
                                <td>
                                    <div id="selected_teams" style="width:200px;height:90px;border:1px solid;overflow:auto;font-weight:bold;">
HTML;
                                        if ($teams_arr != null) {
                                            foreach ($teams_arr as $team) {
                                                if (in_array($team->id, $stage_data['approver_team'])) {
                                                    $html .= <<<HTML
                                                    {$team->name}<br>
HTML;
                                                }
                                            }
                                        }
                                        $html .= <<<HTML
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div style="margin: 20px 0;display:none;" class="enterprise_mail"><b style="vertical-align: top;">Product Offering</b>
                    <select size="6" name="enterprise_product_offering[]" class="product_offering" multiple="true">
HTML;
                    $productOfferingList = $this->productOfferingList();
                    foreach ($productOfferingList as $productOfferingList) {
                        $selectedProductOffering = '';
                        if(!empty($enterprise_data['product_offering']) && in_array($productOfferingList['id'], $enterprise_data['product_offering'])) {
                            $selectedProductOffering = ' selected="selected" ';
                        }
                        $disabledProductOffering = '';
                        if(!empty($stage_data['product_offering']) && in_array($productOfferingList['id'], $stage_data['product_offering'])) {
                            $disabledProductOffering = ' disabled="disabled"';
                        }
                        $html .= <<<HTML
                        <option {$selectedProductOffering} {$disabledProductOffering} value="{$productOfferingList['id']}">{$productOfferingList['name']}</option>
HTML;
                    }
                    $html .= <<<HTML
                    </select>
                </div>
                <div class="enterprise_mail" style="display:none;">
                    Enterprise Mail Approver
            		<table class="other view">
                        <tbody>
                            <tr>
                                <td width="20%" scope="row">Enterprise Mail Approver</td>
                                <td width="30%">
                                    <select id="enterprise_mail_approver" name="enterprise_mail_approver">
HTML;
                                        foreach ($roles as $role) {
                                            $selected = '';
                                            if (isset($enterprise_data['1']) && ($enterprise_data['1'] == $role['id']))
                                                $selected .= ' selected ';
                                            $html .= <<<HTML
                                            <option {$selected} value="{$role['id']}">{$role['name']}</option>
HTML;
                                        }
                                        $html .= <<<HTML
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <script>
                    // CSRF
                    if($('input[name="csrf_token"]').length == 0) {
                        $('form[name="EditView"]').append('<input type="hidden" name="csrf_token" >')
                    }
                    $('input[name="csrf_token"]').val(SUGAR.csrf.form_token);

                   $("#approver_team").on('change', function() {
                      var str = "";
                      $("#approver_team option:selected").each(function () {
                         str += $(this).text()+'<br>';
                      });
                      $("#selected_teams").html(str);
                    });
                </script>
                <script>
                    $('[name="enterprise_product_offering[]"]').on('change', function(){
                        $('select[name="gmone_product_offering[]"] option').prop('disabled', false).removeAttr('disabled');
                        $('[name="enterprise_product_offering[]"] option:selected').each(function(){
                            var enable = $(this).val();
                            $('[name="gmone_product_offering[]"]').find('option[value="'+enable+'"]').prop('disabled', true);
                        })
                    })

                    $('[name="gmone_product_offering[]"]').on('change', function(){
                        $('select[name="enterprise_product_offering[]"] option').prop('disabled', false).removeAttr('disabled');
                        $('[name="gmone_product_offering[]"] option:selected').each(function(){
                            var enable = $(this).val();
                            $('[name="enterprise_product_offering[]"]').find('option[value="'+enable+'"]').prop('disabled', true);
                        });
                    });
                </script>
HTML;
                $html .= <<<HTML
                <div class="action_buttons">
                    <input type="submit" value="Save" onclick="this.form.action.value='Save';return true;" class="button primary" accesskey="a" id="save_button" title="Save">&nbsp;<input type="button" onclick="document.location.href='index.php?module=Administration&action=AccessControlExt';" value="Cancel" name="save" class="button cancel_button" title="Cancel">
                </div>
                </form>
HTML;

                echo $html;
            }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,  '', '');
    }

    /**
     * Method created for checking json or not
     * @param string $string
     * @return boolean true or false
     */
    function isJson($string)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'string : '.print_r($string,true), 'Method created for checking json or not');
        $string = ((is_string($string) && (is_object(json_decode($string)) || is_array(json_decode($string))))) ? true : false;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'string : '.print_r($string,true), '');
        return $string;
    }

   /** Method to set json data
    */
    function setJsonData($data, $key)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'data : '.print_r($data,true), 'Method to set json data');
        AppSettings::save($key, json_encode($data));
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * private method for getting Product offering list from PMS
     *
     * @return array Product offering list
     * @author Rajul.Mondal
     * @since April 25, 2017
     */
    private function productOfferingList()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'private method for getting Product offering list from PMS');
        // unset session product offering
        global $sugar_config;
        $catalog_id = isset($sugar_config['pms_product']['catalog_id']) ? $sugar_config['pms_product']['catalog_id'] : '';
        $po= $sugar_config['pms_product']['po_prefix'] . '_' . $catalog_id;
        unset($_SESSION['pms_product']);

        // set & get session product offering
        $this->obj_gc_contracts = new ClassGcContracts();
        $list_product_offerings = $this->obj_gc_contracts->getProductOfferings(true);

        $list = array();
        if(!empty($list_product_offerings)) {
            foreach ($list_product_offerings as $key => $productOffering) {
                $list[$key]['id'] = $key;
                $list[$key]['name'] = $productOffering;
                $list[$key]['selected'] = false;
            }
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '$list '. print_r($list, true), '');
        return $list;
    }
}

$objclassAccessControlExt = new classAccessControlExt();