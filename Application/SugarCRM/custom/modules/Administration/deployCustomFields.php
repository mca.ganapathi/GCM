<?php

/**
* This class will deploy custom created fields (from studio) into SugarCRM application
*/
class deployCustomFields
{

    /**
     * Method for deploying custom fields
     */
    function deployCustomFields()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'Method for deploying custom fields');
        $moduleList = array(
                            'Contacts', 
                            'Accounts'
        ); // custom modules array list
        
        $deployFieldsList = array(); // global array variable to collect set of fields from all modules
        
        foreach ($moduleList as $moduleName) {
            
            $bean = BeanFactory::getBean($moduleName); // fetch module bean data
            $ModuleFieldDefs = $bean->getFieldDefinitions(); // fetch fields from module bean
            
            if (file_exists("custom/modules/Administration/include/ModuleCustomFieldsMeta/{$moduleName}CustomFieldsMeta.php")) {
                
                $moduleFieldArray = array(); // module fields array
                                             
                // include file that contains list of fields in array format
                require_once ("custom/modules/Administration/include/ModuleCustomFieldsMeta/{$moduleName}CustomFieldsMeta.php");
                foreach (${$moduleName . 'Fields'} as $ModuleField)
                    if (!isset($ModuleFieldDefs[(rtrim($ModuleField['name'], '_c') . '_c')]['id']))
                        array_push($moduleFieldArray, $ModuleField); // compare array field from file with module bean field (to check whether to pick field for installation or not)
                
                $deployFieldsList = array_merge($deployFieldsList, $moduleFieldArray);
            }
        }
        
        // finally deploy the fields which are not present in all list module beans
        require_once ('ModuleInstall/ModuleInstaller.php');
        $moduleInstaller = new ModuleInstaller();
        $moduleInstaller->install_custom_fields($deployFieldsList);
        
        // User message
        echo '<br>';
        echo '<h3>Database tables are synced with vardefs...</h3>';
        echo '<br>';
        echo '<a href="index.php?module=Administration&action=index">Return to Administration page</a>';
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}

// call the class file
$objDeployCustomFields = new deployCustomFields();

?>