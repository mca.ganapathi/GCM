<?php

/**
 * @author Sagar Salunkhe
 * @since 18 Sep 2015
 * @desc System will read this file while deploying custom fields
 * @internal : array name should be strictly like "<modulename>Fields"
 */
$ContactsFields = array(
                        // Address (Latin Character)
                        array(
                            'name' => 'addr_1_c', 
                            'label' => 'LBL_ADDR_1', 
                            'type' => 'text', 
                            'module' => 'Contacts', 
                            'help' => '', 
                            'comment' => '', 
                            'default_value' => '', 
                            'rows' => '4', 
                            'cols' => '20', 
                            'required' => false,  // true or false
                            'reportable' => false,  // true or false
                            'audited' => false,  // true or false
                            'importable' => 'false',  // 'true', 'false', 'required'
                            'duplicate_merge' => false,  // true or false
                            'source' => 'custom_fields', 
                            'custom_module' => 'Contacts', 
                            'studio' => true
                        ), 
                        // Address (Local Character)
                        array(
                            'name' => 'addr_2_c', 
                            'label' => 'LBL_ADDR_2', 
                            'type' => 'text', 
                            'module' => 'Contacts', 
                            'help' => '', 
                            'comment' => '', 
                            'default_value' => '', 
                            'rows' => '4', 
                            'cols' => '20', 
                            'required' => false,  // true or false
                            'reportable' => false,  // true or false
                            'audited' => false,  // true or false
                            'importable' => 'false',  // 'true', 'false', 'required'
                            'duplicate_merge' => false,  // true or false
                            'source' => 'custom_fields', 
                            'custom_module' => 'Contacts', 
                            'studio' => true
                        ), 
                        // Contact Type
                        array(
                            'name' => 'contact_type_c', 
                            'label' => 'LBL_CONTACT_TYPE', 
                            'type' => 'enum', 
                            'module' => 'Contacts', 
                            'help' => '', 
                            'comment' => '', 
                            'ext1' => 'contact_type_list',  // maps to options - specify list name
                            'default_value' => '',  // key of entry in specified list
                            'required' => false,  // true or false
                            'reportable' => false,  // true or false
                            'audited' => false,  // true or false
                            'importable' => 'false',  // 'true', 'false', 'required'
                            'duplicate_merge' => false,  // true or false
                            'source' => 'custom_fields', 
                            'custom_module' => 'Contacts', 
                            'studio' => true
                        ),
		//Division (Latin Character)
        array(
            'name' => 'division_1_c', 
            'label' => 'LBL_DIVISION_1', 
            'type' => 'varchar', 
            'module' => 'Contacts', 
            'help' => '', 
            'comment' => '', 
            'default_value' => '', 
            'max_size' => 100, 
            'required' => false,  // true or false
            'reportable' => false,  // true or false
            'audited' => false,  // true or false
            'importable' => 'false',  // 'true', 'false', 'required'
            'duplicate_merge' => false,  // true or false
            'source' => 'custom_fields', 
            'custom_module' => 'Contacts', 
            'studio' => true
                        ), 
                        // Division (Local Character)
                        array(
                            'name' => 'division_2_c', 
                            'label' => 'LBL_DIVISION_2', 
                            'type' => 'varchar', 
                            'module' => 'Contacts', 
                            'help' => '', 
                            'comment' => '', 
                            'default_value' => '', 
                            'max_size' => 100, 
                            'required' => false,  // true or false
                            'reportable' => false,  // true or false
                            'audited' => false,  // true or false
                            'importable' => 'false',  // 'true', 'false', 'required'
                            'duplicate_merge' => false,  // true or false
                            'source' => 'custom_fields', 
                            'custom_module' => 'Contacts', 
                            'studio' => true
                        ), 
                        // Extension number
                        array(
                            'name' => 'ext_no_c', 
                            'label' => 'LBL_EXT_NO', 
                            'type' => 'varchar', 
                            'module' => 'Contacts', 
                            'help' => '', 
                            'comment' => '', 
                            'default_value' => '', 
                            'max_size' => 5, 
                            'required' => false,  // true or false
                            'reportable' => false,  // true or false
                            'audited' => false,  // true or false
                            'importable' => 'false',  // 'true', 'false', 'required'
                            'duplicate_merge' => false,  // true or false
                            'source' => 'custom_fields', 
                            'custom_module' => 'Contacts', 
                            'studio' => true
                        ), 
                        // Contact Person Name (Local Character)
                        array(
                            'name' => 'name_2_c', 
                            'label' => 'LBL_NAME_2', 
                            'type' => 'varchar', 
                            'module' => 'Contacts', 
                            'help' => 'Contact Person in Local character (Japanese)', 
                            'comment' => 'Contact Person in Local character (Japanese)', 
                            'default_value' => '', 
                            'max_size' => 60, 
                            'required' => false,  // true or false
                            'reportable' => false,  // true or false
                            'audited' => false,  // true or false
                            'importable' => 'false',  // 'true', 'false', 'required'
                            'duplicate_merge' => false,  // true or false
                            'source' => 'custom_fields', 
                            'custom_module' => 'Contacts', 
                            'studio' => true
                        ), 
                        // Postal Number
                        array(
                            'name' => 'postal_no_c', 
                            'label' => 'LBL_POSTAL_NO', 
                            'type' => 'varchar', 
                            'module' => 'Contacts', 
                            'help' => '', 
                            'comment' => '', 
                            'default_value' => '', 
                            'max_size' => 9, 
                            'required' => false,  // true or false
                            'reportable' => false,  // true or false
                            'audited' => false,  // true or false
                            'importable' => 'false',  // 'true', 'false', 'required'
                            'duplicate_merge' => false,  // true or false
                            'source' => 'custom_fields', 
                            'custom_module' => 'Contacts', 
                            'studio' => true
                        ), 
                        // Title (Local Character)
                        array(
                            'name' => 'title_2_c', 
                            'label' => 'LBL_TITLE_2', 
                            'type' => 'varchar', 
                            'module' => 'Contacts', 
                            'help' => '', 
                            'comment' => '', 
                            'default_value' => '', 
                            'max_size' => 100, 
                            'required' => false,  // true or false
                            'reportable' => false,  // true or false
                            'audited' => false,  // true or false
                            'importable' => 'false',  // 'true', 'false', 'required'
                            'duplicate_merge' => false,  // true or false
                            'source' => 'custom_fields', 
                            'custom_module' => 'Contacts', 
                            'studio' => true
                        ), 
                        // Country Name Code Relate Field
                        array(
                            'name' => 'country_code_c', 
                            'label' => 'LBL_COUNTRY_CODE', 
                            'type' => 'relate', 
                            'help' => '', 
                            'comment' => '', 
                            'default_value' => '', 
                            'required' => false,  // true or false
                            'reportable' => false,  // true or false
                            'audited' => false,  // true or false
                            'importable' => 'false',  // 'true', 'false', 'required'
                            'duplicate_merge' => false,  // true or false
                            'source' => 'custom_fields', 
                            'custom_module' => 'c_country', 
                            'max_size' => 255, 
                            'id_name' => 'c_country_id_c', 
                            'ext2' => 'c_country', 
                            'module' => 'Contacts', 
                            'rname' => 'name', 
                            'studio' => true
                        ), 
                        array(
                            'required' => false, 
                            'name' => 'c_country_id_c', 
                            'vname' => '', 
                            'type' => 'id', 
                            'massupdate' => 0, 
                            'comments' => '', 
                            'help' => '', 
                            'importable' => 'true', 
                            'duplicate_merge' => 'disabled', 
                            'duplicate_merge_dom_value' => 0, 
                            'audited' => false, 
                            'reportable' => true, 
                            'module' => 'Contacts', 
                            'calculated' => false, 
                            'max_size' => 36, 
                            'size' => '36'
                        ), 
                        // Address (General/Global)
                        array(
                            'name' => 'addr_general_c', 
                            'label' => 'LBL_ADDR_GENERAL', 
                            'type' => 'text', 
                            'module' => 'Contacts', 
                            'help' => '', 
                            'comment' => '', 
                            'default_value' => '', 
                            'rows' => '4', 
                            'cols' => '60', 
                            'required' => false,  // true or false
                            'reportable' => false,  // true or false
                            'audited' => true,  // true or false
                            'importable' => 'false',  // 'true', 'false', 'required'
                            'duplicate_merge' => false,  // true or false
                            'source' => 'custom_fields', 
                            'custom_module' => 'Contacts', 
                            'studio' => true
                        ), 
                        // City (General/Global)
                        array(
                            'name' => 'city_general_c', 
                            'label' => 'LBL_CITY_GENERAL', 
                            'type' => 'varchar', 
                            'module' => 'Contacts', 
                            'help' => '', 
                            'comment' => '', 
                            'default_value' => '', 
                            'max_size' => 100, 
                            'required' => false,  // true or false
                            'reportable' => false,  // true or false
                            'audited' => true,  // true or false
                            'importable' => 'false',  // 'true', 'false', 'required'
                            'duplicate_merge' => false,  // true or false
                            'source' => 'custom_fields', 
                            'custom_module' => 'Contacts', 
                            'studio' => true
                        ), 
                        // State Name Relate Field
                        array(
                            'name' => 'state_general_c', 
                            'label' => 'LBL_STATE_GENERAL', 
                            'type' => 'relate', 
                            'help' => '', 
                            'comment' => '', 
                            'default_value' => '', 
                            'required' => false,  // true or false
                            'reportable' => false,  // true or false
                            'audited' => true,  // true or false
                            'importable' => 'false',  // 'true', 'false', 'required'
                            'duplicate_merge' => false,  // true or false
                            'source' => 'custom_fields', 
                            'custom_module' => 'c_State', 
                            'max_size' => 255, 
                            'id_name' => 'c_state_id_c', 
                            'ext2' => 'c_State', 
                            'module' => 'Contacts', 
                            'rname' => 'name', 
                            'studio' => true
                        ), 
                        array(
                            'required' => false, 
                            'name' => 'c_state_id_c', 
                            'vname' => '', 
                            'type' => 'id', 
                            'massupdate' => 0, 
                            'comments' => '', 
                            'help' => '', 
                            'importable' => 'true', 
                            'duplicate_merge' => 'disabled', 
                            'duplicate_merge_dom_value' => 0, 
                            'audited' => false, 
                            'reportable' => true, 
                            'module' => 'Contacts', 
                            'calculated' => false, 
                            'max_size' => 36, 
                            'size' => '36'
                        )
);
?>