<?php
$module_name = 'pa_ProductAttributeValues';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'uom' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_UOM',
        'width' => '10%',
        'default' => true,
        'name' => 'uom',
      ),
      'parent_attribute' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_PARENT_ATTRIBUTE',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'id' => 'PA_PRODUCTATTRIBUTES_ID_C',
        'name' => 'parent_attribute',
      ),
      'favorites_only' => 
      array (
        'name' => 'favorites_only',
        'label' => 'LBL_FAVORITES_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'uom' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_UOM',
        'width' => '10%',
        'default' => true,
        'name' => 'uom',
      ),
      'parent_attribute' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_PARENT_ATTRIBUTE',
        'id' => 'PA_PRODUCTATTRIBUTES_ID_C',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'name' => 'parent_attribute',
      ),
      'favorites_only' => 
      array (
        'name' => 'favorites_only',
        'label' => 'LBL_FAVORITES_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>