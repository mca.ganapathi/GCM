<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (�MSA�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

require_once ('include/MVC/View/views/view.detail.php');

class pa_ProductAttributeValuesViewDetail extends ViewDetail
{

    function pa_ProductAttributeValuesViewDetail()
    {
        // call parent ViewDetail method
        parent::__construct();    
    }

    function display()
    {   
        /**
         * * START : Hide unused buttons from View **
         */
        unset($this->dv->defs['templateMeta']['form']['buttons'][1]); // Hide Duplicate button
        unset($this->dv->defs['templateMeta']['form']['buttons'][2]); // Hide Delete button
        unset($this->dv->defs['templateMeta']['form']['buttons'][3]); // Hide Find Duplicate button
        unset($this->dv->defs['templateMeta']['form']['buttons'][4]); // Hide Get Data button
        /**
         * * END : Hide unused buttons from View **
         */

        // call parent display method
        parent::display();    
    }
}