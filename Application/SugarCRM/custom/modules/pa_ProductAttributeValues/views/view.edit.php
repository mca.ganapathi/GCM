<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (�MSA�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

require_once ('include/MVC/View/views/view.edit.php');

class pa_ProductAttributeValuesViewEdit extends ViewEdit
{

    function pa_ProductAttributeValuesViewEdit()
    {
        // call parent ViewEdit method
        parent::__construct();
    }

    function preDisplay()
    {
        
        $this->restrictEditAction();
        
        // call parent preDisplay method
        parent::preDisplay();
    }

    function display()
    {
        // restrict create view
        $display_txt = '<br><div id="div_related_values"><div><span style="color:red;">Action Prohibited: Cannot Create or Edit Accounts (Japan Specific) from Sugar Application.</span></h4></div><div><br>';
        echo $display_txt;
        echo $this->ev->display($this->showTitle);
        echo '<script>$("#SAVE_HEADER").hide(); $( ".action_buttons" ).last().css( "display", "none" ); $(".moduleTitle").hide(); $("#CANCEL_HEADER").val("Back");$("#EditView_tabs,#detailpanel_30,#btn_view_change_log").hide();</script>';
        
        // call parent display method
        // parent::display();
    }

    function restrictEditAction()
    {
        if (isset($this->bean->id) && ! empty($this->bean->id)) {
            // redirect to Product Atributes module
            $params = array(
                'module' => 'pa_ProductAttributes',
                'action' => $this->action,
                'record' => $this->bean->pa_productattributes_id_c
            );
            // redirect
            SugarApplication::redirect('index.php?' . http_build_query($params));
        }
    }
}