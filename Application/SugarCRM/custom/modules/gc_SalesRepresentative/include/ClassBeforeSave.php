<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * class ClassSalesRepresentativeBeforeSave
 */
class ClassSalesRepresentativeBeforeSave
{

    /**
     * Method SetContractsAuditEntry to save Sales Representative logs into Contract module
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author dinesh.itkar
     * @since Nov 27, 2015
     */
    function SetContractsAuditEntry($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'Method to Sales Representative logs into Contract module');
        
        // Avoid audit log for new record.
        if (empty($bean->fetched_row['id'])) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'', '');
            return;
        }
        
        // Audit Sales Company
        if ($bean->gc_nttcomgroupcompany_id_c != $bean->fetched_row['gc_nttcomgroupcompany_id_c']) {
            $before_sales_company_name = (!empty($bean->fetched_row['gc_nttcomgroupcompany_id_c'])) ? $this->getSalesCompanyNameByID($bean->fetched_row['gc_nttcomgroupcompany_id_c']) : '';
            $after_sales_company_name = (!empty($bean->gc_nttcomgroupcompany_id_c)) ? $this->getSalesCompanyNameByID($bean->gc_nttcomgroupcompany_id_c) : '';
            $this->insertSaleRepAudit($bean, 'Sales Company', 'text', $before_sales_company_name, $after_sales_company_name);
        }
        
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'', '');
    }

    /**
     * Method insertSaleRepAudit to Update Sales Representative Audit record.
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param string $field_name field name to audit
     * @param string $data_type data type of field
     * @param string $before value before update
     * @param string $after value after update
     * @author dinesh.itkar
     * @since Nov 27, 2015
     */
    private function insertSaleRepAudit($bean, $field_name, $data_type, $before, $after)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'field_name : '.$field_name.GCM_GL_LOG_VAR_SEPARATOR.'data_type : '.$data_type.GCM_GL_LOG_VAR_SEPARATOR.'before : '.$before.GCM_GL_LOG_VAR_SEPARATOR.'after : '.$after, 'Method to Update Sales Representative Audit record');
        $audit_entry = array();
        $audit_entry['field_name'] = $field_name;
        $audit_entry['data_type'] = $data_type;
        $audit_entry['before'] = $before;
        $audit_entry['after'] = $after;
        $bean->db->save_audit_records($bean, $audit_entry);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'', '');
    }

    /**
     * Method getSalesCompanyNameByID to get sales company name by sales company id.
     * 
     * @param $sales_company_id
     * @return string $sales_company_name  Sales company name
     * @author dinesh.itkar
     * @since May 11, 2016
     */
    private function getSalesCompanyNameByID($sales_company_id = null)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'sales_company_id : '.$sales_company_id, 'Method to get sales company name by sales company id');
        
        $sales_company_name = '';
        $sales_company_bean = BeanFactory::getBean('gc_NTTComGroupCompany');
        if ($sales_company_id != null) {
            $sales_company_bean_result = $sales_company_bean->retrieve($sales_company_id);
            $sales_company_name = $sales_company_bean_result->name;
        }
        
        unset($sales_company_bean, $sales_company_bean_result);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'sales_company_name'.$sales_company_name, '');
        return $sales_company_name;
    }
}

?>