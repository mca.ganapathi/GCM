<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/gc_SalesRepresentative/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['gc_SalesRepresentative']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/gc_SalesRepresentative/Ext/Vardefs/sugarfield_division_1.php

 // created: 2016-11-04 08:18:21
$dictionary['gc_SalesRepresentative']['fields']['division_1']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/gc_SalesRepresentative/Ext/Vardefs/sugarfield_division_2.php

 // created: 2016-11-04 08:18:41
$dictionary['gc_SalesRepresentative']['fields']['division_2']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/gc_SalesRepresentative/Ext/Vardefs/sugarfield_email1.php

 // created: 2016-11-04 08:20:43
$dictionary['gc_SalesRepresentative']['fields']['email1']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/gc_SalesRepresentative/Ext/Vardefs/sugarfield_ext_no.php

 // created: 2016-11-04 08:19:55
$dictionary['gc_SalesRepresentative']['fields']['ext_no']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/gc_SalesRepresentative/Ext/Vardefs/sugarfield_fax_no.php

 // created: 2016-11-04 08:20:10
$dictionary['gc_SalesRepresentative']['fields']['fax_no']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/gc_SalesRepresentative/Ext/Vardefs/sugarfield_mob_no.php

 // created: 2017-04-26 10:42:19
$dictionary['gc_SalesRepresentative']['fields']['mob_no']['audited']=false;
$dictionary['gc_SalesRepresentative']['fields']['mob_no']['massupdate']=false;
$dictionary['gc_SalesRepresentative']['fields']['mob_no']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);

 
?>
<?php
// Merged from custom/Extension/modules/gc_SalesRepresentative/Ext/Vardefs/sugarfield_name.php

 // created: 2016-11-04 08:17:11
$dictionary['gc_SalesRepresentative']['fields']['name']['audited']=true;
$dictionary['gc_SalesRepresentative']['fields']['name']['full_text_search']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/gc_SalesRepresentative/Ext/Vardefs/sugarfield_sales_channel_code.php

 // created: 2016-12-13 08:53:56
$dictionary['gc_SalesRepresentative']['fields']['sales_channel_code']['audited']=true;
$dictionary['gc_SalesRepresentative']['fields']['sales_channel_code']['massupdate']=false;
$dictionary['gc_SalesRepresentative']['fields']['sales_channel_code']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);

 
?>
<?php
// Merged from custom/Extension/modules/gc_SalesRepresentative/Ext/Vardefs/sugarfield_sales_rep_name_1.php

 // created: 2016-11-04 08:17:40
$dictionary['gc_SalesRepresentative']['fields']['sales_rep_name_1']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/gc_SalesRepresentative/Ext/Vardefs/sugarfield_tel_no.php

 // created: 2016-11-04 08:19:32
$dictionary['gc_SalesRepresentative']['fields']['tel_no']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/gc_SalesRepresentative/Ext/Vardefs/sugarfield_title_1.php

 // created: 2017-04-26 10:41:26
$dictionary['gc_SalesRepresentative']['fields']['title_1']['audited']=false;
$dictionary['gc_SalesRepresentative']['fields']['title_1']['massupdate']=false;
$dictionary['gc_SalesRepresentative']['fields']['title_1']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);

 
?>
<?php
// Merged from custom/Extension/modules/gc_SalesRepresentative/Ext/Vardefs/sugarfield_title_2.php

 // created: 2017-04-26 10:46:06
$dictionary['gc_SalesRepresentative']['fields']['title_2']['audited']=true;
$dictionary['gc_SalesRepresentative']['fields']['title_2']['massupdate']=false;
$dictionary['gc_SalesRepresentative']['fields']['title_2']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);

 
?>
