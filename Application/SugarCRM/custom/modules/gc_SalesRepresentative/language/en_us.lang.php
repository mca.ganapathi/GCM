<?php
// created: 2016-11-04 08:20:43
$mod_strings = array (
  'LBL_SALES_CHANNEL_CODE' => 'Sales Channel Code',
  'LBL_NAME' => 'Sales Representative Name (Latin Character)',
  'LBL_SALES_REP_NAME_1' => 'Sales Representative  Name (Local Character)',
  'LBL_DIVISION_1' => 'Division (Latin Character)',
  'LBL_DIVISION_2' => 'Division (Local Character)',
  'LBL_TITLE_1' => 'Title (Latin Character)',
  'LBL_TITLE_2' => 'Title (Local Character)',
  'LBL_TEL_NO' => 'Telephone number',
  'LBL_EXT_NO' => 'Extension number',
  'LBL_FAX_NO' => 'FAX number',
  'LBL_MOB_NO' => 'Mobile Number',
  'LBL_EMAIL1' => 'Email Address',
);