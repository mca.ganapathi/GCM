<?php
// created: 2016-09-29 04:41:37
$viewdefs['Emails']['base']['menu']['quickcreate'] = array (
  'layout' => 'compose',
  'label' => 'LBL_COMPOSE_MODULE_NAME_SINGULAR',
  'visible' => false,
  'icon' => 'fa-plus',
  'order' => 8,
);