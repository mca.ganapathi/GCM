<?php
// created: 2015-09-08 12:57:42
$mod_strings = array (
  'LBL_EMAILS_ACCOUNTS_REL' => 'Emails:Corporates',
  'LBL_EMAILS_CONTACTS_REL' => 'Emails:Accounts',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Corporates',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Accounts',
  'LBL_CONTACTS_SUBPANEL_TITLE_SNIP' => 'Email Accounts',
);