<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * class ClassAccountsJsBeforeSave
 */
class ClassAccountsJsBeforeSave
{

    /**
     * Method setAccountsAuditEntry to save Accounts js logs into Accounts module
     * 
     * @param object $bean record sugar object [Sugar default]
     * @param object $event hook event [Sugar default]
     * @param array $arguments required parameters passed by framework [Sugar default]
     * @author sagar.salunkhe
     * @since Nov 25, 2015
     */
    function setAccountsAuditEntry($bean, $event, $arguments)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'event : '.print_r($event,true).GCM_GL_LOG_VAR_SEPARATOR.'arguments : '.print_r($arguments,true), 'Method to save Accounts js logs into Accounts module');
        $obj_accounts_bean = BeanFactory::getBean('Contacts');
        $obj_accounts_bean->retrieve($bean->contact_id_c);
        
        $obj_accounts_bean->load_relationship('contacts');
        $beanAddr1 = ((!empty($bean->addr1))?$bean->addr1:'');
        $beanAddr2 = ((!empty($bean->addr2))?$bean->addr2:'');
        
        $beanFetchedAddr1 = ((!empty($bean->fetched_row['addr1']))?$bean->fetched_row['addr1']:'');
        $beanFetchedAddr2 = ((!empty($bean->fetched_row['addr2']))?$bean->fetched_row['addr2']:'');
        
        if ($beanAddr1 != $beanFetchedAddr1) {
            // create an array to audit changes in the calls module's audit table
            $audit_entry = array();
            $audit_entry['field_name'] = 'ndb_addr1';
            $audit_entry['data_type'] = 'text';
            $audit_entry['before'] = $beanFetchedAddr1;
            $audit_entry['after'] = $beanAddr1;
            // save audit entry
            $obj_accounts_bean->db->save_audit_records($obj_accounts_bean, $audit_entry);
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'audit_entry : '.print_r($audit_entry,true), 'save audit entry');
        }
        
        if ($beanAddr2 != $beanFetchedAddr2) {
            // create an array to audit changes in the calls module�s audit table
            $audit_entry = array();
            $audit_entry['field_name'] = 'ndb_addr2';
            $audit_entry['data_type'] = 'text';
            $audit_entry['before'] = $beanFetchedAddr2;
            $audit_entry['after'] = $beanAddr2;
            // save audit entry
            $obj_accounts_bean->db->save_audit_records($obj_accounts_bean, $audit_entry);
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'audit_entry : '.print_r($audit_entry,true), 'save audit entry');
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__,'', '');
    }
}
?>