<?php
$popupMeta = array (
    'moduleMain' => 'js_Accounts_js',
    'varName' => 'js_Accounts_js',
    'orderBy' => 'js_accounts_js.name',
    'whereClauses' => array (
  'name' => 'js_accounts_js.name',
  'accounts_id' => 'js_accounts_js.accounts_id',
  'addr1' => 'js_accounts_js.addr1',
  'addr2' => 'js_accounts_js.addr2',
  'favorites_only' => 'js_accounts_js.favorites_only',
  'created_by_name' => 'js_accounts_js.created_by_name',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'accounts_id',
  5 => 'addr1',
  6 => 'addr2',
  7 => 'favorites_only',
  8 => 'created_by_name',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'accounts_id' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_ACCOUNTS_ID',
    'id' => 'CONTACT_ID_C',
    'link' => true,
    'width' => '10%',
    'name' => 'accounts_id',
  ),
  'addr1' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'LBL_ADDR1',
    'sortable' => false,
    'width' => '10%',
    'name' => 'addr1',
  ),
  'addr2' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'LBL_ADDR2',
    'sortable' => false,
    'width' => '10%',
    'name' => 'addr2',
  ),
  'favorites_only' => 
  array (
    'name' => 'favorites_only',
    'label' => 'LBL_FAVORITES_FILTER',
    'type' => 'bool',
    'width' => '10%',
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'name' => 'created_by_name',
  ),
),
);
