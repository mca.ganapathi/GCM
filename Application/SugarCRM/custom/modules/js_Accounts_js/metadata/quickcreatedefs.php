<?php
$module_name = 'js_Accounts_js';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'accounts_id',
            'studio' => 'visible',
            'label' => 'LBL_ACCOUNTS_ID',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'addr1',
            'studio' => 'visible',
            'label' => 'LBL_ADDR1',
          ),
          1 => 
          array (
            'name' => 'addr2',
            'studio' => 'visible',
            'label' => 'LBL_ADDR2',
          ),
        ),
        2 => 
        array (
          0 => 'assigned_user_name',
          1 => 
          array (
            'name' => 'team_name',
            'displayParams' => 
            array (
              'display' => true,
            ),
          ),
        ),
      ),
    ),
  ),
);
?>