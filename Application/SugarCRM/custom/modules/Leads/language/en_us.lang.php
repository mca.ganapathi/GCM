<?php
// created: 2015-09-08 12:57:43
$mod_strings = array (
  'LBL_REPORTS_TO' => 'Reports To:',
  'LBL_CONTACTS' => 'Accounts',
  'LBL_ACCOUNT' => 'Corporate',
  'LBL_LEADS' => 'Leads',
  'LBL_CONTACT_ID' => 'Account ID',
  'LBL_CONVERTED_CONTACT' => 'Converted Account:',
  'LBL_ACCOUNT_DESCRIPTION' => 'Corporate Description',
  'LBL_ACCOUNT_ID' => 'Corporate ID',
  'LBL_ACCOUNT_NAME' => 'Corporate Name:',
  'LBL_LIST_ACCOUNT_NAME' => 'Corporate Name',
  'LBL_CONVERTED_ACCOUNT' => 'Converted Corporate:',
  'LNK_SELECT_ACCOUNTS' => ' OR Select Corporate',
  'LNK_NEW_ACCOUNT' => 'Create Corporate',
);