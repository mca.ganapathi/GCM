<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement ('MSA'), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
class gc_commentsViewEdit extends ViewEdit
{

    function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'constructor');
        // call parent ViewEdit method
        parent::__construct();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    function display()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'display');
        // call parent display method
        // parent::display();
        $this->restrictCreateAction();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * restrict create action on Comments Log EditView
     * 
     * @author dinesh.itkar
     *         @date 25 Feb 2016
     */
    function restrictCreateAction()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', 'restrict create action on Comments Log EditView');
        // display error message
        $display_txt = '<br><div id="div_related_values"><div><span style="color:red;">Action Prohibited: Cannot Create Comments from Sugar Application.</span></h4></div><div><br>';
        echo $display_txt;
        echo $this->ev->display($this->showTitle);
        echo '<script>$("#SAVE_HEADER").hide(); $( ".action_buttons" ).last().css( "display", "none" ); $(".moduleTitle").hide(); $("#CANCEL_HEADER").val("Back");$("#EditView_tabs,#detailpanel_30,#btn_view_change_log").hide();</script>';
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}