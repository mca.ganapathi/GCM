<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/pi_product_item/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['pi_product_item']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/pi_product_item/Ext/Vardefs/pm_products_pi_product_item_pi_product_item.php

// created: 2016-02-18 07:06:47
$dictionary["pi_product_item"]["fields"]["pm_products_pi_product_item"] = array (
  'name' => 'pm_products_pi_product_item',
  'type' => 'link',
  'relationship' => 'pm_products_pi_product_item',
  'source' => 'non-db',
  'module' => 'pm_Products',
  'bean_name' => 'pm_Products',
  'vname' => 'LBL_PM_PRODUCTS_PI_PRODUCT_ITEM_FROM_PM_PRODUCTS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/pi_product_item/Ext/Vardefs/sugarfield_introduction_date.php

 // created: 2016-09-29 04:48:38
$dictionary['pi_product_item']['fields']['introduction_date']['audited'] = true;
$dictionary['pi_product_item']['fields']['introduction_date']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/pi_product_item/Ext/Vardefs/sugarfield_name.php

 // created: 2016-09-29 04:48:38
$dictionary['pi_product_item']['fields']['name']['len'] = '100';
$dictionary['pi_product_item']['fields']['name']['unified_search'] = false;
$dictionary['pi_product_item']['fields']['name']['calculated'] = false;
$dictionary['pi_product_item']['fields']['name']['audited'] = true;
$dictionary['pi_product_item']['fields']['name']['full_text_search']['enabled'] = true;
$dictionary['pi_product_item']['fields']['name']['full_text_search']['searchable'] = true;
$dictionary['pi_product_item']['fields']['name']['full_text_search']['boost'] = 1.5500000000000000444089209850062616169452667236328125;


?>
<?php
// Merged from custom/Extension/modules/pi_product_item/Ext/Vardefs/sugarfield_product_item_id.php

 // created: 2016-09-29 04:48:38
$dictionary['pi_product_item']['fields']['product_item_id']['len'] = '16';
$dictionary['pi_product_item']['fields']['product_item_id']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/pi_product_item/Ext/Vardefs/sugarfield_product_status.php

 // created: 2016-09-29 04:48:38
$dictionary['pi_product_item']['fields']['product_status']['audited'] = true;
$dictionary['pi_product_item']['fields']['product_status']['full_text_search']['boost'] = 1;


?>
