<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/pi_product_item/Ext/Layoutdefs/pm_products_pi_product_item_pi_product_item.php

 // created: 2016-02-18 07:06:47
$layout_defs["pi_product_item"]["subpanel_setup"]['pm_products_pi_product_item'] = array (
  'order' => 100,
  'module' => 'pm_Products',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PM_PRODUCTS_PI_PRODUCT_ITEM_FROM_PM_PRODUCTS_TITLE',
  'get_subpanel_data' => 'pm_products_pi_product_item',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/pi_product_item/Ext/Layoutdefs/removeSubpanelCustom.php

/*** START : Remove unnecessary subpanels that we can't hide from Admin Panel ***/
unset($layout_defs["pi_product_item"]["subpanel_setup"]['pm_products_pi_product_item']['top_buttons'][0]);			//unset create button of contracts subpanel
unset($layout_defs["pi_product_item"]["subpanel_setup"]['pm_products_pi_product_item']['top_buttons'][1]);			//unset select button of contracts subpanel
/*** END : Remove unnecessary subpanels that we can't hide from Admin Panel ***/
 
?>
<?php
// Merged from custom/Extension/modules/pi_product_item/Ext/Layoutdefs/_overridepi_product_item_subpanel_pm_products_pi_product_item.php

//auto-generated file DO NOT EDIT
$layout_defs['pi_product_item']['subpanel_setup']['pm_products_pi_product_item']['override_subpanel_name'] = 'pi_product_item_subpanel_pm_products_pi_product_item';

?>
