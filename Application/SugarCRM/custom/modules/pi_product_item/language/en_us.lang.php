<?php
// created: 2015-12-24 05:17:32
$mod_strings = array (
  'LBL_EDITVIEW_PANEL1' => 'Attribute Order List',
  'LBL_EDITVIEW_PANEL2' => 'New Panel 2',
  'LBL_APPROVE' => 'Product Approved',
  'LBL_CANNOT_APPROVE' => 'Missing Values for below fields, Please edit and fill the values!!',
  'LBL_ALERT_DATE_VALIDATION' => ' should be greater or equal to ',
  'LBL_PLZ_SELECT' => 'Please select at least one attribute in order to save record',
  'LBL_DETAILVIEW_PANEL1' => 'Attribute List',
  'LBL_ATTRIBUTE_ITEM' => 'Attribute',
  'LBL_DENY_DELETE' => 'Record cannot be deleted as it has status: "Approved"',
  'LBL_NOTHING_TO_DISPLAY' => 'No Attribute Selected!!',
  'LBL_DENY_EDIT' => 'Record cannot be edited as it has status: "Approved"',
  'LBL_PRODUCT_ITEM_ID' => 'Product Item ID',
  'LBL_NAME' => 'Product Item Name',
  'LBL_DUPLICATE' => 'Duplicate Product Item Name!',
  'LBL_PM_PRODUCTS_PI_PRODUCT_ITEM_FROM_PM_PRODUCTS_TITLE' => 'Products',
  'LBL_ID' => 'Object ID',
  'LBL_PRODUCT_STATUS' => 'Product Status',
  'LBL_INTRODUCTION_DATE' => 'Product Item Introduction Date',
  'LBL_SALES_END_DATE' => 'Product Item Sales End Date',
);