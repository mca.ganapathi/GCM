<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (?MSA?), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
/**
 * : ClassProductItems Utils Class
 * @author : Shrikant.Gaware
 *         @date : 30-Sep-2015
 */
class ClassProductItems extends ModUtils
{

    /* Class Variable */
    var $db;

    /* Class Variable */
    
    /**
     * : Constructor Method
     * @param
     *        :
     *        
     * @return :
     * @author : Sagar. Salunkhe
     *         @date : 30-Sep-2015
     */
    function __construct()
    {
        global $db;
        $this->db = $db;
    }

    /**
     * : Method to get related attributes to Product Items
     * @param
     *        :
     *        <string> $AttributeId
     * @return : <array> $res
     * @author : Sagar. Salunkhe
     *         @date : 30-Sep-2015
     */
    function getAttributeRelatedProducts($AttributeId)
    {
        $response_code = 1;
        $response_msg = 'Success!';
        
        $obj_product_attributes = BeanFactory::getBean('pi_product_item_attribute');
        $where = " pi_product_item_attribute.is_enabled = 1
				   AND pi_product_item_attribute.is_sub_attribute = 0 
				   AND pi_product_item_attribute. pa_productattributes_id_c='$AttributeId' ";
        $array_obj_prod_att = $obj_product_attributes->get_full_list('attribute_order', $where);
        
        $array_count_modules = array();
        if ($array_obj_prod_att != NULL) {
            
            if (count($array_obj_prod_att) > 0) $array_count_modules['pi_product_item'] = count($array_obj_prod_att);
            
            $response_code = 0;
            $response_msg = 'Sorry! You cannot delete this record as it is used in below Modules!';
            $array_obj_prod_att = count($array_obj_prod_att);
            $array_count_modules = $array_count_modules;
        }
        
        $res = array(
            'ResponseCode' => $response_code,
            'ResponseMsg' => $response_msg,
            'CountRelatedModules' => count($array_obj_prod_att),
            'ArrayRelatedModuleRecords' => $array_count_modules
        );
        return $res;
    }

    /**
     * : Method to get related attributes in Id=>Name pair
     * @param
     *        :
     *        <string> $module
     *        <array> $ids
     *        <string> $where
     * @return : <array> $list array('id'=>'name')
     * @author : Shrikant.Gaware
     *         @date : 30-Sep-2015
     */
    public function getAttributeIdNamePair($module, $ids = null, $where = '')
    {
        $GLOBALS['log']->info("Begin: ClassProductItems->getAttributeIdNamePair()");
        $list = array();
        $bean = BeanFactory::getBean($module);
        if (isset($ids)) {
            foreach ($ids as $id) {
                $temp = @clone ($bean);
                
                $temp->retrieve($id);
                $list[$temp->id] = $temp->name;
            }
        } else {
            $bean_list = $bean->get_full_list('name', $where);
            if (!empty($bean_list)) {
                foreach ($bean_list as $modBean) {
                    $list[$modBean->id] = $modBean->name;
                }
            }
        }
        $GLOBALS['log']->info("End: ClassProductItems->getAttributeIdNamePair()");
        return $list;
    }

    /**
     * : Method to get Attributes related to product
     * @param
     *        :
     *        <array> $id [Product ID]
     * @return : <array> $attribute array('id'=>'name')
     * @author : Shrikant.Gaware
     *         @date : 30-Sep-2015
     */
    public function getAttributes($id, $falg = NULL)
    {
        $GLOBALS['log']->info("Begin: ClassProductItems->getAttributes($id)");
        $attribute = array();
        $pa_bean = BeanFactory::getBean('pi_product_item_attribute');
        $where = " pi_product_item_attribute.is_enabled = 1
				   AND pi_product_item_attribute.is_sub_attribute = 0 
				   AND pi_product_item_attribute.pi_product_item_id_c='$id' ";
        $bean_list = $pa_bean->get_full_list('attribute_order', $where);
        if ($bean_list != NULL) {
            foreach ($bean_list as $bean) {
				if(!in_array($bean->pa_productattributes_id_c,$attribute))
					$attribute[] = $bean->pa_productattributes_id_c;
            }
        }		
        if ($falg == 1) {
            $GLOBALS['log']->info("End: ClassProductItems->getAttributes($id)");
            return $attribute;
        }
        
        $attribute_list = array();
        if (! empty($attribute)) $attribute_list = $this->getAttributeIdNamePair('pa_ProductAttributes', $attribute);
        
        $GLOBALS['log']->info("End: ClassProductItems->getAttributes($id)");
        return $attribute_list;
    }

    /**
     * : Method [Recursive] to build a array tree of Attribute and Attribute Values
     * @param
     *        :
     *        <int> $index
     *        <string> $attributeid
     *        <array> $node
     * @return : <array> $attribute array('label'=>'id')
     * @author : Shrikant.Gaware
     *         @date : 30-Sep-2015
     */
    function getChildAttributes($index, $attributeid, $node)
    {
        $GLOBALS['log']->info("Begin: ModUtils->getChildAttributes($index, $attributeid, " . print_r($node, true) . ")");
        include_once ('custom/modules/pa_ProductAttributes/include/ClassProductAttributes.php');
        $obj_class_prod_attributes = new ClassProductAttributes();
        $obj_array_parent_att = array();
        $obj_array_parent_att = $obj_class_prod_attributes->getChildAttributes($attributeid);
        if ($obj_array_parent_att != NULL) {
            $i = 0;
            foreach ($obj_array_parent_att as $key => $value) {
                $label = $value->name . '@X@[' . $value->id . ']';
                $node[$i] = array(
                    'label' => $label,
                    'id' => $value->id,
                    'children' => $this->getChildAttributes($i, $value->id, $node[$i]['children'])
                );
                $i ++;
            }
        }
        
        $childs = $obj_class_prod_attributes->getAttributeValueLineItems($attributeid);
        if ($childs != NULL) {
            $i = 0;
            foreach ($childs as $key => $value) {
                $label = $value->name . '@X@[' . $value->id . ']';
                $node[$i] = array(
                    'label' => $label,
                    'id' => $value->id
                );
                $i ++;
            }
        }
        $GLOBALS['log']->info("End: ModUtils->getChildAttributes()");
        return $node;
    }

    /**
     * : Method get the total number of records inside the table
     * @param
     *        :
     *        <string> $tablename
     *        <string> $where
     * @return : <int> $count
     * @author : Shrikant.Gaware
     *         @date : 05-Oct-2015
     */
    function getTotalCount($tablename, $where)
    {
        $GLOBALS['log']->info("Begin: ClassProductItems->getTotalCount($tablename)");
        $count = 0;
        $query = "SELECT COUNT(id) AS rowcount FROM $tablename $where LIMIT 1";
        $result = $this->db->query($query);
        $row_count = $this->db->getRowCount($result);
        if ($row_count > 0) {
            $count = $this->db->fetchByAssoc($result);
            $count = $count['rowcount'];
        }
        $GLOBALS['log']->info("End: ClassProductItems->getTotalCount($tablename)");
        return $count;
    }
}
?>