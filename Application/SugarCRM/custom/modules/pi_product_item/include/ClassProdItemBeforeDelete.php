<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
/**
 * : ClassProdItemBeforeDelete Module LOGIC HOOK Class for executing hooks
 * @author : Shrikant.Gaware
 *         @date : 30-Sep-2015
 */
class ClassProdItemBeforeDelete
{

    /**
     * : Method logic hook before record delete action is perform
     * @param <object> $bean
     *        <string> $event
     *        <array> $arguments
     * @return :
     * @author : Shrikant.Gaware
     *         @date : 30-Sep-2015
     */
    function checkProductItemBeforeDelete($bean, $event, $arguments)
    {
        $GLOBALS['log']->info("Begin: logic_hooks_class->checkProductItemBeforeDelete(" . print_r($bean, true) . " , $event, " . print_r($arguments, true) . " )");
        global $mod_strings;
        if ($bean->product_status == 'Approved') {
            $redirect_action = (isset($_REQUEST['record'])) ? 'DetailView' : 'ListView';
            $queryParams = array(
                'module' => 'pi_product_item',
                'action' => $redirect_action,
                'record' => $arguments['id'],
                'offset' => $_REQUEST['offset'],
                'stamp' => $_REQUEST['stamp'],
                'return_module' => $_REQUEST['return_module']
            );
            SugarApplication::appendErrorMessage('"' . $bean->name . '" ' . $mod_strings['LBL_DENY_DELETE']);
            $GLOBALS['log']->info("End: logic_hooks_class->checkProductItemBeforeDelete(" . print_r($bean, true) . " , $event, " . print_r($arguments, true) . " )");
            SugarApplication::redirect('index.php?' . http_build_query($queryParams));
        }
        $GLOBALS['log']->info("End: logic_hooks_class->checkProductItemBeforeDelete(" . print_r($bean, true) . " , $event, " . print_r($arguments, true) . " )");
    }
}
?>