<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

class ClassProdItemAfterSave
{
    /**
     * method to Save selected attributes with the product item record.
     *
     * @author Debasish.Gupta
     * @param (object)$bean - record sugar object [Sugar default]
     *        (object)$event - hook event [Sugar default]
     *        (array)$arguments - required parameters passed by framework [Sugar default]
     * @return (void method)
     *         @date 20-Oct-2016
     */
    function saveProdItemAttributes($prodItembean, $event, $arguments)
    {
        $GLOBALS['log']->debug("Begin: ClassProdItemAfterSave->saveProdItemAttributes.");
        $product_attribute = array();
        $product_attribute_order = array();
        if (!empty($_POST['product_attribute'])) {
            $product_attribute = explode("^", $_POST['product_attribute']);
        }
        if (!empty($_POST['product_attribute_order'])) {
            $product_attribute_order = explode("^", $_POST['product_attribute_order']);
        }
		

		$attribute_list = array();
		if (! empty($product_attribute)) {
			foreach ($product_attribute as $k => $v) {
				$attribute_list[$v] = $product_attribute_order[$k];
			}
		}
		$prodItembeanId = ((!empty($prodItembean->id))?$prodItembean->id:'');
		$where = " pi_product_item_attribute.is_sub_attribute = 0 
				   AND pi_product_item_attribute.pi_product_item_id_c = '".$prodItembeanId."' ";
		$paBean = BeanFactory::getBean('pi_product_item_attribute');
		$tempBean = @clone ($paBean);
		$beanList = $paBean->get_full_list('', $where);

		if ($beanList != NULL) {
			foreach ($beanList as $beanitem) {
				$beanitem->pi_product_item_id_c = $prodItembeanId;
				if (array_key_exists($beanitem->pa_productattributes_id_c, $attribute_list)) {
					$beanitem->attribute_order = $attribute_list[$beanitem->pa_productattributes_id_c];
					$beanitem->is_sub_attribute = 0;
					$beanitem->is_enabled = 1;
				} else {
					$beanitem->is_enabled = 0;
				}
				$beanitem->save();
				$attribute_list[$beanitem->pa_productattributes_id_c] = '';
				unset($attribute_list[$beanitem->pa_productattributes_id_c]);
			}
		}

		if (! empty($attribute_list)) {
			foreach ($attribute_list as $key => $value) {
				if ($value != '' && $key != '') {					
					$prodItemAttrbean = @clone ($tempBean);
					$prodItemAttrbean->assigned_user_id = ((!empty($_POST['assigned_user_id']))?$_POST['assigned_user_id']:'');
					$prodItemAttrbean->pi_product_item_id_c = $prodItembeanId;
					$prodItemAttrbean->pa_productattributes_id_c = $key;
					$prodItemAttrbean->attribute_order = $value;
					$prodItemAttrbean->is_sub_attribute = 0;
					$prodItemAttrbean->is_enabled = 1;
					$prodItemAttrbean->save();
				}
			}
		}		
        $GLOBALS['log']->debug("End: ClassProdItemAfterSave->saveProdItemAttributes.");
    }
}
?>