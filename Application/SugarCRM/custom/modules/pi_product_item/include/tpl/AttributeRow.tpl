{*
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

*}
<table>
	<tr>
		<td valign="top">
			<select id="MasterSelectBox" name="MasterSelectBox" multiple style="width: 210px;" size="10">
				{foreach from=$UNSELECTEDATTRIBUTES key=k item=v}
					<option value="{$k}">{$v}</option>
				{/foreach}
			</select>
		</td>
		<td valign="top">
			<br />
			<input type="button" id="btnAdd" name="btnAdd" value=">" class="toggle_rl"/><br /><br />
			<input type="button" id="btnRemove" name="btnRemove" value="<" class="toggle_rl"/>
		</td>
		<td valign="top">
			<select id="PairedSelectBox" name="PairedSelectBox" multiple style="width: 210px;" size="10">
			</select>
		</td>
		<td valign="top">
			<br />
			<input type="button" id="btnUp" name="btnUp" value="&uarr;" class="toggle_ud"/><br /><br />
			<input type="button" id="btnDown" name="btnDown" value="&darr;" class="toggle_ud" />
		</td>
	</tr>
</table>