{*
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

*}
<link rel="stylesheet" href="custom/modules/pi_product_item/include/css/jqtree.css" />
{if empty($ATTRIBUTES)}
	<div style="color: red; text-align: center; padding: 5px 0px;margin-top: 10px;">
		{sugar_translate label='LBL_NOTHING_TO_DISPLAY' module='pi_product_item'}
	</div>
{else}
	<link rel="stylesheet" href="custom/include/assests/css/bootstrap.min.css" />
	<link rel="stylesheet" href="custom/include/assests/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" href="custom/include/assests/css/font-awesome.min.css">
	<link rel="stylesheet" href="custom/modules/pi_product_item/include/css/jqtree.css" />
	<style>
		{literal}
		#dcmenuSugarCube .notifCount{
			height: 24px !important;
		}
		#glblSearchBtn{
			top: 0px !important;
			width: 45px !important;
		}	
		{/literal}
	</style>
	<div class='cn_container'>
		<ul class="list-group" style="margin-left:0px">
			{foreach from=$ATTRIBUTES key=k item=v}
			<li class="list-group-item" style="margin-left: 0px;" data={$k}>
				<div class="row">
				    <div class="col-xs-10">
					  <strong>{$v}</strong>	  
				    </div>
				    <div class="col-xs-2">
						<i id="arrow-id-{$rowid}" class="fa fa-chevron-down pull-right"></i>
					</div>
				</div>
				<div class = 'hide scrollContainer treeView'>
				</div>
			</li>
			{/foreach}
		</ul>	
	</div>
{/if}