/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/
/**
 * @desc : Function to init DOM objects on JQuery Document Load
 * @author : Shrikant.Gaware
 * @date : 30-Sep-2015
 */
$(document)
		.ready(
				function() {
					/**
					 * @desc : Called Plug-in Method to Map and Sync master and
					 *       paired Select Box
					 * @author : Shrikant.Gaware
					 * @date : 30-Sep-2015
					 */
					$('#MasterSelectBox').pairMaster();
					/**
					 * @desc : Code for click event on right and left button
					 * @author : Shrikant.Gaware
					 * @date : 30-Sep-2015
					 * 
					 */
					$('.toggle_rl').click(
							function() {
								$this = $(this);
								($this.val() == '>') ? $('#MasterSelectBox')
										.addSelected('#PairedSelectBox') : $(
										'#PairedSelectBox').removeSelected(
										'#MasterSelectBox');
								$('#MasterSelectBox option').removeAttr(
										'selected');
								appendAttributeList();
							});
					/**
					 * @desc : Code for click event on up and down button
					 * @author : Shrikant.Gaware
					 * @date : 30-Sep-2015
					 * 
					 */
					$('.toggle_ud')
							.click(
									function() {
										var $op = $('#PairedSelectBox option:selected'), $this = $(this);
										if ($op.length) {
											($this.val() == '↑') ? $op.first()
													.prev().before($op) : $op
													.last().next().after($op);
										}
									});
					/**
					 * @desc : Code called on load once for mapping the Master &
					 *       Paired Select Box for Edit Record
					 * @author : Shrikant.Gaware
					 * @date : 30-Sep-2015
					 * 
					 */
					record = $('[name="record"]');
					if (record.val() != '') {
						var attStr = $('#product_attribute').val();
						if (attStr != '') {
							var attArray = $('#product_attribute').val().split(
									"^");
							for (i = 0; i < attArray.length; i++) {
								$("#MasterSelectBox option").each(function() {
									$this = $(this);
									if ($this.val() == attArray[i]) {
										$this.attr('selected', true);
									}
								});
							}
							$('#MasterSelectBox').addSelected(
									'#PairedSelectBox');
						}

						var product_status = $('#product_status').val();
						if (product_status == 'Approved') {
							var introduction_date = SUGAR.language.get(
									'pi_product_item', 'LBL_INTRODUCTION_DATE');
							var sales_end_date = SUGAR.language.get(
									'pi_product_item', 'LBL_SALES_END_DATE');

							addToValidate('EditView', 'introduction_date',
									'date', true, introduction_date);
							$('#introduction_date_label').html(
									introduction_date
											+ ': <font color="red">*</font>');

							addToValidate('EditView', 'sales_end_date', 'date',
									true, sales_end_date);
							$('#sales_end_date_label').html(
									sales_end_date
											+ ': <font color="red">*</font>');
						}
					}
					/**
					 * @desc : Function to override form save on click even and
					 *       add custom validation
					 * @author : Shrikant.Gaware
					 * @date : 30-Sep-2015
					 * 
					 */
					$('#SAVE_HEADER,#SAVE_FOOTER').on('click', function(e) {
						e.preventDefault();
						if (cstmValidation()) {
							var _form = document.getElementById('EditView');
							_form.action.value = 'Save';
							if (check_form('EditView') && ValidateSave())
								SUGAR.ajaxUI.submitForm(_form);
						}
						return false;
					});
				});

/**
 * @desc : Function to build cap separated Attribute Id string
 * @params :
 * @return :
 * @author : Shrikant.Gaware
 * @date : 30-Sep-2015
 */
function appendAttributeList() {
	$('#PairedSelectBox option').attr('selected', true);
	var attributes = $('#PairedSelectBox').val();
	var attList = '';
	var attListOrder = '';
	if (attributes != null) {
		attList = $('#PairedSelectBox').val().join("^");
		for (i = 1; i <= attributes.length; i++)
			attListOrder = attListOrder + '^' + i;
		while (attListOrder.charAt(0) === '^')
			attListOrder = attListOrder.substr(1);
	}
	document.getElementById('product_attribute').value = attList;
	document.getElementById('product_attribute_order').value = attListOrder;
}

/**
 * @desc : Function to calculate difference between two supplied dates
 * @params : <string> - startD <string> - EndD
 * @return : <int> - _diffDays
 * @author : Shrikant.Gaware
 * @date : 30-Sep-2015
 */
function diffDays(startD, EndD) {
	startD = startD.replace(/-|\./gi, "/");
	EndD = EndD.replace(/-|\./gi, "/");
	var NstartD = DateFormat(startD);
	var NEndD = DateFormat(EndD);
	var date1 = Math.round(new Date(NEndD).getTime() / 1000);
	var date2 = Math.round(new Date(NstartD).getTime() / 1000);
	var daylen = 60 * 60 * 24;
	var _diffDays = ((date1) - (date2)) / daylen;
	return _diffDays;
}

/**
 * @desc : Function to format date in Y-m-d
 * @params : <string> - dt
 * @return : <string> - yyyy+'-'+mm+'-'+dd
 * @author : Shrikant.Gaware
 * @date : 30-Sep-2015
 */
function DateFormat(dt) {
	var ymd_date = '';
	$.ajax({
		type : "POST",
		url : "index.php",
		data : {
			module : 'pi_product_item',
			action : 'Actions',
			method : 'asDbDateFormat',
			sugar_body_only : '1',
			date : dt,
		},
		async : false,
		success : function(data) {
			var result = $.parseJSON(data);
			if (result.hasOwnProperty('error') || $.isEmptyObject(result))
				return;
			else if (result.date != '')
				ymd_date= result.date;
		},
		error : function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status);
			alert(thrownError);
		}
	});
	return ymd_date;
}

/**
 * @desc : Function to check duplicate product name
 * @params :
 * @return : <boolean> true/false
 * @author : Shrikant.Gaware
 * @date : 30-Sep-2015
 */
function checkDuplicateValidation() {
	SUGAR.ajaxUI.showLoadingPanel();
	var valid = false;
	var record = $('[name="record"]').val();
	var name = $('#name').val();
	$.ajax({
		type : "POST",
		url : "index.php",
		data : {
			module : 'pi_product_item',
			action : 'Actions',
			method : 'validateProductItemName',
			sugar_body_only : '1',
			record : record,
			name : name,
		},
		async : false,
		success : function(data) {
			var result = $.parseJSON(data);
			if (result.hasOwnProperty('error') || $.isEmptyObject(result))
				valid = true;
			else if (result.exist == '1')
				valid = true;
		},
		error : function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status);
			alert(thrownError);
		}
	});
	SUGAR.ajaxUI.hideLoadingPanel();
	return valid;
}

/**
 * @desc : Function to check added custom validations
 * @params :
 * @return : <boolean> true/false
 * @author : Shrikant.Gaware
 * @date : 30-Sep-2015
 */
function cstmValidation() {
	appendAttributeList();
	var startD = document.getElementById("introduction_date").value;
	var EndD = document.getElementById("sales_end_date").value;
	var product_status = document.getElementById("product_status").value;
	var attributes = document.getElementById("PairedSelectBox").value;

	clear_all_errors();
	if (checkDuplicateValidation()) {
		add_error_style('EditView', 'name', SUGAR.language.get(
				'pi_product_item', 'LBL_DUPLICATE'), true);
		return false;
	}

	if (product_status == 'Approved' && attributes == '') {
		add_error_style('EditView', 'product_status', SUGAR.language.get(
				'pi_product_item', 'LBL_PLZ_SELECT'), true);
		return false;
	}

	if (startD != '' && EndD != '') {
		var difference = diffDays(startD, EndD);
		if (difference < 0) {
			var msg = SUGAR.language.get('pi_product_item',
					'LBL_SALES_END_DATE')
					+ SUGAR.language.get('pi_product_item',
							'LBL_ALERT_DATE_VALIDATION')
					+ SUGAR.language.get('pi_product_item',
							'LBL_INTRODUCTION_DATE');
			add_error_style('EditView', 'sales_end_date', msg, true);
			return false;
		}
	}
	return check_form('EditView');
}