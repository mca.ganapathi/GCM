/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/
/**
 * @desc : Function to init DOM objects on JQuery Document Load
 * @author : Shrikant.Gaware
 * @date : 30-Sep-2015
 */
$(document)
		.ready(
				function() {
					var attributeTree = [];
					var container = $('.cn_container');

					/**
					 * @desc : Code for open hide tree
					 * @author : Shrikant.Gaware
					 * @date : 30-Sep-2015
					 */
					container.find('.row').click(
							function() {
								$this = $(this);
								$parent = $this.parent();
								container.find('.treeView').addClass('hide');
								SUGAR.ajaxUI.showLoadingPanel();
								displayTreeStructure($parent);
								SUGAR.ajaxUI.hideLoadingPanel();
								var node = $parent.find('.fa-chevron-down');
								if (node.length == 1) {
									container.find('.fa-chevron-up').addClass(
											'fa-chevron-down').removeClass(
											'fa-chevron-up');
									$parent.find('.fa-chevron-down').addClass(
											'fa-chevron-up').removeClass(
											'fa-chevron-down');
									$parent.find('.treeView').removeClass(
											'hide');
								} else {
									container.find('.fa-chevron-up').addClass(
											'fa-chevron-down').removeClass(
											'fa-chevron-up');
									$parent.find('.treeView').addClass('hide');
								}

							});

					/**
					 * @desc : Function to fetch Attribute Details [JSON Format]
					 * @params : <string> attributeid
					 * @return : <int> array index
					 * @author : Shrikant.Gaware
					 * @date : 30-Sep-2015
					 */
					function fetchData(attributeid) {
						record = $('[name="record"]').val();
						$.ajax({
							type : "POST",
							url : "index.php",
							data : {
								module : 'pi_product_item',
								action : 'Actions',
								method : 'getAttributes',
								sugar_body_only : '1',
								record : record,
								attributeid : attributeid
							},
							async : false,
							success : function(data) {
								var result = $.parseJSON(data);
								if (result.hasOwnProperty('error')
										|| $.isEmptyObject(result))
									attributeTree.push('@@');
								else
									attributeTree.push(result);
							},
							error : function(xhr, ajaxOptions, thrownError) {
								alert(xhr.status);
								alert(thrownError);
							}
						});
						return attributeTree.length - 1;
					}

					/**
					 * @desc : Function to display Tree structure
					 * @params : <jQuery object> node
					 * @return :
					 * @author : Shrikant.Gaware
					 * @date : 30-Sep-2015
					 */
					function displayTreeStructure(node) {
						if (!node.hasClass('loaded')) {
							var index = fetchData(node.attr('data'));
							if (attributeTree[index] == '@@') {
								node
										.find('.treeView')
										.html(
												'<h4>'
														+ SUGAR.language
																.get(
																		'pi_product_item',
																		'LBL_NOTHING_TO_DISPLAY')
														+ '</h4>');
							} else {
								var disptree = node.find('.treeView');
								disptree.tree({
									data : attributeTree[index],
									autoOpen : true
								});
								node.addClass('loaded');
								var n = disptree.find('.jqtree-title');
								n
										.each(function(index, dom) {
											var jqDom = $(dom);
											var res = jqDom.text().split("@X@");
											var str = res[0]
													+ '&nbsp;&nbsp;&nbsp;&nbsp;<span class="gasmall">'
													+ res[1] + '</span>';
											jqDom.html(str);
										});
							}
						}
					}
				});
