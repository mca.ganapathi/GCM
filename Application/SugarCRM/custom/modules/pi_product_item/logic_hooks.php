<?php
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
$hook_version = 1;
$hook_array = Array();
$hook_array['after_save'][] = array(1,'Save attributes with product item','custom/modules/pi_product_item/include/ClassProdItemAfterSave.php','ClassProdItemAfterSave','saveProdItemAttributes',);
$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(
    // Processing index. For sorting the array.
    1,
    
    // Label. A string value to identify the hook.
    'before_delete check entity',
    
    // The PHP file where your class is located.
    'custom/modules/pi_product_item/include/ClassProdItemBeforeDelete.php',
    
    // The class the method is in.
    'ClassProdItemBeforeDelete',
    
    // The method to call.
    'checkProductItemBeforeDelete'
);

?>