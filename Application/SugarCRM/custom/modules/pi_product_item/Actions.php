<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
include_once ('custom/modules/pi_product_item/include/ClassProductItems.php');

/**
 * : Actions Class to Handle Ajax Request
 * @author : Shrikant.Gaware
 *         @date : 24-Sep-2015
 */
class Actions extends ClassProductItems
{

    /**
     * : Method to get Related Attributes and Sub Attributes and Attribute Values
     * @param
     *        :
     *        
     * @return :
     * @author : Shrikant.Gaware
     *         @date : 24-Sep-2015
     */
    function getAttributes()
    {
        $GLOBALS['log']->info("Begin: Actions->getAttributes()");
        $data = array();
        $record = ((!empty($_REQUEST['attributeid']))?$_REQUEST['attributeid']:'');
        $attribute_list = $this->getAttributeIdNamePair('pa_ProductAttributes', array(
            $record
        ));
        $i = 0;
        if (is_array($attribute_list) && count($attribute_list)>0) {
            foreach ($attribute_list as $attribute_id => $attribute_name) {
                $label = $attribute_name . '@X@[' . $attribute_id . ']';
                $data[$i] = array(
                    'label' => $label,
                    'id' => $attribute_id
                );
                $data[$i]['children'] = $this->getChildAttributes($i, $attribute_id, $data[$i]['children']);
                $i ++;
            }
        }
        $GLOBALS['log']->info("End: Actions->getAttributes()");
        $this->outputData($data);
    }

    /**
     * : Method to check for duplicate Product Item
     * @param
     *        :
     *        
     * @return : array ('exist' => '0|1')
     * @author : Shrikant.Gaware
     *         @date : 24-Sep-2015
     */
    function validateProductItemName()
    {
        $GLOBALS['log']->info("Begin: Actions->validateProductItemName()");
        $flag = 0;
        $check = ((!empty($_REQUEST['name']))?trim($_REQUEST['name']):'');
        $record = ((!empty($_REQUEST['record']))?$_REQUEST['record']:'');
        $where = " pi_product_item.name = '$check' AND pi_product_item.id != '$record' ";
        $bean = BeanFactory::getBean('pi_product_item')->get_list('pi_product_item.name', $where, null, 1);
        if (sizeof($bean['list']) > 0) {
            $flag = 1;
        }
        $GLOBALS['log']->info("End: Actions->validateProductItemName()");
        $this->outputData(array(
            'exist' => $flag
        ));
    }

    /**
     * : Method to date as DB format regardless user date format
     * @param
     *        :
     *        
     * @return : array ('exist' => '0|1')
     * @author : Shrikant.Gaware
     *         @date : 24-Sep-2015
     */
    function asDbDateFormat()
    {
        $GLOBALS['log']->info("Begin: Actions->asDbDateFormat()");
        $date = ((!empty($_REQUEST['date']))?trim($_REQUEST['date']):'');
        $dt = new SugarDateTime($date);
        $date = TimeDate::getInstance()->asDb($dt);
        $dt_arr = explode(' ', TimeDate::getInstance()->asDb($dt));
        $GLOBALS['log']->info("End: Actions->asDbDateFormat()");
        $this->outputData(array(
            'date' => ((!empty($dt_arr[0]))?$dt_arr[0]:'')
        ));
    }
}
/* Initialize action class object */
$a = new Actions();
$a->process();
/* Initialize action class object */
?>