<?php
$module_name = 'pi_product_item';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'form' => 
      array (
        'hidden' => 
        array (
          0 => '<input type="hidden" name="product_attribute" 
					id="product_attribute" value="{$SELECTEDATTRIBUTES}"/> 
				<input type="hidden" name="product_attribute_order" 
					id="product_attribute_order" value="{$ORDERATTRIBUTE}"/>
				<input type="hidden" name="product_status" 
					id="product_status" value="{$fields.product_status.value}"/>',
        ),
        'buttons' => 
        array (
          0 => 
          array (
            'customCode' => '<input type="submit" name="save" id="save" onClick="this.form.return_action.value=\'DetailView\'; 
								this.form.action.value=\'Save\'; return cstmValidation();" value="Save">',
          ),
          1 => 'CANCEL',
        ),
      ),
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'cache/include/javascript/sugar_grp_yui_widgets.js',
        ),
        1 => 
        array (
          'file' => 'custom/modules/pi_product_item/include/js/pair-select.min.js',
        ),
        2 => 
        array (
          'file' => 'custom/modules/pi_product_item/include/js/EditView.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
            'displayParams' => 
            array (
              'field' => 
              array (
                'autocomplete' => 'off',
              ),
            ),
          ),
          1 => 'assigned_user_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'introduction_date',
            'label' => 'LBL_INTRODUCTION_DATE',
            'displayParams' => 
            array (
              'field' => 
              array (
                'style' => 'width:210px',
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'sales_end_date',
            'label' => 'LBL_SALES_END_DATE',
            'displayParams' => 
            array (
              'field' => 
              array (
                'style' => 'width:210px',
              ),
            ),
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'product_type',
            'studio' => 'visible',
            'label' => 'LBL_PRODUCT_TYPE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'product_attribute',
            'studio' => 'visible',
            'label' => 'LBL_PRODUCT_ATTRIBUTE_LIST',
            'customCode' => '{include file=$ATTRIBCOMBINETPL}',
          ),
        ),
      ),
    ),
  ),
);
?>
