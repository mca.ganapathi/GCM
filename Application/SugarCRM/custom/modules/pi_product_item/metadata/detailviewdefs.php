<?php
$module_name = 'pi_product_item';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'FIND_DUPLICATES',
          3 => 
          array (
            'customCode' => '<input type="submit" value="{sugar_translate label=\'LBL_APPROVE\' module=\'pi_product_item\'}"
			 name="button" onclick="this.form.return_module.value=\'pi_product_item\'; this.form.return_action.value=\'DetailView\';this.form.return_id.value=\'{$fields.id.value}\'; this.form.action.value=\'approve\';" class="button" accesskey="A" title="Approve Product [Alt+A]" id="btn_approve_product" />',
          ),
          4 => 'DELETE',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/include/assests/js/bootstrap.min.js',
        ),
        1 => 
        array (
          'file' => 'custom/modules/pi_product_item/include/js/tree.jquery.js',
        ),
        2 => 
        array (
          'file' => 'custom/modules/pi_product_item/include/js/DetailView.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'id',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'introduction_date',
            'label' => 'LBL_INTRODUCTION_DATE',
          ),
          1 => 
          array (
            'name' => 'sales_end_date',
            'label' => 'LBL_SALES_END_DATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'product_status',
            'studio' => 'visible',
            'label' => 'LBL_PRODUCT_STATUS',
          ),
          1 => 
          array (
            'name' => 'product_item_id',
            'label' => 'LBL_PRODUCT_ITEM_ID',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 => 
          array (
            'name' => 'date_modified',
            'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
            'label' => 'LBL_DATE_MODIFIED',
          ),
        ),
        4 => 
        array (
          0 => 'assigned_user_name',
          1 => 
          array (
            'name' => 'product_type',
            'studio' => 'visible',
            'label' => 'LBL_PRODUCT_TYPE',
          ),
        ),
      ),
      'lbl_detailview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'product_attribute',
            'studio' => 'visible',
            'label' => 'LBL_PRODUCT_ATTRIBUTE_LIST',
            'customCode' => '{include file=$ATTRIBCOMBINETPL}',
            'hideLabel' => true,
          ),
        ),
      ),
    ),
  ),
);
?>
