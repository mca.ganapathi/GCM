<?php
// created: 2015-12-18 09:27:34
$subpanel_layout['list_fields'] = array (
  'product_item_id' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_PRODUCT_ITEM_ID',
    'width' => '20%',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '30%',
    'default' => true,
  ),
  'product_status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_PRODUCT_STATUS',
    'width' => '20%',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '20%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'width' => '10%',
    'vname' => 'LBL_REMOVE',
    'default' => true,
    'widget_class' => 'SubPanelRemoveButton',
  ),
);