<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
require_once ('custom/include/MVC/View/views/view.list.php');

/**
 * : pi_product_itemViewList ListView Class
 * @author : Shrikant.Gaware
 *         @date : 24-Sep-2015
 */
class pi_product_itemViewList extends CustomViewList
{

    /**
     * : Method to process data for List View before display
     * @param  :
     *
     * @return :
     * @author : Shrikant.Gaware
     *         @date : 24-Sep-2015
     */
    public function preDisplay()
    {
        $GLOBALS['log']->info("Begin: pi_product_itemViewList->preDisplay()");
        parent::preDisplay();
        $this->lv->quickViewLinks = false; // Hide Quick Edit Pencil
        $GLOBALS['log']->info('End: pi_product_itemViewList->preDisplay()');
    }

}
