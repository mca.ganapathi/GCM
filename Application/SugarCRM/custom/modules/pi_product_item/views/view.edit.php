<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
/**
 * : pi_product_itemViewEdit EditView Class
 * @author : Shrikant.Gaware
 *         @date : 24-Sep-2015
 */
class pi_product_itemViewEdit extends ViewEdit
{

    /**
     * : Method to process data for List View before display
     * @param  :
     *
     * @return :
     * @author : Shrikant.Gaware
     *         @date : 01-Oct-2015
     */
    function preDisplay()
    {
        $GLOBALS['log']->info("Begin: pi_product_itemViewEdit->preDisplay()");
        global $mod_strings;
        if ($this->bean->product_status == 'Approved') {
            $redirect_action = (isset($_REQUEST['record'])) ? 'DetailView' : 'ListView';
            $queryParams = array(
                'module' => 'pi_product_item',
                'action' => $redirect_action,
                'record' => $_REQUEST['record'],
                'offset' => $_REQUEST['offset'],
                'stamp' => $_REQUEST['stamp'],
                'return_module' => $_REQUEST['return_module']
            )
            ;
            SugarApplication::appendErrorMessage('"' . $this->bean->name . '" ' . $mod_strings['LBL_DENY_EDIT']);
            $GLOBALS['log']->info("End: pi_product_itemViewEdit->preDisplay()");
            SugarApplication::redirect('index.php?' . http_build_query($queryParams));
        }
        $GLOBALS['log']->info("End: pi_product_itemViewEdit->preDisplay()");
        parent::preDisplay();
    }

    /**
     * : Method to display the processed data on the Html page
     * @param  :
     *
     * @return :
     * @author : Shrikant.Gaware
     *         @date : 24-Sep-2015
     */
    function display()
    {
        $GLOBALS['log']->info("Begin: pi_product_itemViewEdit->display()");
        $util = ModUtils::getInstance('pi_product_item', 'ClassProductItems');
        $where = " (pa_productattributes.pa_productattributes_id_c IS NULL 
					OR pa_productattributes.pa_productattributes_id_c = '') ";
        $attribute_list = $util->getAttributeIdNamePair('pa_ProductAttributes', NULL, $where);
        
        $selected_list = $util->getAttributes($this->bean->id, 1);
        $temp_order = '';
        if (is_array($selected_list) && count($selected_list)>0) {
            for ($i = 0; $i < sizeof($selected_list); $i ++) {
                $temp_order .= '^' . ($i + 1);
            }            
        }

        $temp_order = ltrim($temp_order, '^');
        
        $temp_att_list = $attribute_list;
        $temp = array();
        if (is_array($selected_list) && count($selected_list)>0) {
            foreach ($selected_list as $k => $v) {
                if ($v != '') {
                    $temp[$v] = $temp_att_list[$v];
                    unset($temp_att_list[$v]);
                }
            }
        }
        if (is_array($temp_att_list) && count($temp_att_list)>0) {
            foreach ($temp_att_list as $k => $v) {
                $temp[$k] = $v;
            }
        }
        $attribute_list = $temp;
        
        /**
         * ********Assinging Values to TPL Variables************
         */
        $this->ss->assign('SELECTEDATTRIBUTES', implode("^", $selected_list));
        $this->ss->assign('ORDERATTRIBUTE', $temp_order);
        $this->ss->assign('UNSELECTEDATTRIBUTES', $attribute_list);
        $this->ss->assign('ATTRIBCOMBINETPL', 'custom/modules/pi_product_item/include/tpl/AttributeRow.tpl');
        /**
         * ********Assinging Values to TPL Variables************
         */
        $this->ev->defs['templateMeta']['form']['hideAudit'] = true;
        $this->ev->process();
        if ($this->ev->isDuplicate) {
            foreach ($this->ev->fieldDefs as $name => $defs) {
                if (! empty($defs['auto_increment'])) {
                    $this->ev->fieldDefs[$name]['value'] = '';
                }
            }
        }
        echo $this->ev->display($this->showTitle);
        $GLOBALS['log']->info('End: pi_product_itemViewEdit->display()');
    }
}