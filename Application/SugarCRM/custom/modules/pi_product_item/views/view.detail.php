<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
/**
 * : pi_product_itemViewList DetailVIew Class
 * @author : Shrikant.Gaware
 *         @date : 24-Sep-2015
 */
class pi_product_itemViewDetail extends ViewDetail
{

    /**
     * : Method to display the processed data on the Html page
     * @param  :
     *
     * @return :
     * @author : Shrikant.Gaware
     *         @date : 24-Sep-2015
     */
    function display()
    {
        $GLOBALS['log']->info("Begin: pi_product_itemViewDetail->display()");
        if ($this->bean->product_status == 'Approved') {
            unset($this->dv->defs['templateMeta']['form']['buttons'][0]);
            unset($this->dv->defs['templateMeta']['form']['buttons'][3]);
            unset($this->dv->defs['templateMeta']['form']['buttons'][4]);
        }
        unset($this->dv->defs['templateMeta']['form']['buttons'][1]);
        unset($this->dv->defs['templateMeta']['form']['buttons'][2]);
        //$this->dv->defs['templateMeta']['form']['hideAudit'] = true;
        $util = ModUtils::getInstance('pi_product_item', 'ClassProductItems');
        /**
         * ********Assinging Values to TPL Variables************
         */
        $this->ss->assign('ATTRIBUTES', $util->getAttributes($this->bean->id));
        $this->ss->assign('ATTRIBCOMBINETPL', 'custom/modules/pi_product_item/include/tpl/AttributeList.tpl');
        /**
         * ********Assinging Values to TPL Variables************
         */
        $GLOBALS['log']->info('End: pi_product_itemViewDetail->display()');
        parent::display();
    }
    
    /**
     * Override the _displaySubPanels method to support subpanel customizations *
     */
    function _displaySubPanels()
    {
    	require_once ('custom/include/SubPanel/SubPanelTiles.php');
    	$subpanel = new CustomSubPanelTiles($this->bean, $this->module);
    	unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts']['top_buttons'][0]); // hiding create
    	unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts']['top_buttons'][1]); // hiding select
    	echo $subpanel->display();
    }
}
