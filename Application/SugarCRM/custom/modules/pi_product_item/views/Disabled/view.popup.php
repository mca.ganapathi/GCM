<?php
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

/**
 * : pi_product_itemViewPopup PopupView Class
 * @author : Shrikant.Gaware
 *         @date : 07-Jan-2016
 */
require_once ('include/MVC/View/views/view.popup.php');

class pi_product_itemViewPopup extends ViewPopup
{

    /**
     * : Method to invoke parent viewpopup method
     * @param
     *        :
     *        
     * @return :
     * @author : Shrikant.Gaware
     *         @date : 07-Jan-2016
     */
    function CustomViewPopup()
    {
        parent::ViewPopup();
    }

    /**
     * : Method to display the processed data on the Html page & apply filter to the page
     * @param
     *        :
     *        
     * @return :
     * @author : Shrikant.Gaware
     *         @date : 07-Jan-2016
     */
    function display()
    {
        global $popupMeta, $mod_strings, $custom_filter_gcm;
        
        $source_module = ((!empty($_REQUEST['source_module']))?$_REQUEST['source_module']:'');
        
        // popup filter
        if ($source_module == 'gc_Contracts') {
            $product_id = ((!empty($_REQUEST['pm_products_pi_product_itempm_products_ida_advanced']))?$_REQUEST['pm_products_pi_product_itempm_products_ida_advanced']:'');
            
            $custom_filter_gcm = " pi_product_item.id in (SELECT pm_products_pi_product_itempi_product_item_idb FROM pm_products_pi_product_item_c WHERE pm_products_pi_product_itempm_products_ida = '$product_id') ";
        }
        parent::display();
    }
}
?>
