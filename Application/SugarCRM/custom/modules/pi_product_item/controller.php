<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
/**
 * : pi_product_itemController Controller Class
 * @author : Shrikant.Gaware
 *         @date : 24-Sep-2015
 */
class pi_product_itemController extends SugarController
{

    /**
     * : Method to update Product Status as Approve
     * @param
     *        :
     *        
     * @return :
     * @author : Shrikant.Gaware
     *         @date : 24-Sep-2015
     */
    function action_approve()
    {
        $GLOBALS['log']->info("Begin: pi_product_itemController->action_approve()");
        global $mod_strings;
        $product_id = $this->bean->id;
        $flag = 0;
        
        if ($this->bean->introduction_date != '' && $this->bean->sales_end_date != '') {
            $this->bean->product_status = 'Approved';
            $this->bean->save();
        } else {
            $flag = 1;
            $lbl = array();
            if ($this->bean->introduction_date == '') $lbl[] = $mod_strings['LBL_INTRODUCTION_DATE'];
            if ($this->bean->sales_end_date == '') $lbl[] = $mod_strings['LBL_SALES_END_DATE'];
        }
        
        if ($flag == 1) {
            $str = '';
            if (is_array($lbl) && count($lbl)>0) {
                foreach ($lbl as $k => $v) {
                    $str .= ($k + 1) . '. ' . $v . '<br />';
                }
            }
            SugarApplication::appendErrorMessage($mod_strings['LBL_CANNOT_APPROVE'] . '<pre>' . $str . '</pre>');
        }
        $params = array(
            'module' => 'pi_product_item',
            'action' => 'DetailView',
            'record' => $product_id
        );
        $GLOBALS['log']->info("Begin: pi_product_itemController->action_approve()");
        SugarApplication::redirect('index.php?' . http_build_query($params));
    }

    /**
     * : Method to save Product Item information
     * @param
     *        :
     *        
     * @return :
     * @author : Debasish.gupta
     *         @date : 20-Oct-2016
     */	
    public function action_save()
    {
        $GLOBALS['log']->debug("Begin: pi_product_itemController->action_save.");	
		$product_bean = BeanFactory::getBean('pi_product_item');
		if (! isset($sugar_config['default_sequence_start']['pi_product_item'])) $sugar_config['default_sequence_start']['pi_product_item'] = 100001;
		
		if(isset($this->bean->id) && ($this->bean->id <> ""))
			$product_bean->retrieve($this->bean->id);
		else {
			$p_bean = @clone ($product_bean);
			$tab = $product_bean->table_name;
			$where = "$tab.deleted IN(0,1,NULL)";
			$bean_list = $p_bean->get_full_list('', $where, '', 2);
			$product_bean->product_item_id = $sugar_config['default_sequence_start']['pi_product_item'] + sizeof($bean_list);
		}

		$product_bean->name = ((!empty($_POST['name']))?$_POST['name']:'');
		$product_bean->assigned_user_id = ((!empty($_POST['assigned_user_id']))?$_POST['assigned_user_id']:'');
		$product_bean->introduction_date = ((!empty($_POST['introduction_date']))?$_POST['introduction_date']:'');
		$product_bean->sales_end_date = ((!empty($_POST['sales_end_date']))?$_POST['sales_end_date']:'');
		$product_bean->product_status = ((!empty($_POST['product_status']))?$_POST['product_status']:'');
		$product_bean->product_type = ((!empty($_POST['product_type']))?$_POST['product_type']:'');
		$product_bean->save();		
		
		$query_params = array(
			'module' => 'pi_product_item',
			'action' => 'DetailView',
			'record' => $product_bean->id
		);
		$GLOBALS['log']->debug("End: pi_product_itemController->action_save.");
		SugarApplication::redirect('index.php?' . http_build_query($query_params));			
    }	
	
}
?>