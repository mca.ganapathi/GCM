<?php
// WARNING: The contents of this file are auto-generated.


// created: 2016-09-29 04:41:43
$viewdefs['fm_Factory']['base']['layout']['subpanels']['components'][] = array (
  'label' => 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_PM_PRODUCTS_TITLE',
  'context' => 
  array (
    'link' => 'fm_factory_pm_products',
  ),
  'layout' => 'subpanel',
);

// created: 2016-09-29 04:41:43
$viewdefs['fm_Factory']['base']['layout']['subpanels']['components'][] = array (
  'override_subpanel_list_view' => 
  array (
    'view' => 'subpanel-fm_factory_subpanel_fm_factory_pm_products',
    'link' => 'fm_factory_pm_products',
  ),
);