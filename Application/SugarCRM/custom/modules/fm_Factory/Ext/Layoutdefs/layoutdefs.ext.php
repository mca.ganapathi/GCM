<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/fm_Factory/Ext/Layoutdefs/fm_factory_pm_products_fm_Factory.php

 // created: 2015-12-17 13:01:42
$layout_defs["fm_Factory"]["subpanel_setup"]['fm_factory_pm_products'] = array (
  'order' => 100,
  'module' => 'pm_Products',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_PM_PRODUCTS_TITLE',
  'get_subpanel_data' => 'fm_factory_pm_products',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/fm_Factory/Ext/Layoutdefs/removeSubpanelCustom.php

/*** START : Remove unnecessary subpanels that we can't hide from Admin Panel ***/
unset($layout_defs["fm_Factory"]["subpanel_setup"]['fm_factory_pm_products']['top_buttons'][0]);			//unset create button of contracts subpanel
unset($layout_defs["fm_Factory"]["subpanel_setup"]['fm_factory_pm_products']['top_buttons'][1]);			//unset select button of contracts subpanel
/*** END : Remove unnecessary subpanels that we can't hide from Admin Panel ***/
 
?>
<?php
// Merged from custom/Extension/modules/fm_Factory/Ext/Layoutdefs/_overridefm_Factory_subpanel_fm_factory_pm_products.php

//auto-generated file DO NOT EDIT
$layout_defs['fm_Factory']['subpanel_setup']['fm_factory_pm_products']['override_subpanel_name'] = 'fm_Factory_subpanel_fm_factory_pm_products';

?>
