<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/fm_Factory/Ext/Vardefs/fm_factory_pm_products_fm_Factory.php

// created: 2015-12-17 13:01:43
$dictionary["fm_Factory"]["fields"]["fm_factory_pm_products"] = array (
  'name' => 'fm_factory_pm_products',
  'type' => 'link',
  'relationship' => 'fm_factory_pm_products',
  'source' => 'non-db',
  'module' => 'pm_Products',
  'bean_name' => 'pm_Products',
  'side' => 'right',
  'vname' => 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_PM_PRODUCTS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/fm_Factory/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-29 04:48:38
$dictionary['fm_Factory']['full_text_search']=false;

?>
