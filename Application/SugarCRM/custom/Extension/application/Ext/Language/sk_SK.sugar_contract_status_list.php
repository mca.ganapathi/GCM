<?php
 // created: 2016-10-06 09:29:52

$app_list_strings['contract_status_list']=array (
  0 => 'Draft',
  3 => 'Sent for Approval',
  1 => 'Approved',
  4 => 'Activated',
  5 => 'Sent for Termination Approval',
  6 => 'Termination Approved',
  2 => 'Deactivated',
  '' => '',
  7 => 'Upgraded',
);