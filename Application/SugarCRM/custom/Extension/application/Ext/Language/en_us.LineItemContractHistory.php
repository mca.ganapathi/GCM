<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['gc_LineItemContractHistory'] = 'LineItemContractHistory';
$app_list_strings['moduleList']['gc_Line_Item_Contract_History'] = 'LineItemContractHistory';
/*$app_list_strings['payment_type_list']['AccountTransfer'] = ' Account Transfer';
$app_list_strings['payment_type_list']['PostalTransfer'] = 'Postal Transfer';
$app_list_strings['payment_type_list']['CreditCard'] = 'Credit Card';
$app_list_strings['payment_type_list']['DirectDeposit'] = 'Direct Deposit';*/
$app_list_strings['payment_type_list']['00'] = 'General: Direct Deposit';
$app_list_strings['payment_type_list'][10] = 'Account Transfer';
$app_list_strings['payment_type_list'][30] = 'Postal Transfer';
$app_list_strings['payment_type_list'][60] = 'Credit Card';
$app_list_strings['payment_type_list'][''] = '';
$app_list_strings['customer_billing_method_list'][''] = '';
$app_list_strings['customer_billing_method_list']['eCSS_Billing'] = 'eCSS Billing';
$app_list_strings['customer_billing_method_list']['GBS_Billing'] = 'GBS Billing';
$app_list_strings['customer_billing_method_list']['Beluga_Billing'] = 'Beluga Billing';
$app_list_strings['customer_billing_method_list']['A_end_Billing_Calculated_bill'] = 'A-end Billing (Calculated bill sent by Factory)';
$app_list_strings['customer_billing_method_list']['A_end_Billing_Usage_data'] = 'A-end Billing (Usage data sent by Factory)';
$app_list_strings['customer_billing_method_list']['A_end_Billing_No_charge_data'] = 'A-end Billing (No charge data sent by Factory)';

//$app_list_strings['igc_settlement_method_list']['Quarterly_Settlement'] = 'Quarterly Settlement';
//$app_list_strings['igc_settlement_method_list']['NMC_Inter_Group_Company_Transaction'] = 'NMC Inter-Group-Company Transaction';
//$app_list_strings['igc_settlement_method_list']['NMC_Inter_NTTCom_Transaction'] = 'NMC Inter-NTTCom Transaction';
$app_list_strings['igc_settlement_method_list']['Individual_Contract'] = 'Individual Contract';
$app_list_strings['igc_settlement_method_list'][''] = '';
$app_list_strings['contract_line_item_type_list']['New'] = 'New';
$app_list_strings['contract_line_item_type_list']['Change'] = 'Change';
$app_list_strings['contract_line_item_type_list']['Termination'] = 'Termination';
$app_list_strings['contract_line_item_type_list']['Activated'] = 'Activated';
$app_list_strings['bi_action_list']['add'] = 'add';
$app_list_strings['bi_action_list']['change'] = 'change';
$app_list_strings['bi_action_list']['remove'] = 'remove';
$app_list_strings['bi_action_list']['no_change'] = 'no change';
$app_list_strings['moduleListSingular']['gc_Line_Item_Contract_History'] = 'LineItemContractHistory';
