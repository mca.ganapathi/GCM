<?php
 // created: 2016-09-29 04:41:57

$app_list_strings['document_template_type_dom']=array (
  '' => '',
  'MasterServiceAgreement' => 'MSA (Master Service Agreement)',
  'ServiceDescription' => 'Service Description',
  'TsCs_ServiceSpecificTermsConditions' => 'Ts & Cs (Service Specific Terms & conditions)',
  'ServiceLevelAgreement' => 'SLA (Service Level Agreement)',
  'ServiceOrderForm' => 'SOF (Service Order Form)',
  'ScopeOfWork' => 'SoW (Scope of Work)',
  'OtherContractDocuments' => 'Other Contract Documents',
  'OtherNonContractDocuments' => 'Other Non-Contract Documents',
);