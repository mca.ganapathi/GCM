<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_MyApprovals'] = 'My Approvals';
$app_list_strings['gc_approvals_action_list']['Approved'] = 'Approved';
$app_list_strings['gc_approvals_action_list']['Rejected'] = 'Rejected';
$app_list_strings['gc_approval_stages_list'][0] = 'Draft';
$app_list_strings['gc_approval_stages_list'][1] = 'Approval Stage1';
$app_list_strings['gc_approval_stages_list'][2] = 'Approval Stage2';
$app_list_strings['gc_approval_stages_list'][3] = 'Approval Stage3';
$app_list_strings['workflow_flag_list'][0] = 'Contract in progress';
$app_list_strings['workflow_flag_list'][1] = 'Contract submitted to workflow';
$app_list_strings['workflow_flag_list'][2] = 'Approved by 1st level';
$app_list_strings['workflow_flag_list'][3] = 'Approved by 2nd level';
$app_list_strings['workflow_flag_list'][4] = 'Approved by 3rd level';
