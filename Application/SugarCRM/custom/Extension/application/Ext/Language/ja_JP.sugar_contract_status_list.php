<?php
 // created: 2016-10-06 09:29:51

$app_list_strings['contract_status_list']=array (
  0 => 'Draft',
  1 => 'Approved',
  2 => 'Deactivated',
  '' => '',
  3 => 'Sent for Approval',
  4 => 'Activated',
  5 => 'Sent for Termination Approval',
  6 => 'Termination Approved',
  7 => 'Upgraded',
);