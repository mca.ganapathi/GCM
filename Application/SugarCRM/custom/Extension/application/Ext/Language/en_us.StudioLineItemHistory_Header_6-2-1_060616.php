<?php 
$app_list_strings['payment_type_list'] = array (
  '' => '',
  '00' => 'General: Direct Deposit',
  10 => 'Account Transfer',
  30 => 'Postal Transfer',
  60 => 'Credit Card',
);$app_list_strings['igc_settlement_method_list'] = array (
  '' => '',
  'No_Settlement_per_Contract' => 'No Settlement per Contract',
  'GERP_Inter_Group_Company_Transaction' => 'GERP+ Inter-Group-Company Transaction',
  'GERP_NTTCom_Internal_Transaction' => 'GERP+ NTTCom Internal Transaction',
  'Individual_Contract' => 'Individual Contract',
);