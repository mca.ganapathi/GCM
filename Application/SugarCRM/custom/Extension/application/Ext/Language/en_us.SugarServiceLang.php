<?php
/*********************************************************************************
 * Custom API Service Labels
 ********************************************************************************/

$app_strings['custom_api_service']['ERR_EMPTY_REQ_PARAMS'] = 'Required parameter details are blank.';
$app_strings['custom_api_service']['MSG_NO_RECORD_UPDATE'] = 'Record not created/updated.';
$app_strings['custom_api_service']['MSG_RECORD_UPDATE_SUCCESS'] = 'Record created/updated successfully ';
$app_strings['custom_api_service']['CIDAS_NO_DATA'] = 'Data not found in CIDAS';

