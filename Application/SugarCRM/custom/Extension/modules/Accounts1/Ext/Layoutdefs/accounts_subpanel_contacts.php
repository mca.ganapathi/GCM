<?php 
// Call the function to get accounts 
$layout_defs['Accounts']['subpanel_setup']['account_subpanel'] =
        array('order' => 99,
            'module' => 'Contacts',
            'subpanel_name' => 'default',
			'sort_order' => 'desc',   
			'sort_by' => 'id',   
            'get_subpanel_data' => 'function:get_all_accounts',
            'generate_select' => true,
            'title_key' => 'LBL_CONTACTS',
            'top_buttons' => array(),
            'function_parameters' => array(
                'import_function_file' => 'custom/modules/Accounts/customAccountsSubpanel.php',
                'account_id' => $this->_focus->id,
                'return_as_array' => 'true'
            ),
);
