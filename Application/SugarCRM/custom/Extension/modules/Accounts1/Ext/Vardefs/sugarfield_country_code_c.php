<?php
 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['country_code_c']['labelValue'] = 'Country Name Code';
$dictionary['Account']['fields']['country_code_c']['enforced'] = '';
$dictionary['Account']['fields']['country_code_c']['dependency'] = '';
$dictionary['Account']['fields']['country_code_c']['required'] = false;
$dictionary['Account']['fields']['country_code_c']['audited'] = true;
$dictionary['Account']['fields']['country_code_c']['full_text_search']['boost'] = 1;

