<?php
 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['name_2_c']['labelValue'] = 'Corporate  Name (Local Character 1)';
$dictionary['Account']['fields']['name_2_c']['enforced'] = '';
$dictionary['Account']['fields']['name_2_c']['dependency'] = '';
$dictionary['Account']['fields']['name_2_c']['required'] = false;
$dictionary['Account']['fields']['name_2_c']['audited'] = true;
$dictionary['Account']['fields']['name_2_c']['full_text_search']['boost'] = 1;

