<?php
 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['country_name_c']['labelValue'] = 'Country Name';
$dictionary['Account']['fields']['country_name_c']['enforced'] = '';
$dictionary['Account']['fields']['country_name_c']['dependency'] = '';
$dictionary['Account']['fields']['country_name_c']['required'] = false;
$dictionary['Account']['fields']['country_name_c']['audited'] = true;
$dictionary['Account']['fields']['country_name_c']['full_text_search']['boost'] = 1;

