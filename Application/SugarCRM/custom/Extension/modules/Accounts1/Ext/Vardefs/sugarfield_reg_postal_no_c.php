<?php
 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['reg_postal_no_c']['labelValue'] = 'Registered Postal Number';
$dictionary['Account']['fields']['reg_postal_no_c']['enforced'] = '';
$dictionary['Account']['fields']['reg_postal_no_c']['dependency'] = '';
$dictionary['Account']['fields']['reg_postal_no_c']['required'] = false;
$dictionary['Account']['fields']['reg_postal_no_c']['audited'] = true;
$dictionary['Account']['fields']['reg_postal_no_c']['full_text_search']['boost'] = 1;

