<?php
 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['reg_addr_2_c']['labelValue'] = 'Registered Address (Local Character)';
$dictionary['Account']['fields']['reg_addr_2_c']['enforced'] = '';
$dictionary['Account']['fields']['reg_addr_2_c']['dependency'] = '';
$dictionary['Account']['fields']['reg_addr_2_c']['required'] = false;
$dictionary['Account']['fields']['reg_addr_2_c']['audited'] = true;
$dictionary['Account']['fields']['reg_addr_2_c']['full_text_search']['boost'] = 1;

