<?php
 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['vat_no_c']['duplicate_merge_dom_value'] = 0;
$dictionary['Account']['fields']['vat_no_c']['labelValue'] = 'VAT Number';
$dictionary['Account']['fields']['vat_no_c']['enforced'] = '';
$dictionary['Account']['fields']['vat_no_c']['dependency'] = '';
$dictionary['Account']['fields']['vat_no_c']['audited'] = false;
$dictionary['Account']['fields']['vat_no_c']['full_text_search']['boost'] = 1;

