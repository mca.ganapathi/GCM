<?php
 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['name']['len'] = '100';
$dictionary['Account']['fields']['name']['help'] = 'Corporate name in Latin character (English)';
$dictionary['Account']['fields']['name']['comments'] = 'Corporate name in Latin character (English)';
$dictionary['Account']['fields']['name']['merge_filter'] = 'disabled';
$dictionary['Account']['fields']['name']['calculated'] = false;
$dictionary['Account']['fields']['name']['reportable'] = false;
$dictionary['Account']['fields']['name']['required'] = false;
$dictionary['Account']['fields']['name']['audited'] = true;
$dictionary['Account']['fields']['name']['full_text_search']['enabled'] = true;
$dictionary['Account']['fields']['name']['full_text_search']['searchable'] = true;
$dictionary['Account']['fields']['name']['full_text_search']['boost'] = 1.9099999999999999200639422269887290894985198974609375;

