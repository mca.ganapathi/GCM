<?php 
 // created: 2016-06-06 13:49:43
$mod_strings['LBL_DETAILVIEW_PANEL1'] = 'Contract Overview';
$mod_strings['LBL_DETAILVIEW_PANEL2'] = 'Contract Line Items';
$mod_strings['LBL_GLOBAL_CONTRACT'] = 'Global Contract ID';
$mod_strings['LBL_VERSION'] = 'Version';
$mod_strings['LBL_CONTRACT_NAME'] = 'Name';
$mod_strings['LBL_CONTRACTING_COMPANY'] = 'Contracting Company';
$mod_strings['LBL_PRODUCT_OFFERING'] = 'Product Offering';
$mod_strings['LBL_LINE_ITEM'] = 'Global Contract Item ID';
$mod_strings['LBL_PRODUCT_SPECIFICATION'] = 'Product Specification';
$mod_strings['LBL_SERVICE_START_DATE'] = 'Service Start Date';
$mod_strings['LBL_SERVICE_END_DATE'] = 'Service End Date';

?>
