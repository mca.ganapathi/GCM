<?php
 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['phone_fax']['len'] = '20';
$dictionary['Contact']['fields']['phone_fax']['comments'] = 'Contact fax number';
$dictionary['Contact']['fields']['phone_fax']['importable'] = 'false';
$dictionary['Contact']['fields']['phone_fax']['merge_filter'] = 'disabled';
$dictionary['Contact']['fields']['phone_fax']['reportable'] = false;
$dictionary['Contact']['fields']['phone_fax']['calculated'] = false;
$dictionary['Contact']['fields']['phone_fax']['audited'] = true;
$dictionary['Contact']['fields']['phone_fax']['full_text_search']['enabled'] = true;
$dictionary['Contact']['fields']['phone_fax']['full_text_search']['searchable'] = true;
$dictionary['Contact']['fields']['phone_fax']['full_text_search']['boost'] = 1.060000000000000053290705182007513940334320068359375;

