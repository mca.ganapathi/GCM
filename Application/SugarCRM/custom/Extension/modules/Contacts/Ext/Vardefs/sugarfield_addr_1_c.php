<?php
 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['addr_1_c']['labelValue'] = 'Address (Latin Character)';
$dictionary['Contact']['fields']['addr_1_c']['enforced'] = '';
$dictionary['Contact']['fields']['addr_1_c']['dependency'] = '';
$dictionary['Contact']['fields']['addr_1_c']['audited'] = true;
$dictionary['Contact']['fields']['addr_1_c']['full_text_search']['boost'] = 1;

