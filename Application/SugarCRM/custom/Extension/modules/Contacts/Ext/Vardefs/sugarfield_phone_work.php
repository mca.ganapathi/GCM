<?php
 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['phone_work']['len'] = '20';
$dictionary['Contact']['fields']['phone_work']['comments'] = 'Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['merge_filter'] = 'disabled';
$dictionary['Contact']['fields']['phone_work']['calculated'] = false;
$dictionary['Contact']['fields']['phone_work']['audited'] = true;
$dictionary['Contact']['fields']['phone_work']['full_text_search']['enabled'] = true;
$dictionary['Contact']['fields']['phone_work']['full_text_search']['searchable'] = true;
$dictionary['Contact']['fields']['phone_work']['full_text_search']['boost'] = 1.0800000000000000710542735760100185871124267578125;

