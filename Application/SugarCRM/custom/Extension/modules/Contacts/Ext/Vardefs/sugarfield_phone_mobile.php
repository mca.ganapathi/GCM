<?php
 // created: 2017-04-26 09:41:36
$dictionary['Contact']['fields']['phone_mobile']['len']='20';
$dictionary['Contact']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Contact']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_mobile']['calculated']=false;
$dictionary['Contact']['fields']['phone_mobile']['audited']=false;
$dictionary['Contact']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.09',
  'searchable' => true,
);
$dictionary['Contact']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge_dom_value']='1';

 ?>