<?php
 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['contact_type_c']['labelValue'] = 'Contact Type';
$dictionary['Contact']['fields']['contact_type_c']['dependency'] = '';
$dictionary['Contact']['fields']['contact_type_c']['visibility_grid'] = '';
$dictionary['Contact']['fields']['contact_type_c']['required'] = true;
$dictionary['Contact']['fields']['contact_type_c']['audited'] = true;
$dictionary['Contact']['fields']['contact_type_c']['full_text_search']['boost'] = 1;

