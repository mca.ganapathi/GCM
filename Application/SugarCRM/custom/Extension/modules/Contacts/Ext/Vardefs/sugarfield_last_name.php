<?php
 // created: 2017-04-19 10:17:11
$dictionary['Contact']['fields']['last_name']['len']='100';
$dictionary['Contact']['fields']['last_name']['help']='Contact Person in Latin character (English)';
$dictionary['Contact']['fields']['last_name']['comments']='Contact Person in Latin character (English)';
$dictionary['Contact']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Contact']['fields']['last_name']['calculated']=false;
$dictionary['Contact']['fields']['last_name']['required']=false;
$dictionary['Contact']['fields']['last_name']['audited']=true;
$dictionary['Contact']['fields']['last_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.97',
  'searchable' => true,
);
$dictionary['Contact']['fields']['last_name']['massupdate']=false;
$dictionary['Contact']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['last_name']['duplicate_merge_dom_value']='1';

 ?>