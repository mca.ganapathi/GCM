<?php
 // created: 2017-04-19 10:18:37
$dictionary['Contact']['fields']['name_2_c']['labelValue']='Contact Person Name (Local Character)';
$dictionary['Contact']['fields']['name_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['name_2_c']['enforced']='';
$dictionary['Contact']['fields']['name_2_c']['dependency']='';
 ?>