<?php
 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['first_name']['len'] = '60';
$dictionary['Contact']['fields']['first_name']['help'] = 'Contact Person in Latin character (English)';
$dictionary['Contact']['fields']['first_name']['comments'] = 'Contact Person in Latin character (English)';
$dictionary['Contact']['fields']['first_name']['merge_filter'] = 'disabled';
$dictionary['Contact']['fields']['first_name']['calculated'] = false;
$dictionary['Contact']['fields']['first_name']['full_text_search']['enabled'] = true;
$dictionary['Contact']['fields']['first_name']['full_text_search']['searchable'] = true;
$dictionary['Contact']['fields']['first_name']['full_text_search']['boost'] = 1.9899999999999999911182158029987476766109466552734375;

