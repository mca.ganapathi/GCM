<?php
 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['do_not_call']['audited'] = false;
$dictionary['Contact']['fields']['do_not_call']['comments'] = 'An indicator of whether contact can be called';
$dictionary['Contact']['fields']['do_not_call']['merge_filter'] = 'disabled';
$dictionary['Contact']['fields']['do_not_call']['calculated'] = false;

