<?php
 // created: 2016-09-29 04:48:38
$dictionary['Contact']['fields']['email1']['len'] = '140';
$dictionary['Contact']['fields']['email1']['merge_filter'] = 'disabled';
$dictionary['Contact']['fields']['email1']['calculated'] = false;
$dictionary['Contact']['fields']['email1']['audited'] = true;
$dictionary['Contact']['fields']['email1']['full_text_search']['boost'] = 1;

