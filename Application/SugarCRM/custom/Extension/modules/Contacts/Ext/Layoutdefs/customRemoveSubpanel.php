<?php
/*** START : Remove unnecessary subpanels that we can't hide from Admin Panel ***/

unset($layout_defs["Contacts"]["subpanel_setup"]['activities']);		//unset subpanel Activities
unset($layout_defs["Contacts"]["subpanel_setup"]['history']);			//unset subpanel History
unset($layout_defs["Contacts"]["subpanel_setup"]['contacts']);			//unset subpanel Direct Reports
unset($layout_defs["Contacts"]["subpanel_setup"]['documents']);			//unset subpanel Documents
unset($layout_defs["Contacts"]["subpanel_setup"]['opportunities']);		//unset subpanel opportunities
unset($layout_defs["Contacts"]["subpanel_setup"]['quotes']);			//unset subpanel quotes
unset($layout_defs["Contacts"]["subpanel_setup"]['products']);			//unset subpanel products
unset($layout_defs["Contacts"]["subpanel_setup"]['leads']);				//unset subpanel leads
unset($layout_defs["Contacts"]["subpanel_setup"]['campaigns']);			//unset subpanel campaigns
unset($layout_defs["Contacts"]["subpanel_setup"]['cases']);				//unset subpanel cases
unset($layout_defs["Contacts"]["subpanel_setup"]['bugs']);				//unset subpanel bugs
unset($layout_defs["Contacts"]["subpanel_setup"]['project']);			//unset subpanel project
unset($layout_defs["Contacts"]["subpanel_setup"]['contracts']);			//unset subpanel contracts
unset($layout_defs["Contacts"]["subpanel_setup"]['contacts_gc_contracts_1']['top_buttons'][0]);			//unset create button of contracts subpanel
unset($layout_defs["Contacts"]["subpanel_setup"]['contacts_gc_contracts_1']['top_buttons'][1]);			//unset select button of contracts subpanel

/*** END : Remove unnecessary subpanels that we can't hide from Admin Panel ***/
 ?>