<?php
 // created: 2016-01-19 07:00:33
$layout_defs["c_country"]["subpanel_setup"]['c_country_c_state_1'] = array (
  'order' => 100,
  'module' => 'c_State',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_C_COUNTRY_C_STATE_1_FROM_C_STATE_TITLE',
  'get_subpanel_data' => 'c_country_c_state_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
