<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Country Name';
$mod_strings['LBL_COUNTRY_CODE_ALPHABET'] = 'Country Code (alphabet)';
$mod_strings['LBL_COUNTRY_CODE_NUMERIC'] = 'Country Code (numeric)';
$mod_strings['LBL_REGION'] = 'Region';
