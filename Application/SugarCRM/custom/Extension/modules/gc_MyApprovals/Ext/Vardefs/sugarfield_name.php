<?php
 // created: 2016-09-29 04:48:38
$dictionary['gc_MyApprovals']['fields']['name']['unified_search'] = false;
$dictionary['gc_MyApprovals']['fields']['name']['calculated'] = false;
$dictionary['gc_MyApprovals']['fields']['name']['full_text_search']['enabled'] = true;
$dictionary['gc_MyApprovals']['fields']['name']['full_text_search']['searchable'] = true;
$dictionary['gc_MyApprovals']['fields']['name']['full_text_search']['boost'] = 1.5500000000000000444089209850062616169452667236328125;

