<?php 
$viewdefs['gc_MyApprovals']['base']['menu']['header'][] = array (
  'label' => 'View My Approvals',
  'acl_action' => 'list',
  'acl_module' => 'gc_MyApprovals',
  'route' => '#bwc/index.php?module=gc_MyApprovals&action=index&return_module=gc_MyApprovals&return_action=DetailView',
);
