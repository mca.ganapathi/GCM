<?php
 // created: 2015-12-17 13:01:47
$layout_defs["fm_Factory"]["subpanel_setup"]['fm_factory_pm_products'] = array (
  'order' => 100,
  'module' => 'pm_Products',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_PM_PRODUCTS_TITLE',
  'get_subpanel_data' => 'fm_factory_pm_products',
);
