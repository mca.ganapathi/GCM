<?php
 // created: 2015-12-17 13:01:42
$layout_defs["fm_Factory"]["subpanel_setup"]['fm_factory_pm_products'] = array (
  'order' => 100,
  'module' => 'pm_Products',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_PM_PRODUCTS_TITLE',
  'get_subpanel_data' => 'fm_factory_pm_products',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
