<?php
/*** START : Remove unnecessary subpanels that we can't hide from Admin Panel ***/


unset($layout_defs["Documents"]["subpanel_setup"]['contracts']);    // Hide Contract subpanel
unset($layout_defs["Documents"]["subpanel_setup"]['accounts']);     // Hide accounts subpanel
unset($layout_defs["Documents"]["subpanel_setup"]['contacts']);     // Hide contacts subpanel

