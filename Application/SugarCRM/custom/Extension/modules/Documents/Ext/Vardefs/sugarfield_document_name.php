<?php
 // created: 2016-09-29 04:48:38
$dictionary['Document']['fields']['document_name']['audited'] = true;
$dictionary['Document']['fields']['document_name']['merge_filter'] = 'disabled';
$dictionary['Document']['fields']['document_name']['full_text_search']['enabled'] = true;
$dictionary['Document']['fields']['document_name']['full_text_search']['searchable'] = true;
$dictionary['Document']['fields']['document_name']['full_text_search']['boost'] = 0.81999999999999995115018691649311222136020660400390625;
$dictionary['Document']['fields']['document_name']['calculated'] = false;

