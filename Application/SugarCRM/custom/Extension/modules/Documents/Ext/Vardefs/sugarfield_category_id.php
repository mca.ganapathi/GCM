<?php
 // created: 2016-09-29 04:48:38
$dictionary['Document']['fields']['category_id']['audited'] = true;
$dictionary['Document']['fields']['category_id']['merge_filter'] = 'disabled';
$dictionary['Document']['fields']['category_id']['calculated'] = false;
$dictionary['Document']['fields']['category_id']['dependency'] = false;
$dictionary['Document']['fields']['category_id']['full_text_search']['boost'] = 1;

