<?php
// created: 2016-01-07 11:52:10
$dictionary["Document"]["fields"]["gc_contracts_documents_1"] = array (
  'name' => 'gc_contracts_documents_1',
  'type' => 'link',
  'relationship' => 'gc_contracts_documents_1',
  'source' => 'non-db',
  'module' => 'gc_Contracts',
  'bean_name' => 'gc_Contracts',
  'vname' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_GC_CONTRACTS_TITLE',
  'id_name' => 'gc_contracts_documents_1gc_contracts_ida',
);
$dictionary["Document"]["fields"]["gc_contracts_documents_1_name"]=array (
  'name' => 'gc_contracts_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_GC_CONTRACTS_TITLE',
  'save' => true,
  'id_name' => 'gc_contracts_documents_1gc_contracts_ida',
  'link' => 'gc_contracts_documents_1',
  'table' => 'gc_contracts',
  'module' => 'gc_Contracts',
  'rname' => 'name',
);

$dictionary["Document"]["fields"]["gc_contracts_documents_1gc_contracts_ida"]=array (
  'name' => 'gc_contracts_documents_1gc_contracts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'id_name' => 'gc_contracts_documents_1gc_contracts_ida',
  'link' => 'gc_contracts_documents_1',
  'table' => 'gc_contracts',
  'module' => 'gc_Contracts',
  'rname' => 'id',
  'reportable' => false,
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
$dictionary["Document"]["fields"]["gc_contracts_documents_1_right"]=array (
  'name' => 'gc_contracts_documents_1_right',
  'type' => 'link',
  'relationship' => 'gc_contracts_documents_1',
  'source' => 'non-db',
  'vname' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'id_name' => '_idb',
  'side' => 'right',
  'link-type' => 'many',
);

