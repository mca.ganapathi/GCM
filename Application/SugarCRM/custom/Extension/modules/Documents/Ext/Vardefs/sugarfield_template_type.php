<?php
 // created: 2016-09-29 04:48:38
$dictionary['Document']['fields']['template_type']['audited'] = true;
$dictionary['Document']['fields']['template_type']['merge_filter'] = 'disabled';
$dictionary['Document']['fields']['template_type']['calculated'] = false;
$dictionary['Document']['fields']['template_type']['dependency'] = false;
$dictionary['Document']['fields']['template_type']['full_text_search']['boost'] = 1;

