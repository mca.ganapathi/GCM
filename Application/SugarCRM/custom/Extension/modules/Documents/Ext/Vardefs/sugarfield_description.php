<?php
 // created: 2016-09-29 04:48:38
$dictionary['Document']['fields']['description']['audited'] = true;
$dictionary['Document']['fields']['description']['comments'] = 'Full text of the note';
$dictionary['Document']['fields']['description']['merge_filter'] = 'disabled';
$dictionary['Document']['fields']['description']['calculated'] = false;
$dictionary['Document']['fields']['description']['full_text_search']['enabled'] = true;
$dictionary['Document']['fields']['description']['full_text_search']['searchable'] = true;
$dictionary['Document']['fields']['description']['full_text_search']['boost'] = 0.60999999999999998667732370449812151491641998291015625;

