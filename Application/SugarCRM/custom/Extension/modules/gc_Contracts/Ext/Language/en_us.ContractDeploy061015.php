<?php 
 // created: 2015-10-06 05:18:39
$mod_strings['LBL_CONTRACT_CUST_COMPANY_ACCOUNT_ID'] = 'Contracting Customer Company (related Corporate ID)';
$mod_strings['LBL_CONTRACT_CUST_COMPANY'] = 'Contracting Customer Company';
$mod_strings['LBL_NDB_QUOTE_ID'] = 'Quote ID';
$mod_strings['LBL_NDB_QUOTE_NAME'] = 'Quote Name';
$mod_strings['LBL_NDB_SOLUTION_ID'] = 'Solution ID';
$mod_strings['LBL_NO_RECORD_FOUND'] = 'No record found.';
$mod_strings['LBL_DETAILVIEW_PANEL2'] = 'Line Item Details';
$mod_strings['LBL_CREATED'] = 'Sugar Created By';
$mod_strings['LBL_DATE_ENTERED'] = 'Sugar Date Created';
$mod_strings['LBL_MODIFIED_NAME'] = 'Sugar Modified By Name';
$mod_strings['LBL_DATE_MODIFIED'] = 'Sugar Date Modified';

?>