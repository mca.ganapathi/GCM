<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_BILLING_TYPE'] = 'Billing Scheme';
$mod_strings['LBL_DATE_ENTERED'] = 'GCM Date Created';
$mod_strings['LBL_DATE_MODIFIED'] = 'GCM Date Modified';
$mod_strings['LBL_GLOBAL_CONTRACT_ID_UUID'] = 'Object ID';
