<?php 
// Call the function to get contract revision history
	$layout_defs['gc_Contracts']['subpanel_setup']['contract_version_history'] =
        array('order' => 99,
            'module' => 'gc_Contracts',
            'subpanel_name' => 'default',
			'sort_order' => 'desc', 
			'sort_by' => 'contract_version', 
            'get_subpanel_data' => 'function:get_all_versions_of_contract',
            'generate_select' => true,
            'title_key' => 'LBL_CONTRACT_REVISION_HISTORY',
            'top_buttons' => array(),
            'function_parameters' => array(
                'import_function_file' => 'custom/modules/gc_Contracts/customContractRevisionHistorySubpanel.php',
                'global_contract_id' => $this->_focus->global_contract_id,
                'return_as_array' => 'true'
            ),
);
