<?php
 // created: 2016-09-29 04:48:38
$dictionary['gc_Contracts']['fields']['product_offering']['type'] = 'enum';
$dictionary['gc_Contracts']['fields']['product_offering']['options'] = 'product_offering_list';
$dictionary['gc_Contracts']['fields']['product_offering']['required'] = true;
$dictionary['gc_Contracts']['fields']['product_offering']['dependency'] = false;
$dictionary['gc_Contracts']['fields']['product_offering']['full_text_search']['boost'] = 1;

