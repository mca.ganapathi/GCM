<?php

$dictionary["gc_Contracts"]["fields"]["contacts_gc_contracts_1"] = array (
  'name' => 'contacts_gc_contracts_1',
  'type' => 'link',
  'relationship' => 'contacts_gc_contracts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_GC_CONTRACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_gc_contracts_1contacts_ida',
);
$dictionary["gc_Contracts"]["fields"]["contacts_gc_contracts_1_name"] = array (
  'name' => 'contacts_gc_contracts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_GC_CONTRACTS_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_gc_contracts_1contacts_ida',
  'link' => 'contacts_gc_contracts_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
  'audited' => true,
);
$dictionary["gc_Contracts"]["fields"]["contacts_gc_contracts_1contacts_ida"] = array (
  'name' => 'contacts_gc_contracts_1contacts_ida',
  'type' => 'id',
  'relationship' => 'contacts_gc_contracts_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
  'link' => 'contacts_gc_contracts_1',
  'rname' => 'id',
);
