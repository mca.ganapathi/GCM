<?php
 // created: 2016-01-19 07:00:34
$layout_defs["c_country"]["subpanel_setup"]['c_country_c_state_1'] = array (
  'order' => 100,
  'module' => 'c_State',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_C_COUNTRY_C_STATE_1_FROM_C_STATE_TITLE',
  'get_subpanel_data' => 'c_country_c_state_1',
);
