<?php
 // created: 2016-01-08 05:22:36
$layout_defs["gc_LineItem"]["subpanel_setup"]['gc_lineitem_documents_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_GC_LINEITEM_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'gc_lineitem_documents_1',
);
