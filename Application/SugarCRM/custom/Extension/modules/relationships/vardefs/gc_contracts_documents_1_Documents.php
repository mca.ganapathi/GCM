<?php
// created: 2016-01-07 11:52:10
$dictionary["Document"]["fields"]["gc_contracts_documents_1"] = array (
  'name' => 'gc_contracts_documents_1',
  'type' => 'link',
  'relationship' => 'gc_contracts_documents_1',
  'source' => 'non-db',
  'module' => 'gc_Contracts',
  'bean_name' => 'gc_Contracts',
  'vname' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_GC_CONTRACTS_TITLE',
  'id_name' => 'gc_contracts_documents_1gc_contracts_ida',
);
$dictionary["Document"]["fields"]["gc_contracts_documents_1_name"] = array (
  'name' => 'gc_contracts_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_GC_CONTRACTS_TITLE',
  'save' => true,
  'id_name' => 'gc_contracts_documents_1gc_contracts_ida',
  'link' => 'gc_contracts_documents_1',
  'table' => 'gc_contracts',
  'module' => 'gc_Contracts',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["gc_contracts_documents_1gc_contracts_ida"] = array (
  'name' => 'gc_contracts_documents_1gc_contracts_ida',
  'type' => 'link',
  'relationship' => 'gc_contracts_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
