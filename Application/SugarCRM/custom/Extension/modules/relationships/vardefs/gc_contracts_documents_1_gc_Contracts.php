<?php
// created: 2016-01-07 11:52:10
$dictionary["gc_Contracts"]["fields"]["gc_contracts_documents_1"] = array (
  'name' => 'gc_contracts_documents_1',
  'type' => 'link',
  'relationship' => 'gc_contracts_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_GC_CONTRACTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
