<?php
// created: 2016-05-04 05:46:28
$dictionary["gc_InvoiceSubtotalGroup"]["fields"]["gc_invoicesubtotalgroup_gc_line_item_contract_history_1"] = array (
  'name' => 'gc_invoicesubtotalgroup_gc_line_item_contract_history_1',
  'type' => 'link',
  'relationship' => 'gc_invoicesubtotalgroup_gc_line_item_contract_history_1',
  'source' => 'non-db',
  'module' => 'gc_Line_Item_Contract_History',
  'bean_name' => 'gc_Line_Item_Contract_History',
  'side' => 'right',
  'vname' => 'LBL_GC_INVOICESUBTOTALGROUP_GC_LINE_ITEM_CONTRACT_HISTORY_1_FROM_GC_LINE_ITEM_CONTRACT_HISTORY_TITLE',
);
