<?php
// created: 2016-05-04 05:33:03
$dictionary["gc_Contracts"]["fields"]["gc_contracts_gc_invoicesubtotalgroup_1"] = array (
  'name' => 'gc_contracts_gc_invoicesubtotalgroup_1',
  'type' => 'link',
  'relationship' => 'gc_contracts_gc_invoicesubtotalgroup_1',
  'source' => 'non-db',
  'module' => 'gc_InvoiceSubtotalGroup',
  'bean_name' => 'gc_InvoiceSubtotalGroup',
  'side' => 'right',
  'vname' => 'LBL_GC_CONTRACTS_GC_INVOICESUBTOTALGROUP_1_FROM_GC_INVOICESUBTOTALGROUP_TITLE',
);
