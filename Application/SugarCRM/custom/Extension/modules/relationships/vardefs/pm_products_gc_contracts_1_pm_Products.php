<?php
// created: 2015-12-21 12:06:51
$dictionary["pm_Products"]["fields"]["pm_products_gc_contracts_1"] = array (
  'name' => 'pm_products_gc_contracts_1',
  'type' => 'link',
  'relationship' => 'pm_products_gc_contracts_1',
  'source' => 'non-db',
  'module' => 'gc_Contracts',
  'bean_name' => 'gc_Contracts',
  'side' => 'right',
  'vname' => 'LBL_PM_PRODUCTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
);
