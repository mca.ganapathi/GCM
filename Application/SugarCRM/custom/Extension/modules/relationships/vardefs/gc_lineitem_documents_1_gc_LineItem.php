<?php
// created: 2016-01-08 05:22:36
$dictionary["gc_LineItem"]["fields"]["gc_lineitem_documents_1"] = array (
  'name' => 'gc_lineitem_documents_1',
  'type' => 'link',
  'relationship' => 'gc_lineitem_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_GC_LINEITEM_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
