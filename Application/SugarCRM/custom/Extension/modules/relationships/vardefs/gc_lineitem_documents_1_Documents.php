<?php
// created: 2016-01-08 05:22:36
$dictionary["Document"]["fields"]["gc_lineitem_documents_1"] = array (
  'name' => 'gc_lineitem_documents_1',
  'type' => 'link',
  'relationship' => 'gc_lineitem_documents_1',
  'source' => 'non-db',
  'module' => 'gc_LineItem',
  'bean_name' => 'gc_LineItem',
  'vname' => 'LBL_GC_LINEITEM_DOCUMENTS_1_FROM_GC_LINEITEM_TITLE',
  'id_name' => 'gc_lineitem_documents_1gc_lineitem_ida',
);
$dictionary["Document"]["fields"]["gc_lineitem_documents_1_name"] = array (
  'name' => 'gc_lineitem_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_GC_LINEITEM_DOCUMENTS_1_FROM_GC_LINEITEM_TITLE',
  'save' => true,
  'id_name' => 'gc_lineitem_documents_1gc_lineitem_ida',
  'link' => 'gc_lineitem_documents_1',
  'table' => 'gc_lineitem',
  'module' => 'gc_LineItem',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["gc_lineitem_documents_1gc_lineitem_ida"] = array (
  'name' => 'gc_lineitem_documents_1gc_lineitem_ida',
  'type' => 'link',
  'relationship' => 'gc_lineitem_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_GC_LINEITEM_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
