<?php
// created: 2015-09-28 09:40:15
$dictionary["Contact"]["fields"]["contacts_gc_contracts_1"] = array (
  'name' => 'contacts_gc_contracts_1',
  'type' => 'link',
  'relationship' => 'contacts_gc_contracts_1',
  'source' => 'non-db',
  'module' => 'gc_Contracts',
  'bean_name' => 'gc_Contracts',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
);
