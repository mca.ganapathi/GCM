<?php
// created: 2016-01-19 07:00:34
$dictionary["c_country"]["fields"]["c_country_c_state_1"] = array (
  'name' => 'c_country_c_state_1',
  'type' => 'link',
  'relationship' => 'c_country_c_state_1',
  'source' => 'non-db',
  'module' => 'c_State',
  'bean_name' => 'c_State',
  'side' => 'right',
  'vname' => 'LBL_C_COUNTRY_C_STATE_1_FROM_C_STATE_TITLE',
);
