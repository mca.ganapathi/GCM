<?php
 // created: 2016-05-04 05:33:01
$layout_defs["gc_Contracts"]["subpanel_setup"]['gc_contracts_gc_invoicesubtotalgroup_1'] = array (
  'order' => 100,
  'module' => 'gc_InvoiceSubtotalGroup',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_GC_CONTRACTS_GC_INVOICESUBTOTALGROUP_1_FROM_GC_INVOICESUBTOTALGROUP_TITLE',
  'get_subpanel_data' => 'gc_contracts_gc_invoicesubtotalgroup_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
