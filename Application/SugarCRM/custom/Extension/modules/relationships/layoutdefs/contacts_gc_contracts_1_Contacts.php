<?php
 // created: 2015-09-28 09:40:14
$layout_defs["Contacts"]["subpanel_setup"]['contacts_gc_contracts_1'] = array (
  'order' => 100,
  'module' => 'gc_Contracts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
  'get_subpanel_data' => 'contacts_gc_contracts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
