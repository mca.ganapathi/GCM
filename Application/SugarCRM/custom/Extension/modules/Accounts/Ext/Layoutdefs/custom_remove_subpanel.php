<?php

//Disable unnecessary subpanel from corporate detailview

unset($layout_defs["Accounts"]["subpanel_setup"]['insideview']);    
unset($layout_defs["Accounts"]["subpanel_setup"]['activities']);
unset($layout_defs["Accounts"]["subpanel_setup"]['history']); 
unset($layout_defs["Accounts"]["subpanel_setup"]['documents']); 
unset($layout_defs["Accounts"]["subpanel_setup"]['opportunities']); 
unset($layout_defs["Accounts"]["subpanel_setup"]['quotes']); 
unset($layout_defs["Accounts"]["subpanel_setup"]['campaigns']);
unset($layout_defs["Accounts"]["subpanel_setup"]['accounts']);
unset($layout_defs["Accounts"]["subpanel_setup"]['leads']);
unset($layout_defs["Accounts"]["subpanel_setup"]['cases']);
unset($layout_defs["Accounts"]["subpanel_setup"]['accounts_gc_contracts_1']);

?>