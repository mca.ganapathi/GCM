<?php
 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['hq_addr_1_c']['labelValue'] = 'Headquarter Address (Latin Character)';
$dictionary['Account']['fields']['hq_addr_1_c']['enforced'] = '';
$dictionary['Account']['fields']['hq_addr_1_c']['dependency'] = '';
$dictionary['Account']['fields']['hq_addr_1_c']['required'] = false;
$dictionary['Account']['fields']['hq_addr_1_c']['audited'] = true;
$dictionary['Account']['fields']['hq_addr_1_c']['full_text_search']['boost'] = 1;

