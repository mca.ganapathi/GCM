<?php
 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['customer_status_code_c']['labelValue'] = 'Customer Status Code';
$dictionary['Account']['fields']['customer_status_code_c']['dependency'] = '';
$dictionary['Account']['fields']['customer_status_code_c']['visibility_grid'] = '';
$dictionary['Account']['fields']['customer_status_code_c']['required'] = true;
$dictionary['Account']['fields']['customer_status_code_c']['audited'] = true;

