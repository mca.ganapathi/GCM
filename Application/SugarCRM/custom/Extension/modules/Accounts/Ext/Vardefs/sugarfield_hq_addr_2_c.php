<?php
 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['hq_addr_2_c']['labelValue'] = 'Headquarter Address (Local Character)';
$dictionary['Account']['fields']['hq_addr_2_c']['enforced'] = '';
$dictionary['Account']['fields']['hq_addr_2_c']['dependency'] = '';
$dictionary['Account']['fields']['hq_addr_2_c']['required'] = false;
$dictionary['Account']['fields']['hq_addr_2_c']['audited'] = true;
$dictionary['Account']['fields']['hq_addr_2_c']['full_text_search']['boost'] = 1;

