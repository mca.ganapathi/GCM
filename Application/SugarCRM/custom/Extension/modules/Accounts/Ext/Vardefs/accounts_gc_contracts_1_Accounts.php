<?php
// created: 2015-09-28 09:38:20
$dictionary["Account"]["fields"]["accounts_gc_contracts_1"] = array (
  'name' => 'accounts_gc_contracts_1',
  'type' => 'link',
  'relationship' => 'accounts_gc_contracts_1',
  'source' => 'non-db',
  'module' => 'gc_Contracts',
  'bean_name' => 'gc_Contracts',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
);
