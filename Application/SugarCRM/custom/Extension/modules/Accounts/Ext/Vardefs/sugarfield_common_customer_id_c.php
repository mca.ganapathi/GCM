<?php
 // created: 2016-09-29 04:48:38
$dictionary['Account']['fields']['common_customer_id_c']['labelValue'] = 'Common Customer ID';
$dictionary['Account']['fields']['common_customer_id_c']['enforced'] = '';
$dictionary['Account']['fields']['common_customer_id_c']['dependency'] = '';
$dictionary['Account']['fields']['common_customer_id_c']['required'] = false;
$dictionary['Account']['fields']['common_customer_id_c']['audited'] = true;
$dictionary['Account']['fields']['common_customer_id_c']['full_text_search']['boost'] = 1;

