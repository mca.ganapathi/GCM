<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Corporate';
$mod_strings['LNK_CREATE'] = 'Create Company';
$mod_strings['LBL_MODULE_NAME'] = 'Corporates';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Corporate';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Corporate';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Corporates';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Corporate Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Corporates';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Corporate record you are about to create might be a duplicate of an Corporate record that already exists. Corporate records containing similar names are listed below.Click Save to continue creating this new Corporate, or click Cancel to return to the module without creating the Corporate.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Corporate';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Corporate List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Corporate Search';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Corporates';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Corporate from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Corporate.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Corporate Name:';
$mod_strings['LBL_ACCOUNT'] = 'Corporate:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Corporates';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Corporates';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Corporate';
$mod_strings['LBL_MODULE_TITLE'] = 'Corporates: Home';
$mod_strings['LBL_MODULE_ID'] = 'Corporates';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Corporate ID';
$mod_strings['MSG_DUPLICATE'] = 'The Corporate record you are about to create might be a duplicate of an Corporate record that already exists. Corporate records containing similar names are listed below.Click Create Corporate to continue creating this new Corporate, or select an existing Corporate listed below.';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Corporate Type';
