<?php

$mod_strings['LBL_REPAIR_FIELDS_META'] = 'Repair Custom Fields';
$mod_strings['LBL_REPAIR_FIELDS_META_DESC'] = 'Check and repair Sugar custom fields';
$mod_strings['LBL_ACCESS_CONTROL_EXT_META'] = 'Access Control Management';
$mod_strings['LBL_ACCESS_CONTROL_EXT_META_DESC'] = 'Manage extended Access Control here';
$mod_strings['LBL_CUSTOM_ADMIN_TITLE'] = 'Sugar Admin-Extension';
$mod_strings['LBL_CUSTOM_ADMIN_DESC'] = 'Manage custom admin scripts developed by NTTC SugarCRM Developers.';
$mod_strings['LBL_VIEW_API_LOG'] = 'API Log Viewer';
$mod_strings['LBL_VIEW_API_LOG_DESC'] = 'Search and Verify the API Log';
?>