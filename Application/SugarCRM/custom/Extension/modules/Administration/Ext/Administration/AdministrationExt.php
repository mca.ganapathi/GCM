<?php

$admin_option_defs = array();
$admin_option_defs['Administration']['repair_fields_meta'] = array(
    // Icon name. Available icons are located in ./themes/default/images
    'Administration',
    // Link name label
    'LBL_REPAIR_FIELDS_META',
    // Link description label
    'LBL_REPAIR_FIELDS_META_DESC',
    // Link URL
    './index.php?module=Administration&action=deployCustomFields'
);

$admin_option_defs['Administration']['access_control_ext'] = array(
    // Icon name. Available icons are located in ./themes/default/images
    'Roles',
    // Link name label
    'LBL_ACCESS_CONTROL_EXT_META',
    // Link description label
    'LBL_ACCESS_CONTROL_EXT_META_DESC',
    // Link URL
    './index.php?module=Administration&action=AccessControlExt'
);

$admin_option_defs['Administration']['view_api_log'] = array(
    // Icon name. Available icons are located in ./themes/default/images
    'Administration',
    // Link name label
    'LBL_VIEW_API_LOG',
    // Link description label
    'LBL_VIEW_API_LOG_DESC',
    // Link URL
    './index.php?module=Administration&action=ViewAPILog'
);

$admin_group_header[] = array(
    // Section header label
    'LBL_CUSTOM_ADMIN_TITLE',
    // $other_text parameter for get_form_header()
    '',
    // $show_help parameter for get_form_header()
    false,
    // Section links
    $admin_option_defs,
    // Section description label
    'LBL_CUSTOM_ADMIN_DESC'
);