<?php

if (isset($viewdefs['be_Beluga_Export']['base']['menu']['header'])) {
    foreach ($viewdefs['be_Beluga_Export']['base']['menu']['header'] as $key => $moduleAction) {
        //remove the link by label key
        if ($moduleAction['label'] == "LNK_NEW_RECORD") {
            unset($viewdefs['be_Beluga_Export']['base']['menu']['header'][$key]);
            $viewdefs['be_Beluga_Export']['base']['menu']['header'] = array_values($viewdefs['be_Beluga_Export']['base']['menu']['header']);
        }
    }
}
