<?php
 // created: 2016-09-29 04:48:38
$dictionary['pi_product_item']['fields']['name']['len'] = '100';
$dictionary['pi_product_item']['fields']['name']['unified_search'] = false;
$dictionary['pi_product_item']['fields']['name']['calculated'] = false;
$dictionary['pi_product_item']['fields']['name']['audited'] = true;
$dictionary['pi_product_item']['fields']['name']['full_text_search']['enabled'] = true;
$dictionary['pi_product_item']['fields']['name']['full_text_search']['searchable'] = true;
$dictionary['pi_product_item']['fields']['name']['full_text_search']['boost'] = 1.5500000000000000444089209850062616169452667236328125;

