<?php 
$viewdefs['pa_ProductAttributeValues']['base']['menu']['header'][] = array (
  'label' => 'View Product Attribute Values',
  'acl_action' => 'list',
  'acl_module' => 'pa_ProductAttributeValues',
  'route' => '#bwc/index.php?module=pa_ProductAttributeValues&action=index&return_module=pa_ProductAttributeValues&return_action=DetailView',
);
