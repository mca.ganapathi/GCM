<?php
 // created: 2016-02-18 07:06:47
$layout_defs["pm_Products"]["subpanel_setup"]['pm_products_pi_product_item'] = array (
  'order' => 100,
  'module' => 'pi_product_item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_PM_PRODUCTS_PI_PRODUCT_ITEM_FROM_PI_PRODUCT_ITEM_TITLE',
  'get_subpanel_data' => 'pm_products_pi_product_item',
);
