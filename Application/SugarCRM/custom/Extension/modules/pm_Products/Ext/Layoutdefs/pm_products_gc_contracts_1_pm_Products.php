<?php
 // created: 2015-12-21 12:06:40
$layout_defs["pm_Products"]["subpanel_setup"]['pm_products_gc_contracts_1'] = array (
  'order' => 100,
  'module' => 'gc_Contracts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PM_PRODUCTS_GC_CONTRACTS_1_FROM_GC_CONTRACTS_TITLE',
  'get_subpanel_data' => 'pm_products_gc_contracts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
