<?php
/*** START : Remove unnecessary subpanels that we can't hide from Admin Panel ***/
unset($layout_defs["pm_Products"]["subpanel_setup"]['pm_products_pi_product_item']['top_buttons'][0]);			//unset create button of Product Items subpanel
unset($layout_defs["pm_Products"]["subpanel_setup"]['pm_products_gc_contracts_1']['top_buttons'][0]);			//unset create button of contracts subpanel
unset($layout_defs["pm_Products"]["subpanel_setup"]['pm_products_gc_contracts_1']['top_buttons'][1]);			//unset select button of contracts subpanel
/*** END : Remove unnecessary subpanels that we can't hide from Admin Panel ***/
 ?>