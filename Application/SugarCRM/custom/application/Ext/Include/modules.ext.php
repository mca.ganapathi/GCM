<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Include/Accounts.php
 
 $bwcModules[] = 'Accounts';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/AccountsJapanSpecific.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['js_Accounts_js'] = 'js_Accounts_js';
$beanFiles['js_Accounts_js'] = 'modules/js_Accounts_js/js_Accounts_js.php';
$moduleList[] = 'js_Accounts_js';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/AccountTransfer.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_AccountTransfer'] = 'gc_AccountTransfer';
$beanFiles['gc_AccountTransfer'] = 'modules/gc_AccountTransfer/gc_AccountTransfer.php';
$moduleList[] = 'gc_AccountTransfer';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/BelugaExport.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['be_Beluga_Export'] = 'be_Beluga_Export';
$beanFiles['be_Beluga_Export'] = 'modules/be_Beluga_Export/be_Beluga_Export.php';
$moduleList[] = 'be_Beluga_Export';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/BelugaProductCode.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['bp_Beluga_Product_Code'] = 'bp_Beluga_Product_Code';
$beanFiles['bp_Beluga_Product_Code'] = 'modules/bp_Beluga_Product_Code/bp_Beluga_Product_Code.php';
$moduleList[] = 'bp_Beluga_Product_Code';
$bwcModules[] = 'bp_Beluga_Product_Code';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Contacts.php
 
$bwcModules[] = 'Contacts';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/ContractCommentsLogModule.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_comments'] = 'gc_comments';
$beanFiles['gc_comments'] = 'modules/gc_comments/gc_comments.php';
$moduleList[] = 'gc_comments';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/ContractMyApproval.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_MyApprovals'] = 'gc_MyApprovals';
$beanFiles['gc_MyApprovals'] = 'modules/gc_MyApprovals/gc_MyApprovals.php';
$moduleList[] = 'gc_MyApprovals';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Country.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['c_country'] = 'c_country';
$beanFiles['c_country'] = 'modules/c_country/c_country.php';
$moduleList[] = 'c_country';
$bwcModules[] = 'c_country';

?>
<?php
// Merged from custom/Extension/application/Ext/Include/CreditCard.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_CreditCard'] = 'gc_CreditCard';
$beanFiles['gc_CreditCard'] = 'modules/gc_CreditCard/gc_CreditCard.php';
$moduleList[] = 'gc_CreditCard';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/DirectDeposit.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_DirectDeposit'] = 'gc_DirectDeposit';
$beanFiles['gc_DirectDeposit'] = 'modules/gc_DirectDeposit/gc_DirectDeposit.php';
$moduleList[] = 'gc_DirectDeposit';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/FactoryModule.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['fm_Factory'] = 'fm_Factory';
$beanFiles['fm_Factory'] = 'modules/fm_Factory/fm_Factory.php';
$moduleList[] = 'fm_Factory';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/GBS_Organization.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_organization'] = 'gc_organization';
$beanFiles['gc_organization'] = 'modules/gc_organization/gc_organization.php';
$moduleList[] = 'gc_organization';
$bwcModules[] = 'gc_organization';

?>
<?php
// Merged from custom/Extension/application/Ext/Include/GCM_Contracts.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_Contracts'] = 'gc_Contracts';
$beanFiles['gc_Contracts'] = 'modules/gc_Contracts/gc_Contracts.php';
$moduleList[] = 'gc_Contracts';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/GCM_ProductAttributes.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['pa_ProductAttributes'] = 'pa_ProductAttributes';
$beanFiles['pa_ProductAttributes'] = 'modules/pa_ProductAttributes/pa_ProductAttributes.php';
$moduleList[] = 'pa_ProductAttributes';
$beanList['pa_ProductAttributeValues'] = 'pa_ProductAttributeValues';
$beanFiles['pa_ProductAttributeValues'] = 'modules/pa_ProductAttributeValues/pa_ProductAttributeValues.php';
$moduleList[] = 'pa_ProductAttributeValues';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/GCM_ProductsModule.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['pm_Products'] = 'pm_Products';
$beanFiles['pm_Products'] = 'modules/pm_Products/pm_Products.php';
$moduleList[] = 'pm_Products';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/IineItemConfigSummary.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_Line_Item_Config_Summary'] = 'gc_Line_Item_Config_Summary';
$beanFiles['gc_Line_Item_Config_Summary'] = 'modules/gc_Line_Item_Config_Summary/gc_Line_Item_Config_Summary.php';
$moduleList[] = 'gc_Line_Item_Config_Summary';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/InvoiceSubtotalGroup.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_InvoiceSubtotalGroup'] = 'gc_InvoiceSubtotalGroup';
$beanFiles['gc_InvoiceSubtotalGroup'] = 'modules/gc_InvoiceSubtotalGroup/gc_InvoiceSubtotalGroup.php';
$modules_exempt_from_availability_check['gc_InvoiceSubtotalGroup'] = 'gc_InvoiceSubtotalGroup';
$report_include_modules['gc_InvoiceSubtotalGroup'] = 'gc_InvoiceSubtotalGroup';
$modInvisList[] = 'gc_InvoiceSubtotalGroup';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/LineItem.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_LineItem'] = 'gc_LineItem';
$beanFiles['gc_LineItem'] = 'modules/gc_LineItem/gc_LineItem.php';
$moduleList[] = 'gc_LineItem';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/LineItemContractHistory.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_Line_Item_Contract_History'] = 'gc_Line_Item_Contract_History';
$beanFiles['gc_Line_Item_Contract_History'] = 'modules/gc_Line_Item_Contract_History/gc_Line_Item_Contract_History.php';
$moduleList[] = 'gc_Line_Item_Contract_History';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/NTTComGroupCompany.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_NTTComGroupCompany'] = 'gc_NTTComGroupCompany';
$beanFiles['gc_NTTComGroupCompany'] = 'modules/gc_NTTComGroupCompany/gc_NTTComGroupCompany.php';
$moduleList[] = 'gc_NTTComGroupCompany';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/PostalTransfer.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_PostalTransfer'] = 'gc_PostalTransfer';
$beanFiles['gc_PostalTransfer'] = 'modules/gc_PostalTransfer/gc_PostalTransfer.php';
$moduleList[] = 'gc_PostalTransfer';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/ProductConfiguration.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_ProductConfiguration'] = 'gc_ProductConfiguration';
$beanFiles['gc_ProductConfiguration'] = 'modules/gc_ProductConfiguration/gc_ProductConfiguration.php';
$modules_exempt_from_availability_check['gc_ProductConfiguration'] = 'gc_ProductConfiguration';
$report_include_modules['gc_ProductConfiguration'] = 'gc_ProductConfiguration';
$modInvisList[] = 'gc_ProductConfiguration';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/ProductItem.php

 //WARNING: The contents of this file are auto-generated
$beanList['pi_product_item'] = 'pi_product_item';
$beanFiles['pi_product_item'] = 'modules/pi_product_item/pi_product_item.php';
$moduleList[] = 'pi_product_item';
$bwcModules[] = 'pi_product_item';

?>
<?php
// Merged from custom/Extension/application/Ext/Include/ProductPrice.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_ProductPrice'] = 'gc_ProductPrice';
$beanFiles['gc_ProductPrice'] = 'modules/gc_ProductPrice/gc_ProductPrice.php';
$modules_exempt_from_availability_check['gc_ProductPrice'] = 'gc_ProductPrice';
$report_include_modules['gc_ProductPrice'] = 'gc_ProductPrice';
$modInvisList[] = 'gc_ProductPrice';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/ProductRule.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_ProductRule'] = 'gc_ProductRule';
$beanFiles['gc_ProductRule'] = 'modules/gc_ProductRule/gc_ProductRule.php';
$modules_exempt_from_availability_check['gc_ProductRule'] = 'gc_ProductRule';
$report_include_modules['gc_ProductRule'] = 'gc_ProductRule';
$modInvisList[] = 'gc_ProductRule';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Products.php

$bwcModules[] = 'pm_Products';
?>
<?php
// Merged from custom/Extension/application/Ext/Include/product_item_attribute.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['pi_product_item_attribute'] = 'pi_product_item_attribute';
$beanFiles['pi_product_item_attribute'] = 'modules/pi_product_item_attribute/pi_product_item_attribute.php';
$moduleList[] = 'pi_product_item_attribute';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/State.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['c_State'] = 'c_State';
$beanFiles['c_State'] = 'modules/c_State/c_State.php';
$moduleList[] = 'c_State';
$bwcModules[] = 'c_State';

?>
<?php
// Merged from custom/Extension/application/Ext/Include/TimeZone.php

 //WARNING: The contents of this file are auto-generated
$beanList['gc_TimeZone'] = 'gc_TimeZone';
$beanFiles['gc_TimeZone'] = 'modules/gc_TimeZone/gc_TimeZone.php';
$moduleList[] = 'gc_TimeZone';
$bwcModules[] = 'gc_TimeZone';

?>
<?php
// Merged from custom/Extension/application/Ext/Include/upgrade_bwc.php
 
/* This file was generated by Sugar Upgrade */
$bwcModules[] = 'be_Beluga_Export';
$bwcModules[] = 'c_State';
$bwcModules[] = 'c_country';
$bwcModules[] = 'fm_Factory';
$bwcModules[] = 'gc_AccountTransfer';
$bwcModules[] = 'gc_Contracts';
$bwcModules[] = 'gc_CreditCard';
$bwcModules[] = 'gc_DirectDeposit';
$bwcModules[] = 'gc_InvoiceSubtotalGroup';
$bwcModules[] = 'gc_LineItem';
$bwcModules[] = 'gc_Line_Item_Contract_History';
$bwcModules[] = 'gc_MyApprovals';
$bwcModules[] = 'gc_NTTComGroupCompany';
$bwcModules[] = 'gc_PostalTransfer';
$bwcModules[] = 'gc_ProductConfiguration';
$bwcModules[] = 'gc_ProductPrice';
$bwcModules[] = 'gc_ProductRule';
$bwcModules[] = 'gc_SalesRepresentative';
$bwcModules[] = 'gc_TimeZone';
$bwcModules[] = 'gc_comments';
$bwcModules[] = 'js_Accounts_js';
$bwcModules[] = 'pa_ProductAttributeValues';
$bwcModules[] = 'pa_ProductAttributes';
$bwcModules[] = 'pi_product_item';
$bwcModules[] = 'pm_Products';

?>
<?php
// Merged from custom/Extension/application/Ext/Include/SalesRepresentative.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['gc_SalesRepresentative'] = 'gc_SalesRepresentative';
$beanFiles['gc_SalesRepresentative'] = 'modules/gc_SalesRepresentative/gc_SalesRepresentative.php';
$moduleList[] = 'gc_SalesRepresentative';


?>
