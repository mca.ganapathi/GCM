<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.AccountsJapanSpecific.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['js_Accounts_js'] = 'Accounts (Japan Specific)';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.AccountTransfer.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_AccountTransfer'] = 'Account Transfer';
$app_list_strings['deposit_type_list'][''] = '';
$app_list_strings['deposit_type_list'][1] = 'Ordinary Deposit';
$app_list_strings['deposit_type_list'][2] = 'Current Deposit';
$app_list_strings['deposit_type_list'][3] = 'Tax Payment (General)';
$app_list_strings['deposit_type_list'][9] = 'Others';
$app_list_strings['account_no_display_list'][''] = '';
$app_list_strings['account_no_display_list'][0] = 'No';
$app_list_strings['account_no_display_list'][1] = 'Yes';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.BelugaExport.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['be_Beluga_Export'] = 'Beluga Export';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.BelugaProductCode.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$app_list_strings['moduleList']['bp_Beluga_Product_Code'] = 'Beluga Product Code';
$app_list_strings['moduleListSingular']['bp_Beluga_Product_Code'] = 'Beluga Product Code';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.ContractCommentsLogModule.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_comments'] = 'Contract Comments';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.ContractDeploy061015.php
 

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.ContractMyApproval.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_MyApprovals'] = 'My Approvals';
$app_list_strings['gc_approvals_action_list']['Approved'] = 'Approved';
$app_list_strings['gc_approvals_action_list']['Rejected'] = 'Rejected';
$app_list_strings['gc_approval_stages_list'][0] = 'Draft';
$app_list_strings['gc_approval_stages_list'][1] = 'Approval Stage1';
$app_list_strings['gc_approval_stages_list'][2] = 'Approval Stage2';
$app_list_strings['gc_approval_stages_list'][3] = 'Approval Stage3';
$app_list_strings['workflow_flag_list'][0] = 'Contract in progress';
$app_list_strings['workflow_flag_list'][1] = 'Contract submitted to workflow';
$app_list_strings['workflow_flag_list'][2] = 'Approved by 1st level';
$app_list_strings['workflow_flag_list'][3] = 'Approved by 2nd level';
$app_list_strings['workflow_flag_list'][4] = 'Approved by 3rd level';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Country.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['c_country'] = 'Country';
$app_list_strings['regions_list']['North_America'] = 'North America';
$app_list_strings['regions_list']['Latin_America'] = 'Latin America';
$app_list_strings['regions_list']['Asia'] = 'Asia';
$app_list_strings['regions_list']['Africa'] = 'Africa';
$app_list_strings['regions_list']['Europe'] = 'Europe';
$app_list_strings['regions_list']['Oceania'] = 'Oceania';
$app_list_strings['regions_list']['Antarctic'] = 'Antarctic';
$app_list_strings['regions_list']['Middle_East'] = 'Middle East';
$app_list_strings['regions_list']['Russia'] = 'Russia';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.CreditCard.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_CreditCard'] = 'Credit Card';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.DirectDeposit.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_DirectDeposit'] = 'Direct Deposit';
$app_list_strings['deposit_type_list'][''] = '';
$app_list_strings['deposit_type_list'][1] = 'Ordinary Deposit';
$app_list_strings['deposit_type_list'][2] = 'Current Deposit';
$app_list_strings['deposit_type_list'][3] = 'Tax Payment (General)';
$app_list_strings['deposit_type_list'][9] = 'Others';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.FactoryModule.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['fm_Factory'] = 'Factory';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.GBS_Organization.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['gc_organization'] = 'Organizations';
$app_list_strings['moduleListSingular']['gc_organization'] = 'Organization';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.GCM_Contracts.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_Contracts'] = 'Contracts';
$app_list_strings['contract_type_list'][''] = '';
$app_list_strings['contract_type_list']['GMSA'] = 'GMSA (Service Ts&Cs + SOF)';
$app_list_strings['contract_type_list']['Adhesion_Contract'] = 'Adhesion Contract';
$app_list_strings['contract_type_list']['Terms_of_Service'] = 'Terms of Service';
$app_list_strings['contract_type_list']['Individual_Contract'] = 'Individual Contract';
$app_list_strings['contract_status_list'][0] = 'Draft';
$app_list_strings['contract_status_list'][1] = 'Approved';
$app_list_strings['contract_status_list'][2] = 'Deactivated';
$app_list_strings['contract_status_list'][''] = '';
$app_list_strings['loi_contract_list'][''] = '';
$app_list_strings['loi_contract_list']['Yes'] = 'Yes';
$app_list_strings['loi_contract_list']['No'] = 'No';
$app_list_strings['billing_type_list'][''] = '';
$app_list_strings['billing_type_list']['OneStop'] = 'One-stop';
$app_list_strings['billing_type_list']['Separate'] = 'Separate';
$app_list_strings['contract_stage_list']['stage0'] = 'Stage 0';
$app_list_strings['contract_stage_list']['stage1'] = 'Stage 1';
$app_list_strings['contract_stage_list']['stage2'] = 'Stage 2';
$app_list_strings['contract_stage_list']['stage3'] = 'Stage 3';
$app_list_strings['workflow_flag_list'][0] = 'Contract in progress';
$app_list_strings['workflow_flag_list'][1] = 'Contract submitted to workflow';
$app_list_strings['workflow_flag_list'][2] = 'Approved by 1st level';
$app_list_strings['workflow_flag_list'][3] = 'Approved by 2nd level';
$app_list_strings['workflow_flag_list'][4] = 'Approved by 3rd level';
$app_list_strings['contract_scheme_list'][''] = '';
$app_list_strings['contract_scheme_list']['GSC'] = 'GSC';
$app_list_strings['contract_scheme_list']['Separate'] = 'Separate';
$app_list_strings['contract_period_unit_list'][''] = '';
$app_list_strings['contract_period_unit_list']['Days'] = 'Days';
$app_list_strings['contract_period_unit_list']['Months'] = 'Months';
$app_list_strings['contract_period_unit_list']['Years'] = 'Years';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.GCM_ProductAttributes.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['pa_ProductAttributes'] = 'Product Attributes';
$app_list_strings['moduleList']['pa_ProductAttributeValues'] = 'Product Attribute Values';
$app_list_strings['attribute_type_list'][''] = '';
$app_list_strings['attribute_type_list']['AttributeValue'] = 'Attribute Value';
$app_list_strings['attribute_type_list']['ParentAttribute'] = 'Parent Attribute';
$app_list_strings['attribute_type_list']['EntryField'] = 'Entry Field';
$app_list_strings['pa_value_data_type_list']['Text'] = 'Text';
$app_list_strings['pa_value_data_type_list']['Number'] = 'Number';
$app_list_strings['pa_value_data_type_list']['Date'] = 'Date';
$app_list_strings['pa_value_data_type_list']['DateTime'] = 'Date & Time';
$app_list_strings['pa_value_data_type_list']['Boolean'] = 'Boolean';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.GCM_ProductsModule.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['pm_Products'] = 'Products';
$app_list_strings['product_status_list']['Draft'] = 'Draft';
$app_list_strings['product_status_list']['Approved'] = 'Approved';
$app_list_strings['product_type_list']['GM1'] = 'GM1';
$app_list_strings['product_type_list']['Cloud'] = 'Cloud';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.IineItemConfigSummary.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_IineItemConfigSummary'] = 'Line Item Config Summary';
$app_list_strings['moduleList']['gc_Line_Item_Config_Summary'] = 'Line Item Config Summary';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.InvoiceSubtotalGroup.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_InvoiceSubtotalGroup'] = 'Invoice Subtotal Group';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.LineItem.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_LineItem'] = 'Line Item';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.LineItemContractHistory.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['gc_LineItemContractHistory'] = 'LineItemContractHistory';
$app_list_strings['moduleList']['gc_Line_Item_Contract_History'] = 'LineItemContractHistory';
/*$app_list_strings['payment_type_list']['AccountTransfer'] = ' Account Transfer';
$app_list_strings['payment_type_list']['PostalTransfer'] = 'Postal Transfer';
$app_list_strings['payment_type_list']['CreditCard'] = 'Credit Card';
$app_list_strings['payment_type_list']['DirectDeposit'] = 'Direct Deposit';*/
$app_list_strings['payment_type_list']['00'] = 'General: Direct Deposit';
$app_list_strings['payment_type_list'][10] = 'Account Transfer';
$app_list_strings['payment_type_list'][30] = 'Postal Transfer';
$app_list_strings['payment_type_list'][60] = 'Credit Card';
$app_list_strings['payment_type_list'][''] = '';
$app_list_strings['customer_billing_method_list'][''] = '';
$app_list_strings['customer_billing_method_list']['eCSS_Billing'] = 'eCSS Billing';
$app_list_strings['customer_billing_method_list']['GBS_Billing'] = 'GBS Billing';
$app_list_strings['customer_billing_method_list']['Beluga_Billing'] = 'Beluga Billing';
$app_list_strings['customer_billing_method_list']['A_end_Billing_Calculated_bill'] = 'A-end Billing (Calculated bill sent by Factory)';
$app_list_strings['customer_billing_method_list']['A_end_Billing_Usage_data'] = 'A-end Billing (Usage data sent by Factory)';
$app_list_strings['customer_billing_method_list']['A_end_Billing_No_charge_data'] = 'A-end Billing (No charge data sent by Factory)';

//$app_list_strings['igc_settlement_method_list']['Quarterly_Settlement'] = 'Quarterly Settlement';
//$app_list_strings['igc_settlement_method_list']['NMC_Inter_Group_Company_Transaction'] = 'NMC Inter-Group-Company Transaction';
//$app_list_strings['igc_settlement_method_list']['NMC_Inter_NTTCom_Transaction'] = 'NMC Inter-NTTCom Transaction';
$app_list_strings['igc_settlement_method_list']['Individual_Contract'] = 'Individual Contract';
$app_list_strings['igc_settlement_method_list'][''] = '';
$app_list_strings['contract_line_item_type_list']['New'] = 'New';
$app_list_strings['contract_line_item_type_list']['Change'] = 'Change';
$app_list_strings['contract_line_item_type_list']['Termination'] = 'Termination';
$app_list_strings['contract_line_item_type_list']['Activated'] = 'Activated';
$app_list_strings['bi_action_list']['add'] = 'add';
$app_list_strings['bi_action_list']['change'] = 'change';
$app_list_strings['bi_action_list']['remove'] = 'remove';
$app_list_strings['bi_action_list']['no_change'] = 'no change';
$app_list_strings['moduleListSingular']['gc_Line_Item_Contract_History'] = 'LineItemContractHistory';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.NTTComGroupCompany.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_NTTComGroupCompany'] = 'NTT Com Group Company';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.PostalTransfer.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_PostalTransfer'] = 'Postal Transfer';
$app_list_strings['postal_passbook_no_display_list'][0] = 'No';
$app_list_strings['postal_passbook_no_display_list'][1] = 'Yes';
$app_list_strings['postal_passbook_no_display_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.ProductConfiguration.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_ProductConfiguration'] = 'Product Configuration';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.ProductItem.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['pi_product_item'] = 'Product Item';
$app_list_strings['product_status_list']['Draft'] = 'Draft';
$app_list_strings['product_status_list']['Approved'] = 'Approved';
$app_list_strings['product_type_list']['GMOneWorkflow'] = 'GM One Workflow';
$app_list_strings['product_type_list']['NotApplicable'] = 'Not Applicable';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.ProductPrice.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_ProductPrice'] = 'Product Price';
$app_list_strings['moduleList']['gc_ProductPrice1'] = 'Product Price';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.ProductRule.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_ProductRule'] = 'Product Rule';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.product_item_attribute.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$app_list_strings['moduleList']['pi_product_item_attribute'] = 'Product Item Attributes';
$app_list_strings['moduleListSingular']['pi_product_item_attribute'] = 'Product Item Attributes';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.State.php

/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['c_State'] = 'State';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.StudioContractModule.php
 

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.StudioGcContract_6-2-1_060616.php
 
$app_list_strings['contract_status_list'] = array (
  0 => 'Draft',
  3 => 'Sent for Approval',
  1 => 'Approved',
  4 => 'Activated',
  5 => 'Sent for Termination Approval',
  6 => 'Termination Approved',
  2 => 'Deactivated',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.StudioInvoiceGroup_6-2-1_060616.php
 

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.StudioLineItemHistory_6-2-1_060616.php
 
$app_list_strings['payment_type_list'] = array (
  '' => '',
  '00' => 'General: Direct Deposit',
  10 => 'Account Transfer',
  30 => 'Postal Transfer',
  60 => 'Credit Card',
);$app_list_strings['igc_settlement_method_list'] = array (
  '' => '',
  'No_Settlement_per_Contract' => 'No Settlement per Contract',
  'GERP_Inter_Group_Company_Transaction' => 'GERP+ Inter-Group-Company Transaction',
  'GERP_NTTCom_Internal_Transaction' => 'GERP+ NTTCom Internal Transaction',
  'Individual_Contract' => 'Individual Contract',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.StudioLineItemHistory_Header_6-2-1_060616.php
 
$app_list_strings['payment_type_list'] = array (
  '' => '',
  '00' => 'General: Direct Deposit',
  10 => 'Account Transfer',
  30 => 'Postal Transfer',
  60 => 'Credit Card',
);$app_list_strings['igc_settlement_method_list'] = array (
  '' => '',
  'No_Settlement_per_Contract' => 'No Settlement per Contract',
  'GERP_Inter_Group_Company_Transaction' => 'GERP+ Inter-Group-Company Transaction',
  'GERP_NTTCom_Internal_Transaction' => 'GERP+ NTTCom Internal Transaction',
  'Individual_Contract' => 'Individual Contract',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.StudioProductConfig_6-2-1_060616.php
 

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.StudioProductPrice_6-2-2_060616.php
 

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.StudioProductRules_6-2-1_060616.php
 

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.SugarServiceLang.php

/*********************************************************************************
 * Custom API Service Labels
 ********************************************************************************/

$app_strings['custom_api_service']['ERR_EMPTY_REQ_PARAMS'] = 'Required parameter details are blank.';
$app_strings['custom_api_service']['MSG_NO_RECORD_UPDATE'] = 'Record not created/updated.';
$app_strings['custom_api_service']['MSG_RECORD_UPDATE_SUCCESS'] = 'Record created/updated successfully ';
$app_strings['custom_api_service']['CIDAS_NO_DATA'] = 'Data not found in CIDAS';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_billing_type_list.php

 // created: 2016-12-13 08:38:48

$app_list_strings['billing_type_list']=array (
  'OneStop' => 'One-stop',
  'Separate' => 'Separate',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_contract_status_list.php

 // created: 2016-10-06 09:29:54

$app_list_strings['contract_status_list']=array (
  0 => 'Draft',
  3 => 'Sent for Approval',
  1 => 'Approved',
  4 => 'Activated',
  5 => 'Sent for Termination Approval',
  6 => 'Termination Approved',
  2 => 'Deactivated',
  7 => 'Upgraded',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_document_category_dom.php

 // created: 2016-09-29 04:41:56

$app_list_strings['document_category_dom']=array (
  '' => '',
  'Customer' => 'Customer',
  'Partner' => 'Partner',
  'Affiliate' => 'Affiliate',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_document_template_type_dom.php

 // created: 2016-09-29 04:41:57

$app_list_strings['document_template_type_dom']=array (
  '' => '',
  'MasterServiceAgreement' => 'MSA (Master Service Agreement)',
  'ServiceDescription' => 'Service Description',
  'TsCs_ServiceSpecificTermsConditions' => 'Ts & Cs (Service Specific Terms & conditions)',
  'ServiceLevelAgreement' => 'SLA (Service Level Agreement)',
  'ServiceOrderForm' => 'SOF (Service Order Form)',
  'ScopeOfWork' => 'SoW (Scope of Work)',
  'OtherContractDocuments' => 'Other Contract Documents',
  'OtherNonContractDocuments' => 'Other Non-Contract Documents',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_moduleList.php

 //created: 2016-09-29 04:41:56

$app_list_strings['moduleList']['gc_Contracts']='GCM Contracts';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.TimeZone.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$app_list_strings['moduleList']['gc_TimeZone'] = 'Time Zone';
$app_list_strings['moduleListSingular']['gc_Line_Item_Config_Summary'] = 'Line Item Config Summary';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.SalesRepresentative.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['gc_SalesRepresentative'] = 'Sales Representative';
$app_list_strings['moduleListSingular']['gc_SalesRepresentative'] = 'Sales Representative';

?>
