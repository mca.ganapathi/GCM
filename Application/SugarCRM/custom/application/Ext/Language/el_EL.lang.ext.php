<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/el_EL.sugar_billing_type_list.php

 // created: 2016-12-13 08:38:47

$app_list_strings['billing_type_list']=array (
  'OneStop' => 'One-stop',
  'Separate' => 'Separate',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/el_EL.sugar_contract_status_list.php

 // created: 2016-10-06 09:29:52

$app_list_strings['contract_status_list']=array (
  0 => 'Draft',
  3 => 'Sent for Approval',
  1 => 'Approved',
  4 => 'Activated',
  5 => 'Sent for Termination Approval',
  6 => 'Termination Approved',
  2 => 'Deactivated',
  '' => '',
  7 => 'Upgraded',
);
?>
