<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.ContractDeploy061015.php
 

?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.StudioContractModule.php
 

?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.StudioGcContract_6-2-1_060616.php
 
$app_list_strings['contract_status_list'] = array (
  0 => 'Draft',
  1 => 'Approved',
  2 => 'Deactivated',
  '' => '',
  3 => 'Sent for Approval',
  4 => 'Activated',
  5 => 'Sent for Termination Approval',
  6 => 'Termination Approved',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.StudioInvoiceGroup_6-2-1_060616.php
 

?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.StudioLineItemHistory_6-2-1_060616.php
 
$app_list_strings['payment_type_list'] = array (
  '' => '',
  '00' => 'General: Direct Deposit',
  10 => 'Account Transfer',
  30 => 'Postal Transfer',
  60 => 'Credit Card',
);$app_list_strings['igc_settlement_method_list'] = array (
  '' => '',
  'Individual_Contract' => 'Individual Contract',
  'No_Settlement_per_Contract' => 'No Settlement per Contract',
  'GERP_Inter_Group_Company_Transaction' => 'GERP+ Inter-Group-Company Transaction',
  'GERP_NTTCom_Internal_Transaction' => 'GERP+ NTTCom Internal Transaction',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.StudioLineItemHistory_Header_6-2-1_060616.php
 
$app_list_strings['payment_type_list'] = array (
  '' => '',
  '00' => 'General: Direct Deposit',
  10 => 'Account Transfer',
  30 => 'Postal Transfer',
  60 => 'Credit Card',
);$app_list_strings['igc_settlement_method_list'] = array (
  '' => '',
  'Individual_Contract' => 'Individual Contract',
  'No_Settlement_per_Contract' => 'No Settlement per Contract',
  'GERP_Inter_Group_Company_Transaction' => 'GERP+ Inter-Group-Company Transaction',
  'GERP_NTTCom_Internal_Transaction' => 'GERP+ NTTCom Internal Transaction',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.StudioProductConfig_6-2-1_060616.php
 

?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.StudioProductPrice_6-2-2_060616.php
 

?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.StudioProductRules_6-2-1_060616.php
 

?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.sugar_billing_type_list.php

 // created: 2016-12-13 08:38:47

$app_list_strings['billing_type_list']=array (
  'OneStop' => 'One-stop',
  'Separate' => 'Separate',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.sugar_contract_status_list.php

 // created: 2016-10-06 09:29:51

$app_list_strings['contract_status_list']=array (
  0 => 'Draft',
  1 => 'Approved',
  2 => 'Deactivated',
  '' => '',
  3 => 'Sent for Approval',
  4 => 'Activated',
  5 => 'Sent for Termination Approval',
  6 => 'Termination Approved',
  7 => 'Upgraded',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.sugar_document_category_dom.php

 // created: 2016-09-29 04:41:57

$app_list_strings['document_category_dom']=array (
  '' => '',
  'Customer' => 'Customer',
  'Partner' => 'Partner',
  'Affiliate' => 'Affiliate',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.sugar_document_template_type_dom.php

 // created: 2016-09-29 04:41:57

$app_list_strings['document_template_type_dom']=array (
  '' => '',
  'MasterServiceAgreement' => 'MSA (Master Service Agreement)',
  'ServiceDescription' => 'Service Description',
  'TsCs_ServiceSpecificTermsConditions' => 'Ts & Cs (Service Specific Terms & conditions)',
  'ServiceLevelAgreement' => 'SLA (Service Level Agreement)',
  'ServiceOrderForm' => 'SOF (Service Order Form)',
  'ScopeOfWork' => 'SoW (Scope of Work)',
  'OtherContractDocuments' => 'Other Contract Documents',
  'OtherNonContractDocuments' => 'Other Non-Contract Documents',
);
?>
