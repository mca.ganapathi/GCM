<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
require_once ('custom/include/XMLUtils.php');

/**
 * CIDAS API class file
 *
 * @author Dinesh Itkar
 * @since 20 May 2016
 */
/**
 * @type unix-file system UTF-8
 */
class ClassCIDASConnection extends XMLUtils
{

    /**
     * CIDAS API URL
     */
    private $url;

    /**
     * Context Resource
     */
    private $context;

    /**
     * Flag to store boolean value. True: To get all records for CIDAS API. False: To get first record for CIDAS API.
     */
    private $get_full_list;

    /**
     * Set CIDAS record limit
     */
    const CIDAS_RECORD_LIMIT = '100';

    /**
     * Set char code
     */
    const CHAR_CODE = 'UTF8';

    /**
     * Set CIDAS path
     */
    const CIDAS_PATH = 'cid_search/corp_cust';

    public function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        // Get CIDAS connection details from config file.
        global $sugar_config;
        $cidas_user = isset($sugar_config['cidas']['user'])?$sugar_config['cidas']['user']:'';
        $cidas_pass = isset($sugar_config['cidas']['passcode'])?$sugar_config['cidas']['passcode']:'';
        $cidas_proxy_url = isset($sugar_config['cidas']['proxy_url'])?$sugar_config['cidas']['proxy_url']:'';
        $cidas_url = isset($sugar_config['cidas']['url'])?$sugar_config['cidas']['url']:'';

        // Create context resource.
        $this->context = stream_context_create(array(
                                                    "https" => array(
                                                                    "proxy" => $cidas_proxy_url,
                                                                    "request_fulluri" => true
                                                    ),
                                                    "http" => array(
                                                                    "proxy" => $cidas_proxy_url,
                                                                    "request_fulluri" => true
                                                    ),
                                                    'ssl' => array(
                                                                'SNI_enabled' => false
                                                    )
        )); // Disable SNI for https over http proxies
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'Debugging CIDAS: context array: '.print_r(array(
                                            "https" => array(
                                                            "proxy" => $cidas_proxy_url,
                                                            "request_fulluri" => true
                                            ),
                                            "http" => array(
                                                            "proxy" => $cidas_proxy_url,
                                                            "request_fulluri" => true
                                            )
        ), true), 'Debug');

        // Generate CIDAS API URL
        $this->url = $cidas_url . self::CIDAS_PATH . '?user=' . $cidas_user . '&passCode=' . $cidas_pass . '&limitCount=' . self::CIDAS_RECORD_LIMIT . '&charCode=' . self::CHAR_CODE;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'URL: '.$this->url, '');
    }

    /**
     * Method to get CIDAS details by Common Customer Id
     *
     * @param (string) $cid Common Customer Id
     * @param (boolean) $get_full_list True: To get all records for CIDAS API. False: To get first record for CIDAS API.
     * @param (int) $version_no API version number
     * @return (array) $result Convert CIDAS details into corporate database format
     */
    public function getCorporateDetailsFromCIDASByCID($cid, $get_full_list, $version_no)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'cid: '.$cid.GCM_GL_LOG_VAR_SEPARATOR.'get_full_list: '.$get_full_list.GCM_GL_LOG_VAR_SEPARATOR.'version_no: '.$version_no, ' Method to get CIDAS details by Common Customer Id');
        $result = '';
        if (!empty($cid)) {
            $this->get_full_list = $get_full_list;
            $this->generateQueryStringCommonCustomerId($cid);
            $result = $this->getCorporateDetailsFromCIDAS($version_no);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'result: '.print_r($result,true), '');
        return $result;
    }

    /**
     * Method to get CIDAS details by Corporate Name Local Character and Corporate Name Latin Character
     *
     * @param (string) $name Corporate Name Local Character
     * @param (string) $name_latin Corporate Name Latin Character
     * @param (boolean) $get_full_list True: To get all records for CIDAS API. False: To get first record for CIDAS API.
     * @return (array) $result Convert CIDAS details into corporate database format
     */
    public function getCorporateDetailsFromCIDASByName($name, $name_latin, $get_full_list)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'name: '.$name.GCM_GL_LOG_VAR_SEPARATOR.'name_latin: '.$name_latin.GCM_GL_LOG_VAR_SEPARATOR.'get_full_list: '.$get_full_list, 'Method to get CIDAS details by Corporate Name Local Character and Corporate Name Latin Character');
        if (!empty($name) || !empty($name_latin)) {
            $this->get_full_list = $get_full_list;
            if (!empty($name))
                $this->generateQueryStringCompanyName($name);
            elseif (!empty($name_latin))
                $this->generateQueryStringCompanyNameEng($name_latin);
            $result = $this->getCorporateDetailsFromCIDAS(null);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'result: '.print_r($result,true), '');
        return $result;
    }

    /**
     * Method to get CIDAS details from API
     *
     * @param (int) $version_no API version number
     * @return (array) $parse_cidas_xml_response Convert CIDAS details into corporate database format
     */
    private function getCorporateDetailsFromCIDAS($version_no)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'version_no: '.$version_no, 'Method to get CIDAS details from API');
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'URL: '.$this->url, 'The request url');
        $cidas_xml_response = $this->getDetailsFromCidasCurl($this->url);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'cidas_xml_response: '.$cidas_xml_response, 'CIDAS response in xml');
        if (!empty($cidas_xml_response)) {
            $parse_cidas_xml_response = $this->parseXmlResponse($cidas_xml_response, $version_no);
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'parse_cidas_xml_response: '.print_r($parse_cidas_xml_response,true), 'Parsed CIDAS xml response to array');
        } else {
            if (is_null($version_no)) {
                $parse_cidas_xml_response = $this->fnGetErrorXMLData(array(
                                                                            '3003'
                ), null, $this->get_full_list);
            } else {
                $parse_cidas_xml_response = array(
                                                'cidas_failed' => 1,
                                                'error_code' => '3003'
                );
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'parse_cidas_xml_response: '.print_r($parse_cidas_xml_response,true), '');
        return $parse_cidas_xml_response;
    }

    /**
     * Method to varify XML response received from CIDAS API
     *
     * @param (xml string) $cidas_xml_response
     * @param (int) $version_no API version number
     * @return (array/xml) $parse_result Convert CIDAS details into corporate database format or XML error response
     */
    private function parseXmlResponse($cidas_xml_response, $version_no)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'cidas_xml_response: '.$cidas_xml_response.GCM_GL_LOG_VAR_SEPARATOR.'version_no: '.$version_no, 'Method to varify XML response received from CIDAS API');
        $cidas_obj_xml = simplexml_load_string($cidas_xml_response, 'SimpleXMLElement', null, 'tns', true);
        $parse_result = array();

        if (isset($cidas_obj_xml->result)) {
            if ($cidas_obj_xml->result->recordCount == 0) {
                if (is_null($version_no)) {
                    $parse_result = $this->fnGetErrorXMLData(array(
                                                                    '3005'
                    ), null, $this->get_full_list);
                } else {
                    $parse_result = array(
                                        'cidas_failed' => 1,
                                        'error_code' => '3005'
                    );
                }
            } elseif ($cidas_obj_xml->result->recordCount >= 1) {
                $parse_result = $this->parseXmlResponseToArray($cidas_obj_xml->result->record);
                if (!$this->get_full_list) {
                    $cidas_first_record = isset($parse_result[0])?$parse_result[0]:array();
                    $parse_result = $cidas_first_record;
                }
            }
        } elseif (isset($cidas_obj_xml->error)) {
            $errorCode = (string) $cidas_obj_xml->error->errorCode;
            if (is_null($version_no)) {
                $parse_result = $this->fnGetErrorXMLData(array(
                                                                $errorCode
                ), null, $this->get_full_list);
            } else {
                $parse_result = array(
                                    'cidas_failed' => 1,
                                    'error_code' => $errorCode
                );
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'parse_result: '.print_r($parse_result,true), '');
        return $parse_result;
    }

    /**
     * Method to convert XML response received into array
     *
     * @param (xml string) $xml_obj_cidas_records
     * @return (array) $arr_corporate_details_record Convert CIDAS details into corporate database format
     */
    private function parseXmlResponseToArray($xml_obj_cidas_records)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'xml_obj_cidas_records: '.$xml_obj_cidas_records, 'Method to convert XML response received into array');
        $record_index = 0;
        $arr_corporate_details = $arr_corporate_details_record = array();
        if(!empty($xml_obj_cidas_records))
        {
            foreach ($xml_obj_cidas_records as $items) {
                if(!empty($items)){
                    foreach ($items as $item) {
                        $arr_corporate_details[$record_index][(string) $item->key] = (string) $item->value;
                    }
                }
                if (isset($arr_corporate_details[$record_index]['customerStatusCode']) && $arr_corporate_details[$record_index]['customerStatusCode'] == 1) {
                    $customer_status_code = 1;
                } else {
                    $customer_status_code = 0;
                }
                $arr_corporate_details_record[] = array(
                                                        'common_customer_id_c' => isset($arr_corporate_details[$record_index]['commonCustomerId'])?$arr_corporate_details[$record_index]['commonCustomerId']:'',
                                                        'name' => isset($arr_corporate_details[$record_index]['companyNameEng'])?$arr_corporate_details[$record_index]['companyNameEng']:'',
                                                        'name_2_c' => isset($arr_corporate_details[$record_index]['companyName'])?$arr_corporate_details[$record_index]['companyName']:'',
                                                        'name_3_c' => isset($arr_corporate_details[$record_index]['companyNameKana'])?$arr_corporate_details[$record_index]['companyNameKana']:'',
                                                        'country_code_c' => isset($arr_corporate_details[$record_index]['countryNameCode'])?$arr_corporate_details[$record_index]['countryNameCode']:'',
                                                        'country_name_c' => isset($arr_corporate_details[$record_index]['countryName'])?$arr_corporate_details[$record_index]['countryName']:'',
                                                        'customer_status_code_c' => $customer_status_code,
                                                        'hq_addr_1_c' => isset($arr_corporate_details[$record_index]['headquarterAddressEng'])?$arr_corporate_details[$record_index]['headquarterAddressEng']:'',
                                                        'hq_addr_2_c' => isset($arr_corporate_details[$record_index]['headquarterAddress'])?$arr_corporate_details[$record_index]['headquarterAddress']:'',
                                                        'hq_postal_no_c' => isset($arr_corporate_details[$record_index]['headquarterPostalNumber'])?$arr_corporate_details[$record_index]['headquarterPostalNumber']:'',
                                                        'reg_addr_1_c' => isset($arr_corporate_details[$record_index]['registerAddressEng'])?$arr_corporate_details[$record_index]['registerAddressEng']:'',
                                                        'reg_addr_2_c' => isset($arr_corporate_details[$record_index]['registerAddress'])?$arr_corporate_details[$record_index]['registerAddress']:'',
                                                        'reg_postal_no_c' => isset($arr_corporate_details[$record_index]['registerPostalNumber'])?$arr_corporate_details[$record_index]['registerPostalNumber']:'',
                                                        'cidas_data' => 'Yes',
                                                        'cidas_search' => 'Yes',
                                                        'is_block_exist' => 'Yes'
                );
                $record_index++;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'arr_corporate_details_record: '.print_r($arr_corporate_details_record,true), '');
        return $arr_corporate_details_record;
    }

    /**
     * Method to set query string for CommonCustomerId
     *
     * @param (string) $cid Common Customer Id
     */
    private function generateQueryStringCommonCustomerId($cid)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'cid: '.$cid, 'Method to set query string for CommonCustomerId');
        $this->url .= '&commonCustomerId=' . urlencode($cid);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'Url: '.$this->url, '');
    }

    /**
     * Method to set query string for CompanyName
     *
     * @param (string) $company_name_local Company Name Local Character
     */
    private function generateQueryStringCompanyName($company_name_local)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'company_name_local: '.$company_name_local, 'Method to set query string for CompanyName');
        $this->url .= '&companyName=' . urlencode($company_name_local);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'Url: '.$this->url, '');
    }

    /**
     * Method to set query string for CompanyNameEng
     *
     * @param (string) $company_name_Eng Company Name Latin Character
     */
    private function generateQueryStringCompanyNameEng($company_name_Eng)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'company_name_Eng: '.$company_name_Eng, 'Method to set query string for CompanyNameEng');
        $this->url .= '&companyNameEng=' . urlencode($company_name_Eng);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'Url: '.$this->url, '');
    }

    /**
     * Method to get corporate details from CIDAS using cURL
     *
     * @param (string) $url CIDAS request URL formed
     * @return (string) $data XML responded by CIDAS
     * @author Mohamed.Siddiq
     * @since 05 Oct 2016
     */
    private function getDetailsFromCidasCurl($url)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'url: '.$url, 'Method to get corporate details from CIDAS using cURL');
        global $sugar_config;
        $cidas_proxy_url = isset($sugar_config['cidas']['proxy_url'])?$sugar_config['cidas']['proxy_url']:'';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PROXY, $cidas_proxy_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // which is equal to curl -k

        $data = curl_exec($ch);
        curl_close($ch);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'data: '.print_r($data,true), '');
        return $data;
    }
}

?>