<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

/**
 * API common functions file
 *
 * @author sagar.salunkhe
 */
class ClassServiceHelper
{

    /**
     * initiate all module service class objects here
     *
     * @author sagar.salunkhe
     * @since Jun 10, 2016
     */
    private $var_api_codes;

    private $dir_api_helper;

    private $api_strings;

    /**
     * Display value when no description defined for error code
     */
    const NO_ERROR_DESC = '(No error description defined).';

    public function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        global $sugar_config;
        $this->var_api_codes = 'api_service_codes_v';
        $this->dir_api_helper = $sugar_config['root_dir_path'] . '/custom/service/v4_1_custom/APIHelper';
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    /**
     * get api response message of respective api version
     *
     * @author sagar.salunkhe
     * @param int $version
     * @param string $api_code
     * @param string $additional_dtls
     * @return string api response message
     * @since Jun 10, 2016
     */
    public function getServiceCodeDetails($version, $api_code, $additional_dtls)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'version: '.$version.GCM_GL_LOG_VAR_SEPARATOR.'api_code: '.$api_code.GCM_GL_LOG_VAR_SEPARATOR.'additional_dtls: '.$additional_dtls, 'Get api response message of respective api version');
        if (file_exists($this->dir_api_helper . "/v$version/SugarServiceCodes_v$version.php")) {
            include ($this->dir_api_helper . "/v$version/SugarServiceCodes_v$version.php");
            $this->api_strings = ${$this->var_api_codes . $version};
        }

        $res_string = isset($this->api_strings[$api_code]) ? $this->api_strings[$api_code] : self::NO_ERROR_DESC;
        $res_string .= empty($res_string) ? $additional_dtls : ' ' . $additional_dtls;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'res_string: '.$res_string, '');
        return $res_string;
    }
}