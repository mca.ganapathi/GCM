<?php

trait BeforeTriggerMiddleware
{
    /**
     * parse xml to array before cidas validation
     *
     * @author Rajul.Mondal
     * @since Sept 05, 2017
     */
    public function postBeforeInvalidCidasConnectionFailed()
    {
        $this->request_data = ApiValidationHelper::convertPostXmlToArray($this->request_data);
        $this->request_data = ApiValidationHelper::convertRequestDataDatetoGMTDate($this->request_data);
    }

    /**
     * parse xml to array before cidas validation
     *
     * @author Rajul.Mondal
     * @since Sept 26, 2017
     */
    public function versionUpBeforeInvalidCidasConnectionFailed()
    {
        $this->request_data = ApiValidationHelper::convertPostXmlToArray($this->request_data);
        $this->request_data = ApiValidationHelper::convertRequestDataDatetoGMTDate($this->request_data);
    }

    /**
     * parse xml to array before cidas validation
     *
     * @author Rajul.Mondal
     * @since Sept 26, 2017
     */
    public function patchBeforeInvalidCidasConnectionFailed()
    {
        $this->request_data = ApiValidationHelper::convertPatchXmlToArray($this->request_data, $this->global_contract_id, $this->global_contract_uuid);
        $this->request_data = ApiValidationHelper::convertRequestDataDatetoGMTDate($this->request_data);
    }

    /**
     * parse xml to array before cidas validation
     *
     * @author Rajul.Mondal
     * @since Sept 05, 2017
     */
    public function postBeforeMandatoryProductSpecificationBase()
    {
        // line item list from post request data
        $xml_line_items = $this->request_data['ContractDetails']['ContractLineItemDetails'];

        $characteristic_ids     = array();
        $characteristic_xml_ids = array();
        $rule_ids               = array();
        $rule_xml_ids           = array();
        $product_prices         = array();
        $product_xml_prices     = array();

        foreach ($xml_line_items as $key => $line_item) {
            $xml_specification_id = $line_item['ContractLineItemHeader']['product_specification'];

            $xml_product_configurations = $line_item['ContractLineProductConfiguration'];
            if (!empty($xml_product_configurations)) {
                foreach ($xml_product_configurations as $product_configuration) {
                    if (!empty($characteristic_ids[$xml_specification_id."_".$key]) && array_key_exists($product_configuration['name'], $characteristic_ids[$xml_specification_id."_".$key])) {
                        $characteristic_ids[$xml_specification_id."_".$key][$product_configuration['name']]     = 'duplicate';
                        $characteristic_xml_ids[$xml_specification_id."_".$key][$product_configuration['name']] = $product_configuration['characteristic_xml_id'];
                    } elseif (!empty($product_configuration['name'])) {
                        $characteristic_ids[$xml_specification_id."_".$key][$product_configuration['name']]     = $product_configuration['characteristic_value'];
                        $characteristic_xml_ids[$xml_specification_id."_".$key][$product_configuration['name']] = $product_configuration['characteristic_xml_id'];
                    }
                }
            }

            $xml_product_rules = $line_item['ContractLineProductRule'];
            if (!empty($xml_product_rules)) {
                foreach ($xml_product_rules as $product_rule) {
                    if (!empty($rule_ids[$xml_specification_id."_".$key]) && array_key_exists($product_rule['name'], $rule_ids[$xml_specification_id."_".$key])) {
                        $rule_ids[$xml_specification_id."_".$key][$product_rule['name']]     = 'duplicate';
                        $rule_xml_ids[$xml_specification_id."_".$key][$product_rule['name']] = $product_rule['rule_xml_id'];
                    } elseif (!empty($product_rule['name'])) {
                        $rule_ids[$xml_specification_id."_".$key][$product_rule['name']]     = $product_rule['description'];
                        $rule_xml_ids[$xml_specification_id."_".$key][$product_rule['name']] = $product_rule['rule_xml_id'];
                    }
                }
            }

            $xml_product_prices = $line_item['ContractLineProductPrice'];
            if (!empty($xml_product_prices)) {
                foreach ($xml_product_prices as $product_price) {
                    if (!empty($product_prices[$xml_specification_id."_".$key]) && array_key_exists($product_price['name'], $product_prices[$xml_specification_id."_".$key])) {
                        $product_prices[$xml_specification_id."_".$key][$product_price['name']]     = 'duplicate';
                        $product_xml_prices[$xml_specification_id."_".$key][$product_price['name']] = $product_price['charge_xml_id'];
                    } elseif (!empty($product_price['name'])) {
                        $product_prices[$xml_specification_id."_".$key][$product_price['name']]['is_new_record'] = $product_price['is_new_record'];
                        $product_prices[$xml_specification_id."_".$key][$product_price['name']]['xml_data']      = $product_price['xml_data'];
                        $product_xml_prices[$xml_specification_id."_".$key][$product_price['name']]              = $product_price['charge_xml_id'];
                    }
                }
            }
        }

        $this->xml_characteristic_ids     = $characteristic_ids;
        $this->xml_characteristic_xml_ids = $characteristic_xml_ids;
        $this->xml_rule_ids               = $rule_ids;
        $this->xml_rule_xml_ids           = $rule_xml_ids;
        $this->xml_product_prices         = $product_prices;
        $this->xml_product_xml_prices     = $product_xml_prices;
    }

    /**
     * parse xml to array before cidas validation
     *
     * @author Rajul.Mondal
     * @since Sept 05, 2017
     */
    public function versionUpBeforeMandatoryProductSpecificationBase()
    {
        $this->postBeforeMandatoryProductSpecificationBase();
    }

    /**
     * populate pms offering list
     *
     * @author Rajul.Mondal
     * @since Oct 06, 2017
     */
    public function postBeforeInvalidProductOfferingId()
    {
        $this->pms_offering_list           = ApiValidationHelper::pmsProductOfferingList();
        $this->xml_product_offering_id     = $this->request_data['ContractDetails']['ContractsHeader']['product_offering'];
        $this->xml_product_offering_xml_id = $this->request_data['ContractDetails']['ContractsHeader']['product_offering_xml_id'];
    }

    /**
     * populate pms offering list
     *
     * @author Rajul.Mondal
     * @since Oct 06, 2017
     */
    public function versionUpBeforeInvalidProductOfferingId()
    {
        $this->postBeforeInvalidProductOfferingId();
    }

    /**
     * populate pms offering details
     *
     * @author Rajul.Mondal
     * @since Oct 06, 2017
     */
    public function postBeforeMandatoryProductOfferingDetails()
    {
        $this->pms_offering_details      = ApiValidationHelper::pmsProductOfferingDetails($this->xml_product_offering_id);
        $this->pms_specification_details = $this->pms_offering_details['specification_details'];
        $this->pms_charge_mapping        = $this->pms_offering_details['charge_mapping'];
        $this->pms_charge_details        = $this->pms_offering_details['charge_details'];
    }

    /**
     * populate pms offering details
     *
     * @author Rajul.Mondal
     * @since Oct 06, 2017
     */
    public function versionUpBeforeMandatoryProductOfferingDetails()
    {
        $this->postBeforeMandatoryProductOfferingDetails();
    }

    /**
     * fetch database contract details after pms connection success & before starting priority 9
     *
     * @author Rajul.Mondal
     * @since Oct 05, 2017
     */
    public function getBeforeInvalidParams()
    {
        $this->contract_data = ApiValidationHelper::dbContractDetails($this->global_contract_id, $this->global_contract_uuid, $this->version_number, $this->interaction_status, $this->request_type);
    }

    /**
     * fetch database contract details after pms connection success & before starting priority 9
     *
     * @author Rajul.Mondal
     * @since Oct 05, 2017
     */
    public function versionUpBeforeInvalidParams()
    {
        $this->contract_data = ApiValidationHelper::dbContractDetails($this->global_contract_id, $this->global_contract_uuid, '', '', $this->request_type);
    }

    /**
     * fetch database contract details after pms connection success & before starting priority 9
     *
     * @author Rajul.Mondal
     * @since Oct 05, 2017
     */
    public function patchBeforeInvalidParams()
    {
        $this->contract_data = ApiValidationHelper::dbContractDetails($this->global_contract_id, $this->global_contract_uuid, '', '', $this->request_type);
    }

    /**
     * version_up line_item before validating logic
     *
     * @author Rajul.Mondal
     * @since Oct 20, 2017
     */
    public function versionUpBeforeMandatoryLineItemBiItem()
    {
        //
    }
}
