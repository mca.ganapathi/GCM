<?php

/**
 * GET Validation Layers
 */
require_once __DIR__ . '/layers/get/PreValidationLayer.php';
require_once __DIR__ . '/layers/get/BusinessValidationLayer.php';
require_once __DIR__ . '/layers/get/PMSValidationLayer.php';
require_once __DIR__ . '/layers/get/BeforeSaveValidationLayer.php';
require_once __DIR__ . '/layers/get/PreResponseValidationLayer.php';
/**
 * POST Validation Layers
 */
require_once __DIR__ . '/layers/post/PreValidationLayer.php';
require_once __DIR__ . '/layers/post/BusinessValidationLayer.php';
require_once __DIR__ . '/layers/post/PMSValidationLayer.php';
require_once __DIR__ . '/layers/post/BeforeSaveValidationLayer.php';
require_once __DIR__ . '/layers/post/PreResponseValidationLayer.php';
/**
 * VERSION_UP Validation Layers
 */
require_once __DIR__ . '/layers/versionup/PreValidationLayer.php';
require_once __DIR__ . '/layers/versionup/BusinessValidationLayer.php';
require_once __DIR__ . '/layers/versionup/PMSValidationLayer.php';
require_once __DIR__ . '/layers/versionup/BeforeSaveValidationLayer.php';
require_once __DIR__ . '/layers/versionup/PreResponseValidationLayer.php';
/**
 * PATCH Validation Layers
 */
require_once __DIR__ . '/layers/patch/PreValidationLayer.php';
require_once __DIR__ . '/layers/patch/BusinessValidationLayer.php';
require_once __DIR__ . '/layers/patch/PMSValidationLayer.php';
require_once __DIR__ . '/layers/patch/BeforeSaveValidationLayer.php';
require_once __DIR__ . '/layers/patch/PreResponseValidationLayer.php';
/**
 * GBS Validation Layers
 */
require_once __DIR__ . '/layers/gbs/PreValidationLayer.php';
require_once __DIR__ . '/layers/gbs/BusinessValidationLayer.php';
require_once __DIR__ . '/layers/gbs/PMSValidationLayer.php';
require_once __DIR__ . '/layers/gbs/BeforeSaveValidationLayer.php';
require_once __DIR__ . '/layers/gbs/PreResponseValidationLayer.php';
/**
 * IDWH Validation Layers
 */
require_once __DIR__ . '/layers/idwh/PreValidationLayer.php';
require_once __DIR__ . '/layers/idwh/BusinessValidationLayer.php';
require_once __DIR__ . '/layers/idwh/PMSValidationLayer.php';
require_once __DIR__ . '/layers/idwh/BeforeSaveValidationLayer.php';
require_once __DIR__ . '/layers/idwh/PreResponseValidationLayer.php';
/**
 * Common Validation Layers
 */
require_once __DIR__ . '/custom/CustomPreValidationLayer.php';
require_once __DIR__ . '/custom/CustomBusinessValidationLayer.php';
require_once __DIR__ . '/custom/CustomPMSValidationLayer.php';
require_once __DIR__ . '/custom/CustomBeforeSaveValidationLayer.php';
require_once __DIR__ . '/custom/CustomPreResponseValidationLayer.php';
/**
 * Middlewares
 */
require_once __DIR__ . '/middlewares/BeforeTriggerMiddleware.php';
require_once __DIR__ . '/middlewares/AfterTriggerMiddleware.php';
/**
 * Api Validation Helper
 */
require_once __DIR__ . '/helpers/ApiValidationHelper.php';

/**
 * ModUtils Class
 */
require_once 'custom/include/ModUtils.php';
/**
 * XMLUtils Class
 */
require_once 'custom/include/XMLUtils.php';

/**
 * Validation layer
 */
class ValidationLayer
{
    // get validation layers
    use GetPreValidationLayer, GetBusinessValidationLayer, GetPMSValidationLayer, GetBeforeSaveValidationLayer, GetPreResponseValidationLayer;
    // post validation layers
    use PostPreValidationLayer, PostBusinessValidationLayer, PostPMSValidationLayer, PostBeforeSaveValidationLayer, PostPreResponseValidationLayer;
    // version_up validation layers
    use VersionUpPreValidationLayer, VersionUpBusinessValidationLayer, VersionUpPMSValidationLayer, VersionUpBeforeSaveValidationLayer, VersionUpPreResponseValidationLayer;
    // patch validation layers
    use PatchPreValidationLayer, PatchBusinessValidationLayer, PatchPMSValidationLayer, PatchBeforeSaveValidationLayer, PatchPreResponseValidationLayer;
    // gbs validation layers
    use GbsPreValidationLayer, GbsBusinessValidationLayer, GbsPMSValidationLayer, GbsBeforeSaveValidationLayer, GbsPreResponseValidationLayer;
    // idwh validation layers
    use IdwhPreValidationLayer, IdwhBusinessValidationLayer, IdwhPMSValidationLayer, IdwhBeforeSaveValidationLayer, IdwhPreResponseValidationLayer;
    // Custom validation layers
    use CustomPreValidationLayer, CustomBusinessValidationLayer, CustomPMSValidationLayer, CustomBeforeSaveValidationLayer, CustomPreResponseValidationLayer;
    // middlewares
    use BeforeTriggerMiddleware, AfterTriggerMiddleware;

    const REQUEST_TYPE_GET        = 'get';
    const REQUEST_TYPE_POST       = 'post';
    const REQUEST_TYPE_VERSION_UP = 'version_up';
    const REQUEST_TYPE_PATCH      = 'patch';
    const REQUEST_TYPE_IDWH       = 'idwh';
    const REQUEST_TYPE_GBS        = 'gbs';

    const REQUEST_XML_VALIDATION_METHOD_SUFFIX = 'PreResponseInvalidXmlData';
    /**
     * Line Item bi:action constants
     */
    const LI_NO_CHANGE = 'no change';
    const LI_CHANGE    = 'change';
    const LI_REMOVE    = 'remove';
    const LI_ADD       = 'add';

    public $mod_utils;
    public $characteristic_ids;
    public $rule_ids;
    public $request_type;

    public function __construct()
    {
        $this->errors = array(
            'codes'    => array(),
            'messages' => array(),
            'details'  => array(),
        );

        $this->mod_utils = new ModUtils();
        $this->xml_utils = new XMLUtils();
    }

    /**
     * initialisation of api request to set properties
     *
     * @param $session_id string request session id
     * @param $params array request params other than session id
     * @param $request_type string request category [post|version_up|patch|idwh|gbs]
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function init($session_id, $params, $request_type)
    {
        $this->session_id = $session_id;
        $request_type  = $this->request_type = strtolower($request_type);

        // get
        if ($this->request_type == self::REQUEST_TYPE_GET) {
            $this->global_contract_id   = !empty($params['global_contract_id']) ? $params['global_contract_id'] : '';
            $this->global_contract_uuid = !empty($params['global_contract_uuid']) ? $params['global_contract_uuid'] : '';
            $this->version_number       = (!empty($params['version_number']) || $params['version_number'] === '0') ? $params['version_number'] : '';
            $this->interaction_status   = !empty($params['interaction_status']) ? $params['interaction_status'] : '';
        }
        // post|version_up|patch
        if ($this->request_type == self::REQUEST_TYPE_POST || $this->request_type == self::REQUEST_TYPE_VERSION_UP || $this->request_type == self::REQUEST_TYPE_PATCH) {
            $this->request_data         = !empty($params['request_data']) ? $params['request_data'] : [];
            $this->global_contract_id   = !empty($params['global_contract_id']) ? $params['global_contract_id'] : '';
            $this->global_contract_uuid = !empty($params['global_contract_uuid']) ? $params['global_contract_uuid'] : '';
            $this->version_number       = !empty($params['version_number']) ? $params['version_number'] : '';
        }
        // gbs
        if ($this->request_type == self::REQUEST_TYPE_GBS) {
            $this->accountType = !empty($params['accountType']) ? $params['accountType'] : '';
            $this->dateAfter   = !empty($params['dateAfter']) ? $params['dateAfter'] : '';
            $this->dateBefore  = !empty($params['dateBefore']) ? $params['dateBefore'] : '';
        }
        // idwh
        if ($this->request_type == self::REQUEST_TYPE_IDWH) {
            $this->contractCustomerId = !empty($params['contractCustomerId']) ? $params['contractCustomerId'] : '';
            $this->contractProductId  = !empty($params['contractProductId']) ? $params['contractProductId'] : '';
            $this->updatedAfter       = !empty($params['updatedAfter']) ? $params['updatedAfter'] : '';
            $this->contractStatus     = !empty($params['contractStatus']) ? $params['contractStatus'] : '';
            $this->versionFlag        = !empty($params['versionFlag']) ? $params['versionFlag'] : '';
        }

        // generating priority list from config/api_config
        $this->priority_list = ApiValidationHelper::generatePriorityList($this->request_type);
    }

    /**
     * Validate GET
     *
     * @param $request_data array
     * @param $global_contract_id string
     * @param $global_contract_uuid string
     * @return array
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function validateGETXMLData($session_id, $global_contract_uuid, $global_contract_id, $version_number, $interaction_status)
    {
        $request_type = self::REQUEST_TYPE_GET;
        $params       = compact('global_contract_uuid', 'global_contract_id', 'version_number', 'interaction_status');
        // initialize request data
        $this->init($session_id, $params, $request_type);

        // call priority list
        $success = $this->priority();

        // merge error if there is error in pre validation layer
        if ($success == false) {
            $this->mergeErrors($this->pre_errors);
            $this->mergeErrors($this->business_errors);
            $this->mergeErrors($this->pms_errors);
            $this->mergeErrors($this->before_save_errors);
        }

        // xml = has error
        // false = no error
        if (!empty($this->errors['codes'])) {
            return $this->xml_utils->fnGetErrorXMLDataV2($this->errors);
        }

        return false;
    }

    /**
     * Validate POST xml data
     *
     * @param $request_data array
     * @param $global_contract_id string
     * @param $global_contract_uuid string
     * @return array
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function validatePOSTXMLData($session_id, $request_data, $global_contract_id, $global_contract_uuid, $version_number)
    {
        $request_type = self::REQUEST_TYPE_POST;
        $params       = compact('request_data', 'global_contract_id', 'global_contract_uuid', 'version_number');
        // initialize request data
        $this->init($session_id, $params, $request_type);

        // call priority list
        $success = $this->priority();

        // merge error if there is error in pre validation layer
        if ($success == false) {
            $this->mergeErrors($this->pre_errors);
            $this->mergeErrors($this->business_errors);
            $this->mergeErrors($this->pms_errors);
            $this->mergeErrors($this->before_save_errors);
        }

        // xml = has error
        // false = no error
        if (!empty($this->errors['codes'])) {
            return $this->xml_utils->fnGetErrorXMLDataV2($this->errors);
        }

        return false;
    }

    /**
     * Validate POST Version Up xml data
     *
     * @param $request_data array
     * @param $global_contract_id string
     * @param $global_contract_uuid string
     * @return array
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function validateVersionUpPOSTXMLData($session_id, $request_data, $global_contract_uuid, $global_contract_id, $version_number)
    {
        $request_type = self::REQUEST_TYPE_VERSION_UP;
        $params       = compact('request_data', 'global_contract_id', 'global_contract_uuid', 'version_number');
        // initialize request data
        $this->init($session_id, $params, $request_type);

        // call priority list
        $success = $this->priority();

        // merge error if there is error in pre validation layer
        if ($success == false) {
            $this->mergeErrors($this->pre_errors);
            $this->mergeErrors($this->business_errors);
            $this->mergeErrors($this->pms_errors);
            $this->mergeErrors($this->before_save_errors);
        }

        // xml = has error
        // false = no error
        if (!empty($this->errors['codes'])) {
            return $this->xml_utils->fnGetErrorXMLDataV2($this->errors);
        }

        return false;
    }

    /**
     * Validate PATCH xml data
     *
     * @param $session_id string
     * @param $request_data array
     * @param $global_contract_id string
     * @param $global_contract_uuid string
     * @return array
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function validatePATCHXMLData($session_id, $request_data, $global_contract_id, $global_contract_uuid, $version_number)
    {
        $request_type = self::REQUEST_TYPE_PATCH;
        $params       = compact('request_data', 'global_contract_id', 'global_contract_uuid', 'version_number');
        // initialize request data
        $this->init($session_id, $params, $request_type);

        // call priority list
        $success = $this->priority();

        // merge error if there is error in pre validation layer
        if ($success == false) {
            $this->mergeErrors($this->pre_errors);
            $this->mergeErrors($this->business_errors);
            $this->mergeErrors($this->pms_errors);
            $this->mergeErrors($this->before_save_errors);
        }

        // xml = has error
        // false = no error
        if (!empty($this->errors['codes'])) {
            return $this->xml_utils->fnGetErrorXMLDataV2($this->errors);
        }

        return false;
    }

    /**
     * Validate IDWH xml data
     *
     * @param $session_id string
     * @param $request_data array
     * @param $global_contract_id string
     * @param $global_contract_uuid string
     * @return array
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function validateIDWHXMLData($session_id, $contractCustomerId, $contractProductId, $updatedAfter, $contractStatus, $versionFlag)
    {
        $request_type = self::REQUEST_TYPE_IDWH;
        $params       = compact('contractCustomerId', 'contractProductId', 'updatedAfter', 'contractStatus', 'versionFlag');
        // initialize request data
        $this->init($session_id, $params, $request_type);

        // call priority list
        $success = $this->priority();

        // merge error if there is error in pre validation layer
        if ($success == false) {
            $this->mergeErrors($this->pre_errors);
            $this->mergeErrors($this->business_errors);
            $this->mergeErrors($this->pms_errors);
            $this->mergeErrors($this->before_save_errors);
        }

        // xml = has error
        // false = no error
        if (!empty($this->errors['codes'])) {
            return $this->xml_utils->fnGetErrorXMLDataV2($this->errors);
        }

        return false;
    }

    /**
     * Validate GBS xml data
     * @param $session_id string
     * @param $request_data array
     * @param $global_contract_id string
     * @param $global_contract_uuid string
     * @return array
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function validateGBSXMLData($session_id, $accountType, $dateAfter, $dateBefore)
    {
        $request_type = self::REQUEST_TYPE_GBS;
        $params       = compact('accountType', 'dateAfter', 'dateBefore');
        // initialize request data
        $this->init($session_id, $params, $request_type);

        // call priority list
        $success = $this->priority();

        // merge error if there is error in pre validation layer
        if ($success == false) {
            $this->mergeErrors($this->pre_errors);
            $this->mergeErrors($this->business_errors);
            $this->mergeErrors($this->pms_errors);
            $this->mergeErrors($this->before_save_errors);
        }

        // xml = has error
        // false = no error
        if (!empty($this->errors['codes'])) {
            return $this->xml_utils->fnGetErrorXMLDataV2($this->errors);
        }

        return false;
    }

    /**
     * Validate Response xml data
     * @param $response_xml xml
     * @param $request_type string get|post|patch|idwh|gbs
     * @return xml
     *
     * @author Rajul.Mondal
     * @since Sept 25, 2017
     */
    public function validateResponseXMLData($response_xml, $request_type)
    {
        $this->response_xml = $response_xml;
        $method_name        = $request_type . self::REQUEST_XML_VALIDATION_METHOD_SUFFIX;

        // call validation method
        $success = $this->{$method_name}();

        // merge error if there is error in pre validation layer
        if ($success == false) {
            $this->errors = $this->pre_response_errors;
        }

        // xml = has error
        // false = no error
        if (!empty($this->errors['codes'])) {
            return $this->xml_utils->fnGetErrorXMLDataV2($this->errors);
        }

        return false;
    }

    /**
     * Magic method to call method dynamically according to the priority set in config/api_config
     * @param $name string
     * @param $arguments array
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function __call($name, $arguments)
    {
        $current_priority       = 1;
        $current_group          = 1;
        $current_priority_level = $current_priority . $current_group;
        $advance_to_next_level  = true;
        $has_missing_methods    = false;

        // validate priority_list
        if (empty($this->priority_list)) {
            echo "Please set priority in config/api_config";die();
        }

        foreach ($this->priority_list as $priority_level => $priority_details) {
            $max_method_count = count($priority_details);
            $has_error        = false;
            foreach ($priority_details as $current_method_index => $method_name) {
                $snake_case_method_name = ApiValidationHelper::fromCamelCase($method_name);
                $method_details         = explode('_', $snake_case_method_name);
                // validate if method dosn't exist in layer
                if (!method_exists($this, $method_name)) {
                    $has_missing_methods = true;

                    if ($method_details[1] == 'pre') {
                        $layer_name = 'PreValidationLayer.php';
                    } else if ($method_details[1] == 'business') {
                        $layer_name = 'BusinessValidationLayer.php';
                    } else if ($method_details[1] == 'pms') {
                        $layer_name = 'PMSValidationLayer.php';
                    } else if ($method_details[1] == 'beforesave') {
                        $layer_name = 'BeforeSaveValidationLayer.php';
                    } else if ($method_details[1] == 'before') {
                        $layer_name = 'BeforeTriggerMiddleware.php';
                    } else if ($method_details[1] == 'after') {
                        $layer_name = 'AfterTriggerMiddleware.php';
                    }

                    echo "In `" . $layer_name . "` method `" . $method_name . "` not exist <br>";
                    exit;
                }

                $success = null;

                if ($advance_to_next_level == true) {
                    $success = $this->{$method_name}($arguments);
                } else {
                    break;
                }

                if (in_array($method_details[1], ['before', 'after']) || in_array($method_details[2], ['before', 'after'])) {
                    $success = true;
                }

                if ($success === false) {
                    $has_error = true;
                }

                $current_method_index += 1;

                if ($success === false && $current_priority_level == $priority_level && $current_method_index != $max_method_count) {
                    $advance_to_next_level = true;
                } else if ($success === false && $current_priority_level == $priority_level && $current_method_index == $max_method_count) {
                    $advance_to_next_level = false;
                } else if ($success === true && $current_priority_level == $priority_level && $current_method_index != $max_method_count) {
                    $advance_to_next_level = true;
                } else if ($success === true && $current_priority_level == $priority_level && $current_method_index == $max_method_count) {
                    if ($has_error == false) {
                        $advance_to_next_level = true;
                    } else {
                        $advance_to_next_level = false;
                    }
                } else if ($success === false && $current_priority_level != $priority_level && $current_method_index != $max_method_count) {
                    $advance_to_next_level  = true;
                    $current_priority_level = $priority_level;
                } else if ($success === false && $current_priority_level != $priority_level && $current_method_index == $max_method_count) {
                    $advance_to_next_level = false;
                } else if ($success === true && $current_priority_level != $priority_level && $current_method_index != $max_method_count) {
                    $advance_to_next_level  = true;
                    $current_priority_level = $priority_level;
                } else if ($success === true && $current_priority_level != $priority_level && $current_method_index == $max_method_count) {
                    if ($has_error == false) {
                        $advance_to_next_level = true;
                    } else {
                        $advance_to_next_level = false;
                    }
                } else {
                    $advance_to_next_level = false;
                }
            }
        }
    }

    /**
     * merge errors details
     *
     * @param $errors array
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function mergeErrors($errors)
    {
        if (empty($errors)) {
            return true;
        }

        $this->errors          = ApiValidationHelper::mergeArrays($this->errors, $errors, '');
        $this->errors['codes'] = array_unique($this->errors['codes']);
    }

}
