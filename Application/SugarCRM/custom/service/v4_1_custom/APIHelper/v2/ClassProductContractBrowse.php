<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once 'custom/include/XMLUtils.php';
require_once 'custom/service/v4_1_custom/APIHelper/v2/XMLBrowse.php';

/**
 * ClassProductContractBrowse Class
 */
class ClassProductContractBrowse extends XMLUtils
{

    private $xml;
    private $schema_path;
    private $root;

    /**
     * Class Constructor to initialize class variables
     */
    public function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Constructor function to initialize the variables');
        $GLOBALS['log']->info("Begin: ClassProductContractBrowse->__construct()");
        $this->xml         = '';
        $this->schema_path = '';
        $this->root        = array();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * Method to construct the get response for contract
     *
     * @param array $array_res_array
     * @return xml Returns the xml string.
     * @author Kamal.Thiyagarajan
     * @since Nov 23, 2016
     */
    public function getProductContractXML($array_res_array)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'array_res_array: '.print_r($array_res_array,true), 'Method to construct the get response for contract');
        global $sugar_config;
        $arr_respone_data = array();
        if (!is_array($array_res_array) || (is_array($array_res_array) && count($array_res_array) < 1)) {
            $arr_respone_data['respone_code']     = '1';
            $arr_respone_data['respone_xml_data'] = '';
        } else {
            $this->schema_path = 'https://gcm.ntt.com/api/xml-schema/v2/';
            $this->setRootNodes();
            $this->xml .= xmlProductRootBlock($this->root);
            $this->xml .= xmlProductContratBlock($array_res_array);
            $this->xml .= "
            </gcm-lopc:list-of-product-contracts>";

            $arr_respone_data['respone_code']     = '1';
            $arr_respone_data['respone_xml_data'] = $this->xml;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_respone_data: '.print_r($arr_respone_data,true), '');
        return $arr_respone_data;
    }

    /**
     * Method to build xml paths and respective field name for value replace
     *
     * @author Kamal.Thiyagarajan
     * @since Nov 23, 2016
     */
    private function setRootNodes()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to build xml paths and respective field name for value replace');
        global $sugar_config;
        $local_schema_path = $sugar_config['site_url'] . 'api/xml-schema/v2/system-specific/gcm/';
        $this->root        = array(
            'xsi:schemaLocation' => $this->schema_path . 'system-specific/gcm/gcm-lopc ' . $local_schema_path . 'list-of-product-contracts.xsd',
            'xmlns:gcm-lopc'     => $this->schema_path . 'system-specific/gcm/gcm-lopc',
            'xmlns:datatypes'    => $this->schema_path . 'cbe-datatypes',
            'xmlns:cbe'          => $this->schema_path . 'common-business-entity',
            'xmlns:xsi'          => 'http://www.w3.org/2001/XMLSchema-instance',
            'xmlns:xsd'          => 'http://www.w3.org/2001/XMLSchema',
        );
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'root: '.print_r($this->root,true), '');
    }
}
