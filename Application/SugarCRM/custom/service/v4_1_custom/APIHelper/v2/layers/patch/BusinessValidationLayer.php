<?php

trait PatchBusinessValidationLayer
{
	/**
     * Contract Reference ID should not be empty in patch request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Oct 16, 2017
     */

    private $null_possible_err_list;
    private $agreement_period_err_list;
    private $line_item_dates_err_list;
	private $billing_acc_activation_err_lists;

    public function patchBusinessMandatoryParams()
    {
        return $this->getBusinessMandatoryParams();
    }

    public function patchBusinessInvalidContractHeaderFieldsMatch()
    {
        $has_error = false;

        $obj_contract    = $this->contract_data['header'];
        $contract_header = (isset($this->request_data['ContractDetails']['ContractsHeader']) && !empty(array_filter($this->request_data['ContractDetails']['ContractsHeader']))) ? $this->request_data['ContractDetails']['ContractsHeader'] : array();

        foreach ($contract_header as $fieldname => $value) {

            if (strpos($fieldname, 'timezone') !== false || strpos($fieldname, '_xml_id') === false) {
                continue;
            }

            $fieldname = str_replace('_xml_id', '_uuid', $fieldname);
            if ($value != $obj_contract->$fieldname) {
                $has_error = true;

                $xpath_ids = array(
                    'field_xml_id' => $value,
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }

        if ($has_error) {
            return false;
        }

        return true;
    }

    public function patchBusinessInvalidLineItemHeaderUuidMatch()
    {
        $has_error = false;

        $obj_lineitems = $this->contract_data['line_items'];

        $line_item_details = (isset($this->request_data['ContractDetails']['ContractLineItemDetails']) && !empty(array_filter($this->request_data['ContractDetails']['ContractLineItemDetails']))) ? $this->request_data['ContractDetails']['ContractLineItemDetails'] : array();

        foreach ($line_item_details as $index => $line_item) {
            foreach ($obj_lineitems as $i => $db_lineitem) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $db_lineitem['ContractLineItemHeader']['line_item_id_xml_id']) {
                    foreach ($line_item['ContractLineItemHeader'] as $fieldname => $value) {

                        if (strpos($fieldname, 'timezone') !== false || strpos($fieldname, '_xml_id') === false) {
                            continue;
                        }

                        if ($value != $db_lineitem['ContractLineItemHeader'][$fieldname]) {
                            $has_error = true;

                            $xpath_ids = array(
                                'xml_field_value' => $line_item['ContractLineItemHeader'][str_replace('_xml_id', '', $fieldname)],
                                'field_xml_id'    => $value,
                            );
                            $this->addBusinessValidationErrors($xpath_ids);
                        }
                    }
                } else {
                    continue;
                }

            }
        }

        if(isset($this->request_data['uuid_not_in_db']) && is_array($this->request_data['uuid_not_in_db'])) {
            foreach($this->request_data['uuid_not_in_db'] as $field => $field_uuids){
                foreach($field_uuids as $i => $uuids) {
                    $has_error = true;
                     $xpath_ids = array(
                                'field_xml_id'    => $uuids
                            );
                    $this->addBusinessValidationErrors($xpath_ids);
                }
            }
        }

        if ($has_error) {
            return false;
        }

        return true;
    }

    /**
     * [global contract id] PATCH invalid request params
     *
     * @return boolean
     *
     * @author rajul.mondal
     * @since Oct 04, 2017
     */
    public function patchBusinessInvalidParams()
    {
        return $this->getBusinessInvalidParams();
    }

    /**
     * mandatory contract name value check in patch
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractName()
    {
      return $this->postBusinessMandatoryContractName();
    }

    /**
     * restrict patch for billing type value is "Separate" in db
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidBillingTypeDb()
    {
        return $this->versionUpBusinessInvalidBillingTypeDb();
    }

    public function patchBusinessInvalidBillingType()
    {
      return $this->postBusinessInvalidBillingType();
    }

    /**
     * lock version mandatory in PATCH
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryLockVersion()
    {
        if(!isset($this->request_data['ContractDetails']['ContractsHeader']['lock_version'])){
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * restrict patch for lock version mismatch
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidLockVersion()
    {
        if(isset($this->request_data['ContractDetails']['ContractsHeader']['lock_version'])){
            $lock_version_db  = $this->contract_data['header']->lock_version;
            $lock_version_xml = $this->request_data['ContractDetails']['ContractsHeader']['lock_version'];
            if ($lock_version_db != $lock_version_xml) {
                $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
                $xpath_ids                   = array(
                    'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                    'lock_version'                => $lock_version_xml,
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }

    /**
     * restrict patch for deactivated contract
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidContractStatusDeactivateDb()
    {
        return $this->versionUpBusinessInvalidContractStatusDeactivateDb();
    }

    /**
     * invalid standard-business-state in patch xml
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidContractStatusXml()
    {
        if (array_key_exists('contract_status_xml_val', $this->request_data['ContractDetails']['ContractsHeader'])) {
            $contract_status_xml_val = trim($this->request_data['ContractDetails']['ContractsHeader']['contract_status_xml_val']);
            if (!empty($contract_status_xml_val)) {
                if (!in_array($contract_status_xml_val, $GLOBALS['app_list_strings']['contract_status_list'])) {
                    $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
                    $xpath_ids                   = array(
                        'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                        'contract_status_xml'         => $contract_status_xml_val,
                    );
                    $this->addBusinessValidationErrors($xpath_ids);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * patch another than "Sent for Termination Approval" and "Deactivated"
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidContractStatusApplicable()
    {
        if (isset($this->request_data['ContractDetails']['ContractsHeader']['contract_status'])) {
            $contract_status_xml_id = trim($this->request_data['ContractDetails']['ContractsHeader']['contract_status']);
            $contract_status_xml    = trim($this->request_data['ContractDetails']['ContractsHeader']['contract_status_xml_val']);

            if (!empty($contract_status_xml_id) && $contract_status_xml_id != 5 && $contract_status_xml_id != 2) {
                $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
                $xpath_ids                   = array(
                    'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                    'contract_status_xml'         => $contract_status_xml,
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }

    /**
     * product offering and contract status mismatch in patch
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidContractStatusApplicablePo()
    {
        if (isset($this->request_data['ContractDetails']['ContractsHeader']['contract_status'])) {
            $contract_status_xml_id = trim($this->request_data['ContractDetails']['ContractsHeader']['contract_status']);
            $contract_status_xml    = $GLOBALS['app_list_strings']['contract_status_list'][$contract_status_xml_id];
            if (!empty($contract_status_xml)) {
                $has_error = false;
                global $sugar_config;
                $contract_status_db          = $this->contract_data['header']->contract_status;
                $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
                $product_offering_db         = $this->contract_data['header']->product_offering;
                if (isset($sugar_config['enterprise_mail_po']) && in_array($product_offering_db, $sugar_config['enterprise_mail_po']) && ($contract_status_xml_id != 2 || ($contract_status_xml_id == 2 && $contract_status_db != 4))) {
                    $has_error = true;
                } elseif ((isset($sugar_config['workflow_flag'][$product_offering_db]) && $sugar_config['workflow_flag'][$product_offering_db] == 'Yes' && !in_array($product_offering_db, $sugar_config['enterprise_mail_po']) && $contract_status_xml_id == 2) || ((!isset($sugar_config['workflow_flag'][$product_offering_db]) || (isset($sugar_config['workflow_flag'][$product_offering_db]) && $sugar_config['workflow_flag'][$product_offering_db] == 'No')) && $contract_status_xml_id == 5)) {
                    $has_error = true;
                }
                if ($has_error) {
                    $xpath_ids = array(
                        'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                        'contract_status_xml'         => $contract_status_xml,
                        'product_offering_db'         => ApiValidationHelper::formatPMSID($product_offering_db, 1),
                    );
                    $this->addBusinessValidationErrors($xpath_ids);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * contract start date is mandatory in patch
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractStartdate()
    {
        $this->agreement_period_err_list = $this->patchAgreementPeriodValidation($this->request_data['ContractDetails']['ContractsHeader']);
        if (isset($this->agreement_period_err_list['start_mandatory_failed']) && $this->agreement_period_err_list['start_mandatory_failed']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * contract end date from xml should be greater than contract start date in db
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidAgreementPeriodEndDateXml()
    {
        if (isset($this->agreement_period_err_list['date_compare_failed_ced_xml']) && $this->agreement_period_err_list['date_compare_failed_ced_xml']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                'contract_startdate' => $this->agreement_period_err_list['contract_startdate'],
                'contract_enddate' => $this->agreement_period_err_list['contract_enddate'],
                'contract_enddate_timezone' => $this->request_data['ContractDetails']['ContractsHeader']['contract_enddate_timezone'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * contract end date as system date should be greater than contract start date in db
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidAgreementPeriodEndDateSys()
    {
       if (isset($this->agreement_period_err_list['date_compare_failed_ced_sys'])&& $this->agreement_period_err_list['date_compare_failed_ced_sys']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                'contract_startdate' => $this->agreement_period_err_list['contract_startdate'],
                'contract_enddate' => $this->agreement_period_err_list['contract_enddate'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }


    /**
     * invalid organization code 1 in patch
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidBillingAffiliateOrgCode1()
    {
        return $this->postBusinessInvalidBillingAffiliateOrgCode1();
    }

    /**
     * invalid organization code 2 in patch
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidBillingAffiliateOrgCode2()
    {
        return $this->postBusinessInvalidBillingAffiliateOrgCode2();
    }

    /**
     * organization code 1 and organization code 2 combination is invalid
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidBillingAffiliateOrgCodeCompare()
    {
        return $this->postBusinessInvalidBillingAffiliateOrgCodeCompare();
    }

    /**
     * mandatory check of contract line credit card number in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLineCreditcardNo(){
        return $this->postBusinessMandatoryContractLineCreditcardNo();
    }

    /**
     * mandatory check of contract line credit card expiration-date in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLineCreditcardExpDate(){
        return $this->postBusinessMandatoryContractLineCreditcardExpDate();
    }

    /**
     * mandatory check of contract line direct deposit bank code in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLineDirectDepositBankcode(){
        return $this->postBusinessMandatoryContractLineDirectDepositBankcode();
    }

    /**
     * mandatory check of contract line direct deposit branch code in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLineDirectDepositBranchcode(){
        return $this->postBusinessMandatoryContractLineDirectDepositBranchcode();
    }

    /**
     * mandatory check of contract line direct deposit deposit-type in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLineDirectDepositType(){
        return $this->postBusinessMandatoryContractLineDirectDepositType();
    }

    /**
     * mandatory check of contract line direct deposit account-number in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLineDirectDepositAccno(){
        return $this->postBusinessMandatoryContractLineDirectDepositAccno();
    }

    /**
     * mandatory check of contract line account transfer name in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLineAccTransferName(){
        return $this->postBusinessMandatoryContractLineAccTransferName();
    }

    /**
     * mandatory check of contract line account transfer bank code in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLineAccTransferBankcode(){
        return $this->postBusinessMandatoryContractLineAccTransferBankcode();
    }

    /**
     * mandatory check of contract line account transfer branch code in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLineAccTransferBranchcode(){
        return $this->postBusinessMandatoryContractLineAccTransferBranchcode();
    }

    /**
     * mandatory check of contract line account transfer type code in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLineAccTransferType(){
        return $this->postBusinessMandatoryContractLineAccTransferType();
    }

    /**
     * mandatory check of contract line account transfer account number in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLineAccTransferAccno(){
        return $this->postBusinessMandatoryContractLineAccTransferAccno();
    }

    /**
     * mandatory check of contract line account transfer account number display in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLineAccTransferAccnoDisplay(){
        return $this->postBusinessMandatoryContractLineAccTransferAccnoDisplay();
    }

    /**
     * mandatory check of contract line postal transfer account holder's name in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLinePostalTransferName(){
        return $this->postBusinessMandatoryContractLinePostalTransferName();
    }


    /**
     * mandatory check of contract line postal transfer Postal Passbook Mark in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLinePostalTransferPassbookMark(){
        return $this->postBusinessMandatoryContractLinePostalTransferPassbookMark();
    }

    /**
     * mandatory check of contract line postal transfer Postal Passbook Number in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLinePostalTransferPassbook(){
        return $this->postBusinessMandatoryContractLinePostalTransferPassbook();
    }


    /**
     * mandatory check of contract line postal transfer Postal Passbook Number Display in PATCH request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractLinePostalTransferPassbookNoDisplay(){
        return $this->postBusinessMandatoryContractLinePostalTransferPassbookNoDisplay();
    }

    /**
     * service start date is mandatory in patch
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryServiceStartdate()
    {
        $has_error = false;

        $line_item_details_xml = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        $line_item_details_db = $this->contract_data['line_items'];
        $contract_status = $this->request_data['ContractDetails']['ContractsHeader']['contract_status'];
        $this->line_item_dates_err_list = $this->patchGetLineItemDateErrorList($line_item_details_xml,$line_item_details_db,$contract_status);

        if(isset($this->line_item_dates_err_list['service_start_date_mandatory_failed'])){
            $service_start_date_mandatory_failed_lists = $this->line_item_dates_err_list['service_start_date_mandatory_failed'];
            foreach($service_start_date_mandatory_failed_lists as $service_start_date_mandatory_failed_list){
                $has_error = true;
                $xpath_ids = array(
                    'line_item_xml_id'          => $service_start_date_mandatory_failed_list['line_item_id_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    /**
     * service start date and service end date range validation in patch
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidServiceDateRange()
    {
        $has_error = false;
        if(isset($this->line_item_dates_err_list['service_date_compare_failed'])){
            $service_date_compare_failed_lists = $this->line_item_dates_err_list['service_date_compare_failed'];
            foreach($service_date_compare_failed_lists as $service_date_compare_failed_list){
                $has_error = true;
                $xpath_ids = array(
                    'service_start_date_err_msg' => $service_date_compare_failed_list['service_start_date_err_msg'],
                    'service_end_date_err_msg' => $service_date_compare_failed_list['service_end_date_err_msg'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    /**
     * billing start date is mandatory in patch
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryBillingStartdate()
    {
        $has_error = false;
        if(isset($this->line_item_dates_err_list['billing_start_date_mandatory_failed'])){
            $billing_start_date_mandatory_failed_lists = $this->line_item_dates_err_list['billing_start_date_mandatory_failed'];
            foreach($billing_start_date_mandatory_failed_lists as $billing_start_date_mandatory_failed_list){
                $has_error = true;
                $xpath_ids = array(
                    'line_item_xml_id'          => $billing_start_date_mandatory_failed_list['line_item_id_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    /**
     * billing start date and billing end date range validation in patch
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidBillingDateRange()
    {
        $has_error = false;
        if(isset($this->line_item_dates_err_list['billing_date_compare_failed'])){
            $billing_date_compare_failed_lists = $this->line_item_dates_err_list['billing_date_compare_failed'];
            foreach($billing_date_compare_failed_lists as $billing_date_compare_failed_list){
                $has_error = true;
                $xpath_ids = array(
                    'billing_start_date_err_msg'          => $billing_date_compare_failed_list['billing_start_date_err_msg'],
                    'billing_end_date_err_msg'          => $billing_date_compare_failed_list['billing_end_date_err_msg'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    /**
     * One stop dependency (osd) check for Customer Contract Currency
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidOsdCustContractCurrency()
    {
        $osd_failed                  = false;
        $prev_cust_contract_currency = '';
        $line_items                  = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        for ($i = 0; $i <= count($this->request_data['ContractDetails']['ContractLineItemDetails']) - 1; $i++) {
            $cust_contract_currency = $this->request_data['ContractDetails']['ContractLineItemDetails'][$i]['ContractLineItemHeader']['cust_contract_currency'];
            if (!empty($cust_contract_currency)) {
                if ($i == 0) {
                    $prev_cust_contract_currency = $cust_contract_currency;
                } else {
                    if ($prev_cust_contract_currency != $cust_contract_currency) {
                        $osd_failed                    = true;
                        $cust_contract_currency_xml_id = $this->request_data['ContractDetails']['ContractLineItemDetails'][$i]['ContractLineItemHeader']['cust_contract_currency_xml_id'];
                        break;
                    }
                }
            }
        }
        if ($osd_failed) {
            $xpath_ids = array(
                'cust_contract_currency_id' => $cust_contract_currency_xml_id,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }
    /**
     * One stop dependency (osd) check for Customer Billing Currency
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidOsdCustBillingCurrency()
    {
        $osd_failed                 = false;
        $prev_cust_billing_currency = '';
        $line_items                 = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        for ($i = 0; $i <= count($this->request_data['ContractDetails']['ContractLineItemDetails']) - 1; $i++) {
            $cust_billing_currency = $this->request_data['ContractDetails']['ContractLineItemDetails'][$i]['ContractLineItemHeader']['cust_billing_currency'];
            if (!empty($cust_billing_currency)) {
                if ($i == 0) {
                    $prev_cust_billing_currency = $cust_billing_currency;
                } else {
                    if ($prev_cust_billing_currency != $cust_billing_currency) {
                        $osd_failed                   = true;
                        $cust_billing_currency_xml_id = $this->request_data['ContractDetails']['ContractLineItemDetails'][$i]['ContractLineItemHeader']['cust_billing_currency_xml_id'];
                        break;
                    }
                }
            }
        }
        if ($osd_failed) {
            $xpath_ids = array(
                'cust_billing_currency_id' => $cust_billing_currency_xml_id,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }
    /**
     * One stop dependency (osd) check for IGC Contract Currency
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidOsdIgcContractCurrency()
    {
        $osd_failed                 = false;
        $prev_igc_contract_currency = '';
        $line_items                 = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        for ($i = 0; $i <= count($this->request_data['ContractDetails']['ContractLineItemDetails']) - 1; $i++) {
            $igc_contract_currency = $this->request_data['ContractDetails']['ContractLineItemDetails'][$i]['ContractLineItemHeader']['igc_contract_currency'];
            if (!empty($igc_contract_currency)) {
                if ($i == 0) {
                    $prev_igc_contract_currency = $igc_contract_currency;
                } else {
                    if ($prev_igc_contract_currency != $igc_contract_currency) {
                        $osd_failed                   = true;
                        $igc_contract_currency_xml_id = $this->request_data['ContractDetails']['ContractLineItemDetails'][$i]['ContractLineItemHeader']['igc_contract_currency_xml_id'];
                        break;
                    }
                }
            }
        }
        if ($osd_failed) {
            $xpath_ids = array(
                'igc_contract_currency_id' => $igc_contract_currency_xml_id,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }
    /**
     * One stop dependency (osd) check for IGC Billing Currency
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidOsdIgcBillingCurrency()
    {
        $osd_failed                = false;
        $prev_igc_billing_currency = '';
        $line_items                = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        for ($i = 0; $i <= count($this->request_data['ContractDetails']['ContractLineItemDetails']) - 1; $i++) {
            $igc_billing_currency = $this->request_data['ContractDetails']['ContractLineItemDetails'][$i]['ContractLineItemHeader']['igc_billing_currency'];
            if (!empty($igc_billing_currency)) {
                if ($i == 0) {
                    $prev_igc_billing_currency = $igc_billing_currency;
                } else {
                    if ($prev_igc_billing_currency != $igc_billing_currency) {
                        $osd_failed                  = true;
                        $igc_billing_currency_xml_id = $this->request_data['ContractDetails']['ContractLineItemDetails'][$i]['ContractLineItemHeader']['igc_billing_currency_xml_id'];
                        break;
                    }
                }
            }
        }
        if ($osd_failed) {
            $xpath_ids = array(
                'igc_billing_currency_id' => $igc_billing_currency_xml_id,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }
    /**
     * One stop dependency (osd) check for Customer Billing Method
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidOsdCustomerBillingMethod()
    {
        $osd_failed                   = false;
        $prev_customer_billing_method = '';
        $line_items                   = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        for ($i = 0; $i <= count($this->request_data['ContractDetails']['ContractLineItemDetails']) - 1; $i++) {
            $customer_billing_method = $this->request_data['ContractDetails']['ContractLineItemDetails'][$i]['ContractLineItemHeader']['customer_billing_method'];
            if (!empty($customer_billing_method)) {
                if ($i == 0) {
                    $prev_customer_billing_method = $customer_billing_method;
                } else {
                    if ($prev_customer_billing_method != $customer_billing_method) {
                        $osd_failed                     = true;
                        $customer_billing_method_xml_id = $this->request_data['ContractDetails']['ContractLineItemDetails'][$i]['ContractLineItemHeader']['customer_billing_method_xml_id'];
                        break;
                    }
                }
            }
        }
        if ($osd_failed) {
            $xpath_ids = array(
                'customer_billing_method_id' => $customer_billing_method_xml_id,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }
    /**
     * One stop dependency (osd) check for IGC Settlement Method
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidOsdIgcSettlementMethod()
    {
        $osd_failed                 = false;
        $prev_igc_settlement_method = '';
        $line_items                 = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        for ($i = 0; $i <= count($this->request_data['ContractDetails']['ContractLineItemDetails']) - 1; $i++) {
            $igc_settlement_method = $this->request_data['ContractDetails']['ContractLineItemDetails'][$i]['ContractLineItemHeader']['igc_settlement_method'];
            if (!empty($igc_settlement_method)) {
                if ($i == 0) {
                    $prev_igc_settlement_method = $igc_settlement_method;
                } else {
                    if ($prev_igc_settlement_method != $igc_settlement_method) {
                        $osd_failed                   = true;
                        $igc_settlement_method_xml_id = $this->request_data['ContractDetails']['ContractLineItemDetails'][$i]['ContractLineItemHeader']['igc_settlement_method_xml_id'];
                        break;
                    }
                }
            }
        }
        if ($osd_failed) {
            $xpath_ids = array(
                'igc_settlement_method_id' => $igc_settlement_method_xml_id,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * contact person name is mandatory for contract customer
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContactPersonNameContractCustomer()
    {
        $this->corp_acc_err_arr = $this->validatePatchAccountCorporateDetails($this->request_data, $this->contract_data['header']);
        if (isset($this->corp_acc_err_arr['Contract']['is_contact_person_name_empty']) && $this->corp_acc_err_arr['Contract']['is_contact_person_name_empty']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * contact person name is mandatory for billing customer
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContactPersonNameBillingCustomer()
    {
        if (isset($this->corp_acc_err_arr['Billing']['is_contact_person_name_empty']) && $this->corp_acc_err_arr['Billing']['is_contact_person_name_empty']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * contact person name is mandatory for customer technical representative
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContactPersonNameTechRep()
    {
        if (isset($this->corp_acc_err_arr['Technology']['is_contact_person_name_empty']) && $this->corp_acc_err_arr['Technology']['is_contact_person_name_empty']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * if no contracting account is there in the db and corporate details is missing the xml then throw this error
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryContractCorporationXml()
    {
        if (isset($this->corp_acc_err_arr['Contract']['is_corporate_details_xml_missing']) && $this->corp_acc_err_arr['Contract']['is_corporate_details_xml_missing']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * if no billing account is there in the db and corporate details is missing the xml then throw this error
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryBillingCorporationXml()
    {
        if (isset($this->corp_acc_err_arr['Billing']['is_corporate_details_xml_missing']) && $this->corp_acc_err_arr['Billing']['is_corporate_details_xml_missing']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }
    /**
     * if no technical account is there in the db and corporate details is missing the xml then throw this error
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryTechnicalCorporationXml()
    {
        if (isset($this->corp_acc_err_arr['Technology']['is_corporate_details_xml_missing']) && $this->corp_acc_err_arr['Technology']['is_corporate_details_xml_missing']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * corporation name (either Local or Latin) is mandatory for Contract Customer.
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryCorporationNameContractCustomer()
    {
        if (isset($this->corp_acc_err_arr['Contract']['is_corporate_name_empty']) && $this->corp_acc_err_arr['Contract']['is_corporate_name_empty']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * corporation name (either Local or Latin) is mandatory for Billing Customer.
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryCorporationNameBillingCustomer()
    {
        if (isset($this->corp_acc_err_arr['Billing']['is_corporate_name_empty']) && $this->corp_acc_err_arr['Billing']['is_corporate_name_empty']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * corporation name (either Local or Latin) is mandatory for Technical Representative.
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessMandatoryCorporationNameTechRep()
    {
        if (isset($this->corp_acc_err_arr['Technology']['is_corporate_name_empty']) && $this->corp_acc_err_arr['Technology']['is_corporate_name_empty']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * invalid country code for Contract Account
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidContractAccountCountryCode()
    {
        if (isset($this->corp_acc_err_arr['Contract']['country_not_found']['is_invalid']) && $this->corp_acc_err_arr['Contract']['country_not_found']['is_invalid']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                'country_id_xml'              => $this->corp_acc_err_arr['Contract']['country_not_found']['country_code_numeric'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * invalid country code for Billing Account
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidBillingAccountCountryCode()
    {
        if (isset($this->corp_acc_err_arr['Billing']['country_not_found']['is_invalid']) && $this->corp_acc_err_arr['Billing']['country_not_found']['is_invalid']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                'country_id_xml'              => $this->corp_acc_err_arr['Billing']['country_not_found']['country_code_numeric'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * invalid country code for Technical Account
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidTechnicalAccountCountryCode()
    {
        if (isset($this->corp_acc_err_arr['Technology']['country_not_found']['is_invalid']) && $this->corp_acc_err_arr['Technology']['country_not_found']['is_invalid']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                'country_id_xml'              => $this->corp_acc_err_arr['Technology']['country_not_found']['country_code_numeric'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * country code and address block mismatch valiadation for contract account
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidContractAccountCountryTagMismatch()
    {
        if ( !isset($this->corp_acc_err_arr['Contract']['country_not_found']['is_invalid']) && isset($this->corp_acc_err_arr['Contract']['country_tag_mismatch']['is_invalid']) && $this->corp_acc_err_arr['Contract']['country_tag_mismatch']['is_invalid']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                'country_id_xml'              => $this->corp_acc_err_arr['Contract']['country_tag_mismatch']['country_code_numeric'],
                'addr_tag_xpath'              => $this->corp_acc_err_arr['Contract']['country_tag_mismatch']['addr_tag_xpath'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * country code and address block mismatch valiadation for billing account
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidBillingAccountCountryTagMismatch()
    {
        if ( !isset($this->corp_acc_err_arr['Billing']['country_not_found']['is_invalid']) && isset($this->corp_acc_err_arr['Billing']['country_tag_mismatch']['is_invalid']) && $this->corp_acc_err_arr['Billing']['country_tag_mismatch']['is_invalid']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                'country_id_xml'              => $this->corp_acc_err_arr['Billing']['country_tag_mismatch']['country_code_numeric'],
                'addr_tag_xpath'              => $this->corp_acc_err_arr['Billing']['country_tag_mismatch']['addr_tag_xpath'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * country code and address block mismatch valiadation for techical representative account
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidTechnicalAccountCountryTagMismatch()
    {
        if ( !isset($this->corp_acc_err_arr['Technology']['country_not_found']['is_invalid']) && isset($this->corp_acc_err_arr['Technology']['country_tag_mismatch']['is_invalid']) && $this->corp_acc_err_arr['Technology']['country_tag_mismatch']['is_invalid']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                'country_id_xml'              => $this->corp_acc_err_arr['Technology']['country_tag_mismatch']['country_code_numeric'],
                'addr_tag_xpath'              => $this->corp_acc_err_arr['Technology']['country_tag_mismatch']['addr_tag_xpath'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * account country and state combination does not match for contract account
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidContractCountryStateMismatch()
    {
        if (isset($this->corp_acc_err_arr['Contract']['country_state_mismatch']['is_invalid']) && $this->corp_acc_err_arr['Contract']['country_state_mismatch']['is_invalid']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                'country_id_xml'              => $this->corp_acc_err_arr['Contract']['country_state_mismatch']['country_code_numeric'],
                'state_name_xml'              => $this->corp_acc_err_arr['Contract']['country_state_mismatch']['state_general_code'],
                'addr_tag_xpath'              => $this->corp_acc_err_arr['Contract']['country_state_mismatch']['addr_tag_xpath'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * account country and state combination does not match for billing account
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidBillingCountryStateMismatch()
    {
        if (isset($this->corp_acc_err_arr['Billing']['country_state_mismatch']['is_invalid']) && $this->corp_acc_err_arr['Billing']['country_state_mismatch']['is_invalid']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                'country_id_xml'              => $this->corp_acc_err_arr['Billing']['country_state_mismatch']['country_code_numeric'],
                'state_name_xml'              => $this->corp_acc_err_arr['Billing']['country_state_mismatch']['state_general_code'],
                'addr_tag_xpath'              => $this->corp_acc_err_arr['Billing']['country_state_mismatch']['addr_tag_xpath'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * account country and state combination does not match for technical account
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidTechnicalCountryStateMismatch()
    {
        if (isset($this->corp_acc_err_arr['Technology']['country_state_mismatch']['is_invalid']) && $this->corp_acc_err_arr['Technology']['country_state_mismatch']['is_invalid']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                'country_id_xml'              => $this->corp_acc_err_arr['Technology']['country_state_mismatch']['country_code_numeric'],
                'state_name_xml'              => $this->corp_acc_err_arr['Technology']['country_state_mismatch']['state_general_code'],
                'addr_tag_xpath'              => $this->corp_acc_err_arr['Technology']['country_state_mismatch']['addr_tag_xpath'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * invalid email for contract accounts
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidContractAccountEmail()
    {
        return $this->postBusinessInvalidContractAccountEmail();
    }

    /**
     * invalid email for billing accounts
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidBillingAccountEmail()
    {
        return $this->postBusinessInvalidBillingAccountEmail();
    }

    /**
     * invalid email for technical accounts
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidTechnologyAccountEmail()
    {
        return $this->postBusinessInvalidTechnologyAccountEmail();
    }

    /**
     * invalid email for sales accounts
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidSalesRepresentativeEmail()
    {
         return $this->postBusinessInvalidSalesRepresentativeEmail();
    }
    /**
     * sales company not exist in the system
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidSalesCompany()
    {
      return $this->postBusinessInvalidSalesCompany();
    }

    /**
     * Direct deposit email validation
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidLinePayeeEmail()
    {
      return $this->postBusinessInvalidLinePayeeEmail();
    }

    /**
     * null possible : contracting accounts corporate id field validation for PATCH
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function patchBusinessInvalidNullPossibleContractAccCorp(){
       require_once 'custom/service/v4_1_custom/APIHelper/v2/BusinessValidationConfig.php';
       global $patch_parse_xml_arr;
       $this->null_possible_err_list = $this->mod_utils->array_diff_multi($patch_parse_xml_arr['NullFields'], $validation_config['NullFields']);
       if(in_array('common_customer_id_c', $this->null_possible_err_list['ContractDetails']['Accounts']['Contract']['CorporateDetails'])){
           $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function patchBusinessInvalidContractEnddtTz()
    {
        $this->timezones = array();
        $contract_header = $this->request_data['ContractDetails']['ContractsHeader'];

        $timezone_bean = BeanFactory::getBean('gc_TimeZone');
        $timezone_list = $timezone_bean->get_full_list('', "c_country_id_c = ''");
        if (is_array($timezone_list)) {
            foreach ($timezone_list as $timezone) {
                $this->timezones[] = $timezone->name;
            }
        }
        return $this->postBusinessInvalidContractEnddtTz();
    }

    public function patchBusinessInvalidServiceStartDateTimezone()
    {
        return $this->postBusinessInvalidServiceStartDateTimezone();
    }

    public function patchBusinessInvalidServiceEndDateTimezone()
    {
        return $this->postBusinessInvalidServiceEndDateTimezone();
    }

    public function patchBusinessInvalidBillingStartDateTimezone()
    {
        return $this->postBusinessInvalidBillingStartDateTimezone();
    }

    public function patchBusinessInvalidBillingEndDateTimezone()
    {
        return $this->postBusinessInvalidBillingEndDateTimezone();
    }

    public function patchBusinessInvalidGlobalContractUuidMatch()
    {
        $has_error = false;

        if (isset($this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'])) {
            $global_contract_id_uuid = trim($this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id']);

            if ($global_contract_id_uuid != '' && $global_contract_id_uuid != trim($this->contract_data['header']->global_contract_id_uuid)) {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

	 /**
     * DF-2009 beluga validation : billing account is mandatory for activated contract.
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Feb 02, 2018
     */
    public function patchBusinessMandatoryBelugaBillingAccount()
    {
        $has_error = false;
        $this->billing_acc_activation_err_lists = array();
        $db_product_offering_id  = $this->contract_data['header']->product_offering;
        // validation if contract status is in Activated state
        if($this->contract_data['header']->contract_status == 4) {
            $this->billing_acc_activation_err_lists = ApiValidationHelper::validateXmlBillingAccountForBelugaProduct($this->request_data, $this->request_type, $db_product_offering_id);
        }
        if(!empty($this->billing_acc_activation_err_lists)){
            $first_line_item_err_details = current($this->billing_acc_activation_err_lists);
            if(empty($first_line_item_err_details)){
                $has_error = true;
            }
        }
        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * DF-2009 beluga validation : Payment Type is mandatory for activated contract.
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Feb 02, 2018
     */
    public function patchBusinessMandatoryBelugaPaymentType()
    {
        $has_error = false;
        if(!empty($this->billing_acc_activation_err_lists)){
            $first_line_item_err_details = current($this->billing_acc_activation_err_lists);
            if(!empty($first_line_item_err_details) && in_array('Payment Type',$first_line_item_err_details)){
                $has_error = true;
            }
        }
        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * DF-2009 beluga validation : Billing Start Date is mandatory for activated contract.
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Feb 05, 2018
     */
    public function patchBusinessMandatoryBelugaBillingStartDate()
    {
        $has_error = false;
        if(!empty($this->billing_acc_activation_err_lists)){
            foreach($this->billing_acc_activation_err_lists as $key => $value){
                if( !empty($value) && in_array('Billing Start Date',$value)){
                    $xpath_ids = array(
                        'line_item_xml_id' => $key,
                    );
                    $this->addBusinessValidationErrors($xpath_ids);
                    $has_error = true;
                }
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    /**
     * DF-2009 beluga validation : Billing End Date is mandatory for activated contract.
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Feb 05, 2018
     */
    public function patchBusinessMandatoryBelugaBillingEndDate()
    {
        $has_error = false;
        if(!empty($this->billing_acc_activation_err_lists)){
            foreach($this->billing_acc_activation_err_lists as $key => $value){
                if( !empty($value) && in_array('Billing End Date',$value)){
                    $xpath_ids = array(
                        'line_item_xml_id' => $key,
                    );
                    $this->addBusinessValidationErrors($xpath_ids);
                    $has_error = true;
                }
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }
}
