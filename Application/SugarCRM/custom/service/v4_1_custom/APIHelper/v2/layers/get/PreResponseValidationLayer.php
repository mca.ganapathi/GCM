<?php

trait GetPreResponseValidationLayer
{
	/**
     * validate response xml in GET request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 25, 2017
     */
    public function getPreResponseInvalidXmlData()
    {
        // check xml validation
        $has_error = ApiValidationHelper::xsdValidation($this->response_xml);
        if ($has_error) {
            $custom_error_details = [
                'details' => $has_error,
            ];
            // add error in error list
            $this->addPreResponseValidationErrors([], $custom_error_details);
            return false;
        }
        return true;
    } 
}