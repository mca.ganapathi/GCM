<?php

trait PostPreResponseValidationLayer
{
	/**
     * validate response xml in POST & Version Up request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 25, 2017
     */
    public function postPreResponseInvalidXmlData()
    {
        // check xml validation
        $has_error = ApiValidationHelper::xsdValidation($this->request_data);
        if ($has_error) {
            $custom_error_details = [
                'details' => $has_error,
            ];
            // add error in error list
            $this->addPreResponseValidationErrors([], $custom_error_details);
            return false;
        }
        return true;
    }
}