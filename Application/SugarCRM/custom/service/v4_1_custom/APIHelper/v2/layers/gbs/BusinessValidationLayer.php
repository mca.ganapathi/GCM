<?php

trait GbsBusinessValidationLayer
{
    /**
     * validate gbs product offerings list is present in global variable or not
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Sept 05, 2017
     */
    public function gbsBusinessMandatoryGbsApiPo()
    {
        global $sugar_config;
        $has_error = false;

        if (!isset($sugar_config['gbs_api_po']) || empty($sugar_config['gbs_api_po'])) {
            $has_error = true;
        } else {
            $gbs_api_po = array_filter($sugar_config['gbs_api_po']);
            if (empty($gbs_api_po)) {
                $has_error = false;
            }
        }

        if ($has_error) {
            $xpath_ids = array();
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * gbs required field account-type validation
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Sept 05, 2017
     */
    public function gbsBusinessMandatoryAccountType()
    {
        $has_error = false;
        if ($this->accountType == '') {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array();
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * gbs required field date-after validation
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Sept 05, 2017
     */
    public function gbsBusinessMandatoryDateAfter()
    {
        $has_error            = false;
        $this->gbs_valid_date = true;
        if (trim($this->dateAfter) == '') {
            $has_error = true;
        }

        if ($has_error) {
            $this->gbs_valid_date = false;
            $xpath_ids            = array();
            return $this->addBusinessValidationErrors($xpath_ids);
        }
        return true;
    }

    /**
     * gbs required field date-before validation
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Sept 05, 2017
     */
    public function gbsBusinessMandatoryDateBefore()
    {
        $has_error = false;
        if (trim($this->dateBefore) == '') {
            $has_error = true;
        }

        if ($has_error) {
            $this->gbs_valid_date = false;
            $xpath_ids            = array();
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * gbs account type field validation
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Sept 05, 2017
     */
    public function gbsBusinessInvalidAccountTypeValid()
    {
        $has_error = false;
        if (!empty($this->accountType) && !in_array($this->accountType, array('billing-account', 'contract-account'))) {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array('account_type' => $this->accountType);
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * gbs field date-after format validation
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Sept 05, 2017
     */
    public function gbsBusinessInvalidDateAfterFormat()
    {
        $has_error = false;
        if (trim($this->dateAfter) == '') {
            return true;
        }

        if (!$this->mod_utils->validateDateFormat($this->dateAfter)) {
            $has_error = true;
        }

        if ($has_error) {
            $this->gbs_valid_date = false;
            $xpath_ids            = array('date_after' => $this->dateAfter);
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * gbs field date-before format validation
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Sept 05, 2017
     */
    public function gbsBusinessInvalidDateBeforeFormat()
    {
        $has_error = false;

        if (trim($this->dateBefore) == '') {
            return true;
        }

        if (!$this->mod_utils->validateDateFormat($this->dateBefore)) {
            $has_error = true;
        }

        if ($has_error) {
            $this->gbs_valid_date = false;
            $xpath_ids            = array('date_before' => $this->dateBefore);
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * gbs fields date-after and date-before comparison
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Sept 05, 2017
     */
    public function gbsBusinessInvalidDateBeforeAfterCompare()
    {
        $has_error = false;

        if ($this->gbs_valid_date == true && (strtotime($this->dateAfter) > strtotime($this->dateBefore))) {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = ['date_before' => $this->dateBefore, 'date_after' => $this->dateAfter];
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }
}
