<?php

trait PostBusinessValidationLayer
{

    private $belunga_contract_activation_err;
	/**
     * validate global contract id for POST request
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Oct 06, 2017
     */
    public function postBusinessInvalidGlobalContractIdUrl()
    {
        $has_error = false;

        if (!$this->request_data['ContractDetails']['ContractsHeader']['is_new_record'] && ($this->global_contract_id == '' && $this->global_contract_uuid == '')) {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array();
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractName()
    {
        $has_error = false;

        if (isset($this->request_data['ContractDetails']['ContractsHeader']['name']) && trim($this->request_data['ContractDetails']['ContractsHeader']['name']) == '') {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }

        return true;
    }

    public function postBusinessMandatoryProductOfferingId()
    {
        $has_error = false;

        if (trim($this->request_data['ContractDetails']['ContractsHeader']['product_offering']) == '') {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array();
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryLineItemDetails()
    {
        $has_error        = false;
        $this->line_index = 0;
        if (!array_filter($this->request_data['ContractDetails']['ContractLineItemDetails'])) {
            $has_error = true;
        } else {
            $this->line_index = key($this->request_data['ContractDetails']['ContractLineItemDetails']);
        }

        if ($has_error) {
            $xpath_ids = array();
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryProdCharacteristicDtls()
    {
        $has_error = false;

        $contract_header = $this->request_data['ContractDetails']['ContractsHeader'];

        if (empty($contract_header['surrogate_teams']) && trim($contract_header['contract_scheme']) == '' &&
            trim($contract_header['loi_contract']) == '' && trim($contract_header['contract_type']) == '' &&
            trim($contract_header['billing_type']) == '' && trim($contract_header['factory_reference_no']) == '') {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessInvalidBillingType()
    {
        $has_error = false;
        $contract_header = $this->request_data['ContractDetails']['ContractsHeader'];

        if (isset($contract_header['billing_type']) && $contract_header['billing_type'] != '' && $contract_header['billing_type'] != 'OneStop') {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array(
                'billing_type'        => $this->request_data['ContractDetails']['ContractsHeader']['billing_type'],
                'billing_type_xml_id' => $this->request_data['ContractDetails']['ContractsHeader']['billing_type_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryClientReqId()
    {
        $has_error = false;

        if (!$this->version_number && trim($this->request_data['ContractDetails']['ContractsHeader']['contract_request_id']) == '') {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessInvalidClientReqId()
    {
        $has_error = false;

        $client_req_id = (!empty($this->request_data['ContractDetails']['ContractsHeader']['contract_request_id'])) ? $this->request_data['ContractDetails']['ContractsHeader']['contract_request_id'] : '';

        if (empty($client_req_id)) {
            return true;
        }

        $obj_contracts_bean = BeanFactory::getBean('gc_Contracts');
        $obj_contracts_bean->retrieve_by_string_fields(array(
            'contract_request_id' => $this->request_data['ContractDetails']['ContractsHeader']['contract_request_id'],
        ));

        if (!empty($obj_contracts_bean->id)) {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array(
                'contract_request_id'     => $this->request_data['ContractDetails']['ContractsHeader']['contract_request_id'],
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractsStartDate()
    {
        global $sugar_config;
        $has_error = false;

        $contract_header     = $this->request_data['ContractDetails']['ContractsHeader'];

        if(isset($contract_header['contract_startdate_invalid']) && $contract_header['contract_startdate_invalid'] != 0)
          return true;

        $contract_start_date = $contract_header['contract_startdate'];
        $product_offering    = trim($contract_header['product_offering']);

        if (isset($sugar_config['workflow_flag'][$product_offering]) && strtolower($sugar_config['workflow_flag'][$product_offering]) == 'yes') {
            if ($contract_header['is_aggreement_period_block'] && empty($contract_start_date)) {
                $has_error = true;
            }
        } else {
            if (!$contract_header['is_aggreement_period_block'] || empty($contract_start_date)) {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }

        return true;
    }

    public function postBusinessInvalidContractsDatesCompare()
    {
        $has_error = false;

        $contract_header     = $this->request_data['ContractDetails']['ContractsHeader'];
        $contract_start_date = $contract_header['contract_startdate'];
        $contract_end_date   = $contract_header['contract_enddate'];

        $contract_start_date_xml_local = ApiValidationHelper::convertGMTDatetoLocalDataDate($contract_header['contract_startdate'],$contract_header['contract_startdate_timezone']);
        $contract_end_date_xml_local = ApiValidationHelper::convertGMTDatetoLocalDataDate($contract_header['contract_enddate'],$contract_header['contract_enddate_timezone']);

        if (!empty($contract_start_date) && !empty($contract_end_date) && (strtotime($contract_start_date) > strtotime($contract_end_date))) {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid'     => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                'contract_startdate'          => str_replace(' ', 'T', $contract_start_date_xml_local),
                'contract_enddate'            => str_replace(' ', 'T', $contract_end_date_xml_local),
                'contract_startdate_timezone' => $this->request_data['ContractDetails']['ContractsHeader']['contract_startdate_timezone'],
                'contract_enddate_timezone'   => $this->request_data['ContractDetails']['ContractsHeader']['contract_enddate_timezone'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessInvalidSalesCompany()
    {
        if (isset($this->request_data['ContractDetails']['ContractSalesRepresentative'][0]['sales_company']) && !empty($this->request_data['ContractDetails']['ContractSalesRepresentative'][0]['sales_company'])) {
            $sales_company_name_xml = $this->request_data['ContractDetails']['ContractSalesRepresentative'][0]['sales_company'];
            $is_sales_company_exist = $this->isValidSalesCompany($sales_company_name_xml);
            if (!$is_sales_company_exist) {
                $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
                $xpath_ids                   = array(
                    'global_contract_id_uuid' => $global_contract_id_uuid_xml,
                    'sales_company'      => $sales_company_name_xml,
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }

    public function postBusinessInvalidContractStartdtTz()
    {
        $has_error       = false;
        $this->timezones = array();
        $contract_header = $this->request_data['ContractDetails']['ContractsHeader'];

        $timezone_bean = BeanFactory::getBean('gc_TimeZone');
        $timezone_list = $timezone_bean->get_full_list('', "c_country_id_c = ''");
        if (is_array($timezone_list)) {
            foreach ($timezone_list as $timezone) {
                $this->timezones[] = $timezone->name;
            }
        }

        if (trim($contract_header['contract_startdate_timezone']) != '' && !in_array($contract_header['contract_startdate_timezone'], $this->timezones)) {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array(
                'contract_startdate'          => $this->request_data['ContractDetails']['ContractsHeader']['contract_startdate'],
                'contract_startdate_timezone' => $this->request_data['ContractDetails']['ContractsHeader']['contract_startdate_timezone'],
                'global_contract_id_uuid'     => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessInvalidContractEnddtTz()
    {
        $has_error = false;

        $contract_header = $this->request_data['ContractDetails']['ContractsHeader'];

        if (trim($contract_header['contract_enddate_timezone']) != '' && !in_array($contract_header['contract_enddate_timezone'], $this->timezones)) {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array(
                'contract_enddate'          => $this->request_data['ContractDetails']['ContractsHeader']['contract_enddate'],
                'contract_enddate_timezone' => $this->request_data['ContractDetails']['ContractsHeader']['contract_enddate_timezone'],
                'global_contract_id_uuid'   => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessInvalidLinePayeeEmail()
    {
              if (isset($this->request_data['ContractDetails']['ContractLineItemDetails'][0]['ContractDirectDeposit']['payee_email_address']) && $this->request_data['ContractDetails']['ContractLineItemDetails'][0]['ContractDirectDeposit']['payee_email_address'] != '' && !$this->mod_utils->email_validation($this->request_data['ContractDetails']['ContractLineItemDetails'][0]['ContractDirectDeposit']['payee_email_address'])) {
                  $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
                  $xpath_ids = array(
                          'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                          'payee_email_address'     => $this->request_data['ContractDetails']['ContractLineItemDetails'][0]['ContractDirectDeposit']['payee_email_address'],
                      );
                  $this->addBusinessValidationErrors($xpath_ids);
                  return false;
              }
              return true;
    }

    public function postBusinessMandatoryLineItemDetailsIndex()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];

        foreach ($line_item_details as $i => $line_item) {
            if (trim($line_item['ContractLineItemHeader']['product_specification']) == '') {
                $has_error = true;
                $xpath_ids = array(
                    'line_item_id_xml_id' => $line_item['ContractLineItemHeader']['line_item_id_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }

        if ($has_error) {
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryProductSpecPmsId()
    {
        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];

        foreach ($line_item_details as $i => $line_item) {
            $line_item_group  = $line_item['InvoiceGroupDtls'];
            $line_item_header = $line_item['ContractLineItemHeader'];
            if (trim($line_item_header['service_start_date']) == '' && trim($line_item_header['service_end_date']) == '' &&
                trim($line_item_header['billing_start_date']) == '' && trim($line_item_header['billing_end_date']) == '' &&
                trim($line_item_header['cust_contract_currency']) == '' && trim($line_item_header['cust_billing_currency']) == '' &&
                trim($line_item_header['igc_contract_currency']) == '' && trim($line_item_header['igc_billing_currency']) == '' &&
                trim($line_item_header['customer_billing_method']) == '' && trim($line_item_header['igc_settlement_method']) == '' &&
                trim($line_item_header['description']) == '' && trim($line_item_header['factory_reference_no']) == '' &&
                trim($line_item_group['name']) == '' && trim($line_item_group['description']) == '') {
                $has_error = true;
                $xpath_ids = array(
                    'line_item_id_xml_id' => $line_item_header['line_item_id_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }

        return true;
    }

    public function postBusinessInvalidLineServiceDatesCompare()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $i => $line_item) {
            $service_start_date = $service_end_date = '';
            $line_item_header = $line_item['ContractLineItemHeader'];
            if (!empty($line_item_header['service_start_date']) && !empty($line_item_header['service_end_date']) && (strtotime($line_item_header['service_start_date']) > strtotime($line_item_header['service_end_date']))) {
                $has_error = true;
                $service_start_date = ApiValidationHelper::convertGMTDatetoLocalDataDate($line_item_header['service_start_date'], $line_item_header['service_start_date_timezone']);
                $service_end_date = ApiValidationHelper::convertGMTDatetoLocalDataDate($line_item_header['service_end_date'], $line_item_header['service_end_date_timezone']);
                $xpath_ids = array(
                    'service_start_date'          => str_replace(' ', 'T', $service_start_date),
                    'service_start_date_timezone' => $line_item_header['service_start_date_timezone'],
                    'service_start_date_xml_id'   => $line_item_header['service_start_date_xml_id'],
                    'service_end_date'            => str_replace(' ', 'T', $service_end_date),
                    'service_end_date_timezone'   => $line_item_header['service_end_date_timezone'],
                    'service_end_date_xml_id'     => $line_item_header['service_end_date_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }

        if ($has_error) {
            return false;
        }
        return true;
    }

    public function postBusinessInvalidLineBillingDatesCompare()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $i => $line_item) {
            $billing_start_date = $billing_end_date = '';
            $line_item_header = $line_item['ContractLineItemHeader'];
            if (!empty($line_item_header['billing_start_date']) && !empty($line_item_header['billing_end_date']) && (strtotime($line_item_header['billing_start_date']) > strtotime($line_item_header['billing_end_date']))) {
                $has_error = true;
                $billing_start_date = ApiValidationHelper::convertGMTDatetoLocalDataDate($line_item_header['billing_start_date'], $line_item_header['billing_start_date_timezone']);
                $billing_end_date = ApiValidationHelper::convertGMTDatetoLocalDataDate($line_item_header['billing_end_date'], $line_item_header['billing_end_date_timezone']);
                $xpath_ids = array(
                    'billing_start_date'          => str_replace(' ', 'T', $billing_start_date),
                    'billing_start_date_timezone' => $line_item_header['billing_start_date_timezone'],
                    'billing_start_date_xml_id'   => $line_item_header['billing_start_date_xml_id'],
                    'billing_end_date'            => str_replace(' ', 'T', $billing_end_date),
                    'billing_end_date_timezone'   => $line_item_header['billing_end_date_timezone'],
                    'billing_end_date_xml_id'     => $line_item_header['billing_end_date_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }

        if ($has_error) {
            return false;
        }
        return true;
    }

    public function postBusinessInvalidServiceStartDateTimezone()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];

        foreach ($line_item_details as $i => $line_item) {
            $line_item_header = $line_item['ContractLineItemHeader'];
            if (trim($line_item_header['service_start_date_timezone']) != '' && trim($line_item_header['service_start_date_timezone']) != '00:00' && !in_array($line_item_header['service_start_date_timezone'], $this->timezones)) {
                $has_error = true;
                $xpath_ids = array(
                    'service_start_date'          => $line_item_header['service_start_date'],
                    'service_start_date_timezone' => $line_item_header['service_start_date_timezone'],
                    'service_start_date_xml_id'   => $line_item_header['service_start_date_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function postBusinessInvalidServiceEndDateTimezone()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];

        foreach ($line_item_details as $i => $line_item) {
            $line_item_header = $line_item['ContractLineItemHeader'];
            if (trim($line_item_header['service_end_date_timezone']) != '' && trim($line_item_header['service_end_date_timezone']) != '00:00' && !in_array($line_item_header['service_end_date_timezone'], $this->timezones)) {
                $has_error = true;
                $xpath_ids = array(
                    'service_end_date'          => $line_item_header['service_end_date'],
                    'service_end_date_timezone' => $line_item_header['service_end_date_timezone'],
                    'service_end_date_xml_id'   => $line_item_header['service_end_date_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function postBusinessInvalidBillingStartDateTimezone()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];

        foreach ($line_item_details as $i => $line_item) {
            $line_item_header = $line_item['ContractLineItemHeader'];

            if (trim($line_item_header['billing_start_date_timezone']) != '' && trim($line_item_header['billing_start_date_timezone']) != '00:00' && !in_array($line_item_header['billing_start_date_timezone'], $this->timezones)) {
                $has_error = true;
                $xpath_ids = array(
                    'billing_start_date'          => $line_item_header['billing_start_date'],
                    'billing_start_date_timezone' => $line_item_header['billing_start_date_timezone'],
                    'billing_start_date_xml_id'   => $line_item_header['billing_start_date_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }

        if ($has_error) {
            return false;
        }
        return true;
    }

    public function postBusinessInvalidBillingEndDateTimezone()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];

        foreach ($line_item_details as $i => $line_item) {
            $line_item_header = $line_item['ContractLineItemHeader'];
            if (trim($line_item_header['billing_end_date_timezone']) != '' && trim($line_item_header['billing_end_date_timezone']) != '00:00' && !in_array($line_item_header['billing_end_date_timezone'], $this->timezones)) {
                $has_error = true;
                $xpath_ids = array(
                    'billing_end_date'          => $line_item_header['billing_end_date'],
                    'billing_end_date_timezone' => $line_item_header['billing_end_date_timezone'],
                    'billing_end_date_xml_id'   => $line_item_header['billing_end_date_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }

        if ($has_error) {
            return false;
        }
        return true;
    }

    public function postBusinessInvalidContractSurrogateTeams()
    {
        $has_error = false;

        $contract_header = $this->request_data['ContractDetails']['ContractsHeader'];
        $teamname        = '';

        if (!empty($contract_header['surrogate_teams'])) {
            foreach ($contract_header['surrogate_teams'] as $teamname) {
                $teamid = $this->mod_utils->retrieveTeamId($teamname);
                if ($teamid === false) {
                    $has_error = true;
                    $xpath_ids = array(
                        'surrogate_team_name'    => $teamname,
                        'surrogate_teams_xml_id' => $contract_header['surrogate_teams_xml_id'][0],
                    );
                    $this->addBusinessValidationErrors($xpath_ids);
                }
            }
        }

        if ($has_error) {
            return false;
        }
        return true;
    }

    public function postBusinessInvalidSalesRepresentativeEmail()
    {
        global $sugar_config;
        $has_error = false;

        if (isset($this->request_data['ContractDetails']['ContractSalesRepresentative']) && !empty($this->request_data['ContractDetails']['ContractSalesRepresentative'])) {
            foreach ($this->request_data['ContractDetails']['ContractSalesRepresentative'] as $ContractSalesRepresentative) {
                if (isset($ContractSalesRepresentative['email1']) && $ContractSalesRepresentative['email1'] != '' && !$this->mod_utils->email_validation($ContractSalesRepresentative['email1'])) {
                    $has_error = true;
                }
            }
            if ($has_error) {
                $xpath_ids = array(
                    'email1'                  => $this->request_data['ContractDetails']['ContractSalesRepresentative'][0]['email1'],
                    'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }

    public function postBusinessMandatoryContractingAccountName()
    {
        $has_error = false;

        if (isset($this->request_data['Accounts']['Contract'])) {
            if (trim($this->request_data['Accounts']['Contract']['last_name']) == '' && trim($this->request_data['Accounts']['Contract']['name_2_c']) == '') {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryBillingAccountName()
    {
        $has_error = false;

        if (isset($this->request_data['Accounts']['Billing'])) {
            if (trim($this->request_data['Accounts']['Billing']['last_name']) == '' && trim($this->request_data['Accounts']['Billing']['name_2_c']) == '') {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryTechnologyAccountName()
    {
        $has_error = false;

        if (isset($this->request_data['Accounts']['Technology'])) {
            if (trim($this->request_data['Accounts']['Technology']['last_name']) == '' && trim($this->request_data['Accounts']['Technology']['name_2_c']) == '') {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractingCompanyDtls()
    {
        $has_error = false;
        if (isset($this->request_data['Accounts']['Contract']) && (!array_filter($this->request_data['Accounts']['Contract']['CorporateDetails'])  || $this->request_data['Accounts']['Contract']['CorporateDetails']['is_block_exist'] != 'Yes')) {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryBillingCompanyDtls()
    {
        $has_error = false;
        if (isset($this->request_data['Accounts']['Billing']) && isset($this->request_data['Accounts']['Billing']['CorporateDetails'])) {
            $corporate_details = $this->request_data['Accounts']['Billing']['CorporateDetails'];
            if (trim($corporate_details['common_customer_id_c']) == '' && trim($corporate_details['name']) == '' && trim($corporate_details['name_2_c']) == '') {
                $has_error = true;
            }
        }

        if ($has_error) {

            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryTechnologyCompanyDtls()
    {
        $has_error = false;
        if (isset($this->request_data['Accounts']['Technology']) && isset($this->request_data['Accounts']['Technology']['CorporateDetails'])) {
            $corporate_details = $this->request_data['Accounts']['Technology']['CorporateDetails'];
            if (trim($corporate_details['common_customer_id_c']) == '' && trim($corporate_details['name']) == '' && trim($corporate_details['name_2_c']) == '') {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractingCorporateNameId()
    {
        $has_error = false;
        if (isset($this->request_data['Accounts']['Contract']) && isset($this->request_data['Accounts']['Contract']['CorporateDetails'])) {
            $corporate_details = $this->request_data['Accounts']['Contract']['CorporateDetails'];
            if ($corporate_details['is_block_exist'] == 'Yes' && trim($corporate_details['common_customer_id_c']) == '') {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /*
    * @deprecated function
    */
    public function postBusinessMandatoryBillingCorporateNameId()
    {
        return true;
    }

    /*
    * @deprecated function
    */
    public function postBusinessMandatoryTechnologyCorporateNameId()
    {
        return true;
    }

    public function postBusinessInvalidContractingAccountCountryCode()
    {
        $has_error           = false;
        $this->country_codes = array();

        $country_bean = BeanFactory::getBean('c_country');
        $country_list = $country_bean->get_full_list('', '');
        if (is_array($country_list)) {
            foreach ($country_list as $country) {
                $this->country_codes[] = $country->country_code_numeric;
            }
        }

        if (isset($this->request_data['Accounts']['Contract'])) {
            $country_code_numeric = isset($this->request_data['Accounts']['Contract']['country_code_numeric']) ? $this->request_data['Accounts']['Contract']['country_code_numeric'] : '';

            if (trim($country_code_numeric) != '') {
                if (!in_array($country_code_numeric, $this->country_codes)) {
                    $has_error = true;
                }
            }
            if ($has_error) {
                $xpath_ids = array(
                    'country_code_numeric'    => $country_code_numeric,
                    'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }
    public function postBusinessInvalidBillingAccountCountryCode()
    {
        $has_error = false;

        if (isset($this->request_data['Accounts']['Billing'])) {
            $country_code_numeric = isset($this->request_data['Accounts']['Billing']['country_code_numeric']) ? $this->request_data['Accounts']['Billing']['country_code_numeric'] : '';

            if (trim($country_code_numeric) != '') {
                if (!in_array($country_code_numeric, $this->country_codes)) {
                    $has_error = true;
                }
            }
            if ($has_error) {
                $xpath_ids = array(
                    'country_code_numeric'    => $country_code_numeric,
                    'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }
    public function postBusinessInvalidTechnologyAccountCountryCode()
    {
        $has_error = false;

        if (isset($this->request_data['Accounts']['Technology'])) {
            $country_code_numeric = isset($this->request_data['Accounts']['Technology']['country_code_numeric']) ? $this->request_data['Accounts']['Technology']['country_code_numeric'] : '';

            if (trim($country_code_numeric) != '') {
                if (!in_array($country_code_numeric, $this->country_codes)) {
                    $has_error = true;
                }
            }
            if ($has_error) {
                $xpath_ids = array(
                    'country_code_numeric'    => $country_code_numeric,
                    'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }

    public function postBusinessInvalidContractAccountCountryStateCompare()
    {
        $has_error = false;

        if (isset($this->request_data['Accounts']['Contract'])) {
            $account_dtls         = $this->request_data['Accounts']['Contract'];
            $country_code_numeric = isset($account_dtls['country_code_numeric']) ? $account_dtls['country_code_numeric'] : '';

            if (trim($country_code_numeric) != '' && $account_dtls['address_tag_name'] != 'japanese') {
                if ($account_dtls['general_address']['state_general_code'] != '' && !ApiValidationHelper::checkCountryStateValidation($account_dtls['country_code_numeric'], $account_dtls['general_address']['state_general_code'])) {
                    $has_error = true;
                }
            }

            if ($has_error) {
                $xpath_ids = array(
                    'country_code_numeric'    => $country_code_numeric,
                    'state_general_code'      => $account_dtls['general_address']['state_general_code'],
                    'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                    'addr_tag_xpath' => $account_dtls['address_tag_name']
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }
    public function postBusinessInvalidBillingAccountCountryStateCompare()
    {
        $has_error = false;

        if (isset($this->request_data['Accounts']['Billing'])) {
            $account_dtls         = $this->request_data['Accounts']['Billing'];
            $country_code_numeric = isset($account_dtls['country_code_numeric']) ? $account_dtls['country_code_numeric'] : '';

            if (trim($country_code_numeric) != '' && $account_dtls['address_tag_name'] != 'japanese') {
                if ($account_dtls['general_address']['state_general_code'] != '' && !ApiValidationHelper::checkCountryStateValidation($account_dtls['country_code_numeric'], $account_dtls['general_address']['state_general_code'])) {
                    $has_error = true;
                }
            }

            if ($has_error) {
                $xpath_ids = array(
                    'country_code_numeric'    => $country_code_numeric,
                    'state_general_code'      => $account_dtls['general_address']['state_general_code'],
                    'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                    'addr_tag_xpath' => $account_dtls['address_tag_name']
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }
    public function postBusinessInvalidTechnologyAccountCountryStateCompare()
    {
        $has_error = false;

        if (isset($this->request_data['Accounts']['Technology'])) {
            $account_dtls         = $this->request_data['Accounts']['Technology'];
            $country_code_numeric = isset($account_dtls['country_code_numeric']) ? $account_dtls['country_code_numeric'] : '';

            if (trim($country_code_numeric) != '' && $account_dtls['address_tag_name'] != 'japanese') {
                if ($account_dtls['general_address']['state_general_code'] != '' && !ApiValidationHelper::checkCountryStateValidation($account_dtls['country_code_numeric'], $account_dtls['general_address']['state_general_code'])) {
                    $has_error = true;
                }
            }

            if ($has_error) {
                $xpath_ids = array(
                    'country_code_numeric'    => $country_code_numeric,
                    'state_general_code'      => $account_dtls['general_address']['state_general_code'],
                    'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                    'addr_tag_xpath' => $account_dtls['address_tag_name']
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }

    public function postBusinessInvalidContractAccountAddressTag()
    {
        $has_error = false;

        if (isset($this->request_data['Accounts']['Contract'])) {
            $account_dtls         = $this->request_data['Accounts']['Contract'];
            $country_code_numeric = isset($account_dtls['country_code_numeric']) ? $account_dtls['country_code_numeric'] : '';

            if (!in_array($country_code_numeric, $this->country_codes)) {
                return true;
            }

            $expected_addr_tag_name = '';
            if (isset($account_dtls['address_tag_name']) && trim($account_dtls['address_tag_name']) != '') {
                switch ($country_code_numeric) {
                    case '840':
                        $expected_addr_tag_name = 'american';
                        break;
                    case '124':
                        $expected_addr_tag_name = 'canadian';
                        break;
                    case '392':
                        $expected_addr_tag_name = 'japanese';
                        break;
                    default:
                        $expected_addr_tag_name = 'any';
                        break;
                }
                if (trim(strtolower($account_dtls['address_tag_name'])) != $expected_addr_tag_name) {
                    $has_error = true;
                }
            }

            if ($has_error) {
                $xpath_ids = array(
                    'country_code_numeric'    => $country_code_numeric,
                    'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                    'country_tag_name' => $account_dtls['address_tag_name']
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }

    public function postBusinessInvalidBillingAccountAddressTag()
    {
        $has_error = false;

        if (isset($this->request_data['Accounts']['Billing'])) {
            $account_dtls         = $this->request_data['Accounts']['Billing'];
            $country_code_numeric = isset($account_dtls['country_code_numeric']) ? $account_dtls['country_code_numeric'] : '';

            if (!in_array($country_code_numeric, $this->country_codes)) {
                return true;
            }

            $expected_addr_tag_name = '';
            if (isset($account_dtls['address_tag_name']) && trim($account_dtls['address_tag_name']) != '') {
                switch ($country_code_numeric) {
                    case '840':
                        $expected_addr_tag_name = 'american';
                        break;
                    case '124':
                        $expected_addr_tag_name = 'canadian';
                        break;
                    case '392':
                        $expected_addr_tag_name = 'japanese';
                        break;
                    default:
                        $expected_addr_tag_name = 'any';
                        break;
                }
                if (trim(strtolower($account_dtls['address_tag_name'])) != $expected_addr_tag_name) {
                    $has_error = true;
                }
            }

            if ($has_error) {
                $xpath_ids = array(
                    'country_code_numeric'    => $country_code_numeric,
                    'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                    'country_tag_name' => $account_dtls['address_tag_name']
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }

    public function postBusinessInvalidTechnologyAccountAddressTag()
    {
        $has_error = false;

        if (isset($this->request_data['Accounts']['Technology'])) {
            $account_dtls         = $this->request_data['Accounts']['Technology'];
            $country_code_numeric = isset($account_dtls['country_code_numeric']) ? $account_dtls['country_code_numeric'] : '';

            if (!in_array($country_code_numeric, $this->country_codes)) {
                return true;
            }

            $expected_addr_tag_name = '';
            if (isset($account_dtls['address_tag_name']) && trim($account_dtls['address_tag_name']) != '') {
                switch ($country_code_numeric) {
                    case '840':
                        $expected_addr_tag_name = 'american';
                        break;
                    case '124':
                        $expected_addr_tag_name = 'canadian';
                        break;
                    case '392':
                        $expected_addr_tag_name = 'japanese';
                        break;
                    default:
                        $expected_addr_tag_name = 'any';
                        break;
                }
                if (trim(strtolower($account_dtls['address_tag_name'])) != $expected_addr_tag_name) {
                    $has_error = true;
                }
            }

            if ($has_error) {
                $xpath_ids = array(
                    'country_code_numeric'    => $country_code_numeric,
                    'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                    'country_tag_name' => $account_dtls['address_tag_name']
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }

    public function postBusinessInvalidContractAccountEmail()
    {
        global $sugar_config;
        $has_error = false;

        if (isset($this->request_data['Accounts']['Contract'])) {
            $account_dtls = $this->request_data['Accounts']['Contract'];

            if (isset($account_dtls['email1']) && $account_dtls['email1'] != '' && !$this->mod_utils->email_validation($account_dtls['email1'])) {
                $has_error = true;
            }

            if ($has_error) {
                $xpath_ids = array(
                    'email1'                  => $account_dtls['email1'],
                    'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }

    public function postBusinessInvalidBillingAccountEmail()
    {
        global $sugar_config;
        $has_error = false;

        if (isset($this->request_data['Accounts']['Billing'])) {
            $account_dtls = $this->request_data['Accounts']['Billing'];

            if (isset($account_dtls['email1']) && $account_dtls['email1'] != '' && !$this->mod_utils->email_validation($account_dtls['email1'])) {
                $has_error = true;
            }

            if ($has_error) {
                $xpath_ids = array(
                    'email1'                  => $account_dtls['email1'],
                    'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }

    public function postBusinessInvalidTechnologyAccountEmail()
    {
        global $sugar_config;
        $has_error = false;

        if (isset($this->request_data['Accounts']['Technology'])) {
            $account_dtls = $this->request_data['Accounts']['Technology'];

            if (isset($account_dtls['email1']) && $account_dtls['email1'] != '' && !$this->mod_utils->email_validation($account_dtls['email1'])) {
                $has_error = true;
            }

            if ($has_error) {
                $xpath_ids = array(
                    'email1'                  => $account_dtls['email1'],
                    'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
                return false;
            }
        }
        return true;
    }

    public function postBusinessInvalidBillingAffiliateOrgCode1()
    {
        if(isset($this->request_data['ContractDetails']['BillingAffiliate'][0]) && !empty($this->request_data['ContractDetails']['BillingAffiliate'][0])) {
            $this->billing_affiliate_err_dtls = $this->validateBillingAffiliate($this->request_data['ContractDetails']);

            $organization_code_1 = isset($this->request_data['ContractDetails']['BillingAffiliate'][0]['organization_code_1']) ? $this->request_data['ContractDetails']['BillingAffiliate'][0]['organization_code_1'] : '';

            if (!empty($organization_code_1)) {
                if (isset($this->billing_affiliate_err_dtls['invalid_code_1']) && $this->billing_affiliate_err_dtls['invalid_code_1']) {
                    $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
                    $xpath_ids = array(
                        'organization_code_1'     => $organization_code_1,
                        'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                    );
                    $this->addBusinessValidationErrors($xpath_ids);
                    return false;
                }
            }
          }
          return true;
    }

    public function postBusinessInvalidBillingAffiliateOrgCode2()
    {
      if(isset($this->request_data['ContractDetails']['BillingAffiliate'][0]) && !empty($this->request_data['ContractDetails']['BillingAffiliate'][0])) {

          $organization_code_2 = isset($this->request_data['ContractDetails']['BillingAffiliate'][0]['organization_code_2']) ? $this->request_data['ContractDetails']['BillingAffiliate'][0]['organization_code_2'] : '';

          if (!empty($organization_code_2)) {
              if (isset($this->billing_affiliate_err_dtls['invalid_code_2']) && $this->billing_affiliate_err_dtls['invalid_code_2']) {
                  $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
                  $xpath_ids = array(
                      'organization_code_2'     => $organization_code_2,
                      'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                  );
                  $this->addBusinessValidationErrors($xpath_ids);
                  return false;
              }
          }
        }
      return true;
    }

    public function postBusinessInvalidBillingAffiliateOrgCodeCompare()
    {
      if(isset($this->request_data['ContractDetails']['BillingAffiliate'][0]) && !empty($this->request_data['ContractDetails']['BillingAffiliate'][0])) {

          $organization_code_1 = isset($this->request_data['ContractDetails']['BillingAffiliate'][0]['organization_code_1']) ? $this->request_data['ContractDetails']['BillingAffiliate'][0]['organization_code_1'] : '';
          $organization_code_2 = isset($this->request_data['ContractDetails']['BillingAffiliate'][0]['organization_code_2']) ? $this->request_data['ContractDetails']['BillingAffiliate'][0]['organization_code_2'] : '';

          if (!empty($organization_code_1) && !empty($organization_code_2)) {
              if (isset($this->billing_affiliate_err_dtls['invalid_combination_code']) && $this->billing_affiliate_err_dtls['invalid_combination_code']) {
                  $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
                  $xpath_ids = array(
                      'organization_code_1'     => $organization_code_1,
                      'organization_code_2'     => $organization_code_2,
                      'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
                  );
                  $this->addBusinessValidationErrors($xpath_ids);
                  return false;
              }
          }
        }
      return true;
    }

    public function postBusinessInvalidCustContractCurrencyOsd()
    {
      $has_error           = false;
      $line_items_dtls     = $this->request_data['ContractDetails']['ContractLineItemDetails'];
      $base_cust_contract_currency = $line_items_dtls[0]['ContractLineItemHeader']['cust_contract_currency'];
      for ($i = 1; $i < count($line_items_dtls); $i++) {
        $cust_contract_currency = $line_items_dtls[$i]['ContractLineItemHeader']['cust_contract_currency'];
        if ($base_cust_contract_currency != $cust_contract_currency) {
            $has_error           = true;
            $xpath_ids           = array(
                    'cust_contract_currency_xml_id' => $line_items_dtls[$i]['ContractLineItemHeader']['cust_contract_currency_xml_id'],
                    );
            $this->addBusinessValidationErrors($xpath_ids);
        }
        continue;
      }
      if ($has_error) {
        return false;
      }
      return true;
    }

    public function postBusinessInvalidCustBillingCurrencyOsd()
    {
        $has_error           = false;
        $line_items_dtls     = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        $base_cust_billing_currency = $line_items_dtls[0]['ContractLineItemHeader']['cust_billing_currency'];
        for ($i = 1; $i < count($line_items_dtls); $i++) {
          $cust_billing_currency = $line_items_dtls[$i]['ContractLineItemHeader']['cust_billing_currency'];
          if ($base_cust_billing_currency != $cust_billing_currency) {
              $has_error           = true;
              $xpath_ids           = array(
                      'cust_billing_currency_xml_id' => $line_items_dtls[$i]['ContractLineItemHeader']['cust_billing_currency_xml_id'],
                      );
              $this->addBusinessValidationErrors($xpath_ids);
          }
          continue;
        }
        if ($has_error) {
          return false;
        }
        return true;
    }

    public function postBusinessInvalidIgcContractCurrencyOsd()
    {
        $has_error           = false;
        $line_items_dtls     = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        $base_igc_contract_currency = $line_items_dtls[0]['ContractLineItemHeader']['igc_contract_currency'];
        for ($i = 1; $i < count($line_items_dtls); $i++) {
          $igc_contract_currency = $line_items_dtls[$i]['ContractLineItemHeader']['igc_contract_currency'];
          if ($base_igc_contract_currency != $igc_contract_currency) {
              $has_error           = true;
              $xpath_ids           = array(
                      'igc_contract_currency_xml_id' => $line_items_dtls[$i]['ContractLineItemHeader']['igc_contract_currency_xml_id'],
                      );
              $this->addBusinessValidationErrors($xpath_ids);
          }
          continue;
        }
        if ($has_error) {
          return false;
        }
        return true;
    }

    public function postBusinessInvalidIgcBillingCurrencyOsd()
    {
        $has_error           = false;
        $line_items_dtls     = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        $base_igc_billing_currency = $line_items_dtls[0]['ContractLineItemHeader']['igc_billing_currency'];
        for ($i = 1; $i < count($line_items_dtls); $i++) {
          $igc_billing_currency = $line_items_dtls[$i]['ContractLineItemHeader']['igc_billing_currency'];
          if ($base_igc_billing_currency != $igc_billing_currency) {
              $has_error           = true;
              $xpath_ids           = array(
                      'igc_billing_currency_xml_id' => $line_items_dtls[$i]['ContractLineItemHeader']['igc_billing_currency_xml_id'],
                      );
              $this->addBusinessValidationErrors($xpath_ids);
          }
          continue;
        }
        if ($has_error) {
          return false;
        }
        return true;
    }

    public function postBusinessInvalidCustomerBillingMethodOsd()
    {
        $has_error           = false;
        $line_items_dtls     = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        $base_customer_billing_method = $line_items_dtls[0]['ContractLineItemHeader']['customer_billing_method'];
        for ($i = 1; $i < count($line_items_dtls); $i++) {
          $customer_billing_method = $line_items_dtls[$i]['ContractLineItemHeader']['customer_billing_method'];
          if ($base_customer_billing_method != $customer_billing_method) {
              $has_error           = true;
              $xpath_ids           = array(
                      'customer_billing_method_xml_id' => $line_items_dtls[$i]['ContractLineItemHeader']['customer_billing_method_xml_id'],
                      );
              $this->addBusinessValidationErrors($xpath_ids);
          }
          continue;
        }
        if ($has_error) {
          return false;
        }
        return true;
    }

    public function postBusinessInvalidIgcSettlementMethodOsd()
    {
        $has_error           = false;
        $line_items_dtls     = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        $base_igc_settlement_method = $line_items_dtls[0]['ContractLineItemHeader']['igc_settlement_method'];
        for ($i = 1; $i < count($line_items_dtls); $i++) {
          $igc_settlement_method = $line_items_dtls[$i]['ContractLineItemHeader']['igc_settlement_method'];
          if ($base_igc_settlement_method != $igc_settlement_method) {
              $has_error           = true;
              $xpath_ids           = array(
                      'igc_settlement_method_xml_id' => $line_items_dtls[$i]['ContractLineItemHeader']['igc_settlement_method_xml_id'],
                      );
              $this->addBusinessValidationErrors($xpath_ids);
          }
          continue;
        }
        if ($has_error) {
          return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLineCreditcardNo()
    {
        $this->mandatory_field_list = $this->validatePaymentDetails($this->request_data['ContractDetails']['ContractLineItemDetails'][0]);
        if (isset($this->mandatory_field_list['Credit Card']['credit_card_no']) && $this->mandatory_field_list['Credit Card']['credit_card_no']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLineCreditcardExpDate()
    {
        if (isset($this->mandatory_field_list['Credit Card']['expiration_date']) && $this->mandatory_field_list['Credit Card']['expiration_date']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLineDirectDepositBankcode()
    {
        if (isset($this->mandatory_field_list['Direct Deposit']['bank_code']) && $this->mandatory_field_list['Direct Deposit']['bank_code']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLineDirectDepositBranchcode()
    {
        if (isset($this->mandatory_field_list['Direct Deposit']['branch_code']) && $this->mandatory_field_list['Direct Deposit']['branch_code']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLineDirectDepositType()
    {
        if (isset($this->mandatory_field_list['Direct Deposit']['deposit_type']) && $this->mandatory_field_list['Direct Deposit']['deposit_type']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLineDirectDepositAccno()
    {
        if (isset($this->mandatory_field_list['Direct Deposit']['account_no']) && $this->mandatory_field_list['Direct Deposit']['account_no']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLineAccTransferName()
    {
        if (isset($this->mandatory_field_list['Account Transfer']['name']) && $this->mandatory_field_list['Account Transfer']['name']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLineAccTransferBankcode()
    {
        if (isset($this->mandatory_field_list['Account Transfer']['bank_code']) && $this->mandatory_field_list['Account Transfer']['bank_code']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLineAccTransferBranchcode()
    {
        if (isset($this->mandatory_field_list['Account Transfer']['branch_code']) && $this->mandatory_field_list['Account Transfer']['branch_code']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLineAccTransferType()
    {
        if (isset($this->mandatory_field_list['Account Transfer']['deposit_type']) && $this->mandatory_field_list['Account Transfer']['deposit_type']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLineAccTransferAccno()
    {
        if (isset($this->mandatory_field_list['Account Transfer']['account_no']) && $this->mandatory_field_list['Account Transfer']['account_no']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLineAccTransferAccnoDisplay()
    {
        if (isset($this->mandatory_field_list['Account Transfer']['account_no_display']) && $this->mandatory_field_list['Account Transfer']['account_no_display']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLinePostalTransferName()
    {
        if (isset($this->mandatory_field_list['Postal Transfer']['name']) && $this->mandatory_field_list['Postal Transfer']['name']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLinePostalTransferPassbookMark()
    {
        if (isset($this->mandatory_field_list['Postal Transfer']['postal_passbook_mark']) && $this->mandatory_field_list['Postal Transfer']['postal_passbook_mark']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLinePostalTransferPassbook()
    {
        if (isset($this->mandatory_field_list['Postal Transfer']['postal_passbook']) && $this->mandatory_field_list['Postal Transfer']['postal_passbook']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractLinePostalTransferPassbookNoDisplay()
    {
        if (isset($this->mandatory_field_list['Postal Transfer']['postal_passbook_no_display']) && $this->mandatory_field_list['Postal Transfer']['postal_passbook_no_display']) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid' => $global_contract_id_uuid_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

	 /**
     * DF-2009 beluga validation : billing account is mandatory for non-workflow contracts.
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Feb 05, 2018
     */
    public function postBusinessMandatoryBelugaBillingAccount(){

    $has_error = false;

        $this->belunga_contract_activation_err = ApiValidationHelper::validateXmlBillingAccountForBelugaProduct($this->request_data, $this->request_type, $this->xml_product_offering_id);

        if(!empty($this->belunga_contract_activation_err)){
            $first_line_item_err_details = current($this->belunga_contract_activation_err);
            if(empty($first_line_item_err_details)){
                $has_error = true;
            }
        }
        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

	 /**
     * DF-2009 beluga validation : payment type is mandatory for non-workflow contracts.
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Feb 05, 2018
     */
    public function postBusinessMandatoryBelugaPaymentType(){

    $has_error = false;

        if($this->belunga_contract_activation_err == null)
            $this->belunga_contract_activation_err = ApiValidationHelper::validateXmlBillingAccountForBelugaProduct($this->request_data, $this->request_type, $this->xml_product_offering_id);

        if(!empty($this->belunga_contract_activation_err)){
            $first_line_item_err_details = current($this->belunga_contract_activation_err);
            if(!empty($first_line_item_err_details) && in_array('Payment Type',$first_line_item_err_details)){
                $has_error = true;
            }
        }
        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

	 /**
     * DF-2009 beluga validation : Line Item billing start date is mandatory for non-workflow contracts.
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Feb 05, 2018
     */
    public function postBusinessMandatoryBelugaBillingStartDate(){

    $has_error = false;

        if($this->belunga_contract_activation_err == null)
            $this->belunga_contract_activation_err = ApiValidationHelper::validateXmlBillingAccountForBelugaProduct($this->request_data, $this->request_type, $this->xml_product_offering_id);

        if(!empty($this->belunga_contract_activation_err)){
             foreach($this->belunga_contract_activation_err as $key => $value){
                if( !empty($value) && in_array('Billing Start Date',$value)){
                    $xpath_ids = array(
                        'line_item_xml_id' => $key,
                    );
                    $this->addBusinessValidationErrors($xpath_ids);
                    $has_error = true;
                }
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

	 /**
     * DF-2009 beluga validation : Line item Billing End date is mandatory for non-workflow contracts.
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Feb 05, 2018
     */
    public function postBusinessMandatoryBelugaBillingEndDate(){

    $has_error = false;

        if($this->belunga_contract_activation_err == null)
            $this->belunga_contract_activation_err = ApiValidationHelper::validateXmlBillingAccountForBelugaProduct($this->request_data, $this->request_type, $this->xml_product_offering_id);

        if(!empty($this->belunga_contract_activation_err)){
             foreach($this->belunga_contract_activation_err as $key => $value){
                if( !empty($value) && in_array('Billing End Date',$value)){
                    $xpath_ids = array(
                        'line_item_xml_id' => $key,
                    );
                    $this->addBusinessValidationErrors($xpath_ids);
                    $has_error = true;
                }
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function postBusinessMandatoryContractAccount()
    {
        $has_error = false;
        if (!isset($this->request_data['Accounts']['Contract'])) {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }
}
