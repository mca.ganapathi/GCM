<?php

trait GbsPreValidationLayer
{
	/**
     * invalid session validate in GBS request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function gbsPreInvalidSession()
    {
        // check session validation
        $success = $this->invalidSession();
        if ($success == false) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * PMS connection validation in gbs request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function gbsPreInvalidPmsConnection()
    {
        // check pms connection
        $success = ApiValidationHelper::pmsConnection();
        // has validation error
        if ($success == false) {
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    } 
}