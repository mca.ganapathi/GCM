<?php

trait IdwhBusinessValidationLayer
{
    public $idwh_updated_after_err_list;
	/**
     * mandatory check for contract-customer-id, 'contract-product-id' or 'updated-after' parameters in iDWH
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 08, 2017
     */
    public function idwhBusinessMandatoryIdwhParams()
    {
        $has_error = false;

        if (trim($this->contractCustomerId) == '' && trim($this->contractProductId) == '' && trim($this->updatedAfter) == '') {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array();
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }

        return true;
    }

    /**
     * invalid value check for contract-product-id parameters in iDWH
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 08, 2017
     */
    public function idwhBusinessInvalidContractProductId()
    {
        $has_error = false;

        if (trim($this->contractProductId) != '') {
            $cp_det = explode(':', $this->contractProductId);
            $pos    = strpos($this->contractProductId, ':');
            if (count($cp_det) > 2 || $pos === false || $pos == false || ($pos !== false && isset($cp_det[1]) && $cp_det[1] !== '1') || isset($cp_det[0]) && !$this->mod_utils->validatePMSIdPattern($cp_det[0])) {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'contract_product_id' => $this->contractProductId,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }

        return true;
    }

    /**
     * invalid date format check for updated-after parameters in iDWH
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 08, 2017
     */
    public function idwhBusinessInvalidUpdatedAfter()
    {
        $this->idwh_updated_after_err_list = $this->isInvalidUpdatedAfter($this->updatedAfter);
        if ($this->idwh_updated_after_err_list['updated_after']['invalid']) {
            $xpath_ids = array(
                'updated_after' => $this->updatedAfter,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * invalid timezone check for updated-after parameters in iDWH
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 08, 2017
     */
    public function idwhBusinessInvalidUpdatedAfterTimzone()
    {
       if ($this->idwh_updated_after_err_list['time_zone']['invalid']) {
            $xpath_ids = array(
                'updated_after_timzone' => $this->updatedAfter,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
       }
        return true;
    }

    /**
     * invalid contract status value check for status parameters in iDWH
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 08, 2017
     */
    public function idwhBusinessInvalidContractStatus()
    {
        $has_error = false;

        if (trim($this->contractStatus) != '') {
            $reqContractStatusList = explode(',', $this->contractStatus);
            $reqContractStatusList = array_filter($reqContractStatusList);
            if (empty($reqContractStatusList)) {
                $wrong_status_list = ' ';
                $has_error         = true;
            } else {
                $reqContractStatusList = array_map('trim', $reqContractStatusList);
                $contractStatusList    = array_filter($GLOBALS['app_list_strings']['contract_status_list']);
                $wrong_status          = array_diff($reqContractStatusList, $contractStatusList);
                if (!empty($wrong_status)) {
                    $wrong_status_list = implode(',', $wrong_status);
                    $has_error         = true;
                }
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'contract_status' => $wrong_status_list,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }

        return true;
    }
}