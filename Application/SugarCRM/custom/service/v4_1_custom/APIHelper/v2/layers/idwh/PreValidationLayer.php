<?php

trait IdwhPreValidationLayer
{
	/**
     * invalid session validate in IDWH request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function idwhPreInvalidSession()
    {
        // check session validation
        $success = $this->invalidSession();
        if ($success == false) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }
}