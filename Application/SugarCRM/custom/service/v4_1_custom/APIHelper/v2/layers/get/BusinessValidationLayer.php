<?php

trait GetBusinessValidationLayer
{
	/**
     * Contract Reference ID should not be empty in get request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Oct 16, 2017
     */
    public function getBusinessMandatoryParams()
    {
        $has_error = false;

        if (($this->global_contract_id == '' && $this->global_contract_uuid == '') || $this->version_number === '0') {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array();
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * [global contract id] GET invalid request params
     *
     * @return boolean
     *
     * @author rajul.mondal
     * @since Oct 04, 2017
     */
    public function getBusinessInvalidParams()
    {
        $has_error = false;
        if (empty($this->contract_data['header'])) {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id' => !empty($this->global_contract_id) ? $this->global_contract_id : $this->global_contract_uuid,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }

        return true;
    } 
}