<?php

trait GetPreValidationLayer
{
	/**
     * invalid session validate in GET request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function getPreInvalidSession()
    {
        // check session validation
        $success = $this->invalidSession();
        if ($success == false) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * PMS connection validation in post request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function getPreInvalidPmsConnection()
    {
        return $this->postPreInvalidPmsConnection();   
    }
}