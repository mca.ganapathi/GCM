<?php

trait PatchPreValidationLayer
{
	/**
     * invalid session validate in PATCH request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function patchPreInvalidSession()
    {
        // check session validation
        $success = $this->invalidSession();
        if ($success == false) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * XML empty validation in patch request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function patchPreMandatoryXmlValidation()
    {
        return $this->postPreMandatoryXmlValidation();
    }

    /**
     * XSD validation in patch request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function patchPreInvalidXsdValidation()
    {
        // check XSD validation
        $has_error = ApiValidationHelper::xsdValidation($this->request_data);
        if ($has_error) {
            $custom_error_details = [
                'details' => $has_error,
            ];
            // add error in error list
            $this->addPreValidationErrors([], $custom_error_details);
            return false;
        }
        return true;
    }

    /**
     * [3003] CIDAS connection validation in patch request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function patchPreInvalidCidasConnectionFailed()
    {
        return $this->postPreInvalidCidasConnectionFailed();
    }

    /**
     * [3005] CIDAS connection validation in patch request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function patchPreInvalidCidasConnectionZeroRecord()
    {
        return $this->postPreInvalidCidasConnectionZeroRecord();
    }

    /**
     * [E001] CIDAS connection validation in patch request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function patchPreInvalidCidasConnectionE001()
    {
        $error_code = 'E001';
        // check [E001] CIDAS connection validation
        $has_error = ApiValidationHelper::cidasConnection($this->request_data, $error_code);
        if ($has_error == false) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * [E002] CIDAS connection validation in patch request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function patchPreInvalidCidasConnectionE002()
    {
        $error_code = 'E002';
        // check [E002] CIDAS connection validation
        $has_error = ApiValidationHelper::cidasConnection($this->request_data, $error_code);
        if ($has_error == false) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * [E003] CIDAS connection validation in patch request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function patchPreInvalidCidasConnectionE003()
    {
        $error_code = 'E003';
        // check [E003] CIDAS connection validation
        $has_error = ApiValidationHelper::cidasConnection($this->request_data, $error_code);
        if ($has_error == false) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * [E004] CIDAS connection validation in patch request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function patchPreInvalidCidasConnectionE004()
    {
        $error_code = 'E004';
        // check [E004] CIDAS connection validation
        $has_error = ApiValidationHelper::cidasConnection($this->request_data, $error_code);
        if ($has_error == false) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * PMS connection validation in patch request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function patchPreInvalidPmsConnection()
    {
        // check pms connection
        $success = ApiValidationHelper::pmsConnection();
        // has validation error
        if ($success == false) {
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }
}