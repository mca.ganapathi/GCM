<?php

trait VersionUpBusinessValidationLayer
{
    /**
     * Contract Reference ID should not be empty in version_up request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Oct 16, 2017
     */
    public function versionUpBusinessMandatoryParams()
    {
        return $this->getBusinessMandatoryParams();
    }

    /**
     * validate global contract id for POST version-up request
     *
     * @return boolean
     *
     * @author sagar.salunkhe
     * @since Oct 06, 2017
     */
    public function versionUpBusinessInvalidGlobalContractIdUrl()
    {
        $has_error = false;

        if ($this->request_data['ContractDetails']['ContractsHeader']['is_new_record'] && ($this->global_contract_id != '' || $this->global_contract_uuid != '')) {
            $has_error = true;
        }

        if ($has_error) {
            $xpath_ids = array();
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function versionUpBusinessMandatoryContractName()
    {
        return $this->postBusinessMandatoryContractName();
    }

    public function versionUpBusinessMandatoryProductOfferingId()
    {
        return $this->postBusinessMandatoryProductOfferingId();
    }

    public function versionUpBusinessMandatoryLineItemDetails()
    {
        return $this->postBusinessMandatoryLineItemDetails();
    }

    public function versionUpBusinessMandatoryProdCharacteristicDtls()
    {
        return $this->postBusinessMandatoryProdCharacteristicDtls();
    }

    public function versionUpBusinessInvalidBillingType()
    {
        return $this->postBusinessInvalidBillingType();
    }

    public function versionUpBusinessMandatoryClientReqId()
    {
        return $this->postBusinessMandatoryClientReqId();
    }

    public function versionUpBusinessMandatoryContractsStartDate()
    {
        return $this->postBusinessMandatoryContractsStartDate();
    }

    public function versionUpBusinessInvalidContractsDatesCompare()
    {
        return $this->postBusinessInvalidContractsDatesCompare();
    }

    public function versionUpBusinessInvalidSalesCompany()
    {
        return $this->postBusinessInvalidSalesCompany();
    }

    public function versionUpBusinessInvalidContractStartdtTz()
    {
        return $this->postBusinessInvalidContractStartdtTz();
    }

    public function versionUpBusinessInvalidContractEnddtTz()
    {
        return $this->postBusinessInvalidContractEnddtTz();
    }

    public function versionUpBusinessInvalidLinePayeeEmail()
    {
        return $this->postBusinessInvalidLinePayeeEmail();
    }

    public function versionUpBusinessMandatoryLineItemDetailsIndex()
    {
        return $this->postBusinessMandatoryLineItemDetailsIndex();
    }

    public function versionUpBusinessMandatoryProductSpecPmsId()
    {
        return $this->postBusinessMandatoryProductSpecPmsId();
    }

    public function versionUpBusinessInvalidLineServiceDatesCompare()
    {
        return $this->postBusinessInvalidLineServiceDatesCompare();
    }

    public function versionUpBusinessInvalidLineBillingDatesCompare()
    {
        return $this->postBusinessInvalidLineBillingDatesCompare();
    }

    public function versionUpBusinessInvalidServiceStartDateTimezone()
    {
        return $this->postBusinessInvalidServiceStartDateTimezone();
    }

    public function versionUpBusinessInvalidServiceEndDateTimezone()
    {
        return $this->postBusinessInvalidServiceEndDateTimezone();
    }

    public function versionUpBusinessInvalidBillingStartDateTimezone()
    {
        return $this->postBusinessInvalidBillingStartDateTimezone();
    }

    public function versionUpBusinessInvalidBillingEndDateTimezone()
    {
        return $this->postBusinessInvalidBillingEndDateTimezone();
    }

    public function versionUpBusinessInvalidContractSurrogateTeams()
    {
        return $this->postBusinessInvalidContractSurrogateTeams();
    }

    public function versionUpBusinessInvalidSalesRepresentativeEmail()
    {
        return $this->postBusinessInvalidSalesRepresentativeEmail();
    }

    public function versionUpBusinessMandatoryContractingAccountName()
    {
        return $this->postBusinessMandatoryContractingAccountName();
    }

    public function versionUpBusinessMandatoryBillingAccountName()
    {
        return $this->postBusinessMandatoryBillingAccountName();
    }

    public function versionUpBusinessMandatoryTechnologyAccountName()
    {
        return $this->postBusinessMandatoryTechnologyAccountName();
    }

    public function versionUpBusinessMandatoryContractingCompanyDtls()
    {
        return $this->postBusinessMandatoryContractingCompanyDtls();
    }

    public function versionUpBusinessMandatoryBillingCompanyDtls()
    {
        return $this->postBusinessMandatoryBillingCompanyDtls();
    }

    public function versionUpBusinessMandatoryTechnologyCompanyDtls()
    {
        return $this->postBusinessMandatoryTechnologyCompanyDtls();
    }

    public function versionUpBusinessMandatoryContractingCorporateNameId()
    {
        return $this->postBusinessMandatoryContractingCorporateNameId();
    }

    public function versionUpBusinessInvalidContractingAccountCountryCode()
    {
        return $this->postBusinessInvalidContractingAccountCountryCode();
    }

    public function versionUpBusinessInvalidBillingAccountCountryCode()
    {
        return $this->postBusinessInvalidBillingAccountCountryCode();
    }

    public function versionUpBusinessInvalidTechnologyAccountCountryCode()
    {
        return $this->postBusinessInvalidTechnologyAccountCountryCode();
    }

    public function versionUpBusinessInvalidContractAccountCountryStateCompare()
    {
        return $this->postBusinessInvalidContractAccountCountryStateCompare();
    }

    public function versionUpBusinessInvalidBillingAccountCountryStateCompare()
    {
        return $this->postBusinessInvalidBillingAccountCountryStateCompare();
    }

    public function versionUpBusinessInvalidTechnologyAccountCountryStateCompare()
    {
        return $this->postBusinessInvalidTechnologyAccountCountryStateCompare();
    }

    public function versionUpBusinessInvalidContractAccountAddressTag()
    {
        return $this->postBusinessInvalidContractAccountAddressTag();
    }

    public function versionUpBusinessInvalidBillingAccountAddressTag()
    {
        return $this->postBusinessInvalidBillingAccountAddressTag();
    }

    public function versionUpBusinessInvalidTechnologyAccountAddressTag()
    {
        return $this->postBusinessInvalidTechnologyAccountAddressTag();
    }

    public function versionUpBusinessInvalidContractAccountEmail()
    {
        return $this->postBusinessInvalidContractAccountEmail();
    }

    public function versionUpBusinessInvalidBillingAccountEmail()
    {
        return $this->postBusinessInvalidBillingAccountEmail();
    }

    public function versionUpBusinessInvalidTechnologyAccountEmail()
    {
        return $this->postBusinessInvalidTechnologyAccountEmail();
    }

    public function versionUpBusinessInvalidBillingAffiliateOrgCode1()
    {
        return $this->postBusinessInvalidBillingAffiliateOrgCode1();
    }

    public function versionUpBusinessInvalidBillingAffiliateOrgCode2()
    {
        return $this->postBusinessInvalidBillingAffiliateOrgCode2();
    }

    public function versionUpBusinessInvalidBillingAffiliateOrgCodeCompare()
    {
        return $this->postBusinessInvalidBillingAffiliateOrgCodeCompare();
    }

    public function versionUpBusinessInvalidCustContractCurrencyOsd()
    {
        return $this->postBusinessInvalidCustContractCurrencyOsd();
    }

    public function versionUpBusinessInvalidCustBillingCurrencyOsd()
    {
        return $this->postBusinessInvalidCustBillingCurrencyOsd();
    }

    public function versionUpBusinessInvalidIgcContractCurrencyOsd()
    {
        return $this->postBusinessInvalidIgcContractCurrencyOsd();
    }

    public function versionUpBusinessInvalidIgcBillingCurrencyOsd()
    {
        return $this->postBusinessInvalidIgcBillingCurrencyOsd();
    }

    public function versionUpBusinessInvalidCustomerBillingMethodOsd()
    {
        return $this->postBusinessInvalidCustomerBillingMethodOsd();
    }

    public function versionUpBusinessInvalidIgcSettlementMethodOsd()
    {
        return $this->postBusinessInvalidIgcSettlementMethodOsd();
    }

    public function versionUpBusinessMandatoryContractLineCreditcardNo()
    {
        return $this->postBusinessMandatoryContractLineCreditcardNo();
    }

    public function versionUpBusinessMandatoryContractLineCreditcardExpDate()
    {
        return $this->postBusinessMandatoryContractLineCreditcardExpDate();
    }

    public function versionUpBusinessMandatoryContractLineDirectDepositBankcode()
    {
        return $this->postBusinessMandatoryContractLineDirectDepositBankcode();
    }

    public function versionUpBusinessMandatoryContractLineDirectDepositBranchcode()
    {
        return $this->postBusinessMandatoryContractLineDirectDepositBranchcode();
    }

    public function versionUpBusinessMandatoryContractLineDirectDepositType()
    {
        return $this->postBusinessMandatoryContractLineDirectDepositType();
    }

    public function versionUpBusinessMandatoryContractLineDirectDepositAccno()
    {
        return $this->postBusinessMandatoryContractLineDirectDepositAccno();
    }

    public function versionUpBusinessMandatoryContractLineAccTransferName()
    {
        return $this->postBusinessMandatoryContractLineAccTransferName();
    }

    public function versionUpBusinessMandatoryContractLineAccTransferBankcode()
    {
        return $this->postBusinessMandatoryContractLineAccTransferBankcode();
    }

    public function versionUpBusinessMandatoryContractLineAccTransferBranchcode()
    {
        return $this->postBusinessMandatoryContractLineAccTransferBranchcode();
    }

    public function versionUpBusinessMandatoryContractLineAccTransferType()
    {
        return $this->postBusinessMandatoryContractLineAccTransferType();
    }

    public function versionUpBusinessMandatoryContractLineAccTransferAccno()
    {
        return $this->postBusinessMandatoryContractLineAccTransferAccno();
    }

    public function versionUpBusinessMandatoryContractLineAccTransferAccnoDisplay()
    {
        return $this->postBusinessMandatoryContractLineAccTransferAccnoDisplay();
    }

    public function versionUpBusinessMandatoryContractLinePostalTransferName()
    {
        return $this->postBusinessMandatoryContractLinePostalTransferName();
    }

    public function versionUpBusinessMandatoryContractLinePostalTransferPassbookMark()
    {
        return $this->postBusinessMandatoryContractLinePostalTransferPassbookMark();
    }

    public function versionUpBusinessMandatoryContractLinePostalTransferPassbook()
    {
        return $this->postBusinessMandatoryContractLinePostalTransferPassbook();
    }

    public function versionUpBusinessMandatoryContractLinePostalTransferPassbookNoDisplay()
    {
        return $this->postBusinessMandatoryContractLinePostalTransferPassbookNoDisplay();
    }

    public function versionUpBusinessMandatoryContractAccount()
    {
        return $this->postBusinessMandatoryContractAccount();
    }

    public function versionUpBusinessMandatoryBelugaBillingAccount()
    {
        return $this->postBusinessMandatoryBelugaBillingAccount();
    }

    public function versionUpBusinessMandatoryBelugaPaymentType()
    {
        return $this->postBusinessMandatoryBelugaPaymentType();
    }

    public function versionUpBusinessMandatoryBelugaBillingStartDate()
    {
        return $this->postBusinessMandatoryBelugaBillingStartDate();
    }

    public function versionUpBusinessMandatoryBelugaBillingEndDate()
    {
        return $this->postBusinessMandatoryBelugaBillingEndDate();
    }

/***END : Version-up POST related validation***/

    public function versionUpBusinessInvalidGlobalContractIdMatch()
    {
        $has_error = false;

        if (isset($this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'])) {
            $global_contract_id_uuid = trim($this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id']);

            if ($global_contract_id_uuid != '' && $global_contract_id_uuid != trim($this->contract_data['header']->global_contract_id_uuid)) {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidGlobalContractUuidMatch()
    {

        $has_error = false;

        if (isset($this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'])) {
            $global_contract_id_uuid = trim($this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id']);

            if ($global_contract_id_uuid != '' && $global_contract_id_uuid != trim($this->contract_data['header']->global_contract_id_uuid)) {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'global_contract_id_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidProductOfferingIdUuidMatch()
    {
        $has_error = false;

        if (isset($this->request_data['ContractDetails']['ContractsHeader']['product_offering_xml_id'])) {
            $product_offering_uuid = trim($this->request_data['ContractDetails']['ContractsHeader']['product_offering_xml_id']);

            if ($product_offering_uuid != '' && $product_offering_uuid != trim($this->contract_data['header']->product_offering_uuid)) {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'product_offering'      => ApiValidationHelper::formatPMSID($this->request_data['ContractDetails']['ContractsHeader']['product_offering'],1),
                'product_offering_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['product_offering_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidContractSchemeUuidMatch()
    {
        $has_error = false;

        if (isset($this->request_data['ContractDetails']['ContractsHeader']['contract_scheme_xml_id'])) {
            $contract_scheme_uuid = trim($this->request_data['ContractDetails']['ContractsHeader']['contract_scheme_xml_id']);

            if ($contract_scheme_uuid != '' && $contract_scheme_uuid != trim($this->contract_data['header']->contract_scheme_uuid)) {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'contract_scheme_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['contract_scheme_xml_id']
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidContractTypeUuidMatch()
    {
        $has_error = false;

        if (isset($this->request_data['ContractDetails']['ContractsHeader']['contract_type_xml_id'])) {
            $contract_type_uuid = trim($this->request_data['ContractDetails']['ContractsHeader']['contract_type_xml_id']);

            if ($contract_type_uuid != '' && $contract_type_uuid != trim($this->contract_data['header']->contract_type_uuid)) {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'contract_type_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['contract_type_xml_id']
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidBillingTypeUuidMatch()
    {
        $has_error = false;

        if (isset($this->request_data['ContractDetails']['ContractsHeader']['billing_type_xml_id'])) {
            $billing_type_uuid = trim($this->request_data['ContractDetails']['ContractsHeader']['billing_type_xml_id']);

            if ($billing_type_uuid != '' && $billing_type_uuid != trim($this->contract_data['header']->billing_type_uuid)) {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'billing_type_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['billing_type_xml_id']
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidLoiContractUuidMatch()
    {
        $has_error = false;

        if (isset($this->request_data['ContractDetails']['ContractsHeader']['loi_contract_xml_id'])) {
            $loi_contract_uuid = trim($this->request_data['ContractDetails']['ContractsHeader']['loi_contract_xml_id']);

            if ($loi_contract_uuid != '' && $loi_contract_uuid != trim($this->contract_data['header']->loi_contract_uuid)) {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'loi_contract_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['loi_contract_xml_id']
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidFactoryReferenceNoUuidMatch()
    {
        $has_error = false;

        if (isset($this->request_data['ContractDetails']['ContractsHeader']['factory_reference_no_xml_id'])) {
            $factory_reference_no_uuid = trim($this->request_data['ContractDetails']['ContractsHeader']['factory_reference_no_xml_id']);

            if ($factory_reference_no_uuid != '' && $factory_reference_no_uuid != trim($this->contract_data['header']->factory_reference_no_uuid)) {
                $has_error = true;
            }
        }

        if ($has_error) {
            $xpath_ids = array(
                'factory_reference_no_uuid' => $this->request_data['ContractDetails']['ContractsHeader']['factory_reference_no_xml_id']
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidProductSpecificationUuidMatch()
    {
        $has_error                     = false;
        $prev_line_item_prod_spec_uuid = [];
        foreach ($this->contract_data['line_items'] as $prev_line_item) {
            $prev_line_item_prod_spec_uuid[] = $prev_line_item['ContractLineItemHeader']['product_specification_xml_id'];
        }

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $index => $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            //check if provided product_specification_xml_id is present in db for respective contract
            if (!in_array($line_item['ContractLineItemHeader']['product_specification_xml_id'], $prev_line_item_prod_spec_uuid)) {
                $has_error = true;
                $xpath_ids = array(
                    'product_specification_xml_id' => $line_item['ContractLineItemHeader']['product_specification_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }

        if ($has_error) {
            return false;
        }

        return true;
    }

    public function versionUpBusinessInvalidServiceStartDateUuidMatch()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            if (!empty($line_item['ContractLineItemHeader']['service_start_date_xml_id']) && $line_item['ContractLineItemHeader']['service_start_date_xml_id'] != $prev_line_item['ContractLineItemHeader']['service_start_date_xml_id']) {
                $has_error = true;
                $xpath_ids = array(
                    'service_start_date_xml_id' => $line_item['ContractLineItemHeader']['service_start_date_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidServiceEndDateUuidMatch()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            if (!empty($line_item['ContractLineItemHeader']['service_end_date_xml_id']) && $line_item['ContractLineItemHeader']['service_end_date_xml_id'] != $prev_line_item['ContractLineItemHeader']['service_end_date_xml_id']) {
                $has_error = true;
                $xpath_ids = array(
                    'service_end_date_xml_id' => $line_item['ContractLineItemHeader']['service_end_date_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }
    public function versionUpBusinessInvalidBillingStartDateUuidMatch()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            if (!empty($line_item['ContractLineItemHeader']['billing_start_date_xml_id']) && $line_item['ContractLineItemHeader']['billing_start_date_xml_id'] != $prev_line_item['ContractLineItemHeader']['billing_start_date_xml_id']) {
                $has_error = true;
                $xpath_ids = array(
                    'billing_start_date_xml_id' => $line_item['ContractLineItemHeader']['billing_start_date_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidBillingEndDateUuidMatch()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            if (!empty($line_item['ContractLineItemHeader']['billing_end_date_xml_id']) && $line_item['ContractLineItemHeader']['billing_end_date_xml_id'] != $prev_line_item['ContractLineItemHeader']['billing_end_date_xml_id']) {
                $has_error = true;
                $xpath_ids = array(
                    'billing_end_date_xml_id' => $line_item['ContractLineItemHeader']['billing_end_date_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidLineFactoryReferenceNoUuidMatch()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            if (!empty($line_item['ContractLineItemHeader']['factory_reference_no_xml_id']) && $line_item['ContractLineItemHeader']['factory_reference_no_xml_id'] != $prev_line_item['ContractLineItemHeader']['factory_reference_no_xml_id']) {
                $has_error = true;
                $xpath_ids = array(
                    'factory_reference_no_xml_id' => $line_item['ContractLineItemHeader']['factory_reference_no_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidCustContractCurrencyUuidMatch()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            if (!empty($line_item['ContractLineItemHeader']['cust_contract_currency_xml_id']) && $line_item['ContractLineItemHeader']['cust_contract_currency_xml_id'] != $prev_line_item['ContractLineItemHeader']['cust_contract_currency_xml_id']) {
                $has_error = true;
                $xpath_ids = array(
                    'cust_contract_currency_xml_id' => $line_item['ContractLineItemHeader']['cust_contract_currency_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidCustBillingCurrencyUuidMatch()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            if (!empty($line_item['ContractLineItemHeader']['cust_billing_currency_xml_id']) && $line_item['ContractLineItemHeader']['cust_billing_currency_xml_id'] != $prev_line_item['ContractLineItemHeader']['cust_billing_currency_xml_id']) {
                $has_error = true;
                $xpath_ids = array(
                    'cust_billing_currency_xml_id' => $line_item['ContractLineItemHeader']['cust_billing_currency_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidIgcContractCurrencyUuidMatch()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            if (!empty($line_item['ContractLineItemHeader']['igc_contract_currency_xml_id']) && $line_item['ContractLineItemHeader']['igc_contract_currency_xml_id'] != $prev_line_item['ContractLineItemHeader']['igc_contract_currency_xml_id']) {
                $has_error = true;
                $xpath_ids = array(
                    'igc_contract_currency_xml_id' => $line_item['ContractLineItemHeader']['igc_contract_currency_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidIgcBillingCurrencyUuidMatch()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            if (!empty($line_item['ContractLineItemHeader']['igc_billing_currency_xml_id']) && $line_item['ContractLineItemHeader']['igc_billing_currency_xml_id'] != $prev_line_item['ContractLineItemHeader']['igc_billing_currency_xml_id']) {
                $has_error = true;
                $xpath_ids = array(
                    'igc_billing_currency_xml_id' => $line_item['ContractLineItemHeader']['igc_billing_currency_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidCustomerBillingMethodUuidMatch()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            if (!empty($line_item['ContractLineItemHeader']['customer_billing_method_xml_id']) && $line_item['ContractLineItemHeader']['customer_billing_method_xml_id'] != $prev_line_item['ContractLineItemHeader']['customer_billing_method_xml_id']) {
                $has_error = true;
                $xpath_ids = array(
                    'customer_billing_method_xml_id' => $line_item['ContractLineItemHeader']['customer_billing_method_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidIgcSettlementMethodUuidMatch()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            if (!empty($line_item['ContractLineItemHeader']['igc_settlement_method_xml_id']) && $line_item['ContractLineItemHeader']['igc_settlement_method_xml_id'] != $prev_line_item['ContractLineItemHeader']['igc_settlement_method_xml_id']) {
                $has_error = true;
                $xpath_ids = array(
                    'igc_settlement_method_xml_id' => $line_item['ContractLineItemHeader']['igc_settlement_method_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidLineHeaderDescriptionUuidMatch()
    {
        $has_error = false;

        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            if (!empty($line_item['ContractLineItemHeader']['description_xml_id']) && $line_item['ContractLineItemHeader']['description_xml_id'] != $prev_line_item['ContractLineItemHeader']['description_xml_id']) {
                $has_error = true;
                $xpath_ids = array(
                    'description_xml_id' => $line_item['ContractLineItemHeader']['description_xml_id'],
                );
                $this->addBusinessValidationErrors($xpath_ids);
            }
        }
        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidLineProductConfigurationUuidMatch()
    {
        $has_error         = false;
        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            foreach ($line_item['ContractLineProductConfiguration'] as $product_configuration) {
                if ($product_configuration['is_new_record']) {
                    continue;
                }

                foreach ($prev_line_item['ContractLineProductConfiguration'] as $prev_product_configuration) {
                    if ($product_configuration['name'] == $prev_product_configuration['name'] && $product_configuration['characteristic_xml_id'] != $prev_product_configuration['characteristic_xml_id']) {
                        $has_error = true;
                        $xpath_ids = array(
                            'characteristic_xml_id' => $product_configuration['characteristic_xml_id'],
                        );
                        $this->addBusinessValidationErrors($xpath_ids);
                    }
                    continue;
                }
            }
        }

        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidLineProductRuleUuidMatch()
    {
        $has_error         = false;
        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            foreach ($line_item['ContractLineProductRule'] as $product_rule) {
                if ($product_rule['is_new_record']) {
                    continue;
                }

                foreach ($prev_line_item['ContractLineProductRule'] as $prev_product_rule) {
                    if ($product_rule['name'] == $prev_product_rule['name'] && $product_rule['rule_xml_id'] != $prev_product_rule['rule_xml_id']) {
                        $has_error = true;
                        $xpath_ids = array(
                            'rule_xml_id' => $product_rule['rule_xml_id'],
                        );
                        $this->addBusinessValidationErrors($xpath_ids);
                    }
                    continue;
                }
            }
        }

        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidLineProductPriceChargeUuidMatch()
    {
        $has_error         = false;
        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            foreach ($line_item['ContractLineProductPrice'] as $product_price) {
                if ($product_price['is_new_record']) {
                    continue;
                }

                foreach ($prev_line_item['ContractLineProductPrice'] as $prev_product_price) {
                    if ($product_price['name'] == $prev_product_price['name'] && $product_price['charge_xml_id'] != $prev_product_price['charge_xml_id']) {
                        $has_error = true;
                        $xpath_ids = array(
                            'charge_xml_id' => $product_price['charge_xml_id'],
                        );
                        $this->addBusinessValidationErrors($xpath_ids);
                    }
                    continue;
                }
            }
        }

        if ($has_error) {
            return false;
        }
        return true;
    }

    public function versionUpBusinessInvalidLineProductPriceCurrencyidUuidMatch()
    {
        $has_error         = false;
        $line_item_details = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        foreach ($line_item_details as $line_item) {
            //if new line item record -- skip xml_id and uuid comparison for the line item
            if ($line_item['is_new_record']) {
                continue;
            }

            $prev_line_item = [];
            foreach ($this->contract_data['line_items'] as $arr_line_items) {
                if ($line_item['ContractLineItemHeader']['line_item_id_xml_id'] == $arr_line_items['ContractLineItemHeader']['line_item_id_xml_id']) {
                    $prev_line_item = $arr_line_items;
                    break;
                }
                continue;
            }

            foreach ($line_item['ContractLineProductPrice'] as $product_price) {
                if ($product_price['is_new_record']) {
                    continue;
                }

                foreach ($prev_line_item['ContractLineProductPrice'] as $prev_product_price) {
                    if ($product_price['name'] == $prev_product_price['name']) {

                        foreach($product_price['xml_data'] as $cur_field=>$cur_dtls){

                            //ignore if field xml_data is empty - it will get caught in pms validation
                            if(!array_filter($product_price['xml_data'][$cur_field])) continue;

                            if($product_price['xml_data'][$cur_field]['xml_id'] != $prev_product_price['xml_data'][$cur_field]['xml_id']){
                                $has_error = true;
                                    $xpath_ids = array(
                                        'currencyid_xml_id' => $product_price['xml_data'][$cur_field]['xml_id'],
                                    );
                                    $this->addBusinessValidationErrors($xpath_ids);
                            };
                        }
                    }
                    continue;
                }
            }
        }

        if ($has_error) {
            return false;
        }
        return true;
    }

    /**
     * [global contract id] VERSION UP invalid request params
     *
     * @return boolean
     *
     * @author rajul.mondal
     * @since Oct 04, 2017
     */
    public function versionUpBusinessInvalidParams()
    {
        return $this->getBusinessInvalidParams();
    }

    /**
     * For only activated contract, version up process is applicable
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function versionUpBusinessInvalidContractStatus()
    {
        $contract_status_id = $this->contract_data['header']->contract_status;
        $is_db_status_activated = ApiValidationHelper::isDbContractStatusActivated($contract_status_id);

        if (!$is_db_status_activated) {
            $contract_status_name        = $GLOBALS['app_list_strings']['contract_status_list'][$contract_status_id];
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                'contract_status_name'        => $contract_status_name,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * restrict version up for deactivated contract
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function versionUpBusinessInvalidContractStatusDeactivateDb()
    {
        $contract_status_db = $this->contract_data['header']->contract_status;
        if ($contract_status_db == 2 && isset($this->request_data['ContractDetails']['ContractsHeader']['contract_status'])) {
            $xpath_ids = array();
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * restrict version up for billing type value is "Separate" in db
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function versionUpBusinessInvalidBillingTypeDb()
    {
        $billing_type_db = $this->contract_data['header']->billing_type;
        if ($billing_type_db != 'OneStop') {
            $xpath_ids = array();
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * Check Product offering is same as version up contract
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function versionUpBusinessInvalidProductOffering()
    {
        $product_offering_xml = $this->request_data['ContractDetails']['ContractsHeader']['product_offering'];
        $product_offering_db  = $this->contract_data['header']->product_offering;
        if ($product_offering_xml != $product_offering_db) {
            $xpath_ids = array(
                'product_offering_id'       => ApiValidationHelper::formatPMSID($product_offering_xml,1),
                'product_offering_uuid_xml' => $this->request_data['ContractDetails']['ContractsHeader']['product_offering_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * Contract version-up not applicable for non-workflow product.
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function versionUpBusinessInvalidProductOfferingWorkflow()
    {
        $product_offering_xml = $this->request_data['ContractDetails']['ContractsHeader']['product_offering'];
        global $sugar_config;
        if (!(isset($sugar_config['workflow_flag'][$product_offering_xml]) && strtolower($sugar_config['workflow_flag'][$product_offering_xml]) == 'yes')) {
            $xpath_ids = array(
                'product_offering_id'       => ApiValidationHelper::formatPMSID($product_offering_xml, 1),
                'product_offering_uuid_xml' => $this->request_data['ContractDetails']['ContractsHeader']['product_offering_xml_id'],
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * Contract version-up not applicable if product specification id changed from previous version.
     *
     * @return boolean
     *
     * @author rajul.mondal
     * @since Dec 08, 2017
     */
    public function versionUpBusinessInvalidProductSpecificationIdMatch()
    {
        $success = true;

        $db_line_items = $this->contract_data['line_items'];
        $db_product_specification_ids = [];
        foreach ($db_line_items as $db_line_item) {
            $db_product_specification_id = $db_line_item['ContractLineItemHeader']['product_specification'];
            $db_product_specification_xml_id = $db_line_item['ContractLineItemHeader']['product_specification_xml_id'];
            $db_product_specification_ids[$db_product_specification_xml_id] = $db_product_specification_id;
        }

        $xml_line_items = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        $xml_product_specification_ids = [];
        foreach ($xml_line_items as $line_item) {
            $xml_product_specification_id = $line_item['ContractLineItemHeader']['product_specification'];
            $xml_product_specification_xml_id = $line_item['ContractLineItemHeader']['product_specification_xml_id'];
            $xml_product_specification_ids[$xml_product_specification_xml_id] = $xml_product_specification_id;
        }

        // intersect product specifications
        $intersected = array_intersect_assoc($db_product_specification_ids, $xml_product_specification_ids);

        // missing product specifications
        $missing = array_diff_assoc($db_product_specification_ids, $intersected);

        // has error
        if($db_product_specification_ids != $intersected && !empty($missing)) {
            foreach ($missing as $missing_xml_id => $missing_specification_id) {
                $product_specification_id = $xml_product_specification_ids[$missing_xml_id];
                if(empty($product_specification_id))
                    continue;
                $xpath_ids = array(
                    'product_specification_id' => ApiValidationHelper::formatPMSID($product_specification_id, 1),
                    'product_specification_xml_id' => $missing_xml_id,
                );
                $this->addBusinessValidationErrors($xpath_ids);
                $success =  false;
            }
        }

        return $success;
    }

    /**
     * Contract Start Date should not be changed from initial version
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function versionUpBusinessInvalidContractStartDate()
    {
        $contract_start_date_db_utc    = $this->contract_data['header']->contract_startdate;
        $contract_start_date_xml_utc    = $this->request_data['ContractDetails']['ContractsHeader']['contract_startdate'];
        $contract_start_date_tz_xml = $this->request_data['ContractDetails']['ContractsHeader']['contract_startdate_timezone'];
        $contract_start_date_xml_local = ApiValidationHelper::convertGMTDatetoLocalDataDate($contract_start_date_xml_utc, $contract_start_date_tz_xml);
        if ( !empty($contract_start_date_db_utc) && !empty($contract_start_date_xml_utc) && ( $contract_start_date_db_utc != $contract_start_date_xml_utc)) {
            $has_error = true;
        }
        if ($has_error) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array(
                'global_contract_id_uuid_xml' => $global_contract_id_uuid_xml,
                'contract_start_date_value'   => str_replace(' ', 'T', $contract_start_date_xml_local).$contract_start_date_tz_xml,
            );
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * Check bi-item key exist for the line item which is mandatory regardless of bi-action in version_up request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Oct 20, 2017
     */
    public function versionUpBusinessMandatoryLineItemBiItem()
    {
        $has_error               = false;
        $are_all_li_removed      = true;
        $are_all_base_li_removed = false;
        $count_base_li = 0;

        $req_bi_action = "";
        //List of timezone related fields
        $timezonefields_arr = array('billing_start_date_timezone', 'billing_end_date_timezone', 'service_start_date_timezone', 'service_end_date_timezone');
        require_once 'custom/include/contractFieldNames.php';
        $xml_line_items      = $this->request_data['ContractDetails']['ContractLineItemDetails'];
        $contract_line_items = $this->contract_data['line_items'];
        $this->versionUpBeforeInvalidProductOfferingId();
        $this->postBeforeMandatoryProductOfferingDetails();
        $pms_specification_details = $this->pms_offering_details['specification_details'];

        //Fetch available atomic product key, atomic product offering key, bii-itme key in previous contract.
        //That is product_specification_xml_id,product_specification,line_item_id_xml_id
        $contract_line_items     = ApiValidationHelper::fetchAvailableLItemDetails($this->contract_data['line_items']);
        $prev_line_items_ids_arr = $contract_line_items['line_item_id_xml_ids'];

        if (!empty($xml_line_items) && !empty($contract_line_items)) {
            foreach ($xml_line_items as $xml_line_item) {
                $has_error_contract_line_product_rule = false;
                $pms_id                 = ApiValidationHelper::formatPMSID($xml_line_item['ContractLineItemHeader']['product_specification'], 1);
                $xml_lint_item_xpath_id = $xml_line_item['ContractLineItemHeader']['line_item_id_xml_id'];
                $xml_specification_id   = $xml_line_item['ContractLineItemHeader']['product_specification'];
                $req_bi_action          = trim($xml_line_item['ContractLineItemHeader']['bi_action']);
                $contract_line_item     = $contract_line_items[$xml_lint_item_xpath_id];
                // count total base line item
                if(trim($pms_specification_details[$xml_specification_id]['spectypeid']) == 1) {
                    $count_base_li += 1;
                }
                //if any timezonefield is set as 00:00, then it means timezone is not set. So make it as empty.
                foreach ($timezonefields_arr as $timezonefield) {
                    if (isset($xml_line_item['ContractLineItemHeader'][$timezonefield]) && $xml_line_item['ContractLineItemHeader'][$timezonefield] == '00:00') {
                        $xml_line_item['ContractLineItemHeader'][$timezonefield] = '';
                    }
                }
                // Check bi-item key exist for the line item which is mandatory regardless of bi-action
                /*
                if (empty(trim($xml_line_item['ContractLineItemHeader']['line_item_id_xml_id']))) {
                $xpath_ids = [];
                $custom_error_details = [
                'details' => 'PMS ID "'.$pms_id.'" - bii-itme key should not be empty',
                ];
                $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                $has_error = true;
                // 'PMS ID "'.$pms_id.'" - bii-itme key should not be empty'
                continue;
                }
                 */

                // Check atomic product key is exist for the line item regardless of bi-action
                /*
                if (empty(trim($xml_line_item['ContractLineItemHeader']['product_specification_xml_id']))) {
                $xpath_ids = [];
                $custom_error_details = [
                'details' => 'PMS ID "'.$pms_id.'" - Atomic Product Key shoud not be empty',
                ];
                $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                $has_error = true;
                // 'PMS ID "'.$pms_id.'" - Atomic Product Key shoud not be empty'
                }
                 */

                // Check bi-action is not empty for all the line item
                if (empty(trim($xml_line_item['ContractLineItemHeader']['bi_action']))) {
                    $xpath_ids            = [];
                    $custom_error_details = [
                        'messages' => 'Contract process terminated. Mandatory element(s) should not be empty.',
                        'details' => '/contract-info:contract-information/bi:product-biitems/bi:product-biitem[@id="' . $xml_line_item['ContractLineItemHeader']['line_item_id_xml_id'] . '"]/bi:action is mandatory',
                    ];
                    $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                    $has_error = true;
                    // 'PMS ID "'.$pms_id.'" -bi-action should not be empty'
                }

                // If any line item is not set as 'remove' then it measn there is at lease one line item available
                if ($are_all_li_removed && trim($xml_line_item['ContractLineItemHeader']['bi_action']) != self::LI_REMOVE) {
                    $are_all_li_removed = false;
                }

                //Check if at least one base line item available without removing it
                if ($xml_line_item['ContractLineItemHeader']['bi_action'] == self::LI_REMOVE && trim($pms_specification_details[$xml_specification_id]['spectypeid']) == 1) {
                    $are_all_base_li_removed = true;
                    // 'Cannot remove all base biitem. Atleast one biitem biitem should be exist.'
                }

                //Check is all are no change line item
                if (trim($xml_line_item['ContractLineItemHeader']['bi_action']) != self::LI_NO_CHANGE) {
                    $this->is_contract_changed = true;
                }
                if (trim($xml_line_item['is_new_record']) == '1') {
                    //If this is new line item then check whether bi-action is add.
                    if (trim($xml_line_item['ContractLineItemHeader']['bi_action']) != self::LI_ADD) {
                        $xpath_ids            = [];
                        $custom_error_details = [
                            'details' => '('.$xml_line_item['ContractLineItemHeader']['bi_action'].') in  /contract-info:contract-information/bi:product-biitems/bi:product-biitem[@id="' . $xml_lint_item_xpath_id . ':1"]/cbe:identified-by/bi:product-biitem-key/cbe:key-type-for-cbe-standard/cbe:object-id is invalid.',
                        ];
                        $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                        $has_error = true;
                        // 'PMS ID "'.$pms_id.'" - bi-action should be \'add\' as it is a new line item'
                    }
                    continue;
                } else {
                    //If this not a new line item then check the bi-action is not add.
                    if (trim($xml_line_item['ContractLineItemHeader']['bi_action']) == self::LI_ADD) {
                        $xpath_ids            = [];
                        $custom_error_details = [
                            'details' => '('.$xml_line_item['ContractLineItemHeader']['bi_action'].') in  /contract-info:contract-information/bi:product-biitems/bi:product-biitem[@id="' . $xml_lint_item_xpath_id . ':1"]/cbe:identified-by/bi:product-biitem-key/cbe:key-type-for-cbe-standard/cbe:object-id is invalid.',
                        ];
                        $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                        $has_error = true;
                        // 'PMS ID "'.$pms_id.'" - bi-action should not be \'add\' for the existing line item'
                    }
                }

                //Below validations are applicable only for old line item

                //Check Product Biitem Key is different from previous Version
                if (!in_array(trim($xml_lint_item_xpath_id), $contract_line_items['line_item_id_xml_ids'])) {
                    $xpath_ids            = [];
                    $custom_error_details = [
                        'details' => '(' . $xml_lint_item_xpath_id . ') in /contract-info:contract-information/bi:product-biitems/bi:product-biitem[@id="' . $xml_lint_item_xpath_id . ':1"]/cbe:identified-by/bi:product-biitem-key/cbe:key-type-for-cbe-standard/cbe:object-id is invalid with previous version of contract.',
                    ];
                    $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                    $has_error = true;
                    // 'PMS ID "'.$pms_id.'" - Invalid Biitem'
                } else {
                    //Check Atomic Product Key is different from previous Version
                    if (!in_array(trim($xml_line_item['ContractLineItemHeader']['product_specification_xml_id']), $contract_line_items['product_specification_xml_ids'])) {
                        $xpath_ids            = [];
                        $custom_error_details = [
                            'details' => '(' . $xml_line_item['ContractLineItemHeader']['product_specification_xml_id'] . ') in /contract-info:contract-information/product:atomic-products/product:atomic-product[@id="' . $xml_line_item['ContractLineItemHeader']['product_specification_xml_id'] . ':1"]/cbe:identified-by/product:atomic-product-key/cbe:key-type-for-cbe-standard/cbe:object-id is invalid with previous version of contract.',
                        ];
                        $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                        $has_error = true;
                        // 'PMS ID "'.$pms_id.'" - Product Specification Key shoud not be changed from previous version'
                    }

                    //Check Atomic Product Offering Key is different from previous Version
                    if (!empty(trim($xml_specification_id)) && !in_array(trim($xml_specification_id), $contract_line_items['product_specifications'])) {
                        $xpath_ids            = [];
                        $custom_error_details = [
                            'details' => '(' . $xml_specification_id . ') in /contract-info:contract-information/product:atomic-products/product:atomic-product[@id="' . $xml_line_item['ContractLineItemHeader']['product_specification_xml_id'] . ':1"]/cbe:described-by/product:reference-to-atomic-product-offering/product:atomic-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id is invalid with previous version of contract.',
                        ];
                        $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                        $has_error = true;
                        // 'PMS ID "'.$pms_id.'" - Product Specification should not be changed from previous version'
                    }

                    //compare the line itmes and through the errors
                    //$this->compareLineItems($xml_line_item, $contract_line_item);
                    foreach ($xml_line_item['ContractLineProductPrice'] as $key => $ContractLineProductPrice) {
                        unset($xml_line_item['ContractLineProductPrice'][$key]['xml_data']['charge_type']['tag_value']);
                        unset($xml_line_item['ContractLineProductPrice'][$key]['xml_data']['charge_period']['tag_value']);
                    }
                    foreach ($xml_line_item['ContractLineProductConfiguration'] as $key => $ContractLineProductPrice) {
                        unset($xml_line_item['ContractLineProductConfiguration'][$key]['is_new_record']);
                    }
                    $contract_line_item['ContractLineProductConfiguration'] = array_values($contract_line_item['ContractLineProductConfiguration']);
                    $contract_line_item['ContractLineProductRule']          = array_values($contract_line_item['ContractLineProductRule']);
                    $contract_line_item['ContractLineProductPrice']         = array_values($contract_line_item['ContractLineProductPrice']);

                    $diff_in_request_line_item = $this->mod_utils->array_diff_assoc_recursive($xml_line_item, $contract_line_item);
                    //Interchange the array and compare here to fetch the changes from another array if it is needed in future
                    $diff_in_avail_line_item = $this->mod_utils->array_diff_assoc_recursive($contract_line_item, $xml_line_item);

                    if (isset($diff_in_avail_line_item) && !empty($diff_in_avail_line_item)) {
                        $validation_parts = array_keys($diff_in_avail_line_item);
                        foreach ($validation_parts as $validationpart) {
                            switch ($validationpart) {
                                case 'ContractLineItemHeader':
                                    break;
                                case 'InvoiceGroupDtls':
                                    foreach ($diff_in_avail_line_item[$validationpart] as $key => $val) {
                                        $tmpkey = str_replace('_xml_id', '', $key);
                                        if (strpos($key, '_xml_id') !== false && !isset($diff_in_avail_line_item[$validationpart][$tmpkey])) {
                                            unset($diff_in_avail_line_item[$validationpart][$key]);
                                        }
                                        $tmp2key = str_replace('_timezone', '', $key);
                                        if (strpos($key, '_timezone') !== false && !isset($diff_in_avail_line_item[$validationpart][$tmp2key])) {
                                            unset($diff_in_avail_line_item[$validationpart][$key]);
                                        }
                                    }
                                    if (isset($diff_in_avail_line_item[$validationpart]['payment_type'])) {
                                        unset($diff_in_avail_line_item[$validationpart]['payment_type']);
                                    }
                                    if (empty($diff_in_avail_line_item[$validationpart])) {
                                        unset($diff_in_avail_line_item[$validationpart]);
                                    }

                                    break;
                                case 'ContractLineProductConfiguration':
                                    foreach ($diff_in_avail_line_item[$validationpart] as $key => $prod_configs) {
                                        if ($prod_configs['characteristic_value'] == '') {
                                            unset($diff_in_avail_line_item[$validationpart][$key]);
                                        }
                                    }
                                    if (empty($diff_in_avail_line_item[$validationpart])) {
                                        unset($diff_in_avail_line_item[$validationpart]);
                                    }

                                    break;
                                case 'ContractLineProductRule':
                                    foreach ($diff_in_avail_line_item[$validationpart] as $key => $prod_rule) {
                                        if ($prod_rule['rule_value'] == '') {
                                            unset($diff_in_avail_line_item[$validationpart][$key]);
                                        }
                                    }
                                    if (empty($diff_in_avail_line_item[$validationpart])) {
                                        unset($diff_in_avail_line_item[$validationpart]);
                                    }

                                    break;
                                case 'ContractLineProductPrice':
                                    if (empty($diff_in_avail_line_item[$validationpart])) {
                                        unset($diff_in_avail_line_item[$validationpart]);
                                    }

                                    break;
                                case 'ContractDirectDeposit':
                                    break;
                                case 'ContractPostalTransfer':
                                    break;
                                case 'ContractAccountTransfer':
                                    break;
                                case 'ContractCreditCard':
                                    unset($diff_in_avail_line_item[$validationpart]);
                                    break;
                            }
                        }
                    }

                    //Removing the default expected difference
                    if (isset($diff_in_request_line_item['is_new_line_item'])) {
                        unset($diff_in_request_line_item['is_new_line_item']);
                    }
                    if (isset($diff_in_request_line_item['standard_state'])) {
                        unset($diff_in_request_line_item['standard_state']);
                    }
                    if (isset($diff_in_request_line_item['object_id'])) {
                        unset($diff_in_request_line_item['object_id']);
                    }
                    if (isset($diff_in_request_line_item['object_version'])) {
                        unset($diff_in_request_line_item['object_version']);
                    }
                    if (isset($diff_in_request_line_item['ContractLineItems'])) {
                        unset($diff_in_request_line_item['ContractLineItems']);
                    }
                    if (isset($diff_in_request_line_item['ContractLineItemHeader'])) {
                        unset($diff_in_request_line_item['ContractLineItemHeader']['bi_action']);
                        //exclude payment type check for bi-action
                        if (isset($diff_in_request_line_item['ContractLineItemHeader']['payment_type'])) {
                            unset($diff_in_request_line_item['ContractLineItemHeader']['payment_type']);
                        }
                        //if the array is empty after removing bi-action then unset the ContractLineItemHeader also
                        $diff_li_header = array_filter($diff_in_request_line_item['ContractLineItemHeader']);
                        if (empty($diff_li_header)) {
                            unset($diff_in_request_line_item['ContractLineItemHeader']);
                        }
                    }
                    if (isset($diff_in_request_line_item['ContractLineProductPrice'])) {
                        //Remove the is_new_record from all price array
                        if (isset($diff_in_request_line_item['ContractLineProductPrice']) && !empty($diff_in_request_line_item['ContractLineProductPrice'])) {
                            foreach (array_keys($diff_in_request_line_item['ContractLineProductPrice']) as $key) {
                                unset($diff_in_request_line_item['ContractLineProductPrice'][$key]['is_new_record']);
                            }
                        }
                        //if the array is empty after removing bi-action then unset the ContractLineProductPrice also
                        $diff_li_item = array_filter($diff_in_request_line_item['ContractLineProductPrice']);
                        if (empty($diff_li_item)) {
                            unset($diff_in_request_line_item['ContractLineProductPrice']);
                        }
                    }
                    //Sprint 27 exclue payment type for bi-action check
                    if (isset($diff_in_request_line_item['ContractDirectDeposit'])) {
                        unset($diff_in_request_line_item['ContractDirectDeposit']);
                    }
                    if (isset($diff_in_request_line_item['ContractPostalTransfer'])) {
                        unset($diff_in_request_line_item['ContractPostalTransfer']);
                    }
                    if (isset($diff_in_request_line_item['ContractAccountTransfer'])) {
                        unset($diff_in_request_line_item['ContractAccountTransfer']);
                    }
                    if (isset($diff_in_request_line_item['ContractCreditCard'])) {
                        unset($diff_in_request_line_item['ContractCreditCard']);
                    }

                    //Remove the empty values which means those tags are not present in the xml
                    $diff_in_request_line_item = array_filter(array_map('array_filter', $diff_in_request_line_item));

                    //This array will be having only changes or newly added fields in request array
                    if (isset($diff_in_request_line_item) && !empty($diff_in_request_line_item)) {
                        $validation_parts = array_keys($diff_in_request_line_item);
                        require_once 'custom/include/contractFieldNames.php';
                        $this->CONTRACT_FIELD_NAMES = $CONTRACT_FIELD_NAMES;
                        foreach ($validation_parts as $validationpart) {
                            switch ($validationpart) {
                                case 'ContractLineItemHeader':
                                    break;
                                case 'InvoiceGroupDtls':
                                    $has_error_invoice_group = false;
                                    foreach ($diff_in_request_line_item[$validationpart] as $key => $value) {
                                        if ($req_bi_action == self::LI_NO_CHANGE || $req_bi_action == self::LI_REMOVE) {
                                            $has_error_invoice_group = true;
                                            break;
                                            // 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES[$validationpart][$key].' should not be changed';
                                        } else {
                                            if ($this->mod_utils->validateUUID($value) && array_key_exists($key, $this->LI_MANDATORY_VERSIONUP_FIELDS[$validationpart])) {
                                                $has_error_invoice_group = true;
                                                break;
                                                //$this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES[$validationpart][$key].' should not be changed');
                                            }
                                        }
                                    }
                                    break;
                                case 'ContractLineProductConfiguration':
                                    break;
                                case 'ContractLineProductRule':
                                    $has_error_contract_line_product_rule = false;
                                    foreach ($diff_in_request_line_item[$validationpart] as $key => $product_cofigs) {
                                        foreach ($product_cofigs as $key1 => $product_config) {
                                            if ($key1 == 'is_new_record') {
                                                unset($diff_in_request_line_item[$validationpart][$key][$key1]);
                                                continue;
                                            }
                                            if ($req_bi_action == self::LI_NO_CHANGE || $req_bi_action == self::LI_REMOVE) {
                                                $has_error_contract_line_product_rule = true;
                                                break;
                                                //$this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES[$validationpart][$key1].' should not be changed');
                                            } else {
                                                if ($this->mod_utils->validateUUID($product_config) && array_key_exists($key1, $this->LI_MANDATORY_VERSIONUP_FIELDS[$validationpart])) {
                                                    $has_error_contract_line_product_rule = true;
                                                    break;
                                                    //$this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES[$validationpart][$key1].' should not be changed');

                                                }

                                            }

                                        }
                                        if (empty($diff_in_request_line_item[$validationpart][$key])) {
                                            unset($diff_in_request_line_item[$validationpart][$key]);
                                        }
                                        if($has_error_contract_line_product_rule) {
                                            break;
                                        }
                                    }
                                    if (empty($diff_in_request_line_item[$validationpart])) {
                                        unset($diff_in_request_line_item[$validationpart]);
                                    }
                                    break;
                                case 'ContractLineProductPrice':
                                    $has_error_contract_lineProduct_price = false;
                                    foreach ($diff_in_request_line_item['ContractLineProductPrice'] as $key => $product_prices) {
                                        foreach ($product_prices as $key1 => $product_price) {
                                            if ($req_bi_action == self::LI_NO_CHANGE || $req_bi_action == self::LI_REMOVE) {
                                                if ($key1 != 'xml_data') {
                                                    $has_error_contract_lineProduct_price = true;
                                                    break;
                                                    // $this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES['ContractLineProductPrice'][$key1].' should not be changed');
                                                } else {
                                                    foreach ($product_price as $pkey => $pval) {
                                                        foreach ($pval as $pckey => $pcval) {
                                                            $has_error_contract_lineProduct_price = true;
                                                            break;
                                                            //$this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES['ContractLineProductPrice'][$pkey][$pckey].' should not be changed');
                                                        }

                                                    }
                                                }
                                            } else {
                                                if ($key1 != 'xml_data' && $this->mod_utils->validateUUID($product_price) && array_key_exists($key1, $this->LI_MANDATORY_VERSIONUP_FIELDS['ContractLineProductPrice'])) {
                                                    $has_error_contract_lineProduct_price = true;
                                                    break;
                                                    //$this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES['ContractLineProductPrice'][$key].' should not be changed');

                                                } else {
                                                    foreach ($product_price as $pkey => $paval) {
                                                        foreach ($paval as $pckey => $pcval) {
                                                            if ($this->mod_utils->validateUUID($pcval) && isset($this->LI_MANDATORY_VERSIONUP_FIELDS['ContractLineProductPrice']['xml_data'][$pkey][$pckey])) {
                                                                $has_error_contract_lineProduct_price = true;
                                                                break;
                                                                //$this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES['ContractLineProductPrice'][$pkey][$pckey].' should not be changed');
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    break;
                            }

                        }
                    }

                    //IF the bi-action is 'change' then there should be atlease one change
                    if (trim($req_bi_action) == self::LI_CHANGE) {
                        if (empty($diff_in_request_line_item) && empty($diff_in_avail_line_item)) {
                            $xpath_ids            = [];
                            $custom_error_details = [
                                'details' => '/contract-info:contract-information/bi:product-biitems/bi:product-biitem[@id="' . $xml_lint_item_xpath_id . ':1"]/cbe:identified-by/bi:product-biitem-key/cbe:key-type-for-cbe-standard/cbe:object-id didn\'t have any change.',
                            ];
                            $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                            $has_error = true;
                            // 'PMS ID "'.$pms_id.'" - There should be some change if bi-action is \'change\''
                        }
                    }

                    //IF the bi-action is 'no change' or 'remove' then there should not be any change
                    if (trim($req_bi_action) == self::LI_NO_CHANGE || trim($req_bi_action) == self::LI_REMOVE) {
                        if (!empty($diff_in_request_line_item) && !empty($diff_in_avail_line_item)) {
                            $xpath_ids            = [];
                            $custom_error_details = [
                                'details' => '/contract-info:contract-information/bi:product-biitems/bi:product-biitem[@id="' . $xml_lint_item_xpath_id . ':1"]/cbe:identified-by/bi:product-biitem-key/cbe:key-type-for-cbe-standard/cbe:object-id shouldn\'t have any change.',
                            ];
                            $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                            $has_error = true;
                            // 'PMS ID "'.$pms_id.'" - There should not be any change if bi-action is \'no change\' or \'remove\''
                        } elseif ($has_error_contract_line_product_rule || $has_error_contract_line_product_rule || $has_error_invoice_group) {
                            $xpath_ids            = [];
                            $custom_error_details = [
                                'details' => '/contract-info:contract-information/bi:product-biitems/bi:product-biitem[@id="' . $xml_lint_item_xpath_id . ':1"]/cbe:identified-by/bi:product-biitem-key/cbe:key-type-for-cbe-standard/cbe:object-id shouldn\'t have any change.',
                            ];
                            $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                            $has_error = true;
                        }
                    }
                }
                //unset the found line item from previous ids. At final, if any line itme found in previous version, then it means some line item is missing
                unset($prev_line_items_ids_arr[$xml_lint_item_xpath_id]);
            }

            //Check if at lease one base line item available without removing it
            if ($are_all_base_li_removed && $count_base_li == 1) {
                $xpath_ids = [];
                $custom_error_details = [
                    'details' => '/contract-info:contract-information/bi:product-biitems/bi:product-biitem/bi:action should not be in "remove" state for all base atomic product.',
                ];
                $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                $has_error = true;
                // 'Cannot remove all base biitem. Atleast one base biitem should be exist.'
            }

            //Check if at lease one line item available without removing it
            if ($are_all_li_removed) {
                $xpath_ids            = [];
                $custom_error_details = [
                    'details' => 'bi:action should not be in "remove" state for all /bi:product-biitems.',
                ];
                $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                $has_error = true;
                // 'Cannot remove all biitem. Atleast one biitem should be exist.'
            }

            //Check if existing line item is missing
            if (!empty($prev_line_items_ids_arr)) {
                foreach ($prev_line_items_ids_arr as $prev_line_items_ids) {
                    $xpath_ids            = [];
                    $custom_error_details = [
                        'details' => '('.$prev_line_items_ids.') in /contract-info:contract-information/bi:product-biitems/bi:product-biitem[@id="'.$prev_line_items_ids.':1"]/cbe:identified-by/bi:product-biitem-key/cbe:key-type-for-cbe-standard/cbe:object-id is mandatory in version up request.',
                    ];
                    $this->addBusinessValidationErrors($xpath_ids, $custom_error_details);
                    $has_error = true;
                    // 'Existing line item(s) missing. ('.implode(',',$prev_line_items_ids_arr).')'
                }
            }
        }

        if ($has_error) {
            return false;
        }

        return true;
    }

    /**
     * If there is no change in the version up request
     *
     * @return boolean
     *
     * @author dinesh.itkar
     * @since Sept 11, 2017
     */
    public function versionUpBusinessInvalidNoChange()
    {
        //IF CCID is set check whether it is changed or not
        if (isset($this->request_data['Accounts']['Contract']['CorporateDetails']['common_customer_id_c'])) {
            $contract_customer_id_c              = !empty($this->contract_data['contract_customer_id']) ? $this->contract_data['contract_customer_id'] : '';
            if (trim($contract_customer_id_c) != trim($this->request_data['Accounts']['Contract']['CorporateDetails']['common_customer_id_c'])) {
                $this->is_contract_changed = true;
            }
        }

        $contract_end_date_xml               = !empty($this->request_data['ContractDetails']['ContractsHeader']['contract_enddate']) ? $this->request_data['ContractDetails']['ContractsHeader']['contract_enddate'] : '';
        $contract_end_date_tz_xml            = !empty($this->request_data['ContractDetails']['ContractsHeader']['contract_enddate_timezone']) ? $this->request_data['ContractDetails']['ContractsHeader']['contract_enddate_timezone'] : '';
        $previous_contract_end_date_utc      = !empty($this->contract_data['header']->contract_enddate) ? $this->contract_data['header']->contract_enddate : '';
        $previous_contract_end_date_timezone = !empty($this->contract_data['header']->contract_enddate_timezone) ? $this->contract_data['header']->contract_enddate_timezone : '';
        if ((strtotime($contract_end_date_xml) != strtotime($previous_contract_end_date_utc)) || (trim($contract_end_date_tz_xml) != trim($previous_contract_end_date_timezone))) {
            $this->is_contract_changed = true;
        }

        if ($this->is_contract_changed == false) {
            $global_contract_id_uuid_xml = $this->request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
            $xpath_ids                   = array();
            $this->addBusinessValidationErrors($xpath_ids);
            return false;
        }

        return true;
    }
}
