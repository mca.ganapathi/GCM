<?php

trait PostPreValidationLayer
{
	/**
     * invalid session validate in POST request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function postPreInvalidSession()
    {
        // check session validation
        $success = $this->invalidSession();
        if ($success == false) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * XML empty validation in post request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function postPreMandatoryXmlValidation()
    {
        if (empty($this->request_data)) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * XSD validation in post request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function postPreInvalidXsdValidation()
    {
        // check XSD validation
        $has_error = ApiValidationHelper::xsdValidation($this->request_data);
        if ($has_error) {
            $custom_error_details = [
                'details' => $has_error,
            ];
            // add error in error list
            $this->addPreValidationErrors([], $custom_error_details);
            return false;
        }
        return true;
    }

    /**
     * [3003] CIDAS connection validation in post request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function postPreInvalidCidasConnectionFailed()
    {
        $error_code = '3003';
        $has_error  = ApiValidationHelper::cidasConnection($this->request_data, $error_code);
        if ($has_error == false) {
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * [3005] CIDAS connection validation in post request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 21, 2017
     */
    public function postPreInvalidCidasConnectionZeroRecord()
    {
        $error_code = '3005';
        $has_error  = ApiValidationHelper::cidasConnection($this->request_data, $error_code);
        if ($has_error == false) {
            // cidas_contract_id | cidas_billing_id | cidas_technology_id
            $xpath_ids = [
                'cidas_id' => $this->request_data['cidas_id'],
                'global_contract_xml_id' => $this->request_data['global_contract_xml_id'],
            ];
            if($this->request_data['account_type'] == 'contract') {
                $xpath_ids['account_type_xpath'] = 'contract-customer';
            }
            if($this->request_data['account_type'] == 'billing') {
                $xpath_ids['account_type_xpath'] = 'billing-customer';
            }
            if($this->request_data['account_type'] == 'technology') {
                $xpath_ids['account_type_xpath'] = 'customer-technical-representative';
            }
            $this->addPreValidationErrors($xpath_ids);
            return false;
        }
        return true;
    }

    /**
     * [E001] CIDAS connection validation in post request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function postPreInvalidCidasConnectionE001()
    {
        $error_code = 'E001';
        // check [E001] CIDAS connection validation
        $has_error = ApiValidationHelper::cidasConnection($this->request_data, $error_code);
        if ($has_error == false) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * [E002] CIDAS connection validation in post request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function postPreInvalidCidasConnectionE002()
    {
        $error_code = 'E002';
        // check [E002] CIDAS connection validation
        $has_error = ApiValidationHelper::cidasConnection($this->request_data, $error_code);
        if ($has_error == false) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * [E003] CIDAS connection validation in post request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function postPreInvalidCidasConnectionE003()
    {
        $error_code = 'E003';
        // check [E003] CIDAS connection validation
        $has_error = ApiValidationHelper::cidasConnection($this->request_data, $error_code);
        if ($has_error == false) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * [E004] CIDAS connection validation in post request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function postPreInvalidCidasConnectionE004()
    {
        $error_code = 'E004';
        // check [E004] CIDAS connection validation
        $has_error = ApiValidationHelper::cidasConnection($this->request_data, $error_code);
        if ($has_error == false) {
            // add error in error list
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    }

    /**
     * PMS connection validation in post request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function postPreInvalidPmsConnection()
    {
        // check pms connection
        $success = ApiValidationHelper::pmsConnection();
        // has validation error
        if ($success == false) {
            $this->addPreValidationErrors([]);
            return false;
        }
        return true;
    } 
}