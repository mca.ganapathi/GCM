<?php

trait VersionUpPreValidationLayer
{
	/**
     * invalid session validate in VERSION_UP request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function versionUpPreInvalidSession()
    {
        return $this->postPreInvalidSession();
    }

    /**
     * XML empty validation in version_up request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function versionUpPreMandatoryXmlValidation()
    {
        return $this->postPreMandatoryXmlValidation();
    }

    /**
     * XSD validation in version_up request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function versionUpPreInvalidXsdValidation()
    {
        return $this->postPreInvalidXsdValidation();
    }

    /**
     * [3003] CIDAS connection validation in version_up request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function versionUpPreInvalidCidasConnectionFailed()
    {
        return $this->postPreInvalidCidasConnectionFailed();
    }

    /**
     * [3005] CIDAS connection validation in version_up request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function versionUpPreInvalidCidasConnectionZeroRecord()
    {
        return $this->postPreInvalidCidasConnectionZeroRecord();
    }

    /**
     * [E001] CIDAS connection validation in version_up request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function versionUpPreInvalidCidasConnectionE001()
    {
        return $this->postPreInvalidCidasConnectionE001();
    }

    /**
     * [E002] CIDAS connection validation in version_up request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function versionUpPreInvalidCidasConnectionE002()
    {
        return $this->postPreInvalidCidasConnectionE002();
    }

    /**
     * [E003] CIDAS connection validation in version_up request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function versionUpPreInvalidCidasConnectionE003()
    {
        return $this->postPreInvalidCidasConnectionE003();
    }

    /**
     * [E004] CIDAS connection validation in version_up request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function versionUpPreInvalidCidasConnectionE004()
    {
        return $this->postPreInvalidCidasConnectionE004();
    } 

    /**
     * PMS connection validation in version_up request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function versionUpPreInvalidPmsConnection()
    {
        return $this->postPreInvalidPmsConnection();
    }
}