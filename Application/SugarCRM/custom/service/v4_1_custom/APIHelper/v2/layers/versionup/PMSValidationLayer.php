<?php

trait VersionUpPMSValidationLayer
{
	/**
     * mandatory validation for product offering id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 13, 2017
     */
    public function versionUpPmsMandatoryProductOfferingId()
    {
        return $this->postPmsMandatoryProductOfferingId();
    }

    /**
     * invalid validation for product offering id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 13, 2017
     */
    public function versionUpPmsInvalidProductOfferingId()
    {
        $success = true;
        // check product_offering_id exist in pms list
        if (!array_key_exists($this->xml_product_offering_id, $this->pms_offering_list)) {
            $success = false;
        }
        // has validation error
        if ($success == false) {
            $xpath_ids = [
                'product_offering_xml_id' => $this->xml_product_offering_xml_id,
                'product_offering_id'     => ApiValidationHelper::formatPMSID($this->xml_product_offering_id, 1),
            ];
            $this->addPmsValidationErrors($xpath_ids);
            return false;
        }

        return true;
    }

    /**
     * [PMS] product offering details not empty for specific product offering id in pms.
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 13, 2017
     */
    public function versionUpPmsMandatoryProductOfferingDetails()
    {
        return $this->postPmsMandatoryProductOfferingDetails();
    }

    /**
     * mandatory validation for product specification id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 13, 2017
     */
    public function versionUpPmsMandatoryProductSpecificationId()
    {
        return $this->postPmsMandatoryProductSpecificationId();
    }

    /**
     * invalid validation for product specification id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 13, 2017
     */
    public function versionUpPmsInvalidProductSpecificationId()
    {
        return $this->postPmsInvalidProductSpecificationId();
    }

    /**
     * invalid validation for product specification id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 13, 2017
     */
    public function versionUpPmsMandatoryProductSpecificationBase()
    {
        return $this->postPmsMandatoryProductSpecificationBase();
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsMandatoryProductPriceChargeType($xml_specification_id, $product_price_id)
    {
        $success = true;

        if (empty($this->xml_charge_type)) {
            $xml_price_xpath_id = $this->xml_price_xpath_id;
            $xpath_ids          = [
                'product_price_xml_id' => $xml_price_xpath_id,
                'product_price_id'     => ApiValidationHelper::formatPMSID($product_price_id, 1),
            ];
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsInvalidProductPriceChargeType($xml_specification_id, $product_price_id)
    {
        $success                  = true;
        $xml_price_xpath_id       = $this->xml_price_xpath_id;
        $xml_charge_type_xpath_id = $this->xml_charge_type_xpath_id;
        $xml_charge_type          = $this->xml_charge_type;
        if (!empty($this->xml_charge_type_tag)) {
            $xml_charge_type = $this->xml_charge_type_tag;
        }
        $xpath_ids = [
            'product_charge_type_xml_id' => $xml_charge_type_xpath_id,
            'product_charge_type_value'  => $xml_charge_type,
            'product_price_xml_id'       => $xml_price_xpath_id,
            'product_price_id'           => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if (!empty($this->xml_charge_type) && $this->pms_charge_type !== $this->xml_charge_type) {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsMandatoryProductPriceChargePeriod($xml_specification_id, $product_price_id)
    {
        $success = true;
        $xml_price_xpath_id = $this->xml_price_xpath_id;

        if (empty($this->xml_charge_period)) {
            $xpath_ids = [
                'product_price_xml_id' => $xml_price_xpath_id,
                'product_price_id'     => ApiValidationHelper::formatPMSID($product_price_id, 1),
            ];
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsInvalidProductPriceChargePeriod($xml_specification_id, $product_price_id)
    {
        $success                    = true;
        $xml_price_xpath_id         = $this->xml_price_xpath_id;
        $xml_charge_period_xpath_id = $this->xml_charge_period_xpath_id;
        $xml_charge_period          = $this->xml_charge_period;
        if (!empty($this->xml_charge_period_tag)) {
            $xml_charge_period = $this->xml_charge_period_tag;
        }
        $xpath_ids = [
            'product_charge_period_xml_id' => $this->xml_charge_period_xpath_id,
            'product_charge_period_value'  => $xml_charge_period,
            'product_price_xml_id'         => $xml_price_xpath_id,
            'product_price_id'             => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if (!empty($this->xml_charge_period) && $this->pms_charge_period !== $this->xml_charge_period) {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsMandatoryProductPriceListPrice($xml_specification_id, $product_price_id, $add_validation_only)
    {
        $success            = true;
        $xml_price_xpath_id = $this->xml_price_xpath_id;
        $xpath_ids          = [
            'product_price_xml_id' => $xml_price_xpath_id,
            'product_price_id'     => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        // adding custom validtion from price block
        if ($add_validation_only == true && $this->add_list_price_validation_only == true) {
            $this->addPmsValidationErrors($xpath_ids);
            return false;
        } elseif ($this->xml_list_price == '' && $this->pms_icb_flag != 'true' && $this->add_list_price_validation_only == false) {
            $this->addPmsValidationErrors($xpath_ids);
            $this->add_list_price_validation_only = false;
            return false;
        }

        if($success) {
            $success = $this->versionUpPmsInvalidProductPriceListPrice($xml_specification_id, $product_price_id);
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsInvalidProductPriceListPrice($xml_specification_id, $product_price_id)
    {
        $success                 = true;
        $xml_price_xpath_id      = $this->xml_price_xpath_id;
        $xml_list_price_xpath_id = $this->xml_list_price_xpath_id;
        $xml_list_price          = $this->xml_list_price;
        $xpath_ids               = [
            'product_list_price_xml_id' => $xml_list_price_xpath_id,
            'product_list_price_value'  => $xml_list_price,
            'product_price_xml_id'      => $xml_price_xpath_id,
            'product_price_id'          => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if ($this->xml_list_price != '' && (float) $this->pms_list_price !== (float) $this->xml_list_price && $this->pms_icb_flag != 'true') {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        // list price must be 16digits with dot
        elseif (!filter_var($this->xml_list_price, FILTER_VALIDATE_FLOAT) && (float) $this->xml_list_price != 0 && $this->pms_list_price != 'true') {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsMandatoryProductPriceCustomerContractPrice($xml_specification_id, $product_price_id)
    {
        $success            = true;
        $xml_price_xpath_id = $this->xml_price_xpath_id;
        $xpath_ids          = [
            'product_price_xml_id' => $xml_price_xpath_id,
            'product_price_id'     => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if ($this->xml_customer_contract_price === '') {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsInvalidProductPriceCustomerContractPrice($xml_specification_id, $product_price_id, $add_validation_only)
    {
        $success                              = true;
        $xml_price_xpath_id                   = $this->xml_price_xpath_id;
        $xml_customer_contract_price_xpath_id = $this->xml_customer_contract_price_xpath_id;
        $xml_customer_contract_price          = $this->xml_customer_contract_price;
        $xpath_ids                            = [
            'product_customer_contract_price_xml_id' => $xml_customer_contract_price_xpath_id,
            'product_customer_contract_price_value'  => $xml_customer_contract_price,
            'product_price_xml_id'                   => $xml_price_xpath_id,
            'product_price_id'                       => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];
        // adding custom validtion from price block
        if ($add_validation_only == true && $this->add_customer_contract_price_validation_only == true) {
            $this->addPmsValidationErrors($xpath_ids);
            return false;
        } elseif ($this->add_customer_contract_price_validation_only == false) {
            // customer contract price is -ve && must be 16 digits
            if ((float) $this->xml_customer_contract_price < 0) {
                $this->addPmsValidationErrors($xpath_ids);
                $this->add_customer_contract_price_validation_only = false;
                $success                                           = false;
            }
            // customer contract price must be 16digits with dot
            elseif (!filter_var($this->xml_customer_contract_price, FILTER_VALIDATE_FLOAT) && (float) $this->xml_customer_contract_price != 0) {
                $this->addPmsValidationErrors($xpath_ids);
                $this->add_customer_contract_price_validation_only = false;
                $success                                           = false;
            }
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsMandatoryProductPriceIcbFlag($xml_specification_id, $product_price_id)
    {
        $success               = true;
        $xml_price_xpath_id    = $this->xml_price_xpath_id;
        $xml_icb_flag          = $this->xml_icb_flag;
        $xpath_ids             = [
            'product_icb_flag_value'  => (int) $xml_icb_flag,
            'product_price_xml_id'    => $xml_price_xpath_id,
            'product_price_id'        => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if (empty($this->xml_icb_flag)) {
            $this->addPmsValidationErrors($xpath_ids);
            return false;
        }

        if($success) {
            $success = $this->versionUpPmsInvalidProductPriceIcbFlag($xml_specification_id, $product_price_id);
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsInvalidProductPriceIcbFlag($xml_specification_id, $product_price_id)
    {
        $success               = true;
        $xml_price_xpath_id    = $this->xml_price_xpath_id;
        $xml_icb_flag_xpath_id = $this->xml_icb_flag_xpath_id;
        $xml_icb_flag          = $this->xml_icb_flag ? 'true' : 'false';
        $xpath_ids             = [
            'product_icb_flag_xml_id' => $xml_icb_flag_xpath_id,
            'product_icb_flag_value'  => $xml_icb_flag,
            'product_price_xml_id'    => $xml_price_xpath_id,
            'product_price_id'        => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if ($this->xml_icb_flag != $this->pms_icb_flag) {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsInvalidProductPriceCurrency($xml_specification_id, $product_price_id)
    {
        $success               = true;
        $xml_price_xpath_id    = $this->xml_price_xpath_id;
        $xml_currency_xpath_id = $this->xml_price_currency_xpath_id;
        $xml_currency          = $this->xml_price_currency;
        $xpath_ids             = [
            'product_currency_xml_id' => $xml_currency_xpath_id,
            'product_currency_value'  => $xml_currency,
            'product_price_xml_id'    => $xml_price_xpath_id,
            'product_price_id'        => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if (!empty($this->xml_price_currency) && $this->xml_price_currency != $this->pms_price_currency) {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsInvalidProductPriceIgcSettlementPrice($xml_specification_id, $product_price_id)
    {
        $success                           = true;
        $xml_price_xpath_id                = $this->xml_price_xpath_id;
        $xml_igc_settlement_price_xpath_id = $this->xml_igc_settlement_price_xpath_id;
        $xml_igc_settlement_price          = $this->xml_igc_settlement_price;
        $xpath_ids                         = [
            'product_igc_settlement_price_xml_id' => $xml_igc_settlement_price_xpath_id,
            'product_igc_settlement_price_value'  => $xml_igc_settlement_price,
            'product_price_xml_id'                => $xml_price_xpath_id,
            'product_price_id'                    => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if ((float) $this->xml_igc_settlement_price < 0) {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        // IGC settlement price must be 16digits with dot
        else if (!filter_var($this->xml_igc_settlement_price, FILTER_VALIDATE_FLOAT) && (float) $this->xml_igc_settlement_price != '0') {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsInvalidProductPriceDiscountAmount($xml_specification_id, $product_price_id)
    {
        $success            = true;
        $xml_price_xpath_id = $this->xml_price_xpath_id;
        $xpath_ids          = [
            'product_price_xml_id' => $xml_price_xpath_id,
            'product_price_id'     => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];
        // if isCustomPrice true discount will not work
        if ($this->pms_icb_flag != 'true') {

            // if both discount rate & discount amount not be empty
            if ((int) $this->xml_discount_rate != 0 && (float) $this->xml_discount_amount != 0) {
                $this->versionUpPmsInvalidProductPriceDiscountShouldNotExist($xml_specification_id, $product_price_id);
                $success = false;
            }

            // discount rate must be less the 100
            else if ((float) $this->xml_discount_amount == 0 && (int) $this->xml_discount_rate > 100) {
                $add_validation_only = true;
                $this->versionUpPmsInvalidProductPriceDiscountRate($xml_specification_id, $product_price_id, $add_validation_only);
                $success = false;
            }

            // discount amount or rate is -ve
            else if ((float) $this->xml_discount_amount < 0 || (int) $this->xml_discount_rate < 0) {
                if ((float) $this->xml_discount_amount != 0) {
                    $xpath_ids['product_discount_amount_xml_id'] = $this->xml_discount_amount_xpath_id;
                    $xpath_ids['product_discount_amount_value']  = $this->xml_discount_amount;
                    $this->addPmsValidationErrors($xpath_ids);
                } else {
                    $add_validation_only = true;
                    $this->versionUpPmsInvalidProductPriceDiscountRate($xml_specification_id, $product_price_id, $add_validation_only);
                }
                $success = false;
            }

            // discount amount must be less than offering price
            else if ((int) $this->xml_discount_rate == 0 && (float) $this->xml_discount_amount > (float) $this->xml_list_price) {
                $xpath_ids['product_discount_amount_xml_id'] = $this->xml_discount_amount_xpath_id;
                $xpath_ids['product_discount_amount_value']  = $this->xml_discount_amount;
                $this->addPmsValidationErrors($xpath_ids);
                $success = false;
            }
            // ....
            else {
                // calculating desire price according to discount
                $desirePrice = 0;
                if ((int) $this->xml_discount_rate != 0) {
                    $desirePrice = ($this->xml_discount_rate / 100) * $this->xml_list_price;
                    $desirePrice = $this->xml_list_price - $desirePrice;
                } else if ((float) $this->xml_discount_amount != 0) {
                    $desirePrice = $this->xml_list_price - $this->xml_discount_amount;
                } else if ((int) $this->xml_discount_rate == 0 && (float) $this->xml_discount_amount == 0) {
                    $desirePrice = $this->xml_list_price;
                }

                // if customer contract price is empty assign value
                if ($this->xml_customer_contract_price === '') {
                    $this->xml_customer_contract_price = $this->xml_customer_contract_price = $desirePrice;
                }

                // customer contract price is valid or not for discount percent
                else if ((float) $desirePrice !== (float) $this->xml_customer_contract_price) {
                    // calling function to add proper error details from api_config
                    $add_validation_only = true;
                    $this->versionUpPmsInvalidProductPriceCustomerContractPrice($xml_specification_id, $product_price_id, $add_validation_only);
                    $success = false;
                }
            }
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Feb 08, 2018
     */
    public function versionUpPmsInvalidProductPriceDiscountRate($xml_specification_id, $product_price_id, $add_validation_only)
    {
        $success = true;
        $xml_price_xpath_id = $this->xml_price_xpath_id;
        $xml_discount_rate_xpath_id = $this->xml_discount_rate_xpath_id;
        $xml_discount_rate = $this->xml_discount_rate;

        $xpath_ids          = [
            'product_price_xml_id' => $xml_price_xpath_id,
            'product_price_id'     => ApiValidationHelper::formatPMSID($product_price_id, 1),
            'product_discount_rate_xml_id' => $xml_discount_rate_xpath_id,
            'product_discount_rate_value'  => $xml_discount_rate
        ];

        if($add_validation_only == true) {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product price Discount amount & rate should not exist
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function versionUpPmsInvalidProductPriceDiscountShouldNotExist($xml_specification_id, $product_price_id)
    {
        $xml_price_xpath_id = $this->xml_price_xpath_id;
        $xpath_ids          = [
            'product_price_xml_id'           => $xml_price_xpath_id,
            'product_price_id'               => ApiValidationHelper::formatPMSID($product_price_id, 1),
            'product_discount_rate_xml_id'   => $this->xml_discount_rate_xpath_id,
            'product_discount_rate_value'    => $this->xml_discount_rate,
            'product_discount_amount_xml_id' => $this->xml_discount_amount_xpath_id,
            'product_discount_amount_value'  => $this->xml_discount_amount,
        ];
        $this->addPmsValidationErrors($xpath_ids);

        return false;
    }
}