<?php

trait GbsPreResponseValidationLayer
{
	/**
     * validate response xml in GBS request
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 25, 2017
     */
    public function gbsPreResponseInvalidXmlData()
    {
        // check xml validation
        $has_error = ApiValidationHelper::xsdValidation($this->request_data);
        if ($has_error) {
            $custom_error_details = [
                'details' => $has_error,
            ];
            // add error in error list
            $this->addPreResponseValidationErrors([], $custom_error_details);
            return false;
        }
        return true;
    }
}