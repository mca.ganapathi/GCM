<?php

trait PostPMSValidationLayer
{
    /**
     * invalid validation for product offering id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 13, 2017
     */
    public function postPmsInvalidProductOfferingId()
    {
        $success = true;
        // check product_offering_id exist in pms list
        if (!array_key_exists($this->xml_product_offering_id, $this->pms_offering_list)) {
            $success = false;
        }
        // has validation error
        if ($success == false) {
            $xpath_ids = [
                'product_offering_xml_id' => $this->xml_product_offering_xml_id,
                'product_offering_id'     => ApiValidationHelper::formatPMSID($this->xml_product_offering_id, 1),
            ];
            $this->addPmsValidationErrors($xpath_ids);
            return false;
        }

        return true;
    }

    /**
     * [PMS] product offering details not empty for specific product offering id in pms.
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 13, 2017
     */
    public function postPmsMandatoryProductOfferingDetails()
    {
        $success = true;
        // check product offering details empty
        if (empty($this->pms_offering_details)) {
            $success = false;
        }
        // has validation error
        if ($success == false) {
            $xpath_ids = [
                'product_offering_xml_id' => $this->xml_product_offering_xml_id,
                'product_offering_id'     => ApiValidationHelper::formatPMSID($this->xml_product_offering_id, 1),
            ];
            $this->addPmsValidationErrors($xpath_ids);
            return false;
        }

        return true;
    }

    /**
     * mandatory validation for product specification id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 13, 2017
     */
    public function postPmsMandatoryProductSpecificationId()
    {
        $success = true;
        // line item list from post request data
        $xml_line_items = !empty($this->request_data['ContractDetails']['ContractLineItemDetails']) ? $this->request_data['ContractDetails']['ContractLineItemDetails'] : array();
        if (!empty($xml_line_items)) {
            foreach ($xml_line_items as $line_item) {
                $has_specification_id = true;
                // product specification id from post request data
                $xml_specification_id = !empty($line_item['ContractLineItemHeader']['product_specification']) ? $line_item['ContractLineItemHeader']['product_specification'] : '';
                // check product_specification_id empty
                if (empty($xml_specification_id)) {
                    $has_specification_id = false;
                }
                // has validation error
                if ($has_specification_id == false) {
                    $xpath_ids = [
                        'product_specification_id' => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                        'product_offering_xml_id'  => $this->xml_product_offering_xml_id,
                        'product_offering_id'      => ApiValidationHelper::formatPMSID($this->xml_product_offering_id, 1),
                    ];
                    $this->addPmsValidationErrors($xpath_ids);
                    $success = false;
                }
            }
        }

        return $success;
    }

    /**
     * invalid validation for product specification id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 13, 2017
     */
    public function postPmsInvalidProductSpecificationId()
    {
        $success = true;
        // line item list from post request data
        $xml_line_items = !empty($this->request_data['ContractDetails']['ContractLineItemDetails']) ? $this->request_data['ContractDetails']['ContractLineItemDetails'] : array();
        if (!empty($xml_line_items)) {
            foreach ($xml_line_items as $line_item) {
                $valid_specification_id = true;
                // product specification id from post request data
                $xml_specification_id     = !empty($line_item['ContractLineItemHeader']['product_specification']) ? $line_item['ContractLineItemHeader']['product_specification'] : '';
                $xml_specification_xml_id = !empty($line_item['ContractLineItemHeader']['product_specification_xml_id']) ? $line_item['ContractLineItemHeader']['product_specification_xml_id'] : '';
                // check product_specification_id empty
                if (!array_key_exists($xml_specification_id, $this->pms_specification_details)) {
                    $valid_specification_id = false;
                }
                // has validation error
                if ($valid_specification_id == false) {
                    $xpath_ids = [
                        'product_specification_xml_id' => $xml_specification_xml_id,
                        'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                        'product_offering_xml_id'      => $this->xml_product_offering_xml_id,
                        'product_offering_id'          => ApiValidationHelper::formatPMSID($this->xml_product_offering_id, 1),
                    ];
                    $this->addPmsValidationErrors($xpath_ids);
                    $success = false;
                }
            }
        }

        return $success;
    }

    /**
     * invalid validation for product specification id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 13, 2017
     */
    public function postPmsMandatoryProductSpecificationBase()
    {
        $has_base                 = false;
        $validateCharacteristics  = true;
        $validate_characteristics = true;
        $validate_rules           = true;
        $validate_price           = true;
        // line item list from post request data
        $xml_line_items = !empty($this->request_data['ContractDetails']['ContractLineItemDetails']) ? $this->request_data['ContractDetails']['ContractLineItemDetails'] : array();
        if (!empty($xml_line_items)) {
            foreach ($xml_line_items as $line_item) {
                // product specification id from post request data
                $xml_specification_id = !empty($line_item['ContractLineItemHeader']['product_specification']) ? $line_item['ContractLineItemHeader']['product_specification'] : '';
                if ($this->pms_specification_details[$xml_specification_id]['spectypeid'] == '1') {
                    $has_base = true;
                }
            }
        }
        // has validation error
        if ($has_base == false) {
            $this->addPmsValidationErrors([]);
            return false;
        }
        // manual validation for validateCharacteristics, validateRules, validatePrice to avoid code repetation
        if (!empty($xml_line_items)) {
            foreach ($xml_line_items as $line_item_index => $line_item) {
                // flag to check HTTP request is POST or VERSION UP
                $this->is_new_registration = isset($line_item['is_new_record']) ? $line_item['is_new_record'] : '';
                // product specification id from post request data
                $this->xml_specification_id = !empty($line_item['ContractLineItemHeader']['product_specification']) ? $line_item['ContractLineItemHeader']['product_specification'] : '';
                // product specification xml id from post request data
                $this->xml_specification_xml_id = !empty($line_item['ContractLineItemHeader']['product_specification_xml_id']) ? $line_item['ContractLineItemHeader']['product_specification_xml_id'] : '';
                // product contract currency from post request data
                $this->xml_contract_currency = !empty($line_item['ContractLineItemHeader']['cust_contract_currency']) ? $line_item['ContractLineItemHeader']['cust_contract_currency'] : '';
                // validateCharacteristics
                if ($this->postPmsMandatoryProductConfigurationId($this->xml_specification_id, $line_item_index) == false) {
                    $validate_characteristics = false;
                }
                // validateRules
                if ($this->postPmsMandatoryProductRuleId($this->xml_specification_id, $line_item_index) == false) {
                    $validate_rules = false;
                }
                // validatePrice
                if ($this->postPmsMandatoryProductPriceId($this->xml_specification_id, $line_item_index, $this->xml_contract_currency) == false) {
                    $validate_price = false;
                }
            }
        }

        if ($validate_characteristics == false || $validate_rules == false || $validate_price == false) {
            return false;
        }

        $data = [
            'request_data'              => $this->request_data,
            'xml_new_configuration_ids' => $this->xml_new_configuration_ids,
            'xml_new_rule_ids'          => $this->xml_new_rule_ids,
            'xml_price_ids'             => $this->xml_price_ids,
        ];
        // reconstract xml array to add missing configuration, rules, price
        $this->request_data = ApiValidationHelper::reconstractRequestDataAfterPmsValidationSuccess($data);

        return true;
    }

    /**
     * mandatory validation for product configuration id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsMandatoryProductConfigurationId($xml_specification_id, $line_item_index)
    {
        $success = true;
        $valid   = true;
        // populating from before middleware [post_before_mandatory_product_configuration_id]
        $xml_configuration = !empty($this->xml_characteristic_ids[$xml_specification_id."_".$line_item_index]) ? $this->xml_characteristic_ids[$xml_specification_id."_".$line_item_index] : array();
        $pms_configuration = !empty($this->pms_specification_details[$xml_specification_id]['characteristics']) ? $this->pms_specification_details[$xml_specification_id]['characteristics'] : array();
        if (!empty($pms_configuration)) {
            foreach ($pms_configuration as $pms_configuration_key => $pms_configuration_details) {
                if ($pms_configuration_details['ismandatory'] == 'true' && !array_key_exists($pms_configuration_key, $xml_configuration)) {
                    $xpath_ids = [
                        'pms_product_configuration_id' => ApiValidationHelper::formatPMSID($pms_configuration_key, 1),
                        'product_specification_xml_id' => $this->xml_specification_xml_id,
                        'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                    ];
                    $this->addPmsValidationErrors($xpath_ids);
                    $success = false;
                }
            }
        }
        // validate Invalid Configuration Id
        $valid = $this->postPmsInvalidProductConfigurationId($xml_specification_id, $line_item_index);
        // has validation error
        if ($success == false || $valid == false) {
            return false;
        }
        // validate Invalid Configuration value
        $valid = $this->postPmsMandatoryProductConfigurationValue($xml_specification_id, $line_item_index);
        // has validation error
        if ($success == false || $valid == false) {
            return false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsInvalidProductConfigurationId($xml_specification_id, $line_item_index)
    {
        $success = true;
        // populating from before middleware [post_before_mandatory_product_configuration_id]
        $xml_configuration = !empty($this->xml_characteristic_ids[$xml_specification_id."_".$line_item_index]) ? $this->xml_characteristic_ids[$xml_specification_id."_".$line_item_index] : array();
        $pms_configuration = !empty($this->pms_specification_details[$xml_specification_id]['characteristics']) ? $this->pms_specification_details[$xml_specification_id]['characteristics'] : array();
        if (!empty($xml_configuration)) {
            foreach ($xml_configuration as $xml_configuration_key => $xml_configuration_details) {
                // configuration id not exist in pms validation
                if (!array_key_exists($xml_configuration_key, $pms_configuration)) {
                    $xml_configuration_xml_id = !empty($this->xml_characteristic_xml_ids[$xml_specification_id."_".$line_item_index][$xml_configuration_key]) ? $this->xml_characteristic_xml_ids[$xml_specification_id."_".$line_item_index][$xml_configuration_key] : '';
                    $xml_specification_xml_id = $this->xml_specification_xml_id;
                    $xpath_ids                = [
                        'product_configuration_xml_id' => $xml_configuration_xml_id,
                        'product_configuration_id'     => ApiValidationHelper::formatPMSID($xml_configuration_key, 1),
                        'product_specification_xml_id' => $xml_specification_xml_id,
                        'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                        'invalid_or_duplicate'         => 'invalid'
                    ];
                    $this->addPmsValidationErrors($xpath_ids);
                    $success = false;
                }
                // configuration id duplicate validation
                elseif (!empty($xml_configuration_details) && is_string($xml_configuration_details) && $xml_configuration_details == 'duplicate') {
                    $xml_configuration_xml_id = !empty($this->xml_characteristic_xml_ids[$xml_specification_id."_".$line_item_index][$xml_configuration_key]) ? $this->xml_characteristic_xml_ids[$xml_specification_id."_".$line_item_index][$xml_configuration_key] : '';
                    $xml_specification_xml_id = $this->xml_specification_xml_id;
                    $xpath_ids                = [
                        'product_configuration_xml_id' => $xml_configuration_xml_id,
                        'product_configuration_id'     => ApiValidationHelper::formatPMSID($xml_configuration_key, 1),
                        'product_specification_xml_id' => $xml_specification_xml_id,
                        'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                        'invalid_or_duplicate'         => 'duplicate'
                    ];
                    $this->addPmsValidationErrors($xpath_ids);
                    $success = false;
                }
            }
        }
        // has validation error
        if ($success == false) {
            return false;
        }

        return $success;
    }

    /**
     * mandatory validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsMandatoryProductConfigurationValue($xml_specification_id, $line_item_index)
    {
        $success                   = true;
        $valid                     = true;
        $xml_new_configuration_ids = [];
        // populating from before middleware [post_before_mandatory_product_configuration_id]
        $xml_configuration = !empty($this->xml_characteristic_ids[$xml_specification_id."_".$line_item_index]) ? $this->xml_characteristic_ids[$xml_specification_id."_".$line_item_index] : array();
        $pms_configuration = !empty($this->pms_specification_details[$xml_specification_id]['characteristics']) ? $this->pms_specification_details[$xml_specification_id]['characteristics'] : array();
        if (!empty($pms_configuration)) {
            foreach ($pms_configuration as $pms_configuration_key => $pms_configuration_details) {
                if (array_key_exists($pms_configuration_key, $xml_configuration)) {
                    if ($pms_configuration_details['ismandatory'] == 'true' && empty($xml_configuration[$pms_configuration_key])) {
                        $xml_configuration_xml_id = !empty($this->xml_characteristic_xml_ids[$xml_specification_id."_".$line_item_index][$pms_configuration_key]) ? $this->xml_characteristic_xml_ids[$xml_specification_id."_".$line_item_index][$pms_configuration_key] : '';
                        $xml_specification_xml_id = $this->xml_specification_xml_id;
                        $xpath_ids                = [
                            'product_configuration_xml_id' => $xml_configuration_xml_id,
                            'product_specification_xml_id' => $xml_specification_xml_id,
                            'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                        ];
                        $this->addPmsValidationErrors($xpath_ids);
                        $success = false;
                    }
                } else {
                    $xml_new_configuration_ids[$pms_configuration_key] = array(
                        'configuration_id'    => (string) $pms_configuration_key,
                        'configuration_value' => '',
                    );
                }
            }
        }
        // to populate missing apiCharacteristics in result xml
        $this->xml_new_configuration_ids[$xml_specification_id."_".$line_item_index] = $xml_new_configuration_ids;
        // validate Invalid Configuration Value
        $valid = $this->postPmsInvalidProductConfigurationValue($xml_specification_id, $line_item_index);
        // has validation error
        if ($success == false || $valid == false) {
            return false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsInvalidProductConfigurationValue($xml_specification_id, $line_item_index)
    {
        $success = true;
        // populating from before middleware [post_before_mandatory_product_configuration_id]
        $xml_configuration = !empty($this->xml_characteristic_ids[$xml_specification_id."_".$line_item_index]) ? $this->xml_characteristic_ids[$xml_specification_id."_".$line_item_index] : array();
        $pms_configuration = !empty($this->pms_specification_details[$xml_specification_id]['characteristics']) ? $this->pms_specification_details[$xml_specification_id]['characteristics'] : array();
        if (!empty($pms_configuration)) {
            foreach ($pms_configuration as $pms_configuration_key => $pms_configuration_details) {
                if (array_key_exists($pms_configuration_key, $xml_configuration)) {
                    // validate range field
                    if ($pms_configuration_details['valtypeid'] == '2') {
                        $valTypeRange = array();
                        // generating available configuration value list
                        for ($i = $pms_configuration_details['lowervalue']; $i <= $pms_configuration_details['uppervalue']; $i += $pms_configuration_details['interval']) {
                            $valTypeRange[] = $i;
                        }
                        // if [valtypeid] = 2; check valtypeid is empty or not
                        if (!in_array($xml_configuration[$pms_configuration_key], $valTypeRange)) {
                            $xml_configuration_xml_id = !empty($this->xml_characteristic_xml_ids[$xml_specification_id."_".$line_item_index][$pms_configuration_key]) ? $this->xml_characteristic_xml_ids[$xml_specification_id."_".$line_item_index][$pms_configuration_key] : '';
                            $xml_specification_xml_id = $this->xml_specification_xml_id;
                            $xpath_ids                = [
                                'product_configuration_xml_id' => $xml_configuration_xml_id,
                                'product_configuration_value'  => $xml_configuration[$pms_configuration_key],
                                'product_specification_xml_id' => $xml_specification_xml_id,
                                'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                            ];
                            $this->addPmsValidationErrors($xpath_ids);
                            $success = false;
                        }
                    }
                    // validate select option field
                    else if ($pms_configuration_details['valtypeid'] == '1' && !empty($pms_configuration_details['characteristicvalue'])) {
                        $valTypeRange = array();
                        // generating available characteristic value list
                        foreach ($pms_configuration_details['characteristicvalue'] as $characteristic_key => $characteristic_value) {
                            $valTypeRange[] = $characteristic_key;
                        }
                        // if [valtypeid] = 1; check valtypeid is from the value of characteristicvalue list
                        if (!in_array($xml_configuration[$pms_configuration_key], $valTypeRange)) {
                            $xml_configuration_xml_id = !empty($this->xml_characteristic_xml_ids[$xml_specification_id."_".$line_item_index][$pms_configuration_key]) ? $this->xml_characteristic_xml_ids[$xml_specification_id."_".$line_item_index][$pms_configuration_key] : '';
                            $xml_specification_xml_id = $this->xml_specification_xml_id;
                            $xpath_ids                = [
                                'product_configuration_xml_id' => $xml_configuration_xml_id,
                                'product_configuration_value'  => $xml_configuration[$pms_configuration_key],
                                'product_specification_xml_id' => $xml_specification_xml_id,
                                'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                            ];
                            $this->addPmsValidationErrors($xpath_ids);
                            $success = false;
                        }
                    }
                }
            }
        }
        // has validation error
        if ($success == false) {
            return false;
        }

        return $success;
    }

    /**
     * mandatory validation for product rule id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsMandatoryProductRuleId($xml_specification_id, $line_item_index)
    {
        $success = true;
        $valid   = true;
        // populating from before middleware [post_before_mandatory_product_configuration_id]
        $xml_rules = !empty($this->xml_rule_ids[$xml_specification_id."_".$line_item_index]) ? $this->xml_rule_ids[$xml_specification_id."_".$line_item_index] : array();
        $pms_rules = !empty($this->pms_specification_details[$xml_specification_id]['rules']) ? $this->pms_specification_details[$xml_specification_id]['rules'] : array();
        if (!empty($pms_rules)) {
            foreach ($pms_rules as $pms_rule_key => $pms_rule_details) {
                if ($pms_rule_details['ismandatory'] == 'true' && !array_key_exists($pms_rule_key, $xml_rules)) {
                    $xpath_ids = [
                        'pms_product_rule_id'          => ApiValidationHelper::formatPMSID($pms_rule_key, 2),
                        'product_specification_xml_id' => $this->xml_specification_xml_id,
                        'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                    ];
                    $this->addPmsValidationErrors($xpath_ids);
                    $success = false;
                }
            }
        }
        // // validate Invalid Rule Id
        $valid = $this->postPmsInvalidProductRuleId($xml_specification_id, $line_item_index);
        // has validation error
        if ($success == false || $valid == false) {
            return false;
        }
        // validate Invalid Rule Value
        $valid = $this->postPmsMandatoryProductRuleValue($xml_specification_id, $line_item_index);
        // has validation error
        if ($success == false || $valid == false) {
            return false;
        }

        return $success;
    }

    /**
     * invalid validation for product rule id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsInvalidProductRuleId($xml_specification_id, $line_item_index)
    {
        $success = true;
        // populating from before middleware [post_before_mandatory_product_configuration_id]
        $xml_rules = !empty($this->xml_rule_ids[$xml_specification_id."_".$line_item_index]) ? $this->xml_rule_ids[$xml_specification_id."_".$line_item_index] : array();
        $pms_rules = !empty($this->pms_specification_details[$xml_specification_id]['rules']) ? $this->pms_specification_details[$xml_specification_id]['rules'] : array();
        if (!empty($xml_rules)) {
            foreach ($xml_rules as $xml_rule_key => $xml_rule_details) {
                // rule id not exist in pms validation
                if (!array_key_exists($xml_rule_key, $pms_rules)) {
                    $xml_rule_xml_id          = $this->xml_rule_xml_ids[$xml_specification_id."_".$line_item_index][$xml_rule_key];
                    $xml_specification_xml_id = $this->xml_specification_xml_id;
                    $xpath_ids                = [
                        'product_rule_xml_id'          => $xml_rule_xml_id,
                        'product_rule_id'              => ApiValidationHelper::formatPMSID($xml_rule_key, 2),
                        'product_specification_xml_id' => $xml_specification_xml_id,
                        'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                        'invalid_or_duplicate'         => 'invalid'
                    ];
                    $this->addPmsValidationErrors($xpath_ids);
                    $success = false;
                }
                // configuration id duplicate validation
                elseif (!empty($xml_rule_details) && is_string($xml_rule_details) && $xml_rule_details == 'duplicate') {
                    $xml_rule_xml_id          = $this->xml_rule_xml_ids[$xml_specification_id."_".$line_item_index][$xml_rule_key];
                    $xml_specification_xml_id = $this->xml_specification_xml_id;
                    $xpath_ids                = [
                        'product_rule_xml_id'          => $xml_rule_xml_id,
                        'product_rule_id'              => ApiValidationHelper::formatPMSID($xml_rule_key, 2),
                        'product_specification_xml_id' => $xml_specification_xml_id,
                        'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                        'invalid_or_duplicate'         => 'duplicate'
                    ];
                    $this->addPmsValidationErrors($xpath_ids);
                    $success = false;
                }
            }
        }
        // has validation error
        if ($success == false) {
            return false;
        }

        return $success;
    }

    /**
     * mandatory validation for product rule value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsMandatoryProductRuleValue($xml_specification_id, $line_item_index)
    {
        $success          = true;
        $xml_new_rule_ids = [];
        // populating from before middleware [post_before_mandatory_product_configuration_id]
        $xml_rules = !empty($this->xml_rule_ids[$xml_specification_id."_".$line_item_index]) ? $this->xml_rule_ids[$xml_specification_id."_".$line_item_index] : array();
        $pms_rules = !empty($this->pms_specification_details[$xml_specification_id]['rules']) ? $this->pms_specification_details[$xml_specification_id]['rules'] : array();
        if (!empty($pms_rules)) {
            foreach ($pms_rules as $pms_rule_key => $pms_rule_details) {
                if (array_key_exists($pms_rule_key, $xml_rules)) {
                    if ($pms_rule_details['ismandatory'] == 'true' && empty($xml_rules[$pms_rule_key])) {
                        $xml_rule_xml_id          = $this->xml_rule_xml_ids[$xml_specification_id."_".$line_item_index][$pms_rule_key];
                        $xml_specification_xml_id = $this->xml_specification_xml_id;
                        $xpath_ids                = [
                            'product_rule_xml_id'          => $xml_rule_xml_id,
                            'product_specification_xml_id' => $xml_specification_xml_id,
                            'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                        ];
                        $this->addPmsValidationErrors($xpath_ids);
                        $success = false;
                    }
                } else {
                    $xml_new_rule_ids[$pms_rule_key] = array(
                        'rule_id'    => (string) $pms_rule_key,
                        'rule_value' => '',
                    );
                }
            }
        }
        // to populate missing apiRulesIds in result xml
        $this->xml_new_rule_ids[$xml_specification_id."_".$line_item_index] = $xml_new_rule_ids;
        // has validation error
        if ($success == false) {
            return false;
        }

        return $success;
    }

    /**
     * mandatory validation for product price id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsMandatoryProductPriceId($xml_specification_id, $line_item_index, $xml_contract_currency)
    {
        $success  = true;
        $valid    = true;
        $is_valid = true;
        // populate xml data from before middleware
        $xml_price = !empty($this->xml_product_prices[$xml_specification_id."_".$line_item_index]) ? $this->xml_product_prices[$xml_specification_id."_".$line_item_index] : array();

        // get post selected characteristic from custom layer
        $this->availableProductPriceList($xml_specification_id, $line_item_index, $xml_contract_currency);
        // start validation for each available price list for the selected product specification
        if (!empty($this->pms_charge_ids)) {
            foreach ($this->pms_charge_ids as $pms_price_key) {
                // pricelineid is empty
                if (!array_key_exists($pms_price_key, $xml_price)) {
                    $xml_specification_xml_id = $this->xml_specification_xml_id;
                    $xpath_ids                = [
                        'pms_product_price_id'         => ApiValidationHelper::formatPMSID($pms_price_key, 1),
                        'product_specification_xml_id' => $this->xml_specification_xml_id,
                        'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                    ];
                    $this->addPmsValidationErrors($xpath_ids);
                    $success = false;
                }
                // pricelineid is empty
                elseif (empty($xml_price[$pms_price_key])) {
                    $xml_specification_xml_id = $this->xml_specification_xml_id;
                    $xpath_ids                = [
                        'pms_product_price_id'         => ApiValidationHelper::formatPMSID($pms_price_key, 1),
                        'product_specification_xml_id' => $this->xml_specification_xml_id,
                        'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                    ];
                    $this->addPmsValidationErrors($xpath_ids);
                    $success = false;
                }
            }
        }
        // validate Invalid Price Id
        $valid = $this->postPmsInvalidProductPriceId($xml_specification_id, $line_item_index);
        // has validation error
        if ($success == false || $valid == false) {
            return false;
        }
        // validate Invalid Price Value
        $valid = $this->postPmsMandatoryProductPriceValue($xml_specification_id, $line_item_index);
        // has validation error
        if ($valid == false) {
            return false;
        }

        return $success;
    }

    /**
     * invalid validation for product price id
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsInvalidProductPriceId($xml_specification_id, $line_item_index)
    {
        $success   = true;
        $xml_price = !empty($this->xml_product_prices[$xml_specification_id."_".$line_item_index]) ? $this->xml_product_prices[$xml_specification_id."_".$line_item_index] : array();
        if (!empty($xml_price)) {
            foreach ($xml_price as $xml_price_key => $xml_price_details) {
                // pricelineid is exist in pms or not
                if (!in_array($xml_price_key, $this->pms_charge_ids)) {
                    $xml_price_xml_id         = $this->xml_product_xml_prices[$xml_specification_id."_".$line_item_index][$xml_price_key];
                    $xml_specification_xml_id = $this->xml_specification_xml_id;
                    $xpath_ids                = [
                        'product_price_xml_id'         => $xml_price_xml_id,
                        'product_price_id'             => ApiValidationHelper::formatPMSID($xml_price_key, 1),
                        'product_specification_xml_id' => $xml_specification_xml_id,
                        'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                        'invalid_or_duplicate'         => 'invalid'
                    ];
                    $this->addPmsValidationErrors($xpath_ids);
                    $success = false;
                }
                // configuration id duplicate validation
                elseif (!empty($xml_price_details) && is_string($xml_price_details) && $xml_price_details == 'duplicate') {
                    $xml_price_xml_id         = $this->xml_product_xml_prices[$xml_specification_id."_".$line_item_index][$xml_price_key];
                    $xml_specification_xml_id = $this->xml_specification_xml_id;
                    $xpath_ids                = [
                        'product_price_xml_id'         => $xml_price_xml_id,
                        'product_price_id'             => ApiValidationHelper::formatPMSID($xml_price_key, 1),
                        'product_specification_xml_id' => $xml_specification_xml_id,
                        'product_specification_id'     => ApiValidationHelper::formatPMSID($xml_specification_id, 1),
                        'invalid_or_duplicate'         => 'duplicate'
                    ];
                    $this->addPmsValidationErrors($xpath_ids);
                    $success = false;
                }
            }
        }
        // has validation error
        if ($success == false) {
            return false;
        }

        return $success;
    }

    /**
     * mandatory validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsMandatoryProductPriceValue($xml_specification_id, $line_item_index)
    {
        $success = true;
        $valid   = true;
        // populating from before middleware [post_before_mandatory_product_configuration_id]
        $pms_charge_mapping = !empty($this->pms_charge_mapping[$xml_specification_id]) ? $this->pms_charge_mapping[$xml_specification_id] : array();
        $pms_charge_details = !empty($this->pms_charge_details) ? $this->pms_charge_details : array();
        // xml price details
        $xml_price           = !empty($this->xml_product_prices[$xml_specification_id."_".$line_item_index]) ? $this->xml_product_prices[$xml_specification_id."_".$line_item_index] : array();
        $xml_price_xpath_ids = !empty($this->xml_product_xml_prices[$xml_specification_id."_".$line_item_index]) ? $this->xml_product_xml_prices[$xml_specification_id."_".$line_item_index] : array();

        // start validation for each available price list for the selected product specification
        if (!empty($pms_charge_mapping)) {
            foreach ($pms_charge_mapping as $mapping) {
                // pricelineid is empty
                if (in_array($mapping['pricelineid'], $this->pms_charge_ids)) {
                    $has_charge_type    = true;
                    $has_charge_period  = true;
                    $has_list_price     = true;
                    $has_contract_price = true;
                    $has_icb_flag       = true;
                    $has_currency       = true;

                    $validate_charge_type          = true;
                    $validate_charge_period        = true;
                    $validate_list_price           = true;
                    $validate_contract_price       = true;
                    $validate_currency             = true;
                    $validate_igc_settlement_price = true;
                    $validate_discount             = true;
                    $validate_icb_flag             = true;

                    $product_price_id = $mapping['pricelineid'];
                    // ..
                    $pms_price_details        = $pms_charge_details[$mapping['pricelineid']];
                    $xml_price_details        = $xml_price[$mapping['pricelineid']];
                    $this->xml_price_xpath_id = $xml_price_xpath_ids[$mapping['pricelineid']];

                    $this->add_customer_contract_price_validation_only = true;
                    $this->add_list_price_validation_only == true;
                    // pms price details
                    $this->pms_charge_type    = isset($pms_price_details['chargeid']) ? $pms_price_details['chargeid'] : '';
                    $this->pms_charge_period  = isset($pms_price_details['chargeperiodid']) ? $pms_price_details['chargeperiodid'] : '';
                    $this->pms_list_price     = isset($pms_price_details['offeringprice']) ? $pms_price_details['offeringprice'] : '';
                    $this->pms_icb_flag       = isset($pms_price_details['isCustomPrice']) ? $pms_price_details['isCustomPrice'] : '';
                    $this->pms_price_currency = isset($pms_price_details['currencyid']) ? $pms_price_details['currencyid'] : '';
                    // xml price details
                    $this->xml_is_new_record           = isset($xml_price_details['is_new_record']) ? $xml_price_details['is_new_record'] : '';
                    $this->xml_price_currency          = isset($xml_price_details['xml_data']['currencyid']['xml_value']) ? $xml_price_details['xml_data']['currencyid']['xml_value'] : '';
                    $this->xml_icb_flag                = isset($xml_price_details['xml_data']['icb_flag']['xml_value']) ? $xml_price_details['xml_data']['icb_flag']['xml_value'] : '';
                    $this->xml_charge_type             = isset($xml_price_details['xml_data']['charge_type']['xml_value']) ? $xml_price_details['xml_data']['charge_type']['xml_value'] : '';
                    $this->xml_charge_type_tag         = isset($xml_price_details['xml_data']['charge_type']['tag_value']) ? $xml_price_details['xml_data']['charge_type']['tag_value'] : '';
                    $this->xml_charge_period           = isset($xml_price_details['xml_data']['charge_period']['xml_value']) ? $xml_price_details['xml_data']['charge_period']['xml_value'] : '';
                    $this->xml_charge_period_tag       = isset($xml_price_details['xml_data']['charge_period']['tag_value']) ? $xml_price_details['xml_data']['charge_period']['tag_value'] : '';
                    $this->xml_list_price              = isset($xml_price_details['xml_data']['list_price']['xml_value']) ? $xml_price_details['xml_data']['list_price']['xml_value'] : '';
                    $this->xml_discount_rate           = isset($xml_price_details['xml_data']['discount_rate']['xml_value']) ? $xml_price_details['xml_data']['discount_rate']['xml_value'] : '';
                    $this->xml_discount_amount         = isset($xml_price_details['xml_data']['discount_amount']['xml_value']) ? $xml_price_details['xml_data']['discount_amount']['xml_value'] : '';
                    $this->xml_customer_contract_price = isset($xml_price_details['xml_data']['customer_contract_price']['xml_value']) ? $xml_price_details['xml_data']['customer_contract_price']['xml_value'] : '';
                    $this->xml_igc_settlement_price    = isset($xml_price_details['xml_data']['igc_settlement_price']['xml_value']) ? $xml_price_details['xml_data']['igc_settlement_price']['xml_value'] : '';
                    // xpath id
                    $this->xml_price_currency_xpath_id          = isset($xml_price_details['xml_data']['currencyid']['xml_id']) ? $xml_price_details['xml_data']['currencyid']['xml_id'] : '';
                    $this->xml_icb_flag_xpath_id                = isset($xml_price_details['xml_data']['icb_flag']['xml_id']) ? $xml_price_details['xml_data']['icb_flag']['xml_id'] : '';
                    $this->xml_charge_type_xpath_id             = isset($xml_price_details['xml_data']['charge_type']['xml_id']) ? $xml_price_details['xml_data']['charge_type']['xml_id'] : '';
                    $this->xml_charge_period_xpath_id           = isset($xml_price_details['xml_data']['charge_period']['xml_id']) ? $xml_price_details['xml_data']['charge_period']['xml_id'] : '';
                    $this->xml_list_price_xpath_id              = isset($xml_price_details['xml_data']['list_price']['xml_id']) ? $xml_price_details['xml_data']['list_price']['xml_id'] : '';
                    $this->xml_discount_rate_xpath_id           = isset($xml_price_details['xml_data']['discount_rate']['xml_id']) ? $xml_price_details['xml_data']['discount_rate']['xml_id'] : '';
                    $this->xml_discount_amount_xpath_id         = isset($xml_price_details['xml_data']['discount_amount']['xml_id']) ? $xml_price_details['xml_data']['discount_amount']['xml_id'] : '';
                    $this->xml_customer_contract_price_xpath_id = isset($xml_price_details['xml_data']['customer_contract_price']['xml_id']) ? $xml_price_details['xml_data']['customer_contract_price']['xml_id'] : '';
                    $this->xml_igc_settlement_price_xpath_id    = isset($xml_price_details['xml_data']['igc_settlement_price']['xml_id']) ? $xml_price_details['xml_data']['igc_settlement_price']['xml_id'] : '';

                    // if version up
                    if ($this->is_new_registration == false && $this->xml_is_new_record !== '1') {

                        // charge type empty or not
                        $has_charge_type = $this->versionUpPmsMandatoryProductPriceChargeType($xml_specification_id, $product_price_id);
                        // charge type same or not
                        $validate_charge_type = $this->versionUpPmsInvalidProductPriceChargeType($xml_specification_id, $product_price_id);
                        // charge period same or not
                        $has_charge_period = $this->versionUpPmsMandatoryProductPriceChargePeriod($xml_specification_id, $product_price_id);
                        // charge period same or not
                        $validate_charge_period = $this->versionUpPmsInvalidProductPriceChargePeriod($xml_specification_id, $product_price_id);
                        // list price must not be empty && list price same or not
                        $has_list_price = $this->versionUpPmsMandatoryProductPriceListPrice($xml_specification_id, $product_price_id, false);
                        // list price must not be empty
                        $has_contract_price = $this->versionUpPmsMandatoryProductPriceCustomerContractPrice($xml_specification_id, $product_price_id);
                        // customer contract price is -ve && must be 16 digits
                        $validate_contract_price = $this->versionUpPmsInvalidProductPriceCustomerContractPrice($xml_specification_id, $product_price_id, false);
                        // icb_flag must be empty && icb_flag must be equal to pms value
                        $has_icb_flag = $this->versionUpPmsMandatoryProductPriceIcbFlag($xml_specification_id, $product_price_id);
                        // currency id must be equal to pms value
                        $validate_currency = $this->versionUpPmsInvalidProductPriceCurrency($xml_specification_id, $product_price_id);
                        // IGC settlement price is -ve
                        $validate_igc_settlement_price = $this->versionUpPmsInvalidProductPriceIgcSettlementPrice($xml_specification_id, $product_price_id);
                        // if isCustomPrice true discount will not work
                        $validate_discount = $this->versionUpPmsInvalidProductPriceDiscountAmount($xml_specification_id, $product_price_id);
                    }
                    // if new contract registration
                    else {
                        // if charge type empty assign value from pms
                        $has_charge_type = $this->postPmsMandatoryProductPriceChargeType();
                        // charge type same or not
                        $validate_charge_type = $this->postPmsInvalidProductPriceChargeType($xml_specification_id, $product_price_id);
                        // if charge period empty assign value from pms
                        $has_charge_period = $this->postPmsMandatoryProductPriceChargePeriod();
                        // charge period same or not
                        $validate_charge_period = $this->postPmsInvalidProductPriceChargePeriod($xml_specification_id, $product_price_id);
                        // if list price empty assign value from pms
                        $has_list_price = $this->postPmsMandatoryProductPriceListPrice($xml_specification_id, $product_price_id, false);
                        // list price same or not && not float
                        $validate_list_price = $this->postPmsInvalidProductPriceListPrice($xml_specification_id, $product_price_id);
                        // customer contract price must not be empty
                        $has_contract_price = $this->postPmsMandatoryProductPriceCustomerContractPrice($xml_specification_id, $product_price_id);
                        // customer contract price is -ve
                        $validate_contract_price = $this->postPmsInvalidProductPriceCustomerContractPrice($xml_specification_id, $product_price_id, false);
                        // icb_flag empty
                        $has_icb_flag = $this->postPmsMandatoryProductPriceIcbFlag();
                        // icb_flag must be equal to pms value
                        $validate_icb_flag = $this->postPmsInvalidProductPriceIcbFlag($xml_specification_id, $product_price_id);
                        // if currency id empty assign value from pms
                        $has_currency = $this->postPmsMandatoryProductPriceCurrency();
                        // currency id must be equal to pms value
                        $validate_currency = $this->postPmsInvalidProductPriceCurrency($xml_specification_id, $product_price_id);
                        // IGC settlement price is -ve
                        $validate_igc_settlement_price = $this->postPmsInvalidProductPriceIgcSettlementPrice($xml_specification_id, $product_price_id);
                        // if isCustomPrice true discount will not work
                        $validate_discount = $this->postPmsInvalidProductPriceDiscountAmount($xml_specification_id, $product_price_id);
                    }

                    // pms price details to populate api result
                    $this->xml_price_ids[$xml_specification_id."_".$line_item_index][$mapping['pricelineid']] = array(
                        'is_new_record'                  => $this->xml_is_new_record,
                        'charge_type'                    => $this->pms_charge_type,
                        'charge_period'                  => $this->pms_charge_period,
                        'list_price'                     => (float) $this->pms_list_price,
                        'icb_flag'                       => $this->pms_icb_flag,
                        'percent_discount_flag'          => '0',
                        'discount'                       => (float) $this->xml_discount_amount,
                        'customer_contract_price'        => (float) $this->xml_customer_contract_price,
                        'igc_settlement_price'           => (float) $this->xml_igc_settlement_price,
                        'currencyid'                     => $this->pms_price_currency,

                        'charge_xml_id'                  => $this->xml_price_xpath_id,
                        'charge_type_xml_id'             => $this->xml_charge_type_xpath_id,
                        'charge_period_xml_id'           => $this->xml_charge_period_xpath_id,
                        'list_price_xml_id'              => $this->xml_list_price_xpath_id,
                        'icb_flag_xml_id'                => $this->xml_icb_flag_xpath_id,
                        'discount_rate_xml_id'           => $this->xml_discount_rate_xpath_id,
                        'discount_amount_xml_id'         => $this->xml_discount_amount_xpath_id,
                        'customer_contract_price_xml_id' => $this->xml_customer_contract_price_xpath_id,
                        'igc_settlement_price_xml_id'    => $this->xml_igc_settlement_price_xpath_id,
                        'currencyid_xml_id'              => $this->xml_price_currency_xpath_id,
                    );

                    if ($this->pms_icb_flag != 'true' && (int) $this->xml_discount_rate != 0) {
                        $this->xml_price_ids[$xml_specification_id."_".$line_item_index][$mapping['pricelineid']]['percent_discount_flag'] = '1';
                        $this->xml_price_ids[$xml_specification_id."_".$line_item_index][$mapping['pricelineid']]['discount']              = $this->xml_discount_rate;
                    }

                    if ($has_charge_type == false ||
                        $has_charge_period == false ||
                        $has_list_price == false ||
                        $has_contract_price == false ||
                        $has_icb_flag == false ||
                        $has_currency == false ||
                        $validate_charge_type == false ||
                        $validate_charge_period == false ||
                        $validate_list_price == false ||
                        $validate_contract_price == false ||
                        $validate_icb_flag == false ||
                        $validate_currency == false ||
                        $validate_igc_settlement_price == false ||
                        $validate_discount == false) {
                        $success = false;
                    }
                }
            }
        }

        // has validation error
        if ($success == false) {
            return false;
        }

        return true;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsMandatoryProductPriceChargeType()
    {
        if (empty($this->xml_charge_type)) {
            $this->xml_charge_type = $this->pms_charge_type;
        }

        return true;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsInvalidProductPriceChargeType($xml_specification_id, $product_price_id)
    {
        $success                  = true;
        $xml_price_xpath_id       = $this->xml_price_xpath_id;
        $xml_charge_type_xpath_id = $this->xml_charge_type_xpath_id;
        $xml_charge_type          = $this->xml_charge_type;
        if (!empty($this->xml_charge_type_tag)) {
            $xml_charge_type = $this->xml_charge_type_tag;
        }
        $xpath_ids = [
            'product_charge_type_xml_id' => $xml_charge_type_xpath_id,
            'product_charge_type_value'  => $xml_charge_type,
            'product_price_xml_id'       => $xml_price_xpath_id,
            'product_price_id'           => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if (!empty($this->xml_charge_type) && $this->pms_charge_type !== $this->xml_charge_type) {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsMandatoryProductPriceChargePeriod()
    {
        if (empty($this->xml_charge_period)) {
            $this->xml_charge_period = $this->pms_charge_period;
        }

        return true;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsInvalidProductPriceChargePeriod($xml_specification_id, $product_price_id)
    {
        $success                    = true;
        $xml_price_xpath_id         = $this->xml_price_xpath_id;
        $xml_charge_period_xpath_id = $this->xml_charge_period_xpath_id;
        $xml_charge_period          = $this->xml_charge_period;
        if (!empty($this->xml_charge_period_tag)) {
            $xml_charge_period = $this->xml_charge_period_tag;
        }
        $xpath_ids = [
            'product_charge_period_xml_id' => $this->xml_charge_period_xpath_id,
            'product_charge_period_value'  => $xml_charge_period,
            'product_price_xml_id'         => $xml_price_xpath_id,
            'product_price_id'             => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if (!empty($this->xml_charge_period) && $this->pms_charge_period !== $this->xml_charge_period) {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsMandatoryProductPriceListPrice($xml_specification_id, $product_price_id, $add_validation_only)
    {
        $success            = true;
        $xml_price_xpath_id = $this->xml_price_xpath_id;
        $xpath_ids          = [
            'product_price_xml_id' => $xml_price_xpath_id,
            'product_price_id'     => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        // adding custom validtion from price block
        if ($add_validation_only == true) {
            $this->addPmsValidationErrors($xpath_ids);
            return false;
        }

        if ($this->xml_list_price == '' && $this->pms_icb_flag != 'true') {
            $this->xml_list_price = (float) $this->pms_list_price;
        }

        return true;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsInvalidProductPriceListPrice($xml_specification_id, $product_price_id)
    {
        $success                 = true;
        $xml_price_xpath_id      = $this->xml_price_xpath_id;
        $xml_list_price_xpath_id = $this->xml_list_price_xpath_id;
        $xml_list_price          = $this->xml_list_price;
        $xpath_ids               = [
            'product_list_price_xml_id' => $xml_list_price_xpath_id,
            'product_list_price_value'  => $xml_list_price,
            'product_price_xml_id'      => $xml_price_xpath_id,
            'product_price_id'          => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if ($this->xml_list_price != '' && (float) $this->pms_list_price !== (float) $this->xml_list_price && $this->pms_icb_flag != 'true') {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        // list price must be 16digits with dot
        elseif (!filter_var($this->xml_list_price, FILTER_VALIDATE_FLOAT) && (float) $this->xml_list_price != 0 && $this->pms_list_price != 'true') {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsMandatoryProductPriceCustomerContractPrice($xml_specification_id, $product_price_id)
    {
        $success            = true;
        $xml_price_xpath_id = $this->xml_price_xpath_id;
        $xpath_ids          = [
            'product_price_xml_id' => $xml_price_xpath_id,
            'product_price_id'     => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if ($this->xml_customer_contract_price === '' && ($this->xml_discount_rate !== '' || $this->xml_discount_amount !== '')) {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsInvalidProductPriceCustomerContractPrice($xml_specification_id, $product_price_id, $add_validation_only)
    {
        $success                              = true;
        $xml_price_xpath_id                   = $this->xml_price_xpath_id;
        $xml_customer_contract_price_xpath_id = $this->xml_customer_contract_price_xpath_id;
        $xml_customer_contract_price          = $this->xml_customer_contract_price;
        $xpath_ids                            = [
            'product_customer_contract_price_xml_id' => $xml_customer_contract_price_xpath_id,
            'product_customer_contract_price_value'  => $xml_customer_contract_price,
            'product_price_xml_id'                   => $xml_price_xpath_id,
            'product_price_id'                       => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        // adding custom validtion from price block
        if ($add_validation_only == true && $this->add_customer_contract_price_validation_only == true) {
            $this->addPmsValidationErrors($xpath_ids);
            return false;
        } elseif ($this->add_customer_contract_price_validation_only == false) {
            // customer contract price is -ve && must be 16 digits
            if ((float) $this->xml_customer_contract_price < 0) {
                $this->addPmsValidationErrors($xpath_ids);
                $this->add_customer_contract_price_validation_only = false;
                $success                                           = false;
            }
            // customer contract price must be 16digits with dot
            elseif (!filter_var($this->xml_customer_contract_price, FILTER_VALIDATE_FLOAT) && (float) $this->xml_customer_contract_price != 0) {
                $this->addPmsValidationErrors($xpath_ids);
                $this->add_customer_contract_price_validation_only = false;
                $success                                           = false;
            }
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsMandatoryProductPriceIcbFlag()
    {
        if (empty($this->xml_icb_flag)) {
            $this->xml_icb_flag = $this->pms_icb_flag;
        }

        return true;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsInvalidProductPriceIcbFlag($xml_specification_id, $product_price_id)
    {
        $success               = true;
        $xml_price_xpath_id    = $this->xml_price_xpath_id;
        $xml_icb_flag_xpath_id = $this->xml_icb_flag_xpath_id;
        $xml_icb_flag          = $this->xml_icb_flag ? 'true' : 'false';
        $xpath_ids             = [
            'product_icb_flag_xml_id' => $xml_icb_flag_xpath_id,
            'product_icb_flag_value'  => $xml_icb_flag,
            'product_price_xml_id'    => $xml_price_xpath_id,
            'product_price_id'        => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if ($this->xml_icb_flag != $this->pms_icb_flag) {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsMandatoryProductPriceCurrency()
    {
        if (empty($this->xml_price_currency)) {
            $this->xml_price_currency = $this->pms_price_currency;
        }

        return true;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsInvalidProductPriceCurrency($xml_specification_id, $product_price_id)
    {
        $success               = true;
        $xml_price_xpath_id    = $this->xml_price_xpath_id;
        $xml_currency_xpath_id = $this->xml_price_currency_xpath_id;
        $xml_currency          = $this->xml_price_currency;
        $xpath_ids             = [
            'product_currency_xml_id' => $xml_currency_xpath_id,
            'product_currency_value'  => $xml_currency,
            'product_price_xml_id'    => $xml_price_xpath_id,
            'product_price_id'        => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if (!empty($this->xml_price_currency) && $this->xml_price_currency != $this->pms_price_currency) {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsInvalidProductPriceIgcSettlementPrice($xml_specification_id, $product_price_id)
    {
        $success                           = true;
        $xml_price_xpath_id                = $this->xml_price_xpath_id;
        $xml_igc_settlement_price_xpath_id = $this->xml_igc_settlement_price_xpath_id;
        $xml_igc_settlement_price          = $this->xml_igc_settlement_price;
        $xpath_ids                         = [
            'product_igc_settlement_price_xml_id' => $xml_igc_settlement_price_xpath_id,
            'product_igc_settlement_price_value'  => $xml_igc_settlement_price,
            'product_price_xml_id'                => $xml_price_xpath_id,
            'product_price_id'                    => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];

        if ((float) $this->xml_igc_settlement_price < 0) {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        // IGC settlement price must be 16digits with dot
        else if (!filter_var($this->xml_igc_settlement_price, FILTER_VALIDATE_FLOAT) && (float) $this->xml_igc_settlement_price != '0') {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsInvalidProductPriceDiscountAmount($xml_specification_id, $product_price_id)
    {
        $success            = true;
        $xml_price_xpath_id = $this->xml_price_xpath_id;
        $xpath_ids          = [
            'product_price_xml_id' => $xml_price_xpath_id,
            'product_price_id'     => ApiValidationHelper::formatPMSID($product_price_id, 1),
        ];
        // if isCustomPrice true discount will not work
        if ($this->pms_icb_flag != 'true') {

            // if both discount rate & discount amount not be empty
            if ((int) $this->xml_discount_rate != 0 && (float) $this->xml_discount_amount != 0) {
                $this->postPmsInvalidProductPriceDiscountShouldNotExist($xml_specification_id, $product_price_id);
                $success = false;
            }

            // discount rate must be less the 100
            else if ((float) $this->xml_discount_amount == 0 && (int) $this->xml_discount_rate > 100) {
                $add_validation_only = true;
                $this->versionUpPmsInvalidProductPriceDiscountRate($xml_specification_id, $product_price_id, $add_validation_only);
                $success = false;
            }

            // discount amount or rate is -ve
            else if ((float) $this->xml_discount_amount < 0 || (int) $this->xml_discount_rate < 0) {
                if ((float) $this->xml_discount_amount < 0) {
                    $xpath_ids['product_discount_amount_xml_id'] = $this->xml_discount_amount_xpath_id;
                    $xpath_ids['product_discount_amount_value']  = $this->xml_discount_amount;
                    $this->addPmsValidationErrors($xpath_ids);
                } else {
                    $add_validation_only = true;
                    $this->versionUpPmsInvalidProductPriceDiscountRate($xml_specification_id, $product_price_id, $add_validation_only);
                }
                $success = false;
            }

            // discount amount must be less than offering price
            else if ((int) $this->xml_discount_rate == 0 && (float) $this->xml_discount_amount > (float) $this->xml_list_price) {
                $xpath_ids['product_discount_amount_xml_id'] = $this->xml_discount_amount_xpath_id;
                $xpath_ids['product_discount_amount_value']  = $this->xml_discount_amount;
                $this->addPmsValidationErrors($xpath_ids);
                $success = false;
            }
            // ....
            else {
                // calculating desire price according to discount
                $desirePrice = 0;
                if ((int) $this->xml_discount_rate != 0) {
                    $desirePrice = ($this->xml_discount_rate / 100) * $this->xml_list_price;
                    $desirePrice = $this->xml_list_price - $desirePrice;
                } else if ((float) $this->xml_discount_amount != 0) {
                    $desirePrice = $this->xml_list_price - $this->xml_discount_amount;
                } else if ((int) $this->xml_discount_rate == 0 && (float) $this->xml_discount_amount == 0) {
                    $desirePrice = $this->xml_list_price;
                }

                // if customer contract price is empty assign value
                if ($this->xml_customer_contract_price === '') {
                    $this->xml_customer_contract_price = $this->xml_customer_contract_price = $desirePrice;
                }

                // customer contract price is valid or not for discount percent
                else if ((float) $desirePrice !== (float) $this->xml_customer_contract_price) {
                    // calling function to add proper error details from api_config
                    $add_validation_only = true;
                    $this->postPmsInvalidProductPriceCustomerContractPrice($xml_specification_id, $product_price_id, $add_validation_only);
                    $success = false;
                }
            }
        }
        // if isCustomPrice flase
        else {

            // list price exist && ICB = true
            if ($this->xml_list_price != '') {
                // calling function to add proper error details from api_config
                $add_validation_only = true;
                $this->postPmsMandatoryProductPriceListPrice($xml_specification_id, $product_price_id, $add_validation_only);
                $success = false;
            }

            // if both discount rate & discount amount exist && ICB = true
            if ($this->xml_discount_rate != '' || $this->xml_discount_amount != '') {
                $xpath_ids['product_discount_xml_id'] = $this->xml_discount_rate_xpath_id;
                $xpath_ids['product_discount_value']  = $this->xml_discount_rate;
                $this->addPmsValidationErrors($xpath_ids);
                $success = false;
            }
        }

        return $success;
    }

    /**
     * invalid validation for product configuration value
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Feb 08, 2018
     */
    public function postPmsInvalidProductPriceDiscountRate($xml_specification_id, $product_price_id, $add_validation_only)
    {
        $success = true;
        $xml_price_xpath_id = $this->xml_price_xpath_id;
        $xml_discount_rate_xpath_id = $this->xml_discount_rate_xpath_id;
        $xml_discount_rate = $this->xml_discount_rate;

        $xpath_ids          = [
            'product_price_xml_id' => $xml_price_xpath_id,
            'product_price_id'     => ApiValidationHelper::formatPMSID($product_price_id, 1),
            'product_discount_rate_xml_id' => $xml_discount_rate_xpath_id,
            'product_discount_rate_value'  => $xml_discount_rate
        ];

        if($add_validation_only == true) {
            $this->addPmsValidationErrors($xpath_ids);
            $success = false;
        }

        return $success;
    }

    /**
     * invalid validation for product price Discount amount & rate should not exist
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 14, 2017
     */
    public function postPmsInvalidProductPriceDiscountShouldNotExist($xml_specification_id, $product_price_id)
    {
        $success            = true;
        $xml_price_xpath_id = $this->xml_price_xpath_id;
        $xpath_ids          = [
            'product_price_xml_id'           => $xml_price_xpath_id,
            'product_price_id'               => ApiValidationHelper::formatPMSID($product_price_id, 1),
            'product_discount_rate_xml_id'   => $this->xml_discount_rate_xpath_id,
            'product_discount_rate_value'    => $this->xml_discount_rate,
            'product_discount_amount_xml_id' => $this->xml_discount_amount_xpath_id,
            'product_discount_amount_value'  => $this->xml_discount_amount,
        ];
        $this->addPmsValidationErrors($xpath_ids);
        $success = false;
    }
}
