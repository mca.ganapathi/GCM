<?php
require_once ('custom/include/XMLUtils.php');
/**
 * @type unix-file system UTF-8
 */
/**
 * Class to parse PATCH request xml data into array for API 2.0
 *
 * @author Dinesh.Itkar
 * @since 09 Aug 2016
 */
class ClassContractUpdateXMLParseV2 extends XMLUtils
{

    /**
     * Method to parse patch xml data
     *
     * @param xml $contract_xml_data xml data
     * @return array Convert xml data into array format
     */
    public function getContractParseArray($contract_xml_data)
    {
        // Initialize array to store parse xml data
        $post_xml_parse_arr = array();

        // Populate Contract Tag Details
        $post_xml_parse_arr = $this->getContractTagDetailsFromXML($contract_xml_data);

        // Populate Characteristic Value Tag Details
        $this->getCharValDetailsFromXML($contract_xml_data, $post_xml_parse_arr);

        $credit_card_details = $this->getCreditCardTagDetailsFromXML($contract_xml_data);
        $acc_transfer_details = $this->getAccountTransferTagDetailsFromXML($contract_xml_data);
        $postal_transfer_details = $this->getPostalTransferTagDetailsFromXML($contract_xml_data);
        $direct_deposit_details = $this->getDirectDepositTagDetailsFromXML($contract_xml_data);

        if (!empty($credit_card_details)) {
            $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractCreditCard'] = $credit_card_details;
        }elseif (!empty($acc_transfer_details)) {
            $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractAccountTransfer'] = $acc_transfer_details;
        } elseif (!empty($postal_transfer_details)) {
            $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractPostalTransfer'] = $postal_transfer_details;
        } elseif (!empty($direct_deposit_details)) {
            $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractDirectDeposit'] = $direct_deposit_details;
            if(isset($direct_deposit_details['payee_contact_person_name_2']) && empty($direct_deposit_details['payee_contact_person_name_2'])){
                $post_xml_parse_arr['NullFields']['ContractDetails']['ContractDirectDeposit'][] = 'payee_contact_person_name_2';
            }
            if(isset($direct_deposit_details['payee_tel_no']) && empty($direct_deposit_details['payee_tel_no'])){
                $post_xml_parse_arr['NullFields']['ContractDetails']['ContractDirectDeposit'][] = 'payee_tel_no';
            }
            if(isset($direct_deposit_details['payee_email_address']) && empty($direct_deposit_details['payee_email_address'])){
                $post_xml_parse_arr['NullFields']['ContractDetails']['ContractDirectDeposit'][] = 'payee_email_address';
            }
            if(isset($direct_deposit_details['remarks']) && empty($direct_deposit_details['remarks'])){
                $post_xml_parse_arr['NullFields']['ContractDetails']['ContractDirectDeposit'][] = 'remarks';
            }
        }

        // Unset unnecessary keys from array
        $post_xml_parse_arr_result = $this->filterFinalArray($post_xml_parse_arr);

        $acc_cid_lists = array(
                            'Contract' => isset($post_xml_parse_arr_result['Accounts']['Contract']['CorporateDetails']['common_customer_id_c']) ? $post_xml_parse_arr_result['Accounts']['Contract']['CorporateDetails']['common_customer_id_c'] : '',
                            'Billing' => isset($post_xml_parse_arr_result['Accounts']['Billing']['CorporateDetails']['common_customer_id_c']) ? $post_xml_parse_arr_result['Accounts']['Billing']['CorporateDetails']['common_customer_id_c'] : '',
                            'Technology' => isset($post_xml_parse_arr_result['Accounts']['Technology']['CorporateDetails']['common_customer_id_c']) ? $post_xml_parse_arr_result['Accounts']['Technology']['CorporateDetails']['common_customer_id_c'] : ''
        );
        foreach ($acc_cid_lists as $acc_key => $cid_val) {
            if (!empty($cid_val)) {
                $obj_check_cid = BeanFactory::getBean('Accounts');
                $obj_check_cid->retrieve_by_string_fields(array(
                                                                'common_customer_id_c' => $cid_val
                ));
                if (empty($obj_check_cid->id)) {
                    // Get Corporate details from CIDAS API for contracting account
                    $cidas_record_by_id = $this->getCorporateDetails($cid_val);
                    if (isset($cidas_record_by_id['cidas_failed']) && $cidas_record_by_id['cidas_failed']) {
                        $account_type = strtolower($acc_key); // contract | billing | technology
                        $cidas_record_by_id['cidas_id'] = $cid_val;
                        $cidas_record_by_id['account_type'] = $account_type;
                        $cidas_record_by_id['global_contract_xml_id'] = $post_xml_parse_arr_result['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
                        return $cidas_record_by_id;
                    }
                    $post_xml_parse_arr_result['Accounts'][$acc_key]['CorporateDetails'] = $cidas_record_by_id;
                }
            }
        }

        return $post_xml_parse_arr_result;
    }

    /**
     * Method to get corporate details from CIDAS API
     *
     * @param string $cid common customer id
     * @return array corporate details from CIDAS
     */
    private function getCorporateDetails($cid)
    {
        require_once ('custom/service/v4_1_custom/APIHelper/ClassCIDASConnection.php');
        $cidas_connection_obj = new ClassCIDASConnection();
        $cidas_record_by_id = $cidas_connection_obj->getCorporateDetailsFromCIDASByCID($cid, false, '2');
        return $cidas_record_by_id;
    }

    /**
     * Method to unset unnecessary keys from array
     *
     * @param array $post_xml_parse_arr xml data in array format
     * @return array
     */
    private function filterFinalArray($post_xml_parse_arr)
    {
        $post_xml_parse_arr_result['NullFields'] = !empty($post_xml_parse_arr['NullFields']) ? $post_xml_parse_arr['NullFields'] : array();
        $post_xml_parse_arr_result['ContractDetails'] = reset($post_xml_parse_arr);
        if (!empty($post_xml_parse_arr_result['ContractDetails']['Accounts'])) {
            $post_xml_parse_arr_result['Accounts'] = $post_xml_parse_arr_result['ContractDetails']['Accounts'];
            unset($post_xml_parse_arr_result['ContractDetails']['Accounts']);
        }
        if (empty($post_xml_parse_arr_result['ContractDetails'])){
            unset($post_xml_parse_arr_result['ContractDetails']);
        }
        if(isset($post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractCreditCard']['payment_type'])){
            $post_xml_parse_arr_result['ContractDetails']['ContractsHeader']['payment_type'] = $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractCreditCard']['payment_type'];
        }
        if(isset($post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractDirectDeposit']['payment_type'])){
            $post_xml_parse_arr_result['ContractDetails']['ContractsHeader']['payment_type'] = $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractDirectDeposit']['payment_type'];
        }
        if(isset($post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractAccountTransfer']['payment_type'])){
            $post_xml_parse_arr_result['ContractDetails']['ContractsHeader']['payment_type'] = $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractAccountTransfer']['payment_type'];
        }
        if(isset($post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractPostalTransfer']['payment_type'])){
            $post_xml_parse_arr_result['ContractDetails']['ContractsHeader']['payment_type'] = $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractPostalTransfer']['payment_type'];
        }
        return $post_xml_parse_arr_result;
    }

    /**
     * Method to parse product-contracts tag from xml
     *
     * @param xml $post_request xml data
     * @return array Convert contract details tag xml data into array format
     */
    private function getContractTagDetailsFromXML($post_request)
    {
        $tag_details = $this->getTagElements($post_request, '//bi:product-contracts');

        $contract_tag_id = '';
        if (isset($tag_details[0]['/product-contracts/product-contract[1]/identified-by[1]/product-contract-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
            $contract_tag_id = $tag_details[0]['/product-contracts/product-contract[1]/identified-by[1]/product-contract-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
            $contract_tag_details_arr['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'] = $contract_tag_id;
        }

        if (isset($tag_details[0]['/product-contracts/product-contract[1]/name[1]'])) {
            $contract_tag_details_arr['ContractDetails']['ContractsHeader']['name'] = $tag_details[0]['/product-contracts/product-contract[1]/name[1]'];
            if(empty($tag_details[0]['/product-contracts/product-contract[1]/name[1]'])){
                $contract_tag_details_arr['NullFields']['ContractDetails']['ContractsHeader'][] = 'name';
            }
        }

        if (isset($tag_details[0]['/product-contracts/product-contract[1]/description[1]'])) {
            $contract_tag_details_arr['ContractDetails']['ContractsHeader']['description'] = $tag_details[0]['/product-contracts/product-contract[1]/description[1]'];
            if(empty($tag_details[0]['/product-contracts/product-contract[1]/description[1]'])){
                $contract_tag_details_arr['NullFields']['ContractDetails']['ContractsHeader'][] = 'description';
            }
        }

        if (isset($tag_details[0]['/product-contracts/product-contract[1]/standard-business-state[1]'])) {
            $contract_status_val = $tag_details[0]['/product-contracts/product-contract[1]/standard-business-state[1]'];
            foreach ($GLOBALS['app_list_strings']['contract_status_list'] as $cs_key => $cs_val) {
                if ($cs_val == $contract_status_val)
                    $contract_status_id = $cs_key;
            }
            $contract_tag_details_arr['ContractDetails']['ContractsHeader']['contract_status'] = $contract_status_id;
            $contract_tag_details_arr['ContractDetails']['ContractsHeader']['contract_status_xml_val'] = $contract_status_val;
        }

        if (isset($tag_details[0]['/product-contracts/product-contract[1]/last-modified-user[1]'])) {
            $contract_tag_details_arr['ContractDetails']['ContractsHeader']['ext_sys_modified_by'] = $tag_details[0]['/product-contracts/product-contract[1]/last-modified-user[1]'];
            if(empty($tag_details[0]['/product-contracts/product-contract[1]/last-modified-user[1]'])){
                $contract_tag_details_arr['NullFields']['ContractDetails']['ContractsHeader'][] = 'ext_sys_modified_by';
            }
        }

        if (isset($tag_details[0]['/product-contracts/product-contract[1]/lock-version[1]'])) {
            $contract_tag_details_arr['ContractDetails']['ContractsHeader']['lock_version'] = $tag_details[0]['/product-contracts/product-contract[1]/lock-version[1]'];
        }

        if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/contract-customer[1]/corporation[1]/vat-no[1]'])) {
            $contract_tag_details_arr['ContractDetails']['ContractsHeader']['vat_no'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/contract-customer[1]/corporation[1]/vat-no[1]'];
            if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/contract-customer[1]/corporation[1]/vat-no[1]'])){
                $contract_tag_details_arr['NullFields']['ContractDetails']['ContractsHeader'][] = 'vat_no';
            }
        }

        if (isset($tag_details[0]['/product-contracts/product-contract[1]/agreement-period[1]/end-date-time[1]'])) {
            $contract_enddate_xml_value = '';
            $contract_enddate_xml_value = $tag_details[0]['/product-contracts/product-contract[1]/agreement-period[1]/end-date-time[1]'];
            if (!empty($contract_enddate_xml_value)) {
                $contract_enddate_datatimezone_arr = array();
                $contract_enddate = '';
                $contract_enddate_timezone = '';
                $contract_enddate_datatimezone_arr = $this->fnGetDateTimeZone($contract_enddate_xml_value);
                $contract_tag_details_arr['ContractDetails']['ContractsHeader']['contract_enddate'] = isset($contract_enddate_datatimezone_arr['date_time']) ? $contract_enddate_datatimezone_arr['date_time'] : '';
                $contract_tag_details_arr['ContractDetails']['ContractsHeader']['contract_enddate_timezone'] = isset($contract_enddate_datatimezone_arr['time_zone']) ? $contract_enddate_datatimezone_arr['time_zone'] : '';
            }
        }

        $check_sales_rep_tag_exist = $this->getTagElements($post_request, '//gcm:sales-channel');
        if (!empty($check_sales_rep_tag_exist)) {

            if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/corporation[1]/sales-channel-code[1]'])){
                $contract_tag_details_arr['ContractDetails']['ContractSalesRepresentative']['0']['sales_channel_code'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/corporation[1]/sales-channel-code[1]'];
                if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/corporation[1]/sales-channel-code[1]'])){
                    $contract_tag_details_arr['NullFields']['ContractDetails']['ContractSalesRepresentative'][] = 'sales_channel_code';
                }
            }

            if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/name-latin[1]'])){
                $contract_tag_details_arr['ContractDetails']['ContractSalesRepresentative']['0']['name'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/name-latin[1]'];
                if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/name-latin[1]'])){
                    $contract_tag_details_arr['NullFields']['ContractDetails']['ContractSalesRepresentative'][] = 'name';
                }
            }

            if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/name[1]'])){
                $contract_tag_details_arr['ContractDetails']['ContractSalesRepresentative']['0']['sales_rep_name_1'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/name[1]'];
                if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/name[1]'])){
                    $contract_tag_details_arr['NullFields']['ContractDetails']['ContractSalesRepresentative'][] = 'sales_rep_name_1';
                }
            }

            if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/division[1]/name-latin[1]'])){
                $contract_tag_details_arr['ContractDetails']['ContractSalesRepresentative']['0']['division_1'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/division[1]/name-latin[1]'];
                if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/division[1]/name-latin[1]'])){
                    $contract_tag_details_arr['NullFields']['ContractDetails']['ContractSalesRepresentative'][]  = 'division_1';
                }
            }


            if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/division[1]/name[1]'])){
                $contract_tag_details_arr['ContractDetails']['ContractSalesRepresentative']['0']['division_2'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/division[1]/name[1]'];
                if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/division[1]/name[1]'])){
                    $contract_tag_details_arr['NullFields']['ContractDetails']['ContractSalesRepresentative'][] = 'division_2';
                }
            }
            if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/title-latin[1]'])){
                $contract_tag_details_arr['ContractDetails']['ContractSalesRepresentative']['0']['title_1'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/title-latin[1]'];
                if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/title-latin[1]'])){
                    $contract_tag_details_arr['NullFields']['ContractDetails']['ContractSalesRepresentative'][] = 'title_1';
                }
            }
            if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/title[1]'])){
                $contract_tag_details_arr['ContractDetails']['ContractSalesRepresentative']['0']['title_2'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/title[1]'];
                if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/title[1]'])){
                    $contract_tag_details_arr['NullFields']['ContractDetails']['ContractSalesRepresentative'][] = 'title_2';
                }
            }
            if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/telephone-number[1]'])){
                $contract_tag_details_arr['ContractDetails']['ContractSalesRepresentative']['0']['tel_no'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/telephone-number[1]'];
                if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/telephone-number[1]'])){
                    $contract_tag_details_arr['NullFields']['ContractDetails']['ContractSalesRepresentative'][] = 'tel_no';
                }
            }
            if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/extension-number[1]'])){
                $contract_tag_details_arr['ContractDetails']['ContractSalesRepresentative']['0']['ext_no'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/extension-number[1]'];
                if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/extension-number[1]'])){
                    $contract_tag_details_arr['NullFields']['ContractDetails']['ContractSalesRepresentative'][] = 'ext_no';
                }
            }
            if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/fax-number[1]'])){
                $contract_tag_details_arr['ContractDetails']['ContractSalesRepresentative']['0']['fax_no'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/fax-number[1]'];
                if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/fax-number[1]'])){
                    $contract_tag_details_arr['NullFields']['ContractDetails']['ContractSalesRepresentative'][] = 'fax_no';
                }
            }
            if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/email-address[1]'])){
                $contract_tag_details_arr['ContractDetails']['ContractSalesRepresentative']['0']['email1'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/email-address[1]'];
                if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/email-address[1]'])){
                    $contract_tag_details_arr['NullFields']['ContractDetails']['ContractSalesRepresentative'][] = 'email1';
                }
            }
            if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/corporation[1]/standard-corporation-name[1]/name[1]'])){
                $contract_tag_details_arr['ContractDetails']['ContractSalesRepresentative']['0']['sales_company'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/corporation[1]/standard-corporation-name[1]/name[1]'];
                if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/corporation[1]/standard-corporation-name[1]/name[1]'])){
                    $contract_tag_details_arr['NullFields']['ContractDetails']['ContractSalesRepresentative'][] = 'sales_company';
                }
            }
        }

        $acc_types = array(
                        'Contract' => 'contract-customer',
                        'Billing' => 'billing-customer',
                        'Technology' => 'customer-technical-representative'
        );

        foreach ($acc_types as $key => $acc_type) {
            $acc_tag_details = $this->getTagElements($post_request, '//gcm:' . $acc_type);

            if (!empty($acc_tag_details)) {

                foreach ($acc_tag_details[0] as $acc_key => $acc_val) {
                    if (preg_match('/\-address\[1\]\//', $acc_key)) {
                        $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['address_tag_name'] = $this->get_country_tag_name("/address[1]/", "-address[1]/", $acc_key);
                        break;
                    }
                }

                $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['contact_type_c'] = $key;
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/name-latin[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['last_name'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/name-latin[1]'];
                    if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/name-latin[1]'])){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key][] = 'last_name';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/name[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['name_2_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/name[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/american-property-address[1]/zip-code[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['postal_no_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/american-property-address[1]/zip-code[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/canadian-property-address[1]/postal-code[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['postal_no_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/canadian-property-address[1]/postal-code[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/postal-code[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['postal_no_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/postal-code[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/any-address[1]/address[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['addr_1_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/any-address[1]/address[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/division[1]/name-latin[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['division_1_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/division[1]/name-latin[1]'];
                    if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/division[1]/name-latin[1]'])){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key][] = 'division_1_c';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/division[1]/name[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['division_2_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/division[1]/name[1]'];
                    if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/division[1]/name[1]'])){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key][] = 'division_2_c';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/title[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['title_2_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/title[1]'];
                    if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/title[1]'])){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key][] = 'title_2_c';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/telephone-number[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['phone_work'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/telephone-number[1]'];
                    if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/telephone-number[1]'])){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key][] = 'phone_work';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/extension-number[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['ext_no_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/extension-number[1]'];
                    if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/extension-number[1]'])){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key][] = 'ext_no_c';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/fax-number[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['phone_fax'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/fax-number[1]'];
                    if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/fax-number[1]'])){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key][] = 'phone_fax';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/email-address[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['email1'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/email-address[1]'];
                    if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/email-address[1]'])){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key][] = 'email1';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/iso3166-1numeric[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['country_code_numeric'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/iso3166-1numeric[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/american-property-address[1]/address-line[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['general_address']['addr_general_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/american-property-address[1]/address-line[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/canadian-property-address[1]/address-line[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['general_address']['addr_general_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/canadian-property-address[1]/address-line[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/address[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['Accounts_js']['addr1'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/address[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/address-line[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['Accounts_js']['addr2'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/address-line[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/american-property-address[1]/city[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['general_address']['city_general_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/american-property-address[1]/city[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/canadian-property-address[1]/city[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['general_address']['city_general_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/canadian-property-address[1]/city[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/city[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['general_address']['city_general_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/city[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/american-property-address[1]/state-abbr[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['general_address']['state_general_code'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/american-property-address[1]/state-abbr[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/canadian-property-address[1]/state-abbr[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['general_address']['state_general_code'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/canadian-property-address[1]/state-abbr[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/state-abbr[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['general_address']['state_general_code'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/state-abbr[1]'];
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/iso3166-1numeric[1]'])) {
                    $country_code_numeric = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/iso3166-1numeric[1]'];
                    if (!empty($country_code_numeric)) {
                        $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['country_code_numeric'] = $country_code_numeric;
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/american-property-address[1]/attention-line[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['attention_line_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/american-property-address[1]/attention-line[1]'];
                    if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/american-property-address[1]/attention-line[1]'])){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key][] = 'attention_line_c';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/canadian-property-address[1]/attention-line[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['attention_line_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/canadian-property-address[1]/attention-line[1]'];
                    if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/canadian-property-address[1]/attention-line[1]'])){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key][] = 'attention_line_c';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/any-address[1]/attention-line[1]'])) {
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['attention_line_c'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/any-address[1]/attention-line[1]'];
                    if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/any-address[1]/attention-line[1]'])){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key][] = 'attention_line_c';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/corporation-id[1]'])) {
                    $common_customer_id_c = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/corporation-id[1]'];

                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['CorporateDetails']['common_customer_id_c'] = $common_customer_id_c;
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['CorporateDetails']['cidas_data'] = 'No';
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['CorporateDetails']['cidas_search'] = 'No';
                    if(empty($common_customer_id_c)){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key]['CorporateDetails'][] = 'common_customer_id_c';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/standard-corporation-name[1]/name-latin[1]'])) {
                    $corporation_name_latin = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/standard-corporation-name[1]/name-latin[1]'];

                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['CorporateDetails']['name'] = $corporation_name_latin;
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['CorporateDetails']['cidas_data'] = 'No';
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['CorporateDetails']['cidas_search'] = 'No';
                    if(empty($corporation_name_latin)){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key]['CorporateDetails'][] = 'name';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/standard-corporation-name[1]/name[1]'])) {
                    $corporation_name_local = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/standard-corporation-name[1]/name[1]'];

                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['CorporateDetails']['name_2_c'] = $corporation_name_local;
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['CorporateDetails']['cidas_data'] = 'No';
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['CorporateDetails']['cidas_search'] = 'No';
                    if(empty($corporation_name_local)){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key]['CorporateDetails'][] = 'name_2_c';
                    }
                }
                if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/standard-corporation-name[1]/name-furigana[1]'])) {
                    $corporation_name_furigana = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/standard-corporation-name[1]/name-furigana[1]'];

                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['CorporateDetails']['name_3_c'] = $corporation_name_furigana;
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['CorporateDetails']['cidas_data'] = 'No';
                    $contract_tag_details_arr['ContractDetails']['Accounts'][$key]['CorporateDetails']['cidas_search'] = 'No';
                    if(empty($corporation_name_furigana)){
                        $contract_tag_details_arr['NullFields']['ContractDetails']['Accounts'][$key]['CorporateDetails'][] = 'name_3_c';
                    }
                }
            }
        }

        if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/billing-affiliate[1]/organization-code[1]/code1[1]'])) {
            $contract_tag_details_arr['ContractDetails']['BillingAffiliate']['0']['organization_code_1'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/billing-affiliate[1]/organization-code[1]/code1[1]'];
        }

        if (isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/billing-affiliate[1]/organization-code[1]/code2[1]'])) {
            $contract_tag_details_arr['ContractDetails']['BillingAffiliate']['0']['organization_code_2'] = $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/billing-affiliate[1]/organization-code[1]/code2[1]'];
            if(empty($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/billing-affiliate[1]/organization-code[1]/code2[1]'])){
                $contract_tag_details_arr['NullFields']['ContractDetails']['BillingAffiliate'][] = 'organization_code_2';
            }
        }

        return $contract_tag_details_arr;
    }

    /**
     * Method to parse characteristic-value tag from xml
     *
     * @param xml $post_request xml data
     * @param array &$post_xml_parse_arr xml data in array format
     */
    private function getCharValDetailsFromXML($post_request, &$post_xml_parse_arr)
    {
        $ARR_GUUID = self::getGlobalUUID();
        $tag_details = $this->getTagElements($post_request, '//cbe:characteristic-value');

        if (!empty($tag_details)) {
            foreach ($tag_details as $tag_detail) {
                $char_val_tag_id = '';
                if (isset($tag_detail['/characteristic-value/identified-by[1]/charc-value-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                    $char_val_tag_id = $tag_detail['/characteristic-value/identified-by[1]/charc-value-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
                }

                if(isset($tag_detail['/characteristic-value/service-start-date-val[1]'])){
                    $li_service_start_date_array = array();
                    $li_service_start_date_array = $this->fnGetDateTimeZone(end($tag_detail));
                    $service_start_date = isset($li_service_start_date_array['date_time']) ? $li_service_start_date_array['date_time'] : '';
                    $service_start_date_timezone = isset($li_service_start_date_array['time_zone']) ? $li_service_start_date_array['time_zone'] : '';
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['service_start_date']['val'][] = $service_start_date;
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['service_start_date_timezone']['val'][] = $service_start_date_timezone;
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['service_start_date']['xml_id'][] = $char_val_tag_id;
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['service_start_date_timezone']['xml_id'][] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/service-end-date-val[1]'])){
                    $li_service_end_date_array = array();
                    $li_service_end_date_array = $this->fnGetDateTimeZone(end($tag_detail));
                    $service_end_date = isset($li_service_end_date_array['date_time']) ? $li_service_end_date_array['date_time'] : '';
                    $service_end_date_timezone = isset($li_service_end_date_array['time_zone']) ? $li_service_end_date_array['time_zone'] : '';
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['service_end_date']['val'][] = $service_end_date;
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['service_end_date_timezone']['val'][] = $service_end_date_timezone;
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['service_end_date']['xml_id'][] = $char_val_tag_id;
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['service_end_date_timezone']['xml_id'][] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/billing-start-date-val[1]'])){
                    $li_billing_start_date_array = array();
                    $li_billing_start_date_array = $this->fnGetDateTimeZone(end($tag_detail));
                    $billing_start_date = isset($li_billing_start_date_array['date_time']) ? $li_billing_start_date_array['date_time'] : '';
                    $billing_start_date_timezone = isset($li_billing_start_date_array['time_zone']) ? $li_billing_start_date_array['time_zone'] : '';
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['billing_start_date']['val'][] = $billing_start_date;
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['billing_start_date_timezone']['val'][] = $billing_start_date_timezone;
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['billing_start_date']['xml_id'][] = $char_val_tag_id;
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['billing_start_date_timezone']['xml_id'][] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/billing-end-date-val[1]'])){
                    $li_billing_end_date_array = array();
                    $li_billing_end_date_array = $this->fnGetDateTimeZone(end($tag_detail));
                    $billing_end_date = isset($li_billing_end_date_array['date_time']) ? $li_billing_end_date_array['date_time'] : '';
                    $billing_end_date_timezone = isset($li_billing_end_date_array['time_zone']) ? $li_billing_end_date_array['time_zone'] : '';
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['billing_end_date']['val'][] = $billing_end_date;
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['billing_end_date_timezone']['val'][] = $billing_end_date_timezone;
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['billing_end_date']['xml_id'][] = $char_val_tag_id;
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['billing_end_date_timezone']['xml_id'][] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/contract-scheme-val[1]'])){
                    $post_xml_parse_arr['ContractDetails']['ContractsHeader']['contract_scheme'] = end($tag_detail);
                    $post_xml_parse_arr['ContractDetails']['ContractsHeader']['contract_scheme_xml_id'] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/contract-type-val[1]'])){
                    $post_xml_parse_arr['ContractDetails']['ContractsHeader']['contract_type'] = end($tag_detail);
                    $post_xml_parse_arr['ContractDetails']['ContractsHeader']['contract_type_xml_id'] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/billing-scheme-val[1]'])){
                    $global_uuid_value = end($tag_detail);
                    $billing_type_list_key = array_search($global_uuid_value, $GLOBALS['app_list_strings']['billing_type_list']);
                    $post_xml_parse_arr['ContractDetails']['ContractsHeader']['billing_type'] = $billing_type_list_key;
                    $post_xml_parse_arr['ContractDetails']['ContractsHeader']['billing_type_xml_id'] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/loi-contract-val[1]'])){
                    $loi_contract_list = self::getLOIContractValue();
                    $loi_contract_xml_val = end($tag_detail);
                    $post_xml_parse_arr['ContractDetails']['ContractsHeader']['loi_contract'] = isset($loi_contract_list[$loi_contract_xml_val]) ? $loi_contract_list[$loi_contract_xml_val] : '';
                    $post_xml_parse_arr['ContractDetails']['ContractsHeader']['loi_contract_xml_id'] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/factory-reference-number-val[1]'])){
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['factory_reference_no']['val'][] = end($tag_detail);
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['factory_reference_no']['xml_id'][] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/customer-contract-currency-val[1]'])){
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['cust_contract_currency']['val'][] = end($tag_detail);
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['cust_contract_currency']['xml_id'][] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/customer-billing-currency-val[1]'])){
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['cust_billing_currency']['val'][] = end($tag_detail);
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['cust_billing_currency']['xml_id'][] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/inter-group-company-contract-currency-val[1]'])){
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['igc_contract_currency']['val'][] = end($tag_detail);
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['igc_contract_currency']['xml_id'][] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/inter-group-company-billing-currency-val[1]'])){
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['igc_billing_currency']['val'][] = end($tag_detail);
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['igc_billing_currency']['xml_id'][] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/customer-billing-method-val[1]'])){
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['customer_billing_method']['val'][] = end($tag_detail);
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['customer_billing_method']['xml_id'][] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/inter-group-company-settlement-method-val[1]'])){
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['igc_settlement_method']['val'][] = end($tag_detail);
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['igc_settlement_method']['xml_id'][] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/remarks-val[1]'])){
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['description']['val'][] = end($tag_detail);
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['description']['xml_id'][] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/group-name-val[1]'])){
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['group_name']['val'][] = end($tag_detail);
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['group_name']['xml_id'][] = $char_val_tag_id;
                }elseif(isset($tag_detail['/characteristic-value/group-description-val[1]'])){
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['group_description']['val'][] = end($tag_detail);
                    $post_xml_parse_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']['group_description']['xml_id'][] = $char_val_tag_id;
                }
            }
        }

        return;
    }

    /**
     * Method to get global UUID
     *
     * @return array
     */
    private function getGlobalUUID()
    {
        include 'custom/include/globalUUIDConfig.php';
        return $ARR_GUUID;
    }

    /**
     * Method to get loi contract value
     *
     * @return array
     */
    private function getLOIContractValue()
    {
        include 'custom/include/globalUUIDConfig.php';
        return $loi_contract_array;
    }

    /**
     * Method to parse jpn-postal-transfer-pm tag from xml
     *
     * @param xml $post_request xml data
     * @return array $pt_details_array jpn-postaltransfer-pm tag details
     */
    private function getPostalTransferTagDetailsFromXML($post_request)
    {
        $pt_details_array = array();
        $tag_details = $this->getTagElements($post_request, '//gcm:jpn-postal-transfer-pm');
        if (!empty($tag_details)) {
            $payment_type_pt = '30';
            if(isset($tag_details[0]['/jpn-postal-transfer-pm/postal-account-holder-name[1]'])){
                $pt_details_array['name'] =  $tag_details[0]['/jpn-postal-transfer-pm/postal-account-holder-name[1]'] ;
            }
            if(isset($tag_details[0]['/jpn-postal-transfer-pm/postal-passbook-mark[1]'])){
                $pt_details_array['postal_passbook_mark'] = $tag_details[0]['/jpn-postal-transfer-pm/postal-passbook-mark[1]'];
            }
            if(isset($tag_details[0]['/jpn-postal-transfer-pm/postal-passbook-number[1]'])){
                $pt_details_array['postal_passbook'] = $tag_details[0]['/jpn-postal-transfer-pm/postal-passbook-number[1]'];
            }
            if(isset($tag_details[0]['/jpn-postal-transfer-pm/is-postal-passbook-number-display[1]'])){
                $pt_details_array['postal_passbook_no_display'] = $tag_details[0]['/jpn-postal-transfer-pm/is-postal-passbook-number-display[1]'];
            }
            $pt_details_array['payment_type'] = $payment_type_pt;
        }
        return $pt_details_array;
    }

    /**
     * Method to parse jpn-account-transfer-pm tag from xml
     *
     * @param xml $post_request xml data
     * @return array jpn-account-transfer-pm tag details
     */
    private function getAccountTransferTagDetailsFromXML($post_request)
    {
        $at_details_array = array();
        $tag_details = $this->getTagElements($post_request, '//gcm:jpn-account-transfer-pm');
        if (!empty($tag_details)) {
            $payment_type_at = '10';
            if(isset($tag_details[0]['/jpn-account-transfer-pm/bank-account-holder-name[1]'])){
                $at_details_array['name'] = $tag_details[0]['/jpn-account-transfer-pm/bank-account-holder-name[1]'];
            }
            if(isset($tag_details[0]['/jpn-account-transfer-pm/bank-code[1]'])){
                $at_details_array['bank_code'] = $tag_details[0]['/jpn-account-transfer-pm/bank-code[1]'];
            }
            if(isset($tag_details[0]['/jpn-account-transfer-pm/branch-code[1]'])){
                $at_details_array['branch_code'] = $tag_details[0]['/jpn-account-transfer-pm/branch-code[1]'];
            }
            if(isset($tag_details[0]['/jpn-account-transfer-pm/deposit-type[1]'])){
                $at_details_array['deposit_type'] = $tag_details[0]['/jpn-account-transfer-pm/deposit-type[1]'];
            }
            if(isset($tag_details[0]['/jpn-account-transfer-pm/account-number[1]'])){
                $at_details_array['account_no'] = $tag_details[0]['/jpn-account-transfer-pm/account-number[1]'];
            }
            if(isset($tag_details[0]['/jpn-account-transfer-pm/is-account-number-display[1]'])){
                $at_details_array['account_no_display'] = $tag_details[0]['/jpn-account-transfer-pm/is-account-number-display[1]'];
            }
            $at_details_array['payment_type'] = $payment_type_at;
        }
        return $at_details_array;
    }

    /**
     * Method to parse jpn-direct-deposit-pm tag from xml
     *
     * @param xml $post_request xml data
     * @return array jpn-direct-deposit-pm tag details
     */
    private function getDirectDepositTagDetailsFromXML($post_request)
    {
        $dd_details_array = array();
        $tag_details = $this->getTagElements($post_request, '//gcm:jpn-direct-deposit-pm');
        if (!empty($tag_details)) {
            $payment_type_dd = '00';
            if(isset($tag_details[0]['/jpn-direct-deposit-pm/bank-code[1]'])){
                $dd_details_array['bank_code'] = $tag_details[0]['/jpn-direct-deposit-pm/bank-code[1]'];
            }
            if(isset($tag_details[0]['/jpn-direct-deposit-pm/branch-code[1]'])){
                $dd_details_array['branch_code'] = $tag_details[0]['/jpn-direct-deposit-pm/branch-code[1]'];
            }
            if(isset($tag_details[0]['/jpn-direct-deposit-pm/deposit-type[1]'])){
                $dd_details_array['deposit_type'] = $tag_details[0]['/jpn-direct-deposit-pm/deposit-type[1]'];
            }
            if(isset($tag_details[0]['/jpn-direct-deposit-pm/account-number[1]'])){
                $dd_details_array['account_no'] = $tag_details[0]['/jpn-direct-deposit-pm/account-number[1]'];
            }
            if(isset($tag_details[0]['/jpn-direct-deposit-pm/name-of-person-in-charge[1]'])){
                $dd_details_array['payee_contact_person_name_2'] = $tag_details[0]['/jpn-direct-deposit-pm/name-of-person-in-charge[1]'];
            }
            if(isset($tag_details[0]['/jpn-direct-deposit-pm/telephone-number[1]'])){
                $dd_details_array['payee_tel_no'] = $tag_details[0]['/jpn-direct-deposit-pm/telephone-number[1]'];
            }
            if(isset($tag_details[0]['/jpn-direct-deposit-pm/email-address[1]'])){
                $dd_details_array['payee_email_address'] = $tag_details[0]['/jpn-direct-deposit-pm/email-address[1]'];
            }
            if(isset($tag_details[0]['/jpn-direct-deposit-pm/remarks[1]'])){
                $dd_details_array['remarks'] = $tag_details[0]['/jpn-direct-deposit-pm/remarks[1]'];
            }
            $dd_details_array['payment_type'] = $payment_type_dd;
        }
        return $dd_details_array;
    }

    /**
     * Method to parse credit-card-pm tag from xml
     *
     * @param xml $post_request xml data
     * @return array credit-card-pm tag details
     */
    private function getCreditCardTagDetailsFromXML($post_request)
    {
        $cc_details_array = array();
        $tag_details = $this->getTagElements($post_request, '//gcm:credit-card-pm');
        if (!empty($tag_details)) {
            $payment_type_cc = '60';
            if(isset($tag_details[0]['/credit-card-pm/credit-card-number[1]'])){
                $cc_details_array['credit_card_no'] = $tag_details[0]['/credit-card-pm/credit-card-number[1]'];
            }
            if(isset($tag_details[0]['/credit-card-pm/expiration-date[1]'])){
                $cc_details_array['expiration_date'] = $tag_details[0]['/credit-card-pm/expiration-date[1]'];
            }
            $cc_details_array['payment_type'] = $payment_type_cc;
        }
        return $cc_details_array;
    }

}

?>
