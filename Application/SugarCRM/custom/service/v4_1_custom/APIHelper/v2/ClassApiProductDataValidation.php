<?php

require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';

/**
 * Class to handle PMS validation from API
 */
class ApiProductDataValidation
{
    /**
     * set error msg list with error code
     *
     * @var array
     */
    private $errorMsgs;

    /**
     * set api request date
     *
     * @var array
     */
    private $apiPmsData;

    /**
     * set api respose date
     *
     * @var array
     */
    private $response;

    /**
     * set PMS product offering list
     *
     * @var array
     */
    private $offeringList;

    /**
     * set PMS product offering details of a specific offering id
     *
     * @var array
     */
    private $offeringDetails;

    /**
     * set ClassProductConfiguration object
     *
     * @var object
     */
    private $objProduct;

    /**
     * set api request specification details
     *
     * @var array
     */
    private $apiSpecificationDetails;

    /**
     * set PMS specification details
     *
     * @var array
     */
    private $pmsSpecificationDetails;

    /**
     * set PMS charge details
     *
     * @var array
     */
    private $pmsChargeDetails;

    /**
     * set PMS charge mapping
     *
     * @var array
     */
    private $pmsChargeMapping;

    /**
     * set api request offering id
     *
     * @var errorMsgs
     */
    private $offeringId;

    /**
     * set each specification id of api request date in validateSpecications()
     *
     * @var string
     */
    private $specificationId;

    /**
     * set contract currency from api request date
     *
     * @var string
     */
    private $apiContractCurrency;

    /**
     * set Characteristics Id from api request date
     *
     * @var errorMsgs
     */
    private $apiCharacteristicsIds;

    /**
     * set Rules Id from api request date
     *
     * @var errorMsgs
     */
    private $apiRulesIds;

    /**
     * set price details of PMS date
     *
     * @var array
     */
    private $pmsArrayforPrice;

    /**
     * set contract type new|modify
     *
     * @var boolean
     */
    private $is_new_registration;

    /**
     * Constructor method for class ApiProductDataValidation
     *
     * @author Rajul.Mondal
     * @since Jun 10, 2016
     */
    public function __construct()
    {
        $this->apiPmsData              = array();
        $this->offeringList            = array();
        $this->offeringDetails         = array();
        $this->apiSpecificationDetails = array();
        $this->pmsSpecificationDetails = array();
        $this->pmsChargeDetails        = array();
        $this->pmsChargeMapping        = array();
        $this->offeringId              = null;
        $this->specificationId         = null;
        $this->apiContractCurrency     = '';
        $this->apiCharacteristicsIds   = array();
        $this->apiRulesIds             = array();
        $this->pmsArrayforPrice        = array();
        $this->errorMsgs               = array(
            '8004' => 'Contract process terminated. Please contact System Administrator.',
            '3010' => 'Contract process terminated. Base Atomic Product should not be empty.',
            '3013' => '',
        );
        $this->response = array(
            'codes'    => array(),
            'messages' => array(),
            'data'     => array(),
        );

        $this->objProduct          = new ClassProductConfiguration();
        $this->is_new_registration = false;
    }

    /**
     * public method for validate Product Details Outside classes can call this method to validate Product Details
     *
     * @param array $apiPmsData api request data
     * @return array api request data with error msg and error code
     * @author Rajul.Mondal
     * @since Jun 10, 2016
     */
    public function validateProductDetails($apiPmsData)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'apiPmsData : ' . print_r($apiPmsData, true), 'public method for validate Product Details Outside classes can call this method to validate Product Details');
        if (empty($apiPmsData)) {
            $this->setError('8004', '');
        } else {
            $this->regenerateApiData($apiPmsData);
            if (empty($this->apiPmsData)) {
                $this->setError('8004', '');
            } else {
                $this->offeringId = $this->apiPmsData['offering_id'];
                $offering_list    = $this->objProduct->getProductOfferings(true);
                if ($offering_list['response_code'] == '0') {
                    $this->setError('8004', '');
                } elseif (empty($offering_list['respose_array'])) {
                    $this->setError('3013', 'Offering details not available #' . $this->formatPMSID($this->offeringId, 1));
                } else {
                    $this->offeringList = $offering_list['response_code'] == '1' ? $offering_list['respose_array'] : array();
                    if ($this->validateProductOffering()) {
                        $this->collectSpecificationDetails();
                        if ($this->validateSpecications()) {
                            $this->response = array(
                                'codes'    => array(),
                                'messages' => array(),
                                'data'     => $this->resultApiData($apiPmsData),
                            );
                        }
                    }
                }
            }
        }
        // log response
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response : ' . print_r($this->response, true), '');
        return $this->response;
    }

    /**
     * Method to validate the Product Offering
     *
     * @return boolean true|false
     * @author Rajul.Mondal
     * @since Jun 10, 2016
     */
    private function validateProductOffering()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to validate the Product Offering');
        if (empty($this->offeringId)) {
            $this->setError('3013', 'Offering id is empty');
            return false;
        } else {
            if (!array_key_exists($this->offeringId, $this->offeringList)) {
                $this->setError('3013', 'Invalid offering id #' . $this->formatPMSID($this->offeringId, 1));
                return false;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return true;
    }

    /**
     * Method to collect Specification Details from PMS
     *
     * @author Rajul.Mondal
     * @since Jun 10, 2016
     */
    private function collectSpecificationDetails()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to collect Specification Details from PMS');
        $offeringDetails       = $this->objProduct->getProductOfferingDetails($this->offeringId);
        $this->offeringDetails = $offeringDetails['response_code'] == '1' ? $offeringDetails['respose_array'] : array();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * Method to validate the Specications
     *
     * @return boolean true|false
     * @author Rajul.Mondal
     * @since Jun 10, 2016
     */
    private function validateSpecications()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to validate the Specications');
        if (empty($this->offeringDetails)) {
            $this->setError('3013', 'Offering details is empty');
            return false;
        }

        $apiSpecifications = $this->apiPmsData['specification_details'];
        $pmsSpecifications = $this->offeringDetails['specification_details'];
        $isValid           = true;

        if (empty($apiSpecifications)) {
            $this->setError('3013', 'Specification details is empty');
            $isValid = false;
        } elseif (!$this->specificationExistInPms()) {
            $isValid = false;
        } elseif (!$this->baseSpecificationValidation()) {
            $this->setError('3010', '');
            $isValid = false;
        } else {
            foreach ($apiSpecifications as $specification_details) {
                $this->specificationId = $specification_details['specification_id'];
                if (!empty($this->specificationId)) {
                    $this->apiSpecificationDetails = $specification_details;
                    $this->pmsSpecificationDetails = $pmsSpecifications[$this->specificationId];
                    $this->apiContractCurrency     = $specification_details['cust_contract_currency'];
                    $this->pmsChargeDetails        = $this->offeringDetails['charge_details'];
                    $this->pmsChargeMapping        = $this->offeringDetails['charge_mapping'];

                    if (!$this->validateCharacteristics()) {
                        $isValid = false;
                    }

                    if (!$this->validateRules()) {
                        $isValid = false;
                    }

                    if (!$this->validatePricepoints()) {
                        $isValid = false;
                    }
                }
            }

        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return $isValid;
    }

    /**
     * Method to validate the specification with pms
     *
     * @return boolean true|false
     * @author Rajul.Mondal
     * @since Aug 04, 2016
     */
    private function specificationExistInPms()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to validate the specification with pms');
        $apiSpecifications = $this->apiPmsData['specification_details'];
        $pmsSpecifications = $this->offeringDetails['specification_details'];
        $isValid           = true;

        foreach ($apiSpecifications as $specification_details) {
            $specificationId = $specification_details['specification_id'];
            if (!array_key_exists($specificationId, $pmsSpecifications)) {
                $this->setError('3013', 'Invalid specification id #' . $this->formatPMSID($specificationId, 1));
                $isValid = false;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return $isValid;
    }

    /**
     * Method to validate the base specification
     *
     * @return boolean true|false
     * @author Rajul.Mondal
     * @since Jun 15, 2016
     */
    private function baseSpecificationValidation()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to validate the base specification');
        $apiSpecifications = $this->apiPmsData['specification_details'];
        $pmsSpecifications = $this->offeringDetails['specification_details'];
        $isValid           = false;

        foreach ($apiSpecifications as $specification_details) {
            $specificationId = $specification_details['specification_id'];
            if ($pmsSpecifications[$specificationId]['spectypename'] == 'Base') {
                $isValid = true;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return $isValid;
    }

    /**
     * Method to validate the Specications
     *
     * @return boolean true|false
     * @author Rajul.Mondal
     * @since Jun 13, 2016
     */
    private function validateCharacteristics()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to validate the Specications');
        $apiCharacteristics = $this->apiSpecificationDetails['characteristics'];
        $pmsCharacteristics = $this->pmsSpecificationDetails['characteristics'];

        $pmsCharacteristicsIds = array();
        foreach ($pmsCharacteristics as $pmsCharacteristicKey => $pmsCharacteristic) {
            $pmsCharacteristicsIds[] = $pmsCharacteristicKey;
        }
        $apiCharacteristicsIds = array();
        foreach ($apiCharacteristics as $apiCharacteristic) {
            $apiCharacteristicsIds[] = $apiCharacteristic['characteristic_id'];
            if (!empty($apiCharacteristic['characteristic_id']) && !in_array($apiCharacteristic['characteristic_id'], $pmsCharacteristicsIds)) {
                $this->setError('3013', 'Invalid characteristics id #' . $this->formatPMSID($apiCharacteristic['characteristic_id'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
                return false;
            }
        }
        foreach ($pmsCharacteristics as $pmsCharacteristicKey => $pmsCharacteristic) {
            if (!in_array($pmsCharacteristicKey, $apiCharacteristicsIds) && !empty($pmsCharacteristicKey) && $pmsCharacteristic['ismandatory'] == 'true') {
                $this->setError('3013', 'Invalid characteristics for specification id #' . $this->formatPMSID($this->specificationId, 1));
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
                return false;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return $this->validateCharacteristicsValues();
    }

    /**
     * Method to validate the Specications
     *
     * @return boolean true|false
     * @author Rajul.Mondal
     * @since Jun 13, 2016
     */
    private function validateCharacteristicsValues()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to validate the Specications');
        $apiCharacteristics = $this->apiSpecificationDetails['characteristics'];
        $pmsCharacteristics = $this->pmsSpecificationDetails['characteristics'];
        $isValid            = true;

        $apiCharacteristicsIds = array();
        foreach ($apiCharacteristics as $apiCharacteristic) {
            $apiCharacteristicsIds[$apiCharacteristic['characteristic_id']] = array(
                'characteristic_id' => $apiCharacteristic['characteristic_id'],
                'charvalid'         => $apiCharacteristic['charvalid'],
            );
        }

        foreach ($pmsCharacteristics as $pmsCharacteristicKey => $pmsCharacteristic) {
            $pmsCharacteristic['ismandatory'] = true;
            if (array_key_exists($pmsCharacteristicKey, $apiCharacteristicsIds)) {
                $characteristicvalue = !empty($pmsCharacteristic['characteristicvalue']) ? $pmsCharacteristic['characteristicvalue'] : array();
                $charvalid           = (isset($apiCharacteristicsIds[$pmsCharacteristicKey]['charvalid']) && !empty($apiCharacteristicsIds[$pmsCharacteristicKey]['charvalid'])) ? $apiCharacteristicsIds[$pmsCharacteristicKey]['charvalid'] : '0';

                // check valid charvalid
                if (!empty($characteristicvalue) && $charvalid != '') {
                    if (!array_key_exists($charvalid, $characteristicvalue)) {
                        $this->setError('3013', 'Invalid characteristic value on characteristic id #' . $this->formatPMSID($pmsCharacteristicKey, 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                        $isValid = false;
                    }
                }

                $valTypeRange = array();
                if ($pmsCharacteristic['valtypeid'] === '2') {
                    // generating available characteristic value list
                    for ($i = $pmsCharacteristic['lowervalue']; $i <= $pmsCharacteristic['uppervalue']; $i += $pmsCharacteristic['interval']) {
                        $valTypeRange[] = $i;
                    }

                    // if [valtypeid] = 2; check valtypeid is empty or not
                    if ($charvalid == '') {
                        $this->setError('3013', 'Characteristic value must not be empty on characteristic id #' . $this->formatPMSID($pmsCharacteristicKey, 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                        $isValid = false;
                    }

                    // if [valtypeid] = 2; check valtypeid is in range or not
                    elseif (!in_array($charvalid, $valTypeRange)) {
                        $this->setError('3013', 'Characteristic value must be in range on characteristic id #' . $this->formatPMSID($pmsCharacteristicKey, 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                        $isValid = false;
                    }
                } else if ($pmsCharacteristic['valtypeid'] === '1' && is_array($pmsCharacteristic['characteristicvalue']) && !empty($pmsCharacteristic['characteristicvalue'])) {
                    // generating available characteristic value list
                    foreach ($pmsCharacteristic['characteristicvalue'] as $characteristicvalue_key => $characteristicvalue) {
                        $valTypeRange[] = $characteristicvalue_key;
                    }

                    // if [valtypeid] = 1; check valtypeid is from the value of characteristicvalue list
                    if (!in_array($charvalid, $valTypeRange)) {
                        $this->setError('3013', 'Invalid characteristic value on characteristic id #' . $this->formatPMSID($pmsCharacteristicKey, 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                        $isValid = false;
                    }
                }
            } else {
                $apiCharacteristicsIds[$pmsCharacteristicKey] = array(
                    'characteristic_id' => (string) $pmsCharacteristicKey,
                    'charvalid'         => '',
                );
            }
        }

        // to populate missing apiCharacteristics in result xml
        $this->apiCharacteristicsIds[$this->specificationId] = $apiCharacteristicsIds;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return $isValid;
    }

    /**
     * Method to validate the Rules
     *
     * @return boolean true|false
     * @author Rajul.Mondal
     * @since Jun 13, 2016
     */
    private function validateRules()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to validate the Rules');
        $apirules = $this->apiSpecificationDetails['rules'];
        $pmsrules = $this->pmsSpecificationDetails['rules'];

        $pmsRulesIds = array();
        foreach ($pmsrules as $pmsruleKey => $pmsrule) {
            $pmsRulesIds[] = $pmsruleKey;

        }
        $apiRulesIds = array();
        foreach ($apirules as $apirule) {
            $apiRulesIds[] = $apirule['rule_id'];
            if (empty($apirule['rule_id'])) {
                $this->setError('3013', 'Invalid rule for specification id #' . $this->formatPMSID($this->specificationId, 1));
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
                return false;
            } elseif (!in_array($apirule['rule_id'], $pmsRulesIds)) {
                $this->setError('3013', 'Invalid rule id #' . $this->formatPMSID($apirule['rule_id'], 2) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
                return false;
            }
        }
        foreach ($pmsrules as $pmsruleKey => $pmsrule) {
            if (!in_array($pmsruleKey, $apiRulesIds) && $pmsrule['ismandatory'] == 'true') {
                $this->setError('3013', 'Invalid rule for specification id #' . $this->formatPMSID($this->specificationId, 1));
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
                return false;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return $this->validateRuleValues();
    }

    /**
     * Method to validate the Rule Values
     *
     * @return boolean true|false
     * @author Rajul.Mondal
     * @since Jun 13, 2016
     */
    private function validateRuleValues()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to validate the Rule Values');
        $apirules = $this->apiSpecificationDetails['rules'];
        $pmsrules = $this->pmsSpecificationDetails['rules'];
        $isValid  = true;

        $apiRulesIds = array();
        foreach ($apirules as $apirule) {
            $apiRulesIds[$apirule['rule_id']] = array(
                'rule_id'   => $apirule['rule_id'],
                'rulevalue' => $apirule['rulevalue'],
            );
        }
        foreach ($pmsrules as $pmsruleKey => $pmsrule) {
            if (array_key_exists($pmsruleKey, $apiRulesIds)) {
                $ismandatory = !empty($pmsrule['ismandatory']) ? $pmsrule['ismandatory'] : array();
                $rulevalue   = (isset($apiRulesIds[$pmsruleKey]['rulevalue']) && !empty($apiRulesIds[$pmsruleKey]['rulevalue'])) ? $apiRulesIds[$pmsruleKey]['rulevalue'] : '';
                if ($ismandatory === 'true' && $rulevalue === '') {
                    $this->setError('3013', 'Rule value is mandatory for #' . $this->formatPMSID($pmsruleKey, 2));
                    $isValid = false;
                }
            } else {
                $apiRulesIds[$pmsruleKey] = array(
                    'rule_id'   => (string) $pmsruleKey,
                    'rulevalue' => '',
                );
            }
        }

        // to populate missing apiRulesIds in result xml
        $this->apiRulesIds[$this->specificationId] = $apiRulesIds;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return $isValid;
    }

    /**
     * Method to validate the Pricepoints
     *
     * @return boolean true|false
     * @author Rajul.Mondal
     * @since Jun 14, 2016
     */
    private function validatePricepoints()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to validate the Pricepoints');
        $apiPricePoints   = $this->apiSpecificationDetails['pricepoints'];
        $pmsChargeMapping = (isset($this->pmsChargeMapping[$this->specificationId])) ? $this->pmsChargeMapping[$this->specificationId] : array();
        $pmsChargeDetails = $this->pmsChargeDetails;
        $isValid          = true;

        // get post selected characteristic
        $selectedCharacteristic = [];
        if (!empty($this->apiSpecificationDetails['characteristics'])) {
            foreach ($this->apiSpecificationDetails['characteristics'] as $characteristic) {
                if ($this->pmsSpecificationDetails['characteristics'][$characteristic['characteristic_id']]['valtypeid'] != 2) {
                    $selectedCharacteristic[] = $characteristic['charvalid'];
                }
            }
        }

        $pmsChargeIds = array();
        if (!empty($pmsChargeMapping)) {
            foreach ($pmsChargeMapping as $mapping) {
                // if no Contract Currency && no charge mapping charvalid
                if (empty($this->apiContractCurrency) && empty($mapping['charvalid'])) {
                    $pmsChargeIds[] = $mapping['pricelineid'];
                }
                // if there is a Contract Currency && no charge mapping charvalid
                else if (!empty($this->apiContractCurrency) && ($this->apiContractCurrency === $mapping['currency'] || $pmsChargeDetails[$mapping['pricelineid']]['isCustomPrice'] == 'true') && empty($mapping['charvalid'])) {
                    $pmsChargeIds[] = $mapping['pricelineid'];
                }
                // if no Contract Currency && filter selected characteristic
                else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && empty($this->apiContractCurrency) && count($mapping['charvalid']) == 1) {
                    $pmsChargeIds[] = $mapping['pricelineid'];
                }
                // if there is a Contract Currency && filter selected characteristic
                else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && !empty($this->apiContractCurrency) && ($this->apiContractCurrency === $mapping['currency'] || $pmsChargeDetails[$mapping['pricelineid']]['isCustomPrice'] == 'true') && count($mapping['charvalid']) == 1) {
                    $pmsChargeIds[] = $mapping['pricelineid'];
                }
                // if no Contract Currency && filter selected characteristic
                else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && empty($this->apiContractCurrency) && count($mapping['charvalid']) == count($selectedCharacteristic)) {
                    $pmsChargeIds[] = $mapping['pricelineid'];
                }
                // if there is a Contract Currency && filter selected characteristic
                else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && !empty($this->apiContractCurrency) && ($this->apiContractCurrency === $mapping['currency'] || $pmsChargeDetails[$mapping['pricelineid']]['isCustomPrice'] == 'true') && count($mapping['charvalid']) == count($selectedCharacteristic)) {
                    $pmsChargeIds[] = $mapping['pricelineid'];
                }
            }
        }
        // validate if there is missing or invalid proce block in the xml data
        if (!empty($apiPricePoints)) {
            foreach ($apiPricePoints as $apiPricePointKey => $apiPricePoint) {
                // pricelineid is empty
                if (empty($apiPricePointKey)) {
                    $this->setError('3013', 'Invalid Charge for specification id #' . $this->formatPMSID($this->specificationId, 1));
                    $isValid = false;
                }
                // pricelineid is exist in pms or not
                elseif (!in_array($apiPricePointKey, $pmsChargeIds)) {
                    $this->setError('3013', 'Invalid Charge id #' . $this->formatPMSID($apiPricePointKey, 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                    $isValid = false;
                }
            }
        }
        // validate if proce block is missing as per as PMS data
        if (!empty($pmsChargeIds)) {
            foreach ($pmsChargeIds as $pmsChargeId) {
                if (!array_key_exists($pmsChargeId, $apiPricePoints)) {
                    $this->setError('3013', 'Invalid Charge for specification id #' . $this->formatPMSID($this->specificationId, 1));
                    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
                    return false;
                }
            }
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        if (!$isValid) {
            return false;
        }

        return $this->validatePricepointValues();
    }

    /**
     * Method to validate the Pricepoint Values
     *
     * @return boolean true|false
     * @author Rajul.Mondal
     * @since Jun 14, 2016
     */
    private function validatePricepointValues()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to validate the Pricepoint Values');
        $apiPricePoints   = $this->apiSpecificationDetails['pricepoints'];
        $pmsChargeDetails = $this->pmsChargeDetails;
        $pmsChargeMapping = (isset($this->pmsChargeMapping[$this->specificationId])) ? $this->pmsChargeMapping[$this->specificationId] : array();
        $isValid          = true;

        // get post selected characteristic
        $selectedCharacteristic = [];
        if (!empty($this->apiSpecificationDetails['characteristics'])) {
            foreach ($this->apiSpecificationDetails['characteristics'] as $characteristic) {
                if ($this->pmsSpecificationDetails['characteristics'][$characteristic['characteristic_id']]['valtypeid'] != 2) {
                    $selectedCharacteristic[] = $characteristic['charvalid'];
                }
            }
        }

        $pmsChargeIds = array();
        if (!empty($pmsChargeMapping)) {
            foreach ($pmsChargeMapping as $mapping) {
                // if no Contract Currency && no charge mapping charvalid
                if (empty($this->apiContractCurrency) && empty($mapping['charvalid'])) {
                    $pmsChargeIds[] = $mapping['pricelineid'];
                }
                // if there is a Contract Currency && no charge mapping charvalid
                else if (!empty($this->apiContractCurrency) && ($this->apiContractCurrency === $mapping['currency'] || $pmsChargeDetails[$mapping['pricelineid']]['isCustomPrice'] == 'true') && empty($mapping['charvalid'])) {
                    $pmsChargeIds[] = $mapping['pricelineid'];
                }
                // if no Contract Currency && filter selected characteristic
                else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && empty($this->apiContractCurrency) && count($mapping['charvalid']) == 1) {
                    $pmsChargeIds[] = $mapping['pricelineid'];
                }
                // if there is a Contract Currency && filter selected characteristic
                else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && !empty($this->apiContractCurrency) && ($this->apiContractCurrency === $mapping['currency'] || $pmsChargeDetails[$mapping['pricelineid']]['isCustomPrice'] == 'true') && count($mapping['charvalid']) == 1) {
                    $pmsChargeIds[] = $mapping['pricelineid'];
                }
                // if no Contract Currency && filter selected characteristic
                else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && empty($this->apiContractCurrency) && count($mapping['charvalid']) == count($selectedCharacteristic)) {
                    $pmsChargeIds[] = $mapping['pricelineid'];
                }
                // if there is a Contract Currency && filter selected characteristic
                else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && !empty($this->apiContractCurrency) && ($this->apiContractCurrency === $mapping['currency'] || $pmsChargeDetails[$mapping['pricelineid']]['isCustomPrice'] == 'true') && count($mapping['charvalid']) == count($selectedCharacteristic)) {
                    $pmsChargeIds[] = $mapping['pricelineid'];
                }
            }
        }

        if (!empty($pmsChargeMapping)) {
            foreach ($pmsChargeMapping as $mapping) {
                if (in_array($mapping['pricelineid'], $pmsChargeIds)) {
                    $pmsDetails = $pmsChargeDetails[$mapping['pricelineid']];
                    $apiDetails = $apiPricePoints[$pmsDetails['pricelineid']];

                    $list_price              = isset($apiDetails['list_price']) ? $apiDetails['list_price'] : '0';
                    $discount_rate           = isset($apiDetails['discount_rate']) ? $apiDetails['discount_rate'] : '0';
                    $discount_amount         = isset($apiDetails['discount_amount']) ? $apiDetails['discount_amount'] : '0';
                    $customer_contract_price = isset($apiDetails['customer_contract_price']) ? $apiDetails['customer_contract_price'] : '0';

                    // if version up
                    if ($this->is_new_registration == false && $apiDetails['is_new_record'] !== '1') {

                        // charge id is empty
                        if (empty($apiDetails['chargeid'])) {
                            $this->setError('3013', 'Invalid Charge for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // charge id same or not
                        elseif ($pmsDetails['pricelineid'] !== $apiDetails['chargeid']) {
                            $this->setError('3013', 'Invalid Charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // charge type empty or not
                        if (empty($apiDetails['charge_type'])) {
                            $this->setError('3013', 'Charge type is empty on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // charge type same or not
                        else if ($pmsDetails['chargeid'] !== $apiDetails['charge_type']) {
                            $this->setError('3013', 'Invalid Charge type "' . $apiDetails['charge_type'] . '" on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // charge period same or not
                        if (empty($apiDetails['charge_period'])) {
                            $this->setError('3013', 'Charge period is empty on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // charge period same or not
                        else if ($pmsDetails['chargeperiodid'] !== $apiDetails['charge_period']) {
                            $this->setError('3013', 'Invalid Charge period "' . $apiDetails['charge_period'] . '" on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // list price must not be empty
                        if ($apiDetails['list_price'] == '' && $pmsDetails['isCustomPrice'] != 'true') {
                            $this->setError('3013', 'List price must not be empty on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // list price same or not
                        elseif ((float) $pmsDetails['offeringprice'] !== (float) $apiDetails['list_price'] && $pmsDetails['isCustomPrice'] != 'true') {
                            $this->setError('3013', 'Invalid list price on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // list price must be 16digits with dot
                        elseif (!filter_var($apiDetails['list_price'], FILTER_VALIDATE_FLOAT) && (float) $apiDetails['list_price'] != 0 && $pmsDetails['isCustomPrice'] != 'true') {
                            $this->setError('3013', 'Invalid list price on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // list price must not be empty
                        if ($apiDetails['customer_contract_price'] === '') {
                            $this->setError('3013', 'Contract contract price must not be empty on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // customer contract price is -ve
                        elseif ((float) $apiDetails['customer_contract_price'] < 0) {
                            $this->setError('3013', 'Customer contract price should not be negative on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // customer contract price must be 16digits with dot
                        elseif (!filter_var($apiDetails['customer_contract_price'], FILTER_VALIDATE_FLOAT) && (float) $apiDetails['customer_contract_price'] != 0) {
                            $this->setError('3013', 'Invalid contract contract price on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        if ($apiDetails['icb_flag'] != $pmsDetails['isCustomPrice']) {
                            $this->setError('3013', 'Invalid icb_flag on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        if ($apiDetails['currencyid'] != $pmsDetails['currencyid']) {
                            $this->setError('3013', 'Invalid currency id on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // IGC settlement price is -ve
                        if ((float) $apiDetails['igc_settlement_price'] < 0) {
                            $this->setError('3013', 'IGC settlement price should not be negative on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // IGC settlement price must be 16digits with dot
                        else if (!filter_var($apiDetails['igc_settlement_price'], FILTER_VALIDATE_FLOAT) && (float) $apiDetails['igc_settlement_price'] != 0) {
                            $this->setError('3013', 'Invalid IGC settlement price on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // if isCustomPrice true discount will not work
                        if ($pmsDetails['isCustomPrice'] != 'true') {

                            // if both discount rate & discount amount not be empty
                            if ((int) $apiDetails['discount_rate'] != 0 && (float) $apiDetails['discount_amount'] != 0) {
                                $this->setError('3013', 'Discount rate and Discount amount should not be exist at same time on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                $isValid = false;
                            }

                            // discount amount or rate is -ve
                            else if ((float) $apiDetails['discount_amount'] < 0 || (int) $apiDetails['discount_rate'] < 0) {
                                $this->setError('3013', 'Discount should not be negative on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                $isValid = false;
                            }

                            // discount rate must be less the 100
                            else if ((float) $apiDetails['discount_amount'] == 0 && (int) $apiDetails['discount_rate'] > 100) {
                                $this->setError('3013', 'Discount rate must in range (0~100) on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                $isValid = false;
                            }

                            // discount amount must be less than offering price
                            else if ((int) $apiDetails['discount_rate'] == 0 && (float) $apiDetails['discount_amount'] > (float) $apiDetails['list_price']) {
                                $this->setError('3013', 'Discount amount must be less than list price on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                $isValid = false;
                            } else {
                                // calculating desire price according to discount
                                $desirePrice = 0;
                                if ((int) $discount_rate != 0 && (float) $customer_contract_price != 0) {
                                    $desirePrice = ($discount_rate / 100) * $list_price;
                                    $desirePrice = $list_price - $desirePrice;
                                } else if ((float) $discount_amount != 0 && (float) $customer_contract_price != 0) {
                                    $desirePrice = $list_price - $discount_amount;
                                } else if ((int) $discount_rate == 0 && (float) $discount_amount == 0) {
                                    $desirePrice = $list_price;
                                }

                                // customer contract price is valid or not for discount percent
                                if ((float) $desirePrice !== (float) $customer_contract_price) {
                                    $this->setError('3013', 'Invalid customer contract price on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                    $isValid = false;
                                }
                            }

                        } else {

                            // list price exist && ICB = true
                            if ($apiDetails['list_price'] != '') {
                                $this->setError('3013', 'List price should not be exist for charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                $isValid = false;
                            }

                            // if both discount rate & discount amount not be empty
                            if ($apiDetails['discount_rate'] != '' || $apiDetails['discount_amount'] != '') {
                                $this->setError('3013', 'Discount rate or Discount amount should not be exist for charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                $isValid = false;
                            }
                        }

                    }

                    // if new contract registration
                    else {
                        // if charge id is empty assign value from pms
                        if (empty($apiDetails['chargeid'])) {
                            $apiDetails['chargeid'] = $pmsDetails['pricelineid'];

                        }

                        // charge id same or not
                        elseif ($pmsDetails['pricelineid'] !== $apiDetails['chargeid']) {
                            $this->setError('3013', 'Invalid Charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // if charge type empty assign value from pms
                        if (empty($apiDetails['charge_type'])) {
                            $apiDetails['charge_type'] = $pmsDetails['chargeid'];
                        }

                        // charge type same or not
                        else if ($pmsDetails['chargeid'] !== $apiDetails['charge_type']) {
                            $this->setError('3013', 'Invalid Charge type "' . $apiDetails['charge_type'] . '" on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // if charge period empty assign value from pms
                        if (empty($apiDetails['charge_period'])) {
                            $apiDetails['charge_period'] = $pmsDetails['chargeperiodid'];
                        }

                        // charge period same or not
                        else if ($pmsDetails['chargeperiodid'] !== $apiDetails['charge_period']) {
                            $this->setError('3013', 'Invalid Charge period "' . $apiDetails['charge_period'] . '" on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // if list price empty assign value from pms
                        if ($apiDetails['list_price'] == '' && $pmsDetails['isCustomPrice'] != 'true') {
                            $apiDetails['list_price'] = $list_price = (float) $pmsDetails['offeringprice'];
                        }

                        // list price same or not
                        elseif ((float) $pmsDetails['offeringprice'] !== (float) $apiDetails['list_price'] && $pmsDetails['isCustomPrice'] != 'true') {
                            $this->setError('3013', 'Invalid list price on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // list price must be 16digits with dot
                        elseif (!filter_var($apiDetails['list_price'], FILTER_VALIDATE_FLOAT) && (float) $apiDetails['list_price'] != 0 && $pmsDetails['isCustomPrice'] != 'true') {
                            $this->setError('3013', 'Invalid list price on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // customer contract price must not be empty
                        if ($apiDetails['customer_contract_price'] === '' && ($apiDetails['discount_rate'] !== '' || $apiDetails['discount_amount'] !== '')) {
                            $this->setError('3013', 'Contract contract price must not be empty on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // customer contract price is -ve
                        elseif ((float) $apiDetails['customer_contract_price'] < 0) {
                            $this->setError('3013', 'Customer contract price should not be negative on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // customer contract price must be 16digits with dot
                        elseif (!filter_var($apiDetails['customer_contract_price'], FILTER_VALIDATE_FLOAT) && (float) $apiDetails['customer_contract_price'] != 0) {
                            $this->setError('3013', 'Invalid contract contract price on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // icb_flag empty
                        if (empty($apiDetails['icb_flag'])) {
                            $apiDetails['icb_flag'] = $pmsDetails['isCustomPrice'];
                        }

                        // icb_flag must be equal to pms value
                        elseif ($apiDetails['icb_flag'] != $pmsDetails['isCustomPrice']) {
                            $this->setError('3013', 'Invalid icb_flag on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // if currency id empty assign value from pms
                        if (empty($apiDetails['currencyid'])) {
                            $apiDetails['currencyid'] = $pmsDetails['currencyid'];
                        }

                        // currency id must be equal to pms value
                        elseif ($apiDetails['currencyid'] != $pmsDetails['currencyid']) {
                            $this->setError('3013', 'Invalid currency id on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // IGC settlement price is -ve
                        if ((float) $apiDetails['igc_settlement_price'] < 0) {
                            $this->setError('3013', 'IGC settlement price should not be negative on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // IGC settlement price must be 16digits with dot
                        else if (!filter_var($apiDetails['igc_settlement_price'], FILTER_VALIDATE_FLOAT) && (float) $apiDetails['igc_settlement_price'] != '0') {
                            $this->setError('3013', 'Invalid IGC settlement price on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                            $isValid = false;
                        }

                        // if isCustomPrice true discount will not work
                        if ($pmsDetails['isCustomPrice'] != 'true') {

                            // if both discount rate & discount amount not be empty
                            if ((int) $apiDetails['discount_rate'] != 0 && (float) $apiDetails['discount_amount'] != 0) {
                                $this->setError('3013', 'Discount rate and Discount amount should not be exist at same time on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                $isValid = false;
                            }

                            // discount rate must be less the 100
                            else if ((float) $apiDetails['discount_amount'] == 0 && (int) $apiDetails['discount_rate'] > 100) {
                                $this->setError('3013', 'Discount rate must in range (0~100) on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                $isValid = false;
                            }

                            // discount amount or rate is -ve
                            else if ((float) $apiDetails['discount_amount'] < 0 || (int) $apiDetails['discount_rate'] < 0) {
                                $this->setError('3013', 'Discount should not be negative on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                $isValid = false;
                            }

                            // discount amount must be less than offering price
                            else if ((int) $apiDetails['discount_rate'] == 0 && (float) $apiDetails['discount_amount'] > (float) $apiDetails['list_price']) {
                                $this->setError('3013', 'Discount amount must be less than list price on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                $isValid = false;
                            } else {
                                // calculating desire price according to discount
                                $desirePrice = 0;
                                if ((int) $discount_rate != 0) {
                                    $desirePrice = ($discount_rate / 100) * $list_price;
                                    $desirePrice = $list_price - $desirePrice;
                                } else if ((float) $discount_amount != 0) {
                                    $desirePrice = $list_price - $discount_amount;
                                } else if ((int) $discount_rate == 0 && (float) $discount_amount == 0) {
                                    $desirePrice = $list_price;
                                }

                                // if customer contract price is empty assign value
                                if ($apiDetails['customer_contract_price'] === '') {
                                    $apiDetails['customer_contract_price'] = $customer_contract_price = $desirePrice;
                                }

                                // customer contract price is valid or not for discount percent
                                else if ((float) $desirePrice !== (float) $customer_contract_price) {
                                    $this->setError('3013', 'Invalid customer contract price on charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                    $isValid = false;
                                }
                            }

                        } else {

                            // list price exist && ICB = true
                            if ($apiDetails['list_price'] != '') {
                                $this->setError('3013', 'List price should not be exist for charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                $isValid = false;
                            }

                            // if both discount rate & discount amount exist && ICB = true
                            if ($apiDetails['discount_rate'] != '' || $apiDetails['discount_amount'] != '') {
                                $this->setError('3013', 'Discount rate or Discount amount should not be exist for charge id #' . $this->formatPMSID($apiDetails['chargeid'], 1) . ' for specification id #' . $this->formatPMSID($this->specificationId, 1));
                                $isValid = false;
                            }
                        }
                    }

                    // pms price details to populate api result
                    $this->pmsArrayforPrice[$this->specificationId][$pmsDetails['pricelineid']] = array(
                        'charge_type'             => $pmsDetails['chargeid'],
                        'charge_period'           => $pmsDetails['chargeperiodid'],
                        'list_price'              => (float) $pmsDetails['offeringprice'],
                        'icb_flag'                => $pmsDetails['isCustomPrice'],
                        'percent_discount_flag'   => '0',
                        'discount'                => (float) $apiDetails['discount_amount'],
                        'customer_contract_price' => (float) $apiDetails['customer_contract_price'],
                        'igc_settlement_price'    => (float) $apiDetails['igc_settlement_price'],
                        'currencyid'              => $pmsDetails['currencyid'],

                    );

                    if ($pmsDetails['isCustomPrice'] != 'true' && (int) $apiDetails['discount_rate'] != 0) {
                        $this->pmsArrayforPrice[$this->specificationId][$pmsDetails['pricelineid']]['percent_discount_flag'] = '1';
                        $this->pmsArrayforPrice[$this->specificationId][$pmsDetails['pricelineid']]['discount']              = $apiDetails['discount_rate'];
                    }
                }
            }
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return $isValid;
    }

    /**
     * Method to set error code and descriptions
     *
     * @param $errorCode string error code in string
     * @param $errorMsg string additional message reruired for error message in string
     * @author Rajul.Mondal
     * @since Jun 16, 2016
     */
    private function setError($errorCode, $errorMsg)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'errorCode : ' . $errorCode . GCM_GL_LOG_VAR_SEPARATOR . 'errorMsg : ' . $errorCode, 'Method to set error code and descriptions');
        if ($errorCode != '' || !empty($errorCode)) {
            if (array_key_exists($errorCode, $this->errorMsgs)) {
                if (!isset($this->response['error_desc'][$errorCode])) {
                    if (!in_array($errorCode, $this->response['codes'])) {
                        $this->response['codes'] = array_merge($this->response['codes'], array(
                            $errorCode,
                        ));
                    }
                    $this->response['messages'][$errorCode][] = $this->errorMsgs[$errorCode] . $errorMsg;
                } else {
                    array_push($this->response['messages'][$errorCode], $this->errorMsgs[$errorCode] . $errorMsg);
                }
            } else {
                if (!isset($this->response['messages'][$errorCode])) {
                    if (!in_array($errorCode, $this->response['codes'])) {
                        $this->response['codes'] = array_merge($this->response['codes'], array(
                            null,
                        ));
                    }
                    $this->response['messages'][$errorCode][] = $errorMsg;
                } else {
                    array_push($this->response['messages'][$errorCode], $errorMsg);
                }
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * Method to regenerate the api structure as need
     *
     * @param $apiPmsData PMS data in array
     * @return array reformating array stucture according to the pms data
     * @author Rajul.Mondal
     * @since Jun 16, 2016
     */
    private function regenerateApiData($apiPmsData)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'apiPmsData : ' . print_r($apiPmsData, true), 'Method to regenerate the api structure as need');
        $productXMLDetails = array();
        $lineItems         = array();
        $offeringId        = '';

        if (!empty($apiPmsData) && isset($apiPmsData['ContractDetails'])) {
            if (!empty($apiPmsData['ContractDetails']['ContractsHeader'])) {
                $offeringId                = isset($apiPmsData['ContractDetails']['ContractsHeader']['product_offering']) ? $apiPmsData['ContractDetails']['ContractsHeader']['product_offering'] : '';
                $this->is_new_registration = isset($apiPmsData['ContractDetails']['ContractsHeader']['is_new_record']) ? $apiPmsData['ContractDetails']['ContractsHeader']['is_new_record'] : false;
            }

            if (!empty($apiPmsData['ContractDetails']['ContractLineItemDetails'])) {
                foreach ($apiPmsData['ContractDetails']['ContractLineItemDetails'] as $key => $ContractLineItemDetails) {
                    $lineItems[$key]['specification_id']       = '';
                    $lineItems[$key]['cust_contract_currency'] = '';
                    $lineItems[$key]['is_new_record']          = '';
                    $lineItems[$key]['characteristics']        = array();
                    $lineItems[$key]['rules']                  = array();
                    $lineItems[$key]['pricepoints']            = array();

                    if (isset($ContractLineItemDetails['is_new_record'])) {
                        $lineItems[$key]['is_new_record'] = $ContractLineItemDetails['is_new_record'];
                    }
                    if (isset($ContractLineItemDetails['ContractLineItemHeader']['product_specification'])) {
                        $lineItems[$key]['specification_id'] = $ContractLineItemDetails['ContractLineItemHeader']['product_specification'];
                    }
                    if (isset($ContractLineItemDetails['ContractLineItemHeader']['cust_contract_currency'])) {
                        $lineItems[$key]['cust_contract_currency'] = $ContractLineItemDetails['ContractLineItemHeader']['cust_contract_currency'];
                    }
                    if (isset($ContractLineItemDetails['ContractLineProductConfiguration']) && is_array($ContractLineItemDetails['ContractLineProductConfiguration'])) {
                        foreach ($ContractLineItemDetails['ContractLineProductConfiguration'] as $LineItemConfigKey => $ContractLineProductConfiguration) {
                            $characteristic_id = (isset($ContractLineProductConfiguration['name']) && !empty($ContractLineProductConfiguration['name'])) ? $ContractLineProductConfiguration['name'] : '';

                            $lineItems[$key]['characteristics'][$LineItemConfigKey]['characteristic_id'] = $characteristic_id;
                            $lineItems[$key]['characteristics'][$LineItemConfigKey]['charvalid']         = '';
                            if (isset($ContractLineProductConfiguration['characteristic_value'])) {
                                $lineItems[$key]['characteristics'][$LineItemConfigKey]['charvalid'] = $ContractLineProductConfiguration['characteristic_value'];
                            }
                        }
                    }

                    $lineItems[$key]['rules'] = array();
                    if (isset($ContractLineItemDetails['ContractLineProductRule']) && is_array($ContractLineItemDetails['ContractLineProductRule'])) {
                        foreach ($ContractLineItemDetails['ContractLineProductRule'] as $LineItemRulesKey => $ContractLineProductRule) {
                            $rule_id = (isset($ContractLineProductRule['name']) && !empty($ContractLineProductRule['name'])) ? $ContractLineProductRule['name'] : '';

                            $lineItems[$key]['rules'][$LineItemRulesKey]['rule_id']   = $rule_id;
                            $lineItems[$key]['rules'][$LineItemRulesKey]['rulevalue'] = '';

                            if (isset($ContractLineProductRule['description'])) {
                                $lineItems[$key]['rules'][$LineItemRulesKey]['rulevalue'] = $ContractLineProductRule['description'];
                            }
                        }
                    }

                    $lineItems[$key]['pricepoints'] = array();
                    if (isset($ContractLineItemDetails['ContractLineProductPrice']) && is_array($ContractLineItemDetails['ContractLineProductPrice'])) {
                        foreach ($ContractLineItemDetails['ContractLineProductPrice'] as $ProductPriceKey => $ProductPrice) {
                            $productPriceId = (isset($ProductPrice['name']) && !empty($ProductPrice['name'])) ? $ProductPrice['name'] : $ProductPriceKey;

                            $lineItems[$key]['pricepoints'][$productPriceId]['chargeid']                = $productPriceId;
                            $lineItems[$key]['pricepoints'][$productPriceId]['is_new_record']           = '';
                            $lineItems[$key]['pricepoints'][$productPriceId]['currencyid']              = '';
                            $lineItems[$key]['pricepoints'][$productPriceId]['icb_flag']                = '';
                            $lineItems[$key]['pricepoints'][$productPriceId]['charge_type']             = '';
                            $lineItems[$key]['pricepoints'][$productPriceId]['charge_period']           = '';
                            $lineItems[$key]['pricepoints'][$productPriceId]['list_price']              = '';
                            $lineItems[$key]['pricepoints'][$productPriceId]['discount_rate']           = '';
                            $lineItems[$key]['pricepoints'][$productPriceId]['discount_amount']         = '';
                            $lineItems[$key]['pricepoints'][$productPriceId]['customer_contract_price'] = '';
                            $lineItems[$key]['pricepoints'][$productPriceId]['igc_settlement_price']    = '';
                            $lineItems[$key]['pricepoints'][$productPriceId]['is_new_record']           = isset($ProductPrice['is_new_record']) ? $ProductPrice['is_new_record'] : '';

                            $xml_data = !empty($ProductPrice['xml_data']) ? $ProductPrice['xml_data'] : array();

                            if (!empty($xml_data)) {
                                $lineItems[$key]['pricepoints'][$productPriceId]['currencyid']              = isset($xml_data['currencyid']['xml_value']) ? trim($xml_data['currencyid']['xml_value']) : '';
                                $lineItems[$key]['pricepoints'][$productPriceId]['icb_flag']                = isset($xml_data['icb_flag']['xml_value']) ? trim($xml_data['icb_flag']['xml_value']) : '';
                                $lineItems[$key]['pricepoints'][$productPriceId]['charge_type']             = isset($xml_data['charge_type']['xml_value']) ? trim($xml_data['charge_type']['xml_value']) : '';
                                $lineItems[$key]['pricepoints'][$productPriceId]['charge_period']           = isset($xml_data['charge_period']['xml_value']) ? trim($xml_data['charge_period']['xml_value']) : '';
                                $lineItems[$key]['pricepoints'][$productPriceId]['list_price']              = isset($xml_data['list_price']['xml_value']) ? trim($xml_data['list_price']['xml_value']) : '';
                                $lineItems[$key]['pricepoints'][$productPriceId]['discount_rate']           = isset($xml_data['discount_rate']['xml_value']) ? trim($xml_data['discount_rate']['xml_value']) : '';
                                $lineItems[$key]['pricepoints'][$productPriceId]['discount_amount']         = isset($xml_data['discount_amount']['xml_value']) ? trim($xml_data['discount_amount']['xml_value']) : '';
                                $lineItems[$key]['pricepoints'][$productPriceId]['customer_contract_price'] = isset($xml_data['customer_contract_price']['xml_value']) ? trim($xml_data['customer_contract_price']['xml_value']) : '';
                                $lineItems[$key]['pricepoints'][$productPriceId]['igc_settlement_price']    = isset($xml_data['igc_settlement_price']['xml_value']) ? trim($xml_data['igc_settlement_price']['xml_value']) : '';
                            }
                        }
                    }
                }
            }
        }

        $productXMLDetails['offering_id']           = $offeringId;
        $productXMLDetails['specification_details'] = array();
        foreach ($lineItems as $lineItemKey => $lineItem) {
            $productXMLDetails['specification_details'][$lineItemKey]['is_new_record']          = isset($lineItem['specification_id']) ? trim($lineItem['is_new_record']) : '';
            $productXMLDetails['specification_details'][$lineItemKey]['specification_id']       = isset($lineItem['specification_id']) ? trim($lineItem['specification_id']) : '';
            $productXMLDetails['specification_details'][$lineItemKey]['cust_contract_currency'] = isset($lineItem['cust_contract_currency']) ? trim($lineItem['cust_contract_currency']) : '';
            $productXMLDetails['specification_details'][$lineItemKey]['characteristics']        = array();
            $productXMLDetails['specification_details'][$lineItemKey]['rules']                  = array();
            $productXMLDetails['specification_details'][$lineItemKey]['pricepoints']            = array();

            foreach ($lineItem['characteristics'] as $characteristicsKey => $characteristics) {
                $productXMLDetails['specification_details'][$lineItemKey]['characteristics'][$characteristicsKey]['characteristic_id'] = isset($characteristics['characteristic_id']) ? trim($characteristics['characteristic_id']) : '';
                $productXMLDetails['specification_details'][$lineItemKey]['characteristics'][$characteristicsKey]['charvalid']         = isset($characteristics['charvalid']) ? trim($characteristics['charvalid']) : '';
            }
            foreach ($lineItem['rules'] as $rulesKey => $rules) {
                $productXMLDetails['specification_details'][$lineItemKey]['rules'][$rulesKey]['rule_id']   = isset($rules['rule_id']) ? trim($rules['rule_id']) : '';
                $productXMLDetails['specification_details'][$lineItemKey]['rules'][$rulesKey]['rulevalue'] = isset($rules['rulevalue']) ? trim($rules['rulevalue']) : '';
            }
            foreach ($lineItem['pricepoints'] as $pricepointsKey => $pricepoints) {
                $productXMLDetails['specification_details'][$lineItemKey]['pricepoints'][$pricepointsKey]['chargeid']                = isset($pricepoints['chargeid']) ? trim($pricepoints['chargeid']) : '';
                $productXMLDetails['specification_details'][$lineItemKey]['pricepoints'][$pricepointsKey]['is_new_record']           = isset($pricepoints['is_new_record']) ? trim($pricepoints['is_new_record']) : '';
                $productXMLDetails['specification_details'][$lineItemKey]['pricepoints'][$pricepointsKey]['currencyid']              = isset($pricepoints['currencyid']) ? trim($pricepoints['currencyid']) : '';
                $productXMLDetails['specification_details'][$lineItemKey]['pricepoints'][$pricepointsKey]['icb_flag']                = isset($pricepoints['icb_flag']) ? trim($pricepoints['icb_flag']) : '';
                $productXMLDetails['specification_details'][$lineItemKey]['pricepoints'][$pricepointsKey]['charge_type']             = isset($pricepoints['charge_type']) ? trim($pricepoints['charge_type']) : '';
                $productXMLDetails['specification_details'][$lineItemKey]['pricepoints'][$pricepointsKey]['charge_period']           = isset($pricepoints['charge_period']) ? trim($pricepoints['charge_period']) : '';
                $productXMLDetails['specification_details'][$lineItemKey]['pricepoints'][$pricepointsKey]['list_price']              = isset($pricepoints['list_price']) ? trim($pricepoints['list_price']) : '';
                $productXMLDetails['specification_details'][$lineItemKey]['pricepoints'][$pricepointsKey]['discount_rate']           = isset($pricepoints['discount_rate']) ? trim($pricepoints['discount_rate']) : '';
                $productXMLDetails['specification_details'][$lineItemKey]['pricepoints'][$pricepointsKey]['discount_amount']         = isset($pricepoints['discount_amount']) ? trim($pricepoints['discount_amount']) : '';
                $productXMLDetails['specification_details'][$lineItemKey]['pricepoints'][$pricepointsKey]['customer_contract_price'] = isset($pricepoints['customer_contract_price']) ? trim($pricepoints['customer_contract_price']) : '';
                $productXMLDetails['specification_details'][$lineItemKey]['pricepoints'][$pricepointsKey]['igc_settlement_price']    = isset($pricepoints['igc_settlement_price']) ? trim($pricepoints['igc_settlement_price']) : '';
            }
        }
        $this->apiPmsData = isset($productXMLDetails) ? $productXMLDetails : array();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'apiPmsData : ' . print_r($this->apiPmsData, true), '');
    }

    /**
     * Method to regenerate the api structure excluding xml_data and putting pms data
     *
     * @param $apiPmsData PMS data in array
     * @return array reformating array stucture according to the request api array
     * @author Rajul.Mondal
     * @since Aug 04, 2016
     */
    private function resultApiData($apiPmsData)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'apiPmsData : ' . print_r($apiPmsData, true), 'Method to regenerate the api structure excluding xml_data and putting pms data');
        $data                    = array();
        $data['ContractDetails'] = array(
            'ContractsHeader'             => !empty($apiPmsData['ContractDetails']['ContractsHeader']) ? $apiPmsData['ContractDetails']['ContractsHeader'] : array(),
            'ContractSalesRepresentative' => !empty($apiPmsData['ContractDetails']['ContractSalesRepresentative']) ? $apiPmsData['ContractDetails']['ContractSalesRepresentative'] : array(),
            'BillingAffiliate'            => !empty($apiPmsData['ContractDetails']['BillingAffiliate']) ? $apiPmsData['ContractDetails']['BillingAffiliate'] : array(),
            'ContractLineItemDetails'     => array(),
        );
        $data['Accounts'] = !empty($apiPmsData['Accounts']) ? $apiPmsData['Accounts'] : array();

        $ContractLineItemDetails = array();
        foreach ($apiPmsData['ContractDetails']['ContractLineItemDetails'] as $lineItemKey => $lineItem) {
            $ContractLineItemDetails[$lineItemKey]['is_new_record']                    = !empty(trim($lineItem['is_new_record'])) ? $lineItem['is_new_record'] : '';
            $ContractLineItemDetails[$lineItemKey]['standard_state']                   = !empty(trim($lineItem['standard_state'])) ? $lineItem['standard_state'] : '';
            $ContractLineItemDetails[$lineItemKey]['object_id']                        = !empty(trim($lineItem['object_id'])) ? $lineItem['object_id'] : '';
            $ContractLineItemDetails[$lineItemKey]['object_version']                   = !empty($lineItem['object_version']) ? $lineItem['object_version'] : '';
            $ContractLineItemDetails[$lineItemKey]['ContractLineItems']                = !empty($lineItem['ContractLineItems']) ? $lineItem['ContractLineItems'] : array();
            $ContractLineItemDetails[$lineItemKey]['ContractLineItemHeader']           = !empty($lineItem['ContractLineItemHeader']) ? $lineItem['ContractLineItemHeader'] : array();
            $ContractLineItemDetails[$lineItemKey]['InvoiceGroupDtls']                 = !empty($lineItem['InvoiceGroupDtls']) ? $lineItem['InvoiceGroupDtls'] : array();
            $ContractLineItemDetails[$lineItemKey]['ContractLineProductConfiguration'] = !empty($lineItem['ContractLineProductConfiguration']) ? $lineItem['ContractLineProductConfiguration'] : array();
            $ContractLineItemDetails[$lineItemKey]['ContractLineProductRule']          = !empty($lineItem['ContractLineProductRule']) ? $lineItem['ContractLineProductRule'] : array();
            $ContractLineItemDetails[$lineItemKey]['ContractLineProductPrice']         = array();
            $ContractLineItemDetails[$lineItemKey]['ContractCreditCard']               = isset($lineItem['ContractCreditCard']) ? $lineItem['ContractCreditCard'] : array();
            $ContractLineItemDetails[$lineItemKey]['ContractDirectDeposit']            = isset($lineItem['ContractDirectDeposit']) ? $lineItem['ContractDirectDeposit'] : array();
            $ContractLineItemDetails[$lineItemKey]['ContractAccountTransfer']          = isset($lineItem['ContractAccountTransfer']) ? $lineItem['ContractAccountTransfer'] : array();
            $ContractLineItemDetails[$lineItemKey]['ContractPostalTransfer']           = isset($lineItem['ContractPostalTransfer']) ? $lineItem['ContractPostalTransfer'] : array();

            // array to store post request xml's ProductConfiguration
            $tmpContractLineProductConfiguration = array();
            $ContractLineProductConfigurations   = $ContractLineItemDetails[$lineItemKey]['ContractLineProductConfiguration'];
            foreach ($ContractLineProductConfigurations as $ContractLineProductConfiguration) {
                $tmpContractLineProductConfiguration[] = $ContractLineProductConfiguration['name'];
            }

            // populate product characterishtics those are not present in post XML
            if (!empty($this->apiCharacteristicsIds)) {
                // get all Characteristics for each specification
                foreach ($this->apiCharacteristicsIds as $specificationId => $apiCharacteristics) {
                    // $apiCharacteristics = Characteristics details of $specificationId
                    foreach ($apiCharacteristics as $apisKey => $apiCharacteristic) {
                        // if $specificationId == product_specification id of line item we will populate the characterishtic
                        if (!in_array($apisKey, $tmpContractLineProductConfiguration) && $ContractLineItemDetails[$lineItemKey]['ContractLineItemHeader']['product_specification'] == $specificationId) {
                            $ContractLineItemDetails[$lineItemKey]['ContractLineProductConfiguration'][] = array(
                                "name"                  => (string) $apisKey,
                                "characteristic_value"  => "",
                                "characteristic_xml_id" => "AdditionalFunctions" . $apisKey,
                            );
                        }
                    }
                }
            }

            // array to store post request xml's ProductRule
            $tmpContractLineProductContractLineProductRule = array();
            $ContractLineProductContractLineProductRules   = $ContractLineItemDetails[$lineItemKey]['ContractLineProductRule'];
            foreach ($ContractLineProductContractLineProductRules as $ContractLineProductRule) {
                $tmpContractLineProductContractLineProductRule[] = $ContractLineProductRule['name'];
            }

            // populate product Rules those are not present in post XML
            if (!empty($this->apiRulesIds)) {
                // get all Rules for each specification
                foreach ($this->apiRulesIds as $specificationId => $apiRulesIds) {
                    // $apiRulesIds = Rules details of $specificationId
                    foreach ($apiRulesIds as $apisKey => $apiRule) {
                        // if $specificationId == product_specification id of line item we will populate the characterishtic
                        if (!in_array($apisKey, $tmpContractLineProductContractLineProductRule) && $ContractLineItemDetails[$lineItemKey]['ContractLineItemHeader']['product_specification'] == $specificationId) {
                            $ContractLineItemDetails[$lineItemKey]['ContractLineProductRule'][] = array(
                                "name"        => (string) $apisKey,
                                "description" => "",
                                "rule_xml_id" => "AdditionalRules" . $apisKey,
                            );
                        }
                    }
                }
            }

            $ContractLineProductPrice = array();
            $product_specification    = $lineItem['ContractLineItemHeader']['product_specification'];
            if (isset($lineItem['ContractLineProductPrice'])) {
                foreach ($lineItem['ContractLineProductPrice'] as $priceDetailsKey => $priceDetails) {
                    $pricePointId                                                          = !empty($priceDetails['name']) ? $priceDetails['name'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['name']                    = isset($pricePointId) ? $pricePointId : '';
                    $ContractLineProductPrice[$priceDetailsKey]['is_new_record']           = !empty($priceDetails['is_new_record']) ? $priceDetails['is_new_record'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['charge_type']             = !empty($this->pmsArrayforPrice[$product_specification][$pricePointId]['charge_type']) ? (string) $this->pmsArrayforPrice[$product_specification][$pricePointId]['charge_type'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['charge_period']           = !empty($this->pmsArrayforPrice[$product_specification][$pricePointId]['charge_period']) ? (string) $this->pmsArrayforPrice[$product_specification][$pricePointId]['charge_period'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['list_price']              = !empty($this->pmsArrayforPrice[$product_specification][$pricePointId]['list_price']) ? (string) $this->pmsArrayforPrice[$product_specification][$pricePointId]['list_price'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['icb_flag']                = !empty($this->pmsArrayforPrice[$product_specification][$pricePointId]['icb_flag']) ? (string) $this->pmsArrayforPrice[$product_specification][$pricePointId]['icb_flag'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['percent_discount_flag']   = !empty($this->pmsArrayforPrice[$product_specification][$pricePointId]['percent_discount_flag']) ? (string) $this->pmsArrayforPrice[$product_specification][$pricePointId]['percent_discount_flag'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['discount']                = !empty($this->pmsArrayforPrice[$product_specification][$pricePointId]['discount']) ? (string) $this->pmsArrayforPrice[$product_specification][$pricePointId]['discount'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['customer_contract_price'] = !empty($this->pmsArrayforPrice[$product_specification][$pricePointId]['customer_contract_price']) ? (string) $this->pmsArrayforPrice[$product_specification][$pricePointId]['customer_contract_price'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['igc_settlement_price']    = !empty($this->pmsArrayforPrice[$product_specification][$pricePointId]['igc_settlement_price']) ? (string) $this->pmsArrayforPrice[$product_specification][$pricePointId]['igc_settlement_price'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['currencyid']              = !empty($this->pmsArrayforPrice[$product_specification][$pricePointId]['currencyid']) ? (string) $this->pmsArrayforPrice[$product_specification][$pricePointId]['currencyid'] : '';

                    $ContractLineProductPrice[$priceDetailsKey]['charge_xml_id']                  = !empty($priceDetails['charge_xml_id']) ? $priceDetails['charge_xml_id'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['charge_type_xml_id']             = !empty($priceDetails['xml_data']['charge_type']['xml_id']) ? $priceDetails['xml_data']['charge_type']['xml_id'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['charge_period_xml_id']           = !empty($priceDetails['xml_data']['charge_period']['xml_id']) ? $priceDetails['xml_data']['charge_period']['xml_id'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['list_price_xml_id']              = !empty($priceDetails['xml_data']['list_price']['xml_id']) ? $priceDetails['xml_data']['list_price']['xml_id'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['icb_flag_xml_id']                = !empty($priceDetails['xml_data']['icb_flag']['xml_id']) ? $priceDetails['xml_data']['icb_flag']['xml_id'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['discount_rate_xml_id']           = !empty($priceDetails['xml_data']['discount_rate']['xml_id']) ? $priceDetails['xml_data']['discount_rate']['xml_id'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['discount_amount_xml_id']         = !empty($priceDetails['xml_data']['discount_amount']['xml_id']) ? $priceDetails['xml_data']['discount_amount']['xml_id'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['customer_contract_price_xml_id'] = !empty($priceDetails['xml_data']['customer_contract_price']['xml_id']) ? $priceDetails['xml_data']['customer_contract_price']['xml_id'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['igc_settlement_price_xml_id']    = !empty($priceDetails['xml_data']['igc_settlement_price']['xml_id']) ? $priceDetails['xml_data']['igc_settlement_price']['xml_id'] : '';
                    $ContractLineProductPrice[$priceDetailsKey]['currencyid_xml_id']              = !empty($priceDetails['xml_data']['currencyid']['xml_id']) ? $priceDetails['xml_data']['currencyid']['xml_id'] : '';
                }
            }
            $ContractLineItemDetails[$lineItemKey]['ContractLineProductPrice'] = isset($ContractLineProductPrice) ? $ContractLineProductPrice : array();
        }
        $data['ContractDetails']['ContractLineItemDetails'] = isset($ContractLineItemDetails) ? $ContractLineItemDetails : array();
        
        // populate missing uuid's from db
        $this->populateMissingXmlUuidFromDB($data);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'data : ' . print_r($data, true), '');
        return $data;
    }

    /**
     * Method to format the PMS ID
     *
     * @param string $pms_id PMS ID string
     * @param int $type Used to indicate the prefix value in the format, the default value is 1
     * @return Formatted PMS ID string value.
     * @author Sathish Kumar Ganesan
     * @since Jun 04, 2016
     */
    public function formatPMSID($pms_id, $type)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'pms_id : ' . $pms_id . GCM_GL_LOG_VAR_SEPARATOR . 'type : ' . $type, 'Method to format the PMS ID');
        $prefix = '00000000-0000-0000-0000-';
        if ($type == 2) {
            $prefix = '00000001-0000-0000-0000-';
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'pms_id : ' . $prefix . sprintf('%012s', $pms_id), '');
        return $prefix . sprintf('%012s', $pms_id);
    }

    /**
     * while doing version of a contract fetch missing uuids for product configs and rules from db
     *
     * @param array $xml_data xml data array
     * @return boolean
     * @author Rajul.Mondal
     * @since June 7, 2017
     */
    private function populateMissingXmlUuidFromDB(&$xml_data)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'while doing version of a contract fetch missing uuids for product configs and rules from db');

        $modUtils   = new ModUtils();
        $line_items = $xml_data['ContractDetails']['ContractLineItemDetails'];

        // skip if post request id not a version up
        if ($this->is_new_registration) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
            return false;
        }
        // skip if there is not line items in the xml
        if (empty($line_items)) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
            return false;
        }

        // populate for each line item
        foreach ($line_items as $line_itemkey => $line_item) {
            $line_item_header        = !empty($line_item['ContractLineItemHeader']) ? $line_item['ContractLineItemHeader'] : array();
            $line_item_configuration = !empty($line_item['ContractLineProductConfiguration']) ? $line_item['ContractLineProductConfiguration'] : array();
            $line_item_rules         = !empty($line_item['ContractLineProductRule']) ? $line_item['ContractLineProductRule'] : array();
            $line_item_id_uuid       = !empty($line_item_header['line_item_id_xml_id']) ? $line_item_header['line_item_id_xml_id'] : '';

            //gc_Line_Item_Contract_History
            $where = array(
                "line_item_id_uuid" => $line_item_id_uuid,
            );
            $gc_Line_Item_Contract_History = BeanFactory::getBean('gc_Line_Item_Contract_History');
            $gc_Line_Item_Contract_History->retrieve_by_string_fields($where);

            // get line_item_id from db
            $line_item_id = $gc_Line_Item_Contract_History->line_item_id;

            //gc_ProductConfiguration
            if (!empty($line_item_configuration)) {
                foreach ($line_item_configuration as $configurationKey => $configuration) {
                    if (!$modUtils->validateUUID($configuration['characteristic_xml_id'])) {
                        $where = array(
                            "line_item_id" => $line_item_id,
                            "name"         => $configuration['name'],
                        );
                        $configuration_bean = BeanFactory::getBean('gc_ProductConfiguration');
                        $configuration_bean->retrieve_by_string_fields($where);

                        // populating configuration uuid from db
                        $xml_data['ContractDetails']['ContractLineItemDetails'][$line_itemkey]['ContractLineProductConfiguration'][$configurationKey]['characteristic_xml_id'] = $configuration_bean->characteristic_uuid;
                    }
                }
            }

            //gc_productrule
            if (!empty($line_item_rules)) {
                foreach ($line_item_rules as $rulesKey => $rules) {
                    if (!$modUtils->validateUUID($configuration['rule_xml_id'])) {
                        $where = array(
                            "line_item_id" => $line_item_id,
                            "name"         => $rules['name'],
                        );
                        $rules_bean = BeanFactory::getBean('gc_ProductRule');
                        $rules_bean->retrieve_by_string_fields($where);

                        // populating rules uuid from db
                        $xml_data['ContractDetails']['ContractLineItemDetails'][$line_itemkey]['ContractLineProductRule'][$rulesKey]['rule_xml_id'] = $rules_bean->rule_uuid;
                    }
                }
            }
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return true;
    }
}
