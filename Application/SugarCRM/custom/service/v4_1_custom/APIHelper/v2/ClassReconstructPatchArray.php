<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (â€œMSAâ€�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
/**
 * ClassReconstructPatchArray to reconstruct parse xml array for save functionality
 * @since : 10-Oct-2017
 * @type unix-file system UTF-8
 */
include_once ('custom/modules/gc_Contracts/CustomFunctionGcContract.php');
require_once 'custom/include/ModUtils.php';

class ClassReconstructPatchArray
{
    /**
    * Payment Type Constants
    */
    const DIRECT_DEPOSIT = '00';
    const ACCOUNT_TRANSFER = '10';
    const POSTAL_TRANSFER = '30';
    const CREDIT_CARD = '60';

    /**
    * Class Variable
    */
	private $modUtils;

    /**
     * Method to convert parse xml array for save functionality
     * @param array $xml_arr Parsed array from request XML
     * @param string $global_contract_id Global contract id of the contract
     * @param string $global_contract_uuid Global contract id uuid of the contract
     * @return array Formated array for save functionality
     * @since : 10-Oct-2017
     */
    public function fnReconstructPatchArray($xml_arr, $global_contract_id, $global_contract_uuid){

        global $sugar_config;

        if(!empty($xml_arr)){
            $modUtils = new ModUtils();

            $contract_header = $xml_arr['ContractDetails']['ContractsHeader'];
            $contract_list = array();
            $where_cond = '';
            if(trim($global_contract_uuid)!=''){
                $where_cond = "global_contract_id_uuid='".$global_contract_uuid."'";
            }elseif(trim($global_contract_id)!=''){
                $where_cond = "global_contract_id='".$global_contract_id."'";
            }
            $contract = BeanFactory::getBean('gc_Contracts');
            $contract_list = $contract->get_full_list('contract_version desc',$where_cond);

            if(!empty($contract_list)){
                    //Sprint 27 end
                    $contract =  $contract->retrieve($contract_list[0]->id);

                    $reconstruct_line_item=array();

                    $CustomFunctionGcContractObj = new CustomFunctionGcContract();
                    $prev_line_item_headers = (isset($contract->id) && $contract->id!='')?$CustomFunctionGcContractObj->getLineItemHeader($contract->id,0,'',false):array();
                    $xml_line_item_headers = !empty($xml_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader']) ? $xml_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader'] : array();
                    $line_item_header = $this->constructLineItemHeader($prev_line_item_headers, $xml_line_item_headers, $contract);
                    if(!empty($line_item_header['contract_header'])){
    					foreach($line_item_header['contract_header'] as $key => $val){
    						$xml_arr['ContractDetails']['ContractsHeader'][$key] = $val;
    					}
                    }

                        $index = 0;
                        $loopstart=0;

                        if(!empty($prev_line_item_headers)){
                            foreach($prev_line_item_headers as $key => $prev_line_item_header){
                                if($key=='order'){
                                    continue;
                                }
                                if(!isset($line_item_header['line_item_header'][$key])){
                                    $line_item_header['line_item_header'][$key]['ContractLineItemHeader'] = array();
                                }
                                $lineitem = $line_item_header['line_item_header'][$key]['ContractLineItemHeader'];
                                $service_start_date = $service_start_time_zone = $service_end_date = $service_end_time_zone = $billing_start_date = $billing_start_time_zone = $billing_end_date = $billing_end_time_zone = '';
                                //service date validation
                                if(isset($lineitem['service_start_date'])){
                                    $service_start_date = $lineitem['service_start_date'];
                                    $service_start_time_zone = (isset($lineitem['service_start_date_timezone']) && $lineitem['service_start_date_timezone']!='') ? $lineitem['service_start_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                                }else if(isset($prev_line_item_header['service_start_date']) && $prev_line_item_header['service_start_date']!=''){
                                    $service_start_date = $prev_line_item_header['service_start_date'];
                                    $service_start_time_zone = (!empty($prev_line_item_header['service_start_date_timezone_actual'])) ? $prev_line_item_header['service_start_date_timezone_actual'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                                    $service_start_date_tmp = new DateTime($service_start_date, new DateTimeZone($service_start_time_zone));
                                    $service_start_date_tmp = $service_start_date_tmp->format('c');
                                    $service_start_date_tmp = $modUtils->ConvertDatetoGMTDate($service_start_date_tmp);
                                    $service_start_date = $service_start_date_tmp['date'];
                                }
                                $service_flag = true;
                                if(isset($lineitem['service_end_date'])){
                                	$service_flag = false;
                                    $service_end_date = $lineitem['service_end_date'];
                                    $service_end_time_zone = (isset($lineitem['service_end_date_timezone']) && $lineitem['service_end_date_timezone']!='') ? $lineitem['service_end_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                                }else if(isset($prev_line_item_header['service_end_date']) && $prev_line_item_header['service_end_date']!=''){
                                    $service_end_date = $prev_line_item_header['service_end_date'];
                                    $service_end_time_zone = (!empty($prev_line_item_header['service_end_date_timezone_actual'])) ? $prev_line_item_header['service_end_date_timezone_actual'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                                    $service_end_date_tmp = new DateTime($service_end_date, new DateTimeZone($service_end_time_zone));
                                    $service_end_date_tmp = $service_end_date_tmp->format('c');
                                    $service_end_date_tmp = $modUtils->ConvertDatetoGMTDate($service_end_date_tmp);
                                    $service_end_date = $service_end_date_tmp['date'];
                                }
                                if ($service_end_date =='' || ($service_flag && strtotime($service_end_date) > strtotime(date('Y-m-d H:i:s')))){
                                    $service_end_date = date('Y-m-d H:i:s');
                                }

                                //billing date validation
                                if(isset($lineitem['billing_start_date'])){
                                    $billing_start_date = $lineitem['billing_start_date'];
                                    $billing_start_time_zone = (isset($lineitem['billing_start_date_timezone']) && $lineitem['billing_start_date_timezone']!='') ? $lineitem['billing_start_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                                }else if(isset($prev_line_item_header['billing_start_date']) && $prev_line_item_header['billing_start_date']!=''){
                                    $billing_start_date = $prev_line_item_header['billing_start_date'];
                                    $billing_start_time_zone = (!empty($prev_line_item_header['billing_start_date_timezone_actual'])) ? $prev_line_item_header['billing_start_date_timezone_actual'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                                    $billing_start_date_tmp = new DateTime($billing_start_date, new DateTimeZone($billing_start_time_zone));
                                    $billing_start_date_tmp = $billing_start_date_tmp->format('c');
                                    $billing_start_date_tmp = $modUtils->ConvertDatetoGMTDate($billing_start_date_tmp);
                                    $billing_start_date = $billing_start_date_tmp['date'];
                                }
                                $billing_flag = true;
                                if(isset($lineitem['billing_end_date'])){
                                	$billing_flag = false;
                                    $billing_end_date = $lineitem['billing_end_date'];
                                }else if(isset($prev_line_item_header['billing_end_date']) && $prev_line_item_header['billing_end_date']!=''){
                                    $billing_end_date = $prev_line_item_header['billing_end_date'];
                                    $billing_end_time_zone = (!empty($prev_line_item_header['billing_end_date_timezone_actual'])) ? $prev_line_item_header['billing_end_date_timezone_actual'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                                    $billing_end_date_tmp = new DateTime($billing_end_date, new DateTimeZone($billing_end_time_zone));
                                    $billing_end_date_tmp = $billing_end_date_tmp->format('c');
                                    $billing_end_date_tmp = $modUtils->ConvertDatetoGMTDate($billing_end_date_tmp);
                                    $billing_end_date = $billing_end_date_tmp['date'];
                                }
                                if($billing_end_date=='' || ($billing_flag && strtotime($billing_end_date) > strtotime(date('Y-m-d H:i:s')))){
                                    $billing_end_date = date('Y-m-d H:i:s');
                                }

                                $reconstruct_line_item[$index]['ContractLineItemHeader'] = $lineitem;
                                $reconstruct_line_item[$index]['ContractLineItemHeader']['line_item_id_xml_id'] = $prev_line_item_header['line_item_id_uuid'];
                                //price
                                if(isset($lineitem['cust_contract_currency'])){
                                    $reconstruct_line_item[$index]['ContractLineItemHeader']['cust_contract_currency_flag'] = 0;
                                    if($lineitem['cust_contract_currency'] != $prev_line_item_header['cust_contract_currency_code']){
                                        $reconstruct_line_item[$index]['ContractLineItemHeader']['cust_contract_currency_flag'] = 1;
                                        // Price array construction function
                                        $price_details = $this->getPMSPriceDetails($contract->product_offering, $prev_line_item_header['product_specification_id'], $lineitem['cust_contract_currency'],$prev_line_item_header['product_config']);
                                        if($price_details !== false) //need to handle false condition
                                            $reconstruct_line_item[$index]['ContractLineProductPrice'] = $price_details;
                                    }
                                }
                                if(isset($line_item_header['line_item_header'][$key]['InvoiceGroupDtls'])){
                                    $reconstruct_line_item[$index]['InvoiceGroupDtls'] = $line_item_header['line_item_header'][$key]['InvoiceGroupDtls'];
                                }
                                unset($lineitem);
                                //payment
                                if(isset($contract_header['payment_type'])){
                                    $reconstruct_line_item[$index]['ContractLineItemHeader']['payment_type'] = $contract_header['payment_type'];
                                    $reconstruct_line_item[$index]['ContractLineItemHeader']['payment_type_flag'] = 0;
                                    if($contract_header['payment_type'] != $prev_line_item_header['payment_type_id']){
                                        $reconstruct_line_item[$index]['ContractLineItemHeader']['payment_type_flag'] = 1;
                                    }
                                    switch ($contract_header['payment_type']) {
                                        case self::DIRECT_DEPOSIT:
                                            $reconstruct_line_item[$index]['ContractDirectDeposit'] = $xml_arr['ContractDetails']['ContractLineItemDetails']['ContractDirectDeposit'];
                                            break;

                                        case self::ACCOUNT_TRANSFER:
                                            $reconstruct_line_item[$index]['ContractAccountTransfer'] = $xml_arr['ContractDetails']['ContractLineItemDetails']['ContractAccountTransfer'];
                                            break;

                                        case self::POSTAL_TRANSFER:
                                            $reconstruct_line_item[$index]['ContractPostalTransfer'] = $xml_arr['ContractDetails']['ContractLineItemDetails']['ContractPostalTransfer'];
                                            break;

                                        case self::CREDIT_CARD:
                                            $reconstruct_line_item[$index]['ContractCreditCard'] = $xml_arr['ContractDetails']['ContractLineItemDetails']['ContractCreditCard'];
                                            break;
                                        default:
                                            // Do nothing
                                            break;
                                    }
                                }
                                $loopstart = 1;
                                $index++;
                            }
                        }
    				$xml_arr['ContractDetails']['ContractLineItemDetails'] = $reconstruct_line_item;
    				$xml_arr['uuid_not_in_db'] = $line_item_header['uuid_not_in_db'];
    				unset($reconstruct_line_item);
            }
		}
        return $xml_arr;
    }

    /**
     * method to construct Line Item Header Array for Save.
     *
     * @param array $db_array contract line item data from db
     * @param array $api_array line item data from api
     * @param string $contract_status
     * @return array $return_array contains reconstruct line item array and uuid list not exist in db
     * @author Sathish Kumar Ganesan
     * @since 05-May-2017
     */
    private function constructLineItemHeader($db_array, $api_array, $contract)
    {
        unset($db_array['order']);
        $return_array = $line_item_header = $uuid_not_in_db = $contract_header = array();
        $index = 'ContractLineItemHeader';
        foreach($api_array as $field_name => $field_values){
            foreach($field_values['xml_id'] as $key => $val){
                $fieldname_key = $field_name;
                if($field_name == 'group_name' || $field_name == 'group_description'){
                    $index = 'InvoiceGroupDtls';
                    $fieldname_key = str_replace('group_','',$field_name);
                }else{
                    $index = 'ContractLineItemHeader';
                }
                if($field_name == 'factory_reference_no' && $val == $contract->factory_reference_no_uuid){
                    $contract_header['factory_reference_no'] = $field_values['val'][$key];
                    $contract_header['factory_reference_no_xml_id'] = $val;
                    continue;
                }
                foreach($db_array as $dbkey => $db_line_item){
                    if((isset($db_line_item[$field_name.'_uuid']) && $val == $db_line_item[$field_name.'_uuid']) || (strpos($field_name,'_timezone')!==false &&  $db_line_item[str_replace('_timezone','',$field_name).'_uuid'] == $val)){
                        $line_item_header[$dbkey][$index][$fieldname_key.'_xml_id'] = $val;
                        $line_item_header[$dbkey][$index][$fieldname_key] = $field_values['val'][$key];
                        continue 2;
                    }
                }
                if(strpos($field_name,'_timezone')==false)
                    $uuid_not_in_db[$field_name][] = $val;
            }
        }
        $return_array['contract_header'] = $contract_header;
        $return_array['line_item_header'] = $line_item_header;
        $return_array['uuid_not_in_db'] = $uuid_not_in_db;

        return $return_array;
    }

    /**
     * method for get Product price details from PMS
     *
     * @param string $offeringId product offering id
     * @param string $specificationId product specification id
     * @param string $currency currency
     * @param array $selectedCharacteristic selected Characteristic ids
     * @return array price details as an array || return false if failed
     * @author Rajul.Mondal
     * @since May 05, 2017
     */
    private function getPMSPriceDetails($offeringId, $specificationId, $currency, $selectedCharacteristic)
    {

        // validate if offeringId or specificationId
        if (empty($offeringId) || empty($specificationId)) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'validate if offeringId or specificationId');
            return false;
        }
        // is $selectedCharacteristic is empty
        if(empty($selectedCharacteristic)) {
            $selectedCharacteristic = array();
        }
        // $selectedCharacteristic must be array
        elseif (!is_array($selectedCharacteristic)) {
            return false;
        }

        require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';
        $objProduct    = new ClassProductConfiguration();
        $offering_list = $objProduct->getProductOfferings(true);

        // validate if there is error in PMS while fetch product offering
        if ($offering_list['response_code'] == '0') {
            return false;
        }
        // validate if offering list is empty
        elseif (empty($offering_list['respose_array'])) {
            return false;
        }
        // proceed...
        else {
            $offeringList = $offering_list['response_code'] == '1' ? $offering_list['respose_array'] : array();
            // if product offering not exist in PMS product offering list
            if (!array_key_exists($offeringId, $offeringList)) {
                return false;
            }

            // get product offering detials from the PMS
            $offeringDetails  = $objProduct->getProductOfferingDetails($offeringId);
            $offeringDetails  = $offeringDetails['response_code'] == '1' ? $offeringDetails['respose_array'] : array();
            $pmsChargeDetails = $offeringDetails['charge_details'];
            $pmsChargeMapping = $offeringDetails['charge_mapping'];
            $pmsChargeMapping = isset($pmsChargeMapping[$specificationId]) ? $pmsChargeMapping[$specificationId] : array();
            $pmsSpeciDetails  = isset($offeringDetails['specification_details'][$specificationId]) ? $offeringDetails['specification_details'][$specificationId] : array();

            // validate if offerdetails is empty for the product offering id
            if (empty($offeringDetails)) {
                return false;
            }

            $pmsSpecifications = $offeringDetails['specification_details'];
            // validate if product specification id not exist in PMS specification details in PMS
            if (!array_key_exists($specificationId, $pmsSpecifications)) {
                return false;
            }

            // extract selectedCharacteristic
            $tmpCharacteristic = array();
            foreach ($selectedCharacteristic as $characteristic) {
                $valtypeid = isset($pmsSpeciDetails['characteristics'][$characteristic['name']]['valtypeid']) ? $pmsSpeciDetails['characteristics'][$characteristic['name']]['valtypeid'] : '';
                if ($characteristic['characteristic_value'] != '' && $valtypeid != 2) {
                    $tmpCharacteristic[] = $characteristic['characteristic_value'];
                }
            }
            $selectedCharacteristic = $tmpCharacteristic;

            // array having price line id accouding to the curreny passed
            $pmsChargeIds = array();
            // proceed if PMS charge mapping is not empty
            if (!empty($pmsChargeMapping)) {
                foreach ($pmsChargeMapping as $mapping) {
                    // if no Contract Currency && no charge mapping charvalid
                    if (empty($currency) && empty($mapping['charvalid'])) {
                        $pmsChargeIds[] = $mapping['pricelineid'];
                    }
                    // if there is a Contract Currency && no charge mapping charvalid
                    else if (!empty($currency) && ($currency === $mapping['currency'] || $pmsChargeDetails[$mapping['pricelineid']]['isCustomPrice'] == 'true') && empty($mapping['charvalid'])) {
                        $pmsChargeIds[] = $mapping['pricelineid'];
                    }
                    // if no Contract Currency && filter selected characteristic
                    else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && empty($currency) && count($mapping['charvalid']) == 1) {
                        $pmsChargeIds[] = $mapping['pricelineid'];
                    }
                    // if there is a Contract Currency && filter selected characteristic
                    else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && !empty($currency) && ($currency === $mapping['currency'] || $pmsChargeDetails[$mapping['pricelineid']]['isCustomPrice'] == 'true') && count($mapping['charvalid']) == 1) {
                        $pmsChargeIds[] = $mapping['pricelineid'];
                    }
                    // if no Contract Currency && filter selected characteristic
                    else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && empty($currency) && count($mapping['charvalid']) == count($selectedCharacteristic)) {
                        $pmsChargeIds[] = $mapping['pricelineid'];
                    }
                    // if there is a Contract Currency && filter selected characteristic
                    else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && !empty($currency) && ($currency === $mapping['currency'] || $pmsChargeDetails[$mapping['pricelineid']]['isCustomPrice'] == 'true') && count($mapping['charvalid']) == count($selectedCharacteristic)) {
                        $pmsChargeIds[] = $mapping['pricelineid'];
                    }
                }
            }

            $priceDetails = array();
            // proceed if PMS charge mapping is not empty
            if (!empty($pmsChargeMapping)) {
                foreach ($pmsChargeMapping as $mapping) {
                    // proceed if price line id is exist in $pmsChargeIds
                    if (in_array($mapping['pricelineid'], $pmsChargeIds)) {
                        $pmsDetails = $pmsChargeDetails[$mapping['pricelineid']];
                        $tmp['name']                           = $pmsDetails['pricelineid']; // name
                        $tmp['charge_type']                    = $pmsDetails['chargeid']; // charge_type
                        $tmp['charge_period']                  = $pmsDetails['chargeperiodid']; // charge_period
                        $tmp['list_price']                     = $pmsDetails['offeringprice']; // list_price
                        $tmp['icb_flag']                       = $pmsDetails['isCustomPrice']; // icb_flag
                        $tmp['currencyid']                     = $pmsDetails['currencyid']; // currencyid
                        $tmp['customer_contract_price']        = $pmsDetails['offeringprice'];
                        $tmp['percent_discount_flag']          = '';
                        $tmp['discount']                       = '';
                        $tmp['igc_settlement_price']           = '';
                        // generate the expected array from the price details
                        $priceDetails[] = $tmp;
                    }
                }
            }
            return $priceDetails;
        }
    }

}
?>
