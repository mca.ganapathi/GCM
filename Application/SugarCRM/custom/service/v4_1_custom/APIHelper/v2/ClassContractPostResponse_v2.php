<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class ClassContractPostResponse_v2
{

    public $xml_schema_path;

    /**
     * xpath object to read and manipulate xml
     *
     * @var object
     */
    private $xpath;

    private $contract_version;

    const LOG_DEBUG = 'debug';

    CONST SCHEMA_DIR = 'api/xml-schema/v2';

    CONST XML_ROOT_TAG = '//contract-info:contract-information';

    public function __construct()
    {
        $site_url = rtrim($GLOBALS['sugar_config']['site_url'], '/');
        $this->xml_schema_path = $site_url . '/' . self::SCHEMA_DIR;
    }

    /**
     * method to prepare contract post response
     *
     * @param string $xml_content xml string
     * @param array $data
     * @return string xml file in string
     *
     * @author sagar.salunkhe
     * @since Jun 14, 2016
     */
    public function getContractPostResponseXML($xml_content, $additional_data)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'xml_content : ' . print_r($xml_content, true) . GCM_GL_LOG_VAR_SEPARATOR . 'additional_data : ' . print_r($additional_data, true), 'method to process response xml from input request xml');

        $dom_element = new DOMDocument();
        $dom_element->preserveWhiteSpace = false;
        $dom_element->formatOutput = true;
        $dom_element->loadXML($xml_content);
        $this->xpath = new DOMXPath($dom_element);

        self::registerPostNamespace($this->xpath);

        $filter_tags = self::getPostFilterTagDetails($additional_data);
        foreach ($filter_tags as $xpath_query => $tag_meta) {
            $search_result = $this->xpath->query(self::XML_ROOT_TAG . $xpath_query);
            foreach ($search_result as $xml_nodes) {
                self::attachPostResponseDataTags($dom_element, $xml_nodes, $tag_meta['attach_tags']);
                self::amplifyPostResponseData($xml_nodes, $tag_meta['set_property']);
                self::detachRedundantXmlData($xml_nodes, $tag_meta['detach_tags']);
            }
        }

        $product_contracts_tag = $this->xpath->query(self::XML_ROOT_TAG . '/bi:product-contracts/bi:product-contract');

        if (isset($additional_data['ContractHeader']['ContractUpgrade']) && $additional_data['ContractHeader']['ContractUpgrade']) {
            foreach ($product_contracts_tag as $xml_nodes) {
                $id_split = explode(":", $xml_nodes->getAttribute('id'));
                if (is_array($id_split) && count($id_split) > 1) {
                    $xml_nodes->setAttribute('id', $id_split[0] . ':' . $this->contract_version); // hard-code version number in xml tag id
                }

                $xml_nodes->getElementsByTagName('object-version')->item(0)->nodeValue = $this->contract_version;
                $xml_nodes->getElementsByTagName('contract-id')->item(0)->nodeValue = $additional_data['ContractHeader']['GlobalContractId'];
            }
        } else {
            foreach ($product_contracts_tag as $xml_nodes) {
                $key_parent_tag = $dom_element->createElement('bi:contract-id', $additional_data['ContractHeader']['GlobalContractId']);
                $xml_nodes->appendChild($key_parent_tag);
            }
        }

        self::responseXMLSanityCheckup($this->xpath);

        $response_xml = $dom_element->saveXML();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response_xml : ' . print_r($response_xml, true), '');
        return $response_xml;
    }

    /**
     * register required xml namespace in xpath object variable for using xml search query
     *
     * @param object &$xpath xpath object variable to read and manipulate xml
     * @author sagar.salunkhe
     * @since Jun 18, 2016
     */
    private function registerPostNamespace(&$xpath)
    {
        $arr_namespace = array(
                            'product' => 'product',
                            'cbe' => 'common-business-entity',
                            'bi' => 'business-interaction'
        );

        foreach ($arr_namespace as $namespace => $uri){
            $xpath->registerNamespace($namespace, $this->xml_schema_path . "/" . $uri);
        }

        return;
    }

    /**
     * master array for manipulating post response xml
     *
     * @param array $additional_data input array with required response data
     * @return array
     *
     * @author sagar.salunkhe
     * @since Jun 18, 2016
     */
    private function getPostFilterTagDetails($additional_data)
    {
        $this->contract_version = round($additional_data['ContractHeader']['ContractVersion']);

        $response_data = array();

        $response_data['contract_header'] = $response_data['line_item_details'] = $response_data['product_conf'] = $response_data['product_rule'] = $response_data['invoice_group'] = $response_data['product_prices'] = array();

        foreach ($additional_data['ContractHeader']['local_uuid'] as $tag_id => $local_uuid){
            $response_data['contract_header'][$tag_id] = $local_uuid;
        }

        foreach ($additional_data['ContractLineItemDetails'] as $line_item_sub_modules) {

            foreach ($line_item_sub_modules['ContractLineItems']['local_uuid'] as $tag_id => $local_uuid){
                $response_data['line_item_details'][$tag_id] = $local_uuid;
            }

            if(isset($line_item_sub_modules['InvoiceGroupDtls']['local_uuid'])) {
                foreach ($line_item_sub_modules['InvoiceGroupDtls']['local_uuid'] as $tag_id => $local_uuid){
                    $response_data['invoice_group'][$tag_id] = $local_uuid;
                }
            }

            if(isset($line_item_sub_modules['ContractLineProductPrice'])){
                foreach ($line_item_sub_modules['ContractLineProductPrice'] as $product_price) {
                    foreach ($product_price['local_uuid'] as $tag_id => $local_uuid){
                        $response_data['product_prices'][$tag_id] = $local_uuid;
                    }
                }
            }

            if(isset($line_item_sub_modules['ContractLineProductConfiguration'])) {
                foreach ($line_item_sub_modules['ContractLineProductConfiguration'] as $product_confs) {
                    foreach ($product_confs['local_uuid'] as $tag_id => $local_uuid){
                        $response_data['product_conf'][$tag_id] = $local_uuid;
                    }
                }
            }

            if(isset($line_item_sub_modules['ContractLineProductRule'])) {
                foreach ($line_item_sub_modules['ContractLineProductRule'] as $product_rules) {
                    foreach ($product_rules['local_uuid'] as $tag_id => $local_uuid){
                        $response_data['product_rule'][$tag_id] = $local_uuid;
                    }
                }
            }
        }

        $characteristic_value = array_merge($response_data['contract_header'], $response_data['line_item_details'], $response_data['product_conf'], $response_data['product_rule'], $response_data['invoice_group'], $response_data['product_prices']);

        $filter_tags = array(
                            '/product:bundled-products' => array(
                                                                '/product:bundled-product' => array(
                                                                                                    'set_property' => $response_data['contract_header'],
                                                                                                    'detach_tags' => array(
                                                                                                                        'described-by',
                                                                                                                        'name',
                                                                                                                        'key-type-for-registration'
                                                                                                    ),
                                                                                                    'attach_tags' => array(
                                                                                                                        'key' => 'bundled-product-key',
                                                                                                                        'version' => $this->contract_version,
                                                                                                                        'tag_local_id_mapping' => $response_data['contract_header']
                                                                                                    )
                                                                )
                            ),

                            '/product:atomic-products' => array(
                                                                '/product:atomic-product' => array(
                                                                                                'set_property' => $response_data['line_item_details'],
                                                                                                'detach_tags' => array(
                                                                                                                    'described-by',
                                                                                                                    'name',
                                                                                                                    'key-type-for-registration'
                                                                                                ),
                                                                                                'attach_tags' => array(
                                                                                                                    'key' => 'atomic-product-key',
                                                                                                                    'version' => $this->contract_version,
                                                                                                                    'tag_local_id_mapping' => $response_data['line_item_details']
                                                                                                )
                                                                )
                            ),

                            '/product:atomic-product-prices' => array(
                                                                    '/product:atomic-product-price' => array(
                                                                                                            'set_property' => $response_data['product_prices'],
                                                                                                            'detach_tags' => array(
                                                                                                                                'described-by',
                                                                                                                                'key-type-for-registration',
                                                                                                                                'name',
                                                                                                                                'association-ends'
                                                                                                            ),
                                                                                                            'attach_tags' => array(
                                                                                                                                'key' => 'atomic-product-price-key',
                                                                                                                                'version' => $this->contract_version,
                                                                                                                                'tag_local_id_mapping' => $response_data['product_prices']
                                                                                                            )
                                                                    )
                            ),

                            '/bi:product-contracts' => array(
                                                            '/bi:product-contract' => array(
                                                                                            'set_property' => $response_data['contract_header'],
                                                                                            'detach_tags' => array(
                                                                                                                'object-created-date',
                                                                                                                'created-date',
                                                                                                                'object-created-user',
                                                                                                                'object-last-modified-date',
                                                                                                                'last-modified-date',
                                                                                                                'object-last-modified-user',
                                                                                                                'created-user',
                                                                                                                'lock-version',
                                                                                                                'interaction-status',
                                                                                                                'standard-business-state',
                                                                                                                'last-modified-user',
                                                                                                                'client-request-id',
                                                                                                                'name',
                                                                                                                'description',
                                                                                                                'expansions',
                                                                                                                'described-by',
                                                                                                                'key-type-for-registration',
                                                                                                                'agreement-period',
                                                                                                                'association-ends'
                                                                                            ),
                                                                                            'attach_tags' => array(
                                                                                                                'key' => 'product-contract-key',
                                                                                                                'version' => $this->contract_version,
                                                                                                                'tag_local_id_mapping' => $response_data['contract_header']
                                                                                            )
                                                            )
                            ),

                            '/bi:product-biitems' => array(
                                                        '/bi:product-biitem' => array(
                                                                                    'set_property' => $response_data['line_item_details'],
                                                                                    'detach_tags' => array(
                                                                                                        'standard-state',
                                                                                                        'standard-business-state',
                                                                                                        'described-by',
                                                                                                        'key-type-for-registration',
                                                                                                        'sequence',
                                                                                                        'action',
                                                                                                        'association-ends'
                                                                                    ),
                                                                                    'attach_tags' => array(
                                                                                                        'key' => 'product-biitem-key',
                                                                                                        'version' => $this->contract_version,
                                                                                                        'tag_local_id_mapping' => $response_data['line_item_details']
                                                                                    )
                                                        )
                            ),

                            '/cbe:characteristic-values' => array(
                                                                '/cbe:characteristic-value' => array(
                                                                                                    'set_property' => $characteristic_value,
                                                                                                    'detach_tags' => array(
                                                                                                                        'key-type-for-registration',
                                                                                                                        'loi-contract-val',
                                                                                                                        'billing-scheme-val',
                                                                                                                        'contract-scheme-val',
                                                                                                                        'sequence',
                                                                                                                        'contract-type-val',
                                                                                                                        'customer-billing-method-val',
                                                                                                                        'customer-billing-currency-val',
                                                                                                                        'customer-contract-currency-val',
                                                                                                                        'inter-group-company-settlement-method-val',
                                                                                                                        'inter-group-company-billing-currency-val',
                                                                                                                        'inter-group-company-contract-currency-val',
                                                                                                                        'currency-val',
                                                                                                                        'is-icb-rate-val',
                                                                                                                        'charge-type-val',
                                                                                                                        'charge-period-val',
                                                                                                                        'list-price-val',
                                                                                                                        'discount-rate-val',
                                                                                                                        'discount-amount-val',
                                                                                                                        'customer-contract-price-val',
                                                                                                                        'inter-group-company-settlement-price-val',
                                                                                                                        'factory-reference-number-val',
                                                                                                                        'billing-start-date-val',
                                                                                                                        'billing-end-date-val',
                                                                                                                        'service-start-date-val',
                                                                                                                        'service-end-date-val',
                                                                                                                        'remarks-val',
                                                                                                                        'group-name-val',
                                                                                                                        'group-description-val',
                                                                                                                        'teams-val',
                                                                                                                        'GMONEPRICERULE',
                                                                                                                        'GMONEPRICERULE1',
                                                                                                                        'ECL2GCMNUMBER',
                                                                                                                        'GMONEADDSC',
                                                                                                                        'association-ends'
                                                                                                    ),
                                                                                                    'attach_tags' => array(
                                                                                                                        'key' => 'charc-value-key',
                                                                                                                        'version' => $this->contract_version,
                                                                                                                        'tag_local_id_mapping' => $characteristic_value
                                                                                                    )
                                                                )
                            )
        );

        return self::buildRecursiveArray($filter_tags, "", array(), array());
    }

    /**
     * build xml query string by reading multi-dimentional array data recursively
     *
     * @param array $data input array that needs to be modified
     * @param string $prefix index key prefix used for data manipulation
     * @param array $result result array
     * @param array $prev_data last array value data
     *
     * @author sagar.salunkhe
     * @since Jun 18, 2016
     */
    private function buildRecursiveArray($data, $prefix, $result, $prev_data)
    {
        if (is_array($data) && !empty($data)) {
            foreach ($data as $key => $value) {
                if (substr($key, 0, 1) === '/'){
                    $result = self::buildRecursiveArray($value, "{$prefix}{$key}", $result, $value);
                }
                else {
                    $result[$prefix] = $prev_data;
                    break;
                }
            }
        } else {
            $result[$prefix] = $prev_data;
        }
        return $result;
    }

    /**
     * manipulate xml data
     *
     * @param object &$xml_nodes xml node object
     * @param array $tag_meta xml tag definition with data
     * @author sagar.salunkhe
     * @since Jun 18, 2016
     */
    private function amplifyPostResponseData(&$xml_node, $amplify_data)
    {
        foreach ($amplify_data as $attribute_name => $attribute_value) {
            if ($xml_node->getAttribute('id') == $attribute_name) {
                $xml_node->setAttribute('temporary-id', $xml_node->getAttribute('id'));
                if (!empty($attribute_value)){
                    $attribute_value .= ':1'; // hard-coded value DF-370, confirmed with sudharsan-san
                }
                $xml_node->setAttribute('id', $attribute_value);
            }
        }
        return;
    }

    /**
     * remove tags from xml
     *
     * @param object &$xml_nodes xml node object
     * @param array $tag_meta xml tag definitions to be removed
     *
     * @author sagar.salunkhe
     * @since Jun 18, 2016
     */
    private function detachRedundantXmlData(&$xml_nodes, $tag_meta)
    {
        $filter_tag_meta = array();
        foreach ($tag_meta as $key => $remove) {
            $metas = $xml_nodes->getElementsByTagName($remove);
            for ($count = 0; $count < $metas->length; $count++) {
                $filter_tag_meta[] = $metas->item($count);
            }
        }
        foreach ($filter_tag_meta as $tag) {
            if (!($tag->hasChildNodes()) && $tag->nodeValue == ''){
                $tag->nodeValue = 'temporary-value'; // remove empty tags
            }

            if ($tag->hasChildNodes()){
                self::deleteNode($tag);
            }
            else {
                self::deleteChildren($tag);
            }
        }
        return;
    }

    /**
     * attach additional tags to xml
     *
     * @param object &$xml_nodes xml node object
     * @param array $add_data additional data
     * @param string $parent_path xml path query
     *
     * @author sagar.salunkhe
     * @since Jun 16, 2016
     */
    private function attachPostResponseDataTags(&$dom_element, &$xml_node, $add_data)
    {
        foreach ($add_data['tag_local_id_mapping'] as $tag_id => $tag_value) {
            if ($xml_node->getAttribute('id') == $tag_id) {

                $meta = $xml_node->getElementsByTagName($add_data['key'])->item(0);

                $key_parent_tag = $dom_element->createElement('cbe:key-type-for-cbe-standard');
                $meta->appendChild($key_parent_tag);
                $object_child = $dom_element->createElement('cbe:object-id', $tag_value);
                $key_parent_tag->appendChild($object_child);

                $version_child = $dom_element->createElement('cbe:object-version', '1');
                $key_parent_tag->appendChild($version_child);
            }
        }
        return;
    }

    /**
     * delete xml parent node
     *
     * @author sagar.salunkhe
     */
    private function deleteNode($node)
    {
        self::deleteChildren($node);
        $parent = $node->parentNode;
        $parent->removeChild($node);
    }

    /**
     * delete children nodes recursively
     *
     * @author sagar.salunkhe
     */
    private function deleteChildren($node)
    {
        while (isset($node->firstChild)) {
            self::deleteChildren($node->firstChild);
            $node->removeChild($node->firstChild);
        }
    }

    /**
     * remove tags which has not processed in save (additional tags)
     *
     * @param object $xpath
     * @author sagar.salunkhe
     * @since Aug 25, 2016
     */
    private function responseXMLSanityCheckup(&$xpath)
    {
        $tags_array = array(
                            '/product:atomic-products/product:atomic-product',
                            '/product:atomic-product-prices/product:atomic-product-price',
                            '/bi:product-biitems/bi:product-biitem',
                            '/cbe:characteristic-values/cbe:characteristic-value'
        );

        foreach ($tags_array as $tag_path) {
            $contract_info_tags = $xpath->query(self::XML_ROOT_TAG . $tag_path);
            foreach ($contract_info_tags as $xml_nodes) {
                if (!self::isValidXmlTagId($xml_nodes->getAttribute('id'))) {
                    if ($xml_nodes->hasChildNodes()){
                        self::deleteNode($xml_nodes);
                    }
                    else {
                        self::deleteChildren($xml_nodes);
                    }
                }
            }
        }
    }

    /**
     * check valid xml tag id (ex regex d783eb30-0b23-4fbb-ae43-4c940732e7fe:10)
     *
     * @param string $id
     * @return boolean
     * @author sagar.salunkhe
     * @since Aug 25, 2016
     */
    private function isValidXmlTagId($id)
    {
        if (empty($id)) {
            return false;
        }
        if (preg_match('/^\{?[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}(.*)\}?$/', $id)) {
            return true;
        }
        return false;
    }
}