<?php

global $api_middleware;

/**
 * --------------------------------------
 * LAYERS
 * --------------------------------------
 *
 * pre         => PreValidationLayer
 * business    => BusinessValidationLayer
 * pms         => PMSValidationLayer
 * beforesave  => BeforeSaveValidationLayer
 * -----------------------------------------
 */

/**
 * ------------------------------------------------------------
 * EXAMPLE
 * ------------------------------------------------------------
 *
 * $api_middleware[] = [
 *     'api_config_key'  => 'key_name',
 *     'methods'         => 'get|post|version_up|patch|idwh|gbs',
 *     'trigger'         => 'before|after',
 *     'description'     => 'purpose and details of the middleware',
 * ];
 * ------------------------------------------------------------
 *
 * methods => get|post|version_up|patch|idwh|gbs
 * trigger => before|after
 *
 * Once added we need to added a function to the respective 'layer'
 * and method name should be in camel case :
 *
 * public function [methods][Trigger][Type][Field][Slug]() { }
 *
 * N.B.:
 *   field => never start with number
 *   slug  => never start with number
 *
 * Don't add middleware for ignore => true in config/api_config.php
 */

/**
 * @method  postBeforeInvalidCidasConnectionFailed
 * @method  versionUpBeforeInvalidCidasConnectionFailed
 * @method  patchBeforeInvalidCidasConnectionFailed
 */
$api_middleware[] = [
    'field'       => 'cidas_connection',
    'slug'        => 'failed',
    'methods'     => 'post|version_up|patch',
    'type'        => 'invalid',
    'trigger'     => 'before',
    'description' => 'parse xml in to array before validating CIDAS connection',
];
/**
 * @method  getBeforeInvalidParams
 * @method  versionUpBeforeInvalidParams
 * @method  patchBeforeInvalidParams
 */
$api_middleware[] = [
    'field'       => 'params',
    'slug'        => '',
    'methods'     => 'get|version_up|patch',
    'type'        => 'invalid',
    'trigger'     => 'before',
    'description' => 'fetch database contract details after pms connection success & before starting priority 9',
];
/**
 * @method  postBeforeInvalidProductOfferingId
 */
$api_middleware[] = [
    'field'       => 'product_offering',
    'slug'        => 'id',
    'methods'     => 'post|version_up',
    'type'        => 'invalid',
    'trigger'     => 'before',
    'description' => 'populate pms offering list',
];
/**
 * @method  postBeforeMandatoryProductOfferingDetails
 */
$api_middleware[] = [
    'field'       => 'product_offering',
    'slug'        => 'details',
    'methods'     => 'post|version_up',
    'type'        => 'mandatory',
    'trigger'     => 'before',
    'description' => 'populate pms offering details',
];
/**
 * @method  postBeforeMandatoryProductSpecificationBase
 */
$api_middleware[] = [
    'field'       => 'product_specification',
    'slug'        => 'base',
    'methods'     => 'post|version_up',
    'type'        => 'mandatory',
    'trigger'     => 'before',
    'description' => 'generate characteristics_ids with values from request data',
];
/**
 * @method  versionUpBeforeMandatoryLineItemBiItem
 */
$api_middleware[] = [
    'field'       => 'line_item',
    'slug'        => 'bi_item',
    'methods'     => 'version_up',
    'type'        => 'mandatory',
    'trigger'     => 'before',
    'description' => 'version_up line_item before validating logic',
];
