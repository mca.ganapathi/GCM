<?php
global $api_config;

/**
 * --------------------------------------
 * LAYERS
 * --------------------------------------
 *
 * pre         => PreValidationLayer
 * business    => BusinessValidationLayer
 * pms         => PMSValidationLayer
 * beforesave  => BeforeSaveValidationLayer
 * -----------------------------------------
 */

/**
 * ------------------------------------------------------------
 * EXAMPLE
 * ------------------------------------------------------------
 *
 * // priority
 * $api_config['xpath'][] = [
 *     // group
 *     [
 *         [
 *             'field'   => 'contract_start_date',
 *             'slug'    => 'format',
 *             'methods' => 'get|post|version_up|patch|idwh|gbs',
 *             'type'    => 'invalid|mandatory',
 *             'layer'   => 'pre|business|pms|beforesave',
 *             'code'    => 'error code',
 *             'message' => 'error message',
 *             'details' => 'error details with xpath and {xpath_ids}"',
 *             'ignore'  => false|true,
 *         ],
 *     ],
 * ];
 * ------------------------------------------------------------
 *
 * methods => get|post|version_up|patch|idwh|gbs
 * layer   => pre|business|pms|beforesave
 * type    => invalid|mandatory
 * ignore  => false|true if we pass true it will ignore from validation layer validation
 *
 * Once added we need to added a function to the respective 'layer'
 * and method name should be in camel case :
 *
 * public function [methods][Layer][Type][Field][Slug]() { }
 *
 * N.B.:
 *   field => never start with number
 *   slug  => never start with number
 */

// priority 1
$api_config['xpath'][] = [
    // group A
    /** start : proxy module validations **/
    [
        /**
         * validate http method in API.2.0 request
         *
         * @method getPreInvalidHttpMethod
         * @method postPreInvalidHttpMethod
         * @method patchPreInvalidHttpMethod
         */
        [
            'field'   => 'http_method',
            'slug'    => '',
            'methods' => 'get|post|patch',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '4050101001',
            'message' => 'Invalid HTTP Method.',
            'details' => 'HTTP method "{http_method}" is invalid for "api/v2/contract-information"',
            'ignore'  => true,
        ],
        /**
         * validate http method in idwh request
         *
         * @method getPreInvalidHttpMethodIdwh
         */
        [
            'field'   => 'http_method',
            'slug'    => 'idwh',
            'methods' => 'get',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '4050101001',
            'message' => 'Invalid HTTP Method.',
            'details' => 'HTTP method "{http_method}" is invalid for "api/v2/views/list-of-product-contracts"',
            'ignore'  => true,
        ],
        /**
         * validate http method in gbs request
         *
         * @method getPreInvalidHttpMethodGbs
         */
        [
            'field'   => 'http_method',
            'slug'    => 'gbs',
            'methods' => 'get',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '4050101001',
            'message' => 'Invalid HTTP Method.',
            'details' => 'HTTP method "{http_method}" is invalid for "api/v2/export-csv-for-gbs"',
            'ignore'  => true,
        ],
        /**
         * validate http method in API.2.0 request
         *
         * @method getPreInvalidHttpMethod
         * @method postPreInvalidHttpMethod
         * @method patchPreInvalidHttpMethod
         */
        [
            'field'   => 'internal_server_error',
            'slug'    => '',
            'methods' => 'get|post|patch',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '5000101002',
            'message' => 'Contract process terminated. Please contact System Administrator with this XML message.',
            'details' => '',
            'ignore'  => true,
        ],
        /**
         * validate http method in get idwh request
         *
         * @method getPreInvalidHttpMethodIdwh
         */
        [
            'field'   => 'internal_server_error',
            'slug'    => 'idwh',
            'methods' => 'get',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '5000101002',
            'message' => 'Contract process terminated. Please contact System Administrator with this XML message.',
            'details' => '',
            'ignore'  => true,
        ],
        /**
         * validate http method in get gbs request
         *
         * @method getPreInvalidHttpMethodGbs
         */
        [
            'field'   => 'internal_server_error',
            'slug'    => 'gbs',
            'methods' => 'get',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '5000101003',
            'message' => 'Contract process terminated. Please contact System Administrator with this XML message.',
            'details' => '',
            'ignore'  => true,
        ],
    ],
    /** end : proxy module validations **/
];

// priority 2
$api_config['xpath'][] = [
    // group A
    /** start: session validation **/
    [
        /**
         * validate session in API.2.0|idwh|gbs request
         *
         * @method getPreInvalidSession
         * @method postPreInvalidSession
         * @method versionUpPreInvalidSession
         * @method patchPreInvalidSession
         * @method idwhPreInvalidSession
         * @method gbsPreInvalidSession
         */
        [
            'field'   => 'session',
            'slug'    => '',
            'methods' => 'get|post|version_up|patch|idwh|gbs',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '4010101001',
            'message' => 'Invalid Session Id.',
            'details' => '',
            'ignore'  => false,
        ],
    ],
    /** end: session validation **/
];

// priority 3
$api_config['xpath'][] = [
    // group A
    /** start: GBS product offering validation **/
    [
        /**
         * validate gbs product offerings list is present in global variable or not
         *
         * @method gbsBusinessMandatoryGbsApiPo
         */
        [
            'field'   => 'gbs_api_po',
            'slug'    => '',
            'methods' => 'gbs',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '5000101004',
            'message' => 'Contract process terminated. Please contact System Administrator with this XML message.',
            'details' => '',
            'ignore'  => false,
        ],
    ],
    /** end: GBS product offering validation **/
];

// priority 4
$api_config['xpath'][] = [
    // group A
    /** start: url params validation **/
    [
        /**
         * mandatory check of GCM ID or Contract UUID in get|version_up|patch request url
         *
         * @method getBusinessMandatoryParams
         * @method versionUpBusinessMandatoryParams
         * @method patchBusinessMandatoryParams
         */
        [
            'field'   => 'params',
            'slug'    => '',
            'methods' => 'get|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000101001',
            'message' => 'Contract process terminated. Contract Reference ID (GCM ID or Contract UUID) should not be empty.',
            'details' => '',
            'ignore'  => false,
        ],
    ],
    /** end: url params validation **/
    // group B
    /** start: gbs mandatory validation **/
    [
        /**
         * gbs required field account-type validation
         *
         * @method gbsBusinessMandatoryAccountType
         */
        [
            'field'   => 'account_type',
            'slug'    => '',
            'methods' => 'gbs',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000101002',
            'message' => 'Contract process terminated. Invalid GBS Request.',
            'details' => '"type" prarameter is mandatory.',
            'ignore'  => false,
        ],
        /**
         * gbs required field date-after validation
         *
         * @method gbsBusinessMandatoryDateAfter
         */
        [
            'field'   => 'date_after',
            'slug'    => '',
            'methods' => 'gbs',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000101002',
            'message' => 'Contract process terminated. Invalid GBS Request.',
            'details' => '"date-after" parameter is mandatory.',
            'ignore'  => false,
        ],
        /**
         * gbs required field date-before validation
         *
         * @method gbsBusinessMandatoryDateBefore
         */
        [
            'field'   => 'date_before',
            'slug'    => '',
            'methods' => 'gbs',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000101002',
            'message' => 'Contract process terminated. Invalid GBS Request.',
            'details' => '"date-before" paramter is mandatory.',
            'ignore'  => false,
        ],
        /**
         * gbs account type field validation
         *
         * @method gbsBusinessInvalidAccountTypeValid
         */
        [
            'field'   => 'account_type',
            'slug'    => 'valid',
            'methods' => 'gbs',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000102001',
            'message' => 'Contract process terminated. Invalid GBS Request.',
            'details' => '({account_type}) is invalid "type" value. "type" parameter must be either "billing-account" or "contract-account".',
            'ignore'  => false,
        ],
        /**
         * gbs field date-after format validation
         *
         * @method gbsBusinessInvalidDateAfterFormat
         */
        [
            'field'   => 'date_after',
            'slug'    => 'format',
            'methods' => 'gbs',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000102001',
            'message' => 'Contract process terminated. Invalid GBS Request.',
            'details' => '({date_after}) is invalid "date-after" value. "date-after" parameter must be in  "YYYY-MM-DD" format.',
            'ignore'  => false,
        ],
        /**
         * gbs field date-before format validation
         *
         * @method gbsBusinessInvalidDateBeforeFormat
         */
        [
            'field'   => 'date_before',
            'slug'    => 'format',
            'methods' => 'gbs',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000102001',
            'message' => 'Contract process terminated. Invalid GBS Request.',
            'details' => '({date_before}) is invalid "date-before" value. "date-before" parameter must be in  "YYYY-MM-DD" format.',
            'ignore'  => false,
        ],
    ],
    // group D
    /** start: gbs invalid value validation **/
    [
        /**
         * gbs fields date-after and date-before comparison
         *
         * @method gbsBusinessInvalidDateBeforeAfterCompare
         */
        [
            'field'   => 'date_before_after',
            'slug'    => 'compare',
            'methods' => 'gbs',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000102001',
            'message' => 'Contract process terminated. Invalid GBS Request.',
            'details' => 'The value of "date-before" ({date_before}) should be greater than or equal to "date-after" ({date_after}).',
            'ignore'  => false,
        ],
    ],
    /** end: gbs invalid format validation **/
    /** start: gbs invalid value validation **/
    // group C
    /** start: idwh mandatory validation **/
    [
        /**
         * mandatory check for contract-customer-id, 'contract-product-id' or 'updated-after' parameters in iDWH
         *
         * @method idwhBusinessMandatoryIdwhParams
         */
        [
            'field'   => 'idwh_params',
            'slug'    => '',
            'methods' => 'IDWH',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000101003',
            'message' => 'Contract process terminated. Invalid Product Contract Request.',
            'details' => 'At least one of the parameters of "contract-customer-id", "contract-product-id" or "updated-after" is mandatory in the request.',
            'ignore'  => false,
        ],
    ],
    /** end: idwh mandatory validation **/
    // group D
    /** start: idwh invalid validation **/
    [
        /**
         * invalid value check for contract-product-id parameters in iDWH
         *
         * @method idwhBusinessInvalidContractProductId
         */
        [
            'field'   => 'contract_product_id',
            'slug'    => '',
            'methods' => 'IDWH',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000102002',
            'message' => 'Contract process terminated. Invalid Product Contract Request.',
            'details' => '({contract_product_id}) is invalid "contract-product-id" value.',
            'ignore'  => false,
        ],
        /**
         * invalid date format check for updated-after parameters in iDWH
         *
         * @method idwhBusinessInvalidUpdatedAfter
         */
        [
            'field'   => 'updated_after',
            'slug'    => '',
            'methods' => 'IDWH',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000102002',
            'message' => 'Contract process terminated. Invalid Product Contract Request.',
            'details' => '({updated_after}) is invalid "updated-after" value. "updated-after" parameter must be in "YYYY-MM-DDTHH:MM:SS(+/-)HH:MM" format.',
            'ignore'  => false,
        ],
        /**
         * invalid timezone check for updated-after parameters in iDWH
         *
         * @method idwhBusinessInvalidUpdatedAfterTimzone
         */
        [
            'field'   => 'updated_after',
            'slug'    => 'timzone',
            'methods' => 'IDWH',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000102002',
            'message' => 'Contract process terminated. Invalid Product Contract Request.',
            'details' => 'The timezone of "updated-after" ({updated_after_timzone}) value does not exist in the GCM timezone master.',
            'ignore'  => false,
        ],
        /**
         * invalid contract status value check for status parameters in iDWH
         *
         * @method idwhBusinessInvalidContractStatus
         */
        [
            'field'   => 'contract_status',
            'slug'    => '',
            'methods' => 'IDWH',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000102002',
            'message' => 'Contract process terminated. Invalid Product Contract Request.',
            'details' => '({contract_status}) is invalid "status" value. "status" parameter must be "Draft", "Approved", "Deactivated", "Sent for Approval", "Activated", "Sent for Termination Approval", "Termination Approved" or "Upgraded".',
            'ignore'  => false,
        ],
    ],
    /** end: idwh invalid validation **/
];

// priority 5
$api_config['xpath'][] = [
    // group A
    /** start: XML empty validation **/
    [
        /**
         * XML empty validation in post|version_up|patch request
         *
         * @method postPreMandatoryXmlValidation
         * @method versionUpPreMandatoryXmlValidation
         * @method patchPreMandatoryXmlValidation
         */
        [
            'field'   => 'xml_validation',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'pre',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => 'Request XML should not be empty.',
            'ignore'  => false,
        ],
    ],
    /** end: XML empty validation **/

    // group B
    /** start: XSD invalid validation **/
    [
        /**
         * XSD validation in post|version_up|patch request
         *
         * @method postPreInvalidXsdValidation
         * @method versionUpPreInvalidXsdValidation
         * @method patchPreInvalidXsdValidation
         */
        [
            'field'   => 'xsd_validation',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '4000203001',
            'message' => 'Contract process terminated. XSD validation failed.',
            'details' => '',
            'ignore'  => false,
        ],
    ],
    /** end: XSD invalid validation **/
];

// priority 6
$api_config['xpath'][] = [
    // group A
    /** start: [3003] CIDAS Connection failed **/
    [
        /**
         * [3003] CIDAS Connection failed.
         *
         * @method postPreInvalidCidasConnectionFailed
         * @method versionUptPreInvalidCidasConnectionFailed
         * @method patchtPreInvalidCidasConnectionFailed
         */
        [
            'field'   => 'cidas_connection',
            'slug'    => 'failed',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '5000301001',
            'message' => 'Contract process terminated. Please contact System Administrator with this XML message.',
            'details' => '',
            'ignore'  => false,
        ],
    ],
    /** end: [3003] CIDAS Connection failed **/
    // group B
    /** start: [3005] CIDAS Zero result found **/
    [
        /**
         * [3005] Error from CIDAS. Zero result found
         *
         * @method postPreInvalidCidasConnectionZeroRecord
         * @method versionUpPreInvalidCidasConnectionZeroRecord
         * @method patchPreInvalidCidasConnectionZeroRecord
         */
        [
            'field'   => 'cidas_connection',
            'slug'    => 'zero_record',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '4000202009',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({cidas_id}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_xml_id}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:{account_type_xpath}/gcm:corporation/gcm:corporation-id is not found in CIDAS.',
            'ignore'  => false,
        ],
    ],
    /** end: [3005] CIDAS Zero result found **/
    // group C
    /** start: [E001] Error from CIDAS **/
    [
        /**
         * [E001] Error from CIDAS. The number of search result exceeds threshold or the company name and the company name in English after cleansing is blank.
         *
         * @method postPreInvalidCidasConnectionE001
         * @method versionUpPreInvalidCidasConnectionE001
         * @method patchPreInvalidCidasConnectionE001
         */
        [
            'field'   => 'cidas_connection',
            'slug'    => 'E001',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '5000301002',
            'message' => 'Contract process terminated. Please contact System Administrator with this XML message.',
            'details' => '',
            'ignore'  => false,
        ],
    ],
    /** end: [E001] Error from CIDAS **/
    // group D
    /** start: [E002] Error from CIDAS **/
    [
        /**
         * [E002] Error from CIDAS. Wrong authentication pass code.
         *
         * @method postPreInvalidCidasConnectionE002
         * @method versionUpPreInvalidCidasConnectionE002
         * @method patchPreInvalidCidasConnectionE002
         */
        [
            'field'   => 'cidas_connection',
            'slug'    => 'E002',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '5000301003',
            'message' => 'Contract process terminated. Please contact System Administrator with this XML message.',
            'details' => '',
            'ignore'  => false,
        ],
    ],
    /** end: [E002] Error from CIDAS **/
    // group E
    /** start: [E003] Error from CIDAS **/
    [
        /**
         * [E003] Error from CIDAS. Wrong GET parameter.
         *
         * @method postPreInvalidCidasConnectionE003
         * @method versionUpPreInvalidCidasConnectionE003
         * @method patchPreInvalidCidasConnectionE003
         */
        [
            'field'   => 'cidas_connection',
            'slug'    => 'E003',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '5000301004',
            'message' => 'Contract process terminated. Please contact System Administrator with this XML message.',
            'details' => '',
            'ignore'  => false,
        ],
    ],
    /** end: [E003] Error from CIDAS **/
    // group F
    /** start: [E004] Error from CIDAS **/
    [
        /**
         * [E004] Error from CIDAS. System error.
         *
         * @method postPreInvalidCidasConnectionE004
         * @method versionUpPreInvalidCidasConnectionE004
         * @method patchPreInvalidCidasConnectionE004
         */
        [
            'field'   => 'cidas_connection',
            'slug'    => 'E004',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '5000301005',
            'message' => 'Contract process terminated. Please contact System Administrator with this XML message.',
            'details' => '',
            'ignore'  => false,
        ],
    ],
    /** end: [E004] Error from CIDAS **/
    // group G
    /** start: [8004] PMS connection failed **/
    [
        /**
         * [8004] PMS connection failed.
         *
         * @method getPreInvalidPmsConnection
         * @method postPreInvalidPmsConnection
         * @method versionUpPreInvalidPmsConnection
         * @method patchPreInvalidPmsConnection
         * @method gbsPreInvalidPmsConnection
         */
        [
            'field'   => 'pms_connection',
            'slug'    => '',
            'methods' => 'get|post|version_up|patch|gbs',
            'type'    => 'invalid',
            'layer'   => 'pre',
            'code'    => '5000201001',
            'message' => 'Contract process terminated. Please contact System Administrator with this XML message.',
            'details' => '',
            'ignore'  => false,
        ],
    ],
    /** end: [8004] PMS connection failed **/
];

//priority 7
$api_config['xpath'][] = [
    // group A
    /** START : validate XML and URL Global contract id param for API.2.0 **/
    [
        /**
         * validate global contract id for POST request
         *
         * @method postBusinessInvalidGlobalContractIdUrl
         */
        [
            'field'   => 'global_contract_id',
            'slug'    => 'url',
            'methods' => 'post',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => 'POST Version up XML is not applicable for POST Registration request.',
            'ignore'  => false,
        ],
        /**
         * validate global contract id for POST request
         *
         * @method versionUpBusinessInvalidGlobalContractIdUrl
         */
        [
            'field'   => 'global_contract_id',
            'slug'    => 'url',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => 'POST Registration XML is not applicable for POST Version up request.',
            'ignore'  => false,
        ],
    ],
    /** END : validate XML and URL Global contract id param for API.2.0 **/
    // group B
    [
        /**
         * mandatory client request id in POST request
         *
         * @method postBusinessMandatoryClientReqId
         * @method versionUpBusinessMandatoryClientReqId
         */
        [
            'field'   => 'client_req_id',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:client-request-id is mandatory.',
            'ignore'  => false,
        ],
    ],
];

// priority 8
$api_config['xpath'][] = [
    // group A
    /** START : validate GCM ID or Contract UUID present in DB for API.2.0 **/
    [
        /**
         * invalid validation for GCM ID or Contract UUID.
         *
         * @method getBusinessInvalidParams
         * @method versionUpBusinessInvalidParams
         * @method patchBusinessInvalidParams
         */
        [
            'field'   => 'params',
            'slug'    => '',
            'methods' => 'get|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4040101001',
            'message' => 'Contract process terminated. Contract Reference ID (GCM ID or Contract UUID) does not exist.',
            'details' => 'GCM ID of the target contract [{global_contract_id}] does not exist.',
            'ignore'  => false,
        ],
    ],
    /** END : validate GCM ID or Contract UUID present in DB for API.2.0 **/
    // group B
    /** START : invalid check of client request id/product offering id in POST request **/
    [
        /**
         * mandatory contract product offering id check in POST
         *
         * @method postBusinessMandatoryProductOfferingId
         * @method versionUpBusinessMandatoryProductOfferingId
         */
        [
            'field'   => 'product_offering',
            'slug'    => 'id',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/product:bundled-products/product:bundled-product is mandatory.',
            'ignore'  => false,
        ],
        /**
         * For only activated contract, version up process is applicable
         *
         * @method versionUpBusinessInvalidContractStatus
         */
        [
            'field'   => 'contract_status',
            'slug'    => '',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => 'Contract Status in DB ({contract_status_name}) is not applicable for version up.',
            'ignore'  => false,
        ],
    ],
    /** END : invalid check of client request id/product offering id in POST request **/
    // group C
    /** START : compare global contract id with url and xml in POST Version-Up and PATCH request **/
    [
        /**
         * invalid global_contract_uuid with old value/old version
         *
         * @method versionUpBusinessInvalidGlobalContractUuidMatch
         */
        [
            'field'   => 'global_contract_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({global_contract_id_uuid}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:identified-by/bi:product-contract-key/cbe:key-type-for-cbe-standard/cbe:object-id is invalid with previous version of contract.',
            'ignore'  => false,
        ],
        /**
         *  reference object id in URL global_contract_id_uuid mismatch
         *
         * @method patchBusinessInvalidGlobalContractUuidMatch
         */
        [
            'field'   => 'global_contract_uuid',
            'slug'    => 'match',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({global_contract_id_uuid}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:identified-by/bi:product-contract-key/cbe:key-type-for-cbe-standard/cbe:object-id invalid reference key',
            'ignore'  => false,
        ],
    ],
    /** END : compare global contract id with url and xml in POST Version-Up and PATCH request **/
    // group D
    /** START: PMS validation **/
    [
        /**
         * Invalid validation for product offering id.
         *
         * @method versionUpPmsInvalidProductOfferingId
         */
        [
            'field'   => 'product_offering',
            'slug'    => 'id',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/product:bundled-products/product:bundled-product[@id="{product_offering_xml_id}"]/cbe:described-by/product:reference-to-bundled-product-offering/product:bundled-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_offering_id} ) is invalid',
            'ignore'  => false,
        ],

        /**
         * invalid Standard-business-state in patch xml
         *
         * @method patchBusinessInvalidContractStatusXml
         */
        [
        'field'   => 'contract_status_xml',
        'slug'    => '',
        'methods' => 'patch',
        'type'    => 'invalid',
        'layer'   => 'business',
        'code'    => '4000202006',
        'message' => 'Contract process terminated. Invalid Contract Detail(s).',
        'details' => '({contract_status_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}"]/cbe:standard-business-state is mismatches with the registred contract status.',
        'ignore'  => false,
        ],
        /**
         * patch if contract_status is other than "Sent for Termination Approval" and "Deactivated"
        *
        * @method patchBusinessInvalidContractStatusApplicable
        */
        [
            'field'   => 'contract_status_applicable',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({contract_status_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}"]/cbe:standard-business-state is not applicable. It should be either "Sent for Termination Approval" or "Deactivated".',
            'ignore'  => false,
        ],
        /**
         * product offering and contract status mismatch in patch
        *
        * @method patchBusinessInvalidContractStatusApplicablePo
        */
        [
            'field'   => 'contract_status_applicable_po',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({contract_status_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}"]/cbe:standard-business-state is not applicable for the bundled product ({product_offering_db})',
            'ignore'  => false,
        ],

    ],
    // group E
    /** START : Contract version-up not applicable for non-workflow product **/
    [
        /**
         * Contract version-up not applicable for non-workflow product.
         *
         * @method versionUpBusinessInvalidProductOfferingWorkflow
         */
        [
            'field'   => 'product_offering',
            'slug'    => 'workflow',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({product_offering_id}) in /contract-info:contract-information/product:bundled-products/product:bundled-product[@id="{product_offering_uuid_xml}:1"]/cbe:identified-by/product:bundled-product-key/cbe:key-type-for-cbe-standard/cbe:object-id is not applicable for version-up.',
            'ignore'  => false,
        ],
    ],
    /** END : Contract version-up not applicable for non-workflow product **/
    // group F
    /** START : Contract version-up|patch not applicable if product specification id changed from previous version **/
    [
        /**
         * Contract version-up|patch not applicable if product specification id changed from previous version.
         *
         * @method versionUpBusinessInvalidProductSpecificationIdMatch
         */
        [
            'field'   => 'product_specification_id',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({product_specification_id}) in /contract-info:contract-information/product:atomic-products/product:atomic-product[@id="{product_specification_xml_id}:1"]/cbe:described-by/product:reference-to-atomic-product-offering/product:atomic-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id is invalid with previous version of contract.',
            'ignore'  => false,
        ],
    ],
    /** END : Contract version-up|patch not applicable if product specification id changed from previous version **/
];

// priority 9
$api_config['xpath'][] = [
    // group A
    /** START : PATCH xml-id and db uuid validation API.2.0 **/
    [
        /**
         * invalid xml tag-id and db uuid of contract header fields in PATCH request
         *
         * @method patchBusinessInvalidContractHeaderFieldsMatch
         */
        [
            'field'   => 'contract_header_fields',
            'slug'    => 'match',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{field_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id invalid reference key.',
            'ignore'  => false,
        ],
        /**
         * invalid xml tag-id and db uuid of line item header fields in PATCH request
         *
         * @method patchBusinessInvalidLineItemHeaderUuidMatch
         */
        [
            'field'   => 'line_item_header_uuid',
            'slug'    => 'match',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{field_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id invalid reference key.',
            'ignore'  => false,
        ],
    ],
    /** END : PATCH xml-id and db uuid validation API.2.0 **/
    //group B
    /** START : Business mandatory Validations for POST|VERSION_UP|PATCH API.2.2 **/
    [
		/**
         * DF-2009 beluga validation : billing account is mandatory for activated contract.
         *
         * @method postBusinessMandatoryBelugaBillingAccount
         * @method patchBusinessMandatoryBelugaBillingAccount
         * @method versionUpBusinessMandatoryBelugaBillingAccount
         */
        [
            'field'   => 'beluga',
            'slug'    => 'billing_account',
            'methods' => 'post|patch|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201002',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer is mandatory.',
            'ignore'  => false,
        ],

        /**
         * DF-2009 beluga validation : Payment Type is mandatory for activated contract.
         *
         * @method postBusinessMandatoryBelugaPaymentType
         * @method patchBusinessMandatoryBelugaPaymentType
         * @method versionUpBusinessMandatoryBelugaPaymentType
         */
        [
        'field'   => 'beluga',
        'slug'    => 'payment_type',
        'methods' => 'post|patch|version_up',
        'type'    => 'mandatory',
        'layer'   => 'business',
        'code'    => '4000201002',
        'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
        'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method is mandatory.',
        'ignore'  => false,
        ],

        /**
         * DF-2009 beluga validation : Billing Start Date is mandatory for activated contract.
         *
         * @method postBusinessMandatoryBelugaBillingStartDate
         * @method patchBusinessMandatoryBelugaBillingStartDate
         * @method versionUpBusinessMandatoryBelugaBillingStartDate
         */
        [
        'field'   => 'beluga',
        'slug'    => 'billing_start_date',
        'methods' => 'post|patch|version_up',
        'type'    => 'mandatory',
        'layer'   => 'business',
        'code'    => '4000201002',
        'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
        'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value/prod-cntrct-item-000002:billing-start-date-val for /contract-info:contract-information/bi:product-biitems/bi:product-biitem[@id="{line_item_xml_id}:1"] is mandatory.',
        'ignore'  => false,
        ],

        /**
         * DF-2009 beluga validation : Billing End Date is mandatory for activated contract.
         *
         * @method postBusinessMandatoryBelugaBillingEndDate
         * @method patchBusinessMandatoryBelugaBillingEndDate
         * @method versionUpBusinessMandatoryBelugaBillingEndDate
         */
        [
        'field'   => 'beluga',
        'slug'    => 'billing_end_date',
        'methods' => 'post|patch|version_up',
        'type'    => 'mandatory',
        'layer'   => 'business',
        'code'    => '4000201002',
        'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
        'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value/prod-cntrct-item-000002:billing-end-date-val for /contract-info:contract-information/bi:product-biitems/bi:product-biitem[@id="{line_item_xml_id}:1"] is mandatory.',
        'ignore'  => false,
        ],

        /**
         * null possible : contracting accounts corporate id field validation for PATCH
         *
         * @method patchBusinessInvalidNullPossibleContractAccCorp
         */
        [
            'field'   => 'null_possible',
            'slug'    => 'contract_acc_corp',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:corporation/gcm:corporation-id not satisfied NULL possible rule.',
            'ignore'  => false,
        ],

        /**
         * mandatory contract name value check in post
         *
         * @method postBusinessMandatoryContractName
         * @method versionUpBusinessMandatoryContractName
         * @method patchBusinessMandatoryContractName
         */
        [
            'field'   => 'contract_name',
            'slug'    => '',
            'methods' => 'post|patch|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:name is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory contract line item details in POST
         *
         * @method postBusinessMandatoryLineItemDetails
         * @method versionUpBusinessMandatoryLineItemDetails
         */
        [
            'field'   => 'line_item_details',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/product:atomic-products/product:atomic-product is mandatory',
            'ignore'  => false,
        ],
        /**
         * mandatory product characteristic details in POST
         *
         * @method postBusinessMandatoryProdCharacteristicDtls
         * @method versionUpBusinessMandatoryProdCharacteristicDtls
         */
        [
            'field'   => 'prod_characteristic_dtls',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"] characteristic is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory field contract start date in POST request
         *
         * @method postBusinessMandatoryContractsStartDate
         * @method versionUpBusinessMandatoryContractsStartDate
         */
        [
            'field'   => 'contracts_start_date',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/bi:agreement-period/datatypes:start-date-time is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory contract line item (individual line) details in POST
         *
         * @method postBusinessMandatoryLineItemDetailsIndex
         * @method versionUpBusinessMandatoryLineItemDetailsIndex
         */
        [
            'field'   => 'line_item_details',
            'slug'    => 'index',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/product:atomic-products/product:atomic-product for /contract-info:contract-information/bi:product-biitems/bi:product-biitem [@id="{line_item_id_xml_id}:1"] is mandatory',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contracting account in POST request
         *
         * @method postBusinessMandatoryContractAccount
         * @method versionUpBusinessMandatoryContractAccount
         */
        [
            'field'   => 'contract_account',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:corporation is mandatory',
            'ignore'  => false,
        ],
        /**
         * mandatory contracting account latin or local name in POST request
         *
         * @method postBusinessMandatoryContractingAccountName
         * @method versionUpBusinessMandatoryContractingAccountName
         */
        [
            'field'   => 'contracting_account_name',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:person-in-charge/gcm:individual/gcm:name or /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:person-in-charge/gcm:individual/gcm:name-latin is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory billing account latin or local name in POST request
         *
         * @method postBusinessMandatoryBillingAccountName
         * @method versionUpBusinessMandatoryBillingAccountName
         */
        [
            'field'   => 'billing_account_name',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:person-in-charge/gcm:individual/gcm:name or /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:person-in-charge/gcm:individual/gcm:name-latin is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory Technology account latin or local name in POST request
         *
         * @method postBusinessMandatoryTechnologyAccountName
         * @method versionUpBusinessMandatoryTechnologyAccountName
         */
        [
            'field'   => 'technology_account_name',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:person-in-charge/gcm:individual/gcm:name or /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:person-in-charge/gcm:individual/gcm:name-latin is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory Contracting corporate details in POST request
         *
         * @method postBusinessMandatoryContractingCompanyDtls
         * @method versionUpBusinessMandatoryContractingCompanyDtls
         * @deprecated this function is deprecated by Sudharsan in error code sheet validation on 06-Dec-17
         */
        [
            'field'   => 'contracting_company_dtls',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:corporation is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory Billing corporate details in POST request
         *
         * @method postBusinessMandatoryBillingCompanyDtls
         * @method versionUpBusinessMandatoryBillingCompanyDtls
         */
        [
            'field'   => 'billing_company_dtls',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:corporation is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory Technology corporate details in POST request
         *
         * @method postBusinessMandatoryTechnologyCompanyDtls
         * @method versionUpBusinessMandatoryTechnologyCompanyDtls
         */
        [
            'field'   => 'technology_company_dtls',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:corporation is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory Contracting corporate name or id in POST request
         *
         * @method postBusinessMandatoryContractingCorporateNameId
         * @method versionUpBusinessMandatoryContractingCorporateNameId
         */
        [
            'field'   => 'contracting_corporate_name_id',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:corporation/gcm:corporation-id is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line credit card number in POST request
         *
         * @method postBusinessMandatoryContractLineCreditcardNo
         * @method versionUpBusinessMandatoryContractLineCreditcardNo
         * @method patchBusinessMandatoryContractLineCreditcardNo
         */
        [
            'field'   => 'contract_line_creditcard_no',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:credit-card-pm/gcm:credit-card-number is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line credit card expiration-date in POST request
         *
         * @method postBusinessMandatoryContractLineCreditcardExpDate
         * @method versionUpBusinessMandatoryContractLineCreditcardExpDate
         * @method patchBusinessMandatoryContractLineCreditcardExpDate
         */
        [
            'field'   => 'contract_line_creditcard_exp_date',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:credit-card-pm/gcm:expiration-date is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line direct deposit bank code in POST request
         *
         * @method postBusinessMandatoryContractLineDirectDepositBankcode
         * @method versionUpBusinessMandatoryContractLineDirectDepositBankcode
         * @method patchBusinessMandatoryContractLineDirectDepositBankcode
         */
        [
            'field'   => 'contract_line_direct_deposit_bankcode',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-direct-deposit-pm/gcm:bank-code is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line direct deposit branch code in POST request
         *
         * @method postBusinessMandatoryContractLineDirectDepositBranchcode
         * @method versionUpBusinessMandatoryContractLineDirectDepositBranchcode
         * @method patchBusinessMandatoryContractLineDirectDepositBranchcode
         */
        [
            'field'   => 'contract_line_direct_deposit_branchcode',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-direct-deposit-pm/gcm:branch-code is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line direct deposit deposit-type in POST request
         *
         * @method postBusinessMandatoryContractLineDirectDepositType
         * @method versionUpBusinessMandatoryContractLineDirectDepositType
         * @method patchBusinessMandatoryContractLineDirectDepositType
         */
        [
            'field'   => 'contract_line_direct_deposit_type',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-direct-deposit-pm/gcm:deposit-type is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line direct deposit account-number in POST request
         *
         * @method postBusinessMandatoryContractLineDirectDepositAccno
         * @method versionUpBusinessMandatoryContractLineDirectDepositAccno
         * @method patchBusinessMandatoryContractLineDirectDepositAccno
         */
        [
            'field'   => 'contract_line_direct_deposit_accno',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-direct-deposit-pm/gcm:account-number is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line account transfer name in POST request
         *
         * @method postBusinessMandatoryContractLineAccTransferName
         * @method versionUpBusinessMandatoryContractLineAccTransferName
         * @method patchBusinessMandatoryContractLineAccTransferName
         */
        [
            'field'   => 'contract_line_acc_transfer_name',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-account-transfer-pm/gcm:bank-account-holder-name is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line account transfer bank code in POST request
         *
         * @method postBusinessMandatoryContractLineAccTransferBankcode
         * @method versionUpBusinessMandatoryContractLineAccTransferBankcode
         * @method patchBusinessMandatoryContractLineAccTransferBankcode
         */
        [
            'field'   => 'contract_line_acc_transfer_bankcode',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-account-transfer-pm/gcm:bank-code is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line account transfer branch code in POST request
         *
         * @method postBusinessMandatoryContractLineAccTransferBranchcode
         * @method versionUpBusinessMandatoryContractLineAccTransferBranchcode
         * @method patchBusinessMandatoryContractLineAccTransferBranchcode
         */
        [
            'field'   => 'contract_line_acc_transfer_branchcode',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-account-transfer-pm/gcm:branch-code is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line account transfer type code in POST request
         *
         * @method postBusinessMandatoryContractLineAccTransferType
         * @method versionUpBusinessMandatoryContractLineAccTransferType
         * @method patchBusinessMandatoryContractLineAccTransferType
         */
        [
            'field'   => 'contract_line_acc_transfer_type',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-account-transfer-pm/gcm:deposit-type is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line account transfer account number in POST request
         *
         * @method postBusinessMandatoryContractLineAccTransferAccno
         * @method versionUpBusinessMandatoryContractLineAccTransferAccno
         * @method patchBusinessMandatoryContractLineAccTransferAccno
         */
        [
            'field'   => 'contract_line_acc_transfer_accno',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-account-transfer-pm/gcm:account-number is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line account transfer account number display in POST request
         *
         * @method postBusinessMandatoryContractLineAccTransferAccnoDisplay
         * @method versionUpBusinessMandatoryContractLineAccTransferAccnoDisplay
         * @method patchBusinessMandatoryContractLineAccTransferAccnoDisplay
         */
        [
            'field'   => 'contract_line_acc_transfer_accno_display',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-account-transfer-pm/gcm:is-account-number-display is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line postal transfer account holder's name in POST request
         *
         * @method postBusinessMandatoryContractLinePostalTransferName
         * @method versionUpBusinessMandatoryContractLinePostalTransferName
         * @method patchBusinessMandatoryContractLinePostalTransferName
         */
        [
            'field'   => 'contract_line_postal_transfer_name',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-postal-transfer-pm/gcm:postal-account-holder-name is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line postal transfer Postal Passbook Mark in POST request
         *
         * @method postBusinessMandatoryContractLinePostalTransferPassbookMark
         * @method versionUpBusinessMandatoryContractLinePostalTransferPassbookMark
         * @method patchBusinessMandatoryContractLinePostalTransferPassbookMark
         */
        [
            'field'   => 'contract_line_postal_transfer_passbook_mark',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-postal-transfer-pm/gcm:postal-passbook-mark is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line postal transfer Postal Passbook Number in POST request
         *
         * @method postBusinessMandatoryContractLinePostalTransferPassbook
         * @method versionUpBusinessMandatoryContractLinePostalTransferPassbook
         * @method patchBusinessMandatoryContractLinePostalTransferPassbook
         */
        [
            'field'   => 'contract_line_postal_transfer_passbook',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-postal-transfer-pm/gcm:postal-passbook-number is mandatory.',
            'ignore'  => false,
        ],
        /**
         * mandatory check of contract line postal transfer Postal Passbook Number Display in POST request
         *
         * @method postBusinessMandatoryContractLinePostalTransferPassbookNoDisplay
         * @method versionUpBusinessMandatoryContractLinePostalTransferPassbookNoDisplay
         * @method patchBusinessMandatoryContractLinePostalTransferPassbookNoDisplay
         */
        [
            'field'   => 'contract_line_postal_transfer_passbook_no_display',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-postal-transfer-pm/gcm:is-postal-passbook-number-display is mandatory.',
            'ignore'  => false,
        ],

        /**
         * contract start date is mandatory in patch
         *
         * @method patchBusinessMandatoryContractStartdate
         */
        [
            'field'   => 'contract_startdate',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/bi:agreement-period/datatypes:start-date-time is mandatory.',
            'ignore'  => false,
        ],
        /**
         * service start date is mandatory in patch
         *
         * @method patchBusinessMandatoryServiceStartdate
         */
        [
            'field'   => 'service_startdate',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value/prod-cntrct-item-000002:service-start-date-val for /contract-info:contract-information/bi:product-biitems/bi:product-biitem[@id="{line_item_xml_id}:1"] is mandatory.',
            'ignore'  => false,
        ],
        /**
         * billing start date is mandatory in patch
         *
         * @method patchBusinessMandatoryBillingStartdate
         */
        [
            'field'   => 'billing_startdate',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value/prod-cntrct-item-000002:billing-start-date-val for /contract-info:contract-information/bi:product-biitems/bi:product-biitem[@id="{line_item_xml_id}:1"] is mandatory.',
            'ignore'  => false,
        ],
        /**
         * contact person name is mandatory for contract customer
         *
         * @method patchBusinessMandatoryContactPersonNameContractCustomer
         */
        [
            'field'   => 'contact_person_name',
            'slug'    => 'contract_customer',
            'methods' => 'patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:person-in-charge/gcm:individual/gcm:name or /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:person-in-charge/gcm:individual/gcm:name-latin is mandatory.',
            'ignore'  => false,
        ],
        /**
         * contact person name is mandatory for billing customer
         *
         * @method patchBusinessMandatoryContactPersonNameBillingCustomer
         */
        [
            'field'   => 'contact_person_name',
            'slug'    => 'billing_customer',
            'methods' => 'patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:person-in-charge/gcm:individual/gcm:name or /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:person-in-charge/gcm:individual/gcm:name-latin is mandatory.',
            'ignore'  => false,
        ],
        /**
         * contact person name is mandatory for customer technical representative
         *
         * @method patchBusinessMandatoryContactPersonNameTechRep
         */
        [
            'field'   => 'contact_person_name',
            'slug'    => 'tech_rep',
            'methods' => 'patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:person-in-charge/gcm:individual/gcm:name or /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:person-in-charge/gcm:individual/gcm:name-latin is mandatory.',
            'ignore'  => false,
        ],
        /**
         * if no contracting account is there in the db and corporate details is missing the xml then throw this error
         *
         * @method patchBusinessMandatoryContractCorporationXml
         */
        [
            'field'   => 'contract_corporation',
            'slug'    => 'xml',
            'methods' => 'patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:corporation is mandatory.',
            'ignore'  => false,
        ],
        /**
         * if no billing account is there in the db and corporate details is missing the xml then throw this error
         *
         * @method patchBusinessMandatoryBillingCorporationXml
         */
        [
            'field'   => 'billing_corporation',
            'slug'    => 'xml',
            'methods' => 'patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:corporation is mandatory',
            'ignore'  => false,
        ],
        /**
         * if no technical account is there in the db and corporate details is missing the xml then throw this error
         *
         * @method patchBusinessMandatoryTechnicalCorporationXml
         */
        [
            'field'   => 'technical_corporation',
            'slug'    => 'xml',
            'methods' => 'patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:corporation is mandatory',
            'ignore'  => false,
        ],
        /**
         * corporation name (either Local or Latin) is mandatory for Contract Customer.
         *
         *  @method patchBusinessMandatoryCorporationNameContractCustomer
         */
        [
            'field'   => 'corporation_name',
            'slug'    => 'contract_customer',
            'methods' => 'patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:corporation/gcm:corporation-id is mandatory.',
            'ignore'  => false,
        ],
        /**
         * corporation name (either Local or Latin) is mandatory for Billing Customer.
         *
         *  @method patchBusinessMandatoryCorporationNameBillingCustomer
         */
        [
            'field'   => 'corporation_name',
            'slug'    => 'billing_customer',
            'methods' => 'patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:corporation is mandatory.',
            'ignore'  => false,
        ],
        /**
         *  corporation name (either Local or Latin) is mandatory for Technical Representative.
         *
         *  @method patchBusinessMandatoryCorporationNameTechRep
         */
        [
            'field'   => 'corporation_name',
            'slug'    => 'tech_rep',
            'methods' => 'patch',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:corporation is mandatory.',
            'ignore'  => false,
        ],
        /**
         * validate line item product price currencyid xml_id with old version
         *
         * @method versionUpBusinessInvalidLineProductPriceCurrencyidUuidMatch
         */
        [
            'field'   => 'line_product_price_currencyid_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({currencyid_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{currencyid_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * Check bi-item key exist for the line item which is mandatory regardless of bi-action
         *
         * @method versionUpBusinessMandatoryLineItemBiItem
         */
        [
            'field'   => 'line_item',
            'slug'    => 'bi_item',
            'methods' => 'version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '',
            'ignore'  => false,
        ],
    ],
    /** END : Business mandatory Validations for POST|VERSION_UP|PATCH API.2.0 **/
    // group C
    /** START : Business invalid Validations for POST|VERSION_UP|PATCH API.2.0 **/
    [

        /**
         * mandatory check of all characteristic values for the selected PMS in POST request
         *
         * @method postBusinessMandatoryProductSpecPmsId
         * @method versionUpBusinessMandatoryProductSpecPmsId
         */
        [
            'field'   => 'product_spec_pms_id',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-biitems/bi:product-biitem[@id="{line_item_id_xml_id}:1"] characteristic is mandatory.',
            'ignore'  => false,
        ],
        /**
         * validate billing type in POST request
         *
         * @method postBusinessInvalidBillingType
         * @method versionUpBusinessInvalidBillingType
         * @method patchBusinessInvalidBillingType
         */
        [
            'field'   => 'billing_type',
            'slug'    => '',
            'methods' => 'post|patch|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({billing_type}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{billing_type_xml_id}:1"]/gmone-pcs-001:billing-scheme-val is not applicable for API.',
            'ignore'  => false,
        ],
        /**
         * validate compare contract start date and contract end date in POST request
         *
         * @method postBusinessInvalidContractsDatesCompare
         * @method versionUpBusinessInvalidContractsDatesCompare
         */
        [
            'field'   => 'contracts_dates',
            'slug'    => 'compare',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({contract_enddate}{contract_enddate_timezone}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/bi:agreement-period/datatypes:end-date-time should be greater than or equal to ({contract_startdate}{contract_startdate_timezone}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/bi:agreement-period/datatypes:start-date-time',
            'ignore'  => false,
        ],
        /**
         * validate contract start date timezone in POST request
         *
         * @method postBusinessInvalidContractStartdtTz
         * @method versionUpBusinessInvalidContractStartdtTz
         */
        [
            'field'   => 'contract_startdt_tz',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({contract_startdate_timezone}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/bi:agreement-period/datatypes:start-date-time is mismatches with the registered timezone ',
            'ignore'  => false,
        ],
        /**
         * validate contract start date timezone in POST request
         *
         * @method postBusinessInvalidContractEnddtTz
         * @method versionUpBusinessInvalidContractEnddtTz
         * @method patchBusinessInvalidContractEnddtTz
         */
        [
            'field'   => 'contract_enddt_tz',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({contract_enddate_timezone}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/bi:agreement-period/datatypes:end-date-time is mismatches with the registered timezone.',
            'ignore'  => false,
        ],
        /**
         * validate sales company of sales representative in POST request
         *
         * @method postBusinessInvalidSalesCompany
         * @method versionUpBusinessInvalidSalesCompany
         */
        [
            'field'   => 'sales_company',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({sales_company}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:sales-channel/gcm:corporation/gcm:standard-corporation-name/gcm:name is mismatches with the registered companies.',
            'ignore'  => false,
        ],
        /**
         * validate sales representative email address in POST request
         *
         * @method postBusinessInvalidSalesRepresentativeEmail
         * @method versionUpBusinessInvalidSalesRepresentativeEmail
         * @method patchBusinessInvalidSalesRepresentativeEmail
         */
        [
            'field'   => 'sales_representative_email',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({email1}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:sales-channel/gcm:person-in-charge/gcm:contact-medium/gcm:email-address is invalid.',
            'ignore'  => false,
        ],
        /**
         * validate compare service start and service end date at line item level in POST request
         *
         * @method postBusinessInvalidLineServiceDatesCompare
         * @method versionUpBusinessInvalidLineServiceDatesCompare
         */
        [
            'field'   => 'line_service_dates',
            'slug'    => 'compare',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({service_end_date}{service_end_date_timezone}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{service_end_date_xml_id}:1"]/prod-cntrct-item-000002:service-end-date-val should be greater than or equal to ({service_start_date}{service_start_date_timezone}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{service_start_date_xml_id}:1"]/prod-cntrct-item-000002:service-start-date-val',
            'ignore'  => false,
        ],
        /**
         * validate compare billing start and billing end date at line item level in POST request
         *
         * @method postBusinessInvalidLineBillingDatesCompare
         * @method versionUpBusinessInvalidLineBillingDatesCompare
         */
        [
            'field'   => 'line_billing_dates',
            'slug'    => 'compare',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({billing_end_date}{billing_end_date_timezone}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{billing_end_date_xml_id}:1"]/prod-cntrct-item-000002:billing-end-date-val should be greater than or equal to ({billing_start_date}{billing_start_date_timezone}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{billing_start_date_xml_id}:1"]/prod-cntrct-item-000002:billing-start-date-val',
            'ignore'  => false,
        ],
        /**
         * validate service start date timezone at line item level in POST request
         *
         * @method postBusinessInvalidServiceStartDateTimezone
         * @method versionUpBusinessInvalidServiceStartDateTimezone
         * @method patchBusinessInvalidServiceStartDateTimezone
         */
        [
            'field'   => 'service_start_date_timezone',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({service_start_date_timezone}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{service_start_date_xml_id}:1"]/prod-cntrct-item-000002:service-start-date-val is mismatches with the registered timezone.',
            'ignore'  => false,
        ],
        /**
         * validate service end date timezone at line item level in POST request
         *
         * @method postBusinessInvalidServiceEndDateTimezone
         * @method versionUpBusinessInvalidServiceEndDateTimezone
         * @method patchBusinessInvalidServiceEndDateTimezone
         */
        [
            'field'   => 'service_end_date_timezone',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({service_end_date_timezone}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{service_end_date_xml_id}:1"]/prod-cntrct-item-000002:service-end-date-val is mismatches with the registered timezone.',
            'ignore'  => false,
        ],

        /**
         * validate billing start date timezone at line item level in POST request
         *
         * @method postBusinessInvalidBillingStartDateTimezone
         * @method versionUpBusinessInvalidBillingStartDateTimezone
         * @method patchBusinessInvalidBillingStartDateTimezone
         */
        [
            'field'   => 'billing_start_date_timezone',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({billing_start_date_timezone}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{billing_start_date_xml_id}:1"]/prod-cntrct-item-000002:billing-start-date-val is mismatches with the registered timezone.',
            'ignore'  => false,
        ],
        /**
         * validate billing end date timezone at line item level in POST request
         *
         * @method postBusinessInvalidBillingEndDateTimezone
         * @method versionUpBusinessInvalidBillingEndDateTimezone
         * @method patchBusinessInvalidBillingEndDateTimezone
         */
        [
            'field'   => 'billing_end_date_timezone',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({billing_end_date_timezone}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{billing_end_date_xml_id}:1"]/prod-cntrct-item-000002:billing-end-date-val is mismatches with the registered timezone.',
            'ignore'  => false,
        ],
        /**
         * validate contract header surrogate teams in POST request
         *
         * @method postBusinessInvalidContractSurrogateTeams
         * @method versionUpBusinessInvalidContractSurrogateTeams
         */
        [
            'field'   => 'contract_surrogate_teams',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202003',
            'message' => 'Contract process terminated. Invalid Team Detail(s).',
            'details' => '({surrogate_team_name}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{surrogate_teams_xml_id}"]/gmone-pcs-001:teams-val is mismatches with the registered Team names.',
            'ignore'  => false,
        ],
        /**
         * validate Contracting account country code in POST request
         *
         * @method postBusinessInvalidContractingAccountCountryCode
         * @method versionUpBusinessInvalidContractingAccountCountryCode
         */
        [
            'field'   => 'contracting_account_country_code',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_code_numeric}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/location:iso3166-1numeric is mismatches with the registered countries.',
            'ignore'  => false,
        ],
        /**
         * validate Billing account country code in POST request
         *
         * @method postBusinessInvalidBillingAccountCountryCode
         * @method versionUpBusinessInvalidBillingAccountCountryCode
         */
        [
            'field'   => 'billing_account_country_code',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_code_numeric}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/location:iso3166-1numeric is mismatches with the registered countries.',
            'ignore'  => false,
        ],
        /**
         * validate Technology account country code in POST request
         *
         * @method postBusinessInvalidTechnologyAccountCountryCode
         * @method versionUpBusinessInvalidTechnologyAccountCountryCode
         */
        [
            'field'   => 'technology_account_country_code',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_code_numeric}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:address/location:iso3166-1numeric is mismatches with the registered countries.',
            'ignore'  => false,
        ],
        /**
         * validate Contract account country state combination in POST request
         *
         * @method postBusinessInvalidContractAccountCountryStateCompare
         * @method versionUpBusinessInvalidContractAccountCountryStateCompare
         */
        [
            'field'   => 'contract_account_country_state',
            'slug'    => 'compare',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_code_numeric}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/location:iso3166-1numeric & ({state_general_code}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:{addr_tag_xpath}-property-address/gcm:state-abbr combination does not match.',
            'ignore'  => false,
        ],
        /**
         * validate Billing account country state combination in POST request
         *
         * @method postBusinessInvalidBillingAccountCountryStateCompare
         * @method versionUpBusinessInvalidBillingAccountCountryStateCompare
         */
        [
            'field'   => 'billing_account_country_state',
            'slug'    => 'compare',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_code_numeric}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/location:iso3166-1numeric & ({state_general_code}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:{addr_tag_xpath}-property-address/gcm:state-abbr combination does not match.',
            'ignore'  => false,
        ],
        /**
         * validate Technology account country state combination in POST request
         *
         * @method postBusinessInvalidTechnologyAccountCountryStateCompare
         * @method versionUpBusinessInvalidTechnologyAccountCountryStateCompare
         */
        [
            'field'   => 'technology_account_country_state',
            'slug'    => 'compare',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_code_numeric}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:address/location:iso3166-1numeric & ({state_general_code}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:address/gcm:{addr_tag_xpath}-property-address/gcm:state-abbr combination does not match.',
            'ignore'  => false,
        ],
        /**
         * validate Contract account address tag name in POST request
         *
         * @method postBusinessInvalidContractAccountAddressTag
         * @method versionUpBusinessInvalidContractAccountAddressTag
         */
        [
            'field'   => 'contract_account_address_tag',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_code_numeric}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/location:iso3166-1numeric & /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:{country_tag_name}-property-address combination does not match.',
            'ignore'  => false,
        ],
        /**
         * validate Billing account address tag name in POST request
         *
         * @method postBusinessInvalidBillingAccountAddressTag
         * @method versionUpBusinessInvalidBillingAccountAddressTag
         */
        [
            'field'   => 'billing_account_address_tag',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_code_numeric}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/location:iso3166-1numeric & /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:{country_tag_name}-property-address combination does not match.',
            'ignore'  => false,
        ],
        /**
         * validate Technology account address tag name in POST request
         *
         * @method postBusinessInvalidTechnologyAccountAddressTag
         * @method versionUpBusinessInvalidTechnologyAccountAddressTag
         */
        [
            'field'   => 'technology_account_address_tag',
            'slug'    => '',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_code_numeric}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:address/location:iso3166-1numeric & /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:address/gcm:{country_tag_name}-property-address combination does not match.',
            'ignore'  => false,
        ],
        /**
         * validate Contract account email1 address in POST request
         *
         * @method postBusinessInvalidContractAccountEmail
         * @method versionUpBusinessInvalidContractAccountEmail
         * @method patchBusinessInvalidContractAccountEmail
         */
        [
            'field'   => 'contract_account_email',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({email1}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:person-in-charge/gcm:contact-medium/gcm:email-address is invalid',
            'ignore'  => false,
        ],
        /**
         * validate Billing account email1 address in POST request
         *
         * @method postBusinessInvalidBillingAccountEmail
         * @method versionUpBusinessInvalidBillingAccountEmail
         * @method patchBusinessInvalidBillingAccountEmail
         */
        [
            'field'   => 'billing_account_email',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({email1}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:person-in-charge/gcm:contact-medium/gcm:email-address is invalid',
            'ignore'  => false,
        ],
        /**
         * validate Technology account email1 address in POST request
         *
         * @method postBusinessInvalidTechnologyAccountEmail
         * @method versionUpBusinessInvalidTechnologyAccountEmail
         * @method patchBusinessInvalidTechnologyAccountEmail
         */
        [
            'field'   => 'technology_account_email',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({email1}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:person-in-charge/gcm:contact-medium/gcm:email-address is invalid',
            'ignore'  => false,
        ],
        /**
         * validate billing affiliate org code 1 in POST request
         *
         * @method postBusinessInvalidBillingAffiliateOrgCode1
         * @method versionUpBusinessInvalidBillingAffiliateOrgCode1
         * @method patchBusinessInvalidBillingAffiliateOrgCode1
         */
        [
            'field'   => 'billing_affiliate_org_code1',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({organization_code_1}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-affiliate/gcm:organization-code/gcm:code1 is mismatches with the registered organization code.',
            'ignore'  => false,
        ],
        /**
         * validate billing affiliate org code 2 in POST request
         *
         * @method postBusinessInvalidBillingAffiliateOrgCode2
         * @method versionUpBusinessInvalidBillingAffiliateOrgCode2
         * @method patchBusinessInvalidBillingAffiliateOrgCode2
         */
        [
            'field'   => 'billing_affiliate_org_code2',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({organization_code_2}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-affiliate/gcm:organization-code/gcm:code2 is mismatches with the registered organization code.',
            'ignore'  => false,
        ],
        /**
         * validate compare billing affiliate org code 1 and org code 2 in POST request
         *
         * @method postBusinessInvalidBillingAffiliateOrgCodeCompare
         * @method versionUpBusinessInvalidBillingAffiliateOrgCodeCompare
         * @method patchBusinessInvalidBillingAffiliateOrgCodeCompare
         */
        [
            'field'   => 'billing_affiliate_org_code',
            'slug'    => 'compare',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({organization_code_1}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-affiliate/gcm:organization-code/gcm:code1 & ({organization_code_2}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-affiliate/gcm:organization-code/gcm:code2 combination does not match.',
            'ignore'  => false,
        ],

        /**
         * Validate One stop dependency check for Customer Contract Currency in POST and Version-up Request
         *
         * @method postBusinessInvalidCustContractCurrencyOsd
         * @method versionUpBusinessInvalidCustContractCurrencyOsd
         */
        [
            'field'   => 'cust_contract_currency',
            'slug'    => 'osd',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{cust_contract_currency_xml_id}:1"]/prod-cntrct-item-000002:customer-contract-currency-val not satisfied one stop dependency rule.',
            'ignore'  => false,
        ],
        /**
         * Validate One stop dependency check for Customer Billing Currency in POST and Version-up Request
         *
         * @method postBusinessInvalidCustBillingCurrencyOsd
         * @method versionUpBusinessInvalidCustBillingCurrencyOsd
         */
        [
            'field'   => 'cust_billing_currency',
            'slug'    => 'osd',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{cust_billing_currency_xml_id}:1"]/prod-cntrct-item-000002:customer-billing-currency-val not satisfied one stop dependency rule.',
            'ignore'  => false,
        ],
        /**
         *  Validate One stop dependency check for IGC Contract Currency in POST and Version-up Request
         *
         * @method postBusinessInvalidIgcContractCurrencyOsd
         * @method versionUpBusinessInvalidIgcContractCurrencyOsd
         */
        [
            'field'   => 'igc_contract_currency',
            'slug'    => 'osd',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{igc_contract_currency_xml_id}:1"]/prod-cntrct-item-000002:inter-group-company-contract-currency-val not satisfied one stop dependency rule.',
            'ignore'  => false,
        ],
        /**
         *  Validate One stop dependency check for IGC Billing Currency in POST and Version-up Request
         *
         * @method postBusinessInvalidIgcBillingCurrencyOsd
         * @method versionUpBusinessInvalidIgcBillingCurrencyOsd
         */
        [
            'field'   => 'igc_billing_currency',
            'slug'    => 'osd',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{igc_billing_currency_xml_id}:1"]/prod-cntrct-item-000002:inter-group-company-billing-currency-val not satisfied one stop dependency rule.',
            'ignore'  => false,
        ],
        /**
         *  Validate One stop dependency check for Customer Billing Method in POST and Version-up Request
         *
         * @method postBusinessInvalidCustomerBillingMethodOsd
         * @method versionUpBusinessInvalidCustomerBillingMethodOsd
         */
        [
            'field'   => 'customer_billing_method',
            'slug'    => 'osd',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{customer_billing_method_xml_id}:1"]/prod-cntrct-item-000002:customer-billing-method-val not satisfied one stop dependency rule.',
            'ignore'  => false,
        ],
        /**
         *  Validate One stop dependency check for IGC Settlement Method in POST and Version-up Request
         *
         * @method postBusinessInvalidIgcSettlementMethodOsd
         * @method versionUpBusinessInvalidIgcSettlementMethodOsd
         */
        [
            'field'   => 'igc_settlement_method',
            'slug'    => 'osd',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{igc_settlement_method_xml_id}:1"]/prod-cntrct-item-000002:inter-group-company-settlement-method-val not satisfied one stop dependency rule.',
            'ignore'  => false,
        ],
        /**
         * validate line item direct deposit payee address in POST request
         *
         * @method postBusinessInvalidLinePayeeEmail
         * @method versionUpBusinessInvalidLinePayeeEmail
         * @method patchBusinessInvalidLinePayeeEmail
         */
        [
            'field'   => 'line_payee_email',
            'slug'    => '',
            'methods' => 'post|version_up|patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({payee_email_address}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:payment-method/gcm:jpn-direct-deposit-pm/gcm:email-address is invalid.',
            'ignore'  => false,
        ],
        /**
         * invalid product_offering_uuid with old value/old version
         *
         * @method versionUpBusinessInvalidProductOfferingIdUuidMatch
         */
        [
            'field'   => 'product_offering_id_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({product_offering}) in /contract-info:contract-information/product:bundled-products/product:bundled-product[@id="{product_offering_uuid}:1"]/cbe:identified-by/product:bundled-product-key/cbe:key-type-for-cbe-standard/cbe:object-id is invalid with previous version of contract',
            'ignore'  => false,
        ],
        /**
         * invalid contract_scheme_uuid with old value/old version
         *
         * @method versionUpBusinessInvalidContractSchemeUuidMatch
         */
        [
            'field'   => 'contract_scheme_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({contract_scheme_uuid}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{contract_scheme_uuid}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * invalid contract_type_uuid with old value/old version
         *
         * @method versionUpBusinessInvalidContractTypeUuidMatch
         */
        [
            'field'   => 'contract_type_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({contract_type_uuid}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{contract_type_uuid}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * invalid billing_type_uuid with old value/old version
         *
         * @method versionUpBusinessInvalidBillingTypeUuidMatch
         */
        [
            'field'   => 'billing_type_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({billing_type_uuid}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{billing_type_uuid}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * invalid loi_contract_uuid with old value/old version
         *
         * @method versionUpBusinessInvalidLoiContractUuidMatch
         */
        [
            'field'   => 'loi_contract_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({loi_contract_uuid}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{loi_contract_uuid}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate factory_reference_no_uuid with old value/old version
         *
         * @method versionUpBusinessInvalidFactoryReferenceNoUuidMatch
         */
        [
            'field'   => 'factory_reference_no_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({factory_reference_no_uuid}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{factory_reference_no_uuid}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate line item product specification xml_id with old value/version
         *
         * @method versionUpBusinessInvalidProductSpecificationUuidMatch
         */
        [
            'field'   => 'product_specification_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({product_specification_xml_id}) in /contract-info:contract-information/product:atomic-products/product:atomic-product[@id="{product_specification_xml_id}:1"]/cbe:identified-by/product:atomic-product-key/cbe:key-type-for-cbe-standard/cbe:object-id is invalid with previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate line item service start date xml_id with old version
         *
         * @method versionUpBusinessInvalidServiceStartDateUuidMatch
         */
        [
            'field'   => 'service_start_date_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({service_start_date_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{service_start_date_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate line item service end date xml_id with old version
         *
         * @method versionUpBusinessInvalidServiceEndDateUuidMatch
         */
        [
            'field'   => 'service_end_date_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({service_end_date_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{service_end_date_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate line item billing start date xml_id with old version
         *
         * @method versionUpBusinessInvalidBillingStartDateUuidMatch
         */
        [
            'field'   => 'billing_start_date_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({billing_start_date_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{billing_start_date_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate line item billing end date xml_id with old version
         *
         * @method versionUpBusinessInvalidBillingEndDateUuidMatch
         */
        [
            'field'   => 'billing_end_date_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({billing_end_date_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{billing_end_date_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate line item factory Reference number xml_id with old version
         *
         * @method versionUpBusinessInvalidLineFactoryReferenceNoUuidMatch
         */
        [
            'field'   => 'line_factory_reference_no_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({factory_reference_no_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{factory_reference_no_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate line item customer contract currency xml_id with old version
         *
         * @method versionUpBusinessInvalidCustContractCurrencyUuidMatch
         */
        [
            'field'   => 'cust_contract_currency_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({cust_contract_currency_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{cust_contract_currency_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],

        /**
         * validate line item customer billing currency xml_id with old version
         *
         * @method versionUpBusinessInvalidCustBillingCurrencyUuidMatch
         */
        [
            'field'   => 'cust_billing_currency_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({cust_billing_currency_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{cust_billing_currency_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],

        /**
         * validate line item IGC Contract currency xml_id with old version
         *
         * @method versionUpBusinessInvalidIgcContractCurrencyUuidMatch
         */
        [
            'field'   => 'igc_contract_currency_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({igc_contract_currency_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{igc_contract_currency_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate line item IGC Billing currency xml_id with old version
         *
         * @method versionUpBusinessInvalidIgcBillingCurrencyUuidMatch
         */
        [
            'field'   => 'igc_billing_currency_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({igc_billing_currency_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{igc_billing_currency_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate line item customer billing method xml_id with old version
         *
         * @method versionUpBusinessInvalidCustomerBillingMethodUuidMatch
         */
        [
            'field'   => 'customer_billing_method_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({customer_billing_method_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{customer_billing_method_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],

        /**
         * validate line item IGC Settlement method xml_id with old version
         *
         * @method versionUpBusinessInvalidIgcSettlementMethodUuidMatch
         */
        [
            'field'   => 'igc_settlement_method_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({igc_settlement_method_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{igc_settlement_method_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate line item description xml_id with old version
         *
         * @method versionUpBusinessInvalidLineHeaderDescriptionUuidMatch
         */
        [
            'field'   => 'line_header_description_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({description_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{description_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate line item product configuration xml_id with old version
         *
         * @method versionUpBusinessInvalidLineProductConfigurationUuidMatch
         */
        [
            'field'   => 'line_product_configuration_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({characteristic_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{characteristic_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate line item product rule xml_id with old version
         *
         * @method versionUpBusinessInvalidLineProductRuleUuidMatch
         */
        [
            'field'   => 'line_product_rule_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({rule_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{rule_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * validate line item product price charge xml_id with old version
         *
         * @method versionUpBusinessInvalidLineProductPriceChargeUuidMatch
         */
        [
            'field'   => 'line_product_price_charge_uuid',
            'slug'    => 'match',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({charge_xml_id}) in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{charge_xml_id}:1"]/cbe:identified-by/cbe:charc-value-key/cbe:key-type-for-cbe-standard/cbe:object-id is mismatches with the previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * restrict patch for billing type value is "Separate" in db
         *
         * @method patchBusinessInvalidBillingTypeDb
         * @method versionUpBusinessInvalidBillingTypeDb
         */
        [
            'field'   => 'billing_type_db',
            'slug'    => '',
            'methods' => 'patch|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => 'Billing Scheme value in DB (Separate) is not applicable for API request.',
            'ignore'  => false,
        ],
        /**
         * lock version mandatory in PATCH
         *
         * @method patchBusinessMandatoryLockVersion
         */
        [
            'field'   => 'lock_version',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'Mandatory',
            'layer'   => 'business',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:lock-version is mandatory.',
            'ignore'  => false,
        ],
        /**
         * restrict patch for lock version mismatch
         *
         * @method patchBusinessInvalidLockVersion
         */
        [
            'field'   => 'lock_version',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({lock_version}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:lock-version is mismatches with the current lock version.',
            'ignore'  => false,
        ],
        /**
         * restrict patch for deactivated contract
         *
         * @method patchBusinessInvalidContractStatusDeactivateDb
         * @method versionUpBusinessInvalidContractStatusDeactivateDb
         */
        [
            'field'   => 'contract_status_deactivate_db',
            'slug'    => '',
            'methods' => 'patch|version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202005',
            'message' => 'Contract process terminated. Contract in Deactivated state.',
            'details' => '',
            'ignore'  => false,
        ],

        /**
         * contract end date from xml should be greater than contract start date in db
         *
         * @method patchBusinessInvalidAgreementPeriodEndDateXml
         */
        [
            'field'   => 'agreement_period',
            'slug'    => 'end_date_xml',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({contract_enddate}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/bi:agreement-period/datatypes:end-date-time should be greater than or equal to contract start date value in DB ({contract_startdate})',
            'ignore'  => false,
        ],

        /**
         * contract end date as system date should be greater than contract start date in db
         *
         * @method patchBusinessInvalidAgreementPeriodEndDateSys
         */
        [
            'field'   => 'agreement_period',
            'slug'    => 'end_date_sys',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => 'Contract end date system value ({contract_enddate}) should be greater than or equal to contract start date value in DB ({contract_startdate})',
            'ignore'  => false,
        ],

        /**
         * service start date and service end date range validation in patch
         *
         * @method patchBusinessInvalidServiceDateRange
         */
        [
            'field'   => 'service_date_range',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '{service_end_date_err_msg} should be greater than or equal to {service_start_date_err_msg}',
            'ignore'  => false,
        ],
        /**
         * billing start date and billing end date range validation in patch
         *
         * @method patchBusinessInvalidBillingDateRange
         */
        [
            'field'   => 'billing_date_range',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '{billing_end_date_err_msg} should be greater than or equal to {billing_start_date_err_msg}',
            'ignore'  => false,
        ],
        /**
         * One stop dependency (osd) check for Customer Contract Currency
         *
         * @method patchBusinessInvalidOsdCustContractCurrency
         */
        [
            'field'   => 'osd_cust_contract_currency',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{cust_contract_currency_id}:1"]/prod-cntrct-item-000002:customer-contract-currency-val not satisfied one stop dependency rule.',
            'ignore'  => false,
        ],
        /**
         * One stop dependency (osd) check for Customer Billing Currency
         *
         * @method patchBusinessInvalidOsdCustBillingCurrency
         */
        [
            'field'   => 'osd_cust_billing_currency',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{cust_billing_currency_id}:1"]/prod-cntrct-item-000002:customer-billing-currency-val not satisfied one stop dependency rule.',
            'ignore'  => false,
        ],
        /**
         * One stop dependency (osd) check for IGC Contract Currency
         *
         * @method patchBusinessInvalidOsdIgcContractCurrency
         */
        [
            'field'   => 'osd_igc_contract_currency',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{igc_contract_currency_id}:1"]/prod-cntrct-item-000002:inter-group-company-contract-currency-val not satisfied one stop dependency rule.',
            'ignore'  => false,
        ],
        /**
         * One stop dependency (osd) check for IGC Billing Currency
         *
         * @method patchBusinessInvalidOsdIgcBillingCurrency
         */
        [
            'field'   => 'osd_igc_billing_currency',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{igc_billing_currency_id}:1"]/prod-cntrct-item-000002:inter-group-company-billing-currency-val not satisfied one stop dependency rule.',
            'ignore'  => false,
        ],
        /**
         * One stop dependency (osd) check for Customer Billing Method
         *
         * @method patchBusinessInvalidOsdCustomerBillingMethod
         */
        [
            'field'   => 'osd_customer_billing_method',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{customer_billing_method_id}:1"]/prod-cntrct-item-000002:customer-billing-method-val not satisfied one stop dependency rule.',
            'ignore'  => false,
        ],
        /**
         * One stop dependency (osd) check for IGC Settlement Method
         *
         * @method patchBusinessInvalidOsdIgcSettlementMethod
         */
        [
            'field'   => 'osd_igc_settlement_method',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{igc_settlement_method_id}:1"]/prod-cntrct-item-000002:inter-group-company-settlement-method-val not satisfied one stop dependency rule.',
            'ignore'  => false,
        ],
        /**
         * invalid country code for Contract Account
         *
         * @method patchBusinessInvalidContractAccountCountryCode
         */
        [
            'field'   => 'contract_account',
            'slug'    => 'country_code',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_id_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/location:iso3166-1numeric is mismatches with the registered countries.',
            'ignore'  => false,
        ],
        /**
         * invalid country code for Billing Account
         *
         * @method patchBusinessInvalidBillingAccountCountryCode
         */
        [
            'field'   => 'billing_account',
            'slug'    => 'country_code',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_id_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/location:iso3166-1numeric is mismatches with the registered countries.',
            'ignore'  => false,
        ],
        /**
         * invalid country code for Technical Account
         *
         * @method patchBusinessInvalidTechnicalAccountCountryCode
         */
        [
            'field'   => 'technical_account',
            'slug'    => 'country_code',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_id_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:address/location:iso3166-1numeric is mismatches with the registered countries.',
            'ignore'  => false,
        ],
        /**
         * country code and address block mismatch valiadation for contract account
         *
         * @method patchBusinessInvalidContractAccountCountryTagMismatch
         */
        [
            'field'   => 'contract_account_country_tag_mismatch',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_id_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/location:iso3166-1numeric & /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:{addr_tag_xpath}-address combination does not match.',
            'ignore'  => false,
        ],
        /**
         * country code and address block mismatch valiadation for billing account
         *
         * @method patchBusinessInvalidBillingAccountCountryTagMismatch
         */
        [
            'field'   => 'billing_account_country_tag_mismatch',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_id_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/location:iso3166-1numeric & /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:{addr_tag_xpath}-address combination does not match.',
            'ignore'  => false,
        ],
        /**
         * country code and address block mismatch valiadation for techical representative account
         *
         * @method patchBusinessInvalidTechnicalAccountCountryTagMismatch
         */
        [
            'field'   => 'technical_account_country_tag_mismatch',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_id_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:address/location:iso3166-1numeric & /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:address/gcm:{addr_tag_xpath}-address combination does not match.',
            'ignore'  => false,
        ],
        /**
         * account country and state combination does not match for contract account
         *
         * @method patchBusinessInvalidContractCountryStateMismatch
         */
        [
            'field'   => 'contract_country_state_mismatch',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_id_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/location:iso3166-1numeric & ({state_name_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:{addr_tag_xpath}-address/gcm:state-abbr combination does not match.',
            'ignore'  => false,
        ],
        /**
         * account country and state combination does not match for billing account
         *
         * @method patchBusinessInvalidBillingCountryStateMismatch
         */
        [
            'field'   => 'billing_country_state_mismatch',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_id_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/location:iso3166-1numeric & ({state_name_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:{addr_tag_xpath}-address/gcm:state-abbr combination does not match.',
            'ignore'  => false,
        ],
        /**
         * account country and state combination does not match for technical account
         *
         * @method patchBusinessInvalidTechnicalCountryStateMismatch
         */
        [
            'field'   => 'technical_country_state_mismatch',
            'slug'    => '',
            'methods' => 'patch',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202004',
            'message' => 'Contract process terminated. Invalid Account Detail(s).',
            'details' => '({country_id_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:address/location:iso3166-1numeric & ({state_name_xml}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:customer-technical-representative/gcm:address/gcm:{addr_tag_xpath}-address/gcm:state-abbr combination does not match.',
            'ignore'  => false,
        ],
        /**
         * Check Product offering is same as version up contract
         *
         * @method versionUpBusinessInvalidProductOffering
         */
        [
            'field'   => 'product_offering',
            'slug'    => '',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({product_offering_id}) in /contract-info:contract-information/product:bundled-products/product:bundled-product[@id="{product_offering_uuid_xml}:1"]/cbe:described-by/product:reference-to-bundled-product-offering/product:bundled-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id is invalid with previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * Contract Start Date should not be changed from initial version
         *
         * @method versionUpBusinessInvalidContractStartDate
         */
        [
            'field'   => 'contract_start_date',
            'slug'    => '',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => '({contract_start_date_value}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid_xml}:1"]/bi:agreement-period/datatypes:start-date-time is invalid with previous version of contract.',
            'ignore'  => false,
        ],
        /**
         * If there is no change in the version up request
         *
         * @method versionUpBusinessInvalidNoChange
         */
        [
            'field'   => 'no_change',
            'slug'    => '',
            'methods' => 'version_up',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202006',
            'message' => 'Contract process terminated. Invalid Contract Detail(s).',
            'details' => 'No change in version up request.',
            'ignore'  => false,
        ],
    ],
    /** END : Business invalid Validations for POST|VERSION_UP|PATCH API.2.0 **/
    // group D
    /** START: PMS validation **/
    [
        /**
         * invalid validation for product offering id.
         *
         * @method postPmsInvalidProductOfferingId
         */
        [
            'field'   => 'product_offering',
            'slug'    => 'id',
            'methods' => 'post',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/product:bundled-products/product:bundled-product[@id="{product_offering_xml_id}"]/cbe:described-by/product:reference-to-bundled-product-offering/product:bundled-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_offering_id} ) is invalid',
            'ignore'  => false,
        ],
    ],
    // group E
    [
        /**
         * [PMS] product offering details not empty for specific product offering id in pms.
         *
         * @method postPmsMandatoryProductOfferingDetails
         */
        [
            'field'   => 'product_offering',
            'slug'    => 'details',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/product:bundled-products/product:bundled-product[@id="{product_offering_xml_id}"]/cbe:described-by/product:reference-to-bundled-product-offering/product:bundled-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_offering_id} ) is invalid',
            'ignore'  => false,
        ],
    ],
    // group F
    [
        /**
         * mandatory validation for product specification id.
         *
         * @method postPmsMandatoryProductSpecificationId
         */
        [
            'field'   => 'product_specification',
            'slug'    => 'id',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'pms',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/product:atomic-products/product:atomic-product ({product_specification_id}) is mandatory for bundle-product /contract-info:contract-information/product:bundled-products/product:bundled-product[@id="{product_offering_xml_id}"]/cbe:described-by/product:reference-to-bundled-product-offering/product:bundled-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_offering_id} )',
            'ignore'  => false,
        ],
    ],
    // group G
    [
        /**
         * invalid validation for product specification id.
         *
         * @method postPmsInvalidProductSpecificationId
         */
        [
            'field'   => 'product_specification',
            'slug'    => 'id',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/product:atomic-products/product:atomic-product[@id="{product_specification_xml_id}"]/cbe:described-by/product:reference-to-atomic-product-offering/product:atomic-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_specification_id} ) is invalid for bundle-product /contract-info:contract-information/product:bundled-products/product:bundled-product[@id="{product_offering_xml_id}"]/cbe:described-by/product:reference-to-bundled-product-offering/product:bundled-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_offering_id} )',
            'ignore'  => false,
        ],
    ],
    // group H
    [
        /**
         * [base] mandatory validation for base product specification id.
         *
         * @method postPmsMandatoryProductSpecificationIdBase
         */
        [
            'field'   => 'product_specification',
            'slug'    => 'base',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'pms',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/product:atomic-products/product:atomic-product base atomic product is mandatory.',
            'ignore'  => false,
        ],
    ],
    // group I
    [
        /**
         * mandatory validation for product configuration id.
         *
         * @method postPmsMandatoryProductConfigurationId
         */
        [
            'field'   => 'product_configuration',
            'slug'    => 'id',
            'methods' => 'post',
            'type'    => 'mandatory',
            'layer'   => 'pms',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value ({pms_product_configuration_id}) is mandatory for /contract-info:contract-information/product:atomic-products/product:atomic-product[@id="{product_specification_xml_id}:1"] ({product_specification_id})',
            'ignore'  => true,
        ],
    ],
    // group J
    [
        /**
         * invalid validation for product configuration id.
         *
         * @method postPmsInvalidProductConfigurationId
         */
        [
            'field'   => 'product_configuration',
            'slug'    => 'id',
            'methods' => 'post',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_configuration_xml_id}"] ( {product_configuration_id} ) is {invalid_or_duplicate} for atomic-product /contract-info:contract-information/product:atomic-products/product:atomic-product[@id="{product_specification_xml_id}:1"]/cbe:described-by/product:reference-to-atomic-product-offering/product:atomic-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_specification_id} )',
            'ignore'  => true,
        ],
    ],
    // group K
    [
        /**
         * mandatory validation for product configuration value.
         *
         * @method postPmsMandatoryProductConfigurationValue
         */
        [
            'field'   => 'product_configuration',
            'slug'    => 'value',
            'methods' => 'post',
            'type'    => 'mandatory',
            'layer'   => 'pms',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_configuration_xml_id}"] is mandatory for /contract-info:contract-information/product:atomic-products/product:atomic-product[@id="{product_specification_xml_id}:1"]/cbe:described-by/product:reference-to-atomic-product-offering/product:atomic-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_specification_id} )',
            'ignore'  => true,
        ],
    ],
    // group L
    [
        /**
         * invalid validation for product configuration value.
         *
         * @method postPmsInvalidProductConfigurationValue
         */
        [
            'field'   => 'product_configuration',
            'slug'    => 'value',
            'methods' => 'post',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_configuration_xml_id}"] ( {product_configuration_value} ) is invalid for atomic-product /contract-info:contract-information/product:atomic-products/product:atomic-product[@id="{product_specification_xml_id}:1"]/cbe:described-by/product:reference-to-atomic-product-offering/product:atomic-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_specification_id} )',
            'ignore'  => true,
        ],
    ],
    // group M
    [
        /**
         * mandatory validation for product rule id.
         *
         * @method postPmsMandatoryProductRuleId
         */
        [
            'field'   => 'product_rule',
            'slug'    => 'id',
            'methods' => 'post',
            'type'    => 'mandatory',
            'layer'   => 'pms',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value ({pms_product_rule_id}) is mandatory for /contract-info:contract-information/product:atomic-products/product:atomic-product[@id="{product_specification_xml_id}:1"] ({product_specification_id})',
            'ignore'  => true,
        ],
    ],
    // group N
    [
        /**
         * invalid validation for product rule id.
         *
         * @method postPmsInvalidProductRuleId
         */
        [
            'field'   => 'product_rule',
            'slug'    => 'id',
            'methods' => 'post',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_rule_xml_id}"] ( {product_rule_id} ) is {invalid_or_duplicate} for atomic-product /contract-info:contract-information/product:atomic-products/product:atomic-product[@id="{product_specification_xml_id}:1"]/cbe:described-by/product:reference-to-atomic-product-offering/product:atomic-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_specification_id} )',
            'ignore'  => true,
        ],
    ],
    // group O
    [
        /**
         * mandatory validation for product rule value.
         *
         * @method postPmsMandatoryProductRuleValue
         */
        [
            'field'   => 'product_rule',
            'slug'    => 'value',
            'methods' => 'post',
            'type'    => 'mandatory',
            'layer'   => 'pms',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_rule_xml_id}"] is mandatory for /contract-info:contract-information/product:atomic-products/product:atomic-product[@id="{product_specification_xml_id}:1"]/cbe:described-by/product:reference-to-atomic-product-offering/product:atomic-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_specification_id} )',
            'ignore'  => true,
        ],
    ],
    // group P
    [
        /**
         * mandatory validation for product price id.
         *
         * @method postPmsMandatoryProductPriceId
         */
        [
            'field'   => 'product_price',
            'slug'    => 'id',
            'methods' => 'post',
            'type'    => 'mandatory',
            'layer'   => 'pms',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price ( {pms_product_price_id} ) is mandatory for /contract-info:contract-information/product:atomic-products/product:atomic-product[@id="{product_specification_xml_id}"]/cbe:described-by/product:reference-to-atomic-product-offering/product:atomic-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_specification_id} )',
            'ignore'  => true,
        ],
    ],
    // group Q
    [
        /**
         * invalid validation for product price id.
         *
         * @method postPmsInvalidProductPriceId
         */
        [
            'field'   => 'product_price',
            'slug'    => 'id',
            'methods' => 'post',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} ) is {invalid_or_duplicate} for atomic-product /contract-info:contract-information/product:atomic-products/product:atomic-product[@id="{product_specification_xml_id}:1"]/cbe:described-by/product:reference-to-atomic-product-offering/product:atomic-product-offering-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_specification_id} )',
            'ignore'  => true,
        ],
    ],
    // group R
    [
        /**
         * mandatory validation for product price charge type.
         *
         * @method versionUpPmsMandatoryProductPriceChargeType
         */
        [
            'field'   => 'product_price',
            'slug'    => 'charge_type',
            'methods' => 'version_up',
            'type'    => 'mandatory',
            'layer'   => 'pms',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value/gmone-prod-price-001:charge-type-val is mandatory for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
    ],
    // group S
    [
        /**
         * invalid validation for product price charge type.
         *
         * @method postPmsInvalidProductPriceChargeType
         * @method versionUpPmsInvalidProductPriceChargeType
         */
        [
            'field'   => 'product_price',
            'slug'    => 'charge_type',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_charge_type_xml_id}"]/gmone-prod-price-001:charge-type-val ( {product_charge_type_value} ) is invalid for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
        /**
         * mandatory validation for product price charge period.
         *
         * @method postPmsMandatoryProductPriceChargePeriod
         * @method versionUpPmsMandatoryProductPriceChargePeriod
         */
        [
            'field'   => 'product_price',
            'slug'    => 'charge_period',
            'methods' => 'version_up',
            'type'    => 'mandatory',
            'layer'   => 'pms',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value/gmone-prod-price-001:charge-period-val is mandatory for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
        /**
         * invalid validation for product price charge period.
         *
         * @method postPmsInvalidProductPriceChargePeriod
         * @method versionUpPmsInvalidProductPriceChargePeriod
         */
        [
            'field'   => 'product_price',
            'slug'    => 'charge_period',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_charge_period_xml_id}"]/gmone-prod-price-001:charge-period-val ( {product_charge_period_value} ) is invalid for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="[{product_price_xml_id}]"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
        /**
         * mandatory validation for product price list price.
         *
         * @method versionUpPmsMandatoryProductPriceListPrice
         */
        [
            'field'   => 'product_price',
            'slug'    => 'list_price',
            'methods' => 'version_up',
            'type'    => 'mandatory',
            'layer'   => 'pms',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value/gmone-prod-price-001:list-price-val is mandatory for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
        /**
         * invalid validation for product price list price.
         *
         * @method postPmsInvalidProductPriceListPrice
         * @method versionUpPmsInvalidProductPriceListPrice
         */
        [
            'field'   => 'product_price',
            'slug'    => 'list_price',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_list_price_xml_id}"]/gmone-prod-price-001:list-price-val ( {product_list_price_value} ) is invalid for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
        /**
         * mandatory validation for product price Customer Contract Price.
         *
         * @method postPmsMandatoryProductPriceCustomerContractPrice
         * @method versionUpPmsMandatoryProductPriceCustomerContractPrice
         */
        [
            'field'   => 'product_price',
            'slug'    => 'customer_contract_price',
            'methods' => 'post|version_up',
            'type'    => 'mandatory',
            'layer'   => 'pms',
            'code'    => '4000201001',
            'message' => 'Contract process terminated. Mandatory element(s) should not be empty.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value/gmone-prod-price-001:customer-contract-price-val is mandatory for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
        /**
         * invalid validation for product price Customer Contract Price.
         *
         * @method postPmsInvalidProductPriceCustomerContractPrice
         * @method versionUpPmsInvalidProductPriceCustomerContractPrice
         */
        [
            'field'   => 'product_price',
            'slug'    => 'customer_contract_price',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_customer_contract_price_xml_id}"]/gmone-prod-price-001:customer-contract-price-val ( {product_customer_contract_price_value} ) is invalid for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
        /**
         * mandatory validation for product price icb flag.
         *
         * @method versionUpPmsMandatoryProductPriceIcbFlag
         */
        [
            'field'   => 'product_price',
            'slug'    => 'icb_flag',
            'methods' => 'version_up',
            'type'    => 'mandatory',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/gmone-prod-price-001:is-icb-rate-val is mandatory for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
        /**
         * invalid validation for product price icb flag.
         *
         * @method postPmsInvalidProductPriceIcbFlag
         * @method versionUpPmsInvalidProductPriceIcbFlag
         */
        [
            'field'   => 'product_price',
            'slug'    => 'icb_flag',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_icb_flag_xml_id}"]/gmone-prod-price-001:is-icb-rate-val ( {product_icb_flag_value} ) is invalid for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
        /**
         * invalid validation for product price Currency.
         *
         * @method postPmsInvalidProductPriceCurrency
         * @method versionUpPmsInvalidProductPriceCurrency
         */
        [
            'field'   => 'product_price',
            'slug'    => 'currency',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_currency_xml_id}"]/gmone-prod-price-001:currency-val ( {product_currency_value} ) is invalid for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
        /**
         * invalid validation for product price Igc Settlement Price.
         *
         * @method postPmsInvalidProductPriceIgcSettlementPrice
         * @method versionUpPmsInvalidProductPriceIgcSettlementPrice
         */
        [
            'field'   => 'product_price',
            'slug'    => 'igc_settlement_price',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_igc_settlement_price_xml_id}"]/gmone-prod-price-001:inter-group-company-settlement-price-val ( {product_igc_settlement_price_value} ) is invalid for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
        /**
         * invalid validation for product price Discount amount.
         *
         * @method postPmsInvalidProductPriceDiscount
         * @method versionUpPmsInvalidProductPriceDiscount
         */
        [
            'field'   => 'product_price',
            'slug'    => 'discount_amount',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_discount_amount_xml_id}"]/gmone-prod-price-001:discount-amount-val ( {product_discount_amount_value} ) is invalid for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
        /**
         * invalid validation for product price Discount rate.
         *
         * @method postPmsInvalidProductPriceDiscountRate
         * @method versionUpPmsInvalidProductPriceDiscountRate
         */
        [
            'field'   => 'product_price',
            'slug'    => 'discount_rate',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_discount_rate_xml_id}"]/gmone-prod-price-001:discount-rate-val ( {product_discount_rate_value} ) is invalid for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} )',
            'ignore'  => true,
        ],
        /**
         * invalid validation for product price Discount amount & rate should not exist.
         *
         * @method postPmsInvalidProductPriceDiscountShouldNotExist
         * @method versionUpPmsInvalidProductPriceDiscountShouldNotExist
         */
        [
            'field'   => 'product_price',
            'slug'    => 'discount_should_not_exist',
            'methods' => 'post|version_up',
            'type'    => 'invalid',
            'layer'   => 'pms',
            'code'    => '4000202002',
            'message' => 'Contract process terminated. Product In-Consistency Occurred.',
            'details' => '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_discount_amount_xml_id}"]/gmone-prod-price-001:discount-amount-val ({product_discount_amount_value}) & /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value[@id="{product_discount_rate_xml_id}"]/gmone-prod-price-001:discount-rate-val ({product_discount_rate_value}) is invalid for /contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price[@id="{product_price_xml_id}"]/cbe:association-ends/cbe:association-end/cbe:target/product:reference-to-atomic-product-offering-price/product:atomic-product-offering-price-key/cbe:key-type-for-cbe-standard/cbe:object-id ( {product_price_id} ) because discount rate & discount amount should not be present for same atomic product price',
            'ignore'  => true,
        ],
    ],
    /** END: PMS validation **/
    // group T
    // client request id duplicate check in db
    [
        /**
         * invalid check of client request id in POST request
         *
         * @method postBusinessInvalidClientReqId
         */
        [
            'field'   => 'client_req_id',
            'slug'    => '',
            'methods' => 'post',
            'type'    => 'invalid',
            'layer'   => 'business',
            'code'    => '4000202001',
            'message' => 'Contract process terminated. Client Request ID already exists in the system.',
            'details' => '({contract_request_id}) in /contract-info:contract-information/bi:product-contracts/bi:product-contract[@id="{global_contract_id_uuid}"]/cbe:client-request-id already exists in the system.',
            'ignore'  => false,
        ],
    ],
    /** END: PMS validation **/
];

// priority 10
$api_config['xpath'][] = [
    // group A
    [
        /**
         * validate response xml in GET request.
         *
         * @method getPreResponseInvalidXmlData
         * @method postPreResponseInvalidXmlData
         * @method patchPreResponseInvalidXmlData
         * @method idwhPreResponseInvalidXmlData
         * @method gbsPreResponseInvalidXmlData
         */
        [
            'field'   => 'xml_data',
            'slug'    => '',
            'methods' => 'get|post|patch|idwh|gbs',
            'type'    => 'invalid',
            'layer'   => 'pre_response',
            'code'    => '5000101003',
            'message' => 'Contract process terminated. Please contact System Administrator with this XML message.',
            'details' => '',
            'ignore'  => true,
        ],
    ],
];
