<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (â€œMSAâ€�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */

require_once 'custom/include/XMLUtils.php';
require_once 'custom/include/ModUtils.php';
require_once 'custom/service/v4_1_custom/APIHelper/v2/XMLBrowse.php';
require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';
require_once 'modules/Teams/TeamSet.php';

/**
 * ClassContractBrowse Class
 */
class ClassContractBrowse extends XMLUtils
{

    /**
     * set xml data
     *
     * @var string
     */
    private $xml;

    /**
     * set schema path
     *
     * @var string
     */
    private $schema_path;

    /**
     * set exceptions
     *
     * @var object
     */
    private $exceptions;

    /**
     * xml errors
     *
     * @var array
     */
    private $xmlerrors;

    /**
     * set dom array
     *
     * @var array
     */
    private $dom_array;

    /**
     * set dom obj
     *
     * @var object
     */
    private $dom_obj;

    /**
     * set root data
     *
     * @var array
     */
    private $root;

    /* Class Variable */

    /**
     * Class Constructor to initialize class variables
     */
    public function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Constructor function to initialize the variables');
        $this->xml         = '';
        $this->schema_path = '';
        $this->exceptions  = array();
        $this->xmlerrors   = array();
        $this->dom_array   = array();
        $this->root        = array();
        libxml_use_internal_errors(true);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * Method to build the date time format in standard format
     *
     * @param string $value Date
     * @param string $zone Time Zone
     * @return datetime Formatted Date as Y-m-d/THH:MM/UTC
     * @author Sathish.Kumar.Ganesan
     * @since Jun 04, 2016
     */
    private function formatDate($value, $zone)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'value: '.$value.' '.GCM_GL_LOG_VAR_SEPARATOR.' zone: '.$zone, 'Method to build the date time format in standard format');
		$modUtils = new ModUtils();
        $zone = $modUtils->checkAndSetDefaultTimezone($zone);
        $dt     = new SugarDateTime($value);
        $dt_arr = explode(' ', TimeDate::getInstance()->asDb($dt));
        $return_val = $dt_arr[0] . 'T' . $dt_arr[1] . $zone;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_val: '.$return_val, '');
        return $return_val;
    }

    /**
     * Common Method to sanitize the data
     *
     * @param mixed $value It can be the array which hold the value and It can be the string
     * @return mixed It can be the array with sanitized values or it can be the sanitized string
     * @author Sathish.Kumar.Ganesan
     * @since Jun 04, 2016
     */
    private function sanitize($value)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'value: '.print_r($value,true), 'Common Method to sanitize the data');
        if (is_array($value) && count($value) > 0) {
            $sanitize_val = array();
            foreach ($value as $key => $val) {
                if (is_array($val)) {
                    $this->sanitize($val);
                } elseif ($val != '') {
                    $val = html_entity_decode($val, ENT_QUOTES | 'ENT_XML1');
                    $val = htmlspecialchars($val, ENT_QUOTES | 'ENT_XML1');
                }
                $sanitize_val[$key] = $val;
            }
        } else {
            $sanitize_val = '';
            $sanitize_val = html_entity_decode($value, ENT_QUOTES | 'ENT_XML1');
            $sanitize_val = htmlspecialchars($sanitize_val, ENT_QUOTES | 'ENT_XML1');
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'sanitize_val: '.print_r($sanitize_val, true), '');
        return $sanitize_val;
    }

    /**
     * Method to format the PMS ID
     *
     * @param string $pms_id PMS ID
     * @param int $type Used to indicate the prefix value in the format, the default value is 1
     * @return string Formatted PMS ID string value.
     * @author Sathish.Kumar.Ganesan
     * @since Jun 04, 2016
     */
    private function formatPMSID($pms_id, $type)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'pms_id: '.$pms_id.' '.GCM_GL_LOG_VAR_SEPARATOR.'type: '.$type, 'Method to format the PMS ID');
        $GLOBALS['log']->info("Begin: ClassContractBrowse->formatPMSID($pms_id, $type)");
        $prefix = '00000000-0000-0000-0000-';
        if ($type == 2) {
            $prefix = '00000001-0000-0000-0000-';
        }
        $return_val = $prefix . sprintf('%012s', $pms_id);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_val: '.$return_val, '');
        return $return_val;
    }

    /**
     * Method to construct the get response for contract
     *
     * @param string $object_id - Object ID of the contract
     * @param string $contract_id - Global Contract ID of the contract
     * @param string $version_number contract version no
     * @param string $str_interaction_status  contract interacting status
     * @return string Returns the xml string.
     * @author Sathish.Kumar.Ganesan
     * @since Jun 04, 2016
     */
    public function getContractXML($object_id, $contract_id, $version_number, $str_interaction_status)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'object_id: '.$object_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' contract_id: '.$contract_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' version_number: '.$version_number.' '.GCM_GL_LOG_VAR_SEPARATOR.' str_interaction_status: '.$str_interaction_status, 'Method to construct the get response for contract');
        include 'custom/include/globalUUIDConfig.php';
        global $sugar_config;
        $this->dom_array                              = $arr_respone_data                              = array();
        $where_cond                                   = '';
        $arr_respone_data['respone_code']             = '0';
        $this->dom_array['glb_parameters']['api_url'] = $sugar_config['site_url'] . 'api/v2';
        $this->schema_path                            = 'https://gcm.ntt.com/api/xml-schema/v2/';
        if (trim($object_id) != '') {
            $where_cond = "global_contract_id_uuid='" . $object_id . "'";
        } else if (trim($contract_id) != '') {
            $where_cond = "global_contract_id='" . $contract_id . "'";
        } else {
            $arr_respone_data['respone_error_code'][] = '3002';
        }
        if (!isset($arr_respone_data['respone_error_code'])) {
            if (trim($version_number) != '') {
                $version_number = sprintf('%.1f', $version_number);
                $where_cond .= " AND contract_version = " . $version_number;
            }
            if (trim($str_interaction_status) != '') {
                $arr_interaction_status = explode(',', $str_interaction_status);
                $arr_contract_status    = array();
                foreach ($arr_interaction_status as $status) {
                    $interaction_status_index = array_search(trim($status), $interaction_status);
                    if ($interaction_status_index !== false) {
                        $arr_contract_status = array_merge($arr_contract_status, array_keys($contract_interaction_status_mapping, $interaction_status_index));
                    }
                }
                if (!empty($arr_contract_status)) {
                    $where_cond .= " AND contract_status IN (" . implode(',', $arr_contract_status) . ")";
                } else {
                    $arr_respone_data['respone_error_code'][] = '6001';
                }
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'where_cond: '.$where_cond.' '.GCM_GL_LOG_VAR_SEPARATOR.' arr_respone_data: '.print_r($arr_respone_data,true), 'Check the condition for response error code and where condition');
        if (!isset($arr_respone_data['respone_error_code']) && $where_cond != '') {
            $this->setRootNodes();
            $contract      = BeanFactory::getBean('gc_Contracts');
            $contract_list = $contract->get_full_list('contract_version desc', $where_cond);

            if (!empty($contract_list)) {
                $contract = $contract->retrieve($contract_list[0]->id);
            }
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract: '.$contract->id, 'Contract Details from Sugar Bean');
            if ($contract->name != '' && $contract->product_offering != '') {
                $i = $j = 0;
                //product offering details from pms
                $pms_obj                 = new ClassProductConfiguration();
                $product_offering_detail = $pms_obj->getProductOfferingDetails($contract->product_offering);
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_offering_detail: '.print_r($product_offering_detail,true), 'Product Offering Details');
                if ($product_offering_detail['response_code'] == 1) {
                    $contract_id = $contract->id;
                    $contract_version = round($contract->contract_version);
                    //Bundled product block
                    $this->dom_array['bundle_product']['char_uuid'] = $contract->product_offering_uuid;
                    $this->dom_array['bundle_product']['name']      = $product_offering_detail['respose_array']['offering_details']['offering_name'];
                    $this->dom_array['bundle_product']['pms_uuid']  = $this->formatPMSID($contract->product_offering, 1);
                    $this->dom_array['bundle_product']['version']   = '1';
                    $this->dom_array['bundle_product']              = $this->sanitize($this->dom_array['bundle_product']);
                    
                    //Atomic product block
                    $line_item_bean              = BeanFactory::getBean('gc_Line_Item_Contract_History');
                    $line_item_list              = $line_item_bean->get_full_list('line_item_id asc', "contracts_id='" . $contract_id . "'");
                    $arr_atomic_product_uuid     = array();
                    $billing_account_id          = $technical_account_id          = $payment_type          = $contract_line_item_history_id          = $line_item_id          = $organization_id          = '';
                    $contract_interaction_status = $interaction_status[$contract_interaction_status_mapping[$contract->contract_status]];
                    ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_item_list: '.count($line_item_list), 'Line Item Details from Sugar Bean');
                    if ($line_item_list != '') {
                        foreach ($line_item_list as $atomic_product) {
                            if ($atomic_product->contract_line_item_type == 'Service_Termination' && ($contract->contract_status == 2 || $contract->contract_status == 4 || $contract->contract_status == 5 || $contract->contract_status == 6 || $contract->contract_status == 7)) {
                                continue;
                            }
                            $speccode                                            = $product_offering_detail['respose_array']['specification_details'][$atomic_product->product_specification]['speccode'];
                            $this->root['xmlns:' . $speccode]                    = $this->schema_path . 'product/' . $speccode;
                            $this->dom_array['atomic_products'][$i]['char_uuid'] = $atomic_product->product_specification_uuid;
                            $this->dom_array['atomic_products'][$i]['name']      = $product_offering_detail['respose_array']['specification_details'][$atomic_product->product_specification]['specname'];
                            $this->dom_array['atomic_products'][$i]['pms_uuid']  = $this->formatPMSID($atomic_product->product_specification, 1);
                            $this->dom_array['atomic_products'][$i]['version']   = '1';

                            $this->dom_array['atomic_products'][$i] = $this->sanitize($this->dom_array['atomic_products'][$i]);
                            //$arr_line_item_id[$i] = $value->line_item_id;
                            $arr_atomic_product_uuid[$atomic_product->line_item_id]['uuid']   = $atomic_product->product_specification_uuid;
                            $arr_atomic_product_uuid[$atomic_product->line_item_id]['pms_id'] = $atomic_product->product_specification;

                            $bi_action = isset($atomic_product->bi_action) ? $GLOBALS['app_list_strings']['bi_action_list'][$atomic_product->bi_action] : "";

                            //Bi item block data construction
                            $this->dom_array['product_biitems'][$i]['char_uuid']               = $atomic_product->line_item_id_uuid;
                            $this->dom_array['product_biitems'][$i]['standard_business_state'] = isset($atomic_product->contract_line_item_type) ? $GLOBALS['app_list_strings']['contract_line_item_type_list'][$atomic_product->contract_line_item_type] : ''; // Contract Line Item Type
                            if ($contract_interaction_status == $interaction_status[0]) {
                                if ($atomic_product->contract_line_item_type == 'Activated' && $atomic_product->bi_action == 'no_change') {
                                    $atomic_product->bi_action = 'no_change2';
                                } else if ($atomic_product->contract_line_item_type == 'Service_Termination' && $atomic_product->bi_action == 'remove') {
                                    $atomic_product->bi_action = 'remove2';
                                }
                            }
                            $this->dom_array['product_biitems'][$i]['standard_state']           = isset($atomic_product->contract_line_item_type) && isset($atomic_product->bi_action) ? $standard_state[$lineitem_action_statdardstate_status_mapping[$atomic_product->contract_line_item_type][$atomic_product->bi_action]] : '';
                            $this->dom_array['product_biitems'][$i]['product_contract_uuid']    = $contract->global_contract_id_uuid;
                            $this->dom_array['product_biitems'][$i]['product_contract_version'] = $contract_version;
                            $this->dom_array['product_biitems'][$i]['atomic_products_uuid']     = $atomic_product->product_specification_uuid;
                            $this->dom_array['product_biitems'][$i]['atomic_products_version']  = '1'; // hard coded
                            $this->dom_array['product_biitems'][$i]['described_by']             = $ARR_GUUID['general']['product_biitem'];
                            $this->dom_array['product_biitems'][$i]['action']                   = $bi_action;
                            $this->dom_array['product_biitems'][$i]                             = $this->sanitize($this->dom_array['product_biitems'][$i]);
                            //Atomic product characteristics data construction
                            foreach ($ARR_GUUID['gc_line_item_contract_history'] as $key => $atomic_prod_char_field) {
                                if ($atomic_product->$key != '') {
                                    $this->dom_array['apbi_char_val'][$j]['char_uuid']    = $atomic_product->{$key . '_uuid'};
                                    $this->dom_array['apbi_char_val'][$j]['pbi_uuid']     = $atomic_product->line_item_id_uuid;
                                    $this->dom_array['apbi_char_val'][$j]['described_by'] = $atomic_prod_char_field['uuid'];
                                    $this->dom_array['apbi_char_val'][$j]['tag']          = $atomic_prod_char_field['tag'];
                                    if (strpos($key, '_date') !== false) {
                                        $timezone = '';
                                        if ($atomic_product->{$atomic_prod_char_field['timezone_field']} != '') {
                                            $obj_timezone = BeanFactory::getBean('gc_TimeZone', $atomic_product->{$atomic_prod_char_field['timezone_field']});
                                            $timezone     = $obj_timezone->utcoffset;
                                        }
                                        $this->dom_array['apbi_char_val'][$j]['value'] = $this->formatDate($atomic_product->$key, $timezone);
                                    } else {
                                        $this->dom_array['apbi_char_val'][$j]['value'] = $atomic_product->$key;
                                    }
                                    $this->dom_array['apbi_char_val'][$j]['version'] = '1'; //hardcode
                                    $this->dom_array['apbi_char_val'][$j]            = $this->sanitize($this->dom_array['apbi_char_val'][$j]);
                                    $j++;
                                }
                            }

                            $billing_account_id            = $atomic_product->bill_account_id;
                            $technical_account_id          = $atomic_product->tech_account_id;
                            $payment_type                  = $atomic_product->payment_type;
                            $contract_line_item_history_id = $atomic_product->id;
                            $organization_id               = $atomic_product->gc_organization_id_c;
                            $i++;
                        }
                        if (isset($this->dom_array['apbi_char_val'])) {
                            $GLOBALS['log']->info("Bi item characteristic & Info " . print_r($this->dom_array['apbi_char_val'], true));
                        }
                        // Atomic product price block
                        $line_item_id       = implode(",", array_keys($arr_atomic_product_uuid));
                        $line_item_id       = str_replace(',', "','", $line_item_id);
                        $product_price_bean = BeanFactory::getBean('gc_ProductPrice');
                        $product_price_list = $product_price_bean->get_full_list('line_item_id asc', "line_item_id in ('" . $line_item_id . "')");
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_item_id: '.$line_item_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' product_price_list: '.count($product_price_list), 'Product Price Details from Sugar Bean');
                        if ($product_price_list != null) {
                            $i = $j = 0;
                            foreach ($product_price_list as $atomic_product_price) {
                                $this->dom_array['atomic_product_prices'][$i]['char_uuid']      = $atomic_product_price->charge_uuid;
                                $this->dom_array['atomic_product_prices'][$i]['name']           = isset($atomic_product_price->name) && isset($product_offering_detail['respose_array']['charge_details'][$atomic_product_price->name]['chargedesc']) ? $product_offering_detail['respose_array']['charge_details'][$atomic_product_price->name]['chargedesc'] : '';
                                $this->dom_array['atomic_product_prices'][$i]['pms_uuid']       = $this->formatPMSID($atomic_product_price->name, 1);
                                $this->dom_array['atomic_product_prices'][$i]['version']        = '1'; //hardcoded
                                $this->dom_array['atomic_product_prices'][$i]['atomic_product'] = $arr_atomic_product_uuid[$atomic_product_price->line_item_id]['uuid']; // Related Atomic Products Local UUID
                                $this->dom_array['atomic_product_prices'][$i]['described_by']   = $ARR_GUUID['general']['atomic_product_price'];
                                $this->dom_array['atomic_product_prices'][$i]                   = $this->sanitize($this->dom_array['atomic_product_prices'][$i]);
                                //Atomic price characteristics data construction
                                foreach ($ARR_GUUID['gc_productprice'] as $key => $product_price_field) {
                                    $value_key = $key;
                                    if ($key == 'discount_rate' || $key == 'discount_amount') {
                                        $value_key = 'discount';
                                    }
                                    if ($atomic_product_price->$value_key != '') {
                                        if($atomic_product_price->icb_flag==1 && ($value_key=='discount' || $value_key=='list_price')){
                                            continue;
                                        }
                                        $this->dom_array['app_char_val'][$j]['char_uuid']     = $atomic_product_price->{$key . '_uuid'};
                                        $this->dom_array['app_char_val'][$j]['ap_price_uuid'] = $atomic_product_price->charge_uuid;
                                        $this->dom_array['app_char_val'][$j]['described_by']  = $product_price_field['uuid'];
                                        $this->dom_array['app_char_val'][$j]['tag']           = $product_price_field['tag'];
                                        $price_field_value                                    = $atomic_product_price->$value_key;
                                        if (($key == 'discount_rate' && $atomic_product_price->percent_discount_flag == 0) || ($key == 'discount_amount' && $atomic_product_price->percent_discount_flag == 1)) {
                                            $price_field_value = 0;
                                        }
                                        if ($value_key == 'icb_flag') {
                                            $price_field_value = $boolean_array[$atomic_product_price->$value_key];
                                        } else if ($value_key == 'charge_type') {
                                            $price_field_value = $product_offering_detail['respose_array']['charge_details'][$atomic_product_price->name]['chargeid'] . ' : ' . $product_offering_detail['respose_array']['charge_details'][$atomic_product_price->name]['chargetype'];
                                        } else if ($value_key == 'charge_period') {
                                            $price_field_value = $product_offering_detail['respose_array']['charge_details'][$atomic_product_price->name]['chargeperiodid'] . ' : ' . $product_offering_detail['respose_array']['charge_details'][$atomic_product_price->name]['chargeperioddesc'];
                                        }
                                        $this->dom_array['app_char_val'][$j]['value']   = $price_field_value;
                                        $this->dom_array['app_char_val'][$j]['version'] = '1'; //hardcoded
                                        $this->dom_array['app_char_val'][$j]            = $this->sanitize($this->dom_array['app_char_val'][$j]);
                                        $j++;
                                    }
                                }
                                $i++;
                            }
                        }
                    }

                    //root block
                    $this->xml .= xmlRootBlock($this->root);
                    
                    ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['bundle_product']: ".print_r($this->dom_array['bundle_product'],true), 'Bundle Product Details');
                    $this->xml .= xmlBundledProducts($this->dom_array);                    
                    unset($this->dom_array['bundle_product']);

                    if (isset($this->dom_array['atomic_products'])) {
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['atomic_products']: ".print_r($this->dom_array['atomic_products'],true), 'Atomic Product Details');
                        $this->xml .= xmlAtomicProducts($this->dom_array);
                        unset($this->dom_array['atomic_products']);
                    }

                    if (isset($this->dom_array['atomic_product_prices'])) {
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['atomic_product_prices']: ".print_r($this->dom_array['atomic_product_prices'],true), 'Atomic Product Price Details');
                        $this->xml .= xmlAtomicProductPrices($this->dom_array);
                        unset($this->dom_array['atomic_product_prices']);
                    }
                    
                    if (isset($this->dom_array['app_char_val'])){
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['app_char_val']: ".print_r($this->dom_array['app_char_val'],true), 'Product Price Characteristics Details');
                    }

                    // Product contract block
                    $arr_users = array();
                    $user_bean = BeanFactory::getBean('Users');
                    $user_list = $user_bean->get_full_list('', "users.id in ('" . $contract->created_by . "','" . $contract->modified_user_id . "')", false, 2);
                    ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'created_by: '.$contract->created_by.' '.GCM_GL_LOG_VAR_SEPARATOR.' modified_user_id: '.$contract->modified_user_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' user_list: '.count($user_list), 'User List from Sugar Bean');
                    if ($user_list != null) {
                        foreach ($user_list as $key => $value) {
                            $arr_users[$value->id] = $value->first_name . ' ' . $value->last_name; //todo: need to confirm
                        }
                    }
                    $this->dom_array['product_contracts']['char_uuid']               = $contract->global_contract_id_uuid;
                    $this->dom_array['product_contracts']['version']                 = $contract_version;
                    $this->dom_array['product_contracts']['created_date']            = $this->formatDate($contract->date_entered, $contract->date_entered_timezone);
                    $this->dom_array['product_contracts']['created_user']            = trim($arr_users[$contract->created_by]);
                    $this->dom_array['product_contracts']['last_modified_date']      = $this->formatDate($contract->date_modified, $contract->date_modified_timezone);
                    $this->dom_array['product_contracts']['last_modified_user']      = trim($arr_users[$contract->modified_user_id]);
                    $this->dom_array['product_contracts']['external_created_user']   = $contract->ext_sys_created_by; // External System Created By
                    $this->dom_array['product_contracts']['external_modified_user']  = $contract->ext_sys_modified_by; // External System Modified By
                    $this->dom_array['product_contracts']['lock_version']            = round($contract->lock_version);
                    $this->dom_array['product_contracts']['client_request_id']       = $contract->contract_request_id;
                    $this->dom_array['product_contracts']['name']                    = $contract->name;
                    $this->dom_array['product_contracts']['description']             = $contract->description;
                    $this->dom_array['product_contracts']['interaction_status']      = $contract_interaction_status;
                    $this->dom_array['product_contracts']['standard_business_state'] = $GLOBALS['app_list_strings']['contract_status_list'][$contract->contract_status]; // Contract Status
                    $this->dom_array['product_contracts']['product_offering_uuid']   = $contract->product_offering_uuid;
                    $this->dom_array['product_contracts']['described_by']            = $ARR_GUUID['general']['product_contract'];
                    $this->dom_array['product_contracts']['contract_id']             = $contract->global_contract_id;
                    $this->dom_array['product_contracts']['b_version']               = 1;
                    $startdate_timezone                                              = $enddate_timezone                                              = '';
                    if ($contract->gc_timezone_id_c != '') {
                        $obj_timezone       = BeanFactory::getBean('gc_TimeZone', $contract->gc_timezone_id_c);
                        $startdate_timezone = $obj_timezone->utcoffset;
                    }
                    if ($contract->gc_timezone_id1_c != '') {
                        $obj_timezone     = BeanFactory::getBean('gc_TimeZone', $contract->gc_timezone_id1_c);
                        $enddate_timezone = $obj_timezone->utcoffset;
                    }
                    $this->dom_array['product_contracts']['start_date_time'] = ($contract->contract_startdate != '0000-00-00 00:00:00' && $contract->contract_startdate != '' && !is_null($contract->contract_startdate)) ? $this->formatDate($contract->contract_startdate, $startdate_timezone) : '';
                    $this->dom_array['product_contracts']['end_date_time']   = ($contract->contract_enddate != '0000-00-00 00:00:00' && $contract->contract_enddate != '' && !is_null($contract->contract_enddate)) ? $this->formatDate($contract->contract_enddate, $enddate_timezone) : '';

                    // Product characteristics value construction
                    $i = 0;
                    foreach ($ARR_GUUID['gc_contracts'] as $key => $product_field) {
                        if ($contract->$key != '') {
                            $this->dom_array['pc_char_val'][$i]['char_uuid']    = $contract->{$key . '_uuid'};
                            $this->dom_array['pc_char_val'][$i]['pc_uuid']      = $contract->global_contract_id_uuid;
                            $this->dom_array['pc_char_val'][$i]['described_by'] = $product_field['uuid'];
                            $this->dom_array['pc_char_val'][$i]['tag']          = $product_field['tag'];
                            $this->dom_array['pc_char_val'][$i]['value']        = $contract->$key;
                            if ($key == 'loi_contract') {
                                $this->dom_array['pc_char_val'][$i]['value'] = $boolean_array[strtolower($contract->$key)];
                            } else if ($key == 'contract_scheme') {
                                $this->dom_array['pc_char_val'][$i]['value'] = $GLOBALS['app_list_strings']['contract_scheme_list'][$contract->$key];
                            } else if ($key == 'billing_type') {
                                $this->dom_array['pc_char_val'][$i]['value'] = $GLOBALS['app_list_strings']['billing_type_list'][$contract->$key];
                            }
                            $this->dom_array['pc_char_val'][$i]['version']    = '1'; // hardcoded
                            $this->dom_array['pc_char_val'][$i]['pc_version'] = $contract_version; // hardcoded
                            $this->dom_array['pc_char_val'][$i]               = $this->sanitize($this->dom_array['pc_char_val'][$i]);
                            $i++;
                        }
                    }

                    // Product characteristics team block
                    $teamSetBean = new TeamSet();
                    $teams       = $teamSetBean->getTeams($contract->team_set_id);
                    ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'team_set_id: '.$contract->team_set_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' teams: '.count($teams), 'Product Team List from Sugar Bean');
                    foreach ($teams as $team) {
                        if ($team->id != '1') {
                            $this->dom_array['pc_char_val'][$i]['char_uuid']    = $team->id;
                            $this->dom_array['pc_char_val'][$i]['pc_uuid']      = $contract->global_contract_id_uuid;
                            $this->dom_array['pc_char_val'][$i]['described_by'] = $ARR_GUUID['teams']['uuid'];
                            $this->dom_array['pc_char_val'][$i]['tag']          = $ARR_GUUID['teams']['tag'];
                            $this->dom_array['pc_char_val'][$i]['value']        = $team->name;
                            if ($team->name_2 != '') {
                                $this->dom_array['pc_char_val'][$i]['value'] = $team->name . ' ' . $team->name_2;
                            }
                            $this->dom_array['pc_char_val'][$i]['version']    = '1'; // hardcoded
                            $this->dom_array['pc_char_val'][$i]['pc_version'] = $contract_version; // hardcoded
                            $this->dom_array['pc_char_val'][$i]               = $this->sanitize($this->dom_array['pc_char_val'][$i]);
                            $i++;
                        }
                    }

                    //Contract Customer Block
                    $contact_corporate = $contract->get_linked_beans('contacts_gc_contracts_1', 'Contacts');
                    ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contact_corporate: '.count($contact_corporate), 'Contract Corporate from Sugar Bean');
                    if (count($contact_corporate) == 1) {
                        $this->getCustomerDetails('', 'contract', $contact_corporate[0]); // todo: need to check and optimize
                        $this->dom_array['cd_contract']['customer_details']['corporation_vat_no'] = $contract->vat_no;
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['cd_contract']: ".print_r($this->dom_array['cd_contract'],true), 'Contracting Customer Details');
                    }

                    //Billing Customer Block
                    if ($billing_account_id != '') {
                        $this->getCustomerDetails($billing_account_id, 'billing', null);
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['cd_billing']: ".print_r($this->dom_array['cd_billing'],true), 'Billing Customer Details');
                    }

                    //Sales representative Block
                    $this->getSalesRepresentativeDetails($contract->id);
                    if (isset($this->dom_array['cd_sales'])) {
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['cd_sales']: ".print_r($this->dom_array['cd_sales'],true), 'Sales Customer Details');
                    }

                    //Billing Affiliate Block
                    $this->getBillingAffiliateDetails($organization_id);
                    if (isset($this->dom_array['cd_billing_affiliate'])) {
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['cd_billing_affiliate']: ".print_r($this->dom_array['cd_billing_affiliate'],true), 'Billing Affiliate Details');
                    }

                    //Technical Customer Block
                    if ($technical_account_id != '') {
                        $this->getCustomerDetails($technical_account_id, 'technical', null);
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['cd_technical']: ".print_r($this->dom_array['cd_technical'],true), 'Technical Customer Details');
                    }

                    //Payment Block
                    if ($payment_type != '') {
                        if ($payment_type == '00') {
                            // General: Direct Deposit
                            $payment_bean = BeanFactory::getBean('gc_DirectDeposit');
                            $payment_bean->retrieve_by_string_fields(array(
                                'contract_history_id' => $contract_line_item_history_id,
                            ));
                            $this->dom_array['payment_method']['payment_type']                = 00;
                            $this->dom_array['payment_method']['dd_bank_code']                = $payment_bean->bank_code;
                            $this->dom_array['payment_method']['dd_branch_code']              = $payment_bean->branch_code;
                            $this->dom_array['payment_method']['dd_deposit_type']             = $payment_bean->deposit_type;
                            $this->dom_array['payment_method']['dd_account_number']           = $payment_bean->account_no;
                            $this->dom_array['payment_method']['dd_name_of_person_in_charge'] = $payment_bean->payee_contact_person_name_2;
                            $this->dom_array['payment_method']['dd_telephone_number']         = $payment_bean->payee_tel_no;
                            $this->dom_array['payment_method']['dd_email_address']            = $payment_bean->payee_email_address;
                            $this->dom_array['payment_method']['dd_remarks']                  = $payment_bean->remarks;

                        } else if ($payment_type == '10') {
                            //Account Transfer
                            $payment_bean = BeanFactory::getBean('gc_AccountTransfer');
                            $payment_bean->retrieve_by_string_fields(array(
                                'contract_history_id' => $contract_line_item_history_id,
                            ));
                            $this->dom_array['payment_method']['payment_type']                 = 10;
                            $this->dom_array['payment_method']['at_bank_account_holder_name']  = $payment_bean->name;
                            $this->dom_array['payment_method']['at_bank_code']                 = $payment_bean->bank_code;
                            $this->dom_array['payment_method']['at_branch_code']               = $payment_bean->branch_code;
                            $this->dom_array['payment_method']['at_deposit_type']              = $payment_bean->deposit_type;
                            $this->dom_array['payment_method']['at_account_number']            = $payment_bean->account_no;
                            $this->dom_array['payment_method']['at_is_account_number_display'] = $payment_bean->account_no_display;

                        } else if ($payment_type == '30') {
                            //Postal Transfer
                            $payment_bean = BeanFactory::getBean('gc_PostalTransfer');
                            $payment_bean->retrieve_by_string_fields(array(
                                'contract_history_id' => $contract_line_item_history_id,
                            ));
                            $this->dom_array['payment_method']['payment_type']                         = 30;
                            $this->dom_array['payment_method']['pt_postal_account_holder_name']        = $payment_bean->name;
                            $this->dom_array['payment_method']['pt_postal_passbook_mark']              = $payment_bean->postal_passbook_mark;
                            $this->dom_array['payment_method']['pt_postal_passbook_number']            = $payment_bean->postal_passbook;
                            $this->dom_array['payment_method']['pt_is_postal_passbook_number_display'] = $payment_bean->postal_passbook_no_display;

                        } else if ($payment_type == '60') {
                            //Credit Card
                            $payment_bean = BeanFactory::getBean('gc_CreditCard');
                            $payment_bean->retrieve_by_string_fields(array(
                                'contract_history_id' => $contract_line_item_history_id,
                            ));
                            $this->dom_array['payment_method']['payment_type']          = 60;
                            $this->dom_array['payment_method']['cc_credit_card_number'] = $payment_bean->credit_card_no;
                            $this->dom_array['payment_method']['cc_expiration_date']    = $payment_bean->expiration_date;
                        }
                        $this->dom_array['payment_method'] = $this->sanitize($this->dom_array['payment_method']);
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['payment_method']: ".print_r($this->dom_array['payment_method'],true), 'Payment Method Details');
                    }

                    if (isset($this->dom_array['pc_char_val'])) {
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['pc_char_val']: ".print_r($this->dom_array['pc_char_val'],true), 'Product Contract characteristic Details');
                    }
                    $this->dom_array['product_contracts'] = $this->sanitize($this->dom_array['product_contracts']);
                    ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['product_contracts']: ".print_r($this->dom_array['product_contracts'],true), 'Product Contract Details');
                    $this->xml .= xmlProductContracts($this->dom_array);
                    unset($this->dom_array['product_contracts']);

                    // Bi-item block construction
                    if (isset($this->dom_array['product_biitems'])) {
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['product_biitems']: ".print_r($this->dom_array['product_biitems'],true), 'Product Bi Items Details');
                        $this->xml .= xmlProductBiitems($this->dom_array);
                        unset($this->dom_array['product_biitems']);
                    }

                    if ($line_item_id != '') {
                        //Product Configuration and Production Information block for each line item
                        $product_config_bean     = BeanFactory::getBean('gc_ProductConfiguration');
                        $product_config_list     = $product_config_bean->get_full_list('', "line_item_id in ('" . $line_item_id . "')");
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_item_id: '.$line_item_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' product_config_list: '.count($product_config_list), 'Product Configuration List from Sugar Bean');
                        $product_config_list_cus = array();
                        if ($product_config_list != null) {
                            foreach ($product_config_list as $key => $value) {
                                $product_config_list_cus[$value->line_item_id][$key] = $value;
                            }
                            unset($product_config_list);
                        }

                        $product_rule_bean     = BeanFactory::getBean('gc_ProductRule');
                        $product_rule_list     = $product_rule_bean->get_full_list('', "line_item_id in ('" . $line_item_id . "')");
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_item_id: '.$line_item_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' product_rule_list: '.count($product_rule_list), 'Product Rule List from Sugar Bean');
                        $product_rule_list_cus = array();
                        if ($product_rule_list != null) {
                            foreach ($product_rule_list as $key => $value) {
                                $product_rule_list_cus[$value->line_item_id][$key] = $value;
                            }
                            unset($product_rule_list);
                        }
                        $i = 0;
                        foreach ($arr_atomic_product_uuid as $line_item_id => $atomic_value) {
                            if (isset($product_config_list_cus[$line_item_id])) {
                                $speccode = $product_offering_detail['respose_array']['specification_details'][$atomic_value['pms_id']]['speccode'];
                                foreach ($product_config_list_cus[$line_item_id] as $key => $value) {
                                    if (trim($value->characteristic_value) != '') {
                                        $this->dom_array['ap_char_val'][$i]['char_uuid']            = $value->characteristic_uuid;
                                        $this->dom_array['ap_char_val'][$i]['atomic_products_uuid'] = $atomic_value['uuid'];
                                        $this->dom_array['ap_char_val'][$i]['pms_uuid']             = $this->formatPMSID($value->name, 1);
                                        $charcode                                                   = $product_offering_detail['respose_array']['specification_details'][$atomic_value['pms_id']]['characteristics'][$value->name]['charcode'];
                                        $this->dom_array['ap_char_val'][$i]['tag_name']             = $speccode . ':' . $charcode;
                                        $this->dom_array['ap_char_val'][$i]['tag_value']            = $value->characteristic_value;
                                        $this->dom_array['ap_char_val'][$i]['version']              = '1';
                                        $this->dom_array['ap_char_val'][$i]                         = $this->sanitize($this->dom_array['ap_char_val'][$i]);
                                    }
                                    $i++;
                                }
                            }
                            if (isset($product_rule_list_cus[$line_item_id])) {
                                $speccode = $product_offering_detail['respose_array']['specification_details'][$atomic_value['pms_id']]['speccode'];
                                foreach ($product_rule_list_cus[$line_item_id] as $key => $value) {
                                    if (trim($value->description) != '') {
                                        $this->dom_array['ap_char_val'][$i]['char_uuid']            = $value->rule_uuid;
                                        $this->dom_array['ap_char_val'][$i]['atomic_products_uuid'] = $atomic_value['uuid'];
                                        $this->dom_array['ap_char_val'][$i]['pms_uuid']             = $this->formatPMSID($value->name, 2);
                                        $charcode                                                   = $product_offering_detail['respose_array']['specification_details'][$atomic_value['pms_id']]['rules'][$value->name]['rulecode'];
                                        $this->dom_array['ap_char_val'][$i]['tag_name']             = $speccode . ':' . $charcode;
                                        $this->dom_array['ap_char_val'][$i]['tag_value']            = $value->description;
                                        $this->dom_array['ap_char_val'][$i]['version']              = '1';
                                        $this->dom_array['ap_char_val'][$i]                         = $this->sanitize($this->dom_array['ap_char_val'][$i]);
                                    }
                                    $i++;
                                }
                            }
                        }
                    }

                    if (isset($this->dom_array['ap_char_val'])) {
                        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['ap_char_val']: ".print_r($this->dom_array['ap_char_val'],true), 'Product Configuration and Rule Details');
                    }
                    // All characteristics value block
                    $this->xml .= xmlCharacteristicValue($this->dom_array);
                    unset($this->dom_array);
                    $this->xml .= "\n</contract-info:contract-information>";
                    $arr_respone_data['respone_code']     = '1';
                    $arr_respone_data['respone_xml_data'] = $this->xml;
                } else {
                    $arr_respone_data['respone_error_code'][] = '8004';
                }
            } else {
                $arr_respone_data['respone_error_code'][] = ($contract->name == '') ? '6001' : '3008';
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "arr_respone_data: ".print_r($arr_respone_data,true), 'Final XML');
        return $arr_respone_data;
    }

    /**
     * Method to generate the Contract, Billing, Technical customer details
     *
     * @param string $customer_id String value contains customer id
     * @param string $customer_type String value contains the type of the customer (contract, billing, technical)
     * @param object $customer_obj If the type is contract, this hold the contract customer object otherwise default is null
     * @author Sathish.Kumar.Ganesan
     * @since Jun 04, 2016
     */
    private function getCustomerDetails($customer_id, $customer_type, $customer_obj)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'customer_id: '.$customer_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' customer_type: '.$customer_type.' '.GCM_GL_LOG_VAR_SEPARATOR.' customer_obj: '.print_r($customer_obj,true), 'Method to generate the Contract, Billing, Technical customer details');
        $this->dom_array['cd_' . $customer_type]['customer_details']['customer_type'] = $customer_type; // contract, billing, technical
        if ($customer_obj == null) {
            $account_bean = BeanFactory::getBean('Contacts'); // Account details
            $account_bean->retrieve($customer_id);
        } else {
            $account_bean = $customer_obj;
        }
        $this->dom_array['cd_' . $customer_type]['customer_details']['pi_division_name']         = $account_bean->division_2_c;
        $this->dom_array['cd_' . $customer_type]['customer_details']['pi_division_name_latin']   = $account_bean->division_1_c;
        $this->dom_array['cd_' . $customer_type]['customer_details']['pi_individual_name']       = $account_bean->name_2_c;
        $this->dom_array['cd_' . $customer_type]['customer_details']['pi_individual_name_latin'] = $account_bean->last_name;
        $this->dom_array['cd_' . $customer_type]['customer_details']['pi_individual_title']      = $account_bean->title_2_c;
        $this->dom_array['cd_' . $customer_type]['customer_details']['pi_contact_medium_tel_no'] = $account_bean->phone_work;
        $this->dom_array['cd_' . $customer_type]['customer_details']['pi_contact_medium_ext_no'] = $account_bean->ext_no_c;
        $this->dom_array['cd_' . $customer_type]['customer_details']['pi_contact_medium_fax_no'] = $account_bean->phone_fax;
        $this->dom_array['cd_' . $customer_type]['customer_details']['pi_contact_medium_email']  = $account_bean->email1;

        // Begin : Account Address Details
        $country_bean = BeanFactory::getBean('c_country'); // Country details
        $country_bean->retrieve($account_bean->c_country_id_c);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'country_id: '.$account_bean->c_country_id_c.' '.GCM_GL_LOG_VAR_SEPARATOR.' country_bean: '.$country_bean->id, 'Country Details from Sugar Bean');
        $this->dom_array['cd_' . $customer_type]['customer_details']['address_iso3166_1numeric'] = $country_bean->country_code_numeric;
        $this->dom_array['cd_' . $customer_type]['customer_details']['address_postal_code']      = $account_bean->postal_no_c;
        $this->dom_array['cd_' . $customer_type]['customer_details']['address_address']          = $account_bean->addr_2_c;
        $this->dom_array['cd_' . $customer_type]['customer_details']['attention_line']           = $account_bean->attention_line_c;
        $this->dom_array['cd_' . $customer_type]['customer_details']['address_address_line']     = '';
        $this->dom_array['cd_' . $customer_type]['customer_details']['address_address_city']     = '';
        $this->dom_array['cd_' . $customer_type]['customer_details']['address_address_state']    = '';

        if ($country_bean->country_code_numeric == '392') {
            // Japan country
            $japan_specific_bean = BeanFactory::getBean('js_Accounts_js'); // Japan Spefic char details
            $japan_specific_bean->retrieve_by_string_fields(array(
                'contact_id_c' => $account_bean->id,
            ));
            $this->dom_array['cd_' . $customer_type]['customer_details']['attention_line']       = '';
            $this->dom_array['cd_' . $customer_type]['customer_details']['address_address']      = $japan_specific_bean->addr1;
            $this->dom_array['cd_' . $customer_type]['customer_details']['address_address_line'] = $japan_specific_bean->addr2;

        } else if ($country_bean->country_code_numeric == '124' || $country_bean->country_code_numeric == '840') {
            // Canada or America
            $this->dom_array['cd_' . $customer_type]['customer_details']['address_address']      = '';
            $this->dom_array['cd_' . $customer_type]['customer_details']['address_address_line'] = $account_bean->addr_general_c;
            $this->dom_array['cd_' . $customer_type]['customer_details']['address_address_city'] = $account_bean->city_general_c;

            $state_bean = BeanFactory::getBean('c_State'); // State details
            $state_bean->retrieve($account_bean->c_state_id_c);
            $this->dom_array['cd_' . $customer_type]['customer_details']['address_address_state'] = $state_bean->state_code;
        } else if (trim($account_bean->addr_1_c) != '') {
            $this->dom_array['cd_' . $customer_type]['customer_details']['address_address'] = $account_bean->addr_1_c;
        }

        $corporate_bean = BeanFactory::getBean('Accounts'); // Corporate details
        $corporate_bean->retrieve($account_bean->account_id);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'account_id: '.$account_bean->account_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' account_bean: '.$corporate_bean->id, 'Corporate Details from Sugar Bean');
        $this->dom_array['cd_' . $customer_type]['customer_details']['corporation_name']           = $corporate_bean->name_2_c;
        $this->dom_array['cd_' . $customer_type]['customer_details']['corporation_name_latin']     = $corporate_bean->name;
        $this->dom_array['cd_' . $customer_type]['customer_details']['corporation_name_furigana']  = $corporate_bean->name_3_c;
        $this->dom_array['cd_' . $customer_type]['customer_details']['corporation_corporation_id'] = $corporate_bean->common_customer_id_c;
        $this->dom_array['cd_' . $customer_type]['customer_details']['corporation_vat_no']         = '';
        $this->dom_array['cd_' . $customer_type]['customer_details']                               = $this->sanitize($this->dom_array['cd_' . $customer_type]['customer_details']);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['cd_".$customer_type."']: ".print_r($this->dom_array['cd_' . $customer_type],true), '');
    }
    /**
     * Method to generate the Sales Representative customer details
     *
     * @param string $contract_id String value contains id of the contract
     * @author Sathish.Kumar.Ganesan
     * @since Jun 04, 2016
     */
    private function getSalesRepresentativeDetails($contract_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'contract_id: '.$contract_id, 'Method to generate the Sales Representative customer details');
        $this->dom_array['cd_sales'] = array();
        $sales_representative_bean = BeanFactory::getBean('gc_SalesRepresentative');
        $sales_rep_details         = $sales_representative_bean->retrieve_by_string_fields(array(
            'contracts_id' => $contract_id,
        ));
        if ($sales_rep_details != null) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'sales_rep_details: '.$sales_representative_bean->id, 'Sales Person Details from Sugar Bean');
            $this->dom_array['cd_sales']['customer_details']['customer_type']                  = 'sales';
            $this->dom_array['cd_sales']['customer_details']['pi_division_name']               = $sales_representative_bean->division_2;
            $this->dom_array['cd_sales']['customer_details']['pi_division_name_latin']         = $sales_representative_bean->division_1;
            $this->dom_array['cd_sales']['customer_details']['pi_individual_name']             = $sales_representative_bean->sales_rep_name_1;
            $this->dom_array['cd_sales']['customer_details']['pi_individual_name_latin']       = $sales_representative_bean->name;
            $this->dom_array['cd_sales']['customer_details']['pi_individual_title']            = $sales_representative_bean->title_2;
            $this->dom_array['cd_sales']['customer_details']['pi_contact_medium_tel_no']       = $sales_representative_bean->tel_no;
            $this->dom_array['cd_sales']['customer_details']['pi_contact_medium_ext_no']       = $sales_representative_bean->ext_no;
            $this->dom_array['cd_sales']['customer_details']['pi_contact_medium_fax_no']       = $sales_representative_bean->fax_no;
            $this->dom_array['cd_sales']['customer_details']['pi_contact_medium_email']        = $sales_representative_bean->email1;
            $this->dom_array['cd_sales']['customer_details']['corporation_sales_channel_code'] = $sales_representative_bean->sales_channel_code;
            $this->dom_array['cd_sales']['customer_details']['address_iso3166_1numeric']       = '';
            $this->dom_array['cd_sales']['customer_details']['address_postal_code']            = '';
            $this->dom_array['cd_sales']['customer_details']['address_address']                = '';
            $this->dom_array['cd_sales']['customer_details']['address_address_line']           = '';
            $this->dom_array['cd_sales']['customer_details']['address_address_city']           = '';
            $this->dom_array['cd_sales']['customer_details']['address_address_state']          = '';

            $this->dom_array['cd_sales']['customer_details']['corporation_name']           = '';
            $this->dom_array['cd_sales']['customer_details']['corporation_name_latin']     = '';
            $this->dom_array['cd_sales']['customer_details']['corporation_name_furigana']  = '';
            $this->dom_array['cd_sales']['customer_details']['corporation_corporation_id'] = '';
            $this->dom_array['cd_sales']['customer_details']['corporation_vat_no']         = '';

            if ($sales_representative_bean->gc_nttcomgroupcompany_id_c != '') {
                $groupcompany_bean = BeanFactory::getBean('gc_NTTComGroupCompany'); // Corporate details
                $groupcompany_bean->retrieve($sales_representative_bean->gc_nttcomgroupcompany_id_c);
                $this->dom_array['cd_sales']['customer_details']['corporation_name'] = $groupcompany_bean->name;
            }
            $this->dom_array['cd_sales']['customer_details'] = $this->sanitize($this->dom_array['cd_sales']['customer_details']);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['cd_sales']: ".print_r($this->dom_array['cd_sales'],true), '');
    }

    /**
     * Method to generate the Billing Affiliate details
     *
     * @param string $organization_id String value contains id of the organization
     * @return no return value
     * @author Sudharsan.Ganesan
     * @since Nov 11, 2016
     */
    private function getBillingAffiliateDetails($organization_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'organization_id: '.$organization_id, 'Method to generate the Billing Affiliate details');
        $this->dom_array['cd_billing_affiliate'] = array();
        if (!empty($organization_id)) {
            $organization_bean = BeanFactory::getBean('gc_organization');
            $organization_bean->retrieve($organization_id);

            if (trim($organization_bean->organization_code_1) != '') {
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'organization_bean: '.$organization_bean->id, 'Organization Details from Sugar Bean');
                $this->dom_array['cd_billing_affiliate']['org_code1']                = trim($organization_bean->organization_code_1);
                $this->dom_array['cd_billing_affiliate']['org_code2']                = trim($organization_bean->organization_code_2);
                $this->dom_array['cd_billing_affiliate']['org_level1']['name']       = $organization_bean->name;
                $this->dom_array['cd_billing_affiliate']['org_level1']['name_latin'] = $organization_bean->organization_name_1_latin;
                $this->dom_array['cd_billing_affiliate']['org_level2']['name']       = $organization_bean->organization_name_2_local;
                $this->dom_array['cd_billing_affiliate']['org_level2']['name_latin'] = $organization_bean->organization_name_2_latin;
                $this->dom_array['cd_billing_affiliate']['org_level3']['name']       = $organization_bean->organization_name_3_local;
                $this->dom_array['cd_billing_affiliate']['org_level3']['name_latin'] = $organization_bean->organization_name_3_latin;
                $this->dom_array['cd_billing_affiliate']['org_level4']['name']       = $organization_bean->organization_name_4_local;
                $this->dom_array['cd_billing_affiliate']['org_level4']['name_latin'] = $organization_bean->organization_name_4_latin;
                $this->dom_array['cd_billing_affiliate']                             = $this->sanitize($this->dom_array['cd_billing_affiliate']);
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, "dom_array['cd_billing_affiliate']: ".print_r($this->dom_array['cd_billing_affiliate'],true), '');
    }

    /**
     * Method to build xml paths and respective field name for value replace
     *
     * @author Sathish.Kumar.Ganesan
     * @since Jun 04, 2016
     */
    private function setRootNodes()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Method to build xml paths and respective field name for value replac');
        global $sugar_config;
        $local_schema_path = $sugar_config['site_url'] . 'api/xml-schema/v2/';
        $this->root        = array(
            'xsi:schemaLocation'            => $this->schema_path . 'sidem ' . $local_schema_path . 'sidem.xsd',
            'xmlns:contract-info'           => $this->schema_path . 'contract-information',
            'xmlns:gcm'                     => $this->schema_path . 'expansion-resources/gcm',
            'xmlns:datatypes'               => $this->schema_path . 'cbe-datatypes',
            'xmlns:cbe'                     => $this->schema_path . 'common-business-entity',
            'xmlns:party'                   => $this->schema_path . 'party',
            'xmlns:location'                => $this->schema_path . 'location',
            'xmlns:product'                 => $this->schema_path . 'product',
            'xmlns:bi'                      => $this->schema_path . 'business-interaction',
            'xmlns:customer'                => $this->schema_path . 'customer',
            'xmlns:doc'                     => $this->schema_path . 'document',
            'xmlns:gmone-pcs-001'           => $this->schema_path . 'business-interaction/gmone-pcs-001',
            'xmlns:prod-cntrct-item-000002' => $this->schema_path . 'business-interaction/product-contract-item-000002',
            'xmlns:gmone-prod-price-001'    => $this->schema_path . 'product/gmone-prod-price-001',
            'xmlns:xsi'                     => 'http://www.w3.org/2001/XMLSchema-instance',
            'xmlns:xsd'                     => 'http://www.w3.org/2001/XMLSchema',
        );
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'root: '.print_r($this->root,true), '');
    }
}
