<?php
$validation_config['NullFields'] =  array
(
    'ContractDetails' => array
        (
            'ContractsHeader' => array
                (
                    'description',
                    'ext_sys_modified_by',
                    'vat_no'
                ),

            'ContractSalesRepresentative' => array
                (
                    'sales_channel_code',
                    'name',
                    'sales_rep_name_1',
                    'division_1',
                    'division_2',
                    'title_2',
                    'tel_no',
                    'ext_no',
                    'fax_no',
                    'email1',
                    'sales_company'
                ),

            'Accounts' => array
                (
                    'Contract' => array
                        (
                            'division_1_c',
                            'division_2_c',
                            'title_2_c',
                            'last_name',
                            'phone_work',
                            'ext_no_c',
                            'phone_fax',
                            'email1',
                            'attention_line_c',
                            'CorporateDetails' => array
                                (
									'name',
                                    'name_2_c',
                                    'name_3_c'
                                )

                        ),

                    'Billing' => array
                        (
                            'division_1_c',
                            'division_2_c',
                            'title_2_c',
                            'last_name',
                            'phone_work',
                            'ext_no_c',
                            'phone_fax',
                            'email1',
                            'attention_line_c',
                            'CorporateDetails' => array
                                (
                                    'common_customer_id_c',
									'name',
                                    'name_2_c',
                                    'name_3_c'
                                )
                        ),

                    'Technology' => array
                        (
                            'division_1_c',
                            'division_2_c',
                            'title_2_c',
                            'last_name',
                            'phone_work',
                            'ext_no_c',
                            'phone_fax',
                            'email1',
                            'attention_line_c',
                            'CorporateDetails' => array
                                (
                                    'common_customer_id_c',
									'name',
                                    'name_2_c',
                                    'name_3_c'
                                )
                        )
                ),

            'BillingAffiliate' => array
                (
                    'organization_code_2',
                ),

            'ContractDirectDeposit' => array
                (
                    'payee_contact_person_name_2',
                    'payee_tel_no',
                    'payee_email_address',
                    'remarks'
                )
        )
);
