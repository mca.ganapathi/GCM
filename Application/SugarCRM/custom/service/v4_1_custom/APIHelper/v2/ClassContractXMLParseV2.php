<?php
require_once ('custom/include/XMLUtils.php');
/**
 * @type unix-file system UTF-8
 */
/**
 * Class to parse request xml data into array for API 2.0
 *
 * @author Dinesh.Itkar
 * @since 18 June 2016
 */
class ClassContractXMLParseV2 extends XMLUtils
{

    /**
     * Method to parse xml data
     *
     * @param xml $contract_xml_data xml data
     * @return array Convert xml data into array format
     */
    public function getContractParseArrayV2($contract_xml_data)
    {
        // Initialize array to store parse xml data
        $post_xml_parse_arr = array();

        // Populate Contract Tag Details
        $post_xml_parse_arr = $this->getContractTagDetailsFromXML($contract_xml_data);

        // Populate Product Specification Tag Details
        $this->getProductSpecTagDetailsFromXML($contract_xml_data, $post_xml_parse_arr);

        // Populate Product Price Tag Details
        $this->getProductPriceTagDetailsFromXML($contract_xml_data, $post_xml_parse_arr);

        // Populate Characteristic Value Tag Details
        $this->getCharValDetailsFromXML($contract_xml_data, $post_xml_parse_arr);

        // Populate Line Item Tag Details
        $this->getLineItemTagDetailsFromXML($contract_xml_data, $post_xml_parse_arr);

        // Unset unnecessary keys from array
        $post_xml_parse_arr_result = $this->filterFinalArray($post_xml_parse_arr);

        $acc_cid_lists = array(
                            'Contract' => isset($post_xml_parse_arr_result['Accounts']['Contract']['CorporateDetails']['common_customer_id_c']) ? $post_xml_parse_arr_result['Accounts']['Contract']['CorporateDetails']['common_customer_id_c'] : '',
                            'Billing' => isset($post_xml_parse_arr_result['Accounts']['Billing']['CorporateDetails']['common_customer_id_c']) ? $post_xml_parse_arr_result['Accounts']['Billing']['CorporateDetails']['common_customer_id_c'] : '',
                            'Technology' => isset($post_xml_parse_arr_result['Accounts']['Technology']['CorporateDetails']['common_customer_id_c']) ? $post_xml_parse_arr_result['Accounts']['Technology']['CorporateDetails']['common_customer_id_c'] : ''
        );
        foreach ($acc_cid_lists as $acc_key => $cid_val) {
            if (!empty($cid_val)) {
                $obj_check_cid = BeanFactory::getBean('Accounts');
                $obj_check_cid->retrieve_by_string_fields(array(
                                                                'common_customer_id_c' => $cid_val
                ));
                if (empty($obj_check_cid->id)) {
                    // Get Corporate details from CIDAS API for contracting account
                    $cidas_record_by_id = $this->getCorporateDetails($cid_val);
                    if (isset($cidas_record_by_id['cidas_failed']) && $cidas_record_by_id['cidas_failed']) {
                        $account_type = strtolower($acc_key); // contract | billing | technology
                        $cidas_record_by_id['cidas_id'] = $cid_val;
                        $cidas_record_by_id['account_type'] = $account_type;
                        $cidas_record_by_id['global_contract_xml_id'] = $post_xml_parse_arr_result['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'];
                        return $cidas_record_by_id;
                    }
                    $post_xml_parse_arr_result['Accounts'][$acc_key]['CorporateDetails'] = $cidas_record_by_id;
                }
            }
        }
        return $post_xml_parse_arr_result;
    }

    /**
     * Method to get corporate details from CIDAS API
     *
     * @param string $cid common customer id
     * @return array corporate details from CIDAS
     */
    private function getCorporateDetails($cid)
    {
        require_once ('custom/service/v4_1_custom/APIHelper/ClassCIDASConnection.php');
        $cidas_connection_obj = new ClassCIDASConnection();
        $cidas_record_by_id = $cidas_connection_obj->getCorporateDetailsFromCIDASByCID($cid, false, '2');
        return $cidas_record_by_id;
    }

    /**
     * Method to unset unnecessary keys from array
     *
     * @param array $post_xml_parse_arr xml data in array format
     * @return array
     */
    private function filterFinalArray($post_xml_parse_arr)
    {
        $post_xml_parse_arr_result = array();
        if (!empty($post_xml_parse_arr)) {
            $post_xml_parse_arr_result['ContractDetails'] = reset($post_xml_parse_arr);
            if (isset($post_xml_parse_arr_result['ContractDetails']['ContractLineItemDetails'])) {
                foreach ($post_xml_parse_arr_result['ContractDetails']['ContractLineItemDetails'] as $key => $val) {
                    if (!is_numeric($key)) {
                        unset($post_xml_parse_arr_result['ContractDetails']['ContractLineItemDetails'][$key]);
                    }
                }
                foreach ($post_xml_parse_arr_result['ContractDetails']['ContractLineItemDetails'] as $key => $val) {
                    unset($post_xml_parse_arr_result['ContractDetails']['ContractLineItemDetails'][$key]['spec_details']);
                }
            }

            if (isset($post_xml_parse_arr_result['ContractDetails']['Accounts'])) {
                $post_xml_parse_arr_result['Accounts'] = $post_xml_parse_arr_result['ContractDetails']['Accounts'];
                unset($post_xml_parse_arr_result['ContractDetails']['Accounts']);
            }
        }
        return $post_xml_parse_arr_result;
    }

    /**
     * Method to parse product-biitem tag from xml
     *
     * @param xml $post_request xml data
     * @param array &$post_xml_parse_arr xml data in array format
     */
    private function getLineItemTagDetailsFromXML($post_request, &$post_xml_parse_arr)
    {
        $tag_details = $this->getTagElements($post_request, '//bi:product-biitem');

        if (!empty($tag_details)) {

            $ARR_GUUID = self::getGlobalUUID();
            $sequence_no = 0;

            foreach ($tag_details as $tag_detail) {

                $contract_tag_id = '';
                if (isset($tag_detail['/product-biitem/association-ends[1]/association-end[1]/target[1]/reference-to-product-contract[1]/product-contract-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                    $contract_tag_id = $tag_detail['/product-biitem/association-ends[1]/association-end[1]/target[1]/reference-to-product-contract[1]/product-contract-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
                } elseif (isset($tag_detail['/product-biitem/association-ends[1]/association-end[1]/target[1]/reference-to-product-contract[1]/product-contract-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                    $contract_tag_id = $tag_detail['/product-biitem/association-ends[1]/association-end[1]/target[1]/reference-to-product-contract[1]/product-contract-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
                }

                if (!empty($contract_tag_id)) {

                    $is_new_line_item = false;

                    $line_item_tag_id = '';
                    if (isset($tag_detail['/product-biitem/identified-by[1]/product-biitem-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                        $is_new_line_item = true;
                        $line_item_tag_id = $tag_detail['/product-biitem/identified-by[1]/product-biitem-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
                    } elseif (isset($tag_detail['/product-biitem/identified-by[1]/product-biitem-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                        $line_item_tag_id = $tag_detail['/product-biitem/identified-by[1]/product-biitem-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
                    }

                    $product_spec_tag_id = '';
                    if (isset($tag_detail['/product-biitem/association-ends[1]/association-end[2]/target[1]/reference-to-atomic-product[1]/atomic-product-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                        $product_spec_tag_id = $tag_detail['/product-biitem/association-ends[1]/association-end[2]/target[1]/reference-to-atomic-product[1]/atomic-product-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
                    } elseif (isset($tag_detail['/product-biitem/association-ends[1]/association-end[2]/target[1]/reference-to-atomic-product[1]/atomic-product-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                        $product_spec_tag_id = $tag_detail['/product-biitem/association-ends[1]/association-end[2]/target[1]/reference-to-atomic-product[1]/atomic-product-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
                    }

                    $li_standard_state = isset($tag_detail['/product-biitem/standard-state[1]']) ? $tag_detail['/product-biitem/standard-state[1]'] : '';
                    $spec_ref_id = isset($tag_detail['/product-biitem/described-by[1]/reference-to-product-biitem-spec[1]/product-biitem-spec-key[1]/key-type-for-cbe-standard[1]/object-id[1]']) ? $tag_detail['/product-biitem/described-by[1]/reference-to-product-biitem-spec[1]/product-biitem-spec-key[1]/key-type-for-cbe-standard[1]/object-id[1]'] : '';

                    $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['is_new_record'] = $is_new_line_item;
                    $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['spec_details'] = isset($post_xml_parse_arr[$product_spec_tag_id]) ? $post_xml_parse_arr[$product_spec_tag_id] : '';
                    $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['standard_state'] = $li_standard_state;
                    $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['object_id'] = $spec_ref_id;

                    $li_service_start_date_array = array();
                    if (isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['service_start_date']['uuid']]['tag_value'])) {
                        $li_service_start_date_array = $this->fnGetDateTimeZone($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['service_start_date']['uuid']]['tag_value']);
                    }

                    $li_billing_start_date_array = array();
                    if (isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['billing_start_date']['uuid']]['tag_value'])) {
                        $li_billing_start_date_array = $this->fnGetDateTimeZone($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['billing_start_date']['uuid']]['tag_value']);
                    }

                    $li_service_end_date_array = array();
                    if (isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['service_end_date']['uuid']]['tag_value'])) {
                        $li_service_end_date_array = $this->fnGetDateTimeZone($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['service_end_date']['uuid']]['tag_value']);
                    }

                    $li_billing_end_date_array = array();
                    if (isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['billing_end_date']['uuid']]['tag_value'])) {
                        $li_billing_end_date_array = $this->fnGetDateTimeZone($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['billing_end_date']['uuid']]['tag_value']);
                    }

                    $li_product_specification = '';
                    if (!empty($product_spec_tag_id)) {
                        $li_product_specification = $this->uuidRemoveFormat($post_xml_parse_arr[$product_spec_tag_id]['specid']);
                    }

                    $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['ContractLineItemHeader'] = array(
                                                                                                                                    'service_start_date' => isset($li_service_start_date_array['date_time']) ? $li_service_start_date_array['date_time'] : '',
                                                                                                                                    'service_start_date_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['service_start_date']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['service_start_date']['uuid']]['xml_id'] : '',
                                                                                                                                    'billing_start_date' => isset($li_billing_start_date_array['date_time']) ? $li_billing_start_date_array['date_time'] : '',
                                                                                                                                    'billing_start_date_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['billing_start_date']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['billing_start_date']['uuid']]['xml_id'] : '',
                                                                                                                                    'service_end_date' => isset($li_service_end_date_array['date_time']) ? $li_service_end_date_array['date_time'] : '',
                                                                                                                                    'service_end_date_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['service_end_date']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['service_end_date']['uuid']]['xml_id'] : '',
                                                                                                                                    'billing_end_date' => isset($li_billing_end_date_array['date_time']) ? $li_billing_end_date_array['date_time'] : '',
                                                                                                                                    'billing_end_date_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['billing_end_date']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['billing_end_date']['uuid']]['xml_id'] : '',
                                                                                                                                    'service_start_date_timezone' => isset($li_service_start_date_array['time_zone']) ? $li_service_start_date_array['time_zone'] : '',
                                                                                                                                    'billing_start_date_timezone' => isset($li_billing_start_date_array['time_zone']) ? $li_billing_start_date_array['time_zone'] : '',
                                                                                                                                    'service_end_date_timezone' => isset($li_service_end_date_array['time_zone']) ? $li_service_end_date_array['time_zone'] : '',
                                                                                                                                    'billing_end_date_timezone' => isset($li_billing_end_date_array['time_zone']) ? $li_billing_end_date_array['time_zone'] : '',
                                                                                                                                    'factory_reference_no' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['factory_reference_no']['uuid']]['tag_value']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['factory_reference_no']['uuid']]['tag_value'] : '',
                                                                                                                                    'factory_reference_no_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['factory_reference_no']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['factory_reference_no']['uuid']]['xml_id'] : '',
                                                                                                                                    'product_specification' => $li_product_specification,
                                                                                                                                    'product_specification_xml_id' => $product_spec_tag_id,
                                                                                                                                    'cust_contract_currency' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['cust_contract_currency']['uuid']]['tag_value']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['cust_contract_currency']['uuid']]['tag_value'] : '',
                                                                                                                                    'cust_contract_currency_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['cust_contract_currency']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['cust_contract_currency']['uuid']]['xml_id'] : '',
                                                                                                                                    'cust_billing_currency' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['cust_billing_currency']['uuid']]['tag_value']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['cust_billing_currency']['uuid']]['tag_value'] : '',
                                                                                                                                    'cust_billing_currency_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['cust_billing_currency']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['cust_billing_currency']['uuid']]['xml_id'] : '',
                                                                                                                                    'igc_contract_currency' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['igc_contract_currency']['uuid']]['tag_value']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['igc_contract_currency']['uuid']]['tag_value'] : '',
                                                                                                                                    'igc_contract_currency_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['igc_contract_currency']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['igc_contract_currency']['uuid']]['xml_id'] : '',
                                                                                                                                    'igc_billing_currency' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['igc_billing_currency']['uuid']]['tag_value']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['igc_billing_currency']['uuid']]['tag_value'] : '',
                                                                                                                                    'igc_billing_currency_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['igc_billing_currency']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['igc_billing_currency']['uuid']]['xml_id'] : '',
                                                                                                                                    'customer_billing_method' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['customer_billing_method']['uuid']]['tag_value']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['customer_billing_method']['uuid']]['tag_value'] : '',
                                                                                                                                    'customer_billing_method_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['customer_billing_method']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['customer_billing_method']['uuid']]['xml_id'] : '',
                                                                                                                                    'igc_settlement_method' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['igc_settlement_method']['uuid']]['tag_value']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['igc_settlement_method']['uuid']]['tag_value'] : '',
                                                                                                                                    'igc_settlement_method_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['igc_settlement_method']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['igc_settlement_method']['uuid']]['xml_id'] : '',
                                                                                                                                    'description' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['description']['uuid']]['tag_value']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['description']['uuid']]['tag_value'] : '',
                                                                                                                                    'description_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['description']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['gc_line_item_contract_history']['description']['uuid']]['xml_id'] : '',
                                                                                                                                    'contract_line_item_type' => isset($tag_detail['/product-biitem/standard-business-state[1]']) ? $tag_detail['/product-biitem/standard-business-state[1]'] : '',
                                                                                                                                    'bi_action' => isset($tag_detail['/product-biitem/action[1]']) ? $tag_detail['/product-biitem/action[1]'] : '',
                                                                                                                                    'line_item_id_xml_id' => $line_item_tag_id
                    );

                    $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['InvoiceGroupDtls'] = array(
                                                                                                                            'name' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['invoicesubtotalgroup']['name']['uuid']]['tag_value']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['invoicesubtotalgroup']['name']['uuid']]['tag_value'] : '',
                                                                                                                            'name_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['invoicesubtotalgroup']['name']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['invoicesubtotalgroup']['name']['uuid']]['xml_id'] : '',
                                                                                                                            'description' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['invoicesubtotalgroup']['description']['uuid']]['tag_value']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['invoicesubtotalgroup']['description']['uuid']]['tag_value'] : '',
                                                                                                                            'description_xml_id' => isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['invoicesubtotalgroup']['description']['uuid']]['xml_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$ARR_GUUID['invoicesubtotalgroup']['description']['uuid']]['xml_id'] : ''
                    );

                    $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['ContractLineProductConfiguration'] = isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['spec_details']['config_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['spec_details']['config_id'] : '';
                    $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['ContractLineProductRule'] = isset($post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['spec_details']['rule_id']) ? $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['spec_details']['rule_id'] : '';

                    if (!empty($product_spec_tag_id) && isset($post_xml_parse_arr[$product_spec_tag_id]['product_price']) && !empty(isset($post_xml_parse_arr[$product_spec_tag_id]['product_price']))) {
                        foreach ($post_xml_parse_arr[$product_spec_tag_id]['product_price'] as $product_price_array) {
                            $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['ContractLineProductPrice'][] = array(
                                                                                                                                                'name' => isset($product_price_array['pms_price_id']) ? $product_price_array['pms_price_id'] : '',
                                                                                                                                                'charge_xml_id' => isset($product_price_array['product_price_tag_id']) ? $product_price_array['product_price_tag_id'] : '',
                                                                                                                                                'is_new_record' => isset($product_price_array['is_new_product_price']) ? $product_price_array['is_new_product_price'] : '',
                                                                                                                                                'xml_data' => array(
                                                                                                                                                                    'currencyid' => array(
                                                                                                                                                                                        'xml_id' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['currencyid']['xml_id']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['currencyid']['xml_id'] : '',
                                                                                                                                                                                        'xml_value' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['currencyid']['xml_value']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['currencyid']['xml_value'] : ''
                                                                                                                                                                    ),
                                                                                                                                                                    'icb_flag' => array(
                                                                                                                                                                                        'xml_id' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['icb_flag']['xml_id']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['icb_flag']['xml_id'] : '',
                                                                                                                                                                                        'xml_value' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['icb_flag']['xml_value']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['icb_flag']['xml_value'] : ''
                                                                                                                                                                    ),
                                                                                                                                                                    'charge_type' => array(
                                                                                                                                                                                        'xml_id' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['charge_type']['xml_id']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['charge_type']['xml_id'] : '',
                                                                                                                                                                                        'xml_value' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['charge_type']['xml_value']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['charge_type']['xml_value'] : '',
                                                                                                                                                                                        'tag_value' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['charge_type']['tag_value']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['charge_type']['tag_value'] : ''
                                                                                                                                                                    ),
                                                                                                                                                                    'charge_period' => array(
                                                                                                                                                                                            'xml_id' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['charge_period']['xml_id']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['charge_period']['xml_id'] : '',
                                                                                                                                                                                            'xml_value' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['charge_period']['xml_value']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['charge_period']['xml_value'] : '',
                                                                                                                                                                                            'tag_value' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['charge_period']['tag_value']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['charge_period']['tag_value'] : ''
                                                                                                                                                                    ),
                                                                                                                                                                    'list_price' => array(
                                                                                                                                                                                        'xml_id' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['list_price']['xml_id']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['list_price']['xml_id'] : '',
                                                                                                                                                                                        'xml_value' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['list_price']['xml_value']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['list_price']['xml_value'] : ''
                                                                                                                                                                    ),
                                                                                                                                                                    'discount_rate' => array(
                                                                                                                                                                                            'xml_id' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['discount_rate']['xml_id']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['discount_rate']['xml_id'] : '',
                                                                                                                                                                                            'xml_value' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['discount_rate']['xml_value']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['discount_rate']['xml_value'] : ''
                                                                                                                                                                    ),
                                                                                                                                                                    'discount_amount' => array(
                                                                                                                                                                                            'xml_id' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['discount_amount']['xml_id']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['discount_amount']['xml_id'] : '',
                                                                                                                                                                                            'xml_value' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['discount_amount']['xml_value']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['discount_amount']['xml_value'] : ''
                                                                                                                                                                    ),
                                                                                                                                                                    'customer_contract_price' => array(
                                                                                                                                                                                                    'xml_id' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['customer_contract_price']['xml_id']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['customer_contract_price']['xml_id'] : '',
                                                                                                                                                                                                    'xml_value' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['customer_contract_price']['xml_value']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['customer_contract_price']['xml_value'] : ''
                                                                                                                                                                    ),
                                                                                                                                                                    'igc_settlement_price' => array(
                                                                                                                                                                                                    'xml_id' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['igc_settlement_price']['xml_id']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['igc_settlement_price']['xml_id'] : '',
                                                                                                                                                                                                    'xml_value' => isset($post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['igc_settlement_price']['xml_value']) ? $post_xml_parse_arr['product_price_details'][$product_price_array['product_price_tag_id']]['igc_settlement_price']['xml_value'] : ''
                                                                                                                                                                    )
                                                                                                                                                )
                            );
                        }
                    }

                    $credit_card_details = $this->getCreditCardTagDetailsFromXML($post_request);
                    $direct_deposit_details = $this->getDirectDepositTagDetailsFromXML($post_request);
                    $acc_transfer_details = $this->getAccountTransferTagDetailsFromXML($post_request);
                    $postal_transfer_details = $this->getPostalTransferTagDetailsFromXML($post_request);
                    if (!empty($credit_card_details)) {
                        $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['ContractCreditCard'] = $credit_card_details;
                        $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['ContractLineItemHeader']['payment_type'] = isset($credit_card_details['payment_type']) ? $credit_card_details['payment_type'] : '';
                    } elseif (!empty($direct_deposit_details)) {
                        $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['ContractDirectDeposit'] = $direct_deposit_details;
                        $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['ContractLineItemHeader']['payment_type'] = isset($direct_deposit_details['payment_type']) ? $direct_deposit_details['payment_type'] : '';
                    } elseif (!empty($acc_transfer_details)) {
                        $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['ContractAccountTransfer'] = $acc_transfer_details;
                        $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['ContractLineItemHeader']['payment_type'] = isset($acc_transfer_details['payment_type']) ? $acc_transfer_details['payment_type'] : '';
                    } elseif (!empty($postal_transfer_details)) {
                        $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['ContractPostalTransfer'] = $postal_transfer_details;
                        $post_xml_parse_arr[$contract_tag_id]['ContractLineItemDetails'][$sequence_no]['ContractLineItemHeader']['payment_type'] = isset($postal_transfer_details['payment_type']) ? $postal_transfer_details['payment_type'] : '';
                    }
                    $sequence_no++;
                }
            }
        }

        return;
    }

    /**
     * Method to get global UUID
     *
     * @return array
     */
    private function getGlobalUUID()
    {
        include 'custom/include/globalUUIDConfig.php';
        return $ARR_GUUID;
    }

    /**
     * Method to get loi contract value
     *
     * @return array
     */
    private function getLOIContractValue()
    {
        include 'custom/include/globalUUIDConfig.php';
        return $loi_contract_array;
    }

    /**
     * Method to parse jpn-postal-transfer-pm tag from xml
     *
     * @param xml $post_request xml data
     * @return array $pt_details_array jpn-postaltransfer-pm tag details
     */
    private function getPostalTransferTagDetailsFromXML($post_request)
    {
        $pt_details_array = array();
        $tag_details = $this->getTagElements($post_request, '//gcm:jpn-postal-transfer-pm');
        if (!empty($tag_details)) {
            $payment_type_pt = '30';
            $pt_details_array['name'] = isset($tag_details[0]['/jpn-postal-transfer-pm/postal-account-holder-name[1]']) ? $tag_details[0]['/jpn-postal-transfer-pm/postal-account-holder-name[1]'] : '';
            $pt_details_array['postal_passbook_mark'] = isset($tag_details[0]['/jpn-postal-transfer-pm/postal-passbook-mark[1]']) ? $tag_details[0]['/jpn-postal-transfer-pm/postal-passbook-mark[1]'] : '';
            $pt_details_array['postal_passbook'] = isset($tag_details[0]['/jpn-postal-transfer-pm/postal-passbook-number[1]']) ? $tag_details[0]['/jpn-postal-transfer-pm/postal-passbook-number[1]'] : '';
            $pt_details_array['postal_passbook_no_display'] = isset($tag_details[0]['/jpn-postal-transfer-pm/is-postal-passbook-number-display[1]']) ? $tag_details[0]['/jpn-postal-transfer-pm/is-postal-passbook-number-display[1]'] : '';
            $pt_details_array['payment_type'] = $payment_type_pt;
        }
        return $pt_details_array;
    }

    /**
     * Method to parse jpn-account-transfer-pm tag from xml
     *
     * @param xml $post_request xml data
     * @return array jpn-account-transfer-pm tag details
     */
    private function getAccountTransferTagDetailsFromXML($post_request)
    {
        $at_details_array = array();
        $tag_details = $this->getTagElements($post_request, '//gcm:jpn-account-transfer-pm');

        if (!empty($tag_details)) {
            $payment_type_at = '10';
            $at_details_array['name'] = isset($tag_details[0]['/jpn-account-transfer-pm/bank-account-holder-name[1]']) ? $tag_details[0]['/jpn-account-transfer-pm/bank-account-holder-name[1]'] : '';
            $at_details_array['bank_code'] = isset($tag_details[0]['/jpn-account-transfer-pm/bank-code[1]']) ? $tag_details[0]['/jpn-account-transfer-pm/bank-code[1]'] : '';
            $at_details_array['branch_code'] = isset($tag_details[0]['/jpn-account-transfer-pm/branch-code[1]']) ? $tag_details[0]['/jpn-account-transfer-pm/branch-code[1]'] : '';
            $at_details_array['deposit_type'] = isset($tag_details[0]['/jpn-account-transfer-pm/deposit-type[1]']) ? $tag_details[0]['/jpn-account-transfer-pm/deposit-type[1]'] : '';
            $at_details_array['account_no'] = isset($tag_details[0]['/jpn-account-transfer-pm/account-number[1]']) ? $tag_details[0]['/jpn-account-transfer-pm/account-number[1]'] : '';
            $at_details_array['account_no_display'] = isset($tag_details[0]['/jpn-account-transfer-pm/is-account-number-display[1]']) ? $tag_details[0]['/jpn-account-transfer-pm/is-account-number-display[1]'] : '';
            $at_details_array['payment_type'] = $payment_type_at;
        }
        return $at_details_array;
    }

    /**
     * Method to parse jpn-direct-deposit-pm tag from xml
     *
     * @param xml $post_request xml data
     * @return array jpn-direct-deposit-pm tag details
     */
    private function getDirectDepositTagDetailsFromXML($post_request)
    {
        $dd_details_array = array();
        $tag_details = $this->getTagElements($post_request, '//gcm:jpn-direct-deposit-pm');
        if (!empty($tag_details)) {
            $payment_type_dd = '00';
            $dd_details_array['bank_code'] = isset($tag_details[0]['/jpn-direct-deposit-pm/bank-code[1]']) ? $tag_details[0]['/jpn-direct-deposit-pm/bank-code[1]'] : '';
            $dd_details_array['branch_code'] = isset($tag_details[0]['/jpn-direct-deposit-pm/branch-code[1]']) ? $tag_details[0]['/jpn-direct-deposit-pm/branch-code[1]'] : '';
            $dd_details_array['deposit_type'] = isset($tag_details[0]['/jpn-direct-deposit-pm/deposit-type[1]']) ? $tag_details[0]['/jpn-direct-deposit-pm/deposit-type[1]'] : '';
            $dd_details_array['account_no'] = isset($tag_details[0]['/jpn-direct-deposit-pm/account-number[1]']) ? $tag_details[0]['/jpn-direct-deposit-pm/account-number[1]'] : '';
            $dd_details_array['payee_contact_person_name_2'] = isset($tag_details[0]['/jpn-direct-deposit-pm/name-of-person-in-charge[1]']) ? $tag_details[0]['/jpn-direct-deposit-pm/name-of-person-in-charge[1]'] : '';
            $dd_details_array['payee_tel_no'] = isset($tag_details[0]['/jpn-direct-deposit-pm/telephone-number[1]']) ? $tag_details[0]['/jpn-direct-deposit-pm/telephone-number[1]'] : '';
            $dd_details_array['payee_email_address'] = isset($tag_details[0]['/jpn-direct-deposit-pm/email-address[1]']) ? $tag_details[0]['/jpn-direct-deposit-pm/email-address[1]'] : '';
            $dd_details_array['remarks'] = isset($tag_details[0]['/jpn-direct-deposit-pm/remarks[1]']) ? $tag_details[0]['/jpn-direct-deposit-pm/remarks[1]'] : '';
            $dd_details_array['payment_type'] = $payment_type_dd;
        }
        return $dd_details_array;
    }

    /**
     * Method to parse credit-card-pm tag from xml
     *
     * @param xml $post_request xml data
     * @return array credit-card-pm tag details
     */
    private function getCreditCardTagDetailsFromXML($post_request)
    {
        $cc_details_array = array();
        $tag_details = $this->getTagElements($post_request, '//gcm:credit-card-pm');
        if (!empty($tag_details)) {
            $payment_type_cc = '60';
            $cc_details_array['credit_card_no'] = isset($tag_details[0]['/credit-card-pm/credit-card-number[1]']) ? $tag_details[0]['/credit-card-pm/credit-card-number[1]'] : '';
            $cc_details_array['expiration_date'] = isset($tag_details[0]['/credit-card-pm/expiration-date[1]']) ? $tag_details[0]['/credit-card-pm/expiration-date[1]'] : '';
            $cc_details_array['payment_type'] = $payment_type_cc;
        }
        return $cc_details_array;
    }

    /**
     * Method to parse atomic-product-price tag from xml
     *
     * @param xml $post_request xml data
     * @param array &$post_xml_parse_arr xml data in array format
     */
    private function getProductPriceTagDetailsFromXML($post_request, &$post_xml_parse_arr)
    {
        $tag_details = $this->getTagElements($post_request, '//product:atomic-product-price');

        if (!empty($tag_details)) {
            foreach ($tag_details as $tag_detail) {

                $product_spec_tag_id = '';
                $pms_price_id = '';
                $product_price_tag_id = '';

                if (isset($tag_detail['/atomic-product-price/association-ends[1]/association-end[2]/target[1]/reference-to-atomic-product[1]/atomic-product-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                    $product_spec_tag_id = $tag_detail['/atomic-product-price/association-ends[1]/association-end[2]/target[1]/reference-to-atomic-product[1]/atomic-product-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
                } elseif (isset($tag_detail['/atomic-product-price/association-ends[1]/association-end[2]/target[1]/reference-to-atomic-product[1]/atomic-product-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                    $product_spec_tag_id = $tag_detail['/atomic-product-price/association-ends[1]/association-end[2]/target[1]/reference-to-atomic-product[1]/atomic-product-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
                }

                $product_price_object_id = isset($tag_detail['/atomic-product-price/association-ends[1]/association-end[1]/target[1]/reference-to-atomic-product-offering-price[1]/atomic-product-offering-price-key[1]/key-type-for-cbe-standard[1]/object-id[1]']) ? $tag_detail['/atomic-product-price/association-ends[1]/association-end[1]/target[1]/reference-to-atomic-product-offering-price[1]/atomic-product-offering-price-key[1]/key-type-for-cbe-standard[1]/object-id[1]'] : '';
                if (!empty($product_price_object_id)) {
                    $pms_price_id = $this->uuidRemoveFormat($product_price_object_id, false);
                }

                $is_new_product_price = false;
                if (isset($tag_detail['/atomic-product-price/identified-by[1]/atomic-product-price-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                    $is_new_product_price = true;
                    $product_price_tag_id = $tag_detail['/atomic-product-price/identified-by[1]/atomic-product-price-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
                } elseif ($tag_detail['/atomic-product-price/identified-by[1]/atomic-product-price-key[1]/key-type-for-cbe-standard[1]/object-id[1]']) {
                    $product_price_tag_id = $tag_detail['/atomic-product-price/identified-by[1]/atomic-product-price-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
                }

                if (!empty($product_spec_tag_id) && !empty($product_price_tag_id) && !empty($pms_price_id)) {
                    $post_xml_parse_arr[$product_spec_tag_id]['product_price'][] = array(
                                                                                        'pms_price_id' => $pms_price_id,
                                                                                        'product_price_tag_id' => $product_price_tag_id,
                                                                                        'is_new_product_price' => $is_new_product_price,
                    );
                }
            }
        }
        return;
    }

    /**
     * Method to parse atomic-product tag from xml
     *
     * @param xml $post_request xml data
     * @param array &$post_xml_parse_arr xml data in array format
     */
    private function getProductSpecTagDetailsFromXML($post_request, &$post_xml_parse_arr)
    {
        $tag_details = $this->getTagElements($post_request, '//product:atomic-product');

        if (!empty($tag_details)) {
            $product_spec_id = '';
            foreach ($tag_details as $tag_detail) {

                if (isset($tag_detail['/atomic-product/identified-by[1]/atomic-product-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                    $product_spec_id = $tag_detail['/atomic-product/identified-by[1]/atomic-product-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
                } elseif (isset($tag_detail['/atomic-product/identified-by[1]/atomic-product-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                    $product_spec_id = $tag_detail['/atomic-product/identified-by[1]/atomic-product-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
                }

                if (!empty($product_spec_id) && isset($tag_detail['/atomic-product/described-by[1]/reference-to-atomic-product-offering[1]/atomic-product-offering-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                    $post_xml_parse_arr[$product_spec_id] = array(
                                                                'specid' => $tag_detail['/atomic-product/described-by[1]/reference-to-atomic-product-offering[1]/atomic-product-offering-key[1]/key-type-for-cbe-standard[1]/object-id[1]']
                    );
                }
            }
        }
        return;
    }

    /**
     * Method to parse characteristic-value tag from xml
     *
     * @param xml $post_request xml data
     * @param array &$post_xml_parse_arr xml data in array format
     */
    private function getCharValDetailsFromXML($post_request, &$post_xml_parse_arr)
    {
        $ARR_GUUID = self::getGlobalUUID();
        $tag_details = $this->getTagElements($post_request, '//cbe:characteristic-value');
        if (!empty($tag_details)) {

            foreach ($tag_details as $tag_detail) {

                $char_val_tag_id = '';
                $is_new_char_tag = false;
                if (isset($tag_detail['/characteristic-value/identified-by[1]/charc-value-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                    $is_new_char_tag = true;
                    $char_val_tag_id = $tag_detail['/characteristic-value/identified-by[1]/charc-value-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
                } elseif (isset($tag_detail['/characteristic-value/identified-by[1]/charc-value-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                    $char_val_tag_id = $tag_detail['/characteristic-value/identified-by[1]/charc-value-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
                }

                $global_uuid = isset($tag_detail['/characteristic-value/association-ends[1]/association-end[2]/target[1]/reference-to-atomic-entity-spec-charc[1]/atomic-entity-spec-charc-key[1]/key-type-for-cbe-standard[1]/object-id[1]']) ? $tag_detail['/characteristic-value/association-ends[1]/association-end[2]/target[1]/reference-to-atomic-entity-spec-charc[1]/atomic-entity-spec-charc-key[1]/key-type-for-cbe-standard[1]/object-id[1]'] : '';

                $contract_tag_id = '';
                if (isset($tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-product-contract[1]/product-contract-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                    $contract_tag_id = $tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-product-contract[1]/product-contract-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
                } elseif (isset($tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-product-contract[1]/product-contract-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                    $contract_tag_id = $tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-product-contract[1]/product-contract-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
                }

                $product_spec_tag_id = '';
                if (isset($tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-atomic-product[1]/atomic-product-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                    $product_spec_tag_id = $tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-atomic-product[1]/atomic-product-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
                } elseif (isset($tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-atomic-product[1]/atomic-product-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                    $product_spec_tag_id = $tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-atomic-product[1]/atomic-product-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
                }

                $line_item_tag_id = '';
                if (isset($tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-product-biitem[1]/product-biitem-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                    $line_item_tag_id = $tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-product-biitem[1]/product-biitem-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
                } elseif (isset($tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-product-biitem[1]/product-biitem-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                    $line_item_tag_id = $tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-product-biitem[1]/product-biitem-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
                }

                $price_tag_id = '';
                if (isset($tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-atomic-product-price[1]/atomic-product-price-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                    $price_tag_id = $tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-atomic-product-price[1]/atomic-product-price-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
                } elseif (isset($tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-atomic-product-price[1]/atomic-product-price-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                    $price_tag_id = $tag_detail['/characteristic-value/association-ends[1]/association-end[1]/target[1]/reference-to-atomic-product-price[1]/atomic-product-price-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
                }

                if (!empty($contract_tag_id) && !empty($char_val_tag_id) && !empty($global_uuid)) {
                    if ($global_uuid == $ARR_GUUID['gc_contracts']['loi_contract']['uuid']) {
                        $global_uuid_value = end($tag_detail);
                        $loi_contract_list = self::getLOIContractValue();
                        $loi_contract_value = isset($loi_contract_list[$global_uuid_value]) ? $loi_contract_list[$global_uuid_value] : '';
                        $post_xml_parse_arr[$contract_tag_id]['ContractsHeader']['loi_contract'] = $loi_contract_value;
                        $post_xml_parse_arr[$contract_tag_id]['ContractsHeader']['loi_contract_xml_id'] = $char_val_tag_id;
                    } elseif ($global_uuid == $ARR_GUUID['gc_contracts']['contract_scheme']['uuid']) {
                        $global_uuid_value = end($tag_detail);
                        $post_xml_parse_arr[$contract_tag_id]['ContractsHeader']['contract_scheme'] = $global_uuid_value;
                        $post_xml_parse_arr[$contract_tag_id]['ContractsHeader']['contract_scheme_xml_id'] = $char_val_tag_id;
                    } elseif ($global_uuid == $ARR_GUUID['gc_contracts']['contract_type']['uuid']) {
                        $global_uuid_value = end($tag_detail);
                        $post_xml_parse_arr[$contract_tag_id]['ContractsHeader']['contract_type'] = $global_uuid_value;
                        $post_xml_parse_arr[$contract_tag_id]['ContractsHeader']['contract_type_xml_id'] = $char_val_tag_id;
                    } elseif ($global_uuid == $ARR_GUUID['gc_contracts']['billing_type']['uuid']) {
                        $global_uuid_value = end($tag_detail);
                        $billing_type_list_key = array_search($global_uuid_value, $GLOBALS['app_list_strings']['billing_type_list']);
                        $post_xml_parse_arr[$contract_tag_id]['ContractsHeader']['billing_type'] = $billing_type_list_key;
                        $post_xml_parse_arr[$contract_tag_id]['ContractsHeader']['billing_type_xml_id'] = $char_val_tag_id;
                    } elseif ($global_uuid == $ARR_GUUID['teams']['uuid']) {
                        $global_uuid_value = end($tag_detail);
                        $post_xml_parse_arr[$contract_tag_id]['ContractsHeader']['surrogate_teams'][] = $global_uuid_value;
                        $post_xml_parse_arr[$contract_tag_id]['ContractsHeader']['surrogate_teams_xml_id'][] = $char_val_tag_id;
                    } elseif ($global_uuid == $ARR_GUUID['gc_contracts']['factory_reference_no']['uuid']) {
                        $global_uuid_value = end($tag_detail);
                        $post_xml_parse_arr[$contract_tag_id]['ContractsHeader']['factory_reference_no'] = $global_uuid_value;
                        $post_xml_parse_arr[$contract_tag_id]['ContractsHeader']['factory_reference_no_xml_id'] = $char_val_tag_id;
                    }
                } elseif (!empty($product_spec_tag_id) && !empty($char_val_tag_id) && !empty($global_uuid)) {
                    $config_id_zero_format = '00000000';
                    $rule_id_zero_format = '00000001';

                    if (substr($global_uuid, 0, 8) == $config_id_zero_format) {
                        $post_xml_parse_arr[$product_spec_tag_id]['config_id'][] = array(
                                                                                        'is_new_record' => $is_new_char_tag,
                                                                                        'name' => $this->uuidRemoveFormat($global_uuid),
                                                                                        'characteristic_value' => end($tag_detail),
                                                                                        'characteristic_xml_id' => $char_val_tag_id
                        );
                    } elseif (substr($global_uuid, 0, 8) == $rule_id_zero_format) {
                        $post_xml_parse_arr[$product_spec_tag_id]['rule_id'][] = array(
                                                                                    'is_new_record' => $is_new_char_tag,
                                                                                    'name' => $this->uuidRemoveFormat($global_uuid),
                                                                                    'description' => end($tag_detail),
                                                                                    'rule_xml_id' => $char_val_tag_id
                        );
                    }
                } elseif (!empty($line_item_tag_id) && !empty($char_val_tag_id) && !empty($global_uuid)) {
                    $get_contract_xml_tag_id = $this->getTagElements($post_request, '//bi:product-contracts');
                    $product_contract_tag_id = '';

                    if (isset($get_contract_xml_tag_id[0]['/product-contracts/product-contract[1]/identified-by[1]/product-contract-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                        $product_contract_tag_id = $get_contract_xml_tag_id[0]['/product-contracts/product-contract[1]/identified-by[1]/product-contract-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
                    } elseif (isset($get_contract_xml_tag_id[0]['/product-contracts/product-contract[1]/identified-by[1]/product-contract-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                        $product_contract_tag_id = $get_contract_xml_tag_id[0]['/product-contracts/product-contract[1]/identified-by[1]/product-contract-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
                    }

                    if (!empty($product_contract_tag_id)) {
                        $post_xml_parse_arr[$product_contract_tag_id]['ContractLineItemDetails'][$line_item_tag_id][$global_uuid] = array(
                                                                                                                                        'tag_value' => end($tag_detail),
                                                                                                                                        'xml_id' => $char_val_tag_id
                        );
                    }
                } elseif (!empty($price_tag_id) && !empty($char_val_tag_id) && !empty($global_uuid)) {

                    if ($global_uuid == $ARR_GUUID['gc_productprice']['currencyid']['uuid']) {
                        $post_xml_parse_arr['product_price_details'][$price_tag_id]['currencyid'] = array(
                                                                                                        'xml_value' => end($tag_detail),
                                                                                                        'xml_id' => $char_val_tag_id
                        );
                    }
                    if ($global_uuid == $ARR_GUUID['gc_productprice']['icb_flag']['uuid']) {
                        $post_xml_parse_arr['product_price_details'][$price_tag_id]['icb_flag'] = array(
                                                                                                        'xml_value' => end($tag_detail),
                                                                                                        'xml_id' => $char_val_tag_id
                        );
                    }
                    if ($global_uuid == $ARR_GUUID['gc_productprice']['charge_type']['uuid']) {
                        $charge_type_id = explode(':', end($tag_detail));
                        $post_xml_parse_arr['product_price_details'][$price_tag_id]['charge_type'] = array(
                                                                                                        'xml_value' => trim($charge_type_id[0]),
                                                                                                        'xml_id' => $char_val_tag_id,
                                                                                                        'tag_value' => end($tag_detail),
                        );
                    }
                    if ($global_uuid == $ARR_GUUID['gc_productprice']['charge_period']['uuid']) {
                        $charge_product_id = explode(':', end($tag_detail));
                        $post_xml_parse_arr['product_price_details'][$price_tag_id]['charge_period'] = array(
                                                                                                            'xml_value' => trim($charge_product_id[0]),
                                                                                                            'xml_id' => $char_val_tag_id,
                                                                                                            'tag_value' => end($tag_detail),
                        );
                    }
                    if ($global_uuid == $ARR_GUUID['gc_productprice']['list_price']['uuid']) {
                        $post_xml_parse_arr['product_price_details'][$price_tag_id]['list_price'] = array(
                                                                                                        'xml_value' => end($tag_detail),
                                                                                                        'xml_id' => $char_val_tag_id
                        );
                    }
                    if ($global_uuid == $ARR_GUUID['gc_productprice']['discount_rate']['uuid']) {
                        $post_xml_parse_arr['product_price_details'][$price_tag_id]['discount_rate'] = array(
                                                                                                            'xml_value' => end($tag_detail),
                                                                                                            'xml_id' => $char_val_tag_id
                        );
                    }
                    if ($global_uuid == $ARR_GUUID['gc_productprice']['discount_amount']['uuid']) {
                        $post_xml_parse_arr['product_price_details'][$price_tag_id]['discount_amount'] = array(
                                                                                                            'xml_value' => end($tag_detail),
                                                                                                            'xml_id' => $char_val_tag_id
                        );
                    }
                    if ($global_uuid == $ARR_GUUID['gc_productprice']['customer_contract_price']['uuid']) {
                        $post_xml_parse_arr['product_price_details'][$price_tag_id]['customer_contract_price'] = array(
                                                                                                                    'xml_value' => end($tag_detail),
                                                                                                                    'xml_id' => $char_val_tag_id
                        );
                    }
                    if ($global_uuid == $ARR_GUUID['gc_productprice']['igc_settlement_price']['uuid']) {
                        $post_xml_parse_arr['product_price_details'][$price_tag_id]['igc_settlement_price'] = array(
                                                                                                                    'xml_value' => end($tag_detail),
                                                                                                                    'xml_id' => $char_val_tag_id
                        );
                    }
                }
            }
        }

        return;
    }

    /**
     * Method to parse product-contracts tag from xml
     *
     * @param xml $post_request xml data
     * @return array Convert contract details tag xml data into array format
     */
    private function getContractTagDetailsFromXML($post_request)
    {
        $contract_tag_details_arr = array();
        // get product-contracts details
        $tag_details = $this->getTagElements($post_request, '//bi:product-contracts');

        // If product-contracts tag not found in XML then return empty array
        if (empty($tag_details)) {
            return $contract_tag_details_arr;
        }

        // get product-contracts tag id
        $contract_tag_id = '';
        $is_new_contract = true;
        if (isset($tag_details[0]['/product-contracts/product-contract[1]/identified-by[1]/product-contract-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
            $contract_tag_id = $tag_details[0]['/product-contracts/product-contract[1]/identified-by[1]/product-contract-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
        } elseif (isset($tag_details[0]['/product-contracts/product-contract[1]/identified-by[1]/product-contract-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
            $contract_tag_id = $tag_details[0]['/product-contracts/product-contract[1]/identified-by[1]/product-contract-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
            $is_new_contract = false;
        }

        // populate contract header details from post xml
        if (!empty($contract_tag_id)) {

            // get bundled-product reference tag value
            $bundled_product_tag_id = '';
            if (isset($tag_details[0]['/product-contracts/product-contract[1]/association-ends[1]/association-end[1]/target[1]/reference-to-bundled-product[1]/bundled-product-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                $bundled_product_tag_id = $tag_details[0]['/product-contracts/product-contract[1]/association-ends[1]/association-end[1]/target[1]/reference-to-bundled-product[1]/bundled-product-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
            } elseif (isset($tag_details[0]['/product-contracts/product-contract[1]/association-ends[1]/association-end[1]/target[1]/reference-to-bundled-product[1]/bundled-product-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                $bundled_product_tag_id = $tag_details[0]['/product-contracts/product-contract[1]/association-ends[1]/association-end[1]/target[1]/reference-to-bundled-product[1]/bundled-product-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
            }

            // DF-1292 Check agreement-period xml block exists
            $is_aggreement_period_block = true;
            $aggreement_period_block = $this->getTagElements($post_request, '//bi:agreement-period');
            if( empty($aggreement_period_block) || (isset($aggreement_period_block['0']['/agreement-period']) && trim($aggreement_period_block['0']['/agreement-period'] == '' )) ){
                $is_aggreement_period_block = false;
            }

            // get contract start-date-time tag value
            $contract_startdate_datatimezone_arr = array();
            $contract_startdate_xml_value = '';
			$contract_startdate = '';
			$contract_startdate_timezone = '';
            if (isset($tag_details[0]['/product-contracts/product-contract[1]/agreement-period[1]/start-date-time[1]'])) {
                $contract_startdate_xml_value = $tag_details[0]['/product-contracts/product-contract[1]/agreement-period[1]/start-date-time[1]'];
                $contract_startdate_datatimezone_arr = $this->fnGetDateTimeZone($contract_startdate_xml_value);
			    $contract_startdate = isset($contract_startdate_datatimezone_arr['date_time']) ? $contract_startdate_datatimezone_arr['date_time'] : '';
				$contract_startdate_timezone = isset($contract_startdate_datatimezone_arr['time_zone']) ? $contract_startdate_datatimezone_arr['time_zone'] : '';
            }

            // get contract end-date-time tag value
            $contract_enddate_datatimezone_arr = array();
            $contract_enddate_xml_value = '';
            $contract_enddate = '';
            $contract_enddate_timezone = '';
            if (isset($tag_details[0]['/product-contracts/product-contract[1]/agreement-period[1]/end-date-time[1]'])) {
                $contract_enddate_xml_value = $tag_details[0]['/product-contracts/product-contract[1]/agreement-period[1]/end-date-time[1]'];
            }


            if (!empty($contract_startdate_xml_value) && empty($contract_enddate_xml_value)) {
                global $sugar_config;
                $contract_enddate_xml_value = $sugar_config['gcm_cstm_const']['default_end_datetime'];
            }

            if (!empty($contract_enddate_xml_value)) {
                $contract_enddate_datatimezone_arr = $this->fnGetDateTimeZone($contract_enddate_xml_value);
                $contract_enddate = isset($contract_enddate_datatimezone_arr['date_time']) ? $contract_enddate_datatimezone_arr['date_time'] : '';
                $contract_enddate_timezone = isset($contract_enddate_datatimezone_arr['time_zone']) ? $contract_enddate_datatimezone_arr['time_zone'] : '';
            }

            // get product offering pms id from bundled-product tag
            $product_offering_id = '';
            $result_bundled_product = $this->getBundledProductDetailsFromXML($post_request);
            if (!empty($result_bundled_product) && !empty($bundled_product_tag_id)) {
                $product_offering_id = $this->uuidRemoveFormat($result_bundled_product[$bundled_product_tag_id]);
            }

            // get contract global id
            $global_contract_id = isset($tag_details[0]['/product-contracts/product-contract[1]/contract-id[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/contract-id[1]'] : '';

            // get contract version
            $contract_version = isset($tag_details[0]['/product-contracts/product-contract[1]/described-by[1]/reference-to-product-contract-spec[1]/product-contract-spec-key[1]/key-type-for-cbe-standard[1]/object-version[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/described-by[1]/reference-to-product-contract-spec[1]/product-contract-spec-key[1]/key-type-for-cbe-standard[1]/object-version[1]'] : '';

            // get contract name
            $contract_name = isset($tag_details[0]['/product-contracts/product-contract[1]/name[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/name[1]'] : '';

            // get contract description
            $contract_description = isset($tag_details[0]['/product-contracts/product-contract[1]/description[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/description[1]'] : '';

            $contract_status_id = '';
            $contract_status_val = '';
            //set contract status
            if (isset($tag_details[0]['/product-contracts/product-contract[1]/standard-business-state[1]'])) {
                $contract_status_val = $tag_details[0]['/product-contracts/product-contract[1]/standard-business-state[1]'];
                foreach ($GLOBALS['app_list_strings']['contract_status_list'] as $cs_key => $cs_val) {
                    if ($cs_val == $contract_status_val)
                        $contract_status_id = $cs_key;
                }
            }

            // get client request id
            $contract_request_id = isset($tag_details[0]['/product-contracts/product-contract[1]/client-request-id[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/client-request-id[1]'] : '';

            // get ext_sys_created_by
            $ext_sys_created_by = isset($tag_details[0]['/product-contracts/product-contract[1]/created-user[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/created-user[1]'] : '';

            // get ext_sys_modified_by
            $ext_sys_modified_by = isset($tag_details[0]['/product-contracts/product-contract[1]/last-modified-user[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/last-modified-user[1]'] : '';

            // set lock version
            $lock_version = '1';

            // set workflow_flag
            $workflow_flag = '0';

            // get vat no
            $vat_no = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/contract-customer[1]/corporation[1]/vat-no[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/contract-customer[1]/corporation[1]/vat-no[1]'] : '';

            $contract_tag_details_arr[$contract_tag_id]['ContractsHeader'] = array(
                                                                                'is_new_record' => $is_new_contract,
                                                                                'is_aggreement_period_block' => $is_aggreement_period_block,
                                                                                'global_contract_id_xml_id' => $contract_tag_id,
                                                                                'global_contract_id' => $global_contract_id,
                                                                                'contract_version' => $contract_version,
                                                                                'name' => $contract_name,
                                                                                'description' => $contract_description,
                                                                                'contract_status' => $contract_status_id,
                                                                                'contract_status_xml_val' => $contract_status_val,
                                                                                'contract_request_id' => $contract_request_id,
                                                                                'product_offering' => $product_offering_id,
                                                                                'product_offering_xml_id' => $bundled_product_tag_id,
                                                                                'ext_sys_created_by' => $ext_sys_created_by,
                                                                                'ext_sys_modified_by' => $ext_sys_modified_by,
                                                                                'lock_version' => $lock_version,
                                                                                'total_nrc' => '',
                                                                                'total_mrc' => '',
                                                                                'grand_total' => '',
                                                                                'workflow_flag' => $workflow_flag,
                                                                                'vat_no' => $vat_no
            );

            if(!empty($contract_startdate)){
                $contract_tag_details_arr[$contract_tag_id]['ContractsHeader']['contract_startdate'] = $contract_startdate;
            }
            if(!empty($contract_startdate_timezone)){
                $contract_tag_details_arr[$contract_tag_id]['ContractsHeader']['contract_startdate_timezone'] = $contract_startdate_timezone;
            }
            if(!empty($contract_enddate)){
                $contract_tag_details_arr[$contract_tag_id]['ContractsHeader']['contract_enddate'] = $contract_enddate;
            }
            if(!empty($contract_enddate_timezone)){
                $contract_tag_details_arr[$contract_tag_id]['ContractsHeader']['contract_enddate_timezone'] = $contract_enddate_timezone;
            }

            // get sales channel code
            $sales_channel_code = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/corporation[1]/sales-channel-code[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/corporation[1]/sales-channel-code[1]'] : '';

            // get sales representative latin name
            $sales_rep_name_latin = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/name-latin[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/name-latin[1]'] : '';

            // get sales representative local name
            $sales_rep_name_local = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/name[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/name[1]'] : '';

            // get sales representative division latin
            $sales_division_latin = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/division[1]/name-latin[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/division[1]/name-latin[1]'] : '';

            // get sales representative division local
            $sales_division_local = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/division[1]/name[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/division[1]/name[1]'] : '';

            // get sales representative title latin
            $sales_title_latin = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/title-latin[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/title-latin[1]'] : '';

            // get sales representative title local
            $sales_title_local = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/title[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/individual[1]/title[1]'] : '';

            // get sales representative tel no
            $sales_tel_no = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/telephone-number[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/telephone-number[1]'] : '';

            // get sales representative ext no
            $sales_ext_no = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/extension-number[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/extension-number[1]'] : '';

            // get sales representative fax no
            $sales_fax_no = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/fax-number[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/fax-number[1]'] : '';

            // get sales representative email
            $sales_email1 = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/email-address[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/person-in-charge[1]/contact-medium[1]/email-address[1]'] : '';

            // get sales company
            $sales_company = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/corporation[1]/standard-corporation-name[1]/name[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/sales-channel[1]/corporation[1]/standard-corporation-name[1]/name[1]'] : '';

            // populate sales representative details from post xml
			$check_sales_rep_tag_exist = '';
			$check_sales_rep_tag_exist = $this->getTagElements($post_request, '//gcm:sales-channel');
            if (! empty($check_sales_rep_tag_exist))
			{
                $contract_tag_details_arr[$contract_tag_id]['ContractSalesRepresentative'] = array(
                                                                                            '0' => array(
                                                                                                        'sales_channel_code' => $sales_channel_code,
                                                                                                        'name' => $sales_rep_name_latin,
                                                                                                        'sales_rep_name_1' => $sales_rep_name_local,
                                                                                                        'division_1' => $sales_division_latin,
                                                                                                        'division_2' => $sales_division_local,
                                                                                                        'title_1' => $sales_title_latin,
                                                                                                        'title_2' => $sales_title_local,
                                                                                                        'tel_no' => $sales_tel_no,
                                                                                                        'ext_no' => $sales_ext_no,
                                                                                                        'fax_no' => $sales_fax_no,
                                                                                                        'mob_no' => '',
                                                                                                        'email1' => $sales_email1,
                                                                                                        'sales_company' => $sales_company
                                                                                            )
                );
			}

            /**
             * Parse account details
             */

            $acc_types = array(
                            'Contract' => 'contract-customer',
                            'Billing' => 'billing-customer',
                            'Technology' => 'customer-technical-representative'
            );

            foreach ($acc_types as $key => $acc_type) {
                $acc_tag_details = $this->getTagElements($post_request, '//gcm:' . $acc_type);
                if (!empty($acc_tag_details)) {
                    $country_code = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/iso3166-1numeric[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/iso3166-1numeric[1]'] : '';
                    $country_name = '';
                    $postal_code = '';
                    $addr_general_c_us_canada = '';
                    $attention_line = '';

                    if ($country_code == '840') {
                        $country_name = 'american';
                        $postal_code = 'zip-code';
                        $addr_general_c_us_canada = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/american-property-address[1]/address-line[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/american-property-address[1]/address-line[1]'] : '';
                    } elseif ($country_code == '124') {
                        $country_name = 'canadian';
                        $postal_code = 'postal-code';
                        $addr_general_c_us_canada = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/canadian-property-address[1]/address-line[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/canadian-property-address[1]/address-line[1]'] : '';
                    } elseif ($country_code == '392') {
                        $country_name = 'japanese';
                        $postal_code = 'postal-code';
                    }

                    // get attention line
                    if (!empty($country_name)) {
                        $attention_line = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/' . $country_name . '-property-address[1]/attention-line[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/' . $country_name . '-property-address[1]/attention-line[1]'] : '';
                    } elseif(!empty($country_code)) {
                        $attention_line = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/any-address[1]/attention-line[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/any-address[1]/attention-line[1]'] : '';
                    }

                    // get account individual name latin
                    $individual_name_latin = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/name-latin[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/name-latin[1]'] : '';

                    // get account individual name local
                    $individual_name_local = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/name[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/name[1]'] : '';

                    // get account postal number
                    $acc_postal_no = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/' . $country_name . '-property-address[1]/' . $postal_code . '[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/' . $country_name . '-property-address[1]/' . $postal_code . '[1]'] : '';

                    // get account any address 1
					$acc_any_address_1 = '';
					if(!empty($country_code)){
						$acc_any_address_1 = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/any-address[1]/address[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/any-address[1]/address[1]'] : '';
					}

                    // get account division latin
                    $acc_division_latin = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/division[1]/name-latin[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/division[1]/name-latin[1]'] : '';

                    // get account division local
                    $acc_division_local = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/division[1]/name[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/division[1]/name[1]'] : '';

                    // get account title latin
                    $acc_title_latin = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/title-latin[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/title-latin[1]'] : '';

                    // get account title local
                    $acc_title_local = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/title[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/individual[1]/title[1]'] : '';

                    // get account phone work
                    $acc_phone_work = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/telephone-number[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/telephone-number[1]'] : '';

                    // get account phone ext number
                    $acc_ext_no = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/extension-number[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/extension-number[1]'] : '';

                    // get account phone fax number
                    $acc_phone_fax = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/fax-number[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/fax-number[1]'] : '';

                    // get account email
                    $acc_email = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/email-address[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/person-in-charge[1]/contact-medium[1]/email-address[1]'] : '';

                    // get account japan address 1
                    $acc_japan_address_1 = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/address[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/address[1]'] : '';

                    // get account japan address 2
                    $acc_japan_address_2 = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/address-line[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/japanese-property-address[1]/address-line[1]'] : '';

                    // get account city
                    $acc_city = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/' . $country_name . '-property-address[1]/city[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/' . $country_name . '-property-address[1]/city[1]'] : '';

                    // get account state
                    $acc_state_code = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/' . $country_name . '-property-address[1]/state-abbr[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/address[1]/' . $country_name . '-property-address[1]/state-abbr[1]'] : '';

                    // Check corporate tag exist in XML
                    $is_corp_tag_exist = 'Yes';
                    $is_corp_tag_exist_arr = $this->getTagElements($post_request, '//gcm:'.$acc_type.'/gcm:corporation');
                    if( empty($is_corp_tag_exist_arr) || isset($is_corp_tag_exist_arr['0']['/corporation'])){
                        $is_corp_tag_exist = 'No';
                    }

                    // get common customer id
                    $common_customer_id = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/corporation-id[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/corporation-id[1]'] : '';

                    // get corporate name latin
                    $corporate_name_latin = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/standard-corporation-name[1]/name-latin[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/standard-corporation-name[1]/name-latin[1]'] : '';

                    // get corporate name local 1
                    $corporate_name_local_1 = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/standard-corporation-name[1]/name[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/standard-corporation-name[1]/name[1]'] : '';

                    // get corporate name local 2
                    $corporate_name_local_2 = isset($tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/standard-corporation-name[1]/name-furigana[1]']) ? $tag_details[0]['/product-contracts/product-contract[1]/expansions[1]/expansion-of-product-contract-by-gcm[1]/' . $acc_type . '[1]/corporation[1]/standard-corporation-name[1]/name-furigana[1]'] : '';

                    // set customer status code
                    $customer_status_code_c = '0';

                    // set cidas_data flag
                    $cidas_data = 'No';

                    // set cidas_search flag
                    $cidas_search = 'No';

                    // populate accounts details from post xml
                    $contract_tag_details_arr[$contract_tag_id]['Accounts'][$key] = array(
                                                                                        'contact_type_c' => $key,
                                                                                        'last_name' => $individual_name_latin,
                                                                                        'name_2_c' => $individual_name_local,
                                                                                        'postal_no_c' => $acc_postal_no,
                                                                                        'addr_1_c' => $acc_any_address_1,
                                                                                        'addr_2_c' => '',
                                                                                        'division_1_c' => $acc_division_latin,
                                                                                        'division_2_c' => $acc_division_local,
                                                                                        'title' => $acc_title_latin,
                                                                                        'title_2_c' => $acc_title_local,
                                                                                        'phone_work' => $acc_phone_work,
                                                                                        'ext_no_c' => $acc_ext_no,
                                                                                        'phone_fax' => $acc_phone_fax,
                                                                                        'phone_mobile' => '',
                                                                                        'email1' => $acc_email,
                                                                                        'country_code_numeric' => $country_code,
                                                                                        'attention_line_c' => $attention_line,
                                                                                        'Accounts_js' => array(
                                                                                                            'addr1' => $acc_japan_address_1,
                                                                                                            'addr2' => $acc_japan_address_2
                                                                                        ),
                                                                                        'general_address' => array(
                                                                                                                'addr_general_c' => $addr_general_c_us_canada,
                                                                                                                'city_general_c' => $acc_city,
                                                                                                                'state_general_code' => $acc_state_code
                                                                                        ),
                                                                                        'CorporateDetails' => array(
                                                                                                                    'common_customer_id_c' => $common_customer_id,
                                                                                                                    'name' => $corporate_name_latin,
                                                                                                                    'name_2_c' => $corporate_name_local_1,
                                                                                                                    'name_3_c' => $corporate_name_local_2,
                                                                                                                    'country_code_c' => '',
                                                                                                                    'country_name_c' => '',
                                                                                                                    'customer_status_code_c' => $customer_status_code_c,
                                                                                                                    'hq_addr_1_c' => '',
                                                                                                                    'hq_addr_2_c' => '',
                                                                                                                    'hq_postal_no_c' => '',
                                                                                                                    'reg_addr_1_c' => '',
                                                                                                                    'reg_addr_2_c' => '',
                                                                                                                    'reg_postal_no_c' => '',
                                                                                                                    'cidas_data' => $cidas_data,
                                                                                                                    'cidas_search' => $cidas_search,
                                                                                                                    'is_block_exist' => $is_corp_tag_exist
                                                                                        )
                    );
                    foreach ($acc_tag_details[0] as $acc_key => $acc_val) {
                        if (preg_match('/\-address\[1\]\//', $acc_key)) {
                            $contract_tag_details_arr[$contract_tag_id]['Accounts'][$key]['address_tag_name'] = $this->get_country_tag_name("/address[1]/", "-address[1]/", $acc_key);
                            break;
                        }
                    }
                }
            }

            // populate billing affiliate details from post xml
            $contract_tag_details_arr[$contract_tag_id]['BillingAffiliate']['0'] = $this->getBillingAffiliateTagDetailsFromXML($post_request);
        }
        return $contract_tag_details_arr;
    }

    /**
     * Method to parse bundled-product tag from xml
     *
     * @param xml $post_request xml data
     * @return array Convert bundled product tag xml data into array format
     */
    private function getBundledProductDetailsFromXML($post_request)
    {
        $product_offering_details_arr = array();
        $tag_details = $this->getTagElements($post_request, "//product:bundled-products");

        if (!empty($tag_details)) {
            if (isset($tag_details[0]['/bundled-products/bundled-product[1]/identified-by[1]/bundled-product-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'])) {
                $bundled_product_tag_id = $tag_details[0]['/bundled-products/bundled-product[1]/identified-by[1]/bundled-product-key[1]/key-type-for-registration[1]/valid-id-only-in-message[1]'];
            } elseif (isset($tag_details[0]['/bundled-products/bundled-product[1]/identified-by[1]/bundled-product-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                $bundled_product_tag_id = $tag_details[0]['/bundled-products/bundled-product[1]/identified-by[1]/bundled-product-key[1]/key-type-for-cbe-standard[1]/object-id[1]'];
            }
            if (!empty($bundled_product_tag_id) && isset($tag_details[0]['/bundled-products/bundled-product[1]/described-by[1]/reference-to-bundled-product-offering[1]/bundled-product-offering-key[1]/key-type-for-cbe-standard[1]/object-id[1]'])) {
                $product_offering_details_arr = array(
                                                    $bundled_product_tag_id => $tag_details[0]['/bundled-products/bundled-product[1]/described-by[1]/reference-to-bundled-product-offering[1]/bundled-product-offering-key[1]/key-type-for-cbe-standard[1]/object-id[1]']
                );
            }
        }
        return $product_offering_details_arr;
    }

    /**
     * Method to parse billing-affiliate tag from xml
     *
     * @param xml $post_request xml data
     * @return array billing affiliate tag details
     */
    private function getBillingAffiliateTagDetailsFromXML($post_request)
    {
        $tag_details = $this->getTagElements($post_request, '//gcm:billing-affiliate');
        $billing_affiliate_array = array();
        if (!empty($tag_details)) {
            $billing_affiliate_array['name'] = isset($tag_details[0]['/billing-affiliate/organization-name[1]/level1[1]/name[1]'])?$tag_details[0]['/billing-affiliate/organization-name[1]/level1[1]/name[1]']:'';
            $billing_affiliate_array['organization_name_2_local'] = isset($tag_details[0]['/billing-affiliate/organization-name[1]/level2[1]/name[1]'])?$tag_details[0]['/billing-affiliate/organization-name[1]/level2[1]/name[1]']:'';
            $billing_affiliate_array['organization_name_3_local'] = isset($tag_details[0]['/billing-affiliate/organization-name[1]/level3[1]/name[1]'])?$tag_details[0]['/billing-affiliate/organization-name[1]/level3[1]/name[1]']:'';
            $billing_affiliate_array['organization_name_4_local'] = isset($tag_details[0]['/billing-affiliate/organization-name[1]/level4[1]/name[1]'])?$tag_details[0]['/billing-affiliate/organization-name[1]/level4[1]/name[1]']:'';
            $billing_affiliate_array['organization_name_1_latin'] = isset($tag_details[0]['/billing-affiliate/organization-name[1]/level1[1]/name-latin[1]'])?$tag_details[0]['/billing-affiliate/organization-name[1]/level1[1]/name-latin[1]']:'';
            $billing_affiliate_array['organization_name_2_latin'] = isset($tag_details[0]['/billing-affiliate/organization-name[1]/level2[1]/name-latin[1]'])?$tag_details[0]['/billing-affiliate/organization-name[1]/level2[1]/name-latin[1]']:'';
            $billing_affiliate_array['organization_name_3_latin'] = isset($tag_details[0]['/billing-affiliate/organization-name[1]/level3[1]/name-latin[1]'])?$tag_details[0]['/billing-affiliate/organization-name[1]/level3[1]/name-latin[1]']:'';
            $billing_affiliate_array['organization_name_4_latin'] = isset($tag_details[0]['/billing-affiliate/organization-name[1]/level4[1]/name-latin[1]'])?$tag_details[0]['/billing-affiliate/organization-name[1]/level4[1]/name-latin[1]']:'';
            $billing_affiliate_array['organization_code_1'] = isset($tag_details[0]['/billing-affiliate/organization-code[1]/code1[1]'])?$tag_details[0]['/billing-affiliate/organization-code[1]/code1[1]']:'';
            $billing_affiliate_array['organization_code_2'] = isset($tag_details[0]['/billing-affiliate/organization-code[1]/code2[1]'])?$tag_details[0]['/billing-affiliate/organization-code[1]/code2[1]']:'';
        }
        return $billing_affiliate_array;
    }

}
?>
