<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (â€œMSAâ€�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
/**
 * ClassBusinessLogicValidation to do the business validation for all API request
 * @author : Sathish Kumar Ganesan
 * @since : 11-Jun-2016
 * @type unix-file system UTF-8
 */
include_once ('custom/modules/gc_Contracts/CustomFunctionGcContract.php');
require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';
require_once 'custom/include/ModUtils.php';
require_once 'include/SugarEmailAddress/SugarEmailAddress.php';

class ClassBusinessLogicValidation
{
    /**
    * Payment Type Constants
    */
    const DIRECT_DEPOSIT = '00';
    const ACCOUNT_TRANSFER = '10';
    const POSTAL_TRANSFER = '30';
    const CREDIT_CARD = '60';

    /**
    * Line Item bi:action constants
    */
    const LI_NO_CHANGE = 'no change';
    const LI_CHANGE = 'change';
    const LI_REMOVE = 'remove';
    const LI_ADD = 'add';


    /**
    * Class Variable
    */
    private $errors;
    private $error_message;
	private $modUtils;


    /**
     * Class Constructor to initialize class variables
     * @return none
     */
    public function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Class Constructor to initialize class variables');
        $this->errors = array();
        $this->error_message = array();
		$this->modUtils = new ModUtils();
		ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * Method to validate the business logic for API 2.0 POST request
     * @param array $xml_array POST request parse array
     * @param string $global_contract_id - Global Contract ID string
     * @param string $global_contract_uuid - Global Contract UUID string
     * @return array error codes in array. Empty array if no errors
     * @author Sathish Kumar Ganesan
     * @since Jun 04, 2016
     */
    public function validatePOSTXMLData($xml_array, $global_contract_id, $global_contract_uuid, $is_version_up){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'xml_array: '.print_r($xml_array,true).' '.GCM_GL_LOG_VAR_SEPARATOR.' global_contract_id: '.$global_contract_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' global_contract_uuid: '.$global_contract_uuid.' '.GCM_GL_LOG_VAR_SEPARATOR.' is_version_up: '.$is_version_up, 'Method to validate the business logic for API 2.0 POST request');
        global $sugar_config;
        // convert UTC datetime => local datetime
        $xml_array = $this->processRequestDatetime($xml_array);
        $contract_header = !empty($xml_array['ContractDetails']['ContractsHeader']) ? $xml_array['ContractDetails']['ContractsHeader'] : array();
        if($this->validateReferenceID($xml_array, $global_contract_id, $global_contract_uuid)){

            $this->isEmptyField($contract_header['name'],'3007');
            $this->isEmptyField($contract_header['product_offering'],'3008');
            $this->isEmptyArray($xml_array['ContractDetails']['ContractLineItemDetails'],'3009');

            //If all Product characteristics are empty
            if(empty($contract_header['surrogate_teams']) && trim($contract_header['contract_scheme'])=='' &&
               trim($contract_header['loi_contract'])=='' && trim($contract_header['contract_type'])=='' &&
               trim($contract_header['billing_type'])=='' && trim($contract_header['factory_reference_no'])==''){
                $this->error_message['3011'] = '';
            }
            //DF-1187 - Throw error if the billing type is not 'OneStop'
            if(isset($contract_header['billing_type']) && $contract_header['billing_type']!='' && $contract_header['billing_type']!='OneStop'){
                $this->error_message['3022'][] = 'Invalid Billing Type';
            }
            $timezones = $sales_companies = $country_codes = array();
            $timezone_bean = $obj_timezone = BeanFactory::getBean('gc_TimeZone');
            $timezone_list = $timezone_bean->get_full_list('',"c_country_id_c = ''");
            if(is_array($timezone_list)){
                foreach($timezone_list as $timezone){
                    $timezones[] = $timezone->name;
                }
            }

            $sales_company_bean = BeanFactory::getBean('gc_NTTComGroupCompany');
            $sales_company_list = $sales_company_bean->get_full_list('','');
            if(is_array($sales_company_list)){
                foreach($sales_company_list as $sales_company){
                    $sales_companies[] = $sales_company->name;
                }
            }

            $country_bean = $obj_timezone = BeanFactory::getBean('c_country');
            $country_list = $country_bean->get_full_list('','');
            if(is_array($country_list)){
                foreach($country_list as $country){
                    $country_codes[] = $country->country_code_numeric;
                }
            }

            if(!$is_version_up && trim($contract_header['contract_request_id'])==''){
                $this->error_message['3020'][] = 'Client Request ID should not be empty';
            }

            $contract_start_date = $contract_header['contract_startdate'];
            $contract_end_date = $contract_header['contract_enddate'];
            $product_offering = trim($contract_header['product_offering']);
            if(!$is_version_up){
                if(isset($sugar_config['workflow_flag'][$product_offering]) && strtolower($sugar_config['workflow_flag'][$product_offering]) == 'yes'){
                    if($contract_header['is_aggreement_period_block'] && empty($contract_start_date)){
                        $this->error_message['3020'][] = 'Contract Start Date is mandatory';
                    }
                }else{
                    if(!$contract_header['is_aggreement_period_block'] || empty($contract_start_date)){
                        $this->error_message['3020'][] = 'Contract Start Date is mandatory';
                    }
                }
            }
            if(!empty($contract_start_date) && !empty($contract_end_date) && (strtotime($contract_start_date) > strtotime($contract_end_date))){
                $this->error_message['3020'][] = 'Contract End Date should be greater than Contract Start Date';
            }
            if(!empty($xml_array['ContractDetails']['ContractSalesRepresentative']) && trim($xml_array['ContractDetails']['ContractSalesRepresentative'][0]['sales_company']) !='' && !in_array($xml_array['ContractDetails']['ContractSalesRepresentative'][0]['sales_company'],$sales_companies)){
                $this->error_message['3018'][] = 'Sales company not exist in the system';
            }
            if(trim($contract_header['contract_startdate_timezone'])!='' && !in_array($contract_header['contract_startdate_timezone'], $timezones)){
                $this->error_message['3018'][] = 'Contract Start Date timezone '.$contract_header['contract_startdate_timezone'].' does not exist';
            }
            if(trim($contract_header['contract_enddate_timezone'])!='' && !in_array(trim($contract_header['contract_enddate_timezone']), $timezones)){
                $this->error_message['3018'][] = 'Contract End Date timezone '.$contract_header['contract_enddate_timezone'].' does not exist';
            }
            // Validating email id
            if(isset($xml_array['ContractDetails']['ContractLineItemDetails'][0]['ContractDirectDeposit']['payee_email_address']) && $xml_array['ContractDetails']['ContractLineItemDetails'][0]['ContractDirectDeposit']['payee_email_address']!='' && !$this->modUtils->email_validation($xml_array['ContractDetails']['ContractLineItemDetails'][0]['ContractDirectDeposit']['payee_email_address'])){
                    $this->error_message['3018'][] = 'Direct Deposit Payee Email ('.$xml_array['ContractDetails']['ContractLineItemDetails'][0]['ContractDirectDeposit']['payee_email_address'].') is not valid';
            }
            //Line Item characteristics
            $index = 0;
            if(!empty($xml_array['ContractDetails']['ContractLineItemDetails'])){
                foreach($xml_array['ContractDetails']['ContractLineItemDetails'] as $line_item){
                    $line_item_header = $line_item['ContractLineItemHeader'];
                    if(trim($line_item_header['product_specification'])!=''){
                        $pms_id = $this->formatPMSID($line_item_header['product_specification'],1);
                        $line_item_group = $line_item['InvoiceGroupDtls'];
                        //if all characteristic values are empty for the selected PMS
                        if(trim($line_item_header['service_start_date']) == '' && trim($line_item_header['service_end_date']) == '' &&
                           trim($line_item_header['billing_start_date']) == '' && trim($line_item_header['billing_end_date']) == '' &&
                           trim($line_item_header['cust_contract_currency']) == '' && trim($line_item_header['cust_billing_currency']) == '' &&
                           trim($line_item_header['igc_contract_currency']) == '' && trim($line_item_header['igc_billing_currency']) == '' &&
                           trim($line_item_header['customer_billing_method']) == '' && trim($line_item_header['igc_settlement_method']) == '' &&
                           trim($line_item_header['description']) == '' && trim($line_item_header['factory_reference_no']) == '' &&
                           trim($line_item_group['name']) == '' && trim($line_item_group['description'])==''){
                            $this->error_message['3012'][] = 'PMS ID "'.$pms_id.'" - Characteristics should not be empty';
                        }
                        if(!empty($line_item_header['service_start_date']) && !empty($line_item_header['service_end_date']) && (strtotime($line_item_header['service_start_date']) > strtotime($line_item_header['service_end_date']))){
                            $this->error_message['3020'][] = 'PMS ID "'.$pms_id.'" - Service End Date should be greater than Service Start Date';
                        }
                        if(!empty($line_item_header['billing_start_date']) && !empty($line_item_header['billing_end_date']) && (strtotime($line_item_header['billing_start_date']) > strtotime($line_item_header['billing_end_date']))){
                            $this->error_message['3020'][] = 'PMS ID "'.$pms_id.'" - Billing End Date should be greater than Billing Start Date';
                        }
                        if(trim($line_item_header['service_start_date_timezone'])!='' && trim($line_item_header['service_start_date_timezone'])!='00:00' && !in_array($line_item_header['service_start_date_timezone'],$timezones)){
                            $this->error_message['3018'][] = 'PMS ID "'.$pms_id.'" - Service Start Date Timezone "'.$line_item_header['service_start_date_timezone'].'" does not exist';
                        }
                        if(trim($line_item_header['service_end_date_timezone'])!='' && trim($line_item_header['service_end_date_timezone'])!='00:00' && !in_array($line_item_header['service_end_date_timezone'],$timezones)){
                            $this->error_message['3018'][] = 'PMS ID "'.$pms_id.'" - Service End Date Timezone "'.$line_item_header['service_end_date_timezone'].'" does not exist';
                        }
                        if(trim($line_item_header['billing_start_date_timezone'])!='' && trim($line_item_header['billing_start_date_timezone'])!='00:00' && !in_array(trim($line_item_header['billing_start_date_timezone']),$timezones)){
                            $this->error_message['3018'][] = 'PMS ID "'.$pms_id.'" - Billing Start Date Timezone "'.$line_item_header['billing_start_date_timezone'].'" does not exist';
                        }
                        if(trim($line_item_header['billing_end_date_timezone'])!='' && trim($line_item_header['billing_start_date_timezone'])!='00:00' && !in_array($line_item_header['billing_end_date_timezone'],$timezones)){
                            $this->error_message['3018'][] = 'PMS ID "'.$pms_id.'" - Billing End Date Timezone "'.$line_item_header['billing_end_date_timezone'].'" does not exist';
                        }
                    }else{
                        $this->error_message['3009'][] = 'Atomic Product should not be empty for "'.$line_item_header['line_item_id_xml_id'].'" product-biitem';
                    }
                }

                //Payment Details
                // update sprint 27
                $index = key($xml_array['ContractDetails']['ContractLineItemDetails']);
            }


            //team validation
            if(!$this->isEmptyArray($contract_header['surrogate_teams'],'')){
                foreach($contract_header['surrogate_teams'] as $teamname){
                    $teamid = $this->modUtils->retrieveTeamId($teamname);
                    if($teamid === false){
                        $this->error_message['3014'][] = 'Team "'.$teamname.'" does not exist';
                    }
                }
            }
            //Validate ContractSalesRepresentative has valid email id

            if(isset($xml_array['ContractDetails']['ContractSalesRepresentative']) && !empty($xml_array['ContractDetails']['ContractSalesRepresentative'])){
                foreach($xml_array['ContractDetails']['ContractSalesRepresentative'] as $ContractSalesRepresentative){
                    if(isset($ContractSalesRepresentative['email1']) && trim($ContractSalesRepresentative['email1'])!='' && !$this->modUtils->email_validation($ContractSalesRepresentative['email1'])){
                        $this->error_message['3018'][] = 'Sales representative Email ('.$ContractSalesRepresentative['email1'].') is not valid';
                    }
                }
            }

            foreach($xml_array['Accounts'] as $key => $account){
                if(isset($xml_array['Accounts'][$key])){
                    if(trim($account['last_name'])=='' && trim($account['name_2_c'])==''){
                        $this->error_message['3015'][] = $key.' - Contact Person Name is mandatory';
                    }
                    if(isset($account['CorporateDetails'])){
                        if(trim($account['CorporateDetails']['common_customer_id_c']) == '' && trim($account['CorporateDetails']['name']) == '' &&
                            trim($account['CorporateDetails']['name_2_c']) == ''){
                                $this->error_message['3017'][] = $key.' - Corporate ID or Name is mandatory';
                        }
                    }else{
                        $this->error_message['3017'][] = $key.' - Corporate Details is mandatory';
                    }
                    $country_code_numeric = isset($account['country_code_numeric'])?$account['country_code_numeric']:'';
                    if(trim($country_code_numeric) != ''){
                        if(!in_array($account['country_code_numeric'],$country_codes)){
                            $this->error_message['3018'][] = $key.' Account Reference Country not exist';
                        }
                        if($account['general_address']['state_general_code']!='' && !$this->checkCountryStateValidation($account['country_code_numeric'],$account['general_address']['state_general_code'])){
                            $this->error_message['3018'][] = $key.' Account Country and State combination does not match';
                        }
                    }

                    //Check if the country tag name and address block is mismatch
                    if(isset($account['address_tag_name']) && trim($account['address_tag_name'])!=''){
                        switch ($country_code_numeric) {
                            case '840':
                                $expected_addr_tag_name = 'american';
                                break;
                            case '124':
                                $expected_addr_tag_name = 'canadian';
                                break;
                            case '392':
                                $expected_addr_tag_name = 'japanese';
                                break;
                            default:
                                $expected_addr_tag_name = 'any';
                                break;
                        }
                        if(trim(strtolower($account['address_tag_name'])) != $expected_addr_tag_name){
                            $this->error_message['3018'][] = $key.' Country code & Address block XML Mis-match';
                        }

                    }

                    //DF-838 - Email validation added
                    if(isset($account['email1']) && trim($account['email1'])!='' && !SugarEmailAddress::isValidEmail($account['email1'])){
                        $email_msg = '';
                        if($key == 'Contract'){
                            $email_msg = 'Contracting Account';
                        }
                        elseif($key == 'Billing'){
                            $email_msg = 'Billing Account';
                        }
                        elseif($key == 'Technology'){
                            $email_msg = 'Technology Account';
                        }
                        $this->error_message['3018'][] = $email_msg.' Email ('.$account['email1'].') is not valid';
                    }
                }
            }

            //Adding validation for billing affiliate - DF-1053
            //update sprint 27
            $this->validateBillingAffiliate($xml_array['ContractDetails']);
            $this->validatePaymentDetails($xml_array['ContractDetails']['ContractLineItemDetails'][$index]);

            if(!isset($xml_array['Accounts']['Contract'])){
                $this->error_message['3018'][] = 'Contracting Customer Company is mandatory';
            }
        }
        if(!empty($this->error_message)){
            $this->errors['codes'] = array_keys($this->error_message);
            $this->errors['messages'] = $this->error_message;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'errors: '.print_r($this->errors,true), '');
        return $this->errors;
    }

    /**
     * Method to do the business logic validation for version up contract
     * @param array $xml_arr Arrat
     * @param string $global_contract_id Global contract id of the contract
     * @param string $global_contract_uuid Global contract id uuid of the contract
     * @return array error codes in array. Empty array if no errors
     * @author mohamed.siddiq
     * @since Jul 29, 2016
     */
    public function validateVersionUpPOSTXMLData($xml_arr, $global_contract_id, $global_contract_uuid){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'xml_arr: '.print_r($xml_arr,true).' '.GCM_GL_LOG_VAR_SEPARATOR.' global_contract_id: '.$global_contract_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' global_contract_uuid: '.$global_contract_uuid, 'Method to do the business logic validation for version up contract');
        global $sugar_config;
        // convert UTC datetime => local datetime
        $xml_arr = $this->processRequestDatetime($xml_arr);
        //Check is global contract id is exist in the url
        if($this->checkCondition((trim($global_contract_id)!='' || trim($global_contract_uuid)!=''), '3021', 'Contract Reference ID should not be empty versionup')){

            //For all the request do the basic validation
            $this->validatePOSTXMLData($xml_arr, $global_contract_id, $global_contract_uuid, true);

            //Get the contract_header
            $contract_header = $xml_arr['ContractDetails']['ContractsHeader'];

            //Take the new most version of contract
            $CustomFunctionGcContractObj = new CustomFunctionGcContract();
            $preivious_contract_header = $CustomFunctionGcContractObj->getContractHeader($global_contract_id, $global_contract_uuid);

            if($this->checkCondition((!empty($preivious_contract_header)),'6001', 'Contract details not exist in the system')){

                //Check the status of the previous version of contract
                //If the contract status is Deactivated, then through the 3019 error
                if($this->checkCondition(($preivious_contract_header->contract_status
                !='2'), '3019', 'Version up process not applicable for the requested contract')){

                    //DF-1066 - If contract billing type is not onestop then through the error.
                    if($this->checkCondition(($preivious_contract_header->billing_type=='OneStop'), '3021', 'Invalid Billing Type')){

                        //Allow only if contract status is Activated.
                        if($this->checkCondition(($preivious_contract_header->contract_status
                        =='4'), '3021', 'Version up process not applicable for the requested contract')){

                            //Check bundle product offering is same
                            if($this->checkCondition((trim($contract_header['product_offering']) == trim($preivious_contract_header->product_offering)), '3021', 'Bundle product offering should not be changed from previous version')){

                                $product_offering = trim($contract_header['product_offering']);

                                if($this->checkCondition((isset($sugar_config['workflow_flag'][$product_offering]) && strtolower($sugar_config['workflow_flag'][$product_offering]) == 'yes'), '3023', 'Contract version-up not applicable for non-workflow product')){

                                    require_once ('custom/include/contractFieldNames.php');
                                    $this->CONTRACT_FIELD_NAMES = $CONTRACT_FIELD_NAMES;
                                    $this->LI_MANDATORY_VERSIONUP_FIELDS = $LI_MANDATORY_VERSIONUP_FIELDS;
                                    $this->LI_OPTIONAL_VERSIONUP_FIELDS = $LI_OPTIONAL_VERSIONUP_FIELDS;
                                    $this->is_contract_changed = false;

                                    //IF CCID is set check whether it is changed or not
                                    if(isset($xml_arr['Accounts']['Contract']['CorporateDetails']['common_customer_id_c'])){

                                        $contract_customer_id_c = '';
                                        $contact_corporate = $preivious_contract_header->get_linked_beans('contacts_gc_contracts_1', 'Contacts');
                                        if (count($contact_corporate) == 1 && isset($contact_corporate[0]) && is_object($contact_corporate[0])){
                                            $corporate_bean = BeanFactory::getBean('Accounts'); // Corporate details
                                            $corporate_bean->retrieve($contact_corporate[0]->account_id);
                                            $contract_customer_id_c = $corporate_bean->common_customer_id_c;
                                        }
                                        if(trim($contract_customer_id_c)!=trim($xml_arr['Accounts']['Contract']['CorporateDetails']['common_customer_id_c'])){
                                            $this->is_contract_changed = true;
                                        }
                                    }

                                    //Check the global contract id is same
                                    $this->checkCondition((!isset($contract_header['global_contract_id']) || trim($contract_header['global_contract_id']) =='' || trim($contract_header['global_contract_id'])==trim($preivious_contract_header->global_contract_id)), '3021', 'Global Contract ID should not be changed from previous version');

                                    //Check the product contract UUID is same
                                    $this->checkCondition((!isset($contract_header['global_contract_id_xml_id']) || trim($contract_header['global_contract_id_xml_id']) =='' || trim($contract_header['global_contract_id_xml_id']) == trim($preivious_contract_header->global_contract_id_uuid)), '3021', 'Global Contract UUID should not be changed from previous version');

                                    //Check bundle product UUID is same
                                    $this->checkCondition((!isset($contract_header['product_offering_xml_id']) || trim($contract_header['product_offering_xml_id']) =='' || trim($contract_header['product_offering_xml_id'])==trim($preivious_contract_header->product_offering_uuid)), '3021', 'Bundle product UUID should not be changed from previous version');

                                    //Check Contract Scheme UUID is same
                                    $this->checkCondition((!isset($contract_header['contract_scheme_xml_id']) || trim($contract_header['contract_scheme_xml_id']) =='' || trim($contract_header['contract_scheme_xml_id'])==trim($preivious_contract_header->contract_scheme_uuid)), '3021', 'Contract Scheme UUID is not same from previous version');

                                    //Check Contract Type UUID is same
                                    $this->checkCondition((!isset($contract_header['contract_type_xml_id']) || trim($contract_header['contract_type_xml_id']) =='' || trim($contract_header['contract_type_xml_id'])==trim($preivious_contract_header->contract_type_uuid)), '3021', 'Contract Type UUID is not same from previous version');

                                    //Check Billing Scheme UUID is same
                                    $this->checkCondition((!isset($contract_header['billing_type_xml_id']) || trim($contract_header['billing_type_xml_id']) =='' || trim($contract_header['billing_type_xml_id'])==trim($preivious_contract_header->billing_type_uuid)), '3021', 'Billing Scheme UUID is not same from previous version');

                                    //Check LOI Contract UUID is same
                                    $this->checkCondition((!isset($contract_header['loi_contract_xml_id']) || trim($contract_header['loi_contract_xml_id']) =='' || trim($contract_header['loi_contract_xml_id'])==trim($preivious_contract_header->loi_contract_uuid)), '3021', 'LOI Contract UUID is not same from previous version');

                                    //Check Factory Reference UUID is same
                                    $this->checkCondition((!isset($contract_header['factory_reference_no_xml_id']) || trim($contract_header['factory_reference_no_xml_id']) =='' || trim($contract_header['factory_reference_no_xml_id'])==trim($preivious_contract_header->factory_reference_no_uuid)), '3021', 'Factory Reference Number UUID is not same from previous version');

                                    $startdate_timezone = '';
                                    if ($preivious_contract_header->gc_timezone_id_c != '') {
                                        $obj_timezone = BeanFactory::getBean('gc_TimeZone', $preivious_contract_header->gc_timezone_id_c);
                                        $startdate_timezone = $obj_timezone->utcoffset;
                                    }
                                    //Set the default timezone if it is empty and then compare
                                    $startdate_timezone = $this->modUtils->checkAndSetDefaultTimezone($startdate_timezone);

                                    // converting contract_start_date datetime => UTC time
                                    $preivious_contract_start_date = $preivious_contract_header->contract_startdate;
                                    if (!empty($preivious_contract_start_date)) {
                                        $preivious_contract_startdate_timezone = !empty($startdate_timezone) ? $startdate_timezone : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                                        $preivious_contract_start_date         = new DateTime($preivious_contract_start_date, new DateTimeZone($preivious_contract_startdate_timezone));
                                        $preivious_contract_start_date         = $preivious_contract_start_date->format('c');
                                        $preivious_contract_start_date_tmp     = $this->modUtils->ConvertDatetoGMTDate($preivious_contract_start_date);

                                        $preivious_contract_header->contract_startdate = $preivious_contract_start_date_tmp['date'];
                                    }
                                    //Check contract start date is same
                                    // Sprint 27 change
                                    $this->checkCondition((strtotime($contract_header['contract_startdate']) == strtotime($preivious_contract_header->contract_startdate)), '3021', 'Contract Start Date should not be changed from initial version');
                                    //Check contract start date timezone is same
                                    $this->checkCondition((trim($contract_header['contract_startdate_timezone']) == trim($startdate_timezone)), '3021', 'Contract Start Date Timezone should not be changed from initial version');

                                    $enddate_timezone = '';
                                    if ($preivious_contract_header->gc_timezone_id1_c != '') {
                                        $obj_timezone = BeanFactory::getBean('gc_TimeZone', $preivious_contract_header->gc_timezone_id1_c);
                                        $enddate_timezone = $obj_timezone->utcoffset;
                                    }
                                    $enddate_timezone = $this->modUtils->checkAndSetDefaultTimezone($enddate_timezone);
                                    // converting contract_start_date datetime => UTC time
                                    $preivious_contract_end_date = $preivious_contract_header->contract_enddate;
                                    if (!empty($preivious_contract_end_date)) {
                                        $preivious_contract_enddate_timezone = !empty($enddate_timezone) ? $enddate_timezone : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                                        $preivious_contract_end_date         = new DateTime($preivious_contract_end_date, new DateTimeZone($preivious_contract_enddate_timezone));
                                        $preivious_contract_end_date         = $preivious_contract_end_date->format('c');
                                        $preivious_contract_end_date_tmp     = $this->modUtils->ConvertDatetoGMTDate($preivious_contract_end_date);

                                        $preivious_contract_header->contract_enddate = $preivious_contract_end_date_tmp['date'];
                                    }

                                    //Check contract end date is changed or not
                                    if((strtotime($contract_header['contract_enddate'])!=strtotime($preivious_contract_header->contract_enddate)) || (trim($contract_header['contract_enddate_timezone']) != trim($enddate_timezone))){
                                        $this->is_contract_changed = true;
                                    }

                                    //now form the previous contract array to compare line items and exclude the terminated line item
                                    $previous_contract_details = array();
                                    $prev_line_item_header = $CustomFunctionGcContractObj->getLineItemHeader($preivious_contract_header->id,0,'',false);

                                    //Formate the Line Item as like requested xml array
                                    $prev_line_items = $this->formatContractLineItemDetails($prev_line_item_header);
                                    $previous_contract_details['ContractDetails']['ContractLineItemDetails'] = $prev_line_items;

                                    //Fetch available atomic product key, atomic product offering key, bii-itme key in previous contract.
                                    //That is product_specification_xml_id,product_specification,line_item_id_xml_id
                                    $available_litem_details = $this->fetchAvailableLItemDetails($prev_line_items);

                                    $requested_line_items = $this->formatRequestedContractLineItemDetails($xml_arr['ContractDetails']['ContractLineItemDetails']);

                                    //Fetch the specification details for the selected product offering, to check the specification type
                                    $product_config = new ClassProductConfiguration();
                                    $off_details = $product_config->getProductOfferingDetails($contract_header['product_offering']);
                                    $sp_details = $off_details['respose_array']['specification_details'];

                                    //Check all kind of validation that are based on line items
                                    $this->validateLineItemDetails($requested_line_items, $available_litem_details, $sp_details);

                                    //If there is no change in the version up request through this error
                                    $this->checkCondition(($this->is_contract_changed), '3021', 'No change in version up request');
                                }
                            }
                        }
                    }
                }
            }
        }
        if(!empty($this->error_message)){
            $this->errors['codes'] = array_keys($this->error_message);
            $this->errors['messages'] = $this->error_message;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'errors: '.print_r($this->errors,true), '');
        return $this->errors;
    }

    /**
     * Method to do the business logic validation for patch request
     * @param array $xml_arr Parsed array from request XML
     * @param string $global_contract_id Global contract id of the contract
     * @param string $global_contract_uuid Global contract id uuid of the contract
     * @return array error codes in array. Empty array if no errors
     * @author Sathish Kumar G
     * @since Aug 10, 2016
     */
    public function validatePATCHXMLData($xml_arr, $global_contract_id, $global_contract_uuid){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'xml_arr: '.print_r($xml_arr,true).' '.GCM_GL_LOG_VAR_SEPARATOR.' global_contract_id: '.$global_contract_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' global_contract_uuid: '.$global_contract_uuid, 'Method to do the business logic validation for patch request');
        global $sugar_config;
        require_once 'custom/service/v4_1_custom/APIHelper/v2/BusinessValidationConfig.php';
        if(!empty($xml_arr)){
            $modUtils = new ModUtils();

            $contract_header = $xml_arr['ContractDetails']['ContractsHeader'];
            $contract_list = array();
            $where_cond = '';
            if(trim($global_contract_uuid)!=''){
                $where_cond = "global_contract_id_uuid='".$global_contract_uuid."'";
            }elseif(trim($global_contract_id)!=''){
                $where_cond = "global_contract_id='".$global_contract_id."'";
            }
            $contract = BeanFactory::getBean('gc_Contracts');
            $contract_list = $contract->get_full_list('contract_version desc',$where_cond);
            $this->isEmptyArray($contract_list,'6001');
            if(!empty($contract_list)){
                //nullable validation
                // Sprint 27
                $diff_arr = $modUtils->array_diff_multi($xml_arr['NullFields'], $validation_config['NullFields']);
                if(!empty($diff_arr)){
                    $this->setNullableError($diff_arr['ContractDetails'],'');
                }else{
                    //Sprint 27 end
                    $contract =  $contract->retrieve($contract_list[0]->id);
                    if(isset($contract_header['name']))
                    	$this->isEmptyField($contract_header['name'],'3007');
                    if($contract->global_contract_id_uuid != $contract_header['global_contract_id_xml_id']){
                        $this->error_message['3022'][] = 'Product contract key mismatched with the contract reference id';
                    }
                    //DF-1066 - If contract billing type is not onestop then through the error.
                    if($contract->billing_type!='OneStop'){
                        $this->error_message['3022'][] = 'Invalid Billing Type';
                    }
                    if(round($contract->lock_version) != $contract_header['lock_version']){
                        $this->error_message['3022'][] = 'Lock Version Mismatched';
                    }
                    //Restrict the PATCH if the contract status is "Deactivated"
                    if($contract->contract_status == 2 && isset($contract_header['contract_status'])){
                        $this->error_message['3019'][] = '';
                    }
                    if (isset($contract_header['contract_status']) || (array_key_exists('contract_status', $contract_header) && empty($contract_header['contract_status']))) {
                        $contract_status = !empty(trim($contract_header['contract_status'])) ? $contract_header['contract_status'] : 0;
                        if($contract_status==''){
                            $this->error_message['3022'][] = 'Standard-business-state does not exist';
                        }elseif($contract_status != 5 && $contract_status != 2){
                            $this->error_message['3022'][] = 'Standard-business-state is not applicable';
                        }elseif(isset($sugar_config['enterprise_mail_po']) && in_array($contract->product_offering, $sugar_config['enterprise_mail_po']) && ($contract_status != 2 || ($contract_status == 2 && $contract->contract_status != 4)) ){
                            // if db contract status != Activated & post xml contract status != Deactivated & product offering is not in enterprise_mail_po
                            $this->error_message['3022'][] = 'Standard-business-state is not applicable for the bundled product';
                        }elseif((isset($sugar_config['workflow_flag'][$contract->product_offering]) && $sugar_config['workflow_flag'][$contract->product_offering]=='Yes' && !in_array($contract->product_offering, $sugar_config['enterprise_mail_po']) && $contract_status == 2) || ((!isset($sugar_config['workflow_flag'][$contract->product_offering]) || (isset($sugar_config['workflow_flag'][$contract->product_offering]) && $sugar_config['workflow_flag'][$contract->product_offering]=='No')) && $contract_status == 5)){
                            $this->error_message['3022'][] = 'Standard-business-state is not applicable for the bundled product';
                        }
                    }
                    
					// Begin : DF-1870 
					
					$obj_timezone = $contract_startdate_timezone = $contract_startdate_tmp = '';
					
					if(!empty($contract->contract_startdate)) 
					{
						if ($contract->gc_timezone_id_c != '') 
						{
							$obj_timezone       = BeanFactory::getBean('gc_TimeZone', $contract->gc_timezone_id_c);
							$contract_startdate_timezone = $obj_timezone->utcoffset;
						} 
						if(trim($contract_startdate_timezone) == '') 
						{
							$contract_startdate_timezone = $sugar_config['gcm_cstm_const']['timezone']['gmt'];
						}
						$contract_startdate_tmp = new DateTime($contract->contract_startdate, new DateTimeZone($contract_startdate_timezone));
						$contract_startdate_tmp = $contract_startdate_tmp->format('c');
						$contract_startdate_tmp = $modUtils->ConvertDatetoGMTDate($contract_startdate_tmp);
						$contract->contract_startdate = $contract_startdate_tmp['date'];
					}
					
					if(isset($contract_header['contract_enddate']) && $contract_header['contract_enddate']!='')
					{
						$contract_enddate_timezone = !empty($contract_header['contract_enddate_timezone']) ? $contract_header['contract_enddate_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
						$contract_enddate_tmp = new DateTime($contract_header['contract_enddate'], new DateTimeZone($contract_enddate_timezone));
						$contract_enddate_tmp = $contract_enddate_tmp->format('c');
						$contract_enddate_tmp = $modUtils->ConvertDatetoGMTDate($contract_enddate_tmp);
						$contract_header['contract_enddate'] = $contract_enddate_tmp['date'];
					}
					else
					{
						$contract_header['contract_enddate'] = date('Y-m-d H:i:s');
					}
					
					// End : DF-1870 
					
                    if(empty($contract->contract_startdate) && ($contract_status==2 || (isset($contract_header['contract_enddate']) && $contract_header['contract_enddate']!=''))){
                    	$this->error_message['3020'][] = 'Contract Start Date is mandatory';
                    }
                    if(!empty($contract->contract_startdate) && strtotime($contract->contract_startdate) > strtotime($contract_header['contract_enddate'])){
                        $this->error_message['3020'][] = 'Contract End Date should be greater than Contract Start Date';
                    }
                    $this->validateBillingAffiliate($xml_arr['ContractDetails']);
                    $this->validatePaymentDetails($xml_arr['ContractDetails']['ContractLineItemDetails']);
                    $reconstruct_line_item=array();

                    $CustomFunctionGcContractObj = new CustomFunctionGcContract();
                    $prev_line_item_headers = (isset($contract->id) && $contract->id!='')?$CustomFunctionGcContractObj->getLineItemHeader($contract->id,0,'',false):array();
                    $line_item_header = $this->constructLineItemHeader($prev_line_item_headers, $xml_arr['ContractDetails']['ContractLineItemDetails']['ContractLineItemHeader'], $contract);
                    if(!empty($line_item_header['contract_header'])){
    					foreach($line_item_header['contract_header'] as $key => $val){
    						$xml_arr['ContractDetails']['ContractsHeader'][$key] = $val;
    					}
                    }
                    foreach($contract_header as $key => $val){
                        if(strpos('_xml_id',$key)!==false){
                            $fieldname = strreplace('_xml_id','_uuid',$key);
                            if($val != $contract->$fieldname){
                                $this->error_message['3020'][] = $val.' - Reference object id is not exist';
                            }
                        }
                    }
                    if(!empty($line_item_header['uuid_not_in_db'])){
                        foreach($line_item_header['uuid_not_in_db'] as $fieldname => $value){
                            foreach($value as $uuid){
                                $this->error_message['3020'][] = $uuid.' - Reference object id is not exist';
                            }
                        }
                        unset($line_item_header);
                    }else{
                        $index = 0;
                        $loopstart=0;
                        $tmp_one_stop_field_values = $one_stop_discrepency = array();
                        $one_stop_array = array(
                            'cust_contract_currency' => 'Customer Contract Currency',
                            'cust_billing_currency' => 'Customer Billing Currency',
                            'igc_contract_currency' => 'IGC Contract Currency',
                            'igc_billing_currency' => 'IGC Billing Currency',
                            'customer_billing_method' => 'Customer Billing Method',
                            'igc_settlement_method' => 'IGC Settlement Method'
                        );
                        if(!empty($prev_line_item_headers)){
                            foreach($prev_line_item_headers as $key => $prev_line_item_header){
                                if($key=='order'){
                                    continue;
                                }
                                if(!isset($line_item_header['line_item_header'][$key])){
                                    $line_item_header['line_item_header'][$key]['ContractLineItemHeader'] = array();
                                }
                                $lineitem = $line_item_header['line_item_header'][$key]['ContractLineItemHeader'];
                                $service_start_date = $service_start_time_zone = $service_end_date = $service_end_time_zone = $billing_start_date = $billing_start_time_zone = $billing_end_date = $billing_end_time_zone = $service_start_date_tmp = $service_end_date_tmp = $billing_start_date_tmp = $billing_end_date_tmp = '';
                                //service date validation
                                if(isset($lineitem['service_start_date'])){
                                    $service_start_date = $lineitem['service_start_date'];
                                    $service_start_time_zone = (isset($lineitem['service_start_date_timezone']) && $lineitem['service_start_date_timezone']!='') ? $lineitem['service_start_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
									// Begin : DF-1870
									$service_start_date_tmp = new DateTime($service_start_date, new DateTimeZone($service_start_time_zone));
									$service_start_date_tmp = $service_start_date_tmp->format('c');
									$service_start_date_tmp = $modUtils->ConvertDatetoGMTDate($service_start_date_tmp);
									$service_start_date = $service_start_date_tmp['date'];
									// End : DF-1870
                                }else if(isset($prev_line_item_header['service_start_date']) && $prev_line_item_header['service_start_date']!=''){
                                    $service_start_date = $prev_line_item_header['service_start_date'];
                                    $service_start_time_zone = (!empty($prev_line_item_header['service_start_date_timezone_actual'])) ? $prev_line_item_header['service_start_date_timezone_actual'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                                    $service_start_date_tmp = new DateTime($service_start_date, new DateTimeZone($service_start_time_zone));
                                    $service_start_date_tmp = $service_start_date_tmp->format('c');
                                    $service_start_date_tmp = $modUtils->ConvertDatetoGMTDate($service_start_date_tmp);
                                    $service_start_date = $service_start_date_tmp['date'];
                                }
                                $service_flag = true;
                                if(isset($lineitem['service_end_date'])){
                                	$service_flag = false;
                                    $service_end_date = $lineitem['service_end_date'];
                                    $service_end_time_zone = (isset($lineitem['service_end_date_timezone']) && $lineitem['service_end_date_timezone']!='') ? $lineitem['service_end_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
									// Begin : DF-1870
									$service_end_date_tmp = new DateTime($service_end_date, new DateTimeZone($service_end_time_zone));
									$service_end_date_tmp = $service_end_date_tmp->format('c');
									$service_end_date_tmp = $modUtils->ConvertDatetoGMTDate($service_end_date_tmp);
									$service_end_date = $service_end_date_tmp['date'];
									// End : DF-1870
                                }else if(isset($prev_line_item_header['service_end_date']) && $prev_line_item_header['service_end_date']!=''){
                                    $service_end_date = $prev_line_item_header['service_end_date'];
                                    $service_end_time_zone = (!empty($prev_line_item_header['service_end_date_timezone_actual'])) ? $prev_line_item_header['service_end_date_timezone_actual'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                                    $service_end_date_tmp = new DateTime($service_end_date, new DateTimeZone($service_end_time_zone));
                                    $service_end_date_tmp = $service_end_date_tmp->format('c');
                                    $service_end_date_tmp = $modUtils->ConvertDatetoGMTDate($service_end_date_tmp);
                                    $service_end_date = $service_end_date_tmp['date'];
                                }
                                if ($service_end_date =='' || ($service_flag && strtotime($service_end_date) > strtotime(date('Y-m-d H:i:s')))){
                                    $service_end_date = date('Y-m-d H:i:s');
                                }
                                if($service_start_date=='' && $contract_status!=2 && isset($lineitem['service_end_date'])){
                                	$this->error_message['3020'][] = 'Global Contract Item ID "'.$prev_line_item_header['global_contract_item_id'].'" - Service Start Date is mandatory';
                                }
                                if($service_start_date!='' && $service_end_date!=''){    
                                    if(strtotime($service_start_date) > strtotime($service_end_date)){
                                        $this->error_message['3020'][] = 'Global Contract Item ID "'.$prev_line_item_header['global_contract_item_id'].'" - Service End Date should be greater than Service Start Date';
                                    }
                                }
                                //billing date validation
                                if(isset($lineitem['billing_start_date'])){
                                    $billing_start_date = $lineitem['billing_start_date'];
                                    $billing_start_time_zone = (isset($lineitem['billing_start_date_timezone']) && $lineitem['billing_start_date_timezone']!='') ? $lineitem['billing_start_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
									// Begin : DF-1870 
									$billing_start_date_tmp = new DateTime($billing_start_date, new DateTimeZone($billing_start_time_zone));
									$billing_start_date_tmp = $billing_start_date_tmp->format('c');
									$billing_start_date_tmp = $modUtils->ConvertDatetoGMTDate($billing_start_date_tmp);
									$billing_start_date = $billing_start_date_tmp['date'];
									// End : DF-1870
                                }else if(isset($prev_line_item_header['billing_start_date']) && $prev_line_item_header['billing_start_date']!=''){
                                    $billing_start_date = $prev_line_item_header['billing_start_date'];
                                    $billing_start_time_zone = (!empty($prev_line_item_header['billing_start_date_timezone_actual'])) ? $prev_line_item_header['billing_start_date_timezone_actual'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                                    $billing_start_date_tmp = new DateTime($billing_start_date, new DateTimeZone($billing_start_time_zone));
                                    $billing_start_date_tmp = $billing_start_date_tmp->format('c');
                                    $billing_start_date_tmp = $modUtils->ConvertDatetoGMTDate($billing_start_date_tmp);
                                    $billing_start_date = $billing_start_date_tmp['date'];
                                }
                                $billing_flag = true;
                                if(isset($lineitem['billing_end_date'])){
                                	$billing_flag = false;
                                    $billing_end_date = $lineitem['billing_end_date'];
									// Begin : DF-1870 
									$billing_end_time_zone = (isset($lineitem['billing_end_date_timezone']) && $lineitem['billing_end_date_timezone']!='') ? $lineitem['billing_end_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
									$billing_end_date_tmp = new DateTime($billing_end_date, new DateTimeZone($billing_end_time_zone));
									$billing_end_date_tmp = $billing_end_date_tmp->format('c');
									$billing_end_date_tmp = $modUtils->ConvertDatetoGMTDate($billing_end_date_tmp);
									$billing_end_date = $billing_end_date_tmp['date'];
									// End : DF-1870
                                }else if(isset($prev_line_item_header['billing_end_date']) && $prev_line_item_header['billing_end_date']!=''){
                                    $billing_end_date = $prev_line_item_header['billing_end_date'];
                                    $billing_end_time_zone = (!empty($prev_line_item_header['billing_end_date_timezone_actual'])) ? $prev_line_item_header['billing_end_date_timezone_actual'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                                    $billing_end_date_tmp = new DateTime($billing_end_date, new DateTimeZone($billing_end_time_zone));
                                    $billing_end_date_tmp = $billing_end_date_tmp->format('c');
                                    $billing_end_date_tmp = $modUtils->ConvertDatetoGMTDate($billing_end_date_tmp);
                                    $billing_end_date = $billing_end_date_tmp['date'];
                                }
                                if($billing_end_date=='' || ($billing_flag && strtotime($billing_end_date) > strtotime(date('Y-m-d H:i:s')))){
                                    $billing_end_date = date('Y-m-d H:i:s');
                                }      
                                if($billing_start_date=='' && $contract_status!=2 && isset($lineitem['billing_end_date'])){
                                	$this->error_message['3020'][] = 'Global Contract Item ID "'.$prev_line_item_header['global_contract_item_id'].'" - Billing Start Date is mandatory';
                                }
                                if($billing_start_date!='' && $billing_end_date!=''){                                
                                    if(strtotime($billing_start_date) > strtotime($billing_end_date)){
                                        $this->error_message['3020'][] = 'Global Contract Item ID "'.$prev_line_item_header['global_contract_item_id'].'" - Billing End Date should be greater than Billing Start Date';
                                    }
                                }
                                //one stop validation
                                foreach($one_stop_array as $one_stop_key => $one_stop_value){
                                    if($loopstart){
                                        if((!isset($tmp_one_stop_field_values[$one_stop_key]) && isset($lineitem[$one_stop_key])) || (isset($tmp_one_stop_field_values[$one_stop_key]) && !isset($lineitem[$one_stop_key])) || ($tmp_one_stop_field_values[$one_stop_key] != $lineitem[$one_stop_key])){
                                            // set one stop error
                                            if(!in_array($one_stop_key, $one_stop_discrepency)){
                                                $this->error_message['3020'][] = $one_stop_value.' - One stop dependency validation failed';
                                            }
                                            array_push($one_stop_discrepency,$one_stop_key);
                                        }
                                    }else if(isset($lineitem[$one_stop_key])){
                                        $tmp_one_stop_field_values[$one_stop_key] = $lineitem[$one_stop_key];
                                    }
                                }
                                $reconstruct_line_item[$index]['ContractLineItemHeader'] = $lineitem;
                                $reconstruct_line_item[$index]['ContractLineItemHeader']['line_item_id_xml_id'] = $prev_line_item_header['line_item_id_uuid'];
                                //price
                                if(isset($lineitem['cust_contract_currency'])){
                                    $reconstruct_line_item[$index]['ContractLineItemHeader']['cust_contract_currency_flag'] = 0;
                                    if($lineitem['cust_contract_currency'] != $prev_line_item_header['cust_contract_currency_code']){
                                        $reconstruct_line_item[$index]['ContractLineItemHeader']['cust_contract_currency_flag'] = 1;
                                        // Price array construction function
                                        $price_details = $this->getPMSPriceDetails($contract->product_offering, $prev_line_item_header['product_specification_id'], $lineitem['cust_contract_currency'],$prev_line_item_header['product_config']);
                                        if($price_details !== false) //need to handle false condition
                                            $reconstruct_line_item[$index]['ContractLineProductPrice'] = $price_details;
                                    }
                                }
                                if(isset($line_item_header['line_item_header'][$key]['InvoiceGroupDtls'])){
                                    $reconstruct_line_item[$index]['InvoiceGroupDtls'] = $line_item_header['line_item_header'][$key]['InvoiceGroupDtls'];
                                }
                                unset($lineitem);
                                //payment
                                if(isset($contract_header['payment_type'])){
                                    $reconstruct_line_item[$index]['ContractLineItemHeader']['payment_type'] = $contract_header['payment_type'];
                                    $reconstruct_line_item[$index]['ContractLineItemHeader']['payment_type_flag'] = 0;
                                    if($contract_header['payment_type'] != $prev_line_item_header['payment_type_id']){
                                        $reconstruct_line_item[$index]['ContractLineItemHeader']['payment_type_flag'] = 1;
                                    }
                                    switch ($contract_header['payment_type']) {
                                        case self::DIRECT_DEPOSIT:
                                            $reconstruct_line_item[$index]['ContractDirectDeposit'] = $xml_arr['ContractDetails']['ContractLineItemDetails']['ContractDirectDeposit'];
                                            break;

                                        case self::ACCOUNT_TRANSFER:
                                            $reconstruct_line_item[$index]['ContractAccountTransfer'] = $xml_arr['ContractDetails']['ContractLineItemDetails']['ContractAccountTransfer'];
                                            break;

                                        case self::POSTAL_TRANSFER:
                                            $reconstruct_line_item[$index]['ContractPostalTransfer'] = $xml_arr['ContractDetails']['ContractLineItemDetails']['ContractPostalTransfer'];
                                            break;

                                        case self::CREDIT_CARD:
                                            $reconstruct_line_item[$index]['ContractCreditCard'] = $xml_arr['ContractDetails']['ContractLineItemDetails']['ContractCreditCard'];
                                            break;
                                        default:
                                            // Do nothing
                                            break;
                                    }
                                }
                                $loopstart = 1;
                                $index++;
                            }
                        }
                    }
    				$xml_arr['ContractDetails']['ContractLineItemDetails'] = $reconstruct_line_item;
    				unset($reconstruct_line_item);
                    //sprint 27 end

    				if(isset($xml_arr['Accounts']) && !empty($xml_arr['Accounts'])){
    				    //Get the country list. It will use to validate the country in request
    				    $country_codes = array();
    				    $country_bean = $obj_timezone = BeanFactory::getBean('c_country');
    				    $country_list = $country_bean->get_full_list('','');
    				    if(is_array($country_list)){
    				        foreach($country_list as $country){
    				            $country_codes[] = $country->country_code_numeric;
    				        }
    				    }
    				    foreach($xml_arr['Accounts'] as $account_type =>$account_details){
    				        $account_id = '';
    				        //Get the account id based on the account type
    				        if($account_type == 'Contract'){
    				            $account_id = $contract->contacts_gc_contracts_1contacts_ida;
    				        }else{
    				            $where_condition_line_items = " gc_line_item_contract_history.contracts_id = '" . $contract->id . "' ";
    				            $obj_array_line_contract_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History')->get_list('gc_line_item_contract_history.id', $where_condition_line_items, null, 1);
    				            if(!empty($obj_array_line_contract_history_bean['list'])){
    				                foreach ($obj_array_line_contract_history_bean['list'] as $line_contract_bean) {
    				                    $tech_account_id = $line_contract_bean->tech_account_id;
    				                    $bill_account_id = $line_contract_bean->bill_account_id;
    				                    continue;
    				                }
    				            }
    				            if($account_type == 'Technology'){
    				                $account_id = $tech_account_id;
    				            }
    				            elseif($account_type == 'Billing'){
    				                $account_id = $bill_account_id;
    				            }

    				        }
    				        $obj_contract_account_bean = BeanFactory::getBean('Contacts');
    				        if($account_id !=''){
    				            $obj_contract_account_bean->retrieve($account_id);
    				        }

    				        //This block is to prevent both last_name and name_2_c are being empty if we are saving it.
    				        //if both last_name and name_2_c is specified and both are empty then through this error
    				        if(isset($account_details['last_name']) && isset($account_details['name_2_c']) && trim($account_details['last_name'])=='' && trim($account_details['name_2_c'])==''){
    				            $this->error_message['3015'][] = $account_type.' - Contact Person Name is mandatory';
    				        }
    				        else{
    				            //if this account is not empty in the existing contract
    				            if(!empty($account_id) && isset($obj_contract_account_bean->id))
    				            {

    				                //this block is to avoid appending same error message for same error code again.
    				                //if either one is present and it's value is empty
    				                if((isset($account_details['last_name']) && !isset($account_details['name_2_c']) && $account_details['last_name']=='' && $obj_contract_account_bean->name_2_c == '') || (isset($account_details['name_2_c']) && !isset($account_details['last_name']) && $account_details['name_2_c']=='' && $obj_contract_account_bean->last_name == '')){
    				                    $this->error_message['3015'][] = $account_type.' - Contact Person Name is mandatory';
    				                }
    				            }
    				            else{
    				                //if not at all present or either one name is present and if that also empty in the request
    				                if((!isset($account_details['last_name']) && !isset($account_details['name_2_c']) ) ||  (isset($account_details['last_name']) && !isset($account_details['name_2_c']) && $account_details['last_name']=='') || (isset($account_details['name_2_c']) && !isset($account_details['last_name']) && $account_details['name_2_c']=='')){
    				                    $this->error_message['3015'][] = $account_type.' - Contact Person Name is mandatory';
    				                }
    				            }
    				        }

    				        //if no account is there in the db and corporate details is missing the xml then throw this error
    				        if(empty($account_id) && !isset($account_details['CorporateDetails']) ){
    				            $this->error_message['3017'][] = $account_type.' - Corporation ID or Corporation Name (either Local or Latin) is mandatory.';
    				        }

    				        //validate the corporate name details if CCID is empty
    				        if(isset($account_details['CorporateDetails']) && (!isset($account_details['CorporateDetails']['common_customer_id_c']) || $account_details['CorporateDetails']['common_customer_id_c']=='')){
    				            $corporate_details = $account_details['CorporateDetails'];
    				            //if both name and name_2_c is specified and both are empty then through this error
    				            if(isset($corporate_details['name']) && isset($corporate_details['name_2_c']) && trim($corporate_details['name'])=='' && trim($corporate_details['name_2_c'])==''){
    				                $this->error_message['3017'][] = $account_type.' - Corporation ID or Corporation Name (either Local or Latin) is mandatory.';
    				            }
    				            else{

    				                //validate the corporate name details from the db
    				                if(isset($obj_contract_account_bean->account_id) && $obj_contract_account_bean->account_id!=''){
    				                    $obj_corporate_bean = BeanFactory::getBean('Accounts');
    				                    $obj_corporate_bean ->retrieve($obj_contract_account_bean->account_id);

    				                    //if either one is present and it's value is empty
    				                    if((isset($corporate_details['name']) && !isset($corporate_details['name_2_c']) && $corporate_details['name']=='' && $obj_corporate_bean->name_2_c == '') || (isset($corporate_details['name_2_c']) && !isset($corporate_details['name']) && $corporate_details['name_2_c']=='' && $obj_corporate_bean->name == '')){
    				                        $this->error_message['3017'][] = $account_type.' - Corporation ID or Corporation Name (either Local or Latin) is mandatory.';
    				                    }
    				                }
    				                else{
    				                    //If ccid not present in the xml then check the corporate details
    				                    if(!isset($account['CorporateDetails']['common_customer_id_c'])){
    				                        //if not at all present or either one name is present and if that also empty in the request
    				                        if( (!isset($corporate_details['name']) && !isset($corporate_details['name_2_c'])) || (isset($corporate_details['name']) && !isset($corporate_details['name_2_c']) && $corporate_details['name']=='') || (isset($corporate_details['name_2_c']) && !isset($corporate_details['name']) && $corporate_details['name_2_c']=='')){
    				                            $this->error_message['3017'][] = $account_type.' - Corporation ID or Corporation Name (either Local or Latin) is mandatory.';
    				                        }
    				                    }
    				                }
    				            }
    				        }


    				        $country_code_numeric = isset($account_details['country_code_numeric'])?$account_details['country_code_numeric']:'';
    				        // if country code is not sent in the xml then set the country stored in the db
    				        if(!isset($account_details['country_code_numeric']) || trim($account_details['country_code_numeric'] == '') && (isset($obj_contract_account_bean->c_country_id_c) && $obj_contract_account_bean->c_country_id_c!='')){
    				            $prev_country_bean = BeanFactory::getBean('c_country', $obj_contract_account_bean->c_country_id_c);
    				            $country_code_numeric = $prev_country_bean->country_code_numeric;
    				            $account_details['country_code_numeric'] = $country_code_numeric;
    				        }

    				        //Check if the country tag name and address block is mismatch
    				        if(isset($account_details['address_tag_name']) && trim($account_details['address_tag_name'])!=''){
    				            switch ($country_code_numeric) {
    				                case '840':
    				                    $expected_addr_tag_name = 'american';
    				                    break;
    				                case '124':
    				                    $expected_addr_tag_name = 'canadian';
    				                    break;
    				                case '392':
    				                    $expected_addr_tag_name = 'japanese';
    				                    break;
    				                default:
    				                    $expected_addr_tag_name = 'any';
    				                    break;
    				            }
    				            if(trim(strtolower($account_details['address_tag_name'])) != $expected_addr_tag_name){
    				                $this->error_message['3018'][] = $account_type.' Country code & Address block XML Mis-match';
    				            }

    				        }

    				        //Check if the country reference is valid and country-state combination is right
    				        if(trim($account_details['country_code_numeric']) != ''){
    				            if(!in_array($account_details['country_code_numeric'],$country_codes)){
    				                $this->error_message['3018'][] = $account_type.' Account Reference Country not exist';
    				            }
    				            if(isset($account_details['general_address']['state_general_code']) && $account_details['general_address']['state_general_code']!='' && !$this->checkCountryStateValidation($account_details['country_code_numeric'],$account_details['general_address']['state_general_code'])){
    				                $this->error_message['3018'][] = $account_type.' Account Country and State combination does not match';
    				            }
    				        }
    				        //DF-1066, DF-1069 DF-838 - Email validation added for PATCH
    				        if(isset($account_details['email1']) && trim($account_details['email1'])!='' && !SugarEmailAddress::isValidEmail($account_details['email1'])){
    				            $email_msg = '';
    				            if($account_type == 'Contract'){
    				                $email_msg = 'Contracting Account';
    				            }
    				            elseif($account_type == 'Billing'){
    				                $email_msg = 'Billing Account';
    				            }
    				            elseif($account_type == 'Technology'){
    				                $email_msg = 'Technology Account';
    				            }
    				            $this->error_message['3018'][] = $email_msg.' Email ('.$account_details['email1'].') is not valid';
    				        }
    				    }
    				}
    				//DF-1069 - Sales channel block needs to be updated by PATCH request
    				if(!empty($xml_arr['ContractDetails']['ContractSalesRepresentative']) && trim($xml_arr['ContractDetails']['ContractSalesRepresentative'][0]['sales_company']) !=''){
    				    $sales_companies = array();
    				    $sales_company_bean = BeanFactory::getBean('gc_NTTComGroupCompany');
    				    $sales_company_list = $sales_company_bean->get_full_list('','');
    				    if(is_array($sales_company_list)){
    				        foreach($sales_company_list as $sales_company){
    				            $sales_companies[] = $sales_company->name;
    				        }
    				    }
    				    if(!in_array($xml_arr['ContractDetails']['ContractSalesRepresentative'][0]['sales_company'],$sales_companies)){
    				        $this->error_message['3018'][] = 'Sales company not exist in the system';
    				    }

    				}
    				//DF-1069, DF-838 We need to validate the email
    				if(isset($xml_arr['ContractDetails']['ContractSalesRepresentative'][0]['email1']) && trim($xml_arr['ContractDetails']['ContractSalesRepresentative'][0]['email1'])!='' && !$this->modUtils->email_validation($xml_arr['ContractDetails']['ContractSalesRepresentative'][0]['email1'])){
    				    $this->error_message['3018'][] = 'Sales Representative Email ('.$xml_arr['ContractDetails']['ContractSalesRepresentative'][0]['email1'].') is not valid';
    				}
                }
            }
		}
        if(!empty($this->error_message)){
            $this->errors['codes'] = array_keys($this->error_message);
            $this->errors['messages'] = $this->error_message;
        }else{
            $this->errors['response'] = $xml_arr;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'errors: '.print_r($this->errors,true), '');
        return $this->errors;
    }

    /**
     * Method to check whether country and state code combination is correct or not
     * @param string $country_code
     * @param string $state_code
     * @return boolean true if correct, false if not
     */
    private function checkCountryStateValidation($country_code, $state_code){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'country_code: '.$country_code.' '.GCM_GL_LOG_VAR_SEPARATOR.' state_code: '.$state_code, 'Method to check whether country and state code combination is correct or not');
        /* BEGIN : validate country state combination for US and Canada */
        $obj_country_bean = BeanFactory::getBean('c_country');
        $obj_country_bean->retrieve_by_string_fields(array(
            'country_code_numeric' => $country_code));
        if ($country_code == '840' || $country_code == '124') {
            $obj_state_bean = BeanFactory::getBean('c_State');
            $obj_state_bean->retrieve_by_string_fields(array(
                'state_code' => $state_code));
            $obj_state_bean->retrieve($obj_state_bean->id);
            if ($obj_country_bean->id != $obj_state_bean->c_country_c_state_1c_country_ida) {
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'false', '');
                return false;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'true', '');
        return true;
        /* END : validate country state combination for US and Canada */
    }

    /**
     * Method to check if value is empty
     * @param string $fieldvalue
     * @param string $error_code
     * @return boolean true if value is empty, false if not
     */
    private function isEmptyField($fieldvalue, $error_code){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'fieldvalue: '.$fieldvalue.' '.GCM_GL_LOG_VAR_SEPARATOR.' error_code: '.$error_code, 'Method to check if value is empty');
        if(trim($fieldvalue)==''){
            if($error_code!=''){
                $this->error_message[$error_code] = '';
            }
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'true', '');
            return true;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'false', '');
        return false;
    }

    /**
     * Method to check if array is empty
     * @param array $fieldarray
     * @param string $error_code
     * @return boolean true if array is empty, false if not
     */
    private function isEmptyArray($fieldarray, $error_code){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'fieldarray: '.print_r($fieldarray,true).' '.GCM_GL_LOG_VAR_SEPARATOR.' error_code: '.$error_code, 'Method to check if array is empty');
        if(empty($fieldarray)){
            if($error_code!=''){
                $this->error_message[$error_code] = '';
            }
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'true', '');
            return true;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'false', '');
        return false;
    }

    /**
     * Method to format PMS ID with prefix zeros
     * @param string $pms_id
     * @param int $type
     * @return string PMS ID with prefix zeros
     */
    private function formatPMSID($pms_id, $type){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'pms_id: '.$pms_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' type: '.$type, 'Method to format PMS ID with prefix zeros');
        $prefix = '00000000-0000-0000-0000-';
        if($type == 2){
            $prefix = '00000001-0000-0000-0000-';
        }
        $return_val = $prefix.sprintf('%012s',$pms_id);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_val: '.$return_val, '');
        return $return_val;
    }
    /**
     * Method to check the condition and set the error if it is false
     * @param callable $condition valid php condition
     * @param string $error_code Error Code need to be set
     * @param string $error_msessage Error message need to be set
     * @return boolean true if condition passed, fase if condition failed
     * @author mohamed.siddiq
     * @since Aug 03, 2016
     */
    private function checkCondition($condition, $error_code, $error_msessage){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'condition: '.$condition.' '.GCM_GL_LOG_VAR_SEPARATOR.' error_code: '.$error_code.' '.GCM_GL_LOG_VAR_SEPARATOR.' error_msessage: '.$error_msessage, 'Method to check the condition and set the error if it is false');
        if($condition){
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'true', '');
            return true;
        }
        else{
            if($error_code != ''){
                $this->setError($error_code, $error_msessage);
            }
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'false', '');
            return false;
        }
    }

    /**
     * Method to set the error if any error found
     * @param string $error_code
     * @param string $error_msessage
     * @return none
     * @author mohamed.siddiq
     * @since Aug 03, 2016
     */
    private function setError($error_code, $error_msessage){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'error_code: '.$error_code.' '.GCM_GL_LOG_VAR_SEPARATOR.' error_msessage: '.$error_msessage.' '.GCM_GL_LOG_VAR_SEPARATOR.' error_msessage: '.$error_msessage, 'Method to set the error if any error found');
        if($error_code!=''){
            $this->error_message[$error_code][] = $error_msessage;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * Method to format the line item details as like the request array. So that we can compare the difference
     * @param array $line_item_header Array of requested line item
     * @return array Formatted line item array
     * @author mohamed.siddiq
     * @since Aug 03, 2016
     */
    private function formatContractLineItemDetails($line_item_header){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_item_header: '.print_r($line_item_header,true), 'Method to format the line item details as like the request array. So that we can compare the difference');
        $all_lineitems = array();
        if(!empty($line_item_header)){
            foreach ($line_item_header as $key => $value) {
                if($key!='order'){
                    $curr_lineitem = array();
                    //Contract header
                    $curr_lineitem['ContractLineItemHeader']['service_start_date'] = $value['service_start_date'];
                    $curr_lineitem['ContractLineItemHeader']['service_start_date_xml_id'] = $value['service_start_date_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['billing_start_date'] = $value['billing_start_date'];
                    $curr_lineitem['ContractLineItemHeader']['billing_start_date_xml_id'] = $value['billing_start_date_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['service_end_date'] = $value['service_end_date'];
                    $curr_lineitem['ContractLineItemHeader']['service_end_date_xml_id'] = $value['service_end_date_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['billing_end_date'] = $value['billing_end_date'];
                    $curr_lineitem['ContractLineItemHeader']['billing_end_date_xml_id'] = $value['billing_end_date_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['service_start_date_timezone'] = $this->modUtils->checkAndSetDefaultTimezone($value['service_start_date_timezone_actual']);
                    $curr_lineitem['ContractLineItemHeader']['billing_start_date_timezone'] = $this->modUtils->checkAndSetDefaultTimezone($value['billing_start_date_timezone_actual']);
                    $curr_lineitem['ContractLineItemHeader']['service_end_date_timezone'] = $this->modUtils->checkAndSetDefaultTimezone($value['service_end_date_timezone_actual']);
                    $curr_lineitem['ContractLineItemHeader']['billing_end_date_timezone'] = $this->modUtils->checkAndSetDefaultTimezone($value['billing_end_date_timezone_actual']);

                    $curr_lineitem['ContractLineItemHeader']['factory_reference_no'] = $value['factory_reference_no'];
                    $curr_lineitem['ContractLineItemHeader']['factory_reference_no_xml_id'] = $value['factory_reference_no_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['product_specification'] = $value['product_specification_id'];
                    $curr_lineitem['ContractLineItemHeader']['product_specification_xml_id'] = $value['product_specification_uuid'];


                    $curr_lineitem['ContractLineItemHeader']['cust_contract_currency'] = $value['cust_contract_currency_code'];
                    $curr_lineitem['ContractLineItemHeader']['cust_contract_currency_xml_id'] = $value['cust_contract_currency_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['cust_billing_currency'] = $value['cust_billing_currency_code'];
                    $curr_lineitem['ContractLineItemHeader']['cust_billing_currency_xml_id'] = $value['cust_billing_currency_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['igc_contract_currency'] = $value['igc_contract_currency_code'];
                    $curr_lineitem['ContractLineItemHeader']['igc_contract_currency_xml_id'] = $value['igc_contract_currency_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['igc_billing_currency'] = $value['igc_billing_currency_code'];
                    $curr_lineitem['ContractLineItemHeader']['igc_billing_currency_xml_id'] = $value['igc_billing_currency_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['customer_billing_method'] = $value['customer_billing_method_value'];
                    $curr_lineitem['ContractLineItemHeader']['customer_billing_method_xml_id'] = $value['customer_billing_method_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['igc_settlement_method'] = $value['igc_settlement_method_value'];
                    $curr_lineitem['ContractLineItemHeader']['igc_settlement_method_xml_id'] = $value['igc_settlement_method_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['description'] = $value['description'];
                    $curr_lineitem['ContractLineItemHeader']['description_xml_id'] = $value['description_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['contract_line_item_type'] = $value['contract_line_item_type'];
                    $curr_lineitem['ContractLineItemHeader']['line_item_id_xml_id'] = $value['line_item_id_uuid'];
                    $curr_lineitem['ContractLineItemHeader']['payment_type'] = $value['payment_type_id'];

                    //Invoice Group Details
                    $curr_lineitem['InvoiceGroupDtls']['name'] = $value['group_name'];
                    $curr_lineitem['InvoiceGroupDtls']['name_xml_id'] = $value['group_name_uuid'];

                    $curr_lineitem['InvoiceGroupDtls']['description'] = $value['group_description'];
                    $curr_lineitem['InvoiceGroupDtls']['description_xml_id'] = $value['group_description_uuid'];

                    //Contract Line Product Configuration
                    $all_li_prod_config = array();
                    if(isset($value['product_config']) && !empty($value['product_config'])){
                        foreach($value['product_config'] as $prod_config){
                            $lineitem_prod_config = array();
                            $lineitem_prod_config['name'] = $prod_config['name'];
                            $lineitem_prod_config['characteristic_value'] = $prod_config['characteristic_value'];
                            $lineitem_prod_config['characteristic_xml_id'] = $prod_config['characteristic_uuid'];
                            $all_li_prod_config[$prod_config['name']]= $lineitem_prod_config;

                        }
                    }
                    $curr_lineitem['ContractLineProductConfiguration'] = $all_li_prod_config;

                    //Contract Line Product Rule
                    $all_li_prod_rule = array();
                    if(isset($value['rules']) && !empty($value['rules'])){
                        foreach($value['rules'] as $prod_rule){
                            $lineitem_prod_rule = array();
                            $lineitem_prod_rule['name'] = $prod_rule['ruleid'];
                            $lineitem_prod_rule['description'] = $prod_rule['rule_value'];
                            $lineitem_prod_rule['rule_xml_id'] = $prod_rule['rule_uuid'];
                            $all_li_prod_rule[$prod_rule['ruleid']]= $lineitem_prod_rule;

                        }
                    }
                    $curr_lineitem['ContractLineProductRule'] = (!empty($all_li_prod_rule))?$all_li_prod_rule:'';

                    //Contract Line Product Price
                    $all_li_prod_price = array();
                    if(isset($value['product_price']) && !empty($value['product_price'])){
                        foreach($value['product_price'] as $prod_price){
                            $lineitem_prod_price = array();
                            $lineitem_prod_price['name'] = $prod_price['pricelineid'];
                            $lineitem_prod_price['charge_xml_id'] = $prod_price['charge_uuid'];


                            $lineitem_prod_price['xml_data']['currencyid']['xml_id'] = $prod_price['currencyid_uuid'];
                            $lineitem_prod_price['xml_data']['currencyid']['xml_value'] = $prod_price['currencyid'];

                            $lineitem_prod_price['xml_data']['icb_flag']['xml_id'] = $prod_price['icb_flag_uuid'];
                            $lineitem_prod_price['xml_data']['icb_flag']['xml_value'] = $prod_price['icb_flag_boolean'];

                            $lineitem_prod_price['xml_data']['charge_type']['xml_id'] = $prod_price['charge_type_uuid'];
                            $lineitem_prod_price['xml_data']['charge_type']['xml_value'] = $prod_price['charge_type'];

                            $lineitem_prod_price['xml_data']['charge_period']['xml_id'] = $prod_price['charge_period_uuid'];
                            $lineitem_prod_price['xml_data']['charge_period']['xml_value'] = $prod_price['charge_period'];

                            if($prod_price['icb_flag']==1){
                                $lineitem_prod_price['xml_data']['list_price']['xml_id'] = '';
                                $lineitem_prod_price['xml_data']['list_price']['xml_value'] = '';

                                $lineitem_prod_price['xml_data']['discount_rate']['xml_id'] = '';
                                $lineitem_prod_price['xml_data']['discount_rate']['xml_value'] = '';

                                $lineitem_prod_price['xml_data']['discount_amount']['xml_id'] = '';
                                $lineitem_prod_price['xml_data']['discount_amount']['xml_value'] = '';
                            }else{
                                $lineitem_prod_price['xml_data']['list_price']['xml_id'] = $prod_price['list_price_uuid'];
                                $lineitem_prod_price['xml_data']['list_price']['xml_value'] = $prod_price['list_price'];

                                $lineitem_prod_price['xml_data']['discount_rate']['xml_id'] = $prod_price['discount_rate_uuid'];
                                $lineitem_prod_price['xml_data']['discount_rate']['xml_value'] = ($prod_price['percent_discount_flag'])?$prod_price['discount']:'0'; //if it is percentage discount then set value here

                                $lineitem_prod_price['xml_data']['discount_amount']['xml_id'] = $prod_price['discount_amount_uuid'];
                                $lineitem_prod_price['xml_data']['discount_amount']['xml_value'] = (!$prod_price['percent_discount_flag'])?$prod_price['discount']:'0'; //if it is not percentage discount then set value here
                            }

                            $lineitem_prod_price['xml_data']['customer_contract_price']['xml_id'] = $prod_price['customer_contract_price_uuid'];
                            $lineitem_prod_price['xml_data']['customer_contract_price']['xml_value'] = $prod_price['charge_price'];

                            $lineitem_prod_price['xml_data']['igc_settlement_price']['xml_id'] = $prod_price['igc_settlement_price_uuid'];
                            $lineitem_prod_price['xml_data']['igc_settlement_price']['xml_value'] = $prod_price['igc_settlement_price'];
                            $all_li_prod_price[$prod_price['pricelineid']]= $lineitem_prod_price;

                        }
                    }
                    $curr_lineitem['ContractLineProductPrice'] = $all_li_prod_price;


                    //Payment details

                    if(isset($value['payment_type_id']) && $value['payment_type_id']!=''){
                        switch ($value['payment_type_id']) {
                            case self::DIRECT_DEPOSIT:
                                $curr_lineitem['ContractDirectDeposit']['bank_code'] = $value['deposit_deposit_details']['bank_code'];
                                $curr_lineitem['ContractDirectDeposit']['branch_code'] = $value['deposit_deposit_details']['branch_code'];
                                $curr_lineitem['ContractDirectDeposit']['deposit_type'] = $value['deposit_deposit_details']['deposit_type_value'];
                                $curr_lineitem['ContractDirectDeposit']['payee_contact_person_name_1'] = $value['deposit_deposit_details']['person_name_1'];
                                $curr_lineitem['ContractDirectDeposit']['payee_contact_person_name_2'] = $value['deposit_deposit_details']['person_name_2'];
                                $curr_lineitem['ContractDirectDeposit']['payee_email_address'] = $value['deposit_deposit_details']['payee_email_address'];
                                $curr_lineitem['ContractDirectDeposit']['remarks'] = $value['deposit_deposit_details']['remarks'];
                                $curr_lineitem['ContractDirectDeposit']['payee_tel_no'] = $value['deposit_deposit_details']['payee_tel_no'];
                                $curr_lineitem['ContractDirectDeposit']['account_no'] = $value['deposit_deposit_details']['account_no'];
                                $curr_lineitem['ContractDirectDeposit']['payment_type'] = $value['payment_type_id'];
                                break;

                            case self::ACCOUNT_TRANSFER:
                                $curr_lineitem['ContractAccountTransfer']['name'] = $value['acc_trans_details']['name'];
                                $curr_lineitem['ContractAccountTransfer']['bank_code'] = $value['acc_trans_details']['bank_code'];
                                $curr_lineitem['ContractAccountTransfer']['branch_code'] = $value['acc_trans_details']['branch_code'];
                                $curr_lineitem['ContractAccountTransfer']['deposit_type'] = $value['acc_trans_details']['deposit_type_value'];
                                $curr_lineitem['ContractAccountTransfer']['account_no'] = $value['acc_trans_details']['account_no'];
                                $curr_lineitem['ContractAccountTransfer']['account_no_display'] = $value['acc_trans_details']['account_no_display_value'];
                                $curr_lineitem['ContractAccountTransfer']['payment_type'] = $value['payment_type_id'];
                                break;

                            case self::POSTAL_TRANSFER:
                                $curr_lineitem['ContractPostalTransfer']['name'] = $value['postal_trans_details']['name'];
                                $curr_lineitem['ContractPostalTransfer']['postal_passbook_mark'] = $value['postal_trans_details']['postal_passbook_mark'];
                                $curr_lineitem['ContractPostalTransfer']['postal_passbook'] = $value['postal_trans_details']['postal_passbook'];
                                $curr_lineitem['ContractPostalTransfer']['postal_passbook_no_display'] = $value['postal_trans_details']['postal_passbook_no_display_value'];
                                $curr_lineitem['ContractPostalTransfer']['payment_type'] = $value['payment_type_id'];
                                break;

                            case self::CREDIT_CARD:
                                $curr_lineitem['ContractCreditCard']['credit_card_no'] = $value['credit_card_details']['credit_card_no'];
                                $curr_lineitem['ContractCreditCard']['expiration_date'] = $value['credit_card_details']['expiration_date'];
                                $curr_lineitem['ContractCreditCard']['payment_type'] = $value['payment_type_id'];
                                break;
                            default:
                                // Do nothing
                                break;
                        }
                    }
                    $all_lineitems[]=$curr_lineitem;
                }

            }

        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'all_lineitems: '.print_r($all_lineitems,true), '');
        return $all_lineitems;
    }

    /**
     * Method to do fetch the available line item details like product specification, specification uuid
     * @param array $line_items Available line item details
     * @return array list of available line item IDs and its details
     * @author mohamed.siddiq
     * @since  Aug 03, 2016
     */
    private function fetchAvailableLItemDetails($line_items){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_items: '.print_r($line_items,true), 'Method to do fetch the available line item details like product specification, specification uuid');
        $ret_arr = array();
        if(!empty($line_items)){
            foreach($line_items as $line_item){
                $ret_arr['product_specification_xml_ids'][$line_item['ContractLineItemHeader']['product_specification_xml_id']]=$line_item['ContractLineItemHeader']['product_specification_xml_id'];
                $ret_arr['product_specifications'][$line_item['ContractLineItemHeader']['product_specification']]=$line_item['ContractLineItemHeader']['product_specification'];
                $ret_arr['line_item_id_xml_ids'][$li_xml_id=$line_item['ContractLineItemHeader']['line_item_id_xml_id']]=$li_xml_id=$line_item['ContractLineItemHeader']['line_item_id_xml_id'];
                $ret_arr[$li_xml_id] = $line_item;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'ret_arr: '.print_r($ret_arr,true), '');
        return $ret_arr;
    }

    /**
     * Method to check all validation that related to line item
     * @param array $request_li_arr Array of requested line item
     * @param array $prev_li_arr Array of previous available line item
     * @param array $specification_det Array of specification available
     * @return none
     * @author mohamed.siddiq
     * @since Aug 03, 2016
     */
    private function validateLineItemDetails($request_li_arr, $prev_li_arr, $specification_det){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'request_li_arr: '.print_r($request_li_arr,true).' '.GCM_GL_LOG_VAR_SEPARATOR.' prev_li_arr: '.print_r($prev_li_arr,true).' '.GCM_GL_LOG_VAR_SEPARATOR.' specification_det: '.print_r($specification_det,true), 'Method to check all validation that related to line item');
        $are_all_li_removed = true;
        $are_all_base_li_removed = true;
        $prev_line_items_ids_arr = $prev_li_arr['line_item_id_xml_ids'];
        if(is_array($request_li_arr) && is_array($prev_li_arr) && !empty($request_li_arr) && !empty($prev_li_arr)){
            foreach($request_li_arr as $req_litem){
                if(isset($req_litem['ContractLineItemHeader']['product_specification']) && $req_litem['ContractLineItemHeader']['product_specification']!=''){
                    $pms_id = $this->formatPMSID($req_litem['ContractLineItemHeader']['product_specification'],1);

                    //Check bi-item key exist for the line item which is mandatory regardless of bi-action
                    if(!$this->checkCondition((isset($req_litem['ContractLineItemHeader']['line_item_id_xml_id']) && trim($req_litem['ContractLineItemHeader']['line_item_id_xml_id'])!=''), '3021', 'PMS ID "'.$pms_id.'" - bii-itme key should not be empty')){
                        continue;
                    }



                    //Check atomic product key is exist for the line item regardless of bi-action
                    $this->checkCondition((isset($req_litem['ContractLineItemHeader']['product_specification_xml_id']) && trim($req_litem['ContractLineItemHeader']['product_specification_xml_id'])!=''), '3021', 'PMS ID "'.$pms_id.'" - Atomic Product Key shoud not be empty');

                    $req_lint_item_id = $req_litem['ContractLineItemHeader']['line_item_id_xml_id'];
                    $req_specification_id = $req_litem['ContractLineItemHeader']['product_specification'];

                    //Check bi-action is not empty for all the line item
                    $this->checkCondition((trim($req_litem['ContractLineItemHeader']['bi_action'])!=''), '3021', 'PMS ID "'.$pms_id.'" -bi-action should not be empty');


                    //If any line item is not set as 'remove' then it measn there is at lease one line item available
                    if($are_all_li_removed && trim($req_litem['ContractLineItemHeader']['bi_action'])!=self::LI_REMOVE){
                        $are_all_li_removed = false;
                    }

                    //Check any base line item available without removing
                    if($are_all_base_li_removed && !empty($specification_det) && isset($specification_det[$req_specification_id]['spectypename']) && (strtolower(trim($specification_det[$req_specification_id]['spectypename'])) == 'base') && (trim($req_litem['ContractLineItemHeader']['bi_action'])!=self::LI_REMOVE)){
                        $are_all_base_li_removed = false;
                    }

                    //Check is all are no change line item
                    if(trim($req_litem['ContractLineItemHeader']['bi_action'])!=self::LI_NO_CHANGE){
                        $this->is_contract_changed = true;
                    }
                    if(trim($req_litem['is_new_record']) == '1'){
                        //If this is new line item then check whether bi-action is add.
                        $this->checkCondition((trim($req_litem['ContractLineItemHeader']['bi_action'])==self::LI_ADD), '3021', 'PMS ID "'.$pms_id.'" - bi-action should be \'add\' as it is a new line item');
                        continue;
                    }
                    else{
                        //If this not a new line item then check the bi-action is not add.
                        $this->checkCondition((trim($req_litem['ContractLineItemHeader']['bi_action'])!=self::LI_ADD), '3021', 'PMS ID "'.$pms_id.'" - bi-action should not be \'add\' for the existing line item');
                    }


                    //Below validations are applicable only for old line item


                    //Check Product Biitem Key is different from previous Version
                    if($this->checkCondition((in_array(trim($req_lint_item_id),$prev_li_arr['line_item_id_xml_ids'])), '3021', 'PMS ID "'.$pms_id.'" - Invalid Biitem')){

                        //Check Atomic Product Key is different from previous Version
                        $this->checkCondition((in_array(trim($req_litem['ContractLineItemHeader']['product_specification_xml_id']),$prev_li_arr['product_specification_xml_ids'])), '3021', 'PMS ID "'.$pms_id.'" - Product Specification Key shoud not be changed from previous version');

                        //Check Atomic Product Offering Key is different from previous Version
                        $this->checkCondition((in_array(trim($req_specification_id),$prev_li_arr['product_specifications'])), '3021', 'PMS ID "'.$pms_id.'" - Product Specification should not be changed from previous version');
                        //compare the line itmes and through the errors
                        $this->compareLineItems($req_litem, $prev_li_arr[$req_lint_item_id]);
                    }
                    //unset the found line item from previous ids. At final, if any line itme found in previous version, then it means some line item is missing
                    unset($prev_line_items_ids_arr[$req_lint_item_id]);

                }
            }
            //Check if at lease one line item available without removing it
            $this->checkCondition((!$are_all_li_removed), '3021', 'Cannot remove all biitem. Atleast one biitem should be exist.');
            //Check if at least one base line item available without removing it
            $this->checkCondition((!$are_all_base_li_removed), '3021', 'Cannot remove all base biitem. Atleast one biitem biitem should be exist.');

            //Check if existing line item is missing
            if($this->checkCondition((empty($prev_line_items_ids_arr)),'3021','Existing line item(s) missing. ('.implode(',',$prev_line_items_ids_arr).')'));

        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }
    /**
     * Method to compare two line item and return if there is any change available
     * @param array $request_li_arr Array of requested line item
     * @param array $prev_li_arr Array of previous available line item
     * @return array List of changes between two line items in array format
     * @author mohamed.siddiq
     * @since Aug 03, 2016
     */
    private function compareLineItems($request_line_item, $available_line_item){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'request_line_item: '.print_r($request_line_item,true).' '.GCM_GL_LOG_VAR_SEPARATOR.' available_line_item: '.print_r($available_line_item,true), 'Method to compare two line item and return if there is any change available');
        $pms_id = $this->formatPMSID($request_line_item['ContractLineItemHeader']['product_specification'],1);

        $req_bi_action = trim($request_line_item['ContractLineItemHeader']['bi_action']);

        //List of timezone related fields
        $timezonefields_arr = array('billing_start_date_timezone', 'billing_end_date_timezone', 'service_start_date_timezone', 'service_end_date_timezone');
        //if any timezonefield is set as 00:00, then it means timezone is not set. So make it as empty.
        foreach($timezonefields_arr as $timezonefield){
            if(isset($request_line_item['ContractLineItemHeader'][$timezonefield]) && $request_line_item['ContractLineItemHeader'][$timezonefield] == '00:00'){
                $request_line_item['ContractLineItemHeader'][$timezonefield] = '';
            }
        }

        $diff_in_request_line_item = $this->modUtils->array_diff_assoc_recursive($request_line_item, $available_line_item);
        //Interchange the array and compare here to fetch the changes from another array if it is needed in future
        $diff_in_avail_line_item = $this->modUtils->array_diff_assoc_recursive($available_line_item, $request_line_item);

        if(isset($diff_in_avail_line_item) && !empty($diff_in_avail_line_item))
        {
            $validation_parts =  array_keys($diff_in_avail_line_item);
            foreach($validation_parts as $validationpart){
                switch($validationpart){
                    case 'ContractLineItemHeader':
                    case 'InvoiceGroupDtls':
                        foreach($diff_in_avail_line_item[$validationpart] as $key => $val){
                            $tmpkey = str_replace('_xml_id','',$key);
                            if(strpos($key,'_xml_id')!==false && !isset($diff_in_avail_line_item[$validationpart][$tmpkey])){
                                unset($diff_in_avail_line_item[$validationpart][$key]);
                            }
                            $tmp2key = str_replace('_timezone','',$key);
                            if(strpos($key,'_timezone')!==false && !isset($diff_in_avail_line_item[$validationpart][$tmp2key])){
                                unset($diff_in_avail_line_item[$validationpart][$key]);
                            }
                        }
                        if(isset($diff_in_avail_line_item[$validationpart]['payment_type'])){
                            unset($diff_in_avail_line_item[$validationpart]['payment_type']);
                        }
                        if(empty($diff_in_avail_line_item[$validationpart]))
                            unset($diff_in_avail_line_item[$validationpart]);
                        break;
                    case 'ContractLineProductConfiguration':
                        foreach($diff_in_avail_line_item[$validationpart] as $key => $prod_configs){
                            if($prod_configs['characteristic_value']==''){
                                unset($diff_in_avail_line_item[$validationpart][$key]);
                            }
                        }
                        if(empty($diff_in_avail_line_item[$validationpart]))
                            unset($diff_in_avail_line_item[$validationpart]);
                            break;
                    case 'ContractLineProductRule':
                        foreach($diff_in_avail_line_item[$validationpart] as $key => $prod_rule){
                            if($prod_rule['rule_value']==''){
                                unset($diff_in_avail_line_item[$validationpart][$key]);
                            }
                        }
                        if(empty($diff_in_avail_line_item[$validationpart]))
                            unset($diff_in_avail_line_item[$validationpart]);
                        break;
                    case 'ContractLineProductPrice':
                        if(empty($diff_in_avail_line_item[$validationpart]))
                            unset($diff_in_avail_line_item[$validationpart]);
                        break;
                    case 'ContractDirectDeposit':
                    case 'ContractPostalTransfer':
                    case 'ContractAccountTransfer':
                    case 'ContractCreditCard':
                        unset($diff_in_avail_line_item[$validationpart]);
                        break;
                }
            }
        }

        //Removing the default expected difference
        if(isset($diff_in_request_line_item['is_new_line_item'])){
            unset($diff_in_request_line_item['is_new_line_item']);
        }
        if(isset($diff_in_request_line_item['standard_state'])){
            unset($diff_in_request_line_item['standard_state']);
        }
        if(isset($diff_in_request_line_item['object_id'])){
            unset($diff_in_request_line_item['object_id']);
        }
        if(isset($diff_in_request_line_item['object_version'])){
            unset($diff_in_request_line_item['object_version']);
        }
        if(isset($diff_in_request_line_item['ContractLineItems'])){
            unset($diff_in_request_line_item['ContractLineItems']);
        }
        if(isset($diff_in_request_line_item['ContractLineItemHeader'])){
            unset($diff_in_request_line_item['ContractLineItemHeader']['bi_action']);
            //exclude payment type check for bi-action
            if(isset($diff_in_request_line_item['ContractLineItemHeader']['payment_type'])){
                unset($diff_in_request_line_item['ContractLineItemHeader']['payment_type']);
            }
            //if the array is empty after removing bi-action then unset the ContractLineItemHeader also
            $diff_li_header = array_filter($diff_in_request_line_item['ContractLineItemHeader']);
            if(empty($diff_li_header)){
                unset($diff_in_request_line_item['ContractLineItemHeader']);
            }
        }
        if(isset($diff_in_request_line_item['ContractLineProductPrice'])){
            //Remove the is_new_record from all price array
            if(isset($diff_in_request_line_item['ContractLineProductPrice']) && !empty($diff_in_request_line_item['ContractLineProductPrice'])){
                foreach(array_keys($diff_in_request_line_item['ContractLineProductPrice']) as $key) {
                    unset($diff_in_request_line_item['ContractLineProductPrice'][$key]['is_new_record']);
                }
            }
            //if the array is empty after removing bi-action then unset the ContractLineProductPrice also
            $diff_li_item = array_filter($diff_in_request_line_item['ContractLineProductPrice']);
            if(empty($diff_li_item)){
                unset($diff_in_request_line_item['ContractLineProductPrice']);
            }
        }
        //Sprint 27 exclue payment type for bi-action check
        if(isset($diff_in_request_line_item['ContractDirectDeposit'])){
            unset($diff_in_request_line_item['ContractDirectDeposit']);
        }
        if(isset($diff_in_request_line_item['ContractPostalTransfer'])){
            unset($diff_in_request_line_item['ContractPostalTransfer']);
        }
        if(isset($diff_in_request_line_item['ContractAccountTransfer'])){
            unset($diff_in_request_line_item['ContractAccountTransfer']);
        }
        if(isset($diff_in_request_line_item['ContractCreditCard'])){
            unset($diff_in_request_line_item['ContractCreditCard']);
        }

        //Remove the empty values which means those tags are not present in the xml
        $diff_in_request_line_item = array_filter(array_map('array_filter', $diff_in_request_line_item));

        //This array will be having only changes or newly added fields in request array
        if(isset($diff_in_request_line_item) && !empty($diff_in_request_line_item))
        {
            $validation_parts =  array_keys($diff_in_request_line_item);
            foreach($validation_parts as $validationpart){
                switch($validationpart){
                    case 'ContractLineItemHeader':
                    case 'InvoiceGroupDtls':
                            foreach($diff_in_request_line_item[$validationpart] as $key => $value){
                                if($req_bi_action == self::LI_NO_CHANGE || $req_bi_action == self::LI_REMOVE){
                                    $this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES[$validationpart][$key].' should not be changed');
                                }
                                else{
                                    if($this->modUtils->validateUUID($value) && array_key_exists($key,$this->LI_MANDATORY_VERSIONUP_FIELDS[$validationpart])){
                                        $this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES[$validationpart][$key].' should not be changed');
                                    }
                                }
                            }
                        break;
                    case 'ContractLineProductConfiguration':
                    case 'ContractLineProductRule':
                            foreach($diff_in_request_line_item[$validationpart] as $key => $product_cofigs){
                                foreach($product_cofigs as $key1 => $product_config){
                                    if($key1 == 'is_new_record'){
                                        unset($diff_in_request_line_item[$validationpart][$key][$key1]);
                                        continue;
                                    }
                                    if($req_bi_action == self::LI_NO_CHANGE || $req_bi_action == self::LI_REMOVE){
                                        $this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES[$validationpart][$key1].' should not be changed');
                                    }
                                    else{
                                        if($this->modUtils->validateUUID($product_config) && array_key_exists($key1,$this->LI_MANDATORY_VERSIONUP_FIELDS[$validationpart])){
                                            $this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES[$validationpart][$key1].' should not be changed');

                                        }

                                    }

                                }
                                if(empty($diff_in_request_line_item[$validationpart][$key])){
                                    unset($diff_in_request_line_item[$validationpart][$key]);
                                }
                            }
                            if(empty($diff_in_request_line_item[$validationpart])){
                                unset($diff_in_request_line_item[$validationpart]);
                            }
                        break;
                    case 'ContractLineProductPrice':
                            foreach($diff_in_request_line_item['ContractLineProductPrice'] as $key => $product_prices){
                                foreach($product_prices as $key1 => $product_price){
                                    if($req_bi_action == self::LI_NO_CHANGE || $req_bi_action == self::LI_REMOVE){
                                        if($key1!='xml_data')
                                        {
                                            $this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES['ContractLineProductPrice'][$key1].' should not be changed');
                                        }
                                        else{
                                            foreach($product_price as $pkey => $pval){
                                                    foreach($pval as $pckey => $pcval){
                                                        $this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES['ContractLineProductPrice'][$pkey][$pckey].' should not be changed');
                                                    }

                                            }
                                        }
                                    }
                                    else{
                                        if($key1!='xml_data' && $this->modUtils->validateUUID($product_price) && array_key_exists($key1,$this->LI_MANDATORY_VERSIONUP_FIELDS['ContractLineProductPrice'])){
                                            $this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES['ContractLineProductPrice'][$key].' should not be changed');

                                        }
                                        else{
                                            foreach($product_price as $pkey => $paval){
                                                foreach($paval as $pckey => $pcval){
                                                    if($this->modUtils->validateUUID($pcval) && isset($this->LI_MANDATORY_VERSIONUP_FIELDS['ContractLineProductPrice']['xml_data'][$pkey][$pckey])){
                                                        $this->setError('3021', 'PMS ID "'.$pms_id.'" - '.$this->CONTRACT_FIELD_NAMES['ContractLineProductPrice'][$pkey][$pckey].' should not be changed');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        break;
                }

            }
        }

        //IF the bi-action is 'change' then there should be atlease one change
        if(trim($req_bi_action) == self::LI_CHANGE){
            $this->checkCondition((!empty($diff_in_request_line_item || !empty($diff_in_avail_line_item))), '3021', 'PMS ID "'.$pms_id.'" - There should be some change if bi-action is \'change\'');
        }

        //IF the bi-action is 'no change' or 'remove' then there should not be any change
        if(trim($req_bi_action) == self::LI_NO_CHANGE || trim($req_bi_action) == self::LI_REMOVE){
            $this->checkCondition((empty($diff_in_request_line_item) && empty($diff_in_avail_line_item)), '3021', 'PMS ID "'.$pms_id.'" - There should not be any change if bi-action is \'no change\' or \'remove\'');
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }
    /**
     * Method to check if Reference ID is valid
     * @param array $xml_array
     * @param sting $global_contract_id - Global Contract ID
     * @param string $global_contract_uuid - Global Contract ID UUID
     * @return boolean
     * @author Sathish Kumar Ganesan
     * @since Aug 11, 2016
     */
    private function validateReferenceID($xml_arr, $global_contract_id, $global_contract_uuid){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, ' xml_arr: '.print_r($xml_arr,true).' '.GCM_GL_LOG_VAR_SEPARATOR.' global_contract_id: '.$global_contract_id.' '.GCM_GL_LOG_VAR_SEPARATOR.' global_contract_uuid: '.$global_contract_uuid, 'Method to check if Reference ID is valid');
        $contract_header = !empty($xml_arr['ContractDetails']['ContractsHeader']) ? $xml_arr['ContractDetails']['ContractsHeader'] : array();
        if($contract_header['is_new_record'] && ($global_contract_id != '' || $global_contract_uuid != '')){
            $this->error_message['3021'][] = 'Contract Reference ID should not be empty';
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'false', '');
            return false;
        }elseif(!$contract_header['is_new_record'] && $global_contract_id == '' && $global_contract_uuid == ''){
            $this->error_message['3020'][] = 'Invalid Reference XML';
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'false', '');
            return false;
        }
        unset($xml_arr,$contract_header);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'true', '');
        return true;
    }

	/**
     * Method to formate the request contract line item details to compare
     * @param array $line_item_details
     * @return array with the name as key
     * @author mohamed.siddiq
     * @since Sep 20, 2016
     */
    private function formatRequestedContractLineItemDetails($line_item_details){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, ' line_item_details: '.print_r($line_item_details,true), 'Method to formate the request contract line item details to compare');
        if(isset($line_item_details) && is_array($line_item_details) && !empty($line_item_details)){
            foreach($line_item_details as $key => $req_litem){
                //Contract Line Product Configuration
                $all_li_prod_config = array();
                if(isset($req_litem['ContractLineProductConfiguration']) && !empty($req_litem['ContractLineProductConfiguration'])){
                    foreach($req_litem['ContractLineProductConfiguration'] as $key1 => $prod_config){
                        $line_item_details[$key]['ContractLineProductConfiguration'][$prod_config['name']] = $prod_config;
                        unset($line_item_details[$key]['ContractLineProductConfiguration'][$key1]);
                    }
                }

                //Contract Line Product Rule
                $all_li_prod_config = array();
                if(isset($req_litem['ContractLineProductRule']) && !empty($req_litem['ContractLineProductRule'])){
                    foreach($req_litem['ContractLineProductRule'] as $key2 => $prod_rule){
                        $line_item_details[$key]['ContractLineProductRule'][$prod_rule['name']] = $prod_rule;
                        unset($line_item_details[$key]['ContractLineProductRule'][$key2]);
                    }
                }

                //Contract Line Product Price
                $all_li_prod_config = array();
                if(isset($req_litem['ContractLineProductPrice']) && !empty($req_litem['ContractLineProductPrice'])){
                    foreach($req_litem['ContractLineProductPrice'] as $key3 => $prod_price){
                        $line_item_details[$key]['ContractLineProductPrice'][$prod_price['name']] = $prod_price;
                        unset($line_item_details[$key]['ContractLineProductPrice'][$key3]);
                    }
                }
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_item_details: '.print_r($line_item_details,true), '');
        return $line_item_details;
    }

    /**
     * Method to validate the business logic for GBS Browse
     * @param string $accountType Request Type (billing-account or contract-account)
     * @param string $dateAfter Start date of filter
     * @param string $dateBefore End date of filter
     * @return array error codes in array. Empty array if no errors
     * @author mohamed.siddiq
     * @since Nov 10, 2016
     */
    public function validateGBSBrowseRequestData($accountType, $dateAfter, $dateBefore){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, ' accountType: '.$accountType.' '.GCM_GL_LOG_VAR_SEPARATOR.' dateAfter: '.$dateAfter.' '.GCM_GL_LOG_VAR_SEPARATOR.' dateBefore: '.$dateBefore, 'Method to validate the business logic for GBS Browse');
        global $sugar_config;

        $date_valid_flag = true;
        //Validate account type
        if($accountType==''){
            $this->error_message['10004'][] = 'Type is mandatory in the request';
        }else{
            if(!in_array($accountType, array('billing-account','contract-account'))){
                $this->error_message['10004'][] = 'Invalid Request Type. It should be either "billing-account" or "contract-account"';
            }
        }
        //Validate date
        if(trim($dateAfter)=='' || trim($dateBefore)==''){
            $this->error_message['10004'][] = 'Date(s) are mandatory. (Both "date-after" and "date-before" are mandatory)';
        }

        if(trim($dateAfter)!=''){
            //Validate date-after field
            if(!$this->modUtils->validateDateFormat($dateAfter)){ //To check if the date is in valid format "yyyy-mm-dd"
                $this->error_message['10004'][] = 'Invalid Date Format. "date-after" should be in "YYYY-MM-DD" format';
                $date_valid_flag=false;
            }elseif(!$this->modUtils->validateDate($dateAfter)){ //Even though the date format is correct, given date should also be correct. Ex.2016-02-31
                $this->error_message['10004'][] = 'Invalid Date Combination. "date-after" is not a valid date';
                $date_valid_flag=false;
            }
        }

        if(trim($dateBefore)!=''){
            //Validate date-before field
            if(!$this->modUtils->validateDateFormat($dateBefore)){
                $this->error_message['10004'][] = 'Invalid Date Format. "date-before" should be in "YYYY-MM-DD" format';
                $date_valid_flag=false;
            }elseif(!$this->modUtils->validateDate($dateBefore)){
                $this->error_message['10004'][] = 'Invalid Date Combination. "date-before" is not a valid date';
                $date_valid_flag=false;
            }
        }

        //Validate the combination of dates
        if(trim($dateAfter)!='' && trim($dateBefore)!='' && $date_valid_flag && (strtotime($dateAfter)>strtotime($dateBefore))){
            $this->error_message['10004'][] = 'Invalid Date Combination. The value of "date-after" must be less than "date-before"';
        }

        //Check whether the product offering exists in the config
        if(!isset($sugar_config['gbs_api_po']) || empty($sugar_config['gbs_api_po'])){
            $this->error_message['10005'] = '';
        }else{
            $gbs_api_po = array_filter($sugar_config['gbs_api_po']);
            if(empty($gbs_api_po)){
              $this->error_message['10005'] = '';
            }
        }
        if(!empty($this->error_message)){
            $this->errors['codes'] = array_keys($this->error_message);
            $this->errors['messages'] = $this->error_message;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'errors: '.print_r($this->errors,true), '');
        return $this->errors;
    }

    /**
     * Method to validate the business logic for GBS Product Contract Browse
     * @param string $contractCustomerId
     * @param string $contractProductId
     * @param string $updatedAfter
     * @param string $contractStatus
     * @param boolean $versionFlag
     * @return array error codes in array. Empty array if no errors
     */
    public function validateProductContractBrowseRequestData($contractCustomerId, $contractProductId, $updatedAfter, $contractStatus, $versionFlag){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, ' contractCustomerId: '.$contractCustomerId.' '.GCM_GL_LOG_VAR_SEPARATOR.' contractProductId: '.$contractProductId.' '.GCM_GL_LOG_VAR_SEPARATOR.' updatedAfter: '.$updatedAfter.' '.GCM_GL_LOG_VAR_SEPARATOR.' contractStatus: '.$contractStatus.' '.GCM_GL_LOG_VAR_SEPARATOR.' versionFlag: '.$versionFlag, 'Method to validate the business logic for GBS Product Contract Browse');
        $versionFlag = !empty($versionFlag)?$versionFlag:false;
        global $sugar_config;

        if(trim($contractCustomerId)=='' && trim($contractProductId)=='' && trim($updatedAfter)==''){
            $this->error_message['3024'][] = 'At least one of the parameters contract-customer-id, contract-product-id and updated-after is mandatory';
        }
        if(trim($contractProductId)!=''){
            $cp_det = explode(':', $contractProductId);
            $pos = strpos($contractProductId, ':');
            if(count($cp_det) > 2){ //if it has more than one ":"
                $this->error_message['3024'][] = 'Invalid contract-product-id value';
            }
            elseif ($pos === false) { // if : is not found
                $this->error_message['3024'][] = 'Invalid contract-product-id value';
            }
            elseif($pos==false){ // if : found at first
                $this->error_message['3024'][] = 'Invalid contract-product-id value';
            }
            elseif($pos!==false && isset($cp_det[1]) && $cp_det[1]!=='1'){ //if colon is found & value is not 1
                $this->error_message['3024'][] = 'Invalid contract-product-id value';
            }
            elseif(isset($cp_det[0]) && !$this->modUtils->validatePMSIdPattern($cp_det[0])){ // if PMS id is not a valid pattern
                $this->error_message['3024'][] = 'Invalid contract-product-id value pattern';
            }
        }
        if(trim($updatedAfter)!=''){
            $updatedAfter =  str_replace(' ', '+', $updatedAfter);
            $date_res = $this->modUtils->ConvertDatetoGMTDate(trim($updatedAfter));
            if(isset($date_res['result']) && !$date_res['result']){
                if(isset($date_res['error_type']) && $date_res['error_type']==2){
                    $this->error_message['3024'][] = 'Updated-after timezone is not exist in the system';
                }
                else{
                    $this->error_message['3024'][] = 'Invalid updated-after value';
                }
            }
        }
        if(trim($contractStatus)!=''){
            $reqContractStatusList = explode(',', $contractStatus);
            $reqContractStatusList = array_filter($reqContractStatusList);
            if(empty($reqContractStatusList)){
                $this->error_message['3024'][] = 'Invalid status value';
            }
            else{
                $reqContractStatusList = array_map('trim',$reqContractStatusList);
                $contractStatusList = array_filter($GLOBALS['app_list_strings']['contract_status_list']);
                $wrong_status = array_diff($reqContractStatusList, $contractStatusList);
                if(!empty($wrong_status)){
                    $this->error_message['3024'][] = 'Invalid status value ('.implode(',', $wrong_status).')';
                }
            }
        }
        if(!empty($this->error_message)){
            $this->errors['codes'] = array_keys($this->error_message);
            $this->errors['messages'] = $this->error_message;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'errors: '.print_r($this->errors,true), '');
        return $this->errors;

    }

    /**
     * method to converte request_data datetime.
     *
     * @param array $request_data request data from api xml
     * @param array $request_data
     * @author Rajul.Mondal
     * @since 21-Mar-2017
     */
    public function processRequestDatetime($request_data = array())
    {
        // not passed $request_data in log cause it a huge data
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'method to converte request_data datetime');
        global $sugar_config;
        $modUtils = new ModUtils();

        // converting contract_start_date datetime => UTC time
        $contract_start_date = $request_data['ContractDetails']['ContractsHeader']['contract_startdate'];
        if (!empty($contract_start_date)) {
            $contract_startdate_timezone = !empty($request_data['ContractDetails']['ContractsHeader']['contract_startdate_timezone']) ? $request_data['ContractDetails']['ContractsHeader']['contract_startdate_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $contract_start_date         = new DateTime($contract_start_date, new DateTimeZone($contract_startdate_timezone));
            $contract_start_date         = $contract_start_date->format('c');
            $contract_start_date_tmp     = $modUtils->ConvertDatetoGMTDate($contract_start_date);

            $request_data['ContractDetails']['ContractsHeader']['contract_startdate'] = $contract_start_date_tmp['date'];
        }

        // converting contract_end_date datetime => UTC time
        $contract_end_date = $request_data['ContractDetails']['ContractsHeader']['contract_enddate'];
        if (!empty($contract_end_date)) {
            $contract_enddate_timezone = !empty($request_data['ContractDetails']['ContractsHeader']['contract_enddate_timezone']) ? $request_data['ContractDetails']['ContractsHeader']['contract_enddate_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $contract_end_date         = new DateTime($contract_end_date, new DateTimeZone($contract_enddate_timezone));
            $contract_end_date         = $contract_end_date->format('c');
            $contract_end_date_tmp     = $modUtils->ConvertDatetoGMTDate($contract_end_date);

            $request_data['ContractDetails']['ContractsHeader']['contract_enddate'] = $contract_end_date_tmp['date'];
        }

        if (!empty($request_data['ContractLineItemDetails'])) {
            foreach ($request_data['ContractLineItemDetails'] as $key => $request_line_item) {
                // line item service start date => UTC time
                $service_start_date = $request_line_item['ContractLineItemHeader']['service_start_date'];
                if (!empty($service_start_date)) {
                    $service_start_date_timezone = !empty($request_line_item['ContractLineItemHeader']['service_start_date_timezone']) ? $request_line_item['ContractLineItemHeader']['service_start_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                    $service_start_date          = new DateTime($service_start_date, new DateTimeZone($service_start_date_timezone));
                    $service_start_date          = $service_start_date->format('c');
                    $service_start_date_tmp      = $modUtils->ConvertDatetoGMTDate($service_start_date);

                    $request_data['ContractLineItemDetails'][$key]['ContractLineItemHeader']['service_start_date'] = $service_start_date_tmp['date'];
                }

                // line item service end date => UTC time
                $service_end_date = $request_line_item['ContractLineItemHeader']['service_end_date'];
                if (!empty($service_end_date)) {
                    $service_end_date_timezone = !empty($request_line_item['ContractLineItemHeader']['service_end_date_timezone']) ? $request_line_item['ContractLineItemHeader']['service_end_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                    $service_end_date          = new DateTime($service_end_date, new DateTimeZone($service_end_date_timezone));
                    $service_end_date          = $service_end_date->format('c');
                    $service_end_date_tmp      = $modUtils->ConvertDatetoGMTDate($service_end_date);

                    $request_data['ContractLineItemDetails'][$key]['ContractLineItemHeader']['service_end_date'] = $service_end_date_tmp['date'];
                }

                // line item billing start date => UTC time
                $billing_start_date = $request_line_item['ContractLineItemHeader']['billing_start_date'];
                if (!empty($billing_start_date)) {
                    $billing_start_date_timezone = !empty($request_line_item['ContractLineItemHeader']['billing_start_date_timezone']) ? $request_line_item['ContractLineItemHeader']['billing_start_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                    $billing_start_date          = new DateTime($billing_start_date, new DateTimeZone($billing_start_date_timezone));
                    $billing_start_date          = $billing_start_date->format('c');
                    $billing_start_date_tmp      = $modUtils->ConvertDatetoGMTDate($billing_start_date);

                    $request_data['ContractLineItemDetails'][$key]['ContractLineItemHeader']['billing_start_date'] = $billing_start_date_tmp['date'];
                }

                // line item billing end date => UTC time
                $billing_end_date = $request_line_item['ContractLineItemHeader']['billing_end_date'];
                if (!empty($billing_end_date)) {
                    $billing_end_date_timezone = !empty($request_line_item['ContractLineItemHeader']['billing_end_date_timezone']) ? $request_line_item['ContractLineItemHeader']['billing_end_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                    $billing_end_date          = new DateTime($billing_end_date, new DateTimeZone($billing_end_date_timezone));
                    $billing_end_date          = $billing_end_date->format('c');
                    $billing_end_date_tmp      = $modUtils->ConvertDatetoGMTDate($billing_end_date);

                    $request_data['ContractLineItemDetails'][$key]['ContractLineItemHeader']['billing_end_date'] = $billing_end_date_tmp['date'];
                }
            }
        }

        // not passed $request_data in log cause it a huge data
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return $request_data;
    }

    /**
     * method to construct Line Item Header Array for Save.
     *
     * @param array $db_array contract line item data from db
     * @param array $api_array line item data from api
     * @param string $contract_status
     * @return array $return_array contains reconstruct line item array and uuid list not exist in db
     * @author Sathish Kumar Ganesan
     * @since 05-May-2017
     */
    private function constructLineItemHeader($db_array, $api_array, $contract)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, ' db_array: '.print_r($db_array,true).' '.GCM_GL_LOG_VAR_SEPARATOR.' api_array: '.print_r($api_array,true).' '.GCM_GL_LOG_VAR_SEPARATOR.' contract: '.print_r($contract,true), 'method to construct Line Item Header Array for Save');
        unset($db_array['order']);
        $return_array = $line_item_header = $uuid_not_in_db = $contract_header = array();
        $index = 'ContractLineItemHeader';
        foreach($api_array as $field_name => $field_values){
            foreach($field_values['xml_id'] as $key => $val){
                $fieldname_key = $field_name;
                if($field_name == 'group_name' || $field_name == 'group_description'){
                    $index = 'InvoiceGroupDtls';
                    $fieldname_key = str_replace('group_','',$field_name);
                }else{
                    $index = 'ContractLineItemHeader';
                }
                if($field_name == 'factory_reference_no' && $val == $contract->factory_reference_no_uuid){
                    $contract_header['factory_reference_no'] = $field_values['val'][$key];
                    $contract_header['factory_reference_no_xml_id'] = $val;
                    continue;
                }
                foreach($db_array as $dbkey => $db_line_item){
                    if((isset($db_line_item[$field_name.'_uuid']) && $val == $db_line_item[$field_name.'_uuid']) || (strpos($field_name,'_timezone')!==false &&  $db_line_item[str_replace('_timezone','',$field_name).'_uuid'] == $val)){
                        $line_item_header[$dbkey][$index][$fieldname_key.'_xml_id'] = $val;
                        $line_item_header[$dbkey][$index][$fieldname_key] = $field_values['val'][$key];
                        continue 2;
                    }
                }
                if(strpos($field_name,'_timezone')==false)
                    $uuid_not_in_db[$field_name][] = $val;
            }
        }
        $return_array['contract_header'] = $contract_header;
        $return_array['line_item_header'] = $line_item_header;
        $return_array['uuid_not_in_db'] = $uuid_not_in_db;
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_array: '.print_r($return_array,true), '');
        return $return_array;
    }

    /**
     * method for get Product price details from PMS
     *
     * @param string $offeringId product offering id
     * @param string $specificationId product specification id
     * @param string $currency currency
     * @param array $selectedCharacteristic selected Characteristic ids
     * @return array price details as an array || return false if failed
     * @author Rajul.Mondal
     * @since May 05, 2017
     */
    private function getPMSPriceDetails($offeringId, $specificationId, $currency, $selectedCharacteristic)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, ' offeringId: '. $offeringId .' '.GCM_GL_LOG_VAR_SEPARATOR.' specificationId: '. $specificationId.' '.GCM_GL_LOG_VAR_SEPARATOR.' currency: '. $currency .' '.GCM_GL_LOG_VAR_SEPARATOR.' selectedCharacteristic: '. print_r($selectedCharacteristic, true), 'method for get Product price details from PMS');
        // validate if offeringId or specificationId
        if (empty($offeringId) || empty($specificationId)) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'validate if offeringId or specificationId');
            return false;
        }
        // is $selectedCharacteristic is empty
        if(empty($selectedCharacteristic)) {
            $selectedCharacteristic = array();
        }
        // $selectedCharacteristic must be array
        elseif (!is_array($selectedCharacteristic)) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'selectedCharacteristic must be array');
            return false;
        }

        require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';
        $objProduct    = new ClassProductConfiguration();
        $offering_list = $objProduct->getProductOfferings(true);

        // validate if there is error in PMS while fetch product offering
        if ($offering_list['response_code'] == '0') {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'validate if there is error in PMS while fetch product offering');
            return false;
        }
        // validate if offering list is empty
        elseif (empty($offering_list['respose_array'])) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'validate if offering list is empty');
            return false;
        }
        // proceed...
        else {
            $offeringList = $offering_list['response_code'] == '1' ? $offering_list['respose_array'] : array();
            // if product offering not exist in PMS product offering list
            if (!array_key_exists($offeringId, $offeringList)) {
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'if product offering not exist in PMS product offering list');
                return false;
            }

            // get product offering detials from the PMS
            $offeringDetails  = $objProduct->getProductOfferingDetails($offeringId);
            $offeringDetails  = $offeringDetails['response_code'] == '1' ? $offeringDetails['respose_array'] : array();
            $pmsChargeDetails = $offeringDetails['charge_details'];
            $pmsChargeMapping = $offeringDetails['charge_mapping'];
            $pmsChargeMapping = isset($pmsChargeMapping[$specificationId]) ? $pmsChargeMapping[$specificationId] : array();
            $pmsSpeciDetails  = isset($offeringDetails['specification_details'][$specificationId]) ? $offeringDetails['specification_details'][$specificationId] : array();

            // validate if offerdetails is empty for the product offering id
            if (empty($offeringDetails)) {
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'validate if offerdetails is empty for the product offering id');
                return false;
            }

            $pmsSpecifications = $offeringDetails['specification_details'];
            // validate if product specification id not exist in PMS specification details in PMS
            if (!array_key_exists($specificationId, $pmsSpecifications)) {
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'validate if product specification id not exist in PMS specification details in PMS');
                return false;
            }

            // extract selectedCharacteristic
            $tmpCharacteristic = array();
            foreach ($selectedCharacteristic as $characteristic) {
                $valtypeid = isset($pmsSpeciDetails['characteristics'][$characteristic['name']]['valtypeid']) ? $pmsSpeciDetails['characteristics'][$characteristic['name']]['valtypeid'] : '';
                if ($characteristic['characteristic_value'] != '' && $valtypeid != 2) {
                    $tmpCharacteristic[] = $characteristic['characteristic_value'];
                }
            }
            $selectedCharacteristic = $tmpCharacteristic;

            // array having price line id accouding to the curreny passed
            $pmsChargeIds = array();
            // proceed if PMS charge mapping is not empty
            if (!empty($pmsChargeMapping)) {
                foreach ($pmsChargeMapping as $mapping) {
                    // if no Contract Currency && no charge mapping charvalid
                    if (empty($currency) && empty($mapping['charvalid'])) {
                        $pmsChargeIds[] = $mapping['pricelineid'];
                    }
                    // if there is a Contract Currency && no charge mapping charvalid
                    else if (!empty($currency) && ($currency === $mapping['currency'] || $pmsChargeDetails[$mapping['pricelineid']]['isCustomPrice'] == 'true') && empty($mapping['charvalid'])) {
                        $pmsChargeIds[] = $mapping['pricelineid'];
                    }
                    // if no Contract Currency && filter selected characteristic
                    else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && empty($currency) && count($mapping['charvalid']) == 1) {
                        $pmsChargeIds[] = $mapping['pricelineid'];
                    }
                    // if there is a Contract Currency && filter selected characteristic
                    else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && !empty($currency) && ($currency === $mapping['currency'] || $pmsChargeDetails[$mapping['pricelineid']]['isCustomPrice'] == 'true') && count($mapping['charvalid']) == 1) {
                        $pmsChargeIds[] = $mapping['pricelineid'];
                    }
                    // if no Contract Currency && filter selected characteristic
                    else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && empty($currency) && count($mapping['charvalid']) == count($selectedCharacteristic)) {
                        $pmsChargeIds[] = $mapping['pricelineid'];
                    }
                    // if there is a Contract Currency && filter selected characteristic
                    else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && !empty($currency) && ($currency === $mapping['currency'] || $pmsChargeDetails[$mapping['pricelineid']]['isCustomPrice'] == 'true') && count($mapping['charvalid']) == count($selectedCharacteristic)) {
                        $pmsChargeIds[] = $mapping['pricelineid'];
                    }
                }
            }

            $priceDetails = array();
            // proceed if PMS charge mapping is not empty
            if (!empty($pmsChargeMapping)) {
                foreach ($pmsChargeMapping as $mapping) {
                    // proceed if price line id is exist in $pmsChargeIds
                    if (in_array($mapping['pricelineid'], $pmsChargeIds)) {
                        $pmsDetails = $pmsChargeDetails[$mapping['pricelineid']];
                        $tmp['name']                           = $pmsDetails['pricelineid']; // name
                        $tmp['charge_type']                    = $pmsDetails['chargeid']; // charge_type
                        $tmp['charge_period']                  = $pmsDetails['chargeperiodid']; // charge_period
                        $tmp['list_price']                     = $pmsDetails['offeringprice']; // list_price
                        $tmp['icb_flag']                       = $pmsDetails['isCustomPrice']; // icb_flag
                        $tmp['currencyid']                     = $pmsDetails['currencyid']; // currencyid
                        $tmp['customer_contract_price']        = $pmsDetails['offeringprice'];
                        $tmp['percent_discount_flag']          = '';
                        $tmp['discount']                       = '';
                        $tmp['igc_settlement_price']           = '';
                        // generate the expected array from the price details
                        $priceDetails[] = $tmp;
                    }
                }
            }
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'priceDetails: '.print_r($priceDetails,true), '');
            return $priceDetails;
        }
    }

    /**
     * method to validate the Billing Affiliate Details
     *
     * @param array $contract_details contract details
     * @author SathishKumar.Ganesan
     * @since May 11, 2017
     */
    private function validateBillingAffiliate($contract_details){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, ' contract_details: '.print_r($contract_details,true), 'Method to validate the Billing Affiliate Details');
        $organization_code1_list = $organization_code2_list = array();
        if(isset($contract_details['BillingAffiliate'][0]) && !empty($contract_details['BillingAffiliate'][0])){
            $organization_bean = BeanFactory::getBean('gc_organization');
            $organization_list = $organization_bean->get_full_list('','');
            if(is_array($organization_list)){
                foreach($organization_list as $organization){
                    if(isset($organization->organization_code_1) && !empty($organization->organization_code_1)){
                        $organization_code1_list[] = $organization->organization_code_1;
                    }
                    if(isset($organization->organization_code_2) && !empty($organization->organization_code_2)){
                        $organization_code2_list[] = $organization->organization_code_2;
                    }
                }
            }
            $is_valid_org_code = true;

            //If organization codes are set then check whether those are valid
            $organization_code_1 = (isset($contract_details['BillingAffiliate'][0]['organization_code_1'])) ? $contract_details['BillingAffiliate'][0]['organization_code_1'] : '';

            if ($organization_code_1 != '' && !in_array($organization_code_1, $organization_code1_list)) {
                $this->error_message['3020'][] = 'Invalid Organization code 1 (' . $organization_code_1 . ')';
                $is_valid_org_code             = false;
            }
            $organization_code_2 = (isset($contract_details['BillingAffiliate'][0]['organization_code_2'])) ? $contract_details['BillingAffiliate'][0]['organization_code_2'] : '';
            if ($organization_code_2 != '' && !in_array($organization_code_2, $organization_code2_list)) {
                $this->error_message['3020'][] = 'Invalid Organization code 2 (' . $organization_code_2 . ')';
                $is_valid_org_code             = false;
            }

            if ($is_valid_org_code) {
                $where_org_cond  = "(organization_code_1 = '" . $organization_code_1 . "'";
                if($organization_code_1 == '')
                    $where_org_cond  .= " or organization_code_1 is null";
                $where_org_cond .= " )and (organization_code_2 = '" . $organization_code_2 . "'";
                if($organization_code_2 == '')
                    $where_org_cond  .= " or organization_code_2 is null";
                $where_org_cond .= ")";
                $org_search_list = BeanFactory::getBean('gc_organization')->get_full_list('', $where_org_cond);
                if (empty($org_search_list)) {
                    $this->error_message['3020'][] = 'Organization code combination is Invalid';
                }
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * method to validate the Payment Details
     *
     * @param array $contractLineItemDetails contract lineitem details
     * @author SathishKumar.Ganesan
     * @since May 11, 2017
     */
    private function validatePaymentDetails($contractLineItemDetails){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, ' contractLineItemDetails: '.print_r($contractLineItemDetails,true), 'Method to validate the Payment Details');
        $payment_details = array();
        $mandatory_check = 0;
        if(isset($contractLineItemDetails['ContractCreditCard'])){
            $payment_details = $contractLineItemDetails['ContractCreditCard'];
            $payment_validate_fields = array(
                'credit_card_no' => 'Number',
                'expiration_date' => 'Expiration Date');
            $payment_type = 'Credit Card';
            $mandatory_check = 1;
        }elseif(isset($contractLineItemDetails['ContractDirectDeposit'])){
            $payment_details = $contractLineItemDetails['ContractDirectDeposit'];
            $payment_validate_fields = array(
                'bank_code' => 'Bank Code',
                'branch_code' => 'Branch Code',
                'deposit_type' => 'Deposit Type',
                'account_no' => 'Account Number');
            $payment_mandatory_check_fields = array('bank_code', 'branch_code', 'deposit_type', 'account_no', 'payee_contact_person_name_2', 'payee_tel_no', 'payee_email_address', 'remarks');
            $payment_type = 'Direct Deposit';
            $mandatory_check = 0;
        }elseif(isset($contractLineItemDetails['ContractAccountTransfer'])){
            $payment_details = $contractLineItemDetails['ContractAccountTransfer'];
            $payment_validate_fields = array(
                'name' => 'Account Holder Name',
                'bank_code' => 'Bank Code',
                'branch_code' => 'Branch Code',
                'deposit_type' => 'Deposit Type',
                'account_no' => 'Account Number',
                'account_no_display' => 'Account Number Display');
            $payment_type = 'Account Transfer';
            $mandatory_check = 1;
        }elseif(isset($contractLineItemDetails['ContractPostalTransfer'])){
            $payment_validate_fields = array(
                'name' => 'Account Holder Name',
                'postal_passbook_mark' => 'Postal Passbook Mark',
                'postal_passbook' => 'Postal Passbook Number',
                'postal_passbook_no_display' => 'Postal Passbook Number Display');
            $payment_details = $contractLineItemDetails['ContractPostalTransfer'];
            $payment_type = 'Postal Transfer';
            $mandatory_check = 1;
        }

        if(!empty($payment_details)){
            if($payment_type == 'Direct Deposit'){
                foreach($payment_mandatory_check_fields as $fields){
                    if(trim($payment_details[$fields]) != ''){
                        $mandatory_check = 1;
                    }
                }
            }
            if($mandatory_check==1){
                foreach($payment_validate_fields as $key => $fields){
                    if(trim($payment_details[$key]) == ''){
                        $this->error_message['3016'][] = $payment_type.' '.$fields.' is mandatory';
                    }
                }
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * method to set error for not nullable fields
     *
     * @param array $nullablearray not nullable filed list array
     * @param string $paramkey empty string on recursive call it contains index value
     * @author SathishKumar.Ganesan
     * @since May 11, 2017
     */
    private function setNullableError($nullablearray,$paramkey){
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, ' nullablearray: '.print_r($nullablearray,true).' '.GCM_GL_LOG_VAR_SEPARATOR.' paramkey: '.$paramkey, 'Method to set error for not nullable fields');
        foreach($nullablearray as $key => $value){
            if(is_array($value)){
                if($paramkey!=''){
                    $key = $paramkey.'->'.$key;
                }
                $this->setNullableError($value,$key);
            }else{
                $this->error_message['3020'][] = $paramkey.' '.$value.' - NULL possible condition failed';
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }
}
?>
