<?php

/**
 * Common Business Validation layey
 */
trait CustomBusinessValidationLayer
{
    /**
     * array business validation layer error details
     */
    public $business_errors;
    /**
     * custom property
     */
    public $corp_acc_err_arr;
    public $billing_affiliate_err_dtls;
    public $mandatory_field_list;
    public $is_contract_changed = false;
    /**
     * populate $business_errors
     *
     * @param $xpath_ids array (OPTIONAL) key value pairs of xpath ids which going to replace with
     * @param $error_details array (OPTIONAL) key value pairs of error details to override config details
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function addBusinessValidationErrors($xpath_ids = [], $custom_error_details = [])
    {
        // find function name from where method add_pre_errors is calling
        $function         = debug_backtrace()[1]['function'];
        $function         = ApiValidationHelper::fromCamelCase($function);
        $function_details = explode('_', $function);
        // if HTTP request is version_up
        if (in_array($function_details[1], ['up'])) {
            $field   = explode($function_details[3] . '_', $function);
            $field   = $field[1];
            $methods = $function_details[0] . '_' . $function_details[1];
            $type    = $function_details[3];
            $layer   = $function_details[2];
        } else {
            $field   = explode($function_details[2] . '_', $function);
            $field   = $field[1];
            $methods = $function_details[0];
            $type    = $function_details[2];
            $layer   = $function_details[1];
        }

        $api_config_details = [
            'field'   => $field, // field
            'methods' => $methods, // methods
            'type'    => $type, // type
            'layer'   => $layer, // layer
        ];
        $error = ApiValidationHelper::generateError($api_config_details, $xpath_ids);

        if (!empty($error)) {
            $error_codes    = '';
            $error_messages = '';
            $error_details  = [];

            $error_codes    = !empty($custom_error_details['codes']) ? $custom_error_details['codes'] : $error['codes'];
            $error_messages = !empty($custom_error_details['messages']) ? $custom_error_details['messages'] : $error['messages'];
            if (!empty($custom_error_details['details'])) {
                $error_details = $custom_error_details['details'];
            } elseif (!empty($error['details'])) {
                $error_details = $error['details'];
            } else {
                $error_details = [];
            }

            // GCM ID of the target contract
            $gcm_id = !empty($this->global_contract_id) ? $this->global_contract_id : $this->global_contract_uuid;
            // if request = version_up|patch add global_contract_id|global_contract_uuid in message details
            if(!empty(trim($gcm_id)) && in_array($this->request_type, [self::REQUEST_TYPE_VERSION_UP, self::REQUEST_TYPE_PATCH])) {
                $error_messages = 'GCM ID of the target contract is ['. $gcm_id .']. ' . $error_messages;
            }

            $this->business_errors['codes'][]                  = $error_codes;
            $this->business_errors['messages'][$error_codes][] = $error_messages;
            $this->business_errors['details'][$error_codes][]  = $error_details;

            return false;
        }

        return true;
    }

    /**
     * method to validate the Billing Affiliate Details
     *
     * @param array $contract_details contract details
     * @since Oct 11, 2017
     */
    private function validateBillingAffiliate($contract_details)
    {
        $organization_code1_list = $organization_code2_list = $validation_result = array();
        if (isset($contract_details['BillingAffiliate'][0]) && !empty($contract_details['BillingAffiliate'][0])) {
            $organization_bean = BeanFactory::getBean('gc_organization');
            $organization_list = $organization_bean->get_full_list('', '');
            if (is_array($organization_list)) {
                foreach ($organization_list as $organization) {
                    if (isset($organization->organization_code_1) && !empty($organization->organization_code_1)) {
                        $organization_code1_list[] = $organization->organization_code_1;
                    }
                    if (isset($organization->organization_code_2) && !empty($organization->organization_code_2)) {
                        $organization_code2_list[] = $organization->organization_code_2;
                    }
                }
            }
            $is_valid_org_code = true;
            //If organization codes are set then check whether those are valid
            $organization_code_1 = (isset($contract_details['BillingAffiliate'][0]['organization_code_1'])) ? $contract_details['BillingAffiliate'][0]['organization_code_1'] : '';
            if ($organization_code_1 != '' && !in_array($organization_code_1, $organization_code1_list)) {
                $validation_result['invalid_code_1'] = true;
                $is_valid_org_code                   = false;
            }
            $organization_code_2 = (isset($contract_details['BillingAffiliate'][0]['organization_code_2'])) ? $contract_details['BillingAffiliate'][0]['organization_code_2'] : '';
            if ($organization_code_2 != '' && !in_array($organization_code_2, $organization_code2_list)) {
                $validation_result['invalid_code_2'] = true;
                $is_valid_org_code                   = false;
            }
            if ($is_valid_org_code) {
                $where_org_cond = "(organization_code_1 = '" . $organization_code_1 . "'";
                if ($organization_code_1 == '') {
                    $where_org_cond .= " or organization_code_1 is null";
                }

                $where_org_cond .= " )and (organization_code_2 = '" . $organization_code_2 . "'";
                if ($organization_code_2 == '') {
                    $where_org_cond .= " or organization_code_2 is null";
                }

                $where_org_cond .= ")";
                $org_search_list = BeanFactory::getBean('gc_organization')->get_full_list('', $where_org_cond);
                if (empty($org_search_list)) {
                    $validation_result['invalid_combination_code'] = true;
                }
            }
        }
        return $validation_result;
    }
    /**
     * method to validate the Payment Details
     *
     * @param array $contractLineItemDetails contract lineitem details
     * @since Oct 11, 2017
     */
    private function validatePaymentDetails($contractLineItemDetails)
    {
        $payment_details      = array();
        $mandatory_check      = 0;
        $mandatory_field_list = array();
        if (isset($contractLineItemDetails['ContractCreditCard'])) {
            $payment_details         = $contractLineItemDetails['ContractCreditCard'];
            $payment_validate_fields = array(
                'credit_card_no'  => 'Number',
                'expiration_date' => 'Expiration Date');
            $payment_type    = 'Credit Card';
            $mandatory_check = 1;
        } elseif (isset($contractLineItemDetails['ContractDirectDeposit'])) {
            $payment_details         = $contractLineItemDetails['ContractDirectDeposit'];
            $payment_validate_fields = array(
                'bank_code'    => 'Bank Code',
                'branch_code'  => 'Branch Code',
                'deposit_type' => 'Deposit Type',
                'account_no'   => 'Account Number');
            $payment_mandatory_check_fields = array('bank_code', 'branch_code', 'deposit_type', 'account_no', 'payee_contact_person_name_2', 'payee_tel_no', 'payee_email_address', 'remarks');
            $payment_type                   = 'Direct Deposit';
            $mandatory_check                = 0;
        } elseif (isset($contractLineItemDetails['ContractAccountTransfer'])) {
            $payment_details         = $contractLineItemDetails['ContractAccountTransfer'];
            $payment_validate_fields = array(
                'name'               => 'Account Holder Name',
                'bank_code'          => 'Bank Code',
                'branch_code'        => 'Branch Code',
                'deposit_type'       => 'Deposit Type',
                'account_no'         => 'Account Number',
                'account_no_display' => 'Account Number Display');
            $payment_type    = 'Account Transfer';
            $mandatory_check = 1;
        } elseif (isset($contractLineItemDetails['ContractPostalTransfer'])) {
            $payment_validate_fields = array(
                'name'                       => 'Account Holder Name',
                'postal_passbook_mark'       => 'Postal Passbook Mark',
                'postal_passbook'            => 'Postal Passbook Number',
                'postal_passbook_no_display' => 'Postal Passbook Number Display');
            $payment_details = $contractLineItemDetails['ContractPostalTransfer'];
            $payment_type    = 'Postal Transfer';
            $mandatory_check = 1;
        }

        if (!empty($payment_details)) {
            if ($payment_type == 'Direct Deposit') {
                foreach ($payment_mandatory_check_fields as $fields) {
                    if (trim($payment_details[$fields]) != '') {
                        $mandatory_check = 1;
                    }
                }
            }
            if ($mandatory_check == 1) {
                foreach ($payment_validate_fields as $key => $fields) {
                    if (trim($payment_details[$key]) == '') {
                        $mandatory_field_list[$payment_type][$key] = true;
                    }
                }
            }
        }
        return $mandatory_field_list;
    }

    /**
     * method to accounts and corporate details
     *
     * @param array $contractLineItemDetails contract lineitem details
     * @since Oct 11, 2017
     */
    private function validatePatchAccountCorporateDetails($xml_arr, $contract)
    {
        if (isset($xml_arr['Accounts']) && !empty($xml_arr['Accounts'])) {
            //Get the country list. It will use to validate the country in request
            $country_codes = array();
            $country_bean  = $obj_timezone  = BeanFactory::getBean('c_country');
            $country_list  = $country_bean->get_full_list('', '');
            if (is_array($country_list)) {
                foreach ($country_list as $country) {
                    $country_codes[] = $country->country_code_numeric;
                }
            }
            foreach ($xml_arr['Accounts'] as $account_type => $account_details) {
                $account_id = '';
                //Get the account id based on the account type
                if ($account_type == 'Contract') {
                    $account_id = $contract->contacts_gc_contracts_1contacts_ida;
                } else {
                    $where_condition_line_items           = " gc_line_item_contract_history.contracts_id = '" . $contract->id . "' ";
                    $obj_array_line_contract_history_bean = BeanFactory::getBean('gc_Line_Item_Contract_History')->get_list('gc_line_item_contract_history.id', $where_condition_line_items, null, 1);
                    if (!empty($obj_array_line_contract_history_bean['list'])) {
                        foreach ($obj_array_line_contract_history_bean['list'] as $line_contract_bean) {
                            $tech_account_id = $line_contract_bean->tech_account_id;
                            $bill_account_id = $line_contract_bean->bill_account_id;
                            continue;
                        }
                    }
                    if ($account_type == 'Technology') {
                        $account_id = $tech_account_id;
                    } elseif ($account_type == 'Billing') {
                        $account_id = $bill_account_id;
                    }

                }
                $obj_contract_account_bean = BeanFactory::getBean('Contacts');
                if ($account_id != '') {
                    $obj_contract_account_bean->retrieve($account_id);
                }
                //This block is to prevent both last_name and name_2_c are being empty if we are saving it.
                //if both last_name and name_2_c is specified and both are empty then through this error
                if (isset($account_details['last_name']) && isset($account_details['name_2_c']) && trim($account_details['last_name']) == '' && trim($account_details['name_2_c']) == '') {
                    $error_message[$account_type]['is_contact_person_name_empty'] = true;
                } else {
                    //if this account is not empty in the existing contract
                    if (!empty($account_id) && isset($obj_contract_account_bean->id)) {
                        //this block is to avoid appending same error message for same error code again.
                        //if either one is present and it's value is empty
                        if ((isset($account_details['last_name']) && !isset($account_details['name_2_c']) && $account_details['last_name'] == '' && $obj_contract_account_bean->name_2_c == '') || (isset($account_details['name_2_c']) && !isset($account_details['last_name']) && $account_details['name_2_c'] == '' && $obj_contract_account_bean->last_name == '')) {
                            $error_message[$account_type]['is_contact_person_name_empty'] = true;
                        }
                    } else {
                        //if not at all present or either one name is present and if that also empty in the request
                        if ((!isset($account_details['last_name']) && !isset($account_details['name_2_c'])) || (isset($account_details['last_name']) && !isset($account_details['name_2_c']) && $account_details['last_name'] == '') || (isset($account_details['name_2_c']) && !isset($account_details['last_name']) && $account_details['name_2_c'] == '')) {
                            $error_message[$account_type]['is_contact_person_name_empty'] = true;
                        }
                    }
                }
                //if no account is there in the db and corporate details is missing the xml then throw this error
                if (empty($account_id) && !isset($account_details['CorporateDetails'])) {
                    $error_message[$account_type]['is_corporate_details_xml_missing'] = true;
                }

                //validate the corporate name details if CCID is empty
                if (isset($account_details['CorporateDetails']) && (!isset($account_details['CorporateDetails']['common_customer_id_c']) || $account_details['CorporateDetails']['common_customer_id_c'] == '')) {
                    $corporate_details = $account_details['CorporateDetails'];
                    //if both name and name_2_c is specified and both are empty then through this error
                    if (isset($corporate_details['name']) && isset($corporate_details['name_2_c']) && trim($corporate_details['name']) == '' && trim($corporate_details['name_2_c']) == '') {
                        $error_message[$account_type]['is_corporate_name_empty'] = true;
                    } else {
                        //validate the corporate name details from the db
                        if (isset($obj_contract_account_bean->account_id) && $obj_contract_account_bean->account_id != '') {
                            $obj_corporate_bean = BeanFactory::getBean('Accounts');
                            $obj_corporate_bean->retrieve($obj_contract_account_bean->account_id);

                            //if either one is present and it's value is empty
                            if ((isset($corporate_details['name']) && !isset($corporate_details['name_2_c']) && $corporate_details['name'] == '' && $obj_corporate_bean->name_2_c == '') || (isset($corporate_details['name_2_c']) && !isset($corporate_details['name']) && $corporate_details['name_2_c'] == '' && $obj_corporate_bean->name == '')) {
                                $error_message[$account_type]['is_corporate_name_empty'] = true;
                            }
                        } else {
                            //If ccid not present in the xml then check the corporate details
                            if (!isset($account['CorporateDetails']['common_customer_id_c'])) {
                                //if not at all present or either one name is present and if that also empty in the request
                                if ((!isset($corporate_details['name']) && !isset($corporate_details['name_2_c'])) || (isset($corporate_details['name']) && !isset($corporate_details['name_2_c']) && $corporate_details['name'] == '') || (isset($corporate_details['name_2_c']) && !isset($corporate_details['name']) && $corporate_details['name_2_c'] == '')) {
                                    $error_message[$account_type]['is_corporate_name_empty'] = true;
                                }
                            }
                        }
                    }
                }
                $country_code_numeric = isset($account_details['country_code_numeric']) ? $account_details['country_code_numeric'] : '';
                // if country code is not sent in the xml then set the country stored in the db
                if (!isset($account_details['country_code_numeric']) || trim($account_details['country_code_numeric'] == '') && (isset($obj_contract_account_bean->c_country_id_c) && $obj_contract_account_bean->c_country_id_c != '')) {
                    $prev_country_bean                       = BeanFactory::getBean('c_country', $obj_contract_account_bean->c_country_id_c);
                    $country_code_numeric                    = $prev_country_bean->country_code_numeric;
                    $account_details['country_code_numeric'] = $country_code_numeric;
                }

                //Check if the country tag name and address block is mismatch
                if (isset($account_details['address_tag_name']) && trim($account_details['address_tag_name']) != '') {
                    switch ($country_code_numeric) {
                        case '840':
                            $expected_addr_tag_name = 'american';
                            break;
                        case '124':
                            $expected_addr_tag_name = 'canadian';
                            break;
                        case '392':
                            $expected_addr_tag_name = 'japanese';
                            break;
                        default:
                            $expected_addr_tag_name = 'any';
                            break;
                    }
                    $country_tag_list = array('japanese', 'american', 'canadian');
                    if (in_array($account_details['address_tag_name'], $country_tag_list)) {
                        $addr_tag_xpath = $account_details['address_tag_name'] . '-property';
                    } else {
                        $addr_tag_xpath = $account_details['address_tag_name'];
                    }
                    if (trim(strtolower($account_details['address_tag_name'])) != $expected_addr_tag_name) {
                        $error_message[$account_type]['country_tag_mismatch']['is_invalid']           = true;
                        $error_message[$account_type]['country_tag_mismatch']['country_code_numeric'] = $account_details['country_code_numeric'];
                        $error_message[$account_type]['country_tag_mismatch']['addr_tag_xpath']       = $addr_tag_xpath;
                    }
                }
                //Check if the country reference is valid and country-state combination is right
                if (trim($account_details['country_code_numeric']) != '') {
                    if (!in_array($account_details['country_code_numeric'], $country_codes)) {
                        $error_message[$account_type]['country_not_found']['is_invalid']           = true;
                        $error_message[$account_type]['country_not_found']['country_code_numeric'] = $account_details['country_code_numeric'];
                    }
                    if (!$error_message[$account_type]['country_tag_mismatch']['is_invalid'] && isset($account_details['general_address']['state_general_code']) && $account_details['general_address']['state_general_code'] != '' && !$this->checkCountryStateValidation($account_details['country_code_numeric'], $account_details['general_address']['state_general_code'])) {
                        $error_message[$account_type]['country_state_mismatch']['is_invalid']           = true;
                        $error_message[$account_type]['country_state_mismatch']['country_code_numeric'] = $account_details['country_code_numeric'];
                        $error_message[$account_type]['country_state_mismatch']['state_general_code']   = $account_details['general_address']['state_general_code'];
                        $error_message[$account_type]['country_state_mismatch']['addr_tag_xpath']       = $addr_tag_xpath;
                    }
                }
                //DF-1066, DF-1069 DF-838 - Email validation added for PATCH
                if (isset($account_details['email1']) && trim($account_details['email1']) != '' && !$this->mod_utils->email_validation($account_details['email1'])) {
                    $email_msg = '';
                    if ($account_type == 'Contract') {
                        $email_msg = 'Contracting Account';
                    } elseif ($account_type == 'Billing') {
                        $email_msg = 'Billing Account';
                    } elseif ($account_type == 'Technology') {
                        $email_msg = 'Technology Account';
                    }
                    $error_message[$account_type]['email1']['is_invalid'] = true;
                    $error_message[$account_type]['email1']['value']      = $account_details['email1'];
                }
            }
        }
        //DF-1069 - Sales channel block needs to be updated by PATCH request
        if (!empty($xml_arr['ContractDetails']['ContractSalesRepresentative']) && trim($xml_arr['ContractDetails']['ContractSalesRepresentative'][0]['sales_company']) != '') {
            $sales_companies    = array();
            $sales_company_bean = BeanFactory::getBean('gc_NTTComGroupCompany');
            $sales_company_list = $sales_company_bean->get_full_list('', '');
            if (is_array($sales_company_list)) {
                foreach ($sales_company_list as $sales_company) {
                    $sales_companies[] = $sales_company->name;
                }
            }
            if (!in_array($xml_arr['ContractDetails']['ContractSalesRepresentative'][0]['sales_company'], $sales_companies)) {
                $error_message['sales']['sales_company']['is_invalid'] = true;
                $error_message['sales']['sales_company']['value']      = $xml_arr['ContractDetails']['ContractSalesRepresentative'][0]['sales_company'];
            }

        }
        //DF-1069, DF-838 We need to validate the email
        if (isset($xml_arr['ContractDetails']['ContractSalesRepresentative'][0]['email1']) && trim($xml_arr['ContractDetails']['ContractSalesRepresentative'][0]['email1']) != '' && !$this->mod_utils->email_validation($xml_arr['ContractDetails']['ContractSalesRepresentative'][0]['email1'])) {
            $error_message['sales']['email1']['is_invalid'] = true;
            $error_message['sales']['email1']['value']      = $xml_arr['ContractDetails']['ContractSalesRepresentative'][0]['email1'];
        }
        return $error_message;
    }
    /**
     * Method to check whether country and state code combination is correct or not
     * @param string $country_code
     * @param string $state_code
     * @return boolean true if correct, false if not
     */
    private function checkCountryStateValidation($country_code, $state_code)
    {
        $obj_country_bean = BeanFactory::getBean('c_country');
        $obj_country_bean->retrieve_by_string_fields(array(
            'country_code_numeric' => $country_code));
        if ($country_code == '840' || $country_code == '124') {
            $obj_state_bean = BeanFactory::getBean('c_State');
            $obj_state_bean->retrieve_by_string_fields(array(
                'state_code' => $state_code));
            $obj_state_bean->retrieve($obj_state_bean->id);
            if ($obj_country_bean->id != $obj_state_bean->c_country_c_state_1c_country_ida) {
                return false;
            }
        }
        return true;
    }
    /**
     * Method to check whether Sales company exist in the system
     * @param string $sales_company_name Sales company name
     * @return boolean true if correct, false if not
     */
    private function isValidSalesCompany($sales_company_name)
    {
        if (!empty($sales_company_name) && trim($sales_company_name) != '') {
            $sales_companies    = array();
            $sales_company_bean = BeanFactory::getBean('gc_NTTComGroupCompany');
            $sales_company_list = $sales_company_bean->get_full_list('', '');
            if (is_array($sales_company_list)) {
                foreach ($sales_company_list as $sales_company) {
                    $sales_companies[] = $sales_company->name;
                }
            }
            if (in_array($sales_company_name, $sales_companies)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to check whether updated-after parameter is in correct format and TimeZone is register with gcm application
     * $updatedAfter string updated-after date value
     * @return array
     */
    private function isInvalidUpdatedAfter($updatedAfter)
    {
        $invalid_list = array();
        if(trim($updatedAfter)!=''){
            $updatedAfter =  str_replace(' ', '+', $updatedAfter);
            $date_res = $this->mod_utils->ConvertDatetoGMTDate(trim($updatedAfter));
            if(isset($date_res['result']) && !$date_res['result']){
                if(isset($date_res['error_type']) && $date_res['error_type']==2){
                    $invalid_list['time_zone']['invalid'] = true;
                }
                else{
                    $invalid_list['updated_after']['invalid'] = true;
                }
            }
        }
        return $invalid_list;
    }

    private function patchAgreementPeriodValidation($contract_xml_details)
    {
        $err_type_list = array();
        $contract_status = !empty(trim($contract_xml_details['contract_status'])) ? $contract_xml_details['contract_status'] : '';
        $contract_start_date_db_utc = $contract_end_date_xml_utc = $contract_end_date_sys  = '';
        $contract_start_date_tz_db = $contract_start_date_local_db = '';
        if(isset($this->contract_data['header']->contract_startdate) && $this->contract_data['header']->contract_startdate!=''){
            $contract_start_date_db_utc = $this->contract_data['header']->contract_startdate;
            $contract_start_date_tz_db = $this->contract_data['header']->contract_startdate_timezone;
            $contract_start_date_local_db = ApiValidationHelper::convertGMTDatetoLocalDataDate($contract_start_date_db_utc, $contract_start_date_tz_db);
        }

        if($contract_status==2){
           $contract_end_date_sys = date('Y-m-d H:i:s');
        }elseif(isset($contract_xml_details['contract_enddate']) && $contract_xml_details['contract_enddate']!=''){
           $contract_end_date_xml_utc = $contract_xml_details['contract_enddate'];
           $contract_end_date_tz_xml = $contract_xml_details['contract_enddate_timezone'];
           $contract_end_date_xml_local = ApiValidationHelper::convertGMTDatetoLocalDataDate($contract_end_date_xml_utc, $contract_end_date_tz_xml);
           $contract_end_date_xml_local = str_replace(' ', 'T', $contract_end_date_xml_local);
        }

        if(empty($contract_start_date_db_utc) && ( $contract_status==2 || !empty($contract_end_date_xml_utc) )){
          $err_type_list['start_mandatory_failed'] = true;
        }

        if(!empty($contract_start_date_db_utc) && $contract_status==2 && strtotime($contract_start_date_db_utc) > strtotime($contract_end_date_sys)){
           $err_type_list['date_compare_failed_ced_sys'] = true;
           $err_type_list['contract_startdate'] = $contract_start_date_local_db.$contract_start_date_tz_db;
           $err_type_list['contract_enddate'] = $contract_end_date_sys.'+00:00';
        }elseif(!empty($contract_start_date_db_utc) && !empty($contract_end_date_xml_utc) && strtotime($contract_start_date_db_utc) > strtotime($contract_end_date_xml_utc)){
            $err_type_list['date_compare_failed_ced_xml'] = true;
            $err_type_list['contract_startdate'] = $contract_start_date_local_db.$contract_start_date_tz_db;
            $err_type_list['contract_enddate'] = $contract_end_date_xml_local.$contract_end_date_tz_xml;
        }
        return $err_type_list;
    }

    private function patchGetLineItemDateErrorList($line_item_details_xml,$line_item_details_db,$contract_status){
        global $sugar_config;
        $li_date_error_list = array();
        foreach($line_item_details_db as $line_item_key => $line_item_detail){
            $service_start_date  = $service_end_date = $billing_start_date = $billing_end_date  = '';
            $service_start_date_err_msg = $service_end_date_err_msg = $billing_start_date_err_msg = $billing_end_date_err_msg = '';
            $service_start_date_local = $service_end_date_local = $billing_start_date_local = $billing_end_date_local = '';
            $li_header_xml =  $line_item_details_xml[$line_item_key]['ContractLineItemHeader'];
            $li_header_db = $line_item_detail['ContractLineItemHeader'];
            if(isset($li_header_xml['service_start_date']) && !empty($li_header_xml['service_start_date'])){
                $service_start_date = $li_header_xml['service_start_date'];
                $service_start_date_local = ApiValidationHelper::convertGMTDatetoLocalDataDate($service_start_date, $li_header_xml['service_start_date_timezone']);
                $service_start_date_local = str_replace(' ', 'T', $service_start_date_local);
                $service_start_date_err_msg = '('.$service_start_date_local.$li_header_xml['service_start_date_timezone'].') in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value/prod-cntrct-item-000002:service-start-date-val';
            }elseif(isset($li_header_db['service_start_date']) && !empty($li_header_db['service_start_date'])){
                $service_start_date = $li_header_db['service_start_date'];
                $service_start_date_local = ApiValidationHelper::convertGMTDatetoLocalDataDate($service_start_date, $li_header_db['service_start_date_timezone']);
                $service_start_date_err_msg = 'Service start date value in DB ('.$service_start_date_local.$li_header_db['service_start_date_timezone'].') for DB line item id ('.$li_header_db['line_item_id_xml_id'].')';
            }

            $service_flag = true;
            if(isset($li_header_xml['service_end_date']) && !empty($li_header_xml['service_end_date'])){
                $service_flag = false;
                $service_end_date = $li_header_xml['service_end_date'];
                $service_end_date_local = ApiValidationHelper::convertGMTDatetoLocalDataDate($service_end_date, $li_header_xml['service_end_date_timezone']);
                $service_end_date_local = str_replace(' ', 'T', $service_end_date_local);
                $service_end_date_err_msg = '('.$service_end_date_local.$li_header_xml['service_end_date_timezone'].') in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value/prod-cntrct-item-000002:service-end-date-val';
            }elseif(isset($li_header_db['service_end_date']) && !empty($li_header_db['service_end_date'])){
                $service_end_date = $li_header_db['service_end_date'];
                $service_end_date_local = ApiValidationHelper::convertGMTDatetoLocalDataDate($service_end_date, $li_header_db['service_end_date_timezone']);
                $service_end_date_err_msg = 'Service end date value in DB ('.$service_end_date_local.$li_header_db['service_end_date_timezone'].') for DB line item id ('.$li_header_db['line_item_id_xml_id'].')';
            }

            if ( $contract_status==2 && ($service_end_date =='' || ($service_flag && strtotime($service_end_date) > strtotime(date('Y-m-d H:i:s'))) )){
                $service_end_date = date('Y-m-d H:i:s');
                $service_end_date_err_msg = 'Service end date system value ('.$service_end_date.'+00:00) for DB line item id ('.$li_header_db['line_item_id_xml_id'].')';
            }

            if( $service_start_date=='' && $contract_status!=2 && isset($line_item_details_xml[$line_item_key]['ContractLineItemHeader']['service_end_date'])){
                $li_date_error_list['service_start_date_mandatory_failed'][$line_item_key]['line_item_id_xml_id'] = $line_item_details_db[$line_item_key]['ContractLineItemHeader']['line_item_id_xml_id'];
            }
            if($service_start_date!='' && $service_end_date!=''){
                if(strtotime($service_start_date) > strtotime($service_end_date)){
                    $li_date_error_list['service_date_compare_failed'][$line_item_key]['service_start_date_err_msg'] = $service_start_date_err_msg;
                    $li_date_error_list['service_date_compare_failed'][$line_item_key]['service_end_date_err_msg'] = $service_end_date_err_msg;
                }
            }

            if(isset($li_header_xml['billing_start_date']) && !empty($li_header_xml['billing_start_date'])){
                $billing_start_date = $li_header_xml['billing_start_date'];
                $billing_start_date_local = ApiValidationHelper::convertGMTDatetoLocalDataDate($billing_start_date, $li_header_xml['billing_start_date_timezone']);
                $billing_start_date_local = str_replace(' ', 'T', $billing_start_date_local);
                $billing_start_date_err_msg = '('.$billing_start_date_local.$li_header_xml['billing_start_date_timezone'].') in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value/prod-cntrct-item-000002:billing-start-date-val';
            }elseif(isset($li_header_db['billing_start_date']) && !empty($li_header_db['billing_start_date'])){
                $billing_start_date = $li_header_db['billing_start_date'];
                $billing_start_date_local = ApiValidationHelper::convertGMTDatetoLocalDataDate($billing_start_date, $li_header_db['billing_start_date_timezone']);
                $billing_start_date_err_msg = 'Billing start date value in DB ('.$billing_start_date_local.$li_header_db['billing_start_date_timezone'].') for DB line item id ('.$li_header_db['line_item_id_xml_id'].')';
            }

            $billing_flag = true;
            if(isset($li_header_xml['billing_end_date']) && !empty($li_header_xml['billing_end_date'])){
                $billing_flag = false;
                $billing_end_date = $li_header_xml['billing_end_date'];
                $billing_end_date_local = ApiValidationHelper::convertGMTDatetoLocalDataDate($billing_end_date, $li_header_xml['billing_end_date_timezone']);
                $billing_end_date_local = str_replace(' ', 'T', $billing_end_date_local);
                $billing_end_date_err_msg = '('.$billing_end_date_local.$li_header_xml['billing_end_date_timezone'].') in /contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value/prod-cntrct-item-000002:billing-end-date-val';
            }elseif(isset($li_header_db['billing_end_date']) && !empty($li_header_db['billing_end_date'])){
                $billing_end_date = $li_header_db['billing_end_date'];
                $billing_end_date_local = ApiValidationHelper::convertGMTDatetoLocalDataDate($billing_end_date, $li_header_db['billing_end_date_timezone']);
                $billing_end_date_err_msg = 'Billing end date value in DB ('.$billing_end_date_local.$li_header_db['billing_end_date_timezone'].') for DB line item id ('.$li_header_db['line_item_id_xml_id'].')';
            }

            if ( $contract_status==2 && ($billing_end_date =='' || ($billing_flag && strtotime($billing_end_date) > strtotime(date('Y-m-d H:i:s'))) )){
                $billing_end_date = date('Y-m-d H:i:s');
                $billing_end_date_err_msg = 'Billing end date system value ('.$billing_end_date.'+00:00) for DB line item id ('.$li_header_db['line_item_id_xml_id'].')';
            }

            if( $billing_start_date=='' && $contract_status!=2 && isset($line_item_details_xml[$line_item_key]['ContractLineItemHeader']['billing_end_date'])){
                $li_date_error_list['billing_start_date_mandatory_failed'][$line_item_key]['line_item_id_xml_id'] = $line_item_details_db[$line_item_key]['ContractLineItemHeader']['line_item_id_xml_id'];
            }
            if($billing_start_date!='' && $billing_end_date!=''){
                if(strtotime($billing_start_date) > strtotime($billing_end_date)){
                    $li_date_error_list['billing_date_compare_failed'][$line_item_key]['billing_start_date_err_msg'] = $billing_start_date_err_msg;
                    $li_date_error_list['billing_date_compare_failed'][$line_item_key]['billing_end_date_err_msg'] = $billing_end_date_err_msg;

                }
            }
        }

        return $li_date_error_list;
    }
}
