<?php

/**
 * Common Before Save Validation layey
 */
trait CustomBeforeSaveValidationLayer
{
    /**
     * array Before Save validation layer error details
     */
    public $before_save_errors;

    /**
     * populate $before_save_errors
     *
     * @param $xpath_ids array (OPTIONAL) key value pairs of xpath ids which going to replace with
     * @param $error_details array (OPTIONAL) key value pairs of error details to override config details
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function addBeforeSaveValidationErrors($xpath_ids = [], $custom_error_details = [])
    {
        // find function name from where method add_pre_errors is calling
        $function         = debug_backtrace()[1]['function'];
        $function         = ApiValidationHelper::fromCamelCase($function);
        $function_details = explode('_', $function);
        // if HTTP request is version_up
        if (in_array($function_details[1], ['up'])) {
            $field   = explode($function_details[3] . '_', $function);
            $field   = $field[1];
            $methods = $function_details[0] . '_' . $function_details[1];
            $type    = $function_details[3];
            $layer   = $function_details[2];
        } else {
            $field   = explode($function_details[2] . '_', $function);
            $field   = $field[1];
            $methods = $function_details[0];
            $type    = $function_details[2];
            $layer   = $function_details[1];
        }

        $api_config_details = [
            'field'   => $field, // field
            'methods' => $methods, // methods
            'type'    => $type, // type
            'layer'   => $layer, // layer
        ];
        $error = ApiValidationHelper::generateError($api_config_details, $xpath_ids);

        if (!empty($error)) {
            $error_codes    = '';
            $error_messages = '';
            $error_details  = [];

            $error_codes    = !empty($custom_error_details['codes']) ? $custom_error_details['codes'] : $error['codes'];
            $error_messages = !empty($custom_error_details['messages']) ? $custom_error_details['messages'] : $error['messages'];
            if (!empty($custom_error_details['details'])) {
                $error_details = $custom_error_details['details'];
            } elseif (!empty($error['details'])) {
                $error_details = $error['details'];
            } else {
                $error_details = [];
            }

            // GCM ID of the target contract
            $gcm_id = !empty($this->global_contract_id) ? $this->global_contract_id : $this->global_contract_uuid;
            // if request = version_up|patch add global_contract_id|global_contract_uuid in message details
            if(!empty(trim($gcm_id)) && in_array($this->request_type, [self::REQUEST_TYPE_VERSION_UP, self::REQUEST_TYPE_PATCH])) {
                $error_messages = 'GCM ID of the target contract is ['. $gcm_id .']. ' . $error_messages;
            }

            $this->before_save_errors['codes'][]                  = $error_codes;
            $this->before_save_errors['messages'][$error_codes][] = $error_messages;
            $this->before_save_errors['details'][$error_codes][]  = $error_details;

            return false;
        }

        return true;
    }
}
