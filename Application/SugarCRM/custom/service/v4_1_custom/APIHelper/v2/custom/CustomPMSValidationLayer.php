<?php

/**
 * Common PMS Validation layey
 */
trait CustomPMSValidationLayer
{
    /**
     * array PMS validation layer error details
     */
    public $pms_errors;

    /**
     * populate $pms_errors
     *
     * @param $xpath_ids array (OPTIONAL) key value pairs of xpath ids which going to replace with
     * @param $error_details array (OPTIONAL) key value pairs of error details to override config details
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function addPmsValidationErrors($xpath_ids = [], $custom_error_details = [])
    {
        // find function name from where method addPmsValidationErrors is calling
        $function         = debug_backtrace()[1]['function'];
        $function         = ApiValidationHelper::fromCamelCase($function);
        $function_details = explode('_', $function);
        // if HTTP request is version_up
        if (in_array($function_details[1], ['up'])) {
            $field   = explode($function_details[3] . '_', $function);
            $field   = $field[1];
            $methods = $function_details[0] . '_' . $function_details[1];
            $type    = $function_details[3];
            $layer   = $function_details[2];
        } else {
            $field   = explode($function_details[2] . '_', $function);
            $field   = $field[1];
            $methods = $function_details[0];
            $type    = $function_details[2];
            $layer   = $function_details[1];
        }

        $api_config_details = [
            'field'   => $field, // field
            'methods' => $methods, // methods
            'type'    => $type, // type
            'layer'   => $layer, // layer
        ];
        $error = ApiValidationHelper::generateError($api_config_details, $xpath_ids);

        if (!empty($error)) {
            $error_codes    = '';
            $error_messages = '';
            $error_details  = [];

            $error_codes    = !empty($custom_error_details['codes']) ? $custom_error_details['codes'] : $error['codes'];
            $error_messages = !empty($custom_error_details['messages']) ? $custom_error_details['messages'] : $error['messages'];
            if (!empty($custom_error_details['details'])) {
                $error_details = $custom_error_details['details'];
            } elseif (!empty($error['details'])) {
                $error_details = $error['details'];
            } else {
                $error_details = [];
            }

            // GCM ID of the target contract
            $gcm_id = !empty($this->global_contract_id) ? $this->global_contract_id : $this->global_contract_uuid;
            // if request = version_up|patch add global_contract_id|global_contract_uuid in message details
            if(!empty(trim($gcm_id)) && in_array($this->request_type, [self::REQUEST_TYPE_VERSION_UP, self::REQUEST_TYPE_PATCH])) {
                $error_messages = 'GCM ID of the target contract is ['. $gcm_id .']. ' . $error_messages;
            }

            $this->pms_errors['codes'][]                  = $error_codes;
            $this->pms_errors['messages'][$error_codes][] = $error_messages;
            $this->pms_errors['details'][$error_codes][]  = $error_details;

            return false;
        }

        return true;
    }

    /**
     * get available price list for the selected product specification
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Oct 07, 2017
     */
    public function availableProductPriceList($xml_specification_id, $line_item_index, $xml_contract_currency)
    {
        // populating from before middleware [post_before_mandatory_product_configuration_id]
        $pms_charge_mapping = $this->pms_charge_mapping[$xml_specification_id];
        $pms_charge_details = $this->pms_charge_details;
        $pms_configuration  = $this->pms_specification_details[$xml_specification_id]['characteristics'];

        $xml_configuration = $this->xml_characteristic_ids[$xml_specification_id."_".$line_item_index];
        $xml_price         = $this->xml_product_prices[$xml_specification_id."_".$line_item_index];
        // get post selected characteristic
        $selectedCharacteristic = [];
        foreach ($xml_configuration as $xml_configuration_key => $xml_configuration_details) {
            if ($pms_configuration[$xml_configuration_key]['valtypeid'] != 2) {
                $selectedCharacteristic[] = $xml_configuration_details;
            }
        }
        $pmsChargeIds = array();
        foreach ($pms_charge_mapping as $mapping) {
            // if no Contract Currency && no charge mapping charvalid
            if (empty($xml_contract_currency) && empty($mapping['charvalid'])) {
                $pmsChargeIds[] = $mapping['pricelineid'];
            }
            // if there is a Contract Currency && no charge mapping charvalid
            else if (!empty($xml_contract_currency) && ($xml_contract_currency === $mapping['currency'] || $pms_charge_details[$mapping['pricelineid']]['isCustomPrice'] == 'true') && empty($mapping['charvalid'])) {
                $pmsChargeIds[] = $mapping['pricelineid'];
            }
            // if no Contract Currency && filter selected characteristic
            else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && empty($xml_contract_currency) && count($mapping['charvalid']) == 1) {
                $pmsChargeIds[] = $mapping['pricelineid'];
            }
            // if there is a Contract Currency && filter selected characteristic
            else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && !empty($xml_contract_currency) && ($xml_contract_currency === $mapping['currency'] || $pms_charge_details[$mapping['pricelineid']]['isCustomPrice'] == 'true') && count($mapping['charvalid']) == 1) {
                $pmsChargeIds[] = $mapping['pricelineid'];
            }
            // if no Contract Currency && filter selected characteristic
            else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && empty($xml_contract_currency) && count($mapping['charvalid']) == count($selectedCharacteristic)) {
                $pmsChargeIds[] = $mapping['pricelineid'];
            }
            // if there is a Contract Currency && filter selected characteristic
            else if (!empty($selectedCharacteristic) && empty(array_diff($mapping['charvalid'], $selectedCharacteristic)) && !empty($xml_contract_currency) && ($xml_contract_currency === $mapping['currency'] || $pms_charge_details[$mapping['pricelineid']]['isCustomPrice'] == 'true') && count($mapping['charvalid']) == count($selectedCharacteristic)) {
                $pmsChargeIds[] = $mapping['pricelineid'];
            }
        }
        // valid price list
        $this->pms_charge_ids = $pmsChargeIds;
    }
}
