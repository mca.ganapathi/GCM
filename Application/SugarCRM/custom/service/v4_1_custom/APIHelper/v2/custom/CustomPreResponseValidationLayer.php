<?php

/**
 * Common Pre Response Validation layey
 */
trait CustomPreResponseValidationLayer
{
    /**
     * array Before Save validation layer error details
     */
    public $pre_response_errors;

    /**
     * populate $pre_response_errors
     *
     * @param $xpath_ids array(OPTIONAL) key value pairs of xpath ids which going to replace with
     * @param $error_details array (OPTIONAL) key value pairs of error details to override config details
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public function addPreResponseValidationErrors($xpath_ids = [], $custom_error_details = [])
    {
        // find function name from where method add_pre_errors is calling
        $function         = debug_backtrace()[1]['function'];
        $function         = ApiValidationHelper::fromCamelCase($function); // get_pre_response_invalid_xml_data
        $function_details = explode('_', $function);
        $field            = explode($function_details[3] . '_', $function);

        $api_config_details = [
            'field'   => $field[1], // field
            'methods' => $function_details[0], // methods
            'type'    => $function_details[3], // type
            'layer'   => $function_details[1] . '_' . $function_details[2], // layer
        ];
        $error = ApiValidationHelper::generateError($api_config_details, $xpath_ids);

        if (!empty($error)) {
            $error_codes    = '';
            $error_messages = '';
            $error_details  = [];

            $error_codes    = !empty($custom_error_details['codes']) ? $custom_error_details['codes'] : $error['codes'];
            $error_messages = !empty($custom_error_details['messages']) ? $custom_error_details['messages'] : $error['messages'];
            if (!empty($custom_error_details['details'])) {
                $error_details = $custom_error_details['details'];
            } elseif (!empty($error['details'])) {
                $error_details = $error['details'];
            } else {
                $error_details = [];
            }

            // GCM ID of the target contract
            $gcm_id = !empty($this->global_contract_id) ? $this->global_contract_id : $this->global_contract_uuid;
            // if request = version_up|patch add global_contract_id|global_contract_uuid in message details
            if(!empty(trim($gcm_id)) && in_array($this->request_type, [self::REQUEST_TYPE_VERSION_UP, self::REQUEST_TYPE_PATCH])) {
                $error_messages = 'GCM ID of the target contract is ['. $gcm_id .']. ' . $error_messages;
            }

            $this->pre_response_errors['codes'][]                  = $error_codes;
            $this->pre_response_errors['messages'][$error_codes][] = $error_messages;
            $this->pre_response_errors['details'][$error_codes][]  = $error_details;

            return false;
        }

        return true;
    }
}

