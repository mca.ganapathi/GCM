<?php

require_once __DIR__ . '/../configs/api_config.php';
require_once __DIR__ . '/../configs/api_middleware.php';
/**
 * ModUtils Class
 */
require_once 'custom/include/ModUtils.php';
require_once 'custom/include/XMLUtils.php';

/**
 * Api Validation Helper
 */
class ApiValidationHelper
{

    const KEY_CIDAS_FAILED = 'cidas_failed';
    const SEPARATOR_UNDERSCORE = '_';
    const SEPARATOR_SPACE = ' ';
    const SEPARATOR_BRACES_OPEN = '{';
    const SEPARATOR_BRACES_CLOSE = '}';
    const SEPARATOR_PIPE = '|';

    /**
     * HTTP request type
     */
    const REQUEST_TYPE_POST       = 'post';
    const REQUEST_TYPE_VERSION_UP = 'version_up';
    const REQUEST_TYPE_PATCH      = 'patch';

    /**
     * Payment Type Constants
     */
    const DIRECT_DEPOSIT = '00';
    const ACCOUNT_TRANSFER = '10';
    const POSTAL_TRANSFER = '30';
    const CREDIT_CARD = '60';

    /**
     * Parse XML into Array
     *
     * @param $xml_data xml the request xml
     * @return array
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public static function convertPostXmlToArray($xml_data)
    {
        if (empty($xml_data)) {
            return null;
        }

        require_once 'custom/service/v4_1_custom/APIHelper/v2/ClassContractXMLParseV2.php';
        $contract_xml_parse_obj = new ClassContractXMLParseV2();
        $request_data = $contract_xml_parse_obj->getContractParseArrayV2($xml_data);
        //ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'request_data: ' . print_r($request_data, true), 'Parse Array');

        return $request_data;
    }

    /**
     * method to converte request_data datetime.
     *
     * @param array $request_data request data from api xml
     * @return array
     *
     * @author Rajul.Mondal
     * @since 21-Mar-2017
     */
    public static function convertRequestDataDatetoGMTDate($request_data)
    {
        // not passed $request_data in log cause it a huge data
        //ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'method to converte request_data datetime');
        global $sugar_config;
        $modUtils = new ModUtils();

        if (empty($request_data)) {
            return $request_data;
        }

        // converting contract_start_date datetime => UTC time
        $contract_start_date = !empty($request_data['ContractDetails']['ContractsHeader']['contract_startdate']) ? $request_data['ContractDetails']['ContractsHeader']['contract_startdate'] : '';
        if (!empty($contract_start_date)) {
            $contract_startdate_timezone = !empty($request_data['ContractDetails']['ContractsHeader']['contract_startdate_timezone']) ? $request_data['ContractDetails']['ContractsHeader']['contract_startdate_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $contract_start_date = new DateTime($contract_start_date, new DateTimeZone($contract_startdate_timezone));
            $contract_start_date = $contract_start_date->format('c');
            $contract_start_date_tmp = $modUtils->ConvertDatetoGMTDate($contract_start_date);

            if ($contract_start_date_tmp['result'] == false) {
                $request_data['ContractDetails']['ContractsHeader']['contract_startdate_invalid'] = $contract_start_date_tmp['error_type'];
            }

            $request_data['ContractDetails']['ContractsHeader']['contract_startdate'] = $contract_start_date_tmp['date'];
        }

        // converting contract_end_date datetime => UTC time
        $contract_end_date = !empty($request_data['ContractDetails']['ContractsHeader']['contract_enddate']) ? $request_data['ContractDetails']['ContractsHeader']['contract_enddate'] : '';
        if (!empty($contract_end_date)) {
            $contract_enddate_timezone = !empty($request_data['ContractDetails']['ContractsHeader']['contract_enddate_timezone']) ? $request_data['ContractDetails']['ContractsHeader']['contract_enddate_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $contract_end_date = new DateTime($contract_end_date, new DateTimeZone($contract_enddate_timezone));
            $contract_end_date = $contract_end_date->format('c');
            $contract_end_date_tmp = $modUtils->ConvertDatetoGMTDate($contract_end_date);

            if ($contract_end_date_tmp['result'] == false) {
                $request_data['ContractDetails']['ContractsHeader']['contract_enddate_invalid'] = $contract_end_date_tmp['error_type'];
            }

            $request_data['ContractDetails']['ContractsHeader']['contract_enddate'] = $contract_end_date_tmp['date'];
        }

        if (!empty($request_data['ContractDetails']['ContractLineItemDetails'])) {
            foreach ($request_data['ContractDetails']['ContractLineItemDetails'] as $key => $request_line_item) {
                // line item service start date => UTC time
                $service_start_date = !empty($request_line_item['ContractLineItemHeader']['service_start_date']) ? $request_line_item['ContractLineItemHeader']['service_start_date'] : '';
                if (!empty($service_start_date)) {
                    $service_start_date_timezone = !empty($request_line_item['ContractLineItemHeader']['service_start_date_timezone']) ? $request_line_item['ContractLineItemHeader']['service_start_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                    $service_start_date = new DateTime($service_start_date, new DateTimeZone($service_start_date_timezone));
                    $service_start_date = $service_start_date->format('c');
                    $service_start_date_tmp = $modUtils->ConvertDatetoGMTDate($service_start_date);

                    if ($service_start_date_tmp['result'] == false) {
                        $request_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineItemHeader']['service_start_date_invalid'] = $service_start_date_tmp['error_type'];
                    }

                    $request_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineItemHeader']['service_start_date'] = $service_start_date_tmp['date'];
                }

                // line item service end date => UTC time
                $service_end_date = !empty($request_line_item['ContractLineItemHeader']['service_end_date']) ? $request_line_item['ContractLineItemHeader']['service_end_date'] : '';
                if (!empty($service_end_date)) {
                    $service_end_date_timezone = !empty($request_line_item['ContractLineItemHeader']['service_end_date_timezone']) ? $request_line_item['ContractLineItemHeader']['service_end_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                    $service_end_date = new DateTime($service_end_date, new DateTimeZone($service_end_date_timezone));
                    $service_end_date = $service_end_date->format('c');
                    $service_end_date_tmp = $modUtils->ConvertDatetoGMTDate($service_end_date);

                    if ($service_end_date_tmp['result'] == false) {
                        $request_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineItemHeader']['service_end_date_invalid'] = $service_end_date_tmp['error_type'];
                    }
                    $request_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineItemHeader']['service_end_date'] = $service_end_date_tmp['date'];
                }

                // line item billing start date => UTC time
                $billing_start_date = !empty($request_line_item['ContractLineItemHeader']['billing_start_date']) ? $request_line_item['ContractLineItemHeader']['billing_start_date'] : '';
                if (!empty($billing_start_date)) {
                    $billing_start_date_timezone = !empty($request_line_item['ContractLineItemHeader']['billing_start_date_timezone']) ? $request_line_item['ContractLineItemHeader']['billing_start_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                    $billing_start_date = new DateTime($billing_start_date, new DateTimeZone($billing_start_date_timezone));
                    $billing_start_date = $billing_start_date->format('c');
                    $billing_start_date_tmp = $modUtils->ConvertDatetoGMTDate($billing_start_date);

                    if ($billing_start_date_tmp['result'] == false) {
                        $request_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineItemHeader']['billing_start_date_invalid'] = $billing_start_date_tmp['error_type'];
                    }
                    $request_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineItemHeader']['billing_start_date'] = $billing_start_date_tmp['date'];
                }

                // line item billing end date => UTC time
                $billing_end_date = !empty($request_line_item['ContractLineItemHeader']['billing_end_date']) ? $request_line_item['ContractLineItemHeader']['billing_end_date'] : '';
                if (!empty($billing_end_date)) {
                    $billing_end_date_timezone = !empty($request_line_item['ContractLineItemHeader']['billing_end_date_timezone']) ? $request_line_item['ContractLineItemHeader']['billing_end_date_timezone'] : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
                    $billing_end_date = new DateTime($billing_end_date, new DateTimeZone($billing_end_date_timezone));
                    $billing_end_date = $billing_end_date->format('c');
                    $billing_end_date_tmp = $modUtils->ConvertDatetoGMTDate($billing_end_date);

                    if ($billing_end_date_tmp['result'] == false) {
                        $request_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineItemHeader']['billing_end_date_invalid'] = $billing_end_date_tmp['error_type'];
                    }

                    $request_data['ContractDetails']['ContractLineItemDetails'][$key]['ContractLineItemHeader']['billing_end_date'] = $billing_end_date_tmp['date'];
                }
            }
        }

        // not passed $request_data in log cause it a huge data
        //ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        return $request_data;
    }

    /**
     * method to convert Local date to GMT date
     *
     * @param string $datetime local date
     * @param string $timezone timezone
     * @return string gmt date
     *
     * @author Rajul.Mondal
     * @since 11-Nov-2017
     */
    public static function convertLocalDataDatetoGMTDate($datetime, $timezone)
    {
        if (!empty($datetime) && !empty($timezone)) {
            $modUtils = new ModUtils();
            $datetime = new DateTime($datetime, new DateTimeZone($timezone));
            $datetime = $datetime->format('c');
            $datetime = $modUtils->ConvertDatetoGMTDate($datetime);
            if ($datetime['result'] == true) {
                return $datetime['date'];
            }
        }

        return false;
    }

    /**
     * method to convert GMT date to Local date
     *
     * @param string $datetime GMT date
     * @param string $timezone timezone
     * @return string local date
     *
     * @author Rajul.Mondal
     * @since 11-Nov-2017
     */
    public static function convertGMTDatetoLocalDataDate($datetime, $timezone)
    {
        if (!empty($datetime) && !empty($timezone)) {
            $modUtils = new ModUtils();
            $datetime = $modUtils->ConvertGMTDateToLocalDate($datetime, $timezone);
            if ($datetime['result'] == true) {
                return $datetime['datetime'];
            }
        }

        return false;
    }

    /**
     * Parse XML into Array
     *
     * @param $xml_data xml the request xml
     * @return array
     *
     * @author Rajul.Mondal
     * @since Sept 27, 2017
     */
    public static function convertPatchXmlToArray($xml_data, $global_contract_id, $global_contract_uuid)
    {
        global $patch_parse_xml_arr;
        if (empty($xml_data)) {
            return null;
        }
        require_once 'custom/service/v4_1_custom/APIHelper/v2/ClassContractUpdateXMLParseV2.php';
        $contract_xml_parse_obj = new ClassContractUpdateXMLParseV2();
        $xml_parse_arr = $contract_xml_parse_obj->getContractParseArray($xml_data);
        $patch_parse_xml_arr = $xml_parse_arr;
        require_once 'custom/service/v4_1_custom/APIHelper/v2/ClassReconstructPatchArray.php';
        $obj_reconstruct_patch_arr = new ClassReconstructPatchArray();
        $request_data = $obj_reconstruct_patch_arr->fnReconstructPatchArray($xml_parse_arr, $global_contract_id, $global_contract_uuid);
        return $request_data;
    }

    /**
     * XSD Validation
     *
     * @param $xml_data xml the request xml
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public static function xsdValidation($xml_data)
    {
        $xml_utils_obj = new XMLUtils();
        libxml_use_internal_errors(true);

        $dom_obj = new DOMDocument();
        $dom_obj->loadXML($xml_data);

        // XSD Validation
        if (!$dom_obj->schemaValidate('api/xml-schema/v2/sidem.xsd')) {
            return $xml_utils_obj->libxmlDisplayErrors();
        }

        return false;
    }

    /**
     * Check CIDAS connection failed
     *
     * @param $request_data array converted array from request xml
     * @param $error_code string expected error_code if CIDAS connection failed
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public static function cidasConnection($request_data, $error_code)
    {
        // Check CIDAS connection failed
        if (isset($request_data[self::KEY_CIDAS_FAILED]) && $request_data[self::KEY_CIDAS_FAILED] == '1' && strtolower($request_data['error_code']) == strtolower($error_code)) {
            return false;
        }

        return true;
    }

    /**
     * Check PMS connection failed
     *
     * @return boolean
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public static function pmsConnection()
    {
        require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';

        $has_offering_list = ApiValidationHelper::pmsProductOfferingList();
        if ($has_offering_list == false) {
            return false;
        }

        return true;
    }

    /**
     * Get PMS product offering list
     *
     * @return array list of product offering
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public static function pmsProductOfferingList()
    {
        require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';
        $objProduct = new ClassProductConfiguration();
        $offering_list = $objProduct->getProductOfferings(true);
        if ($offering_list['response_code'] == '0' || empty($offering_list['respose_array'])) {
            return false;
        }
        $_SESSION['pms_product_offering_list'] = $offering_list['respose_array'];

        return $_SESSION['pms_product_offering_list'];
    }

    /**
     * Get PMS product offering list
     *
     * @return array list of product offering
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public static function pmsProductOfferingDetails($pms_product_offering_id)
    {
        require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';
        $objProduct = new ClassProductConfiguration();

        $offeringDetails = $objProduct->getProductOfferingDetails($pms_product_offering_id);
        $_SESSION['product_offering_details'] = $offeringDetails['response_code'] == '1' ? $offeringDetails['respose_array'] : array();

        return $_SESSION['product_offering_details'];
    }

    /**
     * Get database contract details
     *
     * @param $global_contract_id string database contract global_contract_id
     * @param $global_contract_uuid string database contract global_contract_uuid
     * @param $version_number string database contract version_number
     * @param $interaction_status string database contract interaction_status
     * @return mixed contract details
     *
     * @author Rajul.Mondal
     * @since Sept 20, 2017
     */
    public static function dbContractDetails($global_contract_id, $global_contract_uuid, $version_number, $interaction_status, $request_type)
    {
        $contract_details = [
            'header' => [],
            'corporate' => [],
            'line_items' => [],
            'start_date_utc' => '',
            'end_date_utc' => '',
            'start_date_timezone' => '',
            'end_date_timezone' => '',
            'order' => [],
            'contract_customer_id' => [],
        ];
        // validating global_contract_id, global_contract_uuid
        if (empty($global_contract_id) && empty($global_contract_uuid)) {
            return $contract_details;
        }

        include_once 'custom/modules/gc_Contracts/CustomFunctionGcContract.php';
        $CustomFunctionGcContractObj = new CustomFunctionGcContract();

        // get database contact details
        $contract_header = $CustomFunctionGcContractObj->getContractHeader($global_contract_id, $global_contract_uuid, $version_number, $interaction_status);

        // get database contract corporate details
        $corporate = ApiValidationHelper::dbContractCorporate($contract_header);
        // convert database contract start date into UTC
        $start_date_utc = ApiValidationHelper::dbContractStartDateUtc($contract_header->contract_startdate, $contract_header->gc_timezone_id_c);
        // convert database contract end date into UTC
        $end_date_utc = ApiValidationHelper::dbContractEndDateUtc($contract_header->contract_enddate, $contract_header->gc_timezone_id1_c);
        // get database line items details
        $line_item_header = $CustomFunctionGcContractObj->getLineItemHeader($contract_header->id, 0, '', false);
        //Formate the Line Item as like requested xml array
        $line_items = ApiValidationHelper::formatContractLineItemDetails($line_item_header);
        // shot line item
        $line_item_order = $line_item_header['order'];
        ksort($line_item_order);

        $contract_details['header'] = $contract_header;
        $contract_details['corporate'] = $corporate;
        $contract_details['line_items'] = $line_items;
        $contract_details['start_date_utc'] = $start_date_utc;
        $contract_details['end_date_utc'] = $end_date_utc;
        $contract_details['order'] = $line_item_order;

        if ($request_type != 'get') {
            $contract_details['header']->contract_startdate = $start_date_utc;
            $contract_details['header']->contract_enddate = $end_date_utc;

            foreach ($contract_details['line_items'] as $line_item_key => $line_item_dtls) {
                $param['ContractDetails']['ContractLineItemDetails'][0] = $line_item_dtls;
                $contract_line_item = ApiValidationHelper::convertRequestDataDatetoGMTDate($param, 1);
                $contract_details['line_items'][$line_item_key] = $contract_line_item['ContractDetails']['ContractLineItemDetails'][0];
            }
        }

        $contract_details['start_date_timezone'] = $contract_header->gc_timezone_id_c;
        $contract_details['end_date_timezone'] = $contract_header->gc_timezone_id1_c;
        $contract_details['contract_customer_id'] = $corporate->common_customer_id_c;

        return $contract_details;
    }

    /**
     * Method to check whether country and state code combination is correct or not
     * @param string $country_code
     * @param string $state_code
     * @return boolean true if correct, false if not
     *
     * @author Sagar.Salunkhe
     * @since Aug 10, 2017
     */
    public static function checkCountryStateValidation($country_code, $state_code)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'country_code: ' . $country_code . ' ' . GCM_GL_LOG_VAR_SEPARATOR . ' state_code: ' . $state_code, 'Method to check whether country and state code combination is correct or not');
        /* BEGIN : validate country state combination for US and Canada */
        $obj_country_bean = BeanFactory::getBean('c_country');
        $obj_country_bean->retrieve_by_string_fields(array(
            'country_code_numeric' => $country_code));
        if ($country_code == '840' || $country_code == '124') {
            $obj_state_bean = BeanFactory::getBean('c_State');
            $obj_state_bean->retrieve_by_string_fields(array(
                'state_code' => $state_code));
            $obj_state_bean->retrieve($obj_state_bean->id);
            if ($obj_country_bean->id != $obj_state_bean->c_country_c_state_1c_country_ida) {
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'false', '');
                return false;
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'true', '');
        return true;
        /* END : validate country state combination for US and Canada */
    }

    /**
     * Method to format the line item details as like the request array. So that we can compare the difference
     *
     * @param array $line_item_header Array of requested line item
     * @return array Formatted line item array
     *
     * @author mohamed.siddiq
     * @since Aug 03, 2016
     */
    public static function formatContractLineItemDetails($line_item_header)
    {
        //ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'line_item_header: '.print_r($line_item_header,true), 'Method to format the line item details as like the request array. So that we can compare the difference');
        require_once 'custom/include/ModUtils.php';
        $modUtils = new ModUtils();

        $all_lineitems = array();
        if (!empty($line_item_header)) {
            foreach ($line_item_header as $key => $value) {
                if ($key != 'order') {
                    $curr_lineitem = array();

                    $curr_lineitem['ContractLineItemHeader']['service_start_date'] = $value['service_start_date'];
                    $curr_lineitem['ContractLineItemHeader']['service_start_date_xml_id'] = $value['service_start_date_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['billing_start_date'] = $value['billing_start_date'];
                    $curr_lineitem['ContractLineItemHeader']['billing_start_date_xml_id'] = $value['billing_start_date_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['service_end_date'] = $value['service_end_date'];
                    $curr_lineitem['ContractLineItemHeader']['service_end_date_xml_id'] = $value['service_end_date_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['billing_end_date'] = $value['billing_end_date'];
                    $curr_lineitem['ContractLineItemHeader']['billing_end_date_xml_id'] = $value['billing_end_date_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['service_start_date_timezone'] = $modUtils->checkAndSetDefaultTimezone($value['service_start_date_timezone_actual']);
                    $curr_lineitem['ContractLineItemHeader']['billing_start_date_timezone'] = $modUtils->checkAndSetDefaultTimezone($value['billing_start_date_timezone_actual']);
                    $curr_lineitem['ContractLineItemHeader']['service_end_date_timezone'] = $modUtils->checkAndSetDefaultTimezone($value['service_end_date_timezone_actual']);
                    $curr_lineitem['ContractLineItemHeader']['billing_end_date_timezone'] = $modUtils->checkAndSetDefaultTimezone($value['billing_end_date_timezone_actual']);

                    $curr_lineitem['ContractLineItemHeader']['factory_reference_no'] = $value['factory_reference_no'];
                    $curr_lineitem['ContractLineItemHeader']['factory_reference_no_xml_id'] = $value['factory_reference_no_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['product_specification'] = $value['product_specification_id'];
                    $curr_lineitem['ContractLineItemHeader']['product_specification_xml_id'] = $value['product_specification_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['cust_contract_currency'] = $value['cust_contract_currency_code'];
                    $curr_lineitem['ContractLineItemHeader']['cust_contract_currency_xml_id'] = $value['cust_contract_currency_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['cust_billing_currency'] = $value['cust_billing_currency_code'];
                    $curr_lineitem['ContractLineItemHeader']['cust_billing_currency_xml_id'] = $value['cust_billing_currency_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['igc_contract_currency'] = $value['igc_contract_currency_code'];
                    $curr_lineitem['ContractLineItemHeader']['igc_contract_currency_xml_id'] = $value['igc_contract_currency_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['igc_billing_currency'] = $value['igc_billing_currency_code'];
                    $curr_lineitem['ContractLineItemHeader']['igc_billing_currency_xml_id'] = $value['igc_billing_currency_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['customer_billing_method'] = $value['customer_billing_method_value'];
                    $curr_lineitem['ContractLineItemHeader']['customer_billing_method_xml_id'] = $value['customer_billing_method_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['igc_settlement_method'] = $value['igc_settlement_method_value'];
                    $curr_lineitem['ContractLineItemHeader']['igc_settlement_method_xml_id'] = $value['igc_settlement_method_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['description'] = $value['description'];
                    $curr_lineitem['ContractLineItemHeader']['description_xml_id'] = $value['description_uuid'];

                    $curr_lineitem['ContractLineItemHeader']['contract_line_item_type'] = $value['contract_line_item_type'];
                    $curr_lineitem['ContractLineItemHeader']['line_item_id_xml_id'] = $value['line_item_id_uuid'];
                    $curr_lineitem['ContractLineItemHeader']['payment_type'] = $value['payment_type_id'];

                    //Invoice Group Details
                    $curr_lineitem['InvoiceGroupDtls']['name'] = $value['group_name'];
                    $curr_lineitem['InvoiceGroupDtls']['name_xml_id'] = $value['group_name_uuid'];

                    $curr_lineitem['InvoiceGroupDtls']['description'] = $value['group_description'];
                    $curr_lineitem['InvoiceGroupDtls']['description_xml_id'] = $value['group_description_uuid'];

                    //Contract Line Product Configuration
                    $all_li_prod_config = array();
                    if (isset($value['product_config']) && !empty($value['product_config'])) {
                        foreach ($value['product_config'] as $prod_config) {
                            $lineitem_prod_config = array();
                            $lineitem_prod_config['name'] = $prod_config['name'];
                            $lineitem_prod_config['characteristic_value'] = $prod_config['characteristic_value'];
                            $lineitem_prod_config['characteristic_xml_id'] = $prod_config['characteristic_uuid'];
                            $all_li_prod_config[$prod_config['name']] = $lineitem_prod_config;

                        }
                    }
                    $curr_lineitem['ContractLineProductConfiguration'] = $all_li_prod_config;

                    //Contract Line Product Rule
                    $all_li_prod_rule = array();
                    if (isset($value['rules']) && !empty($value['rules'])) {
                        foreach ($value['rules'] as $prod_rule) {
                            $lineitem_prod_rule = array();
                            $lineitem_prod_rule['name'] = $prod_rule['ruleid'];
                            $lineitem_prod_rule['description'] = $prod_rule['rule_value'];
                            $lineitem_prod_rule['rule_xml_id'] = $prod_rule['rule_uuid'];
                            $all_li_prod_rule[$prod_rule['ruleid']] = $lineitem_prod_rule;

                        }
                    }
                    $curr_lineitem['ContractLineProductRule'] = (!empty($all_li_prod_rule)) ? $all_li_prod_rule : '';

                    //Contract Line Product Price
                    $all_li_prod_price = array();
                    if (isset($value['product_price']) && !empty($value['product_price'])) {
                        foreach ($value['product_price'] as $prod_price) {
                            $lineitem_prod_price = array();
                            $lineitem_prod_price['name'] = $prod_price['pricelineid'];
                            $lineitem_prod_price['charge_xml_id'] = $prod_price['charge_uuid'];

                            $lineitem_prod_price['xml_data']['currencyid']['xml_id'] = $prod_price['currencyid_uuid'];
                            $lineitem_prod_price['xml_data']['currencyid']['xml_value'] = $prod_price['currencyid'];

                            $lineitem_prod_price['xml_data']['icb_flag']['xml_id'] = $prod_price['icb_flag_uuid'];
                            $lineitem_prod_price['xml_data']['icb_flag']['xml_value'] = $prod_price['icb_flag_boolean'];

                            $lineitem_prod_price['xml_data']['charge_type']['xml_id'] = $prod_price['charge_type_uuid'];
                            $lineitem_prod_price['xml_data']['charge_type']['xml_value'] = $prod_price['charge_type'];

                            $lineitem_prod_price['xml_data']['charge_period']['xml_id'] = $prod_price['charge_period_uuid'];
                            $lineitem_prod_price['xml_data']['charge_period']['xml_value'] = $prod_price['charge_period'];

                            if ($prod_price['icb_flag'] == 1) {
                                $lineitem_prod_price['xml_data']['list_price']['xml_id'] = '';
                                $lineitem_prod_price['xml_data']['list_price']['xml_value'] = '';

                                $lineitem_prod_price['xml_data']['discount_rate']['xml_id'] = '';
                                $lineitem_prod_price['xml_data']['discount_rate']['xml_value'] = '';

                                $lineitem_prod_price['xml_data']['discount_amount']['xml_id'] = '';
                                $lineitem_prod_price['xml_data']['discount_amount']['xml_value'] = '';
                            } else {
                                $lineitem_prod_price['xml_data']['list_price']['xml_id'] = $prod_price['list_price_uuid'];
                                $lineitem_prod_price['xml_data']['list_price']['xml_value'] = $prod_price['list_price'];

                                $lineitem_prod_price['xml_data']['discount_rate']['xml_id'] = $prod_price['discount_rate_uuid'];
                                $lineitem_prod_price['xml_data']['discount_rate']['xml_value'] = ($prod_price['percent_discount_flag']) ? $prod_price['discount'] : '0'; //if it is percentage discount then set value here

                                $lineitem_prod_price['xml_data']['discount_amount']['xml_id'] = $prod_price['discount_amount_uuid'];
                                $lineitem_prod_price['xml_data']['discount_amount']['xml_value'] = (!$prod_price['percent_discount_flag']) ? $prod_price['discount'] : '0'; //if it is not percentage discount then set value here
                            }

                            $lineitem_prod_price['xml_data']['customer_contract_price']['xml_id'] = $prod_price['customer_contract_price_uuid'];
                            $lineitem_prod_price['xml_data']['customer_contract_price']['xml_value'] = $prod_price['charge_price'];

                            $lineitem_prod_price['xml_data']['igc_settlement_price']['xml_id'] = $prod_price['igc_settlement_price_uuid'];
                            $lineitem_prod_price['xml_data']['igc_settlement_price']['xml_value'] = $prod_price['igc_settlement_price'];
                            $all_li_prod_price[$prod_price['pricelineid']] = $lineitem_prod_price;

                        }
                    }
                    $curr_lineitem['ContractLineProductPrice'] = $all_li_prod_price;

                    //Payment details

                    if (isset($value['payment_type_id']) && $value['payment_type_id'] != '') {
                        switch ($value['payment_type_id']) {
                            case self::DIRECT_DEPOSIT:
                                $curr_lineitem['ContractDirectDeposit']['bank_code'] = $value['deposit_deposit_details']['bank_code'];
                                $curr_lineitem['ContractDirectDeposit']['branch_code'] = $value['deposit_deposit_details']['branch_code'];
                                $curr_lineitem['ContractDirectDeposit']['deposit_type'] = $value['deposit_deposit_details']['deposit_type_value'];
                                $curr_lineitem['ContractDirectDeposit']['payee_contact_person_name_1'] = $value['deposit_deposit_details']['person_name_1'];
                                $curr_lineitem['ContractDirectDeposit']['payee_contact_person_name_2'] = $value['deposit_deposit_details']['person_name_2'];
                                $curr_lineitem['ContractDirectDeposit']['payee_email_address'] = $value['deposit_deposit_details']['payee_email_address'];
                                $curr_lineitem['ContractDirectDeposit']['remarks'] = $value['deposit_deposit_details']['remarks'];
                                $curr_lineitem['ContractDirectDeposit']['payee_tel_no'] = $value['deposit_deposit_details']['payee_tel_no'];
                                $curr_lineitem['ContractDirectDeposit']['account_no'] = $value['deposit_deposit_details']['account_no'];
                                $curr_lineitem['ContractDirectDeposit']['payment_type'] = $value['payment_type_id'];
                                break;

                            case self::ACCOUNT_TRANSFER:
                                $curr_lineitem['ContractAccountTransfer']['name'] = $value['acc_trans_details']['name'];
                                $curr_lineitem['ContractAccountTransfer']['bank_code'] = $value['acc_trans_details']['bank_code'];
                                $curr_lineitem['ContractAccountTransfer']['branch_code'] = $value['acc_trans_details']['branch_code'];
                                $curr_lineitem['ContractAccountTransfer']['deposit_type'] = $value['acc_trans_details']['deposit_type_value'];
                                $curr_lineitem['ContractAccountTransfer']['account_no'] = $value['acc_trans_details']['account_no'];
                                $curr_lineitem['ContractAccountTransfer']['account_no_display'] = $value['acc_trans_details']['account_no_display_value'];
                                $curr_lineitem['ContractAccountTransfer']['payment_type'] = $value['payment_type_id'];
                                break;

                            case self::POSTAL_TRANSFER:
                                $curr_lineitem['ContractPostalTransfer']['name'] = $value['postal_trans_details']['name'];
                                $curr_lineitem['ContractPostalTransfer']['postal_passbook_mark'] = $value['postal_trans_details']['postal_passbook_mark'];
                                $curr_lineitem['ContractPostalTransfer']['postal_passbook'] = $value['postal_trans_details']['postal_passbook'];
                                $curr_lineitem['ContractPostalTransfer']['postal_passbook_no_display'] = $value['postal_trans_details']['postal_passbook_no_display_value'];
                                $curr_lineitem['ContractPostalTransfer']['payment_type'] = $value['payment_type_id'];
                                break;

                            case self::CREDIT_CARD:
                                $curr_lineitem['ContractCreditCard']['credit_card_no'] = $value['credit_card_details']['credit_card_no'];
                                $curr_lineitem['ContractCreditCard']['expiration_date'] = $value['credit_card_details']['expiration_date'];
                                $curr_lineitem['ContractCreditCard']['payment_type'] = $value['payment_type_id'];
                                break;
                            default:
                                // Do nothing
                                break;
                        }
                    }
                    $all_lineitems[] = $curr_lineitem;
                }

            }

        }
        //ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'all_lineitems: '.print_r($all_lineitems,true), '');
        return $all_lineitems;
    }

    /**
     * Method to format the line item details as like the request array. So that we can compare the difference
     *
     * @param array $line_items Array of requested line item
     * @return array Formatted line item array
     *
     * @author mohamed.siddiq
     * @since Aug 03, 2016
     */
    public static function fetchAvailableLItemDetails($line_items)
    {
        $ret_arr = array();
        if (!empty($line_items)) {
            foreach ($line_items as $line_item) {
                $product_specification_xml_id = $line_item['ContractLineItemHeader']['product_specification_xml_id'];
                $ret_arr['product_specification_xml_ids'][$product_specification_xml_id] = $product_specification_xml_id;

                $product_specification_id = $line_item['ContractLineItemHeader']['product_specification'];
                $ret_arr['product_specifications'][$product_specification_id] = $product_specification_id;

                $line_item_id_xml_id = $line_item['ContractLineItemHeader']['line_item_id_xml_id'];
                $ret_arr['line_item_id_xml_ids'][$line_item_id_xml_id] = $line_item_id_xml_id;

                $ret_arr[$line_item_id_xml_id] = $line_item;
            }
        }

        return $ret_arr;
    }

    /**
     * Get database contract header corporate
     *
     * @param $contract object database contract data
     * @return object contract header corporate
     *
     * @author Rajul.Mondal
     * @since Sept 20, 2017
     */
    public static function dbContractCorporate($contract)
    {
        $contract_corporate = null;
        // validate global_contract_id && global_contract_uuid
        if (empty($contract)) {
            return $contract_corporate;
        }

        $corporate = $contract->get_linked_beans('contacts_gc_contracts_1', 'Contacts');
        if (count($corporate) == 1 && isset($corporate[0]) && is_object($corporate[0])) {
            $corporate_bean = BeanFactory::getBean('Accounts'); // Corporate details
            $corporate_bean->retrieve($corporate[0]->account_id);
            if ($corporate_bean->common_customer_id_c != '') {
                $contract_corporate = $corporate_bean;
            }
        }

        return $contract_corporate;
    }

    /**
     * Get database contract start date in UTC format
     *
     * @param $contract_start_date string contract start date
     * @param $contract_start_date_timezone string contract start date timezone
     * @return string contract start date in UTC format
     *
     * @author Rajul.Mondal
     * @since Sept 20, 2017
     */
    public static function dbContractStartDateUtc($contract_start_date, $contract_start_date_timezone)
    {
        global $sugar_config;
        require_once 'custom/include/ModUtils.php';
        $modUtils = new ModUtils();

        $start_date_timezone = '';
        if ($contract_start_date_timezone != '') {
            $timezone = BeanFactory::getBean('gc_TimeZone', $contract_start_date_timezone);
            $start_date_timezone = $timezone->utcoffset;
        }
        //set default timezone if the timezone is empty
        $start_date_timezone = $modUtils->checkAndSetDefaultTimezone($start_date_timezone);

        // converting contract_start_date datetime => UTC time
        if (!empty($contract_start_date)) {
            $start_date_timezone = !empty($start_date_timezone) ? $start_date_timezone : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $contract_start_date = new DateTime($contract_start_date, new DateTimeZone($start_date_timezone));
            $contract_start_date = $contract_start_date->format('c');
            $contract_start_date = $modUtils->ConvertDatetoGMTDate($contract_start_date);
            $contract_start_date = $contract_start_date['date'];
        }

        return $contract_start_date;
    }

    /**
     * Get database contract end date in UTC format
     *
     * @param $contract_end_date string contract end date
     * @param $contract_end_date_timezone string contract end date timezone
     * @return string contract end date in UTC format
     *
     * @author Rajul.Mondal
     * @since Sept 20, 2017
     */
    public static function dbContractEndDateUtc($contract_end_date, $contract_end_date_timezone)
    {
        global $sugar_config;
        require_once 'custom/include/ModUtils.php';
        $modUtils = new ModUtils();

        $start_end_timezone = '';
        if ($contract_end_date_timezone != '') {
            $timezone = BeanFactory::getBean('gc_TimeZone', $contract_end_date_timezone);
            $start_end_timezone = $timezone->utcoffset;
        }
        //set default timezone if the timezone is empty
        $start_end_timezone = $modUtils->checkAndSetDefaultTimezone($start_end_timezone);

        // converting contract_end_date datetime => UTC time
        if (!empty($contract_end_date)) {
            $start_end_timezone = !empty($start_end_timezone) ? $start_end_timezone : $sugar_config['gcm_cstm_const']['timezone']['gmt'];
            $contract_end_date = new DateTime($contract_end_date, new DateTimeZone($start_end_timezone));
            $contract_end_date = $contract_end_date->format('c');
            $contract_end_date = $modUtils->ConvertDatetoGMTDate($contract_end_date);
            $contract_end_date = $contract_end_date['date'];
        }

        return $contract_end_date;
    }

    /**
     * generate error details with dynamic xpath from config/api_config
     *
     * @param $api_config_details array details of the xpath key
     * @param $xpath_ids array key value pairs of xpath ids which going to replace with
     * @return array
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public static function generateError($api_config_details, $xpath_ids)
    {
        global $api_config;
        $error_details = [];
        // validate api_config_details or xpath ids empty
        if (!empty($api_config_details) && !is_array($xpath_ids)) {
            return false;
        }

        $xpath_details = ApiValidationHelper::getXpathDetails($api_config_details, $xpath_ids);

        // validate xpath details avilable in config/api_config
        if (empty($xpath_details)) {
            return false;
        }
        // validate xpath code avilable in config/api_config
        if (empty($xpath_details['code'])) {
            return false;
        }
        // validate xpath code avilable in config/api_config
        if (empty($xpath_details['message'])) {
            return false;
        }

        $error_details['codes'] = $xpath_details['code'];
        $error_details['messages'] = $xpath_details['message'];
        $error_details['details'] = $xpath_details['details'];

        return $error_details;
    }

    /**
     * generate priority list from config/api_config
     *
     * @param $http_method string get|post|version_up|patch|idwh|gbs
     * @return array
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public static function generatePriorityList($http_method)
    {
        global $api_config, $api_middleware;
        $priority_list = [];

        if (empty($http_method)) {
            return $priority_list;
        }

        foreach ($api_config['xpath'] as $priority_level => $priority_details) {
            foreach ($priority_details as $group_level => $group_details) {
                foreach ($group_details as $field_details) {
                    if ($field_details['ignore'] == true) {
                        continue;
                    }

                    $field_name = preg_replace('/\s+/', self::SEPARATOR_UNDERSCORE, trim($field_details['field']));
                    $slug = preg_replace('/\s+/', self::SEPARATOR_UNDERSCORE, trim($field_details['slug']));
                    $field_name .= !empty(trim($slug)) ? self::SEPARATOR_UNDERSCORE . $slug : '';
                    $methods = explode(self::SEPARATOR_PIPE, $field_details['methods']);
                    foreach ($methods as $method) {
                        if (strtolower(trim($http_method)) == strtolower(trim($method))) {

                            $middleware_methods = '';
                            $current_priority = $priority_level + 1 . $group_level + 1;

                            // find middleware from config/api_middleware
                            foreach ($api_middleware as $middleware) {
                                $middleware_field_name = preg_replace('/\s+/', self::SEPARATOR_UNDERSCORE, trim($middleware['field']));
                                $middleware_slug = preg_replace('/\s+/', self::SEPARATOR_UNDERSCORE, trim($middleware['slug']));
                                $middleware_field_name .= !empty(trim($middleware_slug)) ? self::SEPARATOR_UNDERSCORE . $middleware_slug : '';
                                $middleware_field_name = strtolower(trim($middleware_field_name));
                                $config_field_name = strtolower(trim($field_name));
                                $trigger = strtolower(trim($middleware['trigger']));
                                if ($middleware_field_name == $config_field_name && !empty($trigger) && $trigger == 'before' && strtolower(trim($field_details['type'])) == strtolower(trim($middleware['type']))) {
                                    $middleware_methods = explode(self::SEPARATOR_PIPE, $middleware['methods']);
                                    // add before middleware in priority_list
                                    if (in_array(strtolower(trim($http_method)), $middleware_methods)) {
                                        $before_trigger_function_name = strtolower(trim($http_method) . self::SEPARATOR_UNDERSCORE . trim($middleware['trigger']) . self::SEPARATOR_UNDERSCORE . trim($middleware['type']) . self::SEPARATOR_UNDERSCORE . trim($field_name));
                                        // before middle ware
                                        $priority_list[$current_priority][] = self::toCamelCase($before_trigger_function_name);
                                    }
                                }
                            }
                            $function_name = strtolower(trim($method) . self::SEPARATOR_UNDERSCORE . $field_details['layer'] . self::SEPARATOR_UNDERSCORE . $field_details['type'] . self::SEPARATOR_UNDERSCORE . $field_name);
                            // add method in priority_list
                            $priority_list[$current_priority][] = self::toCamelCase($function_name);

                            // add after middleware in priority_list
                            foreach ($api_middleware as $middleware) {
                                $middleware_field_name = preg_replace('/\s+/', self::SEPARATOR_UNDERSCORE, trim($middleware['field']));
                                $middleware_slug = preg_replace('/\s+/', self::SEPARATOR_UNDERSCORE, trim($middleware['slug']));
                                $middleware_field_name .= !empty(trim($middleware_slug)) ? self::SEPARATOR_UNDERSCORE . $middleware_slug : '';
                                $middleware_field_name = strtolower(trim($middleware_field_name));
                                $config_field_name = strtolower(trim($field_name));
                                $trigger = strtolower(trim($middleware['trigger']));
                                if ($middleware_field_name == $config_field_name && !empty($trigger) && $trigger == 'after' && strtolower(trim($field_details['type'])) == strtolower(trim($middleware['type']))) {
                                    $middleware_methods = explode(self::SEPARATOR_PIPE, $middleware['methods']);
                                    // add after middleware in priority_list
                                    if (in_array(strtolower(trim($http_method)), $middleware_methods)) {
                                        $after_trigger_function_name = strtolower(trim($http_method) . self::SEPARATOR_UNDERSCORE . trim($middleware['trigger']) . self::SEPARATOR_UNDERSCORE . trim($middleware['type']) . self::SEPARATOR_UNDERSCORE . trim($field_name));
                                        // after middle ware
                                        $priority_list[$current_priority][] = self::toCamelCase($after_trigger_function_name);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $priority_list;
    }

    /**
     * Translates a camel case string into a string with underscores (e.g. firstName > first_name)
     *
     * @param string $str String in camel case format
     * @return string $str Translated into underscore format
     *
     * @author Rajul.Mondal
     * @since Sept 19, 2017
     */
    public static function fromCamelCase($str)
    {
        $str[0] = strtolower($str[0]);
        return preg_replace_callback('/([A-Z])/', function ($m) {
            return '_' . strtolower($m[1]);
        }, $str);
    }

    /**
     * Translates a string with underscores into camel case (e.g. first_name > firstName)
     *
     * @param string $str String in underscore format
     * @param boolean $capitalise_first_char If true, capitalise the first char in $str
     * @return string $str translated into camel caps
     *
     * @author Rajul.Mondal
     * @since Sept 19, 2017
     */
    public static function toCamelCase($str, $capitalise_first_char = false)
    {
        if ($capitalise_first_char) {
            $str[0] = strtoupper($str[0]);
        }
        return preg_replace_callback('/_([a-z])/', function ($m) {
            return strtoupper($m[1]);
        }, $str);
    }

    /**
     * get dynamic xpath details from config/api_config
     *
     * @param $api_config_details array details of the field
     * @param $xpath_ids array key value pair of xpath ids
     * @return array
     *
     * @author Rajul.Mondal
     * @since Sept 04, 2017
     */
    public static function getXpathDetails($api_config_details, $xpath_ids)
    {
        global $api_config;
        $xpath_details = '';

        if (empty($api_config_details) || !is_array($xpath_ids)) {
            return false;
        }

        foreach ($api_config['xpath'] as $priority_level => $priority_details) {
            foreach ($priority_details as $group_level => $group_details) {
                foreach ($group_details as $field_details) {

                    $field_name = preg_replace('/\s+/', self::SEPARATOR_UNDERSCORE, trim($field_details['field']));
                    $slug = preg_replace('/\s+/', self::SEPARATOR_UNDERSCORE, trim($field_details['slug']));
                    $field_name .= !empty(trim($slug)) ? self::SEPARATOR_UNDERSCORE . $slug : '';
                    $type = $field_details['type'];
                    $method_details = explode('|', strtolower($field_details['methods']));

                    if (strtolower($field_name) == strtolower($api_config_details['field']) && strtolower($type) == strtolower($api_config_details['type']) && in_array(strtolower($api_config_details['methods']), $method_details)) {
                        $xpath = trim($field_details['details']);
                        foreach ($xpath_ids as $xpath_name => $xpath_details) {
                            $search = self::SEPARATOR_BRACES_OPEN . $xpath_name . self::SEPARATOR_BRACES_CLOSE;
                            if (!empty($xpath_details) && is_array($xpath_details)) {
                                $xpath_details = current($xpath_details);
                            }
                            $replace_with = $xpath_details;
                            $xpath = str_replace($search, $replace_with, $xpath);
                        }

                        $xpath_details = [
                            'code' => $field_details['code'],
                            'message' => trim($field_details['message']),
                            'details' => $xpath,
                        ];
                    }

                }
            }
        }

        return $xpath_details;
    }

    /**
     * Reconstruct xml array to add missing configuration, rules, price
     *
     * @param $data array request_xml, configuration, rule, price,
     * @return array
     *
     * @author Rajul.Mondal
     * @since Oct 11, 2017
     */
    public static function reconstractRequestDataAfterPmsValidationSuccess($data)
    {
        $request_data = $data['request_data'];
        $xml_new_configuration_ids = $data['xml_new_configuration_ids'];
        $xml_new_rule_ids = $data['xml_new_rule_ids'];
        $xml_price_ids = $data['xml_price_ids'];
        // line items
        $xml_line_items = $request_data['ContractDetails']['ContractLineItemDetails'];

        foreach ($xml_line_items as $line_item_index => $line_item) {
            $xml_specification_id = $line_item['ContractLineItemHeader']['product_specification'];
            // populate new configuration
            if (!empty($xml_new_configuration_ids[$xml_specification_id . "_" . $line_item_index])) {
                foreach ($xml_new_configuration_ids[$xml_specification_id . "_" . $line_item_index] as $new_configuration_key => $new_configuration_value) {
                    $line_item['ContractLineProductConfiguration'][] = [
                        'is_new_record' => true,
                        'name' => $new_configuration_key,
                        'characteristic_value' => $new_configuration_value['configuration_value'],
                        'characteristic_xml_id' => 'AdditionalConfigurations_' . $xml_specification_id . '_' . $new_configuration_key,
                    ];
                }
            }
            // populate new rules
            if (!empty($xml_new_rule_ids[$xml_specification_id . "_" . $line_item_index])) {
                foreach ($xml_new_rule_ids[$xml_specification_id . "_" . $line_item_index] as $new_rule_key => $new_rule_value) {
                    $line_item['ContractLineProductRule'][] = [
                        'is_new_record' => true,
                        'name' => $new_rule_key,
                        'description' => $new_rule_value['rule_value'],
                        'rule_xml_id' => 'AdditionalRules_' . $xml_specification_id . '_' . $new_rule_key,
                    ];
                }
            }
            // populate new price
            if (!empty($xml_price_ids[$xml_specification_id . "_" . $line_item_index])) {
                $line_item['ContractLineProductPrice'] = array();
                foreach ($xml_price_ids[$xml_specification_id . "_" . $line_item_index] as $new_price_key => $new_price_value) {
                    $new_price = [
                        'name' => (string)$new_price_key,
                        'charge_xml_id' => !empty($new_price_value['charge_xml_id']) ? $new_price_value['charge_xml_id'] : 'Price_' . $xml_specification_id . '_' . $new_price_key,
                        'is_new_record' => isset($new_price_value['is_new_record']) ? $new_price_value['is_new_record'] : '',
                        'charge_type' => (string)$new_price_value['charge_type'],
                        'charge_period' => (string)$new_price_value['charge_period'],
                        'list_price' => (string)$new_price_value['list_price'],
                        'icb_flag' => (string)$new_price_value['icb_flag'],
                        'percent_discount_flag' => (string)$new_price_value['percent_discount_flag'],
                        'discount' => (string)$new_price_value['discount'],
                        'customer_contract_price' => (string)$new_price_value['customer_contract_price'],
                        'igc_settlement_price' => (string)$new_price_value['igc_settlement_price'],
                        'currencyid' => (string)$new_price_value['currencyid'],

                        'charge_type_xml_id' => !empty($new_price_value['charge_type_xml_id']) ? $new_price_value['charge_type_xml_id'] : 'ChargeType_' . $xml_specification_id . '_' . $new_price_key,
                        'charge_period_xml_id' => !empty($new_price_value['charge_period_xml_id']) ? $new_price_value['charge_period_xml_id'] : 'ChargePeriod_' . $xml_specification_id . '_' . $new_price_key,
                        'list_price_xml_id' => !empty($new_price_value['list_price_xml_id']) ? $new_price_value['list_price_xml_id'] : 'ListPrice_' . $xml_specification_id . '_' . $new_price_key,
                        'icb_flag_xml_id' => !empty($new_price_value['icb_flag_xml_id']) ? $new_price_value['icb_flag_xml_id'] : 'IcbFlag_' . $xml_specification_id . '_' . $new_price_key,
                        'discount_rate_xml_id' => !empty($new_price_value['discount_rate_xml_id']) ? $new_price_value['discount_rate_xml_id'] : 'DiscountRate_' . $xml_specification_id . '_' . $new_price_key,
                        'discount_amount_xml_id' => !empty($new_price_value['discount_amount_xml_id']) ? $new_price_value['discount_amount_xml_id'] : 'DiscountAmount_' . $xml_specification_id . '_' . $new_price_key,
                        'customer_contract_price_xml_id' => !empty($new_price_value['customer_contract_price_xml_id']) ? $new_price_value['customer_contract_price_xml_id'] : 'CustomerContractPrice_' . $xml_specification_id . '_' . $new_price_key,
                        'igc_settlement_price_xml_id' => !empty($new_price_value['igc_settlement_price_xml_id']) ? $new_price_value['igc_settlement_price_xml_id'] : 'IgcSettlementPrice_' . $xml_specification_id . '_' . $new_price_key,
                        'currencyid_xml_id' => !empty($new_price_value['currencyid_xml_id']) ? $new_price_value['currencyid_xml_id'] : 'CurrencyId_' . $xml_specification_id . '_' . $new_price_key,
                    ];
                    $line_item['ContractLineProductPrice'][] = $new_price;
                }
            }
            $request_data['ContractDetails']['ContractLineItemDetails'][$line_item_index] = $line_item;
        }

        return $request_data;
    }

    /**
     * Method to format the PMS ID
     *
     * @param string $pms_id PMS ID string
     * @param int $type Used to indicate the prefix value in the format, the default value is 1
     * @return string Formatted PMS ID string value.
     *
     * @author Rajul.Mondal
     * @since Oct 16, 2016
     */
    public static function formatPMSID($pms_id, $type)
    {
        //ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'pms_id : ' . $pms_id . GCM_GL_LOG_VAR_SEPARATOR . 'type : ' . $type, 'Method to format the PMS ID');
        $prefix = '00000000-0000-0000-0000-';
        if ($type == 2) {
            $prefix = '00000001-0000-0000-0000-';
        }

        $pms_id = $prefix . sprintf('%012s', $pms_id);

        //ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'pms_id : ' . $prefix . sprintf('%012s', $pms_id), '');
        return $pms_id;
    }

    /**
     * Method to merge array
     *
     * @param array $Arr1 array in which we need to merge
     * @param array $Arr2 array to be merge
     * @param string $level dimension levels codes|messages|details
     * @return array
     *
     * @author Rajul.Mondal
     * @since Oct 24, 2017
     */
    public static function mergeArrays($Arr1, $Arr2, $level = '')
    {
        foreach ($Arr2 as $key => $Value) {
            if ($key == 'codes' && is_array($Value)) {
                $codes = ApiValidationHelper::MergeArrays($Arr1['codes'], $Arr2['codes'], 'codes');
                $Arr1['codes'] = $codes;
            } elseif ($key == 'messages' && is_array($Value)) {
                $messages = ApiValidationHelper::MergeArrays($Arr1['messages'], $Arr2['messages'], 'messages');
                $Arr1['messages'] = $messages;
            } elseif ($key == 'details' && is_array($Value)) {
                $details = ApiValidationHelper::MergeArrays($Arr1['details'], $Arr2['details'], 'details');
                $Arr1['details'] = $details;
            } elseif (array_key_exists($key, $Arr1) && is_array($Value)) {
                $inner = ApiValidationHelper::MergeArrays($Arr1[$key], $Arr2[$key], $level);
                if (isset($Arr1[$key])) {
                    $Arr1[$key][] = $inner;
                } else {
                    $Arr1[$key] = $inner;
                }
            } else {
                if ($level == 'codes' && isset($Arr1[$key])) {
                    $Arr1[] = $Value;
                } else {
                    $Arr1[$key] = $Value;
                }

            }
        }
        return $Arr1;
    }

    /**
     * API Beluga validation for billing account, billing start date, billing end date and payment type.
     *
     * @param string $global_contract_id_uuid
     * @return array
     *
     * @author Dinesh Itkar
     * @since Feb 02, 2018
     */
    public static function validateDbBillingAccountForBelugaProduct($global_contract_id_uuid, $xml_empty_fields, $xml_billing_country, $isGBSBilling_xml){
        $billing_acc_validation_arr = array();
        $gc_contract_bean_obj = BeanFactory::getBean('gc_Contracts');

        if(!empty($global_contract_id_uuid)){
            $contract_bean_result = $gc_contract_bean_obj->retrieve_by_string_fields(array(
                'global_contract_id_uuid' => $global_contract_id_uuid,
            ));
            $contract_id = $gc_contract_bean_obj->id;
        }

        if(!empty($contract_bean_result->product_offering)){
            require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';
            $product_config_obj = new ClassProductConfiguration();
            $product_offering_detail_arr = $product_config_obj->getProductOfferingDetails($contract_bean_result->product_offering);
            if(
                !empty($product_offering_detail_arr) &&
                isset($product_offering_detail_arr['response_code']) &&
                $product_offering_detail_arr['response_code'] == 1 &&
                isset($product_offering_detail_arr['respose_array']['listofferingoption']['beluga_product_type']) &&
                isset($product_offering_detail_arr['respose_array']['listofferingoption']['beluga_service_type']) &&
                !empty($product_offering_detail_arr['respose_array']['listofferingoption']['beluga_product_type']) &&
                !empty($product_offering_detail_arr['respose_array']['listofferingoption']['beluga_service_type'])
                ){
                    include_once 'custom/modules/gc_Contracts/CustomFunctionGcContract.php';
                    $CustomFunctionGcContractObj = new CustomFunctionGcContract();
                    $line_item_details_arr = $CustomFunctionGcContractObj->getLineItemHeader($contract_id, false, $contract_bean_result->product_offering, false);

                    if(!empty($line_item_details_arr)){
                        foreach($line_item_details_arr['order'] as $key => $value){
                            $line_item_uuid = $line_item_details_arr[$value]['line_item_id_uuid'];
                            $country = !empty($line_item_details_arr[$value]['bill_account_country_code_alphabet']) ? $line_item_details_arr[$value]['bill_account_country_code_alphabet'] : '';
                            $db_billing_country = '';

							// check if line item has customer_billing_method = 'GBS_Billing' in DB
							$isGBSBilling_db = false;
							if(isset($line_item_details_arr[$value]['customer_billing_method_value']) && $line_item_details_arr[$value]['customer_billing_method_value'] == 'GBS_Billing'){
								$isGBSBilling_db = true;
							}

                            if(!empty($country) && $country == 'JPN') {
                                $db_billing_country = 'jpn';
                            } else if(!empty($country) && $country != 'JPN') {
                                $db_billing_country = 'other';
                            }
                            if(in_array("Billing Account", $xml_empty_fields[$line_item_uuid]) && empty($line_item_details_arr[$value]['bill_account_id'])) {
                                $billing_acc_validation_arr[$line_item_uuid] = array();
                            }
                            else{

                                // Skip validation if customer billing method is 'GBS_Billing'
                                if(!$isGBSBilling_db && !$isGBSBilling_xml){
                                    // if billing account is JPN
                                    if($xml_billing_country == 392 || ($xml_billing_country == '' && $db_billing_country == 'jpn')) {
                                        // Payment Type validation for japan account
                                        if (in_array("Payment Type", $xml_empty_fields[$line_item_uuid]) && empty($line_item_details_arr[$value]['payment_type_id'])) {
                                            $billing_acc_validation_arr[$line_item_uuid][] = 'Payment Type';
                                        } else {
                                            // Billing Start Date validation for japan account
                                            if (in_array("Billing Start Date", $xml_empty_fields[$line_item_uuid]) && empty($line_item_details_arr[$value]['billing_start_date'])) {
                                                $billing_acc_validation_arr[$line_item_uuid][] = 'Billing Start Date';
                                            }
                                            // Billing End Date validation for japan account
                                            if (in_array("Billing End Date", $xml_empty_fields[$line_item_uuid]) && empty($line_item_details_arr[$value]['billing_end_date'])) {
                                                $billing_acc_validation_arr[$line_item_uuid][] = 'Billing End Date';
                                            }
                                        }
                                    }
                                }

                            }

                        }
                    }
            }
        }
        return $billing_acc_validation_arr;
    }

    /**
     * Beluga product validation for activated contract [POST|VERSION_UP|PATCH]
     *
     * @param array $request_data parsed request xml
     * @param string $request_type HTTP request type
     * @param string $product_offering_id product offering id of the contract
     * @return array|bool
     *
     * @author Rajul.Mondal
     * @since Feb 05, 2018
     */
    public static function validateXmlBillingAccountForBelugaProduct($request_data, $request_type, $product_offering_id)
    {
        $xml_missing_fields = [];
        $xml_empty_fields = [];
        $db_missing_fields = [];

        // empty checking of request data
        if (empty($request_data)) {
            return false;
        }
        $product_offering_id = !empty($request_data['ContractDetails']['ContractsHeader']['product_offering']) ? $request_data['ContractDetails']['ContractsHeader']['product_offering'] : $product_offering_id;
        // empty checking of product offering id
        if (empty($product_offering_id)) {
            return false;
        }
        $global_contract_id_xml_id = !empty($request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id']) ? $request_data['ContractDetails']['ContractsHeader']['global_contract_id_xml_id'] : '';
        // empty checking of $global_contract_id_xml_id
        if (empty($global_contract_id_xml_id)) {
            return false;
        }
        $line_items = $request_data['ContractDetails']['ContractLineItemDetails'];
        // empty checking of line items
        if (empty($line_items)) {
            return false;
        }
        // get pms details for the product offering
        $pms_offering_details = ApiValidationHelper::pmsProductOfferingDetails($product_offering_id);
        // check if the product is eligible for beluga validation
        if (empty($pms_offering_details['listofferingoption']) || (isset($pms_offering_details['listofferingoption']['beluga_product_type']) && $pms_offering_details['listofferingoption']['beluga_product_type'] == '') || (isset($pms_offering_details['listofferingoption']['beluga_service_type']) && $pms_offering_details['listofferingoption']['beluga_service_type'] == '')) {
            return $xml_missing_fields;
        }
        $billing_account = !empty($request_data['Accounts']['Billing']) ? $request_data['Accounts']['Billing'] : array();
        $billing_country = !empty($billing_account['country_code_numeric']) ? $billing_account['country_code_numeric'] : '';

        foreach ($line_items as $line_item) {
            $line_item_id_xml_id = !empty($line_item['ContractLineItemHeader']['line_item_id_xml_id']) ? $line_item['ContractLineItemHeader']['line_item_id_xml_id'] : '';
            $billing_start_date = !empty($line_item['ContractLineItemHeader']['billing_start_date']) ? $line_item['ContractLineItemHeader']['billing_start_date'] : '';
            $billing_end_date = !empty($line_item['ContractLineItemHeader']['billing_end_date']) ? $line_item['ContractLineItemHeader']['billing_end_date'] : '';
            $payment_type = !empty($line_item['ContractLineItemHeader']['payment_type']) ? $line_item['ContractLineItemHeader']['payment_type'] : '';

            // check if line item has customer_billing_method = 'GBS_Billing' in XML
            $isGBSBilling_xml = false;
            if(isset($line_item['ContractLineItemHeader']['customer_billing_method']) && $line_item['ContractLineItemHeader']['customer_billing_method'] == 'GBS_Billing'){
                $isGBSBilling_xml = true;
            }

            $xml_empty_fields[$line_item_id_xml_id] = [];
            if (empty($billing_account)) {
                $xml_empty_fields[$line_item_id_xml_id][] = 'Billing Account';
            }
            if($billing_start_date == '' && !$isGBSBilling_xml) {
                $xml_empty_fields[$line_item_id_xml_id][] = 'Billing Start Date';
            }
            if($billing_end_date == '' && !$isGBSBilling_xml) {
                $xml_empty_fields[$line_item_id_xml_id][] = 'Billing End Date';
            }
            if($payment_type == '' && !$isGBSBilling_xml) {
                $xml_empty_fields[$line_item_id_xml_id][] = 'Payment Type';
            }
            // billing account mandatory validation
            if (empty($billing_account)) {
                $xml_missing_fields[$line_item_id_xml_id] = array();
            }
            // Skip validation if customer billing method is 'GBS_Billing'
            else if(!$isGBSBilling_xml){
                if ($billing_country == 392 && $payment_type == '') {
                    $xml_missing_fields[$line_item_id_xml_id][] = 'Payment Type';
                } else {
                    // Billing Start Date date validation for japan account
                    if ($billing_country == 392 && $billing_start_date == '') {
                        $xml_missing_fields[$line_item_id_xml_id][] = 'Billing Start Date';
                    }
                    // Billing End Date validation for japan account
                    if ($billing_country == 392 && $billing_end_date == '') {
                        $xml_missing_fields[$line_item_id_xml_id][] = 'Billing End Date';
                    }
                }
            }
        }

        if($request_type == self::REQUEST_TYPE_PATCH) {
            $db_missing_fields =  ApiValidationHelper::validateDbBillingAccountForBelugaProduct($global_contract_id_xml_id, $xml_empty_fields, $billing_country,$isGBSBilling_xml);
            return $db_missing_fields;
        }

        return $xml_missing_fields;
    }

    /**
     * Method to check contract status is 'Activated' in DB
     *
     * @return boolean
     *
     * @author Dinesh.Itkar
     * @since April 23, 2018
     */
    public static function isDbContractStatusActivated($db_contract_status_id)
    {
        if( !empty( $db_contract_status_id ) && $db_contract_status_id == '4' ){
            return true;
        }
        return false;
    }
}
