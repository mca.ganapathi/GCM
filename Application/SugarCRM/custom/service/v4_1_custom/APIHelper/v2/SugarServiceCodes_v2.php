<?php
/***API 2.0 service codes***/
$api_service_codes_v2['3001'] = 'Contract process terminated. Client Request ID already exists in the system.';
$api_service_codes_v2['3002'] = 'Contract process terminated. Contract Reference ID should not be empty.';
$api_service_codes_v2['3004'] = 'Contract process terminated. Reference XML data should not be empty.';
$api_service_codes_v2['3006'] = 'Contract process terminated. XSD validation failed.';
$api_service_codes_v2['3007'] = 'Contract process terminated. Contract Name should not be empty.';
$api_service_codes_v2['3008'] = 'Contract process terminated. Bundled Product should not be empty.';    // Bundled Product => Product Offering
$api_service_codes_v2['3009'] = 'Contract process terminated. Atomic Product should not be empty.'; // Atomic Product => Product Specification
$api_service_codes_v2['3010'] = 'Contract process terminated. Base Atomic Product should not be empty.';    // Base Atomic Product => One Base Specification
$api_service_codes_v2['3011'] = 'Contract process terminated. Product Contract Characteristic should not be empty.';
$api_service_codes_v2['3012'] = 'Contract process terminated. Product Biitem Characteristic should not be empty.';
$api_service_codes_v2['3013'] = 'Contract process terminated. Product In-Consistency Occurred.';  // Product Consistency Validation
$api_service_codes_v2['3014'] = 'Contract process terminated. Invalid Team Detail(s).';  // Mismatch with the Application Team Name
$api_service_codes_v2['3015'] = 'Contract process terminated. Contact Person Name (either Local or Latin) is mandatory.';   // Contract, Billing & Technology
$api_service_codes_v2['3016'] = 'Contract process terminated. Insufficient Billing Detail(s).';   // All payment type
$api_service_codes_v2['3017'] = 'Contract process terminated. Corporation ID or Corporation Name (either Local or Latin) is mandatory.';    // Contract, Billing & Technology
$api_service_codes_v2['3018'] = 'Contract process terminated. Invalid Account Detail(s).';
$api_service_codes_v2['3019'] = 'Contract process terminated. Contract in deactivated state.';   // POST (Version Up) / PATCH for deactivated contract
$api_service_codes_v2['3020'] = 'Contract process terminated. Invalid Contract Detail(s).';
$api_service_codes_v2['3021'] = 'Contract process terminated. Invalid Contract Version Up Detail(s).';
$api_service_codes_v2['3022'] = 'Contract process terminated. Invalid Contract Update Detail(s).';
$api_service_codes_v2['3023'] = 'Contract process terminated. Version up process not applicable for the bundled product.';
$api_service_codes_v2['3024'] = 'Contract process terminated. Invalid Product Contract Request.'; // Invalid Input for Product Contract API
$api_service_codes_v2['4001'] = 'Access denied. Please contact System Administrator.';  // Invalid Session Id
$api_service_codes_v2['6001'] = 'Contract process terminated. Contract Reference ID does not exist.';   // Contract reference ID (Global Contract ID or Global Contract UUID) not exist in GCM
$api_service_codes_v2['7001'] = 'Invalid Method. Please contact System Administrator.'; // In case that a http method request other than POST/GET/PATCH/DELETE coming to /api/v2/contract-information, the status code 405 is sent back.
$api_service_codes_v2['7002'] = 'Invalid Method. Please contact System Administrator.'; // In case that a http method request other thanGET coming to /api/v2/views/list-of-product-contracts, the status code 405 is sent back.
$api_service_codes_v2['8001'] = 'Contract process terminated. Please contact System Administrator.';    // Save function receive empty data from Parsing function
$api_service_codes_v2['8002'] = 'Contract process terminated. Please contact System Administrator.';    // In case that RESTful Proxy module calls SugarCRM's REST but the response is empty (even HTTP response header is empty), the status code 500 is sent back.
$api_service_codes_v2['8003'] = 'Contract process terminated. Please contact System Administrator.';    // In case that GCM error code is not mapped with http status code, please send 500 code.
$api_service_codes_v2['8004'] = 'Contract process terminated. Please contact System Administrator.';    // PMS connection failed.
$api_service_codes_v2['3003'] = 'Contract process terminated. Please contact System Administrator.';    // CIDAS Connection failed.
$api_service_codes_v2['3005'] = 'Contract process terminated. Please contact System Administrator.';    // CIDAS System return zero records
$api_service_codes_v2['E001'] = 'Contract process terminated. Please contact System Administrator.';    // Error from CIDAS. The number of search result exceeds threshold or the company name and the company name in English after cleansing is blank.
$api_service_codes_v2['E002'] = 'Contract process terminated. Please contact System Administrator.';    // Error from CIDAS. Wrong authentication pass code.
$api_service_codes_v2['E003'] = 'Contract process terminated. Please contact System Administrator.';    // Error from CIDAS. Wrong GET parameter.
$api_service_codes_v2['E004'] = 'Contract process terminated. Please contact System Administrator.';    // Error from CIDAS. System error.
$api_service_codes_v2['10001'] = 'Invalid Method. Please contact System Administrator.';    // In case that a http method request other than GET coming to /api/v2/export-csv-for-gbs, the status code 405 is sent back.
$api_service_codes_v2['10002'] = 'Contract process terminated. Please contact System Administrator.';    // In case that RESTful Proxy module calls SugarCRM's REST (GBS API) but the response is empty (even HTTP response header is empty), the status code 500 is sent back.
$api_service_codes_v2['10003'] = 'Contract process terminated. No record(s) found for the given search parameters.';    // If the given search criteria has no records
$api_service_codes_v2['10004'] = 'Contract process terminated. Invalid GBS Request.';    // Invalid Parameters
$api_service_codes_v2['10005'] = 'Contract process terminated. Please contact System Administrator.';    // In case target Product offering(s) not found in the config.
/***API 2.0 service codes***/