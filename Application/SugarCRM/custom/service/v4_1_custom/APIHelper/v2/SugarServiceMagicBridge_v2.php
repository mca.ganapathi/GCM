<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
include_once ('custom/modules/gc_Contracts/include/ClassServiceGcContracts.php');
include_once ('custom/service/v4_1_custom/APIHelper/v2/ClassContractPostResponse_v2.php');
require_once ('custom/modules/gc_Contracts/include/ClassReportServiceGcContracts.php');

/**
 * API Service Version 2.0 Class file
 *
 * @author sagar.salunkhe
 */
/**
 * @type unix-file system UTF-8
 */
class SugarServiceMagicBridge_v2
{

    /**
     * set API version number
     */
    const API_VER = 2;

    public $objContractService;

    public $objPostResponseXML;

    public $objReportServiceContracts;

    /**
     * initiate all module service class objects here
     *
     * @author sagar.salunkhe
     * @since Jun 8, 2016
     */
    public function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        self::initiateServiceObjects();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }

    private function initiateServiceObjects()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        $this->objContractService = new ClassServiceGcContracts(self::API_VER);
        $this->objPostResponseXML = new ClassContractPostResponse_v2();
        $this->objReportServiceContracts = new ClassReportServiceGcContracts();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
    }
}