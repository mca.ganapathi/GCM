<?php

// Bundled Products XML root Block
function xmlRootBlock($arrParameters)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arrParameters: '.print_r($arrParameters,true), 'Function to construct the root block');
    $returnXML = '<?xml version="1.0" encoding="UTF-8"?>
    <contract-info:contract-information ';
    foreach ($arrParameters as $key => $value) {
        $returnXML .= $key . '="' . $value . '" ';
    }
    $returnXML .= '>
    ';
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'returnXML: '.$returnXML, '');
    return $returnXML;
}

// Bundled Products XML Block
function xmlBundledProducts($arr_contract_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_informations: '.print_r($arr_contract_informations,true), 'Function to construct the bundle product block');
    $return_xml = '';
    $return_xml = '
    <product:bundled-products>
        <product:bundled-product id="' . $arr_contract_informations['bundle_product']['char_uuid'] . ':' . $arr_contract_informations['bundle_product']['version'] . '">
            <cbe:identified-by>
                <product:bundled-product-key>
                    <cbe:key-type-for-cbe-standard>
                        <cbe:object-id>' . $arr_contract_informations['bundle_product']['char_uuid'] . '</cbe:object-id>
                        <cbe:object-version>' . $arr_contract_informations['bundle_product']['version'] . '</cbe:object-version>
                    </cbe:key-type-for-cbe-standard>
                </product:bundled-product-key>
            </cbe:identified-by>
            <cbe:name>' . $arr_contract_informations['bundle_product']['name'] . '</cbe:name>
            <cbe:described-by>
                <product:reference-to-bundled-product-offering>
                    <cbe:link>
                        <cbe:rel>specification</cbe:rel>
                        <cbe:href>' . $arr_contract_informations['glb_parameters']['api_url'] . '/bundled-product-offerings/' . $arr_contract_informations['bundle_product']['pms_uuid'] . '</cbe:href>
                    </cbe:link>
                    <product:resource-element-name>product:bundled-product-offering</product:resource-element-name>
                    <product:bundled-product-offering-key>
                        <cbe:key-type-for-cbe-standard>
                            <cbe:object-id>' . $arr_contract_informations['bundle_product']['pms_uuid'] . '</cbe:object-id>
                            <cbe:object-version>' . $arr_contract_informations['bundle_product']['version'] . '</cbe:object-version>
                        </cbe:key-type-for-cbe-standard>
                    </product:bundled-product-offering-key>
                </product:reference-to-bundled-product-offering>
            </cbe:described-by>
        </product:bundled-product>
    </product:bundled-products>
    ';
    unset($arr_contract_informations);
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_xml: '.$return_xml, '');
    return $return_xml;
}

// Atomic Products Block
function xmlAtomicProducts($arr_contract_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_informations: '.print_r($arr_contract_informations,true), 'Function to construct the atomic product block');
    $return_xml               = '';
    $primary_url              = $arr_contract_informations['glb_parameters']['api_url'];
    $arr_contract_information = array();
    $return_xml = '
    <product:atomic-products>';
    foreach ($arr_contract_informations['atomic_products'] as $arr_contract_information) {
        $return_xml .= '
        <product:atomic-product id="' . $arr_contract_information['char_uuid'] . ':' . $arr_contract_information['version'] . '">
            <cbe:identified-by>
                <product:atomic-product-key>
                    <cbe:key-type-for-cbe-standard>
                        <cbe:object-id>' . $arr_contract_information['char_uuid'] . '</cbe:object-id>
                        <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                    </cbe:key-type-for-cbe-standard>
                </product:atomic-product-key>
            </cbe:identified-by>
            <cbe:name>' . $arr_contract_information['name'] . '</cbe:name>
            <cbe:described-by>
                <product:reference-to-atomic-product-offering>
                    <cbe:link>
                        <cbe:rel>specification</cbe:rel>
                        <cbe:href>' . $primary_url . '/atomic-product-offerings/' . $arr_contract_information['pms_uuid'] . '</cbe:href>
                    </cbe:link>
                    <product:resource-element-name>product:atomic-product-offering</product:resource-element-name>
                    <product:atomic-product-offering-key>
                        <cbe:key-type-for-cbe-standard>
                            <cbe:object-id>' . $arr_contract_information['pms_uuid'] . '</cbe:object-id>
                            <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                        </cbe:key-type-for-cbe-standard>
                    </product:atomic-product-offering-key>
                </product:reference-to-atomic-product-offering>
            </cbe:described-by>
        </product:atomic-product>
        ';
    }
    $return_xml .= '
    </product:atomic-products>
    ';
    unset($arr_contract_information, $arr_contract_informations);
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_xml: '.$return_xml, '');
    return $return_xml;
}

// Atomic Product Prices Block
function xmlAtomicProductPrices($arr_contract_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_informations: '.print_r($arr_contract_informations,true), 'Function to construct the atomic product prices block');
    $return_xml               = '';
    $primary_url              = $arr_contract_informations['glb_parameters']['api_url'];
    $arr_contract_information = array();
    $return_xml = '
    <product:atomic-product-prices>';
    foreach ($arr_contract_informations['atomic_product_prices'] as $arr_contract_information) {
        $return_xml .= '
        <product:atomic-product-price id="' . $arr_contract_information['char_uuid'] . ':' . $arr_contract_information['version'] . '">
            <cbe:identified-by>
                <product:atomic-product-price-key>
                    <cbe:key-type-for-cbe-standard>
                        <cbe:object-id>' . $arr_contract_information['char_uuid'] . '</cbe:object-id>
                        <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                    </cbe:key-type-for-cbe-standard>
                </product:atomic-product-price-key>
            </cbe:identified-by>
            <cbe:name>' . $arr_contract_information['name'] . '</cbe:name>
            <cbe:association-ends>
                <cbe:association-end name="realizes">
                    <cbe:no>1</cbe:no>
                    <cbe:role>Master</cbe:role>
                    <cbe:target>
                        <product:reference-to-atomic-product-offering-price>
                            <cbe:link>
                                <cbe:rel>specification</cbe:rel>
                                <cbe:href>' . $primary_url . '/atomic-product-offering-prices/' . $arr_contract_information['pms_uuid'] . '</cbe:href>
                            </cbe:link>
                            <product:resource-element-name>product:atomic-product-offering-price</product:resource-element-name>
                            <product:atomic-product-offering-price-key>
                                <cbe:key-type-for-cbe-standard>
                                    <cbe:object-id>' . $arr_contract_information['pms_uuid'] . '</cbe:object-id>
                                    <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                                </cbe:key-type-for-cbe-standard>
                            </product:atomic-product-offering-price-key>
                        </product:reference-to-atomic-product-offering-price>
                    </cbe:target>
                </cbe:association-end>
                <cbe:association-end name="constitutes-value-of">
                    <cbe:no>2</cbe:no>
                    <cbe:role>Product</cbe:role>
                    <cbe:target>
                        <product:reference-to-atomic-product>
                            <cbe:link>
                                <cbe:rel>entity</cbe:rel>
                                <cbe:href>' . $primary_url . '/atomic-products/' . $arr_contract_information['atomic_product'] . '</cbe:href>
                            </cbe:link>
                            <cbe:xml-path>
                                <cbe:location-path>product:atomic-products/product:atomic-product[attribute::id=&quot;' . $arr_contract_information['atomic_product'] . ':' . $arr_contract_information['version'] . '&quot;]</cbe:location-path>
                            </cbe:xml-path>
                            <product:resource-element-name>product:atomic-product</product:resource-element-name>
                            <product:atomic-product-key>
                                <cbe:key-type-for-cbe-standard>
                                    <cbe:object-id>' . $arr_contract_information['atomic_product'] . '</cbe:object-id>
                                    <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                                </cbe:key-type-for-cbe-standard>
                            </product:atomic-product-key>
                        </product:reference-to-atomic-product>
                    </cbe:target>
                </cbe:association-end>
            </cbe:association-ends>
            <cbe:described-by>
                <product:reference-to-atomic-product-price-spec>
                    <cbe:link>
                        <cbe:rel>specification</cbe:rel>
                        <cbe:href>' . $primary_url . '/atomic-product-price-specifications/' . $arr_contract_information['described_by'] . '</cbe:href>
                    </cbe:link>
                    <product:resource-element-name>product:atomic-product-price-specification</product:resource-element-name>
                    <product:atomic-product-price-spec-key>
                        <cbe:key-type-for-cbe-standard>
                            <cbe:object-id>' . $arr_contract_information['described_by'] . '</cbe:object-id>
                            <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                        </cbe:key-type-for-cbe-standard>
                    </product:atomic-product-price-spec-key>
                </product:reference-to-atomic-product-price-spec>
            </cbe:described-by>
        </product:atomic-product-price>
        ';
    }
    $return_xml .= '
    </product:atomic-product-prices>';
    unset($arr_contract_information, $arr_contract_informations);
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_xml: '.$return_xml, '');
    return $return_xml;
}

// Product Contracts Block
function xmlProductContracts($arr_contract_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_informations: '.print_r($arr_contract_informations,true), 'Function to construct the product contract block');
    $return_xml                  = '';
    $primary_url                 = $arr_contract_informations['glb_parameters']['api_url'];
    $payment_type_code           = (isset($arr_contract_informations['payment_method'])) ? $arr_contract_informations['payment_method']['payment_type'] : '';
    $external_created_user_null  = ($arr_contract_informations['product_contracts']['external_created_user'] == '') ? ' xsi:nil="true"' : '';
    $external_modified_user_null = ($arr_contract_informations['product_contracts']['external_modified_user'] == '') ? ' xsi:nil="true"' : '';
    $description_null            = ($arr_contract_informations['product_contracts']['description'] == '') ? ' xsi:nil="true"' : '';
    $client_request_id_null      = ($arr_contract_informations['product_contracts']['client_request_id'] == '') ? ' xsi:nil="true"' : '';
    $last_modified_date_null     = ($arr_contract_informations['product_contracts']['last_modified_date'] == '') ? ' xsi:nil="true"' : '';
    $last_modified_user_null     = ($arr_contract_informations['product_contracts']['last_modified_user'] == '') ? ' xsi:nil="true"' : '';

    $return_xml = '
    <bi:product-contracts>
        <bi:product-contract id="' . $arr_contract_informations['product_contracts']['char_uuid'] . ':' . $arr_contract_informations['product_contracts']['version'] . '">
            <cbe:identified-by>
                <bi:product-contract-key>
                    <cbe:key-type-for-cbe-standard>
                        <cbe:object-id>' . $arr_contract_informations['product_contracts']['char_uuid'] . '</cbe:object-id>
                        <cbe:object-version>' . $arr_contract_informations['product_contracts']['version'] . '</cbe:object-version>
                    </cbe:key-type-for-cbe-standard>
                </bi:product-contract-key>
            </cbe:identified-by>
            <cbe:object-created-date>' . $arr_contract_informations['product_contracts']['created_date'] . '</cbe:object-created-date>
            <cbe:object-created-user>' . $arr_contract_informations['product_contracts']['created_user'] . '</cbe:object-created-user>
            <cbe:object-last-modified-date' . $last_modified_date_null . '>' . $arr_contract_informations['product_contracts']['last_modified_date'] . '</cbe:object-last-modified-date>
            <cbe:object-last-modified-user' . $last_modified_user_null . '>' . $arr_contract_informations['product_contracts']['last_modified_user'] . '</cbe:object-last-modified-user>
            <cbe:created-user' . $external_created_user_null . '>' . $arr_contract_informations['product_contracts']['external_created_user'] . '</cbe:created-user>
            <cbe:last-modified-user' . $external_modified_user_null . '>' . $arr_contract_informations['product_contracts']['external_modified_user'] . '</cbe:last-modified-user>
            <cbe:lock-version>' . $arr_contract_informations['product_contracts']['lock_version'] . '</cbe:lock-version>
            <cbe:client-request-id' . $client_request_id_null . '>' . $arr_contract_informations['product_contracts']['client_request_id'] . '</cbe:client-request-id>
            <cbe:name>' . $arr_contract_informations['product_contracts']['name'] . '</cbe:name>
            <cbe:description' . $description_null . '>' . $arr_contract_informations['product_contracts']['description'] . '</cbe:description>
            <bi:interaction-status>' . $arr_contract_informations['product_contracts']['interaction_status'] . '</bi:interaction-status>
            <cbe:standard-business-state>' . $arr_contract_informations['product_contracts']['standard_business_state'] . '</cbe:standard-business-state>
            <cbe:association-ends>
                <cbe:association-end name="contract-for">
                    <cbe:no>1</cbe:no>
                    <cbe:role>Product</cbe:role>
                    <cbe:target>
                        <product:reference-to-bundled-product>
                            <cbe:link>
                                <cbe:rel>entity</cbe:rel>
                                <cbe:href>' . $primary_url . '/bundled-products/' . $arr_contract_informations['product_contracts']['product_offering_uuid'] . '</cbe:href>
                            </cbe:link>
                            <cbe:xml-path>
                                <cbe:location-path>product:bundled-products/product:bundled-product[attribute::id=&quot;' . $arr_contract_informations['product_contracts']['product_offering_uuid'] . ':' . $arr_contract_informations['product_contracts']['b_version'] . '&quot;]</cbe:location-path>
                            </cbe:xml-path>
                            <product:resource-element-name>product:bundled-product</product:resource-element-name>
                            <product:bundled-product-key>
                                <cbe:key-type-for-cbe-standard>
                                    <cbe:object-id>' . $arr_contract_informations['product_contracts']['product_offering_uuid'] . '</cbe:object-id>
                                    <cbe:object-version>' . $arr_contract_informations['product_contracts']['b_version'] . '</cbe:object-version>
                                </cbe:key-type-for-cbe-standard>
                            </product:bundled-product-key>
                        </product:reference-to-bundled-product>
                    </cbe:target>
                </cbe:association-end>
            </cbe:association-ends>
            <cbe:expansions>
                <gcm:expansion-of-product-contract-by-gcm>';

    if (isset($arr_contract_informations['cd_contract']['customer_details'])) {
        $return_xml .= xmlCustomerDetails($arr_contract_informations['cd_contract']);
    }

    if (isset($arr_contract_informations['cd_billing']['customer_details'])) {
        $return_xml .= xmlCustomerDetails($arr_contract_informations['cd_billing']);
    }

    if (isset($arr_contract_informations['cd_sales']['customer_details'])) {
        $return_xml .= xmlCustomerDetails($arr_contract_informations['cd_sales']);
    }

    if (isset($arr_contract_informations['cd_billing_affiliate']['org_code1'])) {
        $return_xml .= xmlBillingAffiliateDetails($arr_contract_informations['cd_billing_affiliate']);
    }

    if (isset($arr_contract_informations['cd_technical']['customer_details'])) {
        $return_xml .= xmlCustomerDetails($arr_contract_informations['cd_technical']);
    }

    if ($payment_type_code == '00') {
        $return_xml .= xmlPaymentMethodDirectDeposit($arr_contract_informations);
    } else if ($payment_type_code == '10') {
        $return_xml .= xmlPaymentMethodAccountTransfer($arr_contract_informations);
    } else if ($payment_type_code == '30') {
        $return_xml .= xmlPaymentMethodPostalTransfer($arr_contract_informations);
    } else if ($payment_type_code == '60') {
        $return_xml .= xmlPaymentMethodCreditCard($arr_contract_informations);
    }

    $return_xml .= '
                </gcm:expansion-of-product-contract-by-gcm>
            </cbe:expansions>
            <cbe:described-by>
                <bi:reference-to-product-contract-spec>
                    <cbe:link>
                        <cbe:rel>specification</cbe:rel>
                        <cbe:href>' . $primary_url . '/product-contract-specifications/' . $arr_contract_informations['product_contracts']['described_by'] . '</cbe:href>
                    </cbe:link>
                    <bi:resource-element-name>bi:product-contract-specification</bi:resource-element-name>
                    <bi:product-contract-spec-key>
                        <cbe:key-type-for-cbe-standard>
                            <cbe:object-id>' . $arr_contract_informations['product_contracts']['described_by'] . '</cbe:object-id>
                            <cbe:object-version>1</cbe:object-version>
                        </cbe:key-type-for-cbe-standard>
                    </bi:product-contract-spec-key>
                </bi:reference-to-product-contract-spec>
            </cbe:described-by>
            <bi:contract-id>' . $arr_contract_informations['product_contracts']['contract_id'] . '</bi:contract-id>';

    if (!empty($arr_contract_informations['product_contracts']['start_date_time']) || !empty($arr_contract_informations['product_contracts']['end_date_time'])) {
        $return_xml .= '
            <bi:agreement-period>';

        if (!empty($arr_contract_informations['product_contracts']['start_date_time'])) {
            $return_xml .= '
                <datatypes:start-date-time>' . $arr_contract_informations['product_contracts']['start_date_time'] . '</datatypes:start-date-time>';
        }

        if (!empty($arr_contract_informations['product_contracts']['end_date_time'])) {
            $return_xml .= '
                <datatypes:end-date-time>' . $arr_contract_informations['product_contracts']['end_date_time'] . '</datatypes:end-date-time>';
        }

        $return_xml .= '
            </bi:agreement-period>';
    }
    $return_xml .= '
        </bi:product-contract>
    </bi:product-contracts>
    ';
    unset($arr_contract_informations);
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_xml: '.$return_xml, '');
    return $return_xml;
}

// Customer Details Block
function xmlCustomerDetails($arr_contract_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_informations: '.print_r($arr_contract_informations,true), 'Function to construct the customer details block');
    $return_xml = '';
    // Nullable Fields
    $corporation_name_null               = ($arr_contract_informations['customer_details']['corporation_name'] == '') ? ' xsi:nil="true"' : '';
    $corporation_name_latin_null         = ($arr_contract_informations['customer_details']['corporation_name_latin'] == '') ? ' xsi:nil="true"' : '';
    $corporation_name_furigana_null      = ($arr_contract_informations['customer_details']['corporation_name_furigana'] == '') ? ' xsi:nil="true"' : '';
    $corporation_corporation_id_null     = ($arr_contract_informations['customer_details']['corporation_corporation_id'] == '') ? ' xsi:nil="true"' : '';
    $corporation_vat_no_null             = ($arr_contract_informations['customer_details']['corporation_vat_no'] == '') ? ' xsi:nil="true"' : '';
    $corporation_sales_channel_code_null = ($arr_contract_informations['customer_details']['corporation_sales_channel_code'] == '') ? ' xsi:nil="true"' : '';

    $pi_division_name_null       = ($arr_contract_informations['customer_details']['pi_division_name'] == '') ? ' xsi:nil="true"' : '';
    $pi_division_name_latin_null = ($arr_contract_informations['customer_details']['pi_division_name_latin'] == '') ? ' xsi:nil="true"' : '';

    $pi_individual_name_null       = ($arr_contract_informations['customer_details']['pi_individual_name'] == '') ? ' xsi:nil="true"' : '';
    $pi_individual_name_latin_null = ($arr_contract_informations['customer_details']['pi_individual_name_latin'] == '') ? ' xsi:nil="true"' : '';
    $pi_individual_title_null      = ($arr_contract_informations['customer_details']['pi_individual_title'] == '') ? ' xsi:nil="true"' : '';

    $pi_contact_medium_tel_no_null = ($arr_contract_informations['customer_details']['pi_contact_medium_tel_no'] == '') ? ' xsi:nil="true"' : '';
    $pi_contact_medium_ext_no_null = ($arr_contract_informations['customer_details']['pi_contact_medium_ext_no'] == '') ? ' xsi:nil="true"' : '';
    $pi_contact_medium_fax_no_null = ($arr_contract_informations['customer_details']['pi_contact_medium_fax_no'] == '') ? ' xsi:nil="true"' : '';
    $pi_contact_medium_email_null  = ($arr_contract_informations['customer_details']['pi_contact_medium_email'] == '') ? ' xsi:nil="true"' : '';

    // Customer Type Settings

    $primary_tag   = $primary_tag_name   = '';'';
    $customer_type = $arr_contract_informations['customer_details']['customer_type'];
    if ($customer_type == 'contract') {
        $primary_tag      = 'contract-customer';
        $primary_tag_name = '契約先情報';
    } else if ($customer_type == 'billing') {
        $primary_tag      = 'billing-customer';
        $primary_tag_name = '請求先情報';
    } else if ($customer_type == 'technical') {
        $primary_tag      = 'customer-technical-representative';
        $primary_tag_name = '顧客システム技術担当情報';
    } else if ($customer_type == 'sales') {
        $primary_tag      = 'sales-channel';
        $primary_tag_name = '販売チャネル情報';
    }

    $tag_block_corporation     = 'standard';
    $tag_corporation_name      = 'name';
    $tag_corporation_name_eng  = 'name-latin';
    $tag_corporation_name_kana = 'name-furigana';
	$pi_individual_name_null   = ($customer_type == 'sales') ? $pi_individual_name_null : ''; // DF-1486 Improvement

    if (trim($arr_contract_informations['customer_details']['corporation_corporation_id']) != '') {
        $tag_block_corporation     = 'cidas';
        $tag_corporation_name      = 'cidas-company-name';
        $tag_corporation_name_eng  = 'cidas-company-name-eng';
        $tag_corporation_name_kana = 'cidas-company-name-kana';
    }

    $return_xml = '
    <gcm:' . $primary_tag . ' name="' . $primary_tag_name . '">
        <gcm:corporation>
            <gcm:' . $tag_block_corporation . '-corporation-name>
                <gcm:' . $tag_corporation_name . $corporation_name_null . '>' . $arr_contract_informations['customer_details']['corporation_name'] . '</gcm:' . $tag_corporation_name . '>';
    if ($customer_type != 'sales') {
        $return_xml .= '
                <gcm:' . $tag_corporation_name_eng . $corporation_name_latin_null . '>' . $arr_contract_informations['customer_details']['corporation_name_latin'] . '</gcm:' . $tag_corporation_name_eng . '>
                <gcm:' . $tag_corporation_name_kana . $corporation_name_furigana_null . '>' . $arr_contract_informations['customer_details']['corporation_name_furigana'] . '</gcm:' . $tag_corporation_name_kana . '>';
    }
    $return_xml .= '
            </gcm:' . $tag_block_corporation . '-corporation-name>';
    if ($customer_type == 'sales') {
        $return_xml .= '
            <gcm:sales-channel-code' . $corporation_sales_channel_code_null . '>' . $arr_contract_informations['customer_details']['corporation_sales_channel_code'] . '</gcm:sales-channel-code>';
    } else {
        $return_xml .= '
            <gcm:corporation-id' . $corporation_corporation_id_null . '>' . $arr_contract_informations['customer_details']['corporation_corporation_id'] . '</gcm:corporation-id>';
        if ($customer_type == 'contract') {
            $return_xml .= '
            <gcm:vat-no' . $corporation_vat_no_null . '>' . $arr_contract_informations['customer_details']['corporation_vat_no'] . '</gcm:vat-no>';
        }
    }
    $return_xml .= '
        </gcm:corporation>
        <gcm:person-in-charge>
            <gcm:division>
                <gcm:name' . $pi_division_name_null . '>' . $arr_contract_informations['customer_details']['pi_division_name'] . '</gcm:name>
                <gcm:name-latin' . $pi_division_name_latin_null . '>' . $arr_contract_informations['customer_details']['pi_division_name_latin'] . '</gcm:name-latin>
            </gcm:division>
            <gcm:individual>
                <gcm:name' . $pi_individual_name_null . '>' . $arr_contract_informations['customer_details']['pi_individual_name'] . '</gcm:name>
                <gcm:name-latin' . $pi_individual_name_latin_null . '>' . $arr_contract_informations['customer_details']['pi_individual_name_latin'] . '</gcm:name-latin>
                <gcm:title' . $pi_individual_title_null . '>' . $arr_contract_informations['customer_details']['pi_individual_title'] . '</gcm:title>
            </gcm:individual>
            <gcm:contact-medium>
                <gcm:telephone-number' . $pi_contact_medium_tel_no_null . '>' . $arr_contract_informations['customer_details']['pi_contact_medium_tel_no'] . '</gcm:telephone-number>
                <gcm:extension-number' . $pi_contact_medium_ext_no_null . '>' . $arr_contract_informations['customer_details']['pi_contact_medium_ext_no'] . '</gcm:extension-number>
                <gcm:fax-number' . $pi_contact_medium_fax_no_null . '>' . $arr_contract_informations['customer_details']['pi_contact_medium_fax_no'] . '</gcm:fax-number>
                <gcm:email-address' . $pi_contact_medium_email_null . '>' . $arr_contract_informations['customer_details']['pi_contact_medium_email'] . '</gcm:email-address>
            </gcm:contact-medium>
        </gcm:person-in-charge>';

    if ($customer_type != 'sales') {
        $return_xml .= xmlCustomerAddressDetails($arr_contract_informations);
    }

    $return_xml .= '
    </gcm:' . $primary_tag . '>
    ';
    unset($arr_contract_informations);
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_xml: '.$return_xml, '');
    return $return_xml;
}

// Customer Address Details Block
function xmlCustomerAddressDetails($arr_contract_address_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_address_informations: '.print_r($arr_contract_address_informations,true), 'Function to construct the customer address details block');
    $return_xml = '';
    $attention_line_null = ($arr_contract_address_informations['customer_details']['attention_line'] == '') ? ' xsi:nil="true"' : '';
    $iso3166_1numeric    = $arr_contract_address_informations['customer_details']['address_iso3166_1numeric'];

    if (trim($iso3166_1numeric) != '') {
        $return_xml = '
            <gcm:address>
                <location:iso3166-1numeric>' . $iso3166_1numeric . '</location:iso3166-1numeric>';

        if ($iso3166_1numeric == '124' || $iso3166_1numeric == '840') // 124 => Canadian Property Address ; 840 => American Property Address
        {
            if (trim($arr_contract_address_informations['customer_details']['address_address_line']) != '' || trim($arr_contract_address_informations['customer_details']['address_address_city']) != '' || trim($arr_contract_address_informations['customer_details']['address_address_state']) != '' || trim($arr_contract_address_informations['customer_details']['address_postal_code']) != '' || trim($arr_contract_address_informations['customer_details']['attention_line']) != '') {
                $primary_tag = ($iso3166_1numeric == '124') ? 'canadian-property-address' : 'american-property-address';
                $postal_tag  = ($iso3166_1numeric == '124') ? 'postal-code' : 'zip-code';
                $return_xml .= '
                <gcm:' . $primary_tag . '>
                    <gcm:address-line>' . $arr_contract_address_informations['customer_details']['address_address_line'] . '</gcm:address-line>
                    <gcm:city>' . $arr_contract_address_informations['customer_details']['address_address_city'] . '</gcm:city>
                    <gcm:state-abbr>' . $arr_contract_address_informations['customer_details']['address_address_state'] . '</gcm:state-abbr>
                    <gcm:' . $postal_tag . '>' . $arr_contract_address_informations['customer_details']['address_postal_code'] . '</gcm:' . $postal_tag . '>
                    <gcm:attention-line' . $attention_line_null . '>' . $arr_contract_address_informations['customer_details']['attention_line'] . '</gcm:attention-line>
                </gcm:' . $primary_tag . '>';
            }
        } else if ($iso3166_1numeric == '392') // Japanese Property Address
        {
            if (trim($arr_contract_address_informations['customer_details']['address_postal_code']) != '' || trim($arr_contract_address_informations['customer_details']['address_address']) != '' || trim($arr_contract_address_informations['customer_details']['address_address_line']) != '') {
                $return_xml .= '
                <gcm:japanese-property-address>
                    <gcm:postal-code>' . $arr_contract_address_informations['customer_details']['address_postal_code'] . '</gcm:postal-code>
                    <gcm:address>' . $arr_contract_address_informations['customer_details']['address_address'] . '</gcm:address>
                    <gcm:address-line>' . $arr_contract_address_informations['customer_details']['address_address_line'] . '</gcm:address-line>
                </gcm:japanese-property-address>';
            }
        } else // Any Address
        {
            if (trim($arr_contract_address_informations['customer_details']['address_address']) != '' || trim($arr_contract_address_informations['customer_details']['attention_line']) != '') {
                $return_xml .= '
                <gcm:any-address>
                    <gcm:address>' . $arr_contract_address_informations['customer_details']['address_address'] . '</gcm:address>
                    <gcm:attention-line' . $attention_line_null . '>' . $arr_contract_address_informations['customer_details']['attention_line'] . '</gcm:attention-line>
                </gcm:any-address>';
            }
        }

        $return_xml .= '
            </gcm:address>';
        unset($arr_contract_address_informations);
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_xml: '.$return_xml, '');
        return $return_xml;
    }
}

// Billing Affiliate Block
function xmlBillingAffiliateDetails($arr_contract_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_informations: '.print_r($arr_contract_informations,true), 'Function to construct the billing affiliate details block');
    $return_xml = '';
    // Nullable Fields
    $org_code2_null         = ($arr_contract_informations['org_code2'] == '') ? ' xsi:nil="true"' : '';
    $level1_name_latin_null = ($arr_contract_informations['org_level1']['name_latin'] == '') ? ' xsi:nil="true"' : '';
    $level2_name_latin_null = ($arr_contract_informations['org_level2']['name_latin'] == '') ? ' xsi:nil="true"' : '';
    $level3_name_latin_null = ($arr_contract_informations['org_level3']['name_latin'] == '') ? ' xsi:nil="true"' : '';
    $level4_name_latin_null = ($arr_contract_informations['org_level4']['name_latin'] == '') ? ' xsi:nil="true"' : '';

    $org_code1 = isset($arr_contract_informations['org_code1']) ? $arr_contract_informations['org_code1'] : '';
    $org_code2 = isset($arr_contract_informations['org_code2']) ? $arr_contract_informations['org_code2'] : '';
    $return_xml = '
    <gcm:billing-affiliate name="請求先アフィリエイト情報">
        <gcm:organization-code>
            <gcm:code1>' . $org_code1 . '</gcm:code1>
            <gcm:code2' . $org_code2_null . '>' . $org_code2 . '</gcm:code2>
        </gcm:organization-code>
        <gcm:organization-name>';
    $org_level1_name = isset($arr_contract_informations['org_level1']['name']) ? $arr_contract_informations['org_level1']['name'] :'';
    $org_level1_name_latin = isset($arr_contract_informations['org_level1']['name_latin']) ? $arr_contract_informations['org_level1']['name_latin'] :'';
    if ($org_level1_name != '' || $org_level1_name_latin != '') {
        $return_xml .= '
            <gcm:level1>
                <gcm:name>' . $org_level1_name . '</gcm:name>
                <gcm:name-latin' . $level1_name_latin_null . '>' . $org_level1_name_latin . '</gcm:name-latin>
            </gcm:level1>';

    } else {
        $return_xml .= '
            <gcm:level1 xsi:nil="true"/>';
    }
    $org_level2_name = isset($arr_contract_informations['org_level2']['name']) ? $arr_contract_informations['org_level2']['name'] :'';
    $org_level2_name_latin = isset($arr_contract_informations['org_level2']['name_latin']) ? $arr_contract_informations['org_level2']['name_latin'] :'';
    if ($org_level2_name != '' || $org_level2_name_latin != '') {
        $return_xml .= '
            <gcm:level2>
                <gcm:name>' . $org_level2_name . '</gcm:name>
                <gcm:name-latin' . $level2_name_latin_null . '>' . $org_level2_name_latin . '</gcm:name-latin>
            </gcm:level2>';

    } else {
        $return_xml .= '
            <gcm:level2 xsi:nil="true"/>';
    }
    $org_level3_name = isset($arr_contract_informations['org_level3']['name']) ? $arr_contract_informations['org_level3']['name'] :'';
    $org_level3_name_latin = isset($arr_contract_informations['org_level3']['name_latin']) ? $arr_contract_informations['org_level3']['name_latin'] :'';
    if ($org_level3_name != '' || $org_level3_name_latin != '') {
        $return_xml .= '
            <gcm:level3>
                <gcm:name>' . $org_level3_name . '</gcm:name>
                <gcm:name-latin' . $level3_name_latin_null . '>' . $org_level3_name_latin . '</gcm:name-latin>
            </gcm:level3>';

    } else {
        $return_xml .= '
            <gcm:level3 xsi:nil="true"/>';
    }
    $org_level4_name = isset($arr_contract_informations['org_level4']['name']) ? $arr_contract_informations['org_level4']['name'] :'';
    $org_level4_name_latin = isset($arr_contract_informations['org_level4']['name_latin']) ? $arr_contract_informations['org_level4']['name_latin'] :'';
    if ($org_level4_name != '' || $org_level4_name_latin != '') {
        $return_xml .= '
            <gcm:level4>
                <gcm:name>' . $org_level4_name . '</gcm:name>
                <gcm:name-latin' . $level4_name_latin_null . '>' . $org_level4_name_latin . '</gcm:name-latin>
            </gcm:level4>';

    } else {
        $return_xml .= '
            <gcm:level4 xsi:nil="true"/>';
    }

    $return_xml .= '
        </gcm:organization-name>
    </gcm:billing-affiliate>';
    unset($arr_contract_informations);
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_xml: '.$return_xml, '');
    return $return_xml;

}

// Payment Method Block
// Payment Method : General: Direct Deposit Block
function xmlPaymentMethodDirectDeposit($arr_contract_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_informations: '.print_r($arr_contract_informations,true), 'Function to construct the Payment Method - Direct Deposit block');
    $return_xml          = '';
    $direct_deposit_flag = 0;
    $dd_bank_code_null                = ($arr_contract_informations['payment_method']['dd_bank_code'] == '') ? ' xsi:nil="true"' : '';
    $dd_branch_code_null              = ($arr_contract_informations['payment_method']['dd_branch_code'] == '') ? ' xsi:nil="true"' : '';
    $dd_deposit_type_null             = ($arr_contract_informations['payment_method']['dd_deposit_type'] == '') ? ' xsi:nil="true"' : '';
    $dd_account_number_null           = ($arr_contract_informations['payment_method']['dd_account_number'] == '') ? ' xsi:nil="true"' : '';
    $dd_name_of_person_in_charge_null = ($arr_contract_informations['payment_method']['dd_name_of_person_in_charge'] == '') ? ' xsi:nil="true"' : '';
    $dd_telephone_number_null         = ($arr_contract_informations['payment_method']['dd_telephone_number'] == '') ? ' xsi:nil="true"' : '';
    $dd_email_address_null            = ($arr_contract_informations['payment_method']['dd_email_address'] == '') ? ' xsi:nil="true"' : '';
    $dd_remarks_null                  = ($arr_contract_informations['payment_method']['dd_remarks'] == '') ? ' xsi:nil="true"' : '';

    if (!empty($dd_bank_code_null) && !empty($dd_branch_code_null) && !empty($dd_deposit_type_null) && !empty($dd_account_number_null) && !empty($dd_name_of_person_in_charge_null) && !empty($dd_telephone_number_null) && !empty($dd_email_address_null) && !empty($dd_remarks_null)) {
        $direct_deposit_flag = 1;
    }

    $return_xml = '
    <gcm:payment-method name="Direct deposit payment">';

    if ($direct_deposit_flag == 0) {
        $return_xml .= '
        <gcm:jpn-direct-deposit-pm>
            <gcm:bank-code' . $dd_bank_code_null . '>' . $arr_contract_informations['payment_method']['dd_bank_code'] . '</gcm:bank-code>
            <gcm:branch-code' . $dd_branch_code_null . '>' . $arr_contract_informations['payment_method']['dd_branch_code'] . '</gcm:branch-code>
            <gcm:deposit-type' . $dd_deposit_type_null . '>' . $arr_contract_informations['payment_method']['dd_deposit_type'] . '</gcm:deposit-type>
            <gcm:account-number' . $dd_account_number_null . '>' . $arr_contract_informations['payment_method']['dd_account_number'] . '</gcm:account-number>
            <gcm:name-of-person-in-charge' . $dd_name_of_person_in_charge_null . '>' . $arr_contract_informations['payment_method']['dd_name_of_person_in_charge'] . '</gcm:name-of-person-in-charge>
            <gcm:telephone-number' . $dd_telephone_number_null . '>' . $arr_contract_informations['payment_method']['dd_telephone_number'] . '</gcm:telephone-number>
            <gcm:email-address' . $dd_email_address_null . '>' . $arr_contract_informations['payment_method']['dd_email_address'] . '</gcm:email-address>
            <gcm:remarks' . $dd_remarks_null . '>' . $arr_contract_informations['payment_method']['dd_remarks'] . '</gcm:remarks>
        </gcm:jpn-direct-deposit-pm>';
    } else {
        $return_xml .= '
        <gcm:jpn-direct-deposit-pm xsi:nil="true"/>';
    }
    $return_xml .= '
    </gcm:payment-method>
    ';
    unset($arr_contract_informations);
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_xml: '.$return_xml, '');
    return $return_xml;
}

// Payment Method : Account Transfer Block
function xmlPaymentMethodAccountTransfer($arr_contract_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_informations: '.print_r($arr_contract_informations,true), 'Function to construct the Payment Method - Account Transfer block');
    $return_xml = '';
    $return_xml = '
    <gcm:payment-method name="Account transfer payment">
        <gcm:jpn-account-transfer-pm>
            <gcm:bank-account-holder-name>' . $arr_contract_informations['payment_method']['at_bank_account_holder_name'] . '</gcm:bank-account-holder-name>
            <gcm:bank-code>' . $arr_contract_informations['payment_method']['at_bank_code'] . '</gcm:bank-code>
            <gcm:branch-code>' . $arr_contract_informations['payment_method']['at_branch_code'] . '</gcm:branch-code>
            <gcm:deposit-type>' . $arr_contract_informations['payment_method']['at_deposit_type'] . '</gcm:deposit-type>
            <gcm:account-number>' . $arr_contract_informations['payment_method']['at_account_number'] . '</gcm:account-number>
            <gcm:is-account-number-display>' . $arr_contract_informations['payment_method']['at_is_account_number_display'] . '</gcm:is-account-number-display>
        </gcm:jpn-account-transfer-pm>
    </gcm:payment-method>
    ';
    unset($arr_contract_informations);
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_xml: '.$return_xml, '');
    return $return_xml;
}

// Payment Method : Postal Transfer Block
function xmlPaymentMethodPostalTransfer($arr_contract_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_informations: '.print_r($arr_contract_informations,true), 'Function to construct the Payment Method - Postal Transfer block');
    $return_xml = '';
    $return_xml = '
    <gcm:payment-method name="Postal transfer payment">
        <gcm:jpn-postal-transfer-pm>
            <gcm:postal-account-holder-name>' . $arr_contract_informations['payment_method']['pt_postal_account_holder_name'] . '</gcm:postal-account-holder-name>
            <gcm:postal-passbook-mark>' . $arr_contract_informations['payment_method']['pt_postal_passbook_mark'] . '</gcm:postal-passbook-mark>
            <gcm:postal-passbook-number>' . $arr_contract_informations['payment_method']['pt_postal_passbook_number'] . '</gcm:postal-passbook-number>
            <gcm:is-postal-passbook-number-display>' . $arr_contract_informations['payment_method']['pt_is_postal_passbook_number_display'] . '</gcm:is-postal-passbook-number-display>
        </gcm:jpn-postal-transfer-pm>
    </gcm:payment-method>
    ';
    unset($arr_contract_informations);
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_xml: '.$return_xml, '');
    return $return_xml;
}

// Payment Method : Credit Card Block
function xmlPaymentMethodCreditCard($arr_contract_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_informations: '.print_r($arr_contract_informations,true), 'Function to construct the Payment Method - Credit Card block');
    $return_xml = '';
    $return_xml = '
    <gcm:payment-method name="Credit card payment">
        <gcm:credit-card-pm>
            <gcm:credit-card-number>' . $arr_contract_informations['payment_method']['cc_credit_card_number'] . '</gcm:credit-card-number>
            <gcm:expiration-date>' . $arr_contract_informations['payment_method']['cc_expiration_date'] . '</gcm:expiration-date>
        </gcm:credit-card-pm>
    </gcm:payment-method>
    ';
    unset($arr_contract_informations);
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_xml: '.$return_xml, '');
    return $return_xml;
}

// Product Biitems Block
function xmlProductBiitems($arr_contract_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_informations: '.print_r($arr_contract_informations,true), 'Function to construct the Product Bi-item block');
    $return_xml               = '';
    $sequence_no              = 1;
    $primary_url              = $arr_contract_informations['glb_parameters']['api_url'];
    $arr_contract_information = array();
    $return_xml = '
    <bi:product-biitems>';
    foreach ($arr_contract_informations['product_biitems'] as $arr_contract_information) {
        $return_xml .= '
        <bi:product-biitem id="' . $arr_contract_information['char_uuid'] . ':' . $arr_contract_information['atomic_products_version'] . '">
            <cbe:identified-by>
                <bi:product-biitem-key>
                    <cbe:key-type-for-cbe-standard>
                        <cbe:object-id>' . $arr_contract_information['char_uuid'] . '</cbe:object-id>
                        <cbe:object-version>' . $arr_contract_information['atomic_products_version'] . '</cbe:object-version>
                    </cbe:key-type-for-cbe-standard>
                </bi:product-biitem-key>
            </cbe:identified-by>
            <cbe:standard-state>' . $arr_contract_information['standard_state'] . '</cbe:standard-state>
            <cbe:standard-business-state>' . $arr_contract_information['standard_business_state'] . '</cbe:standard-business-state>
            <cbe:association-ends>
                <cbe:association-end name="constitutes">
                    <cbe:no>1</cbe:no>
                    <cbe:role>Product contract</cbe:role>
                    <cbe:target>
                        <bi:reference-to-product-contract>
                            <cbe:link>
                                <cbe:rel>entity</cbe:rel>
                                <cbe:href>' . $primary_url . '/product-contracts/' . $arr_contract_information['product_contract_uuid'] . ':' . $arr_contract_information['product_contract_version'] . '</cbe:href>
                            </cbe:link>
                            <cbe:xml-path>
                                <cbe:location-path>bi:product-contracts/bi:product-contract[attribute::id=&quot;' . $arr_contract_information['product_contract_uuid'] . ':' . $arr_contract_information['product_contract_version'] . '&quot;]</cbe:location-path>
                            </cbe:xml-path>
                            <bi:resource-element-name>bi:product-contract</bi:resource-element-name>
                            <bi:product-contract-key>
                                <cbe:key-type-for-cbe-standard>
                                    <cbe:object-id>' . $arr_contract_information['product_contract_uuid'] . '</cbe:object-id>
                                    <cbe:object-version>' . $arr_contract_information['product_contract_version'] . '</cbe:object-version>
                                </cbe:key-type-for-cbe-standard>
                            </bi:product-contract-key>
                        </bi:reference-to-product-contract>
                    </cbe:target>
                </cbe:association-end>
                <cbe:association-end name="involves">
                    <cbe:no>2</cbe:no>
                    <cbe:role>Product</cbe:role>
                    <cbe:target>
                        <product:reference-to-atomic-product>
                            <cbe:link>
                                <cbe:rel>entity</cbe:rel>
                                <cbe:href>' . $primary_url . '/atomic-products/' . $arr_contract_information['atomic_products_uuid'] . '</cbe:href>
                            </cbe:link>
                            <cbe:xml-path>
                                <cbe:location-path>product:atomic-products/product:atomic-product[attribute::id=&quot;' . $arr_contract_information['atomic_products_uuid'] . ':' . $arr_contract_information['atomic_products_version'] . '&quot;]</cbe:location-path>
                            </cbe:xml-path>
                            <product:resource-element-name>product:atomic-product</product:resource-element-name>
                            <product:atomic-product-key>
                                <cbe:key-type-for-cbe-standard>
                                    <cbe:object-id>' . $arr_contract_information['atomic_products_uuid'] . '</cbe:object-id>
                                    <cbe:object-version>' . $arr_contract_information['atomic_products_version'] . '</cbe:object-version>
                                </cbe:key-type-for-cbe-standard>
                            </product:atomic-product-key>
                        </product:reference-to-atomic-product>
                    </cbe:target>
                </cbe:association-end>
            </cbe:association-ends>
            <cbe:described-by>
                <bi:reference-to-product-biitem-spec>
                    <cbe:link>
                        <cbe:rel>specification</cbe:rel>
                        <cbe:href>' . $primary_url . '/product-biitem-specifications/' . $arr_contract_information['described_by'] . '</cbe:href>
                    </cbe:link>
                    <bi:resource-element-name>bi:product-biitem-specification</bi:resource-element-name>
                    <bi:product-biitem-spec-key>
                        <cbe:key-type-for-cbe-standard>
                            <cbe:object-id>' . $arr_contract_information['described_by'] . '</cbe:object-id>
                            <cbe:object-version>' . $arr_contract_information['atomic_products_version'] . '</cbe:object-version>
                        </cbe:key-type-for-cbe-standard>
                    </bi:product-biitem-spec-key>
                </bi:reference-to-product-biitem-spec>
            </cbe:described-by>
            <bi:sequence>' . $sequence_no . '</bi:sequence>
            <bi:action>' . $arr_contract_information['action'] . '</bi:action>
        </bi:product-biitem>
        ';
        $sequence_no++;
    }

    $return_xml .= '
    </bi:product-biitems>';
    unset($arr_contract_information, $arr_contract_informations);
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_xml: '.$return_xml, '');
    return $return_xml;
}

// Characteristic Value Block
function xmlCharacteristicValue($arr_contract_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_informations: '.print_r($arr_contract_informations,true), 'Function to construct the All characteristic value block');
    $return_xml               = '';
    $team_sequence_no         = 1;
    $primary_url              = $arr_contract_informations['glb_parameters']['api_url'];
    $arr_contract_information = array();

    $return_xml = '
    <cbe:characteristic-values>';
    if (isset($arr_contract_informations['ap_char_val'])) {
        foreach ($arr_contract_informations['ap_char_val'] as $arr_contract_information) {

            $return_xml .= '
        <cbe:characteristic-value id="' . $arr_contract_information['char_uuid'] . ':' . $arr_contract_information['version'] . '">
            <cbe:identified-by>
                <cbe:charc-value-key>
                    <cbe:key-type-for-cbe-standard>
                        <cbe:object-id>' . $arr_contract_information['char_uuid'] . '</cbe:object-id>
                        <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                    </cbe:key-type-for-cbe-standard>
                </cbe:charc-value-key>
            </cbe:identified-by>
            <cbe:association-ends>
                <cbe:association-end name="defines-configuration-of">
                    <cbe:no>1</cbe:no>
                    <cbe:role>Product</cbe:role>
                    <cbe:target>
                        <product:reference-to-atomic-product>
                            <cbe:link>
                                <cbe:rel>entity</cbe:rel>
                                <cbe:href>' . $primary_url . '/atomic-products/' . $arr_contract_information['atomic_products_uuid'] . '</cbe:href>
                            </cbe:link>
                            <cbe:xml-path>
                                <cbe:location-path>product:atomic-products/product:atomic-product[attribute::id=&quot;' . $arr_contract_information['atomic_products_uuid'] . ':' . $arr_contract_information['version'] . '&quot;]</cbe:location-path>
                            </cbe:xml-path>
                            <product:resource-element-name>product:atomic-product</product:resource-element-name>
                            <product:atomic-product-key>
                                <cbe:key-type-for-cbe-standard>
                                    <cbe:object-id>' . $arr_contract_information['atomic_products_uuid'] . '</cbe:object-id>
                                    <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                                </cbe:key-type-for-cbe-standard>
                            </product:atomic-product-key>
                        </product:reference-to-atomic-product>
                    </cbe:target>
                </cbe:association-end>
                <cbe:association-end name="located-in">
                    <cbe:no>4</cbe:no>
                    <cbe:role>Entity spec characteristic</cbe:role>
                    <cbe:target>
                        <cbe:reference-to-atomic-entity-spec-charc>
                            <cbe:link>
                                <cbe:rel>specification</cbe:rel>
                                <cbe:href>' . $primary_url . '/atomic-entity-spec-characteristics/' . $arr_contract_information['pms_uuid'] . '</cbe:href>
                            </cbe:link>
                            <cbe:resource-element-name>cbe:atomic-entity-spec-characteristic</cbe:resource-element-name>
                            <cbe:atomic-entity-spec-charc-key>
                                <cbe:key-type-for-cbe-standard>
                                    <cbe:object-id>' . $arr_contract_information['pms_uuid'] . '</cbe:object-id>
                                    <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                                </cbe:key-type-for-cbe-standard>
                            </cbe:atomic-entity-spec-charc-key>
                        </cbe:reference-to-atomic-entity-spec-charc>
                    </cbe:target>
                </cbe:association-end>
            </cbe:association-ends>
            <' . $arr_contract_information['tag_name'] . '>' . $arr_contract_information['tag_value'] . '</' . $arr_contract_information['tag_name'] . '>
        </cbe:characteristic-value>
        ';
        }

        unset($arr_contract_information);
    }

    if (isset($arr_contract_informations['app_char_val'])) {
        foreach ($arr_contract_informations['app_char_val'] as $arr_contract_information) {

            $return_xml .= '
            <cbe:characteristic-value id="' . $arr_contract_information['char_uuid'] . ':' . $arr_contract_information['version'] . '">
                <cbe:identified-by>
                    <cbe:charc-value-key>
                        <cbe:key-type-for-cbe-standard>
                            <cbe:object-id>' . $arr_contract_information['char_uuid'] . '</cbe:object-id>
                            <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                        </cbe:key-type-for-cbe-standard>
                    </cbe:charc-value-key>
                </cbe:identified-by>
                <cbe:association-ends>
                    <cbe:association-end name="defines-configuration-of">
                        <cbe:no>1</cbe:no>
                        <cbe:role>Product price</cbe:role>
                        <cbe:target>
                            <product:reference-to-atomic-product-price>
                                <cbe:link>
                                    <cbe:rel>entity</cbe:rel>
                                    <cbe:href>' . $primary_url . '/atomic-product-prices/' . $arr_contract_information['ap_price_uuid'] . '</cbe:href>
                                </cbe:link>
                                <cbe:xml-path>
                                    <cbe:location-path>product:atomic-product-prices/product:atomic-product-price[attribute::id=&quot;' . $arr_contract_information['ap_price_uuid'] . ':' . $arr_contract_information['version'] . '&quot;]</cbe:location-path>
                                </cbe:xml-path>
                                <product:resource-element-name>product:atomic-product-price</product:resource-element-name>
                                <product:atomic-product-price-key>
                                    <cbe:key-type-for-cbe-standard>
                                        <cbe:object-id>' . $arr_contract_information['ap_price_uuid'] . '</cbe:object-id>
                                        <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                                    </cbe:key-type-for-cbe-standard>
                                </product:atomic-product-price-key>
                            </product:reference-to-atomic-product-price>
                        </cbe:target>
                    </cbe:association-end>
                    <cbe:association-end name="located-in">
                        <cbe:no>4</cbe:no>
                        <cbe:role>Entity spec characteristic</cbe:role>
                        <cbe:target>
                            <cbe:reference-to-atomic-entity-spec-charc>
                                <cbe:link>
                                    <cbe:rel>specification</cbe:rel>
                                    <cbe:href>' . $primary_url . '/atomic-entity-spec-characteristics/' . $arr_contract_information['described_by'] . '</cbe:href>
                                </cbe:link>
                                <cbe:resource-element-name>cbe:atomic-entity-spec-characteristic</cbe:resource-element-name>
                                <cbe:atomic-entity-spec-charc-key>
                                    <cbe:key-type-for-cbe-standard>
                                        <cbe:object-id>' . $arr_contract_information['described_by'] . '</cbe:object-id>
                                        <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                                    </cbe:key-type-for-cbe-standard>
                                </cbe:atomic-entity-spec-charc-key>
                            </cbe:reference-to-atomic-entity-spec-charc>
                        </cbe:target>
                    </cbe:association-end>
                </cbe:association-ends>
                <gmone-prod-price-001:' . $arr_contract_information['tag'] . '-val>' . $arr_contract_information['value'] . '</gmone-prod-price-001:' . $arr_contract_information['tag'] . '-val>
            </cbe:characteristic-value>
            ';
        }

        unset($arr_contract_information);
    }
    if (isset($arr_contract_informations['pc_char_val'])) {
        foreach ($arr_contract_informations['pc_char_val'] as $arr_contract_information) {

            $return_xml .= '
            <cbe:characteristic-value id="' . $arr_contract_information['char_uuid'] . ':' . $arr_contract_information['version'] . '">
                <cbe:identified-by>
                    <cbe:charc-value-key>
                        <cbe:key-type-for-cbe-standard>
                            <cbe:object-id>' . $arr_contract_information['char_uuid'] . '</cbe:object-id>
                            <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                        </cbe:key-type-for-cbe-standard>
                    </cbe:charc-value-key>
                </cbe:identified-by>
                <cbe:association-ends>
                    <cbe:association-end name="defines-configuration-of">
                        <cbe:no>1</cbe:no>
                        <cbe:role>Product contract</cbe:role>
                        <cbe:target>
                            <bi:reference-to-product-contract>
                                <cbe:link>
                                    <cbe:rel>entity</cbe:rel>
                                    <cbe:href>' . $primary_url . '/product-contracts/' . $arr_contract_information['pc_uuid'] . ':' . $arr_contract_information['pc_version'] . '</cbe:href>
                                </cbe:link>
                                <cbe:xml-path>
                                    <cbe:location-path>bi:product-contracts/bi:product-contract[attribute::id=&quot;' . $arr_contract_information['pc_uuid'] . ':' . $arr_contract_information['pc_version'] . '&quot;]</cbe:location-path>
                                </cbe:xml-path>
                                <bi:resource-element-name>bi:product-contract</bi:resource-element-name>
                                <bi:product-contract-key>
                                    <cbe:key-type-for-cbe-standard>
                                        <cbe:object-id>' . $arr_contract_information['pc_uuid'] . '</cbe:object-id>
                                        <cbe:object-version>' . $arr_contract_information['pc_version'] . '</cbe:object-version>
                                    </cbe:key-type-for-cbe-standard>
                                </bi:product-contract-key>
                            </bi:reference-to-product-contract>
                        </cbe:target>
                    </cbe:association-end>
                    <cbe:association-end name="located-in">
                        <cbe:no>4</cbe:no>
                        <cbe:role>Entity spec characteristic</cbe:role>
                        <cbe:target>
                            <cbe:reference-to-atomic-entity-spec-charc>
                                <cbe:link>
                                    <cbe:rel>specification</cbe:rel>
                                    <cbe:href>' . $primary_url . '/atomic-entity-spec-characteristics/' . $arr_contract_information['described_by'] . '</cbe:href>
                                </cbe:link>
                                <cbe:resource-element-name>cbe:atomic-entity-spec-characteristic</cbe:resource-element-name>
                                <cbe:atomic-entity-spec-charc-key>
                                    <cbe:key-type-for-cbe-standard>
                                        <cbe:object-id>' . $arr_contract_information['described_by'] . '</cbe:object-id>
                                        <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                                    </cbe:key-type-for-cbe-standard>
                                </cbe:atomic-entity-spec-charc-key>
                            </cbe:reference-to-atomic-entity-spec-charc>
                        </cbe:target>
                    </cbe:association-end>
                </cbe:association-ends>';

            if ($arr_contract_information['tag'] == 'teams') {
                $return_xml .= '
                <cbe:sequence>' . $team_sequence_no . '</cbe:sequence>';
                $team_sequence_no++;
            }

            $return_xml .= '
                <gmone-pcs-001:' . $arr_contract_information['tag'] . '-val>' . $arr_contract_information['value'] . '</gmone-pcs-001:' . $arr_contract_information['tag'] . '-val>
            </cbe:characteristic-value>
            ';
        }

        unset($arr_contract_information);
    }
    if (isset($arr_contract_informations['apbi_char_val'])) {
        foreach ($arr_contract_informations['apbi_char_val'] as $arr_contract_information) {

            $return_xml .= '
            <cbe:characteristic-value id="' . $arr_contract_information['char_uuid'] . ':' . $arr_contract_information['version'] . '">
                <cbe:identified-by>
                    <cbe:charc-value-key>
                        <cbe:key-type-for-cbe-standard>
                            <cbe:object-id>' . $arr_contract_information['char_uuid'] . '</cbe:object-id>
                            <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                        </cbe:key-type-for-cbe-standard>
                    </cbe:charc-value-key>
                </cbe:identified-by>
                <cbe:association-ends>
                    <cbe:association-end name="defines-configuration-of">
                        <cbe:no>1</cbe:no>
                        <cbe:role>Product bi item</cbe:role>
                        <cbe:target>
                            <bi:reference-to-product-biitem>
                                <cbe:link>
                                    <cbe:rel>entity</cbe:rel>
                                    <cbe:href>' . $primary_url . '/product-biitems/' . $arr_contract_information['pbi_uuid'] . '</cbe:href>
                                </cbe:link>
                                <cbe:xml-path>
                                    <cbe:location-path>bi:product-biitems/bi:product-biitem[attribute::id=&quot;' . $arr_contract_information['pbi_uuid'] . ':' . $arr_contract_information['version'] . '&quot;]</cbe:location-path>
                                </cbe:xml-path>
                                <bi:resource-element-name>bi:product-biitem</bi:resource-element-name>
                                <bi:product-biitem-key>
                                    <cbe:key-type-for-cbe-standard>
                                        <cbe:object-id>' . $arr_contract_information['pbi_uuid'] . '</cbe:object-id>
                                        <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                                    </cbe:key-type-for-cbe-standard>
                                </bi:product-biitem-key>
                            </bi:reference-to-product-biitem>
                        </cbe:target>
                    </cbe:association-end>
                    <cbe:association-end name="located-in">
                        <cbe:no>4</cbe:no>
                        <cbe:role>Entity spec characteristic</cbe:role>
                        <cbe:target>
                            <cbe:reference-to-atomic-entity-spec-charc>
                                <cbe:link>
                                    <cbe:rel>specification</cbe:rel>
                                    <cbe:href>' . $primary_url . '/atomic-entity-spec-characteristics/' . $arr_contract_information['described_by'] . '</cbe:href>
                                </cbe:link>
                                <cbe:resource-element-name>cbe:atomic-entity-spec-characteristic</cbe:resource-element-name>
                                <cbe:atomic-entity-spec-charc-key>
                                    <cbe:key-type-for-cbe-standard>
                                        <cbe:object-id>' . $arr_contract_information['described_by'] . '</cbe:object-id>
                                        <cbe:object-version>' . $arr_contract_information['version'] . '</cbe:object-version>
                                    </cbe:key-type-for-cbe-standard>
                                </cbe:atomic-entity-spec-charc-key>
                            </cbe:reference-to-atomic-entity-spec-charc>
                        </cbe:target>
                    </cbe:association-end>
                </cbe:association-ends>
                <prod-cntrct-item-000002:' . $arr_contract_information['tag'] . '-val>' . $arr_contract_information['value'] . '</prod-cntrct-item-000002:' . $arr_contract_information['tag'] . '-val>
            </cbe:characteristic-value>
            ';
        }

        unset($arr_contract_information);
    }
    $return_xml .= '
    </cbe:characteristic-values>';
    unset($arr_contract_informations);
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return_xml: '.$return_xml, '');
    return $return_xml;

}

// XML product root block
function xmlProductRootBlock($arrParameters)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arrParameters: '.print_r($arrParameters,true), 'Function to construct the root block for product list xml');
    $returnXML = '<?xml version="1.0" encoding="UTF-8"?>
    <gcm-lopc:list-of-product-contracts ';
    foreach ($arrParameters as $key => $value) {
        $returnXML .= $key . '="' . $value . '" ';
    }
    $returnXML .= '>
    ';
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'returnXM: '.$returnXML, '');
    return $returnXML;
}

// XML product contract block
function xmlProductContratBlock($arr_contract_informations)
{
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'arr_contract_informations: '.print_r($arr_contract_informations,true), 'Function to construct the root block for product contract xml');
    $returnXML = "";
    foreach ($arr_contract_informations as $arr_contract_information) {
        $returnXML .= '
                <gcm-lopc:product-contract id="' . $arr_contract_information['product_contract_uuid'] . ':' . $arr_contract_information['version'] . '"/>';
    }
    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'returnXM: '.$returnXML, '');
    return $returnXML;
}
