<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
 /**
 * @type unix-file system UTF-8
 */
require_once ('custom/include/XMLUtils.php');

class ClassContractUpdateXMLParse extends XMLUtils
{

    function getContractParseArray($contract_update_xml_data)
    {
        $ref_array = array(
            // Billing Account /Attribute (Contract Person Name Latin Character)
            '84c0db7d-0b23-4fbf-8c92-8eb8e9683161' => array(
                'xmlTagName' => 'individualNameLatin',
                'dbFieldName' => 'last_name',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts'
            ),
            // Billing Account /Attribute (Contract Person Name )
            '5dab5bbc-0b23-4fba-a492-a1b4226cadb0' => array(
                'xmlTagName' => 'individualName',
                'dbFieldName' => 'name_2_c',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts'
            ),
            // Billing Account /Corporate Name Latin
            '1c93d00a-0b23-4fbe-bd97-2870c75aa467' => array(
                'xmlTagName' => 'corporationNameLatin',
                'dbFieldName' => 'name',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts',
                'relateModule' => 'CorporateDetails'
            ),
            // Billing Account /Corporate Name Local
            '4dc8cb11-0b23-4fb3-a691-a994d1c85306' => array(
                'xmlTagName' => 'corporationName',
                'dbFieldName' => 'name_2_c',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts',
                'relateModule' => 'CorporateDetails'
            ),
            // Billing Account /Corporate Name Katakana
            '441e993a-0b23-4fb0-b795-0d4fafb868c3' => array(
                'xmlTagName' => 'corporationNameFurigana',
                'dbFieldName' => 'name_3_c',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts',
                'relateModule' => 'CorporateDetails'
            ),
            // Billing Postal Number
            'e6341040-0b23-4fb4-a517-29721dbac53f' => array(
                'xmlTagName' => 'postalCode',
                'dbFieldName' => 'postal_no_c',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts'
            ),
            // Billing Address 1 [Japan specific]
            'c6e2372f-0b23-4fb0-83f5-242b7f786f72' => array(
                'xmlTagName' => 'address',
                'dbFieldName' => 'addr1',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts',
                'relateModule' => 'Accounts_js'
            ),
            // Billing USA Address line
            'b8e3b95f-0b23-4fb3-ab41-534a39fea399' => array(
                'xmlTagName' => 'addressLine',
                'dbFieldName' => 'addr_general_c',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts',
                'relateModule' => 'general_address'
            ),
            // Billing USA City
            'e61d79c5-0b23-4fb6-81e4-4dd99a1b1f94' => array(
                'xmlTagName' => 'city',
                'dbFieldName' => 'city_general_c',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts',
                'relateModule' => 'general_address'
            ),
            // Billing USA State
            'b3750b56-0b23-4fb3-a8c6-894d9dd0b52b' => array(
                'xmlTagName' => 'stateAbbr',
                'dbFieldName' => 'state_general_code',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts',
                'relateModule' => 'general_address'
            ),
            // Billing USA Zip-code
            'c75cc5f6-0b23-4fb3-b425-d7508a9316b7' => array(
                'xmlTagName' => 'zipCode',
                'dbFieldName' => 'postal_no_c',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts'
            ),
            // Billing Canada Address line
            '4b79b11e-0b23-4fb6-beec-289383f2c7f0' => array(
                'xmlTagName' => 'addressLine',
                'dbFieldName' => 'addr_general_c',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts',
                'relateModule' => 'general_address'
            ),
            // Billing Canada City
            '64cb3aea-0b23-4fbc-a094-d2808fa5b2f2' => array(
                'xmlTagName' => 'city',
                'dbFieldName' => 'city_general_c',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts',
                'relateModule' => 'general_address'
            ),
            // Billing Canada State
            'b65b38c4-0b23-4fb3-9327-c414730ea7c0' => array(
                'xmlTagName' => 'stateAbbr',
                'dbFieldName' => 'state_general_code',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts',
                'relateModule' => 'general_address'
            ),
            // Billing Canada Zip-code
            '295994a4-0b23-4fba-9b25-0a90d8be4ef2' => array(
                'xmlTagName' => 'postalCode',
                'dbFieldName' => 'postal_no_c',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts'
            ),
            // Billing Account /Attribute Values(Division )
            '27a49571-0b23-4fb4-a2de-3ceb449f8eef' => array(
                'xmlTagName' => 'divisionName',
                'dbFieldName' => 'division_2_c',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts'
            ),
            // Billing Account /Attribute Values(Division Latin Character)
            '3af86669-0b23-4fbe-9970-223d420ee624' => array(
                'xmlTagName' => 'divisionNameLatin',
                'dbFieldName' => 'division_1_c',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts'
            ),
            // Billing Account Contact Info/Attribute Values(Tel no)
            'a59d300d-0b23-4fbe-bb48-33005d0e50ad' => array(
                'xmlTagName' => 'telephoneNumber',
                'dbFieldName' => 'phone_work',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts'
            ),
            // Billing Account Contact Info/Attribute Values(FAX no)
            '79ebbbea-0b23-4fb6-b700-281a2a3b0ebb' => array(
                'xmlTagName' => 'faxNumber',
                'dbFieldName' => 'phone_fax',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts'
            ),
            // Billing Account Contact Info/Attribute Values(E-mail address)
            '979469bc-0b23-4fb8-bd7a-4273b48a5953' => array(
                'xmlTagName' => 'eMailAddress',
                'dbFieldName' => 'email1',
                'accountType' => 'Billing',
                'moduleName' => 'Accounts'
            ),
            // Customer Technology Contract/Attribute (Contract Person Name Latin Character)
            '2798ed11-0b23-4fb4-a411-9d8c9e58fe7b' => array(
                'xmlTagName' => 'individualNameLatin',
                'dbFieldName' => 'last_name',
                'accountType' => 'Technology',
                'moduleName' => 'Accounts'
            ),
            // Customer Technology Contract/Attribute (Contract Person Name )
            '3b2691a9-0b23-4fbb-9f8d-1325a01dcb93' => array(
                'xmlTagName' => 'individualName',
                'dbFieldName' => 'name_2_c',
                'accountType' => 'Technology',
                'moduleName' => 'Accounts'
            ),
            // Customer Technology Account /Corporate Name Latin
            'ed0150f8-0b23-4fbd-84e6-1e373ffb7f8e' => array(
                'xmlTagName' => 'corporationNameLatin',
                'dbFieldName' => 'name',
                'accountType' => 'Technology',
                'moduleName' => 'Accounts',
                'relateModule' => 'CorporateDetails'
            ),
            // Customer Technology Account /Corporate Name Local
            '6381c2d6-0b23-4fb7-a21a-5f295b57e33c' => array(
                'xmlTagName' => 'corporationName',
                'dbFieldName' => 'name_2_c',
                'accountType' => 'Technology',
                'moduleName' => 'Accounts',
                'relateModule' => 'CorporateDetails'
            ),
            // Customer Technology Account /Corporate Name Katakana
            '1c9db7cd-0b23-4fb5-95a5-6f5ca14126b7' => array(
                'xmlTagName' => 'corporationNameFurigana',
                'dbFieldName' => 'name_3_c',
                'accountType' => 'Technology',
                'moduleName' => 'Accounts',
                'relateModule' => 'CorporateDetails'
            ),
            // Customer Technology Postal Number
            'c037efcd-0b23-4fb6-b94b-0c50007327f5' => array(
                'xmlTagName' => 'postalCode',
                'dbFieldName' => 'postal_no_c',
                'accountType' => 'Technology',
                'moduleName' => 'Accounts'
            ),
            // Customer Technology Address 1 [Japan specific]
            'a9bb942d-0b23-4fbb-9f23-d96fede9a62d' => array(
                'xmlTagName' => 'address',
                'dbFieldName' => 'addr1',
                'accountType' => 'Technology',
                'moduleName' => 'Accounts',
                'relateModule' => 'Accounts_js'
            ),
            // Customer Technology Address 2 [Japan specific]
            'fa0fac61-0b23-4fbb-bb45-b7ee759e6516' => array(
                'xmlTagName' => 'addressLine',
                'dbFieldName' => 'addr2',
                'accountType' => 'Technology',
                'moduleName' => 'Accounts',
                'relateModule' => 'Accounts_js'
            ),
            // Customer Technology Contract/Attribute Values(Division )
            'b46945f1-0b23-4fb0-ac79-f1e02ba75eca' => array(
                'xmlTagName' => 'divisionName',
                'dbFieldName' => 'division_2_c',
                'accountType' => 'Technology',
                'moduleName' => 'Accounts'
            ),
            // Customer Technology Contract/Attribute Values(Division Latin Character)
            // '481c11b5-0b23-4fb0-8212-b3b28424243e' => array(
            // 'xmlTagName' => 'divisionNameLatin',
            // 'dbFieldName' => 'division_1_c',
            // 'accountType' => 'Technology',
            // 'moduleName' => 'Accounts',
            // ) ,
            // Customer Technology ContractContact Info/Attribute Values(Tel no)
            '510d6a84-0b23-4fb2-8b0e-7e60c01ae663' => array(
                'xmlTagName' => 'telephoneNumber',
                'dbFieldName' => 'phone_work',
                'accountType' => 'Technology',
                'moduleName' => 'Accounts'
            ),
            // Customer Technology ContractContact Info/Attribute Values(FAX no)
            // '8528eede-0b23-4fb2-8d0f-9e1b13c977ba' => array(
            // 'xmlTagName' => 'faxNumber',
            // 'dbFieldName' => 'phone_fax',
            // 'accountType' => 'Technology',
            // 'moduleName' => 'Accounts',
            // ) ,
            // Customer Technology ContractContact Info/Attribute Values(E-mail address)
            'cf263f73-0b23-4fbc-bbef-09e67d4b5aea' => array(
                'xmlTagName' => 'eMailAddress',
                'dbFieldName' => 'email1',
                'accountType' => 'Technology',
                'moduleName' => 'Accounts'
            ),
            // Contracting Account /Attribute (Contract Person Name Latin Character)
            'cfb7f22a-0b23-4fb9-aa8f-56e74342da22' => array(
                'xmlTagName' => 'individualNameLatin',
                'dbFieldName' => 'last_name',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts'
            ),
            // Contracting Account /Attribute (Contract Person Name )
            '05593ed9-0b23-4fb0-98d6-68680931c5e0' => array(
                'xmlTagName' => 'individualName',
                'dbFieldName' => 'name_2_c',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts'
            ),
            // Contracting Account / Corporate ID
            '9512b60a-0b23-4fbe-9655-fdf88b53673d' => array(
                'xmlTagName' => 'corporationID',
                'dbFieldName' => 'common_customer_id_c',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts',
                'relateModule' => 'CorporateDetails'
            ),
            // Contract Postal Number
            '2a5d6e66-0b23-4fb3-b316-407fe1e69123' => array(
                'xmlTagName' => 'postalCode',
                'dbFieldName' => 'postal_no_c',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts'
            ),
            // Contract Address 1 [Japan specific]
            '2fefcc9b-0b23-4fba-8f44-66ef43abd279' => array(
                'xmlTagName' => 'address',
                'dbFieldName' => 'addr1',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts',
                'relateModule' => 'Accounts_js'
            ),
            // Contract Address 2 [Japan specific]
            '9a30aa4c-0b23-4fbb-8b86-cbeeda3d4fae' => array(
                'xmlTagName' => 'addressLine',
                'dbFieldName' => 'addr2',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts',
                'relateModule' => 'Accounts_js'
            ),
            // Contract USA Address line
            'f716882f-0b23-4fb1-98ae-a22d31534663' => array(
                'xmlTagName' => 'addressLine',
                'dbFieldName' => 'addr_general_c',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts',
                'relateModule' => 'general_address'
            ),
            // Contract USA City
            '5eb860be-0b23-4fbb-ba41-b62d34cdacfe' => array(
                'xmlTagName' => 'city',
                'dbFieldName' => 'city_general_c',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts',
                'relateModule' => 'general_address'
            ),
            // Contract USA State
            '10cbe88b-0b23-4fb8-8fea-157321523975' => array(
                'xmlTagName' => 'stateAbbr',
                'dbFieldName' => 'state_general_code',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts',
                'relateModule' => 'general_address'
            ),
            // Contract USA postal-code
            '73d7dd10-0b23-4fba-9bb4-6abe2a7b5dc1' => array(
                'xmlTagName' => 'zipCode',
                'dbFieldName' => 'postal_no_c',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts'
            ),
            // Contract Canada Address line
            '8c017f83-0b23-4fbc-8a76-423940d93650' => array(
                'xmlTagName' => 'addressLine',
                'dbFieldName' => 'addr_general_c',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts',
                'relateModule' => 'general_address'
            ),
            // Contract Canada City
            '6c1af77e-0b23-4fb3-afff-60ac7d090a74' => array(
                'xmlTagName' => 'city',
                'dbFieldName' => 'city_general_c',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts',
                'relateModule' => 'general_address'
            ),
            // Contract Canada State
            '5303cb73-0b23-4fbb-a356-740a7df9dcf2' => array(
                'xmlTagName' => 'stateAbbr',
                'dbFieldName' => 'state_general_code',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts',
                'relateModule' => 'general_address'
            ),
            // Contract Canada postal-code
            'e0b0f7eb-0b23-4fbd-b7a7-0d21705f23dc' => array(
                'xmlTagName' => 'postalCode',
                'dbFieldName' => 'postal_no_c',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts'
            ),
            // Contracting Account /Attribute Values(Division )
            '47e926d0-0b23-4fbb-a094-0bbe9f129454' => array(
                'xmlTagName' => 'divisionName',
                'dbFieldName' => 'division_2_c',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts'
            ),
            // Contracting Account /Attribute Values(Division Latin Character)
            '5230c2c5-0b23-4fb6-bb7a-dde626eebbdd' => array(
                'xmlTagName' => 'divisionNameLatin',
                'dbFieldName' => 'division_1_c',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts'
            ),
            // Contracting Account Contact Info/Attribute Values(Tel no)
            '2ab5b9df-0b23-4fb8-8ebb-8370ccbf12be' => array(
                'xmlTagName' => 'telephoneNumber',
                'dbFieldName' => 'phone_work',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts'
            ),
            // Contracting Account Contact Info/Attribute Values(FAX no)
            'c35d457b-0b23-4fb4-bed8-9e620f5bbc68' => array(
                'xmlTagName' => 'faxNumber',
                'dbFieldName' => 'phone_fax',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts'
            ),
            // Contracting Account Contact Info/Attribute Values(E-mail address)
            '537ac9fb-0b23-4fb0-a356-1c6f4ebb8491' => array(
                'xmlTagName' => 'eMailAddress',
                'dbFieldName' => 'email1',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts'
            ),
            // Contracting Account Vat No
            '6834d711-0b23-4fb2-a668-2bbb76f321ac' => array(
                'xmlTagName' => 'vatNo',
                'dbFieldName' => 'vat_no_c',
                'accountType' => 'Contract',
                'moduleName' => 'Accounts',
                'relateModule' => 'CorporateDetails'
            ),
            // Sales Contact Person Name (Latin Character)
            'dd67410b-0b23-4fbb-8e38-01563bfd93cb' => array(
                'xmlTagName' => 'individualNameLatin',
                'dbFieldName' => 'name',
                'moduleName' => 'ContractSalesRepresentative'
            ),
            // Sales Contact Person Name (Local Character)
            '546c56ec-0b23-4fb0-a496-4ff7c6c0de10' => array(
                'xmlTagName' => 'individualName',
                'dbFieldName' => 'sales_rep_name_1',
                'moduleName' => 'ContractSalesRepresentative'
            ),
            // Sales Channel Code
            'e4b03192-0b23-4fbc-aaf2-8f712eaf8504' => array(
                'xmlTagName' => 'salesChannelCode',
                'dbFieldName' => 'sales_channel_code',
                'moduleName' => 'ContractSalesRepresentative'
            ),
            // Sales Division (Latin Character)
            'cb792da0-0b23-4fb3-ae0b-2e93623b0972' => array(
                'xmlTagName' => 'divisionNameLatin',
                'dbFieldName' => 'division_1',
                'moduleName' => 'ContractSalesRepresentative'
            ),
            // Sales Division (Local Character)
            '76345c5d-0b23-4fba-9e7a-19a1c072219b' => array(
                'xmlTagName' => 'divisionName',
                'dbFieldName' => 'division_2',
                'moduleName' => 'ContractSalesRepresentative'
            ),
            // Sales Tel Number
            '0a0fc8ab-0b23-4fbe-9912-9c37c1040b1d' => array(
                'xmlTagName' => 'telephoneNumber',
                'dbFieldName' => 'tel_no',
                'moduleName' => 'ContractSalesRepresentative'
            ),
            // Sales FAX number
            '7f53ce8f-0b23-4fbb-b0b4-23512fcde675' => array(
                'xmlTagName' => 'faxNumber',
                'dbFieldName' => 'fax_no',
                'moduleName' => 'ContractSalesRepresentative'
            ),
            // Sales Email Address
            '55506b99-0b23-4fb0-adbb-ba9af9a81d91' => array(
                'xmlTagName' => 'eMailAddress',
                'dbFieldName' => 'email1',
                'moduleName' => 'ContractSalesRepresentative'
            ),
            // Sales Corporate Name
            '35fd0ebc-0b23-4fba-8317-ded8bcc9e6f0' => array(
                'xmlTagName' => 'corporationName',
                'dbFieldName' => 'salesCorporationName',
                'moduleName' => 'ContractSalesRepresentative'
            )
        );
        // Contracting Account Country Code
        $contact_country_code_id = array(
            '6462fabe-0b23-4fbe-817b-01281f763125', // Japan
            'e3ef5379-0b23-4fb0-b3c5-49f47424df39', // USA
            'ed6ee55e-0b23-4fbe-a09f-a1d7f28c5a73', // Canada
            'f5484485-0b23-4fbb-910b-8a8c4d2f20e0'
        ); // Other

        // Billing Account Country Code
        $billing_country_code_id = array(
            'cfd89aac-0b23-4fb8-9e64-7f5ac5bf5647', // Japan
            '4db67c35-0b23-4fb3-8f52-8cad913105f9', // USA
            'bce60a8d-0b23-4fb6-98f2-0eb2f862593e', // Canada
            'dd4261c5-0b23-4fbe-932c-d8c5f048bddd'
        ); // Other

        // Technology Account Country Code
        $tech_country_code_id = array(
            '3803bf2f-0b23-4fb6-a9b9-6a70d27523e6'
        );
        $xml = new DOMDocument();
        $xml->loadXML($contract_update_xml_data);
        $map_arr = array();
        $check_record = $xml->getElementsByTagName("characteristicValue");
        $update_record = $check_record->length;
        for ($i = 0; $i < $update_record; $i ++) {
            $tag_list = $xml->getElementsByTagName("characteristicValue")->item($i);
            $xml_array = $this->xml_to_array($tag_list);
            $read_arr_data = array();
            $arr_result = $this->display_array($xml_array, $read_arr_data);
            if ($ref_array[$arr_result['objectID']]['moduleName'] == 'Accounts') {
                if ($ref_array[$arr_result['objectID']]['relateModule']) {
                    $map_arr[$ref_array[$arr_result['objectID']]['moduleName']][$ref_array[$arr_result['objectID']]['accountType']][$ref_array[$arr_result['objectID']]['relateModule']][$ref_array[$arr_result['objectID']]['dbFieldName']] = $arr_result[$ref_array[$arr_result['objectID']]['xmlTagName']];
                } else {
                    $map_arr[$ref_array[$arr_result['objectID']]['moduleName']][$ref_array[$arr_result['objectID']]['accountType']][$ref_array[$arr_result['objectID']]['dbFieldName']] = $arr_result[$ref_array[$arr_result['objectID']]['xmlTagName']];
                }
            }
            if ($ref_array[$arr_result['objectID']]['moduleName'] == 'ContractSalesRepresentative') {
                $map_arr['ContractDetails'][$ref_array[$arr_result['objectID']]['moduleName']][0][$ref_array[$arr_result['objectID']]['dbFieldName']] = $arr_result[$ref_array[$arr_result['objectID']]['xmlTagName']];
            }
        }
        $check_record_country_code = $xml->getElementsByTagName("geographicAddress");
        $update_record_country_code = $check_record_country_code->length;
        for ($i = 0; $i < $update_record_country_code; $i ++) {
            $tag_list = $xml->getElementsByTagName("geographicAddress")->item($i);
            $xml_array = $this->xml_to_array($tag_list);

            if (in_array($xml_array['cbe-v0:identifiedBy']['location-v0:geoAddressKey']['cbe-v0:objectID'], $contact_country_code_id)) {
                $map_arr['Accounts']['Contract']['country_code_numeric'] = $xml_array['location-v0:countryKey']['location-v0:iso3166_1numeric'];
                if (! empty($map_arr['Accounts']['Contract']['country_code_numeric'])) {
                    if (! $this->checkLocation($map_arr['Accounts']['Contract']['country_code_numeric'])) {
                        if ($xml_array['location-v0:usedAddressRepresentation']['location-v0:countrySpecificAddress']['location-v0:languageNotation']['datatypes-v0:notation'] == 'Latin') {
                            $map_arr['Accounts']['Contract']['addr_1_c'] = $xml_array['location-v0:usedAddressRepresentation']['location-v0:countrySpecificAddress']['anyAddr:anyAddress']['anyAddr:address']['anyAddr:characteristicValue']['anyAddr:address'];
                            $map_arr['Accounts']['Contract']['addr_2_c'] = '';
                        }
                        if ($xml_array['location-v0:usedAddressRepresentation']['location-v0:countrySpecificAddress']['location-v0:languageNotation']['datatypes-v0:notation'] == 'Any') {
                            $map_arr['Accounts']['Contract']['addr_2_c'] = $xml_array['location-v0:usedAddressRepresentation']['location-v0:countrySpecificAddress']['anyAddr:anyAddress']['anyAddr:address']['anyAddr:characteristicValue']['anyAddr:address'];
                            $map_arr['Accounts']['Contract']['addr_1_c'] = '';
                        }
                    }
                }
            }

            if (in_array($xml_array['cbe-v0:identifiedBy']['location-v0:geoAddressKey']['cbe-v0:objectID'], $billing_country_code_id)) {
                $map_arr['Accounts']['Billing']['country_code_numeric'] = $xml_array['location-v0:countryKey']['location-v0:iso3166_1numeric'];
                if (! empty($map_arr['Accounts']['Billing']['country_code_numeric'])) {
                    if (! $this->checkLocation($map_arr['Accounts']['Billing']['country_code_numeric'])) {
                        if ($xml_array['location-v0:usedAddressRepresentation']['location-v0:countrySpecificAddress']['location-v0:languageNotation']['datatypes-v0:notation'] == 'Latin') {
                            $map_arr['Accounts']['Billing']['addr_1_c'] = $xml_array['location-v0:usedAddressRepresentation']['location-v0:countrySpecificAddress']['anyAddr:anyAddress']['anyAddr:address']['anyAddr:characteristicValue']['anyAddr:address'];
                            $map_arr['Accounts']['Billing']['addr_2_c'] = '';
                        }
                        if ($xml_array['location-v0:usedAddressRepresentation']['location-v0:countrySpecificAddress']['location-v0:languageNotation']['datatypes-v0:notation'] == 'Any') {
                            $map_arr['Accounts']['Billing']['addr_2_c'] = $xml_array['location-v0:usedAddressRepresentation']['location-v0:countrySpecificAddress']['anyAddr:anyAddress']['anyAddr:address']['anyAddr:characteristicValue']['anyAddr:address'];
                            $map_arr['Accounts']['Billing']['addr_1_c'] = '';
                        }
                    }
                }
            }
        }

        $contracting_cid = $map_arr['Accounts']['Contract']['CorporateDetails']['common_customer_id_c'];

        // Begin: Get Corporate details by CID
        if (! empty($contracting_cid)) {
            // Check corporate record in DB by cid
            require_once ('modules/Accounts/Account.php');
            $obj_corporates_bean = new Account();
            $obj_corporates_bean->retrieve_by_string_fields(array(
                'common_customer_id_c' => $contracting_cid
            ));
            if (empty($obj_corporates_bean->id)) {
                // Check cid exists in CIDAS API
                require_once ('custom/service/v4_1_custom/APIHelper/ClassCIDASConnection.php');
                $cidas_connection_obj = new ClassCIDASConnection();
                $cidas_record_by_id = $cidas_connection_obj->getCorporateDetailsFromCIDASByCID($contracting_cid, false);
                $error_occured = mb_strpos($cidas_record_by_id, 'exp-v0:exception');
                if ($error_occured) {
                    return $cidas_record_by_id;
                } else {
                    $contract_corporate_details = $cidas_record_by_id;
                }
            } else {
                // Populate corporate record from db into array
                $contract_corporate_details = array(
                    'common_customer_id_c' => $obj_corporates_bean->common_customer_id_c,
                    'name' => $obj_corporates_bean->name,
                    'name_2_c' => $obj_corporates_bean->name_2_c,
                    'name_3_c' => $obj_corporates_bean->name_3_c,
                    'country_code_c' => $obj_corporates_bean->country_code_c,
                    'country_name_c' => $obj_corporates_bean->country_name_c,
                    'customer_status_code_c' => $obj_corporates_bean->customer_status_code_c,
                    'hq_addr_1_c' => $obj_corporates_bean->hq_addr_1_c,
                    'hq_addr_2_c' => $obj_corporates_bean->hq_addr_2_c,
                    'hq_postal_no_c' => $obj_corporates_bean->hq_postal_no_c,
                    'reg_addr_1_c' => $obj_corporates_bean->reg_addr_1_c,
                    'reg_addr_2_c' => $obj_corporates_bean->reg_addr_2_c,
                    'reg_postal_no_c' => $obj_corporates_bean->reg_postal_no_c,
                    'cidas_data' => 'No',
                    'cidas_search' => 'No'
                );
            }
            $map_arr['Accounts']['Contract']['CorporateDetails'] = $contract_corporate_details;
        }
        // End: Get Corporate details by CID

        if (! empty($map_arr['Accounts']['Billing']['CorporateDetails']['name_2_c']) || ! empty($map_arr['Accounts']['Billing']['CorporateDetails']['name']) || ! empty($map_arr['Accounts']['Billing']['CorporateDetails']['name_3_c'])) {

            $billing_name = $map_arr['Accounts']['Billing']['CorporateDetails']['name'];
            $billing_name_2_c = $map_arr['Accounts']['Billing']['CorporateDetails']['name_2_c'];
            $billing_name_3_c = $map_arr['Accounts']['Billing']['CorporateDetails']['name_3_c'];

            $map_arr['Accounts']['Billing']['CorporateDetails'] = array(
                'common_customer_id_c' => '',
                'name' => $billing_name,
                'name_2_c' => $billing_name_2_c,
                'name_3_c' => $billing_name_3_c,
                'country_code_c' => '',
                'country_name_c' => '',
                'customer_status_code_c' => '',
                'hq_addr_1_c' => '',
                'hq_addr_2_c' => '',
                'hq_postal_no_c' => '',
                'reg_addr_1_c' => '',
                'reg_addr_2_c' => '',
                'reg_postal_no_c' => '',
                'cidas_data' => 'No',
                'cidas_search' => 'No'
            );
        }

        unset($map_arr['']);
        return $map_arr;
    }

    function display_array($xml_array, $read_arr_data)
    {
        foreach ($xml_array as $key => $value) {
            if (is_array($value)) {
                $tag_path = array(
                    "identifiedBy",
                    "charcValueKey"
                );
                if (in_array(substr($key, strpos($key, ":") + 1), $tag_path)) {
                    $read_arr_data = $this->display_array($value, $read_arr_data);
                }
            } else {
                $read_arr_data[substr($key, strpos($key, ":") + 1)] = $value;
            }
        }
        return $read_arr_data;
    }

    function xml_to_array($root)
    {
        $result = array();
        if ($root->hasAttributes()) {
            $attrs = $root->attributes;
            foreach ($attrs as $attr) {
                $result['@attributes'][$attr->name] = $attr->value;
            }
        }
        if ($root->hasChildNodes()) {
            $children = $root->childNodes;
            if ($children->length == 1) {
                $child = $children->item(0);
                if ($child->nodeType == XML_TEXT_NODE) {
                    $result['_value'] = $child->nodeValue;
                    return count($result) == 1 ? $result['_value'] : $result;
                }
            }
            $groups = array();
            foreach ($children as $child) {
                if (! isset($result[$child->nodeName])) {
                    $result[$child->nodeName] = $this->xml_to_array($child);
                } else {
                    if (! isset($groups[$child->nodeName])) {
                        $result[$child->nodeName] = array(
                            $result[$child->nodeName]
                        );
                        $groups[$child->nodeName] = 1;
                    }
                    $result[$child->nodeName][] = $this->xml_to_array($child);
                }
            }
        }
        return $result;
    }
}
?>
