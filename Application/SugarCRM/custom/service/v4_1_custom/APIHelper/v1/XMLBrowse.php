<?php

// Begin : Product XML Block 
/**
 * @type unix-file system UTF-8
 */

function checkBlockFlag($arrParameters = array()) 
{
	$returnFlag = 0;
	foreach($arrParameters['tmp'] as $arrKey => $arrVal) {
	   $arrVal = trim($arrVal);
		if(!empty($arrVal)) { 
			$returnFlag = 1; 
			break; 
		}      
	}
	return $returnFlag;
}

function xmlRootBlock($arrParameters = array()) 
{
	$returnXML = '<?xml version="1.0" encoding="UTF-8"?>
	<res-v0:response ';
	foreach($arrParameters as $key => $value)
	{
		$returnXML .= $key.'="'.$value.'" ';
	}
	$returnXML .='>
	';
	unset($arrParameters);

	return $returnXML;

}

function xmlProductBlock($arrParameters = array()) 
{

$returnXML = '<product-v0:bundledProduct>
		<cbe-v0:identifiedBy>
			<product-v0:bundledProductKey>
				<cbe-v0:type>BundledProduct</cbe-v0:type>
				<cbe-v0:objectID>410b7100-0b23-4fbd-9d9e-763ce233c0a1</cbe-v0:objectID>
			</product-v0:bundledProductKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>'.$arrParameters['glb']['productName'].'</cbe-v0:name>
		<cbe-v0:validFor>
			<datatypes-v0:startDateTime>2015-08-01T10:15:00+09:00</datatypes-v0:startDateTime>
			<datatypes-v0:endDateTime>9999-12-31T23:59:59+09:00</datatypes-v0:endDateTime>
		</cbe-v0:validFor>
		<cbe-v0:describedBy>
			<product-v0:referenceToBundledProductOffering>
				<cbe-v0:type>BundledProductOffering</cbe-v0:type>
				<cbe-v0:objectID>'.$arrParameters['glb']['productId'].'</cbe-v0:objectID>
			</product-v0:referenceToBundledProductOffering>
		</cbe-v0:describedBy>
		<product-v0:comprisedOf>
			<product-v0:atomicProduct>
				<cbe-v0:identifiedBy>
					<product-v0:atomicProductKey>
						<cbe-v0:type>AtomicProduct</cbe-v0:type>
						<cbe-v0:objectID>09b28f78-0b23-4fb8-bf72-df7be5f2602f</cbe-v0:objectID>
					</product-v0:atomicProductKey>
				</cbe-v0:identifiedBy>
				<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
				<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
				<cbe-v0:name>'.$arrParameters['glb']['productName'].'</cbe-v0:name>
				<cbe-v0:validFor>
					<datatypes-v0:startDateTime>2015-08-01T10:15:00+09:00</datatypes-v0:startDateTime>
					<datatypes-v0:endDateTime>9999-12-31T23:59:59+09:00</datatypes-v0:endDateTime>
				</cbe-v0:validFor>
				<cbe-v0:describedBy>
					<product-v0:referenceToAtomicProductOffering>
						<cbe-v0:type>AtomicProductOffering</cbe-v0:type>
						<cbe-v0:objectID>bc4362ac-0b23-4fbb-aee3-bd3ee5eb343a</cbe-v0:objectID>
					</product-v0:referenceToAtomicProductOffering>
				</cbe-v0:describedBy>
			</product-v0:atomicProduct>
		</product-v0:comprisedOf>
	</product-v0:bundledProduct>';

	unset($arrParameters);

	return $returnXML;

}

// End: Product XML Block 


function xmlProductContractBlock($arrParameters = array()) 
{

	$returnXML =  '
	<bi-v0:productContract>
		<cbe-v0:identifiedBy>
			<bi-v0:productContractKey>
				<cbe-v0:type>ProductContract</cbe-v0:type>
				<cbe-v0:objectID>'.$arrParameters['tmp']['contractUUID'].'</cbe-v0:objectID>
			</bi-v0:productContractKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:clientRequestID>'.$arrParameters['tmp']['clientRequestID'].'</cbe-v0:clientRequestID>
		<cbe-v0:name>'.$arrParameters['glb']['contractName'].'</cbe-v0:name>
		<cbe-v0:describedBy>
			<bi-v0:referenceToProductContractSpec>
				<cbe-v0:type>ProductContractSpecification</cbe-v0:type>
				<cbe-v0:objectID>d993ccb0-0b23-4fb7-b06b-2e54f88181a1</cbe-v0:objectID>
			</bi-v0:referenceToProductContractSpec>
		</cbe-v0:describedBy>
		<bi-v0:interactionDate>2015-08-01T10:15:00.555+09:00</bi-v0:interactionDate>
		<bi-v0:interactionDateComplete>9999-12-31T23:59:59.999+09:00</bi-v0:interactionDateComplete>
		<bi-v0:involvedRoles>
			<bi-v0:partyInteractionRoleKey name ="契約先">
				<cbe-v0:type>PartyInteractionRole</cbe-v0:type>
				<cbe-v0:objectID>53b963b7-0b23-4fbd-b701-950b4c19e438</cbe-v0:objectID>
			</bi-v0:partyInteractionRoleKey>
			<bi-v0:partyInteractionRoleKey name="契約先担当者">
				<cbe-v0:type>PartyInteractionRole</cbe-v0:type>
				<cbe-v0:objectID>75317ab8-0b23-4fba-9880-34400d2101f8</cbe-v0:objectID>
			</bi-v0:partyInteractionRoleKey>
			<bi-v0:partyInteractionRoleKey name="請求先">
				<cbe-v0:type>PartyInteractionRole</cbe-v0:type>
				<cbe-v0:objectID>30408e5e-0b23-4fbf-a2f1-fc012d312248</cbe-v0:objectID>
			</bi-v0:partyInteractionRoleKey>
			<bi-v0:partyInteractionRoleKey name="請求先担当者">
				<cbe-v0:type>PartyInteractionRole</cbe-v0:type>
				<cbe-v0:objectID>b7511051-0b23-4fb9-8915-3970473d31da</cbe-v0:objectID>
			</bi-v0:partyInteractionRoleKey>
			<bi-v0:partyInteractionRoleKey name="販売チャネル">
				<cbe-v0:type>PartyInteractionRole</cbe-v0:type>
				<cbe-v0:objectID>a664c0a6-0b23-4fb7-84b5-cde5ca8a8f31</cbe-v0:objectID>
			</bi-v0:partyInteractionRoleKey>
			<bi-v0:partyInteractionRoleKey name="販売担当者">
				<cbe-v0:type>PartyInteractionRole</cbe-v0:type>
				<cbe-v0:objectID>e757d58e-0b23-4fb5-91e0-194a49cdf683</cbe-v0:objectID>
			</bi-v0:partyInteractionRoleKey>			
		</bi-v0:involvedRoles>

		<bi-v0:businessInteractionItems>
			<bi-v0:productItem>
				<cbe-v0:identifiedBy>
					<bi-v0:productItemKey>
						<cbe-v0:type>ProductItem</cbe-v0:type>
						<cbe-v0:objectID>'.$arrParameters['tmp']['lintItemUUID'].'</cbe-v0:objectID>
					</bi-v0:productItemKey>
				</cbe-v0:identifiedBy>
				<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
				<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
				<cbe-v0:name>'.$arrParameters['glb']['productName'].'</cbe-v0:name>
				<cbe-v0:describedBy>
					<bi-v0:referenceToProductItemSpec>
						<cbe-v0:type>ProductItemSpecification</cbe-v0:type>
						<cbe-v0:objectID>256ef460-0b23-4fb0-a79f-84765eecb81e</cbe-v0:objectID>
					</bi-v0:referenceToProductItemSpec>
				</cbe-v0:describedBy>
				<prodCntrctItem0001:productContractItem0001 name="Characteristics of Product Contract Item (任意の名前を設定)">
					<prodCntrctItem0001:billingPeriod>
						<prodCntrctItem0001:billingStartDate>
							<prodCntrctItem0001:characteristicValue>
								<cbe-v0:identifiedBy>
									<cbe-v0:charcValueKey>
										<cbe-v0:type>CharacteristicValue</cbe-v0:type>
										<cbe-v0:objectID>008e8aa8-0b23-4fba-8b4b-f75676637769</cbe-v0:objectID>
									</cbe-v0:charcValueKey>
								</cbe-v0:identifiedBy>
								<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
								<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
								<cbe-v0:chValDescByChSpec>
									<cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
									<cbe-v0:objectID>082ffe16-0b23-4fbe-83cd-ae9f42f3dbdf</cbe-v0:objectID>
								</cbe-v0:chValDescByChSpec>
								<prodCntrctItem0001:billingStartDate>'.$arrParameters['tmp']['billingStartDate'].'</prodCntrctItem0001:billingStartDate>
							</prodCntrctItem0001:characteristicValue>
						</prodCntrctItem0001:billingStartDate>
						<prodCntrctItem0001:billingEndDate>
							<prodCntrctItem0001:characteristicValue>
								<cbe-v0:identifiedBy>
									<cbe-v0:charcValueKey>
										<cbe-v0:type>CharacteristicValue</cbe-v0:type>
										<cbe-v0:objectID>f2be561e-0b23-4fbb-b182-e87258b5297e</cbe-v0:objectID>
									</cbe-v0:charcValueKey>
								</cbe-v0:identifiedBy>
								<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
								<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
								<cbe-v0:chValDescByChSpec>
									<cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
									<cbe-v0:objectID>d370e876-0b23-4fb1-934f-fb6adc50ba74</cbe-v0:objectID>
								</cbe-v0:chValDescByChSpec>
								<prodCntrctItem0001:billingEndDate>'.$arrParameters['tmp']['billingEndDate'].'</prodCntrctItem0001:billingEndDate>
							</prodCntrctItem0001:characteristicValue>
						</prodCntrctItem0001:billingEndDate>
					</prodCntrctItem0001:billingPeriod>
					<prodCntrctItem0001:servicePeriod>
						<prodCntrctItem0001:serviceStartDate>
							<prodCntrctItem0001:characteristicValue>
								<cbe-v0:identifiedBy>
									<cbe-v0:charcValueKey>
										<cbe-v0:type>CharacteristicValue</cbe-v0:type>
										<cbe-v0:objectID>26ddca8f-0b23-4fb9-ba3d-a6cea1043a48</cbe-v0:objectID>
									</cbe-v0:charcValueKey>
								</cbe-v0:identifiedBy>
								<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
								<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
								<cbe-v0:chValDescByChSpec>
									<cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
									<cbe-v0:objectID>452776f6-0b23-4fbc-a415-2264e1b90678</cbe-v0:objectID>
								</cbe-v0:chValDescByChSpec>
								<prodCntrctItem0001:serviceStartDate>'.$arrParameters['tmp']['serviceStartDate'].'</prodCntrctItem0001:serviceStartDate>
							</prodCntrctItem0001:characteristicValue>
						</prodCntrctItem0001:serviceStartDate>
						<prodCntrctItem0001:serviceEndDate>
							<prodCntrctItem0001:characteristicValue>
								<cbe-v0:identifiedBy>
									<cbe-v0:charcValueKey>
										<cbe-v0:type>CharacteristicValue</cbe-v0:type>
										<cbe-v0:objectID>da063062-0b23-4fb8-b08e-d9dd0bd424bd</cbe-v0:objectID>
									</cbe-v0:charcValueKey>
								</cbe-v0:identifiedBy>
								<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
								<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
								<cbe-v0:chValDescByChSpec>
									<cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
									<cbe-v0:objectID>d1c21d42-0b23-4fb4-a835-674ed6e5184b</cbe-v0:objectID>
								</cbe-v0:chValDescByChSpec>
								<prodCntrctItem0001:serviceEndDate>'.$arrParameters['tmp']['serviceEndDate'].'</prodCntrctItem0001:serviceEndDate>
							</prodCntrctItem0001:characteristicValue>
						</prodCntrctItem0001:serviceEndDate>
					</prodCntrctItem0001:servicePeriod>
				</prodCntrctItem0001:productContractItem0001>
				<bi-v0:action>no change</bi-v0:action>
				<bi-v0:quantity>
					<datatypes-v0:amount>1</datatypes-v0:amount>
					<datatypes-v0:units>each</datatypes-v0:units>
				</bi-v0:quantity>
				<bi-v0:involvesProduct>
					<product-v0:atomicProductKey>
						<cbe-v0:type>AtomicProduct</cbe-v0:type>
						<cbe-v0:objectID>09b28f78-0b23-4fb8-bf72-df7be5f2602f</cbe-v0:objectID>
					</product-v0:atomicProductKey>
				</bi-v0:involvesProduct>
			</bi-v0:productItem>
		</bi-v0:businessInteractionItems>
		<bi-v0:contractID>'.$arrParameters['tmp']['contractID'].'</bi-v0:contractID>		

		<bi-v0:contractFor>
			<product-v0:bundledProductKey>
				<cbe-v0:type>BundledProduct</cbe-v0:type>
				<cbe-v0:objectID>410b7100-0b23-4fbd-9d9e-763ce233c0a1</cbe-v0:objectID>
			</product-v0:bundledProductKey>
		</bi-v0:contractFor>
	</bi-v0:productContract>
	
	<bi-v0:partyInteractionRole>
		<cbe-v0:identifiedBy>
			<bi-v0:partyInteractionRoleKey>
				<cbe-v0:type>PartyInteractionRole</cbe-v0:type>
				<cbe-v0:objectID>53b963b7-0b23-4fbd-b701-950b4c19e438</cbe-v0:objectID>
			</bi-v0:partyInteractionRoleKey>
		</cbe-v0:identifiedBy>		
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:describedBy>
			<bi-v0:referenceToPartyInteractionRoleSpec>
				<cbe-v0:type>PartyInteractionRoleSpecification</cbe-v0:type>
				<cbe-v0:objectID>57683710-0b23-4fb2-8abb-dd4befeceef9</cbe-v0:objectID>
			</bi-v0:referenceToPartyInteractionRoleSpec>
		</cbe-v0:describedBy>
		<party-v0:partyRoleKey>
			<cbe-v0:type>PartyRole</cbe-v0:type>
			<cbe-v0:objectID>29456d12-0b23-4fbf-8f7d-e65b6219678c</cbe-v0:objectID>
		</party-v0:partyRoleKey>
	</bi-v0:partyInteractionRole>
	
	<bi-v0:partyInteractionRole>
		<cbe-v0:identifiedBy>
			<bi-v0:partyInteractionRoleKey>
				<cbe-v0:type>PartyInteractionRole</cbe-v0:type>
				<cbe-v0:objectID>75317ab8-0b23-4fba-9880-34400d2101f8</cbe-v0:objectID>
			</bi-v0:partyInteractionRoleKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>		
		<cbe-v0:describedBy>
			<bi-v0:referenceToPartyInteractionRoleSpec>
				<cbe-v0:type>PartyInteractionRoleSpecification</cbe-v0:type>
				<cbe-v0:objectID>666b54c8-0b23-4fb8-98b9-3000578fd3fc</cbe-v0:objectID>
			</bi-v0:referenceToPartyInteractionRoleSpec>
		</cbe-v0:describedBy>
		<party-v0:partyRoleKey>
			<cbe-v0:type>PartyRole</cbe-v0:type>
			<cbe-v0:objectID>61ec3031-0b23-4fbf-9dc1-5b8024fa6001</cbe-v0:objectID>
		</party-v0:partyRoleKey>
	</bi-v0:partyInteractionRole>
	
	<bi-v0:partyInteractionRole>
		<cbe-v0:identifiedBy>
			<bi-v0:partyInteractionRoleKey>
				<cbe-v0:type>PartyInteractionRole</cbe-v0:type>
				<cbe-v0:objectID>30408e5e-0b23-4fbf-a2f1-fc012d312248</cbe-v0:objectID>
			</bi-v0:partyInteractionRoleKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:describedBy>
			<bi-v0:referenceToPartyInteractionRoleSpec>
				<cbe-v0:type>PartyInteractionRoleSpecification</cbe-v0:type>
				<cbe-v0:objectID>f9101833-0b23-4fbc-bfcf-1d8b62aeb257</cbe-v0:objectID>
			</bi-v0:referenceToPartyInteractionRoleSpec>
		</cbe-v0:describedBy>
		<party-v0:partyRoleKey>
			<cbe-v0:type>PartyRole</cbe-v0:type>
			<cbe-v0:objectID>f7f9e523-0b23-4fbb-8e92-cb69e143a4ea</cbe-v0:objectID>
		</party-v0:partyRoleKey>
	</bi-v0:partyInteractionRole>
	
	<bi-v0:partyInteractionRole>
		<cbe-v0:identifiedBy>
			<bi-v0:partyInteractionRoleKey>
				<cbe-v0:type>PartyInteractionRole</cbe-v0:type>
				<cbe-v0:objectID>b7511051-0b23-4fb9-8915-3970473d31da</cbe-v0:objectID>
			</bi-v0:partyInteractionRoleKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:describedBy>
			<bi-v0:referenceToPartyInteractionRoleSpec>
				<cbe-v0:type>PartyInteractionRoleSpecification</cbe-v0:type>
				<cbe-v0:objectID>9aadb297-0b23-4fb9-9871-1f7a07105e35</cbe-v0:objectID>
			</bi-v0:referenceToPartyInteractionRoleSpec>
		</cbe-v0:describedBy>
		<party-v0:partyRoleKey>
			<cbe-v0:type>PartyRole</cbe-v0:type>
			<cbe-v0:objectID>2416efec-0b23-4fb9-80d6-42d67cd8903f</cbe-v0:objectID>
		</party-v0:partyRoleKey>
	</bi-v0:partyInteractionRole>
	
	<bi-v0:partyInteractionRole>
		<cbe-v0:identifiedBy>
			<bi-v0:partyInteractionRoleKey>
				<cbe-v0:type>PartyInteractionRole</cbe-v0:type>
				<cbe-v0:objectID>a664c0a6-0b23-4fb7-84b5-cde5ca8a8f31</cbe-v0:objectID>
			</bi-v0:partyInteractionRoleKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:describedBy>
			<bi-v0:referenceToPartyInteractionRoleSpec>
				<cbe-v0:type>PartyInteractionRoleSpecification</cbe-v0:type>
				<cbe-v0:objectID>e01ac48d-0b23-4fb1-8935-f73d7c7f8738</cbe-v0:objectID>
			</bi-v0:referenceToPartyInteractionRoleSpec>
		</cbe-v0:describedBy>
		<party-v0:partyRoleKey>
			<cbe-v0:type>PartyRole</cbe-v0:type>
			<cbe-v0:objectID>82cafe7d-0b23-4fb1-8e6c-a48b5598d10d</cbe-v0:objectID>
		</party-v0:partyRoleKey>
	</bi-v0:partyInteractionRole>
	
	<bi-v0:partyInteractionRole>
		<cbe-v0:identifiedBy>
			<bi-v0:partyInteractionRoleKey>
				<cbe-v0:type>PartyInteractionRole</cbe-v0:type>
				<cbe-v0:objectID>e757d58e-0b23-4fb5-91e0-194a49cdf683</cbe-v0:objectID>
			</bi-v0:partyInteractionRoleKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:describedBy>
			<bi-v0:referenceToPartyInteractionRoleSpec>
				<cbe-v0:type>PartyInteractionRoleSpecification</cbe-v0:type>
				<cbe-v0:objectID>155b95c9-0b23-4fba-92d8-c4a1497867df</cbe-v0:objectID>
			</bi-v0:referenceToPartyInteractionRoleSpec>
		</cbe-v0:describedBy>
		<party-v0:partyRoleKey>
			<cbe-v0:type>PartyRole</cbe-v0:type>
			<cbe-v0:objectID>477d1f27-0b23-4fb3-bd93-3eeaa400a043</cbe-v0:objectID>
		</party-v0:partyRoleKey>
	</bi-v0:partyInteractionRole>';

	unset($arrParameters);

	return $returnXML;
}


function xmlPartBlock($arrParameters = array()) 
{
	$returnXML = '';
	
	if(!empty($arrParameters['tmp']['companyName']) || !empty($arrParameters['tmp']['companyName']) || !empty($arrParameters['tmp']['companyName'])) 
	{ 

	$returnXML = '
	<party-v0:party>
		<cbe-v0:identifiedBy>
			<party-v0:partyKey>
				<cbe-v0:type>Party</cbe-v0:type>
				<cbe-v0:objectID>43d69f6e-0b23-4fb4-a68c-a57d37646832</cbe-v0:objectID>
			</party-v0:partyKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>		
		<cbe-v0:describedBy>
			<party-v0:referenceToPartySpec name="Organization">
				<cbe-v0:type>PartySpecification</cbe-v0:type>
				<cbe-v0:objectID>42137cd1-0b23-4fb7-9342-474dd5ed0a07</cbe-v0:objectID>
			</party-v0:referenceToPartySpec>
		</cbe-v0:describedBy>
		<party-v0:namedUsing>
			<party-v0:partyName>				
				<cbe-v0:describedBy>
					<party-v0:referenceToPartyNameSpec name="CIDAS Company Name">
						<cbe-v0:type>PartyNameSpecification</cbe-v0:type>
						<cbe-v0:objectID>d915e331-0b23-4fb3-9840-a32263593fbb</cbe-v0:objectID>
					</party-v0:referenceToPartyNameSpec>
				</cbe-v0:describedBy>
					<cidasCmpnyNm:cIDASCompanyName name="CIDAS Company Name">';

						if(!empty($arrParameters['tmp']['companyName'])) 
						{
							$returnXML .= '
							<cidasCmpnyNm:companyName>
								<cidasCmpnyNm:characteristicValue>
									<cbe-v0:chValDescByChSpec>
										<cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
										<cbe-v0:objectID>c38613f4-0b23-4fb9-877c-3d928736f87a</cbe-v0:objectID>
									</cbe-v0:chValDescByChSpec>
									<cidasCmpnyNm:companyName>'.$arrParameters['tmp']['companyName'].'</cidasCmpnyNm:companyName>
								</cidasCmpnyNm:characteristicValue>
							</cidasCmpnyNm:companyName>';
						}
						
						if(!empty($arrParameters['tmp']['companyNameEng'])) 
						{
							$returnXML .= '
							<cidasCmpnyNm:companyNameEng>
								<cidasCmpnyNm:characteristicValue>
									<cbe-v0:chValDescByChSpec>
										<cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
										<cbe-v0:objectID>22163f8b-0b23-4fbd-abfe-8f5c6e127c01</cbe-v0:objectID>
									</cbe-v0:chValDescByChSpec>
									<cidasCmpnyNm:companyNameEng>'.$arrParameters['tmp']['companyNameEng'].'</cidasCmpnyNm:companyNameEng>
								</cidasCmpnyNm:characteristicValue>
							</cidasCmpnyNm:companyNameEng>';
						}
						
						if(!empty($arrParameters['tmp']['companyNameKana'])) 
						{
							$returnXML .= '
							<cidasCmpnyNm:companyNameKana>
								<cidasCmpnyNm:characteristicValue>
									<cbe-v0:chValDescByChSpec>
										<cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
										<cbe-v0:objectID>89f7fc09-0b23-4fb4-8c2d-9fcd074ba10e</cbe-v0:objectID>
									</cbe-v0:chValDescByChSpec>
									<cidasCmpnyNm:companyNameKana>'.$arrParameters['tmp']['companyNameKana'].'</cidasCmpnyNm:companyNameKana>
								</cidasCmpnyNm:characteristicValue>
							</cidasCmpnyNm:companyNameKana>';
						}
						
					$returnXML .= '
				</cidasCmpnyNm:cIDASCompanyName>
			</party-v0:partyName>
		</party-v0:namedUsing>
	</party-v0:party>';
	}
	
	if(!empty($arrParameters['tmp']['corporationID'])) 
	{

	$returnXML .= '
	<party-v0:partyIdentification>
		<cbe-v0:identifiedBy>
			<party-v0:partyIdentificationKey>
				<cbe-v0:type>PartyIdentification</cbe-v0:type>
				<cbe-v0:objectID>47d4584b-0b23-4fbc-925e-bc355d0249cc</cbe-v0:objectID>
			</party-v0:partyIdentificationKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>契約者共通顧客ID</cbe-v0:name>
		<cbe-v0:nameLatin>Corporation ID</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<party-v0:referenceToPartyIdentificationSpec>
				<cbe-v0:type>PartyIdentificationSpecification</cbe-v0:type>
				<cbe-v0:objectID>73a6fc18-0b23-4fb8-9861-1ab890408fa8</cbe-v0:objectID>
			</party-v0:referenceToPartyIdentificationSpec>
		</cbe-v0:describedBy>
		<ptyIdent001:corporateIdentification name="Corporate Identification">
			<ptyIdent001:corporationID>
				<ptyIdent001:characteristicValue>
					<cbe-v0:identifiedBy>
						<cbe-v0:charcValueKey>
							<cbe-v0:type>CharacteristicValue</cbe-v0:type>
							<cbe-v0:objectID>4b13a071-0b23-4fbf-9112-45d08776a080</cbe-v0:objectID>
						</cbe-v0:charcValueKey>
					</cbe-v0:identifiedBy>
					<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
					<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
					<cbe-v0:chValDescByChSpec>
						<cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
						<cbe-v0:objectID>9512b60a-0b23-4fbe-9655-fdf88b53673d</cbe-v0:objectID>
					</cbe-v0:chValDescByChSpec>
					<ptyIdent001:corporationID>'.$arrParameters['tmp']['corporationID'].'</ptyIdent001:corporationID>
				</ptyIdent001:characteristicValue>
			</ptyIdent001:corporationID>
		</ptyIdent001:corporateIdentification>
		<cbe-v0:appliedTo>
			<party-v0:partyKey>
				<cbe-v0:type>Party</cbe-v0:type>
				<cbe-v0:objectID>43d69f6e-0b23-4fb4-a68c-a57d37646832</cbe-v0:objectID>
			</party-v0:partyKey>
		</cbe-v0:appliedTo>
	</party-v0:partyIdentification>';

	}
    
    if(!empty($arrParameters['tmp']['vatNo'])) 
	{

	$returnXML .= '
	<party-v0:partyIdentification>
		<cbe-v0:identifiedBy>
			<party-v0:partyIdentificationKey>
				<cbe-v0:type>CorporateIdentification</cbe-v0:type>
				<cbe-v0:objectID>656e526c-0b23-4fb4-88fe-7658a219fde7</cbe-v0:objectID>
			</party-v0:partyIdentificationKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>契約者VAT番号</cbe-v0:name>
		<cbe-v0:nameLatin>VAT No in XXX</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<party-v0:referenceToPartyIdentificationSpec>
				<cbe-v0:type>PartyIdentificationSpecification</cbe-v0:type>
				<cbe-v0:objectID>56e1cfb8-0b23-4fbd-a87f-ccd2ce0d1a60</cbe-v0:objectID>
			</party-v0:referenceToPartyIdentificationSpec>
		</cbe-v0:describedBy>
		<ptyIdent003:vatNoIdentification  name="Corporate Identification by VAT No">
			<ptyIdent003:vatNo>
				<ptyIdent003:characteristicValue>
					<cbe-v0:identifiedBy>
						<cbe-v0:charcValueKey>
							<cbe-v0:type>CharacteristicValue</cbe-v0:type>
							<cbe-v0:objectID>6834d711-0b23-4fb2-a668-2bbb76f321ac</cbe-v0:objectID>
						</cbe-v0:charcValueKey>
					</cbe-v0:identifiedBy>
					<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
					<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
					<cbe-v0:chValDescByChSpec>
						<cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
						<cbe-v0:objectID>40a30963-0b23-4fb7-a484-a37794675885</cbe-v0:objectID>
					</cbe-v0:chValDescByChSpec>
					<ptyIdent003:vatNo>'.$arrParameters['tmp']['vatNo'].'</ptyIdent003:vatNo>
				</ptyIdent003:characteristicValue>
			</ptyIdent003:vatNo>
		</ptyIdent003:vatNoIdentification>
		<cbe-v0:appliedTo>
			<party-v0:partyKey>
				<cbe-v0:type>Party</cbe-v0:type>
				<cbe-v0:objectID>43d69f6e-0b23-4fb4-a68c-a57d37646832</cbe-v0:objectID>
			</party-v0:partyKey>
		</cbe-v0:appliedTo>
	</party-v0:partyIdentification>';

	}
    
	unset($arrParameters);
	
	return $returnXML;
}


function xmlContractPartBlock($arrParameters = array()) 
{
	
	$arrParms = array();

	$returnXML =  '
	<party-v0:partyRole>
		<cbe-v0:identifiedBy>
			<party-v0:partyRoleKey>
				<cbe-v0:type>PartyRole</cbe-v0:type>
				<cbe-v0:objectID>29456d12-0b23-4fbf-8f7d-e65b6219678c</cbe-v0:objectID>
			</party-v0:partyRoleKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>契約先</cbe-v0:name>
		<cbe-v0:nameLatin>Contract Customer</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<party-v0:referenceToPartyRoleSpec name="Contract Customer">
				<cbe-v0:type>PartyRoleSpecification</cbe-v0:type>
				<cbe-v0:objectID>be8658e3-0b23-4fb8-9c35-b7446a8af342</cbe-v0:objectID>
			</party-v0:referenceToPartyRoleSpec>
		</cbe-v0:describedBy>
		<cbe-v0:appliedTo>
			<party-v0:partyKey>
				<cbe-v0:type>Party</cbe-v0:type>
				<cbe-v0:objectID>43d69f6e-0b23-4fb4-a68c-a57d37646832</cbe-v0:objectID>
			</party-v0:partyKey>
		</cbe-v0:appliedTo>
	</party-v0:partyRole>
	
	<party-v0:partyRole>
		<cbe-v0:identifiedBy>
			<party-v0:partyRoleKey>
				<cbe-v0:type>PartyRole</cbe-v0:type>
				<cbe-v0:objectID>61ec3031-0b23-4fbf-9dc1-5b8024fa6001</cbe-v0:objectID>
			</party-v0:partyRoleKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>契約先担当者</cbe-v0:name>
		<cbe-v0:nameLatin>Person in charge of contract</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<party-v0:referenceToPartyRoleSpec name="Person In Charge Of Contract Pseudo Role">
				<cbe-v0:type>PartyRoleSpecification</cbe-v0:type>
				<cbe-v0:objectID>7da2fb33-0b23-4fb5-ab43-8d5d3db7c965</cbe-v0:objectID>
			</party-v0:referenceToPartyRoleSpec>
		</cbe-v0:describedBy>
		<pICContractPR:pICContractPR name="Person In Charge Of Contract Pseudo Role">';
		
		if(!empty($arrParameters['tmp']['divisionName']) || !empty($arrParameters['tmp']['divisionNameLatin']) || !empty($arrParameters['tmp']['divisionNameFurigana'])) 
		{			
			$returnXML .= '
			<pICContractPR:divisionName>';
			
			if(!empty($arrParameters['tmp']['divisionName'])) 
			{
				$arrParms['prenspace'] = 'pICContractPR';
				$arrParms['nspace'] = 'divisionName';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '47e926d0-0b23-4fbb-a094-0bbe9f129454';
				$arrParms['aobj'] = '09838a91-0b23-4fb5-a770-310deaff0330';
				$arrParms['val'] = $arrParameters['tmp']['divisionName'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}
			
			if(!empty($arrParameters['tmp']['divisionNameLatin'])) 
			{
				$arrParms['prenspace'] = 'pICContractPR';
				$arrParms['nspace'] = 'divisionNameLatin';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '5230c2c5-0b23-4fb6-bb7a-dde626eebbdd';
				$arrParms['aobj'] = '4aefe1bf-0b23-4fb0-b7c0-1f9a8bfb6b86';
				$arrParms['val'] = $arrParameters['tmp']['divisionNameLatin'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}
			
			if(!empty($arrParameters['tmp']['divisionNameFurigana'])) 
			{
				$arrParms['prenspace'] = 'pICContractPR';
				$arrParms['nspace'] = 'divisionNameFurigana';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '64c4fd3b-0b23-4fb2-9c9e-0cde9a4617fa';
				$arrParms['aobj'] = '71fbe1c0-0b23-4fb7-985d-56aa9cf015cb';
				$arrParms['val'] = $arrParameters['tmp']['divisionNameFurigana'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}

			$returnXML .= '
			</pICContractPR:divisionName>';
		}

		if(!empty($arrParameters['tmp']['individualName']) || !empty($arrParameters['tmp']['individualNameLatin']) || !empty($arrParameters['tmp']['individualNameFurigana'])) 
		{
			$returnXML .= '
			<pICContractPR:individualName>';

			if(!empty($arrParameters['tmp']['individualName'])) 
			{
				$arrParms['prenspace'] = 'pICContractPR';
				$arrParms['nspace'] = 'individualName';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '05593ed9-0b23-4fb0-98d6-68680931c5e0';
				$arrParms['aobj'] = '99a26515-0b23-4fbd-b0fd-f821a3f912db';
				$arrParms['val'] = $arrParameters['tmp']['individualName'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}

			if(!empty($arrParameters['tmp']['individualNameLatin'])) 
			{
				$arrParms['prenspace'] = 'pICContractPR';
				$arrParms['nspace'] = 'individualNameLatin';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = 'cfb7f22a-0b23-4fb9-aa8f-56e74342da22';
				$arrParms['aobj'] = 'ebe7d00a-0b23-4fbc-adb2-635dd34c366d';
				$arrParms['val'] = $arrParameters['tmp']['individualNameLatin'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);
			}

			if(!empty($arrParameters['tmp']['individualNameFurigana'])) 
			{
				$arrParms['prenspace'] = 'pICContractPR';
				$arrParms['nspace'] = 'individualNameFurigana';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = 'b40fd010-0b23-4fb0-a414-a1f8d2648105';
				$arrParms['aobj'] = '7eb770c0-0b23-4fb4-aafe-088a9438b27b';
				$arrParms['val'] = $arrParameters['tmp']['individualNameFurigana'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);
			}

			$returnXML .= '
			</pICContractPR:individualName>';
		}

		$returnXML .= '
		</pICContractPR:pICContractPR>
		<party-v0:contactableVia>
			<party-v0:contactMediumKey>
				<cbe-v0:type>ContactMedium</cbe-v0:type>
				<cbe-v0:objectID>effd0ebe-0b23-4fb9-9152-42644cfb18c9</cbe-v0:objectID>
			</party-v0:contactMediumKey>
		</party-v0:contactableVia>
	</party-v0:partyRole>';
	
	unset($arrParameters,$arrParms);

	return $returnXML;
}


function xmlBillingPartBlock($arrParameters = array()) 
{

	$arrParms = array();
	$returnXML = '';

	if(!empty($arrParameters['tmp']['corporationName']) || !empty($arrParameters['tmp']['corporationNameLatin']) || !empty($arrParameters['tmp']['corporationNameFurigana'])) 
	{

	$returnXML = '	
	<party-v0:partyRole>
		<cbe-v0:identifiedBy>
			<party-v0:partyRoleKey>
				<cbe-v0:type>PartyRole</cbe-v0:type>
				<cbe-v0:objectID>f7f9e523-0b23-4fbb-8e92-cb69e143a4ea</cbe-v0:objectID>
			</party-v0:partyRoleKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>請求先</cbe-v0:name>
		<cbe-v0:nameLatin>Billing Customer</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<party-v0:referenceToPartyRoleSpec name="Billing Customer Pseudo Role">
				<cbe-v0:type>PartyRoleSpecification</cbe-v0:type>
				<cbe-v0:objectID>f1d52414-0b23-4fbc-adab-30fd0bfda38e</cbe-v0:objectID>
			</party-v0:referenceToPartyRoleSpec>
		</cbe-v0:describedBy>
			<billingCustomerPR:billingCustomerPseudoRole name="Billing Customer Pseudo Role">';

			if(!empty($arrParameters['tmp']['corporationName'])) {

				$arrParms['prenspace'] = 'billingCustomerPR';
				$arrParms['nspace'] = 'corporationName';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '4dc8cb11-0b23-4fb3-a691-a994d1c85306';
				$arrParms['aobj'] = '42940831-0b23-4fba-bf9e-0eb01de498f1';
				$arrParms['val'] = $arrParameters['tmp']['corporationName'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}

			if(!empty($arrParameters['tmp']['corporationNameLatin'])) 
			{
				$arrParms['prenspace'] = 'billingCustomerPR';
				$arrParms['nspace'] = 'corporationNameLatin';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '1c93d00a-0b23-4fbe-bd97-2870c75aa467';
				$arrParms['aobj'] = '31b4ecf5-0b23-4fb3-a856-fa77a8f48595';
				$arrParms['val'] = $arrParameters['tmp']['corporationNameLatin'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}

			if(!empty($arrParameters['tmp']['corporationNameFurigana'])) 
			{
				$arrParms['prenspace'] = 'billingCustomerPR';
				$arrParms['nspace'] = 'corporationNameFurigana';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '441e993a-0b23-4fb0-b795-0d4fafb868c3';
				$arrParms['aobj'] = '5b19f1e7-0b23-4fb4-bf57-67b02517f029';
				$arrParms['val'] = $arrParameters['tmp']['corporationNameFurigana'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}
			
		$returnXML .= '
			</billingCustomerPR:billingCustomerPseudoRole>
		</party-v0:partyRole>';	
	}

	unset($arrParameters['tmp']['corporationName'],$arrParameters['tmp']['corporationNameLatin'],$arrParameters['tmp']['corporationNameFurigana']);

	if(checkBlockFlag($arrParameters) == 1) { 
		
		$returnXML .= '
		<party-v0:partyRole>
			<cbe-v0:identifiedBy>
				<party-v0:partyRoleKey>
					<cbe-v0:type>PartyRole</cbe-v0:type>
					<cbe-v0:objectID>2416efec-0b23-4fb9-80d6-42d67cd8903f</cbe-v0:objectID>
				</party-v0:partyRoleKey>
			</cbe-v0:identifiedBy>
			<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
			<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
			<cbe-v0:name>請求先担当者</cbe-v0:name>
			<cbe-v0:nameLatin>Person in charge of billing</cbe-v0:nameLatin>
			<cbe-v0:describedBy>
				<party-v0:referenceToPartyRoleSpec name="Person In Charge Of Billing Pseudo Role">
					<cbe-v0:type>PartyRoleSpecification</cbe-v0:type>
					<cbe-v0:objectID>7a10a146-0b23-4fb7-a56b-92830fc8d6d1</cbe-v0:objectID>
				</party-v0:referenceToPartyRoleSpec>
			</cbe-v0:describedBy>
			<pICBillingPR:pICBillingPR name="Person In Charge Of Billing Pseudo Role">';

			if(!empty($arrParameters['tmp']['divisionName']) || !empty($arrParameters['tmp']['divisionNameLatin']) || !empty($arrParameters['tmp']['divisionNameFurigana'])) 
			{
				$returnXML .= '
				<pICBillingPR:divisionName>';

				if(!empty($arrParameters['tmp']['divisionName'])) {

					$arrParms['prenspace'] = 'pICBillingPR';
					$arrParms['nspace'] = 'divisionName';	
					$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
					$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
					$arrParms['cobj'] = '27a49571-0b23-4fb4-a2de-3ceb449f8eef';
					$arrParms['aobj'] = 'a011af96-0b23-4fb4-aa4c-6d49630595bd';
					$arrParms['val'] = $arrParameters['tmp']['divisionName'];

					$returnXML .= glbXMLDataBlock($arrParms);

					unset($arrParms);
				}

				if(!empty($arrParameters['tmp']['divisionNameLatin'])) 
				{
					$arrParms['prenspace'] = 'pICBillingPR';
					$arrParms['nspace'] = 'divisionNameLatin';	
					$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
					$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
					$arrParms['cobj'] = '3af86669-0b23-4fbe-9970-223d420ee624';
					$arrParms['aobj'] = 'd8d8c3e5-0b23-4fb5-843c-68dedabe6dc4';
					$arrParms['val'] = $arrParameters['tmp']['divisionNameLatin'];

					$returnXML .= glbXMLDataBlock($arrParms);

					unset($arrParms);				
				}

				if(!empty($arrParameters['tmp']['divisionNameFurigana'])) 
				{
					$arrParms['prenspace'] = 'pICBillingPR';
					$arrParms['nspace'] = 'divisionNameFurigana';	
					$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
					$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
					$arrParms['cobj'] = '255c5b8f-0b23-4fbf-9fe5-67b4e79ad39d';
					$arrParms['aobj'] = '96422be6-0b23-4fba-89ff-4aa1fe1d7a2f';
					$arrParms['val'] = $arrParameters['tmp']['divisionNameFurigana'];

					$returnXML .= glbXMLDataBlock($arrParms);

					unset($arrParms);				
				}

				$returnXML .= '
				</pICBillingPR:divisionName>';

			}

			if(!empty($arrParameters['tmp']['individualName']) || !empty($arrParameters['tmp']['individualNameLatin']) || !empty($arrParameters['tmp']['individualNameFurigana'])) 
			{
				$returnXML .= '
				<pICBillingPR:individualName>';

				if(!empty($arrParameters['tmp']['individualName'])) 
				{
					$arrParms['prenspace'] = 'pICBillingPR';
					$arrParms['nspace'] = 'individualName';	
					$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
					$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
					$arrParms['cobj'] = '5dab5bbc-0b23-4fba-a492-a1b4226cadb0';
					$arrParms['aobj'] = 'e45be530-0b23-4fb6-886f-b4d6fc186596';
					$arrParms['val'] = $arrParameters['tmp']['individualName'];

					$returnXML .= glbXMLDataBlock($arrParms);

					unset($arrParms);				
				}

				if(!empty($arrParameters['tmp']['individualNameLatin'])) 
				{
					$arrParms['prenspace'] = 'pICBillingPR';
					$arrParms['nspace'] = 'individualNameLatin';	
					$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
					$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
					$arrParms['cobj'] = '84c0db7d-0b23-4fbf-8c92-8eb8e9683161';
					$arrParms['aobj'] = 'a51f42d2-0b23-4fb2-a90b-0244412604c8';
					$arrParms['val'] = $arrParameters['tmp']['individualNameLatin'];

					$returnXML .= glbXMLDataBlock($arrParms);

					unset($arrParms);				
				}

				if(!empty($arrParameters['tmp']['individualNameFurigana'])) 
				{
					$arrParms['prenspace'] = 'pICBillingPR';
					$arrParms['nspace'] = 'individualNameFurigana';	
					$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
					$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
					$arrParms['cobj'] = '2341c24d-0b23-4fb7-a415-c33f4525ed62';
					$arrParms['aobj'] = '227272b4-0b23-4fb1-b3b8-f6b47abd2c81';
					$arrParms['val'] = $arrParameters['tmp']['individualNameFurigana'];

					$returnXML .= glbXMLDataBlock($arrParms);

					unset($arrParms);
				}
				
				$returnXML .= '
				</pICBillingPR:individualName>';
			}

			$returnXML .= '
			</pICBillingPR:pICBillingPR>
			<party-v0:contactableVia>
				<party-v0:contactMediumKey>
					<cbe-v0:type>ContactMedium</cbe-v0:type>
					<cbe-v0:objectID>26af4dc8-0b23-4fbc-b28e-e1fc188bc8c8</cbe-v0:objectID>
				</party-v0:contactMediumKey>
			</party-v0:contactableVia>
		</party-v0:partyRole>';
	}

	unset($arrParameters,$arrParms);

	return $returnXML;

}


function xmlSalesRepPartBlock($arrParameters = array()) 
{
	
	$arrParms = array();
	$returnXML = '';
	
	if(!empty($arrParameters['tmp']['salesChannelCode']) || !empty($arrParameters['tmp']['corporationName']) || !empty($arrParameters['tmp']['corporationNameLatin']) || !empty($arrParameters['tmp']['corporationNameFurigana'])) 
	{

	$returnXML =  '
	<party-v0:partyRole>
		<cbe-v0:identifiedBy>
			<party-v0:partyRoleKey>
				<cbe-v0:type>PartyRole</cbe-v0:type>
				<cbe-v0:objectID>82cafe7d-0b23-4fb1-8e6c-a48b5598d10d</cbe-v0:objectID>
			</party-v0:partyRoleKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>販売チャネル</cbe-v0:name>
		<cbe-v0:nameLatin>Sales Channel</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<party-v0:referenceToPartyRoleSpec name="Sales Channel Pseudo Role">
				<cbe-v0:type>PartyRoleSpecification</cbe-v0:type>
				<cbe-v0:objectID>1d56df26-0b23-4fb4-9514-42ca68dd6586</cbe-v0:objectID>
			</party-v0:referenceToPartyRoleSpec>
		</cbe-v0:describedBy>
		<salesChannelPR:salesChannelPseudoRole name="Sales Channel Pseudo Role">';
				
			if(!empty($arrParameters['tmp']['salesChannelCode'])) 
			{
				$arrParms['prenspace'] = 'salesChannelPR';
				$arrParms['nspace'] = 'salesChannelCode';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = 'e4b03192-0b23-4fbc-aaf2-8f712eaf8504';
				$arrParms['aobj'] = '00d1f3d3-0b23-4fbd-bbd7-753f5a6c5c6a';
				$arrParms['val'] = $arrParameters['tmp']['salesChannelCode'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}

			if(!empty($arrParameters['tmp']['corporationName'])) 
			{
				$arrParms['prenspace'] = 'salesChannelPR';
				$arrParms['nspace'] = 'corporationName';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '35fd0ebc-0b23-4fba-8317-ded8bcc9e6f0';
				$arrParms['aobj'] = '9afa06b0-0b23-4fb7-9906-14142e7f0406';
				$arrParms['val'] = $arrParameters['tmp']['corporationName'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}

			if(!empty($arrParameters['tmp']['corporationNameLatin'])) 
			{
				$arrParms['prenspace'] = 'salesChannelPR';
				$arrParms['nspace'] = 'corporationNameLatin';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = 'ff3bde68-0b23-4fbf-86dd-8bdc85480691';
				$arrParms['aobj'] = 'ff3bde68-0b23-4fbf-86dd-8bdc85480691';
				$arrParms['val'] = $arrParameters['tmp']['corporationNameLatin'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}

			if(!empty($arrParameters['tmp']['corporationNameFurigana'])) 
			{
				$arrParms['prenspace'] = 'salesChannelPR';
				$arrParms['nspace'] = 'corporationNameFurigana';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = 'ed136446-0b23-4fb4-baa4-d08501ae4cfb';
				$arrParms['aobj'] = 'ed136446-0b23-4fb4-baa4-d08501ae4cfb';
				$arrParms['val'] = $arrParameters['tmp']['corporationNameFurigana'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}

		$returnXML .= '
		</salesChannelPR:salesChannelPseudoRole>
	</party-v0:partyRole>';

	}

	unset($arrParameters['tmp']['salesChannelCode'],$arrParameters['tmp']['corporationName'],$arrParameters['tmp']['corporationNameLatin'],$arrParameters['tmp']['corporationNameFurigana']);

	if(checkBlockFlag($arrParameters) == 1) { 

		$returnXML .= '
		<party-v0:partyRole>
			<cbe-v0:identifiedBy>
				<party-v0:partyRoleKey>
					<cbe-v0:type>PartyRole</cbe-v0:type>
					<cbe-v0:objectID>477d1f27-0b23-4fb3-bd93-3eeaa400a043</cbe-v0:objectID>
				</party-v0:partyRoleKey>
			</cbe-v0:identifiedBy>
			<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
			<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
			<cbe-v0:name>販売担当者</cbe-v0:name>
			<cbe-v0:nameLatin>Person in charge of sales</cbe-v0:nameLatin>
			<cbe-v0:describedBy>
				<party-v0:referenceToPartyRoleSpec name="Person In Charge Of Sales Pseudo Role">
					<cbe-v0:type>PartyRoleSpecification</cbe-v0:type>
					<cbe-v0:objectID>269648a0-0b23-4fb7-9c91-368937fe81ac</cbe-v0:objectID>
				</party-v0:referenceToPartyRoleSpec>
			</cbe-v0:describedBy>
			<pICSalesPR:pICSalesPR name="Person In Charge Of Sales Representative Pseudo Role">';

			if(!empty($arrParameters['tmp']['divisionName']) || !empty($arrParameters['tmp']['divisionNameLatin']) || !empty($arrParameters['tmp']['divisionNameFurigana'])) 
			{
				$returnXML .= '
				<pICSalesPR:divisionName>';

				if(!empty($arrParameters['tmp']['divisionName'])) 
				{
					$arrParms['prenspace'] = 'pICSalesPR';
					$arrParms['nspace'] = 'divisionName';	
					$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
					$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
					$arrParms['cobj'] = '76345c5d-0b23-4fba-9e7a-19a1c072219b';
					$arrParms['aobj'] = '0b872b46-0b23-4fb4-9082-7b10ee9aa5ea';
					$arrParms['val'] = $arrParameters['tmp']['divisionName'];

					$returnXML .= glbXMLDataBlock($arrParms);

					unset($arrParms);				
				}

				if(!empty($arrParameters['tmp']['divisionNameLatin'])) 
				{ 
					$arrParms['prenspace'] = 'pICSalesPR';
					$arrParms['nspace'] = 'divisionNameLatin';	
					$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
					$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
					$arrParms['cobj'] = 'cb792da0-0b23-4fb3-ae0b-2e93623b0972';
					$arrParms['aobj'] = 'f5a62afe-0b23-4fba-a289-adee23ec8d0c';
					$arrParms['val'] = $arrParameters['tmp']['divisionNameLatin'];

					$returnXML .= glbXMLDataBlock($arrParms);

					unset($arrParms);				
				}

				if(!empty($arrParameters['tmp']['divisionNameFurigana'])) 
				{ 
					$arrParms['prenspace'] = 'pICSalesPR';
					$arrParms['nspace'] = 'divisionNameFurigana';	
					$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
					$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
					$arrParms['cobj'] = '628839ba-0b23-4fba-ba99-234452f4fda5';
					$arrParms['aobj'] = 'b1bd6ed1-0b23-4fb6-8745-8f841a0ed8d2';
					$arrParms['val'] = $arrParameters['tmp']['divisionNameFurigana'];

					$returnXML .= glbXMLDataBlock($arrParms);

					unset($arrParms);				
				}
				
				$returnXML .= '
				</pICSalesPR:divisionName>';
			}

			if(!empty($arrParameters['tmp']['individualName']) || !empty($arrParameters['tmp']['individualNameLatin']) || !empty($arrParameters['tmp']['individualNameFurigana'])) 
			{
				$returnXML .= '
				<pICSalesPR:individualName>';

				if(!empty($arrParameters['tmp']['individualName'])) 
				{
					$arrParms['prenspace'] = 'pICSalesPR';
					$arrParms['nspace'] = 'individualName';	
					$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
					$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
					$arrParms['cobj'] = '546c56ec-0b23-4fb0-a496-4ff7c6c0de10';
					$arrParms['aobj'] = '4d059424-0b23-4fb2-a45f-85fbe7950449';
					$arrParms['val'] = $arrParameters['tmp']['individualName'];

					$returnXML .= glbXMLDataBlock($arrParms);

					unset($arrParms);				
				}

				if(!empty($arrParameters['tmp']['individualNameLatin'])) 
				{
					$arrParms['prenspace'] = 'pICSalesPR';
					$arrParms['nspace'] = 'individualNameLatin';	
					$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
					$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
					$arrParms['cobj'] = 'dd67410b-0b23-4fbb-8e38-01563bfd93cb';
					$arrParms['aobj'] = 'c0acdcfd-0b23-4fbc-b14c-8ac2ef5ae2b6';
					$arrParms['val'] = $arrParameters['tmp']['individualNameLatin'];

					$returnXML .= glbXMLDataBlock($arrParms);

					unset($arrParms);				
				}
				
				if(!empty($arrParameters['tmp']['individualNameFurigana'])) 
				{
					$arrParms['prenspace'] = 'pICSalesPR';
					$arrParms['nspace'] = 'individualNameFurigana';	
					$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
					$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
					$arrParms['cobj'] = '73118657-0b23-4fbf-be11-0d3c6404d1d6';
					$arrParms['aobj'] = '45935467-0b23-4fb2-9d15-cf21c0aa31e0';
					$arrParms['val'] = $arrParameters['tmp']['individualNameFurigana'];

					$returnXML .= glbXMLDataBlock($arrParms);

					unset($arrParms);				
				}

				$returnXML .= '
				</pICSalesPR:individualName>';
			}

			$returnXML .= '
			</pICSalesPR:pICSalesPR>
			<party-v0:contactableVia>
				<party-v0:contactMediumKey>
					<cbe-v0:type>ContactMedium</cbe-v0:type>
					<cbe-v0:objectID>3ba71060-0b23-4fb2-8962-6419a8272f7d</cbe-v0:objectID>
				</party-v0:contactMediumKey>
			</party-v0:contactableVia>
		</party-v0:partyRole>';
	}

	unset($arrParameters,$arrParms);

	return $returnXML;

}

function xmlContractMediumBlock($arrParameters = array()) 
{

	$arrParms = array();
	
	$returnXML =  '
	<party-v0:contactMedium>
		<cbe-v0:identifiedBy>
			<party-v0:contactMediumKey>
				<cbe-v0:type>ContactMedium</cbe-v0:type>
				<cbe-v0:objectID>effd0ebe-0b23-4fb9-9152-42644cfb18c9</cbe-v0:objectID>
			</party-v0:contactMediumKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>契約先担当者連絡先</cbe-v0:name>
		<cbe-v0:nameLatin>contact medium for person in charge of contract</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<party-v0:referenceToContactMediumSpec name="ContactMedium">
				<cbe-v0:type>ContactMediumSpecification</cbe-v0:type>
				<cbe-v0:objectID>a7fd0ea0-0b23-4fb0-8884-d6eb0f5d8dea</cbe-v0:objectID>
			</party-v0:referenceToContactMediumSpec>
		</cbe-v0:describedBy>
		<contactMedium:contactMedium name="standardContactMedium">';
		
		if(!empty($arrParameters['tmp']['telephoneNumber']) || !empty($arrParameters['tmp']['faxNumber']) || !empty($arrParameters['tmp']['eMailAddress'])) 
		{
			if(!empty($arrParameters['tmp']['telephoneNumber'])) 
			{
				$arrParms['prenspace'] = 'contactMedium';
				$arrParms['nspace'] = 'telephoneNumber';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '2ab5b9df-0b23-4fb8-8ebb-8370ccbf12be';
				$arrParms['aobj'] = '07711ed8-0b23-4fbe-b78e-917d269f6890';
				$arrParms['val'] = $arrParameters['tmp']['telephoneNumber'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}

			if(!empty($arrParameters['tmp']['faxNumber'])) 
			{
				$arrParms['prenspace'] = 'contactMedium';
				$arrParms['nspace'] = 'faxNumber';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = 'c35d457b-0b23-4fb4-bed8-9e620f5bbc68';
				$arrParms['aobj'] = 'ccfb023b-0b23-4fbe-99f8-5a2f4c2a7339';
				$arrParms['val'] = $arrParameters['tmp']['faxNumber'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}			

			if(!empty($arrParameters['tmp']['eMailAddress'])) 
			{
				$arrParms['prenspace'] = 'contactMedium';
				$arrParms['nspace'] = 'eMailAddress';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '537ac9fb-0b23-4fb0-a356-1c6f4ebb8491';
				$arrParms['aobj'] = '131c0683-0b23-4fb6-8ce1-7589daeb5e2e';
				$arrParms['val'] = $arrParameters['tmp']['eMailAddress'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}			

		}

		$returnXML .= '
		</contactMedium:contactMedium>
	</party-v0:contactMedium>';	

	unset($arrParameters,$arrParms);

	return $returnXML;

}

function xmlBillingMediumBlock($arrParameters = array()) 
{

	$arrParms = array();
	
	$returnXML =  '
	<party-v0:contactMedium>
		<cbe-v0:identifiedBy>
			<party-v0:contactMediumKey>
				<cbe-v0:type>ContactMedium</cbe-v0:type>
				<cbe-v0:objectID>26af4dc8-0b23-4fbc-b28e-e1fc188bc8c8</cbe-v0:objectID>
			</party-v0:contactMediumKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>請求先担当者連絡先</cbe-v0:name>
		<cbe-v0:nameLatin>contact medium for person in charge of billing</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<party-v0:referenceToContactMediumSpec name="ContactMedium">
				<cbe-v0:type>ContactMediumSpecification</cbe-v0:type>
				<cbe-v0:objectID>a7fd0ea0-0b23-4fb0-8884-d6eb0f5d8dea</cbe-v0:objectID>
			</party-v0:referenceToContactMediumSpec>
		</cbe-v0:describedBy>
		<contactMedium:contactMedium name="standardContactMedium">';

		if(!empty($arrParameters['tmp']['telephoneNumber']) || !empty($arrParameters['tmp']['faxNumber']) || !empty($arrParameters['tmp']['eMailAddress'])) 
		{
			if(!empty($arrParameters['tmp']['telephoneNumber'])) 
			{
				$arrParms['prenspace'] = 'contactMedium';
				$arrParms['nspace'] = 'telephoneNumber';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = 'a59d300d-0b23-4fbe-bb48-33005d0e50ad';
				$arrParms['aobj'] = '07711ed8-0b23-4fbe-b78e-917d269f6890';
				$arrParms['val'] = $arrParameters['tmp']['telephoneNumber'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}

			if(!empty($arrParameters['tmp']['faxNumber'])) 
			{
				$arrParms['prenspace'] = 'contactMedium';
				$arrParms['nspace'] = 'faxNumber';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '79ebbbea-0b23-4fb6-b700-281a2a3b0ebb';
				$arrParms['aobj'] = 'ccfb023b-0b23-4fbe-99f8-5a2f4c2a7339';
				$arrParms['val'] = $arrParameters['tmp']['faxNumber'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}			

			if(!empty($arrParameters['tmp']['eMailAddress'])) 
			{
				$arrParms['prenspace'] = 'contactMedium';
				$arrParms['nspace'] = 'eMailAddress';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '979469bc-0b23-4fb8-bd7a-4273b48a5953';
				$arrParms['aobj'] = '131c0683-0b23-4fb6-8ce1-7589daeb5e2e';
				$arrParms['val'] = $arrParameters['tmp']['eMailAddress'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}
			
		}

		$returnXML .= '
		</contactMedium:contactMedium>
	</party-v0:contactMedium>';	

	unset($arrParameters,$arrParms);

	return $returnXML;

}

function xmlSalesMediumBlock($arrParameters = array()) 
{
	
	$arrParms = array();

	$returnXML =  '
	<party-v0:contactMedium>
		<cbe-v0:identifiedBy>
			<party-v0:contactMediumKey>
				<cbe-v0:type>ContactMedium</cbe-v0:type>
				<cbe-v0:objectID>3ba71060-0b23-4fb2-8962-6419a8272f7d</cbe-v0:objectID>
			</party-v0:contactMediumKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>販売担当者連絡先</cbe-v0:name>
		<cbe-v0:nameLatin>contact medium for person in charge of sales</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<party-v0:referenceToContactMediumSpec name="ContactMedium">
				<cbe-v0:type>ContactMediumSpecification</cbe-v0:type>
				<cbe-v0:objectID>a7fd0ea0-0b23-4fb0-8884-d6eb0f5d8dea</cbe-v0:objectID>
			</party-v0:referenceToContactMediumSpec>
		</cbe-v0:describedBy>
		<contactMedium:contactMedium name="standardContactMedium">';

		if(!empty($arrParameters['tmp']['telephoneNumber']) || !empty($arrParameters['tmp']['faxNumber']) || !empty($arrParameters['tmp']['eMailAddress'])) 
		{
			if(!empty($arrParameters['tmp']['telephoneNumber'])) 
			{
				$arrParms['prenspace'] = 'contactMedium';
				$arrParms['nspace'] = 'telephoneNumber';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '0a0fc8ab-0b23-4fbe-9912-9c37c1040b1d';
				$arrParms['aobj'] = '07711ed8-0b23-4fbe-b78e-917d269f6890';
				$arrParms['val'] = $arrParameters['tmp']['telephoneNumber'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}

			if(!empty($arrParameters['tmp']['faxNumber'])) 
			{
				$arrParms['prenspace'] = 'contactMedium';
				$arrParms['nspace'] = 'faxNumber';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '7f53ce8f-0b23-4fbb-b0b4-23512fcde675';
				$arrParms['aobj'] = 'ccfb023b-0b23-4fbe-99f8-5a2f4c2a7339';
				$arrParms['val'] = $arrParameters['tmp']['faxNumber'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}			

			if(!empty($arrParameters['tmp']['eMailAddress'])) 
			{
				$arrParms['prenspace'] = 'contactMedium';
				$arrParms['nspace'] = 'eMailAddress';	
				$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
				$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
				$arrParms['cobj'] = '55506b99-0b23-4fb0-adbb-ba9af9a81d91';
				$arrParms['aobj'] = '131c0683-0b23-4fb6-8ce1-7589daeb5e2e';
				$arrParms['val'] = $arrParameters['tmp']['eMailAddress'];

				$returnXML .= glbXMLDataBlock($arrParms);

				unset($arrParms);				
			}				
		}

		$returnXML .= '
		</contactMedium:contactMedium>
	</party-v0:contactMedium>';	

	unset($arrParameters,$arrParms);

	return $returnXML;

}

function xmlContractLocationBlock($arrParameters = array()) 
{
	
	$arrParms = $arrGeoParms =array();	

	$arrGeoParms = glbXMLContractLocaionBlock($arrParameters);

	$returnXML =  '
	<location-v0:geographicAddress>
		<cbe-v0:identifiedBy>
			<location-v0:geoAddressKey>
				<cbe-v0:type>GeographicAddress</cbe-v0:type>
				<cbe-v0:objectID>'.$arrGeoParms['geo']['GeographicAddress'].'</cbe-v0:objectID>
			</location-v0:geoAddressKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:describedBy>
			<location-v0:referenceToGeoAddressSpec>
				<cbe-v0:type>GeoAddressSpecification</cbe-v0:type>
				<cbe-v0:objectID>'.$arrGeoParms['geo']['GeoAddressSpecification'].'</cbe-v0:objectID>
			</location-v0:referenceToGeoAddressSpec>
		</cbe-v0:describedBy>
		<location-v0:countryKey name="'.$arrParameters['tmp']['countryKey'].'">
			<cbe-v0:type>Country</cbe-v0:type>
			<location-v0:iso3166_1numeric>'.$arrParameters['tmp']['iso3166_1numeric'].'</location-v0:iso3166_1numeric>
		</location-v0:countryKey>';
		
		if(!empty($arrParameters['tmp']['postalCode']) || !empty($arrParameters['tmp']['address']) || !empty($arrParameters['tmp']['addressLine']) || !empty($arrParameters['tmp']['addressLatin']) || !empty($arrParameters['tmp']['addressLocal']) || !empty($arrParameters['tmp']['addressGeneral']) || !empty($arrParameters['tmp']['stateCode']) || !empty($arrParameters['tmp']['cityGeneral'])) 
		{
			$returnXML .=  '
			<location-v0:usedAddressRepresentation>
				<location-v0:countrySpecificAddress>
					<cbe-v0:identifiedBy>
						<location-v0:countrySpecificAddressKey>
							<cbe-v0:type>CountrySpecificAddress</cbe-v0:type>
							<cbe-v0:objectID>'.$arrGeoParms['geo']['CountrySpecificAddress'].'</cbe-v0:objectID>
						</location-v0:countrySpecificAddressKey>
					</cbe-v0:identifiedBy>
					<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
					<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
					<cbe-v0:describedBy>
						<location-v0:referenceToCountrySpecificAddressSpec name="'.$arrGeoParms['geo']['property'] .'">
							<cbe-v0:type>CountrySpecificAddressSpecification</cbe-v0:type>
							<cbe-v0:objectID>'.$arrGeoParms['geo']['CountrySpecificAddressSpecification'].'</cbe-v0:objectID>
						</location-v0:referenceToCountrySpecificAddressSpec>
					</cbe-v0:describedBy>';

					if($arrGeoParms['geo']['anyFlag'] == 1) {
						
						$returnXML .=  '
						<anyAddr:anyAddress name="Any Address">';

						$arrParms['prenspace'] = 'anyAddr';
						$arrParms['nspace'] = 'address';	
						$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
						$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
						$arrParms['cobj'] = $arrGeoParms['geo']['cobj'];
						$arrParms['aobj'] = $arrGeoParms['geo']['aobj'];
						$arrParms['val'] = (!empty($arrParameters['tmp']['addressLatin'])) ? $arrParameters['tmp']['addressLatin'] : $arrParameters['tmp']['addressLocal'];

						$returnXML .= glbXMLDataBlock($arrParms);

						unset($arrParms);				
						
						$returnXML .=  '
						</anyAddr:anyAddress>';

					} elseif($arrParameters['tmp']['iso3166_1numeric'] == '124' || 
                             $arrParameters['tmp']['iso3166_1numeric'] == '840') {
                        
                        $returnXML .=  '
						<'.$arrGeoParms['geo']['iso3166_1alpha'].'Addr:'.$arrGeoParms['geo']['property'] .' name="'.$arrGeoParms['geo']['propertyName'] .' Property Address">';
                        
                        if(!empty($arrParameters['tmp']['addressGeneral']))	{   
                            $prenspace = $cobj = $aobj = '';
                            if($arrParameters['tmp']['iso3166_1numeric'] == '124') {
                                $prenspace = 'canAddr';
                                $cobj = '8c017f83-0b23-4fbc-8a76-423940d93650';
                                $aobj = '549e287c-0b23-4fba-a70d-0866d3771417';
                            } else {
                                $prenspace = 'usaAddr';
                                $cobj = 'f716882f-0b23-4fb1-98ae-a22d31534663';
                                $aobj = '2cf0dcdc-0b23-4fb7-a351-1824e4ab6190';
                            }
							$arrParms['prenspace'] = $prenspace;
							$arrParms['nspace'] = 'addressLine';	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = $cobj;
							$arrParms['aobj'] = $aobj;
							$arrParms['val'] = $arrParameters['tmp']['addressGeneral'];
							$returnXML .= glbXMLDataBlock($arrParms);
							unset($arrParms);				
						}
						
						if(!empty($arrParameters['tmp']['cityGeneral'])) {
                            $prenspace = $cobj = $aobj = '';
                            if($arrParameters['tmp']['iso3166_1numeric'] == '124') {
                                $prenspace = 'canAddr';
                                $cobj = '6c1af77e-0b23-4fb3-afff-60ac7d090a74';
                                $aobj = '749c4bea-0b23-4fb0-a28c-bdd5953a91f5';
                            } else {
                                $prenspace = 'usaAddr';
                                $cobj = '5eb860be-0b23-4fbb-ba41-b62d34cdacfe';
                                $aobj = 'afa3eb8f-0b23-4fb2-82a3-0ec2732ede79';
                            }
							$arrParms['prenspace'] = $prenspace;
							$arrParms['nspace'] = 'city';	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = $cobj;
							$arrParms['aobj'] = $aobj;
							$arrParms['val'] = $arrParameters['tmp']['cityGeneral'];
							$returnXML .= glbXMLDataBlock($arrParms);
							unset($arrParms);				
						}						
						
						if(!empty($arrParameters['tmp']['stateCode'])) {
							$prenspace = $cobj = $aobj = '';
                            if($arrParameters['tmp']['iso3166_1numeric'] == '124') {
                                $prenspace = 'canAddr';
                                $cobj = '5303cb73-0b23-4fbb-a356-740a7df9dcf2';
                                $aobj = '1e5ce830-0b23-4fbd-b29d-f435fb661c2a';
                            } else {
                                $prenspace = 'usaAddr';
                                $cobj = '10cbe88b-0b23-4fb8-8fea-157321523975';
                                $aobj = '3ac88a04-0b23-4fb2-86a5-8b8950eaebb1';
                            }
							$arrParms['prenspace'] = $prenspace;
							$arrParms['nspace'] = 'stateAbbr';	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = $cobj;
							$arrParms['aobj'] = $aobj;
                            $arrParms['val'] = $arrParameters['tmp']['stateCode'];
							$returnXML .= glbXMLDataBlock($arrParms);
							unset($arrParms);				
						}
                        
                        if(!empty($arrParameters['tmp']['postalCode']))	{
							$prenspace = $nspace = ''; $cobj = $aobj = '';
                            if($arrParameters['tmp']['iso3166_1numeric'] == '124') {
                                $prenspace = 'canAddr';
                                $nspace = 'postalCode';
                                $cobj = 'e0b0f7eb-0b23-4fbd-b7a7-0d21705f23dc';
                                $aobj = '1e1475aa-0b23-4fb0-9c56-a70d1228a18d';
                            } else {
                                $prenspace = 'usaAddr';
                                $nspace = 'zipCode';
                                $cobj = '73d7dd10-0b23-4fba-9bb4-6abe2a7b5dc1';
                                $aobj = '6d2bffb1-0b23-4fbd-990f-c8957415cdcb';
                            }
							$arrParms['prenspace'] = $prenspace;
							$arrParms['nspace'] = $nspace;	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = $cobj;
							$arrParms['aobj'] = $aobj;
                            $arrParms['val'] = $arrParameters['tmp']['postalCode'];
							$returnXML .= glbXMLDataBlock($arrParms);
							unset($arrParms);				
						}	

							$returnXML .=  '
						</'.$arrGeoParms['geo']['iso3166_1alpha'].'Addr:'.$arrGeoParms['geo']['property'] .'>';
                        
                    
                    } else {

						$returnXML .=  '
						<'.$arrGeoParms['geo']['iso3166_1alpha'].'Addr:'.$arrGeoParms['geo']['property'] .' name="'.$arrGeoParms['geo']['propertyName'] .' Property Address">';
							
							
						if(!empty($arrParameters['tmp']['postalCode'])) 
						{
							$arrParms['prenspace'] = 'jpnAddr';
							$arrParms['nspace'] = 'postalCode';	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = '2a5d6e66-0b23-4fb3-b316-407fe1e69123';
							$arrParms['aobj'] = '48cd78c1-0b23-4fb8-918e-64ea2c129366';
							$arrParms['val'] = $arrParameters['tmp']['postalCode'];

							$returnXML .= glbXMLDataBlock($arrParms);

							unset($arrParms);				
						}						

						if(!empty($arrParameters['tmp']['address'])) 
						{
							$arrParms['prenspace'] = 'jpnAddr';
							$arrParms['nspace'] = 'address';	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = '2fefcc9b-0b23-4fba-8f44-66ef43abd279';
							$arrParms['aobj'] = 'ad5aaa70-0b23-4fb9-98ee-dfb35369a2fb';
							$arrParms['val'] = $arrParameters['tmp']['address'];

							$returnXML .= glbXMLDataBlock($arrParms);

							unset($arrParms);				
						}						
						
						if(!empty($arrParameters['tmp']['addressLine'])) 
						{
							$arrParms['prenspace'] = 'jpnAddr';
							$arrParms['nspace'] = 'addressLine';	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = '9a30aa4c-0b23-4fbb-8b86-cbeeda3d4fae';
							$arrParms['aobj'] = 'd9870b88-0b23-4fbe-9806-2f619b0bad8d';
							$arrParms['val'] = $arrParameters['tmp']['addressLine'];

							$returnXML .= glbXMLDataBlock($arrParms);

							unset($arrParms);				
						}
					
					$returnXML .=  '
					</'.$arrGeoParms['geo']['iso3166_1alpha'].'Addr:'.$arrGeoParms['geo']['property'] .'>';

					}

				$returnXML .=  '
					<location-v0:subordinateTo>
						<location-v0:geoAddressKey>
							<cbe-v0:type>GeographicAddress</cbe-v0:type>
							<cbe-v0:objectID>'.$arrGeoParms['geo']['GeographicAddress'].'</cbe-v0:objectID>
						</location-v0:geoAddressKey>
					</location-v0:subordinateTo>
					<location-v0:languageNotation>
						<datatypes-v0:languageName>'.$arrGeoParms['geo']['languageName'] .'</datatypes-v0:languageName>
						<datatypes-v0:notation>'.$arrGeoParms['geo']['notation'] .'</datatypes-v0:notation>
					</location-v0:languageNotation>
					<location-v0:isDefault>true</location-v0:isDefault>
				</location-v0:countrySpecificAddress>

			</location-v0:usedAddressRepresentation>';

		}
	$returnXML .=  '
	</location-v0:geographicAddress>';

	
	unset($arrParameters,$arrParms,$arrGeoParms);

	return $returnXML;

}

function xmlBillingLocationBlock($arrParameters = array()) 
{

	$arrParms = $arrGeoParms =array();	

	$arrGeoParms = glbXMLBillingLocaionBlock($arrParameters);	

	$returnXML =  '
	<location-v0:geographicAddress>
		<cbe-v0:identifiedBy>
			<location-v0:geoAddressKey>
				<cbe-v0:type>GeographicAddress</cbe-v0:type>
				<cbe-v0:objectID>'.$arrGeoParms['geo']['GeographicAddress'].'</cbe-v0:objectID>
			</location-v0:geoAddressKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:describedBy>
			<location-v0:referenceToGeoAddressSpec>
				<cbe-v0:type>GeoAddressSpecification</cbe-v0:type>
				<cbe-v0:objectID>'.$arrGeoParms['geo']['GeoAddressSpecification'].'</cbe-v0:objectID>
			</location-v0:referenceToGeoAddressSpec>
		</cbe-v0:describedBy>
		<location-v0:countryKey name="'.$arrParameters['tmp']['countryKey'].'">
			<cbe-v0:type>Country</cbe-v0:type>
			<location-v0:iso3166_1numeric>'.$arrParameters['tmp']['iso3166_1numeric'].'</location-v0:iso3166_1numeric>
		</location-v0:countryKey>';

		if(!empty($arrParameters['tmp']['postalCode']) || !empty($arrParameters['tmp']['address']) || !empty($arrParameters['tmp']['addressLine']) || !empty($arrParameters['tmp']['addressLatin']) || !empty($arrParameters['tmp']['addressLocal']) || !empty($arrParameters['tmp']['addressGeneral']) || !empty($arrParameters['tmp']['stateCode']) || !empty($arrParameters['tmp']['cityGeneral'])) 
		{
			$returnXML .=  '
			<location-v0:usedAddressRepresentation>
				<location-v0:countrySpecificAddress>
					<cbe-v0:identifiedBy>
						<location-v0:countrySpecificAddressKey>
							<cbe-v0:type>CountrySpecificAddress</cbe-v0:type>
							<cbe-v0:objectID>'.$arrGeoParms['geo']['CountrySpecificAddress'].'</cbe-v0:objectID>
						</location-v0:countrySpecificAddressKey>
					</cbe-v0:identifiedBy>
					<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
					<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
					<cbe-v0:describedBy>
						<location-v0:referenceToCountrySpecificAddressSpec name="'.$arrGeoParms['geo']['property'] .'">
							<cbe-v0:type>CountrySpecificAddressSpecification</cbe-v0:type>
							<cbe-v0:objectID>'.$arrGeoParms['geo']['CountrySpecificAddressSpecification'].'</cbe-v0:objectID>
						</location-v0:referenceToCountrySpecificAddressSpec>
					</cbe-v0:describedBy>';

					if($arrGeoParms['geo']['anyFlag'] == 1) {
						
						$returnXML .=  '
						<anyAddr:anyAddress name="Any Address">';

						$arrParms['prenspace'] = 'anyAddr';
						$arrParms['nspace'] = 'address';	
						$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
						$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
						$arrParms['cobj'] = $arrGeoParms['geo']['cobj'];
						$arrParms['aobj'] = $arrGeoParms['geo']['aobj'];
						$arrParms['val'] = (!empty($arrParameters['tmp']['addressLatin'])) ? $arrParameters['tmp']['addressLatin'] : $arrParameters['tmp']['addressLocal'];

						$returnXML .= glbXMLDataBlock($arrParms);

						unset($arrParms);				
						
						$returnXML .=  '
						</anyAddr:anyAddress>';

					} elseif($arrParameters['tmp']['iso3166_1numeric'] == '124' || 
                             $arrParameters['tmp']['iso3166_1numeric'] == '840') {
                        $returnXML .=  '
						<'.$arrGeoParms['geo']['iso3166_1alpha'].'Addr:'.$arrGeoParms['geo']['property'] .' name="'.$arrGeoParms['geo']['propertyName'] .' Property Address">';
                        
                        if(!empty($arrParameters['tmp']['addressGeneral']))	{   
                            $prenspace = $cobj = $aobj = '';
                            if($arrParameters['tmp']['iso3166_1numeric'] == '124') {
                                $prenspace = 'canAddr';
                                $cobj = '4b79b11e-0b23-4fb6-beec-289383f2c7f0';
                                $aobj = 'ac71ec55-0b23-4fb5-b5a9-5f2937d06128';
                            } else {
                                $prenspace = 'usaAddr';
                                $cobj = 'b8e3b95f-0b23-4fb3-ab41-534a39fea399';
                                $aobj = 'e93eb141-0b23-4fbf-801f-97e57b75295e';
                            }
							$arrParms['prenspace'] = $prenspace;
							$arrParms['nspace'] = 'addressLine';	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = $cobj;
							$arrParms['aobj'] = $aobj;
							$arrParms['val'] = $arrParameters['tmp']['addressGeneral'];
							$returnXML .= glbXMLDataBlock($arrParms);
							unset($arrParms);				
						}
						
						if(!empty($arrParameters['tmp']['cityGeneral'])) {
                            $prenspace = $cobj = $aobj = '';
                            if($arrParameters['tmp']['iso3166_1numeric'] == '124') {
                                $prenspace = 'canAddr';
                                $cobj = '64cb3aea-0b23-4fbc-a094-d2808fa5b2f2';
                                $aobj = 'c4084aec-0b23-4fb3-8003-940d6aed275b';
                            } else {
                                $prenspace = 'usaAddr';
                                $cobj = 'e61d79c5-0b23-4fb6-81e4-4dd99a1b1f94';
                                $aobj = 'ca9e3e4c-0b23-4fba-8e75-c5669b87372e';
                            }
							$arrParms['prenspace'] = $prenspace;
							$arrParms['nspace'] = 'city';	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = $cobj;
							$arrParms['aobj'] = $aobj;
							$arrParms['val'] = $arrParameters['tmp']['cityGeneral'];
							$returnXML .= glbXMLDataBlock($arrParms);
							unset($arrParms);				
						}						
						
						if(!empty($arrParameters['tmp']['stateCode'])) {
							$prenspace = $cobj = $aobj = '';
                            if($arrParameters['tmp']['iso3166_1numeric'] == '124') {
                                $prenspace = 'canAddr';
                                $cobj = 'b65b38c4-0b23-4fb3-9327-c414730ea7c0';
                                $aobj = 'abd85281-0b23-4fba-a095-c2ffd9fe8881';
                            } else {
                                $prenspace = 'usaAddr';
                                $cobj = 'b3750b56-0b23-4fb3-a8c6-894d9dd0b52b';
                                $aobj = 'fc29ae4f-0b23-4fba-84f8-bc065bacb6ee';
                            }
							$arrParms['prenspace'] = $prenspace;
							$arrParms['nspace'] = 'stateAbbr';	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = $cobj;
							$arrParms['aobj'] = $aobj;
                            $arrParms['val'] = $arrParameters['tmp']['stateCode'];
							$returnXML .= glbXMLDataBlock($arrParms);
							unset($arrParms);				
						}
                        
                        if(!empty($arrParameters['tmp']['postalCode']))	{
							$prenspace = $nspace = ''; $cobj = $aobj = '';
                            if($arrParameters['tmp']['iso3166_1numeric'] == '124') {
                                $prenspace = 'canAddr';
                                $nspace = 'postalCode';
                                $cobj = '295994a4-0b23-4fba-9b25-0a90d8be4ef2';
                                $aobj = '78cbb33b-0b23-4fb8-b8e9-90fb379eee14';
                            } else {
                                $prenspace = 'usaAddr';
                                $nspace = 'zipCode';
                                $cobj = 'c75cc5f6-0b23-4fb3-b425-d7508a9316b7';
                                $aobj = 'cd9b2185-0b23-4fb4-9a87-150b54278b63';
                            }
							$arrParms['prenspace'] = $prenspace;
							$arrParms['nspace'] = $nspace;	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = $cobj;
							$arrParms['aobj'] = $aobj;
                            $arrParms['val'] = $arrParameters['tmp']['postalCode'];
							$returnXML .= glbXMLDataBlock($arrParms);
							unset($arrParms);				
						}	

							$returnXML .=  '
						</'.$arrGeoParms['geo']['iso3166_1alpha'].'Addr:'.$arrGeoParms['geo']['property'] .'>';
                    } else { 

						$returnXML .=  '
						<'.$arrGeoParms['geo']['iso3166_1alpha'].'Addr:'.$arrGeoParms['geo']['property'] .' name="'.$arrGeoParms['geo']['propertyName'] .' Property Address">';

						if(!empty($arrParameters['tmp']['postalCode'])) 
						{
							$arrParms['prenspace'] = 'jpnAddr';
							$arrParms['nspace'] = 'postalCode';	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = 'e6341040-0b23-4fb4-a517-29721dbac53f';
							$arrParms['aobj'] = '48cd78c1-0b23-4fb8-918e-64ea2c129366';
							$arrParms['val'] = $arrParameters['tmp']['postalCode'];
							$returnXML .= glbXMLDataBlock($arrParms);
							unset($arrParms);				
						}	

						if(!empty($arrParameters['tmp']['address'])) 
						{
							$arrParms['prenspace'] = 'jpnAddr';
							$arrParms['nspace'] = 'address';	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = 'c6e2372f-0b23-4fb0-83f5-242b7f786f72';
							$arrParms['aobj'] = 'ad5aaa70-0b23-4fb9-98ee-dfb35369a2fb';
							$arrParms['val'] = $arrParameters['tmp']['address'];
							$returnXML .= glbXMLDataBlock($arrParms);
							unset($arrParms);				
						}
						
						if(!empty($arrParameters['tmp']['addressLine'])) 
						{
							$arrParms['prenspace'] = 'jpnAddr';
							$arrParms['nspace'] = 'addressLine';	
							$arrParms['createdDate'] = $arrParameters['glb']['createdDate'];
							$arrParms['createdUser'] = $arrParameters['glb']['createdUser'];
							$arrParms['cobj'] = 'c6e2372f-0b23-4fb0-83f5-242b7f786f72';
							$arrParms['aobj'] = 'd9870b88-0b23-4fbe-9806-2f619b0bad8d';
							$arrParms['val'] = $arrParameters['tmp']['addressLine'];
							$returnXML .= glbXMLDataBlock($arrParms);
							unset($arrParms);				
						}

							$returnXML .=  '
						</'.$arrGeoParms['geo']['iso3166_1alpha'].'Addr:'.$arrGeoParms['geo']['property'] .'>';
					}
				
					$returnXML .=  '
					<location-v0:subordinateTo>
						<location-v0:geoAddressKey>
							<cbe-v0:type>GeographicAddress</cbe-v0:type>
							<cbe-v0:objectID>'.$arrGeoParms['geo']['GeographicAddress'].'</cbe-v0:objectID>
						</location-v0:geoAddressKey>
					</location-v0:subordinateTo>
					<location-v0:languageNotation>
						<datatypes-v0:languageName>'.$arrGeoParms['geo']['languageName'] .'</datatypes-v0:languageName>
						<datatypes-v0:notation>'.$arrGeoParms['geo']['notation'] .'</datatypes-v0:notation>
					</location-v0:languageNotation>
					<location-v0:isDefault>true</location-v0:isDefault>
				</location-v0:countrySpecificAddress>
			</location-v0:usedAddressRepresentation>';
		}

	$returnXML .=  '
	</location-v0:geographicAddress>';


	unset($arrParameters,$arrParms);

	return $returnXML;

}

function xmlPartyRoleAssocBlock($arrParameters = array()) 
{
	$returnXML =  '
	<location-v0:placePartyRoleAssociation>
		<cbe-v0:identifiedBy>
			<location-v0:placePartyRoleAssocKey>
				<cbe-v0:type>PlacePartyRoleAssociation</cbe-v0:type>
				<cbe-v0:objectID>'.$GLOBALS["geoContractPlaceParty"].'</cbe-v0:objectID>
			</location-v0:placePartyRoleAssocKey>
		</cbe-v0:identifiedBy>		
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>契約先住所(顧客記入)</cbe-v0:name>
		<cbe-v0:nameLatin>Contract Customer Address</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<location-v0:referenceToPlacePartyRoleAssocSpec>
				<cbe-v0:type>PlacePartyRoleAssocSpecification</cbe-v0:type>
				<cbe-v0:objectID>0e45f23e-0b23-4fb2-a8a2-c241025bb61e</cbe-v0:objectID>
			</location-v0:referenceToPlacePartyRoleAssocSpec>
		</cbe-v0:describedBy>
		<cbe-v0:associationEnds>
			<location-v0:geoAddressKey>
				<cbe-v0:type>GeographicAddress</cbe-v0:type>
				<cbe-v0:objectID>'.$GLOBALS["geoContract"].'</cbe-v0:objectID>
			</location-v0:geoAddressKey>
		</cbe-v0:associationEnds>
		<cbe-v0:associationEnds>
			<party-v0:partyRoleKey>
				<cbe-v0:type>PartyRole</cbe-v0:type>
				<cbe-v0:objectID>29456d12-0b23-4fbf-8f7d-e65b6219678c</cbe-v0:objectID>
			</party-v0:partyRoleKey>
		</cbe-v0:associationEnds>
	</location-v0:placePartyRoleAssociation>

	<location-v0:placePartyRoleAssociation>
		<cbe-v0:identifiedBy>
			<location-v0:placePartyRoleAssocKey>
				<cbe-v0:type>PlacePartyRoleAssociation</cbe-v0:type>
				<cbe-v0:objectID>'.$GLOBALS["geoBillingPlaceParty"].'</cbe-v0:objectID>
			</location-v0:placePartyRoleAssocKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>請求先住所</cbe-v0:name>
		<cbe-v0:nameLatin>Billing Address</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<location-v0:referenceToPlacePartyRoleAssocSpec>
				<cbe-v0:type>PlacePartyRoleAssocSpecification</cbe-v0:type>
				<cbe-v0:objectID>0e45f23e-0b23-4fb2-a8a2-c241025bb61e</cbe-v0:objectID>
			</location-v0:referenceToPlacePartyRoleAssocSpec>
		</cbe-v0:describedBy>
		<cbe-v0:associationEnds>
			<location-v0:geoAddressKey>
				<cbe-v0:type>GeographicAddress</cbe-v0:type>
				<cbe-v0:objectID>'.$GLOBALS["geoBilling"].'</cbe-v0:objectID>
			</location-v0:geoAddressKey>
		</cbe-v0:associationEnds>
		<cbe-v0:associationEnds>
			<party-v0:partyRoleKey>
				<cbe-v0:type>PartyRole</cbe-v0:type>
				<cbe-v0:objectID>f7f9e523-0b23-4fbb-8e92-cb69e143a4ea</cbe-v0:objectID>
			</party-v0:partyRoleKey>
		</cbe-v0:associationEnds>
	</location-v0:placePartyRoleAssociation>';

	unset($arrParameters);

	return $returnXML;

}

function xmlCustomerPaymentBlock($arrParameters = array()) 
{

	$returnXML =  '
	<customer-v0:paymentMethod>
		<cbe-v0:identifiedBy>
			<customer-v0:paymentMethodKey>
				<cbe-v0:type>PaymentMethod</cbe-v0:type>
				<cbe-v0:objectID>06da242f-0b23-4fb4-a729-1d9e43ef9ac9</cbe-v0:objectID>
			</customer-v0:paymentMethodKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>一般:口座直接入金情報 (請求書払い)</cbe-v0:name>
		<cbe-v0:nameLatin>General: Direct Deposit</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<customer-v0:referenceToPaymentMethodSpec name="jpnDirectDepositPM">
				<cbe-v0:type>PaymentMethodSpecification</cbe-v0:type>
				<cbe-v0:objectID>c8072bb6-0b23-4fb2-9ffe-c6ac534ed139</cbe-v0:objectID>
			</customer-v0:referenceToPaymentMethodSpec>
		</cbe-v0:describedBy>
	</customer-v0:paymentMethod>

	<customer-v0:pMPartyRoleAssociation>
		<cbe-v0:identifiedBy>
			<customer-v0:pMPartyRoleAssocKey>
				<cbe-v0:type>PMPartyRoleAssociation</cbe-v0:type>
				<cbe-v0:objectID>7d834c18-0b23-4fb3-9876-70768a28e376</cbe-v0:objectID>
			</customer-v0:pMPartyRoleAssocKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>請求先支払方法</cbe-v0:name>
		<cbe-v0:nameLatin>Payment method of billing</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<customer-v0:referenceToPMPartyRoleAssocSpec>
				<cbe-v0:type>PMPartyRoleAssocSpecification</cbe-v0:type>
				<cbe-v0:objectID>a2b1ca44-0b23-4fb0-858b-b53f01c143c0</cbe-v0:objectID>
			</customer-v0:referenceToPMPartyRoleAssocSpec>
		</cbe-v0:describedBy>
		<cbe-v0:associationEnds>
			<customer-v0:paymentMethodKey>
				<cbe-v0:type>PaymentMethod</cbe-v0:type>
				<cbe-v0:objectID>06da242f-0b23-4fb4-a729-1d9e43ef9ac9</cbe-v0:objectID>
			</customer-v0:paymentMethodKey>
		</cbe-v0:associationEnds>
		<cbe-v0:associationEnds>
			<party-v0:partyRoleKey>
				<cbe-v0:type>PartyRole</cbe-v0:type>
				<cbe-v0:objectID>f7f9e523-0b23-4fbb-8e92-cb69e143a4ea</cbe-v0:objectID>
			</party-v0:partyRoleKey>
		</cbe-v0:associationEnds>
	</customer-v0:pMPartyRoleAssociation>

	<customer-v0:pMProductAssociation>
		<cbe-v0:identifiedBy>
			<customer-v0:pMProductAssocKey>
				<cbe-v0:type>PMProductAssociation</cbe-v0:type>
				<cbe-v0:objectID>740e9392-0b23-4fba-b64a-d4ee11e4817c</cbe-v0:objectID>
			</customer-v0:pMProductAssocKey>
		</cbe-v0:identifiedBy>
		<cbe-v0:createdDate>'.$arrParameters['glb']['createdDate'].'</cbe-v0:createdDate>
		<cbe-v0:createdUser>'.$arrParameters['glb']['createdUser'].'</cbe-v0:createdUser>
		<cbe-v0:name>支払方法適用契約商品</cbe-v0:name>
		<cbe-v0:nameLatin>Payment method of product</cbe-v0:nameLatin>
		<cbe-v0:describedBy>
			<customer-v0:referenceToPMProductAssocSpec>
				<cbe-v0:type>PMProductAssocSpecification</cbe-v0:type>
				<cbe-v0:objectID>3e8f5641-0b23-4fb1-9496-541c05d22f27</cbe-v0:objectID>
			</customer-v0:referenceToPMProductAssocSpec>
		</cbe-v0:describedBy>
		<cbe-v0:associationEnds>
			<customer-v0:paymentMethodKey>
				<cbe-v0:type>PaymentMethod</cbe-v0:type>
				<cbe-v0:objectID>06da242f-0b23-4fb4-a729-1d9e43ef9ac9</cbe-v0:objectID>
			</customer-v0:paymentMethodKey>
		</cbe-v0:associationEnds>
		<cbe-v0:associationEnds>
			<product-v0:bundledProductKey>
				<cbe-v0:type>BundledProduct</cbe-v0:type>
				<cbe-v0:objectID>410b7100-0b23-4fbd-9d9e-763ce233c0a1</cbe-v0:objectID>
			</product-v0:bundledProductKey>
		</cbe-v0:associationEnds>
	</customer-v0:pMProductAssociation>';

	unset($arrParameters);

	return $returnXML;

}

function glbXMLDataBlock($arrParms = array()) 
{	

	$returnDataBlock = '';
	$arrParms['val'] = trim($arrParms['val']);

	if(!empty($arrParms['val'])) 
	{
		$returnDataBlock .= '
		<'.$arrParms['prenspace'].':'.$arrParms['nspace'].'>
			<'.$arrParms['prenspace'].':characteristicValue>
				<cbe-v0:identifiedBy>
					<cbe-v0:charcValueKey>
						<cbe-v0:type>CharacteristicValue</cbe-v0:type>
						<cbe-v0:objectID>'.$arrParms['cobj'].'</cbe-v0:objectID>
					</cbe-v0:charcValueKey>
				</cbe-v0:identifiedBy>';

				if(!empty($arrParms['createdDate'])) 
				{ 
					$returnDataBlock .= '
					<cbe-v0:createdDate>'.$arrParms['createdDate'].'</cbe-v0:createdDate>';
				}
				
				if(!empty($arrParms['createdUser'])) 
				{ 
					$returnDataBlock .= '
					<cbe-v0:createdUser>'.$arrParms['createdUser'].'</cbe-v0:createdUser>';
				}
				
				$returnDataBlock .= '
				<cbe-v0:chValDescByChSpec>
					<cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
					<cbe-v0:objectID>'.$arrParms['aobj'].'</cbe-v0:objectID>
				</cbe-v0:chValDescByChSpec>
				<'.$arrParms['prenspace'].':'.$arrParms['nspace'].'>'.$arrParms['val'].'</'.$arrParms['prenspace'].':'.$arrParms['nspace'].'>
			</'.$arrParms['prenspace'].':characteristicValue>
		</'.$arrParms['prenspace'].':'.$arrParms['nspace'].'>';

	}

	return $returnDataBlock;

}

function glbXMLContractLocaionBlock($arrParms = array()) 
{
	
	$returnParms = array(); 

	$arrParms['tmp']['iso3166_1numeric'] = trim($arrParms['tmp']['iso3166_1numeric']);
	$returnParms['geo']['anyFlag'] = 0;

	if($arrParms['tmp']['iso3166_1numeric'] == '124') {
		$returnParms['geo']['GeographicAddress'] = 'ed6ee55e-0b23-4fbe-a09f-a1d7f28c5a73';
		$returnParms['geo']['GeoAddressSpecification'] = '8058f09f-0b23-4fb1-825d-8c234addcddf';
		$returnParms['geo']['CountrySpecificAddress'] = '8f37e211-0b23-4fb0-949e-f034858e3c6e';
		$returnParms['geo']['CountrySpecificAddressSpecification'] = 'e580b922-0b23-4fbe-ab6f-0da4d3ecdc5d';
		$returnParms['geo']['PlacePartyRoleAssociation'] = 'a9699cc7-0b23-4fb1-869d-5f032b0e58b6';
		$returnParms['geo']['property'] = 'canadianPropertyAddress';
		$returnParms['geo']['propertyName'] = 'Canadian';
		$returnParms['geo']['languageName'] = 'eng' ;
		$returnParms['geo']['notation'] = 'Any';
		$returnParms['geo']['iso3166_1alpha'] = strtolower($arrParms['tmp']['iso3166_1alpha']);
        
	} elseif($arrParms['tmp']['iso3166_1numeric'] == '392') {
    
		$returnParms['geo']['GeographicAddress'] = '6462fabe-0b23-4fbe-817b-01281f763125';
		$returnParms['geo']['GeoAddressSpecification'] = '8058f09f-0b23-4fb1-825d-8c234addcddf';
		$returnParms['geo']['CountrySpecificAddress'] = '854fb15f-0b23-4fba-9ae0-f12edf3e7565';
		$returnParms['geo']['CountrySpecificAddressSpecification'] = 'cab7b06b-0b23-4fba-9431-e627cbb3fc27';
        $returnParms['geo']['PlacePartyRoleAssociation'] = 'a9699cc7-0b23-4fb1-869d-5f032b0e58b6';
		$returnParms['geo']['property'] = 'japanesePropertyAddress';
		$returnParms['geo']['propertyName'] = 'Japanese';
		$returnParms['geo']['languageName'] = (!empty($arrParms['tmp']['addressLatin'])) ? 'eng' : 'jpn';
		$returnParms['geo']['notation'] = (!empty($arrParms['tmp']['addressLatin'])) ? 'Any' : 'Japanese';
		$returnParms['geo']['iso3166_1alpha'] = strtolower($arrParms['tmp']['iso3166_1alpha']);
	
    } elseif($arrParms['tmp']['iso3166_1numeric'] == '840') {
    
		$returnParms['geo']['GeographicAddress'] = 'e3ef5379-0b23-4fb0-b3c5-49f47424df39';
		$returnParms['geo']['GeoAddressSpecification'] = '8058f09f-0b23-4fb1-825d-8c234addcddf';
		$returnParms['geo']['CountrySpecificAddress'] = '9f1be72e-0b23-4fbb-a4b6-28920471586e';
		$returnParms['geo']['CountrySpecificAddressSpecification'] = '86a3930f-0b23-4fb1-8aa3-a1504410959c';
		$returnParms['geo']['PlacePartyRoleAssociation'] = 'a9699cc7-0b23-4fb1-869d-5f032b0e58b6';
		$returnParms['geo']['property'] = 'americanPropertyAddress';
		$returnParms['geo']['propertyName'] = 'American';
		$returnParms['geo']['languageName'] = 'eng';
		$returnParms['geo']['notation'] = 'Any';
		$returnParms['geo']['iso3166_1alpha'] = strtolower($arrParms['tmp']['iso3166_1alpha']);
	
    } else {
    
		$returnParms['geo']['GeographicAddress'] = 'f5484485-0b23-4fbb-910b-8a8c4d2f20e0';
		$returnParms['geo']['GeoAddressSpecification'] = '8058f09f-0b23-4fb1-825d-8c234addcddf';
		$returnParms['geo']['CountrySpecificAddress'] = '8e222a80-0b23-4fbf-b3b6-6b4e75476381';
		$returnParms['geo']['CountrySpecificAddressSpecification'] = '8e222a80-0b23-4fbf-b3b6-6b4e75476381';
		$returnParms['geo']['PlacePartyRoleAssociation'] = '5972d831-0b23-4fb3-b68b-c568cac4e0d1';
		$returnParms['geo']['cobj'] = '50cf913e-0b23-4fb8-9b79-59277b37be15';
		$returnParms['geo']['aobj'] = '29cdbe66-0b23-4fba-8235-15cad32bb9e0';
		$returnParms['geo']['property'] = 'anyAddress';
		$returnParms['geo']['propertyName'] = 'Any';
		$returnParms['geo']['languageName'] = (!empty($arrParms['tmp']['addressLatin'])) ? 'eng' : 'jpn';
		$returnParms['geo']['notation'] = (!empty($arrParms['tmp']['addressLatin'])) ? 'Any' : 'Japanese';
		$returnParms['geo']['iso3166_1alpha'] = strtolower($arrParms['tmp']['iso3166_1alpha']);
		$returnParms['geo']['anyFlag'] = 1;
	}

	unset($arrParms,$glbArrGeoAddress);

	$GLOBALS["geoContract"] = $returnParms['geo']['GeographicAddress'];
	$GLOBALS["geoContractPlaceParty"] = $returnParms['geo']['PlacePartyRoleAssociation'];

	return $returnParms;

}

function glbXMLBillingLocaionBlock($arrParms = array()) 
{
	
	$returnParms = array(); 	

	$arrParms['tmp']['iso3166_1numeric'] = trim($arrParms['tmp']['iso3166_1numeric']);
	$returnParms['geo']['anyFlag'] = 0;

	if($arrParms['tmp']['iso3166_1numeric'] == '124') {
    
        $returnParms['geo']['GeographicAddress'] = 'bce60a8d-0b23-4fb6-98f2-0eb2f862593e';
		$returnParms['geo']['GeoAddressSpecification'] = '8058f09f-0b23-4fb1-825d-8c234addcddf';
		$returnParms['geo']['CountrySpecificAddress'] = '17346843-0b23-4fb6-af6f-edc52c8e24f7';
		$returnParms['geo']['CountrySpecificAddressSpecification'] = 'ff55f3d0-0b23-4fb5-9a46-9e236aa7b47e';
		$returnParms['geo']['PlacePartyRoleAssociation'] = '9005ba46-0b23-4fbd-883c-be8d7b607008';
		$returnParms['geo']['property'] = 'canadianPropertyAddress';
		$returnParms['geo']['propertyName'] = 'Canadian';
		$returnParms['geo']['languageName'] = 'eng';
		$returnParms['geo']['notation'] = 'Any';
		$returnParms['geo']['iso3166_1alpha'] = strtolower($arrParms['tmp']['iso3166_1alpha']);

	} elseif($arrParms['tmp']['iso3166_1numeric'] == '392') {
    
		$returnParms['geo']['GeographicAddress'] = 'cfd89aac-0b23-4fb8-9e64-7f5ac5bf5647';
		$returnParms['geo']['GeoAddressSpecification'] = '8058f09f-0b23-4fb1-825d-8c234addcddf';
		$returnParms['geo']['CountrySpecificAddress'] = '2729f900-0b23-4fbe-b1d9-c9e8cd777ef8';
		$returnParms['geo']['CountrySpecificAddressSpecification'] = 'cab7b06b-0b23-4fba-9431-e627cbb3fc27';
		$returnParms['geo']['PlacePartyRoleAssociation'] = '9005ba46-0b23-4fbd-883c-be8d7b607008';
		$returnParms['geo']['property'] = 'japanesePropertyAddress';
		$returnParms['geo']['propertyName'] = 'Japanese';
		$returnParms['geo']['languageName'] = (!empty($arrParms['tmp']['addressLatin'])) ? 'eng' : 'jpn';
		$returnParms['geo']['notation'] = (!empty($arrParms['tmp']['addressLatin'])) ? 'Any' : 'Japanese';
		$returnParms['geo']['iso3166_1alpha'] = strtolower($arrParms['tmp']['iso3166_1alpha']);
        
	} elseif($arrParms['tmp']['iso3166_1numeric'] == '840') {
		$returnParms['geo']['GeographicAddress'] = '4db67c35-0b23-4fb3-8f52-8cad913105f9';
		$returnParms['geo']['GeoAddressSpecification'] = '8058f09f-0b23-4fb1-825d-8c234addcddf';
		$returnParms['geo']['CountrySpecificAddress'] = 'd3742a46-0b23-4fb5-90de-93d49aca0ea7';
		$returnParms['geo']['CountrySpecificAddressSpecification'] = 'e7bd5a6e-0b23-4fbd-be62-ddd5b2e6aee7';
		$returnParms['geo']['PlacePartyRoleAssociation'] = '9005ba46-0b23-4fbd-883c-be8d7b607008';
		$returnParms['geo']['property'] = 'americanPropertyAddress';
		$returnParms['geo']['propertyName'] = 'American';
		$returnParms['geo']['languageName'] = 'eng';
		$returnParms['geo']['notation'] = 'Any';
		$returnParms['geo']['iso3166_1alpha'] = strtolower($arrParms['tmp']['iso3166_1alpha']);
	
    } else {
    
		$returnParms['geo']['GeographicAddress'] = 'dd4261c5-0b23-4fbe-932c-d8c5f048bddd';
		$returnParms['geo']['GeoAddressSpecification'] = '8058f09f-0b23-4fb1-825d-8c234addcddf';
		$returnParms['geo']['CountrySpecificAddress'] = '05a83d0b-0b23-4fb2-a726-3d7be427518c';
		$returnParms['geo']['CountrySpecificAddressSpecification'] = 'a00b616b-0b23-4fbf-a9dd-86f4752e980f';
		$returnParms['geo']['PlacePartyRoleAssociation'] = '3f058d08-0b23-4fb9-a140-e76fb0f0fa22';
		$returnParms['geo']['cobj'] = '5f1cf666-0b23-4fb6-9dc7-49d15f733eb5';
		$returnParms['geo']['aobj'] = '29cdbe66-0b23-4fba-8235-15cad32bb9e0';
		$returnParms['geo']['property'] = 'anyAddress';
		$returnParms['geo']['propertyName'] = 'Any';
		$returnParms['geo']['languageName'] = (!empty($arrParms['tmp']['addressLatin'])) ? 'eng' : 'jpn';
		$returnParms['geo']['notation'] = (!empty($arrParms['tmp']['addressLatin'])) ? 'Any' : 'Japanese';
		$returnParms['geo']['iso3166_1alpha'] = strtolower($arrParms['tmp']['iso3166_1alpha']);
		$returnParms['geo']['anyFlag'] = 1;
	}	

	unset($arrParms);

	$GLOBALS["geoBilling"] = $returnParms['geo']['GeographicAddress'];
	$GLOBALS["geoBillingPlaceParty"] = $returnParms['geo']['PlacePartyRoleAssociation'];

	return $returnParms;
}

?>
