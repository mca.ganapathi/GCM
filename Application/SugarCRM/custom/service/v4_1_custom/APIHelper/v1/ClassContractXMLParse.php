<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (�MSA�), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
 /**
 * @type unix-file system UTF-8
 */
require_once ('custom/include/XMLUtils.php');

class ClassContractXMLParse extends XMLUtils
{

    var $check_required_field = array();

    function getContractParseArray($contract_xml_data)
    {

        $xml = simplexml_load_string($contract_xml_data);
        // Fetch common_customer_id_c start
        $common_customer_id_c = $this->getValueByTag($xml, '//ptyIdent001:', 'corporationID');
        // Required field validation of Corporate ID
        $this->fnCheckRequiredFieldValidation($common_customer_id_c, '2018');

        // Begin: Get Corporate details by CID
        if (! empty($common_customer_id_c)) {
            // Check corporate record in DB by cid
            require_once ('modules/Accounts/Account.php');
            $obj_corporates_bean = new Account();
            $obj_corporates_bean->retrieve_by_string_fields(array(
                'common_customer_id_c' => $common_customer_id_c
            ));
            if (empty($obj_corporates_bean->id)) {
                // Check cid exists in CIDAS API
                require_once ('custom/service/v4_1_custom/APIHelper/ClassCIDASConnection.php');
                $cidas_connection_obj = new ClassCIDASConnection();
                $cidas_record_by_id = $cidas_connection_obj->getCorporateDetailsFromCIDASByCID($common_customer_id_c, false);
                $error_occured = mb_strpos($cidas_record_by_id, 'exp-v0:exception');
                if ($error_occured) {
                    return $cidas_record_by_id;
                } else {
                    $arr_corporate_details_by_id = $cidas_record_by_id;
                }
            } else {
                // Populate corporate record from db into array
                $arr_corporate_details_by_id = array(
                    'common_customer_id_c' => $obj_corporates_bean->common_customer_id_c,
                    'name' => $obj_corporates_bean->name,
                    'name_2_c' => $obj_corporates_bean->name_2_c,
                    'name_3_c' => $obj_corporates_bean->name_3_c,
                    'country_code_c' => $obj_corporates_bean->country_code_c,
                    'country_name_c' => $obj_corporates_bean->country_name_c,
                    'customer_status_code_c' => $obj_corporates_bean->customer_status_code_c,
                    'hq_addr_1_c' => $obj_corporates_bean->hq_addr_1_c,
                    'hq_addr_2_c' => $obj_corporates_bean->hq_addr_2_c,
                    'hq_postal_no_c' => $obj_corporates_bean->hq_postal_no_c,
                    'reg_addr_1_c' => $obj_corporates_bean->reg_addr_1_c,
                    'reg_addr_2_c' => $obj_corporates_bean->reg_addr_2_c,
                    'reg_postal_no_c' => $obj_corporates_bean->reg_postal_no_c,
                    'cidas_data' => 'No',
                    'cidas_search' => 'No'
                );
            }
        }
        // End: Get Corporate details by CID

        // Fetch billing corporate name
        $billing_corporate_name = $this->getValueByTag($xml, '//billingCustomerPR:', 'corporationName');
        // Fetch billing corporate name furigana
        $billing_corporation_name_furigana = $this->getValueByTag($xml, '//billingCustomerPR:', 'corporationNameFurigana');
        // Fetch billing corporate name latin
        $billing_corporation_name_latin = $this->getValueByTag($xml, '//billingCustomerPR:', 'corporationNameLatin');

        $arr_corporate_details_by_name = array(
            'common_customer_id_c' => '',
            'name' => $billing_corporation_name_latin,
            'name_2_c' => $billing_corporate_name,
            'name_3_c' => $billing_corporation_name_furigana,
            'country_code_c' => '',
            'country_name_c' => '',
            'customer_status_code_c' => '',
            'hq_addr_1_c' => '',
            'hq_addr_2_c' => '',
            'hq_postal_no_c' => '',
            'reg_addr_1_c' => '',
            'reg_addr_2_c' => '',
            'reg_postal_no_c' => '',
            'cidas_data' => 'No',
            'cidas_search' => 'No'
        );

        // Required field validation of Billing Corporate Name
        $this->fnCheckRequiredFieldValidation($billing_corporate_name, '2019');
        // Required field validation of Billing Corporate Name
        $this->fnCheckFieldLenghtValidation($billing_corporate_name, '2030', '0', '20');
        // Fetch product_id start
        foreach ($xml->xpath('//product-v0:referenceToAtomicProductOffering') as $item) {
            $row = simplexml_load_string($item->asXML());
            $product_id = (string) $row->{'cbe-v0:objectID'};
        }
        // Required field validation of product_id
        $this->fnCheckRequiredFieldValidation($product_id, '2011');
        // Fetch contract_request_id, ext_sys_created_by, Contract name
        foreach ($xml->xpath('//bi-v0:productContract') as $item) {
            $row = simplexml_load_string($item->asXML());
            $contract_request_id = (string) $row->{'cbe-v0:clientRequestID'};
            $ext_sys_created_by = (string) $row->{'cbe-v0:createdUser'};
            $contract_name = (string) $row->{'cbe-v0:name'};
        }
        // Required field validation of Contract Name
        $this->fnCheckRequiredFieldValidation($contract_name, '2012');
        // Required field validation of createdUser
        $this->fnCheckRequiredFieldValidation($ext_sys_created_by, '2013');
        // Fetch sales_channel_code
        $sales_channel_code = $this->getValueByTag($xml, '//salesChannelPR:', 'salesChannelCode');
        // Fetch c_country_id_c, postalcode, addr1, addr2, postal_no_c
        foreach ($xml->xpath('//location-v0:placePartyRoleAssociation') as $item) {
            $row = simplexml_load_string($item->asXML());
            $loc_arr_1[(string) $row->{'cbe-v0:associationEnds'}[0]->{'location-v0:geoAddressKey'}->{'cbe-v0:validIDOnlyInMessage'}] = (string) $row->{'cbe-v0:associationEnds'}[1]->{'party-v0:partyRoleKey'}->{'cbe-v0:validIDOnlyInMessage'};
        }
        ksort($loc_arr_1);
        foreach ($xml->xpath('//location-v0:geographicAddress') as $item) {
            $row = simplexml_load_string($item->asXML());
            $geoAddresskey_value = (string) $row->{'cbe-v0:identifiedBy'}->{'location-v0:geoAddressKey'}->{'cbe-v0:validIDOnlyInMessage'};

            $loc_arr_2[$geoAddresskey_value]['country_code'] = (string) $row->{'location-v0:countryKey'}->{'location-v0:iso3166_1numeric'};
            $loc_arr_2[$geoAddresskey_value]['notation'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'location-v0:languageNotation'}->{'datatypes-v0:notation'};

            if ($this->checkLocation($loc_arr_2[$geoAddresskey_value]['country_code'])) {
                // Populate address details for japan
                if ($loc_arr_2[$geoAddresskey_value]['country_code'] == '392') {
                    $loc_arr_2[$geoAddresskey_value]['postalcode'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'jpnAddr:japanesePropertyAddress'}->{'jpnAddr:postalCode'}->{'jpnAddr:characteristicValue'}->{'jpnAddr:postalCode'};
                    $loc_arr_2[$geoAddresskey_value]['js_address'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'jpnAddr:japanesePropertyAddress'}->{'jpnAddr:address'}->{'jpnAddr:characteristicValue'}->{'jpnAddr:address'};
                    $loc_arr_2[$geoAddresskey_value]['js_address_line'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'jpnAddr:japanesePropertyAddress'}->{'jpnAddr:addressLine'}->{'jpnAddr:characteristicValue'}->{'jpnAddr:addressLine'};
                }
                // Populate address details for USA
                if ($loc_arr_2[$geoAddresskey_value]['country_code'] == '840') {
                    $loc_arr_2[$geoAddresskey_value]['postalcode'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'usaAddr:americanPropertyAddress'}->{'usaAddr:zipCode'}->{'usaAddr:characteristicValue'}->{'usaAddr:zipCode'};
                    $loc_arr_2[$geoAddresskey_value]['general_address_line'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'usaAddr:americanPropertyAddress'}->{'usaAddr:addressLine'}->{'usaAddr:characteristicValue'}->{'usaAddr:addressLine'};
                    $loc_arr_2[$geoAddresskey_value]['general_city'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'usaAddr:americanPropertyAddress'}->{'usaAddr:city'}->{'usaAddr:characteristicValue'}->{'usaAddr:city'};
                    $loc_arr_2[$geoAddresskey_value]['general_state'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'usaAddr:americanPropertyAddress'}->{'usaAddr:stateAbbr'}->{'usaAddr:characteristicValue'}->{'usaAddr:stateAbbr'};
                }
                // Populate address details for Canada
                if ($loc_arr_2[$geoAddresskey_value]['country_code'] == '124') {
                    $loc_arr_2[$geoAddresskey_value]['postalcode'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'canAddr:canadianPropertyAddress'}->{'canAddr:postalCode'}->{'canAddr:characteristicValue'}->{'canAddr:postalCode'};
                    $loc_arr_2[$geoAddresskey_value]['general_address_line'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'canAddr:canadianPropertyAddress'}->{'canAddr:addressLine'}->{'canAddr:characteristicValue'}->{'canAddr:addressLine'};
                    $loc_arr_2[$geoAddresskey_value]['general_city'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'canAddr:canadianPropertyAddress'}->{'canAddr:city'}->{'canAddr:characteristicValue'}->{'canAddr:city'};
                    $loc_arr_2[$geoAddresskey_value]['general_state'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'canAddr:canadianPropertyAddress'}->{'canAddr:stateAbbr'}->{'canAddr:characteristicValue'}->{'canAddr:stateAbbr'};
                }

            } else {
                if ($loc_arr_2[$geoAddresskey_value]['notation'] == 'Any') {
                    $loc_arr_2[$geoAddresskey_value]['any_local_address'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'anyAddr:anyAddress'}->{'anyAddr:address'}->{'anyAddr:characteristicValue'}->{'anyAddr:address'};
                }

                if ($loc_arr_2[$geoAddresskey_value]['notation'] == 'Latin') {
                    $loc_arr_2[$geoAddresskey_value]['any_latin_address'] = (string) $row->{'location-v0:usedAddressRepresentation'}->{'location-v0:countrySpecificAddress'}->{'anyAddr:anyAddress'}->{'anyAddr:address'}->{'anyAddr:characteristicValue'}->{'anyAddr:address'};
                }
            }
        }
        ksort($loc_arr_2);

        function geoAddr($n, $m)
        {
            return (array(
                $n => $m
            ));
        }
        $location_data = array_map("geoAddr", $loc_arr_1, $loc_arr_2);
        $location_data_item = call_user_func_array('array_merge', $location_data);

        if ($location_data_item['billingCustomerRole']) {
            $this->fnCheckRequiredFieldValidation($location_data_item['billingCustomerRole']['country_code'], '2022');
        }

        if ($location_data_item['contractCustomerRole']) {
            $this->fnCheckRequiredFieldValidation($location_data_item['contractCustomerRole']['country_code'], '2032');
        }

        if ($location_data_item['billingCustomerRole']['country_code'] == '392') {
            $this->fnCheckRequiredFieldValidation($billing_corporation_name_furigana, '2033');
            $this->fnCheckRequiredFieldValidation($location_data_item['billingCustomerRole']['postalcode'], '2023');
            $this->fnCheckRequiredFieldValidation($location_data_item['billingCustomerRole']['js_address'], '2024');
            $this->fnCheckRequiredFieldValidation($location_data_item['billingCustomerRole']['js_address_line'], '2025');
        }

        if (! empty($billing_corporation_name_furigana)) {
            $this->fnCheckFieldLenghtValidation($billing_corporation_name_furigana, '2031', '1', '35');
        }

        // Fetch email1,phone_work for sales,billing and contract
        foreach ($xml->xpath('//party-v0:partyRole') as $item) {
            $row = simplexml_load_string($item->asXML());
            $party_role_key_value = (string) $row->{'cbe-v0:identifiedBy'}->{'party-v0:partyRoleKey'}->{'cbe-v0:validIDOnlyInMessage'};
            if ($party_role_key_value == 'pICSalesRole' || 'pICContractRole' || 'pICBillingRole') {
                if (! empty($row->{'party-v0:contactableVia'}->{'party-v0:contactMediumKey'}->{'cbe-v0:validIDOnlyInMessage'})) {
                    foreach ($xml->xpath('//party-v0:contactMedium') as $item1) {
                        $row1 = simplexml_load_string($item1->asXML());
                        if ((string) $row->{'party-v0:contactableVia'}->{'party-v0:contactMediumKey'}->{'cbe-v0:validIDOnlyInMessage'} == (string) $row1->{'cbe-v0:identifiedBy'}->{'party-v0:contactMediumKey'}->{'cbe-v0:validIDOnlyInMessage'}) {
                            if (! empty($row1->{'contactMedium:contactMedium'}->{'contactMedium:telephoneNumber'}->{'contactMedium:characteristicValue'}->{'contactMedium:telephoneNumber'})) {
                                $contact_details[$party_role_key_value]['telephoneNumber'] = (string) $row1->{'contactMedium:contactMedium'}->{'contactMedium:telephoneNumber'}->{'contactMedium:characteristicValue'}->{'contactMedium:telephoneNumber'};
                                $contact_details[$party_role_key_value]['eMailAddress'] = (string) $row1->{'contactMedium:contactMedium'}->{'contactMedium:eMailAddress'}->{'contactMedium:characteristicValue'}->{'contactMedium:eMailAddress'};
                            }
                        }
                    }
                }
            }
        }

        // Fetch service_start_date, service_end_date, billing_start_date, billing_end_date
        $service_start_date = $this->fnGetDateTimeZone($this->getValueByTag($xml, '//prodCntrctItem0001:', 'serviceStartDate'));
        $service_end_date = $this->fnGetDateTimeZone($this->getValueByTag($xml, '//prodCntrctItem0001:', 'serviceEndDate'));
        $billing_start_date = $this->fnGetDateTimeZone($this->getValueByTag($xml, '//prodCntrctItem0001:', 'billingStartDate'));
        $billing_end_date = $this->fnGetDateTimeZone($this->getValueByTag($xml, '//prodCntrctItem0001:', 'billingEndDate'));

        $this->fnCheckRequiredFieldValidation($service_start_date, '2016');
        $this->fnCheckRequiredFieldValidation($service_end_date, '2017');
        $this->fnCheckRequiredFieldValidation($billing_start_date, '2014');
        $this->fnCheckRequiredFieldValidation($billing_end_date, '2015');
        // Fetch division_2_c for Sales, Billing and Contract
        $billing_division_2_c = $this->getValueByTag($xml, '//pICBillingPR:', 'divisionName');
        $sales_division_2_c = $this->getValueByTag($xml, '//pICSalesPR:', 'divisionName');
        $contract_division_2_c = $this->getValueByTag($xml, '//pICContractPR:', 'divisionName');
        $sales_rep_name_1 = $this->getValueByTag($xml, '//pICSalesPR:', 'individualName');
        // Fetch name_2_c for Billing and Contract
        $billing_rep_name_2_c = $this->getValueByTag($xml, '//pICBillingPR:', 'individualName');
        $contract_rep_name_2_c = $this->getValueByTag($xml, '//pICContractPR:', 'individualName');

        if (empty($this->check_required_field)) {
            $request_data = array(
                'Corporates' => $arr_corporate_details_by_id,
                'Accounts' => array(
                    'Billing' => array(
                        'contact_type_c' => 'Billing',
                        'last_name' => '',
                        'name_2_c' => $billing_rep_name_2_c,
                        'postal_no_c' => $location_data_item['billingCustomerRole']['postalcode'],
                        'addr_1_c' => $location_data_item['billingCustomerRole']['any_latin_address'],
                        'addr_2_c' => $location_data_item['billingCustomerRole']['any_local_address'],
                        'division_1_c' => '',
                        'division_2_c' => $billing_division_2_c,
                        'title' => '',
                        'title_2_c' => '',
                        'phone_work' => $contact_details['pICBillingRole']['telephoneNumber'],
                        'ext_no_c' => '',
                        'phone_fax' => '',
                        'phone_mobile' => '',
                        'email1' => $contact_details['pICBillingRole']['eMailAddress'],
                        'country_code_numeric' => $location_data_item['billingCustomerRole']['country_code'],
                        'Accounts_js' => array(
                            'addr1' => $location_data_item['billingCustomerRole']['js_address'],
                            'addr2' => $location_data_item['billingCustomerRole']['js_address_line']
                        ),
                        'general_address' => array(
                            'addr_general_c' => $location_data_item['billingCustomerRole']['general_address_line'],
                            'city_general_c' => $location_data_item['billingCustomerRole']['general_city'],
                            'state_general_code' => $location_data_item['billingCustomerRole']['general_state']
                        ),
                        'CorporateDetails' => $arr_corporate_details_by_name
                    ),
                    'Technology' => array(
                        'contact_type_c' => 'Technology',
                        'last_name' => '',
                        'name_2_c' => '',
                        'postal_no_c' => '',
                        'addr_1_c' => '',
                        'addr_2_c' => '',
                        'division_1_c' => '',
                        'division_2_c' => '',
                        'title' => '',
                        'title_2_c' => '',
                        'phone_work' => '',
                        'ext_no_c' => '',
                        'phone_fax' => '',
                        'phone_mobile' => '',
                        'email1' => '',
                        'country_code_numeric' => '',
                        'Accounts_js' => array(
                            'addr1' => '',
                            'addr2' => ''
                        )
                    ),
                    'Contract' => array(
                        'contact_type_c' => 'Contract',
                        'last_name' => '',
                        'name_2_c' => $contract_rep_name_2_c,
                        'postal_no_c' => $location_data_item['contractCustomerRole']['postalcode'],
                        'addr_1_c' => $location_data_item['contractCustomerRole']['any_latin_address'],
                        'addr_2_c' => $location_data_item['contractCustomerRole']['any_local_address'],
                        'division_1_c' => '',
                        'division_2_c' => $contract_division_2_c,
                        'title' => '',
                        'title_2_c' => '',
                        'phone_work' => $contact_details['pICContractRole']['telephoneNumber'],
                        'ext_no_c' => '',
                        'phone_fax' => '',
                        'phone_mobile' => '',
                        'email1' => $contact_details['pICContractRole']['eMailAddress'],
                        'country_code_numeric' => $location_data_item['contractCustomerRole']['country_code'],
                        'Accounts_js' => array(
                            'addr1' => $location_data_item['contractCustomerRole']['js_address'],
                            'addr2' => $location_data_item['contractCustomerRole']['js_address_line']
                        ),
                        'general_address' => array(
                            'addr_general_c' => $location_data_item['contractCustomerRole']['general_address_line'],
                            'city_general_c' => $location_data_item['contractCustomerRole']['general_city'],
                            'state_general_code' => $location_data_item['contractCustomerRole']['general_state']
                        ),
                        'CorporateDetails' => $arr_corporate_details_by_id
                    )
                ),
                'ContractDetails' => array(
                    'ContractsHeader' => array(
                        'name' => $contract_name,
                        'description' => '',
                        'contract_type' => '',
                        'contract_version' => '1.0',
                        'contract_request_id' => $contract_request_id,
                        'contract_status' => '1',
                        'loi_contract' => 'No',
                        'billing_type' => '',
                        'ext_sys_created_by' => $ext_sys_created_by,
                        'ext_sys_modified_by' => '',
                        'total_nrc' => '0.00',
                        'total_mrc' => '0.00',
                        'grand_total' => '0.00'
                    ),
                    'ContractSalesRepresentative' => array(
                        0 => array(
                            'sales_channel_code' => $sales_channel_code,
                            'name' => '',
                            'sales_rep_name_1' => $sales_rep_name_1,
                            'division_1' => '',
                            'division_2' => $sales_division_2_c,
                            'title_1' => '',
                            'title_2' => '',
                            'tel_no' => $contact_details['pICSalesRole']['telephoneNumber'],
                            'ext_no' => '',
                            'fax_no' => '',
                            'mob_no' => '',
                            'email1' => $contact_details['pICSalesRole']['eMailAddress']
                        )
                    ),
                    'ContractLineItemDetails' => array(
                        0 => array(
                            'ContractLineItemHeader' => array(
                                'service_start_date' => $service_start_date['date_time'],
                                'service_start_date_timezone' => $service_start_date['time_zone'],
                                'service_end_date' => $service_end_date['date_time'],
                                'service_end_date_timezone' => $service_end_date['time_zone'],
                                'billing_start_date' => $billing_start_date['date_time'],
                                'billing_start_date_timezone' => $billing_start_date['time_zone'],
                                'billing_end_date' => $billing_end_date['date_time'],
                                'billing_end_date_timezone' => $billing_end_date['time_zone'],
                                'payment_type' => '00'
                            ),
                            'ContractLineItems' => array(
                                'product_id' => $product_id
                            ),
                            'ContractDirectDeposit' => array(
                                'bank_code' => '',
                                'branch_code' => '',
                                'deposit_type' => '',
                                'account_no' => '',
                                'payee_contact_person_name_1' => '',
                                'payee_contact_person_name_2' => '',
                                'payee_tel_no' => '',
                                'payee_email_address' => '',
                                'remarks' => ''
                            )
                        )
                    )
                )
            );

            // Set VAT Number in contract header level in xml parse array
            $request_data['ContractDetails']['ContractsHeader']['vat_no'] = $this->getValueByTag($xml, '//ptyIdent003:', 'vatNo');

        } else {
            $request_data = $this->fnGetErrorXMLData($this->check_required_field, null, false);
        }
        return $request_data;
    }

    function fnCheckRequiredFieldValidation($field_name, $error_code)
    {
        if (empty($field_name)) {
            $this->check_required_field[] = $error_code;
        }
    }

    function fnCheckFieldLenghtValidation($field_name, $error_code, $min_length, $max_length)
    {
        if (mb_strlen($field_name, 'UTF-8') < $min_length || mb_strlen($field_name, 'UTF-8') > $max_length) {
            $this->check_required_field[] = $error_code;
        }
    }

}
?>
