<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');
/**
 * @type unix-file system UTF-8
 */
class ClassContractPostResponse_v1
{

    public function getContractPostResponseXML($xml_request_file, $data)
    {

        $site_url = rtrim($GLOBALS['sugar_config']['site_url'], '/');

        $xml_schema_location = $site_url . '/custom/XMLSchema';
        $from = $xml_request_file;

        $find = array(
            'req-v0:request',
            'ContractInformationRequest-v0_1.xsd',
            'xmlns:req-v0'
        );
        $replace = array(
            'res-v0:response',
            'ContractInformationResponse-v0_1.xsd',
            'xmlns:res-v0'
        );
        $to = str_replace($find, $replace, $from);
        $xml_res = new DOMDocument();
        $xml_res->preserveWhiteSpace = false;
        $xml_res->formatOutput = true;
        // $xml_res->loadXML($xml_request_file);
        $xml_res->loadXML($to);
        $xpath = new DOMXPath($xml_res);
        // register namespaces
        $xpath->registerNamespace("res-v0", "$xml_schema_location/contract-information");
        $xpath->registerNamespace("product-v0", "$xml_schema_location/product");
        $xpath->registerNamespace("bi-v0", "$xml_schema_location/business-interaction");
        $xpath->registerNamespace("prodCntrctItem0001", "$xml_schema_location/business-interaction/product-contract-item0001");
        $xpath->registerNamespace("party-v0", "$xml_schema_location/party");
        $xpath->registerNamespace("ptyIdent001", "$xml_schema_location/party/corporate-identification");
        $xpath->registerNamespace("pICContractPR", "$xml_schema_location/party/pic-contract-pseudo-role");
        $xpath->registerNamespace("contactMedium", "$xml_schema_location/party/standard-contact-medium");
        $xpath->registerNamespace("location-v0", "$xml_schema_location/location");
        $xpath->registerNamespace("cbe-v0", "$xml_schema_location/common-business-entity");
        $xpath->registerNamespace("customer-v0", "$xml_schema_location/customer");
        $tag_nodes = array(
            '//res-v0:response/product-v0:bundledProduct' => array(
                'createdUser',
                'name',
                'validFor',
                'describedBy'
            ),
            '//res-v0:response/product-v0:bundledProduct/product-v0:comprisedOf' => array(
                'createdUser',
                'name',
                'validFor',
                'describedBy'
            ),
            '//res-v0:response//bi-v0:productContract' => array(
                'createdUser',
                'clientRequestID',
                'name',
                'describedBy',
                'involvedRoles',
                'agreementPeriod',
                'contractFor'
            ),
            '//res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems' => array(
                'createdUser',
                'name',
                'describedBy'
            ),
            '//res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem' => array(
                'action',
                'quantity',
                'involvesProduct'
            ),
            '//res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:billingPeriod/prodCntrctItem0001:billingStartDate' => array(
                'createdUser',
                'chValDescByChSpec',
                'billingStartDate'
            ),
            '//res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:billingPeriod/prodCntrctItem0001:billingEndDate' => array(
                'createdUser',
                'chValDescByChSpec',
                'billingEndDate'
            ),
            '//res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:servicePeriod/prodCntrctItem0001:serviceStartDate' => array(
                'createdUser',
                'chValDescByChSpec',
                'serviceStartDate'
            ),
            '//res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:servicePeriod/prodCntrctItem0001:serviceEndDate' => array(
                'createdUser',
                'chValDescByChSpec',
                'serviceEndDate'
            ),
            '//res-v0:response/bi-v0:partyInteractionRole' => array(
                'createdUser',
                'name',
                'nameLatin',
                'describedBy',
                'partyRoleKey'
            ),
            '//res-v0:response/party-v0:party' => array(
                'createdUser',
                'describedBy'
            ),
            '//res-v0:response/party-v0:corporateIdentification' => array(
                'createdUser',
                'name',
                'nameLatin',
                'describedBy',
                'appliedTo'
            ),
            '//res-v0:response/party-v0:corporateIdentification/ptyIdent001:corporateIdentification/ptyIdent001:corporationID' => array(
                'createdUser',
                'chValDescByChSpec',
                'corporationID'
            ),
            '//res-v0:response/party-v0:partyRole' => array(
                'createdUser',
                'name',
                'nameLatin',
                'describedBy',
                'appliedTo',
                'contactableVia'
            ),
            '//res-v0:response/party-v0:partyRole/billingCustomerPR:billingCustomerPseudoRole/billingCustomerPR:corporationName' => array(
                'createdUser',
                'chValDescByChSpec',
                'corporationName'
            ),
            '//res-v0:response/party-v0:partyRole/billingCustomerPR:billingCustomerPseudoRole/billingCustomerPR:corporationNameFurigana' => array(
                'chValDescByChSpec',
                'corporationNameFurigana'
            ),
            '//res-v0:response/party-v0:partyRole/salesChannelPR:salesChannelPseudoRole' => array(
                'chValDescByChSpec'
            ),
            '//res-v0:response/party-v0:partyRole/salesChannelPR:salesChannelPseudoRole/salesChannelPR:salesChannelCode/salesChannelPR:characteristicValue' => array(
                'salesChannelCode'
            ),
            '//res-v0:response/party-v0:partyRole/pICSalesPR:pICSalesPR/pICSalesPR:divisionName/pICSalesPR:divisionName' => array(
                'chValDescByChSpec',
                'type',
                'divisionName'
            ),
            '//res-v0:response/party-v0:partyRole/pICSalesPR:pICSalesPR/pICSalesPR:individualName/pICSalesPR:individualName/pICSalesPR:characteristicValue' => array(
                'createdUser',
                'chValDescByChSpec',
                'individualName'
            ),
            '//res-v0:response/party-v0:partyRole/pICBillingPR:pICBillingPR/pICBillingPR:divisionName/pICBillingPR:divisionName' => array(
                'chValDescByChSpec',
                'type',
                'divisionName'
            ),
            '//res-v0:response/party-v0:partyRole/pICBillingPR:pICBillingPR/pICBillingPR:individualName/pICBillingPR:individualName/pICBillingPR:characteristicValue' => array(
                'createdUser',
                'chValDescByChSpec',
                'individualName'
            ),
            '//res-v0:response/party-v0:partyRole/pICContractPR:pICContractPR/pICContractPR:divisionName/pICContractPR:divisionName/pICContractPR:characteristicValue' => array(
                'createdUser',
                'chValDescByChSpec',
                'divisionName'
            ),
            '//res-v0:response/party-v0:partyRole/pICContractPR:pICContractPR/pICContractPR:individualName/pICContractPR:individualName/pICContractPR:characteristicValue' => array(
                'createdUser',
                'chValDescByChSpec',
                'individualName'
            ),
            '//res-v0:response/party-v0:contactMedium' => array(
                'createdUser',
                'name',
                'nameLatin',
                'describedBy'
            ),
            '//res-v0:response/party-v0:contactMedium/contactMedium:contactMedium/contactMedium:telephoneNumber/contactMedium:characteristicValue' => array(
                'createdUser',
                'chValDescByChSpec',
                'telephoneNumber'
            ),
            '//res-v0:response/party-v0:contactMedium/contactMedium:contactMedium/contactMedium:eMailAddress/contactMedium:characteristicValue' => array(
                'createdUser',
                'chValDescByChSpec',
                'eMailAddress'
            ),
            '//res-v0:response/location-v0:geographicAddress' => array(
                'createdUser',
                'describedBy',
                'countryKey'
            ),
            '//res-v0:response/location-v0:geographicAddress/location-v0:usedAddressRepresentation' => array(
                'createdUser',
                'describedBy'
            ),
            '//res-v0:response/location-v0:geographicAddress/location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress' => array(
                'createdUser',
                'describedBy',
                'subordinateTo',
                'languageNotation',
                'isDefault'
            ),
            '//res-v0:response/location-v0:geographicAddress/location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/jpnAddr:japanesePropertyAddress/jpnAddr:postalCode/jpnAddr:characteristicValue' => array(
                'createdUser',
                'chValDescByChSpec',
                'postalCode'
            ),
            '//res-v0:response/location-v0:geographicAddress/location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/jpnAddr:japanesePropertyAddress/jpnAddr:address/jpnAddr:characteristicValue' => array(
                'createdUser',
                'chValDescByChSpec',
                'address'
            ),
            '//res-v0:response/location-v0:geographicAddress/location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/jpnAddr:japanesePropertyAddress/jpnAddr:addressLine/jpnAddr:characteristicValue' => array(
                'createdUser',
                'chValDescByChSpec',
                'addressLine'
            ),
            '//res-v0:response/location-v0:placePartyRoleAssociation' => array(
                'createdUser',
                'name',
                'nameLatin',
                'describedBy',
                'associationEnds'
            ),
            '//res-v0:response/customer-v0:paymentMethod' => array(
                'createdUser',
                'name',
                'nameLatin',
                'describedBy'
            ),
            '//res-v0:response/customer-v0:pMPartyRoleAssociation' => array(
                'createdUser',
                'name',
                'nameLatin',
                'describedBy',
                'associationEnds'
            ),
            '//res-v0:response/customer-v0:pMProductAssociation' => array(
                'createdUser',
                'name',
                'nameLatin',
                'describedBy',
                'associationEnds'
            )
        );
        foreach ($tag_nodes as $xml_query => $tag_meta) {
            $result = $xpath->query($xml_query);
            foreach ($result as $nodes) {
                foreach ($tag_meta as $key => $remove) {
                    $metas = $nodes->getElementsByTagName($remove); // ->item(0);
                    for ($count = 0; $count < $metas->length; $count ++) {
                        $tn[] = $metas->item($count);
                    }
                }
                foreach ($tn as $n) {
                    if (strlen($n->nodeValue))
                        $this->deleteNode($n);
                    else $this->deleteChildren($n);
                }
            }
        }
        $AppendInfo = array(
            '//res-v0:response/product-v0:bundledProduct/cbe-v0:identifiedBy/product-v0:bundledProductKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/product-v0:bundledProduct/product-v0:comprisedOf/product-v0:atomicProduct/cbe-v0:identifiedBy/product-v0:atomicProductKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/product-v0:bundledProduct/product-v0:comprisedOf/product-v0:atomicProduct/cbe-v0:identifiedBy/product-v0:atomicProductKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/bi-v0:productContract' => array(
                'tagName' => 'bi-v0:contractID',
                'value' => $data['ContractHeader']['GlobalContractId']
            ),
            '//res-v0:response/bi-v0:productContract/cbe-v0:identifiedBy/bi-v0:productContractKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => $data['ContractHeader']['ContractObjectId']
            ),
            '//res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/cbe-v0:identifiedBy/bi-v0:productItemKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => $data['ContractLineItemDetails'][0]['ContractLineItems']['LineItemObjectId']
            ),
            '//res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:billingPeriod/prodCntrctItem0001:billingStartDate/prodCntrctItem0001:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:billingPeriod/prodCntrctItem0001:billingEndDate/prodCntrctItem0001:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:servicePeriod/prodCntrctItem0001:serviceStartDate/prodCntrctItem0001:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:servicePeriod/prodCntrctItem0001:serviceEndDate/prodCntrctItem0001:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/bi-v0:partyInteractionRole/cbe-v0:identifiedBy/bi-v0:partyInteractionRoleKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:party/cbe-v0:identifiedBy/party-v0:partyKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '///res-v0:response/party-v0:corporateIdentification/cbe-v0:identifiedBy/party-v0:corporateIdentificationKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:corporateIdentification/ptyIdent001:corporateIdentification/ptyIdent001:corporationID/ptyIdent001:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:partyRole/cbe-v0:identifiedBy/party-v0:partyRoleKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:partyRole/pICContractPR:pICContractPR/pICContractPR:divisionName/pICContractPR:divisionName/pICContractPR:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:partyRole/pICContractPR:pICContractPR/pICContractPR:individualName/pICContractPR:individualName/pICContractPR:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:partyRole/billingCustomerPR:billingCustomerPseudoRole/billingCustomerPR:corporationName/billingCustomerPR:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:partyRole/billingCustomerPR:billingCustomerPseudoRole/billingCustomerPR:corporationNameFurigana/billingCustomerPR:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:partyRole/pICBillingPR:pICBillingPR/pICBillingPR:divisionName/pICBillingPR:divisionName/pICBillingPR:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:partyRole/pICBillingPR:pICBillingPR/pICBillingPR:individualName/pICBillingPR:individualName/pICBillingPR:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:partyRole/salesChannelPR:salesChannelPseudoRole/salesChannelPR:salesChannelCode/salesChannelPR:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:partyRole/pICSalesPR:pICSalesPR/pICSalesPR:divisionName/pICSalesPR:divisionName/pICSalesPR:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:partyRole/pICSalesPR:pICSalesPR/pICSalesPR:individualName/pICSalesPR:individualName/pICSalesPR:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:contactMedium/cbe-v0:identifiedBy/party-v0:contactMediumKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:contactMedium/contactMedium:contactMedium/contactMedium:telephoneNumber/contactMedium:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/party-v0:contactMedium/contactMedium:contactMedium/contactMedium:eMailAddress/contactMedium:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/location-v0:geographicAddress/cbe-v0:identifiedBy/location-v0:geoAddressKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/location-v0:geographicAddress/location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/cbe-v0:identifiedBy/location-v0:countrySpecificAddressKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/location-v0:geographicAddress/location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/jpnAddr:japanesePropertyAddress/jpnAddr:postalCode/jpnAddr:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/location-v0:geographicAddress/location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/jpnAddr:japanesePropertyAddress/jpnAddr:address/jpnAddr:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/location-v0:geographicAddress/location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/jpnAddr:japanesePropertyAddress/jpnAddr:addressLine/jpnAddr:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/location-v0:placePartyRoleAssociation/cbe-v0:identifiedBy/location-v0:placePartyRoleAssocKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/customer-v0:paymentMethod/cbe-v0:identifiedBy/customer-v0:paymentMethodKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/customer-v0:pMPartyRoleAssociation/cbe-v0:identifiedBy/customer-v0:pMPartyRoleAssocKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/customer-v0:pMProductAssociation/cbe-v0:identifiedBy/customer-v0:pMPartyRoleAssocKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            ),
            '//res-v0:response/customer-v0:pMProductAssociation/cbe-v0:identifiedBy/customer-v0:pMProductAssocKey' => array(
                'tagName' => 'cbe-v0:objectID',
                'value' => 'DummyId'
            )
        );
        foreach ($AppendInfo as $path => $TagDetails) {
            $result = $xpath->query($path);
            for ($cnt = 0; $cnt < $result->length; $cnt ++) {
                $Node = $xml_res->createElement($TagDetails['tagName'], $TagDetails['value']);
                $result->item($cnt)->appendChild($Node);
            }
        }
        $AppendValues = array(
            '//res-v0:response/party-v0:partyRole/pICBillingPR:pICBillingPR/pICBillingPR:divisionName/pICBillingPR:divisionName/pICBillingPR:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:type',
                'value' => 'CharacteristicValue'
            ),
            '//res-v0:response/party-v0:partyRole/pICSalesPR:pICSalesPR/pICSalesPR:divisionName/pICSalesPR:divisionName/pICSalesPR:characteristicValue/cbe-v0:identifiedBy/cbe-v0:charcValueKey' => array(
                'tagName' => 'cbe-v0:type',
                'value' => 'CharacteristicValue'
            )
        );
        foreach ($AppendValues as $path => $TagDetails) {
            $result = $xpath->query($path);
            for ($cnt = 0; $cnt < $result->length; $cnt ++) {
                $Node = $xml_res->createElement($TagDetails['tagName'], $TagDetails['value']);
                $result->item($cnt)->appendChild($Node);
            }
        }
        $final_xml = $xml_res->saveXML();
        $xml_ = new DOMDocument();
        $xml_->preserveWhiteSpace = false;
        $xml_->formatOutput = true;
        $xml_->loadXML($final_xml);
        $nodes = $xml_->getElementsByTagName('objectID');
        $default_object_ids = array(
            '410b7100-0b23-4fbd-9d9e-763ce233c0a1',
            '09b28f78-0b23-4fb8-bf72-df7be5f2602f',
            '58e56b28-42fb-425c-88dc-557c50fab184',
            '27dceb32-490f-48b8-ae6f-f2c0ba78f2bd',
            '008e8aa8-0b23-4fba-8b4b-f75676637769',
            'f2be561e-0b23-4fbb-b182-e87258b5297e',
            '26ddca8f-0b23-4fb9-ba3d-a6cea1043a48',
            'da063062-0b23-4fb8-b08e-d9dd0bd424bd',
            '53b963b7-0b23-4fbd-b701-950b4c19e438',
            '75317ab8-0b23-4fba-9880-34400d2101f8',
            '30408e5e-0b23-4fbf-a2f1-fc012d312248',
            'b7511051-0b23-4fb9-8915-3970473d31da',
            'a664c0a6-0b23-4fb7-84b5-cde5ca8a8f31',
            'e757d58e-0b23-4fb5-91e0-194a49cdf683',
            '43d69f6e-0b23-4fb4-a68c-a57d37646832',
            '47d4584b-0b23-4fbc-925e-bc355d0249cc',
            '4b13a071-0b23-4fbf-9112-45d08776a080',
            '29456d12-0b23-4fbf-8f7d-e65b6219678c',
            '61ec3031-0b23-4fbf-9dc1-5b8024fa6001',
            '47e926d0-0b23-4fbb-a094-0bbe9f129454',
            '05593ed9-0b23-4fb0-98d6-68680931c5e0',
            'f7f9e523-0b23-4fbb-8e92-cb69e143a4ea',
            '4dc8cb11-0b23-4fb3-a691-a994d1c85306',
            '441e993a-0b23-4fb0-b795-0d4fafb868c3',
            '2416efec-0b23-4fb9-80d6-42d67cd8903f',
            '27a49571-0b23-4fb4-a2de-3ceb449f8eef',
            '5dab5bbc-0b23-4fba-a492-a1b4226cadb0',
            '82cafe7d-0b23-4fb1-8e6c-a48b5598d10d',
            'e4b03192-0b23-4fbc-aaf2-8f712eaf8504',
            '477d1f27-0b23-4fb3-bd93-3eeaa400a043',
            '76345c5d-0b23-4fba-9e7a-19a1c072219b',
            '546c56ec-0b23-4fb0-a496-4ff7c6c0de10',
            'effd0ebe-0b23-4fb9-9152-42644cfb18c9',
            '2ab5b9df-0b23-4fb8-8ebb-8370ccbf12be',
            '537ac9fb-0b23-4fb0-a356-1c6f4ebb8491',
            '26af4dc8-0b23-4fbc-b28e-e1fc188bc8c8',
            'a59d300d-0b23-4fbe-bb48-33005d0e50ad',
            '979469bc-0b23-4fb8-bd7a-4273b48a5953',
            '3ba71060-0b23-4fb2-8962-6419a8272f7d',
            '0a0fc8ab-0b23-4fbe-9912-9c37c1040b1d',
            '55506b99-0b23-4fb0-adbb-ba9af9a81d91',
            '6462fabe-0b23-4fbe-817b-01281f763125',
            '854fb15f-0b23-4fba-9ae0-f12edf3e7565',
            '2a5d6e66-0b23-4fb3-b316-407fe1e69123',
            '2fefcc9b-0b23-4fba-8f44-66ef43abd279',
            '9a30aa4c-0b23-4fbb-8b86-cbeeda3d4fae',
            'cfd89aac-0b23-4fb8-9e64-7f5ac5bf5647',
            '2729f900-0b23-4fbe-b1d9-c9e8cd777ef8',
            'e6341040-0b23-4fb4-a517-29721dbac53f',
            'c6e2372f-0b23-4fb0-83f5-242b7f786f72',
            'c6e2372f-0b23-4fb0-83f5-242b7f786f72',
            'a9699cc7-0b23-4fb1-869d-5f032b0e58b6',
            '9005ba46-0b23-4fbd-883c-be8d7b607008',
            '06da242f-0b23-4fb4-a729-1d9e43ef9ac9',
            '7d834c18-0b23-4fb3-9876-70768a28e376',
            '740e9392-0b23-4fba-b64a-d4ee11e4817c'
        );
        for ($s = 0; $s < $nodes->length; $s ++) {
            if ($nodes->item($s)->nodeValue == 'DummyId') $nodes->item($s)->nodeValue = $default_object_ids[$s];
        }
        $final_xml = $xml_->saveXML();
        return $final_xml;
    }

    function deleteNode($node)
    {
        $this->deleteChildren($node);
        $parent = $node->parentNode;
        $old_node = $parent->removeChild($node);
    }

    function deleteChildren($node)
    {
        while (isset($node->firstChild)) {
            $this->deleteChildren($node->firstChild);
            $node->removeChild($node->firstChild);
        }
    }
}
?>