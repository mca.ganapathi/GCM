<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');
include_once ('custom/modules/gc_Contracts/include/ClassServiceGcContracts.php');
include_once ('custom/service/v4_1_custom/APIHelper/v1/ClassContractPostResponse_v1.php');

/**
 * API Service Service Version 1.0 Class file
 *
 * @author sagar.salunkhe
 */
class SugarServiceMagicBridge_v1
{

    const API_VER = 1;

    public $objContractService;

    public $objPostResponseXML;

    /**
     * initiate all module service class objects here
     *
     * @author sagar.salunkhe
     * @since Jun 8, 2016
     */
    public function __construct()
    {
        self::initiateServiceObjects();
    }

    private function initiateServiceObjects()
    {
        $this->objContractService = new ClassServiceGcContracts(self::API_VER);
        $this->objPostResponseXML = new ClassContractPostResponse_v1();
    }
}