<?php
/***API 1.0 service codes***/
$api_service_codes_v1['1001'] = 'Contract created successfully.';
$api_service_codes_v1['1002'] = 'Contract Deactivated Successfully.';
$api_service_codes_v1['1003'] = 'Contract details Updated Successfully.';
$api_service_codes_v1['2001'] = 'Cannot process blank data.';
$api_service_codes_v1['2002'] = 'Cannot create Contract Record. Client Request ID already present in the system.';
$api_service_codes_v1['2003'] = 'Cannot find requested Contract Record Id into the GCM System.';
$api_service_codes_v1['2004'] = 'Contract is already in Deactivated stage.';
$api_service_codes_v1['2005'] = 'Contract Id is Empty.';
$api_service_codes_v1['2006'] = 'Cannot update Deactivated Contract.';
$api_service_codes_v1['2011'] = 'Product ID is Empty.';
$api_service_codes_v1['2012'] = 'Corporate Name is Empty.';
$api_service_codes_v1['2013'] = 'Created User is Empty.';
$api_service_codes_v1['2014'] = 'Billing Start Date is Empty.';
$api_service_codes_v1['2015'] = 'Billing End Date is Empty.';
$api_service_codes_v1['2016'] = 'Service Start Date is Empty.';
$api_service_codes_v1['2017'] = 'Service End Date is Empty.';
$api_service_codes_v1['2018'] = 'Corporate ID is Empty.';
$api_service_codes_v1['2019'] = 'Billing Corporate Name is Empty.';
$api_service_codes_v1['2021'] = 'Sales Channel Code is Empty.';
$api_service_codes_v1['2022'] = 'Billing Country Code is Empty.';
$api_service_codes_v1['2023'] = 'Billing Postal Code is Empty.';
$api_service_codes_v1['2024'] = 'Billing Address is Empty.';
$api_service_codes_v1['2025'] = 'Billing Address Line is Empty.';
$api_service_codes_v1['2026'] = 'Contracting Account State and Country combination does not match.';
$api_service_codes_v1['2027'] = 'Billing Account State and Country combination does not match.';
$api_service_codes_v1['2030'] = 'Billing Corporate Name field validation error.  The max length is 20.';
$api_service_codes_v1['2031'] = 'Billing Corporate Name Furigana field validation error.  The max length is 35.';
$api_service_codes_v1['2032'] = 'Contract Country Code is Empty.';
$api_service_codes_v1['2033'] = 'Billing Corporate Name Furigana is Empty.';
$api_service_codes_v1['2034'] = 'Contract Account Country record not found in GCM System.';
$api_service_codes_v1['2035'] = 'Technology Account Country record not found in GCM System.';
$api_service_codes_v1['2036'] = 'Billing Account Country record not found in GCM System.';
$api_service_codes_v1['3001'] = 'Invalid Session Id.';
$api_service_codes_v1['3002'] = 'XML data not found.';
$api_service_codes_v1['3003'] = 'CIDAS connection failed.';
$api_service_codes_v1['3004'] = ''; //dynamic text message
$api_service_codes_v1['3005'] = 'Contract process terminated. Contracting company record not found';
$api_service_codes_v1['4002'] = 'Method not allowed.';
$api_service_codes_v1['5004'] = 'Mis-match in response for RESTful API.';
$api_service_codes_v1['5005'] = 'Others.';
$api_service_codes_v1['E001'] = 'The number of search result exceeds threshold or the company name and the company name in English  after cleansing is blank.';
$api_service_codes_v1['E002'] = 'Wrong authentication pass code.';
$api_service_codes_v1['E003'] = 'Wrong GET parameter.';
$api_service_codes_v1['E004'] = 'System error.';
/***API 1.0 service codes***/