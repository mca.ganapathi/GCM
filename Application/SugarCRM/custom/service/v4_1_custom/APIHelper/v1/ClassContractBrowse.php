<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
/**
 * : ClassContractBrowse Class
 * @author : Shrikant.Gaware
 *         @date : 23-Oct-2015
 * @type unix-file system UTF-8
 */
require_once ('custom/include/XMLUtils.php');
require_once ('custom/service/v4_1_custom/APIHelper/v1/XMLBrowse.php');

class ClassContractBrowse extends XMLUtils
{

    /* Class Variable */
    var $xml;

    var $schema_path;

    var $exceptions;

    var $xmlerrors;

    var $dom_array;

    var $dom_obj;

    var $root;

    /* Class Variable */
    
    /**
     * Class Constructor to initialize class variables
     * @return : null
     */
    public function __construct()
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->__construct()");
        $this->xml = '';
        $this->schema_path = '';
        $this->exceptions = array();
        $this->xmlerrors = array();
        $this->dom_array = array();
        $this->root = array();
        libxml_use_internal_errors(true);
        $GLOBALS['log']->info("End: ClassContractBrowse->__construct()");
    }

    /**
     * : Method to initialize Dom object and xml File paths
     * @param : $xsd_path
     *        - <string> xsd file path
     * @return : @exception :
     * @author : Shrikant.Gaware
     *         @date : 23-Oct-2015
     */
    function initDom($xsd_path = null)
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->initDom($xsd_path)");
        
        if ($xsd_path == null) $xsd_path = 'custom/XMLSchema/Operation/ContractInformation/ContractInformationResponse-v0_1.xsd';
        
        $this->schema_path = $xsd_path;
        $this->initdomArray();
        $GLOBALS['log']->info("End: ClassContractBrowse->initDom($xsd_path)");
    }

    /**
     * : Method to validate xml Schema based on supplied XSD File
     * @param
     *        :
     *        
     * @return : @exception :
     * @author : Shrikant.Gaware
     *         @date : 23-Oct-2015
     */
    function validateSchema()
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->validateSchema()");
        // XSD Validation
        if (! $this->dom_obj->schemaValidate($this->schema_path)) {
            $this->exceptions[] = '3004';
            $this->xmlerrors = $this->libxmlDisplayErrors();
        }
        $GLOBALS['log']->info("End: ClassContractBrowse->validateSchema()");
    }

    /**
     * : Method to build the date time format in standard format
     * @param : $identifier
     *        - Path array key
     *        $focus - <object> Sugar Bean Module Object
     * @return : Formatted Date as Y-m-d/THH:MM/UTC
     *         @exception :
     * @author : Shrikant.Gaware
     *         @date : 23-Oct-2015
     */
    function formatDate($value, $zone)
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->formatDate($value,$zone)");
        if ($zone == '00:00' || $zone == '') {
            $zone = 'Z';
        }
        $dt = new SugarDateTime($value);
        $dt_arr = explode(' ', TimeDate::getInstance()->asDb($dt));
        $GLOBALS['log']->info("End: ClassContractBrowse->formatDate($value,$zone)");
        return $dt_arr[0] . 'T' . $dt_arr[1] . $zone;
    }

    /**
     * : Method to get the values with reference to supplied identifier
     * @param : $identifier
     *        - Path array key
     *        $focus - <object> Sugar Bean Module Object
     * @return : @exception :
     * @author : Shrikant.Gaware
     *         @date : 23-Oct-2015
     */
    function getValues($identifier, $key)
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->getValues($identifier, $key)");
        $fld_value = '';
        if (is_array($this->dom_array[$identifier])) {
            foreach ($this->dom_array[$identifier] as $k => $v) {
                if ($v['key'] == $key) {
                    $fld_value = $v['value'];
                    break;
                }
            }
        }
        return trim($fld_value);
        $GLOBALS['log']->info("End: ClassContractBrowse->getValues($identifier, $key)");
    }

    /**
     * : Method to set the values with reference to supplied identifier
     * @param : $identifier
     *        - Path array key
     *        $focus - <object> Sugar Bean Module Object
     * @return : @exception :
     * @author : Shrikant.Gaware
     *         @date : 23-Oct-2015
     */
    function setValues($identifier, $focus)
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->setValues($identifier, focus<object>)");
        if (is_array($this->dom_array[$identifier])) {
            foreach ($this->dom_array[$identifier] as $key => $value) {
                $fld_value = '';
                if (is_array($value['value'])) {
                    if(strpos($value['value'][1],'gc_timezone_id') !== false && $focus->$value['value'][1] !=''){
                        $obj_timezone = BeanFactory::getBean('gc_TimeZone', $focus->$value['value'][1]);
                        $fld_value = $this->formatDate($focus->$value['value'][0], $obj_timezone->utcoffset);
                    }else{
                        $fld_value = $this->formatDate($focus->$value['value'][0], $focus->$value['value'][1]);
                    }
                } else {
                    // $fld_value = $focus->$value['value'];
                    $fld_value = html_entity_decode($focus->$value['value'], ENT_QUOTES | ENT_XML1);
                    $fld_value = htmlspecialchars($fld_value, ENT_QUOTES | ENT_XML1);
                }
                if ($fld_value != '') {
                    $this->dom_array[$identifier][$key]['value'] = $fld_value;
                } else {
                    $this->dom_array[$identifier][$key]['value'] = '';
                }
            }
        }
        $GLOBALS['log']->info("End: ClassContractBrowse->setValues($identifier, focus<object>)");
    }

    /**
     * : Method to get Contract Details in xml format using sample xml file
     * @param : $object_id
     *        - <string> sugar crm UUID
     *        $contract_id - <string> global contract id
     * @return : $data - <string> xml data;
     *         @exception : <array> Error codes;
     * @author : Shrikant.Gaware
     *         @date : 23-Oct-2015
     */
    function getContractXML($object_id = null, $contract_id = null)
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->getContractXML($object_id, $contract_id)");
        $focus_contract = BeanFactory::getBean('gc_Contracts');
        if ($object_id != null) {
            $focus_contract->retrieve($object_id);
            if ($focus_contract->name == '') {
                $GLOBALS['log']->info("End: ClassContractBrowse->getContractXML($object_id, $contract_id)");
                $this->exceptions[] = '2003';
            }
        } elseif ($contract_id != null) {
            $focus_contract->retrieve_by_string_fields(array(
                'global_contract_id' => $contract_id
            ));
            if ($focus_contract->name == '') {
                $GLOBALS['log']->info("End: ClassContractBrowse->getContractXML($object_id, $contract_id)");
                $this->exceptions[] = '2003';
            }
        }
        $contract_id = $focus_contract->id;
        if (sizeof($this->exceptions) == 0) {
            
            $this->setValues('Contract', $focus_contract);
            $this->getContractAccountDetail($focus_contract);
            $this->getContractCorporateDetail($focus_contract);
            $this->getContractLineItemHistroyDetail($contract_id);
            $this->getContractSalesRepDetail($contract_id);
            
            $this->setRootNodes();
            $this->xml .= xmlRootBlock($this->root);
            $arr_parameters = array();
            
            $arr_parameters['glb']['createdDate'] = $this->getValues('Contract', 'createdDate');
            $arr_parameters['glb']['createdUser'] = $this->getValues('Contract', 'createdUser');
            $arr_parameters['glb']['contractName'] = $this->getValues('Contract', 'contractName');
            $arr_parameters['glb']['productName'] = $this->getValues('Product', 'productName');
            $arr_parameters['glb']['productId'] = $this->getValues('Product', 'productId');
            
            $this->xml .= xmlProductBlock($arr_parameters);
            
            $arr_parameters['tmp']['clientRequestID'] = $this->getValues('Contract', 'clientRequestID');
            $arr_parameters['tmp']['billingStartDate'] = $this->getValues('ContractLineItemHistroy', 'billingStartDate');
            $arr_parameters['tmp']['billingEndDate'] = $this->getValues('ContractLineItemHistroy', 'billingEndDate');
            $arr_parameters['tmp']['serviceStartDate'] = $this->getValues('ContractLineItemHistroy', 'serviceStartDate');
            $arr_parameters['tmp']['serviceEndDate'] = $this->getValues('ContractLineItemHistroy', 'serviceEndDate');
            $arr_parameters['tmp']['contractID'] = $this->getValues('Contract', 'contractID');
            $arr_parameters['tmp']['contractUUID'] = $this->getValues('Contract', 'contractUUID');
            $arr_parameters['tmp']['lintItemUUID'] = $this->getValues('LineItem', 'lintItemUUID');
            $this->xml .= xmlProductContractBlock($arr_parameters);
            unset($arr_parameters['tmp']);
            
            $arr_parameters['tmp']['companyName'] = $this->getValues('ContractCorporate', 'companyName');
            $arr_parameters['tmp']['companyNameEng'] = $this->getValues('ContractCorporate', 'companyNameEng');
            $arr_parameters['tmp']['companyNameKana'] = $this->getValues('ContractCorporate', 'companyNameKana');
            $arr_parameters['tmp']['corporationID'] = $this->getValues('ContractCorporate', 'corporationID');
            $arr_parameters['tmp']['vatNo'] = $this->getValues('Contract', 'vatNo');
            
            if (checkBlockFlag($arr_parameters) == 1) {
                $this->xml .= xmlPartBlock($arr_parameters);
            }
            unset($arr_parameters['tmp']);
            
            $arr_parameters['tmp']['divisionName'] = $this->getValues('ContractAccount', 'divisionName');
            $arr_parameters['tmp']['divisionNameLatin'] = $this->getValues('ContractAccount', 'divisionNameLatin');
            $arr_parameters['tmp']['divisionNameFurigana'] = $this->getValues('ContractAccount', 'divisionNameFurigana');
            $arr_parameters['tmp']['individualName'] = $this->getValues('ContractAccount', 'individualName');
            $arr_parameters['tmp']['individualNameLatin'] = $this->getValues('ContractAccount', 'individualNameLatin');
            $arr_parameters['tmp']['individualNameFurigana'] = $this->getValues('ContractAccount', 'individualNameFurigana');
            
            if (checkBlockFlag($arr_parameters) == 1) {
                $this->xml .= xmlContractPartBlock($arr_parameters);
            }
            unset($arr_parameters['tmp']);
            
            $arr_parameters['tmp']['corporationName'] = $this->getValues('BillingCorporate', 'corporationName');
            $arr_parameters['tmp']['corporationNameLatin'] = $this->getValues('BillingCorporate', 'corporationNameLatin');
            $arr_parameters['tmp']['corporationNameFurigana'] = $this->getValues('BillingCorporate', 'corporationNameFurigana');
            $arr_parameters['tmp']['divisionName'] = $this->getValues('BillingAccount', 'divisionName');
            $arr_parameters['tmp']['divisionNameLatin'] = $this->getValues('BillingAccount', 'divisionNameLatin');
            $arr_parameters['tmp']['divisionNameFurigana'] = $this->getValues('BillingAccount', 'divisionNameFurigana');
            $arr_parameters['tmp']['individualName'] = $this->getValues('BillingAccount', 'individualName');
            $arr_parameters['tmp']['individualNameLatin'] = $this->getValues('BillingAccount', 'individualNameLatin');
            $arr_parameters['tmp']['individualNameFurigana'] = $this->getValues('BillingAccount', 'individualNameFurigana');
            
            if (checkBlockFlag($arr_parameters) == 1) {
                $this->xml .= xmlBillingPartBlock($arr_parameters);
            }
            unset($arr_parameters['tmp']);
            
            $arr_parameters['tmp']['salesChannelCode'] = $this->getValues('SalesRep', 'salesChannelCode');
            $arr_parameters['tmp']['corporationName'] = $this->getValues('SalesRepCorporate', 'corporationName');
            $arr_parameters['tmp']['divisionName'] = $this->getValues('SalesRep', 'divisionName');
            $arr_parameters['tmp']['divisionNameLatin'] = $this->getValues('SalesRep', 'divisionNameLatin');
            $arr_parameters['tmp']['divisionNameFurigana'] = $this->getValues('SalesRep', 'divisionNameFurigana');
            $arr_parameters['tmp']['individualName'] = $this->getValues('SalesRep', 'individualName');
            $arr_parameters['tmp']['individualNameLatin'] = $this->getValues('SalesRep', 'individualNameLatin');
            $arr_parameters['tmp']['individualNameFurigana'] = $this->getValues('SalesRep', 'individualNameFurigana');
            
            if (checkBlockFlag($arr_parameters) == 1) {
                $this->xml .= xmlSalesRepPartBlock($arr_parameters);
            }
            unset($arr_parameters['tmp']);
            
            $arr_parameters['tmp']['telephoneNumber'] = $this->getValues('ContractAccount', 'telephoneNumber');
            $arr_parameters['tmp']['faxNumber'] = $this->getValues('ContractAccount', 'faxNumber');
            $arr_parameters['tmp']['eMailAddress'] = $this->getValues('ContractAccount', 'eMailAddress');
            
            if (checkBlockFlag($arr_parameters) == 1) {
                $this->xml .= xmlContractMediumBlock($arr_parameters);
            }
            unset($arr_parameters['tmp']);
            
            $arr_parameters['tmp']['telephoneNumber'] = $this->getValues('BillingAccount', 'telephoneNumber');
            $arr_parameters['tmp']['faxNumber'] = $this->getValues('BillingAccount', 'faxNumber');
            $arr_parameters['tmp']['eMailAddress'] = $this->getValues('BillingAccount', 'eMailAddress');
            
            if (checkBlockFlag($arr_parameters) == 1) {
                $this->xml .= xmlBillingMediumBlock($arr_parameters);
            }
            unset($arr_parameters['tmp']);
            
            $arr_parameters['tmp']['telephoneNumber'] = $this->getValues('SalesRep', 'telephoneNumber');
            $arr_parameters['tmp']['faxNumber'] = $this->getValues('SalesRep', 'faxNumber');
            $arr_parameters['tmp']['eMailAddress'] = $this->getValues('SalesRep', 'eMailAddress');
            
            if (checkBlockFlag($arr_parameters) == 1) {
                $this->xml .= xmlSalesMediumBlock($arr_parameters);
            }
            unset($arr_parameters['tmp']);
            
            $arr_parameters['tmp']['iso3166_1numeric'] = $this->getValues('ContractAccountCountry', 'iso3166_1numeric');
            $arr_parameters['tmp']['postalCode'] = $this->getValues('ContractAccount', 'postalCode');
            $arr_parameters['tmp']['address'] = $this->getValues('ContractAccountJs', 'address');
            $arr_parameters['tmp']['addressLine'] = $this->getValues('ContractAccountJs', 'addressLine');
            $arr_parameters['tmp']['countryKey'] = $this->getValues('ContractAccountCountry', 'countryKey');
            $arr_parameters['tmp']['iso3166_1alpha'] = $this->getValues('ContractAccountCountry', 'iso3166_1alpha');
            $arr_parameters['tmp']['addressLatin'] = $this->getValues('ContractAccount', 'addressLatin');
            $arr_parameters['tmp']['addressLocal'] = $this->getValues('ContractAccount', 'addressLocal');
            
            $arr_parameters['tmp']['addressGeneral'] = $this->getValues('ContractAccount', 'addressGeneral');
            $arr_parameters['tmp']['cityGeneral'] = $this->getValues('ContractAccount', 'cityGeneral');
            $arr_parameters['tmp']['stateGeneral'] = $this->getValues('ContractAccountState', 'stateGeneral');
            $arr_parameters['tmp']['stateCode'] = $this->getValues('ContractAccountState', 'stateCode');
            
            if (checkBlockFlag($arr_parameters) == 1) {
                $this->xml .= xmlContractLocationBlock($arr_parameters);
            }
            unset($arr_parameters['tmp']);
            
            $arr_parameters['tmp']['iso3166_1numeric'] = $this->getValues('BillingAccountCountry', 'iso3166_1numeric');
            $arr_parameters['tmp']['postalCode'] = $this->getValues('BillingAccount', 'postalCode');
            $arr_parameters['tmp']['address'] = $this->getValues('BillingAccountJs', 'address');
            $arr_parameters['tmp']['addressLine'] = $this->getValues('BillingAccountJs', 'addressLine');
            $arr_parameters['tmp']['countryKey'] = $this->getValues('BillingAccountCountry', 'countryKey');
            $arr_parameters['tmp']['iso3166_1alpha'] = $this->getValues('BillingAccountCountry', 'iso3166_1alpha');
            $arr_parameters['tmp']['addressLatin'] = $this->getValues('BillingAccount', 'addressLatin');
            $arr_parameters['tmp']['addressLocal'] = $this->getValues('BillingAccount', 'addressLocal');
            
            $arr_parameters['tmp']['addressGeneral'] = $this->getValues('BillingAccount', 'addressGeneral');
            $arr_parameters['tmp']['cityGeneral'] = $this->getValues('BillingAccount', 'cityGeneral');
            $arr_parameters['tmp']['stateGeneral'] = $this->getValues('BillingAccountState', 'stateGeneral');
            $arr_parameters['tmp']['stateCode'] = $this->getValues('BillingAccountState', 'stateCode');
            
            if (checkBlockFlag($arr_parameters) == 1) {
                $this->xml .= xmlBillingLocationBlock($arr_parameters);
            }
            unset($arr_parameters['tmp']);
            
            $this->xml .= xmlPartyRoleAssocBlock($arr_parameters);
            $this->xml .= xmlCustomerPaymentBlock($arr_parameters);
            unset($arr_parameters);
            
            $this->xml .= "\n</res-v0:response>";
            $GLOBALS['log']->info("Final Array Values Begin" . print_r($this->dom_array, true) . "Final Array Values End");
            /* Commented the XSD validation & DOM parser start 
             *
             * $this->domObj = new DOMDocument();
             * $this->domObj->loadxml($this->XML);
             * $this->validateSchema();
             * if(!empty($this->exceptions))
             * {
             * $data = array_merge($this->exceptions, $this->xmlerrors);
             * }
             * else{
             * $data = $this->domObj->saveXML();
             * }
             * $data = $this->domObj->saveXML();
             *
            /* Commented the XSD validation & DOM parser end */
            $data = $this->xml;
        } else {
            $data = array_merge($this->exceptions, $this->xmlerrors);
        }
        $GLOBALS['log']->info("End: ClassContractBrowse->getContractXML($object_id, $contract_id)");
        $GLOBALS['log']->info("ClassContractBrowseXML->GeneratedContractXMLAfterDOM(" . $data . ")");
        return $data;
    }

    /**
     * : Get Contract Account Details set at respective xml Paths
     * @param : $focus_contract
     *        - <object> Contract Bean
     * @return : @exception :
     * @author : Shrikant.Gaware
     *         @date : 26-Oct-2015
     */
    function getContractAccountDetail($focus_contract)
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->getContractAccountDetail()");
        
        $focus_contact = $focus_contract->get_linked_beans('contacts_gc_contracts_1', 'Contacts');
        if (sizeof($focus_contact) == 1) $focus_contact = $focus_contact[0];
        
        $focus_account_js = BeanFactory::getBean('js_Accounts_js');
        $focus_account_js->retrieve_by_string_fields(array(
            'contact_id_c' => $focus_contact->id
        ));
        
        $focus_country = BeanFactory::getBean('c_country');
        $focus_country->retrieve($focus_contact->c_country_id_c);
        
        $focus_state = BeanFactory::getBean('c_State');
        $focus_state->retrieve($focus_contact->c_state_id_c);
        
        $this->setValues('ContractAccount', $focus_contact);
        $this->setValues('ContractAccountCountry', $focus_country);
        $this->setValues('ContractAccountJs', $focus_account_js);
        $this->setValues('ContractAccountState', $focus_state);
        $GLOBALS['log']->info("End: ClassContractBrowse->getContractAccountDetail()");
    }

    /**
     * : Get Contract Corporate Details set at respective xml Paths
     * @param : $focus_contract
     *        - <object> Contract Bean
     * @return : @exception :
     * @author : Shrikant.Gaware
     *         @date : 26-Oct-2015
     */
    function getContractCorporateDetail($focus_contract)
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->getContractCorporateDetail()");
        
        $focus_corporate = $focus_contract->get_linked_beans('accounts_gc_contracts_1', 'Accounts');
        if (sizeof($focus_corporate) == 1) $focus_corporate = $focus_corporate[0];
        
        $this->setValues('ContractCorporate', $focus_corporate);
        $GLOBALS['log']->info("End: ClassContractBrowse->getContractCorporateDetail()");
    }

    /**
     * : Get Contract Line Item Details set at respective xml Paths
     * @param : $contract_id
     *        - <string> Contract UUID
     * @return : @exception :
     * @author : Shrikant.Gaware
     *         @date : 26-Oct-2015
     */
    function getContractLineItemHistroyDetail($contract_id)
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->getContractLineItemHistroyDetail()");
        
        $focus_line_contract_history = BeanFactory::getBean('gc_Line_Item_Contract_History');
        $focus_line_contract_history->retrieve_by_string_fields(array(
            'contracts_id' => $contract_id
        ));
        
        $this->setValues('ContractLineItemHistroy', $focus_line_contract_history);
        
        $this->getBillingAccountDetail($focus_line_contract_history->bill_account_id);
        $this->getTechAccountDetail($focus_line_contract_history->tech_account_id);
        
        $focus_line_item = BeanFactory::getBean('gc_LineItem');
        $focus_line_item->retrieve($focus_line_contract_history->line_item_id);
        $this->setValues('LineItem', $focus_line_item);
        
        $product_item = BeanFactory::getBean('pi_product_item');
        $product_item->retrieve($focus_line_item->products_id);
        $this->setValues('Product', $product_item);
        
        $GLOBALS['log']->info("End: ClassContractBrowse->getContractLineItemHistroyDetail()");
    }

    /**
     * : Get Billing Account Details set at respective xml Paths
     * @param : $accountId
     *        - <string> Account UUID
     * @return : @exception :
     * @author : Shrikant.Gaware
     *         @date : 26-Oct-2015
     */
    function getBillingAccountDetail($accountId)
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->getBillingAccountDetail()");
        
        $focus_contact = BeanFactory::getBean('Contacts');
        $focus_contact->retrieve($accountId);
        
        $focus_account_js = BeanFactory::getBean('js_Accounts_js');
        $focus_account_js->retrieve_by_string_fields(array(
            'contact_id_c' => $focus_contact->id
        ));
        
        $focus_country = BeanFactory::getBean('c_country');
        $focus_country->retrieve($focus_contact->c_country_id_c);
        
        $focus_state = BeanFactory::getBean('c_State');
        $focus_state->retrieve($focus_contact->c_state_id_c);
        
        $focus_corporate = BeanFactory::getBean('Accounts');
        $focus_corporate->retrieve($focus_contact->account_id);
        
        $this->setValues('BillingAccount', $focus_contact);
        $this->setValues('BillingAccountCountry', $focus_country);
        $this->setValues('BillingAccountJs', $focus_account_js);
        $this->setValues('BillingCorporate', $focus_corporate);
        $this->setValues('BillingAccountState', $focus_state);
        
        $GLOBALS['log']->info("End: ClassContractBrowse->getBillingAccountDetail()");
    }

    /**
     * : Get Technology Account Details set at respective xml Paths
     * @param : $accountId
     *        - <string> Account UUID
     * @return : @exception :
     * @author : Shrikant.Gaware
     *         @date : 26-Oct-2015
     */
    function getTechAccountDetail($accountId)
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->getTechAccountDetail()");
        
        $focus_contact = BeanFactory::getBean('Contacts');
        $focus_contact->retrieve($accountId);
        
        $focus_corporate = BeanFactory::getBean('Accounts');
        $focus_corporate->retrieve($focus_contact->account_id);
        
        $this->setValues('TechCorporate', $focus_corporate);
        $GLOBALS['log']->info("End: ClassContractBrowse->getTechAccountDetail()");
    }

    /**
     * : Get Sales Representative details set at respective xml Paths
     * @param : $contract_id
     *        - <string> Contract UUID
     * @return : @exception :
     * @author : Shrikant.Gaware
     *         @date : 26-Oct-2015
     */
    function getContractSalesRepDetail($contract_id)
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->getContractSalesRepDetail()");
        
        $focus_sales_rep = BeanFactory::getBean('gc_SalesRepresentative');
        $focus_sales_rep->retrieve_by_string_fields(array(
            'contracts_id' => $contract_id
        ));
        
        $focus_corporate = BeanFactory::getBean('Accounts');
        $focus_corporate->retrieve($focus_sales_rep->corporate_id);
        
        $this->setValues('SalesRep', $focus_sales_rep);
        $this->setValues('SalesRepCorporate', $focus_corporate);
        $GLOBALS['log']->info("End: ClassContractBrowse->getContractSalesRepDetail()");
    }

    function initdomArray()
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->initdomArray()");
        $this->dom_array = array(
            'Contract' => array(
                array(
                    'key' => 'contractUUID',
                    'value' => 'id'
                ),
                array(
                    'key' => 'contractID',
                    'value' => 'global_contract_id'
                ),
                array(
                    'key' => 'contractName',
                    'value' => 'name'
                ),
                array(
                    'key' => 'clientRequestID',
                    'value' => 'contract_request_id'
                ),
                array(
                    'key' => 'createdUser',
                    'value' => 'ext_sys_created_by'
                ),
                array(
                    'key' => 'createdDate',
                    'value' => array(
                        'date_entered',
                        'date_enteredtimezone'
                    )
                ),
                array(
                    'key' => 'vatNo',
                    'value' => 'vat_no'
                )
            ),
            'LineItem' => array(
                array(
                    'key' => 'lintItemUUID',
                    'value' => 'id'
                )
            ),
            'ContractAccount' => array(
                array(
                    'key' => 'individualNameLatin',
                    'value' => 'name'
                ),
                array(
                    'key' => 'individualName',
                    'value' => 'name_2_c'
                ),
                array(
                    'key' => 'postalCode',
                    'value' => 'postal_no_c'
                ),
                array(
                    'key' => 'divisionNameLatin',
                    'value' => 'division_1_c'
                ),
                array(
                    'key' => 'divisionName',
                    'value' => 'division_2_c'
                ),
                array(
                    'key' => 'telephoneNumber',
                    'value' => 'phone_work'
                ),
                array(
                    'key' => 'eMailAddress',
                    'value' => 'email1'
                ),
                array(
                    'key' => 'faxNumber',
                    'value' => 'phone_fax'
                ),
                array(
                    'key' => 'addressLatin',
                    'value' => 'addr_1_c'
                ),
                array(
                    'key' => 'addressLocal',
                    'value' => 'addr_2_c'
                ),
                array(
                    'key' => 'addressGeneral',
                    'value' => 'addr_general_c'
                ),
                array(
                    'key' => 'cityGeneral',
                    'value' => 'city_general_c'
                )
            ),
            'ContractAccountCountry' => array(
                array(
                    'key' => 'iso3166_1numeric',
                    'value' => 'country_code_numeric'
                ),
                array(
                    'key' => 'iso3166_1alpha',
                    'value' => 'country_code_alphabet'
                ),
                array(
                    'key' => 'countryKey',
                    'value' => 'name'
                )
            ),
            'ContractAccountJs' => array(
                array(
                    'key' => 'address',
                    'value' => 'addr1'
                ),
                array(
                    'key' => 'addressLine',
                    'value' => 'addr2'
                )
            ),
            'ContractAccountState' => array(
                array(
                    'key' => 'stateGeneral',
                    'value' => 'name'
                ),
                array(
                    'key' => 'stateCode',
                    'value' => 'state_code'
                )
            ),
            'ContractCorporate' => array(
                array(
                    'key' => 'corporationID',
                    'value' => 'common_customer_id_c'
                ),
                array(
                    'key' => 'companyNameEng',
                    'value' => 'name'
                ),
                array(
                    'key' => 'companyName',
                    'value' => 'name_2_c'
                ),
                array(
                    'key' => 'companyNameKana',
                    'value' => 'name_3_c'
                ),
                array(
                    'key' => 'vatNo',
                    'value' => 'vat_no_c'
                )
            ),
            'ContractLineItemHistroy' => array(
                array(
                    'key' => 'serviceStartDate',
                    'value' => array(
                        'service_start_date',
                        'gc_timezone_id_c'
                    )
                ),
                array(
                    'key' => 'serviceEndDate',
                    'value' => array(
                        'service_end_date',
                        'gc_timezone_id1_c'
                    )
                ),
                array(
                    'key' => 'billingStartDate',
                    'value' => array(
                        'billing_start_date',
                        'gc_timezone_id2_c'
                    )
                ),
                array(
                    'key' => 'billingEndDate',
                    'value' => array(
                        'billing_end_date',
                        'gc_timezone_id3_c'
                    )
                )
            ),
            'Product' => array(
                array(
                    'key' => 'productName',
                    'value' => 'name'
                ),
                array(
                    'key' => 'productId',
                    'value' => 'id'
                )
            ),
            'BillingAccount' => array(
                array(
                    'key' => 'individualNameLatin',
                    'value' => 'name'
                ),
                array(
                    'key' => 'individualName',
                    'value' => 'name_2_c'
                ),
                array(
                    'key' => 'postalCode',
                    'value' => 'postal_no_c'
                ),
                array(
                    'key' => 'divisionNameLatin',
                    'value' => 'division_1_c'
                ),
                array(
                    'key' => 'divisionName',
                    'value' => 'division_2_c'
                ),
                array(
                    'key' => 'telephoneNumber',
                    'value' => 'phone_work'
                ),
                array(
                    'key' => 'eMailAddress',
                    'value' => 'email1'
                ),
                array(
                    'key' => 'faxNumber',
                    'value' => 'phone_fax'
                ),
                array(
                    'key' => 'addressLatin',
                    'value' => 'addr_1_c'
                ),
                array(
                    'key' => 'addressLocal',
                    'value' => 'addr_2_c'
                ),
                array(
                    'key' => 'addressGeneral',
                    'value' => 'addr_general_c'
                ),
                array(
                    'key' => 'cityGeneral',
                    'value' => 'city_general_c'
                )
            ),
            'BillingAccountCountry' => array(
                array(
                    'key' => 'iso3166_1numeric',
                    'value' => 'country_code_numeric'
                ),
                array(
                    'key' => 'iso3166_1alpha',
                    'value' => 'country_code_alphabet'
                ),
                array(
                    'key' => 'countryKey',
                    'value' => 'name'
                )
            ),
            'BillingAccountJs' => array(
                array(
                    'key' => 'address',
                    'value' => 'addr1'
                ),
                array(
                    'key' => 'addressLine',
                    'value' => 'addr2'
                )
            ),
            'BillingAccountState' => array(
                array(
                    'key' => 'stateGeneral',
                    'value' => 'name'
                ),
                array(
                    'key' => 'stateCode',
                    'value' => 'state_code'
                )
            ),
            'BillingCorporate' => array(
                array(
                    'key' => 'corporationNameLatin',
                    'value' => 'name'
                ),
                array(
                    'key' => 'corporationName',
                    'value' => 'name_2_c'
                ),
                array(
                    'key' => 'corporationNameFurigana',
                    'value' => 'name_3_c'
                )
            ),
            'SalesRep' => array(
                array(
                    'key' => 'salesChannelCode',
                    'value' => 'sales_channel_code'
                ),
                array(
                    'key' => 'individualNameLatin',
                    'value' => 'name'
                ),
                array(
                    'key' => 'individualName',
                    'value' => 'sales_rep_name_1'
                ),
                array(
                    'key' => 'individualNameFurigana',
                    'value' => 'sales_rep_name_2'
                ),
                array(
                    'key' => 'divisionNameLatin',
                    'value' => 'division_1'
                ),
                array(
                    'key' => 'divisionName',
                    'value' => 'division_2'
                ),
                array(
                    'key' => 'divisionNameFurigana',
                    'value' => 'division_3'
                ),
                array(
                    'key' => 'telephoneNumber',
                    'value' => 'tel_no'
                ),
                array(
                    'key' => 'eMailAddress',
                    'value' => 'email1'
                ),
                array(
                    'key' => 'faxNumber',
                    'value' => 'fax_no'
                )
            ),
            'SalesRepCorporate' => array(
                array(
                    'key' => 'corporationName',
                    'value' => 'name_2_c'
                )
            )
        );
        $GLOBALS['log']->info("Begin: ClassContractBrowse->initdomArray()");
    }

    /**
     * : Method to build xml paths and respective field name for value replace
     * @param
     *        :
     *        
     * @return : @exception :
     * @author : Shrikant.Gaware
     *         @date : 23-Oct-2015
     */
    function setRootNodes()
    {
        $GLOBALS['log']->info("Begin: ClassContractBrowse->setRootNodes()");
        global $sugar_config;
        $url = rtrim($sugar_config['site_url'], '/') . '/custom/XMLSchema/';
        $path = realpath($this->schema_path);
        $path = str_replace('\\', '/', rtrim($url, '/') . " file:/$path");
        
        $this->root = array(
            'xsi:schemaLocation' => $path,
            'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'xmlns:res-v0' => $url . 'contract-information',
            'xmlns:cloud001' => $url . 'product/cloud-product001',
            'xmlns:prodCntrctItem0001' => $url . 'business-interaction/product-contract-item0001',
            'xmlns:ptyIdent001' => $url . 'party/corporate-identification',
            'xmlns:ptyIdent002' => $url . 'party/sales-channel-identification',
            'xmlns:ptyIdent003' => $url . 'party/vat-no-identification',
            'xmlns:cidasCmpnyNm' => $url . 'party/cidas-company-name',
            'xmlns:pICContractPR' => $url . 'party/pic-contract-pseudo-role',
            'xmlns:billingCustomerPR' => $url . 'party/billing-customer-pseudo-role',
            'xmlns:pICBillingPR' => $url . 'party/pic-billing-pseudo-role',
            'xmlns:salesChannelPR' => $url . 'party/sales-channel-pseudo-role',
            'xmlns:pICSalesPR' => $url . 'party/pic-sales-pr',
            'xmlns:jpnPM01' => $url . 'customer/jpn-account-transfer-pm',
            'xmlns:jpnPM02' => $url . 'customer/jpn-postal-transfer-pm',
            'xmlns:payM01' => $url . 'customer/credit-card-pm',
            'xmlns:jpnPM03' => $url . 'customer/jpn-direct-deposit-pm',
            'xmlns:jpnAddr' => $url . 'location/japanese-property-address',
            'xmlns:usaAddr' => $url . 'location/american-property-address',
            'xmlns:canAddr' => $url . 'location/canadian-property-address',
            'xmlns:anyAddr' => $url . 'location/any-address',
            'xmlns:contactMedium' => $url . 'party/standard-contact-medium',
            'xmlns:customerTechRep' => $url . 'party/customer-tech-rep',
            'xmlns:customerTechRepPR' => $url . 'party/customer-tech-rep-pseudo-role',
            'xmlns:pICCustomerTechPR' => $url . 'party/pic-customer-tech-pr',
            'xmlns:pICSalesChannelPR' => $url . 'party/pic-sales-channel-pseudo-role',
            'xmlns:salesChannel' => $url . 'party/sales-channel',
            'xmlns:datatypes-v0' => $url . 'cbe-datatypes',
            'xmlns:cbe-v0' => $url . 'common-business-entity',
            'xmlns:party-v0' => $url . 'party',
            'xmlns:location-v0' => $url . 'location',
            'xmlns:product-v0' => $url . 'product',
            'xmlns:bi-v0' => $url . 'business-interaction',
            'xmlns:customer-v0' => $url . 'customer',
            'xmlns:xsd' => 'http://www.w3.org/2001/XMLSchema'
        );
        $GLOBALS['log']->info("End: ClassContractBrowse->setRootNodes()");
    }
}
?>