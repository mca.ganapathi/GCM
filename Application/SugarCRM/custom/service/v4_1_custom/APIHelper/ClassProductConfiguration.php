<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

include_once 'custom/include/ModUtils.php';

/**
 * ClassProductConfiguration Class
 */
class ClassProductConfiguration
{
    /**
     * set pms_session_key
     *
     * @var string
     */
    private $pms_session_key;

    /**
     * set response_code_key
     *
     * @var string
     */
    private $response_code_key;

    /**
     * set respose_xml_key
     *
     * @var string
     */
    private $respose_xml_key;

    /**
     * set respose_array_key
     *
     * @var string
     */
    private $respose_array_key;

    /**
     * Constructor method for class ClassProductConfiguration
     *
     * @author : Arunsakthivel
     * @since Mar 16, 2016
     */
    public function __construct()
    {
        $this->pms_session_key   = 'pms_product';
        $this->response_code_key = 'response_code';
        $this->respose_xml_key   = 'respose_xml';
        $this->respose_array_key = 'respose_array';
    }

    /**
     * Method to get Product Offerings from API
     *
     * @param boolean $status_filter
     * @return array $product_offering <array> product offering
     * @author Dinesh.Itkar, Arunsakthivel
     * @since Mar 16, 2016
     */
    public function getProductOfferings($status_filter)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'status_filter : ' . $status_filter, 'Method to get Product Offerings from API');
        global $sugar_config;
        $response[$this->response_code_key] = 0;
        $response[$this->respose_array_key] = "";

        $catalog_id = isset($sugar_config['pms_product']['catalog_id']) ? $sugar_config['pms_product']['catalog_id'] : '';
        $po         = isset($sugar_config['pms_product']['po_prefix']) ? $sugar_config['pms_product']['po_prefix'] . '_' . $catalog_id : '';

        if ($status_filter) {
            $query         = 'stateid=5';
            $offering_type = 'filtered_list';
        } else {
            $query         = '';
            $offering_type = 'full_list';
        }

        // check product catalog in session
        if (!isset($_SESSION[$this->pms_session_key][$po][$offering_type]) || empty($_SESSION[$this->pms_session_key][$po][$offering_type])) {

            $relative_uri = '/catalog/' . $catalog_id . '/offering'; // Product Offering URL
            // Adding the status filter for filter only the live offering in Create/edit and search filter

            // Get product offering from API
            $response = $this->apiProductConfiguration($relative_uri, $query);

            if ($response[$this->response_code_key] != 0) {

                $product_offering_xml = $response[$this->respose_xml_key];
                // Convert response into array
                $product_offering_array = $this->convertProductOfferingsXmlToArray($product_offering_xml);

                // store in session
                $_SESSION[$this->pms_session_key][$po][$offering_type] = $product_offering_array;
                unset($response[$this->respose_xml_key]);

                $response[$this->response_code_key] = 1;
                $response[$this->respose_array_key] = $_SESSION[$this->pms_session_key][$po][$offering_type];
            }

        } else {
            $response[$this->response_code_key] = 1;
            $response[$this->respose_array_key] = $_SESSION[$this->pms_session_key][$po][$offering_type];
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response : ' . print_r($response, true), '');
        return ModUtils::object_to_array($response);
    }

    /**
     * Method to get Product Offerings Details from API
     *
     * @param string $product_offering_id product offering id
     * @return array product offering details
     * @author Dinesh.Itkar, Arunsakthivel
     * @since Mar 16, 2016
     */
    public function getProductOfferingDetails($product_offering_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_offering_id : ' . $product_offering_id, 'Method to get Product Offerings Details from API');
        global $sugar_config;

        $response[$this->response_code_key] = 0;
        $response[$this->respose_array_key] = "";

        if (empty($product_offering_id) || $product_offering_id <= 0) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response : ' . print_r($response, true), '');
            return $response;
        }

        $catalog_id = isset($sugar_config['pms_product']['catalog_id']) ? $sugar_config['pms_product']['catalog_id'] : '';
        $pod_prefix = isset($sugar_config['pms_product']['catalog_id']) ? $sugar_config['pms_product']['pod_prefix'] : '';

        // check product offering details in session
        if (!isset($_SESSION[$this->pms_session_key][$pod_prefix . '_' . $catalog_id . '_' . $product_offering_id]) || empty($_SESSION[$this->pms_session_key][$pod_prefix . '_' . $catalog_id . '_' . $product_offering_id])) {

            $relative_uri = '/catalog/' . $catalog_id . '/offering/' . $product_offering_id . '/detail'; // Product Specification Details

            // Get product offering details from API

            $response = $this->apiProductConfiguration($relative_uri, '');

            if ($response[$this->response_code_key] != 0) {

                $product_offering_details_xml = $response[$this->respose_xml_key];
                // Convert response into array
                $product_offering_details_array = $this->convertProductOfferingDetailsXmlToArray($product_offering_details_xml);
                // store in session
                $_SESSION[$this->pms_session_key][$pod_prefix . '_' . $catalog_id . '_' . $product_offering_id] = $product_offering_details_array;

                $product_offering_details_array = $_SESSION[$this->pms_session_key][$pod_prefix . '_' . $catalog_id . '_' . $product_offering_id];

                unset($response[$this->respose_xml_key]);
                $response[$this->respose_array_key] = $product_offering_details_array;
            }

        } else {
            $response[$this->response_code_key] = 1;
            $response[$this->respose_array_key] = $_SESSION[$this->pms_session_key][$pod_prefix . '_' . $catalog_id . '_' . $product_offering_id];
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'response : ' . print_r($response, true), '');
        return ModUtils::object_to_array($response);

    }

    /**
     * Method to convert product offering xml to array
     *
     * @param xml $product_offering_xml product offering in xml format
     * @return array product offering array
     * @author Dinesh.Itkar
     * @since Mar 16, 2016
     */
    private function convertProductOfferingsXmlToArray($product_offering_xml)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_offering_xml : ' . print_r($product_offering_xml, true), 'Method to convert product offering xml to array');
        $xmlDoc = new DOMDocument(); // Create DOM Document object
        $xmlDoc->loadXML($product_offering_xml); // Load XML content

        $product_offering_array = array();

        $offering_node       = $xmlDoc->getElementsByTagName('offering'); // get offering node
        $offering_node_count = $offering_node->length; // Find length of offering node

        for ($i = 0; $i < $offering_node_count; $i++) // go to each offering 1 by 1
        {
            $offering_id                          = $offering_node->item($i)->getElementsByTagName('id')->item(0)->nodeValue;
            $offering_value                       = $offering_node->item($i)->getElementsByTagName('offrname')->item(0)->nodeValue;
            $product_offering_array[$offering_id] = $offering_value;

        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_offering_array : ' . print_r($product_offering_array, true), '');
        return ModUtils::object_to_array($product_offering_array);
    }

    /**
     * Method to convert product offering details xml to array
     *
     * @param xml $product_offering_details_xml product offering details in xml format
     * @return array product offering details array
     * @author Dinesh.Itkar , Arunsakthivel
     * @since Mar 16, 2016
     */
    private function convertProductOfferingDetailsXmlToArray($product_offering_details_xml)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_offering_details_xml : ' . print_r($product_offering_details_xml, true), 'Method to convert product offering details xml to array');
        $xmlDoc = new DOMDocument(); // Create DOM Document object
        $xmlDoc->loadXML($product_offering_details_xml); // Load XML content
        $arr_result                                              = $this->xml_to_array($xmlDoc); // Convert xml into array
        $product_offering_details_array                          = array();
        $product_offering_details_array['specification_details'] = array();
        $product_offering_details_array['charge_details']        = array();
        $product_offering_details_array['charge_mapping']        = array();
        $chargedetail_chargetype_list                            = $GLOBALS['app_list_strings']['pms_product_chargedetail_chargetype_list'];

        //Get offering name
        $product_offering_details_array['offering_details']['offering_id']   = !empty($arr_result['listcatalogofferingdetailresponse']['listcatalogoffering']['offerings']['offering']['id']) ? $arr_result['listcatalogofferingdetailresponse']['listcatalogoffering']['offerings']['offering']['id'] : '';
        $product_offering_details_array['offering_details']['offering_name'] = !empty($arr_result['listcatalogofferingdetailresponse']['listcatalogoffering']['offerings']['offering']['offrname']) ? $arr_result['listcatalogofferingdetailresponse']['listcatalogoffering']['offerings']['offering']['offrname'] : '';

        // Get charges blocks
        $charges_blocks = !empty($arr_result['listcatalogofferingdetailresponse']['listcatalogofferingprice']['charges']) ? $arr_result['listcatalogofferingdetailresponse']['listcatalogofferingprice']['charges'] : array();

        foreach ($charges_blocks as $charges_block_key => $charges_block_val) {

            $charge_record = array();
            if (is_array($charges_block_val)) {
                $charge_record = $charges_block_val;
            } else {
                $charge_record = $charges_blocks;
            }

            $charge = !empty($charge_record['charge']) ? $charge_record['charge'] : array();
            foreach ($charge as $charge_key => $charge_val) {
                $pricelineid      = isset($charge_val['pricelineid']) ? trim($charge_val['pricelineid']) : ''; // Fetch pricelineid tag value
                $chargedesc       = isset($charge_val['chargedesc']) ? trim($charge_val['chargedesc']) : ''; // Fetch chargedesc tag value
                $chargeid         = isset($charge_val['chargeid']) ? trim($charge_val['chargeid']) : ''; // Fetch chargeid tag value
                $chargeperioddesc = isset($charge_val['chargeperioddesc']) ? trim($charge_val['chargeperioddesc']) : ''; // Fetch chargeperioddesc tag value
                $chargeperiodid   = isset($charge_val['chargeperiodid']) ? trim($charge_val['chargeperiodid']) : ''; // Fetch chargeperiodid tag value
                $currencyid       = isset($charge_val['currencyid']) ? trim($charge_val['currencyid']) : ''; // Fetch currencyid tag value
                $rate             = isset($charge_val['offeringprice']['rate']) ? trim($charge_val['offeringprice']['rate']) : ''; // Fetch rate tag value
                $specid           = isset($charge_val['offeringprice']['specification']['specid']) ? trim($charge_val['offeringprice']['specification']['specid']) : ''; // Fetch specid tag value
                // Fetch charvalid tag value
                $charvalid     = !empty($charge_val['offeringprice']['specification']['charvalid']) ? $charge_val['offeringprice']['specification']['charvalid'] : array(); //  Fetch isCustomPrice tag value
                $isCustomPrice = isset($charge_val['isCustomPrice']) ? $charge_val['isCustomPrice'] : 'false';

                $product_offering_details_array['charge_details'][$pricelineid]['pricelineid'] = $pricelineid; // Populate pricelineid values in array
                $product_offering_details_array['charge_details'][$pricelineid]['chargedesc']  = $chargedesc; // Populate chargedesc values in array

                $product_offering_details_array['charge_details'][$pricelineid]['chargeid']   = $chargeid; // Populate chargeid values in array
                $product_offering_details_array['charge_details'][$pricelineid]['chargetype'] = isset($chargedetail_chargetype_list[$chargeid]) ? trim($chargedetail_chargetype_list[$chargeid]) : '';

                $product_offering_details_array['charge_details'][$pricelineid]['chargeperiodid']   = $chargeperiodid; // Populate chargeperiodid values in array
                $product_offering_details_array['charge_details'][$pricelineid]['chargeperioddesc'] = $chargeperioddesc; // Populate chargeperioddesc values in array

                $product_offering_details_array['charge_details'][$pricelineid]['currencyid']                                  = $currencyid; // Populate currencyid values in array
                $product_offering_details_array['charge_details'][$pricelineid]['isCustomPrice']                               = $isCustomPrice; // Populate isCustomPrice values in array
                $product_offering_details_array['charge_details'][$pricelineid]['offeringprice']['rate']                       = $rate; // Populate rate values in array
                $product_offering_details_array['charge_details'][$pricelineid]['offeringprice']['specification']['specid']    = $specid; // Populate specid values in array
                $product_offering_details_array['charge_details'][$pricelineid]['offeringprice']['specification']['charvalid'] = $charvalid; // Populate charvalid values in array

            }
            unset($charge_record);
        }

        foreach ($product_offering_details_array['charge_details'] as $pricelineid2 => $charge_details) {
            $specid    = isset($charge_details['offeringprice']['specification']['specid']) ? trim($charge_details['offeringprice']['specification']['specid']) : '';
            $charvalid = !empty($charge_details['offeringprice']['specification']['charvalid']) ? $charge_details['offeringprice']['specification']['charvalid'] : array();
            $mapping   = array(
                'pricelineid' => $pricelineid2,
                'currency'    => isset($charge_details['currencyid']) ? trim($charge_details['currencyid']) : '',
            );

            if (is_array($charvalid)) {
                $mapping['charvalid'] = $charvalid;
            } elseif (!empty($charvalid)) {
                $mapping['charvalid'] = array($charvalid);
            } else {
                $mapping['charvalid'] = array();
            }

            $product_offering_details_array['charge_mapping'][$specid][]                      = $mapping;
            $product_offering_details_array['charge_details'][$pricelineid2]['offeringprice'] = isset($charge_details['offeringprice']['rate']) ? trim($charge_details['offeringprice']['rate']) : '';
        }

        // Get specifications block
        // To handle the two type of xml formatting
        if (isset($arr_result['listcatalogofferingdetailresponse']['listcatalogoffering']['offerings']['offering']['specifications']['specification'][0])) {
            $specification_blocks = $arr_result['listcatalogofferingdetailresponse']['listcatalogoffering']['offerings']['offering']['specifications']['specification'];
        } else {
            $specification_blocks[] = $arr_result['listcatalogofferingdetailresponse']['listcatalogoffering']['offerings']['offering']['specifications']['specification'];
        }

        foreach ($specification_blocks as $specification_block) {

            $specid                            = $specification_block['specid']; // Get specification id
            $speccode                          = $specification_block['speccode']; // Get specification code
            $specname                          = $specification_block['specname']; // Get specification name
            $spectypeid                        = $specification_block['spectypeid']; // Get specification type id
            $specification_type_list           = $GLOBALS['app_list_strings']['specification_type_list']; // Get specification type name
            $specification_type_mandatory_list = $GLOBALS['app_list_strings']['specification_type_mandatory_list']; // Get specification type mandatory value

            $product_offering_details_array['specification_details'][$specid]['specname']           = $specname; // Populate specification name in array
            $product_offering_details_array['specification_details'][$specid]['spectypeid']         = $spectypeid; // Populate specification type id in array
            $product_offering_details_array['specification_details'][$specid]['speccode']           = $speccode; // Populate specification code in array
            $product_offering_details_array['specification_details'][$specid]['spectypename']       = $specification_type_list[$spectypeid]; // Populate specification type name in array
            $product_offering_details_array['specification_details'][$specid]['spectype_mandatory'] = $specification_type_mandatory_list[$spectypeid]; // Populate specification type mandatory value

            // To handle the two type of xml formatting
            $characteristic_blocks = array();
            if (isset($specification_block['characteristics']['characteristic'][0])) {
                $characteristic_blocks = $specification_block['characteristics']['characteristic'];
            } else if (isset($specification_block['characteristics']['characteristic'])) {
                $characteristic_blocks[] = $specification_block['characteristics']['characteristic'];
            }
            if (!empty($characteristic_blocks)) {
                foreach ($characteristic_blocks as $characteristic_block) {
                    $sub_char_id   = $characteristic_block['charid']; // Get sub characteristic id
                    $sub_char_name = $characteristic_block['charname']; // Get sub characteristic name
                    $sub_char_code = $characteristic_block['charcode']; // Get sub characteristic code
                    $ismandatory   = $characteristic_block['ismandatory']; // Get ismandatory value

                    $product_offering_details_array['specification_details'][$specid]['characteristics'][$sub_char_id]['charname']    = $sub_char_name; // Populate classification in array
                    $product_offering_details_array['specification_details'][$specid]['characteristics'][$sub_char_id]['charcode']    = $sub_char_code; // Populate characteristic code in array
                    $product_offering_details_array['specification_details'][$specid]['characteristics'][$sub_char_id]['ismandatory'] = $ismandatory; // Populate ismandatory in array

                    // Get characteristicvalue blocks
                    // $characteristicvalue_blocks = $characteristic_block['characteristicvalues']['characteristicvalue'];

                    // To handle the two type of xml formatting
                    $characteristicvalue_blocks = array();
                    if (isset($characteristic_block['characteristicvalues']['characteristicvalue'][0])) {
                        $characteristicvalue_blocks = !empty($characteristic_block['characteristicvalues']['characteristicvalue']) ? $characteristic_block['characteristicvalues']['characteristicvalue'] : array();
                    } else {
                        $characteristicvalue_blocks[] = $characteristic_block['characteristicvalues']['characteristicvalue'];
                    }

                    foreach ($characteristicvalue_blocks as $characteristicvalue_record) {

                        //$characteristicvalue_record = array();
                        //if (is_array($characteristicvalue_block))
                        //    $characteristicvalue_record = $characteristicvalue_block;
                        //else $characteristicvalue_record = $characteristicvalue_blocks;

                        $charvalid   = !empty($characteristicvalue_record['charvalid']) ? $characteristicvalue_record['charvalid'] : ''; // Get charvalid
                        $displaytext = !empty($characteristicvalue_record['displaytext']) ? $characteristicvalue_record['displaytext'] : ''; // Get displaytext
                        $valtypeid   = !empty($characteristicvalue_record['valtypeid']) ? $characteristicvalue_record['valtypeid'] : ''; // Get valtypeid
                        $isdefault   = !empty($characteristicvalue_record['isdefault']) ? $characteristicvalue_record['isdefault'] : ''; // Get isdefault

                        // Populate valtypeid values in array
                        $product_offering_details_array['specification_details'][$specid]['characteristics'][$sub_char_id]['valtypeid'] = $valtypeid;
                        $product_offering_details_array['specification_details'][$specid]['characteristics'][$sub_char_id]['charvalid'] = 0;

                        // Populate classification values in array
                        if (!empty($charvalid) && $valtypeid == 1) {
                            $product_offering_details_array['specification_details'][$specid]['characteristics'][$sub_char_id]['characteristicvalue'][$charvalid]['id']          = $charvalid;
                            $product_offering_details_array['specification_details'][$specid]['characteristics'][$sub_char_id]['characteristicvalue'][$charvalid]['displaytext'] = $displaytext;
                            $product_offering_details_array['specification_details'][$specid]['characteristics'][$sub_char_id]['characteristicvalue'][$charvalid]['isdefault']   = $isdefault;
                        }

                        // Create array for range field.
                        if ($valtypeid == 2) {
                            $lowervalue = isset($characteristicvalue_record['lowervalue']) ? $characteristicvalue_record['lowervalue'] : '';
                            $uppervalue = isset($characteristicvalue_record['uppervalue']) ? $characteristicvalue_record['uppervalue'] : '';
                            $interval   = isset($characteristicvalue_record['interval']) ? $characteristicvalue_record['interval'] : '';
                            // To make sure the coding will not break for the large values like 999999 increments
                            /*if(($uppervalue - $lowervalue) /  $interval > 999){
                            $uppervalue = $lowervalue + ($interval * 999);
                            }*/

                            $product_offering_details_array['specification_details'][$specid]['characteristics'][$sub_char_id]['charvalid']  = $charvalid;
                            $product_offering_details_array['specification_details'][$specid]['characteristics'][$sub_char_id]['lowervalue'] = $lowervalue;
                            $product_offering_details_array['specification_details'][$specid]['characteristics'][$sub_char_id]['uppervalue'] = $uppervalue;
                            $product_offering_details_array['specification_details'][$specid]['characteristics'][$sub_char_id]['interval']   = $interval;

                        }
                    }
                    //unset($characteristicvalue_record);
                }
            }

            if (isset($specification_block['rules'])) {

                // To handle the two type of xml formatting
                $rule_blocks = array();
                if (isset($specification_block['rules']['rule'][0])) {
                    $rule_blocks = !empty($specification_block['rules']['rule']) ? $specification_block['rules']['rule'] : '';
                } else {
                    $rule_blocks[] = !empty($specification_block['rules']['rule']) ? $specification_block['rules']['rule'] : '';
                }

                foreach ($rule_blocks as $rule_block) {
                    $rule_id                                                                                            = !empty($rule_block['ruleid']) ? $rule_block['ruleid'] : '';
                    $product_offering_details_array['specification_details'][$specid]['rules'][$rule_id]['id']          = $rule_id;
                    $product_offering_details_array['specification_details'][$specid]['rules'][$rule_id]['rulename']    = !empty($rule_block['rulename']) ? $rule_block['rulename'] : '';
                    $product_offering_details_array['specification_details'][$specid]['rules'][$rule_id]['ismandatory'] = isset($rule_block['ismandatory']) ? $rule_block['ismandatory'] : 'false';
                    $product_offering_details_array['specification_details'][$specid]['rules'][$rule_id]['rulecode']    = !empty($rule_block['rulecode']) ? $rule_block['rulecode'] : '';
                    $product_offering_details_array['specification_details'][$specid]['rules'][$rule_id]['ruledesc']    = !empty($rule_block['ruledesc']) ? $rule_block['ruledesc'] : '';
                    $product_offering_details_array['specification_details'][$specid]['rules'][$rule_id]['placeholder'] = !empty($rule_block['placeholder']) ? $rule_block['placeholder'] : '';
                }

            } else {
                $product_offering_details_array['specification_details'][$specid]['rules'] = array();
            }

        }

        $listofferingoption_lists = (isset($arr_result['listcatalogofferingdetailresponse']['listofferingoption']['options']['option'])) ? $arr_result['listcatalogofferingdetailresponse']['listofferingoption']['options']['option'] : array();
        if (!empty($listofferingoption_lists)) {
            foreach ($listofferingoption_lists as $listofferingoption_list_value) {
                $charge_record_bill_cycle = array();
                if (is_array($listofferingoption_list_value)) {
                    $charge_record_bill_cycle = $listofferingoption_list_value;
                } else {
                    $charge_record_bill_cycle = $listofferingoption_lists;
                }

                $product_offering_details_array['listofferingoption'][$charge_record_bill_cycle['key']] = !empty($charge_record_bill_cycle['value']) ? $charge_record_bill_cycle['value'] : '';
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_offering_details_array : ' . print_r($product_offering_details_array, true), '');
        return ModUtils::object_to_array($product_offering_details_array);
    }

    /**
     * Method to call API
     *
     * @param string $relativeUri
     * @param string relative url
     * @return xml
     * @author Dinesh.Itkar, Arunsakthivel
     * @since Mar 16, 2016
     */
    private function apiProductConfiguration($relativeUri, $query)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'relativeUri : ' . $relativeUri . GCM_GL_LOG_VAR_SEPARATOR . 'relativeUri : ' . $relativeUri, 'Method to call API');
        global $sugar_config;
        $pms_product_base_url   = !empty($sugar_config['pms_product']['base_url']) ? $sugar_config['pms_product']['base_url'] : '';
        $pms_product_public_key = !empty($sugar_config['pms_product']['public_key']) ? $sugar_config['pms_product']['public_key'] : '';
        $pms_product_secret_key = !empty($sugar_config['pms_product']['secret_key']) ? $sugar_config['pms_product']['secret_key'] : '';

        $timestamp        = date('D, d M Y H:i:s O', time()); // echo $timestamp;
        $httpVerb         = 'GET';
        $absolutePathRoot = '/pms/api';

        // Signature
        $stringToSign = $pms_product_public_key . '$' . $httpVerb . '$' . $absolutePathRoot . '$' . $relativeUri . '$' . $timestamp;
        $signature    = base64_encode(hash_hmac('sha1', $stringToSign, $pms_product_secret_key, true));

        $api_key = $pms_product_public_key . ':' . $signature;

        // HTTP Request(Call API)
        $curl_url   = $sugar_config['pms_product']['base_url'] . $absolutePathRoot . $relativeUri . (strlen($query) > 0 ? '?' : '') . $query;
        $httpClient = curl_init($curl_url);
        // curl_setopt($httpClient, CURLOPT_HEADER, true); // If this flag is true, header information is included in response string.
        curl_setopt($httpClient, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($httpClient, CURLOPT_HTTPHEADER, array(
            'api-key: ' . $api_key,
            'x-mp-date: ' . $timestamp,
        ));

        if (substr(PHP_OS, 0, 3) == 'WIN') {
            // Windows PHP が内包するCA情報は古いので、SSL無視のオプションを指定
            curl_setopt($httpClient, CURLOPT_SSL_VERIFYPEER, false);
        }

        $curl_response = curl_exec($httpClient);
        $GLOBALS['log']->info("Info: PMS url : " . $curl_url);
        if (curl_error($httpClient)) {
            $GLOBALS['log']->info("Error: Curl error with PMS system. Errorno: " . curl_errno($httpClient) . " Error: " . curl_error($httpClient));
            $response_code = 0;
            $curl_response = "";
        } else {
            // TODO handle Http Info error
            $response_code = 1;
            $curl_info     = curl_getinfo($httpClient);
            if ($curl_info['http_code'] != 200) {
                $response_code = 0;
                $GLOBALS['log']->info("Error: HTTP ERROR occured with PMS system. HTTP COde: " . $curl_info['http_code'] . " Error: " . $curl_response);
                $curl_response = "";
            }
        }

        $responseContents[$this->response_code_key] = $response_code;
        $responseContents[$this->respose_xml_key]   = $curl_response;
        curl_close($httpClient);

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'responseContents : ' . print_r($responseContents, true), '');
        return $responseContents;

    }

    /**
     * xml to array convertion
     *
     * @param xml $root
     * @return array
     */
    private function xml_to_array($root)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'root : ' . print_r($root, true), 'xml to array convertion');
        $result = array();
        if ($root->hasAttributes()) {
            $attrs = $root->attributes;
            foreach ($attrs as $attr) {
                $result['@attributes'][$attr->name] = $attr->value;
            }
        }
        if ($root->hasChildNodes()) {
            $children = $root->childNodes;
            if ($children->length == 1) {
                $child = $children->item(0);
                if ($child->nodeType == XML_TEXT_NODE) {
                    $result['_value'] = $child->nodeValue;
                    ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'result : ' . print_r($result, true), '');
                    return count($result) == 1 ? $result['_value'] : $result;
                }
            }
            $groups = array();
            foreach ($children as $child) {
                if (!isset($result[$child->nodeName])) {
                    $result[$child->nodeName] = $this->xml_to_array($child);
                } else {
                    if (!isset($groups[$child->nodeName])) {
                        $result[$child->nodeName] = array(
                            $result[$child->nodeName],
                        );
                        $groups[$child->nodeName] = 1;
                    }
                    $result[$child->nodeName][] = $this->xml_to_array($child);
                }
            }
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'result : ' . print_r($result, true), '');
        return ModUtils::object_to_array($result);
    }
}
