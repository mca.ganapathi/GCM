<?php
if (!defined('sugarEntry'))
    define('sugarEntry', true);
require_once ('service/v4_1/SugarWebServiceImplv4_1.php');
require_once ('custom/service/v4_1_custom/SugarWebServiceUtilv4_1_custom.php');
require_once ('custom/include/XMLUtils.php');
require_once 'custom/service/v4_1_custom/APIHelper/ClassProductConfiguration.php';

/**
 * API Implementation file to handle the request, parsing, business validation and respond with the data
 * @type unix-file system UTF-8
 */
class SugarWebServiceImplv4_1_custom extends SugarWebServiceImplv4
{

    var $paths;

    var $exceptions;

    var $xmlerrors;

    const INVALID_SESSION = 'invalid_session';

    const KEY_RESPONSE_CODE = 'ResponseCode';

    const KEY_SERVICE_RESPONSE_CODE = 'ServiceResponseCode';

    const KEY_CIDAS_FAILED = 'cidas_failed';

    const XSD_ERROR_CODE = '3006';

    private $objSugarServiceMagicBridge;

    public function __construct()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
        self::$helperObject = new SugarWebServiceUtilv4_1_custom();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', '');
    }

    /**
     * Method to validate the session id is correct or wrong
     *
     * @param string $session_id session id sent in the request
     * @return boolean
     */
    function fnValidateSession($session_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'session_id: ' . $session_id, 'Method to validate the session id is correct or wrong');
        $return = false;
        // authenticate
        if (!self::$helperObject->checkSessionAndModuleAccess($session_id, self::INVALID_SESSION, '', '', '', null)) {
            $return = false;
        } else {
            // DF-1472 Enable Product - Workflow Configuration from Admin screen
            ModUtils::setGlobalConfigWorkflowOfferings();
            $return = true;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'return: ' . $return, '');
        return $return;
    }

    /**
     * Method to retrieve the object of magice bridge between API and System
     *
     * @param int $version API Version
     * @return object
     */
    private function getSugarServiceBridgeObject($version)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'version: ' . $version, 'Method to retrieve the object of magice bridge between API and System');
        $class_name = 'SugarServiceMagicBridge_v' . $version;

        if (file_exists("custom/service/v4_1_custom/APIHelper/v$version/$class_name.php")) {
            include_once ("custom/service/v4_1_custom/APIHelper/v$version/$class_name.php");
            $obj_service_bridge = new $class_name();
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', "$class_name class object created.");
            return $obj_service_bridge;
        }

        include_once ("custom/service/v4_1_custom/APIHelper/v1/SugarServiceMagicBridge_v1.php");
        $obj_service_bridge = new SugarServiceMagicBridge_v1();
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'SugarServiceMagicBridge_v1 class object created.');
        return $obj_service_bridge;
    }

    /**
     * Create Contract through API V1 POST Create
     *
     * @param string $session_id
     * @param string $contract_xml_data Requested XML in string format
     * @return string Response of created contract details in XML format
     * @throws string Common Error Exception XML
     */
    function fnContractRegistration($session_id, $contract_xml_data)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'session_id: ' . $session_id . GCM_GL_LOG_VAR_SEPARATOR . 'contract_xml_data: ' . $contract_xml_data, 'Method to Create Contract through API V1 POST Create');
        $this->objSugarServiceMagicBridge = self::getSugarServiceBridgeObject(1);

        $xml_utils_obj = new XMLUtils();
        libxml_use_internal_errors(true);
        $this->paths = array();
        $this->exceptions = array();
        $this->xmlerrors = array();
        $arr_error_code = array();
        // Session Validation
        $session_validation = ($this->fnValidateSession($session_id)) ? 1 : 0;

        if ($session_validation == 1 && !empty($contract_xml_data)) {
            $dom_obj = new DOMDocument();
            $dom_obj->loadXML($contract_xml_data);
            // XSD Validation
            if (!$dom_obj->schemaValidate('custom/XMLSchema/Operation/ContractInformation/ContractInformationRequest-v0_1.xsd')) {
                $this->xmlerrors = $xml_utils_obj->libxmlDisplayErrors();
                $err_lists = $this->xmlerrors;
                $str_err_list = '';
                if (is_array($err_lists) && !empty($err_lists)) {
                    foreach ($err_lists as $err_list) {
                        $str_err_list .= $err_list;
                        $str_err_list .= '|';
                    }
                }
                $res_xml = $xml_utils_obj->fnGetErrorXMLData(array(
                                                                    '3004'
                ), $str_err_list, false);
            } else {
                // Parse XML into Array
                require_once ('custom/service/v4_1_custom/APIHelper/v1/ClassContractXMLParse.php');
                $contract_xml_parse_obj = new ClassContractXMLParse();
                $request_data = $contract_xml_parse_obj->getContractParseArray($contract_xml_data);
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'request_data: ' . print_r($request_data, true), 'Contract Add Array Values End');
                if (!empty($request_data)) {
                    if (isset($request_data['Corporates']['common_customer_id_c']) && !empty($request_data['Corporates']['common_customer_id_c'])) {
                        // Push Array in Sugar Modules
                        global $current_user;
                        $res_xml = $this->objSugarServiceMagicBridge->objContractService->setContractEntryService($current_user, $request_data);
                        // If Records created then create Success Response XML
                        if (isset($res_xml[self::KEY_RESPONSE_CODE]) && $res_xml[self::KEY_RESPONSE_CODE] == 1) {
                            $res_xml = $this->objSugarServiceMagicBridge->objPostResponseXML->getContractPostResponseXML($contract_xml_data, $res_xml['ContractDetails']);
                        } else {
                            $res_xml = $xml_utils_obj->fnGetErrorXMLData(array(
                                                                                isset($res_xml[self::KEY_SERVICE_RESPONSE_CODE]) ? $res_xml[self::KEY_SERVICE_RESPONSE_CODE] : ''
                            ), null, false);
                        }
                    } else {
                        $res_xml = $request_data;
                    }
                } else {
                    $res_xml = $xml_utils_obj->fnGetErrorXMLData(array(
                                                                        '2001'
                    ), null, false);
                }
            }
        } else {
            $arr_error_code[] = ($session_validation != 1) ? '3001' : '3002';
            $res_xml = $xml_utils_obj->fnGetErrorXMLData($arr_error_code, null, false);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'res_xml: ' . print_r($res_xml, true), '');
        return $res_xml;
    }

    /**
     * DELETE Contract through API V1 DELETE
     *
     * @param string $session_id
     * @param string $contract_id
     * @return none if deleted
     * @throws string Common Error Exception XML
     */
    function fnContractDeactivate($session_id, $contract_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'session_id: ' . $session_id . GCM_GL_LOG_VAR_SEPARATOR . 'contract_id: ' . $contract_id, 'Method to DELETE Contract through API V1 DELETE');
        $this->objSugarServiceMagicBridge = self::getSugarServiceBridgeObject(1);

        $xml_utils_obj = new XMLUtils();
        $session_validation = ($this->fnValidateSession($session_id)) ? 1 : 0;
        if ($session_validation == 1 && !empty($contract_id)) {
            $res_deactivate_contract = $this->objSugarServiceMagicBridge->objContractService->deactivateContractEntryService($contract_id);
            if (isset($res_deactivate_contract[self::KEY_RESPONSE_CODE]) && $res_deactivate_contract[self::KEY_RESPONSE_CODE] == 1) {
                return;
            } else {
                $res_xml = $xml_utils_obj->fnGetErrorXMLData(array(
                                                                    isset($res_deactivate_contract[self::KEY_SERVICE_RESPONSE_CODE]) ? $res_deactivate_contract[self::KEY_SERVICE_RESPONSE_CODE] : ''
                ), null, false);
            }
        } else {
            $arr_error_code[] = ($session_validation != 1) ? '3001' : '2005';
            $res_xml = $xml_utils_obj->fnGetErrorXMLData($arr_error_code, null, false);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'res_xml: ' . print_r($res_xml, true), '');
        return $res_xml;
    }

    /**
     * Get OAuth reqtest token
     */
    public function oauth_request_token()
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, '', 'Get OAuth reqtest token');
        require_once "include/SugarOAuthServer.php";
        try {
            $oauth = new SugarOAuthServer(rtrim($GLOBALS['sugar_config']['site_url'], '/') . '/custom/service/v4_1_custom/rest.php');
            $result = $oauth->requestToken() . "&oauth_callback_confirmed=true&authorize_url=" . $oauth->authURL();
        } catch (OAuthException $e) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'e: ' . print_r($e, true), 'OAUTH Exception');
            $errorObject = new SoapError();
            $errorObject->set_error('invalid_login');
            self::$helperObject->setFaultObject($errorObject);
            $result = null;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'result: ' . print_r($result, true), '');
        return $result;
    }

    /**
     * Browse all the contract data against supplied objectid or contract id thorugh API V1 GET
     *
     * @param string $session_id Session ID to validate & authenticate logged in user
     * @param string $contract_id SugarBean ID of Contracts module
     * @param string $object_id Global Contract Id of Contracts module
     * @return string Contract details XML
     * @throws string Common Error Exception XML
     */
    function fnContractBrowse($session_id, $contract_id, $object_id)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'session_id: ' . $session_id . GCM_GL_LOG_VAR_SEPARATOR . 'contract_id: ' . $contract_id . GCM_GL_LOG_VAR_SEPARATOR . 'object_id: ' . $object_id, 'Browse all the contract data against supplied objectid or contract id thorugh API V1 GET');
        $data = null;
        $arr_error_code = array();
        $xml_utils_obj = new XMLUtils();
        if (!self::$helperObject->checkSessionAndModuleAccess($session_id, self::INVALID_SESSION, '', '', '', null)) {
            $arr_error_code[] = '3001';
        } else {
            if ($object_id != null || $contract_id != null) {
                require_once ('custom/service/v4_1_custom/APIHelper/v1/ClassContractBrowse.php');
                $browse_contract = new ClassContractBrowse();
                $browse_contract->initDom();
                // $browse_contract->validateSchema();
                $data = $browse_contract->getContractXML($object_id, $contract_id);
                if (is_array($data)) {
                    foreach ($data as $e) {
                        $arr_error_code[] = $e;
                    }
                }
            } else {
                $arr_error_code[] = '2005';
            }
        }
        if (sizeof($arr_error_code) > 0) {
            $data = $xml_utils_obj->fnGetErrorXMLData($arr_error_code, null, false);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'data: ' . print_r($data, true), '');
        return $data;
    }

    /**
     * Update the contract details against supplied objectid thorugh API V1 PATCH
     *
     * @param string $session_id Session ID to validate & authenticate logged in user
     * @param string $contract_obj_id SugarBean Object ID of Contracts module
     * @param string $contract_update_xml_data Updating field details
     * @return none if updated
     * @throws string Common Error Exception XML
     */
    function fnContractUpdate($session_id, $contract_obj_id, $contract_update_xml_data)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'session_id: ' . $session_id . GCM_GL_LOG_VAR_SEPARATOR . 'contract_obj_id: ' . $contract_obj_id . GCM_GL_LOG_VAR_SEPARATOR . 'contract_update_xml_data: ' . $contract_update_xml_data, 'Update the contract details against supplied objectid thorugh API V1 PATCH');
        $this->objSugarServiceMagicBridge = self::getSugarServiceBridgeObject(1);

        $xml_utils_obj = new XMLUtils();
        libxml_use_internal_errors(true);
        $this->paths = array();
        $this->exceptions = array();
        $this->xmlerrors = array();
        $arr_error_code = array();
        // Session Validation
        $session_validation = ($this->fnValidateSession($session_id)) ? 1 : 0;

        if ($session_validation == 1 && !empty($contract_update_xml_data)) {
            $dom_obj = new DOMDocument();
            $dom_obj->loadXML($contract_update_xml_data);

            // XSD Validation
            if (!$dom_obj->schemaValidate('custom/XMLSchema/Operation/ContractInformation/ContractInformationRequest-v0_1.xsd')) {
                $this->xmlerrors = $xml_utils_obj->libxmlDisplayErrors();
                $err_lists = $this->xmlerrors;
                $str_err_list = '';
                if (is_array($err_lists) && !empty($err_lists)) {
                    foreach ($err_lists as $err_list) {
                        $str_err_list .= $err_list;
                        $str_err_list .= '|';
                    }
                }
                $res_xml = $xml_utils_obj->fnGetErrorXMLData(array(
                                                                    '3004'
                ), $str_err_list, false);
            } else {
                // Parse XML into Array
                require_once ('custom/service/v4_1_custom/APIHelper/v1/ClassContractUpdateXMLParse.php');
                $contract_update_xml_parse = new ClassContractUpdateXMLParse();
                $request_data = $contract_update_xml_parse->getContractParseArray($contract_update_xml_data);

                $error_occured = (is_string($request_data)) ? mb_strpos($request_data, 'exp-v0:exception') : false;
                if ($error_occured) {
                    return $request_data;
                }

                $request_data['ContractObjectId'] = $contract_obj_id;
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'request_data: ' . print_r($request_data, true), 'Contract Update Array Values');
                if (!empty($request_data)) {
                    global $current_user;
                    $res_update_contract = $this->objSugarServiceMagicBridge->objContractService->updateContractEntryService($current_user, $request_data);
                    if (isset($res_update_contract[self::KEY_RESPONSE_CODE]) && $res_update_contract[self::KEY_RESPONSE_CODE] == 1) {
                        return;
                    } else {
                        $res_xml = $xml_utils_obj->fnGetErrorXMLData(array(
                                                                            $res_update_contract[self::KEY_SERVICE_RESPONSE_CODE]
                        ), null, false);
                    }
                }
            }
        } else {
            $arr_error_code[] = ($session_validation != 1) ? '3001' : '3002';
            $res_xml = $xml_utils_obj->fnGetErrorXMLData($arr_error_code, null, false);
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'res_xml: ' . print_r($res_xml, true), '');
        return $res_xml;
    }

    /**
     * Create Contract through API V2 POST
     *
     * @param string $session_id Session ID to validate & authenticate logged in user
     * @param string $contract_xml_data POST XML
     * @param string $object_id Global Contract ID UUID of Contracts module
     * @param string $contract_id Global Contract ID of Contracts module
     * @param string $version_number Contract Version
     * @return string POST Response XML
     * @throws string Common Error Exception XML
     */
    function fnContractRegistrationV2($session_id, $contract_xml_data, $object_id, $contract_id, $version_number)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'session_id: ' . $session_id . GCM_GL_LOG_VAR_SEPARATOR . 'contract_xml_data: ' . $contract_xml_data . GCM_GL_LOG_VAR_SEPARATOR . 'object_id: ' . $object_id . GCM_GL_LOG_VAR_SEPARATOR . 'contract_id: ' . $contract_id . GCM_GL_LOG_VAR_SEPARATOR . 'version_number: ' . $version_number, 'Create Contract through API V2 POST');

        global $current_user;

        //Validation Part
        require_once __DIR__ . '/APIHelper/v2/ValidationLayer.php';
        $validation = new ValidationLayer();
        if(!empty($object_id) || !empty($contract_id)) {
            $layer_data = $validation->validateVersionUpPOSTXMLData($session_id, $contract_xml_data, $object_id, $contract_id, $version_number);
        } else {
            $layer_data = $validation->validatePOSTXMLData($session_id, $contract_xml_data, $object_id, $contract_id, $version_number);
        }

        if($layer_data!=false) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'res_error_xml: ' . print_r($layer_data, true), '');
            return $layer_data;
        }

        $this->objSugarServiceMagicBridge = self::getSugarServiceBridgeObject(2);
        // save part goes here
        $res_xml = $this->objSugarServiceMagicBridge->objContractService->setContractEntryService($current_user, $validation->request_data);

        // If Records created then create Success Response XML
        if (isset($res_xml[self::KEY_RESPONSE_CODE]) && $res_xml[self::KEY_RESPONSE_CODE] == 1) {
            $res_xml = $this->objSugarServiceMagicBridge->objPostResponseXML->getContractPostResponseXML($contract_xml_data, $res_xml['ContractDetails']);
        }

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'res_xml: ' . print_r($res_xml, true), '');
        return $res_xml;
    }

    /**
     * Browse Contract Details against supplied objectid or contract id through API V2 POST
     *
     * @param string $session_id Session ID to validate & authenticate logged in user
     * @param string $object_id Global Contract ID UUID of Contracts module
     * @param string $contract_id Global Contract ID of Contracts module
     * @param string $version_number Contract Version
     * @param string $interaction_status Contract Interaction Status
     * @return string Respective Contract GET Response XML
     * @throws string Common Error Exception XML
     */
    function fnContractBrowseV2($session_id, $object_id, $contract_id, $version_number, $interaction_status)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'session_id: ' . $session_id . GCM_GL_LOG_VAR_SEPARATOR . 'object_id: ' . $object_id . GCM_GL_LOG_VAR_SEPARATOR . 'contract_id: ' . $contract_id . GCM_GL_LOG_VAR_SEPARATOR . 'version_number: ' . $version_number . GCM_GL_LOG_VAR_SEPARATOR . 'interaction_status: ' . $interaction_status, 'Browse Contract Details against supplied objectid or contract id through API V2 POST');
        require_once __DIR__ . '/APIHelper/v2/ValidationLayer.php';
        $validation = new ValidationLayer();
        $success = $validation->validateGETXMLData($session_id, $object_id, $contract_id, $version_number, $interaction_status);
        // return error xml if validation failed
        if($success != false) {
            return $success;
        }
        // fetch db data and conver as response xml fromat
        require_once ('custom/service/v4_1_custom/APIHelper/v2/ClassContractBrowse.php');
        $browse_contract = new ClassContractBrowse();
        $get_response_data = $browse_contract->getContractXML($object_id, $contract_id, $version_number, $interaction_status);

        // xsd validation
        $success = $validation->validateResponseXMLData($get_response_data['respone_xml_data'], 'get');
        // return error xml if xsd validation failed
        if($success != false) {
            $GLOBALS['log']->error('XML contrary to the XSD constraint is below
' . print_r($get_response_data['respone_xml_data'],true));
            return $success;
        }
        return $get_response_data['respone_xml_data'];
    }

    /**
     * Update Contract details against supplied object id or contract id throgh API V2 PATCH
     *
     * @param string $session_id Session ID to validate & authenticate logged in user
     * @param string $contract_update_xml_data PATCH XML
     * @param string $object_id Global Contract ID UUID of Contracts module
     * @param string $contract_id Global Contract ID of Contracts module
     * @param string $version_number Contract Version
     * @return none
     * @throws string Common Error Exception XML
     */
    function fnContractUpdateV2($session_id, $contract_update_xml_data, $object_id, $contract_id, $version_number)
    {
        global $is_api_call;
        $is_api_call = true;

        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'session_id: ' . $session_id . GCM_GL_LOG_VAR_SEPARATOR . 'contract_update_xml_data: ' . $contract_update_xml_data . GCM_GL_LOG_VAR_SEPARATOR . 'object_id: ' . $object_id . GCM_GL_LOG_VAR_SEPARATOR . 'contract_id: ' . $contract_id . GCM_GL_LOG_VAR_SEPARATOR . 'version_number: ' . $version_number, 'Update Contract details against supplied object id or contract id throgh API V2 PATCH');
        $this->objSugarServiceMagicBridge = self::getSugarServiceBridgeObject(2);

        require_once __DIR__ . '/APIHelper/v2/ValidationLayer.php';
        $validation = new ValidationLayer();
        $layer_data = $validation->validatePATCHXMLData($session_id, $contract_update_xml_data, $contract_id ,$object_id, $version_number);

        if(empty($layer_data)){
            global $current_user;
            global $patch_parse_arr;
            $res_xml = $this->objSugarServiceMagicBridge->objContractService->updateContractEntryService($current_user, $validation->request_data);
        }
        // save part goes here

        //ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'res_xml: ' . print_r($res_xml, true), '');
        return $layer_data;
    }

    /**
     * Browse Contract/Account Details against supplied account type and dates through API V2
     *
     * @param string $session_id Session ID to validate & authenticate logged in user
     * @param string $accountType Request Type (billing-account or contract-account)
     * @param string $dateAfter Start date of filter
     * @param string $dateBefore End date of filter
     * @return string Respective GBS GET Response in CSV format
     * @throws string Common Error Exception XML
     */
    function fnGBSContractBrowse($session_id, $accountType, $dateAfter, $dateBefore)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'session_id: ' . $session_id . GCM_GL_LOG_VAR_SEPARATOR . 'accountType: ' . $accountType . GCM_GL_LOG_VAR_SEPARATOR . 'dateAfter: ' . $dateAfter . GCM_GL_LOG_VAR_SEPARATOR . 'dateBefore: ' . $dateBefore, 'Browse Contract/Account Details against supplied account type and dates through API V2');
        require_once __DIR__ . '/APIHelper/v2/ValidationLayer.php';
        $validation = new ValidationLayer();
        $success = $validation->validateGBSXMLData($session_id, $accountType, $dateAfter, $dateBefore);
        if($success != false) {
            return $success;
        }
        global $sugar_config;
        // save part goes here
        if ($accountType == 'billing-account') {
            $pms_details = $this->getProductOfferingDetailsList($sugar_config['gbs_api_po']);
        } else {
            $pms_details = array('result' => 'success');
        }
        //ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'pms_details: ' . print_r($pms_details, true), 'Product offering details list');
        if (isset($pms_details['result']) && strtolower(trim($pms_details['result'])) == 'success') {
            // Fetch the details
            require_once ('custom/modules/gc_Contracts/include/ClassReportServiceGcContracts.php');
            $browse_contract = new ClassReportServiceGcContracts();
            $params = array(
                'export_key' => 'export-' . $accountType . 's-for-gbs',
                'filter' => array(
                    'date-after' => $dateAfter,
                    'date-before' => $dateBefore
                )
            );
            // include PMS details only if type is billing
            if ($accountType == 'billing-account') {
                $params['product_offering_dtls'] = $pms_details['respose_array'];
            }
            //ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'params: ' . print_r($params, true), 'Parameters passed to generate report');

            $arr_resp_data = $browse_contract->generateReport($params);
            $data = $arr_resp_data;
            //ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'data: ' . print_r($data, true), 'Generated report array');
        } else {
            //$arr_error_code[] = '8004';
        }

        //ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'res_xml: ' . print_r($res_xml, true), '');
        return $data;
    }

    /**
     * Fetch product offering details from PMS
     *
     * @param array $product_offering_list List of product offering ids
     * @return array product offering details fetched from PMS
     */
    private function getProductOfferingDetailsList($product_offering_list)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'product_offering_list: ' . print_r($product_offering_list, true), 'Fetch product offering details from PMS');
        $res_product_offering_dtls = $result = array();
        $has_error = false;
        if (!is_array($product_offering_list) || empty($product_offering_list)) {
            $has_error = true;
        }

        if (!$has_error) {
            $obj_product_conf = new ClassProductConfiguration();
            if (!empty($product_offering_list)) {
                foreach ($product_offering_list as $key => $product_offering) {
                    $res = $obj_product_conf->getProductOfferingDetails($product_offering);
                    if (isset($res['response_code']) && $res['response_code']) {
                        $res_product_offering_dtls[$product_offering] = $res['respose_array'];
                    } else { /* send error code */
                        $has_error = true;
                        break;
                    }
                }
            }
        }
        if ($has_error) {
            $result['result'] = 'failure';
        } else {
            $result['result'] = 'success';
            $result['respose_array'] = $res_product_offering_dtls;
        }
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'result: ' . print_r($result, true), '');
        return $result;
    }

    /**
     * Browse Product Contract against supplied parameters
     *
     * @param string $session_id Session ID to validate & authenticate logged in user
     * @param string $contractCustomerId CID of a corporate
     * @param string $contractProductId Product offering id along with object-version(always '1')
     * @param string $updatedAfter Date to filter from
     * @param string $contractStatus Status of contract
     * @param string $versionFlag All-Version flag (Boolean 0 or 1)
     * @return string XML Respective GET Response
     * @throws string Common Error Exception XML
     */
    function fnProductContractBrowse($sessionId, $contractCustomerId, $contractProductId, $updatedAfter, $contractStatus, $versionFlag)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'sessionId: ' . $sessionId . GCM_GL_LOG_VAR_SEPARATOR . 'contractCustomerId: ' . $contractCustomerId . GCM_GL_LOG_VAR_SEPARATOR . 'contractProductId: ' . $contractProductId . GCM_GL_LOG_VAR_SEPARATOR . 'updatedAfter: ' . $updatedAfter . GCM_GL_LOG_VAR_SEPARATOR . 'contractStatus: ' . $contractStatus . GCM_GL_LOG_VAR_SEPARATOR . 'versionFlag: ' . $versionFlag, 'Browse Contract/Account Details against supplied account type and dates through API V2');
        require_once __DIR__ . '/APIHelper/v2/ValidationLayer.php';
        $validation = new ValidationLayer();
        $success = $validation->validateIDWHXMLData($sessionId, $contractCustomerId, $contractProductId, $updatedAfter, $contractStatus, $versionFlag);
        if($success != false) {
            return $success;
        }

        require_once ('custom/modules/gc_Contracts/include/ClassContractListDetailsService.php');
        $browse_contract = new ClassContractListDetailsService();
        if (trim($updatedAfter) != '') {
            require_once 'custom/include/ModUtils.php';
            $modUtils = new ModUtils();
            $updatedAfter = str_replace(' ', '+', $updatedAfter);
            $updatedAfter_res = $modUtils->ConvertDatetoGMTDate(trim($updatedAfter));
            $updatedAfter = (isset($updatedAfter_res['result']) && $updatedAfter_res['result'] && isset($updatedAfter_res['date'])) ? $updatedAfter_res['date'] : '';
        }

        if (trim($contractStatus) != '') {
            $contractStatus = explode(',', $contractStatus);
        }
        $params = array(
            'search_filter' => array(
                'contract-customer-id' => $contractCustomerId,
                'contract-product-id' => $contractProductId,
                'updated-after' => $updatedAfter,
                'status' => $contractStatus,
                'all-versions' => $versionFlag
            )
        );
        $response_array = $browse_contract->getContractList($params);
        // Form the xml output
        require_once ('custom/service/v4_1_custom/APIHelper/v2/ClassProductContractBrowse.php');
        $contract_result = new ClassProductContractBrowse();
        $arr_resp_data = $contract_result->getProductContractXML($response_array);
        $res_xml = $arr_resp_data['respone_xml_data'];

        //ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__, __METHOD__, 'res_xml: ' . print_r($res_xml, true), '');
        return $res_xml;
    }
}
?>
