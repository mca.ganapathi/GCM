<?php

require_once ('service/v4_1/registry.php');

/**
 * class to register methods of sugar api
 */
class registry_v4_1_custom extends registry_v4_1
{
    protected function registerFunction()
    {
        parent::registerFunction();
        $this->serviceClass->registerFunction('fnContractRegistration', array(
                                                                            'sessionId' => 'xsd:string'
        ), array(
                'contractXMLData' => 'xsd:string'
        ), array(
                'return' => 'xsd:string'
        ));
    }
}
