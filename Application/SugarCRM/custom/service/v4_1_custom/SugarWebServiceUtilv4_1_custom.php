<?php
/**
 * *******************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc.
 * product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc. All rights reserved.
 * ******************************************************************************
 */
require_once ('service/v4_1/SugarWebServiceUtilv4_1.php');

class SugarWebServiceUtilv4_1_custom extends SugarWebServiceUtilv4_1
{

    /**
     * Override the checkSessionAndModuleAccess sugar method
     *
     * @param object $session
     * @param string $login_error_key
     * @param string $module_name
     * @param string $access_level
     * @param string $module_access_level_error_key
     * @param object $errorObject
     * @return boolean
     *
     */
    function checkSessionAndModuleAccess($session, $login_error_key, $module_name, $access_level, $module_access_level_error_key, $errorObject)
    {
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_BEGIN, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'session: '.print_r($session, true).GCM_GL_LOG_VAR_SEPARATOR.' login_error_key: '.$login_error_key.GCM_GL_LOG_VAR_SEPARATOR.' module_name: ' . $module_name.GCM_GL_LOG_VAR_SEPARATOR. ' access_level: '.$access_level.GCM_GL_LOG_VAR_SEPARATOR.' module_access_level_error_key: '.$module_access_level_error_key.GCM_GL_LOG_VAR_SEPARATOR. ' errorObject: '.print_r($errorObject, true), 'Override the checkSessionAndModuleAccess sugar method');
        if (!$this->validate_authenticated($session)) {
            ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'module_name: '.$module_name, 'validate_authenticated failed');
            return false;
        } // if

        global $beanList, $beanFiles;
        if (!empty($module_name)) {
            if (empty($beanList[$module_name])) {
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'module_name: '.$module_name, 'module does not exists');
                return false;
            } // if
            global $current_user;
            if (!$this->check_modules_access($current_user, $module_name, $access_level)) {
                ModUtils::buildServiceLogMessage(GCM_GL_LOG_TRACER, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, 'module_name: '.$module_name, 'no module access');
                return false;
            }
        } // if
        ModUtils::buildServiceLogMessage(GCM_GL_LOG_END, GCM_GL_LOG_DEBUG, __FILE__ , __METHOD__, '', '');
        return true;
    } // fn
}