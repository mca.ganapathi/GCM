<?php
// created: 2018-03-07 11:44:28
$dictionary['fm_factory_pm_products'] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'fm_factory_pm_products' => 
    array (
      'lhs_module' => 'fm_Factory',
      'lhs_table' => 'fm_factory',
      'lhs_key' => 'id',
      'rhs_module' => 'pm_Products',
      'rhs_table' => 'pm_products',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'fm_factory_pm_products_c',
      'join_key_lhs' => 'fm_factory_pm_productsfm_factory_ida',
      'join_key_rhs' => 'fm_factory_pm_productspm_products_idb',
    ),
  ),
  'table' => 'fm_factory_pm_products_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    'fm_factory_pm_productsfm_factory_ida' => 
    array (
      'name' => 'fm_factory_pm_productsfm_factory_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    'fm_factory_pm_productspm_products_idb' => 
    array (
      'name' => 'fm_factory_pm_productspm_products_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'fm_factory_pm_productsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'fm_factory_pm_products_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'fm_factory_pm_productsfm_factory_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'fm_factory_pm_products_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'fm_factory_pm_productspm_products_idb',
      ),
    ),
  ),
);