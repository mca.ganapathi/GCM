<?php
// created: 2018-03-07 11:44:28
$dictionary['gc_invoicesubtotalgroup_gc_line_item_contract_history_1'] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'gc_invoicesubtotalgroup_gc_line_item_contract_history_1' => 
    array (
      'lhs_module' => 'gc_InvoiceSubtotalGroup',
      'lhs_table' => 'gc_invoicesubtotalgroup',
      'lhs_key' => 'id',
      'rhs_module' => 'gc_Line_Item_Contract_History',
      'rhs_table' => 'gc_line_item_contract_history',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'gc_invoicesubtotalgroup_gc_line_item_contract_history_1_c',
      'join_key_lhs' => 'gc_invoice806falgroup_ida',
      'join_key_rhs' => 'gc_invoice53c9history_idb',
    ),
  ),
  'table' => 'gc_invoicesubtotalgroup_gc_line_item_contract_history_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    'gc_invoice806falgroup_ida' => 
    array (
      'name' => 'gc_invoice806falgroup_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    'gc_invoice53c9history_idb' => 
    array (
      'name' => 'gc_invoice53c9history_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'gc_invoicesubtotalgroup_gc_line_item_contract_history_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'gc_invoicesubtotalgroup_gc_line_item_contract_history_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'gc_invoice806falgroup_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'gc_invoicesubtotalgroup_gc_line_item_contract_history_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'gc_invoice53c9history_idb',
      ),
    ),
  ),
);