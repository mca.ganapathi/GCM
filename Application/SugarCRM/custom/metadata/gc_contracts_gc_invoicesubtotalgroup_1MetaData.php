<?php
// created: 2018-03-07 11:44:28
$dictionary['gc_contracts_gc_invoicesubtotalgroup_1'] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'gc_contracts_gc_invoicesubtotalgroup_1' => 
    array (
      'lhs_module' => 'gc_Contracts',
      'lhs_table' => 'gc_contracts',
      'lhs_key' => 'id',
      'rhs_module' => 'gc_InvoiceSubtotalGroup',
      'rhs_table' => 'gc_invoicesubtotalgroup',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'gc_contracts_gc_invoicesubtotalgroup_1_c',
      'join_key_lhs' => 'gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida',
      'join_key_rhs' => 'gc_contrac9a02algroup_idb',
    ),
  ),
  'table' => 'gc_contracts_gc_invoicesubtotalgroup_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    'gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida' => 
    array (
      'name' => 'gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    'gc_contrac9a02algroup_idb' => 
    array (
      'name' => 'gc_contrac9a02algroup_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'gc_contracts_gc_invoicesubtotalgroup_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'gc_contracts_gc_invoicesubtotalgroup_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'gc_contracts_gc_invoicesubtotalgroup_1gc_contracts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'gc_contracts_gc_invoicesubtotalgroup_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'gc_contrac9a02algroup_idb',
      ),
    ),
  ),
);