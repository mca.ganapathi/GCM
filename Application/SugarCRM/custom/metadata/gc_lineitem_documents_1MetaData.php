<?php
// created: 2018-03-07 11:44:28
$dictionary['gc_lineitem_documents_1'] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'gc_lineitem_documents_1' => 
    array (
      'lhs_module' => 'gc_LineItem',
      'lhs_table' => 'gc_lineitem',
      'lhs_key' => 'id',
      'rhs_module' => 'Documents',
      'rhs_table' => 'documents',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'gc_lineitem_documents_1_c',
      'join_key_lhs' => 'gc_lineitem_documents_1gc_lineitem_ida',
      'join_key_rhs' => 'gc_lineitem_documents_1documents_idb',
    ),
  ),
  'table' => 'gc_lineitem_documents_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    'gc_lineitem_documents_1gc_lineitem_ida' => 
    array (
      'name' => 'gc_lineitem_documents_1gc_lineitem_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    'gc_lineitem_documents_1documents_idb' => 
    array (
      'name' => 'gc_lineitem_documents_1documents_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
    'document_revision_id' => 
    array (
      'name' => 'document_revision_id',
      'type' => 'varchar',
      'len' => '36',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'gc_lineitem_documents_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'gc_lineitem_documents_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'gc_lineitem_documents_1gc_lineitem_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'gc_lineitem_documents_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'gc_lineitem_documents_1documents_idb',
      ),
    ),
  ),
);