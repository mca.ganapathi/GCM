<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


global $mod_strings, $app_strings, $sugar_config;
 
if(ACLController::checkAccess('js_Accounts_js', 'edit', true))$module_menu[]=Array("index.php?module=js_Accounts_js&action=EditView&return_module=js_Accounts_js&return_action=DetailView", $mod_strings['LNK_NEW_RECORD'],"Createjs_Accounts_js", 'js_Accounts_js');
if(ACLController::checkAccess('js_Accounts_js', 'list', true))$module_menu[]=Array("index.php?module=js_Accounts_js&action=index&return_module=js_Accounts_js&return_action=DetailView", $mod_strings['LNK_LIST'],"js_Accounts_js", 'js_Accounts_js');
if(ACLController::checkAccess('js_Accounts_js', 'import', true))$module_menu[]=Array("index.php?module=Import&action=Step1&import_module=js_Accounts_js&return_module=js_Accounts_js&return_action=index", $app_strings['LBL_IMPORT'],"Import", 'js_Accounts_js');