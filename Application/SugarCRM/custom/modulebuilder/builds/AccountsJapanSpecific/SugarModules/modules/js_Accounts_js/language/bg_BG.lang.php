<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Основен екип по подрабиране',
  'LBL_TEAM' => 'Екипи',
  'LBL_TEAMS' => 'Екипи',
  'LBL_TEAM_ID' => 'Идентификатор на екипа',
  'LBL_TEAM_SET' => 'Дефинирани екипи',
  'LBL_ASSIGNED_TO_ID' => 'Отговорник',
  'LBL_ASSIGNED_TO_NAME' => 'Потребител',
  'LBL_CREATED' => 'Създадено от',
  'LBL_CREATED_ID' => 'Създадено от',
  'LBL_CREATED_USER' => 'Създадено от потребител',
  'LBL_DATE_ENTERED' => 'Създадено на',
  'LBL_DATE_MODIFIED' => 'Модифицирано на',
  'LBL_DELETED' => 'Изтрити',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_EDIT_BUTTON' => 'Редактирай',
  'LBL_ID' => 'Идентификатор',
  'LBL_LIST_NAME' => 'Име',
  'LBL_MODIFIED' => 'Модифицирано от',
  'LBL_MODIFIED_ID' => 'Модифицирано от',
  'LBL_MODIFIED_NAME' => 'Модифицирано от',
  'LBL_MODIFIED_USER' => 'Модифицирано от потребител',
  'LBL_NAME' => 'Име',
  'LBL_REMOVE' => 'Премахни',
  'LBL_LIST_FORM_TITLE' => 'Accounts (Japan Specific) Списък',
  'LBL_MODULE_NAME' => 'Accounts (Japan Specific)',
  'LBL_MODULE_TITLE' => 'Accounts (Japan Specific)',
  'LBL_HOMEPAGE_TITLE' => 'Мои Accounts (Japan Specific)',
  'LNK_NEW_RECORD' => 'Създай Accounts (Japan Specific)',
  'LNK_LIST' => 'Разгледай Accounts (Japan Specific)',
  'LNK_IMPORT_JS_ACCOUNTS_JS' => 'Import Accounts (Japan Specific)',
  'LBL_SEARCH_FORM_TITLE' => 'Търси Accounts (Japan Specific)',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дейности',
  'LBL_JS_ACCOUNTS_JS_SUBPANEL_TITLE' => 'Accounts (Japan Specific)',
  'LBL_NEW_FORM_TITLE' => 'Нов Accounts (Japan Specific)',
  'LBL_ACCOUNTS_ID' => 'Account ID',
  'LBL_ADDR_CODE' => 'Address Code [Japan specific]',
  'LBL_PREFECTURE' => 'Prefecture [Japan specific]',
  'LBL_ADDR_1_CITY' => 'Address 1 (City) [Japan specific]',
  'LBL_ADDR_2_STREET' => 'Address 2 (Street Address) [Japan specific]',
  'LBL_ADDR_3_BUILDING' => 'Address 3 (Building) [Japan specific]',
  'LBL_CARE_OF' => 'C/O (care of) [Japan specific]',
  'LBL_ACCOUNTS_ID_CONTACT_ID' => 'Account ID (related Account ID)',
  'LBL_ADDR_1' => 'Address 1 [Japan specific]',
  'LBL_ADDR_2' => 'Address 2 [Japan specific]',
  'LBL_ADDR1' => 'Address 1 [Japan specific]',
  'LBL_ADDR2' => 'Address 2 [Japan specific]',
);