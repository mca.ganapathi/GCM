<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Equipo Principal por Defecto',
  'LBL_TEAM' => 'Equipos',
  'LBL_TEAMS' => 'Equipos',
  'LBL_TEAM_ID' => 'Id Equipo',
  'LBL_TEAM_SET' => 'Conjunto de Equipo',
  'LBL_ASSIGNED_TO_ID' => 'Asignado a Usuario con Id',
  'LBL_ASSIGNED_TO_NAME' => 'Usuario',
  'LBL_CREATED' => 'Creado Por',
  'LBL_CREATED_ID' => 'Creado Por Id',
  'LBL_CREATED_USER' => 'Creado Por Usuario',
  'LBL_DATE_ENTERED' => 'Fecha de Creación',
  'LBL_DATE_MODIFIED' => 'Última Modificación',
  'LBL_DELETED' => 'Eliminado',
  'LBL_DESCRIPTION' => 'Descripción',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nombre',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nombre',
  'LBL_MODIFIED_USER' => 'Modificado Por Usuario',
  'LBL_NAME' => 'Nombre',
  'LBL_REMOVE' => 'Quitar',
  'LBL_LIST_FORM_TITLE' => 'LineItemContractHistory Lista',
  'LBL_MODULE_NAME' => 'LineItemContractHistory',
  'LBL_MODULE_TITLE' => 'LineItemContractHistory',
  'LBL_HOMEPAGE_TITLE' => 'Mi LineItemContractHistory',
  'LNK_NEW_RECORD' => 'Crear LineItemContractHistory',
  'LNK_LIST' => 'Vista LineItemContractHistory',
  'LNK_IMPORT_GC_LINEITEMCONTRACTHISTORY' => 'Import LineItemContractHistory',
  'LBL_SEARCH_FORM_TITLE' => 'Buscar LineItemContractHistory',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Actividades',
  'LBL_GC_LINEITEMCONTRACTHISTORY_SUBPANEL_TITLE' => 'LineItemContractHistory',
  'LBL_NEW_FORM_TITLE' => 'Nuevo LineItemContractHistory',
  'LBL_SERVICE_START_DATE' => 'Service Start Date',
  'LBL_SERVICE_END_DATE' => 'Service End Date',
  'LBL_BILLING_START_DATE' => 'Billing Start Date',
  'LBL_BILLING_END_DATE' => 'Billing End Date',
  'LBL_PAYMENT_TYPE' => 'Payment Type',
  'LBL_CC_CONTRACT_ID' => 'cc contract id',
  'LBL_CONTRACTS_ID' => 'contracts id',
  'LBL_LINE_ITEM_ID' => 'Line Item Contract ID',
  'LBL_TECH_ACCOUNT_ID' => 'Technology Customer Contact',
  'LBL_BILL_ACOOUNT_ID' => 'Billing Customer Contract',
  'LBL_PAYEE_CONTACT_PERSON_NAME1' => 'Payee Contact Person Name (Latin Character)',
  'LBL_PAYEE_CONTACT_PERSON_NAME2' => 'Payee Contact Person Name (Local Character)',
  'LBL_PAYEE_TELEPHONE_NUMBER' => 'payee_tel_no',
  'LBL_PAYEE_EMAIL' => 'Payee E-mail Address',
  'LBL_REMARKS' => 'Remarks',
);