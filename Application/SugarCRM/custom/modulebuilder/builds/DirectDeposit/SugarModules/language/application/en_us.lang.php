<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_DirectDeposit'] = 'Direct Deposit';
$app_list_strings['deposit_type_list'][''] = '';
$app_list_strings['deposit_type_list'][1] = 'Ordinary Deposit';
$app_list_strings['deposit_type_list'][2] = 'Current Deposit';
$app_list_strings['deposit_type_list'][3] = 'Tax Payment (General)';
$app_list_strings['deposit_type_list'][9] = 'Others';
