<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Varsayılan Asıl Takım',
  'LBL_TEAM' => 'Takımlar',
  'LBL_TEAMS' => 'Takımlar',
  'LBL_TEAM_ID' => 'Takım ID',
  'LBL_TEAM_SET' => 'Takım Ayarla',
  'LBL_ASSIGNED_TO_ID' => 'Atanan Kullanıcı ID',
  'LBL_ASSIGNED_TO_NAME' => 'Atanan Kişi',
  'LBL_CREATED' => 'Oluşturan',
  'LBL_CREATED_ID' => 'Oluşturan ID',
  'LBL_CREATED_USER' => 'Oluşturan Kullanıcı',
  'LBL_DATE_ENTERED' => 'Oluşturulma Tarihi',
  'LBL_DATE_MODIFIED' => 'Değiştirilme Tarihi',
  'LBL_DELETED' => 'Silindi',
  'LBL_DESCRIPTION' => 'Tanım',
  'LBL_EDIT_BUTTON' => 'Değiştir',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'İsim',
  'LBL_MODIFIED' => 'Değiştiren',
  'LBL_MODIFIED_ID' => 'Değiştiren ID',
  'LBL_MODIFIED_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_MODIFIED_USER' => 'Değiştiren Kullanıcı',
  'LBL_NAME' => 'İsim',
  'LBL_REMOVE' => 'Sil',
  'LBL_LIST_FORM_TITLE' => 'Product Attribute Values Liste',
  'LBL_MODULE_NAME' => 'Product Attribute Values',
  'LBL_MODULE_TITLE' => 'Product Attribute Values',
  'LBL_HOMEPAGE_TITLE' => 'Benim Product Attribute Values',
  'LNK_NEW_RECORD' => 'Oluştur Product Attribute Values',
  'LNK_LIST' => 'Göster Product Attribute Values',
  'LNK_IMPORT_PA_PRODUCTATTRIBUTEVALUES' => 'Import Product Attribute Values',
  'LBL_SEARCH_FORM_TITLE' => 'Ara Product Attribute Values',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Tarihçeyi Gör',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteler',
  'LBL_PA_PRODUCTATTRIBUTEVALUES_SUBPANEL_TITLE' => 'Product Attribute Values',
  'LBL_NEW_FORM_TITLE' => 'Yeni Product Attribute Values',
  'LBL_UOM' => 'UoM',
  'LBL_PARENT_ATTRIBUTE_PA_PRODUCTATTRIBUTES_ID' => 'Product Attribute ID (related  ID)',
  'LBL_PARENT_ATTRIBUTE' => 'Product Attribute ID',
);