<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Equipe Primária Padrão',
  'LBL_TEAM' => 'Equipas',
  'LBL_TEAMS' => 'Equipas',
  'LBL_TEAM_ID' => 'Id Equipa',
  'LBL_TEAM_SET' => 'Configuração de Equipe',
  'LBL_ASSIGNED_TO_ID' => 'Atribuído a',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_CREATED_USER' => 'Criado por Utilizador',
  'LBL_DATE_ENTERED' => 'Data da Criação',
  'LBL_DATE_MODIFIED' => 'Data da Modificação',
  'LBL_DELETED' => 'Eliminado',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_MODIFIED_USER' => 'Modificado por Utilizador',
  'LBL_NAME' => 'Nome',
  'LBL_REMOVE' => 'Remover',
  'LBL_LIST_FORM_TITLE' => 'Product Attributes Lista',
  'LBL_MODULE_NAME' => 'Product Attributes',
  'LBL_MODULE_TITLE' => 'Product Attributes',
  'LBL_HOMEPAGE_TITLE' => 'Minha Product Attributes',
  'LNK_NEW_RECORD' => 'Criar Product Attributes',
  'LNK_LIST' => 'Vista Product Attributes',
  'LNK_IMPORT_PA_PRODUCTATTRIBUTES' => 'Import Product Attributes',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Product Attributes',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Atividades',
  'LBL_PA_PRODUCTATTRIBUTES_SUBPANEL_TITLE' => 'Product Attributes',
  'LBL_NEW_FORM_TITLE' => 'Novo Product Attributes',
  'LBL_ATTRIBUTE_TYPE' => 'Attribute Type',
  'LBL_VALUE_DATA_TYPE' => 'Value Data Type',
  'LBL_PARENT_ATTRIBUTE_PA_PRODUCTATTRIBUTES_ID' => 'Parent Attribute ID (related  ID)',
  'LBL_PARENT_ATTRIBUTE' => 'Parent Attribute ID',
);