<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => '默认主要团队',
  'LBL_TEAM' => '团队',
  'LBL_TEAMS' => '团队',
  'LBL_TEAM_ID' => '团队编号',
  'LBL_TEAM_SET' => '团队列表',
  'LBL_ASSIGNED_TO_ID' => '负责人ID',
  'LBL_ASSIGNED_TO_NAME' => '负责人',
  'LBL_CREATED' => '创建人',
  'LBL_CREATED_ID' => '创建人ID',
  'LBL_CREATED_USER' => '创建用户',
  'LBL_DATE_ENTERED' => '创建日期',
  'LBL_DATE_MODIFIED' => '修改日期',
  'LBL_DELETED' => '已删除',
  'LBL_DESCRIPTION' => '描述',
  'LBL_EDIT_BUTTON' => '编辑',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => '名称',
  'LBL_MODIFIED' => '修改人',
  'LBL_MODIFIED_ID' => '修改人ID',
  'LBL_MODIFIED_NAME' => '修改人姓名',
  'LBL_MODIFIED_USER' => '修改用户',
  'LBL_NAME' => '名称',
  'LBL_REMOVE' => '移除',
  'LBL_LIST_FORM_TITLE' => 'Product Attributes 列表',
  'LBL_MODULE_NAME' => 'Product Attributes',
  'LBL_MODULE_TITLE' => 'Product Attributes',
  'LBL_HOMEPAGE_TITLE' => '我的 Product Attributes',
  'LNK_NEW_RECORD' => '创建 Product Attributes',
  'LNK_LIST' => '查看 Product Attributes',
  'LNK_IMPORT_PA_PRODUCTATTRIBUTES' => 'Import Product Attributes',
  'LBL_SEARCH_FORM_TITLE' => '查找 Product Attributes',
  'LBL_HISTORY_SUBPANEL_TITLE' => '查看历史记录',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活动',
  'LBL_PA_PRODUCTATTRIBUTES_SUBPANEL_TITLE' => 'Product Attributes',
  'LBL_NEW_FORM_TITLE' => '新建 Product Attributes',
  'LBL_ATTRIBUTE_TYPE' => 'Attribute Type',
  'LBL_VALUE_DATA_TYPE' => 'Value Data Type',
  'LBL_PARENT_ATTRIBUTE_PA_PRODUCTATTRIBUTES_ID' => 'Parent Attribute ID (related  ID)',
  'LBL_PARENT_ATTRIBUTE' => 'Parent Attribute ID',
);