<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Výchozí primární tým',
  'LBL_TEAM' => 'Týmy',
  'LBL_TEAMS' => 'Týmy',
  'LBL_TEAM_ID' => 'ID týmu',
  'LBL_TEAM_SET' => 'Nastavení týmu',
  'LBL_ASSIGNED_TO_ID' => 'Přidělené ID uživatele',
  'LBL_ASSIGNED_TO_NAME' => 'Uživatel',
  'LBL_CREATED' => 'Vytvořeno',
  'LBL_CREATED_ID' => 'Vytvořeno (ID)',
  'LBL_CREATED_USER' => 'Vytvořeno uživatelem',
  'LBL_DATE_ENTERED' => 'Datum vytvoření',
  'LBL_DATE_MODIFIED' => 'Datum změny',
  'LBL_DELETED' => 'Smazáno',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_EDIT_BUTTON' => 'Upravit',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Jméno',
  'LBL_MODIFIED' => 'Změnil',
  'LBL_MODIFIED_ID' => 'Změněno podle ID',
  'LBL_MODIFIED_NAME' => 'Změněno kým:',
  'LBL_MODIFIED_USER' => 'Změněno uživatelem',
  'LBL_NAME' => 'Jméno',
  'LBL_REMOVE' => 'Odstranit',
  'LBL_LIST_FORM_TITLE' => 'My Approvals Seznam',
  'LBL_MODULE_NAME' => 'My Approvals',
  'LBL_MODULE_TITLE' => 'My Approvals',
  'LBL_HOMEPAGE_TITLE' => 'Moje My Approvals',
  'LNK_NEW_RECORD' => 'Přidat My Approvals',
  'LNK_LIST' => 'Zobrazit My Approvals',
  'LNK_IMPORT_GC_MYAPPROVALS' => 'Import My Approvals',
  'LBL_SEARCH_FORM_TITLE' => 'Hledat My Approvals',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_GC_MYAPPROVALS_SUBPANEL_TITLE' => 'My Approvals',
  'LBL_NEW_FORM_TITLE' => 'Nový My Approvals',
  'LBL_ACTION' => 'Action',
  'LBL_FROM_STAGE' => 'From Stage',
  'LBL_TO_STAGE' => 'To Stage',
  'LBL_GLOBAL_CONTRACT_ID_GC_CONTRACTS_ID' => 'Global Contract Id (related  ID)',
  'LBL_GLOBAL_CONTRACT_ID' => 'Global Contract Id',
  'LBL_CONTRACT_ACTION' => 'Contract Action',
);