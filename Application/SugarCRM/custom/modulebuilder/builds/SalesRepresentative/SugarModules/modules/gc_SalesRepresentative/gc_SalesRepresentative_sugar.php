<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * THIS CLASS IS GENERATED BY MODULE BUILDER
 * PLEASE DO NOT CHANGE THIS CLASS
 * PLACE ANY CUSTOMIZATIONS IN gc_SalesRepresentative
 */


class gc_SalesRepresentative_sugar extends Basic {
    public $new_schema = true;
    public $module_dir = 'gc_SalesRepresentative';
    public $object_name = 'gc_SalesRepresentative';
    public $table_name = 'gc_salesrepresentative';
    public $importable = true;
    public $team_id;
    public $team_set_id;
    public $team_count;
    public $team_name;
    public $team_link;
    public $team_count_link;
    public $teams;
    public $assigned_user_id;
    public $assigned_user_name;
    public $assigned_user_link;
    public $id;
    public $name;
    public $date_entered;
    public $date_modified;
    public $modified_user_id;
    public $modified_by_name;
    public $created_by;
    public $created_by_name;
    public $description;
    public $deleted;
    public $created_by_link;
    public $modified_user_link;
    public $activities;
    public $following;
    public $following_link;
    public $my_favorite;
    public $favorite_link;
    public $tag;
    public $tag_link;
    public $sales_channel_code;
    public $sales_rep_name_1;
    public $division_1;
    public $division_2;
    public $title_1;
    public $title_2;
    public $tel_no;
    public $ext_no;
    public $fax_no;
    public $mob_no;
    public $email1;
    public $corporate_id;
    public $contracts_id;
    public $gc_nttcomgroupcompany_id_c;
    public $sales_company;
    
    /**
     * @deprecated Use __construct() instead
     */
    public function gc_SalesRepresentative_sugar()
    {
        self::__construct();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
    
}