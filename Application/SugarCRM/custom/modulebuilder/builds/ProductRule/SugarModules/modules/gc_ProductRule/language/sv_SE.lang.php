<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Tilldelad användare',
  'LBL_ASSIGNED_TO_NAME' => 'Tilldelad användare',
  'LBL_CREATED' => 'Skapat Av',
  'LBL_CREATED_ID' => 'Skapat Av',
  'LBL_CREATED_USER' => 'Skapat Av Användare',
  'LBL_DATE_ENTERED' => 'Skapat Datum',
  'LBL_DATE_MODIFIED' => 'Modifierat Datum',
  'LBL_DELETED' => 'Raderad',
  'LBL_DESCRIPTION' => 'Beskrivning',
  'LBL_EDIT_BUTTON' => 'Redigera',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Namn',
  'LBL_MODIFIED' => 'Modifierat Av',
  'LBL_MODIFIED_ID' => 'Modifierat Av Id',
  'LBL_MODIFIED_NAME' => 'Modifierat Av Namn',
  'LBL_MODIFIED_USER' => 'Modifierat Av Användare',
  'LBL_NAME' => 'Namn',
  'LBL_REMOVE' => 'Ta bort',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_TEAM' => 'Team',
  'LBL_LIST_FORM_TITLE' => 'Product Rule List',
  'LBL_MODULE_NAME' => 'Product Rule',
  'LBL_MODULE_TITLE' => 'Product Rule',
  'LBL_HOMEPAGE_TITLE' => 'My Product Rule',
  'LNK_NEW_RECORD' => 'Create Product Rule',
  'LNK_LIST' => 'Visa Product Rule',
  'LNK_IMPORT_GC_PRODUCTRULE' => 'Import Product Rule',
  'LBL_SEARCH_FORM_TITLE' => 'Search Product Rule',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_GC_PRODUCTRULE_SUBPANEL_TITLE' => 'Product Rule',
  'LBL_NEW_FORM_TITLE' => 'New Product Rule',
  'LBL_LINE_ITEM_ID' => 'Line Item Id',
  'LBL_RULE_UUID' => 'Rule UUID ',
);