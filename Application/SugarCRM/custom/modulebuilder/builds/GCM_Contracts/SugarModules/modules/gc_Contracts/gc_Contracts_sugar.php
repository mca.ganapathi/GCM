<?PHP
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

/**
 * THIS CLASS IS GENERATED BY MODULE BUILDER
 * PLEASE DO NOT CHANGE THIS CLASS
 * PLACE ANY CUSTOMIZATIONS IN gc_Contracts
 */


class gc_Contracts_sugar extends Basic {
	var $new_schema = true;
	var $module_dir = 'gc_Contracts';
	var $object_name = 'gc_Contracts';
	var $table_name = 'gc_contracts';
	var $importable = true;
		var $id;
		var $name;
		var $date_entered;
		var $date_modified;
		var $modified_user_id;
		var $modified_by_name;
		var $created_by;
		var $created_by_name;
		var $description;
		var $deleted;
		var $created_by_link;
		var $modified_user_link;
		var $team_id;
		var $team_set_id;
		var $team_count;
		var $team_name;
		var $team_link;
		var $team_count_link;
		var $teams;
		var $team_sets;
		var $assigned_user_id;
		var $assigned_user_name;
		var $assigned_user_link;
		var $global_contract_id;
		var $contract_type;
		var $contract_version;
		var $contract_status;
		var $loi_contract;
		var $billing_type;
		var $total_nrc;
		var $total_mrc;
		var $grand_total;
		var $contract_request_id;
		var $ext_sys_created_by;
		var $ext_sys_modified_by;
		var $date_entered_timezone;
		var $date_modified_timezone;
		var $factory_reference_no;
		var $workflow_flag;
		var $contract_scheme;
		var $contract_startdate;
		var $contract_period;
		var $contract_period_unit;
		var $product_offering;
		var $gc_timezone_id_c;
		var $contract_startdate_timezone;
		var $global_contract_id_uuid;
		var $contract_scheme_uuid;
		var $contract_type_uuid;
		var $loi_contract_uuid;
		var $billing_type_uuid;
		var $product_offering_uuid;
		var $factory_reference_no_uuid;
		var $lock_version;
		var $contract_enddate;
		var $gc_timezone_id1_c;
		var $contract_enddate_timezone;
		var $vat_no;
			function gc_Contracts_sugar(){	
		parent::__construct();
	}
	
	function bean_implements($interface){
		switch($interface){
			case 'ACL': return true;
		}
		return false;
}
		
}
?>