<?php
// created: 2016-02-18 07:06:47
$dictionary["pi_product_item"]["fields"]["pm_products_pi_product_item"] = array (
  'name' => 'pm_products_pi_product_item',
  'type' => 'link',
  'relationship' => 'pm_products_pi_product_item',
  'source' => 'non-db',
  'module' => 'pm_Products',
  'bean_name' => 'pm_Products',
  'vname' => 'LBL_PM_PRODUCTS_PI_PRODUCT_ITEM_FROM_PM_PRODUCTS_TITLE',
);
