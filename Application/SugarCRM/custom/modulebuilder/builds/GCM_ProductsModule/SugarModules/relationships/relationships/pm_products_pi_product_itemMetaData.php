<?php
// created: 2016-02-18 07:06:47
$dictionary["pm_products_pi_product_item"] = array (
  'true_relationship_type' => 'many-to-many',
  'relationships' => 
  array (
    'pm_products_pi_product_item' => 
    array (
      'lhs_module' => 'pm_Products',
      'lhs_table' => 'pm_products',
      'lhs_key' => 'id',
      'rhs_module' => 'pi_product_item',
      'rhs_table' => 'pi_product_item',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'pm_products_pi_product_item_c',
      'join_key_lhs' => 'pm_products_pi_product_itempm_products_ida',
      'join_key_rhs' => 'pm_products_pi_product_itempi_product_item_idb',
    ),
  ),
  'table' => 'pm_products_pi_product_item_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'pm_products_pi_product_itempm_products_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'pm_products_pi_product_itempi_product_item_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'pm_products_pi_product_itemspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'pm_products_pi_product_item_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'pm_products_pi_product_itempm_products_ida',
        1 => 'pm_products_pi_product_itempi_product_item_idb',
      ),
    ),
  ),
);