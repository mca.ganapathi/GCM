<?php
// created: 2016-02-18 07:06:47
$dictionary["pm_Products"]["fields"]["pm_products_pi_product_item"] = array (
  'name' => 'pm_products_pi_product_item',
  'type' => 'link',
  'relationship' => 'pm_products_pi_product_item',
  'source' => 'non-db',
  'module' => 'pi_product_item',
  'bean_name' => 'pi_product_item',
  'vname' => 'LBL_PM_PRODUCTS_PI_PRODUCT_ITEM_FROM_PI_PRODUCT_ITEM_TITLE',
);
