<?php
$popupMeta = array (
    'moduleMain' => 'pm_Products',
    'varName' => 'pm_Products',
    'orderBy' => 'pm_products.name',
    'whereClauses' => array (
  'name' => 'pm_products.name',
),
    'searchInputs' => array (
  0 => 'pm_products_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
    'name' => 'name',
  ),
  'PRODUCTS_ID' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PRODUCTS_ID',
    'width' => '10%',
    'default' => true,
    'name' => 'products_id',
  ),
  'PRODUCT_VERSION' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_PRODUCT_VERSION',
    'width' => '10%',
    'default' => true,
    'name' => 'product_version',
  ),
  'PRODUCT_STATUS' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_PRODUCT_STATUS',
    'width' => '10%',
    'name' => 'product_status',
  ),
),
);
