<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_TEAM' => 'Team',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruger-id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_ID' => 'Id',
  'LBL_DATE_ENTERED' => 'Oprettet den',
  'LBL_DATE_MODIFIED' => 'Ændret den',
  'LBL_MODIFIED' => 'Ændret af',
  'LBL_MODIFIED_ID' => 'Ændret af id',
  'LBL_MODIFIED_NAME' => 'Ændret af navn',
  'LBL_CREATED' => 'Oprettet af',
  'LBL_CREATED_ID' => 'Oprettet af id',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Oprettet af bruger',
  'LBL_MODIFIED_USER' => 'Ændret af bruger',
  'LBL_LIST_FORM_TITLE' => 'Account Transfer Liste',
  'LBL_MODULE_NAME' => 'Account Transfer',
  'LBL_MODULE_TITLE' => 'Account Transfer',
  'LBL_HOMEPAGE_TITLE' => 'Min Account Transfer',
  'LNK_NEW_RECORD' => 'Opret Account Transfer',
  'LNK_LIST' => 'Vis Account Transfer',
  'LNK_IMPORT_GC_ACCOUNTTRANSFER' => 'Import Account Transfer',
  'LBL_SEARCH_FORM_TITLE' => 'Søg Account Transfer',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vis historik',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteter',
  'LBL_GC_ACCOUNTTRANSFER_SUBPANEL_TITLE' => 'Account Transfer',
  'LBL_NEW_FORM_TITLE' => 'Ny Account Transfer',
  'LBL_CONTRACT_HISTORY_ID' => 'Contract Line Item History Id',
  'LBL_BANK_CODE' => 'Bank Code',
  'LBL_BRANCH_CODE' => 'Branch Code',
  'LBL_DEPOSIT_TYPE' => 'Deposit Type',
  'LBL_ACCOUNT_NO' => 'account no',
  'LBL_ACCOUNT_NO_DISPLAY' => 'Account Number Display',
);