<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Echipa Implicita',
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'Echipa ID',
  'LBL_TEAM_SET' => 'Setare echipa',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Utilizator',
  'LBL_CREATED' => 'Creat de',
  'LBL_CREATED_ID' => 'Creata de ID',
  'LBL_CREATED_USER' => 'Creata de Utilizator',
  'LBL_DATE_ENTERED' => 'Data Crearii',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_DELETED' => 'Sters',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_MODIFIED' => 'Modificat de',
  'LBL_MODIFIED_ID' => 'Modificata de ID',
  'LBL_MODIFIED_NAME' => 'Modificata de Nume',
  'LBL_MODIFIED_USER' => 'Modificata de Utilizator',
  'LBL_NAME' => 'Nume',
  'LBL_REMOVE' => 'Inlatura',
  'LBL_LIST_FORM_TITLE' => 'Product Item Attributes Lista',
  'LBL_MODULE_NAME' => 'Product Item Attributes',
  'LBL_MODULE_TITLE' => 'Product Item Attributes',
  'LBL_HOMEPAGE_TITLE' => 'Al meu Product Item Attributes',
  'LNK_NEW_RECORD' => 'Creeaza Product Item Attributes',
  'LNK_LIST' => 'Vizualizare Product Item Attributes',
  'LNK_IMPORT_PI_PRODUCT_ITEM_ATTRIBUTE' => 'Import Product Item Attributes',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta Product Item Attributes',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_PI_PRODUCT_ITEM_ATTRIBUTE_SUBPANEL_TITLE' => 'Product Item Attributes',
  'LBL_NEW_FORM_TITLE' => 'Nou Product Item Attributes',
  'LBL_PARENT_PRODUCT_ID_PI_PRODUCT_ITEM_ID' => 'parent product id (related  ID)',
  'LBL_PARENT_PRODUCT_ID' => 'parent product id',
  'LBL_ATTRIBUTE_ID_PA_PRODUCTATTRIBUTES_ID' => 'Attribute Id (related  ID)',
  'LBL_ATTRIBUTE_ID' => 'Attribute Id',
  'LBL_ATTRIBUTE_ORDER' => 'Attribute Order',
  'LBL_IS_SUB_ATTRIBUTE' => 'Is Sub Attribute',
  'LBL_IS_ENABLED' => 'Is Enabled',
);