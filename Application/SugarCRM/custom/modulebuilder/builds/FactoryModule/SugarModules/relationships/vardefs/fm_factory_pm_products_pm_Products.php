<?php
// created: 2015-12-17 13:01:43
$dictionary["pm_Products"]["fields"]["fm_factory_pm_products"] = array (
  'name' => 'fm_factory_pm_products',
  'type' => 'link',
  'relationship' => 'fm_factory_pm_products',
  'source' => 'non-db',
  'module' => 'fm_Factory',
  'bean_name' => 'fm_Factory',
  'vname' => 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_FM_FACTORY_TITLE',
  'id_name' => 'fm_factory_pm_productsfm_factory_ida',
);
$dictionary["pm_Products"]["fields"]["fm_factory_pm_products_name"] = array (
  'name' => 'fm_factory_pm_products_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_FM_FACTORY_TITLE',
  'save' => true,
  'id_name' => 'fm_factory_pm_productsfm_factory_ida',
  'link' => 'fm_factory_pm_products',
  'table' => 'fm_factory',
  'module' => 'fm_Factory',
  'rname' => 'name',
);
$dictionary["pm_Products"]["fields"]["fm_factory_pm_productsfm_factory_ida"] = array (
  'name' => 'fm_factory_pm_productsfm_factory_ida',
  'type' => 'link',
  'relationship' => 'fm_factory_pm_products',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_FM_FACTORY_PM_PRODUCTS_FROM_PM_PRODUCTS_TITLE',
);
