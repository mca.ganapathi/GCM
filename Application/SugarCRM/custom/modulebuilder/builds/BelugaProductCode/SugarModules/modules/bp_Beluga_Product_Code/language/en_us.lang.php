<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Default Primary Team',
  'LBL_TEAM_SET' => 'Team Set',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_LIST_FORM_TITLE' => 'Beluga Product Code List',
  'LBL_MODULE_NAME' => 'Beluga Product Code',
  'LBL_MODULE_TITLE' => 'Beluga Product Code',
  'LBL_HOMEPAGE_TITLE' => 'My Beluga Product Code',
  'LNK_NEW_RECORD' => 'Create Beluga Product Code',
  'LNK_LIST' => 'View Beluga Product Code',
  'LNK_IMPORT_BP_BELUGA_PRODUCT_CODE' => 'Import Beluga Product Code',
  'LBL_SEARCH_FORM_TITLE' => 'Search Beluga Product Code',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_BP_BELUGA_PRODUCT_CODE_SUBPANEL_TITLE' => 'Beluga Product Code',
  'LBL_NEW_FORM_TITLE' => 'New Beluga Product Code',
  'LBL_GCM_PRODUCT_ITEM_ID_PI_PRODUCT_ITEM_ID' => 'gcm product item id (related  ID)',
  'LBL_GCM_PRODUCT_ITEM_ID' => 'gcm product item id',
  'LBL_BELUGA_PRD_TYPE' => 'Beluga Product Type',
  'LBL_BELUGA_SRV_TYPE' => 'Beluga Service Type',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
);