<?php
// created: 2018-03-07 11:46:20
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Alapértelmezett elsődleges csoport',
  'LBL_TEAM' => 'Csoportok',
  'LBL_TEAMS' => 'Csoportok',
  'LBL_TEAM_ID' => 'Csoport azonosító',
  'LBL_TEAM_SET' => 'Csoport készlet',
  'LBL_ASSIGNED_TO_ID' => 'Hozzárendelt felhasználó azonosító',
  'LBL_ASSIGNED_TO_NAME' => 'Felelős felhasználó',
  'LBL_CREATED' => 'Létrehozta',
  'LBL_CREATED_ID' => 'Létrehozó azonosítója',
  'LBL_CREATED_USER' => 'Felhasználó által létrehozva',
  'LBL_DATE_ENTERED' => 'Létrehozás dátuma',
  'LBL_DATE_MODIFIED' => 'Módosítás dátuma',
  'LBL_DELETED' => 'Törölve',
  'LBL_DESCRIPTION' => 'Leírás',
  'LBL_EDIT_BUTTON' => 'Szerkesztés',
  'LBL_ID' => 'Azonosító',
  'LBL_LIST_NAME' => 'Név',
  'LBL_MODIFIED' => 'Módosította',
  'LBL_MODIFIED_ID' => 'Módosította (azonosító szerint)',
  'LBL_MODIFIED_NAME' => 'Módosította (név szerint)',
  'LBL_MODIFIED_USER' => 'Felhasználó által módosítva',
  'LBL_NAME' => 'Név',
  'LBL_REMOVE' => 'Eltávolítás',
  'LBL_LIST_FORM_TITLE' => 'IineItem Config Summary Lista',
  'LBL_MODULE_NAME' => 'IineItem Config Summary',
  'LBL_MODULE_TITLE' => 'IineItem Config Summary',
  'LBL_HOMEPAGE_TITLE' => 'Saját IineItem Config Summary',
  'LNK_NEW_RECORD' => 'Új létrehozása IineItem Config Summary',
  'LNK_LIST' => 'Megtekintés IineItem Config Summary',
  'LNK_IMPORT_GC_IINEITEMCONFIGSUMMARY' => 'Import IineItem Config Summary',
  'LBL_SEARCH_FORM_TITLE' => 'Keresés IineItem Config Summary',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Előzmények megtekintése',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tevékenységek',
  'LBL_GC_IINEITEMCONFIGSUMMARY_SUBPANEL_TITLE' => 'IineItem Config Summary',
  'LBL_NEW_FORM_TITLE' => 'Új IineItem Config Summary',
  'LBL_LINE_ITEM_ID' => 'Line Item Contract ID',
  'LBL_ATTRIBUTE_ID' => 'Attribute ID',
  'LBL_SUB_ATTRIBUTE_ID' => 'Sub Attribute ID',
  'LBL_ATTRIBUTE_VALUE_ID' => 'Attribute Value ID',
  'LBL_CUSTOM_ATTRIBUTE_VALUE' => 'Custom Attribute Value',
);