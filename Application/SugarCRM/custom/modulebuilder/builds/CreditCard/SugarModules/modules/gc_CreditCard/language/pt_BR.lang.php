<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Equipe Primária Padrão',
  'LBL_TEAM' => 'Equipas',
  'LBL_TEAMS' => 'Equipas',
  'LBL_TEAM_ID' => 'Id Equipa',
  'LBL_TEAM_SET' => 'Configuração de Equipe',
  'LBL_ASSIGNED_TO_ID' => 'Atribuído a',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_CREATED_USER' => 'Criado por Utilizador',
  'LBL_DATE_ENTERED' => 'Data da Criação',
  'LBL_DATE_MODIFIED' => 'Data da Modificação',
  'LBL_DELETED' => 'Eliminado',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_MODIFIED_USER' => 'Modificado por Utilizador',
  'LBL_NAME' => 'Nome',
  'LBL_REMOVE' => 'Remover',
  'LBL_LIST_FORM_TITLE' => 'Credit Card Lista',
  'LBL_MODULE_NAME' => 'Credit Card',
  'LBL_MODULE_TITLE' => 'Credit Card',
  'LBL_HOMEPAGE_TITLE' => 'Minha Credit Card',
  'LNK_NEW_RECORD' => 'Criar Credit Card',
  'LNK_LIST' => 'Vista Credit Card',
  'LNK_IMPORT_GC_CREDITCARD' => 'Import Credit Card',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Credit Card',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Atividades',
  'LBL_GC_CREDITCARD_SUBPANEL_TITLE' => 'Credit Card',
  'LBL_NEW_FORM_TITLE' => 'Novo Credit Card',
  'LBL_CONTRACT_HISTORY_ID' => 'Contract Line Item History ID',
  'LBL_CREDIT_CARD_NO' => 'Credit-Card Number',
  'LBL_EXPIRATION_DATE' => 'expiration date',
);