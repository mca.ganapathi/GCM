<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


// THIS CONTENT IS GENERATED BY MBPackage.php
$manifest = array (
  0 => 
  array (
    'acceptable_sugar_versions' => 
    array (
      0 => '',
    ),
  ),
  1 => 
  array (
    'acceptable_sugar_flavors' => 
    array (
      0 => 'PRO',
    ),
  ),
  'readme' => '',
  'key' => 'c',
  'author' => 'Netmagic GCM Development team Mumbai',
  'description' => 'State Module is newly created to store master data of States
BRS_PJ1_JAN_NJ-62_Adding US&Canada Specific Address Fields in Account Module_v1.1_20160118_1100.docx',
  'icon' => '',
  'is_uninstallable' => true,
  'name' => 'State',
  'published_date' => '2016-01-19 06:42:41',
  'type' => 'module',
  'version' => 1453185761,
  'remove_tables' => 'prompt',
);


$installdefs = array (
  'id' => 'State',
  'beans' => 
  array (
    0 => 
    array (
      'module' => 'c_State',
      'class' => 'c_State',
      'path' => 'modules/c_State/c_State.php',
      'tab' => true,
    ),
  ),
  'layoutdefs' => 
  array (
  ),
  'relationships' => 
  array (
  ),
  'image_dir' => '<basepath>/icons',
  'copy' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/SugarModules/modules/c_State',
      'to' => 'modules/c_State',
    ),
  ),
  'language' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/SugarModules/language/application/en_us.lang.php',
      'to_module' => 'application',
      'language' => 'en_us',
    ),
  ),
);