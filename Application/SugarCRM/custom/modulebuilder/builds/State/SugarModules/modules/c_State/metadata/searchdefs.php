<?php
$module_name = 'c_State';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'state_type' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_STATE_TYPE',
        'width' => '10%',
        'default' => true,
        'name' => 'state_type',
      ),
      'state_code' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_STATE_CODE',
        'width' => '10%',
        'default' => true,
        'name' => 'state_code',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'state_type' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_STATE_TYPE',
        'width' => '10%',
        'default' => true,
        'name' => 'state_type',
      ),
      'state_code' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_STATE_CODE',
        'width' => '10%',
        'default' => true,
        'name' => 'state_code',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
