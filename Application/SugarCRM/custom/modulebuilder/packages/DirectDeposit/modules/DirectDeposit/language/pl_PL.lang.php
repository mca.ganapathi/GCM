<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Domyślny zespół podstawowy',
  'LBL_TEAM' => 'Zespoły',
  'LBL_TEAMS' => 'Zespoły',
  'LBL_TEAM_ID' => 'ID zespołu',
  'LBL_TEAM_SET' => 'Ustawiono zespół',
  'LBL_ASSIGNED_TO_ID' => 'Przydzielono do',
  'LBL_ASSIGNED_TO_NAME' => 'Użytkownik',
  'LBL_CREATED' => 'Utworzone przez',
  'LBL_CREATED_ID' => 'ID tworzącego',
  'LBL_CREATED_USER' => 'Utworzone przez użytkownika',
  'LBL_DATE_ENTERED' => 'Data utworzenia',
  'LBL_DATE_MODIFIED' => 'Data modyfikacji',
  'LBL_DELETED' => 'Usunięto',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_EDIT_BUTTON' => 'Edytuj',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nazwa',
  'LBL_MODIFIED' => 'Zmodyfikowane przez',
  'LBL_MODIFIED_ID' => 'ID modyfikującego',
  'LBL_MODIFIED_NAME' => 'Zmodyfikowano przez',
  'LBL_MODIFIED_USER' => 'Zmodyfikowane przez użytkownika',
  'LBL_NAME' => 'Nazwa',
  'LBL_REMOVE' => 'Usuń',
  'LBL_LIST_FORM_TITLE' => 'Direct Deposit Lista',
  'LBL_MODULE_NAME' => 'Direct Deposit',
  'LBL_MODULE_TITLE' => 'Direct Deposit',
  'LBL_HOMEPAGE_TITLE' => 'Moje Direct Deposit',
  'LNK_NEW_RECORD' => 'Utwórz Direct Deposit',
  'LNK_LIST' => 'Widok Direct Deposit',
  'LNK_IMPORT_GC_DIRECTDEPOSIT' => 'Import Direct Deposit',
  'LBL_SEARCH_FORM_TITLE' => 'Wyszukiwanie Direct Deposit',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historia zmian',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Wydarzenia',
  'LBL_GC_DIRECTDEPOSIT_SUBPANEL_TITLE' => 'Direct Deposit',
  'LBL_NEW_FORM_TITLE' => 'Nowy Direct Deposit',
  'LBL_CONTRACT_HISTORY_ID' => 'Contract Line Item History ID',
  'LBL_BANK_CODE' => 'Bank Code',
  'LBL_BRANCH_CODE' => 'Branch Code',
  'LBL_DEPOSIT_TYPE' => 'Deposit Type',
  'LBL_ACCOUNT_NUMBER' => 'Account Number',
  'LBL_PAYEE_CONTACT_PERSON_NAME_1' => 'Payee Contact Person Name (Latin Character)',
  'LBL_PAYEE_CONTACT_PERSON_NAME_2' => 'Payee Contact Person Name (Local Character)',
  'LBL_PAYEE_EMAIL_ADDRESS' => 'Payee E-mail Address',
  'LBL_REMARKS' => 'Remarks',
  'LBL_PAYEE_TEL_NO' => 'Payee Telephone Number',
  'LBL_ACCOUNT_NO' => 'Account Number',
);