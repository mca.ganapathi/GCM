<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Default Primary Team',
  'LBL_TEAM_SET' => 'Team Set',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_LIST_FORM_TITLE' => 'Direct Deposit List',
  'LBL_MODULE_NAME' => 'Direct Deposit',
  'LBL_MODULE_TITLE' => 'Direct Deposit',
  'LBL_HOMEPAGE_TITLE' => 'My Direct Deposit',
  'LNK_NEW_RECORD' => 'Create Direct Deposit',
  'LNK_LIST' => 'View Direct Deposit',
  'LNK_IMPORT_GC_DIRECTDEPOSIT' => 'Import Direct Deposit',
  'LBL_SEARCH_FORM_TITLE' => 'Search Direct Deposit',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_GC_DIRECTDEPOSIT_SUBPANEL_TITLE' => 'Direct Deposit',
  'LBL_NEW_FORM_TITLE' => 'New Direct Deposit',
  'LBL_CONTRACT_HISTORY_ID' => 'Contract Line Item History ID',
  'LBL_BANK_CODE' => 'Bank Code',
  'LBL_BRANCH_CODE' => 'Branch Code',
  'LBL_DEPOSIT_TYPE' => 'Deposit Type',
  'LBL_ACCOUNT_NUMBER' => 'Account No',
  'LBL_PAYEE_CONTACT_PERSON_NAME_1' => 'Payee Contact Person Name (Latin Character)',
  'LBL_PAYEE_CONTACT_PERSON_NAME_2' => 'Payee Contact Person Name (Local Character)',
  'LBL_PAYEE_EMAIL_ADDRESS' => 'Payee E-mail Address',
  'LBL_REMARKS' => 'Remarks',
  'LBL_PAYEE_TEL_NO' => 'Payee Telephone Number',
  'LBL_ACCOUNT_NO' => 'Account Number',
);