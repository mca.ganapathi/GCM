<?php
// created: 2018-03-07 11:46:19
$config = array (
  'team_security' => false,
  'assignable' => true,
  'acl' => true,
  'has_tab' => false,
  'studio' => true,
  'audit' => true,
  'templates' => 
  array (
    'basic' => 1,
  ),
  'label' => 'Product Configuration',
  'importable' => true,
  'taggable' => 1,
);