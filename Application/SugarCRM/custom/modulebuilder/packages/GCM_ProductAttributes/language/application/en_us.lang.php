<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['pa_ProductAttributes'] = 'Product Attributes';
$app_list_strings['moduleList']['pa_ProductAttributeValues'] = 'Product Attribute Values';
$app_list_strings['attribute_type_list'][''] = '';
$app_list_strings['attribute_type_list']['AttributeValue'] = 'Attribute Value';
$app_list_strings['attribute_type_list']['ParentAttribute'] = 'Parent Attribute';
$app_list_strings['attribute_type_list']['EntryField'] = 'Entry Field';
$app_list_strings['pa_value_data_type_list']['Text'] = 'Text';
$app_list_strings['pa_value_data_type_list']['Number'] = 'Number';
$app_list_strings['pa_value_data_type_list']['Date'] = 'Date';
$app_list_strings['pa_value_data_type_list']['DateTime'] = 'Date & Time';
$app_list_strings['pa_value_data_type_list']['Boolean'] = 'Boolean';
