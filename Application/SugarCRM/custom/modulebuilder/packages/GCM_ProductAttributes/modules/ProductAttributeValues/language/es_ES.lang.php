<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Equipo Principal por Defecto',
  'LBL_TEAM' => 'Equipos',
  'LBL_TEAMS' => 'Equipos',
  'LBL_TEAM_ID' => 'Id Equipo',
  'LBL_TEAM_SET' => 'Conjunto de Equipo',
  'LBL_ASSIGNED_TO_ID' => 'Asignado a Usuario con Id',
  'LBL_ASSIGNED_TO_NAME' => 'Usuario',
  'LBL_CREATED' => 'Creado Por',
  'LBL_CREATED_ID' => 'Creado Por Id',
  'LBL_CREATED_USER' => 'Creado Por Usuario',
  'LBL_DATE_ENTERED' => 'Fecha de Creación',
  'LBL_DATE_MODIFIED' => 'Última Modificación',
  'LBL_DELETED' => 'Eliminado',
  'LBL_DESCRIPTION' => 'Descripción',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nombre',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nombre',
  'LBL_MODIFIED_USER' => 'Modificado Por Usuario',
  'LBL_NAME' => 'Nombre',
  'LBL_REMOVE' => 'Quitar',
  'LBL_LIST_FORM_TITLE' => 'Product Attribute Values Lista',
  'LBL_MODULE_NAME' => 'Product Attribute Values',
  'LBL_MODULE_TITLE' => 'Product Attribute Values',
  'LBL_HOMEPAGE_TITLE' => 'Mi Product Attribute Values',
  'LNK_NEW_RECORD' => 'Crear Product Attribute Values',
  'LNK_LIST' => 'Vista Product Attribute Values',
  'LNK_IMPORT_PA_PRODUCTATTRIBUTEVALUES' => 'Import Product Attribute Values',
  'LBL_SEARCH_FORM_TITLE' => 'Buscar Product Attribute Values',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Actividades',
  'LBL_PA_PRODUCTATTRIBUTEVALUES_SUBPANEL_TITLE' => 'Product Attribute Values',
  'LBL_NEW_FORM_TITLE' => 'Nuevo Product Attribute Values',
  'LBL_UOM' => 'UoM',
  'LBL_PARENT_ATTRIBUTE_PA_PRODUCTATTRIBUTES_ID' => 'Product Attribute ID (related  ID)',
  'LBL_PARENT_ATTRIBUTE' => 'Product Attribute ID',
);