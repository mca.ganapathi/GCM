<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Noklusētā primārā darba grupa',
  'LBL_TEAM' => 'Darba grupa',
  'LBL_TEAMS' => 'Darba grupas',
  'LBL_TEAM_ID' => 'Darba grupas ID',
  'LBL_TEAM_SET' => 'Darba grupu kopa',
  'LBL_ASSIGNED_TO_ID' => 'Piešķirts lietotājam ar Id',
  'LBL_ASSIGNED_TO_NAME' => 'Piešķirts lietotājam',
  'LBL_CREATED' => 'Izveidoja',
  'LBL_CREATED_ID' => 'Izveidotāja ID',
  'LBL_CREATED_USER' => 'Izveidoja',
  'LBL_DATE_ENTERED' => 'Izveidots',
  'LBL_DATE_MODIFIED' => 'Modificēts',
  'LBL_DELETED' => 'Dzēsts',
  'LBL_DESCRIPTION' => 'Apraksts',
  'LBL_EDIT_BUTTON' => 'Rediģēt',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nosaukums',
  'LBL_MODIFIED' => 'Modificēja',
  'LBL_MODIFIED_ID' => 'Modificētāja ID',
  'LBL_MODIFIED_NAME' => 'Modificēja',
  'LBL_MODIFIED_USER' => 'Modificēja lietotājs',
  'LBL_NAME' => 'Nosaukums',
  'LBL_REMOVE' => 'Noņemt',
  'LBL_LIST_FORM_TITLE' => 'Product Attributes Saraksts',
  'LBL_MODULE_NAME' => 'Product Attributes',
  'LBL_MODULE_TITLE' => 'Product Attributes',
  'LBL_HOMEPAGE_TITLE' => 'Mans Product Attributes',
  'LNK_NEW_RECORD' => 'Izveidot Product Attributes',
  'LNK_LIST' => 'Skats Product Attributes',
  'LNK_IMPORT_PA_PRODUCTATTRIBUTES' => 'Import Product Attributes',
  'LBL_SEARCH_FORM_TITLE' => 'Meklēt Product Attributes',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Aplūkot vēsturi',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Darbības',
  'LBL_PA_PRODUCTATTRIBUTES_SUBPANEL_TITLE' => 'Product Attributes',
  'LBL_NEW_FORM_TITLE' => 'Jauns Product Attributes',
  'LBL_ATTRIBUTE_TYPE' => 'Attribute Type',
  'LBL_VALUE_DATA_TYPE' => 'Value Data Type',
  'LBL_PARENT_ATTRIBUTE_PA_PRODUCTATTRIBUTES_ID' => 'Parent Attribute ID (related  ID)',
  'LBL_PARENT_ATTRIBUTE' => 'Parent Attribute ID',
);