<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Echipa Implicita',
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'Echipa ID',
  'LBL_TEAM_SET' => 'Setare echipa',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Utilizator',
  'LBL_CREATED' => 'Creat de',
  'LBL_CREATED_ID' => 'Creata de ID',
  'LBL_CREATED_USER' => 'Creata de Utilizator',
  'LBL_DATE_ENTERED' => 'Data Crearii',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_DELETED' => 'Sters',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_MODIFIED' => 'Modificat de',
  'LBL_MODIFIED_ID' => 'Modificata de ID',
  'LBL_MODIFIED_NAME' => 'Modificata de Nume',
  'LBL_MODIFIED_USER' => 'Modificata de Utilizator',
  'LBL_NAME' => 'Nume',
  'LBL_REMOVE' => 'Inlatura',
  'LBL_LIST_FORM_TITLE' => 'Line Item Config Summary Lista',
  'LBL_MODULE_NAME' => 'Line Item Config Summary',
  'LBL_MODULE_TITLE' => 'Line Item Config Summary',
  'LBL_HOMEPAGE_TITLE' => 'Al meu Line Item Config Summary',
  'LNK_NEW_RECORD' => 'Creeaza Line Item Config Summary',
  'LNK_LIST' => 'Vizualizare Line Item Config Summary',
  'LNK_IMPORT_GC_IINEITEMCONFIGSUMMARY' => 'Import Line Item Config Summary',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta Line Item Config Summary',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_GC_IINEITEMCONFIGSUMMARY_SUBPANEL_TITLE' => 'Line Item Config Summary',
  'LBL_NEW_FORM_TITLE' => 'Nou Line Item Config Summary',
  'LBL_LINE_ITEM_ID' => 'Line Item Contract ID',
  'LBL_ATTRIBUTE_ID' => 'Attribute ID',
  'LBL_SUB_ATTRIBUTE_ID' => 'Sub Attribute ID',
  'LBL_ATTRIBUTE_VALUE_ID' => 'Attribute Value ID',
  'LBL_CUSTOM_ATTRIBUTE_VALUE' => 'Custom Attribute Value',
  'LNK_IMPORT_GC_LINE_ITEM_CONFIG_SUMMARY' => 'Import Line Item Config Summary',
  'LBL_GC_LINE_ITEM_CONFIG_SUMMARY_SUBPANEL_TITLE' => 'Line Item Config Summary',
  'LBL_CUSTOM_ATT_VAL' => 'Custom Attribute Val',
);