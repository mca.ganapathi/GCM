<?php
$viewdefs['fm_Factory']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
          'default' => true,
          'enabled' => true,
          'link' => true,
          'width' => '32%',
        ),
        1 => 
        array (
          'name' => 'date_entered',
          'enabled' => true,
          'default' => true,
          'type' => 'datetime',
          'label' => 'LBL_DATE_ENTERED',
          'width' => '10%',
        ),
        2 => 
        array (
          'name' => 'date_modified',
          'enabled' => true,
          'default' => true,
          'type' => 'datetime',
          'label' => 'LBL_DATE_MODIFIED',
          'width' => '10%',
        ),
        3 => 
        array (
          'name' => 'modified_by_name',
          'default' => false,
          'enabled' => true,
          'type' => 'relate',
          'link' => true,
          'label' => 'LBL_MODIFIED_NAME',
          'id' => 'MODIFIED_USER_ID',
          'width' => '10%',
        ),
        4 => 
        array (
          'name' => 'created_by_name',
          'default' => false,
          'enabled' => true,
          'type' => 'relate',
          'link' => true,
          'label' => 'LBL_CREATED',
          'id' => 'CREATED_BY',
          'width' => '10%',
        ),
        5 => 
        array (
          'name' => 'team_name',
          'label' => 'LBL_TEAM',
          'default' => false,
          'enabled' => true,
          'width' => '9%',
        ),
      ),
    ),
  ),
);
