<?php
$popupMeta = array (
    'moduleMain' => 'fm_Factory',
    'varName' => 'fm_Factory',
    'orderBy' => 'fm_factory.name',
    'whereClauses' => array (
  'name' => 'fm_factory.name',
),
    'searchInputs' => array (
  0 => 'fm_factory_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
    'name' => 'name',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
),
);
