<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['c_country'] = 'Country';
$app_list_strings['regions_list']['North_America'] = 'North America';
$app_list_strings['regions_list']['Latin_America'] = 'Latin America';
$app_list_strings['regions_list']['Asia'] = 'Asia';
$app_list_strings['regions_list']['Africa'] = 'Africa';
$app_list_strings['regions_list']['Europe'] = 'Europe';
$app_list_strings['regions_list']['Oceania'] = 'Oceania';
$app_list_strings['regions_list']['Antarctic'] = 'Antarctic';
$app_list_strings['regions_list']['Middle_East'] = 'Middle East';
$app_list_strings['regions_list']['Russia'] = 'Russia';
