<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_TEAM' => 'צוותים',
  'LBL_TEAMS' => 'צוותים',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'משתמש שהוקצה Id',
  'LBL_ASSIGNED_TO_NAME' => 'משתמש',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'נוצר בתאריך',
  'LBL_DATE_MODIFIED' => 'שונה בתאריך',
  'LBL_MODIFIED' => 'שונה על ידי',
  'LBL_MODIFIED_ID' => 'שונה על ידי Id',
  'LBL_MODIFIED_NAME' => 'שונה על ידי ששמו',
  'LBL_CREATED' => 'נוצר על ידי',
  'LBL_CREATED_ID' => 'נוצר על ידי Id',
  'LBL_DESCRIPTION' => 'תיאור',
  'LBL_DELETED' => 'נמחק',
  'LBL_NAME' => 'שם',
  'LBL_CREATED_USER' => 'נוצר על ידי משתמש',
  'LBL_MODIFIED_USER' => 'שונה על ידי משתמש',
  'LBL_LIST_NAME' => 'שם',
  'LBL_LIST_FORM_TITLE' => 'Line Item List',
  'LBL_MODULE_NAME' => 'Line Item',
  'LBL_MODULE_TITLE' => 'Line Item',
  'LBL_HOMEPAGE_TITLE' => 'My Line Item',
  'LNK_NEW_RECORD' => 'Create Line Item',
  'LNK_LIST' => 'View Line Item',
  'LNK_IMPORT_GC_LINEITEM' => 'Import Line Item',
  'LBL_SEARCH_FORM_TITLE' => 'Search Line Item',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_GC_LINEITEM_SUBPANEL_TITLE' => 'Line Item',
  'LBL_NEW_FORM_TITLE' => 'New Line Item',
  'LBL_GLOBAL_CONTRACT_ID' => 'Global Contract Item  ID',
  'LBL_CONTRACT_PRODUCT_ID' => 'Product',
  'LBL_PRODUCT_QUANTITY' => 'Quantity',
  'LBL_NRC' => 'NRC',
  'LBL_MRC' => 'MRC',
  'LBL_ADDITIONAL_FEE' => 'Additional Fee',
  'LBL_PRODUCTS_ID' => 'Product ID',
);