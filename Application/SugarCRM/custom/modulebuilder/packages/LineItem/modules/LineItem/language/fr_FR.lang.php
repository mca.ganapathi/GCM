<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Equipe principale par défaut',
  'LBL_TEAM' => 'Equipe',
  'LBL_TEAMS' => 'Equipes',
  'LBL_TEAM_ID' => 'Equipe (ID)',
  'LBL_TEAM_SET' => 'Groupement d&#39;équipes',
  'LBL_ASSIGNED_TO_ID' => 'Assigné à (ID)',
  'LBL_ASSIGNED_TO_NAME' => 'Assigné à',
  'LBL_CREATED' => 'Créé par',
  'LBL_CREATED_ID' => 'Créé par (ID)',
  'LBL_CREATED_USER' => 'Créé par',
  'LBL_DATE_ENTERED' => 'Date de création',
  'LBL_DATE_MODIFIED' => 'Date de modification',
  'LBL_DELETED' => 'Supprimé',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_EDIT_BUTTON' => 'Editer',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_MODIFIED' => 'Modifié par',
  'LBL_MODIFIED_ID' => 'Modifié par (ID)',
  'LBL_MODIFIED_NAME' => 'Modifié par',
  'LBL_MODIFIED_USER' => 'Modifié par',
  'LBL_NAME' => 'Nom',
  'LBL_REMOVE' => 'Supprimer',
  'LBL_LIST_FORM_TITLE' => 'Line Item Liste',
  'LBL_MODULE_NAME' => 'Line Item',
  'LBL_MODULE_TITLE' => 'Line Item',
  'LBL_HOMEPAGE_TITLE' => 'Mes Line Item',
  'LNK_NEW_RECORD' => 'Créer Line Item',
  'LNK_LIST' => 'Vue Line Item',
  'LNK_IMPORT_GC_LINEITEM' => 'Import Line Item',
  'LBL_SEARCH_FORM_TITLE' => 'Recherche Line Item',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Voir l&#39;Historique',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activités',
  'LBL_GC_LINEITEM_SUBPANEL_TITLE' => 'Line Item',
  'LBL_NEW_FORM_TITLE' => 'Nouveau Line Item',
  'LBL_GLOBAL_CONTRACT_ID' => 'Global Contract Item  ID',
  'LBL_CONTRACT_PRODUCT_ID' => 'Product',
  'LBL_PRODUCT_QUANTITY' => 'Quantity',
  'LBL_NRC' => 'NRC',
  'LBL_MRC' => 'MRC',
  'LBL_ADDITIONAL_FEE' => 'Additional Fee',
  'LBL_PRODUCTS_ID' => 'Product ID',
);