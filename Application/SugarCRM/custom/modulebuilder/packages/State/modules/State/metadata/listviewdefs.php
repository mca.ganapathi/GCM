<?php
// created: 2018-03-07 11:45:26
$listViewDefs['c_State'] = array (
  'NAME' => 
  array (
    'width' => '32',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'STATE_TYPE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_STATE_TYPE',
    'width' => '10',
    'default' => true,
  ),
  'STATE_CODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_STATE_CODE',
    'width' => '10',
    'default' => true,
  ),
);