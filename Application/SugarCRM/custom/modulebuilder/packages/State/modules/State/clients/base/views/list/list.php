<?php
$viewdefs['c_State']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
          'default' => true,
          'enabled' => true,
          'link' => true,
          'width' => '32%',
        ),
        1 => 
        array (
          'name' => 'state_type',
          'default' => true,
          'enabled' => true,
          'type' => 'varchar',
          'label' => 'LBL_STATE_TYPE',
          'width' => '10%',
        ),
        2 => 
        array (
          'name' => 'state_code',
          'default' => true,
          'enabled' => true,
          'type' => 'varchar',
          'label' => 'LBL_STATE_CODE',
          'width' => '10%',
        ),
      ),
    ),
  ),
);
