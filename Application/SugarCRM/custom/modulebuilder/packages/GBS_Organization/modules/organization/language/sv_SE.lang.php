<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Tilldelat användar-ID',
  'LBL_ASSIGNED_TO_NAME' => 'Tilldelad användare',
  'LBL_TAGS_LINK' => 'Taggar',
  'LBL_TAGS' => 'Taggar',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum skapat',
  'LBL_DATE_MODIFIED' => 'Modifierat Datum',
  'LBL_MODIFIED' => 'Modifierat Av',
  'LBL_MODIFIED_ID' => 'Modifierat Av Id',
  'LBL_MODIFIED_NAME' => 'Modifierat Av Namn',
  'LBL_CREATED' => 'Skapat Av',
  'LBL_CREATED_ID' => 'Skapat Av',
  'LBL_DOC_OWNER' => 'Ägare av dokument',
  'LBL_USER_FAVORITES' => 'Användare som favorite',
  'LBL_DESCRIPTION' => 'Beskrivning',
  'LBL_DELETED' => 'Raderad',
  'LBL_NAME' => 'Namn',
  'LBL_CREATED_USER' => 'Skapat Av Användare',
  'LBL_MODIFIED_USER' => 'Modifierat Av Användare',
  'LBL_LIST_NAME' => 'Namn',
  'LBL_EDIT_BUTTON' => 'Redigera',
  'LBL_REMOVE' => 'Ta bort',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ändrad av namn',
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Lag-ID',
  'LBL_LIST_FORM_TITLE' => 'Organizations List',
  'LBL_MODULE_NAME' => 'Organizations',
  'LBL_MODULE_TITLE' => 'Organizations',
  'LBL_MODULE_NAME_SINGULAR' => 'Organization',
  'LBL_HOMEPAGE_TITLE' => 'Min Organizations',
  'LNK_NEW_RECORD' => 'Create Organization',
  'LNK_LIST' => 'Visa Organizations',
  'LNK_IMPORT_GC_ORGANIZATION' => 'Import Organization',
  'LBL_SEARCH_FORM_TITLE' => 'Search Organization',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetsström',
  'LBL_GC_ORGANIZATION_SUBPANEL_TITLE' => 'Organizations',
  'LBL_NEW_FORM_TITLE' => 'Ny Organization',
  'LNK_IMPORT_VCARD' => 'Import Organization vCard',
  'LBL_IMPORT' => 'Import Organizations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Organization record by importing a vCard from your file system.',
  'LBL_ORGANIZATION_NAME_2_LOCAL' => 'Organization Name 2 (Local)',
  'LBL_ORGANIZATION_NAME_3_LOCAL' => 'Organization Name 3 (Local)',
  'LBL_ORGANIZATION_NAME_4_LOCAL' => 'Organization Name 4 (Local)',
  'LBL_ORGANIZATION_NAME_1_LATIN' => 'Organization Name 1 (Latin)',
  'LBL_ORGANIZATION_NAME_2_LATIN' => 'Organization Name 2 (Latin)',
  'LBL_ORGANIZATION_NAME_3_LATIN' => 'Organization Name 3 (Latin)',
  'LBL_ORGANIZATION_NAME_4_LATIN' => 'Organization Name 4 (Latin)',
  'LBL_ORGANIZATION_CODE_1' => 'Organization Code 1',
  'LBL_ORGANIZATION_CODE_2' => 'Organization Code 2',
  'LBL_SALES_ORG_FLAG' => 'Sales Org Flag',
  'LBL_BILLING_ORG_FLAG' => 'Billing Org Flag',
  'LBL_SERVICE_ORG_FLAG' => 'Service Org Flag',
  'LBL_BILLING_AFFILIATE_CODE' => 'GBS Billing Affiliate Code',
);