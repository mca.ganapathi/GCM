<?php
// created: 2018-03-07 11:46:19
$config = array (
  'team_security' => false,
  'assignable' => true,
  'taggable' => 1,
  'acl' => true,
  'has_tab' => true,
  'studio' => true,
  'audit' => true,
  'activity_enabled' => 0,
  'templates' => 
  array (
    'basic' => 1,
  ),
  'label' => 'Organizations',
  'label_singular' => 'Organization',
  'importable' => true,
);