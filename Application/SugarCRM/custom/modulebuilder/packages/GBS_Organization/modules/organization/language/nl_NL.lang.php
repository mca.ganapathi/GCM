<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen aan ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum ingevoerd',
  'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door Naam',
  'LBL_CREATED' => 'Gemaakt door',
  'LBL_CREATED_ID' => 'Gemaakt door ID',
  'LBL_DOC_OWNER' => 'Documenteigenaar',
  'LBL_USER_FAVORITES' => 'Gebruikers die favoriet gemaakt hebben',
  'LBL_DESCRIPTION' => 'Beschrijving',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_NAME' => 'Naam',
  'LBL_CREATED_USER' => 'Gemaakt door Gebruiker',
  'LBL_MODIFIED_USER' => 'Gewijzigd door Gebruiker',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_EDIT_BUTTON' => 'Wijzig',
  'LBL_REMOVE' => 'Verwijder',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Gewijzigd door Naam',
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_LIST_FORM_TITLE' => 'Organizations List',
  'LBL_MODULE_NAME' => 'Organizations',
  'LBL_MODULE_TITLE' => 'Organizations',
  'LBL_MODULE_NAME_SINGULAR' => 'Organization',
  'LBL_HOMEPAGE_TITLE' => 'Mijn Organizations',
  'LNK_NEW_RECORD' => 'Create Organization',
  'LNK_LIST' => 'View Organizations',
  'LNK_IMPORT_GC_ORGANIZATION' => 'Import Organization',
  'LBL_SEARCH_FORM_TITLE' => 'Zoeken Organization',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_GC_ORGANIZATION_SUBPANEL_TITLE' => 'Organizations',
  'LBL_NEW_FORM_TITLE' => 'Nieuw Organization',
  'LNK_IMPORT_VCARD' => 'Import Organization vCard',
  'LBL_IMPORT' => 'Import Organizations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Organization record by importing a vCard from your file system.',
  'LBL_ORGANIZATION_NAME_2_LOCAL' => 'Organization Name 2 (Local)',
  'LBL_ORGANIZATION_NAME_3_LOCAL' => 'Organization Name 3 (Local)',
  'LBL_ORGANIZATION_NAME_4_LOCAL' => 'Organization Name 4 (Local)',
  'LBL_ORGANIZATION_NAME_1_LATIN' => 'Organization Name 1 (Latin)',
  'LBL_ORGANIZATION_NAME_2_LATIN' => 'Organization Name 2 (Latin)',
  'LBL_ORGANIZATION_NAME_3_LATIN' => 'Organization Name 3 (Latin)',
  'LBL_ORGANIZATION_NAME_4_LATIN' => 'Organization Name 4 (Latin)',
  'LBL_ORGANIZATION_CODE_1' => 'Organization Code 1',
  'LBL_ORGANIZATION_CODE_2' => 'Organization Code 2',
  'LBL_SALES_ORG_FLAG' => 'Sales Org Flag',
  'LBL_BILLING_ORG_FLAG' => 'Billing Org Flag',
  'LBL_SERVICE_ORG_FLAG' => 'Service Org Flag',
  'LBL_BILLING_AFFILIATE_CODE' => 'GBS Billing Affiliate Code',
);