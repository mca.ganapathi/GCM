<?php
$viewdefs['pm_Products']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'link' => true,
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'width' => '32%',
        ),
        1 => 
        array (
          'name' => 'assigned_user_name',
          'default' => false,
          'enabled' => true,
          'width' => '9%',
          'label' => 'LBL_ASSIGNED_TO_NAME',
          'id' => 'ASSIGNED_USER_ID',
        ),
      ),
    ),
  ),
);
