<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_TEAM' => 'Komandos',
  'LBL_TEAMS' => 'Komandos',
  'LBL_TEAM_ID' => 'Komandos Id',
  'LBL_ASSIGNED_TO_ID' => 'Atsakingo Id',
  'LBL_ASSIGNED_TO_NAME' => 'Atsakingas',
  'LBL_CREATED' => 'Sukūrė',
  'LBL_CREATED_ID' => 'Kūrėjo Id',
  'LBL_CREATED_USER' => 'Sukūrė',
  'LBL_DATE_ENTERED' => 'Sukurta',
  'LBL_DATE_MODIFIED' => 'Redaguota',
  'LBL_DELETED' => 'Ištrintas',
  'LBL_DESCRIPTION' => 'Aprašymas',
  'LBL_EDIT_BUTTON' => 'Redaguoti',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Pavadinimas',
  'LBL_MODIFIED' => 'Redagavo',
  'LBL_MODIFIED_ID' => 'Redaguotojo Id',
  'LBL_MODIFIED_NAME' => 'Redaguotojo vardas',
  'LBL_MODIFIED_USER' => 'Redagavo',
  'LBL_NAME' => 'Pavadinimas',
  'LBL_REMOVE' => 'Išimti',
  'LBL_LIST_FORM_TITLE' => 'Products Sąrašas',
  'LBL_MODULE_NAME' => 'Products',
  'LBL_MODULE_TITLE' => 'Products',
  'LBL_HOMEPAGE_TITLE' => 'Mano Products',
  'LNK_NEW_RECORD' => 'Sukurti Products',
  'LNK_LIST' => 'View Products',
  'LNK_IMPORT_PM_PRODUCTS' => 'Import Products',
  'LBL_SEARCH_FORM_TITLE' => 'Paieška Products',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Rodyti istoriją',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Priminimai',
  'LBL_PM_PRODUCTS_SUBPANEL_TITLE' => 'Products',
  'LBL_NEW_FORM_TITLE' => 'Naujas Products',
  'LBL_PRODUCTS_ID' => 'Products ID',
  'LBL_PRODUCT_STATUS' => 'Product Status',
  'LBL_PRODUCT_VERSION' => 'Product Version',
  'LBL_PRODUCT_INTRODUCTION_DATE' => 'Product Introduction Date',
  'LBL_PRODUCT_SALES_END_DATE' => 'Product Sales End Date',
  'LBL_FACTORY_ID' => 'Factory ID',
  'LBL_PRODUCT_TYPE' => 'Product Type',
);