<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Default Primary Team',
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_TEAM_SET' => 'Team Set',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_DELETED' => 'Deleted',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Name',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_NAME' => 'Name',
  'LBL_REMOVE' => 'Remove',
  'LBL_LIST_FORM_TITLE' => 'Products List',
  'LBL_MODULE_NAME' => 'Products',
  'LBL_MODULE_TITLE' => 'Products',
  'LBL_HOMEPAGE_TITLE' => 'My Products',
  'LNK_NEW_RECORD' => 'Create Products',
  'LNK_LIST' => 'View Products',
  'LNK_IMPORT_PM_PRODUCTS' => 'Import Products',
  'LBL_SEARCH_FORM_TITLE' => 'Search Products',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_PM_PRODUCTS_SUBPANEL_TITLE' => 'Products',
  'LBL_NEW_FORM_TITLE' => 'New Products',
  'LBL_PRODUCTS_ID' => 'Products ID',
  'LBL_PRODUCT_STATUS' => 'Product Status',
  'LBL_PRODUCT_VERSION' => 'Product Version',
  'LBL_PRODUCT_INTRODUCTION_DATE' => 'Product Introduction Date',
  'LBL_PRODUCT_SALES_END_DATE' => 'Product Sales End Date',
  'LBL_FACTORY_ID' => 'Factory ID',
  'LBL_PRODUCT_TYPE' => 'Product Type',
);