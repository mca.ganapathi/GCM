<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Equip principal per defecte',
  'LBL_TEAM' => 'Equip',
  'LBL_TEAMS' => 'Equips',
  'LBL_TEAM_ID' => 'Id Equip',
  'LBL_TEAM_SET' => 'Selecció d&#39;equip',
  'LBL_ASSIGNED_TO_ID' => 'Usuari Assignat',
  'LBL_ASSIGNED_TO_NAME' => 'Assignat a',
  'LBL_CREATED' => 'Creat Per',
  'LBL_CREATED_ID' => 'Creat Per Id',
  'LBL_CREATED_USER' => 'Creat Per Usuari',
  'LBL_DATE_ENTERED' => 'Data de Creació',
  'LBL_DATE_MODIFIED' => 'Última Modificació',
  'LBL_DELETED' => 'Eliminat',
  'LBL_DESCRIPTION' => 'Descripció',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_MODIFIED' => 'Modificat Per',
  'LBL_MODIFIED_ID' => 'Modificat Per Id',
  'LBL_MODIFIED_NAME' => 'Modificat Per Nom',
  'LBL_MODIFIED_USER' => 'Modificat Per Usuari',
  'LBL_NAME' => 'Nom',
  'LBL_REMOVE' => 'Treure',
  'LBL_LIST_FORM_TITLE' => 'Contract Comments Llista',
  'LBL_MODULE_NAME' => 'Contract Comments',
  'LBL_MODULE_TITLE' => 'Contract Comments',
  'LBL_HOMEPAGE_TITLE' => 'Meu Contract Comments',
  'LNK_NEW_RECORD' => 'Crear Contract Comments',
  'LNK_LIST' => 'Vista Contract Comments',
  'LNK_IMPORT_GC_COMMENTS' => 'Import Contract Comments',
  'LBL_SEARCH_FORM_TITLE' => 'Cercar Contract Comments',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Veure Historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitats',
  'LBL_GC_COMMENTS_SUBPANEL_TITLE' => 'Contract Comments',
  'LBL_NEW_FORM_TITLE' => 'Nou Contract Comments',
  'LBL_CONTRACT_NAME_GC_CONTRACTS_ID' => 'Contract Name (related  ID)',
  'LBL_CONTRACT_NAME' => 'Contract Name',
);