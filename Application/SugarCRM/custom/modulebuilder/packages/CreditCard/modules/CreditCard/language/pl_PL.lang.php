<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Domyślny zespół podstawowy',
  'LBL_TEAM' => 'Zespoły',
  'LBL_TEAMS' => 'Zespoły',
  'LBL_TEAM_ID' => 'ID zespołu',
  'LBL_TEAM_SET' => 'Ustawiono zespół',
  'LBL_ASSIGNED_TO_ID' => 'Przydzielono do',
  'LBL_ASSIGNED_TO_NAME' => 'Użytkownik',
  'LBL_CREATED' => 'Utworzone przez',
  'LBL_CREATED_ID' => 'ID tworzącego',
  'LBL_CREATED_USER' => 'Utworzone przez użytkownika',
  'LBL_DATE_ENTERED' => 'Data utworzenia',
  'LBL_DATE_MODIFIED' => 'Data modyfikacji',
  'LBL_DELETED' => 'Usunięto',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_EDIT_BUTTON' => 'Edytuj',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nazwa',
  'LBL_MODIFIED' => 'Zmodyfikowane przez',
  'LBL_MODIFIED_ID' => 'ID modyfikującego',
  'LBL_MODIFIED_NAME' => 'Zmodyfikowano przez',
  'LBL_MODIFIED_USER' => 'Zmodyfikowane przez użytkownika',
  'LBL_NAME' => 'Nazwa',
  'LBL_REMOVE' => 'Usuń',
  'LBL_LIST_FORM_TITLE' => 'Credit Card Lista',
  'LBL_MODULE_NAME' => 'Credit Card',
  'LBL_MODULE_TITLE' => 'Credit Card',
  'LBL_HOMEPAGE_TITLE' => 'Moje Credit Card',
  'LNK_NEW_RECORD' => 'Utwórz Credit Card',
  'LNK_LIST' => 'Widok Credit Card',
  'LNK_IMPORT_GC_CREDITCARD' => 'Import Credit Card',
  'LBL_SEARCH_FORM_TITLE' => 'Wyszukiwanie Credit Card',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historia zmian',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Wydarzenia',
  'LBL_GC_CREDITCARD_SUBPANEL_TITLE' => 'Credit Card',
  'LBL_NEW_FORM_TITLE' => 'Nowy Credit Card',
  'LBL_CONTRACT_HISTORY_ID' => 'Contract Line Item History ID',
  'LBL_CREDIT_CARD_NO' => 'Credit-Card Number',
  'LBL_EXPIRATION_DATE' => 'expiration date',
);