<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Varsayılan Asıl Takım',
  'LBL_TEAM' => 'Takımlar',
  'LBL_TEAMS' => 'Takımlar',
  'LBL_TEAM_ID' => 'Takım ID',
  'LBL_TEAM_SET' => 'Takım Ayarla',
  'LBL_ASSIGNED_TO_ID' => 'Atanan Kullanıcı ID',
  'LBL_ASSIGNED_TO_NAME' => 'Atanan Kişi',
  'LBL_CREATED' => 'Oluşturan',
  'LBL_CREATED_ID' => 'Oluşturan ID',
  'LBL_CREATED_USER' => 'Oluşturan Kullanıcı',
  'LBL_DATE_ENTERED' => 'Oluşturulma Tarihi',
  'LBL_DATE_MODIFIED' => 'Değiştirilme Tarihi',
  'LBL_DELETED' => 'Silindi',
  'LBL_DESCRIPTION' => 'Tanım',
  'LBL_EDIT_BUTTON' => 'Değiştir',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'İsim',
  'LBL_MODIFIED' => 'Değiştiren',
  'LBL_MODIFIED_ID' => 'Değiştiren ID',
  'LBL_MODIFIED_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_MODIFIED_USER' => 'Değiştiren Kullanıcı',
  'LBL_NAME' => 'İsim',
  'LBL_REMOVE' => 'Sil',
  'LBL_LIST_FORM_TITLE' => 'Contracts Liste',
  'LBL_MODULE_NAME' => 'Contracts',
  'LBL_MODULE_TITLE' => 'Contracts',
  'LBL_HOMEPAGE_TITLE' => 'Benim Contracts',
  'LNK_NEW_RECORD' => 'Oluştur Contracts',
  'LNK_LIST' => 'Göster Contracts',
  'LNK_IMPORT_GC_CONTRACTS' => 'Import Contracts',
  'LBL_SEARCH_FORM_TITLE' => 'Ara Contracts',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Tarihçeyi Gör',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteler',
  'LBL_GC_CONTRACTS_SUBPANEL_TITLE' => 'Contracts',
  'LBL_NEW_FORM_TITLE' => 'Yeni Contracts',
  'LBL_GLOBAL_CONTRACT_ID' => 'Global Contract ID',
  'LBL_CONTRACT_TYPE' => 'Contract Type',
  'LBL_CONTRACT_VERSION' => 'Contract Version',
  'LBL_CONTRACT_STATUS' => 'Contract Status',
  'LBL_LOI_CONTRACT' => 'LOI Contract',
  'LBL_BILLING_TYPE' => 'Billing Type',
  'LBL_TOTAL_NRC' => 'Total NRC',
  'LBL_TOTAL_MRC' => 'Total MRC',
  'LBL_GRAND_TOTAL' => 'Grand Total',
  'LBL_CONTRACT_REQUEST_ID' => 'Contract Request ID',
  'LBL_EXT_SYS_CREATED_BY' => 'External System Created By',
  'LBL_EXT_SYS_MODIFIED_BY' => 'External System Modified By',
  'LBL_DATE_ENTERED_TIMEZONE' => 'Created Date Time Zone',
  'LBL_DATE_MODIFIED_TIMEZONE' => 'Modified Date Time Zone',
  'LBL_FACTORY_REFERENCE_NO' => 'Factory Reference Number',
  'LBL_CONTRACT_STAGE' => 'Contract Stage',
  'LBL_WORKFLOW_FLAG' => 'Workflow Flag',
  'LBL_CONTRACT_SCHEME' => 'Contract Scheme',
  'LBL_CONTRACT_STARTDATE' => 'Contract Start Date',
  'LBL_CONTRACT_STARTDATE_TIMEZONE' => 'Contract Start Date Time Zone',
  'LBL_CONTRACT_PERIOD' => 'Contract Period',
  'LBL_CONTRACT_PERIOD_UNIT' => 'Contract Period Unit',
  'LBL_PRODUCT_OFFERING' => 'Product Offering',
  'LBL_CONTRACT_STARTDATE_TIMEZONE_GC_TIMEZONE_ID' => 'Contract Start Date TimeZone (related  ID)',
  'LBL_GLOBAL_CONTRACT_ID_UUID' => 'Global Contract ID corresponding UUID',
  'LBL_CONTRACT_SCHEME_UUID' => 'Contract Scheme Value UUID',
  'LBL_CONTRACT_TYPE_UUID' => 'Contract Type Value UUID',
  'LBL_LOI_CONTRACT_UUID' => 'LOI Contract Value UUID',
  'LBL_BILLING_TYPE_UUID' => 'Billing Scheme Value UUID',
  'LBL_PRODUCT_OFFERING_UUID' => 'Contract Product Offering UUID',
  'LBL_FACTORY_REFERENCE_NO_UUID' => 'Factory Reference Number Value UUID',
  'LBL_LOCK_VERSION' => 'Lock Version',
  'LBL_CONTRACT_ENDDATE' => 'Contract End Date',
  'LBL_CONTRACT_ENDDATE_TIMEZONE_GC_TIMEZONE_ID' => 'Contract End Date TimeZone (related  ID)',
  'LBL_CONTRACT_ENDDATE_TIMEZONE' => 'Contract End Date TimeZone',
  'LBL_VAT_NO' => 'VAT Number',
);