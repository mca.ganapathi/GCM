<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['gc_Contracts'] = 'Contracts';
$app_list_strings['contract_type_list'][''] = '';
$app_list_strings['contract_type_list']['GMSA'] = 'GMSA (Service Ts&Cs + SOF)';
$app_list_strings['contract_type_list']['Adhesion_Contract'] = 'Adhesion Contract';
$app_list_strings['contract_type_list']['Terms_of_Service'] = 'Terms of Service';
$app_list_strings['contract_type_list']['Individual_Contract'] = 'Individual Contract';
$app_list_strings['contract_status_list'][0] = 'Draft';
$app_list_strings['contract_status_list'][1] = 'Approved';
$app_list_strings['contract_status_list'][2] = 'Deactivated';
$app_list_strings['contract_status_list'][''] = '';
$app_list_strings['loi_contract_list'][''] = '';
$app_list_strings['loi_contract_list']['Yes'] = 'Yes';
$app_list_strings['loi_contract_list']['No'] = 'No';
$app_list_strings['billing_type_list'][''] = '';
$app_list_strings['billing_type_list']['OneStop'] = 'One-stop';
$app_list_strings['billing_type_list']['Separate'] = 'Separate';
$app_list_strings['contract_stage_list']['stage0'] = 'Stage 0';
$app_list_strings['contract_stage_list']['stage1'] = 'Stage 1';
$app_list_strings['contract_stage_list']['stage2'] = 'Stage 2';
$app_list_strings['contract_stage_list']['stage3'] = 'Stage 3';
$app_list_strings['workflow_flag_list'][0] = 'Contract in progress';
$app_list_strings['workflow_flag_list'][1] = 'Contract submitted to workflow';
$app_list_strings['workflow_flag_list'][2] = 'Approved by 1st level';
$app_list_strings['workflow_flag_list'][3] = 'Approved by 2nd level';
$app_list_strings['workflow_flag_list'][4] = 'Approved by 3rd level';
$app_list_strings['contract_scheme_list'][''] = '';
$app_list_strings['contract_scheme_list']['GSC'] = 'GSC';
$app_list_strings['contract_scheme_list']['Separate'] = 'Separate';
$app_list_strings['contract_period_unit_list'][''] = '';
$app_list_strings['contract_period_unit_list']['Days'] = 'Days';
$app_list_strings['contract_period_unit_list']['Months'] = 'Months';
$app_list_strings['contract_period_unit_list']['Years'] = 'Years';
