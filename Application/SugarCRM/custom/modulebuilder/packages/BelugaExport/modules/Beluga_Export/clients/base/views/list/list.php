<?php
$viewdefs['be_Beluga_Export']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'start_date',
          'default' => true,
          'enabled' => true,
          'type' => 'date',
          'label' => 'LBL_START_DATE',
          'width' => '10%',
        ),
        1 => 
        array (
          'name' => 'end_date',
          'default' => true,
          'enabled' => true,
          'type' => 'date',
          'label' => 'LBL_END_DATE',
          'width' => '10%',
        ),
        2 => 
        array (
          'name' => 'date_entered',
          'enabled' => true,
          'default' => true,
          'type' => 'datetime',
          'label' => 'LBL_DATE_ENTERED',
          'width' => '10%',
        ),
        3 => 
        array (
          'name' => 'created_by_name',
          'default' => true,
          'enabled' => true,
          'type' => 'relate',
          'link' => true,
          'label' => 'LBL_CREATED',
          'id' => 'CREATED_BY',
          'width' => '10%',
        ),
        4 => 
        array (
          'name' => 'team_name',
          'label' => 'LBL_TEAM',
          'default' => false,
          'enabled' => true,
          'width' => '9%',
        ),
      ),
    ),
  ),
);
