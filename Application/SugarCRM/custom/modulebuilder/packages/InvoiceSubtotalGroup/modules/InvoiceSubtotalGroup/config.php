<?php
// created: 2018-03-07 11:46:19
$config = array (
  'team_security' => true,
  'assignable' => true,
  'acl' => true,
  'has_tab' => false,
  'studio' => true,
  'audit' => true,
  'templates' => 
  array (
    'basic' => 1,
  ),
  'label' => 'Invoice Subtotal Group',
  'importable' => false,
  'taggable' => 1,
);