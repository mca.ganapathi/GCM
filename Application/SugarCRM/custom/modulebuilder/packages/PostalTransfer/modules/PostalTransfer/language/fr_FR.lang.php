<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Equipe principale par défaut',
  'LBL_TEAM' => 'Equipe',
  'LBL_TEAMS' => 'Equipes',
  'LBL_TEAM_ID' => 'Equipe (ID)',
  'LBL_TEAM_SET' => 'Groupement d&#39;équipes',
  'LBL_ASSIGNED_TO_ID' => 'Assigné à (ID)',
  'LBL_ASSIGNED_TO_NAME' => 'Assigné à',
  'LBL_CREATED' => 'Créé par',
  'LBL_CREATED_ID' => 'Créé par (ID)',
  'LBL_CREATED_USER' => 'Créé par',
  'LBL_DATE_ENTERED' => 'Date de création',
  'LBL_DATE_MODIFIED' => 'Date de modification',
  'LBL_DELETED' => 'Supprimé',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_EDIT_BUTTON' => 'Editer',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_MODIFIED' => 'Modifié par',
  'LBL_MODIFIED_ID' => 'Modifié par (ID)',
  'LBL_MODIFIED_NAME' => 'Modifié par',
  'LBL_MODIFIED_USER' => 'Modifié par',
  'LBL_NAME' => 'Nom',
  'LBL_REMOVE' => 'Supprimer',
  'LBL_LIST_FORM_TITLE' => 'Postal Transfer Liste',
  'LBL_MODULE_NAME' => 'Postal Transfer',
  'LBL_MODULE_TITLE' => 'Postal Transfer',
  'LBL_HOMEPAGE_TITLE' => 'Mes Postal Transfer',
  'LNK_NEW_RECORD' => 'Créer Postal Transfer',
  'LNK_LIST' => 'Vue Postal Transfer',
  'LNK_IMPORT_GC_POSTALTRANSFER' => 'Import Postal Transfer',
  'LBL_SEARCH_FORM_TITLE' => 'Recherche Postal Transfer',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Voir l&#39;Historique',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activités',
  'LBL_GC_POSTALTRANSFER_SUBPANEL_TITLE' => 'Postal Transfer',
  'LBL_NEW_FORM_TITLE' => 'Nouveau Postal Transfer',
  'LBL_CONTRACT_HISTORY_ID' => 'Contract Line Item History ID',
  'LBL_POSTAL_PASSBOOK_MARK' => 'postal passbook mark',
  'LBL_POSTAL_PASSBOOK_NO' => 'postal passbook no',
  'LBL_POSTAL_PASSBOOK' => 'Postal Passbook Number',
  'LBL_POSTAL_PASSBOOK_NO_DISPLAY' => 'Postal Passbook No Display',
);