<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_DEFAULT_PRIMARY_TEAM' => 'Echipa Implicita',
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'Echipa ID',
  'LBL_TEAM_SET' => 'Setare echipa',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Utilizator',
  'LBL_CREATED' => 'Creat de',
  'LBL_CREATED_ID' => 'Creata de ID',
  'LBL_CREATED_USER' => 'Creata de Utilizator',
  'LBL_DATE_ENTERED' => 'Data Crearii',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_DELETED' => 'Sters',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_MODIFIED' => 'Modificat de',
  'LBL_MODIFIED_ID' => 'Modificata de ID',
  'LBL_MODIFIED_NAME' => 'Modificata de Nume',
  'LBL_MODIFIED_USER' => 'Modificata de Utilizator',
  'LBL_NAME' => 'Nume',
  'LBL_REMOVE' => 'Inlatura',
  'LBL_LIST_FORM_TITLE' => 'Postal Transfer Lista',
  'LBL_MODULE_NAME' => 'Postal Transfer',
  'LBL_MODULE_TITLE' => 'Postal Transfer',
  'LBL_HOMEPAGE_TITLE' => 'Al meu Postal Transfer',
  'LNK_NEW_RECORD' => 'Creeaza Postal Transfer',
  'LNK_LIST' => 'Vizualizare Postal Transfer',
  'LNK_IMPORT_GC_POSTALTRANSFER' => 'Import Postal Transfer',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta Postal Transfer',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_GC_POSTALTRANSFER_SUBPANEL_TITLE' => 'Postal Transfer',
  'LBL_NEW_FORM_TITLE' => 'Nou Postal Transfer',
  'LBL_CONTRACT_HISTORY_ID' => 'Contract Line Item History ID',
  'LBL_POSTAL_PASSBOOK_MARK' => 'postal passbook mark',
  'LBL_POSTAL_PASSBOOK_NO' => 'postal passbook no',
  'LBL_POSTAL_PASSBOOK' => 'Postal Passbook Number',
  'LBL_POSTAL_PASSBOOK_NO_DISPLAY' => 'Postal Passbook No Display',
);