<?php
// created: 2018-03-07 11:46:19
$mod_strings = array (
  'LBL_TEAM' => 'Tímy',
  'LBL_TEAMS' => 'Tímy',
  'LBL_TEAM_ID' => 'ID tímu',
  'LBL_ASSIGNED_TO_ID' => 'Pridelené užívateľské ID',
  'LBL_ASSIGNED_TO_NAME' => 'Pridelený k',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Dátum vytvorenia',
  'LBL_DATE_MODIFIED' => 'Dátum úpravy',
  'LBL_MODIFIED' => 'Zmenil',
  'LBL_MODIFIED_ID' => 'Zmenil podľa ID',
  'LBL_MODIFIED_NAME' => 'Zmenil podľa mena',
  'LBL_CREATED' => 'Vytvoril podľa',
  'LBL_CREATED_ID' => 'Vytvoril podľa ID',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_DELETED' => 'Vymazaný',
  'LBL_NAME' => 'Názov dokumentu',
  'LBL_CREATED_USER' => 'Vytvorené používateľom',
  'LBL_MODIFIED_USER' => 'Zmenené používateľom',
  'LBL_LIST_NAME' => 'Názov',
  'LBL_EDIT_BUTTON' => 'Upraviť',
  'LBL_REMOVE' => 'Odstrániť',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmenil Meno',
  'LBL_LIST_FORM_TITLE' => 'LineItemContractHistory Zoznam',
  'LBL_MODULE_NAME' => 'LineItemContractHistory',
  'LBL_MODULE_TITLE' => 'LineItemContractHistory',
  'LBL_MODULE_NAME_SINGULAR' => 'LineItemContractHistory',
  'LBL_HOMEPAGE_TITLE' => 'Moje LineItemContractHistory',
  'LNK_NEW_RECORD' => 'Vytvoriť LineItemContractHistory',
  'LNK_LIST' => 'zobrazenie LineItemContractHistory',
  'LNK_IMPORT_GC_LINE_ITEM_CONTRACT_HISTORY' => 'Import LineItemContractHistory',
  'LBL_SEARCH_FORM_TITLE' => 'Vyhľadávanie LineItemContractHistory',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobraziť Históriu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_GC_LINE_ITEM_CONTRACT_HISTORY_SUBPANEL_TITLE' => 'LineItemContractHistory',
  'LBL_NEW_FORM_TITLE' => 'Nový LineItemContractHistory',
  'LNK_IMPORT_VCARD' => 'Import LineItemContractHistory vCard',
  'LBL_IMPORT' => 'Import LineItemContractHistory',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new LineItemContractHistory record by importing a vCard from your file system.',
  'LBL_BILLING_AFFILIATE_GC_ORGANIZATION_ID' => 'Billing Affiliate (related Organization ID)',
  'LBL_BILLING_AFFILIATE' => 'Billing Affiliate',
);