<?php
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['pi_product_item'] = 'Product Item';
$app_list_strings['product_status_list']['Draft'] = 'Draft';
$app_list_strings['product_status_list']['Approved'] = 'Approved';
$app_list_strings['product_type_list']['GMOneWorkflow'] = 'GM One Workflow';
$app_list_strings['product_type_list']['NotApplicable'] = 'Not Applicable';
