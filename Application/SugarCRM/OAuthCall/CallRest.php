<?php
/**
* A script to consume the custom web service we are producing
*
* This is a very basic script taken and modified from Lona Jane
* http://www.lornajane.net/posts/2008/using-curl-and-php-to-talk-to-a-rest-service
*
*/
ini_set('display_errors', 1);
error_reporting(E_ALL);
$serviceUrl = 'https://qa.devgcm-system.com/custom/service/v4_1_custom/rest.php';
$curl = curl_init($serviceUrl);
// These values are just made up, to show how the calls work

$xmlData =<<<EOD
<?xml version="1.0" encoding="UTF-8"?>
<req-v0:request
                xsi:schemaLocation="http://qa.devgcm-system.com/custom/XMLSchema/
                                                file:/var/www/html/custom/XMLSchema/Operation/ContractInformationRequest-v0_1.xsd"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:req-v0="http://qa.devgcm-system.com/custom/XMLSchema/contract-information"

                xmlns:cloud001="http://qa.devgcm-system.com/custom/XMLSchema/product/cloud-product001"
                xmlns:prodCntrctItem0001="http://qa.devgcm-system.com/custom/XMLSchema/business-interaction/product-contract-item0001"
                xmlns:ptyIdent001="http://qa.devgcm-system.com/custom/XMLSchema/party/corporate-identification"
                xmlns:ptyIdent002="http://qa.devgcm-system.com/custom/XMLSchema/party/sales-channel-identification"
                xmlns:cidasCmpnyNm="http://qa.devgcm-system.com/custom/XMLSchema/party/cidas-company-name"
                xmlns:pICContractPR="http://qa.devgcm-system.com/custom/XMLSchema/party/pic-contract-pseudo-role"
                xmlns:billingCustomerPR="http://qa.devgcm-system.com/custom/XMLSchema/party/billing-customer-pseudo-role"
                xmlns:pICBillingPR="http://qa.devgcm-system.com/custom/XMLSchema/party/pic-billing-pseudo-role"
                xmlns:salesChannelPR="http://qa.devgcm-system.com/custom/XMLSchema/party/sales-channel-pseudo-role"
                xmlns:pICSalesPR="http://qa.devgcm-system.com/custom/XMLSchema/party/pic-sales-pr"
                xmlns:jpnPM01="http://qa.devgcm-system.com/custom/XMLSchema/customer/jpn-account-transfer-pm"
                xmlns:jpnPM02="http://qa.devgcm-system.com/custom/XMLSchema/customer/jpn-postal-transfer-pm"
                xmlns:payM01="http://qa.devgcm-system.com/custom/XMLSchema/customer/credit-card-pm"
                xmlns:jpnPM03="http://qa.devgcm-system.com/custom/XMLSchema/customer/jpn-direct-deposit-pm"
                xmlns:jpnAddr="http://qa.devgcm-system.com/custom/XMLSchema/location/japanese-property-address"
                xmlns:usaAddr="http://qa.devgcm-system.com/custom/XMLSchema/location/american-property-address"
                xmlns:canAddr="http://qa.devgcm-system.com/custom/XMLSchema/location/canadian-property-address"
                xmlns:anyAddr="http://qa.devgcm-system.com/custom/XMLSchema/location/any-address"
                xmlns:contactMedium="http://qa.devgcm-system.com/custom/XMLSchema/party/standard-contact-medium"
                xmlns:customerTechRep="http://qa.devgcm-system.com/custom/XMLSchema/party/customer-tech-rep"
                xmlns:customerTechRepPR="http://qa.devgcm-system.com/custom/XMLSchema/party/customer-tech-rep-pseudo-role"
                xmlns:pICCustomerTechPR="http://qa.devgcm-system.com/custom/XMLSchema/party/pic-customer-tech-pr"
                xmlns:pICSalesChannelPR="http://qa.devgcm-system.com/custom/XMLSchema/party/pic-sales-channel-pseudo-role"
                xmlns:salesChannel="http://qa.devgcm-system.com/custom/XMLSchema/party/sales-channel"

                xmlns:datatypes-v0="http://qa.devgcm-system.com/custom/XMLSchema/cbe-datatypes"
                xmlns:cbe-v0="http://qa.devgcm-system.com/custom/XMLSchema/common-business-entity"
                xmlns:party-v0="http://qa.devgcm-system.com/custom/XMLSchema/party"
                xmlns:location-v0="http://qa.devgcm-system.com/custom/XMLSchema/location"
                xmlns:product-v0="http://qa.devgcm-system.com/custom/XMLSchema/product"
                xmlns:bi-v0="http://qa.devgcm-system.com/custom/XMLSchema/business-interaction"
                xmlns:customer-v0="http://qa.devgcm-system.com/custom/XMLSchema/customer"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema">

                <product-v0:bundledProduct>
                                <cbe-v0:identifiedBy>
                                                <product-v0:bundledProductKey>
                                                                <cbe-v0:type>BundledProduct</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>bundledProduct01</cbe-v0:validIDOnlyInMessage>
                                                </product-v0:bundledProductKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>Enterprise Cloud 2.0</cbe-v0:name>
                                <cbe-v0:validFor>
                                                <datatypes-v0:startDateTime>2015-08-01T10:15:00+09:00</datatypes-v0:startDateTime>
                                                <datatypes-v0:endDateTime>9999-12-31T23:59:59+09:00</datatypes-v0:endDateTime>
                                </cbe-v0:validFor>
                                <cbe-v0:describedBy>
                                                <product-v0:referenceToBundledProductOffering>
                                                                <cbe-v0:type>BundledProductOffering</cbe-v0:type>
                                                                <cbe-v0:objectID>c881eb71-0b23-4fb7-b069-2d06e60c8bea</cbe-v0:objectID>
                                                </product-v0:referenceToBundledProductOffering>
                                </cbe-v0:describedBy>
                                <product-v0:comprisedOf>
                                                <product-v0:atomicProduct>
                                                                <cbe-v0:identifiedBy>
                                                                                <product-v0:atomicProductKey>
                                                                                                <cbe-v0:type>AtomicProduct</cbe-v0:type>
                                                                                                <cbe-v0:validIDOnlyInMessage>atomicProduct01</cbe-v0:validIDOnlyInMessage>
                                                                                </product-v0:atomicProductKey>
                                                                </cbe-v0:identifiedBy>
                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                <cbe-v0:name>Enterprise Cloud 2.0</cbe-v0:name>
                                                                <cbe-v0:validFor>
                                                                                <datatypes-v0:startDateTime>2015-08-01T10:15:00+09:00</datatypes-v0:startDateTime>
                                                                                <datatypes-v0:endDateTime>9999-12-31T23:59:59+09:00</datatypes-v0:endDateTime>
                                                                </cbe-v0:validFor>
                                                                <cbe-v0:describedBy>
                                                                                <product-v0:referenceToAtomicProductOffering>
                                                                                                <cbe-v0:type>AtomicProductOffering</cbe-v0:type>
                                                                                                <cbe-v0:objectID>bc4362ac-0b23-4fbb-aee3-bd3ee5eb343a</cbe-v0:objectID>
                                                                                </product-v0:referenceToAtomicProductOffering>
                                                                </cbe-v0:describedBy>
                                                </product-v0:atomicProduct>
                                </product-v0:comprisedOf>
                </product-v0:bundledProduct>

                <bi-v0:productContract>
                                <cbe-v0:identifiedBy>
                                                <bi-v0:productContractKey>
                                                                <cbe-v0:type>ProductContract</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>prodCntrct01</cbe-v0:validIDOnlyInMessage>
                                                </bi-v0:productContractKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
<<<<<<< HEAD
                                <cbe-v0:clientRequestID>When the Next Cloud Portal calls GCM’s API to register contract information, GCM stores the data sent from the Next Cloud</cbe-v0:clientRequestID>
                                <cbe-v0:name>When the Next Cloud Portal calls GCM’s API to register contract information, GCM stores the data sent from the Next Cloud</cbe-v0:name>
=======
                                <cbe-v0:clientRequestID>0a5c454d-7c35-4956-922c-87da0f6f4349</cbe-v0:clientRequestID>
                                <cbe-v0:name>Enterprise Cloud 2.0</cbe-v0:name>
>>>>>>> 2015Oct-uat-release
<!--                        <cbe-v0:description>契約の概略</cbe-v0:description> -->
                                <cbe-v0:describedBy>
                                                <bi-v0:referenceToProductContractSpec>
                                                                <cbe-v0:type>ProductContractSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>d993ccb0-0b23-4fb7-b06b-2e54f88181a1</cbe-v0:objectID>
                                                </bi-v0:referenceToProductContractSpec>
                                </cbe-v0:describedBy>
<!--                        <bi-v0:interactionDate>2015-08-01T10:15:00.555+09:00</bi-v0:interactionDate> -->
<!--                        <bi-v0:interactionDateComplete>9999-12-31T23:59:59.999+09:00</bi-v0:interactionDateComplete> -->
                                <bi-v0:involvedRoles>
                                                <bi-v0:partyInteractionRoleKey name ="契約先">
                                                                <cbe-v0:type>PartyInteractionRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>ptyIntContractCustomerRole</cbe-v0:validIDOnlyInMessage>
                                                </bi-v0:partyInteractionRoleKey>
                                                <bi-v0:partyInteractionRoleKey name="契約先担当者">
                                                                <cbe-v0:type>PartyInteractionRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>ptyIntPICContractRole</cbe-v0:validIDOnlyInMessage>
                                                </bi-v0:partyInteractionRoleKey>
                                                <bi-v0:partyInteractionRoleKey name="請求先">
                                                                <cbe-v0:type>PartyInteractionRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>ptyIntBillingCustomerRole</cbe-v0:validIDOnlyInMessage>
                                                </bi-v0:partyInteractionRoleKey>
                                                <bi-v0:partyInteractionRoleKey name="請求先担当者">
                                                                <cbe-v0:type>PartyInteractionRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>ptyIntPICBillingRole</cbe-v0:validIDOnlyInMessage>
                                                </bi-v0:partyInteractionRoleKey>
                                                <bi-v0:partyInteractionRoleKey name="販売チャネル">
                                                                <cbe-v0:type>PartyInteractionRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>ptyIntSalesChannelRole</cbe-v0:validIDOnlyInMessage>
                                                </bi-v0:partyInteractionRoleKey>
                                                <bi-v0:partyInteractionRoleKey name="販売担当者">
                                                                <cbe-v0:type>PartyInteractionRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>ptyIntPICSalesRole</cbe-v0:validIDOnlyInMessage>
                                                </bi-v0:partyInteractionRoleKey>
                                </bi-v0:involvedRoles>

                                <bi-v0:businessInteractionItems>
                                                <bi-v0:productItem>
                                                                <cbe-v0:identifiedBy>
                                                                                <bi-v0:productItemKey>
                                                                                                <cbe-v0:type>ProductItem</cbe-v0:type>
                                                                                                <cbe-v0:validIDOnlyInMessage>prodCntrctItem01</cbe-v0:validIDOnlyInMessage>
                                                                                </bi-v0:productItemKey>
                                                                </cbe-v0:identifiedBy>
                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                <cbe-v0:name>Enterprise Cloud 2.0</cbe-v0:name>
                                                                <cbe-v0:describedBy>
                                                                                <bi-v0:referenceToProductItemSpec>
                                                                                                <cbe-v0:type>ProductItemSpecification</cbe-v0:type>
                                                                                                <cbe-v0:objectID>256ef460-0b23-4fb0-a79f-84765eecb81e</cbe-v0:objectID>
                                                                                </bi-v0:referenceToProductItemSpec>
                                                                </cbe-v0:describedBy>
                                                                <prodCntrctItem0001:productContractItem0001 name="Characteristics of Product Contract Item (任意の名前を設定)">
                                                                                <prodCntrctItem0001:billingPeriod>
                                                                                                <prodCntrctItem0001:billingStartDate>
                                                                                                                <prodCntrctItem0001:characteristicValue>
                                                                                                                                <cbe-v0:identifiedBy>
                                                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                                                <cbe-v0:validIDOnlyInMessage>billingStartDate</cbe-v0:validIDOnlyInMessage>
                                                                                                                                                </cbe-v0:charcValueKey>
                                                                                                                                </cbe-v0:identifiedBy>
                                                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                                                <cbe-v0:objectID>082ffe16-0b23-4fbe-83cd-ae9f42f3dbdf</cbe-v0:objectID>
                                                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                                                <prodCntrctItem0001:billingStartDate>2015-08-01+09:00</prodCntrctItem0001:billingStartDate>
                                                                                                                </prodCntrctItem0001:characteristicValue>
                                                                                                </prodCntrctItem0001:billingStartDate>
                                                                                                <prodCntrctItem0001:billingEndDate>
                                                                                                                <prodCntrctItem0001:characteristicValue>
                                                                                                                                <cbe-v0:identifiedBy>
                                                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                                                <cbe-v0:validIDOnlyInMessage>billingEndDate</cbe-v0:validIDOnlyInMessage>
                                                                                                                                                </cbe-v0:charcValueKey>
                                                                                                                                </cbe-v0:identifiedBy>
                                                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                                                <cbe-v0:objectID>d370e876-0b23-4fb1-934f-fb6adc50ba74</cbe-v0:objectID>
                                                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                                                <prodCntrctItem0001:billingEndDate>9999-12-31+09:00</prodCntrctItem0001:billingEndDate>
                                                                                                                </prodCntrctItem0001:characteristicValue>
                                                                                                </prodCntrctItem0001:billingEndDate>
                                                                                </prodCntrctItem0001:billingPeriod>
                                                                                <prodCntrctItem0001:servicePeriod>
                                                                                                <prodCntrctItem0001:serviceStartDate>
                                                                                                                <prodCntrctItem0001:characteristicValue>
                                                                                                                                <cbe-v0:identifiedBy>
                                                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                                                <cbe-v0:validIDOnlyInMessage>serviceStartDate</cbe-v0:validIDOnlyInMessage>
                                                                                                                                                </cbe-v0:charcValueKey>
                                                                                                                                </cbe-v0:identifiedBy>
                                                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                                                <cbe-v0:objectID>452776f6-0b23-4fbc-a415-2264e1b90678</cbe-v0:objectID>
                                                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                                                <prodCntrctItem0001:serviceStartDate>2015-08-01+09:00</prodCntrctItem0001:serviceStartDate>
                                                                                                                </prodCntrctItem0001:characteristicValue>
                                                                                                </prodCntrctItem0001:serviceStartDate>
                                                                                                <prodCntrctItem0001:serviceEndDate>
                                                                                                                <prodCntrctItem0001:characteristicValue>
                                                                                                                                <cbe-v0:identifiedBy>
                                                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                                                <cbe-v0:validIDOnlyInMessage>serviceEndDate</cbe-v0:validIDOnlyInMessage>
                                                                                                                                                </cbe-v0:charcValueKey>
                                                                                                                                </cbe-v0:identifiedBy>
                                                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                                                <cbe-v0:objectID>d1c21d42-0b23-4fb4-a835-674ed6e5184b</cbe-v0:objectID>
                                                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                                                <prodCntrctItem0001:serviceEndDate>9999-12-31+09:00</prodCntrctItem0001:serviceEndDate>
                                                                                                                </prodCntrctItem0001:characteristicValue>
                                                                                                </prodCntrctItem0001:serviceEndDate>
                                                                                </prodCntrctItem0001:servicePeriod>
                                                                </prodCntrctItem0001:productContractItem0001>
                                                                <bi-v0:action>add</bi-v0:action>
                                                                <bi-v0:quantity>
                                                                                <datatypes-v0:amount>1</datatypes-v0:amount>
                                                                                <datatypes-v0:units>each</datatypes-v0:units>
                                                                </bi-v0:quantity>
                                                                <bi-v0:involvesProduct>
                                                                                <product-v0:atomicProductKey>
                                                                                                <cbe-v0:type>AtomicProduct</cbe-v0:type>
                                                                                                <cbe-v0:validIDOnlyInMessage>atomicProduct01</cbe-v0:validIDOnlyInMessage>
                                                                                </product-v0:atomicProductKey>
                                                                </bi-v0:involvesProduct>
                                                </bi-v0:productItem>
                                </bi-v0:businessInteractionItems>
                                <!-- <bi-v0:contractID>110001234567890</bi-v0:contractID> -->
                                <bi-v0:agreementPeriod>
                                                <datatypes-v0:startDateTime>2015-08-01T10:15:00.555+09:00</datatypes-v0:startDateTime>
                                                <datatypes-v0:endDateTime>9999-12-31T23:59:59.999+09:00</datatypes-v0:endDateTime>
                                </bi-v0:agreementPeriod>

                                <bi-v0:contractFor>
                                                <product-v0:bundledProductKey>
                                                                <cbe-v0:type>BundledProduct</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>bundledProduct01</cbe-v0:validIDOnlyInMessage>
                                                </product-v0:bundledProductKey>
                                </bi-v0:contractFor>
                </bi-v0:productContract>

                <bi-v0:partyInteractionRole>
                                <cbe-v0:identifiedBy>
                                                <bi-v0:partyInteractionRoleKey>
                                                                <cbe-v0:type>PartyInteractionRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>ptyIntContractCustomerRole</cbe-v0:validIDOnlyInMessage>
                                                </bi-v0:partyInteractionRoleKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>契約先</cbe-v0:name>
                                <cbe-v0:nameLatin>Contract Customer</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <bi-v0:referenceToPartyInteractionRoleSpec>
                                                                <cbe-v0:type>PartyInteractionRoleSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>57683710-0b23-4fb2-8abb-dd4befeceef9</cbe-v0:objectID>
                                                </bi-v0:referenceToPartyInteractionRoleSpec>
                                </cbe-v0:describedBy>
                                <party-v0:partyRoleKey>
                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                <cbe-v0:validIDOnlyInMessage>contractCustomerRole</cbe-v0:validIDOnlyInMessage>
                                </party-v0:partyRoleKey>
                </bi-v0:partyInteractionRole>

                <bi-v0:partyInteractionRole>
                                <cbe-v0:identifiedBy>
                                                <bi-v0:partyInteractionRoleKey>
                                                                <cbe-v0:type>PartyInteractionRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>ptyIntPICContractRole</cbe-v0:validIDOnlyInMessage>
                                                </bi-v0:partyInteractionRoleKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>契約先担当者</cbe-v0:name>
                                <cbe-v0:nameLatin>Person in charge of contract</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <bi-v0:referenceToPartyInteractionRoleSpec>
                                                                <cbe-v0:type>PartyInteractionRoleSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>666b54c8-0b23-4fb8-98b9-3000578fd3fc</cbe-v0:objectID>
                                                </bi-v0:referenceToPartyInteractionRoleSpec>
                                </cbe-v0:describedBy>
                                <party-v0:partyRoleKey>
                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                <cbe-v0:validIDOnlyInMessage>pICContractRole</cbe-v0:validIDOnlyInMessage>
                                </party-v0:partyRoleKey>
                </bi-v0:partyInteractionRole>

                <bi-v0:partyInteractionRole>
                                <cbe-v0:identifiedBy>
                                                <bi-v0:partyInteractionRoleKey>
                                                                <cbe-v0:type>PartyInteractionRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>ptyIntBillingCustomerRole</cbe-v0:validIDOnlyInMessage>
                                                </bi-v0:partyInteractionRoleKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>請求先</cbe-v0:name>
                                <cbe-v0:nameLatin>Billing Customer</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <bi-v0:referenceToPartyInteractionRoleSpec>
                                                                <cbe-v0:type>PartyInteractionRoleSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>f9101833-0b23-4fbc-bfcf-1d8b62aeb257</cbe-v0:objectID>
                                                </bi-v0:referenceToPartyInteractionRoleSpec>
                                </cbe-v0:describedBy>
                                <party-v0:partyRoleKey>
                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                <cbe-v0:validIDOnlyInMessage>billingCustomerRole</cbe-v0:validIDOnlyInMessage>
                                </party-v0:partyRoleKey>
                </bi-v0:partyInteractionRole>

                <bi-v0:partyInteractionRole>
                                <cbe-v0:identifiedBy>
                                                <bi-v0:partyInteractionRoleKey>
                                                                <cbe-v0:type>PartyInteractionRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>ptyIntPICBillingRole</cbe-v0:validIDOnlyInMessage>
                                                </bi-v0:partyInteractionRoleKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>請求先担当者</cbe-v0:name>
                                <cbe-v0:nameLatin>Person in charge of billing</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <bi-v0:referenceToPartyInteractionRoleSpec>
                                                                <cbe-v0:type>PartyInteractionRoleSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>9aadb297-0b23-4fb9-9871-1f7a07105e35</cbe-v0:objectID>
                                                </bi-v0:referenceToPartyInteractionRoleSpec>
                                </cbe-v0:describedBy>
                                <party-v0:partyRoleKey>
                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                <cbe-v0:validIDOnlyInMessage>pICBillingRole</cbe-v0:validIDOnlyInMessage>
                                </party-v0:partyRoleKey>
                </bi-v0:partyInteractionRole>

                <bi-v0:partyInteractionRole>
                                <cbe-v0:identifiedBy>
                                                <bi-v0:partyInteractionRoleKey>
                                                                <cbe-v0:type>PartyInteractionRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>ptyIntSalesChannelRole</cbe-v0:validIDOnlyInMessage>
                                                </bi-v0:partyInteractionRoleKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>販売チャネル</cbe-v0:name>
                                <cbe-v0:nameLatin>Sales Channel</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <bi-v0:referenceToPartyInteractionRoleSpec>
                                                                <cbe-v0:type>PartyInteractionRoleSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>e01ac48d-0b23-4fb1-8935-f73d7c7f8738</cbe-v0:objectID>
                                                </bi-v0:referenceToPartyInteractionRoleSpec>
                                </cbe-v0:describedBy>
                                <party-v0:partyRoleKey>
                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                <cbe-v0:validIDOnlyInMessage>salesChannelRole</cbe-v0:validIDOnlyInMessage>
                                </party-v0:partyRoleKey>
                </bi-v0:partyInteractionRole>

                <bi-v0:partyInteractionRole>
                                <cbe-v0:identifiedBy>
                                                <bi-v0:partyInteractionRoleKey>
                                                                <cbe-v0:type>PartyInteractionRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>ptyIntPICSalesRole</cbe-v0:validIDOnlyInMessage>
                                                </bi-v0:partyInteractionRoleKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>販売担当者</cbe-v0:name>
                                <cbe-v0:nameLatin>Sales Representative</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <bi-v0:referenceToPartyInteractionRoleSpec>
                                                                <cbe-v0:type>PartyInteractionRoleSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>155b95c9-0b23-4fba-92d8-c4a1497867df</cbe-v0:objectID>
                                                </bi-v0:referenceToPartyInteractionRoleSpec>
                                </cbe-v0:describedBy>
                                <party-v0:partyRoleKey>
                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                <cbe-v0:validIDOnlyInMessage>pICSalesRole</cbe-v0:validIDOnlyInMessage>
                                </party-v0:partyRoleKey>
                </bi-v0:partyInteractionRole>

                <party-v0:party>
                                <cbe-v0:identifiedBy>
                                                <party-v0:partyKey>
                                                                <cbe-v0:type>Party</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>contractCustomer</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:partyKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:describedBy>
                                                <party-v0:referenceToPartySpec name="Organization">
                                                                <cbe-v0:type>PartySpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>42137cd1-0b23-4fb7-9342-474dd5ed0a07</cbe-v0:objectID>
                                                </party-v0:referenceToPartySpec>
                                </cbe-v0:describedBy>
                </party-v0:party>

                <party-v0:corporateIdentification>
                                <cbe-v0:identifiedBy>
                                                <party-v0:corporateIdentificationKey>
                                                                <cbe-v0:type>CorporateIdentification</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>customerIdentification</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:corporateIdentificationKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>契約者共通顧客ID</cbe-v0:name>
                                <cbe-v0:nameLatin>Corporation ID</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <party-v0:referenceToPartyIdentificationSpec>
                                                                <cbe-v0:type>PartyIdentificationSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>73a6fc18-0b23-4fb8-9861-1ab890408fa8</cbe-v0:objectID>
                                                </party-v0:referenceToPartyIdentificationSpec>
                                </cbe-v0:describedBy>
                                <ptyIdent001:corporateIdentification name="Corporate Identification">
                                                <ptyIdent001:corporationID>
                                                                <ptyIdent001:characteristicValue>
                                                                                <cbe-v0:identifiedBy>
                                                                                                <cbe-v0:charcValueKey>
                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                <cbe-v0:validIDOnlyInMessage>contractCustomerCID</cbe-v0:validIDOnlyInMessage>
                                                                                                </cbe-v0:charcValueKey>
                                                                                </cbe-v0:identifiedBy>
                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                <cbe-v0:objectID>9512b60a-0b23-4fbe-9655-fdf88b53673d</cbe-v0:objectID>
                                                                                </cbe-v0:chValDescByChSpec>
                                                                                <ptyIdent001:corporationID>12345678901234</ptyIdent001:corporationID>
                                                                </ptyIdent001:characteristicValue>
                                                </ptyIdent001:corporationID>
                                </ptyIdent001:corporateIdentification>
                                <cbe-v0:appliedTo>
                                                <party-v0:partyKey>
                                                                <cbe-v0:type>Party</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>contractCustomer</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:partyKey>
                                </cbe-v0:appliedTo>
                </party-v0:corporateIdentification>

                <party-v0:partyRole>
                                <cbe-v0:identifiedBy>
                                                <party-v0:partyRoleKey>
                                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>contractCustomerRole</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:partyRoleKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>契約先</cbe-v0:name>
                                <cbe-v0:nameLatin>Contract Customer</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <party-v0:referenceToPartyRoleSpec name="Contract Customer">
                                                                <cbe-v0:type>PartyRoleSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>be8658e3-0b23-4fb8-9c35-b7446a8af342</cbe-v0:objectID>
                                                </party-v0:referenceToPartyRoleSpec>
                                </cbe-v0:describedBy>
                                <cbe-v0:appliedTo>
                                                <party-v0:partyKey>
                                                                <cbe-v0:type>Party</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>contractCustomer</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:partyKey>
                                </cbe-v0:appliedTo>
                </party-v0:partyRole>

                <party-v0:partyRole>
                                <cbe-v0:identifiedBy>
                                                <party-v0:partyRoleKey>
                                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>pICContractRole</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:partyRoleKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>契約先担当者</cbe-v0:name>
                                <cbe-v0:nameLatin>Person in charge of contract</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <party-v0:referenceToPartyRoleSpec name="Person In Charge Of Contract Pseudo Role">
                                                                <cbe-v0:type>PartyRoleSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>7da2fb33-0b23-4fb5-ab43-8d5d3db7c965</cbe-v0:objectID>
                                                </party-v0:referenceToPartyRoleSpec>
                                </cbe-v0:describedBy>
                                <pICContractPR:pICContractPR name="Person In Charge Of Contract Pseudo Role">
                                                <pICContractPR:divisionName>
                                                                <pICContractPR:divisionName>
                                                                                <pICContractPR:characteristicValue>
                                                                                                <cbe-v0:identifiedBy>
                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                <cbe-v0:validIDOnlyInMessage>pICContractPR:divisionName</cbe-v0:validIDOnlyInMessage>
                                                                                                                </cbe-v0:charcValueKey>
                                                                                                </cbe-v0:identifiedBy>
                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                <cbe-v0:objectID>09838a91-0b23-4fb5-a770-310deaff0330</cbe-v0:objectID>
                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                <pICContractPR:divisionName>When the Next Cloud Portal calls GCM’s API to register contract information, GCM stores the data sent from the Next Cloud12345678901234567890</pICContractPR:divisionName>
                                                                                </pICContractPR:characteristicValue>
                                                                </pICContractPR:divisionName>
                                                </pICContractPR:divisionName>
                                                <pICContractPR:individualName>
                                                                <pICContractPR:individualName>
                                                                                <pICContractPR:characteristicValue>
                                                                                                <cbe-v0:identifiedBy>
                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                <cbe-v0:validIDOnlyInMessage>pICContractPR:individualName</cbe-v0:validIDOnlyInMessage>
                                                                                                                </cbe-v0:charcValueKey>
                                                                                                </cbe-v0:identifiedBy>
                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                <cbe-v0:objectID>99a26515-0b23-4fbd-b0fd-f821a3f912db</cbe-v0:objectID>
                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                <pICContractPR:individualName>When the Next Cloud Portal calls GCM’s API to register contract information, GCM stores the data sent from the Next </pICContractPR:individualName>
                                                                                </pICContractPR:characteristicValue>
                                                                </pICContractPR:individualName>
                                                </pICContractPR:individualName>
                                </pICContractPR:pICContractPR>
                                <party-v0:contactableVia>
                                                <party-v0:contactMediumKey>
                                                                <cbe-v0:type>ContactMedium</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>contactMedium01</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:contactMediumKey>
                                </party-v0:contactableVia>
                </party-v0:partyRole>

                <party-v0:partyRole>
                                <cbe-v0:identifiedBy>
                                                <party-v0:partyRoleKey>
                                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>billingCustomerRole</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:partyRoleKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>請求先</cbe-v0:name>
                                <cbe-v0:nameLatin>Billing Customer</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <party-v0:referenceToPartyRoleSpec name="Billing Customer Pseudo Role">
                                                                <cbe-v0:type>PartyRoleSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>f1d52414-0b23-4fbc-adab-30fd0bfda38e</cbe-v0:objectID>
                                                </party-v0:referenceToPartyRoleSpec>
                                </cbe-v0:describedBy>
                                <billingCustomerPR:billingCustomerPseudoRole name="Billing Customer Pseudo Role">
                                                <billingCustomerPR:corporationName>
                                                                <billingCustomerPR:characteristicValue>
                                                                                <cbe-v0:identifiedBy>
                                                                                                <cbe-v0:charcValueKey>
                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                <cbe-v0:validIDOnlyInMessage>billingCustomerPR:corporationName</cbe-v0:validIDOnlyInMessage>
                                                                                                </cbe-v0:charcValueKey>
                                                                                </cbe-v0:identifiedBy>
                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                <cbe-v0:objectID>42940831-0b23-4fba-bf9e-0eb01de498f1</cbe-v0:objectID>
                                                                                </cbe-v0:chValDescByChSpec>
                                                                                <billingCustomerPR:corporationName>When the Next Cloud Portal calls GCM’s API to register contract information, GCM stores the data sent from the Next </billingCustomerPR:corporationName>
                                                                </billingCustomerPR:characteristicValue>
                                                </billingCustomerPR:corporationName>
                                                <billingCustomerPR:corporationNameFurigana>
                                                                <billingCustomerPR:characteristicValue>
                                                                                <cbe-v0:identifiedBy>
                                                                                                <cbe-v0:charcValueKey>
                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                <cbe-v0:validIDOnlyInMessage>billingCustomerPR:corporationNameFurigana</cbe-v0:validIDOnlyInMessage>
                                                                                                </cbe-v0:charcValueKey>
                                                                                </cbe-v0:identifiedBy>
                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                <cbe-v0:objectID>5b19f1e7-0b23-4fb4-bf57-67b02517f029</cbe-v0:objectID>
                                                                                </cbe-v0:chValDescByChSpec>
                                                                                <billingCustomerPR:corporationNameFurigana>エヌティティコミュニケーションズ</billingCustomerPR:corporationNameFurigana>
                                                                </billingCustomerPR:characteristicValue>
                                                </billingCustomerPR:corporationNameFurigana>
                                </billingCustomerPR:billingCustomerPseudoRole>
                </party-v0:partyRole>

                <party-v0:partyRole>
                                <cbe-v0:identifiedBy>
                                                <party-v0:partyRoleKey>
                                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>pICBillingRole</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:partyRoleKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>請求先担当者</cbe-v0:name>
                                <cbe-v0:nameLatin>Person in charge of billing</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <party-v0:referenceToPartyRoleSpec name="Person In Charge Of Billing Pseudo Role">
                                                                <cbe-v0:type>PartyRoleSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>7a10a146-0b23-4fb7-a56b-92830fc8d6d1</cbe-v0:objectID>
                                                </party-v0:referenceToPartyRoleSpec>
                                </cbe-v0:describedBy>
                                <pICBillingPR:pICBillingPR name="Person In Charge Of Billing Pseudo Role">
                                                <pICBillingPR:divisionName>
                                                                <pICBillingPR:divisionName>
                                                                                <pICBillingPR:characteristicValue>
                                                                                                <cbe-v0:identifiedBy>
                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                <cbe-v0:validIDOnlyInMessage>pICBillingPR:divisionName</cbe-v0:validIDOnlyInMessage>
                                                                                                                </cbe-v0:charcValueKey>
                                                                                                </cbe-v0:identifiedBy>
                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                <cbe-v0:objectID>a011af96-0b23-4fb4-aa4c-6d49630595bd</cbe-v0:objectID>
                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                <pICBillingPR:divisionName>When the Next Cloud Portal calls GCM’s API to register contract information, GCM stores the data sent from the Next Cloud12345678901234567890</pICBillingPR:divisionName>
                                                                                </pICBillingPR:characteristicValue>
                                                                </pICBillingPR:divisionName>
                                                </pICBillingPR:divisionName>
                                                <pICBillingPR:individualName>
                                                                <pICBillingPR:individualName>
                                                                                <pICBillingPR:characteristicValue>
                                                                                                <cbe-v0:identifiedBy>
                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                <cbe-v0:validIDOnlyInMessage>pICBillingPR:individualName</cbe-v0:validIDOnlyInMessage>
                                                                                                                </cbe-v0:charcValueKey>
                                                                                                </cbe-v0:identifiedBy>
                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                <cbe-v0:objectID>e45be530-0b23-4fb6-886f-b4d6fc186596</cbe-v0:objectID>
                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                <pICBillingPR:individualName>大角知孝</pICBillingPR:individualName>
                                                                                </pICBillingPR:characteristicValue>
                                                                </pICBillingPR:individualName>
                                                </pICBillingPR:individualName>
                                </pICBillingPR:pICBillingPR>
                                <party-v0:contactableVia>
                                                <party-v0:contactMediumKey>
                                                                <cbe-v0:type>ContactMedium</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>contactMedium02</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:contactMediumKey>
                                </party-v0:contactableVia>
                </party-v0:partyRole>

                <party-v0:partyRole>
                                <cbe-v0:identifiedBy>
                                                <party-v0:partyRoleKey>
                                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>salesChannelRole</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:partyRoleKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>販売チャネル</cbe-v0:name>
                                <cbe-v0:nameLatin>Sales Channel</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <party-v0:referenceToPartyRoleSpec name="Sales Channel Pseudo Role">
                                                                <cbe-v0:type>PartyRoleSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>1d56df26-0b23-4fb4-9514-42ca68dd6586</cbe-v0:objectID>
                                                </party-v0:referenceToPartyRoleSpec>
                                </cbe-v0:describedBy>
                                <salesChannelPR:salesChannelPseudoRole name="Sales Channel Pseudo Role">
                                                <salesChannelPR:salesChannelCode>
                                                                <salesChannelPR:characteristicValue>
                                                                                <cbe-v0:identifiedBy>
                                                                                                <cbe-v0:charcValueKey>
                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                <cbe-v0:validIDOnlyInMessage>salesChannelPR:salesChannelCode</cbe-v0:validIDOnlyInMessage>
                                                                                                </cbe-v0:charcValueKey>
                                                                                </cbe-v0:identifiedBy>
                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                <cbe-v0:objectID>00d1f3d3-0b23-4fbd-bbd7-753f5a6c5c6a</cbe-v0:objectID>
                                                                                </cbe-v0:chValDescByChSpec>
                                                                                <salesChannelPR:salesChannelCode>When the Next Cloud Portal calls GCM’s API to register contract information, GCM stores the data sent from the Next Cloud</salesChannelPR:salesChannelCode>
                                                                </salesChannelPR:characteristicValue>
                                                </salesChannelPR:salesChannelCode>
                                </salesChannelPR:salesChannelPseudoRole>
                </party-v0:partyRole>

                <party-v0:partyRole>
                                <cbe-v0:identifiedBy>
                                                <party-v0:partyRoleKey>
                                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>pICSalesRole</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:partyRoleKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>販売担当者</cbe-v0:name>
                                <cbe-v0:nameLatin>Person in charge of sales</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <party-v0:referenceToPartyRoleSpec name="Person In Charge Of Sales Pseudo Role">
                                                                <cbe-v0:type>PartyRoleSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>269648a0-0b23-4fb7-9c91-368937fe81ac</cbe-v0:objectID>
                                                </party-v0:referenceToPartyRoleSpec>
                                </cbe-v0:describedBy>
                                <pICSalesPR:pICSalesPR name="Person In Charge Of Sales Representative Pseudo Role">
                                                <pICSalesPR:divisionName>
                                                                <pICSalesPR:divisionName>
                                                                                <pICSalesPR:characteristicValue>
                                                                                                <cbe-v0:identifiedBy>
                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                <cbe-v0:validIDOnlyInMessage>pICSalesPR:divisionName</cbe-v0:validIDOnlyInMessage>
                                                                                                                </cbe-v0:charcValueKey>
                                                                                                </cbe-v0:identifiedBy>
                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                <cbe-v0:objectID>0b872b46-0b23-4fb4-9082-7b10ee9aa5ea</cbe-v0:objectID>
                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                <pICSalesPR:divisionName>When the Next Cloud Portal calls GCM’s API to register contract information, GCM stores the data sent from the Next Cloud </pICSalesPR:divisionName>
                                                                                </pICSalesPR:characteristicValue>
                                                                </pICSalesPR:divisionName>
                                                </pICSalesPR:divisionName>
                                                <pICSalesPR:individualName>
                                                                <pICSalesPR:individualName>
                                                                                <pICSalesPR:characteristicValue>
                                                                                                <cbe-v0:identifiedBy>
                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                <cbe-v0:validIDOnlyInMessage>pICSalesPR:individualName</cbe-v0:validIDOnlyInMessage>
                                                                                                                </cbe-v0:charcValueKey>
                                                                                                </cbe-v0:identifiedBy>
                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                <cbe-v0:objectID>4d059424-0b23-4fb2-a45f-85fbe7950449</cbe-v0:objectID>
                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                <pICSalesPR:individualName>When the Next Cloud Portal calls GCM’s API to register contract information, GCM stores the data sent</pICSalesPR:individualName>
                                                                                </pICSalesPR:characteristicValue>
                                                                </pICSalesPR:individualName>
                                                </pICSalesPR:individualName>
                                </pICSalesPR:pICSalesPR>
                                <party-v0:contactableVia>
                                                <party-v0:contactMediumKey>
                                                                <cbe-v0:type>ContactMedium</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>contactMedium03</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:contactMediumKey>
                                </party-v0:contactableVia>
                </party-v0:partyRole>

                <party-v0:contactMedium>
                                <cbe-v0:identifiedBy>
                                                <party-v0:contactMediumKey>
                                                                <cbe-v0:type>ContactMedium</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>contactMedium01</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:contactMediumKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>契約先担当者連絡先</cbe-v0:name>
                                <cbe-v0:nameLatin>contact medium for person in charge of contract</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <party-v0:referenceToContactMediumSpec name="ContactMedium">
                                                                <cbe-v0:type>ContactMediumSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>a7fd0ea0-0b23-4fb0-8884-d6eb0f5d8dea</cbe-v0:objectID>
                                                </party-v0:referenceToContactMediumSpec>
                                </cbe-v0:describedBy>
                                <contactMedium:contactMedium name="standardContactMedium">
                                                <contactMedium:telephoneNumber>
                                                                <contactMedium:characteristicValue>
                                                                                <cbe-v0:identifiedBy>
                                                                                                <cbe-v0:charcValueKey>
                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                <cbe-v0:validIDOnlyInMessage>stdCntctMdm:telephoneNumber-01</cbe-v0:validIDOnlyInMessage>
                                                                                                </cbe-v0:charcValueKey>
                                                                                </cbe-v0:identifiedBy>
                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                <cbe-v0:objectID>07711ed8-0b23-4fbe-b78e-917d269f6890</cbe-v0:objectID>
                                                                                </cbe-v0:chValDescByChSpec>
                                                                                <contactMedium:telephoneNumber>1234567890123456789000</contactMedium:telephoneNumber>
                                                                </contactMedium:characteristicValue>
                                                </contactMedium:telephoneNumber>
                                                <contactMedium:eMailAddress>
                                                                <contactMedium:characteristicValue>
                                                                                <cbe-v0:identifiedBy>
                                                                                                <cbe-v0:charcValueKey>
                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                <cbe-v0:validIDOnlyInMessage>stdCntctMdm:eMailAddress-01</cbe-v0:validIDOnlyInMessage>
                                                                                                </cbe-v0:charcValueKey>
                                                                                </cbe-v0:identifiedBy>
                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                <cbe-v0:objectID>131c0683-0b23-4fb6-8ce1-7589daeb5e2e</cbe-v0:objectID>
                                                                                </cbe-v0:chValDescByChSpec>
                                                                                <contactMedium:eMailAddress>When the Next Cloud Portal calls GCM’s API to register contract information, GCM stores the data sent from the Next Cloud1234567890123456789000@gmail.com</contactMedium:eMailAddress>
                                                                </contactMedium:characteristicValue>
                                                </contactMedium:eMailAddress>
                                </contactMedium:contactMedium>
                </party-v0:contactMedium>

                <party-v0:contactMedium>
                                <cbe-v0:identifiedBy>
                                                <party-v0:contactMediumKey>
                                                                <cbe-v0:type>ContactMedium</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>contactMedium02</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:contactMediumKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>請求先担当者連絡先</cbe-v0:name>
                                <cbe-v0:nameLatin>contact medium for person in charge of billing</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <party-v0:referenceToContactMediumSpec name="ContactMedium">
                                                                <cbe-v0:type>ContactMediumSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>a7fd0ea0-0b23-4fb0-8884-d6eb0f5d8dea</cbe-v0:objectID>
                                                </party-v0:referenceToContactMediumSpec>
                                </cbe-v0:describedBy>
                                <contactMedium:contactMedium name="standardContactMedium">
                                                <contactMedium:telephoneNumber>
                                                                <contactMedium:characteristicValue>
                                                                                <cbe-v0:identifiedBy>
                                                                                                <cbe-v0:charcValueKey>
                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                <cbe-v0:validIDOnlyInMessage>stdCntctMdm:telephoneNumber-02</cbe-v0:validIDOnlyInMessage>
                                                                                                </cbe-v0:charcValueKey>
                                                                                </cbe-v0:identifiedBy>
                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                <cbe-v0:objectID>07711ed8-0b23-4fbe-b78e-917d269f6890</cbe-v0:objectID>
                                                                                </cbe-v0:chValDescByChSpec>
                                                                                <contactMedium:telephoneNumber>1234567890123456789000</contactMedium:telephoneNumber>
                                                                </contactMedium:characteristicValue>
                                                </contactMedium:telephoneNumber>
                                                <contactMedium:eMailAddress>
                                                                <contactMedium:characteristicValue>
                                                                                <cbe-v0:identifiedBy>
                                                                                                <cbe-v0:charcValueKey>
                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                <cbe-v0:validIDOnlyInMessage>stdCntctMdm:eMailAddress-02</cbe-v0:validIDOnlyInMessage>
                                                                                                </cbe-v0:charcValueKey>
                                                                                </cbe-v0:identifiedBy>
                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                <cbe-v0:objectID>131c0683-0b23-4fb6-8ce1-7589daeb5e2e</cbe-v0:objectID>
                                                                                </cbe-v0:chValDescByChSpec>
                                                                                <contactMedium:eMailAddress>When the Next Cloud Portal calls GCM’s API to register contract information, GCM stores the data sent from the Next Cloud1234567890123456789000@gmail.com</contactMedium:eMailAddress>
                                                                </contactMedium:characteristicValue>
                                                </contactMedium:eMailAddress>
                                </contactMedium:contactMedium>
                </party-v0:contactMedium>

                <party-v0:contactMedium>
                                <cbe-v0:identifiedBy>
                                                <party-v0:contactMediumKey>
                                                                <cbe-v0:type>ContactMedium</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>contactMedium03</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:contactMediumKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>販売担当者連絡先</cbe-v0:name>
                                <cbe-v0:nameLatin>contact medium for person in charge of sales</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <party-v0:referenceToContactMediumSpec name="ContactMedium">
                                                                <cbe-v0:type>ContactMediumSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>a7fd0ea0-0b23-4fb0-8884-d6eb0f5d8dea</cbe-v0:objectID>
                                                </party-v0:referenceToContactMediumSpec>
                                </cbe-v0:describedBy>
                                <contactMedium:contactMedium name="standardContactMedium">
                                                <contactMedium:telephoneNumber>
                                                                <contactMedium:characteristicValue>
                                                                                <cbe-v0:identifiedBy>
                                                                                                <cbe-v0:charcValueKey>
                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                <cbe-v0:validIDOnlyInMessage>stdCntctMdm:telephoneNumber-03</cbe-v0:validIDOnlyInMessage>
                                                                                                </cbe-v0:charcValueKey>
                                                                                </cbe-v0:identifiedBy>
                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                <cbe-v0:objectID>07711ed8-0b23-4fbe-b78e-917d269f6890</cbe-v0:objectID>
                                                                                </cbe-v0:chValDescByChSpec>
                                                                                <contactMedium:telephoneNumber>123456789012345678901</contactMedium:telephoneNumber>
                                                                </contactMedium:characteristicValue>
                                                </contactMedium:telephoneNumber>
                                                <contactMedium:eMailAddress>
                                                                <contactMedium:characteristicValue>
                                                                                <cbe-v0:identifiedBy>
                                                                                                <cbe-v0:charcValueKey>
                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                <cbe-v0:validIDOnlyInMessage>stdCntctMdm:eMailAddress-03</cbe-v0:validIDOnlyInMessage>
                                                                                                </cbe-v0:charcValueKey>
                                                                                </cbe-v0:identifiedBy>
                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                <cbe-v0:objectID>131c0683-0b23-4fb6-8ce1-7589daeb5e2e</cbe-v0:objectID>
                                                                                </cbe-v0:chValDescByChSpec>
                                                                                <contactMedium:eMailAddress>When the Next Cloud Portal calls GCM’s API to register contract information, GCM stores the data sent from the Next Cloud12345678901234567890@gmail.com</contactMedium:eMailAddress>
                                                                </contactMedium:characteristicValue>
                                                </contactMedium:eMailAddress>
                                </contactMedium:contactMedium>
                </party-v0:contactMedium>

                <location-v0:geographicAddress>
                                <cbe-v0:identifiedBy>
                                                <location-v0:geoAddressKey>
                                                                <cbe-v0:type>GeographicAddress</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>geoAddr00</cbe-v0:validIDOnlyInMessage>
                                                </location-v0:geoAddressKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:describedBy>
                                                <location-v0:referenceToGeoAddressSpec>
                                                                <cbe-v0:type>GeoAddressSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>8058f09f-0b23-4fb1-825d-8c234addcddf</cbe-v0:objectID>
                                                </location-v0:referenceToGeoAddressSpec>
                                </cbe-v0:describedBy>
                                <location-v0:countryKey name="Japan">
                                                <cbe-v0:type>Country</cbe-v0:type>
                                                <location-v0:iso3166_1numeric>0000</location-v0:iso3166_1numeric>
                                </location-v0:countryKey>

                                <location-v0:usedAddressRepresentation>
                                                <location-v0:countrySpecificAddress>
                                                                <cbe-v0:identifiedBy>
                                                                                <location-v0:countrySpecificAddressKey>
                                                                                                <cbe-v0:type>CountrySpecificAddress</cbe-v0:type>
                                                                                                <cbe-v0:validIDOnlyInMessage>jpnAddrJpn00</cbe-v0:validIDOnlyInMessage>
                                                                                </location-v0:countrySpecificAddressKey>
                                                                </cbe-v0:identifiedBy>
                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                <cbe-v0:describedBy>
                                                                                <location-v0:referenceToCountrySpecificAddressSpec name="japanesePropertyAddress">
                                                                                                <cbe-v0:type>CountrySpecificAddressSpecification</cbe-v0:type>
                                                                                                <cbe-v0:objectID>cab7b06b-0b23-4fba-9431-e627cbb3fc27</cbe-v0:objectID>
                                                                                </location-v0:referenceToCountrySpecificAddressSpec>
                                                                </cbe-v0:describedBy>

                                                                <jpnAddr:japanesePropertyAddress name="Japanese Property Address">
                                                                                <jpnAddr:postalCode>
                                                                                                <jpnAddr:characteristicValue>
                                                                                                                <cbe-v0:identifiedBy>
                                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                                <cbe-v0:validIDOnlyInMessage>jpnAddr:postalCode-00</cbe-v0:validIDOnlyInMessage>
                                                                                                                                </cbe-v0:charcValueKey>
                                                                                                                </cbe-v0:identifiedBy>
                                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                                <cbe-v0:objectID>48cd78c1-0b23-4fb8-918e-64ea2c129366</cbe-v0:objectID>
                                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                                <jpnAddr:postalCode>1234567890</jpnAddr:postalCode>
                                                                                                </jpnAddr:characteristicValue>
                                                                                </jpnAddr:postalCode>
                                                                                <jpnAddr:address>
                                                                                                <jpnAddr:characteristicValue>
                                                                                                                <cbe-v0:identifiedBy>
                                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                                <cbe-v0:validIDOnlyInMessage>jpnAddr:address-00</cbe-v0:validIDOnlyInMessage>
                                                                                                                                </cbe-v0:charcValueKey>
                                                                                                                </cbe-v0:identifiedBy>
                                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                                <cbe-v0:objectID>ad5aaa70-0b23-4fb9-98ee-dfb35369a2fb</cbe-v0:objectID>
                                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                                <jpnAddr:address>東京都千代田区内幸町１丁目１－６</jpnAddr:address>
                                                                                                </jpnAddr:characteristicValue>
                                                                                </jpnAddr:address>
                                                                                <jpnAddr:addressLine>
                                                                                                <jpnAddr:characteristicValue>
                                                                                                                <cbe-v0:identifiedBy>
                                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                                <cbe-v0:validIDOnlyInMessage>jpnAddr:addressLine-00</cbe-v0:validIDOnlyInMessage>
                                                                                                                                </cbe-v0:charcValueKey>
                                                                                                                </cbe-v0:identifiedBy>
                                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                                <cbe-v0:objectID>d9870b88-0b23-4fbe-9806-2f619b0bad8d</cbe-v0:objectID>
                                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                                <jpnAddr:addressLine>NTT日比谷ビル３F</jpnAddr:addressLine>
                                                                                                </jpnAddr:characteristicValue>
                                                                                </jpnAddr:addressLine>
                                                                </jpnAddr:japanesePropertyAddress>

                                                                <location-v0:subordinateTo>
                                                                                <location-v0:geoAddressKey>
                                                                                                <cbe-v0:type>GeographicAddress</cbe-v0:type>
                                                                                                <cbe-v0:validIDOnlyInMessage>geoAddr00</cbe-v0:validIDOnlyInMessage>
                                                                                </location-v0:geoAddressKey>
                                                                </location-v0:subordinateTo>
                                                                <location-v0:languageNotation>
                                                                                <datatypes-v0:languageName>jpn</datatypes-v0:languageName>
                                                                                <datatypes-v0:notation>Japanese</datatypes-v0:notation>
                                                                </location-v0:languageNotation>
                                                                <location-v0:isDefault>true</location-v0:isDefault>
                                                </location-v0:countrySpecificAddress>

                                </location-v0:usedAddressRepresentation>
                </location-v0:geographicAddress>

                <location-v0:geographicAddress>
                                <cbe-v0:identifiedBy>
                                                <location-v0:geoAddressKey>
                                                                <cbe-v0:type>GeographicAddress</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>geoAddr01</cbe-v0:validIDOnlyInMessage>
                                                </location-v0:geoAddressKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:describedBy>
                                                <location-v0:referenceToGeoAddressSpec>
                                                                <cbe-v0:type>GeoAddressSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>8058f09f-0b23-4fb1-825d-8c234addcddf</cbe-v0:objectID>
                                                </location-v0:referenceToGeoAddressSpec>
                                </cbe-v0:describedBy>
                                <location-v0:countryKey name="Japan">
                                                <cbe-v0:type>Country</cbe-v0:type>
                                                <location-v0:iso3166_1numeric>11111</location-v0:iso3166_1numeric>
                                </location-v0:countryKey>

                                <location-v0:usedAddressRepresentation>
                                                <location-v0:countrySpecificAddress>
                                                                <cbe-v0:identifiedBy>
                                                                                <location-v0:countrySpecificAddressKey>
                                                                                                <cbe-v0:type>CountrySpecificAddress</cbe-v0:type>
                                                                                                <cbe-v0:validIDOnlyInMessage>jpnAddrJpn01</cbe-v0:validIDOnlyInMessage>
                                                                                </location-v0:countrySpecificAddressKey>
                                                                </cbe-v0:identifiedBy>
                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                <cbe-v0:describedBy>
                                                                                <location-v0:referenceToCountrySpecificAddressSpec name="japanesePropertyAddress">
                                                                                                <cbe-v0:type>CountrySpecificAddressSpecification</cbe-v0:type>
                                                                                                <cbe-v0:objectID>cab7b06b-0b23-4fba-9431-e627cbb3fc27</cbe-v0:objectID>
                                                                                </location-v0:referenceToCountrySpecificAddressSpec>
                                                                </cbe-v0:describedBy>

                                                                <jpnAddr:japanesePropertyAddress name="Japanese Property Address">
                                                                                <jpnAddr:postalCode>
                                                                                                <jpnAddr:characteristicValue>
                                                                                                                <cbe-v0:identifiedBy>
                                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                                <cbe-v0:validIDOnlyInMessage>jpnAddr:postalCode-01</cbe-v0:validIDOnlyInMessage>
                                                                                                                                </cbe-v0:charcValueKey>
                                                                                                                </cbe-v0:identifiedBy>
                                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                                <cbe-v0:objectID>48cd78c1-0b23-4fb8-918e-64ea2c129366</cbe-v0:objectID>
                                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                                <jpnAddr:postalCode>1234567890</jpnAddr:postalCode>
                                                                                                </jpnAddr:characteristicValue>
                                                                                </jpnAddr:postalCode>
                                                                                <jpnAddr:address>
                                                                                                <jpnAddr:characteristicValue>
                                                                                                                <cbe-v0:identifiedBy>
                                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                                <cbe-v0:validIDOnlyInMessage>jpnAddr:address-01</cbe-v0:validIDOnlyInMessage>
                                                                                                                                </cbe-v0:charcValueKey>
                                                                                                                </cbe-v0:identifiedBy>
                                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                                <cbe-v0:objectID>ad5aaa70-0b23-4fb9-98ee-dfb35369a2fb</cbe-v0:objectID>
                                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                                <jpnAddr:address>東京都千代田区内幸町１丁目</jpnAddr:address>
                                                                                                </jpnAddr:characteristicValue>
                                                                                </jpnAddr:address>
                                                                                <jpnAddr:addressLine>
                                                                                                <jpnAddr:characteristicValue>
                                                                                                                <cbe-v0:identifiedBy>
                                                                                                                                <cbe-v0:charcValueKey>
                                                                                                                                                <cbe-v0:type>CharacteristicValue</cbe-v0:type>
                                                                                                                                                <cbe-v0:validIDOnlyInMessage>jpnAddr:addressLine-01</cbe-v0:validIDOnlyInMessage>
                                                                                                                                </cbe-v0:charcValueKey>
                                                                                                                </cbe-v0:identifiedBy>
                                                                                                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                                                                                                <cbe-v0:chValDescByChSpec>
                                                                                                                                <cbe-v0:type>AtomicCharacteristicSpecification</cbe-v0:type>
                                                                                                                                <cbe-v0:objectID>d9870b88-0b23-4fbe-9806-2f619b0bad8d</cbe-v0:objectID>
                                                                                                                </cbe-v0:chValDescByChSpec>
                                                                                                                <jpnAddr:addressLine>１－６ NTT日比谷ビル３F</jpnAddr:addressLine>
                                                                                                </jpnAddr:characteristicValue>
                                                                                </jpnAddr:addressLine>
                                                                </jpnAddr:japanesePropertyAddress>

                                                                <location-v0:subordinateTo>
                                                                                <location-v0:geoAddressKey>
                                                                                                <cbe-v0:type>GeographicAddress</cbe-v0:type>
                                                                                                <cbe-v0:validIDOnlyInMessage>geoAddr01</cbe-v0:validIDOnlyInMessage>
                                                                                </location-v0:geoAddressKey>
                                                                </location-v0:subordinateTo>
                                                                <location-v0:languageNotation>
                                                                                <datatypes-v0:languageName>jpn</datatypes-v0:languageName>
                                                                                <datatypes-v0:notation>Japanese</datatypes-v0:notation>
                                                                </location-v0:languageNotation>
                                                                <location-v0:isDefault>true</location-v0:isDefault>
                                                </location-v0:countrySpecificAddress>

                                </location-v0:usedAddressRepresentation>
                </location-v0:geographicAddress>

                <location-v0:placePartyRoleAssociation>
                                <cbe-v0:identifiedBy>
                                                <location-v0:placePartyRoleAssocKey>
                                                                <cbe-v0:type>PlacePartyRoleAssociation</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>geoAddrPtyRl00</cbe-v0:validIDOnlyInMessage>
                                                </location-v0:placePartyRoleAssocKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>契約先住所(顧客記入)</cbe-v0:name>
                                <cbe-v0:nameLatin>Contract Customer Address</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <location-v0:referenceToPlacePartyRoleAssocSpec>
                                                                <cbe-v0:type>PlacePartyRoleAssocSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>0e45f23e-0b23-4fb2-a8a2-c241025bb61e</cbe-v0:objectID>
                                                </location-v0:referenceToPlacePartyRoleAssocSpec>
                                </cbe-v0:describedBy>
                                <cbe-v0:associationEnds>
                                                <location-v0:geoAddressKey>
                                                                <cbe-v0:type>GeographicAddress</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>geoAddr00</cbe-v0:validIDOnlyInMessage>
                                                </location-v0:geoAddressKey>
                                </cbe-v0:associationEnds>
                                <cbe-v0:associationEnds>
                                                <party-v0:partyRoleKey>
                                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>contractCustomerRole</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:partyRoleKey>
                                </cbe-v0:associationEnds>
                </location-v0:placePartyRoleAssociation>

                <location-v0:placePartyRoleAssociation>
                                <cbe-v0:identifiedBy>
                                                <location-v0:placePartyRoleAssocKey>
                                                                <cbe-v0:type>PlacePartyRoleAssociation</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>geoAddrPtyRl01</cbe-v0:validIDOnlyInMessage>
                                                </location-v0:placePartyRoleAssocKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>請求先住所</cbe-v0:name>
                                <cbe-v0:nameLatin>Billing Address</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <location-v0:referenceToPlacePartyRoleAssocSpec>
                                                                <cbe-v0:type>PlacePartyRoleAssocSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>0e45f23e-0b23-4fb2-a8a2-c241025bb61e</cbe-v0:objectID>
                                                </location-v0:referenceToPlacePartyRoleAssocSpec>
                                </cbe-v0:describedBy>
                                <cbe-v0:associationEnds>
                                                <location-v0:geoAddressKey>
                                                                <cbe-v0:type>GeographicAddress</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>geoAddr01</cbe-v0:validIDOnlyInMessage>
                                                </location-v0:geoAddressKey>
                                </cbe-v0:associationEnds>
                                <cbe-v0:associationEnds>
                                                <party-v0:partyRoleKey>
                                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>billingCustomerRole</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:partyRoleKey>
                                </cbe-v0:associationEnds>
                </location-v0:placePartyRoleAssociation>

                <customer-v0:paymentMethod>
                                <cbe-v0:identifiedBy>
                                                <customer-v0:paymentMethodKey>
                                                                <cbe-v0:type>PaymentMethod</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>paymentMethod01</cbe-v0:validIDOnlyInMessage>
                                                </customer-v0:paymentMethodKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>一般:口座直接入金情報 (請求書払い)</cbe-v0:name>
                                <cbe-v0:nameLatin>General: Direct Deposit</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <customer-v0:referenceToPaymentMethodSpec name="jpnDirectDepositPM">
                                                                <cbe-v0:type>PaymentMethodSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>c8072bb6-0b23-4fb2-9ffe-c6ac534ed139</cbe-v0:objectID>
                                                </customer-v0:referenceToPaymentMethodSpec>
                                </cbe-v0:describedBy>
                </customer-v0:paymentMethod>

                <customer-v0:pMPartyRoleAssociation>
                                <cbe-v0:identifiedBy>
                                                <customer-v0:pMPartyRoleAssocKey>
                                                                <cbe-v0:type>PMPartyRoleAssociation</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>pMPtyRl01</cbe-v0:validIDOnlyInMessage>
                                                </customer-v0:pMPartyRoleAssocKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>請求先支払方法</cbe-v0:name>
                                <cbe-v0:nameLatin>Payment method of billing</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <customer-v0:referenceToPMPartyRoleAssocSpec>
                                                                <cbe-v0:type>PMPartyRoleAssocSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>a2b1ca44-0b23-4fb0-858b-b53f01c143c0</cbe-v0:objectID>
                                                </customer-v0:referenceToPMPartyRoleAssocSpec>
                                </cbe-v0:describedBy>
                                <cbe-v0:associationEnds>
                                                <customer-v0:paymentMethodKey>
                                                                <cbe-v0:type>PaymentMethod</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>paymentMethod01</cbe-v0:validIDOnlyInMessage>
                                                </customer-v0:paymentMethodKey>
                                </cbe-v0:associationEnds>
                                <cbe-v0:associationEnds>
                                                <party-v0:partyRoleKey>
                                                                <cbe-v0:type>PartyRole</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>billingCustomerRole</cbe-v0:validIDOnlyInMessage>
                                                </party-v0:partyRoleKey>
                                </cbe-v0:associationEnds>
                </customer-v0:pMPartyRoleAssociation>

                <customer-v0:pMProductAssociation>
                                <cbe-v0:identifiedBy>
                                                <customer-v0:pMProductAssocKey>
                                                                <cbe-v0:type>PMProductAssociation</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>pMProd01</cbe-v0:validIDOnlyInMessage>
                                                </customer-v0:pMProductAssocKey>
                                </cbe-v0:identifiedBy>
                                <cbe-v0:createdUser>coe\B1234567</cbe-v0:createdUser>
                                <cbe-v0:name>支払方法適用契約商品</cbe-v0:name>
                                <cbe-v0:nameLatin>Payment method of product</cbe-v0:nameLatin>
                                <cbe-v0:describedBy>
                                                <customer-v0:referenceToPMProductAssocSpec>
                                                                <cbe-v0:type>PMProductAssocSpecification</cbe-v0:type>
                                                                <cbe-v0:objectID>3e8f5641-0b23-4fb1-9496-541c05d22f27</cbe-v0:objectID>
                                                </customer-v0:referenceToPMProductAssocSpec>
                                </cbe-v0:describedBy>
                                <cbe-v0:associationEnds>
                                                <customer-v0:paymentMethodKey>
                                                                <cbe-v0:type>PaymentMethod</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>paymentMethod01</cbe-v0:validIDOnlyInMessage>
                                                </customer-v0:paymentMethodKey>
                                </cbe-v0:associationEnds>
                                <cbe-v0:associationEnds>
                                                <product-v0:bundledProductKey>
                                                                <cbe-v0:type>BundledProduct</cbe-v0:type>
                                                                <cbe-v0:validIDOnlyInMessage>bundledProduct01</cbe-v0:validIDOnlyInMessage>
                                                </product-v0:bundledProductKey>
                                </cbe-v0:associationEnds>
                </customer-v0:pMProductAssociation>

</req-v0:request>
EOD;

$curlPostData = array(
                                '1o7qigafvrinse487i0roorls5',
                                $xmlData
);

$parameters = json_encode($curlPostData);

$post = array(
    "method" => "fnContractRegistration",
    "input_type" => "JSON",
    "response_type" => "JSON",
    "rest_data" => $parameters
);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
$curlResponse = curl_exec($curl);
curl_close($curl);
var_dump(json_decode($curlResponse));
file_put_contents("Response_".time().".xml",json_decode($curlResponse));

?>