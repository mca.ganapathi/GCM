<?php
$context = stream_context_create(
  array(
    "https" => array(
      "proxy" => "tcp://dev-gcm-proxy:8888",
      "request_fulluri" => true
    ),
    "http" => array(
      "proxy" => "tcp://dev-gcm-proxy:8888",
      "request_fulluri" => true
    )
  )
);

$url = 'https://dev-gcm-stub/cid_search/corp_cust?user=GCMTEST&passCode=cidasapp&limitCount=100&charCode=UTF8&commonCustomerId=C0250124869';
//$url = 'http://dev-gcm-stub/cid_search/corp_cust?user=GCMTEST&passCode=cidasapp&limitCount=100&charCode=UTF8';

$responseBody = file_get_contents($url, false, $context);
echo $responseBody;