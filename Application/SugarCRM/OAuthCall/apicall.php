<?php

require_once('config.inc.php');

$apiResponseData = '';

$requestMethod = trim($_REQUEST['requestMethod']);
$sessionId = trim($_REQUEST['sessionId']);
$contractXMLData = trim($_REQUEST['contractXMLData']);


if($requestMethod == 'fnContractRegistration') {

	/* ------------------------------ Begin: XML Contract Creation ------------------------------ */
		
	$arrAPIResponse = array();
	$arrAPIParameters['sessionId'] = $sessionId;
	$arrAPIParameters['contractXMLData'] = $contractXMLData;

	$apiResponseData = call("fnContractRegistration", $arrAPIParameters, SUGARURLCUSTOMREST);
	
	/* ------------------------------ End: XML Contract Creation ------------------------------ */

} else {

	$apiResponseData = '<?xml version="1.0" encoding="UTF-8"?><?xml version="1.0" encoding="UTF-8"?><exp-v0:exception	xsi:schemaLocation="http://localhost:8080/XMLSchemaSample1 file:/C:/takito/Test/GCM/Operation/CommonException-v0_1.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:exp-v0="http://localhost:8080/XMLSchemaSample1/common-exception" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><exp-v0:exceptionDetails><exp-v0:item><exp-v0:messageCode>100001<exp-v0:message>Invalid Request</exp-v0:message></exp-v0:item></exp-v0:exceptionDetails></exp-v0:exception>';	
}

unset($arrAPIParameters);
echo '<pre>';
print_r($apiResponseData);
echo '</pre>';
die;


//function to make cURL request
function call($method, $parameters, $url) {
	ob_start();
	$curl_request = curl_init();

	curl_setopt($curl_request, CURLOPT_URL, $url);
	curl_setopt($curl_request, CURLOPT_POST, 1);
	curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
	curl_setopt($curl_request, CURLOPT_HEADER, 1);
	curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

	$jsonEncodedData = json_encode($parameters);

	$post = array(
		 "method" => $method,
		 "input_type" => "JSON",
		 "response_type" => "JSON",
		 "rest_data" => $jsonEncodedData
	);

	curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
	$result = curl_exec($curl_request);
	curl_close($curl_request);

	$result = explode("\r\n\r\n", $result, 2);
	$response = json_decode($result[1]);
	ob_end_flush();

	return $response;
}

?>