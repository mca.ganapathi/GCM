<?php

//get server timezone difference in hours with respect to UTC
$time = new DateTime('now', new DateTimeZone(date_default_timezone_get()));
$timezoneOffset = $time->format('P');
$timezoneOffset = ($timezoneOffset == 0 ? '00:00' : $timezoneOffset);

echo 'Application Time Zone ='.$timezoneOffset;

?>