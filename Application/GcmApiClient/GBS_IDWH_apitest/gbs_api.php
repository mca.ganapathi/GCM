<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('max_execution_time', 0);
require("config.php");
// Has the Product Offering XML File path and API URL's
require_once("utility.php");
// Has the Common(Utility) Functions
require_once("../../config_override.php");
// Has the DB Configuration
echo "Execution Started : ".date('Ymd_His')."<br>";
$outputString = "";
$finalString = "";
// String variables that will hold the output string which will be written to the log file
$sessionID = "";
$csvArray             = array();
$mismatchArray        = array();
// Mismatch array will hold the contract ID, its order type, the mismatched field as the key and value in both API and DB as value.
$matchArray           = array();
$failedSummary = array();
$productOfferingArray = array(
    '2808',
    '2809'
);
$notinArray = array(
    'DMY'
);
$i                    = 0;
$date_after           = "";
$date_before          = "";
$counter              = 1;
$finalString = "Execution Started :" . date('Ymd_His')."\n\nFailed Scenarios: \n";
try{
    $logFile = fopen("/var/log/sugarcrm/reg_gbs_api_".date('Ymd_His')."_".$sugar_config['dbconfig']['db_host_name'].".log","w");
}
catch(Exception $ex){
    die("Could not create Logfile.");
}
$csvfileName             = "parameters_gbs.csv";
// parameters_gbs.csv holds the paramters
$connectionInstance      = connectToDatabase($sugar_config['dbconfig']['db_host_name'],
                                             $sugar_config['dbconfig']['db_name'], 
                                             $sugar_config['dbconfig']['db_user_name'], 
                                             $sugar_config['dbconfig']['db_password']);
// $connectionInstance will have the instance of PDO returned from the connectToDatabase Function
$session = curl_init();
curl_setopt($session, CURLOPT_URL, $sessionURL);
curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($session, CURLOPT_RETURNTRANSFER, 1);
$sessionID = curl_exec($session);
if (trim($sessionID) == "") {
    fwrite($logFile, "Session ID is not Generated");
    echo "<br> Execution Completed";
	fclose($logFile);
    exit;
}
// Execution will stop if session ID is not generated
curl_close($session);

if(file_exists ($csvfileName)){
    $csvfile = fopen($csvfileName, "r");
}else{
    echo "<br> Execution Completed";
    die(fwrite($logFile, "Paramters file not found. Please check the presence of file or check the Filename."));
}
//Checking for the availabilty of parameters_gbs.csv file
while (!feof($csvfile)) {
// Based on the number of executions provided in the Parameters.csv while loop will be executed 
    $arrayindex       = 0;
    $dbResponsearray  = array();
    $apiResponsearray = array();
	$fieldNamesArray        = array();
	$apibuffer = array();
	$dbbuffer = array();
	$type = '';
	/*
	 * dbbuffer array will have the list of contracts which were available in db and not in API
	 * apibuffer array will have the list of contracts which were available in api and not in db
	 */
    $csvArray         = fgetcsv($csvfile, ',');
    
    if ($i != 0) { 
        //$i will be zero for the first iteration so as to skip the first row of the CSV file since, the first row will contain CSV header.
		if(strcasecmp($csvArray[4],'y')==0)
        // Execution will be done only if the value is y		
		{
			$ch          = curl_init();
			$date_after  = $csvArray[2];
			$date_before = $csvArray[3];
			if($csvArray[1]){
				$param       = http_build_query(array(
				'type' => $csvArray[1] . "-account",
				'date-after' => $date_after,
				'date-before' => $date_before
			));
			}else{
				$param       = http_build_query(array(
					'date-after' => $date_after,
					'date-before' => $date_before
				));
			}
			curl_setopt($ch, CURLOPT_URL, $baseURL_GBS . $param);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				"session-id: $sessionID"
			));
			$response     = curl_exec($ch);
			$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if ($responseCode != 200) {
				$outputString = $outputString . $csvArray[0] ."\n\n".$baseURL_GBS . $param ."\n". "\n\nStatus: Fail\n\n" . $responseCode . "\n" . $response;
				array_push($failedSummary,$csvArray[0]);
				curl_close($ch);
				continue;
			}
            // If API response is negative. Response is written to the log file and this iteration is skipped.		
			curl_close($ch);
			$response_array   = explode("\n", $response);
			foreach ($response_array as $resp) {
				$fieldNamesArray[] = str_getcsv($resp);
			}
			for ($j = 1; $j < count($fieldNamesArray) - 1; $j++) {
				for ($i = 0; $i < count($fieldNamesArray[0]); $i++) {
					$apiResponsearray[$j - 1][$fieldNamesArray[0][$i]] = $fieldNamesArray[$j][$i];
				}
			}
            // Storing API response in an Array
			if (strcasecmp($csvArray[1],'billing')==0) {
            /*
             * For Account type billing New and Modified query will be executed
             * Both new and Modified query should be executed for Billing type
            */
				$sql = "SELECT
		cntracts.global_contract_id AS 'Account IF ID'
		,cntracts.product_offering
		,1 AS 'Order Type'
		,1 AS 'Data Type'
		,org.billing_affiliate_code AS 'Billing Affiliate'
		,NULL AS Affiliate
		,NULL AS 'Service Type'
		,IF(bill_acnt.name IS NULL OR bill_acnt.name = '', bill_acnt_cstm.name_2_c , bill_acnt.name) AS 'Billing Account Name'
		,bill_cntry.country_code_alphabet AS 'Billing Country'
		,IF(bill_cnts_cust.addr_2_c IS NULL OR bill_cnts_cust.addr_2_c = '', bill_cnts_cust.addr_1_c, bill_cnts_cust.addr_2_c) AS billing_address_any_1
		,bill_cnts_cust.addr_general_c AS billing_address_us_canada_1
		,NULL AS billing_address_any_2
		,CONCAT(bill_cnts_cust.city_general_c, ', ', bill_state.name, ', ', bill_cnts_cust.postal_no_c) AS billing_address_us_canada_2
		,NULL AS 'Billing Address 3'
		,bill_cnts_cust.attention_line_c AS Attention
		,bill_acnt_cstm.common_customer_id_c AS 'Billing CID'
		,IF(sales.sales_rep_name_1 IS NULL OR sales.sales_rep_name_1 = '', sales.name, sales.sales_rep_name_1) AS 'Account Manager'
		,IF(bill_cnts_cust.name_2_c IS NULL OR bill_cnts_cust.name_2_c = '', bill_cnts.last_name, bill_cnts_cust.name_2_c) AS 'Contact Name'
		,IF(bill_cnts.phone_work IS NULL OR bill_cnts.phone_work = '', bill_cnts.phone_mobile, bill_cnts.phone_work) AS 'Phone Number'
		,email.email_address AS 'Email Address'
		,NULL AS 'Bill Cycle'
		,tbl_line_item.cust_billing_currency AS 'Billing Currency'

FROM  `gc_contracts` AS cntracts
    JOIN 
    (
	SELECT 
	    first_line_contract_no AS lno,
	    gc_lineitm_hist.contracts_id,
	    gc_lineitm_hist.bill_account_id,
	    gc_lineitm_hist.gc_organization_id_c,
	    gc_lineitm_hist.cust_billing_currency
	FROM 
	   (
	     SELECT MIN(g.name) AS first_line_contract_no, gh.contracts_id AS contracts_id
	     FROM gc_line_item_contract_history gh
	          JOIN gc_lineitem g ON gh.line_item_id = g.id
	     WHERE
		g.deleted = 0 AND gh.deleted = 0
	     GROUP BY contracts_id
	    ) AS tbl_cust_line_item
	    JOIN gc_line_item_contract_history gc_lineitm_hist ON gc_lineitm_hist.contracts_id = tbl_cust_line_item.contracts_id 
	    JOIN gc_lineitem gc_line ON gc_line.id = gc_lineitm_hist.line_item_id AND gc_line.name = tbl_cust_line_item.first_line_contract_no
    ) tbl_line_item 
    ON tbl_line_item.contracts_id = cntracts.id
    LEFT JOIN  `gc_organization` AS org ON org.id = tbl_line_item.gc_organization_id_c AND org.deleted = 0
    LEFT JOIN  `gc_salesrepresentative` AS sales ON sales.contracts_id = cntracts.id AND sales.deleted = 0

    LEFT JOIN  `contacts` AS bill_cnts ON bill_cnts.id = tbl_line_item.bill_account_id AND bill_cnts.deleted = 0
    LEFT JOIN  `contacts_cstm` AS bill_cnts_cust ON bill_cnts_cust.id_c = bill_cnts.id
    LEFT JOIN  `c_country` AS bill_cntry ON bill_cntry.id = bill_cnts_cust.c_country_id_c AND bill_cntry.deleted = 0
    LEFT JOIN  `c_state` AS bill_state ON bill_state.id = bill_cnts_cust.c_state_id_c AND bill_state.deleted = 0
    LEFT JOIN  `accounts_contacts` AS bill_acnt_bill_cnts ON bill_acnt_bill_cnts.contact_id = bill_cnts.id AND bill_acnt_bill_cnts.deleted = 0
    LEFT JOIN  `accounts` AS bill_acnt ON bill_acnt.id = bill_acnt_bill_cnts.account_id AND bill_acnt.deleted = 0
    LEFT JOIN  `accounts_cstm` AS bill_acnt_cstm ON bill_acnt_cstm.id_c = bill_acnt.id
    LEFT JOIN  `email_addr_bean_rel` AS bill_acnt_email ON bill_acnt_email.bean_id = bill_cnts.id AND bill_acnt_email.deleted = 0 AND bill_acnt_email.primary_address = 1
    LEFT JOIN  `email_addresses` AS email ON email.id = bill_acnt_email.email_address_id AND email.deleted = 0
		

	WHERE
		(DATE_FORMAT(cntracts.date_entered,'%Y-%m-%d') BETWEEN '{$date_after}' AND '{$date_before}')
		AND (bill_cnts_cust.c_country_id_c <> '' AND bill_cnts_cust.c_country_id_c IS NOT NULL AND bill_cntry.country_code_numeric <> '392')
		AND bill_cnts_cust.contact_type_c = 'Billing'
		AND (org.billing_affiliate_code NOT IN ('" . implode("', '", $notinArray) . "') OR org.billing_affiliate_code IS NULL)
		AND cntracts.product_offering IN ('" . implode("', '", $productOfferingArray) . "')
		ORDER BY cntracts.date_entered DESC";
				
				if (!billing_query_Exec($sql, $connectionInstance)) {
					continue;
				}
                // If there is any issue during query execution the billing_query_Exec function will return 0 and this iteration will be skipped
				
				$sql = "SELECT
		cntracts.global_contract_id AS 'Account IF ID'
		,cntracts.product_offering
		,2 'Order Type'
		,1 AS 'Data Type'
		,NULL AS 'Billing Affiliate'
		,NULL AS 'Affiliate'
		,NULL AS 'Service Type'
		,NULL AS 'Billing Account Name'
		,NULL AS 'Billing Country'
		,NULL AS billing_address_any_1
		,NULL AS billing_address_us_canada_1
		,NULL AS billing_address_any_2
		,NULL AS billing_address_us_canada_2
		,NULL AS 'Billing Address 3'
		,NULL AS Attention
		,bill_acnt_cstm.common_customer_id_c AS 'Billing CID'
		,IF(sales.sales_rep_name_1 IS NULL OR sales.sales_rep_name_1 = '', sales.name, sales.sales_rep_name_1) AS 'Account Manager'
		,IF(bill_cnts_cust.name_2_c IS NULL OR bill_cnts_cust.name_2_c = '', bill_cnts.last_name, bill_cnts_cust.name_2_c) AS 'Contact Name'
		,IF(bill_cnts.phone_work IS NULL OR bill_cnts.phone_work = '', bill_cnts.phone_mobile, bill_cnts.phone_work) AS 'Phone Number'
		,bill_acnt_email.email_address AS 'Email Address'
		,NULL AS 'Bill Cycle'
		,NULL AS 'Billing Currency'

FROM  `gc_contracts` AS cntracts
    JOIN 
    (
	SELECT 
	    first_line_contract_no AS lno,
	    gc_lineitm_hist.contracts_id,
	    gc_lineitm_hist.bill_account_id,
	    gc_lineitm_hist.gc_organization_id_c,
	    gc_lineitm_hist.cust_billing_currency
	FROM 
	   (
	     SELECT MIN(g.name) AS first_line_contract_no, gh.contracts_id AS contracts_id
	     FROM gc_line_item_contract_history gh
	          JOIN gc_lineitem g ON gh.line_item_id = g.id
	     WHERE
		g.deleted = 0 AND gh.deleted = 0
	     GROUP BY contracts_id
	    ) AS tbl_cust_line_item
	    JOIN gc_line_item_contract_history gc_lineitm_hist ON gc_lineitm_hist.contracts_id = tbl_cust_line_item.contracts_id 
	    JOIN gc_lineitem gc_line ON gc_line.id = gc_lineitm_hist.line_item_id AND gc_line.name = tbl_cust_line_item.first_line_contract_no
    ) tbl_line_item ON tbl_line_item.contracts_id = cntracts.id
    LEFT JOIN  `gc_salesrepresentative` AS sales ON sales.contracts_id = cntracts.id AND sales.deleted = 0
    LEFT JOIN  `contacts` AS bill_cnts ON bill_cnts.id = tbl_line_item.bill_account_id AND bill_cnts.deleted = 0
    LEFT JOIN  `contacts_cstm` AS bill_cnts_cust ON bill_cnts_cust.id_c = bill_cnts.id
    LEFT JOIN  `gc_organization` AS org ON org.id = tbl_line_item.gc_organization_id_c AND org.deleted = 0
    LEFT JOIN  `c_country` AS bill_cntry ON bill_cntry.id = bill_cnts_cust.c_country_id_c AND bill_cntry.deleted = 0
    LEFT JOIN  `accounts_contacts` AS bill_acnt_bill_cnts ON bill_acnt_bill_cnts.contact_id = bill_cnts.id AND bill_acnt_bill_cnts.deleted = 0
    LEFT JOIN  `accounts` AS bill_acnt ON bill_acnt.id = bill_acnt_bill_cnts.account_id AND bill_acnt.deleted = 0
    LEFT JOIN  `accounts_cstm` AS bill_acnt_cstm ON bill_acnt_cstm.id_c = bill_acnt.id
    LEFT JOIN  `email_addr_bean_rel` AS acnt_email ON acnt_email.bean_id = bill_cnts.id AND acnt_email.deleted = 0 AND acnt_email.primary_address = 1
    LEFT JOIN  `email_addresses` AS bill_acnt_email ON bill_acnt_email.id = acnt_email.email_address_id AND bill_acnt_email.deleted = 0
				    

	WHERE
		bill_cnts_cust.contact_type_c = 'Billing'
		AND (bill_cnts_cust.c_country_id_c <> '' AND bill_cnts_cust.c_country_id_c IS NOT NULL AND bill_cntry.country_code_numeric <> '392')
		AND (org.billing_affiliate_code NOT IN ('" . implode("', '", $notinArray) . "') OR org.billing_affiliate_code IS NULL)
		AND cntracts.product_offering IN ('" . implode("', '", $productOfferingArray) . "')
		AND
		(
			(
				DATE_FORMAT(sales.date_entered,'%Y-%m-%d') < '{$date_after}' AND
				(DATE_FORMAT(sales.date_modified,'%Y-%m-%d') BETWEEN '{$date_after}' AND '{$date_before}')
			) OR (
				DATE_FORMAT(bill_cnts.date_entered,'%Y-%m-%d') < '{$date_after}' AND
				(DATE_FORMAT(bill_cnts.date_modified,'%Y-%m-%d') BETWEEN '{$date_after}' AND '{$date_before}')
			)
		)
	ORDER BY cntracts.date_entered DESC";
				
				if (!billing_query_Exec($sql, $connectionInstance)) {
					continue;
				}
                // If there is any issue during query execution the billing_query_Exec function will return 0 and this iteration will be skipped

			} elseif (strcasecmp($csvArray[1],'contract')==0) {
            /*
             * For Account type contract New and Modified query will be executed
             * Both new and Modified query should be executed for Billing type
            */
				$sql = "SELECT
		cntracts.global_contract_id as 'Contract ID'
		,1 as 'Order Type'
		,IF(cntract_acnt.name IS NULL OR cntract_acnt.name = '', cntract_acnt_cstm.name_2_c, cntract_acnt.name) AS 'Contract Account Name'
		,cntract_cntry.country_code_alphabet AS 'Country'
		,IF(cntract_cntry.country_code_numeric ='840' OR cntract_cntry.country_code_numeric ='124',
		concat(cntract_cnts_cust.addr_general_c,' ',cntract_cnts_cust.city_general_c,' ',cntract_state.name,' ',cntract_cnts_cust.postal_no_c),
		IF(cntract_cnts_cust.addr_2_c IS NULL OR cntract_cnts_cust.addr_2_c = '', cntract_cnts_cust.addr_1_c, cntract_cnts_cust.addr_2_c)) as 'Address'
		,cntract_acnt_cstm.common_customer_id_c as CID
		,cntracts.vat_no AS 'VAT No'

	FROM  `gc_contracts` AS cntracts
JOIN
    (
	SELECT 
	    first_line_contract_no AS lno,
	    gc_lineitm_hist.contracts_id,
	    gc_lineitm_hist.bill_account_id,
	    gc_lineitm_hist.gc_organization_id_c,
	    gc_lineitm_hist.cust_billing_currency
	FROM 
	   (
	     SELECT MIN(g.name) AS first_line_contract_no, gh.contracts_id AS contracts_id
	     FROM gc_line_item_contract_history gh
	          JOIN gc_lineitem g ON gh.line_item_id = g.id
	     WHERE
		g.deleted = 0 AND gh.deleted = 0
	     GROUP BY contracts_id
	    ) AS tbl_cust_line_item
	    JOIN gc_line_item_contract_history gc_lineitm_hist ON gc_lineitm_hist.contracts_id = tbl_cust_line_item.contracts_id 
	    JOIN gc_lineitem gc_line ON gc_line.id = gc_lineitm_hist.line_item_id AND gc_line.name = tbl_cust_line_item.first_line_contract_no
    ) tbl_line_item 
	ON tbl_line_item.contracts_id = cntracts.id
    LEFT JOIN  `gc_organization` AS org ON org.id = tbl_line_item.gc_organization_id_c AND org.deleted = 0
        
    LEFT JOIN  `contacts` AS bill_cnts ON bill_cnts.id = tbl_line_item.bill_account_id AND bill_cnts.deleted = 0
    LEFT JOIN  `contacts_cstm` AS bill_cnts_cust ON bill_cnts_cust.id_c = bill_cnts.id
    LEFT JOIN  `c_country` AS bill_cntry ON bill_cntry.id = bill_cnts_cust.c_country_id_c AND bill_cntry.deleted = 0

    LEFT JOIN  `contacts_gc_contracts_1_c` cntract_cnts_cntracts ON cntract_cnts_cntracts.contacts_gc_contracts_1gc_contracts_idb = cntracts.id AND cntract_cnts_cntracts.deleted = 0
    LEFT JOIN  `contacts` cntract_cnts ON cntract_cnts.id = cntract_cnts_cntracts.contacts_gc_contracts_1contacts_ida AND cntract_cnts.deleted = 0
    LEFT JOIN  `contacts_cstm` cntract_cnts_cust ON cntract_cnts_cust.id_c = cntract_cnts.id
    LEFT JOIN  `accounts_gc_contracts_1_c` cntract_acnt_cntracts ON cntract_acnt_cntracts.accounts_gc_contracts_1gc_contracts_idb = cntracts.id AND cntract_acnt_cntracts.deleted = 0
    LEFT JOIN  `accounts` cntract_acnt ON cntract_acnt.id = cntract_acnt_cntracts.accounts_gc_contracts_1accounts_ida AND cntract_acnt.deleted = 0
    LEFT JOIN  `accounts_cstm` cntract_acnt_cstm ON cntract_acnt_cstm.id_c = cntract_acnt.id
    LEFT JOIN  `c_country` cntract_cntry ON cntract_cntry.id = cntract_cnts_cust.c_country_id_c AND cntract_cntry.deleted = 0
    LEFT JOIN  `c_state` cntract_state ON cntract_state.id = cntract_cnts_cust.c_state_id_c AND cntract_state.deleted = 0
	
	WHERE
		(DATE_FORMAT(cntracts.date_entered,'%Y-%m-%d') BETWEEN '{$date_after}' AND '{$date_before}')
		AND (bill_cnts_cust.c_country_id_c <> '' AND bill_cnts_cust.c_country_id_c IS NOT NULL AND bill_cntry.country_code_numeric <> '392')
		AND (org.billing_affiliate_code NOT IN ('" . implode("', '", $notinArray) . "') OR org.billing_affiliate_code IS NULL)
		AND bill_cnts_cust.contact_type_c = 'billing'
		AND cntracts.product_offering IN ('" . implode("', '", $productOfferingArray) . "')
		ORDER BY cntracts.date_entered DESC";
				
				if (!contract_query_Exec($sql, $connectionInstance)) {
					continue;
				}
                // If there is any issue during query execution the contract_query_Exec function will return 0 and this iteration will be skipped
				
				$sql = "SELECT
		cntracts.global_contract_id as 'Contract ID'
		,2 as 'Order Type'
		,IF(cntract_acnt.name IS NULL OR cntract_acnt.name = '', cntract_acnt_cstm.name_2_c, cntract_acnt.name) AS 'Contract Account Name'
		,NULL AS Country
		,IF(cntract_cntry.country_code_numeric ='840' OR cntract_cntry.country_code_numeric ='124',
		concat(cntract_cnts_cust.addr_general_c,' ',cntract_cnts_cust.city_general_c,' ',cntract_state.name,' ',cntract_cnts_cust.postal_no_c),
		IF(cntract_cnts_cust.addr_2_c IS NULL OR cntract_cnts_cust.addr_2_c = '', cntract_cnts_cust.addr_1_c, cntract_cnts_cust.addr_2_c)) as Address
		,cntract_acnt_cstm.common_customer_id_c as CID
		,cntracts.vat_no AS 'VAT No'

	FROM  `gc_contracts` AS cntracts
    JOIN
	(
	SELECT 
	    first_line_contract_no AS lno,
	    gc_lineitm_hist.contracts_id,
	    gc_lineitm_hist.bill_account_id,
	    gc_lineitm_hist.gc_organization_id_c,
	    gc_lineitm_hist.cust_billing_currency
	FROM 
	   (
	     SELECT MIN(g.name) AS first_line_contract_no, gh.contracts_id AS contracts_id
	     FROM gc_line_item_contract_history gh
	          JOIN gc_lineitem g ON gh.line_item_id = g.id
	     WHERE
		g.deleted = 0 AND gh.deleted = 0
	     GROUP BY contracts_id
	    ) AS tbl_cust_line_item
	    JOIN gc_line_item_contract_history gc_lineitm_hist ON gc_lineitm_hist.contracts_id = tbl_cust_line_item.contracts_id 
	    JOIN gc_lineitem gc_line ON gc_line.id = gc_lineitm_hist.line_item_id AND gc_line.name = tbl_cust_line_item.first_line_contract_no
    ) tbl_line_item ON tbl_line_item.contracts_id = cntracts.id
    LEFT JOIN  `gc_organization` AS org ON org.id = tbl_line_item.gc_organization_id_c AND org.deleted = 0
        
    LEFT JOIN  `contacts` AS bill_cnts ON bill_cnts.id = tbl_line_item.bill_account_id AND bill_cnts.deleted = 0
    LEFT JOIN  `contacts_cstm` AS bill_cnts_cust ON bill_cnts_cust.id_c = bill_cnts.id
    LEFT JOIN  `c_country` AS bill_cntry ON bill_cntry.id = bill_cnts_cust.c_country_id_c AND bill_cntry.deleted = 0

    LEFT JOIN  `contacts_gc_contracts_1_c` cntract_cnts_cntracts ON cntract_cnts_cntracts.contacts_gc_contracts_1gc_contracts_idb = cntracts.id AND cntract_cnts_cntracts.deleted = 0
    LEFT JOIN  `contacts` cntract_cnts ON cntract_cnts.id = cntract_cnts_cntracts.contacts_gc_contracts_1contacts_ida AND cntract_cnts.deleted = 0
    LEFT JOIN  `contacts_cstm` cntract_cnts_cust ON cntract_cnts_cust.id_c = cntract_cnts.id
    LEFT JOIN  `accounts_gc_contracts_1_c` cntract_acnt_cntracts ON cntract_acnt_cntracts.accounts_gc_contracts_1gc_contracts_idb = cntracts.id AND cntract_acnt_cntracts.deleted = 0
    LEFT JOIN  `accounts` cntract_acnt ON cntract_acnt.id = cntract_acnt_cntracts.accounts_gc_contracts_1accounts_ida AND cntract_acnt.deleted = 0
    LEFT JOIN  `accounts_cstm` cntract_acnt_cstm ON cntract_acnt_cstm.id_c = cntract_acnt.id
    LEFT JOIN  `c_country` cntract_cntry ON cntract_cntry.id = cntract_cnts_cust.c_country_id_c AND cntract_cntry.deleted = 0
    LEFT JOIN  `c_state` cntract_state ON cntract_state.id = cntract_cnts_cust.c_state_id_c AND cntract_state.deleted = 0

	WHERE
		bill_cnts_cust.contact_type_c = 'billing'
		AND (bill_cnts_cust.c_country_id_c <> '' AND bill_cnts_cust.c_country_id_c IS NOT NULL AND bill_cntry.country_code_numeric <> '392')
		AND (org.billing_affiliate_code NOT IN ('" . implode("', '", $notinArray) . "') OR org.billing_affiliate_code IS NULL)
		AND cntracts.product_offering IN ('" . implode("', '", $productOfferingArray) . "')
		AND
		(
			(
				DATE_FORMAT(cntracts.date_entered,'%Y-%m-%d') < '{$date_after}' AND
				(DATE_FORMAT(cntracts.date_modified,'%Y-%m-%d') BETWEEN '{$date_after}' AND '{$date_before}')
			) OR (
				DATE_FORMAT(cntract_acnt.date_entered,'%Y-%m-%d') < '{$date_after}' AND
				(DATE_FORMAT(cntract_acnt.date_modified,'%Y-%m-%d') BETWEEN '{$date_after}' AND '{$date_before}')
			) OR (
				cntract_cnts_cust.contact_type_c = 'Contract' AND
				DATE_FORMAT(cntract_cnts.date_entered,'%Y-%m-%d') < '{$date_after}' AND
				(DATE_FORMAT(cntract_cnts.date_modified,'%Y-%m-%d') BETWEEN '{$date_after}' AND '{$date_before}')
			)
		)
	ORDER BY cntracts.date_entered DESC";
				
				if (!contract_query_Exec($sql, $connectionInstance)) {
					continue;
				}
                // If there is any issue during query execution the contract_query_Exec function will return 0 and this iteration will be skipped
			}
			$dbbuffer[$csvArray[0]]  = $dbResponsearray;
			$apibuffer[$csvArray[0]] = $apiResponsearray;
			if (count($dbResponsearray) > 0 || count($apiResponsearray) > 0) {
				$index                   = 0;
				for ($m = 0; $m < count($dbResponsearray); $m++) {
					for ($n = 0; $n < count($apiResponsearray); $n++) {
					    if(!isset($apibuffer[$csvArray[0]][$n])){
					        continue;
					    }
						if ($dbResponsearray[$m][$fieldNamesArray[0][0]] == $apiResponsearray[$n][$fieldNamesArray[0][0]] && $dbResponsearray[$m]['Order Type'] == $apiResponsearray[$n]['Order Type']) {
							for ($o = 0; $o < count($fieldNamesArray[0]); $o++) {
								if (($fieldNamesArray[0][$o] != 'Account IF ID' || $fieldNamesArray[0][$o] != 'Contract ID') && $fieldNamesArray[0][$o] != 'Order Type') {
									if ($dbResponsearray[$m][$fieldNamesArray[0][$o]] == $apiResponsearray[$n][$fieldNamesArray[0][$o]]) {
										if (!isset($flag)) {
										// $flag will be 0 if there is no mismatch between the compared data
											$flag = 0;
										}
									} else {
										if (isset($mismatchArray[$csvArray[0]]['API'])) {
											for ($s = 0; $s < count($mismatchArray[$csvArray[0]]['API']); $s++) {
												if ($mismatchArray[$csvArray[0]]['API'][$s]['Contract ID'] == $dbResponsearray[$m][$fieldNamesArray[0][0]] && $mismatchArray[$csvArray[0]]['API'][$s]['Order Type'] == $dbResponsearray[$m]['Order Type']) {
													$mismatchArray[$csvArray[0]]['API'][$s][$fieldNamesArray[0][$o]] = $apiResponsearray[$n][$fieldNamesArray[0][$o]];
													$mismatchArray[$csvArray[0]]['DB'][$s][$fieldNamesArray[0][$o]]  = $dbResponsearray[$m][$fieldNamesArray[0][$o]];
													$flag                                                      = 2;
												}
											}
											if ($flag != 2) {
												mismatcharray_NewInsert($arrayindex,$fieldNamesArray[0][$o]);
												$flag = 1;
											}
										} else {
											mismatcharray_NewInsert($arrayindex,$fieldNamesArray[0][$o]);
											$flag = 1;
										}
									}
									// $flag will be not 0 if there is mismatch between the compared data
								}
							}
						}
						if (isset($flag)) {
							if ($flag == 0) {
								$matchArray[$csvArray[0]][$index]['Contract ID'] = $dbResponsearray[$m][$fieldNamesArray[0][0]];
								$matchArray[$csvArray[0]][$index]['Order Type']  = $dbResponsearray[$m]['Order Type'];
								$index++;
							}
							// Record will be pushed to $matchArray if flag is 0
							unset($apibuffer[$csvArray[0]][$n]);
							unset($dbbuffer[$csvArray[0]][$m]);
							// Record will be removed from $apibuffer and $dbbuffer if flag is set.
							unset($flag);
							break;
						}
					}
				}
			}
			$outputString = $outputString . "\n" . $csvArray[0] . "\n";
			$outputString = $outputString . "\n" . $baseURL_GBS . $param . "\n";
			$outputString = $outputString . "\nSummary \n";
			$outputString = $outputString . "TotalCount in DB Results: " . count($dbResponsearray) . "\n";
			$outputString = $outputString . "TotalCount in API Response: " . count($apiResponsearray) . "\n";
			if (isset($mismatchArray[$csvArray[0]]) || count($dbbuffer[$csvArray[0]])>0 || count($apibuffer[$csvArray[0]])>0) {
			    array_push($failedSummary,$csvArray[0]);
				$outputString = $outputString . "Available in API Response but not in DB Results " . count($apibuffer[$csvArray[0]]) . "\n";
				$outputString = $outputString . "Available in DB Results but not in API Response " . count($dbbuffer[$csvArray[0]]) . "\n";
				if (isset($mismatchArray[$csvArray[0]]['API'])) {
					$outputString = $outputString . "Data Discrepancy: " . count($mismatchArray[$csvArray[0]]['API']) . "\n";
				} else {
					$outputString = $outputString . "Data Discrepancy: 0 \n";
				}
				$outputString = $outputString . "\nStatus : Fail \n";
				if (isset($mismatchArray[$csvArray[0]]['API'])) {
					$outputString = $outputString . "\nDiscrepency in Data \n";
					for ($l = 0; $l < count($mismatchArray[$csvArray[0]]['API']); $l++) {
						$outputString = $outputString . ($l + 1) . ". ";
						$headerString = "\n\nField Name || Value in API || Value in DB\n";
						foreach (array_keys($mismatchArray[$csvArray[0]]['API'][$l]) as $a => $value) {
							if ($value == 'Contract ID' || $value == 'Order Type') {
								$outputString = $outputString . $value . "=>" . $mismatchArray[$csvArray[0]]['API'][$l][$value] . " || ";
							} else {
								$headerString = $headerString . $value . " || " . $mismatchArray[$csvArray[0]]['API'][$l][$value] . " || " . $mismatchArray[$csvArray[0]]['DB'][$l][$value] . "\n";
							}
						}
						$outputString = $outputString . $headerString;
						$outputString = $outputString . "\n";
					}
					$counter = 1;
				}
				if (count($dbbuffer[$csvArray[0]]) > 0) {
					$outputString = $outputString . "\nAvailable in DB Results but not in API Response\n\n";
					foreach ($dbbuffer[$csvArray[0]] as $key) {
						$outputString = $outputString . $counter . ". ";
						foreach ($key as $a => $value) {
							if ($a == 'Contract ID' || $a == 'Account IF ID' || $a == 'Order Type') {
								$outputString = $outputString . $a . "=>" . $value . " || ";
							}
						}
						$counter++;
						rtrim($outputString, " || ");
						$outputString = $outputString . "\n";
					}
				}
				$counter = 1;
				if (count($apibuffer[$csvArray[0]]) > 0) {
					$outputString = $outputString . "\nAvailable in API Response but not in DB Results\n\n";
					foreach ($apibuffer[$csvArray[0]] as $key) {
						$outputString = $outputString . $counter . ". ";
						foreach ($key as $a => $value) {
							if ($a == 'Contract ID' || $a == 'Account IF ID' || $a == 'Order Type') {
								$outputString = $outputString . $a . "=>" . $value . " || ";
							}
						}
						$counter++;
						rtrim($outputString, " || ");
						$outputString = $outputString . "\n";
					}
				}
				$counter = 1;
				unset($apibuffer[$csvArray[0]], $dbbuffer[$csvArray[0]], $mismatchArray[$csvArray[0]], $mismatchArray[$csvArray[0]]['API']);
			} elseif (count($dbResponsearray) == 0 && count($apiResponsearray) == 0) {
				$outputString = $outputString . "\nNo Records Available for the given parameters.\n";
			} else {
				$outputString = $outputString . "\nStatus: Pass\n";
			}
		}elseif(strcasecmp($csvArray[4],'n')==0){
			 $outputString = $outputString ."\n". $csvArray[0] . "\n\nTest Case Skipped\n";
		}else{
		    $outputString = $outputString ."\n". $csvArray[0] . "\n\nInvalid Execution value. It should only be 'Y' or 'N'\n";
		    array_push($failedSummary,$csvArray[0]);
		}
		// After every iteration, the result of that particular iterartion is appended to $outputString which will be written to the log file after all the iteration
    }
    $i = 1;
    // After first iteration $1 will be 1 
}
$outputString = $outputString . "\n\nExecution completed : " . date('Ymd_His');
$finalString = $finalString.implode(",", $failedSummary);
$finalString = $finalString ."\n". $outputString;
fwrite($logFile, $finalString);
// After executing the complete suite output is written to the log file.
fclose($logFile);
echo "<br>Execution completed : ".date('Ymd_His');
function billing_query_Exec($sql, $connectionInstance){
    
    global $dbResponsearray, $xmlpath, $outputString;
    try {
        $retval = $connectionInstance->query($sql);
    }
    catch (PDOException $e) {
        $outputString = $outputString . $csvArray[0] . "\nStatus : Fail \n\n" . $e->getMessage();
        array_push($failedSummary,$csvArray[0]);
        return 0;
        // Will return zero if there is any issue in the query execution
    }
    while ($fetch = $retval->fetch(PDO::FETCH_ASSOC)) {
        if ($fetch['Order Type'] != "2") {
            $xml = simplexml_load_file($xmlpath . "_" . $fetch['product_offering'] . ".xml");
            foreach ($xml->listofferingoption->options->option as $option) {
                if ($option->key == 'gbs_billing_cycle') {
                    $fetch['Bill Cycle'] = (string) $option->value;
                }
            }
        }
        // Bill cycle value is not available in DB. It has to be fetched from the respective product offering XML.
        unset($fetch['product_offering']);
        if ($fetch['billing_address_any_1'] == NULL) {
            $fetch['Billing Address 1'] = $fetch['billing_address_us_canada_1'];
            $fetch['Billing Address 2'] = $fetch['billing_address_us_canada_2'];
            unset($fetch['billing_address_us_canada_1'], $fetch['billing_address_us_canada_2'], $fetch['billing_address_any_1'], $fetch['billing_address_any_2']);
        } else {
            $fetch['Billing Address 1'] = $fetch['billing_address_any_1'];
            $fetch['Billing Address 2'] = $fetch['billing_address_any_2'];
            unset($fetch['billing_address_any_1'], $fetch['billing_address_any_2'], $fetch['billing_address_us_canada_1'], $fetch['billing_address_us_canada_2']);
        }
        array_push($dbResponsearray, $fetch);
    }
    return 1;
}
function contract_query_Exec($sql, $connectionInstance){
    
    global $dbResponsearray,$outputString;
    try {
        $retval = $connectionInstance->query($sql);
    }
    catch (PDOException $e) {
        $outputString = $outputString . $csvArray[0] . "\n Status : Fail \n\n" . $e->getMessage();
        array_push($failedSummary,$csvArray[0]);
        return 0;
        // Will return zero if there is any issue in the query execution
    }
    while ($fetch = $retval->fetch(PDO::FETCH_ASSOC)) {
        array_push($dbResponsearray, $fetch);
    }
    return 1;
}
function mismatcharray_NewInsert($arrayindex,$index){
    global $dbResponsearray,$apiResponsearray,$outputString,$mismatchArray,$fieldNamesArray,$n,$m,$o,$csvArray;
    $mismatchArray[$csvArray[0]]['API'][$arrayindex]['Contract ID']     = $apiResponsearray[$n][$fieldNamesArray[0][0]];
    $mismatchArray[$csvArray[0]]['API'][$arrayindex]['Order Type']      = $apiResponsearray[$n]['Order Type'];
    $mismatchArray[$csvArray[0]]['API'][$arrayindex][$index] = $apiResponsearray[$n][$fieldNamesArray[0][$o]];
    $mismatchArray[$csvArray[0]]['DB'][$arrayindex]['Contract ID']      = $dbResponsearray[$m][$fieldNamesArray[0][0]];
    $mismatchArray[$csvArray[0]]['DB'][$arrayindex]['Order Type']       = $dbResponsearray[$m]['Order Type'];
    $mismatchArray[$csvArray[0]]['DB'][$arrayindex][$index]  = $dbResponsearray[$m][$fieldNamesArray[0][$o]];
    $arrayindex++;
}
?> 