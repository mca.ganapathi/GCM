package com.stta.GCM;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class EditCorporate extends SuiteGcmBase {

	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[] = null;
	static boolean TestCasePass = true;
	static int DataSet = -1;
	static boolean Testskip = false;
	static boolean Testfail = false;
	Screen scrn = new Screen();

	login log = new login();

	/*
	 * @BeforeClass public void login() throws Exception{
	 * System.out.println("Start login"); log.Run(); }
	 */

	@BeforeTest
	public void checkCaseToRun() throws Exception {
		// Called init() function from SuiteBase class to Initialize .xls Files
		init();
		// Thread.sleep(5000);
		System.out.println("Initialize");
		driver = null;
		// log.Run();

		// To set SuiteOne.xls file's path In FilePath Variable.
		FilePath = TestCaseListExcelOne;
		TestCaseName = this.getClass().getSimpleName();
		// SheetName to check CaseToRun flag against test case.
		SheetName = "TestCasesList";
		// Name of column In TestCasesList Excel sheet.
		ToRunColumnNameTestCase = "CaseToRun";
		// Name of column In Test Case Data sheets.
		ToRunColumnNameTestData = "DataToRun";
		// Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(TestCaseName + " : Execution started.");

		// To check test case's CaseToRun = Y or N In related excel sheet.
		// If CaseToRun = N or blank, Test case will skip execution. Else It
		// will be executed.
		if (!SuiteUtility.checkToRunUtility(FilePath, SheetName, ToRunColumnNameTestCase, TestCaseName)) {
			Add_Log.info(TestCaseName + " : CaseToRun = N for So Skipping Execution.");
			// To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
			// To throw skip exception for this test case.
			throw new SkipException(
					TestCaseName + "'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of " + TestCaseName);
		}
		// To retrieve DataToRun flags of all data set lines from related test
		// data sheet.
		TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);

		System.out.println(TestDataToRun);
		System.out.flush();
	}

	@Test(dataProvider = "EditCorporateData")
	public void EditCorporateTest(String Test_CaseId, String Common_Customer_ID, String Corporate_Name_Latin_Character,
			String Corporate_Name_Local_Character_1, String Corporate_Name_Local_Character_2, String Country,
			String country_code, String HQ_postNumber, String HQ_Address_Latin, String HQ_Address_Local,
			String Reg_PostalNumber, String Reg_Address_Latin, String Reg_Address_Local, String VatNumber,
			String ExpectedResult) throws Exception {

		DataSet++;

		// If found DataToRun = "N" for data set then execution will be skipped
		// for that data set.
		if (!TestDataToRun[DataSet].equalsIgnoreCase("Y")) {
			Add_Log.info(TestCaseName + " : DataToRun = N for data set line " + (DataSet + 1)
					+ " So skipping Its execution.");
			// If DataToRun = "N", Set Testskip=true.
			Testskip = true;
			throw new SkipException(
					"DataToRun for row number " + DataSet + " Is No Or Blank. So Skipping Its Execution.");
		}

		// WebElement element = driver.findElement(By.linkText("Corporates"));
		// Actions action = new Actions(driver);
		// action.moveToElement(element).build().perform();
		// driver.findElement(By.id("ViewCorporatesAll")).click();
		String CCid, TempName;
		int count;

		EditCorporate EC = new EditCorporate();
		CreateCorporateNewData CCN = new CreateCorporateNewData();

		Reporter.log("---------------Edit corporate Function --------------------");

		if (Test_CaseId.equalsIgnoreCase("TC_NJ40_004_001")) {

			// As the CID is mandetory for the display in list view, if no CID
			// no edit is possible
			/*
			 * EC.searchCorporate(Common_Customer_ID); EC.clearFeilds();
			 * 
			 * 
			 * driver.findElement(By.id("common_customer_id_c")).sendKeys(
			 * Common_Customer_ID); driver.findElement(By.id("name")).sendKeys(
			 * Corporate_Name_Latin_Character);
			 * driver.findElement(By.id("name_2_c")).sendKeys(
			 * Corporate_Name_Local_Character_1);
			 * driver.findElement(By.id("name_3_c")).sendKeys(
			 * Corporate_Name_Local_Character_2);
			 * driver.findElement(By.id("country_name_c")).sendKeys(Country);
			 * driver.findElement(By.id("country_code_c")).sendKeys(country_code
			 * ); driver.findElement(By.id("hq_postal_no_c")).sendKeys(
			 * HQ_postNumber);
			 * driver.findElement(By.id("hq_addr_1_c")).sendKeys(HQ_postNumber);
			 * driver.findElement(By.id("hq_addr_2_c")).sendKeys(
			 * HQ_Address_Latin);
			 * driver.findElement(By.id("reg_addr_1_c")).sendKeys(
			 * Reg_PostalNumber);
			 * driver.findElement(By.id("reg_addr_2_c")).sendKeys(
			 * Reg_Address_Latin);
			 * 
			 * driver.findElement(By.id("save")).click(); CCid =
			 * driver.findElement(By.id("common_customer_id_c")).getText();
			 * 
			 * if(CCid.equalsIgnoreCase(Common_Customer_ID)){
			 * Reporter.log("-.Common Customer ID is match"+CCid);
			 * Reporter.log("-.All the feilds were updated"); }else{
			 * 
			 * Reporter.log("-.Common Customer ID did not  match ");
			 * Reporter.log("-.Common Customer ID in application"+CCid);
			 * Reporter.log("-.Common Customer ID in test data input"
			 * +Common_Customer_ID); Assert.fail();
			 * 
			 * }
			 */

			/* Added for automation corporate edit without using CID */
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			scrn.getscreenshot();
			driver.findElement(By.id("edit_button")).click();
			Thread.sleep(15000);
			EC.clearFeilds();

			CCN.corporateFillData(Common_Customer_ID, Corporate_Name_Latin_Character, Corporate_Name_Local_Character_1,
					Corporate_Name_Local_Character_2, Country, country_code, HQ_postNumber, HQ_Address_Latin,
					HQ_Address_Local, Reg_PostalNumber, Reg_Address_Latin, Reg_Address_Local);
			scrn.getscreenshot();
			driver.findElement(By.id("save")).click();
			Thread.sleep(6000);
			CCN.corporateVerifyData(Common_Customer_ID, Corporate_Name_Latin_Character,
					Corporate_Name_Local_Character_1, Corporate_Name_Local_Character_2, Country, country_code,
					HQ_postNumber, HQ_Address_Latin, HQ_Address_Local, Reg_PostalNumber, Reg_Address_Latin,
					Reg_Address_Local);
			scrn.getscreenshot();
		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ40_004_002")) {

			// CIDAS EDIT
			EC.searchCorporate(Common_Customer_ID);
			Thread.sleep(10000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			scrn.getscreenshot();
			driver.findElement(By.id("edit_button")).click();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.xpath(".//*[@id='EditView']/table/tbody/tr/td[1]/div/input[3]")).click();
			String winHandleBefore = driver.getWindowHandle();
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
			}
			scrn.getscreenshot();
			driver.switchTo().defaultContent();
			driver.findElement(By.xpath("html/body/table[3]/tbody/tr[2]/td[2]/a")).click();
			driver.switchTo().window(winHandleBefore);
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			scrn.getscreenshot();
			driver.findElement(By.id("save")).click();
			scrn.getscreenshot();
			/*
			 * EC.clearFeilds();
			 * 
			 * driver.findElement(By.id("save")).click(); String ErrrorMsg =
			 * driver.findElement(By.xpath("//table/tbody/tr[1]/td[2]/div")).
			 * getText(); Reporter.log(ErrrorMsg); Reporter.
			 * log("-.Mandetory feild varification completed for Corporate Name (Latin Character)"
			 * );
			 * driver.findElement(By.xpath(".//*[@id='moduleTab_AllHome']/img"))
			 * .click();
			 * 
			 * Alert alert = driver.switchTo().alert(); alert.accept();
			 */

		} else {

			Reporter.log("TestCaseid did not match");
			Assert.fail();
		}

		Reporter.log("--------------- corporate Function --------------------");

	}

	@DataProvider
	public Object[][] EditCorporateData() {
		// To retrieve data from Data 1 Column,Data 2 Column,Data 3 Column and
		// Expected Result column of SuiteOneCaseOne data Sheet.
		// Last two columns (DataToRun and Pass/Fail/Skip) are Ignored
		// programatically when reading test data.
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}

	public void clearFeilds() throws InterruptedException {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("name")).clear();
		driver.findElement(By.id("name_2_c")).clear();
		driver.findElement(By.id("name_3_c")).clear();
		driver.findElement(By.id("country_name_c")).clear();
		driver.findElement(By.id("country_code_c")).clear();
		driver.findElement(By.id("hq_postal_no_c")).clear();
		driver.findElement(By.id("hq_addr_1_c")).clear();
		driver.findElement(By.id("hq_addr_2_c")).clear();
		driver.findElement(By.id("reg_addr_1_c")).clear();
		driver.findElement(By.id("reg_addr_2_c")).clear();
		// driver.findElement(By.id("vat_no_c")).clear();
		// driver.findElement(By.id("common_customer_id_c")).clear();
		driver.findElement(By.id("reg_postal_no_c")).clear();
		// driver.findElement(By.id("vat_no_c")).clear();
	}

	public void searchCorporate(String Common_Customer_ID) throws Exception {

		/*
		 * driver.findElement(By.id("ViewCorporatesAll")).click();
		 * driver.findElement(By.id("search_form_clear")).click();
		 * driver.findElement(By.id("common_customer_id_c_basic")).sendKeys(
		 * Common_Customer_ID);
		 * driver.findElement(By.id("search_form_submit")).click();
		 * driver.findElement(By.xpath("//tr[3]/td[4]/b/a")).click();
		 * driver.findElement(By.id("edit_button")).click();
		 */

		/* Added for automation */
		driver.switchTo().defaultContent();
		Thread.sleep(5000);
		System.out.println("Inside Search Corpoarte ");
		driver.findElement(By.xpath("//div/div[2]/div/div/div/div[1]/ul/li[2]/span/button")).click();
		driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[2]/span/div/ul/li[2]/a")).click();
		// driver.switchTo().frame("bwc-frame");
		System.out.println("After Header Corpoarte ");
		Thread.sleep(5000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		boolean buttonAvailable = driver.findElement(By.id("basic_search_link")).isDisplayed();
		if (buttonAvailable) {
			driver.findElement(By.id("basic_search_link")).click();
			System.out.println("Inside Basic search");
		}
		// driver.findElement(By.id("advanced_search_link")).click();
		driver.findElement(By.id("common_customer_id_c_advanced")).clear();
		driver.findElement(By.id("common_customer_id_c_advanced")).sendKeys(Common_Customer_ID);
		driver.findElement(By.id("search_form_submit_advanced")).click();
		Thread.sleep(5000);
		// scrn.getscreenshot();
		/*
		 * System.out.println("sleep start"); Thread.sleep(5000);
		 * System.out.println("sleep end");
		 */
		// driver.findElement(By.xpath("//tr[3]/td[4]/b/a")).click();
		System.out.println("*************^^After Sleep^^^^^^^^^^^^^^^^^^^^ ");
		// driver.switchTo().defaultContent();
		// sdriver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.xpath(".//*[@id='MassUpdate']/table/tbody/tr[3]/td[4]")).click();
		System.out.println("************* ");
	}

	@AfterTest
	public void close() {
		System.out.println("Inside after close");
		driver.quit();
	}

}
