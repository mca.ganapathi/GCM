package com.stta.GCM;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;



public class File extends SuiteGcmBase{
	
	@Test
	public void fileTest() throws AWTException, InterruptedException{
		
		driver.findElement(By.xpath("//div[4]/div[2]/ul[2]/li[5]/a/em")).click();
		WebElement element = driver.findElement(By.linkText("Contracts"));
		Actions action = new Actions(driver);
	    action.moveToElement(element).build().perform();
        driver.findElement(By.id("ViewContractsAll")).click();
        driver.findElement(By.id("global_contract_id_basic")).sendKeys("920000000001259");
        driver.findElement(By.id("search_form_submit")).click();
        driver.findElement(By.xpath("//div[3]/form[3]/table/tbody/tr[3]/td[4]/b/a")).click();
        driver.findElement(By.xpath("//div/div/table/thead/tr[1]/td/div/ul/li/a")).click();
       
        String winHandleBefore = driver.getWindowHandle();
	    for (String handle : driver.getWindowHandles()) {

	        driver.switchTo().window(handle);
	       
	    }
        
         driver.findElement(By.id("filename_file")).click();
        
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.MINUTES);
         
        StringSelection ss = new StringSelection("C:\\Users\\c_atuld\\Desktop\\path.txt");
      
    
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
       

        Robot r = new Robot();
        r.keyPress(KeyEvent.VK_ENTER);
        r.keyRelease(KeyEvent.VK_ENTER);
        r.keyPress(KeyEvent.VK_CONTROL);  
        r.keyPress(KeyEvent.VK_V);
        r.keyRelease(KeyEvent.VK_V);    
        r.keyRelease(KeyEvent.VK_CONTROL);
        r.keyPress(KeyEvent.VK_ENTER);
        r.keyRelease(KeyEvent.VK_ENTER);
        
        
        driver.findElement(By.id("SAVE_FOOTER")).click();
        
    
        
        driver.switchTo().window(winHandleBefore);
     
        
  		
	}

}
