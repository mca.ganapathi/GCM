package com.stta.GCM;

import java.io.IOException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class EditProduct extends SuiteGcmBase{
	
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[]=null;
	static boolean TestCasePass=true;
	static int DataSet=-1;	
	static boolean Testskip=false;
	static boolean Testfail=false;
	
	

	@BeforeTest
	public void checkCaseToRun() throws IOException{
		//Called init() function from SuiteBase class to Initialize .xls Files
		init();			
		//To set SuiteOne.xls file's path In FilePath Variable.
		FilePath = TestCaseListExcelOne;		
		TestCaseName = this.getClass().getSimpleName();	
		//SheetName to check CaseToRun flag against test case.
		SheetName = "TestCasesList";
		//Name of column In TestCasesList Excel sheet.
		ToRunColumnNameTestCase = "CaseToRun";
		//Name of column In Test Case Data sheets.
		ToRunColumnNameTestData = "DataToRun";
		//Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(TestCaseName+" : Execution started.");
		
		//To check test case's CaseToRun = Y or N In related excel sheet.
		//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
		if(!SuiteUtility.checkToRunUtility(FilePath, SheetName,ToRunColumnNameTestCase,TestCaseName)){
			Add_Log.info(TestCaseName+" : CaseToRun = N for So Skipping Execution.");
			//To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
			//To throw skip exception for this test case.
			throw new SkipException(TestCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+TestCaseName);
		}	
		//To retrieve DataToRun flags of all data set lines from related test data sheet.
		TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);
		
		
		System.out.println(TestDataToRun);
	}
	

	
	@Test(dataProvider="EditProductData")
	public void EditProductTest(String Test_CaseId, String ProductName ){
		
	try{
        DataSet++;
        
        
		//If found DataToRun = "N" for data set then execution will be skipped for that data set.
		if(!TestDataToRun[DataSet].equalsIgnoreCase("Y")){	
			Add_Log.info(TestCaseName+" : DataToRun = N for data set line "+(DataSet+1)+" So skipping Its execution.");
			//If DataToRun = "N", Set Testskip=true.
			Testskip=true;
			throw new SkipException("DataToRun for row number "+DataSet+" Is No Or Blank. So Skipping Its Execution.");
		}
		
		
		CreateProduct product2 = new CreateProduct();
		EditProduct edit = new EditProduct();
		
		
		if(Test_CaseId.equalsIgnoreCase("TC_ProductEdit_001")){
			product2.NavigationView(ProductName);
			String status = driver.findElement(By.id("product_status")).getAttribute("value");
			if(status.equalsIgnoreCase("draft")){
				driver.findElement(By.id("edit_button")).click();
				product2.ClearData();
				product2.mandetory();
				driver.findElement(By.id("CANCEL_FOOTER")).click();
				Alert alert = driver.switchTo().alert();
			    alert.accept();
			    Reporter.log("Mandetory check was success");
			}else{
				
				Reporter.log("Mandetory check was failure because product is approved");
				Assert.fail();
			}
			
			
			
		}else if(Test_CaseId.equalsIgnoreCase("TC_ProductEdit_002")){
			
			product2.NavigationView(ProductName);
			
			String status = driver.findElement(By.id("product_status")).getAttribute("value");
			if(status.equalsIgnoreCase("draft")){
				driver.findElement(By.id("edit_button")).click();
				product2.ClearData();
				product2.DataFill(ProductName);
				Reporter.log("Edit was successful");
			}else{
				Reporter.log("The product cannot be edited as its approved");
				Assert.fail();
			}
			
			
		}
		
	
	}catch(Exception e){
		//System.out.println("Exception thrown  :" + e);
		Reporter.log("Exeception Occured "+e);
	
	}
	
	
}
	
	
	
	@DataProvider
	public Object[][] EditProductData(){
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}
	
	
	

}
