package com.stta.GCM;


import java.io.IOException;

import org.apache.commons.logging.impl.Log4JLogger;
import org.apache.xalan.xsltc.compiler.util.ErrorMsg;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;
import org.testng.asserts.SoftAssert;
import org.testng.log4testng.Logger;

import com.stta.TestSuiteBase.SuiteBase;
import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class CreateCorporate extends SuiteGcmBase{
	/***Check to if case is to run***/
	
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[]=null;
	static boolean TestCasePass=true;
	static int DataSet=-1;	
	static boolean Testskip=false;
	static boolean Testfail=false;
	
    //Log4JLogger log = Logger.getLogger(log.class.getName()) ;
	 

	
	@BeforeTest
	public void checkCaseToRun() throws IOException{
		//Called init() function from SuiteBase class to Initialize .xls Files
		init();			
		//To set SuiteOne.xls file's path In FilePath Variable.
		FilePath = TestCaseListExcelOne;		
		TestCaseName = this.getClass().getSimpleName();	
		//SheetName to check CaseToRun flag against test case.
		SheetName = "TestCasesList";
		//Name of column In TestCasesList Excel sheet.
		ToRunColumnNameTestCase = "CaseToRun";
		//Name of column In Test Case Data sheets.
		ToRunColumnNameTestData = "DataToRun";
		//Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(TestCaseName+" : Execution started.");
		
		//To check test case's CaseToRun = Y or N In related excel sheet.
		//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
		if(!SuiteUtility.checkToRunUtility(FilePath, SheetName,ToRunColumnNameTestCase,TestCaseName)){
			Add_Log.info(TestCaseName+" : CaseToRun = N for So Skipping Execution.");
			//To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
			//To throw skip exception for this test case.
			throw new SkipException(TestCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+TestCaseName);
		}	
		//To retrieve DataToRun flags of all data set lines from related test data sheet.
		TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);
		
		
		System.out.println(TestDataToRun);
	}
	
	

	
	
	
	
	
	
	@Test(dataProvider="CreateCorporateData")
	public void CreateCorporateTest(String Test_CaseId,String userName, String password,String Common_Customer_ID,String Corporate_Name_Latin_Character,String Corporate_Name_Local_Character_1,String Corporate_Name_Local_Character_2, String Country, String country_code, String HQ_postNumber, 
			String HQ_Address_Latin,String HQ_Address_Local, String Reg_PostalNumber, String Reg_Address_Latin, String Reg_Address_Local, String VatNumber, String ExpectedResult) throws Exception{
	
		     DataSet++;
             
		     //login1 log = new login1();
		     CreateCorporate CC = new CreateCorporate();
          
		//If found DataToRun = "N" for data set then execution will be skipped for that data set.
		if(!TestDataToRun[DataSet].equalsIgnoreCase("Y")){	
			Add_Log.info(TestCaseName+" : DataToRun = N for data set line "+(DataSet+1)+" So skipping Its execution.");
			//If DataToRun = "N", Set Testskip=true.
			Testskip=true;
			throw new SkipException("DataToRun for row number "+DataSet+" Is No Or Blank. So Skipping Its Execution.");
		}
 
		     
		
		  
            String CCid,TempName;
            int count;
		
         
            
            
            Reporter.log("--------------- corporate Function --------------------");
		
		  if (Test_CaseId.equalsIgnoreCase("TC_Corporate_001")){
			  
			   //CC.createNavigationPop();
			     //log.login(userName, password); 
			     CC.createNavigation();
			  
			     //driver.navigate().refresh();
				 //Alert alert = driver.switchTo().alert();
				// alert.accept();
			    
				// Thread.sleep(10000);
			     //Alert alert1 = driver.switchTo().alert();
			    // alert1.accept();
			 
			  driver.findElement(By.id("common_customer_id_c")).clear();
			  driver.findElement(By.id("common_customer_id_c")).sendKeys(Common_Customer_ID);
			  TempName = driver.findElement(By.id("common_customer_id_c")).getAttribute("value");
			  count = TempName.length();
			  
			  if(count <=12){
				  Reporter.log("-. The Corporate id is less than 12 and the count =" +count);
				  Reporter.log("-. TC_Corporate_001 executed successfuly"); 
			  }else {
				
				  //Reporter.log(TempName);
				  Reporter.log("-. TC_Corporate_001 failed. The Corporate id is more than 12 characters and the count =  "+count);
				  Assert.fail();
			  }  
			  
			  
			  //Alert alert1 = driver.switchTo().alert();
			  //alert1.accept();
			  
			  
			  
		  }else if(Test_CaseId.equalsIgnoreCase("TC_Corporate_002")){
			  
			  //CC.createNavigation();
			  driver.findElement(By.id("name")).clear();
			  driver.findElement(By.id("name")).sendKeys(Corporate_Name_Latin_Character);
			  TempName =driver.findElement(By.id("name")).getAttribute("value");
			  count = TempName.length();
			  if(count <=100){
				  //Reporter.log(TempName);
				  Reporter.log("-.The Corporate_Name_Latin_Character is less than equal to  100 and the count ="+count);
				  Reporter.log("-.TC_Corporate_002 executed successfuly"); 
			  }else {
				  
				  //Reporter.log(TempName);
				  Reporter.log("-.The Corporate_Name_Latin_Character is more than 100 and the count ="+count);
				  Assert.fail();
			  }  
			  
			  
		  }else if(Test_CaseId.equalsIgnoreCase("TC_Corporate_003")){
			   //CC.createNavigation();
			  driver.findElement(By.id("name_2_c")).clear();
			  driver.findElement(By.id("name_2_c")).sendKeys(Corporate_Name_Local_Character_1);
			  TempName=driver.findElement(By.id("name_2_c")).getAttribute("value");
			  count = TempName.length();
			  if(count <=100){
				  //Reporter.log(TempName);
				  Reporter.log("-.The Corporate_Name_Local_Character1 is less than equal to  100 and the count="+count);
				  Reporter.log("-.TC_Corporate_003 executed successfuly"); 
			  }else {
				  
				  //Reporter.log(TempName);
				  Reporter.log("-.The Corporate_Name_Local_Character1 is more than 100 and the count ="+count);
				  Assert.fail();
			  } 
			  
		  }else if(Test_CaseId.equalsIgnoreCase("TC_Corporate_004")){
			  driver.findElement(By.id("name_3_c")).clear();
			  driver.findElement(By.id("name_3_c")).sendKeys(Corporate_Name_Local_Character_2);
			  TempName=driver.findElement(By.id("name_3_c")).getAttribute("value");
			  count = TempName.length();
			  if(count <=100){
				 //Reporter.log(TempName);
				  Reporter.log("-.The Corporate_Name_Local_Character2 is less than equal to  100 and the count="+count);
				  Reporter.log("-.TC_Corporate_004 executed successfuly"); 
			  }else {
				 
				  Reporter.log(TempName);
				  Reporter.log("-.The Corporate_Name_Local_Character2 is more than 100 and the count ="+count);
				  Assert.fail();
			  }
			  
		  }else if(Test_CaseId.equalsIgnoreCase("TC_Corporate_006")){
			  //CountryName
			  
			  driver.findElement(By.id("country_name_c")).clear();
			  driver.findElement(By.id("country_name_c")).sendKeys(Country);
			  TempName=driver.findElement(By.id("country_name_c")).getAttribute("value");
			  count = TempName.length();
			  if(count <=20){
				  //Reporter.log(TempName);
				  Reporter.log("-.The countryName  is less than equal to  20  and the count="+count);
				  Reporter.log("-.TC_Corporate_006 executed successfuly"); 
			  }else {
				  
				  Reporter.log(TempName);
				  Reporter.log("-.The countryName is more than 20 and the count ="+count);
				  Assert.fail();
			  }
			  
			  
			  
			  
		  }else if(Test_CaseId.equalsIgnoreCase("TC_Corporate_007")){
				  //CountryCode
				  
			  driver.findElement(By.id("country_code_c")).clear();
			  driver.findElement(By.id("country_code_c")).sendKeys(country_code);
			  TempName=driver.findElement(By.id("country_code_c")).getAttribute("value");
			  count = TempName.length();
			  if(count <=3){
				  //Reporter.log(TempName);
				  Reporter.log("-.The countrycode  is less than equal to 3 and the count="+count);
				  Reporter.log("-.TC_Corporate_007 executed successfuly"); 
			  }else {
				 
				  Reporter.log(TempName);
				  Reporter.log("-.The countryName is more than 3 and the count ="+count);
				  Assert.fail();
			  }
				  
		  }else if(Test_CaseId.equalsIgnoreCase("TC_Corporate_008")){
				  //HeadQuatersPostalNumber
			  driver.findElement(By.id("hq_postal_no_c")).clear();
			  driver.findElement(By.id("hq_postal_no_c")).sendKeys(HQ_postNumber);
			  TempName=driver.findElement(By.id("hq_postal_no_c")).getAttribute("value");
			  count = TempName.length();
			  if(count <=10){
				  //Reporter.log(TempName);
				  Reporter.log("-.The HeadQuatersPostalNumber  is less than equal to 10 and the count="+count);
				  Reporter.log("-.TC_Corporate_008 executed successfuly"); 
			  }else {
				
				  Reporter.log(TempName);
				  Reporter.log("-.The HeadQuatersPostalNumber is more than 10 and the count ="+count);
				  Assert.fail();
			  }
				  
				  
		  }else if(Test_CaseId.equalsIgnoreCase("TC_Corporate_009")){
				  //HeadQuatersAddressLatinCharacters
			  driver.findElement(By.id("hq_addr_1_c")).clear();
			  driver.findElement(By.id("hq_addr_1_c")).sendKeys(HQ_Address_Latin);
			  TempName=driver.findElement(By.id("hq_addr_1_c")).getAttribute("value");
			  count = TempName.length();
			  if(count <=400){
				  //Reporter.log(TempName);
				  Reporter.log("-.The HeadQuatersAddressLatinCharacters  is less than equal to 400 and the count="+count);
				  Reporter.log("-.TC_Corporate_009 executed successfuly"); 
			  }else {
				
				  Reporter.log(TempName);
				  Reporter.log("-.The HeadQuatersAddressLatinCharacters is more than 400 and the count ="+count);
				  Assert.fail();
			  }
				  
				  
		  }else if(Test_CaseId.equalsIgnoreCase("TC_Corporate_010")){
			  //HeadQuatersAddressLocalCharacters
				  
			  driver.findElement(By.id("hq_addr_2_c")).clear();
			  driver.findElement(By.id("hq_addr_2_c")).sendKeys(HQ_Address_Local);
			  TempName=driver.findElement(By.id("hq_addr_2_c")).getAttribute("value");
			  count = TempName.length();
			  if(count <=400){
				  //Reporter.log(TempName);
				 
				  Reporter.log("-.The HeadQuatersAddressLatinCharacters  is less than equal to 400 and the count="+count);
				  Reporter.log("-.TC_Corporate_010 executed successfuly"); 
			  }else {
				
				  Reporter.log(TempName);
				  Reporter.log("-.The HeadQuatersAddressLatinCharacters is more than 400 and the count ="+count);
				  Assert.fail();
			  } 
				  
		  }else if(Test_CaseId.equalsIgnoreCase("TC_Corporate_011")){
			  //RegisteredPostalAddress
			  driver.findElement(By.id("reg_postal_no_c")).clear();
			  driver.findElement(By.id("reg_postal_no_c")).sendKeys(Reg_PostalNumber);
			  TempName=driver.findElement(By.id("reg_postal_no_c")).getAttribute("value");
			  count = TempName.length();
			  if(count <=10){
				  //Reporter.log(TempName);
				  Reporter.log("-.The Reg_PostalNumber  is less than equal to 10 and the count="+count);
				  Reporter.log("-.TC_Corporate_011 executed successfuly"); 
			  }else {
				  
				  Reporter.log(TempName);
				  Reporter.log("-.The Reg_PostalNumber is more than 10 and the count ="+count);
				  Assert.fail();
			  }  
				  
				  
		  }else if(Test_CaseId.equalsIgnoreCase("TC_Corporate_012")){
				  //RegisteredAddressLatin
			  
			  driver.findElement(By.id("reg_addr_1_c")).clear();
			  driver.findElement(By.id("reg_addr_1_c")).sendKeys(Reg_Address_Latin);
			  TempName=driver.findElement(By.id("reg_addr_1_c")).getAttribute("value");
			  count = TempName.length();
			  if(count <=400){
				  //Reporter.log(TempName);
				  
				  Reporter.log("-.The Reg_Address_Latin  is less than equal to 400 and the count="+count);
				  Reporter.log("-.TC_Corporate_011 executed successfuly"); 
			  }else {
				 
				  Reporter.log(TempName);
				  Reporter.log("-.The Reg_Address_Latin  is more than 400 and the count ="+count);
				  Assert.fail();
			  } 
			  
				  
				  
				  
			  
			  
			  
		  }else if(Test_CaseId.equalsIgnoreCase("TC_Corporate_013")){
			//RegisteredAddressLocal
			  driver.findElement(By.id("reg_addr_2_c")).clear();
			  driver.findElement(By.id("reg_addr_2_c")).sendKeys(Reg_Address_Local);
			  TempName=driver.findElement(By.id("reg_addr_2_c")).getAttribute("value");
			  count = TempName.length();
			  if(count <=400){
				  //Reporter.log(TempName);
				 
				  Reporter.log("-.The Reg_Address_Local  is less than equal to 400 and the count="+count);
				  Reporter.log("-.TC_Corporate_013 executed successfuly"); 
			  }else {
				 
				  Reporter.log(TempName);
				  Reporter.log("-.The Reg_Address_Local is more than 400 and the count ="+count);
				  Assert.fail();
			  }
			  
			  
		  }else if(Test_CaseId.equalsIgnoreCase("TC_Corporate_014")){
					//VatNumber
			  driver.findElement(By.id("reg_addr_2_c")).clear();
			  driver.findElement(By.id("reg_addr_2_c")).sendKeys(Reg_Address_Local);
			  TempName=driver.findElement(By.id("reg_addr_2_c")).getAttribute("value");
			  count = TempName.length();
			  if(count <=32){
				  //Reporter.log(TempName);
				  Reporter.log("-.The Reg_Address_Latin  is less than equal to 400 and the count="+count);
				  Reporter.log("-.TC_Corporate_013 executed successfuly"); 
			  }else {
				  
				  Reporter.log(TempName);
				  Reporter.log("-.The Reg_Address_Latin is more than 400 and the count ="+count);
				  Assert.fail();
			  }	  
				  
			  
		  }else if(Test_CaseId.equalsIgnoreCase("TC_Corporate_005")){
			  
			 
			   //CC.createNavigation();
			  
			   
			   driver.findElement(By.id("save")).click();
           	   String ErrrorMsg = driver.findElement(By.xpath("//table/tbody/tr[1]/td[2]/div")).getText();
          	   Reporter.log(ErrrorMsg);
          	   Reporter.log("-.Mandetory feild varification completed for Corporate Name (Latin Character)");
          	   Alert alert1 = driver.switchTo().alert();
			   alert1.accept();
          	   
		  }else if(Test_CaseId.equalsIgnoreCase("TC_NJ-40_001")){
			  
			  
			driver.findElement(By.id("common_customer_id_c")).clear();
			driver.findElement(By.id("common_customer_id_c")).sendKeys(Common_Customer_ID);
			driver.findElement(By.id("name")).clear();
          	driver.findElement(By.id("name")).sendKeys(Corporate_Name_Latin_Character);
          	driver.findElement(By.id("name_2_c")).clear();
          	driver.findElement(By.id("name_2_c")).sendKeys(Corporate_Name_Local_Character_1);
          	driver.findElement(By.id("name_3_c")).clear();
          	driver.findElement(By.id("name_3_c")).sendKeys(Corporate_Name_Local_Character_2);
          	driver.findElement(By.id("country_name_c")).clear();
          	driver.findElement(By.id("country_name_c")).sendKeys(Country);
          	driver.findElement(By.id("country_code_c")).clear();
          	driver.findElement(By.id("country_code_c")).sendKeys(country_code);
          	driver.findElement(By.id("hq_postal_no_c")).clear();
          	driver.findElement(By.id("hq_postal_no_c")).sendKeys(HQ_postNumber);
          	driver.findElement(By.id("hq_addr_1_c")).clear();
          	driver.findElement(By.id("hq_addr_1_c")).sendKeys(HQ_postNumber);
          	driver.findElement(By.id("hq_addr_2_c")).clear();
          	driver.findElement(By.id("hq_addr_2_c")).sendKeys(HQ_Address_Latin);
          	//driver.findElement(By.id("reg_postal_no_c")).clear();
          	//driver.findElement(By.id("reg_postal_no_c")).sendKeys(HQ_Address_Local);
          	driver.findElement(By.id("reg_addr_1_c")).clear();
          	driver.findElement(By.id("reg_addr_1_c")).sendKeys(Reg_PostalNumber);
          	driver.findElement(By.id("reg_addr_2_c")).clear();
          	driver.findElement(By.id("reg_addr_2_c")).sendKeys(Reg_Address_Latin);
          	driver.findElement(By.id("vat_no_c")).clear();
          	driver.findElement(By.id("vat_no_c")).sendKeys(VatNumber);
          	driver.findElement(By.id("save")).click();
          	 CCid = driver.findElement(By.id("common_customer_id_c")).getText();
          
          	if(CCid.equalsIgnoreCase(Common_Customer_ID)){
          	Reporter.log("-.Common Customer ID is match"+CCid);
          	}else{
          		
        		Reporter.log("-.Common Customer ID did not  match ");
        		Reporter.log("-.Common Customer ID in application"+CCid);
        		Reporter.log("-.Common Customer ID in test data input"+Common_Customer_ID);
        		Assert.fail();
        	
        		}
        	
        
		  }else{
			  
			  Reporter.log("TestCaseid did not match");
			  Assert.fail();
		  }
		
		   
	 
		   
		   driver.close();
		  Reporter.log("--------------- corporate Function --------------------");
		   
            
          
   } 
            
            
        
       
	

	@DataProvider
	public Object[][] CreateCorporateData(){
		//To retrieve data from Data 1 Column,Data 2 Column,Data 3 Column and Expected Result column of SuiteOneCaseOne data Sheet.
		//Last two columns (DataToRun and Pass/Fail/Skip) are Ignored programatically when reading test data.
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}
	
	
	public void  createNavigationPop(){
		 driver.findElement(By.linkText("Corporates")).click();
		 Alert alert = driver.switchTo().alert();
		  alert.accept();
		 
		  WebElement element = driver.findElement(By.linkText("Corporates"));
		 
		  Actions action = new Actions(driver);
		  action.moveToElement(element).build().perform();
          driver.findElement(By.id("CreateCorporateAll")).click();
	}
	
	public void  createNavigation(){
		  WebElement element = driver.findElement(By.linkText("Corporates"));
		  Actions action = new Actions(driver);
		  action.moveToElement(element).build().perform();
          driver.findElement(By.id("CreateCorporateAll")).click();
	}

}
