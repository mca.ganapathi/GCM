package com.stta.GCM;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;

import junit.framework.AssertionFailedError;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;
import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;

public class Workflow extends SuiteGcmBase {

	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[] = null;
	static boolean TestCasePass = true;
	static int DataSet = -1;
	static boolean Testskip = false;
	static boolean Testfail = false;

	// Code by sky

	/****/

	login log = new login();
	login1 logwf = new login1();
	login1 logad = new login1();

	String WF1userName;
	String WF1password;
	String WF5userName;
	String WF5password;
	String WF8userName;
	String WF8password;
	String userName8;
	String password8;
	String AdminUserName;
	String AdminPassword;
	String ContractName;
	String DocumentDescription1;
	String DocumentDescription2;
	String DocumentDescription3;
	String DocumentName;
	String DocumentDescriptionX1;
	String DocumentNameX1;
	String DocumentDescriptionX2;
	String DocumentNameX2;
	String DocumentDescriptionX3;
	String DocumentNameX3;
	String DocumentDescriptionX4;
	String DocumentNameX4;String DocumentDescriptionX5;String DocumentNameX5;String DocumentDescriptionX6;String DocumentNameX6;String DocumentDescriptionX7;String DocumentNameX7;
	String DocumentDescriptionX8;String DocumentNameX8;String DocumentDescriptionX9;String DocumentNameX9;
	String DocumentDescriptionX10;String DocumentNameX10;String DocumentDescriptionX11;String DocumentNameX11;
	String DocumentDescriptionX12; String DocumentNameX12;String DocumentDescriptionX13;String DocumentNameX13;
	String DocumentDescriptionX14;String DocumentNameX14;

	String ContractDesc;
	String ProductsID; String ProductOffering;
	String ContractStartDateTimeZone; String  ContractEndDateTimeZone;
	String FactoryReferenceNumber; 
	String SalesChannelCode;
	String SalesRepNameLatin; String SalesRepNameLocal;
	String DivisionLatin; String DivisionLocal; String TittleLatin;
	String TittleLocal; String Telephone; String ExtnNumber;
	String FaxNumber; String MobileNumber; String mailId;
	String ProductItem; String ProductSpecification;
	String ProductConfiguration; String ProductConfiguration1;
	String ProductConfiguration2; String ProductConfiguration3;
	String ProductConfiguration4; String ProductConfiguration5;
	String ProductConfiguration6; String ProductConfiguration7;
	String PaymentType; String ProductInformation1;
	String ProductInformation2;
	String ServiceStartDateTimeZone; String ServiceEndDateTimeZone;
	String BillingStartDateTimeZone; String BillingEndDateTimeZone;
	
	String globalContractId = null;

	CreateContract CW = new CreateContract();
	EditContract EW = new EditContract();

	@BeforeTest
	public void checkCaseToRun() throws Exception {
		// Called init() function from SuiteBase class to Initialize .xls Files
		System.out.println("Start");
		init();
		System.out.println("init end");

		log.Run();
		// To set SuiteOne.xls file's path In FilePath Variable.
		FilePath = TestCaseListExcelOne;
		TestCaseName = this.getClass().getSimpleName();
		// SheetName to check CaseToRun flag against test case.
		SheetName = "TestCasesList";
		// Name of column In TestCasesList Excel sheet.
		ToRunColumnNameTestCase = "CaseToRun";
		// Name of column In Test Case Data sheets.
		ToRunColumnNameTestData = "DataToRun";
		// Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(TestCaseName + " : Execution started.");

		// To check test case's CaseToRun = Y or N In related excel sheet.
		// If CaseToRun = N or blank, Test case will skip execution. Else It
		// will be executed.
		if (!SuiteUtility.checkToRunUtility(FilePath, SheetName, ToRunColumnNameTestCase, TestCaseName)) {
			Add_Log.info(TestCaseName + " : CaseToRun = N for So Skipping Execution.");
			// To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
			// To throw skip exception for this test case.
			throw new SkipException(
					TestCaseName + "'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of " + TestCaseName);
		}
		// To retrieve DataToRun flags of all data set lines from related test
		// data sheet.
		TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);

		System.out.println(TestDataToRun);
	}

	Screen srn = new Screen();

	// String status =
	// driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]")).getText();
	@Test(dataProvider = "WorkflowData")
	public void WorkflowTest(String Test_CaseId, String ContractId, String ContractName, String ContractDesc,
			String ProductsID, String ProductOffering, String ContractStartDateTimeZone, String ContractEndDateTimeZone,
			String FactoryReferenceNumber, String SalesChannelCode, String SalesRepNameLatin, String SalesRepNameLocal,
			String DivisionLatin, String DivisionLocal, String TittleLatin, String TittleLocal, String Telephone,
			String ExtnNumber, String FaxNumber, String MobileNumber, String mailId, String ProductItem,
			String ProductSpecification, String ProductConfiguration, String ProductConfiguration1,
			String ProductConfiguration2, String ProductConfiguration3, String ProductConfiguration4,
			String ProductConfiguration5, String ProductConfiguration6, String ProductConfiguration7,
			String PaymentType, String chargeType, String chargeperiod, String Currency, String listprice,
			String ContractPrice, String chargeType1, String chargeperiod1, String Currency1, String listprice1,
			String contractprice1, String ProductInformation1, String ProductInformation2, String DocumentDescription1,
			String DocumentDescription2, String DocumentDescription3, String ServiceStartDateTimeZone,
			String ServiceEndDateTimeZone, String BillingStartDateTimeZone, String BillingEndDateTimeZone,
			String DocumentName,String DocumentDescriptionX1, String DocumentNameX1,
			String DocumentDescriptionX2,String DocumentNameX2,String DocumentDescriptionX3, String DocumentNameX3,
			String DocumentDescriptionX4, String DocumentNameX4,String DocumentDescriptionX5, String DocumentNameX5,
			String DocumentDescriptionX6, String DocumentNameX6,String DocumentDescriptionX7, String DocumentNameX7,
			String DocumentDescriptionX8, String DocumentNameX8,String DocumentDescriptionX9, String DocumentNameX9,
			String DocumentDescriptionX10, String DocumentNameX10,String DocumentDescriptionX11, String DocumentNameX11,
			String DocumentDescriptionX12, String DocumentNameX12,String DocumentDescriptionX13, String DocumentNameX13,
			String DocumentDescriptionX14, String DocumentNameX14) throws Throwable {

		DataSet++;

		// If found DataToRun = "N" for data set then execution will be skipped
		// for that data set.
		if (!TestDataToRun[DataSet].equalsIgnoreCase("Y")) {
			Add_Log.info(TestCaseName + " : DataToRun = N for data set line " + (DataSet + 1)
					+ " So skipping Its execution.");
			// If DataToRun = "N", Set Testskip=true.
			Testskip = true;
			throw new SkipException(
					"DataToRun for row number " + DataSet + " Is No Or Blank. So Skipping Its Execution.");
		}

		Workflow wf = new Workflow();

		if (Test_CaseId.equalsIgnoreCase(
				"RTC_Workflow_001/RTC_Workflow_003/RTC_Workflow_008/RTC_Workflow_011/RTC_Workflow_019/RTC_Workflow_020/RTC_Workflow_018/RTC_Workflow_025/RTC_Workflow_028")) {
			try {

				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				// driver.quit();
				CW.ViewContractNavigation();

				EW.ViewContract(ContractName);

				Validation_SendForApproval();
				// Thread.sleep(10000);
				System.out.println("Workflow_001-passed");
				driver.quit();
				init();
				Validation_ApprovalLevel1(ContractName);
				System.out.println("Workflow_003-passed");
				driver.quit();
				init();
				Validation_ApprovalLevel2(ContractName);
				System.out.println("RTC_Workflow_008-passed");
				driver.quit();
				init();
				Validation_ApprovalLevel3(ContractName);
				System.out.println("RTC_Workflow_011-passed");

				driver.quit();
				init();
				VerifyActivateButtonApprover1(ContractName);
				System.out.println("RTC_Workflow_019-passed");

				driver.quit();
				init();
				VerifyActivateButtonApprover2(ContractName);
				System.out.println("RTC_Workflow_020-passed");
				driver.quit();
				init();
				Activate(ContractName);
				System.out.println("RTC_Workflow_018-passed");

				System.out.println("Workflow_001/RTC_Workflow_003/RTC_Workflow_008/RTC_Workflow_011/RTC_Workflow_019/RTC_Workflow_020/RTC_Workflow_018-passed");
				Reporter.log("Workflow_001/RTC_Workflow_003/RTC_Workflow_008/RTC_Workflow_011/RTC_Workflow_019/RTC_Workflow_020/RTC_Workflow_018-passed");

			}

			catch (Exception e) {

				e.printStackTrace();

				System.out.println("Workflow_001/RTC_Workflow_003/RTC_Workflow_008/RTC_Workflow_011/RTC_Workflow_019/RTC_Workflow_020/RTC_Workflow_018-failed");
				Reporter.log("Workflow_001/RTC_Workflow_003/RTC_Workflow_008/RTC_Workflow_011/RTC_Workflow_019/RTC_Workflow_020/RTC_Workflow_018-failed");
			}

		}

		if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_004")) {
			try {

				CW.CreateContractNavigation();

		

				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber,
						SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
						DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1,
						ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5,
						ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2,						
						ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::"+globalContractId);	
				// driver.quit();
				// init();
				CW.ViewContractNavigation();

				EW.ViewContract(ContractName);

				Validation_SendForApproval();
				Thread.sleep(10000);
				driver.quit();
				init();
				Validation_RejectLevel1(ContractName);

				System.out.println("Workflow_004/RTC_Workflow_004-passed");
				Reporter.log("Workflow_004/RTC_Workflow_004-passed");

			}

			catch (Exception e) {

				e.printStackTrace();
			}

		}
		
		/*Added by net - Start*/
		if(Test_CaseId.equalsIgnoreCase("RTC_Workflow_032")){
			
			try{
				
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber,
						SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
						DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1,
						ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5,
						ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2,
						ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Global Contract Id::"+globalContractId);
				
				
				EW.CreateDocumentInDraft(DocumentDescription1, DocumentDescription3);
				System.out.println("New document has been created in Draft under Contract Header");
				
				EW.CreateDocumentModuleLIWF(DocumentDescription3);
				System.out.println("New document has been created in Draft under Line items");
				
				CW.ViewContractNavigation();
				CW.SelectContractId(globalContractId);
				//Validation_SendForApproval();
				
				
				//EW.ContractStatus();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				System.out.println("Check for Remove button under Contract Header section");
				
				if(driver.findElements(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[8]/ul/li/a")).size() != 0){
					System.out.println("Remove button will not be visible and Document cannot be Removed from document module under contract header section(Sent For Approval)!!!");
					Assert.assertTrue(TestCasePass, "RTC_Workflow_032 passed");
				}else{
					System.out.println("RTC_Workflow_032 failed");
					Assert.fail("RTC_Workflow_032 failed");
				}
				
				System.out.println("Check for Remove button under Line Item section");
				
				//Click Open All Button
				
				driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
				 Thread.sleep(2000);
				if(driver.findElements(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[8]/ul/li/a")).size() != 0){
					System.out.println("Remove button will not be visible and Document cannot be removed from document module under line item section(Sent For Approval)!!!");
					Assert.assertTrue(TestCasePass, "RTC_Workflow_032 passed");
				}else{
					System.out.println("RTC_Workflow_032 failed");
					Assert.fail("The Remove button should not be visible and it shows up now, so RTC_Workflow_032 failed");
				}
				
			}
			
			catch(Exception e){
				e.printStackTrace();
				Assert.fail("RTC_Workflow_032 failed");
			}
			
		}
		
		if(Test_CaseId.equalsIgnoreCase("RTC_Workflow_033")){
			
			try{
				
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber,
						SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
						DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1,
						ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5,
						ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2,
						ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Global Contract Id::"+globalContractId);
				
				
				EW.CreateDocumentInDraft(DocumentDescription1, DocumentDescription3);
				System.out.println("New document has been created in Draft under Contract Header");
				
				EW.CreateDocumentModuleLIWF(DocumentDescription3);
				System.out.println("New document has been created in Draft under Line items");
				
				CW.ViewContractNavigation();
				
				CW.SelectContractId(globalContractId);

				// Thread.sleep(10000);
				System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				System.out.println("Approval Level 1 passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				System.out.println("Approval Level 2 passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				System.out.println("Approval Level 3 passed");
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				
				driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr[1]/td[8]/ul/li/a")).click();
				
				// Get a handle to the open alert, prompt or confirmation
				Alert alert = driver.switchTo().alert();
				
				// Capturing alert message.
				String alertMessage = driver.switchTo().alert().getText();

				// Displaying alert message
				System.out.println(alertMessage);
				
				//Will Click on OK button.
				alert.accept();
				
				System.out.println("Document removed from Contract Header documents");
				
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				
				driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[8]/ul/li/a")).click();
				
				// Get a handle to the open alert, prompt or confirmation
				Alert alertLi = driver.switchTo().alert();
				
				// Capturing alert message.
				String alertMessageLine = driver.switchTo().alert().getText();

				// Displaying alert message
				System.out.println(alertMessageLine);
				
				//Will Click on OK button.
				alertLi.accept();
				
				System.out.println("Document removed from Line Item Details");
				
				Assert.assertTrue(TestCasePass, "RTC_Workflow_033 passed");
			}
			
			catch(Exception e){
				e.printStackTrace();
				Assert.fail("RTC_Workflow_033 failed");
			}
		}
		
		if(Test_CaseId.equalsIgnoreCase("RTC_Workflow_034")){
			
			try{
				
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber,
						SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
						DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1,
						ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5,
						ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2,
						ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Global Contract Id::"+globalContractId);
				
				
				EW.CreateDocumentInDraft(DocumentDescription1, DocumentDescription3);
				System.out.println("New document has been created in Draft under Contract Header");
				
				EW.CreateDocumentModuleLIWF(DocumentDescription3);
				System.out.println("New document has been created in Draft under Line items");
				
				CW.ViewContractNavigation();
				CW.SelectContractId(globalContractId);

				// Thread.sleep(10000);
				System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				System.out.println("Approval Level 1 passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				System.out.println("Approval Level 2 passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				System.out.println("Approval Level 3 passed");
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("activate_contract")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='yui-gen0-button']")).click();
				driver.switchTo().window(winHandleBefore);
				Thread.sleep(3000);
				
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				System.out.println("Check for Remove button under Contract Header section");
				
				if(driver.findElements(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[8]/ul/li/a")).size() != 0){
				    //THEN CLICK ON THE SUBMIT BUTTON
					System.out.println("Remove button will not be visible and Document cannot be Removed from document module under contract header section(Sent For Approval)!!!");
				}else{
				    //DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
					System.out.println("RTC_Workflow_034 failed");
					Assert.fail("The Remove button should not be visible and it shows up now, so RTC_Workflow_034 failed");
				}
				
				System.out.println("Check for Remove button under Line Item section");
				
				//Click Open All Button
				
				driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
				 Thread.sleep(3000);
				if(driver.findElements(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[8]/ul/li/a")).size() != 0){
				    //THEN CLICK ON THE SUBMIT BUTTON
					System.out.println("Remove button will not be visible and Document cannot be removed from document module under line item section(Sent For Approval)!!!");
					Assert.assertTrue(TestCasePass, "RTC_Workflow_034 passed");
				}else{
				    //DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
					System.out.println("RTC_Workflow_034 failed");
					Assert.fail("The Remove button should not be visible and it shows up now, so RTC_Workflow_034 failed");
					Assert.fail("RTC_Workflow_034 failed");
				}
			}
			
			catch(Exception e){
				e.printStackTrace();
				Assert.fail("RTC_Workflow_034 failed");
			}
		}
		
		if(Test_CaseId.equalsIgnoreCase("RTC_Workflow_021")){
			
			try{
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber,
						SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
						DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1,
						ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5,
						ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2,
						ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Global Contract Id::"+globalContractId);
				
				CW.ViewContractNavigation();
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
//				System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
//				System.out.println("Approval Level 1 passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
//				System.out.println("Approval Level 2 passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
//				System.out.println("Approval Level 3 passed");
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				//driver.findElement(By.id("activate_contract")).click();
				driver.findElement(By.xpath(".//*[@id='activate_contract']")).click();
				
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='yui-gen0-button']")).click();
				driver.switchTo().window(winHandleBefore);
				Thread.sleep(5000);
				
				System.out.println("Check for Edit button in details page after activated in Workflow Level 3 user");
				
				if(driver.findElements(By.xpath(".//*[@id='edit_button']")).isEmpty()){
					System.out.println("Edit button will not be visible after contract gets Activated");
					System.out.println("RTC_Workflow_021 has passed");
				}else{
					System.out.println("RTC_Workflow_021 failed");
					Assert.fail("The Edit button should not be visible and it shows up now, so RTC_Workflow_021 failed");
					Assert.fail("RTC_Workflow_021 failed");
				}
				logOut();
				driver.quit();
				
				System.out.println("Check for Edit button in details page after activated in Workflow Level 2 user");
				init();
				VerifyDetailsEditButtonApprover2(globalContractId);
				logOut();
				driver.quit();
				
				System.out.println("Check for Edit button in details page after activated in Workflow Level 1 user");
				init();
				VerifyDetailsEditButtonApprover1(globalContractId);
				logOut();
				driver.quit();
				
				System.out.println("Check for Edit button in details page after activated in Normal user");
				init();
				log.Run();
				CW.ViewContractNavigation();
				CW.SelectContractId(globalContractId);
				if(driver.findElements(By.xpath(".//*[@id='edit_button']")).isEmpty()){
					System.out.println("Edit button will not be visible after contract gets Activated");
					System.out.println("RTC_Workflow_021 has passed");
					Assert.assertTrue(TestCasePass, "RTC_Workflow_021 has passed");
				}else{
					System.out.println("RTC_Workflow_021 failed");
					Assert.fail("The Edit button should not be visible and it shows up now, so RTC_Workflow_021 failed");
					Assert.fail("RTC_Workflow_021 failed");
				}
				
			}
			
			catch (Exception e){
				e.printStackTrace();
				Assert.fail("RTC_Workflow_021 failed");
			}
		}

		if (Test_CaseId.equalsIgnoreCase("RTC_Contract_104/RTC_Contract_105/RTC_Contract_106")){
			
			try{
				
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber,
						SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
						DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1,
						ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5,
						ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2,
						ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Global Contract Id::"+globalContractId);
				
				CW.ViewContractNavigation();
				CW.SelectContractId(globalContractId);
				
				Validation_SendForApproval();
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[2]")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}

				String accept="Accept";
				System.out.println("Approval comment while updating in Comments Dialogue :"+accept);
				Thread.sleep(3000);
				
				String approvalComment = driver.findElement(By.xpath("html/body/div/table/tbody/tr[2]/td[5]")).getText();
				System.out.println("Approval comment after updating in Approvals Log :"+approvalComment);
				
				driver.switchTo().window(winHandleBefore);
				
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				
				if (approvalComment.equalsIgnoreCase(accept)) {
					System.out.println("Approval comment has been updated in Approvals Log");
					Reporter.log("Approval comment for Contract sent for approval");
					Assert.assertTrue(TestCasePass, "RTC_Contract_104 passed");

				} else {
					Reporter.log("Error while updating approval comment in Approvals Log" + approvalComment);
					org.testng.Assert.fail();
					Assert.fail("RTC_Contract_104 failed");
				}
				
				String wftd1 = driver.findElement(By.xpath("html/body/div/table/tbody/tr[1]/td[1]")).getText();
				String usertd2 = driver.findElement(By.xpath("html/body/div/table/tbody/tr[1]/td[2]")).getText();
				String teamtd3 = driver.findElement(By.xpath("html/body/div/table/tbody/tr[1]/td[3]")).getText();
				String datetd4 = driver.findElement(By.xpath("html/body/div/table/tbody/tr[1]/td[4]")).getText();
				String commenttd5 = driver.findElement(By.xpath("html/body/div/table/tbody/tr[1]/td[5]")).getText();
				
				System.out.println("The columns in the Approvals Log are "+wftd1+","+usertd2+","+teamtd3+","+datetd4+","+commenttd5);
				
				if (wftd1.equalsIgnoreCase("Workflow Action") == usertd2.equalsIgnoreCase("User")
						== teamtd3.equalsIgnoreCase("Team") == datetd4.equalsIgnoreCase("Date time")
						 == commenttd5.equalsIgnoreCase("Comment")) {

					System.out.println("The five columns are present in the Approvals Log");
					Reporter.log("Columns are present in log");
					Assert.assertTrue(TestCasePass, "RTC_Contract_105 passed");

				} else {
					org.testng.Assert.fail();
					Assert.fail("RTC_Contract_105 failed");
				}
				
				Validation_WithdrawNormalUser();
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[4]")).click();
				
				String withdraw="Withdraw";
				System.out.println("Withdrawal comment while updating in Comments Dialogue :"+accept);
				String withdrawalComment = driver.findElement(By.xpath("html/body/div/table/tbody/tr[2]/td[5]")).getText();
				System.out.println("Withdrawal comment after updating in Approvals Log :"+withdrawalComment);
				
				if (withdrawalComment.equalsIgnoreCase(withdraw)) {
					System.out.println("Withdrawal comment has been updated in Approvals Log");
					Reporter.log("Withdrawal comment for Contract sent for approval");
					Assert.assertTrue(TestCasePass, "RTC_Contract_106 passed");

				} else {
					Reporter.log("Error while updating Withdrawal comment in Approvals Log" + approvalComment);
					org.testng.Assert.fail();
					Assert.fail("RTC_Contract_106 failed");
				}
			}
			
			catch(Exception e){
				e.printStackTrace();
				Assert.fail("RTC_Contract_104/RTC_Contract_105/RTC_Contract_106 failed");
			}
		}
		
		if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_013")) {
			try{
				
				System.out.println("Normal user logged in successfully");
				driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[1]/span/button")).click();
				System.out.println("Logo clicked");
				
				driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[1]/span/div/ul/li[7]")).click();
				System.out.println("Settings changed");
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				/*String contractsTab1 = driver.findElement(By.xpath(".//*[@id='dashlet_header_93f21a48-0b23-4fb4-a92f-e9732b30a8f1']/div/table/tbody/tr/td[1]/h3/span")).getText();
				System.out.println("Dashboard has : "+contractsTab1);*/
				
				logOut();
				driver.quit();
				init();
				
				Thread.sleep(3000);
				
				System.out.println("Logged out and initiated");
				logwf.WFLevel1login(WF1userName, WF1password);
				System.out.println("Approver level 1 logged in successfully");
				driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[1]/span/button")).click();
				System.out.println("Logo clicked");
				
				driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[1]/span/div/ul/li[7]")).click();
				System.out.println("Settings changed");
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				
				String contracts = driver.findElement(By.xpath(".//*[@id='dashlet_entire_93f21a48-0b23-4fb4-a92f-e9732b30a8f1']/div[2]/div/table/tbody/tr[2]/th[1]/div/a")).getText();
				System.out.println("Dashboard ID has : "+contracts);
				String contractsTab2 = driver.findElement(By.xpath(".//*[@id='dashlet_header_93f21a48-0b23-4fb4-a92f-e9732b30a8f1']/div/table/tbody/tr/td[1]/h3/span")).getText();
				System.out.println("Dashboard has : "+contractsTab2);
				if (contractsTab2.equalsIgnoreCase("My Contracts")) {
					System.out.println("My Contracts tab is present in the dashboard.");
					Reporter.log("My Contracts tab is present in the dashboard.");
					Assert.assertTrue(TestCasePass, "RTC_Workflow_013 passed");

				} else {
					Reporter.log("My Contracts tab is not present in the dashboard.");
					org.testng.Assert.fail();
					Assert.fail("RTC_Workflow_013 failed");
				}
				
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
		
		if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_023")) {
			try{
				
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber,
						SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
						DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1,
						ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5,
						ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2,
						ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Global Contract Id::"+globalContractId);
				
				CW.ViewContractNavigation();
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				
/*				logOut();
				driver.quit();
				init();
				
				Thread.sleep(3000);
				
				System.out.println("Logged out and initiated");
				logwf.WFLevel3login(WF8userName, WF8password);
				System.out.println("Approver level 3 logged in successfully");
				
				CW.ViewContractNavigation();
				CW.SelectContract("Automation_Contractware_2");
				//CW.SelectContractId("920000000005202");
				
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));*/
				
				
				driver.findElement(By.id("edit_button")).click();
				CW.ClearData(PaymentType);
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone, ContractEndDateTimeZone,
						FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin,
						DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem, ProductConfiguration,
						ProductConfiguration1, ProductConfiguration2,
						ProductConfiguration3, ProductConfiguration4,
						ProductConfiguration5, ProductConfiguration6,
						ProductConfiguration7, PaymentType, ProductInformation1,
						ProductInformation2, ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Assert.assertTrue(TestCasePass, "RTC_Workflow_023 passed");
			}
			catch (Exception e){
				e.printStackTrace();
				Assert.fail("RTC_Workflow_023 failed");
			}
		}
		
		if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_047/RTC_Workflow_046")) {
			try{
				
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber,
						SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
						DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1,
						ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5,
						ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2,
						ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Global Contract Id::"+globalContractId);
				
				CW.ViewContractNavigation();
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				//driver.findElement(By.id("activate_contract")).click();
				driver.findElement(By.xpath(".//*[@id='activate_contract']")).click();
				
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='yui-gen0-button']")).click();
				driver.switchTo().window(winHandleBefore);
				Thread.sleep(5000);
				
				logOut();
				driver.quit();
				init();
				Validation_SendForTApproval(globalContractId);
				
				logOut();
				driver.quit();
				init();
				Validation_SendForTApprovalLevel1(globalContractId);
				
				logOut();
				driver.quit();
				init();
				Validation_SendForTApprovalLevel2(globalContractId);
				
				logOut();
				driver.quit();
				init();
				Validation_SendForTApprovalLevel3(globalContractId);
				
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("edit_button")).click();
				
				contractDataFillTermination(ContractName, ContractDesc, ContractEndDateTimeZone, 
						ServiceEndDateTimeZone, BillingEndDateTimeZone);
				
				driver.findElement(By.id("SAVE_HEADER")).click();
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				
				if(driver.findElements(By.id("edit_button")).size() != 0){
					System.out.println("RTC_Workflow_046 passed");
					Assert.assertTrue(TestCasePass, "RTC_Workflow_046 passed");
				}
				else {
					System.out.println("RTC_Workflow_046 failed");
					Assert.fail("RTC_Workflow_046 failed");
				}
				
				
				DeActivateEditWFAdmin();
				
				if (driver.findElements(By.id("deactivate")).size() == 0){
					System.out.println("RTC_Workflow_047 passed");
					Assert.assertTrue(TestCasePass, "RTC_Workflow_047 passed");
				}
				else {
					System.out.println("RTC_Workflow_047 failed");
				}
				
			}
			catch (Exception e){
				e.printStackTrace();
				Assert.fail("RTC_Workflow_047 failed");
			}
		}
		
		
		if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_053")){
			try{
				
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber,
						SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
						DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1,
						ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5,
						ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2,
						ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Global Contract Id::"+globalContractId);
				
				CW.ViewContractNavigation();
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				//driver.findElement(By.id("activate_contract")).click();
				driver.findElement(By.xpath(".//*[@id='activate_contract']")).click();
				
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='yui-gen0-button']")).click();
				driver.switchTo().window(winHandleBefore);
				Thread.sleep(5000);
				
				logOut();
				driver.quit();
				init();
				Validation_SendForTApproval(globalContractId);
				
				logwf.WFLevel1login(WF1userName, WF1password);
				Thread.sleep(5000);
				driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[1]/span/button")).click();
				Thread.sleep(3000);
				driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[1]/span/div/ul/li[7]/a")).click();
				Thread.sleep(3000);
				
				CW.ViewContractNavigationWorkflow();
				CW.SelectContractId(ContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				if(driver.findElements(By.id("rejectContract")).size() != 0){
					driver.findElement(By.id("rejectContract")).click();
					String windowHandleBefore = driver.getWindowHandle();				
					for (String handle : driver.getWindowHandles()) {
						driver.switchTo().window(handle);
					}
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					driver.findElement(By.id("yui-gen1-button")).click();
					if(driver.findElements(By.xpath(".//*[@id='errorMsgSpan']")).size() != 0){
						System.out.println("Comments for reject is mandatory check passed");
						Reporter.log("Comments for reject is mandatory check passed");
					} else {
						System.out.println("Comments for reject is mandatory check failed");
						Reporter.log("Comments for reject is mandatory check failed");
						Assert.fail();
					}
					driver.switchTo().window(windowHandleBefore);
					Assert.assertTrue(TestCasePass, "RTC_Workflow_053 passed");
				} else {
					System.out.println("Reject button not visible hence Comments for reject is mandatory check failed");
					Reporter.log("Reject button not visible hence Comments for reject is mandatory check failed");
					Assert.fail();
				}
			}
			catch (Exception e){
				e.printStackTrace();
				Assert.fail("RTC_Workflow_053 failed");
			}
		}
		
		
		if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_039")){
			try{
				
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber,
						SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
						DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1,
						ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5,
						ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2,
						ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Global Contract Id::"+globalContractId);
				
				CW.ViewContractNavigation();
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				//driver.findElement(By.id("activate_contract")).click();
				driver.findElement(By.xpath(".//*[@id='activate_contract']")).click();
				
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='yui-gen0-button']")).click();
				driver.switchTo().window(winHandleBefore);
				Thread.sleep(5000);
				
				logOut();
				driver.quit();
				init();
				Validation_SendForTApproval(globalContractId);
				
				logOut();
				driver.quit();
				init();
				
				
			}
			catch (Exception e){
				e.printStackTrace();
				Assert.fail("RTC_Workflow_039 failed");
			}
			
		}
		
		
		if (Test_CaseId.equalsIgnoreCase("RTC_Contract_069")) {
			try{
				logOut();
				driver.quit();
				init();
				
				Thread.sleep(3000);
				
				System.out.println("Logged out and initiated");
				logwf.user8login(userName8, password8);
				
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber,
						SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
						DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1,
						ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5,
						ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2,
						ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Global Contract Id::"+globalContractId);
				
				logOut();
				driver.quit();
				init();
				
				Thread.sleep(3000);
				log.Run();
				
				CW.ViewContractNavigation();
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				
				Assert.assertTrue(TestCasePass, "RTC_Contract_069 passed");
				
				/*logOut();
				driver.quit();
				init();
				logwf.Adminlogin(AdminUserName, AdminPassword);
				
				CW.ViewContractNavigation();
				CW.SelectContractId(globalContractId);
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("edit_button")).click();
				Thread.sleep(3000);
				driver.findElement(By.id("SAVE_HEADER")).isDisplayed();
				
				logwf.WFLevel1loginNew(WF1userName, WF1password);
				Thread.sleep(3000);
				Validation_SendForApproval();*/
				
			}
			catch (Exception e){
				e.printStackTrace();
				Assert.fail("RTC_Contract_069 failed");
			}
		}
		
		if (Test_CaseId.equalsIgnoreCase("RTC_Contract_071")){
			try{
				
			}
			catch (Exception e){
				e.printStackTrace();
				Assert.fail("RTC_Contract_071 failed");
			}
		}
		
		/*Added by net - End*/
		
		if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_009")) {
			try {

				CW.CreateContractNavigation();

			

				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber,
						SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
						DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1,
						ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5,
						ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2,						
						ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				// driver.quit();
				// init();

				CW.ViewContractNavigation();

				EW.ViewContract(ContractName);

				Validation_SendForApproval();
				Thread.sleep(10000);

				driver.quit();
				init();
				Validation_ApprovalLevel1(ContractName);

				driver.quit();
				init();
				Validation_RejectLevel2(ContractName);

				System.out.println("Workflow_009/RTC_Workflow_009-passed");
				Reporter.log("Workflow_009/RTC_Workflow_009-passed");

			}

			catch (Exception e) {

				e.printStackTrace();
			}
		}

		if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_012")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID,
						ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber,
						SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
						DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
						ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1,
						ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5,
						ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2,						
						ServiceStartDateTimeZone, ServiceEndDateTimeZone,
						BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				/* Modified by deep - start */
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Global Contract Id::" + globalContractId);
				// driver.quit();
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				// Thread.sleep(10000);
				System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				System.out.println("Approval Level1 passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				System.out.println("Approval Level2 passed");
				logOut();
				driver.quit();
				init();
				Validation_RejectLevel3(globalContractId);
				System.out.println("RTC_Workflow_012-passed");
				Reporter.log("RTC_Workflow_012-passed");
				/* Modified by deep - end */
			}

			catch (Exception e) {

				e.printStackTrace();
			}

		}

		/* Added by Deep - Start */
		else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_005/RTC_Workflow_014/RTC_Workflow_015")) {
			try {
				logwf.WFLevel1login(WF1userName, WF1password);
				Thread.sleep(10000);
				driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[1]/span/button")).click();
				Thread.sleep(3000);
				driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[1]/span/div/ul/li[7]/a"))
						.click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String value1 = driver
						.findElement(By
								.xpath(".//*[@id='dashlet_entire_93f21a48-0b23-4fb4-a92f-e9732b30a8f1']/div[2]/div/table/tbody/tr[2]/th[1]/div/a"))
						.getText();
				String value2 = driver
						.findElement(By
								.xpath(".//*[@id='dashlet_entire_93f21a48-0b23-4fb4-a92f-e9732b30a8f1']/div[2]/div/table/tbody/tr[2]/th[2]/div/a"))
						.getText();
				String value3 = driver
						.findElement(By
								.xpath(".//*[@id='dashlet_entire_93f21a48-0b23-4fb4-a92f-e9732b30a8f1']/div[2]/div/table/tbody/tr[2]/th[4]/div/a"))
						.getText();
				String value4 = driver
						.findElement(By
								.xpath(".//*[@id='dashlet_entire_93f21a48-0b23-4fb4-a92f-e9732b30a8f1']/div[2]/div/table/tbody/tr[2]/th[5]/div/a"))
						.getText();
				if (value1.equals("Global Contract ID")) {
					System.out.println("Global Contract ID filed is available in My Contracts screen");
					Reporter.log("Global Contract ID filed is available in My Contracts screen");
				} else {
					System.out.println("Global Contract ID filed is not available in My Contracts screen");
					Reporter.log("Global Contract ID filed is not available in My Contracts screen");
					Assert.fail();
				}
				if (value2.equals("Name")) {
					System.out.println("Name filed is available in My Contracts screen");
					Reporter.log("Name filed is available in My Contracts screen");
				} else {
					System.out.println("Name filed is not available in My Contracts screen");
					Reporter.log("Name filed is not available in My Contracts screen");
					Assert.fail();
				}
				if (value3.equals("Product Offering")) {
					System.out.println("Product Offering filed is available in My Contracts screen");
					Reporter.log("Product Offering filed is available in My Contracts screen");
				} else {
					System.out.println("Product Offering filed is not available in My Contracts screen");
					Reporter.log("Product Offering filed is not available in My Contracts screen");
					Assert.fail();
				}
				if (value4.equals("Contracting Customer Company")) {
					System.out.println("Contracting Customer Company filed is available in My Contracts screen");
					Reporter.log("Contracting Customer Company filed is available in My Contracts screen");
				} else {
					System.out.println("Contracting Customer Company filed is not available in My Contracts screen");
					Reporter.log("Contracting Customer Company filed is not available in My Contracts screen");
				}
				if (driver
						.findElements(By
								.xpath(".//*[@id='dashlet_entire_93f21a48-0b23-4fb4-a92f-e9732b30a8f1']/div[2]/div/table/tbody/tr[3]/td[2]/b/a"))
						.size() != 0) {
					System.out.println(
							"Contracts pending for approval should be displayed  in My Contracts Screen passed");
					Reporter.log("Contracts pending for approval should be displayed  in My Contracts Screen passed");
				} else {
					System.out.println(
							"Contracts pending for approval should be displayed  in My Contracts Screen failed");
					Reporter.log("Contracts pending for approval should be displayed  in My Contracts Screen failed");
					Assert.fail();
				}
				// Thread.sleep(3000);
				CW.ViewContractNavigation();
				CW.SelectContractId(ContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				if (driver.findElements(By.id("rejectContract")).size() != 0) {
					driver.findElement(By.id("rejectContract")).click();
					String windowHandleBefore = driver.getWindowHandle();
					for (String handle : driver.getWindowHandles()) {
						driver.switchTo().window(handle);
					}
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					driver.findElement(By.id("yui-gen1-button")).click();
					if (driver.findElements(By.xpath(".//*[@id='errorMsgSpan']")).size() != 0) {
						System.out.println("Comments for reject is mandatory check passed");
						Reporter.log("Comments for reject is mandatory check passed");
					} else {
						System.out.println("Comments for reject is mandatory check failed");
						Reporter.log("Comments for reject is mandatory check failed");
						Assert.fail();
					}
					driver.switchTo().window(windowHandleBefore);
				} else {
					System.out.println("Reject button not visible hence Comments for reject is mandatory check failed");
					Reporter.log("Reject button not visible hence Comments for reject is mandatory check failed");
					Assert.fail();
				}
			}

			catch (Exception e) {

				e.printStackTrace();
			}

		} else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_017")) {
			try {
				logwf.WFLevel1login(WF1userName, WF1password);
				Thread.sleep(10000);
				driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[1]/span/button")).click();
				Thread.sleep(3000);
				driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[1]/span/div/ul/li[7]/a"))
						.click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String contractId = driver
						.findElement(By
								.xpath(".//*[@id='dashlet_entire_93f21a48-0b23-4fb4-a92f-e9732b30a8f1']/div[2]/div/table/tbody/tr[3]/td[1]"))
						.getText();
				System.out.println(contractId);
				driver.findElement(By
						.xpath(".//*[@id='dashlet_entire_93f21a48-0b23-4fb4-a92f-e9732b30a8f1']/div[2]/div/table/tbody/tr[3]/td[2]/b/a"))
						.click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sendforapproval")).click();
				String windowHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Automation Test");
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(windowHandleBefore);
				Thread.sleep(5000);
				driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[1]/span/button")).click();
				Thread.sleep(3000);
				driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[1]/span/div/ul/li[7]/a"))
						.click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				if (!driver.getPageSource().contains(contractId)) {
					System.out.println(
							"Contracts appropved by Approver1/Approver2/Approver3 are not dipslayed in their respective My Contracts screens");
					Reporter.log(
							"Contracts appropved by Approver1/Approver2/Approver3 are not dipslayed in their respective My Contracts screens");
				}

				else {
					System.out.println(
							"Contracts appropved by Approver1/Approver2/Approver3 are dipslayed in their respective My Contracts screens");
					Reporter.log(
							"Contracts appropved by Approver1/Approver2/Approver3 are dipslayed in their respective My Contracts screens");
					Assert.fail();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_022")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				// driver.quit();
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				// Thread.sleep(10000);
				System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				System.out.println("Approval Level1 passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				System.out.println("Approval Level2 passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				System.out.println("Approval Level3 passed");
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				if (driver.findElements(By.id("edit_button")).size() != 0) {
					driver.findElement(By.id("edit_button")).click();
					Thread.sleep(5000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					Select contractStatus = new Select(driver.findElement(By.id("contract_status")));
					String contractStatusValue = contractStatus.getFirstSelectedOption().getText();
					if (contractStatusValue.equals("Approved")) {
						System.out.println(
								"Contract status as Approved in edit screen when third level approver approves it passed");
						Reporter.log(
								"Contract status as Approved in edit screen when third level approver approves it passed");
					} else {
						System.out.println(
								"Contract status as Approved in edit screen when third level approver approves it failed");
						Reporter.log(
								"Contract status as Approved in edit screen when third level approver approves it failed");
						Assert.fail();
					}
					Select lineItemType = new Select(driver.findElement(By.id("contract_line_item_type_1")));
					String lineItemTypeValue = lineItemType.getFirstSelectedOption().getText();
					if (lineItemTypeValue.equals("New")) {
						System.out.println(
								"Contract Line Item Type as New in edit screen when third level approver approves it passed");
						Reporter.log(
								"Contract Line Item Type as New in edit screen when third level approver approves it passed");
					} else {
						System.out.println(
								"Contract Line Item Type as New in edit screen when third level approver approves it failed");
						Reporter.log(
								"Contract Line Item Type as New in edit screen when third level approver approves it failed");
						Assert.fail();
					}
				} else {
					System.out.println(
							"Edit button not visible hence presence of contract status as Approved in edit screen when third level approver approves it failed");
					Reporter.log(
							"Edit button not visible hence presence of contract status as Approved in edit screen when third level approver approves it failed");
					Assert.fail();
				}
			}

			catch (Exception e) {

				e.printStackTrace();
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_035")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				Thread.sleep(10000);
				CW.ViewContractNavigation();
				Thread.sleep(5000);
				searchContract(globalContractId);
				System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				CW.ViewContractNavigation();
				Thread.sleep(5000);
				searchContract(globalContractId);
				System.out.println("Contract is visible for Approver Level1");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				CW.ViewContractNavigation();
				Thread.sleep(5000);
				searchContract(globalContractId);
				System.out.println("Contract is visible for Approver Level2");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				CW.CreateContractNavigation();
				Thread.sleep(5000);
				searchContract(globalContractId);
				System.out.println("Contract is visible for Approver Level3");
			}

			catch (Exception e) {

				e.printStackTrace();
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_036")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				// Thread.sleep(10000);
				System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_RejectLevel1(globalContractId);
				CW.ViewContractNavigation();
				Thread.sleep(5000);
				searchContract(globalContractId);
				System.out.println("Rejected Contract is visible for Approver Level1");
				logOut();
				driver.quit();
				init();
				logwf.WFLevel2login(WF5userName, WF5password);
				Thread.sleep(3000);
				CW.ViewContractNavigation();
				Thread.sleep(5000);
				searchContract("920000000004825");
				logOut();
				driver.quit();
				init();
				logwf.WFLevel3login(WF8userName, WF8password);
				Thread.sleep(3000);
				CW.ViewContractNavigation();
				Thread.sleep(5000);
				searchContract("920000000004825");
				System.out.println(
						"Contract is visible to all the Approvers when it gets rejected by any of the Approver(1st,2nd or 3rd) passed");
				Reporter.log(
						"Contract is visible to all the Approvers when it gets rejected by any of the Approver(1st,2nd or 3rd) passed");
			} catch (Exception e) {
				System.out.println(
						"Contract is visible to all the Approvers when it gets rejected by any of the Approver(1st,2nd or 3rd) failed"
								+ e);
				Reporter.log(
						"Contract is visible to all the Approvers when it gets rejected by any of the Approver(1st,2nd or 3rd) failed");
				Assert.fail();
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_037")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				// Thread.sleep(10000);
				System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[2]")).click();
				Thread.sleep(10000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				if (driver.getPageSource().contains("Approved by 1st level")) {
					if (driver.getPageSource().contains("GCMT , GMOne 1st approver")) {
						if (driver.getPageSource().contains("Accept")) {
							System.out.println(
									"Approver Level1 approved details updated in log along with workflow action");
							Reporter.log("Approver Level1 approved details updated in log along with workflow action");
						} else {
							System.out.println(
									"Approver Level1 approved details not updated in log along with workflow action");
							Assert.fail(
									"Approver Level1 approved details not updated in log along with workflow action");
						}
					}
				}
				driver.close();
				driver.switchTo().window(winHandleBefore);
				logOut();
				driver.quit();
				init();
				Validation_RejectLevel2(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[4]")).click();
				Thread.sleep(10000);
				String winHandleBefore1 = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				if (driver.getPageSource().contains("Rejected by 2nd level")) {
					if (driver.getPageSource().contains("GMOne 2nd approver")) {
						if (driver.getPageSource().contains("Reject")) {
							System.out.println(
									"Approver Level2 rejected details updated in log along with workflow action");
							Reporter.log("Approver Level2 rejected details updated in log along with workflow action");
						} else {
							System.out.println(
									"Approver Level2 rejected details not updated in log along with workflow action");
							Assert.fail(
									"Approver Level2 rejected details not updated in log along with workflow action");
						}
					}
				}
				driver.close();
				driver.switchTo().window(winHandleBefore1);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='sendforapproval']")).click();
				Thread.sleep(3000);
				String winHandleBefore2 = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore2);
				Thread.sleep(3000);
				logOut();
				driver.quit();
				init();
				Validation_SendForWithdrawLevel3(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[4]")).click();
				Thread.sleep(10000);
				String winHandleBefore3 = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				if (driver.getPageSource().contains("Withdrawn")) {
					if (driver.getPageSource().contains("GMOne 3rd approver")) {
						if (driver.getPageSource().contains("Withdraw")) {
							System.out.println(
									"Approver Level3 withdraw details updated in log along with workflow action");
							Reporter.log("Approver Level3 withdraw details updated in log along with workflow action");
						} else {
							System.out.println(
									"Approver Level3 withdraw details not updated in log along with workflow action");
							Assert.fail(
									"Approver Level3 withdraw details not updated in log along with workflow action");
						}
					}
				}
				driver.close();
				driver.switchTo().window(winHandleBefore3);
			}

			catch (Exception e) {

				e.printStackTrace();
			}
		}  else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_044")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				// Thread.sleep(10000);
				System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("activate_contract")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='yui-gen0-button']")).click();
				driver.switchTo().window(winHandleBefore);
				Thread.sleep(5000);
				logOut();
				driver.quit();
				try {
					init();
					Validation_SendForTApproval(globalContractId);
					Thread.sleep(5000);
					logOut();
					driver.quit();
					init();
					Validation_SendForTApprovalLevel1(globalContractId);
					Thread.sleep(5000);
					logOut();
					driver.quit();
					init();
					Validation_SendForTApprovalLevel2(globalContractId);
					Thread.sleep(5000);
					logOut();
					driver.quit();
					init();
					Validation_SendForTApprovalLevel3(globalContractId);
					System.out.println(
							"Users who have 1st/2nd/3rd approver role is able to approve termination request when a contract is sent for termination approval in respective stages verification passed");
					Reporter.log(
							"Users who have 1st/2nd/3rd approver role is able to approve termination request when a contract is sent for termination approval in respective stages verification passed");
				} catch (Exception e) {
					System.out.println(
							"Users who have 1st/2nd/3rd approver role is able to approve termination request when a contract is sent for termination approval in respective stages verification failed:\t"
									+ e);
					Assert.fail(
							"Users who have 1st/2nd/3rd approver role is able to approve termination request when a contract is sent for termination approval in respective stages verification failed:\t"
									+ e);
				}
			}

			catch (Exception e) {

				e.printStackTrace();
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_045")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				// Thread.sleep(10000);
				// System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("activate_contract")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='yui-gen0-button']")).click();
				driver.switchTo().window(winHandleBefore);
				Thread.sleep(5000);
				logOut();
				driver.quit();
				try {
					init();
					Validation_SendForTApproval(globalContractId);
					Thread.sleep(5000);
					logOut();
					driver.quit();
					init();
					Validation_SendForTRejectLevel1(globalContractId);
					System.out.println(
							"Users who have 1st/2nd/3rd approver role is able to reject a termination request when a contract is sent for termination approval in respective stages verification passed");
					Reporter.log(
							"Users who have 1st/2nd/3rd approver role is able to reject a termination request when a contract is sent for termination approval in respective stages verification passed");
				} catch (Exception e) {
					System.out.println(
							"Users who have 1st/2nd/3rd approver role is able to reject a termination request when a contract is sent for termination approval in respective stages verification failed:\t"
									+ e);
					Assert.fail(
							"Users who have 1st/2nd/3rd approver role is able to reject a termination request when a contract is sent for termination approval in respective stages verification failed:\t"
									+ e);
				}
			}

			catch (Exception e) {

				e.printStackTrace();
			}
		}

		/* Added by Deep - End */
		// Code by Sky

		else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_024")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);

				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				// EW.CreateDocumentDraft(DocumentDescription1,
				// DocumentDescription3);

				Validation_SendForApproval();
				// Thread.sleep(10000);
				// System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_RejectLevel1(globalContractId);

				logOut();

				driver.quit();

				init();

				Validation_ApprovalLevel1_CreateDocument_Draft(globalContractId);

				Assert.assertTrue(TestCasePass, "RTC_Workflow_024-passed");
				logOut();

			} catch (Exception e) {
				Assert.fail("RTC_Workflow_024-failed");
				e.printStackTrace();
			}
		}

		else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_025")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);

				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				EW.CreateDocumentDraft(DocumentDescription1, DocumentDescription3);
				EW.CreateDocumentDraft(DocumentDescription1, DocumentDescription3);

				Validation_SendForApproval();

				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1_CreateDocument(globalContractId);
				logOut();
				Assert.assertTrue(TestCasePass, "RTC_Workflow_025-passed");
			} catch (Exception e) {

				e.printStackTrace();
				Assert.fail("RTC_Workflow_025-failed");
			}
		}

		else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_026")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				// Thread.sleep(10000);
				// System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				EW.CreateDocumentModuleApprovedWF(DocumentDescription1, DocumentDescription3);

				System.out.println("RTC_Workflow_026-passed");

				Assert.assertTrue(TestCasePass, "RTC_Workflow_026-passed");
			} catch (Exception e) {

				e.printStackTrace();
				Assert.fail("RTC_Workflow_026-failed");
			}

		}

		else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_027")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				// Thread.sleep(10000);
				// System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				driver.findElement(By.id("activate_contract")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='yui-gen0-button']")).click();
				driver.switchTo().window(winHandleBefore);
				Thread.sleep(5000);

				EW.CreateDocumentModuleActivated(DocumentDescription1, DocumentDescription3);
				Assert.assertTrue(TestCasePass, "RTC_Workflow_027-passed");
				logOut();

			} catch (Exception e) {
				Assert.fail("RTC_Workflow_027-failed");
				e.printStackTrace();
			}

		} else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_028")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);

				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				EW.CreateDocumentDraft(DocumentDescription1, DocumentDescription3);
				EW.CreateDocumentDraft(DocumentDescription1, DocumentDescription3);

				Validation_SendForApproval();

				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1_EditDocument(globalContractId);

				Assert.assertTrue(TestCasePass, "RTC_Workflow_028-passed");
				logOut();
			} catch (Exception e) {
				Assert.fail("RTC_Workflow_028-failed");
				e.printStackTrace();
			}
		}

		else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_029")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);

				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				EW.CreateDocumentDraft(DocumentDescription1, DocumentDescription3);

				Validation_SendForApproval();
				// Thread.sleep(10000);
				// System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				EW.EditDocumentModuleApprovedWF(DocumentDescription1, DocumentName);
				EW.EditDocumentModuleApprovedLIWF(DocumentDescription1, DocumentName);
				Assert.assertTrue(TestCasePass, "RTC_Workflow_029-passed");
				logOut();

			} catch (Exception e) {
				Assert.fail("RTC_Workflow_029-failed");
				e.printStackTrace();
			}
		}

		else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_030")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				Validation_SendForApproval();
				// Thread.sleep(10000);
				// System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel1(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel2(globalContractId);
				logOut();
				driver.quit();
				init();
				Validation_ApprovalLevel3(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				driver.findElement(By.id("activate_contract")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='yui-gen0-button']")).click();
				driver.switchTo().window(winHandleBefore);
				Thread.sleep(5000);

				EW.EditDocumentModuleActivated(DocumentDescription1, DocumentDescription3);
				Assert.assertTrue(TestCasePass, "RTC_Workflow_030-passed");
				logOut();

			} catch (Exception e) {
				Assert.fail("RTC_Workflow_30-failed");
				e.printStackTrace();
			}

		}

		else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_031")) {
			try {
				CW.CreateContractNavigation();
				CW.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);

				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				EW.CreateDocumentModuleDraftWF(DocumentDescription1, DocumentDescription3);
				Validation_SendForApproval();
				// Thread.sleep(10000);
				// System.out.println("Workflow_001-passed");
				logOut();
				driver.quit();
				init();
				Validation_RejectLevel1(globalContractId);

				logOut();

				driver.quit();

				init();
				Validation_ApprovalLevel1_RemoveDocument_Draft(globalContractId);

				logOut();

				Assert.assertTrue(TestCasePass, "RTC_Workflow_031-passed");
				logOut();

			} catch (Exception e) {
				Assert.fail("RTC_Workflow_031-failed");
				e.printStackTrace();
			}
		}

		else if (Test_CaseId.equalsIgnoreCase("RTC_Contract_068")) {
			try {
				logOut();
				driver.quit();
				init();
				logad.Adminlogin(AdminUserName, AdminPassword);
				Thread.sleep(3000);

				CW.CreateContractNavigation();
				CW.ContractDataFillAdmin(ContractName, ContractDesc, ProductsID, ProductOffering,
						ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);

				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				EW.CreateDocumentModuleDraftWF(DocumentDescription1, DocumentDescription3);
				Thread.sleep(2000);

				EW.EditDocumentModuleAdminDraftWF(DocumentDescriptionX14, DocumentNameX14);
				Thread.sleep(2000);
				EW.EditDocumentModuleAdminDraftLIWF(DocumentDescriptionX1, DocumentNameX1);

				System.out.println("Document Editable in Draft Status-Admin");

				Validation_SendForApproval();
				EW.EditDocumentModuleAdminSFAWF(DocumentDescriptionX2, DocumentNameX2);
				Thread.sleep(2000);
				EW.EditDocumentModuleAdminSFALIWF(DocumentDescriptionX3, DocumentNameX3);

				System.out.println("Document Editable in Sent For Approval Status-Admin");

				Validation_ApprovalLevel1Admin();

				Validation_ApprovalLevel2Admin();

				Validation_ApprovalLevel3Admin();

				EW.EditDocumentModuleAdminApprovedWF(DocumentDescriptionX4, DocumentNameX4);
				Thread.sleep(2000);
				EW.EditDocumentModuleAdminApprovedLIWF(DocumentDescriptionX5, DocumentNameX5);

				System.out.println("Document Editable in Approved Status-Admin");

				ActivateAdmin();
				EW.EditDocumentModuleAdminActivatedWF(DocumentDescriptionX6, DocumentNameX6);
				Thread.sleep(2000);
				EW.EditDocumentModuleAdminActivatedLIWF(DocumentDescriptionX7, DocumentNameX7);

				System.out.println("Document Editable in Activated Status-Admin");
				Validation_SendForTApprovalAdmin();
				EW.EditDocumentModuleAdminSFTAWF(DocumentDescriptionX8, DocumentNameX8);
				Thread.sleep(2000);
				EW.EditDocumentModuleAdminSFTALIWF(DocumentDescriptionX9, DocumentNameX9);

				System.out.println("Document Editable in Sent For Termination Approval Status-Admin");

				Validation_SendForTApprovalLevel1Admin();
				Validation_SendForTApprovalLevel2Admin();

				Validation_SendForTApprovalLevel3Admin();

				EW.EditDocumentModuleAdminTApprovedWF(DocumentDescriptionX10, DocumentNameX10);
				Thread.sleep(2000);
				EW.EditDocumentModuleAdminTApprovedLIWF(DocumentDescriptionX11, DocumentNameX11);

				System.out.println("Document Editable in Sent For Termination Approved Status-Admin");

				DeActivateAdmin();

				EW.EditDocumentModuleAdminDeactivatedWF(DocumentDescriptionX12, DocumentNameX12);
				Thread.sleep(2000);
				EW.EditDocumentModuleAdminDeactivatedLIWF(DocumentDescriptionX13, DocumentNameX13);

				System.out.println("Document Editable in Deactivated Status-Admin");

				Assert.assertTrue(TestCasePass, "RTC_Workflow_068-passed");
				logOut();

			} catch (Exception e) {
				Assert.fail("RTC_Contract_068-failed");
				e.printStackTrace();
			}
		}

		
		else if (Test_CaseId.equalsIgnoreCase("RTC_Contract_070")) {
			try {
				System.out.println("EXECUTION START");
			logOut();
			driver.quit();
				init();
				logad.Adminlogin(AdminUserName, AdminPassword);
				Thread.sleep(3000);

				CW.CreateContractNavigation();
				CW.ContractDataFillAdmin(ContractName, ContractDesc, ProductsID, ProductOffering,
						ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
	

				ValidationEdit_WFAdmin();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(2000);
				
				Validation_SendForApproval();
				Thread.sleep(2000);
				ValidationEdit_WFAdmin_SFA();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				System.out.println("Contract Editable in SFA Status-Admin");
					
				Thread.sleep(2000);
				
				
				
				
				
				
				
				
				
				ValidationEdit_ApprovalLevel1WFAdmin();
				Thread.sleep(2000);
				ValidationEdit_WFAdmin_Approval1();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				System.out.println("Contract Editable in Approval 1 Status-Admin");
								Thread.sleep(2000);		
				
				
				ValidationEdit_ApprovalLevel2WFAdmin();
				Thread.sleep(2000);
				ValidationEdit_WFAdmin_Approval2();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				System.out.println("Contract Editable in Approval 2 Status-Admin");
							Thread.sleep(2000);			
				
				
				ValidationEdit_ApprovalLevel3WFAdmin();
				
				Thread.sleep(2000);
				ValidationEdit_WFAdmin_Approval3();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				System.out.println("Contract Editable in Approved Status-Admin");
									Thread.sleep(2000);	
				/*ValidationEdit_WFAdmin();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
									Thread.sleep(2000);*/
				ActivateEditWFAdmin();
				Thread.sleep(2000);
				ValidationEdit_WFAdmin_Activate();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				System.out.println("Contract Editable in Activated Status-Admin");
									Thread.sleep(2000);
				/*ValidationEdit_WFAdmin();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
									Thread.sleep(2000);*/
				ValidationEdit_SendForTApprovalWFAdmin();
				Thread.sleep(2000);
				ValidationEdit_WFAdmin_SFTA();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				System.out.println("Contract Editable in SFTA Status-Admin");
									Thread.sleep(2000);
				/*ValidationEdit_WFAdmin();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
									Thread.sleep(2000);*/
				ValidationEdit_TApprovalLevel1WFAdmin();
				Thread.sleep(2000);
				ValidationEdit_WFAdmin_SFTApproval1();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				System.out.println("Contract Editable in TAL1 Status-Admin");
									Thread.sleep(2000);
				ValidationEdit_TApprovalLevel2WFAdmin();
				Thread.sleep(2000);
				ValidationEdit_WFAdmin_SFTApproval2();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				System.out.println("Contract Editable in TAL2 Status-Admin");
									Thread.sleep(2000);
				
				ValidationEdit_TApprovalLevel3WFAdmin();
				
				Thread.sleep(2000);
				ValidationEdit_WFAdmin_SFTApproval3();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				System.out.println("Contract Editable in TAL3 Status-Admin");
				
				/*ValidationEdit_WFAdmin();
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
									Thread.sleep(2000);*/
				DeActivateEditWFAdmin();
				
				
				Thread.sleep(2000);
				ValidationEdit_WFAdmin_Deactivate();
				
				Thread.sleep(1000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				
									Thread.sleep(2000);
				System.out.println("Contract Editable in Deactivated Status-Admin");

				Assert.assertTrue(TestCasePass, "RTC_Workflow_070-passed");
				logOut();

			} catch (Exception e) {
				Assert.fail("RTC_Contract_070-failed");
				e.printStackTrace();
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/**** Coding Ends Here *****/
	}

	// ********************Approval Flow*****************//

	private By By(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	public void Validation_SendForApproval() throws Exception {

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		// Send for approval
		WebElement TxtBoxContent001 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus = TxtBoxContent001.getText();

		WebElement TxtBoxContent002 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status = TxtBoxContent002.getText();

		System.out.println("Printing contract status:" + tempContractStatus);
		System.out.println("Printing workflow flag:" + status);

		if (tempContractStatus.equalsIgnoreCase("Draft")) {
			try {
				// srn.getscreenshot();

				driver.findElement(By.xpath(".//*[@id='sendforapproval']")).click();
				Thread.sleep(2000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(2000);
				driver.findElement(By.xpath(".//*[@id='sugar-message-prompt']")).sendKeys("Accept");
				Thread.sleep(3000);
				driver.findElement(By.xpath(".//*[@id='yui-gen1-button']")).click();

				driver.switchTo().window(winHandleBefore);

				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

			} catch (Exception e) {
				Reporter.log("An Error occured SendForApproval" + e);

			}

			// srn.getscreenshot();

			WebElement TxtBoxContentA = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusA = TxtBoxContentA.getText();

			WebElement TxtBoxContentB = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusA = TxtBoxContentB.getText();

			System.out.println("Printing contract status after send for approval:" + tempContractStatusA);
			System.out.println("Printing workflow flag after send for approval:" + statusA);

			if (statusA.equalsIgnoreCase("Contract submitted to workflow") == tempContractStatusA
					.equalsIgnoreCase("Sent for Approval")) {

				System.out.println("Contract sent for approval");
				Reporter.log("Contract sent for approval");

			} else {
				Reporter.log("Error while sending contract for approval" + status);
				org.testng.Assert.fail();
			}
		}

		else {
			System.out.println("Contract status invalid");
		}

	}

	
	public void Validation_ApprovalLevel1_CreateDocument(String ContractName)throws Exception
	{

		logwf.WFLevel1login(WF1userName, WF1password);
		Thread.sleep(3000);

		CW.ViewContractNavigation();
		// CW.ViewContractWorkflow(ContractName);
		Thread.sleep(5000);
		CW.SelectContractId(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		try {

			EW.CreateDocumentModuleSentForApproval(DocumentDescription1, DocumentDescription3);
			System.out.println("Workflow_025/RTC_Workflow_025-passed");
			Reporter.log("Workflow_025/RTC_Workflow_025-passed");
		}

		catch (Exception e) {
			Reporter.log("Workflow_025/RTC_Workflow_025-failed" + e);

		}
	}

	public void Validation_ApprovalLevel1_CreateDocument_Draft(String ContractName) throws Exception {

		logwf.WFLevel1login(WF1userName, WF1password);
		Thread.sleep(3000);

		CW.ViewContractNavigation();
		// CW.ViewContractWorkflow(ContractName);
		Thread.sleep(5000);
		CW.SelectContractId(ContractName);

		try {

			EW.CreateDocumentModuleDraftWF(DocumentDescription1, DocumentDescription3);
			System.out.println("RTC_Workflow_024-passed");
			Reporter.log("RTC_Workflow_024-passed");
		}

		catch (Exception e) {
			Reporter.log("RTC_Workflow_024-failed" + e);

		}
	}

	public void Validation_ApprovalLevel1_RemoveDocument_Draft(String ContractName) throws Exception {

		logwf.WFLevel1login(WF1userName, WF1password);
		Thread.sleep(3000);

		CW.ViewContractNavigation();
		// CW.ViewContractWorkflow(ContractName);
		Thread.sleep(5000);
		CW.SelectContractId(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		try {

			EW.RemoveDocumentModuleDraftWF(DocumentDescription1, DocumentDescription3);
			System.out.println("Workflow_024/RTC_Workflow_031-passed");
			Reporter.log("Workflow_024/RTC_Workflow_025-passed");
		}

		catch (Exception e) {
			Reporter.log("Workflow_024/RTC_Workflow_031-failed" + e);

		}
	}

	public void Validation_ApprovalLevel1_EditDocument(String ContractName) throws Exception {

		logwf.WFLevel1login(WF1userName, WF1password);
		Thread.sleep(3000);

		CW.ViewContractNavigation();
		// CW.ViewContractWorkflow(ContractName);
		Thread.sleep(5000);
		CW.SelectContractId(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		try {

			EW.EditDocumentModuleSentForApproval(DocumentDescription1, DocumentDescription3);
			System.out.println("Workflow_028/RTC_Workflow_028-passed");
			Reporter.log("Workflow_028/RTC_Workflow_028-passed");
		}

		catch (Exception e) {
			Reporter.log("Workflow_028/RTC_Workflow_028-failed" + e);

		}
	}

	public void Validation_ApprovalLevel1(String ContractName) throws Exception {

		logwf.WFLevel1login(WF1userName, WF1password);
		Thread.sleep(3000);

		CW.ViewContractNavigation();
		// CW.ViewContractWorkflow(ContractName);
		Thread.sleep(5000);
		CW.SelectContractId(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent002 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus1 = TxtBoxContent002.getText();

		WebElement TxtBoxContent003 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status1 = TxtBoxContent003.getText();

		System.out.println("Printing contract status before level 1 approval:" + tempContractStatus1);
		System.out.println("Printing workflow flag before level 1 approval:" + status1);

		if (tempContractStatus1.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 1 approval" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentC = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusB = TxtBoxContentC.getText();

			WebElement TxtBoxContentD = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusB = TxtBoxContentD.getText();

			System.out.println("Printing contract status after level 1 approval:" + tempContractStatusB);
			System.out.println("Printing workflow flag after level 1 approval:" + statusB);

			if (statusB.equalsIgnoreCase("Approved by 1st level") == tempContractStatusB
					.equalsIgnoreCase("Sent for Approval")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 1");
				Assert.assertTrue(TestCasePass, "Level1Approval Done");
				System.out.println("Approval Success:level 1");
			} else {
				System.out.println("Could not do 1st level of approval");
				Assert.fail("Approval Failure:level 1");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}
	public void Validation_ApprovalLevel1_VersionUp(String ContractName) throws Exception {

		logwf.WFLevel1login(WF1userName, WF1password);
		Thread.sleep(3000);

		CW.ViewContractNavigation();
		// CW.ViewContractWorkflow(ContractName);
		Thread.sleep(5000);
		CW.SelectContractId_VersionUp(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent002 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus1 = TxtBoxContent002.getText();

		WebElement TxtBoxContent003 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status1 = TxtBoxContent003.getText();

		System.out.println("Printing contract status before level 1 approval:" + tempContractStatus1);
		System.out.println("Printing workflow flag before level 1 approval:" + status1);

		if (tempContractStatus1.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 1 approval" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentC = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusB = TxtBoxContentC.getText();

			WebElement TxtBoxContentD = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusB = TxtBoxContentD.getText();

			System.out.println("Printing contract status after level 1 approval:" + tempContractStatusB);
			System.out.println("Printing workflow flag after level 1 approval:" + statusB);

			if (statusB.equalsIgnoreCase("Approved by 1st level") == tempContractStatusB
					.equalsIgnoreCase("Sent for Approval")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 1");
				Assert.assertTrue(TestCasePass, "Level1Approval Done");
				System.out.println("Approval Success:level 1");
			} else {
				System.out.println("Could not do 1st level of approval");
				Assert.fail("Approval Failure:level 1");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}
	
	
	public void Validation_ApprovalLevel1_VersionUp2(String ContractName) throws Exception {

		logwf.WFLevel1login(WF1userName, WF1password);
		Thread.sleep(3000);

		CW.ViewContractNavigation();
		// CW.ViewContractWorkflow(ContractName);
		Thread.sleep(5000);
		CW.SelectContractId_VersionUp2(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent002 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus1 = TxtBoxContent002.getText();

		WebElement TxtBoxContent003 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status1 = TxtBoxContent003.getText();

		System.out.println("Printing contract status before level 1 approval:" + tempContractStatus1);
		System.out.println("Printing workflow flag before level 1 approval:" + status1);

		if (tempContractStatus1.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 1 approval" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentC = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusB = TxtBoxContentC.getText();

			WebElement TxtBoxContentD = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusB = TxtBoxContentD.getText();

			System.out.println("Printing contract status after level 1 approval:" + tempContractStatusB);
			System.out.println("Printing workflow flag after level 1 approval:" + statusB);

			if (statusB.equalsIgnoreCase("Approved by 1st level") == tempContractStatusB
					.equalsIgnoreCase("Sent for Approval")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 1");
				Assert.assertTrue(TestCasePass, "Level1Approval Done");
				System.out.println("Approval Success:level 1");
			} else {
				System.out.println("Could not do 1st level of approval");
				Assert.fail("Approval Failure:level 1");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}
	public void Validation_ApprovalLevel1Admin() throws Exception {

		Thread.sleep(3000);

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent002 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus1 = TxtBoxContent002.getText();

		WebElement TxtBoxContent003 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status1 = TxtBoxContent003.getText();

		System.out.println("Printing contract status before level 1 approval:" + tempContractStatus1);
		System.out.println("Printing workflow flag before level 1 approval:" + status1);

		if (tempContractStatus1.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 1 approval" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentC = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusB = TxtBoxContentC.getText();

			WebElement TxtBoxContentD = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusB = TxtBoxContentD.getText();

			System.out.println("Printing contract status after level 1 approval:" + tempContractStatusB);
			System.out.println("Printing workflow flag after level 1 approval:" + statusB);

			if (statusB.equalsIgnoreCase("Approved by 1st level") == tempContractStatusB
					.equalsIgnoreCase("Sent for Approval")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 1");
				System.out.println("Approval Success:level 1");
			} else {
				System.out.println("Could not do 1st level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	public void Validation_ApprovalLevel2Admin() throws Exception {

		Thread.sleep(3000);

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent002 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus1 = TxtBoxContent002.getText();

		WebElement TxtBoxContent003 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status1 = TxtBoxContent003.getText();

		System.out.println("Printing contract status before level 2 approval:" + tempContractStatus1);
		System.out.println("Printing workflow flag before level 2 approval:" + status1);

		if (tempContractStatus1.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 1 approval" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentC = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusB = TxtBoxContentC.getText();

			WebElement TxtBoxContentD = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusB = TxtBoxContentD.getText();

			System.out.println("Printing contract status after level 2 approval:" + tempContractStatusB);
			System.out.println("Printing workflow flag after level 2 approval:" + statusB);

			if (statusB.equalsIgnoreCase("Approved by 2nd level") == tempContractStatusB
					.equalsIgnoreCase("Sent for Approval")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 2");
				System.out.println("Approval Success:level 2");
			} else {
				System.out.println("Could not do 2nd level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	public void Validation_ApprovalLevel3Admin() throws Exception {

		Thread.sleep(3000);

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent002 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus1 = TxtBoxContent002.getText();

		WebElement TxtBoxContent003 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status1 = TxtBoxContent003.getText();

		System.out.println("Printing contract status before level 3 approval:" + tempContractStatus1);
		System.out.println("Printing workflow flag before level 3 approval:" + status1);

		if (tempContractStatus1.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 1 approval" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentC = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusB = TxtBoxContentC.getText();

			WebElement TxtBoxContentD = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusB = TxtBoxContentD.getText();

			System.out.println("Printing contract status after level 3 approval:" + tempContractStatusB);
			System.out.println("Printing workflow flag after level 3 approval:" + statusB);

			if (statusB.equalsIgnoreCase("Approved by 3rd level") == tempContractStatusB
					.equalsIgnoreCase("Approved")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 3");
				System.out.println("Approval Success:level 3");
			} else {
				System.out.println("Could not do 3rd level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	public void Validation_RejectLevel1_VersionUp(String ContractName) throws Exception {

		logwf.WFLevel1login(WF1userName, WF1password);

		CW.ViewContractNavigation();

		// CW.ViewContractWorkflow(ContractName);
		CW.SelectContractId_VersionUp(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent004 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus2 = TxtBoxContent004.getText();

		WebElement TxtBoxContent005 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status2 = TxtBoxContent005.getText();

		System.out.println("Printing contract status before reject:" + tempContractStatus2);
		System.out.println("Printing workflow flag before reject:" + status2);

		if (tempContractStatus2.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();

				Thread.sleep(3000);

				// driver.findElement(By.id("reject")).click();

				driver.findElement(By.xpath(".//*[@id='rejectContract']")).click();

				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Reject");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in rejecting contract by approver level1" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentE = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusC = TxtBoxContentE.getText();

			WebElement TxtBoxContentF = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusC = TxtBoxContentF.getText();

			System.out.println("Printing contract status after reject:" + tempContractStatusC);
			System.out.println("Printing workflow flag after reject:" + statusC);
			if (statusC.equalsIgnoreCase("Contract in progress") == tempContractStatusC.equalsIgnoreCase("Draft")) {

				Reporter.log("Reject Success:level 1");
				Assert.assertTrue(TestCasePass, "Level1Reject Done");
				System.out.println("Reject Success:level 1");
			} else {
				System.out.println("Could not do Reject level 1");
				Assert.fail("Reject Failure:level 1");
			}
		}
	}
	public void Validation_RejectLevel1(String ContractName) throws Exception {

		logwf.WFLevel1login(WF1userName, WF1password);

		CW.ViewContractNavigation();

		// CW.ViewContractWorkflow(ContractName);
		CW.SelectContractId(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent004 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus2 = TxtBoxContent004.getText();

		WebElement TxtBoxContent005 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status2 = TxtBoxContent005.getText();

		System.out.println("Printing contract status before reject:" + tempContractStatus2);
		System.out.println("Printing workflow flag before reject:" + status2);

		if (tempContractStatus2.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();

				Thread.sleep(3000);

				// driver.findElement(By.id("reject")).click();

				driver.findElement(By.xpath(".//*[@id='rejectContract']")).click();

				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Reject");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in rejecting contract by approver level1" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentE = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusC = TxtBoxContentE.getText();

			WebElement TxtBoxContentF = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusC = TxtBoxContentF.getText();

			System.out.println("Printing contract status after reject:" + tempContractStatusC);
			System.out.println("Printing workflow flag after reject:" + statusC);
			if (statusC.equalsIgnoreCase("Contract in progress") == tempContractStatusC.equalsIgnoreCase("Draft")) {

				Reporter.log("Reject Success:level 1");
				Assert.assertTrue(TestCasePass, "Level1Reject Done");
				System.out.println("Reject Success:level 1");
			} else {
				System.out.println("Could not do Reject level 1");
				Assert.fail("Reject Failure:level 1");
			}
		}
	}

	public void Validation_WithdrawLevel1() throws Exception {

		logwf.WFLevel1login(WF1userName, WF1password);
		CW.ViewContractNavigation();
		CW.SelectContract(ContractName);
		WebElement TxtBoxContent006 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus3 = TxtBoxContent006.getText();

		WebElement TxtBoxContent007 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status3 = TxtBoxContent007.getText();
		
		System.out.println("Printing contract status before level 2 approval:" + tempContractStatus3);
		System.out.println("Printing workflow flag before level 2 approval:" + status3);
		if (tempContractStatus3.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("withdrawapprovalrequest")).click();

				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in withdraw contract by approver level1" + e);

			}

			// srn.getscreenshot();
			//EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentI = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusE = TxtBoxContentI.getText();

			WebElement TxtBoxContentJ = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusE = TxtBoxContentJ.getText();

			System.out.println("Printing contract status after withdraw:" + tempContractStatusE);
			System.out.println("Printing workflow flag after withdraw:" + statusE);
			if (statusE.equalsIgnoreCase("Contract in progress") & tempContractStatusE.equalsIgnoreCase("Draft")) {
				// String result1 = wf.Accept();
				// System.out.println("Printing" + result1);
				System.out.println("Wihdraw happened at Level 1");
			} else {
				System.out.println("Could not do Withdraw level 1");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}

	/*Added by net - Start*/
	public void Validation_WithdrawNormalUser() throws Exception {

		/*logwf.WFLevel1login(WF1userName, WF1password);
		CW.ViewContractNavigation();
		CW.SelectContract(ContractName);*/
		
		
		
		WebElement TxtBoxContent006 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus3 = TxtBoxContent006.getText();
		System.out.println("tempContractStatus3 : "+tempContractStatus3);

		WebElement TxtBoxContent007 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status3 = TxtBoxContent007.getText();
		System.out.println("status3 : "+status3);
		
		System.out.println("Printing contract status before level 2 approval:" + tempContractStatus3);
		System.out.println("Printing workflow flag before level 2 approval:" + status3);
		
		if (tempContractStatus3.equalsIgnoreCase("Sent for Approval")) {

			try {
				//srn.getscreenshot();
				driver.findElement(By.id("withdrawapprovalrequest")).click();
				Thread.sleep(2000);
				System.out.println("Withdraw Approval Request button clicked...");
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}

				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.xpath(".//*[@id='sugar-message-prompt']")).sendKeys("Withdraw");
				//srn.getscreenshot();
				Thread.sleep(2000);
				driver.findElement(By.xpath(".//*[@id='yui-gen1-button']")).click();

				driver.switchTo().window(winHandleBefore);
				
		


			} catch (Exception e) {
				Reporter.log("An Error occured in withdraw contract by Normal user" + e);
				System.out.println("Error during Withdraw" +e);
			}

			//srn.getscreenshot();
			/*EW.ContractStatus();*/
			
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentI = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusE = TxtBoxContentI.getText();

			WebElement TxtBoxContentJ = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusE = TxtBoxContentJ.getText();

			System.out.println("Printing contract status after withdraw:" + tempContractStatusE);
			System.out.println("Printing workflow flag after withdraw:" + statusE);
			
			if (statusE.equalsIgnoreCase("Contract in progress") & tempContractStatusE.equalsIgnoreCase("Draft")) {
				System.out.println("Wihdraw happened at Normal user level");
			} else {
				System.out.println("Could not do Withdraw at Normal user level");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}
	/*Added by net - End*/
	
	public void Validation_ApprovalLevel2(String ContractName) throws Throwable {

		logwf.WFLevel2login(WF5userName, WF5password);
		Thread.sleep(3000);
		CW.ViewContractNavigation();
		// CW.ViewContractNavigation();
		// CW.ViewContractWorkflow(ContractName);
		CW.SelectContractId(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent008 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus4 = TxtBoxContent008.getText();

		WebElement TxtBoxContent009 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status4 = TxtBoxContent009.getText();

		System.out.println("Printing contract status before level 2 approval:" + tempContractStatus4);
		System.out.println("Printing workflow flag before level 2 approval:" + status4);

		if (tempContractStatus4.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 1 approval" + e);

			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

			Thread.sleep(30000);
			WebElement TxtBoxContentI = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusE = TxtBoxContentI.getText();

			WebElement TxtBoxContentJ = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusE = TxtBoxContentJ.getText();

			System.out.println("Printing contract status after level 2 approval:" + tempContractStatusE);
			System.out.println("Printing workflow flag after level 2 approval:" + statusE);

			if (statusE.equalsIgnoreCase("Approved by 2nd level") == tempContractStatusE
					.equalsIgnoreCase("Sent for Approval")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 2");
				System.out.println("Approval Success:level 2");
			} else {
				System.out.println("Could not do 2nd level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	public void Validation_ApprovalLevel2_VersionUp(String ContractName) throws Throwable {

		logwf.WFLevel2login(WF5userName, WF5password);
		Thread.sleep(3000);
		CW.ViewContractNavigation();
		// CW.ViewContractNavigation();
		// CW.ViewContractWorkflow(ContractName);
		CW.SelectContractId_VersionUp(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent008 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus4 = TxtBoxContent008.getText();

		WebElement TxtBoxContent009 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status4 = TxtBoxContent009.getText();

		System.out.println("Printing contract status before level 2 approval:" + tempContractStatus4);
		System.out.println("Printing workflow flag before level 2 approval:" + status4);

		if (tempContractStatus4.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 1 approval" + e);

			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

			Thread.sleep(30000);
			WebElement TxtBoxContentI = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusE = TxtBoxContentI.getText();

			WebElement TxtBoxContentJ = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusE = TxtBoxContentJ.getText();

			System.out.println("Printing contract status after level 2 approval:" + tempContractStatusE);
			System.out.println("Printing workflow flag after level 2 approval:" + statusE);

			if (statusE.equalsIgnoreCase("Approved by 2nd level") == tempContractStatusE
					.equalsIgnoreCase("Sent for Approval")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 2");
				System.out.println("Approval Success:level 2");
			} else {
				System.out.println("Could not do 2nd level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	public void Validation_ApprovalLevel2_VersionUp2(String ContractName) throws Throwable {

		logwf.WFLevel2login(WF5userName, WF5password);
		Thread.sleep(3000);
		CW.ViewContractNavigation();
		// CW.ViewContractNavigation();
		// CW.ViewContractWorkflow(ContractName);
		CW.SelectContractId_VersionUp2(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent008 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus4 = TxtBoxContent008.getText();

		WebElement TxtBoxContent009 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status4 = TxtBoxContent009.getText();

		System.out.println("Printing contract status before level 2 approval:" + tempContractStatus4);
		System.out.println("Printing workflow flag before level 2 approval:" + status4);

		if (tempContractStatus4.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 1 approval" + e);

			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

			Thread.sleep(30000);
			WebElement TxtBoxContentI = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusE = TxtBoxContentI.getText();

			WebElement TxtBoxContentJ = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusE = TxtBoxContentJ.getText();

			System.out.println("Printing contract status after level 2 approval:" + tempContractStatusE);
			System.out.println("Printing workflow flag after level 2 approval:" + statusE);

			if (statusE.equalsIgnoreCase("Approved by 2nd level") == tempContractStatusE
					.equalsIgnoreCase("Sent for Approval")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 2");
				System.out.println("Approval Success:level 2");
			} else {
				System.out.println("Could not do 2nd level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	
	public void Validation_RejectLevel2_VersionUp(String ContractName) throws Exception

	{

		logwf.WFLevel2login(WF5userName, WF5password);
		Thread.sleep(3000);
		CW.ViewContractNavigation();
		// CW.ViewContractNavigation();
		// CW.ViewContractWorkflow(ContractName);
		CW.SelectContractId_VersionUp(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent010 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus5 = TxtBoxContent010.getText();

		WebElement TxtBoxContent011 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status5 = TxtBoxContent011.getText();

		System.out.println("Printing contract status before reject level 2:" + tempContractStatus5);
		System.out.println("Printing workflow flag before reject level 2:" + status5);

		if (tempContractStatus5.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();

				Thread.sleep(3000);

				// driver.findElement(By.id("reject")).click();

				driver.findElement(By.xpath(".//*[@id='rejectContract']")).click();

				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Reject");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in rejecting contract by approver level2" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentM = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusG = TxtBoxContentM.getText();

			WebElement TxtBoxContentN = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusG = TxtBoxContentN.getText();

			System.out.println("Printing contract status after reject:" + tempContractStatusG);
			System.out.println("Printing workflow flag after reject:" + statusG);
			if (statusG.equalsIgnoreCase("Contract in progress") == tempContractStatusG.equalsIgnoreCase("Draft")) {

				Reporter.log("Reject Success:level 2");
				System.out.println("Reject Success:level 2");
			} else {
				System.out.println("Could not do Reject level 2");
			}
		}

		else {
			System.out.println("Contract status invalid");
		}

	}
	public void Validation_RejectLevel2(String ContractName) throws Exception

	{

		logwf.WFLevel2login(WF5userName, WF5password);
		Thread.sleep(3000);
		CW.ViewContractNavigation();
		// CW.ViewContractNavigation();
		// CW.ViewContractWorkflow(ContractName);
		CW.SelectContractId(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent010 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus5 = TxtBoxContent010.getText();

		WebElement TxtBoxContent011 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status5 = TxtBoxContent011.getText();

		System.out.println("Printing contract status before reject level 2:" + tempContractStatus5);
		System.out.println("Printing workflow flag before reject level 2:" + status5);

		if (tempContractStatus5.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();

				Thread.sleep(3000);

				// driver.findElement(By.id("reject")).click();

				driver.findElement(By.xpath(".//*[@id='rejectContract']")).click();

				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Reject");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in rejecting contract by approver level2" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentM = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusG = TxtBoxContentM.getText();

			WebElement TxtBoxContentN = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusG = TxtBoxContentN.getText();

			System.out.println("Printing contract status after reject:" + tempContractStatusG);
			System.out.println("Printing workflow flag after reject:" + statusG);
			if (statusG.equalsIgnoreCase("Contract in progress") == tempContractStatusG.equalsIgnoreCase("Draft")) {

				Reporter.log("Reject Success:level 2");
				System.out.println("Reject Success:level 2");
			} else {
				System.out.println("Could not do Reject level 2");
			}
		}

		else {
			System.out.println("Contract status invalid");
		}

	}

	public void Validation_WithdrawLevel2() throws Exception

	{
		logwf.WFLevel2login(WF5userName, WF5password);
		CW.ViewContractNavigation();
		CW.SelectContract(ContractName);
		WebElement TxtBoxContent012 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus6 = TxtBoxContent012.getText();

		WebElement TxtBoxContent013 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status6 = TxtBoxContent013.getText();
		
		System.out.println("Printing contract status before level 2 approval:" + tempContractStatus6);
		System.out.println("Printing workflow flag before level 2 approval:" + status6);

		if (tempContractStatus6.equalsIgnoreCase("Sent for Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("withdrawapprovalrequest")).click();

				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Thread.sleep(30000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured withdraw level 2" + e);

			}

			// srn.getscreenshot();
			//EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentM = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatus16 = TxtBoxContentM.getText();

			WebElement TxtBoxContentN = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String status16 = TxtBoxContentN.getText();

			System.out.println("Printing contract status after withdraw:" + tempContractStatus16);
			System.out.println("Printing workflow flag after withdraw:" + status16);
			if (status16.equalsIgnoreCase("Contract in progress") & tempContractStatus16.equalsIgnoreCase("Draft")) {
				// String result2 = wf.Accept();
				// System.out.println("Printing" + result2);
				System.out.println("withdraw happened at Level 2");
			} else {
				System.out.println("Could not do withdraw level 2");
			}

		}

		else {
			System.out.println("Contract status invalid");
		}

	}

	public void Validation_ApprovalLevel3(String ContractName) throws Exception {

		logwf.WFLevel3login(WF8userName, WF8password);
		Thread.sleep(3000);

		// CW.ViewContractNavigation();

		CW.ViewContractNavigation();
		CW.SelectContractId(ContractName);
		driver.switchTo().defaultContent();
		Thread.sleep(3000);

		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent014 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus7 = TxtBoxContent014.getText();

		WebElement TxtBoxContent015 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status7 = TxtBoxContent015.getText();

		System.out.println("Printing contract status before level 3 approval:" + tempContractStatus7);
		System.out.println("Printing workflow flag before level 3 approval:" + status7);

		if (tempContractStatus7.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 3 approval" + e);

			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentK = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusF = TxtBoxContentK.getText();

			WebElement TxtBoxContentL = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusF = TxtBoxContentL.getText();

			System.out.println("Printing contract status after level 2 approval:" + tempContractStatusF);
			System.out.println("Printing workflow flag after level 2 approval:" + statusF);

			if (statusF.equalsIgnoreCase("Approved by 3rd level") == tempContractStatusF.equalsIgnoreCase("Approved")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 3");
				System.out.println("Approval Success:level 3");
			} else {
				System.out.println("Could not do 3rd level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}
		WebElement TxtBoxContentA1 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[1]/strong"));
		String statusLi1 = TxtBoxContentA1.getText();

		WebElement TxtBoxContentA2 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[2]/strong"));
		String statusLi2 = TxtBoxContentA2.getText();

		WebElement TxtBoxContentA3 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[4]/strong"));
		String statusLi3 = TxtBoxContentA3.getText();

		System.out.println("***Printing Line Item Details**");
		System.out.println("Printing Line Item ID:" + statusLi1);
		System.out.println("Printing Line Item Name:" + statusLi2);
		System.out.println("Printing Line Item State:" + statusLi3);

	}
	
	public void Validation_ApprovalLevel3_VersionUp(String ContractName) throws Exception {

		logwf.WFLevel3login(WF8userName, WF8password);
		Thread.sleep(3000);

		// CW.ViewContractNavigation();

		CW.ViewContractNavigation();
		CW.SelectContractId_VersionUp(ContractName);
		driver.switchTo().defaultContent();
		Thread.sleep(3000);

		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent014 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus7 = TxtBoxContent014.getText();

		WebElement TxtBoxContent015 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status7 = TxtBoxContent015.getText();

		System.out.println("Printing contract status before level 3 approval:" + tempContractStatus7);
		System.out.println("Printing workflow flag before level 3 approval:" + status7);

		if (tempContractStatus7.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 3 approval" + e);

			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentK = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusF = TxtBoxContentK.getText();

			WebElement TxtBoxContentL = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusF = TxtBoxContentL.getText();

			System.out.println("Printing contract status after level 2 approval:" + tempContractStatusF);
			System.out.println("Printing workflow flag after level 2 approval:" + statusF);

			if (statusF.equalsIgnoreCase("Approved by 3rd level") == tempContractStatusF.equalsIgnoreCase("Approved")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 3");
				System.out.println("Approval Success:level 3");
			} else {
				System.out.println("Could not do 3rd level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}
		WebElement TxtBoxContentA1 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[1]/strong"));
		String statusLi1 = TxtBoxContentA1.getText();

		WebElement TxtBoxContentA2 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[2]/strong"));
		String statusLi2 = TxtBoxContentA2.getText();

		WebElement TxtBoxContentA3 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[4]/strong"));
		String statusLi3 = TxtBoxContentA3.getText();

		System.out.println("***Printing Line Item Details**");
		System.out.println("Printing Line Item ID:" + statusLi1);
		System.out.println("Printing Line Item Name:" + statusLi2);
		System.out.println("Printing Line Item State:" + statusLi3);

	}

	public void Validation_ApprovalLevel3_VersionUp2(String ContractName) throws Exception {

		logwf.WFLevel3login(WF8userName, WF8password);
		Thread.sleep(3000);

		// CW.ViewContractNavigation();

		CW.ViewContractNavigation();
		CW.SelectContractId_VersionUp2(ContractName);
		driver.switchTo().defaultContent();
		Thread.sleep(3000);

		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent014 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus7 = TxtBoxContent014.getText();

		WebElement TxtBoxContent015 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status7 = TxtBoxContent015.getText();

		System.out.println("Printing contract status before level 3 approval:" + tempContractStatus7);
		System.out.println("Printing workflow flag before level 3 approval:" + status7);

		if (tempContractStatus7.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 3 approval" + e);

			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentK = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusF = TxtBoxContentK.getText();

			WebElement TxtBoxContentL = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusF = TxtBoxContentL.getText();

			System.out.println("Printing contract status after level 2 approval:" + tempContractStatusF);
			System.out.println("Printing workflow flag after level 2 approval:" + statusF);

			if (statusF.equalsIgnoreCase("Approved by 3rd level") == tempContractStatusF.equalsIgnoreCase("Approved")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 3");
				System.out.println("Approval Success:level 3");
			} else {
				System.out.println("Could not do 3rd level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}
		WebElement TxtBoxContentA1 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[1]/strong"));
		String statusLi1 = TxtBoxContentA1.getText();

		WebElement TxtBoxContentA2 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[2]/strong"));
		String statusLi2 = TxtBoxContentA2.getText();

		WebElement TxtBoxContentA3 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[4]/strong"));
		String statusLi3 = TxtBoxContentA3.getText();

		System.out.println("***Printing Line Item Details**");
		System.out.println("Printing Line Item ID:" + statusLi1);
		System.out.println("Printing Line Item Name:" + statusLi2);
		System.out.println("Printing Line Item State:" + statusLi3);

	}

	public void Validation_RejectLevel3(String ContractName) throws Exception

	{

		logwf.WFLevel3login(WF8userName, WF8password);
		Thread.sleep(3000);

		// CW.ViewContractNavigation();

		CW.ViewContractNavigation();
		// CW.ViewContractWorkflow(ContractName);
		CW.SelectContractId(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent016 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus8 = TxtBoxContent016.getText();

		WebElement TxtBoxContent017 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status8 = TxtBoxContent017.getText();

		System.out.println("Printing contract status before reject level 3:" + tempContractStatus8);
		System.out.println("Printing workflow flag before reject level 3:" + status8);

		if (tempContractStatus8.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();

				Thread.sleep(3000);

				// driver.findElement(By.id("reject")).click();

				driver.findElement(By.xpath(".//*[@id='rejectContract']")).click();

				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Reject");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in rejecting contract by approver level3" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentM = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusG = TxtBoxContentM.getText();

			WebElement TxtBoxContentN = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusG = TxtBoxContentN.getText();

			System.out.println("Printing contract status after reject level 3:" + tempContractStatusG);
			System.out.println("Printing workflow flag after reject level 3:" + statusG);
			if (statusG.equalsIgnoreCase("Contract in progress") == tempContractStatusG.equalsIgnoreCase("Draft")) {

				Reporter.log("Reject Success:level 3");
				System.out.println("Reject Success:level 3");
			} else {
				System.out.println("Could not do Reject level 3");
			}
		}

		else {
			System.out.println("Contract status invalid");
		}

	}

	public void Validation_SendForWithdrawLevel3(String ContractName) throws Exception {
		logwf.WFLevel3login(WF8userName, WF8password);
		CW.ViewContractNavigation();
		// CW.SelectContract(ContractName);
		CW.SelectContractId(ContractName);

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent018 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus9 = TxtBoxContent018.getText();
		System.out.println("Contract Status::" + tempContractStatus9);
		/*
		 * WebElement TxtBoxContent019 =
		 * driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		 * String status9 = TxtBoxContent019.getText();
		 */
		if (tempContractStatus9.equalsIgnoreCase("Approved")
				|| tempContractStatus9.equalsIgnoreCase("Sent for Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.xpath(".//*[@id='withdrawapprovalrequest']")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Withdraw");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				Reporter.log("An Error occured withdraw level 3" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement contractStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String cStatus = contractStatus.getText();
			WebElement workFlow = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String wFlow = workFlow.getText();
			System.out.println("Printing workflow flag:" + wFlow);
			if (wFlow.equalsIgnoreCase("Contract in progress") & cStatus.equalsIgnoreCase("Draft")) {
				// String result3 = wf.Accept();
				// System.out.println("Printing" + result3);
				System.out.println("Withdraw happened at Level 3");
			} else {
				System.out.println("Could not do Withdraw level 3");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}
	public void Validation_SendForWithdrawLevel3_VersionUp(String ContractName) throws Exception {
		logwf.WFLevel3login(WF8userName, WF8password);
		CW.ViewContractNavigation();
		// CW.SelectContract(ContractName);
		CW.SelectContractId_VersionUp(ContractName);

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent018 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus9 = TxtBoxContent018.getText();
		System.out.println("Contract Status::" + tempContractStatus9);
		/*
		 * WebElement TxtBoxContent019 =
		 * driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		 * String status9 = TxtBoxContent019.getText();
		 */
		if (tempContractStatus9.equalsIgnoreCase("Approved")
				|| tempContractStatus9.equalsIgnoreCase("Sent for Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.xpath(".//*[@id='withdrawapprovalrequest']")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Withdraw");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				Reporter.log("An Error occured withdraw level 3" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement contractStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String cStatus = contractStatus.getText();
			WebElement workFlow = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String wFlow = workFlow.getText();
			System.out.println("Printing workflow flag:" + wFlow);
			if (wFlow.equalsIgnoreCase("Contract in progress") & cStatus.equalsIgnoreCase("Draft")) {
				// String result3 = wf.Accept();
				// System.out.println("Printing" + result3);
				System.out.println("Withdraw happened at Level 3");
			} else {
				System.out.println("Could not do Withdraw level 3");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}

	public void Activate(String ContractName) throws Exception {
		logwf.WFLevel3login(WF8userName, WF8password);

		Thread.sleep(3000);

		// CW.ViewContractNavigation();

		CW.ViewContractNavigation();
		//CW.ViewContractWorkflow(ContractName);

		
		
		CW.SelectContractId(globalContractId);
		

		
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		/*
		 * CW.ViewContractNavigation(); CW.SelectContract(ContractName);
		 */

		WebElement TxtBoxContent020 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus10 = TxtBoxContent020.getText();

		WebElement TxtBoxContent021 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status10 = TxtBoxContent021.getText();

		System.out.println("Printing contract status before Activation:" + tempContractStatus10);
		System.out.println("Printing workflow flag before Activation:" + status10);

		if (tempContractStatus10.equalsIgnoreCase("Approved")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("activate_contract")).click();
				driver.findElement(By.id("yui-gen0-button")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}

				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured when accepting activation" + e);

			}

			// srn.getscreenshot();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentK = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusL = TxtBoxContentK.getText();

			WebElement TxtBoxContentL = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusL = TxtBoxContentL.getText();

			System.out.println("Printing contract status after Activation:" + tempContractStatusL);
			System.out.println("Printing workflow flag after Activation:" + statusL);

			if (statusL.equalsIgnoreCase("Approved by 3rd level") == tempContractStatusL
					.equalsIgnoreCase("Activated")) {

				Reporter.log("Activation Success");
				System.out.println("Activation Success");
			} else {
				System.out.println("Could not do Activation");
			}
		}

		else {
			System.out.println("Contract status invalid");
		}

		WebElement TxtBoxContentD1 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[1]/strong"));
		String statusL1 = TxtBoxContentD1.getText();

		WebElement TxtBoxContentD2 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[2]/strong"));
		String statusL2 = TxtBoxContentD2.getText();

		WebElement TxtBoxContentD3 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[4]/strong"));
		String statusL3 = TxtBoxContentD3.getText();

		System.out.println("***Printing Line Item Details**");
		System.out.println("Printing Line Item ID:" + statusL1);
		System.out.println("Printing Line Item Name:" + statusL2);
		System.out.println("Printing Line Item State:" + statusL3);

	}
	public void Activate_VersionUp(String ContractName) throws Exception {
		logwf.WFLevel3login(WF8userName, WF8password);

		Thread.sleep(3000);

		// CW.ViewContractNavigation();

		CW.ViewContractNavigation();
		//CW.ViewContractWorkflow(ContractName);
	CW.SelectContractId_VersionUp(globalContractId);
		

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		/*
		 * CW.ViewContractNavigation(); CW.SelectContract(ContractName);
		 */

		WebElement TxtBoxContent020 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus10 = TxtBoxContent020.getText();

		WebElement TxtBoxContent021 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status10 = TxtBoxContent021.getText();

		System.out.println("Printing contract status before Activation:" + tempContractStatus10);
		System.out.println("Printing workflow flag before Activation:" + status10);

		if (tempContractStatus10.equalsIgnoreCase("Approved")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("activate_contract")).click();
				driver.findElement(By.id("yui-gen0-button")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}

				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured when accepting activation" + e);

			}

			// srn.getscreenshot();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentK = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusL = TxtBoxContentK.getText();

			WebElement TxtBoxContentL = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusL = TxtBoxContentL.getText();

			System.out.println("Printing contract status after Activation:" + tempContractStatusL);
			System.out.println("Printing workflow flag after Activation:" + statusL);

			if (statusL.equalsIgnoreCase("Approved by 3rd level") == tempContractStatusL
					.equalsIgnoreCase("Activated")) {

				Reporter.log("Activation Success");
				System.out.println("Activation Success");
			} else {
				System.out.println("Could not do Activation");
			}
		}

		else {
			System.out.println("Contract status invalid");
		}

		WebElement TxtBoxContentD1 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[1]/strong"));
		String statusL1 = TxtBoxContentD1.getText();

		WebElement TxtBoxContentD2 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[2]/strong"));
		String statusL2 = TxtBoxContentD2.getText();

		WebElement TxtBoxContentD3 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[4]/strong"));
		String statusL3 = TxtBoxContentD3.getText();

		System.out.println("***Printing Line Item Details**");
		System.out.println("Printing Line Item ID:" + statusL1);
		System.out.println("Printing Line Item Name:" + statusL2);
		System.out.println("Printing Line Item State:" + statusL3);

	}

	public void ActivateVersionUp2(String ContractName) throws Exception {
		logwf.WFLevel3login(WF8userName, WF8password);

		Thread.sleep(3000);

		// CW.ViewContractNavigation();

		CW.ViewContractNavigation();
		//CW.ViewContractWorkflow(ContractName);
	CW.SelectContractId_VersionUp2(globalContractId);
		

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		/*
		 * CW.ViewContractNavigation(); CW.SelectContract(ContractName);
		 */

		WebElement TxtBoxContent020 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus10 = TxtBoxContent020.getText();

		WebElement TxtBoxContent021 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status10 = TxtBoxContent021.getText();

		System.out.println("Printing contract status before Activation:" + tempContractStatus10);
		System.out.println("Printing workflow flag before Activation:" + status10);

		if (tempContractStatus10.equalsIgnoreCase("Approved")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("activate_contract")).click();
				driver.findElement(By.id("yui-gen0-button")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}

				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured when accepting activation" + e);

			}

			// srn.getscreenshot();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentK = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusL = TxtBoxContentK.getText();

			WebElement TxtBoxContentL = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusL = TxtBoxContentL.getText();

			System.out.println("Printing contract status after Activation:" + tempContractStatusL);
			System.out.println("Printing workflow flag after Activation:" + statusL);

			if (statusL.equalsIgnoreCase("Approved by 3rd level") == tempContractStatusL
					.equalsIgnoreCase("Activated")) {

				Reporter.log("Activation Success");
				System.out.println("Activation Success");
			} else {
				System.out.println("Could not do Activation");
			}
		}

		else {
			System.out.println("Contract status invalid");
		}

		WebElement TxtBoxContentD1 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[1]/strong"));
		String statusL1 = TxtBoxContentD1.getText();

		WebElement TxtBoxContentD2 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[2]/strong"));
		String statusL2 = TxtBoxContentD2.getText();

		WebElement TxtBoxContentD3 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[4]/strong"));
		String statusL3 = TxtBoxContentD3.getText();

		System.out.println("***Printing Line Item Details**");
		System.out.println("Printing Line Item ID:" + statusL1);
		System.out.println("Printing Line Item Name:" + statusL2);
		System.out.println("Printing Line Item State:" + statusL3);

	}

	
	
	
	
	
	public void ActivateAdmin() throws Exception {
		//logwf.WFLevel3login(WF8userName, WF8password);

		Thread.sleep(3000);

		// CW.ViewContractNavigation();

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		/*
		 * CW.ViewContractNavigation(); CW.SelectContract(ContractName);
		 */

		WebElement TxtBoxContent020 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus10 = TxtBoxContent020.getText();

		WebElement TxtBoxContent021 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status10 = TxtBoxContent021.getText();

		System.out.println("Printing contract status before Activation:" + tempContractStatus10);
		System.out.println("Printing workflow flag before Activation:" + status10);

		if (tempContractStatus10.equalsIgnoreCase("Approved")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("activate_contract")).click();
				driver.findElement(By.id("yui-gen0-button")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}

				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured when accepting activation" + e);

			}

			// srn.getscreenshot();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentK = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusL = TxtBoxContentK.getText();

			WebElement TxtBoxContentL = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusL = TxtBoxContentL.getText();

			System.out.println("Printing contract status after Activation:" + tempContractStatusL);
			System.out.println("Printing workflow flag after Activation:" + statusL);

			if (statusL.equalsIgnoreCase("Approved by 3rd level") == tempContractStatusL
					.equalsIgnoreCase("Activated")) {

				Reporter.log("Activation Success");
				System.out.println("Activation Success");
			} else {
				System.out.println("Could not do Activation");
			}
		}

		else {
			System.out.println("Contract status invalid");
		}

		WebElement TxtBoxContentD1 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[1]/strong"));
		String statusL1 = TxtBoxContentD1.getText();

		WebElement TxtBoxContentD2 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[2]/strong"));
		String statusL2 = TxtBoxContentD2.getText();

		WebElement TxtBoxContentD3 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[4]/strong"));
		String statusL3 = TxtBoxContentD3.getText();

		System.out.println("***Printing Line Item Details**");
		System.out.println("Printing Line Item ID:" + statusL1);
		System.out.println("Printing Line Item Name:" + statusL2);
		System.out.println("Printing Line Item State:" + statusL3);

	}

	public String Accept() throws Exception {
		try {

			// srn.getscreenshot();
			driver.findElement(By.id("approvecontract")).click();
			String winHandleBefore = driver.getWindowHandle();
			for (String handle : driver.getWindowHandles()) {

				driver.switchTo().window(handle);
			}
			// srn.getscreenshot();

			driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
			// srn.getscreenshot();
			driver.findElement(By.id("yui-gen1-button")).click();

			driver.switchTo().window(winHandleBefore);
		} catch (Exception e) {
			Reporter.log("An error occured while approving the contract" + e);
			return "fail";
		}
		// srn.getscreenshot();
		return "sucess";

	}

	public void Reject() throws Exception {
		// rejectContract
		// srn.getscreenshot();
		driver.findElement(By.id("rejectContract")).click();
		String winHandleBefore = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}

		// srn.getscreenshot();
		driver.findElement(By.id("sugar-message-prompt")).sendKeys("Reject");
		driver.findElement(By.id("yui-gen1-button")).click();

		driver.switchTo().window(winHandleBefore);
	}

	public void ApprovalLogs() {
		driver.findElement(By.id("button")).click();
		driver.findElement(By.xpath("//tbody/tr/td[1]/div/div/input[3]")).click();
		String winHandleBefore = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}

		String uname = driver.findElement(By.xpath("//table/tbody/tr[2]/td[1]")).getText();
		Reporter.log(uname);
		String comments = driver.findElement(By.xpath("//table/tbody/tr[2]/td[2]")).getText();
		Reporter.log(comments);

		driver.switchTo().window(winHandleBefore);
	}

	
	public void ActivateVersionUp(String ContractName) throws Exception {
		

		Thread.sleep(3000);

		// CW.ViewContractNavigation();


		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		/*
		 * CW.ViewContractNavigation(); CW.SelectContract(ContractName);
		 */

		WebElement TxtBoxContent020 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus10 = TxtBoxContent020.getText();

		WebElement TxtBoxContent021 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status10 = TxtBoxContent021.getText();

		System.out.println("Printing contract status before Activation:" + tempContractStatus10);
		System.out.println("Printing workflow flag before Activation:" + status10);

		if (tempContractStatus10.equalsIgnoreCase("Approved")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("activate_contract")).click();
				driver.findElement(By.id("yui-gen0-button")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}

				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured when accepting activation" + e);

			}

			// srn.getscreenshot();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentK = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusL = TxtBoxContentK.getText();

			WebElement TxtBoxContentL = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusL = TxtBoxContentL.getText();

			System.out.println("Printing contract status after Activation:" + tempContractStatusL);
			System.out.println("Printing workflow flag after Activation:" + statusL);

			if (statusL.equalsIgnoreCase("Approved by 3rd level") == tempContractStatusL
					.equalsIgnoreCase("Activated")) {

				Reporter.log("Activation Success");
				System.out.println("Activation Success");
			} else {
				System.out.println("Could not do Activation");
			}
		}

		else {
			System.out.println("Contract status invalid");
		}

		WebElement TxtBoxContentD1 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[1]/strong"));
		String statusL1 = TxtBoxContentD1.getText();

		WebElement TxtBoxContentD2 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[2]/strong"));
		String statusL2 = TxtBoxContentD2.getText();

		WebElement TxtBoxContentD3 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[4]/strong"));
		String statusL3 = TxtBoxContentD3.getText();

		System.out.println("***Printing Line Item Details**");
		System.out.println("Printing Line Item ID:" + statusL1);
		System.out.println("Printing Line Item Name:" + statusL2);
		System.out.println("Printing Line Item State:" + statusL3);

	}

	
	
	
	
	
	// ***********Termination Flow********************//

	public void Validation_SendForTApproval(String ContractId) throws Exception {
	
		log.Run();
		CW.ViewContractNavigation();
		CW.SelectContractId(ContractId);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent021 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus11 = TxtBoxContent021.getText();
		/*
		 * WebElement TxtBoxContent022 =
		 * driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"))
		 * ; String status11 = TxtBoxContent022.getText();
		 */
		if (tempContractStatus11.equalsIgnoreCase("Activated")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("send_termination_approval")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Termination");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				Reporter.log("An Error occured in send for termination approval" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement cStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String conStatus = cStatus.getText();
			WebElement TxtBoxContent022 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String status11 = TxtBoxContent022.getText();
			System.out.println("Printing workflow flag:" + status11);
			if (status11.equalsIgnoreCase("Contract submitted to Termination workflow")
					& conStatus.equalsIgnoreCase("Sent for Termination Approval")) {
				Reporter.log("Contract send for termination");
			} else {
				Reporter.log("Error while sending contract for termination" + status11);
				org.testng.Assert.fail();
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}

	public void Validation_SendForTApprovalAdmin() throws Exception {

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent021 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus11 = TxtBoxContent021.getText();
		/*
		 * WebElement TxtBoxContent022 =
		 * driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"))
		 * ; String status11 = TxtBoxContent022.getText();
		 */
		if (tempContractStatus11.equalsIgnoreCase("Activated")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("send_termination_approval")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Termination");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				Reporter.log("An Error occured in send for termination approval" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement cStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String conStatus = cStatus.getText();
			WebElement TxtBoxContent022 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String status11 = TxtBoxContent022.getText();
			System.out.println("Printing workflow flag:" + status11);
			if (status11.equalsIgnoreCase("Contract submitted to Termination workflow")
					& conStatus.equalsIgnoreCase("Sent for Termination Approval")) {
				Reporter.log("Contract send for termination");
			} else {
				Reporter.log("Error while sending contract for termination" + status11);
				org.testng.Assert.fail();
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}

	public void Validation_SendForTApprovalLevel1(String ContractId) throws Exception {
		logwf.WFLevel1login(WF1userName, WF1password);
		CW.ViewContractNavigation();
		// CW.SelectContract(ContractName);
		CW.SelectContractId(ContractId);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent023 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus12 = TxtBoxContent023.getText();
		/*
		 * WebElement TxtBoxContent024 =
		 * driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"))
		 * ; String status12 = TxtBoxContent024.getText();
		 */
		if (tempContractStatus12.equalsIgnoreCase("Sent for Termination Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("termination_approval")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Termination Approve");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				Reporter.log("An Error occured during send for termination approval 1" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement cStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String conStatus = cStatus.getText();
			WebElement TxtBoxContent024 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String status12 = TxtBoxContent024.getText();
			System.out.println("Printing workflow flag:" + status12);
			if (status12.equalsIgnoreCase("Termination Approved by 1st level")
					& conStatus.equalsIgnoreCase("Sent for Termination Approval")) {
				// String resultT1 = wf.Accept();
				// System.out.println("Printing" + resultT1);
				System.out.println("Temination Approval Success:Level1");
			} else {
				System.out.println("Could not do 1st level of termination approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	public void Validation_SendForTApprovalLevel1Admin() throws Exception {

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent023 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus12 = TxtBoxContent023.getText();
		/*
		 * WebElement TxtBoxContent024 =
		 * driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"))
		 * ; String status12 = TxtBoxContent024.getText();
		 */
		if (tempContractStatus12.equalsIgnoreCase("Sent for Termination Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("termination_approval")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Termination Approve");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				Reporter.log("An Error occured during send for termination approval 1" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement cStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String conStatus = cStatus.getText();
			WebElement TxtBoxContent024 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String status12 = TxtBoxContent024.getText();
			System.out.println("Printing workflow flag:" + status12);
			if (status12.equalsIgnoreCase("Termination Approved by 1st level")
					& conStatus.equalsIgnoreCase("Sent for Termination Approval")) {
				// String resultT1 = wf.Accept();
				// System.out.println("Printing" + resultT1);
				System.out.println("Temination Approval Success:Level1");
			} else {
				System.out.println("Could not do 1st level of termination approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	public void Validation_SendForTRejectLevel1(String ContractId) throws Exception {
		logwf.WFLevel1login(WF1userName, WF1password);
		CW.ViewContractNavigation();
		// CW.SelectContract(ContractName);
		CW.SelectContractId(ContractId);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent025 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus13 = TxtBoxContent025.getText();
		/*
		 * WebElement TxtBoxContent026 =
		 * driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"))
		 * ; String status13 = TxtBoxContent026.getText();
		 */
		if (tempContractStatus13.equalsIgnoreCase("Sent for Termination Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("termination_reject")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Termination Reject");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				Assert.fail("An Error occured during rejection level 1" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement cStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String conStatus = cStatus.getText();
			WebElement TxtBoxContent026 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String status13 = TxtBoxContent026.getText();
			if (status13.equalsIgnoreCase("Contract submitted to Termination workflow")
					& conStatus.equalsIgnoreCase("Activated")) {
				// String resultTR1 = wf.Accept();
				// System.out.println("Printing" + resultTR1);
				System.out.println("Temination Reject Success:Level1");
			} else {
				System.out.println("Could not do Reject");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	public void Validation_SendForTApprovalLevel2(String ContractId) throws Exception {
		logwf.WFLevel2login(WF5userName, WF5password);
		CW.ViewContractNavigation();
		// CW.SelectContract(ContractName);
		CW.SelectContractId(ContractId);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent027 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus14 = TxtBoxContent027.getText();
		/*
		 * WebElement TxtBoxContent028 =
		 * driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"))
		 * ; String status14 = TxtBoxContent028.getText();
		 */
		if (tempContractStatus14.equalsIgnoreCase("Sent for Termination Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("termination_approval")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Termination Approve");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				Reporter.log("An Error occured termination approval 2" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement cStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String conStatus = cStatus.getText();
			WebElement TxtBoxContent028 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String status14 = TxtBoxContent028.getText();
			System.out.println("Printing workflow flag:" + status14);
			if (status14.equalsIgnoreCase("Termination Approved by 2nd level")
					& conStatus.equalsIgnoreCase("Sent for Termination Approval")) {
				// String resultTR2 = wf.Accept();
				// System.out.println("Printing" + resultTR2);
				System.out.println("Temination Approval Success:Level2");
			} else {
				System.out.println("Could not do 2nd level of term approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}

	public void Validation_SendForTApprovalLevel2Admin() throws Exception {

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent027 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus14 = TxtBoxContent027.getText();
		/*
		 * WebElement TxtBoxContent028 =
		 * driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"))
		 * ; String status14 = TxtBoxContent028.getText();
		 */
		if (tempContractStatus14.equalsIgnoreCase("Sent for Termination Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("termination_approval")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Termination Approve");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				Reporter.log("An Error occured termination approval 2" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement cStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String conStatus = cStatus.getText();
			WebElement TxtBoxContent028 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String status14 = TxtBoxContent028.getText();
			System.out.println("Printing workflow flag:" + status14);
			if (status14.equalsIgnoreCase("Termination Approved by 2nd level")
					& conStatus.equalsIgnoreCase("Sent for Termination Approval")) {
				// String resultTR2 = wf.Accept();
				// System.out.println("Printing" + resultTR2);
				System.out.println("Temination Approval Success:Level2");
			} else {
				System.out.println("Could not do 2nd level of term approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}

	public void Validation_SendForTRejectLevel2() throws Exception

	{
		logwf.WFLevel2login(WF5userName, WF5password);
		CW.ViewContractNavigation();
		CW.SelectContract(ContractName);

		WebElement TxtBoxContent028 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus15 = TxtBoxContent028.getText();

		WebElement TxtBoxContent029 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status15 = TxtBoxContent029.getText();

		if (tempContractStatus15.equalsIgnoreCase("Sent for Termination Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("termination_reject")).click();

				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}

				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured rejection:leve2" + e);

			}

			// srn.getscreenshot();
			EW.ContractStatus();
			if (status15.equalsIgnoreCase("Contract submitted to Termination workflow")
					& tempContractStatus15.equalsIgnoreCase("Activated")) {
				// String resultTR2 = wf.Accept();
				// System.out.println("Printing" + resultTR2);
				System.out.println("Termination Reject Happened at level 2");
			} else {
				System.out.println("Could not do Reject");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	public void Validation_SendForTApprovalLevel3(String ContractId) throws Exception {
		logwf.WFLevel3login(WF8userName, WF8password);
		CW.ViewContractNavigation();
		// CW.SelectContract(ContractName);
		CW.SelectContractId(ContractId);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent030 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus16 = TxtBoxContent030.getText();
		/*
		 * WebElement TxtBoxContent031 =
		 * driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"))
		 * ; String status16 = TxtBoxContent031.getText();
		 */
		if (tempContractStatus16.equalsIgnoreCase("Sent for Termination Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("termination_approval")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
				System.out.println("Temination Approval Success:Level3");
			} catch (Exception e) {
				Reporter.log("An Error occured" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement cStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String conStatus = cStatus.getText();
			WebElement TxtBoxContent031 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String status16 = TxtBoxContent031.getText();
			System.out.println("Printing workflow flag:" + status16);
			if (status16.equalsIgnoreCase("Termination Approved by 3rd level")
					& conStatus.equalsIgnoreCase("Termination Approved")) {
				// String resultT3 = wf.Accept();
				// System.out.println("Printing" + resultT3);
			} else {
				System.out.println("Could not do 3rd level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}

	public void Validation_SendForTApprovalLevel3Admin() throws Exception {

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent030 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus16 = TxtBoxContent030.getText();
		/*
		 * WebElement TxtBoxContent031 =
		 * driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"))
		 * ; String status16 = TxtBoxContent031.getText();
		 */
		if (tempContractStatus16.equalsIgnoreCase("Sent for Termination Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("termination_approval")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
				System.out.println("Temination Approval Success:Level3");
			} catch (Exception e) {
				Reporter.log("An Error occured" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement cStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String conStatus = cStatus.getText();
			WebElement TxtBoxContent031 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String status16 = TxtBoxContent031.getText();
			System.out.println("Printing workflow flag:" + status16);
			if (status16.equalsIgnoreCase("Termination Approved by 3rd level")
					& conStatus.equalsIgnoreCase("Termination Approved")) {
				// String resultT3 = wf.Accept();
				// System.out.println("Printing" + resultT3);
			} else {
				System.out.println("Could not do 3rd level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}

	public void Validation_SendForTRejectLevel3() throws Exception

	{
		logwf.WFLevel3login(WF8userName, WF8password);
		CW.ViewContractNavigation();
		CW.SelectContract(ContractName);

		WebElement TxtBoxContent031 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus17 = TxtBoxContent031.getText();

		WebElement TxtBoxContent032 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status17 = TxtBoxContent032.getText();

		if (tempContractStatus17.equalsIgnoreCase("Sent for Termination Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("termination_reject")).click();

				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}

				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured Termination Rejection level 3" + e);

			}

			// srn.getscreenshot();
			EW.ContractStatus();
			if (status17.equalsIgnoreCase("Contract submitted to Termination workflow")
					& tempContractStatus17.equalsIgnoreCase("Activated")) {
				// String resultTR3 = wf.Accept();
				// System.out.println("Printing" + resultTR3);
			} else {
				System.out.println("Could not do Reject");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	public void DeActivate() throws Exception {
		logwf.WFLevel2login(WF5userName, WF5password);

		CW.ViewContractNavigation();
		CW.SelectContract(ContractName);

		WebElement TxtBoxContent033 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus18 = TxtBoxContent033.getText();

		WebElement TxtBoxContent034 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status18 = TxtBoxContent034.getText();

		if (tempContractStatus18.equalsIgnoreCase("Termination Approved")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("deactivate")).click();

				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}

				//driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen0-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured during deactivation" + e);

			}

			// srn.getscreenshot();

			EW.ContractStatus();
			System.out.println("Printing workflow flag:" + status18);

			if (status18.equalsIgnoreCase("Termination Approved by 3rd level")
					& tempContractStatus18.equalsIgnoreCase("Deactivated")) {
				Reporter.log("Contract Deactivated");
			} else {
				Reporter.log("Error while doing Deativation" + status18);
				org.testng.Assert.fail();
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	public void DeActivateAdmin() throws Exception {

		WebElement TxtBoxContent033 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus18 = TxtBoxContent033.getText();

		WebElement TxtBoxContent034 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status18 = TxtBoxContent034.getText();

		if (tempContractStatus18.equalsIgnoreCase("Termination Approved")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("deactivate")).click();

				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}

				//driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen0-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured during deactivation" + e);

			}

			// srn.getscreenshot();

			EW.ContractStatus();
			System.out.println("Printing workflow flag:" + status18);

			if (status18.equalsIgnoreCase("Termination Approved by 3rd level")
					& tempContractStatus18.equalsIgnoreCase("Deactivated")) {
				Reporter.log("Contract Deactivated");
			} else {
				Reporter.log("Error while doing Deativation" + status18);
				org.testng.Assert.fail();
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}
	
	

	public void ValidationEdit_WFAdmin() throws Exception{
		
		CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.id("edit_button")).click();
			CW.ClearData_Edit();
			CW.ClearData_Edit_FillData();
		
		
	}
	public void ValidationEdit_WFAdmin_SFA() throws Exception{
		
		CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.id("edit_button")).click();
			CW.ClearData_Edit();
			CW.ClearData_Edit_FillData_SFA();
		
		
	}
	public void ValidationEdit_WFAdmin_Approval1() throws Exception{
		
		CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.id("edit_button")).click();
			CW.ClearData_Edit();
			CW.ClearData_Edit_FillData_Approval1();
		
		
	}
	
	public void ValidationEdit_WFAdmin_Approval2() throws Exception{
		
		CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.id("edit_button")).click();
			CW.ClearData_Edit();
			CW.ClearData_Edit_FillData_Approval2();
		
		
	}
	
	public void ValidationEdit_WFAdmin_Approval3() throws Exception{
		
		CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.id("edit_button")).click();
			CW.ClearData_Edit();
			CW.ClearData_Edit_FillData_Approval3();
		
		
	}
	
	public void ValidationEdit_WFAdmin_Activate() throws Exception{
		
		CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.id("edit_button")).click();
			CW.ClearData_Edit();
			CW.ClearData_Edit_FillData_Activate();
		
		
	}
	
	public void ValidationEdit_WFAdmin_SFTA() throws Exception{
		
		CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.id("edit_button")).click();
			CW.ClearData_Edit();
			CW.ClearData_Edit_FillData_SFTA();
		
		
	}
	public void ValidationEdit_WFAdmin_SFTApproval1() throws Exception{
		
		CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.id("edit_button")).click();
			CW.ClearData_Edit();
			CW.ClearData_Edit_FillData_SFTApproval1();
		
		}
	public void ValidationEdit_WFAdmin_SFTApproval2() throws Exception{
		
		CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.id("edit_button")).click();
			CW.ClearData_Edit();
			CW.ClearData_Edit_FillData_SFTApproval2();
		
		}
	
	public void ValidationEdit_WFAdmin_SFTApproval3() throws Exception{
		
		CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.id("edit_button")).click();
			CW.ClearData_Edit();
			CW.ClearData_Edit_FillData_SFTApproval3();
		
		}
	
	public void ValidationEdit_WFAdmin_Deactivate() throws Exception{
		
		CW.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				CW.SelectContractId(globalContractId);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.id("edit_button")).click();
			CW.ClearData_Edit();
			CW.ClearData_Edit_FillData_Deactivate();
		
		}
	public void ValidationEdit_ApprovalLevel1WFAdmin() throws Exception {

		Thread.sleep(3000);

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent002 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus1 = TxtBoxContent002.getText();

		WebElement TxtBoxContent003 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status1 = TxtBoxContent003.getText();

		System.out.println("Printing contract status before level 1 approval:" + tempContractStatus1);
		System.out.println("Printing workflow flag before level 1 approval:" + status1);

		if (tempContractStatus1.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 1 approval" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentC = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusB = TxtBoxContentC.getText();

			WebElement TxtBoxContentD = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusB = TxtBoxContentD.getText();

			System.out.println("Printing contract status after level 1 approval:" + tempContractStatusB);
			System.out.println("Printing workflow flag after level 1 approval:" + statusB);

			if (statusB.equalsIgnoreCase("Approved by 1st level") == tempContractStatusB
					.equalsIgnoreCase("Sent for Approval")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 1");
				System.out.println("Approval Success:level 1");
			} else {
				System.out.println("Could not do 1st level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	public void ValidationEdit_ApprovalLevel2WFAdmin() throws Exception {

		Thread.sleep(3000);

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent002 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus1 = TxtBoxContent002.getText();

		WebElement TxtBoxContent003 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status1 = TxtBoxContent003.getText();

		System.out.println("Printing contract status before level 2 approval:" + tempContractStatus1);
		System.out.println("Printing workflow flag before level 2 approval:" + status1);

		if (tempContractStatus1.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 1 approval" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentC = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusB = TxtBoxContentC.getText();

			WebElement TxtBoxContentD = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusB = TxtBoxContentD.getText();

			System.out.println("Printing contract status after level 2 approval:" + tempContractStatusB);
			System.out.println("Printing workflow flag after level 2 approval:" + statusB);

			if (statusB.equalsIgnoreCase("Approved by 2nd level") == tempContractStatusB
					.equalsIgnoreCase("Sent for Approval")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 2");
				System.out.println("Approval Success:level 2");
			} else {
				System.out.println("Could not do 2nd level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}

	public void ValidationEdit_ApprovalLevel3WFAdmin() throws Exception {

		Thread.sleep(3000);

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		WebElement TxtBoxContent002 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus1 = TxtBoxContent002.getText();

		WebElement TxtBoxContent003 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status1 = TxtBoxContent003.getText();

		System.out.println("Printing contract status before level 3 approval:" + tempContractStatus1);
		System.out.println("Printing workflow flag before level 3 approval:" + status1);

		if (tempContractStatus1.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("approvecontract")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				Thread.sleep(3000);
				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in level 1 approval" + e);

			}

			// srn.getscreenshot();
			// EW.ContractStatus();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentC = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusB = TxtBoxContentC.getText();

			WebElement TxtBoxContentD = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusB = TxtBoxContentD.getText();

			System.out.println("Printing contract status after level 3 approval:" + tempContractStatusB);
			System.out.println("Printing workflow flag after level 3 approval:" + statusB);

			if (statusB.equalsIgnoreCase("Approved by 3rd level") == tempContractStatusB
					.equalsIgnoreCase("Approved")) {
				// String result = wf.Accept();
				// System.out.println("Printing" + result);
				Reporter.log("Approval Success:level 3");
				System.out.println("Approval Success:level 3");
			} else {
				System.out.println("Could not do 3rd level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}	
	
	
	
	public void ActivateEditWFAdmin() throws Exception {
		//logwf.WFLevel3login(WF8userName, WF8password);

		Thread.sleep(3000);

		// CW.ViewContractNavigation();

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		/*
		 * CW.ViewContractNavigation(); CW.SelectContract(ContractName);
		 */

		WebElement TxtBoxContent020 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus10 = TxtBoxContent020.getText();

		WebElement TxtBoxContent021 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status10 = TxtBoxContent021.getText();

		System.out.println("Printing contract status before Activation:" + tempContractStatus10);
		System.out.println("Printing workflow flag before Activation:" + status10);

		if (tempContractStatus10.equalsIgnoreCase("Approved")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("activate_contract")).click();
				driver.findElement(By.id("yui-gen0-button")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured when accepting activation" + e);

			}

			// srn.getscreenshot();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(30000);
			WebElement TxtBoxContentK = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusL = TxtBoxContentK.getText();

			WebElement TxtBoxContentL = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusL = TxtBoxContentL.getText();

			System.out.println("Printing contract status after Activation:" + tempContractStatusL);
			System.out.println("Printing workflow flag after Activation:" + statusL);

			if (statusL.equalsIgnoreCase("Approved by 3rd level") == tempContractStatusL
					.equalsIgnoreCase("Activated")) {

				Reporter.log("Activation Success");
				System.out.println("Activation Success");
			} else {
				System.out.println("Could not do Activation");
			}
		}

		else {
			System.out.println("Contract status invalid");
		}

		WebElement TxtBoxContentD1 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[1]/strong"));
		String statusL1 = TxtBoxContentD1.getText();

		WebElement TxtBoxContentD2 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[2]/strong"));
		String statusL2 = TxtBoxContentD2.getText();

		WebElement TxtBoxContentD3 = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[4]/strong"));
		String statusL3 = TxtBoxContentD3.getText();

		System.out.println("***Printing Line Item Details**");
		System.out.println("Printing Line Item ID:" + statusL1);
		System.out.println("Printing Line Item Name:" + statusL2);
		System.out.println("Printing Line Item State:" + statusL3);

	}

	public void ValidationEdit_SendForTApprovalWFAdmin() throws Exception {

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent021 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus11 = TxtBoxContent021.getText();
		/*
		 * WebElement TxtBoxContent022 =
		 * driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"))
		 * ; String status11 = TxtBoxContent022.getText();
		 */
		if (tempContractStatus11.equalsIgnoreCase("Activated")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("send_termination_approval")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Termination");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				Reporter.log("An Error occured in send for termination approval" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement cStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String conStatus = cStatus.getText();
			WebElement TxtBoxContent022 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String status11 = TxtBoxContent022.getText();
			System.out.println("Printing workflow flag:" + status11);
			if (status11.equalsIgnoreCase("Contract submitted to Termination workflow")
					& conStatus.equalsIgnoreCase("Sent for Termination Approval")) {
				Reporter.log("Contract send for termination");
			} else {
				Reporter.log("Error while sending contract for termination" + status11);
				org.testng.Assert.fail();
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}
	
public void ValidationEdit_TApprovalLevel1WFAdmin() throws Exception {

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent023 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus12 = TxtBoxContent023.getText();
		/*
		 * WebElement TxtBoxContent024 =
		 * driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"))
		 * ; String status12 = TxtBoxContent024.getText();
		 */
		if (tempContractStatus12.equalsIgnoreCase("Sent for Termination Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("termination_approval")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Termination Approve");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				Reporter.log("An Error occured during send for termination approval 1" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement cStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String conStatus = cStatus.getText();
			WebElement TxtBoxContent024 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String status12 = TxtBoxContent024.getText();
			System.out.println("Printing workflow flag:" + status12);
			if (status12.equalsIgnoreCase("Termination Approved by 1st level")
					& conStatus.equalsIgnoreCase("Sent for Termination Approval")) {
				// String resultT1 = wf.Accept();
				// System.out.println("Printing" + resultT1);
				System.out.println("Temination Approval Success:Level1");
			} else {
				System.out.println("Could not do 1st level of termination approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}
	
	
	public void ValidationEdit_TApprovalLevel2WFAdmin() throws Exception {

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent027 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus14 = TxtBoxContent027.getText();
		/*
		 * WebElement TxtBoxContent028 =
		 * driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"))
		 * ; String status14 = TxtBoxContent028.getText();
		 */
		if (tempContractStatus14.equalsIgnoreCase("Sent for Termination Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("termination_approval")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Termination Approve");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				Reporter.log("An Error occured termination approval 2" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement cStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String conStatus = cStatus.getText();
			WebElement TxtBoxContent028 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String status14 = TxtBoxContent028.getText();
			System.out.println("Printing workflow flag:" + status14);
			if (status14.equalsIgnoreCase("Termination Approved by 2nd level")
					& conStatus.equalsIgnoreCase("Sent for Termination Approval")) {
				// String resultTR2 = wf.Accept();
				// System.out.println("Printing" + resultTR2);
				System.out.println("Temination Approval Success:Level2");
			} else {
				System.out.println("Could not do 2nd level of term approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}
	
	
	
	public void ValidationEdit_TApprovalLevel3WFAdmin() throws Exception {

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent030 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus16 = TxtBoxContent030.getText();
		/*
		 * WebElement TxtBoxContent031 =
		 * driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"))
		 * ; String status16 = TxtBoxContent031.getText();
		 */
		if (tempContractStatus16.equalsIgnoreCase("Sent for Termination Approval")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("termination_approval")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
				System.out.println("Temination Approval Success:Level3");
			} catch (Exception e) {
				Reporter.log("An Error occured" + e);
			}
			// srn.getscreenshot();
			// EW.ContractStatus();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			WebElement cStatus = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String conStatus = cStatus.getText();
			WebElement TxtBoxContent031 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[2]"));
			String status16 = TxtBoxContent031.getText();
			System.out.println("Printing workflow flag:" + status16);
			if (status16.equalsIgnoreCase("Termination Approved by 3rd level")
					& conStatus.equalsIgnoreCase("Termination Approved")) {
				// String resultT3 = wf.Accept();
				// System.out.println("Printing" + resultT3);
			} else {
				System.out.println("Could not do 3rd level of approval");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}

	
	
	public void DeActivateEditWFAdmin() throws Exception {

		WebElement TxtBoxContent033 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus18 = TxtBoxContent033.getText();

		WebElement TxtBoxContent034 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status18 = TxtBoxContent034.getText();

		if (tempContractStatus18.equalsIgnoreCase("Termination Approved")) {
			try {
				// srn.getscreenshot();
				driver.findElement(By.id("deactivate")).click();

				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}

				//driver.findElement(By.id("sugar-message-prompt")).sendKeys("Accept");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen0-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured during deactivation" + e);

			}

			// srn.getscreenshot();

			EW.ContractStatus();
			System.out.println("Printing workflow flag:" + status18);

			if (status18.equalsIgnoreCase("Termination Approved by 3rd level")
					& tempContractStatus18.equalsIgnoreCase("Deactivated")) {
				Reporter.log("Contract Deactivated");
			} else {
				Reporter.log("Error while doing Deativation" + status18);
				org.testng.Assert.fail();
			}
		} else {
			System.out.println("Contract status invalid");
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	public void VerifyActivateButtonApprover1(String ContractName) throws Exception {
		logwf.WFLevel1login(WF1userName, WF1password);
		Thread.sleep(3000);

		CW.ViewContractNavigation();
		CW.ViewContractWorkflow(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		if (driver.findElement(By.id("activate_contract")).isDisplayed()) {
			System.out.println("Activate Button is displayed for approver 1");
		} else {
			System.out.println("Activate Button is not displayed for approver 1");
		}

	}

	public void VerifyActivateButtonApprover2(String ContractName) throws Exception {
		logwf.WFLevel2login(WF5userName, WF5password);
		Thread.sleep(3000);

		CW.ViewContractNavigation();
		CW.ViewContractWorkflow(ContractName);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		if (driver.findElement(By.id("activate_contract")).isDisplayed()) {
			System.out.println("Activate Button is displayed for approver 2");
		} else {
			System.out.println("Activate Button is not displayed for approver 2");
		}

	}

	/*Added by net - Start*/
	public void VerifyDetailsEditButtonApprover1(String ContractId) throws Exception
	{
		logwf.WFLevel1login(WF1userName, WF1password);
		Thread.sleep(3000);

		CW.ViewContractNavigation();
		CW.SelectContractId(globalContractId);
	
		if(driver.findElements(By.xpath(".//*[@id='edit_button']")).isEmpty()){
			System.out.println("Edit button will not be visible after contract gets Activated");
			System.out.println("RTC_Workflow_021 has passed");
		}
		else{
			System.out.println("RTC_Workflow_021 failed");
			Assert.fail("The Edit button should not be visible and it shows up now, so RTC_Workflow_021 failed");
		}
		
	}
	
	public void VerifyDetailsEditButtonApprover2(String ContractId) throws Exception
	{
		logwf.WFLevel2login(WF5userName, WF5password);
		Thread.sleep(3000);

		CW.ViewContractNavigation();
		CW.SelectContractId(globalContractId);
	
		if(driver.findElements(By.xpath(".//*[@id='edit_button']")).isEmpty()){
			System.out.println("Edit button will not be visible after contract gets Activated");
			System.out.println("RTC_Workflow_021 has passed");
		}
		else{
			System.out.println("RTC_Workflow_021 failed");
			Assert.fail("The Edit button should not be visible and it shows up now, so RTC_Workflow_021 failed");
		}
		
	}
	
	public void contractDataFillTermination(String ContractName, String ContractDesc,
			String  ContractEndDateTimeZone, String ServiceEndDateTimeZone, String BillingEndDateTimeZone) throws Exception {
		Thread.sleep(5000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		
		driver.findElement(By.id("name")).sendKeys(ContractName);
		driver.findElement(By.id("description")).sendKeys(ContractDesc);
		driver.findElement(By.id("gc_timezone_id1_c")).sendKeys(ContractEndDateTimeZone);
		driver.findElement(By.id("service_end_date_timezone_1")).sendKeys(ServiceEndDateTimeZone);
		driver.findElement(By.id("billing_end_date_timezone_1")).sendKeys(BillingEndDateTimeZone);
	}
	
	/*Added by net - End*/
	
	/* Added by deep logout - start */
	public void searchContract(String ContractId) throws InterruptedException {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("global_contract_id_basic")).clear();
		driver.findElement(By.id("name_basic")).clear();
		driver.findElement(By.id("global_contract_id_basic")).sendKeys(ContractId);
		// srn.getscreenshot();
		Thread.sleep(5000);
		driver.findElement(By.id("search_form_submit")).click();
		// driver.findElement(By.xpath("//table/tbody/tr[3]/td[4]/b/a")).click();
		Thread.sleep(5000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		if (driver.findElements(By.xpath(".//*[@id='MassUpdate']/table/tbody/tr[3]/td[4]/b/a")).size() > 0) {
			System.out.println("Contract displayed");
			Reporter.log("Contract displayed");
		} else {
			System.out.println("Contract not displayed");
			Reporter.log("Contract not displayed");
			Assert.fail();
		}
		// srn.getscreenshot();
	}

	public void logOut() throws InterruptedException {
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath(".//*[@id='userTab']")).click();
		driver.findElement(By.xpath(".//*[@id='fullmenu']/li[4]/a")).click();
		Thread.sleep(4000);
	}
	/* Added by deep logout - end */
	
	public void Validation_WithdrawNormalUserLevel1(String ContractName) throws Exception {

		//logwf.WFLevel1login(WF1userName, WF1password);
		Thread.sleep(3000);
		CW.ViewContractNavigation();
	
	CW.SelectContractId(globalContractId);
		
	
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent008 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus4 = TxtBoxContent008.getText();

		WebElement TxtBoxContent009 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status4 = TxtBoxContent009.getText();

		System.out.println("Printing contract status before level 1 withdraw:" + tempContractStatus4);
		System.out.println("Printing workflow flag before level 1 withdraw:" + status4);
		if (tempContractStatus4.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("withdrawapprovalrequest")).click();

				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Withdraw");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in withdraw contract by approver level1" + e);

			}

			// srn.getscreenshot();
			//EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

			Thread.sleep(30000);
			WebElement TxtBoxContentI = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusE = TxtBoxContentI.getText();

			WebElement TxtBoxContentJ = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusE = TxtBoxContentJ.getText();

			System.out.println("Printing contract status after level 1 withdraw:" + tempContractStatusE);
			System.out.println("Printing workflow flag after level 1 withdraw:" + statusE);
			if (statusE.equalsIgnoreCase("Contract in progress") & tempContractStatusE.equalsIgnoreCase("Draft")) {
				// String result1 = wf.Accept();
				// System.out.println("Printing" + result1);
				System.out.println("Wihdraw happened at Level 1 normal user");
			} else {
				System.out.println("Could not do Withdraw level 1 normal user");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}
	
	public void Validation_WithdrawNormalUserLevel2(String ContractName) throws Exception {

		//logwf.WFLevel1login(WF1userName, WF1password);
		Thread.sleep(3000);
		CW.ViewContractNavigation();

CW.SelectContractId(globalContractId);
		
	
		

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent008 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus4 = TxtBoxContent008.getText();

		WebElement TxtBoxContent009 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status4 = TxtBoxContent009.getText();

		System.out.println("Printing contract status before level 2 withdraw:" + tempContractStatus4);
		System.out.println("Printing workflow flag before level 2 withdraw:" + status4);
		if (tempContractStatus4.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("withdrawapprovalrequest")).click();

				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Withdraw");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in withdraw contract by approver level2" + e);

			}

			// srn.getscreenshot();
			//EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

			Thread.sleep(30000);
			WebElement TxtBoxContentI = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusE = TxtBoxContentI.getText();

			WebElement TxtBoxContentJ = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusE = TxtBoxContentJ.getText();

			System.out.println("Printing contract status after level 2 withdraw:" + tempContractStatusE);
			System.out.println("Printing workflow flag after level 2 withdraw:" + statusE);
			if (statusE.equalsIgnoreCase("Contract in progress") & tempContractStatusE.equalsIgnoreCase("Draft")) {
				// String result1 = wf.Accept();
				// System.out.println("Printing" + result1);
				System.out.println("Wihdraw happened at Level 2 normal user");
			} else {
				System.out.println("Could not do Withdraw level 2 normal user");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}
	
	public void Validation_WithdrawNormalUserLevel1_VersionUp(String ContractName) throws Exception {

		//logwf.WFLevel1login(WF1userName, WF1password);
		Thread.sleep(3000);
		CW.ViewContractNavigation();
CW.SelectContractId_VersionUp(globalContractId);
		

		

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent008 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus4 = TxtBoxContent008.getText();

		WebElement TxtBoxContent009 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status4 = TxtBoxContent009.getText();

		System.out.println("Printing contract status before level 1 withdraw:" + tempContractStatus4);
		System.out.println("Printing workflow flag before level 1 withdraw:" + status4);
		if (tempContractStatus4.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("withdrawapprovalrequest")).click();

				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Withdraw");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in withdraw contract by approver level1" + e);

			}

			// srn.getscreenshot();
			//EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

			Thread.sleep(30000);
			WebElement TxtBoxContentI = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusE = TxtBoxContentI.getText();

			WebElement TxtBoxContentJ = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusE = TxtBoxContentJ.getText();

			System.out.println("Printing contract status after level 1 withdraw:" + tempContractStatusE);
			System.out.println("Printing workflow flag after level 1 withdraw:" + statusE);
			if (statusE.equalsIgnoreCase("Contract in progress") & tempContractStatusE.equalsIgnoreCase("Draft")) {
				// String result1 = wf.Accept();
				// System.out.println("Printing" + result1);
				System.out.println("Wihdraw happened at Level 1 normal user");
			} else {
				System.out.println("Could not do Withdraw level 1 normal user");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}

	
	
	public void Validation_WithdrawNormalUserLevel2_VersionUp(String ContractName) throws Exception {

		//logwf.WFLevel1login(WF1userName, WF1password);
		Thread.sleep(3000);
		CW.ViewContractNavigation();
CW.SelectContractId(globalContractId);
		

		
	
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent008 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus4 = TxtBoxContent008.getText();

		WebElement TxtBoxContent009 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status4 = TxtBoxContent009.getText();

		System.out.println("Printing contract status before level 2 withdraw:" + tempContractStatus4);
		System.out.println("Printing workflow flag before level 2 withdraw:" + status4);
		if (tempContractStatus4.equalsIgnoreCase("Sent for Approval")) {

			try {
				// srn.getscreenshot();
				driver.findElement(By.id("withdrawapprovalrequest")).click();

				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Thread.sleep(3000);
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Withdraw");
				// srn.getscreenshot();

				driver.findElement(By.id("yui-gen1-button")).click();

				driver.switchTo().window(winHandleBefore);

			} catch (Exception e) {
				Reporter.log("An Error occured in withdraw contract by approver level2" + e);

			}

			// srn.getscreenshot();
			//EW.ContractStatus();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

			Thread.sleep(30000);
			WebElement TxtBoxContentI = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
			String tempContractStatusE = TxtBoxContentI.getText();

			WebElement TxtBoxContentJ = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
			String statusE = TxtBoxContentJ.getText();

			System.out.println("Printing contract status after level 2 withdraw:" + tempContractStatusE);
			System.out.println("Printing workflow flag after level 2 withdraw:" + statusE);
			if (statusE.equalsIgnoreCase("Contract in progress") & tempContractStatusE.equalsIgnoreCase("Draft")) {
				// String result1 = wf.Accept();
				// System.out.println("Printing" + result1);
				System.out.println("Wihdraw happened at Level 2 normal user");
			} else {
				System.out.println("Could not do Withdraw level 2 normal user");
			}
		} else {
			System.out.println("Contract status invalid");
		}
	}
	
	@DataProvider
	public Object[][] WorkflowData() {
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}

	@AfterTest
	public void close() {
		driver.quit();
	}

	}
