package com.stta.GCM;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class CreateContract extends SuiteGcmBase {

	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[] = null;
	static boolean TestCasePass = true;
	static int DataSet = -1;
	static boolean Testskip = false;
	static boolean Testfail = false;

	login log = new login();

	@BeforeTest
	public void checkCaseToRun() throws Exception {
		// Called init() function from SuiteBase class to Initialize .xls Files
		init();
		log.Run();
		// To set SuiteOne.xls file's path In FilePath Variable.
		FilePath = TestCaseListExcelOne;
		TestCaseName = this.getClass().getSimpleName();
		// SheetName to check CaseToRun flag against test case.
		SheetName = "TestCasesList";
		// Name of column In TestCasesList Excel sheet.
		ToRunColumnNameTestCase = "CaseToRun";
		// Name of column In Test Case Data sheets.
		ToRunColumnNameTestData = "DataToRun";
		// Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(TestCaseName + " : Execution started.");

		// To check test case's CaseToRun = Y or N In related excel sheet.
		// If CaseToRun = N or blank, Test case will skip execution. Else It
		// will be executed.
		if (!SuiteUtility.checkToRunUtility(FilePath, SheetName, ToRunColumnNameTestCase, TestCaseName)) {
			Add_Log.info(TestCaseName + " : CaseToRun = N for So Skipping Execution.");
			// To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
			// To throw skip exception for this test case.
			throw new SkipException(
					TestCaseName + "'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of " + TestCaseName);
		}
		// To retrieve DataToRun flags of all data set lines from related test
		// data sheet.
		TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);

		System.out.println(TestDataToRun);

	}

	@Test(dataProvider = "CreateContractData")
	public void CreateContractTest(String Test_CaseId, String ContractName, String ContractDesc, String ProductsID,
			String ProductOffering, String ContractStartDateTimeZone, String ContractEndDateTimeZone,
			String FactoryReferenceNumber, String SalesChannelCode, String SalesRepNameLatin, String SalesRepNameLocal,
			String DivisionLatin, String DivisionLocal, String TittleLatin, String TittleLocal, String Telephone,
			String ExtnNumber, String FaxNumber, String MobileNumber, String mailId, String ProductItem,
			String ProductSpecification, String ProductConfiguration, String ProductConfiguration1,
			String ProductConfiguration2, String ProductConfiguration3, String ProductConfiguration4,
			String ProductConfiguration5, String ProductConfiguration6, String ProductConfiguration7,
			String PaymentType, String chargeType, String chargeperiod, String Currency, String listprice,
			String ContractPrice, String chargeType1, String chargeperiod1, String Currency1, String listprice1,
			String contractprice1, String ProductInformation1, String ProductInformation2,
			String CustomerContractCurrency, String CustomerBillingCurrency, String IGCContractCurrency,
			String IGCBillingCurrency, String ServiceStartDateTimeZone, String ServiceEndDateTimeZone,
			String BillingStartDateTimeZone, String BillingEndDateTimeZone, String DiscountPercentage1,
			String DiscountPercentage2, String DiscountPrice1, String DiscountPrice2, String IGCSettlementPrice1,
			String IGCSettlementPrice2) throws Exception {

		DataSet++;

		// If found DataToRun = "N" for data set then execution will be skipped
		// for that data set.
		if (!TestDataToRun[DataSet].equalsIgnoreCase("Y")) {
			Add_Log.info(TestCaseName + " : DataToRun = N for data set line " + (DataSet + 1)
					+ " So skipping Its execution.");
			// If DataToRun = "N", Set Testskip=true.
			Testskip = true;
			throw new SkipException(
					"DataToRun for row number " + DataSet + " Is No Or Blank. So Skipping Its Execution.");
		}

		/***** Start Coding from here ***/

		Screen srn = new Screen();
		CreateContract contract = new CreateContract();

		// if(Test_CaseId.equalsIgnoreCase("TC_NJ-41_001/TC_NJ-41_018/TC_NJ-41_020/TC_NJ-41_004/TC_NJ-41_009/TC_NJ-41_011/TC_NJ-41_020/TC_NJ-41_013/TC_Df-37_008/TC_DF149-010")){
		if (Test_CaseId.equalsIgnoreCase(
				"RTC_001/RTC_005/RTC_007/RTC_008/RTC_009/RTC_011/RTC_012/RTC_014/RTC_016/RTC_021/RTC_030/RTC_057/RTC_108")) {
			try {
				contract.CreateContractNavigation();
				contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
						ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				Reporter.log("RTC_Contract_007 Passsed");
				driver.findElement(By.id("vat_no")).sendKeys("VAT Number");
				driver.findElement(By.id("SAVE_FOOTER")).click();
				contract.verifyAutoGenerated();

				Reporter.log("RTC_Contract_021 Passsed sales represenatative can be entered from GUI");

				// String vat =
				// driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[13]/td[4]")).getAttribute("agro");
				String vat = driver.findElement(By.id("vat_no")).getText();
				System.out.println("Value of vat" + vat);
				if (vat.isEmpty()) {
					System.out.println("VAT number field is not available ");
					Assert.fail("RTC_Contract_108 failed - VAT number field is not available ");

				} else {
					System.out.println("VAT number field is available and the vat number is  " + vat);
					Reporter.log(
							"RTC_Contract_108 passed - VAT number field is available and the vat number is  " + vat);
				}

				// Thread.sleep(3000);
				// contract.verifyAutoGenerated();
			} catch (Exception e) {
				Assert.fail("Some error occured " + e);
				e.printStackTrace();
			}
			// Thread.sleep(50000);

		} else if (Test_CaseId.equalsIgnoreCase("RTC_010")) {
			try {
				Thread.sleep(5000);
				contract.CreateContractNavigation();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("btn_contacts_gc_contracts_1_name")).click();
				createContract();
				System.out.println("Contracting account created from contract module");
				Thread.sleep(1000);
				// driver.switchTo().defaultContent();
				// driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// Thread.sleep(50000);
				Thread.sleep(1000);
				String accountName = driver.findElement(By.id("contacts_gc_contracts_1_name")).getAttribute("value");
				System.out.println(accountName);
				if (accountName.isEmpty()) {
					Assert.fail("Quick create of acccount failed - RTC_010 fail");
				} else {
					Reporter.log("RTC_010 executed successfully and the account name is" + accountName);
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Contracting account can't created from contract module");
				Reporter.log("Contracting account can't created from contract module");
				Assert.fail("An error occured while executing RTC_010" + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("RTC_013")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("btn_tech_account_name_1")).click();
				createContract();
				System.out.println("Technology Customer Account created from contract module");
				Thread.sleep(1000);
				String accountName = driver.findElement(By.id("tech_account_name_1")).getAttribute("value");
				System.out.println(accountName);
				if (accountName.isEmpty()) {
					Assert.fail("Quick create of acccount failed - RTC_013 fail");
				} else {
					Reporter.log("RTC_013 executed successfully and the account name is" + accountName);
				}
			} catch (Exception e) {
				System.out.println("Technology Customer Account can't created from contract module");
				Reporter.log("Technology Customer Account can't created from contract module");
				Assert.fail();
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_015")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("btn_bill_account_name_1")).click();
				createContract();
				Thread.sleep(1000);
				String accountName = driver.findElement(By.id("bill_account_name_1")).getAttribute("value");
				System.out.println(accountName);
				if (accountName.isEmpty()) {
					Assert.fail("Quick create of acccount failed - RTC_015 fail");
				} else {
					Reporter.log("RTC_015 executed successfully and the account name is" + accountName);
				}
				System.out.println("Billing Customer Account created from contract module");
			} catch (Exception e) {
				System.out.println("Billing Customer Account can't created from contract module");
				Reporter.log("Billing Customer Account can't created from contract module");
				Assert.fail();
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_017")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(10000);
				contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
						ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String tempPaymentType = driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[9]/div[2]/div/span"))
						.getText();
				System.out.println("PaymentType::" + tempPaymentType);
				String tempAccountHolderName = driver
						.findElement(By.xpath(".//*[@id='dd-1']/div/div[1]/div[2]/div/span")).getText().trim();
				String tempPassBookMark = driver.findElement(By.xpath(".//*[@id='dd-1']/div/div[1]/div[4]/div/span"))
						.getText();
				String tempPassBookNumber = driver.findElement(By.xpath(".//*[@id='dd-1']/div/div[2]/div[2]/div/span"))
						.getText();
				String tempPassBookNumberDisplay = driver
						.findElement(By.xpath(".//*[@id='dd-1']/div/div[2]/div[4]/div/span")).getText();

				if (tempPaymentType.equalsIgnoreCase("Postal Transfer")) {
					Reporter.log("The payment type selected is postal transfer");
					if (tempAccountHolderName.equalsIgnoreCase("Testing account")) {
						Reporter.log("Account holder name is selected ");
					} else {
						Assert.fail("Account holder name did not match-RTC_017 fail");
					}
					if (tempPassBookMark.equalsIgnoreCase("Mark")) {
						Reporter.log("System was able to key in passbook mark");
					} else {
						Assert.fail("Passbook mark  did not match-RTC_017 fail");
					}
					if (tempPassBookNumber.equalsIgnoreCase("123")) {
						Reporter.log("System was able to key in passbook number");

					} else {
						Assert.fail("Passbook number did not match-RTC_017 fail");
					}
					if (tempPassBookNumberDisplay.equalsIgnoreCase("Yes")) {
						Reporter.log("System was able to select the pass book number display");
					} else {
						Assert.fail("Passbook number display  did not match-RTC_017 fail ");
					}
				} else {
					Assert.fail("Payment type was not postal transfer -RTC_017 fail");
				}
				System.out.println("Payment type is Postal Transfer selected passed in contract line item");
			} catch (Exception e) {
				System.out.println("Payment type is Postal Transfer selected failed in contract line item" + e);
				Reporter.log("Payment type is Postal Transfer selected failed in contract line item");
				Assert.fail("****");
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_018")) {
			try {
				contract.CreateContractNavigation();
				contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
						ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String tempPaymentType = driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[9]/div[2]/div/span"))
						.getText();
				String tempCreditCardNumber = driver.findElement(By.xpath(".//*[@id='dd-1']/div/div/div[2]/div/span"))
						.getText();
				String tempExpirationDate = driver.findElement(By.xpath(".//*[@id='dd-1']/div/div/div[4]/div/span"))
						.getText();
				if (tempPaymentType.equalsIgnoreCase("Credit Card")) {
					Reporter.log("The payment type selected is Credid card");
					if (tempCreditCardNumber.equalsIgnoreCase("1234567896363636")) {
						Reporter.log("System was able to key in Credi-card Number");
					} else {
						Assert.fail("Credi-card Number did not match-RTC_018 fail ");
					}
					if (tempExpirationDate.equalsIgnoreCase("10/13")) {
						Reporter.log("System was able to key in Expiration Date");
					} else {
						Assert.fail("Expiration Date did not match-RTC_018 fail ");
					}
				} else {
					Assert.fail("Payment type was not Credit-card -RTC_018 fail");
				}
				System.out.println("Payment type is Credit Card selected passed in contract line item");
			} catch (Exception e) {
				System.out.println(
						"Payment type is Credit Card selected failed in contract line item - RTC_018 fail" + e);
				Assert.fail("Payment type is Credit Card selected failed in contract line item - RTC_018 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_019")) {
			try {
				contract.CreateContractNavigation();
				contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
						ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String tempPaymentType = driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[9]/div[2]/div/span"))
						.getText();
				String tempBankCode = driver.findElement(By.xpath(".//*[@id='dd-1']/div/div[1]/div[2]/div/span"))
						.getText();
				String tempBranchCode = driver.findElement(By.xpath(".//*[@id='dd-1']/div/div[1]/div[4]/div/span"))
						.getText();
				String tempDepositType = driver.findElement(By.xpath(".//*[@id='dd-1']/div/div[2]/div[2]/div/span"))
						.getText();
				String tempAccountNumber = driver.findElement(By.xpath(".//*[@id='dd-1']/div/div[2]/div[4]/div/span"))
						.getText();
				String tempPayeeContactPersonNameLatin = driver
						.findElement(By.xpath(".//*[@id='dd-1']/div/div[3]/div[2]/div/span")).getText();
				String tempPayeeContactPersonNameLocal = driver
						.findElement(By.xpath(".//*[@id='dd-1']/div/div[3]/div[4]/div/span")).getText();
				String tempPayeeTelephone = driver.findElement(By.xpath(".//*[@id='dd-1']/div/div[4]/div[2]/div/span"))
						.getText();
				String tempPayeeEmail = driver.findElement(By.xpath(".//*[@id='dd-1']/div/div[4]/div[4]/div/span"))
						.getText();
				String tempRemarks = driver.findElement(By.xpath(".//*[@id='dd-1']/div/div[5]/div[2]/div/span"))
						.getText();
				if (tempPaymentType.equalsIgnoreCase("General: Direct Deposit")) {
					if (tempBankCode.equalsIgnoreCase("1234")) {
						Reporter.log("System was able to key in Bank Code");
					} else {
						Assert.fail("Bank Code did not match-RTC_019 fail");
					}
					if (tempBranchCode.equalsIgnoreCase("12")) {
						Reporter.log("System was able to key in Branch Code");
					} else {
						Assert.fail("Branch Code did not match-RTC_019 fail");
					}
					if (tempDepositType.equalsIgnoreCase("Tax Payment (General)")) {
						Reporter.log("System was able to key in Deposit Type");
					} else {
						Assert.fail("Deposit Type did not match-RTC_019 fail");
					}
					if (tempAccountNumber.equalsIgnoreCase("1234567")) {
						Reporter.log("System was able to key in Account Number");
					} else {
						Assert.fail("Account Number did not match-RTC_019 fail");
					}
					if (tempPayeeContactPersonNameLatin.equalsIgnoreCase("ContactPerson Latin")) {
						Reporter.log("System was able to key in Contact Person Name Latin");
					} else {
						Assert.fail("Contact Person Name Latin did not match-RTC_019 fail");
					}
					if (tempPayeeContactPersonNameLocal.equalsIgnoreCase("ContactPerson Local")) {
						Reporter.log("System was able to key in Contact Person Name Local");
					} else {
						Assert.fail("Contact Person Name Local did not match-RTC_019 fail");
					}
					if (tempPayeeTelephone.equalsIgnoreCase("1234567890")) {
						Reporter.log("System was able to key in Telephone Number");
					} else {
						Assert.fail("Telephone Number did not match-RTC_019 fail");
					}
					if (tempPayeeEmail.equalsIgnoreCase("dummy@email.com")) {
						Reporter.log("System was able to key in E-mail");
					} else {
						Assert.fail("E-mail did not match-RTC_019 fail");
					}
					if (tempRemarks.equalsIgnoreCase("Remarks")) {
						Reporter.log("System was able to key in Remarks");
					} else {
						Assert.fail("Remarks did not match-RTC_019 fail");
					}
				} else {
					Assert.fail("Payment type was not Direct Deposit -RTC_019 fail");
				}
				System.out.println("Payment type is General: Direct Deposit selected passed in contract line item");
			} catch (Exception e) {
				System.out.println(
						"Payment type is General: Direct Deposit selected failed in contract line item - RTC_019 fail"
								+ e);
				Assert.fail(
						"Payment type is General: Direct Deposit selected failed in contract line item - RTC_019 fail:\t "
								+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_029")) {
			try {
				contract.CreateContractNavigation();
				contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
						ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				String windowHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// srn.getscreenshot();
				String tempErrorMessage = driver.findElement(By.xpath(".//*[@id='sugarMsgWindow']/div[2]")).getText();
				Robot r = new Robot();
				r.keyPress(KeyEvent.VK_ENTER);
				r.keyRelease(KeyEvent.VK_ENTER);
				driver.switchTo().window(windowHandleBefore);
				if (tempErrorMessage.equalsIgnoreCase("One Base specification to be included in contract")) {
					Reporter.log("The Error Message was present");
				} else {
					System.out.println("The Error Message was not present");
					Assert.fail("The Error Message was not present -RTC_029 fail");
				}
				System.out.println("Product Specification Type Base is mandatory check passed");
			} catch (Exception e) {
				System.out.println("Product Specification Type Base is mandatory check failed - RTC_029 fail:\t" + e);
				Assert.fail("Product Specification Type Base is mandatory check failed - RTC_029 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_034")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
						ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				/* Code for verifying discount price */
				driver.findElement(By.id("pp_discount_1_160459")).clear();
				driver.findElement(By.id("pp_discount_1_160459")).sendKeys(DiscountPercentage1);
				// driver.findElement(By.id("pp_igc_settlement_price_1_160461")).click();
				// String contractPrice1 =
				// driver.findElement(By.xpath(".//*[@id='spn_customer_contract_price_1_160459']")).getText();
				if (!driver.findElement(By.id("pp_discountflag_1_160459")).isSelected()) {
					driver.findElement(By.id("pp_discountflag_1_160459")).click();
				}
				driver.findElement(By.id("pp_discount_1_160460")).clear();
				driver.findElement(By.id("pp_discount_1_160460")).sendKeys(DiscountPercentage2);
				if (!driver.findElement(By.id("pp_discountflag_1_160460")).isSelected()) {
					driver.findElement(By.id("pp_discountflag_1_160460")).click();
				}
				String contractPrice1 = driver.findElement(By.xpath(".//*[@id='spn_customer_contract_price_1_160459']"))
						.getText();
				if (contractPrice1.equals("0.000")) {
					Reporter.log("Discount is text field with checkbox check passed for Minimum Monthly Charges");
					System.out.println("Discount is text field with checkbox check passed for Minimum Monthly Charges");
				} else {
					System.out.println(
							"Discount is text field with checkbox check failed for Minimum Monthly Charges - RTC_034 fail");
					Assert.fail(
							"Discount is text field with checkbox check failed forr Minimum Monthly Charges - RTC_034 fail");
				}
				String contractPrice2 = driver.findElement(By.xpath(".//*[@id='spn_customer_contract_price_1_160460']"))
						.getText();
				if (contractPrice2.equals("0.000")) {
					Reporter.log(
							"Discount is text field with checkbox check passed for Monthly Unit Price per Unit of Additional Standard Change");
					System.out.println(
							"Discount is text field with checkbox check passed for Monthly Unit Price per Unit of Additional Standard Change");
				} else {
					System.out.println(
							"Discount is text field with checkbox check failed for Monthly Unit Price per Unit of Additional Standard Change - RTC_034 fail");
					Assert.fail(
							"Discount is text field with checkbox check failed for Monthly Unit Price per Unit of Additional Standard Change - RTC_034 fail");
				}
			} catch (Exception e) {
				System.out.println("Error in checking discount is text field with checkbox - RTC_034 fail:\t" + e);
				Assert.fail("Error in checking discount is text field with checkbox - RTC_034 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_035/RTC_038")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(10000);
				contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
						ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("pp_discount_1_160459")).clear();
				driver.findElement(By.id("pp_discount_1_160459")).sendKeys(DiscountPrice1);
				if (driver.findElement(By.xpath(".//*[@id='pp_discountflag_1_160459']")).isSelected()) {
					driver.findElement(By.xpath(".//*[@id='pp_discountflag_1_160459']")).click();
				}
				driver.findElement(By.id("pp_igc_settlement_price_1_160459")).click();
				String contractPrice1 = driver.findElement(By.xpath(".//*[@id='spn_customer_contract_price_1_160459']"))
						.getText();
				if (!contractPrice1.equals(
						driver.findElement(By.xpath(".//*[@id='product_price_1_160459']/table/tbody/tr[5]/td[2]/span"))
								.getText())) {
					Reporter.log(
							"Discount is text field without checkbox check & Customer Contract Price is available check passed for Minimum Monthly Charges");
					System.out.println(
							"Discount is text field without checkbox check & Customer Contract Price is available check passed for Minimum Monthly Charges");
				} else {
					Reporter.log(
							"Discount is text field without checkbox check & Customer Contract Price is available check failed for Minimum Monthly Charges - RTC_035 fail");
					System.out.println(
							"Discount is text field without checkbox check & Customer Contract Price is available check failed for Minimum Monthly Charges - RTC_035 fail");
				}
				driver.findElement(By.id("pp_discount_1_160460")).clear();
				driver.findElement(By.id("pp_discount_1_160460")).sendKeys(DiscountPrice2);
				if (driver.findElement(By.xpath(".//*[@id='pp_discountflag_1_160460']")).isSelected()) {
					driver.findElement(By.xpath(".//*[@id='pp_discountflag_1_160460']")).click();
				}
				driver.findElement(By.id("pp_igc_settlement_price_1_160460")).click();
				String contractPrice2 = driver.findElement(By.xpath(".//*[@id='spn_customer_contract_price_1_160460']"))
						.getText();
				if (!contractPrice2.equals(
						driver.findElement(By.xpath(".//*[@id='product_price_1_160460']/table/tbody/tr[5]/td[2]/span"))
								.getText())) {
					Reporter.log(
							"Discount is text field without checkbox check & Customer Contract Price is available check passed for Monthly Unit Price per Unit of Additional Standard Change");
					System.out.println(
							"Discount is text field without checkbox check & Customer Contract Price is available check passed for Monthly Unit Price per Unit of Additional Standard Change");
				} else {
					Reporter.log(
							"Discount is text field without checkbox check & Customer Contract Price is available check failed for Monthly Unit Price per Unit of Additional Standard Change - RTC_038 fail");
					System.out.println(
							"Discount is text field without checkbox & Customer Contract Price is available check check failed for Monthly Unit Price per Unit of Additional Standard Change - RTC_038 fail");
				}
			} catch (Exception e) {
				Reporter.log(
						"Error in checking discount is text field without checkbox and error in checking Customer Contract Prise - RTC_035/RTC_038 fail:\t"
								+ e);
				Assert.fail(
						"Error in checking discount is text field without checkbox and error in checking Customer Contract Prise - RTC_035/RTC_038 fail:\t"
								+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_039")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(10000);
				contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
						ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("pp_igc_settlement_price_1_160459")).clear();
				driver.findElement(By.id("pp_igc_settlement_price_1_160459")).sendKeys(IGCSettlementPrice1);
				driver.findElement(By.id("pp_igc_settlement_price_1_160460")).clear();
				driver.findElement(By.id("pp_igc_settlement_price_1_160460")).sendKeys(IGCSettlementPrice2);
				driver.findElement(By.id("pp_igc_settlement_price_1_160459")).click();
				String tempIGCSettlementPrice1 = driver.findElement(By.id("pp_igc_settlement_price_1_160459"))
						.getAttribute("value");
				String tempIGCSettlementPrice2 = driver.findElement(By.id("pp_igc_settlement_price_1_160460"))
						.getAttribute("value");
				if (tempIGCSettlementPrice1.equals("10000.000")) {
					Reporter.log("Inter-Group-Company Settlement Price field for Minimum Monthly Charges is available");
				} else {
					System.out.println(
							"Inter-Group-Company Settlement Price field for  Minimum Monthly Chargesis not available - RTC_039 fail");
					Assert.fail(
							"Inter-Group-Company Settlement Price field for Minimum Monthly Charges is not available - RTC_039 fail");
				}
				if (tempIGCSettlementPrice2.equals("10000.000")) {
					Reporter.log("Inter-Group-Company Settlement Price field for Monthly Unit Price is available");
				} else {
					System.out.println(
							"Inter-Group-Company Settlement Price field for Monthly Unit Price is not available - RTC_039 fail");
					Assert.fail(
							"Inter-Group-Company Settlement Price field for Monthly Unit Price is not available - RTC_039 fail");
				}
				Reporter.log("Inter-Group-Company Settlement Price field is available check is passed");
			} catch (Exception e) {
				System.out.println(
						"Error in checking Inter-Group-Company Settlement Price field is available or not - RTC_039 fail:\t"
								+ e);
				Assert.fail(
						"Error in checking Inter-Group-Company Settlement Price field is available or not - RTC_039 fail:\t"
								+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_046")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(10000);
				// Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Select payment = new Select(driver.findElement(By.id("payment_type_1")));
				payment.selectByVisibleText(PaymentType);
				Thread.sleep(2000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				if (PaymentType.equals("Account Transfer")) {
					String accountHolderName = driver
							.findElement(By.xpath(".//*[@id='dd_1']/div/div[1]/div[2]/div/span/div")).getText();
					if (!accountHolderName.isEmpty()) {
						Reporter.log("Account Holer Name mandatory filed check for Account Transfer passed");
					} else {
						System.out.println(
								"Account Holer Name mandatory filed check for Account Transfer failed - RTC_046 fail");
						Assert.fail(
								"Account Holer Name mandatory filed check for Account Transfer failed - RTC_046 fail");
					}
					String bankCode = driver.findElement(By.xpath(".//*[@id='dd_1']/div/div[2]/div[2]/div/span/div"))
							.getText();
					if (!bankCode.isEmpty()) {
						Reporter.log("Bank Codee mandatory filed check for Account Transfer passed");
					} else {
						System.out
								.println("Bank Code mandatory filed check for Account Transfer failed - RTC_046 fail");
						Assert.fail("Bank Code mandatory filed check for Account Transfer failed - RTC_046 fail");
					}
					String accountNo = driver.findElement(By.xpath(".//*[@id='dd_1']/div/div[3]/div[2]/div/span/div"))
							.getText();
					if (!accountNo.isEmpty()) {
						Reporter.log("Account Number mandatory filed check for Account Transfer passed");
					} else {
						System.out.println(
								"Account Number mandatory filed check for Account Transfer failed - RTC_046 fail");
						Assert.fail("Account Number mandatory filed check for Account Transfer failed - RTC_046 fail");
					}
					String depositType = driver.findElement(By.xpath(".//*[@id='dd_1']/div/div[1]/div[4]/div/span/div"))
							.getText();
					if (!depositType.isEmpty()) {
						Reporter.log("Deposit Type mandatory filed check for Account Transfer passed");
					} else {
						System.out.println(
								"Deposit Type mandatory filed check for Account Transfer failed - RTC_046 fail");
						Assert.fail("Deposit Type mandatory filed check for Account Transfer failed - RTC_046 fail");
					}
					String branchCode = driver.findElement(By.xpath(".//*[@id='dd_1']/div/div[2]/div[4]/div/span/div"))
							.getText();
					if (!branchCode.isEmpty()) {
						Reporter.log("Branch Code mandatory filed check for Account Transfer passed");
					} else {
						System.out.println(
								"Branch Code mandatory filed check for Account Transfer failed - RTC_046 fail");
						Assert.fail("Branch Code mandatory filed check for Account Transfer failed - RTC_046 fail");
					}
					String accountNoDisp = driver
							.findElement(By.xpath(".//*[@id='dd_1']/div/div[3]/div[4]/div/span/div")).getText();
					if (!accountNoDisp.isEmpty()) {
						Reporter.log("Account Number Display mandatory filed check for Account Transfer passed");
					} else {
						System.out.println(
								"Account Number Display mandatory filed check for Account Transfer failed - RTC_046 fail");
						Assert.fail(
								"Account Number Display mandatory filed check for Account Transfer failed - RTC_046 fail");
					}
				} else if (PaymentType.equals("Postal Transfer")) {
					String accountHolderName = driver
							.findElement(By.xpath(".//*[@id='dd_1']/div/div[1]/div[2]/div/span/div")).getText();
					if (!accountHolderName.isEmpty()) {
						Reporter.log("Account Number mandatory filed check for Postal Transfer passed");
					} else {
						System.out.println(
								"Account Number mandatory filed check for Postal Transfer failed - RTC_046 fail");
						Assert.fail("Account Number mandatory filed check for Postal Transfer failed - RTC_046 fail");
					}
					String postalPassbookNo = driver
							.findElement(By.xpath(".//*[@id='dd_1']/div/div[2]/div[2]/div/span/div")).getText();
					if (!postalPassbookNo.isEmpty()) {
						Reporter.log("Postal Passbook Number mandatory filed check for Postal Transfer passed");
					} else {
						System.out.println(
								"Postal Passbook Number mandatory filed check for Postal Transfer failed - RTC_046 fail");
						Assert.fail(
								"Postal Passbook Number mandatory filed check for Postal Transfer failed - RTC_046 fail");
					}
					String postalPassbookMark = driver
							.findElement(By.xpath(".//*[@id='dd_1']/div/div[1]/div[4]/div/span/div")).getText();
					if (!postalPassbookMark.isEmpty()) {
						Reporter.log("Postal Passbook Mark mandatory filed check for Postal Transfer passed");
					} else {
						System.out.println(
								"Postal Passbook Mark mandatory filed check for Postal Transfer failed - RTC_046 fail");
						Assert.fail(
								"Postal Passbook Mark mandatory filed check for Postal Transfer failed - RTC_046 fail");
					}
				} else if (PaymentType.equals("Credit Card")) {
					String creditCardNo = driver.findElement(By.xpath(".//*[@id='dd_1']/div/div/div[2]/div/span/div"))
							.getText();
					if (!creditCardNo.isEmpty()) {
						Reporter.log("Credit-Card Number mandatory filed check for Credit Card passed");
					} else {
						System.out.println(
								"Credit-Card Number mandatory filed check for Credit Card failed - RTC_046 fail");
						Assert.fail("Credit-Card Number mandatory filed check for Credit Card failed - RTC_046 fail");
					}
					String expirationDate = driver.findElement(By.xpath(".//*[@id='dd_1']/div/div/div[4]/div/span/div"))
							.getText();
					if (!expirationDate.isEmpty()) {
						Reporter.log("Expiration Date mandatory filed check for Credit Card passed");
					} else {
						System.out
								.println("Expiration Date mandatory filed check for Credit Card failed - RTC_046 fail");
						Assert.fail("Expiration Date mandatory filed check for Credit Card failed - RTC_046 fail");
					}
				}
				Reporter.log("Payment type inside fields is mandatory check passed");
			} catch (Exception e) {
				System.out.println("Mandatory field check for payment type was failed - RTC_046 fail:\t" + e);
				Assert.fail("Mandatory field check for payment type was failed - RTC_046 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_047")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Select custContractCurr = new Select(driver.findElement(By.id("cust_contract_currency_1")));
				custContractCurr.selectByValue(CustomerContractCurrency);
				Thread.sleep(2000);
				Select cutContCurr = new Select(driver.findElement(By.id("cust_contract_currency_1")));
				String contCurr = cutContCurr.getFirstSelectedOption().getText();
				if (contCurr.toLowerCase().indexOf(CustomerContractCurrency.toLowerCase()) != -1) {
					Reporter.log("Customer Contract Currency display ISO 4217 Code JPY check passed");
				} else {
					System.out.println(
							"Customer Contract Currency display ISO 4217 Code JPY check failed - RTC_047 fail");
					Assert.fail("Customer Contract Currency display ISO 4217 Code JPY check failed - RTC_047 fail");
				}
			} catch (Exception e) {
				System.out.println(
						"Customer Contract Currency display ISO 4217 Code JPY check failed failed - RTC_047 fail:\t"
								+ e);
				Assert.fail("Customer Contract Currency display ISO 4217 Code JPY check failed failed - RTC_047 fail:\t"
						+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_049")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(10000);
				// Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Select custBillingCurr = new Select(driver.findElement(By.id("cust_billing_currency_1")));
				custBillingCurr.selectByValue(CustomerBillingCurrency);
				Thread.sleep(2000);
				String billCurr = custBillingCurr.getFirstSelectedOption().getText();
				if (billCurr.toLowerCase().indexOf(CustomerBillingCurrency.toLowerCase()) != -1) {
					Reporter.log("Customer Billing Currency display ISO 4217 Code JPY check passed");
				} else {
					System.out
							.println("Customer Billing Currency display ISO 4217 Code JPY check failed - RTC_049 fail");
					Assert.fail("Customer Billing Currency display ISO 4217 Code JPY check failed - RTC_049 fail");
				}
			} catch (Exception e) {
				System.out.println(
						"Customer Billing Currency display ISO 4217 Code JPY check failed - RTC_049 fail:\t" + e);
				Assert.fail("Customer Billing Currency display ISO 4217 Code JPY check failed - RTC_049 fail:\t" + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("RTC_051")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Select igcContCurr = new Select(driver.findElement(By.id("igc_contract_currency_1")));
				igcContCurr.selectByValue(IGCContractCurrency);
				Thread.sleep(2000);
				String igcCurr = igcContCurr.getFirstSelectedOption().getText();
				if (igcCurr.toLowerCase().indexOf(IGCContractCurrency.toLowerCase()) != -1) {
					Reporter.log("IGC Contract Currency display ISO 4217 Code JPY check passed");
				} else {
					System.out.println("IGC Contract Currency display ISO 4217 Code JPY check failed - RTC_051 fail");
					Assert.fail("IGC Contract Currency display ISO 4217 Code JPY check failed - RTC_051 fail");
				}
			} catch (Exception e) {
				System.out
						.println("IGC Contract Currency display ISO 4217 Code JPY check failed - RTC_051 fail:/t" + e);
				Assert.fail("IGC Contract Currency display ISO 4217 Code JPY check failed - RTC_051 fail:/t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_053")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Select igcBillingCurr = new Select(driver.findElement(By.id("igc_billing_currency_1")));
				igcBillingCurr.selectByValue(IGCBillingCurrency);
				Thread.sleep(2000);
				String igcBillCurr = igcBillingCurr.getFirstSelectedOption().getText();
				if (igcBillCurr.toLowerCase().indexOf(IGCBillingCurrency.toLowerCase()) != -1) {
					Reporter.log("IGC Contract Currency display ISO 4217 Code JPY check passed");
				} else {
					System.out.println("IGC Contract Currency display ISO 4217 Code JPY check failed - RTC_053 fail");
					Assert.fail("IGC Contract Currency display ISO 4217 Code JPY check failed - RTC_053 fail");
				}
			} catch (Exception e) {
				System.out.println("IGC Billing Currency display ISO 4217 Code JPY check failed - RTC_053 fail:/t" + e);
				Assert.fail("IGC Billing Currency display ISO 4217 Code JPY check failed - RTC_053 fail:/t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_056")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sales_company_id")).click();
				/*
				 * Select salesCompany = new
				 * Select(driver.findElement(By.id("sales_company_id")));
				 * salesCompany.
				 * selectByVisibleText("NTT Communications India Private Ltd.");
				 */
				Reporter.log("Regular users can see NTT Com Group companies in Create Contract");
				System.out.println("Regular users can see NTT Com Group companies in Create Contract");
			} catch (Exception e) {
				System.out.println(
						"Regular users can't see NTT Com Group companies in Create Contract - RTC_056 fail:\t" + e);
				Assert.fail("Regular users can't see NTT Com Group companies in Create Contract - RTC_056 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_060")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				/*
				 * Alert alert = driver.switchTo().alert(); alert.accept();
				 */
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("add_invoice_subgroup_1")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("invoice_groupname")).sendKeys("Test");
				driver.findElement(By.id("invoice_groupdesc")).sendKeys("Test Desc");
				driver.findElement(By.xpath("html/body/div[7]/div[3]/div/button")).click();
				driver.switchTo().window(winHandleBefore);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Select invoiceSubGroup = new Select(driver.findElement(By.id("li_invoice_subgroup_1")));
				String invSubGroup = invoiceSubGroup.getFirstSelectedOption().getText();
				if (invSubGroup.equals("Test")) {
					Reporter.log("New group created from contract screen");
				} else {
					System.out.println("New group can't be created from contract screen - RTC_060 fail");
					Assert.fail("New group can't be created from contract screen - RTC_060 fail");
				}
				// driver.findElement(By.id("SAVE_FOOTER")).click();
			} catch (Exception e) {
				System.out.println("New group can't be created from contract screen - RTC_060 fail:\t" + e);
				Assert.fail("New group can't be created from contract screen - RTC_060 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_062/RTC_072")) {
			try {
				// contract.CreateContractNavigation();
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(10000);
				contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
						ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				contract.verifySecondNewLineItemFields();
				Select productSpecification1 = new Select(driver.findElement(By.id("product_specification_2")));
				productSpecification1.selectByVisibleText("Managed Windows Server - on ECL2.0 (Add-on Optional)");
				contract.verifyThirdNewLineItemFields();
				Select productSpecification2 = new Select(driver.findElement(By.id("product_specification_3")));
				productSpecification2.selectByVisibleText("Managed Linux - on ECL2.0 (Add-on Optional)");
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				contract.verifyProductSpecificaionTab();
			} catch (Exception e) {
				System.out.println(
						"Error in checking Billing scheme One-stop values related to billing is copied to later line items or  Product specification name is not displayed on closed line item tab failed - RTC-062/RTC-072 fail:\t"
								+ e);
				Assert.fail(
						"Error in checking Billing scheme One-stop values related to billing is copied to later line items or  Product specification name is not displayed on closed line item tab failed - RTC-062/RTC-072 fail:\t"
								+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_063")) {
			try {
				contract.CreateContractNavigation();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Select billType = new Select(driver.findElement(By.id("billing_type")));
				billType.selectByVisibleText("Separate");
				contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
						ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				// contract.verifySecondNewLineItemFields();
				contract.verifySeperateSecondNewLineItemFields();
				Select productSpecification1 = new Select(driver.findElement(By.id("product_specification_2")));
				productSpecification1.selectByVisibleText("Managed Windows Server - on ECL2.0 (Add-on Optional)");
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Reporter.log(
						"Billing scheme 'Separate' is selected in Contract header,then value related to billing fields of 1st line item is not copied to 2 nd line item or later line items passed");
				System.out.println(
						"Billing scheme 'Separate' is selected in Contract header,then value related to billing fields of 1st line item is not copied to 2 nd line item or later line items passed");
			} catch (Exception e) {
				System.out.println(
						"Billing scheme 'Separate' is selected in Contract header,then value related to billing fields of 1st line item is not copied to 2 nd line item or later line items failed - RTC-063 fail:\t"
								+ e);
				Assert.fail(
						"Billing scheme 'Separate' is selected in Contract header,then value related to billing fields of 1st line item is not copied to 2 nd line item or later line items failed - RTC-063 fail:\t"
								+ e);
			}
		} /*
			 * else if(Test_CaseId.equalsIgnoreCase("RTC_074")){ try{
			 * contract.CreateContractNavigation();
			 * contract.ContractDataFill(ContractName, ContractDesc,ProductsID,
			 * ProductOffering, ContractStartDateTimeZone,
			 * ContractEndDateTimeZone, FactoryReferenceNumber,
			 * SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
			 * DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
			 * Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
			 * ProductSpecification, ProductItem, ProductConfiguration,
			 * ProductConfiguration1, ProductConfiguration2,
			 * ProductConfiguration3, ProductConfiguration4,
			 * ProductConfiguration5, ProductConfiguration6,
			 * ProductConfiguration7, PaymentType, ProductInformation1,
			 * ProductInformation2, ServiceStartDateTimeZone,
			 * ServiceEndDateTimeZone, BillingStartDateTimeZone,
			 * BillingEndDateTimeZone);
			 * driver.findElement(By.id("SAVE_FOOTER")).click();
			 * Thread.sleep(5000); driver.findElement(By.xpath(
			 * ".//*[@id='list_subpanel_gc_lineitem_documents_rowid']/table/thead/tr[1]/td/div/ul/li/a"
			 * )).click(); Thread.sleep(50000); boolean edit =
			 * driver.findElement(By.xpath(
			 * ".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).
			 * isDisplayed(); boolean remove = driver.findElement(By.xpath(
			 * ".//*[@id='contract_doc_list_items']/tr/td[8]/ul/li/a")).
			 * isDisplayed(); if(edit && remove){
			 * System.out.println("Edit & Remove buttons are displayed");
			 * Reporter.log("Edit & Remove buttons are displayed"); } else {
			 * System.out.println("Edit & Remove buttons are not displayed");
			 * Reporter.log("Edit & Remove buttons  are not displayed");
			 * Assert.fail(); } System.out.
			 * println("User can able to Attach a document in contract header or line item When the contract status is in Draft"
			 * ); Reporter.
			 * log("User can able to Attach a document in contract header or line item When the contract status is in Draft"
			 * ); } catch(Exception e){ e.printStackTrace(); System.out.
			 * println("User can't able to Attach a document in contract header or line item When the contract status is in Draft \t"
			 * + e); Reporter.
			 * log("User can't able to Attach a document in contract header or line item When the contract status is in Draft \t"
			 * +e); Assert.fail(); } }
			 */ else if (Test_CaseId.equalsIgnoreCase("RTC_079")) {
			try {
				contract.CreateContractNavigation();
				contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
						ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
						SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
						Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(5000);
				driver.findElement(By.id("sendforapproval")).click();
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {

					driver.switchTo().window(handle);
				}
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("sugar-message-prompt")).sendKeys("Test approval");
				driver.findElement(By.id("yui-gen1-button")).click();
				driver.switchTo().window(winHandleBefore);
				Thread.sleep(3000);
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String teams = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[5]/td[4]")).getText();
				System.out.println("The team is " + teams);
				if (teams.equals("NTTJ SO, GMOne 1st approver, GMOne 2nd approver, GMOne 3rd approver")) {
					System.out.println(
							"All the Approver teams are linked to the contract as soon as submitted for Approval i.e when status changes to Sent for Approval");
					Reporter.log(
							"All the Approver teams are linked to the contract as soon as submitted for Approval i.e when status changes to Sent for Approval");
				} else {

					Assert.fail(
							"All the Approver teams are not linked to the contract as soon as submitted for Approval i.e when status changes to Sent for Approval - RTC-079 fail");
				}
			} catch (Exception e) {
				System.out.println(
						"All the Approver teams are not linked to the contract as soon as submitted for Approval i.e when status changes to Sent for Approval - RTC-079 fail:\t"
								+ e);
				Assert.fail(
						"All the Approver teams are not linked to the contract as soon as submitted for Approval i.e when status changes to Sent for Approval - RTC-079 fail:\t"
								+ e);
			}
		} /*
			 * else if(Test_CaseId.equalsIgnoreCase("RTC_Contract_1116")){
			 * contract.CreateContractNavigation(); Thread.sleep(10000);
			 * driver.switchTo().defaultContent();
			 * driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			 * driver.findElement(By.id("gc_timezone_id_c")).click();
			 * driver.findElement(By.id("btn_contacts_gc_contracts_1_name")).
			 * click(); String winHandleBefore = driver.getWindowHandle(); for
			 * (String handle : driver.getWindowHandles()) {
			 * driver.switchTo().window(handle); }
			 * driver.switchTo().defaultContent();
			 * driver.findElement(By.id("search_form_clear")).click();
			 * driver.findElement(By.id("last_name_advanced")).sendKeys(
			 * "AutomationTestPersonNameLatin");
			 * driver.findElement(By.id("search_form_submit")).click();
			 * Thread.sleep(3000); driver.findElement(By.xpath(
			 * "html/body/table[4]/tbody/tr[3]/td[2]/a")).click();
			 * driver.switchTo().window(winHandleBefore);
			 * driver.switchTo().defaultContent();
			 * driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			 * driver.findElement(By.id("gc_timezone_id_c")).click();
			 * 
			 * }
			 */

		/*
		 * else if(Test_CaseId.equalsIgnoreCase("RTC_023")){ try{
		 * contract.ViewContractNavigation();
		 * contract.SelectContract(ContractName); driver.findElement(By.xpath(
		 * ".//*[@id='list_subpanel_gc_lineitem_documents_rowid']/table/thead/tr[1]/td/div/ul/li/a"
		 * )).click(); Thread.sleep(3000); String winHandleBefore =
		 * driver.getWindowHandle(); for (String handle :
		 * driver.getWindowHandles()) { driver.switchTo().window(handle); }
		 * 
		 * System.out.println(
		 * "Contract status is draft,document can have create functionality passed"
		 * ); } catch(Exception e){ System.out.println(""+e); Reporter.log(
		 * "Contract status is draft,document can have create functionality failed "
		 * ); Assert.fail(); } }
		 */
		else if (Test_CaseId.equalsIgnoreCase("RTC_Test")) {
			try {
				contract.ViewContractNavigation();
				contract.SelectContract(ContractName);
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				List<WebElement> rows = driver
						.findElements(By.xpath(".//table[@class='list view']/tbody[@id='contract_doc_list_items']/tr"));
				System.out.println("Total number of rows :" + rows.size());
				// contract.CreateContractNavigation();
				// System.out.println("Producr
				// Specification...."+ProductSpecification);
				/*
				 * contract.ContractDataFill(ContractName, ContractDesc,
				 * ProductsID, ProductOffering, ContractStartDateTimeZone,
				 * ContractEndDateTimeZone, FactoryReferenceNumber,
				 * SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal,
				 * DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
				 * Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId,
				 * ProductItem, ProductSpecification, ProductConfiguration,
				 * ProductConfiguration1, ProductConfiguration2,
				 * ProductConfiguration3, ProductConfiguration4,
				 * ProductConfiguration5, ProductConfiguration6,
				 * ProductConfiguration7, PaymentType, ProductInformation1,
				 * ProductInformation2, ServiceStartDateTimeZone,
				 * ServiceEndDateTimeZone, BillingStartDateTimeZone,
				 * BillingEndDateTimeZone);
				 */
				/*
				 * Thread.sleep(5000); driver.switchTo().defaultContent();
				 * driver
				 * .switchTo().frame(driver.findElement(By.id("bwc-frame")));
				 * 
				 * Select cSelect3 = new
				 * Select(driver.findElement(By.id("product_offering")));
				 * cSelect3.selectByVisibleText(ProductOffering);
				 * Thread.sleep(2000);
				 * 
				 * Select cSelect = new
				 * Select(driver.findElement(By.id("product_specification_1")));
				 * cSelect.selectByVisibleText(ProductSpecification);
				 */
				// cSelect3.selectByValue("929");
				/*
				 * driver.findElement(By.id("SAVE_FOOTER")).click();
				 * Thread.sleep(3000); Robot r = new Robot();
				 * r.keyPress(KeyEvent.VK_ENTER);
				 * r.keyRelease(KeyEvent.VK_ENTER);
				 * Reporter.log("The Error Message was present");
				 * System.out.println
				 * ("Product Specification Type Base is mandatory check passed"
				 * );
				 */
			} catch (Exception e) {
				System.out.println("Product Specification Type Base is mandatory check failed" + e);
				Reporter.log("Product Specification Type Base is mandatory Error message was not present");
				Assert.fail();
			}

		}

		/*
		 * else if (Test_CaseId.equalsIgnoreCase("RTC_002")) { try {
		 * contract.ViewContractNavigation();
		 * contract.SelectContract(ContractName); System.out
		 * .println("Contract can be search by contract name passed"); } catch
		 * (Exception e) { System.out
		 * .println("Contract can be search by contract name failed");
		 * Reporter.log("Contract can be search by contract name failed");
		 * Assert.fail(); } }
		 */ else if (Test_CaseId.equalsIgnoreCase("TC_NJ-41_013")) {
			contract.CreateContractNavigation();
			contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
					ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
					SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
					Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
					ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
					ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
					PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
					ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-41_014")) {

			contract.CreateContractNavigation();
			contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
					ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
					SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
					Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
					ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
					ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
					PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
					ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-41_015")) {
			contract.CreateContractNavigation();
			contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
					ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
					SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
					Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
					ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
					ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
					PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
					ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-41_016")) {
			contract.CreateContractNavigation();
			contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
					ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
					SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
					Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
					ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
					ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
					PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
					ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);

		} else if (Test_CaseId.equalsIgnoreCase("TC_CreateContract_002")) {

			contract.ViewContractNavigation();
			contract.FileUploadContractHeader(ContractName);
			contract.FileUploadLineItems(ContractName);

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-41_006/TC_NJ-41_008")) {
			contract.CreateContractNavigation();
			contract.MandetoryCheckPayment(PaymentType);

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-41_002")) {
			contract.ViewContractNavigation();
			contract.SelectContract(ContractName);
			driver.findElement(By.id("edit_button")).click();
			contract.ClearData(PaymentType);
			contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
					ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
					SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
					Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
					ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
					ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
					PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
					ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-41_019")) {

			contract.CreateContractNavigation();
			driver.findElement(By.xpath("//div/div[3]/table/tbody/tr/td/div[2]/button")).click();
			contract.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering,
					ContractStartDateTimeZone, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode,
					SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal,
					Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductSpecification, ProductItem,
					ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
					ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
					PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
					ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
			driver.findElement(By.id("btn_product_name_2")).click();

			String winHandleBefore4 = driver.getWindowHandle();

			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
			}

			driver.findElement(By.id("name_advanced")).clear();
			driver.findElement(By.id("name_advanced")).sendKeys(ProductItem);
			driver.findElement(By.id("search_form_submit")).click();
			driver.findElement(By.xpath("//tbody/tr[3]/td[1]/a")).click();
			driver.switchTo().window(winHandleBefore4);

			DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yyyy");

			LocalDateTime now = LocalDateTime.now();
			LocalDateTime then = now.plusDays(10);

			driver.findElement(By.id("service_start_date_2")).sendKeys(now.format(format));
			driver.findElement(By.id("service_end_date_2")).sendKeys(then.format(format));
			driver.findElement(By.id("billing_start_date_2")).sendKeys(now.format(format));
			driver.findElement(By.id("billing_end_date_2")).sendKeys(then.format(format));

			driver.findElement(By.id("service_start_date_timezone_2")).sendKeys("IST");
			driver.findElement(By.id("service_end_date_timezone_2")).sendKeys("IST");
			driver.findElement(By.id("billing_start_date_timezone_2")).sendKeys("IST");
			driver.findElement(By.id("billing_end_date_timezone_2")).sendKeys("IST");
			driver.findElement(By.id("btn_tech_account_name_2")).click();

			String winHandleBefore5 = driver.getWindowHandle();
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
			}

			driver.findElement(By.id("last_name_advanced")).clear();
			driver.findElement(By.id("last_name_advanced")).sendKeys("latin-technology");
			driver.findElement(By.id("search_form_submit")).click();

			driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
			driver.switchTo().window(winHandleBefore5);
			driver.findElement(By.id("btn_bill_account_name_2")).click();

			String winHandleBefore6 = driver.getWindowHandle();
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
			}

			driver.findElement(By.id("last_name_advanced")).clear();
			driver.findElement(By.id("last_name_advanced")).sendKeys("test 3/2/2016");
			driver.findElement(By.id("search_form_submit")).click();
			driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
			driver.switchTo().window(winHandleBefore6);

			Select cSelect6 = new Select(driver.findElement(By.id("cust_contract_currency_2")));
			cSelect6.selectByValue("AFN");

			Select cSelect7 = new Select(driver.findElement(By.id("cust_billing_currency_2")));
			cSelect7.selectByValue("AFN");

			Select cSelect8 = new Select(driver.findElement(By.id("igc_contract_currency_2")));
			cSelect8.selectByValue("AFN");

			Select cSelect9 = new Select(driver.findElement(By.id("igc_billing_currency_2")));
			cSelect9.selectByValue("AFN");

			Select cSelect10 = new Select(driver.findElement(By.id("customer_billing_method_2")));
			cSelect10.selectByValue("eCSS_Billing");

			Select cSelect11 = new Select(driver.findElement(By.id("igc_settlement_method_2")));
			cSelect11.selectByValue("Quarterly_Settlement");

			Select cSelect3 = new Select(driver.findElement(By.id("product_specification_2")));
			cSelect3.selectByValue("333");

			if (PaymentType.equalsIgnoreCase("General: Direct Deposit")) {
				Select oSelect = new Select(driver.findElement(By.id("payment_type_2")));

				oSelect.selectByValue("00");

				driver.findElement(By.id("dd_bank_code_2")).sendKeys("Bank007");
				driver.findElement(By.id("dd_branch_code_2")).sendKeys("dd_branch_code_1");

				Select oSelect1 = new Select(driver.findElement(By.id("dd_deposit_type_2")));
				oSelect1.selectByVisibleText("Tax Payment (General)");

				driver.findElement(By.id("dd_account_no_2")).sendKeys("1234567896363636");
				driver.findElement(By.id("dd_person_name_1_2")).sendKeys("Atul Dandin");
				driver.findElement(By.id("dd_person_name_2_2")).sendKeys("LocalAtul Dandin");
				driver.findElement(By.id("dd_payee_tel_no_2")).sendKeys("1234567890");
				driver.findElement(By.id("dd_payee_email_address_2")).sendKeys("dummy@email.com");
				driver.findElement(By.id("dd_remarks_2")).sendKeys("Remarks");

				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(5000);
				Reporter.log("Multiple line items were created");

				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();

			} else if (PaymentType.equalsIgnoreCase("Account Transfer")) {

				Select oSelect = new Select(driver.findElement(By.id("payment_type_2")));

				oSelect.selectByValue("10");

				driver.findElement(By.id("at_account_no_2")).sendKeys("12345678963636363");
				driver.findElement(By.id("at_name_2")).sendKeys("Atul Dandin");
				driver.findElement(By.id("at_bank_code_2")).sendKeys("1234567890");
				driver.findElement(By.id("at_branch_code_2")).sendKeys("1234567890");

				Select oSelect1 = new Select(driver.findElement(By.id("at_deposit_type_2")));
				oSelect1.selectByVisibleText("Ordinary Deposit");

				Select oSelect2 = new Select(driver.findElement(By.id("at_account_no_display_2")));
				oSelect2.selectByVisibleText("Yes");

				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(5000);
				Reporter.log("Multiple line items were created");

				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();

			} else if (PaymentType.equalsIgnoreCase("Postal Transfer")) {
				Select oSelect = new Select(driver.findElement(By.id("payment_type_2")));

				oSelect.selectByValue("30");

				// driver.findElement(By.id("pt_postal_passbook_1")).click();
				// driver.findElement(By.id("pt_postal_passbook_1")).clear();
				driver.findElement(By.id("pt_postal_passbook_2")).sendKeys("123");
				driver.findElement(By.id("pt_name_2")).sendKeys("Atul Dandin");
				driver.findElement(By.id("pt_postal_passbook_mark_2")).sendKeys("Remarks");

				Select oSelect2 = new Select(driver.findElement(By.id("pt_postal_passbook_no_display_2")));
				oSelect2.selectByVisibleText("Yes");

				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(5000);
				Reporter.log("Multiple line items were created");

				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();

			} else if (PaymentType.equalsIgnoreCase("Credit Card")) {
				Select oSelect = new Select(driver.findElement(By.id("payment_type_2")));

				oSelect.selectByValue("60");
				driver.findElement(By.id("cc_credit_card_no_2")).sendKeys("12345678963636363");
				driver.findElement(By.id("cc_expiration_date_2")).sendKeys("10/13");

				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(5000);
				Reporter.log("Multiple line items were created");

				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();

			}

		} else if (Test_CaseId.equalsIgnoreCase("TC_DF-38-001")) {
			contract.ViewContractNavigation();
			contract.SelectContract(ContractName);
			driver.findElement(By.id("arrow-dd-1-4")).click();

			String chargeTypeGUI = driver
					.findElement(By.xpath("//div[2]/div[5]/ul/li/div[2]/div/div/div[1]/div[3]/div/span")).getText();
			String chargeperiodGUI = driver
					.findElement(By.xpath("//div[2]/div[5]/ul/li/div[2]/div/div/div[1]/div[5]/div/span")).getText();
			String CurrencyGUI = driver
					.findElement(By.xpath("//div[2]/div[5]/ul/li/div[2]/div/div/div[1]/div[7]/div/span")).getText();
			String listpriceGUI = driver
					.findElement(By.xpath("//div[2]/div[5]/ul/li/div[2]/div/div/div[1]/div[9]/div/span")).getText();
			String contractpriceGUI = driver
					.findElement(By.xpath("//div[2]/div[5]/ul/li/div[2]/div/div/div[1]/div[13]/div/span")).getText();
			String chargeType1GUI = driver
					.findElement(By.xpath("//div[2]/div[5]/ul/li/div[2]/div/div/div[2]/div[3]/div")).getText();
			String chargeperiod1GUI = driver
					.findElement(By.xpath("//div[2]/div[5]/ul/li/div[2]/div/div/div[2]/div[5]/div/span")).getText();
			String Currency1GUI = driver
					.findElement(By.xpath("//div[2]/div[5]/ul/li/div[2]/div/div/div[2]/div[7]/div/span")).getText();
			String listprice1GUI = driver
					.findElement(By.xpath("//div[2]/div[5]/ul/li/div[2]/div/div/div[2]/div[9]/div")).getText();
			String contractprice1GUI = driver
					.findElement(By.xpath("//div[2]/div[5]/ul/li/div[2]/div/div/div[2]/div[13]/div/span")).getText();

			Reporter.log("" + chargeTypeGUI);
			Reporter.log("" + chargeperiodGUI);
			Reporter.log("" + CurrencyGUI);
			Reporter.log("" + listpriceGUI);
			Reporter.log("" + contractpriceGUI);
			Reporter.log("" + chargeType1GUI);
			Reporter.log("" + chargeperiod1GUI);
			Reporter.log("" + Currency1GUI);
			Reporter.log("" + listprice1GUI);
			Reporter.log("" + contractprice1GUI);

			if (chargeTypeGUI.equalsIgnoreCase(chargeType)) {
				Reporter.log("Charge type" + chargeTypeGUI + " is matching with expected charge type" + chargeType);

			} else {
				Assert.fail();
			}

			if (chargeperiodGUI.equalsIgnoreCase(chargeperiod)) {
				Reporter.log("Charge period in GUI = " + chargeperiodGUI + " is matching with expected charge period"
						+ chargeperiod);

			} else {
				Assert.fail();
			}
			if (CurrencyGUI.equalsIgnoreCase(Currency)) {
				Reporter.log("Curreny in GUI is" + CurrencyGUI + " is matching with expected currency " + Currency);

			} else {
				Assert.fail();
			}

			/*
			 * if(listpriceGUI.equalsIgnoreCase(listprice)){
			 * Reporter.log("List price is"+listpriceGUI+
			 * " is matching with expected list price"+listprice);
			 * 
			 * }else{ Assert.fail(); }
			 * 
			 * if(contractpriceGUI.equalsIgnoreCase(ContractPrice)){
			 * Reporter.log("Contract Price is"+contractpriceGUI+
			 * " is matching with expected Contract Price"+ContractPrice);
			 * 
			 * }else{ Assert.fail(); }
			 */

			if (chargeType1GUI.equalsIgnoreCase(chargeType1)) {
				Reporter.log("Charge type1" + chargeType1GUI + " is matching with expected charge type" + chargeType1);

			} else {
				Assert.fail();
			}

			if (chargeperiod1GUI.equalsIgnoreCase(chargeperiod1)) {
				Reporter.log("Charge period in GUI = " + chargeperiod1GUI + " is matching with expected charge period"
						+ chargeperiod1);

			} else {
				Assert.fail();
			}
			if (Currency1GUI.equalsIgnoreCase(Currency1)) {
				Reporter.log("Curreny in GUI is" + Currency1GUI + " is matching with expected currency " + Currency1);

			} else {
				Assert.fail();
			}

			/*
			 * if(listprice1GUI.equalsIgnoreCase(listprice1)){
			 * Reporter.log("List price is"+listprice1GUI+
			 * " is matching with expected list price"+listprice1);
			 * 
			 * }else{ Assert.fail(); }
			 * 
			 * if(contractprice1GUI.equalsIgnoreCase(contractprice1)){
			 * Reporter.log("contract price is"+contractprice1GUI+
			 * " is matching with expected contract price"+contractprice1);
			 * 
			 * }else{ Assert.fail(); }
			 */

		} else if (Test_CaseId.equalsIgnoreCase("TC_DF-38-002/TC_DF-149_008")) {
			contract.CreateContractNavigation();
			driver.findElement(By.id("SAVE_FOOTER")).click();
			contract.MandetoryCheck();

		} else if (Test_CaseId.equalsIgnoreCase("TC_DF-149_001/TC_DF-149_002")) {
			contract.ViewContractNavigation();
			contract.SelectContract(ContractName);
			Thread.sleep(5000);
			driver.findElement(By.id("edit_button")).click();
			// driver.findElement(By.id("product_offering")).sendKeys("ProductOffering");
			Select cSelect1 = new Select(driver.findElement(By.id("product_offering")));
			cSelect1.selectByValue("483");
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);

			Thread.sleep(5000);

			Select cSelect2 = new Select(driver.findElement(By.id("product_specification_2")));
			cSelect2.selectByValue("48");

			Select cSelect3 = new Select(driver.findElement(By.id("cust_contract_currency_2")));
			cSelect3.selectByValue("JPY");

			Select cSelect4 = new Select(driver.findElement(By.id("cust_billing_currency_2")));
			cSelect4.selectByValue("JPY");

			Select cSelect5 = new Select(driver.findElement(By.id("igc_contract_currency_2")));
			cSelect5.selectByValue("JPY");

			Select cSelect6 = new Select(driver.findElement(By.id("igc_billing_currency_2")));
			cSelect6.selectByValue("JPY");

			Select cSelect7 = new Select(driver.findElement(By.id("customer_billing_method_2")));
			cSelect7.selectByValue("eCSS_Billing");

			Select cSelect8 = new Select(driver.findElement(By.id("igc_settlement_method_2")));
			cSelect8.selectByValue("Quarterly_Settlement");

			driver.findElement(By.id("btn_tech_account_name_2")).click();

			String winHandleBefore5 = driver.getWindowHandle();
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
			}
			driver.findElement(By.id("last_name_advanced")).clear();
			driver.findElement(By.id("last_name_advanced")).sendKeys("latin-technology");
			driver.findElement(By.id("search_form_submit")).click();

			driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
			driver.switchTo().window(winHandleBefore5);
			driver.findElement(By.id("btn_bill_account_name_2")).click();

			String winHandleBefore6 = driver.getWindowHandle();
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
			}
			driver.findElement(By.id("last_name_advanced")).clear();
			driver.findElement(By.id("last_name_advanced")).sendKeys("test 3/2/2016");
			driver.findElement(By.id("search_form_submit")).click();

			driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
			driver.switchTo().window(winHandleBefore6);

			Select oSelect = new Select(driver.findElement(By.id("payment_type_2")));

			oSelect.selectByValue("60");
			driver.findElement(By.id("cc_credit_card_no_2")).sendKeys("12345678963636363");
			driver.findElement(By.id("cc_expiration_date_2")).sendKeys("10/13");

			driver.findElement(By.id("pi_role_value_2_265")).sendKeys("dummy");
			driver.findElement(By.id("pi_role_value_2_266")).sendKeys("Dummy");
			driver.findElement(By.id("SAVE_FOOTER")).click();
			driver.findElement(By.id("btn_custom_view_change_log")).click();

			String winHandleBefore7 = driver.getWindowHandle();
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
			}

			driver.manage().window().maximize();

			String ProdOffer = driver.findElement(By.xpath("//tbody/tr/td/table[2]/tbody/tr[2]/td[2]")).getText();

			if (ProdOffer.equalsIgnoreCase("Product Offering")) {

				String OldValue = driver.findElement(By.xpath("//tr/td/table[2]/tbody/tr[2]/td[3]/div")).getText();
				if (OldValue.equalsIgnoreCase("Global Management One ECL1.0 Option")) {
					Reporter.log("Old Value match");

				} else {

					Assert.fail("Old Value did not match");
				}

				String NewValue = driver.findElement(By.xpath("//tbody/tr/td/table[2]/tbody/tr[2]/td[4]/div"))
						.getText();
				if (NewValue.equalsIgnoreCase("Cloud(n)")) {
					Reporter.log("New  Value match");

				} else {

					Assert.fail("New  Value did not match");
				}

			} else {
				Reporter.log("Product offering did not match");
			}
			driver.switchTo().window(winHandleBefore7);

		} else if (Test_CaseId.equalsIgnoreCase("TC_DF-149_003/TC_DF-149_004")) {
			// Before running the test case change the old value and new value
			// in if condition
			// Chnage the drop down index value

			contract.ViewContractNavigation();
			contract.SelectContract(ContractName);
			driver.findElement(By.id("edit_button")).click();
			Select cSelect5 = new Select(driver.findElement(By.id("product_specification_1")));
			cSelect5.selectByValue("339");
			driver.findElement(By.id("SAVE_FOOTER")).click();
			driver.findElement(By.id("arrow-li-1")).click();
			driver.findElement(By.id("btn_view_change_log")).click();

			String winHandleBefore7 = driver.getWindowHandle();
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
			}

			driver.manage().window().maximize();

			String oldValue = driver.findElement(By.xpath("//tr/td/table[2]/tbody/tr[2]/td[3]/div")).getText();
			if (oldValue.equalsIgnoreCase("Managed Linux - on ECL1.0")) {
				Reporter.log("Old value Match");
			} else {
				Assert.fail("Old value did not match");
			}

			String newValue = driver.findElement(By.xpath("//tr/td/table[2]/tbody/tr[2]/td[4]/div")).getText();
			if (newValue.equalsIgnoreCase("Managed Oracle Advanced - RAC on ECL1.0 (Add-on Optional)")) {
				Reporter.log("new value Match");
			} else {
				Assert.fail("new value did not match");
			}

			driver.switchTo().window(winHandleBefore7);

		} else if (Test_CaseId.equalsIgnoreCase("TC_DF-149_006")) {
			contract.CreateContractNavigation();
			// contract.ContractDataFill(ContractName, ContractDesc, ProductsID,
			// ProductOffering, FactoryReferenceNumber, SalesChannelCode,
			// SalesRepNameLatin, SalesRepNameLocal, DivisionLatin,
			// DivisionLocal, TittleLatin, TittleLocal, Telephone, ExtnNumber,
			// FaxNumber, MobileNumber, mailId, ProductItem,
			// ProductSpecification, ProductConfiguration,
			// ProductConfiguration1, ProductConfiguration2,
			// ProductConfiguration3, ProductConfiguration4,
			// ProductConfiguration5, ProductConfiguration6,
			// ProductConfiguration7, PaymentType, ProductInformation1,
			// ProductInformation2);
			Select cSelect3 = new Select(driver.findElement(By.id("product_offering")));
			// cSelect3.selectByValue(ProductOffering);
			cSelect3.selectByValue("929");
			driver.findElement(By.id("SAVE_FOOTER")).click();
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			Reporter.log("The Error Message was present");

		} else if (Test_CaseId.equalsIgnoreCase("TC_DF-149_007")) {
			// Do not change any values
			contract.CreateContractNavigation();

			Select cSelect3 = new Select(driver.findElement(By.id("product_offering")));
			// cSelect3.selectByValue(ProductOffering);
			cSelect3.selectByValue("930");
			Select cSelect5 = new Select(driver.findElement(By.id("product_specification_1")));
			cSelect5.selectByValue("344");
			String value = driver.findElement(By.id("product_config_1_561")).getText();
			if (value.isEmpty()) {
				Assert.fail("Default value is not selected");
			} else {
				Reporter.log("The default value is selected");
			}

			// pi_role_value_1_861

		} else if (Test_CaseId.equalsIgnoreCase("TC_DF-149_11/TC_DF-149_12")) {
			contract.ViewContractNavigation();
			contract.SelectContract(ContractName);
			driver.findElement(By.id("edit_button")).click();
			driver.findElement(By.id("pi_role_value_1_861")).clear();
			driver.findElement(By.id("pi_role_value_1_861")).sendKeys("Value Edit");
			driver.findElement(By.id("pi_role_value_1_862")).clear();
			driver.findElement(By.id("pi_role_value_1_862")).sendKeys("Value edit	");
			driver.findElement(By.id("SAVE_FOOTER")).click();
			driver.findElement(By.id("btn_view_change_log")).click();
			String winHandleBefore6 = driver.getWindowHandle();
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
			}

			String value = driver.findElement(By.xpath("//tr/td/table[2]/tbody/tr[2]/td[4]/div")).getText();
			if (value.equalsIgnoreCase("Value Edit")) {
				Reporter.log("New value matching ");
			} else {
				Reporter.log("New value did not matching ");
				Assert.fail("New value did not match");
			}

			driver.switchTo().window(winHandleBefore6);

		} else if (Test_CaseId.equalsIgnoreCase("TC_DF-149_13/TC_DF-149_16/TC_DF-149_17/TC_DF-149_19/TC_DF-149_20")) {
			contract.CreateContractNavigation();
			Select cSelect3 = new Select(driver.findElement(By.id("product_offering")));
			cSelect3.selectByValue("483");
			Select cSelect5 = new Select(driver.findElement(By.id("product_specification_1")));
			cSelect5.selectByValue("48");

			driver.findElement(By.id("SAVE_FOOTER")).click();
			String value = driver
					.findElement(By.xpath("//li/div[2]/div/div[13]/ul/li[2]/div/div/span/div[1]/div[2]/div/span/div"))
					.getText();
			if (value.isEmpty()) {
				Reporter.log("validation missing for product information:");
				Assert.fail();
			} else {
				Reporter.log("Validation for product information: " + value);

			}

			value = driver
					.findElement(
							By.xpath("//ul[2]/li/div[2]/div/div[13]/ul/li[2]/div/div/span/div[2]/div[2]/div/span/div"))
					.getText();

			if (value.isEmpty()) {
				Reporter.log("validation missing for product information: ");
				Assert.fail();
			} else {
				Reporter.log("Validation for product information: " + value);

			}
			driver.findElement(By.id("CANCEL_HEADER")).click();

			Alert alert = driver.switchTo().alert();
			alert.accept();
			contract.CreateContractNavigation();
			// contract.ContractDataFill(ContractName, ContractDesc, ProductsID,
			// ProductOffering, FactoryReferenceNumber, SalesChannelCode,
			// SalesRepNameLatin, SalesRepNameLocal, DivisionLatin,
			// DivisionLocal, TittleLatin, TittleLocal, Telephone, ExtnNumber,
			// FaxNumber, MobileNumber, mailId, ProductItem,
			// ProductSpecification, ProductConfiguration,
			// ProductConfiguration1, ProductConfiguration2,
			// ProductConfiguration3, ProductConfiguration4,
			// ProductConfiguration5, ProductConfiguration6,
			// ProductConfiguration7, PaymentType, ProductInformation1,
			// ProductInformation2);

			Select cSelect4 = new Select(driver.findElement(By.id("product_offering")));
			cSelect4.selectByValue("929");
			Select cSelect6 = new Select(driver.findElement(By.id("product_specification_1")));
			cSelect6.selectByValue("333");

			if (driver.findElement(By.id("pp_discount_1_159810")).isEnabled()) {
				Reporter.log("Discount feild is available when ICB falg is no");

			} else {
				Assert.fail("Discount feild is not available when ICB falg is no");
			}

			if (driver.findElement(By.id("pp_discountflag_1_159810")).isEnabled()) {
				Reporter.log("Discount feild checkbox is available when ICB falg is no");
			} else {
				Assert.fail("Discount feild checkbox is not available when ICB falg is no");
			}

			driver.navigate().refresh();
			Alert alert1 = driver.switchTo().alert();
			alert1.accept();
			contract.ViewContractNavigation();
			contract.SelectContract(ContractName);
			driver.findElement(By.id("edit_button")).click();

			if (driver.findElement(By.id("pp_discount_1_159810")).isEnabled()) {
				Reporter.log("Discount feild is Editable");

			} else {
				Assert.fail("Discount feild is not editable");
			}

			driver.findElement(By.id("pp_discount_1_159810")).sendKeys("100.000");
			driver.findElement(By.id("SAVE_FOOTER")).click();
			driver.findElement(By.id("btn_view_change_log")).click();
			String winHandleBefore6 = driver.getWindowHandle();
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
			}

			value = driver.findElement(By.xpath("//tr/td/table[2]/tbody/tr[2]/td[4]")).getText();
			if (value.equalsIgnoreCase("100.00")) {
				Reporter.log("The discount is edited and audited");
			} else {
				Assert.fail("The discount edited value and new value did not match");
			}
			driver.switchTo().window(winHandleBefore6);

		} else if (Test_CaseId.equalsIgnoreCase("TC_DF-149_23/TC_DF-149_24/TC_DF-149_25/TC_DF-149_26/TC_DF-149_27")) {
			contract.CreateContractNavigation();
			Select cSelect3 = new Select(driver.findElement(By.id("product_offering")));
			cSelect3.selectByValue("931");
			Select cSelect5 = new Select(driver.findElement(By.id("product_specification_1")));
			cSelect5.selectByValue("347");

			if (driver.findElement(By.id("pp_customer_contract_price_1_160049")).isEnabled()) {
				Reporter.log("Contract Price is editable");

			} else {
				Assert.fail("Contract price not editable");
			}

			if (driver.findElement(By.id("pp_igc_settlement_price_1_160049")).isEnabled()) {
				Reporter.log("IGC settlement  Price is editable");

			} else {
				Assert.fail("IGC settlement  price not editable");
			}

			contract.dataFillNonICB(ContractName, ContractDesc, ProductsID, ProductOffering, FactoryReferenceNumber,
					SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin,
					TittleLocal, Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, ProductItem,
					ProductSpecification, ProductConfiguration, ProductConfiguration1, ProductConfiguration2,
					ProductConfiguration3, ProductConfiguration4, ProductConfiguration5, ProductConfiguration6,
					ProductConfiguration7, PaymentType, ProductInformation1, ProductInformation2);
			driver.findElement(By.id("edit_button")).click();

			driver.findElement(By.id("pp_customer_contract_price_1_160049")).sendKeys("200.000");
			driver.findElement(By.id("pp_igc_settlement_price_1_160049")).sendKeys("200.000");

			driver.findElement(By.id("SAVE_FOOTER")).click();

			driver.findElement(By.id("btn_view_change_log")).click();

			String winHandleBefore6 = driver.getWindowHandle();
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
			}

			String value = driver.findElement(By.xpath("//tr/td/table[2]/tbody/tr[3]/td[4]/div")).getText();
			if (value.equalsIgnoreCase("200")) {
				Reporter.log("IGP value  matched");
			} else {
				Assert.fail("IGP value did not match");
			}

			value = driver.findElement(By.xpath("//td/table[2]/tbody/tr[2]/td[4]/div")).getText();
			if (value.equalsIgnoreCase("200")) {
				Reporter.log("CCP value  matched");
			} else {
				Assert.fail("CCP value did not match");
			}

			driver.switchTo().window(winHandleBefore6);

		} else if (Test_CaseId.equalsIgnoreCase("TC_DF-149_21")) {

			contract.CreateContractNavigation();
			Select cSelect3 = new Select(driver.findElement(By.id("product_offering")));
			cSelect3.selectByValue("483");
			Select cSelect5 = new Select(driver.findElement(By.id("product_specification_1")));
			cSelect5.selectByValue("48");

			String value = driver.findElement(By.xpath("//ul/li[3]/div/div/div[2]/div/div[1]/div[13]/div/span"))
					.getText();
			if (value.isEmpty()) {
				Assert.fail("Contract Price Empty");
			} else {
				Reporter.log("Contract Price" + value);
			}

			Reporter.log("Invalid test case ID");
		}

		/*** Coding ends here ****/
		/* Added by net - Start */

		if (Test_CaseId.equalsIgnoreCase("RTC_Contract_116/RTC_Contract_117/RTC_Contract_118")) {
			try {
				CreateContractNavigation();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				Select timezoneCntrAll = new Select(driver.findElement(By.id("gc_timezone_id_c")));
				int timezoneCntrListAll = timezoneCntrAll.getOptions().size();
				System.out.println("Timezone list before : " + timezoneCntrListAll);

				driver.findElement(By.id("btn_contacts_gc_contracts_1_name")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.findElement(By.id("search_form_clear")).click();
				driver.findElement(By.id("last_name_advanced")).sendKeys("AutomationTestPersonNameLatin");
				driver.findElement(By.id("search_form_submit")).click();
				Thread.sleep(3000);
				driver.findElement(By.xpath("html/body/table[4]/tbody/tr[3]/td[2]/a")).click();
				driver.switchTo().window(winHandleBefore);

				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Select timezoneContrSelect = new Select(driver.findElement(By.id("gc_timezone_id_c")));
				int timezonelistContrSelect = timezoneContrSelect.getOptions().size();
				System.out.println("Timezone list after : " + timezonelistContrSelect);

				if (timezoneCntrListAll != timezonelistContrSelect) {
					System.out.println("The country is changed and timezone listed for Contracting customer account");
					Assert.assertTrue(TestCasePass, "RTC_Contract_116 passed");
				} else {
					System.out.println("Error : Timezones are same for Contracting customer account");
					Assert.fail("RTC_Contract_116 failed");
				}

				Select timezoneBillAll = new Select(driver.findElement(By.id("billing_start_date_timezone_1")));
				int timezoneBillListAll = timezoneBillAll.getOptions().size();
				System.out.println("Timezone list before : " + timezoneBillListAll);

				driver.findElement(By.id("btn_bill_account_name_1")).click();
				winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.findElement(By.id("search_form_clear")).click();
				driver.findElement(By.id("last_name_advanced")).sendKeys("BillingAutomationTestNameLatin");
				driver.findElement(By.id("search_form_submit")).click();
				Thread.sleep(3000);
				driver.findElement(By.xpath("html/body/table[4]/tbody/tr[3]/td[2]/a")).click();
				driver.switchTo().window(winHandleBefore);

				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Select timezoneBillSelect = new Select(driver.findElement(By.id("billing_start_date_timezone_1")));
				int timezonelistBillSelect = timezoneBillSelect.getOptions().size();
				System.out.println("Timezone list after : " + timezonelistBillSelect);

				if (timezoneBillListAll != timezonelistBillSelect) {
					System.out.println("The country is changed and timezone listed for Billing customer account");
					Assert.assertTrue(TestCasePass, "RTC_Contract_117 passed");
				} else {
					System.out.println("Error : Timezones are same for Billing customer account");
					Assert.fail("RTC_Contract_117 failed");
				}

				Select timezoneTechAll = new Select(driver.findElement(By.id("service_start_date_timezone_1")));
				int timezoneTechListAll = timezoneTechAll.getOptions().size();
				System.out.println("Timezone list before : " + timezoneTechListAll);

				driver.findElement(By.id("btn_tech_account_name_1")).click();
				winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.findElement(By.id("search_form_clear")).click();
				driver.findElement(By.id("last_name_advanced")).sendKeys("TechnologyAutomationTestNameLatin");
				driver.findElement(By.id("search_form_submit")).click();
				Thread.sleep(3000);
				driver.findElement(By.xpath("html/body/table[4]/tbody/tr[3]/td[2]/a")).click();
				driver.switchTo().window(winHandleBefore);

				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Select timezoneTechSelect = new Select(driver.findElement(By.id("service_start_date_timezone_1")));
				int timezonelistTechSelect = timezoneTechSelect.getOptions().size();
				System.out.println("Timezone list after : " + timezonelistTechSelect);

				if (timezoneTechListAll != timezonelistTechSelect) {
					System.out.println("The country is changed and timezone listed for Technology customer account");
					Assert.assertTrue(TestCasePass, "RTC_Contract_118 passed");
				} else {
					System.out.println("Error : Timezones are same for Technology customer account");
					Assert.fail("RTC_Contract_118 failed");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Assert.fail("RTC_Contract_118 failed");
			}
		}

		/* Added by net - End */

	}

	public void CreateContractNavigation() throws Exception {
		try {
			// srn.getscreenshot();
			// driver.findElement(By.xpath("//*[@id='moduleTabExtraMenuAll']/a/em")).click();
			driver.switchTo().defaultContent();
			driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[4]/span/button")).click();
			// WebElement element =
			// driver.findElement(By.linkText("Contracts"));
			// Actions action = new Actions(driver);
			// action.moveToElement(element).build().perform();
			// driver.findElement(By.id("CreateContractsAll")).click();
			driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[4]/span/div/ul/li[1]/a")).click();
			// srn.getscreenshot();
			Reporter.log("Create functionality worked fine - RTC_Contract_005 pass ");

		} catch (Exception e) {
			Assert.fail("Contract creation failed RTC_Contract_005 failed" + e);
		}

	}

	public void ContractDataFill(String ContractName, String ContractDesc, String ProductsID, String ProductOffering,
			String ContractStartDateTimeZone, String ContractEndDateTimeZone, String FactoryReferenceNumber,
			String SalesChannelCode, String SalesRepNameLatin, String SalesRepNameLocal, String DivisionLatin,
			String DivisionLocal, String TittleLatin, String TittleLocal, String Telephone, String ExtnNumber,
			String FaxNumber, String MobileNumber, String mailId, String ProductItem, String ProductSpecification,
			String ProductConfiguration, String ProductConfiguration1, String ProductConfiguration2,
			String ProductConfiguration3, String ProductConfiguration4, String ProductConfiguration5,
			String ProductConfiguration6, String ProductConfiguration7, String PaymentType, String ProductInformation1,
			String ProductInformation2, String ServiceStartDateTimeZone, String ServiceEndDateTimeZone,
			String BillingStartDateTimeZone, String BillingEndDateTimeZone) throws Exception {
		Thread.sleep(5000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		// WaitForElementPresent(".//*[@id='name']", 30000);
		driver.findElement(By.id("name")).sendKeys(ContractName);
		driver.findElement(By.id("description")).sendKeys(ContractDesc);
		driver.findElement(By.id("btn_pm_products_gc_contracts_1_name")).click();
		String winHandleBefore = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("products_id_advanced")).clear();
		driver.findElement(By.id("products_id_advanced")).sendKeys(ProductsID);
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore);

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		Select cSelect1 = new Select(driver.findElement(By.id("contract_type")));
		cSelect1.selectByValue("GMSA");

		Select cSelect2 = new Select(driver.findElement(By.id("contract_scheme")));
		cSelect2.selectByValue("GSC");

		Select cSelect3 = new Select(driver.findElement(By.id("product_offering")));
		cSelect3.selectByVisibleText(ProductOffering);
		// cSelect3.selectByValue("929");

		DateTimeFormatter format1 = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDateTime now1 = LocalDateTime.now();
		driver.findElement(By.id("contract_startdate")).sendKeys(now1.format(format1));

		// driver.findElement(By.id("contract_startdate_timezone")).sendKeys("IST");

		// driver.findElement(By.id("contract_period")).sendKeys("1");

		// Select cSelect4 = new
		// Select(driver.findElement(By.id("contract_period_unit")));
		// cSelect4.selectByValue("Days");

		driver.findElement(By.id("btn_accounts_gc_contracts_1_name")).click();
		String winHandleBefore1 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}
		// Added by deep - start /
		driver.findElement(By.id("search_form_clear"));
		driver.findElement(By.id("common_customer_id_c_advanced")).sendKeys("C0321124868");
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("html/body/table[4]/tbody/tr[3]/td[3]/a")).click();
		// Added by deep - End /
		// driver.findElement(By.xpath("html/body/table[4]/tbody/tr[5]/td[3]/a")).click();
		driver.switchTo().window(winHandleBefore1);

		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("btn_contacts_gc_contracts_1_name")).click();
		String winHandleBefore2 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("last_name_advanced")).clear();
		driver.findElement(By.id("last_name_advanced")).sendKeys(
				"Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore2);

		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		/* Added by deep for Selection Contract TimeZone - Start */
		driver.findElement(By.id("gc_timezone_id_c")).sendKeys(ContractStartDateTimeZone);
		driver.findElement(By.id("gc_timezone_id1_c")).sendKeys(ContractEndDateTimeZone);
		/* Added by deep for Selection Contract TimeZone - End */

		driver.findElement(By.id("factory_reference_no")).sendKeys(FactoryReferenceNumber);
		driver.findElement(By.id("sales_channel_code")).sendKeys(SalesChannelCode);
		/*
		 * driver.findElement(By.id("btn_corporate_name")).click();
		 * 
		 * String winHandleBefore3 = driver.getWindowHandle(); for (String
		 * handle : driver.getWindowHandles()) {
		 * 
		 * driver.switchTo().window(handle); }
		 * 
		 * 
		 * 
		 * 
		 * driver.findElement(By.xpath("//tbody/tr[3]/td[2]/a")).click();
		 * driver.switchTo().window(winHandleBefore3);
		 */

		driver.findElement(By.id("sales_rep_name")).sendKeys(SalesRepNameLatin);
		driver.findElement(By.id("sales_rep_name_1")).sendKeys(SalesRepNameLocal);
		driver.findElement(By.id("division_1")).sendKeys(DivisionLatin);
		driver.findElement(By.id("division_2")).sendKeys(DivisionLocal);
		driver.findElement(By.id("title_1")).sendKeys(TittleLatin);
		driver.findElement(By.id("title_2")).sendKeys(TittleLocal);
		driver.findElement(By.id("tel_no")).sendKeys(Telephone);
		driver.findElement(By.id("ext_no")).sendKeys(ExtnNumber);
		driver.findElement(By.id("fax_no")).sendKeys(FaxNumber);
		driver.findElement(By.id("mob_no")).sendKeys(MobileNumber);
		driver.findElement(By.id("email1")).sendKeys(mailId);

		Select cSelect5 = new Select(driver.findElement(By.id("product_specification_1")));
		// cSelect5.selectByValue("333");
		cSelect5.selectByVisibleText(ProductSpecification);

		/*
		 * driver.findElement(By.id("btn_product_name_1")).click(); String
		 * winHandleBefore4 = driver.getWindowHandle();
		 * 
		 * for(String handle : driver.getWindowHandles()){
		 * driver.switchTo().window(handle); }
		 * 
		 * 
		 * driver.findElement(By.id("name_advanced")).clear();
		 * driver.findElement(By.id("name_advanced")).sendKeys(ProductItem);
		 * driver.findElement(By.id("search_form_submit")).click();
		 * driver.findElement
		 * (By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		 * driver.switchTo().window(winHandleBefore4);
		 */

		Select cSelect6 = new Select(driver.findElement(By.id("cust_contract_currency_1")));
		cSelect6.selectByValue("JPY");

		Select cSelect7 = new Select(driver.findElement(By.id("cust_billing_currency_1")));
		cSelect7.selectByValue("AFN");

		Select cSelect8 = new Select(driver.findElement(By.id("igc_contract_currency_1")));
		cSelect8.selectByValue("AFN");

		Select cSelect9 = new Select(driver.findElement(By.id("igc_billing_currency_1")));
		cSelect9.selectByValue("AFN");

		Select cSelect10 = new Select(driver.findElement(By.id("customer_billing_method_1")));
		cSelect10.selectByValue("eCSS_Billing");

		Select cSelect11 = new Select(driver.findElement(By.id("igc_settlement_method_1")));
		// cSelect11.selectByValue("Quarterly_Settlement");
		cSelect11.selectByVisibleText("No Settlement per Contract");

		driver.findElement(By.id("description_1")).sendKeys("Testing");

		driver.findElement(By.id("btn_tech_account_name_1")).click();

		String winHandleBefore5 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("last_name_advanced")).clear();
		driver.findElement(By.id("last_name_advanced")).sendKeys(
				"Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("search_form_submit")).click();

		driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore5);

		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("btn_bill_account_name_1")).click();
		String winHandleBefore6 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("last_name_advanced")).clear();
		// driver.findElement(By.id("last_name_advanced")).sendKeys("test
		// 3/2/2016");
		driver.findElement(By.id("last_name_advanced")).sendKeys(
				"Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("search_form_submit")).click();

		driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore6);

		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime then = now.plusDays(10);
		driver.findElement(By.id("service_start_date_1")).sendKeys(now.format(format));
		driver.findElement(By.id("service_end_date_1")).sendKeys(then.format(format));
		driver.findElement(By.id("billing_start_date_1")).sendKeys(now.format(format));
		driver.findElement(By.id("billing_end_date_1")).sendKeys(then.format(format));

		/* Commented and added code for filling Timezone by deep */
		/*
		 * driver.findElement(By.id("service_start_date_timezone_1")).sendKeys(
		 * "IST"); driver.findElement(By.id("service_end_date_timezone_1"))
		 * .sendKeys("IST");
		 * driver.findElement(By.id("billing_start_date_timezone_1")).sendKeys(
		 * "IST"); driver.findElement(By.id("billing_end_date_timezone_1"))
		 * .sendKeys("IST");
		 */

		driver.findElement(By.id("service_start_date_timezone_1")).sendKeys(ServiceStartDateTimeZone);
		driver.findElement(By.id("service_end_date_timezone_1")).sendKeys(ServiceEndDateTimeZone);
		driver.findElement(By.id("billing_start_date_timezone_1")).sendKeys(BillingStartDateTimeZone);
		driver.findElement(By.id("billing_end_date_timezone_1")).sendKeys(BillingEndDateTimeZone);

		/*
		 * driver.findElement(By.id("btn_tech_account_name_1")).click();
		 * 
		 * String winHandleBefore5 = driver.getWindowHandle(); for (String
		 * handle : driver.getWindowHandles()) {
		 * driver.switchTo().window(handle); }
		 * driver.findElement(By.id("last_name_advanced")).clear();
		 * driver.findElement(By.id("last_name_advanced")) .sendKeys(
		 * "Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789"
		 * ); driver.findElement(By.id("search_form_submit")).click();
		 * 
		 * driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click(
		 * ); driver.switchTo().window(winHandleBefore5);
		 * 
		 * driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		 * driver.findElement(By.id("btn_bill_account_name_1")).click(); String
		 * winHandleBefore6 = driver.getWindowHandle(); for (String handle :
		 * driver.getWindowHandles()) { driver.switchTo().window(handle); }
		 * driver.findElement(By.id("last_name_advanced")).clear(); //
		 * driver.findElement(By.id("last_name_advanced")).
		 * sendKeys("test 3/2/2016");
		 * driver.findElement(By.id("last_name_advanced")) .sendKeys(
		 * "Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789"
		 * ); driver.findElement(By.id("search_form_submit")).click();
		 * 
		 * driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click(
		 * ); driver.switchTo().window(winHandleBefore6);
		 */

		if (PaymentType.equalsIgnoreCase("General: Direct Deposit")) {
			// driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));

			oSelect.selectByValue("00");

			driver.findElement(By.id("dd_bank_code_1")).sendKeys("1234");
			driver.findElement(By.id("dd_branch_code_1")).sendKeys("12");

			Select oSelect1 = new Select(driver.findElement(By.id("dd_deposit_type_1")));
			oSelect1.selectByVisibleText("Tax Payment (General)");

			driver.findElement(By.id("dd_account_no_1")).sendKeys("1234567");
			driver.findElement(By.id("dd_person_name_1_1")).sendKeys("ContactPerson Latin");
			driver.findElement(By.id("dd_person_name_2_1")).sendKeys("ContactPerson Local");
			driver.findElement(By.id("dd_payee_tel_no_1")).sendKeys("1234567890");
			driver.findElement(By.id("dd_payee_email_address_1")).sendKeys("dummy@email.com");
			driver.findElement(By.id("dd_remarks_1")).sendKeys("Remarks");
		} else if (PaymentType.equalsIgnoreCase("Account Transfer")) {
			// driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			try {
				Select oSelect = new Select(driver.findElement(By

						.id("payment_type_1")));

				oSelect.selectByValue("10");

				driver.findElement(By.id("at_account_no_1")).sendKeys("12345678963636363");
				driver.findElement(By.id("at_name_1")).sendKeys("Atul Dandin");
				driver.findElement(By.id("at_bank_code_1")).sendKeys("1234567890");
				driver.findElement(By.id("at_branch_code_1")).sendKeys("1234567890");

				Select oSelect1 = new Select(driver.findElement(By.id("at_deposit_type_1")));
				oSelect1.selectByVisibleText("Ordinary Deposit");

				Select oSelect2 = new Select(driver.findElement(By.id("at_account_no_display_1")));
				oSelect2.selectByVisibleText("Yes");

				Reporter.log("RTC_Contract_016 Passed- User was able to select account type Account transfer");
			} catch (Exception e) {
				Assert.fail(
						"RTC_Contract_016 failed an error occured while selecting the payment type as account transfer"
								+ e);
			}
		} else if (PaymentType.equalsIgnoreCase("Postal Transfer")) {
			// driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));

			oSelect.selectByValue("30");

			// driver.findElement(By.id("pt_postal_passbook_1")).click();
			// driver.findElement(By.id("pt_postal_passbook_1")).clear();
			driver.findElement(By.id("pt_postal_passbook_1")).sendKeys("123");
			driver.findElement(By.id("pt_name_1")).sendKeys("Testing account ");
			driver.findElement(By.id("pt_postal_passbook_mark_1")).sendKeys("Mark");

			Select oSelect2 = new Select(driver.findElement(By.id("pt_postal_passbook_no_display_1")));
			oSelect2.selectByVisibleText("Yes");

		} else if (PaymentType.equalsIgnoreCase("Credit Card")) {
			// driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));

			oSelect.selectByValue("60");
			driver.findElement(By.id("cc_credit_card_no_1")).sendKeys("12345678963636363");
			driver.findElement(By.id("cc_expiration_date_1")).sendKeys("10/13");

		} else {
			Reporter.log("Invalid Paymemt Type");
			Assert.fail();
		}

		/*
		 * deep added for filling data based on ProductOffering and
		 * ProductSpecification
		 */
		System.out.println("*****Product offering ***" + ProductOffering);
		if (ProductOffering.equals("Global Management One ECL2.0 Option")) {
			if (ProductSpecification.equals("GMOne ECL2.0 Option Base Specification (Base)")) {
				/*
				 * Select baseProductConfigSelect = new Select(
				 * driver.findElement(By.id("product_config_1_565")));
				 * baseProductConfigSelect.selectByVisibleText("1");
				 */
				driver.findElement(By.id("product_config_1_565")).sendKeys("123");
				driver.findElement(By.id("pi_role_value_1_877")).sendKeys("Product Information2");
			} else if (ProductSpecification.equals("Managed Windows Server - on ECL2.0 (Add-on Optional)")) {
				Select optionalProductConfigSelect1 = new Select(driver.findElement(By.id("product_config_1_563")));
				optionalProductConfigSelect1.selectByVisibleText("UNIT");
			} else if (ProductSpecification.equals("Managed Linux - on ECL2.0 (Add-on Optional)")) {
				Select optionalProductConfigSelect2 = new Select(driver.findElement(By.id("product_config_1_563")));
				optionalProductConfigSelect2.selectByVisibleText("UNIT");
			} else if (ProductSpecification.equals("Managed Oracle Basic - on ECL2.0 (Add-on Optional)")) {
				Select optionalProductConfigSelect3 = new Select(driver.findElement(By.id("product_config_1_613")));
				optionalProductConfigSelect3.selectByVisibleText("UNIT");
			} else if (ProductSpecification.equals("Managed Oracle Advanced - on ECL2.0 (Add-on Optional)")) {
				Select optionalProductConfigSelect4 = new Select(driver.findElement(By.id("product_config_1_614")));
				optionalProductConfigSelect4.selectByVisibleText("UNIT");
			}
		} else if (ProductOffering.equals("Global Management One UNO Option")) {
			driver.findElement(By.id("product_config_1_565")).sendKeys("123");
			driver.findElement(By.id("pi_role_value_1_877")).sendKeys("Product Information2");
		} else if (ProductOffering.equals("Enterprise Mail")) {

		}

		/*
		 * Select cSelect13 = new
		 * Select(driver.findElement(By.id("product_config_1_561")));
		 * cSelect13.selectByValue("14014");
		 * 
		 * Select cSelect14 = new
		 * Select(driver.findElement(By.id("product_config_1_562")));
		 * cSelect14.selectByValue("14015");
		 */

		/*
		 * Select cSelect15 = new
		 * Select(driver.findElement(By.id("product_config_1_565")));
		 * cSelect15.selectByValue("1");
		 */

		/*
		 * Select cSelect16 = new
		 * Select(driver.findElement(By.id("product_config_1_567")));
		 * cSelect16.selectByValue("14035");
		 * 
		 * Select cSelect17 = new
		 * Select(driver.findElement(By.id("product_config_1_565")));
		 * cSelect17.selectByValue("1");
		 */

		// product_config_1_565

		/*
		 * driver.findElement(By.id("pi_role_value_1_861")).sendKeys(
		 * "Product Information1");
		 * driver.findElement(By.id("pi_role_value_1_862"
		 * )).sendKeys("Product Information2");
		 */
		// driver.findElement(By.id("pi_role_value_1_876")).sendKeys("Product
		// Information2");

		// driver.findElement(By.id("SAVE_FOOTER")).click();
		// srn.getscreenshot();

	}

	public void ViewContractNavigation() throws Exception {
		// driver.findElement(By.xpath("//div[5]/div[2]/ul[2]/li[5]/a/em")).click();
		/*
		 * driver.findElement(By.xpath("//*[@id='moduleTabExtraMenuAll']/a/em"))
		 * . click(); WebElement element =
		 * driver.findElement(By.linkText("Contracts")); Actions action = new
		 * Actions(driver); action.moveToElement(element).build().perform();
		 * driver.findElement(By.id("ViewContractsAll")).click();
		 * srn.getscreenshot();
		 */
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[4]/span/button")).click();
		driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[4]/span/div/ul/li[2]/a")).click();
	}

	public void ViewContractNavigationWorkflow() throws Exception {
		/*
		 * driver.findElement(
		 * By.xpath("/html/body/div[4]/div[2]/ul[2]/li[5]/a/em")).click();
		 * WebElement element = driver.findElement(By.linkText("Contracts"));
		 * Actions action = new Actions(driver);
		 * action.moveToElement(element).build().perform();
		 * driver.findElement(By.id("ViewContractsAll")).click();
		 */
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[4]/span/button")).click();
		System.out.println("aaa::" + driver
				.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[4]/span/div/ul/li[2]/a")).getText());
		driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[4]/span/div/ul/li[2]/a")).click();
		// srn.getscreenshot();

	}

	public void ViewContractWorkflow(String ContractName) throws Exception {
		try {
			Thread.sleep(10000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			// driver.findElement(By.xpath(".//*[@id='search_form_clear']")).click();
			// driver.findElement(By.id("search_form_clear")).click();
			// driver.findElement(By.xpath(".//*[@id='name_basic']")).click();
			// Thread.sleep(5000);
			driver.findElement(By.id("global_contract_id_basic")).clear();
			driver.findElement(By.xpath(".//*[@id='name_basic']")).clear();
			// Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='name_basic']")).sendKeys(ContractName);
			driver.findElement(By.id("search_form_submit")).click();
			driver.findElement(By.xpath("//table/tbody/tr[3]/td[4]/b/a")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			// srn.getscreenshot();*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void FileUploadContractHeader(String ContractName) throws AWTException, InterruptedException {

		driver.findElement(By.id("global_contract_id_basic")).clear();
		driver.findElement(By.id("name_basic")).clear();
		driver.findElement(By.id("name_basic")).sendKeys(ContractName);
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//div[3]/form[3]/table/tbody/tr[3]/td[4]/b/a")).click();
		driver.findElement(By.xpath("//div/div/table/thead/tr[1]/td/div/ul/li/a")).click();

		driver.findElement(By.xpath("//td/span/div/ul/li/div[2]/div/div/table/thead/tr[1]/td/div/ul/li/a")).click();

		String winHandleBefore = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}

		driver.findElement(By.id("filename_file")).click();
		// driver.manage().timeouts().implicitlyWait(3, TimeUnit.MINUTES);

		// Thread.sleep(50000);

		StringSelection ss = new StringSelection("C:\\Users\\c_atuld\\Desktop\\path2.txt");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		r.keyPress(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);

		// driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
		Thread.sleep(5000);
		driver.findElement(By.id("SAVE_FOOTER")).click();
		driver.switchTo().window(winHandleBefore);

	}

	public void FileUploadLineItems(String ContractName) throws AWTException, InterruptedException {

		// driver.findElement(By.id("global_contract_id_basic")).clear();
		// driver.findElement(By.id("name_basic")).clear();
		// driver.findElement(By.id("name_basic")).sendKeys(ContractName);
		// driver.findElement(By.id("search_form_submit")).click();
		// driver.findElement(By.xpath("//div[3]/form[3]/table/tbody/tr[3]/td[4]/b/a")).click();
		// driver.findElement(By.xpath("//div/div/table/thead/tr[1]/td/div/ul/li/a")).click();

		driver.findElement(By.id("arrow-doc-1")).click();
		driver.findElement(By
				.xpath("//table/tbody/tr/td/span/div/ul/li/div[2]/div[6]/ul/li/div[2]/div/div/table/thead/tr[1]/td/div/ul/li/a"))
				.click();
		// div[6]/ul/li/div[2]/div/div/table/thead/tr[1]/td/div/ul/li/a
		// driver.findElement(By.className("sugar_action_button")).click();

		String winHandleBefore = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}

		driver.findElement(By.id("filename_file")).click();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);

		Thread.sleep(5000);

		StringSelection ss = new StringSelection("C:\\Users\\c_atuld\\Desktop\\path2.txt");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		r.keyPress(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_CONTROL);
		Thread.sleep(50000);
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);

		// driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
		Thread.sleep(5000);
		driver.findElement(By.id("SAVE_FOOTER")).click();
		driver.switchTo().window(winHandleBefore);

	}

	public void MandetoryCheck() {
		String errMsg, temp;

		temp = driver.findElement(By.xpath("//div/div[1]/table/tbody/tr[1]/td[2]/div")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Contract Name");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Contract Name: " + temp);
		}

		temp = driver.findElement(By.xpath("//div[1]/table/tbody/tr[7]/td[2]/div")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Product Offering");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Product Offering: " + temp);
		}

		temp = driver.findElement(By.xpath("//div[1]/table/tbody/tr[9]/td[2]/div")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Contract Period");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Contract Period: " + temp);
		}

		temp = driver.findElement(By.xpath("//div[1]/table/tbody/tr[9]/td[4]/div")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Contract Period Unit");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Contract Period Unit: " + temp);
		}

		temp = driver.findElement(By.xpath("//div[1]/table/tbody/tr[10]/td[2]/div[2]")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Contracting Customer Company");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Contracting Customer Company: " + temp);
		}

		temp = driver.findElement(By.xpath("//div[1]/table/tbody/tr[10]/td[4]/div[2]")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Contracting Customer Account");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Contracting Customer Account: " + temp);
		}

		temp = driver.findElement(By.xpath("//div[1]/div[2]/div/span/div")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Product Specification");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Product Specification: " + temp);
		}

		temp = driver.findElement(By.xpath("//div/div[2]/div[2]/div/span/div")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Customer Contract Currency");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Customer Contract Currency: " + temp);
		}

		temp = driver.findElement(By.xpath("//div/div[2]/div[4]/div/span/div")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Customer Billing Currency");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Customer Billing Currency: " + temp);
		}

		temp = driver.findElement(By.xpath("//div[2]/div/div[3]/div[2]/div/span/div")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Inter-Group-Company Contract Currency");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Inter-Group-Company Contract Currency: " + temp);
		}

		temp = driver.findElement(By.xpath("//div[2]/div/div[3]/div[4]/div/span/div")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Inter-Group-Company Billing Currency");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Inter-Group-Company Billing Currency: " + temp);
		}

		temp = driver.findElement(By.xpath("//div[2]/div/div[4]/div[2]/div/span/div")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Customer Billing Method");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Customer Billing Method: " + temp);
		}

		temp = driver.findElement(By.xpath("//div[2]/div/div[4]/div[4]/div/span/div")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Inter-Group-Company Settlement Method");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Inter-Group-Company Settlement Method: " + temp);
		}

		temp = driver.findElement(By.xpath("//div[2]/div/div[9]/div[2]/div/span/div[2]")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Technology Customer Account");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Technology Customer Account: " + temp);
		}

		temp = driver.findElement(By.xpath("//div[2]/div/div[9]/div[4]/div/span/div[2]")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Billing Customer Account");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Billing Customer Account: " + temp);
		}

		temp = driver.findElement(By.xpath("//div[2]/div/div[10]/div[2]/div/span/div")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Payment Type");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Payment Type: " + temp);
		}

		driver.navigate().refresh();
		// Alert alert = driver.switchTo().alert();
		// alert.accept();

		Select cSelect3 = new Select(driver.findElement(By.id("product_offering")));
		// cSelect3.selectByValue(ProductOffering);
		cSelect3.selectByValue("930");
		Select cSelect5 = new Select(driver.findElement(By.id("product_specification_1")));
		cSelect5.selectByValue("344");

		driver.findElement(By.id("SAVE_FOOTER")).click();
		Reporter.log("After save");

		temp = driver.findElement(By.xpath("//div/div[13]/ul/li[1]/div/div/span/div[1]/div[4]/div/span/div")).getText();
		if (temp.isEmpty()) {
			Reporter.log("MandetoryFeild check failed for Product Configuration");
			Assert.fail();
		} else {
			Reporter.log("MandetoryFeild check passed for Product Configuration: " + temp);
		}

	}

	public void MandetoryCheckPayment(String PaymentType) throws InterruptedException {

		String errormessage;
		if (PaymentType.equalsIgnoreCase("General: Direct Deposit")) {

			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));
			oSelect.selectByValue("00");

			Thread.sleep(1000);
			driver.findElement(By.id("SAVE_HEADER")).click();
			Thread.sleep(1000);

			String message = driver.findElement(By.xpath("//div[12]/ul/li[1]/div/div/div[1]/div[2]/div/span/div"))
					.getText();
			if (message.isEmpty()) {
				Reporter.log("Error message required" + message);

			} else {

				Reporter.log(message);

			}

			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[1]/div/div/div[1]/div[4]/div/span/div"))
					.getText();

			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}

			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[1]/div/div/div[2]/div[2]/div/span/div"))
					.getText();

			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}
			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[1]/div/div/div[2]/div[4]/div/span/div"))
					.getText();

			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}

		} else if (PaymentType.equalsIgnoreCase("Account Transfer")) {
			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));

			oSelect.selectByValue("10");
			Thread.sleep(1000);
			driver.findElement(By.id("SAVE_HEADER")).click();
			Thread.sleep(1000);

			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[4]/div/div/div[1]/div[2]/div/span/div"))
					.getText();
			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}
			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[4]/div/div/div[1]/div[4]/div/span/div"))
					.getText();

			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}

			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[4]/div/div/div[2]/div[2]/div/span/div"))
					.getText();

			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}

			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[4]/div/div/div[2]/div[4]/div/span/div"))
					.getText();

			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}

			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[4]/div/div/div[3]/div[2]/div/span/div"))
					.getText();

			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}
			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[4]/div/div/div[3]/div[4]/div/span/div"))
					.getText();

			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}
		} else if (PaymentType.equalsIgnoreCase("Postal Transfer")) {
			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));

			oSelect.selectByValue("30");

			Thread.sleep(1000);
			driver.findElement(By.id("SAVE_HEADER")).click();
			Thread.sleep(1000);

			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[3]/div/div/div[1]/div[2]/div/span/div"))
					.getText();

			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}

			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[3]/div/div/div[1]/div[4]/div/span/div"))
					.getText();

			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}

			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[3]/div/div/div[2]/div[2]/div/span/div"))
					.getText();

			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}

		} else if (PaymentType.equalsIgnoreCase("Credit Card")) {
			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));

			oSelect.selectByValue("60");
			Thread.sleep(1000);
			driver.findElement(By.id("SAVE_HEADER")).click();
			Thread.sleep(1000);

			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[2]/div/div/div/div[2]/div/span/div")).getText();

			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}

			errormessage = driver.findElement(By.xpath("//div[12]/ul/li[2]/div/div/div/div[4]/div/span/div")).getText();

			if (errormessage.isEmpty()) {
				Reporter.log("Error message required");
				Assert.fail();

			} else {

				Reporter.log(errormessage);

			}

		} else {
			Reporter.log("Invalid Payment ID");

		}

		// driver.findElement(By.xpath("//*[@id='moduleTabExtraMenuAll']/a/em")).click();
		// Alert alert = driver.switchTo().alert();
		// alert.accept();

	}

	public void DataCount() {

		String count;

		count = driver.findElement(By.id("name")).getText();
		if (count.length() > 100) {

			Reporter.log("Feild lenght for contract Name is more than 100");
			Assert.fail();

		} else {
			Reporter.log("Feild lenght for contract Name is less than 100");
		}

		count = driver.findElement(By.xpath("//div[3]/table/tbody/tr/td/span/div/ul/li/div[2]/div/div[1]/div[2]/div"))
				.getText();
		if (count.length() > 100) {

			Reporter.log("Feild lenght for Sales Channel Code is more than 100");
			Assert.fail();

		} else {
			Reporter.log("Feild lenght for Sales Channel Code is less than 100");
		}

		count = driver
				.findElement(By.xpath("//div[3]/table/tbody/tr/td/span/div/ul/li/div[2]/div/div[2]/div[2]/div/span"))
				.getText();
		if (count.length() > 60) {

			Reporter.log("Feild lenght for Sales Representative Name (Latin Character) is more than 60");
			Assert.fail();

		} else {
			Reporter.log("Feild lenght for Sales Representative Name (Latin Character) is less than 60");
		}

		count = driver
				.findElement(By.xpath("//div[3]/table/tbody/tr/td/span/div/ul/li/div[2]/div/div[2]/div[4]/div/span"))
				.getText();
		if (count.length() > 60) {

			Reporter.log("Feild lenght for Sales Representative Name (Local Character) is more than 60");
			Assert.fail();

		} else {
			Reporter.log("Feild lenght for Sales Representative Name (Local Character) is less than 60");
		}

		count = driver.findElement(By.xpath("//div[3]/table/tbody/tr/td/span/div/ul/li/div[2]/div/div[3]/div[2]/div"))
				.getText();
		if (count.length() > 100) {

			Reporter.log("Feild lenght for Division (Latin Character) is more than 100");
			Assert.fail();

		} else {
			Reporter.log("Feild lenght for Division (Latin Character) is less than 100");
		}

		count = driver
				.findElement(By.xpath("//div[3]/table/tbody/tr/td/span/div/ul/li/div[2]/div/div[3]/div[4]/div/span"))
				.getText();
		if (count.length() > 100) {

			Reporter.log("Feild lenght for Division (Local Character) is more than 100");
			Assert.fail();

		} else {
			Reporter.log("Feild lenght for Division (Local Character) is less than 100");
		}

		count = driver
				.findElement(By.xpath("//div[3]/table/tbody/tr/td/span/div/ul/li/div[2]/div/div[4]/div[2]/div/span"))
				.getText();
		if (count.length() > 100) {

			Reporter.log("Feild lenght for Title (Latin Character) is more than 100");
			Assert.fail();

		} else {
			Reporter.log("Feild lenght for Title (Latin Character) is less than 100");
		}

		count = driver
				.findElement(By.xpath("//div[3]/table/tbody/tr/td/span/div/ul/li/div[2]/div/div[4]/div[4]/div/span"))
				.getText();
		if (count.length() > 100) {

			Reporter.log("Feild lenght for Title (Local Character) is more than 100");
			Assert.fail();

		} else {
			Reporter.log("Feild lenght for Title (Local Character) is less than 100");
		}

		count = driver
				.findElement(By.xpath("//div[3]/table/tbody/tr/td/span/div/ul/li/div[2]/div/div[5]/div[2]/div/span"))
				.getText();
		if (count.length() > 20) {

			Reporter.log("Feild lenght for Telephone number is more than 20");
			Assert.fail();

		} else {
			Reporter.log("Feild lenght for Telephone number is less than 20");
		}

		count = driver
				.findElement(By.xpath("//div[3]/table/tbody/tr/td/span/div/ul/li/div[2]/div/div[5]/div[4]/div/span"))
				.getText();
		if (count.length() > 5) {

			Reporter.log("Feild lenght for Extension number is more than 5");
			Assert.fail();

		} else {
			Reporter.log("Feild lenght for Extension number is less than 5");
		}

		count = driver
				.findElement(By.xpath("//div[3]/table/tbody/tr/td/span/div/ul/li/div[2]/div/div[6]/div[4]/div/span"))
				.getText();
		if (count.length() > 20) {

			Reporter.log("Feild lenght for Mobile number is more than 20");
			Assert.fail();

		} else {
			Reporter.log("Feild lenght for Mobile number is less than 20");
		}

		count = driver
				.findElement(By.xpath("//div[3]/table/tbody/tr/td/span/div/ul/li/div[2]/div/div[7]/div[2]/div/span"))
				.getText();
		if (count.length() > 140) {

			Reporter.log("Feild lenght for Email address is more than 140");
			Assert.fail();

		} else {
			Reporter.log("Feild lenght for Email address is less than 140");
		}

		String payment = driver.findElement(By.xpath("//ul/li/div[2]/div[2]/div[7]/div[2]/div/span")).getText();

		if (payment.equalsIgnoreCase("General: Direct Deposit")) {

			count = driver.findElement(By.xpath("//div[8]/ul/li/div[2]/div/div[1]/div[2]/div/span")).getText();
			if (count.length() > 4) {
				Reporter.log("Feild lenght for Bank code is more than 4");
				Assert.fail();

			} else {
				Reporter.log("Feild lenght for Bank code is less than 4");
			}

			count = driver.findElement(By.xpath("//div[8]/ul/li/div[2]/div/div[1]/div[4]/div/span")).getText();
			if (count.length() > 3) {
				Reporter.log("Feild lenght for Branch code is more than 3");
				Assert.fail();

			} else {
				Reporter.log("Feild lenght for Branch code is less than 3");
			}

			count = driver.findElement(By.xpath("//div[8]/ul/li/div[2]/div/div[2]/div[4]/div/span")).getText();
			if (count.length() > 7) {
				Reporter.log("Feild lenght for Account Number is more than 7");
				Assert.fail();
			} else {
				Reporter.log("Feild lenght for Account Number is less than 7");
			}

			count = driver.findElement(By.xpath("//div[8]/ul/li/div[2]/div/div[3]/div[2]/div/span")).getText();
			if (count.length() > 60) {
				Reporter.log("Feild lenght for Payee Contact Person Name (Latin Character) is more than 60");
				Assert.fail();
			} else {
				Reporter.log("Feild lenght for Payee Contact Person Name (Latin Character) is less than 60");
			}

			count = driver.findElement(By.xpath("//div[8]/ul/li/div[2]/div/div[3]/div[4]/div/span")).getText();
			if (count.length() > 20) {
				Reporter.log("Feild lenght for Payee Contact Person Name (Local Character) is more than 20");
				Assert.fail();
			} else {
				Reporter.log("Feild lenght for Payee Contact Person Name (Local Character) is less than 20");
			}

			count = driver.findElement(By.xpath("//div[8]/ul/li/div[2]/div/div[4]/div[2]/div/span")).getText();
			if (count.length() > 20) {
				Reporter.log("Feild lenght for Payee Telephone Number is more than 20");
				Assert.fail();
			} else {
				Reporter.log("Feild lenght for Payee Telephone Number is less than 20");
			}

			count = driver.findElement(By.xpath("//div[8]/ul/li/div[2]/div/div[4]/div[4]/div/span")).getText();
			if (count.length() > 140) {
				Reporter.log("Feild lenght for Payee E-mail Address is more than 140");
				Assert.fail();
			} else {
				Reporter.log("Feild lenght for Payee E-mail Address is less than 140");
			}

			count = driver.findElement(By.xpath("//div[8]/ul/li/div[2]/div/div[5]/div[2]/div/span")).getText();
			if (count.length() > 40) {
				Reporter.log("Feild lenght for Remarks is more than 40");
				Assert.fail();
			} else {
				Reporter.log("Feild lenght for Remarks is less than 40");
			}

		} else if (payment.equalsIgnoreCase("Account Transfer")) {

			count = driver.findElement(By.xpath("//div[8]/ul/li/div[2]/div/div[1]/div[2]/div/span")).getText();
			if (count.length() > 30) {
				Reporter.log("Feild lenght for Account Holder's Name is more than 30");
				Assert.fail();
			} else {
				Reporter.log("Feild lenght for Account Holder's Name than 30");
			}

			count = driver.findElement(By.xpath("//div[8]/ul/li/div[2]/div/div[2]/div[2]/div/span")).getText();
			if (count.length() > 4) {
				Reporter.log("Feild lenght for Bank Code is more than 4");
				Assert.fail();
			} else {
				Reporter.log("Feild lenght for Bank Code is less than 4");
			}

			count = driver.findElement(By.xpath("//div[8]/ul/li/div[2]/div/div[2]/div[4]/div/span")).getText();
			if (count.length() > 3) {
				Reporter.log("Feild lenght for Branch Code is more than 3");
				Assert.fail();
			} else {
				Reporter.log("Feild lenght for Branch Code is less than 3");
			}

			count = driver.findElement(By.xpath("//div[8]/ul/li/div[2]/div/div[3]/div[4]/div/span")).getText();
			if (count.length() > 7) {
				Reporter.log("Feild lenght for Account Number is more than 71");
				Assert.fail();
			} else {
				Reporter.log("Feild lenght for Account Number is less than 7");
			}

		} else if (payment.equalsIgnoreCase("Postal Transfer")) {

			count = driver.findElement(By.xpath("//div[2]/div[2]/div[8]/ul/li/div[2]/div/div[1]/div[2]/div/span"))
					.getText();
			if (count.length() > 30) {
				Reporter.log("Feild lenght for Account Holder's Namer is more than 30");
				Assert.fail();

			} else {
				Reporter.log("Feild lenght for Account Holder's Name is less than 30");
			}

			count = driver
					.findElement(By.xpath("//div/ul/li/div[2]/div[2]/div[8]/ul/li/div[2]/div/div[1]/div[4]/div/span"))
					.getText();
			if (count.length() > 5) {
				Reporter.log("Feild lenght for Postal Passbook Mark is more than 5");
				Assert.fail();

			} else {
				Reporter.log("Feild lenght for Postal Passbook Mark is less than 5");
			}

			count = driver
					.findElement(By.xpath("//div/ul/li/div[2]/div[2]/div[8]/ul/li/div[2]/div/div[2]/div[2]/div/span"))
					.getText();
			if (count.length() > 8) {
				Reporter.log("Feild lenght for Postal Passbook Number is more than 8");
				Assert.fail();

			} else {
				Reporter.log("Feild lenght for Postal Passbook Number is less than 8");
			}

		} else if (payment.equalsIgnoreCase("Credit Card")) {

			count = driver.findElement(By.xpath("//div[2]/div[2]/div[8]/ul/li/div[2]/div/div/div[2]/div/span"))
					.getText();
			if (count.length() > 16) {
				Reporter.log("Feild lenght for Credit Card is more than 16");
				Assert.fail();

			} else {
				Reporter.log("Feild lenght for Credit is less than 16");
			}

			count = driver
					.findElement(By.xpath("//div/ul/li/div[2]/div[2]/div[8]/ul/li/div[2]/div/div/div[4]/div/span"))
					.getText();
			if (count.length() > 6) {
				Reporter.log("Feild lenght for Credit Card expiry date is more than 6");
				Assert.fail();

			} else {
				Reporter.log("Feild lenght for Credit card expiry date is less than 6");
			}

		} else {

			Reporter.log("Invalid Payment type");
		}

		// driver.close();

	}

	public void SelectContract(String ContractName) throws Exception {
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("global_contract_id_basic")).clear();
		driver.findElement(By.id("name_basic")).clear();
		driver.findElement(By.id("name_basic")).sendKeys(ContractName);
		// srn.getscreenshot();
		driver.findElement(By.id("search_form_submit")).click();
		// driver.findElement(By.xpath("//table/tbody/tr[3]/td[4]/b/a")).click();
		Thread.sleep(4000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.xpath(".//*[@id='MassUpdate']/table/tbody/tr[3]/td[4]/b/a")).click();
		// srn.getscreenshot();

	}

	public void SelectContractId(String ContractId) throws Exception {
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		if (driver.findElements(By.id("basic_search_link")).size() != 0) {
			driver.findElement(By.id("basic_search_link")).click();
		}
		driver.findElement(By.id("global_contract_id_basic")).clear();
		driver.findElement(By.id("name_basic")).clear();
		driver.findElement(By.id("global_contract_id_basic")).sendKeys(ContractId);
		// srn.getscreenshot();
		driver.findElement(By.id("search_form_submit")).click();
		// driver.findElement(By.xpath("//table/tbody/tr[3]/td[4]/b/a")).click();
		Thread.sleep(7000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		// srn.getscreenshot();
		driver.findElement(By.xpath(".//*[@id='MassUpdate']/table/tbody/tr[3]/td[4]/b/a")).click();
	}

	public void SelectContractId_VersionUp(String ContractId) throws Exception {
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		if (driver.findElements(By.id("basic_search_link")).size() != 0) {
			driver.findElement(By.id("basic_search_link")).click();
		}
		driver.findElement(By.id("global_contract_id_basic")).clear();
		driver.findElement(By.id("name_basic")).clear();
		driver.findElement(By.id("global_contract_id_basic")).sendKeys(ContractId);
		// srn.getscreenshot();
		driver.findElement(By.id("search_form_submit")).click();
		// driver.findElement(By.xpath("//table/tbody/tr[3]/td[4]/b/a")).click();
		Thread.sleep(10000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		// srn.getscreenshot();

		WebElement Contract_version = driver.findElement(By.xpath(".//*[@id='MassUpdate']/table/tbody/tr[3]/td[7]"));
		String Contract_version1 = Contract_version.getText();

		if (Contract_version1.equals("2.0")) {

			driver.findElement(By.xpath(".//*[@id='MassUpdate']/table/tbody/tr[3]/td[4]/b/a")).click();
		}

		else {
			driver.findElement(By.xpath(".//*[@id='MassUpdate']/table/tbody/tr[4]/td[4]/b/a")).click();
		}
	}

	public void ClearData(String PaymentType) {

		driver.findElement(By.id("name")).clear();
		driver.findElement(By.id("description")).clear();
		driver.findElement(By.id("contract_startdate_timezone")).clear();
		driver.findElement(By.id("btn_clr_pm_products_gc_contracts_1_name")).click();
		driver.findElement(By.id("btn_clr_accounts_gc_contracts_1_name")).click();
		driver.findElement(By.id("btn_clr_contacts_gc_contracts_1_name")).click();
		driver.findElement(By.id("factory_reference_no")).clear();
		driver.findElement(By.id("sales_channel_code")).clear();
		driver.findElement(By.id("btn_clr_corporate_name")).click();
		driver.findElement(By.id("division_1")).clear();
		driver.findElement(By.id("division_2")).clear();
		driver.findElement(By.id("title_1")).clear();
		driver.findElement(By.id("title_2")).clear();
		driver.findElement(By.id("tel_no")).clear();
		driver.findElement(By.id("ext_no")).clear();
		driver.findElement(By.id("fax_no")).clear();
		driver.findElement(By.id("mob_no")).clear();
		driver.findElement(By.id("email1")).clear();
		driver.findElement(By.id("service_start_date_timezone_1")).clear();
		driver.findElement(By.id("service_end_date_timezone_1")).clear();
		driver.findElement(By.id("billing_start_date_timezone_1")).clear();
		driver.findElement(By.id("billing_end_date_timezone_1")).clear();
		driver.findElement(By.id("description_1")).clear();

		driver.findElement(By.id("btn_clr_product_name_1")).click();
		driver.findElement(By.id("btn_clr_tech_account_name_1")).click();
		driver.findElement(By.id("btn_clr_bill_account_name_1")).click();

		// payment = driver.findElement(By.id("payment_type_1")).getText();

		Reporter.log("The Payment id is " + PaymentType);
		if (PaymentType.equalsIgnoreCase("General: Direct Deposit")) {

			driver.findElement(By.id("dd_bank_code_1")).clear();
			driver.findElements(By.id("dd_branch_code_1")).clear();
			driver.findElement(By.id("dd_account_no_1")).clear();
			driver.findElement(By.id("dd_person_name_1_1")).clear();
			driver.findElement(By.id("dd_person_name_2_1")).clear();
			driver.findElement(By.id("dd_payee_tel_no_1")).clear();
			driver.findElement(By.id("dd_payee_email_address_1")).clear();
			driver.findElement(By.id("dd_remarks_1")).clear();

		} else if (PaymentType.equalsIgnoreCase("Account")) {
			driver.findElement(By.id("at_name_1")).clear();
			driver.findElement(By.id("at_bank_code_1")).clear();
			driver.findElement(By.id("at_branch_code_1")).clear();
			driver.findElement(By.id("at_account_no_1")).clear();

		} else if (PaymentType.equalsIgnoreCase("Postal Transfer")) {

			driver.findElement(By.id("pt_name_1")).clear();
			driver.findElement(By.id("pt_postal_passbook_mark_1")).clear();
			driver.findElement(By.id("pt_postal_passbook_1")).clear();

		} else if (PaymentType.equalsIgnoreCase("Credit Card")) {

			driver.findElement(By.id("cc_credit_card_no_1")).clear();
			driver.findElement(By.id("cc_expiration_date_1")).clear();

		} else {

			Reporter.log("Invalid paymenttype");
		}

	}

	public void dataFillNonICB(String ContractName, String ContractDesc, String ProductsID, String ProductOffering,
			String FactoryReferenceNumber, String SalesChannelCode, String SalesRepNameLatin, String SalesRepNameLocal,
			String DivisionLatin, String DivisionLocal, String TittleLatin, String TittleLocal, String Telephone,
			String ExtnNumber, String FaxNumber, String MobileNumber, String mailId, String ProductItem,
			String ProductSpecification, String ProductConfiguration, String ProductConfiguration1,
			String ProductConfiguration2, String ProductConfiguration3, String ProductConfiguration4,
			String ProductConfiguration5, String ProductConfiguration6, String ProductConfiguration7,
			String PaymentType, String ProductInformation1, String ProductInformation2) throws InterruptedException {

		driver.findElement(By.id("name")).sendKeys(ContractName);
		driver.findElement(By.id("description")).sendKeys(ContractDesc);

		driver.findElement(By.id("btn_pm_products_gc_contracts_1_name")).click();
		String winHandleBefore = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("products_id_advanced")).clear();
		driver.findElement(By.id("products_id_advanced")).sendKeys(ProductsID);
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore);

		Select cSelect1 = new Select(driver.findElement(By.id("contract_type")));
		cSelect1.selectByValue("GMSA");

		Select cSelect2 = new Select(driver.findElement(By.id("contract_scheme")));
		cSelect2.selectByValue("GSC");

		Select cSelect3 = new Select(driver.findElement(By.id("product_offering")));
		// cSelect3.selectByValue(ProductOffering);
		cSelect3.selectByValue("931");

		DateTimeFormatter format1 = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDateTime now1 = LocalDateTime.now();
		driver.findElement(By.id("contract_startdate")).sendKeys(now1.format(format1));

		driver.findElement(By.id("contract_startdate_timezone")).sendKeys("IST");

		driver.findElement(By.id("contract_period")).sendKeys("1");

		Select cSelect4 = new Select(driver.findElement(By.id("contract_period_unit")));
		cSelect4.selectByValue("Days");

		driver.findElement(By.id("btn_accounts_gc_contracts_1_name")).click();
		String winHandleBefore1 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}

		driver.findElement(By.xpath("//tbody/tr[3]/td[2]/a")).click();
		driver.switchTo().window(winHandleBefore1);

		driver.findElement(By.id("btn_contacts_gc_contracts_1_name")).click();
		String winHandleBefore2 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("last_name_advanced")).clear();
		driver.findElement(By.id("last_name_advanced")).sendKeys("testing-SIT");
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore2);

		driver.findElement(By.id("factory_reference_no")).sendKeys(FactoryReferenceNumber);
		driver.findElement(By.id("sales_channel_code")).sendKeys(SalesChannelCode);
		driver.findElement(By.id("btn_corporate_name")).click();

		String winHandleBefore3 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}

		driver.findElement(By.xpath("//tbody/tr[3]/td[2]/a")).click();
		driver.switchTo().window(winHandleBefore3);

		driver.findElement(By.id("sales_rep_name")).sendKeys(SalesRepNameLatin);
		driver.findElement(By.id("sales_rep_name_1")).sendKeys(SalesRepNameLocal);
		driver.findElement(By.id("division_1")).sendKeys(DivisionLatin);
		driver.findElement(By.id("division_2")).sendKeys(DivisionLocal);
		driver.findElement(By.id("title_1")).sendKeys(TittleLatin);
		driver.findElement(By.id("title_2")).sendKeys(TittleLocal);
		driver.findElement(By.id("tel_no")).sendKeys(Telephone);
		driver.findElement(By.id("ext_no")).sendKeys(ExtnNumber);
		driver.findElement(By.id("fax_no")).sendKeys(FaxNumber);
		driver.findElement(By.id("mob_no")).sendKeys(MobileNumber);
		driver.findElement(By.id("email1")).sendKeys(mailId);

		Select cSelect5 = new Select(driver.findElement(By.id("product_specification_1")));
		cSelect5.selectByValue("347");

		driver.findElement(By.id("btn_product_name_1")).click();
		String winHandleBefore4 = driver.getWindowHandle();

		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}

		driver.findElement(By.id("name_advanced")).clear();
		driver.findElement(By.id("name_advanced")).sendKeys(ProductItem);
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore4);

		Select cSelect6 = new Select(driver.findElement(By.id("cust_contract_currency_1")));
		cSelect6.selectByValue("JPY");

		Select cSelect7 = new Select(driver.findElement(By.id("cust_billing_currency_1")));
		cSelect7.selectByValue("AFN");

		Select cSelect8 = new Select(driver.findElement(By.id("igc_contract_currency_1")));
		cSelect8.selectByValue("AFN");

		Select cSelect9 = new Select(driver.findElement(By.id("igc_billing_currency_1")));
		cSelect9.selectByValue("AFN");

		Select cSelect10 = new Select(driver.findElement(By.id("customer_billing_method_1")));
		cSelect10.selectByValue("eCSS_Billing");

		Select cSelect11 = new Select(driver.findElement(By.id("igc_settlement_method_1")));
		cSelect11.selectByValue("Quarterly_Settlement");

		driver.findElement(By.id("description_1")).sendKeys("Testing");

		DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yyyy");

		LocalDateTime now = LocalDateTime.now();
		LocalDateTime then = now.plusDays(10);

		driver.findElement(By.id("service_start_date_1")).sendKeys(now.format(format));
		driver.findElement(By.id("service_end_date_1")).sendKeys(then.format(format));
		driver.findElement(By.id("billing_start_date_1")).sendKeys(now.format(format));
		driver.findElement(By.id("billing_end_date_1")).sendKeys(then.format(format));

		driver.findElement(By.id("service_start_date_timezone_1")).sendKeys("IST");
		driver.findElement(By.id("service_end_date_timezone_1")).sendKeys("IST");
		driver.findElement(By.id("billing_start_date_timezone_1")).sendKeys("IST");
		driver.findElement(By.id("billing_end_date_timezone_1")).sendKeys("IST");
		driver.findElement(By.id("btn_tech_account_name_1")).click();

		String winHandleBefore5 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("last_name_advanced")).clear();
		driver.findElement(By.id("last_name_advanced")).sendKeys("latin-technology");
		driver.findElement(By.id("search_form_submit")).click();

		driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore5);
		driver.findElement(By.id("btn_bill_account_name_1")).click();

		String winHandleBefore6 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("last_name_advanced")).clear();
		driver.findElement(By.id("last_name_advanced")).sendKeys("test 3/2/2016");
		driver.findElement(By.id("search_form_submit")).click();

		driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore6);

		if (PaymentType.equalsIgnoreCase("General: Direct Deposit")) {
			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));

			oSelect.selectByValue("00");

			driver.findElement(By.id("dd_bank_code_1")).sendKeys("Bank007");
			driver.findElement(By.id("dd_branch_code_1")).sendKeys("dd_branch_code_1");

			Select oSelect1 = new Select(driver.findElement(By.id("dd_deposit_type_1")));
			oSelect1.selectByVisibleText("Tax Payment (General)");

			driver.findElement(By.id("dd_account_no_1")).sendKeys("12345678963636363");
			driver.findElement(By.id("dd_person_name_1_1")).sendKeys("Atul Dandin");
			driver.findElement(By.id("dd_person_name_2_1")).sendKeys("LocalAtul Dandin");
			driver.findElement(By.id("dd_payee_tel_no_1")).sendKeys("1234567890");
			driver.findElement(By.id("dd_payee_email_address_1")).sendKeys("dummy@email.com");
			driver.findElement(By.id("dd_remarks_1")).sendKeys("Remarks");
		} else if (PaymentType.equalsIgnoreCase("Account Transfer")) {

			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));

			oSelect.selectByValue("10");

			driver.findElement(By.id("at_account_no_1")).sendKeys("12345678963636363");
			driver.findElement(By.id("at_name_1")).sendKeys("Atul Dandin");
			driver.findElement(By.id("at_bank_code_1")).sendKeys("1234567890");
			driver.findElement(By.id("at_branch_code_1")).sendKeys("1234567890");

			Select oSelect1 = new Select(driver.findElement(By.id("at_deposit_type_1")));
			oSelect1.selectByVisibleText("Ordinary Deposit");

			Select oSelect2 = new Select(driver.findElement(By.id("at_account_no_display_1")));
			oSelect2.selectByVisibleText("Yes");

		} else if (PaymentType.equalsIgnoreCase("Postal Transfer")) {
			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));

			oSelect.selectByValue("30");

			// driver.findElement(By.id("pt_postal_passbook_1")).click();
			// driver.findElement(By.id("pt_postal_passbook_1")).clear();
			driver.findElement(By.id("pt_postal_passbook_1")).sendKeys("123");
			driver.findElement(By.id("pt_name_1")).sendKeys("Atul Dandin");
			driver.findElement(By.id("pt_postal_passbook_mark_1")).sendKeys("Remarks");

			Select oSelect2 = new Select(driver.findElement(By.id("pt_postal_passbook_no_display_1")));
			oSelect2.selectByVisibleText("Yes");

		} else if (PaymentType.equalsIgnoreCase("Credit Card")) {
			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));

			oSelect.selectByValue("60");
			driver.findElement(By.id("cc_credit_card_no_1")).sendKeys("12345678963636363");
			driver.findElement(By.id("cc_expiration_date_1")).sendKeys("10/13");

		} else {
			Reporter.log("Invalid Payment Type");
			Assert.fail();
		}

		// Select cSelect13 = new
		// Select(driver.findElement(By.id("product_config_1_561")));
		// cSelect13.selectByValue("14014");

		// Select cSelect14 = new
		// Select(driver.findElement(By.id("product_config_1_562")));
		// Select14.selectByValue("14015");

		// Select cSelect15 = new
		// Select(driver.findElement(By.id("product_config_1_565")));
		// cSelect15.selectByValue("1");

		// Select cSelect16 = new
		// Select(driver.findElement(By.id("product_config_1_567")));
		// cSelect16.selectByValue("14035");
		//
		Select cSelect17 = new Select(driver.findElement(By.id("product_config_1_565")));
		cSelect17.selectByValue("1");

		// product_config_1_565

		Thread.sleep(5000);

		driver.findElement(By.id("pp_customer_contract_price_1_160049")).click();
		driver.findElement(By.id("pp_igc_settlement_price_1_160049")).click();

		driver.findElement(By.id("pp_customer_contract_price_1_160049")).sendKeys("99.000");
		driver.findElement(By.id("pp_igc_settlement_price_1_160049")).sendKeys("99.000");

		Reporter.log("Contract Price and ICG price saved");

		driver.findElement(By.id("SAVE_FOOTER")).click();
		// srn.getscreenshot();

		//

	}

	@DataProvider
	public Object[][] CreateContractData() {
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}

	public static void WaitForElementPresent(String locator, int timeout) {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void verifyAutoGenerated() {
		String objectId = driver.findElement(By.id("id")).getText();
		String globalContractId = driver.findElement(By.id("global_contract_id")).getText();
		String dateCreated = driver.findElement(By.id("date_entered")).getText();
		String dateModified = driver.findElement(By.id("date_modified")).getText();

		if (!objectId.isEmpty()) {
			Reporter.log("RTC_Contract_008 - ObjectId auto generated");
			System.out.println("ObjectId auto generated");
		} else {

			Reporter.log("RTC_Contract_008 -ObjectId not generated");
			System.out.println("ObjectId not generated");
			Assert.fail("RTC_Contract_008 -ObjectId not generated");
		}

		if (!globalContractId.isEmpty()) {
			Reporter.log(" RTC_Contract_008-lobalContractId auto generated");
			System.out.println("GlobalContractId auto generated");
		} else {
			Reporter.log("GlobalContractId not generated");
			System.out.println("GlobalContractId not generated");
			Assert.fail("RTC_Contract_008 -GlobalContractId not generated");
		}

		if (!dateCreated.isEmpty()) {
			Reporter.log("RTC_Contract_008 -Date Created auto generated");
			System.out.println("Date Created auto generated");
		} else {
			Reporter.log("Date Created not generated");
			System.out.println("Date Created not generated");
			Assert.fail("RTC_Contract_008- Date Created not generated");
		}

		if (!dateModified.isEmpty()) {
			Reporter.log("RTC_Contract_008- Date Modified auto generated");
			System.out.println("Date Modified auto generated");
		} else {
			Reporter.log("Date Modified not generated");
			System.out.println("Date Modified not generated");
			Assert.fail("RTC_Contract_008-Date Modified not generated");
		}

	}

	public void createContract() throws InterruptedException {

		String winHandleBefore1 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.findElement(By.xpath(".//*[@id='addformlink']/input")).click();
		Select dropdown = new Select(driver.findElement(By.id("contact_type_c")));
		dropdown.selectByVisibleText("Contract");
		driver.findElement(By.id("btn_account_name")).click();
		String winHandleBefore = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("common_customer_id_c_advanced")).sendKeys("C2010040802");
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//tbody/tr[3]/td[2]/a")).click();
		driver.switchTo().window(winHandleBefore);
		driver.findElement(By.id("last_name")).sendKeys(
				"Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("name_2_c")).sendKeys(
				"Contact Person  Name Local  0123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("division_1_c")).sendKeys(
				"Division Latin 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("division_2_c")).sendKeys(
				"Division Local 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("title")).sendKeys(
				"Tittile  Latin 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("title_2_c")).sendKeys(
				"Tittile  Local 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("phone_work")).sendKeys("123456789012345000000");
		driver.findElement(By.id("ext_no_c")).sendKeys("123456");
		driver.findElement(By.id("phone_fax")).sendKeys("123456789012345000000");
		driver.findElement(By.id("phone_mobile")).sendKeys("123456789012345000000");
		driver.findElement(By.id("Contacts0emailAddress0")).sendKeys("dummy@gmail.com");
		Select dropdown1 = new Select(driver.findElement(By.id("c_country_id_c")));
		dropdown1.selectByVisibleText("Japan");
		Thread.sleep(3000);
		driver.findElement(By.id("ndb_addr1")).sendKeys("Japan Address 1");
		driver.findElement(By.id("ndb_addr2")).sendKeys("Japan Address 2");
		driver.findElement(By.id("postal_no_c")).sendKeys("112233");
		// scrn.getscreenshot();
		driver.findElement(By.id("Contacts_popupcreate_save_button")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("html/body/table[4]/tbody/tr[3]/td[2]/a")).click();
		driver.switchTo().window(winHandleBefore1);
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

	}

	/* Added by deep for filling new line item - Start */
	public void verifySecondNewLineItemFields() {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("add_line_item")).click();
		Select billScheme = new Select(driver.findElement(By.id("billing_type")));
		String billingScheme = billScheme.getFirstSelectedOption().getText();
		Select custContCurr = new Select(driver.findElement(By.id("cust_contract_currency_1")));
		String custContractCurr = custContCurr.getFirstSelectedOption().getText();
		Select custContCurr1 = new Select(driver.findElement(By.id("cust_contract_currency_2")));
		String custContractCurr1 = custContCurr1.getFirstSelectedOption().getText();
		if (custContractCurr.equals(custContractCurr1)) {
			Reporter.log("Customer Contract Currency copied from 1st contract line item to 2nd contract line item");
			System.out
					.println("Customer Contract Currency copied from 1st contract line item to 2nd contract line item");
		} else {
			System.out.println(
					"Customer Contract Currency not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
			Assert.fail(
					"Customer Contract Currency not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
		}
		Select custBilCur = new Select(driver.findElement(By.id("cust_billing_currency_1")));
		String custBillingCurr = custBilCur.getFirstSelectedOption().getText();
		Select custBilCur1 = new Select(driver.findElement(By.id("cust_billing_currency_2")));
		String custBillingCurr1 = custBilCur1.getFirstSelectedOption().getText();
		if (custBillingCurr.equals(custBillingCurr1)) {
			Reporter.log("Customer Billing Currency copied from 1st contract line item to 2nd contract line item");
			System.out
					.println("Customer Billing Currency copied from 1st contract line item to 2nd contract line item");
		} else {
			System.out.println(
					"Customer Billing Currency not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
			Assert.fail(
					"Customer Billing Currency not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
		}
		Select igcContCur = new Select(driver.findElement(By.id("igc_contract_currency_1")));
		String igcContractCurr = igcContCur.getFirstSelectedOption().getText();
		Select igcContCur1 = new Select(driver.findElement(By.id("igc_contract_currency_2")));
		String igcContractCurr1 = igcContCur1.getFirstSelectedOption().getText();
		if (igcContractCurr.equals(igcContractCurr1)) {
			Reporter.log(
					"Inter-Group-Company Contract Currency copied from 1st contract line item to 2nd contract line item");
			System.out.println(
					"Inter-Group-Company Contract Currency copied from 1st contract line item to 2nd contract line item");
		} else {
			System.out.println(
					"Inter-Group-Company Contract Currency not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
			Assert.fail(
					"Inter-Group-Company Contract Currency not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
		}
		Select igcBillCur = new Select(driver.findElement(By.id("igc_billing_currency_1")));
		String igcBillingCurr = igcBillCur.getFirstSelectedOption().getText();
		Select igcBillCur1 = new Select(driver.findElement(By.id("igc_billing_currency_2")));
		String igcBillingCurr1 = igcBillCur1.getFirstSelectedOption().getText();
		if (igcBillingCurr.equals(igcBillingCurr1)) {
			Reporter.log(
					"Inter-Group-Company Billing Currency copied from 1st contract line item to 2nd contract line item");
			System.out.println(
					"Inter-Group-Company Billing Currency copied from 1st contract line item to 2nd contract line item");
		} else {
			System.out.println(
					"Inter-Group-Company Billing Currency not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
			Assert.fail(
					"Inter-Group-Company Billing Currency not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
		}
		Select custBillMethod = new Select(driver.findElement(By.id("customer_billing_method_1")));
		String custBillingMethod = custBillMethod.getFirstSelectedOption().getText();
		Select custBillMethod1 = new Select(driver.findElement(By.id("customer_billing_method_2")));
		String custBillingMethod1 = custBillMethod1.getFirstSelectedOption().getText();
		if (custBillingMethod.equals(custBillingMethod1)) {
			Reporter.log("Customer Billing Method copied from 1st contract line item to 2nd contract line item");
			System.out.println("Customer Billing Method copied from 1st contract line item to 2nd contract line item");
		} else {
			System.out.println(
					"Customer Billing Method not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
			Assert.fail(
					"Customer Billing Method not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
		}
		Select igcSettleMethod = new Select(driver.findElement(By.id("igc_settlement_method_1")));
		String igcSettlementMethod = igcSettleMethod.getFirstSelectedOption().getText();
		Select igcSettleMethod1 = new Select(driver.findElement(By.id("igc_settlement_method_2")));
		String igcSettlementMethod1 = igcSettleMethod1.getFirstSelectedOption().getText();
		if (igcSettlementMethod.equals(igcSettlementMethod1)) {
			Reporter.log(
					"Inter-Group-Company Settlement Method copied from 1st contract line item to 2nd contract line item");
			System.out.println(
					"Inter-Group-Company Settlement Method copied from 1st contract line item to 2nd contract line item");
		} else {
			System.out.println(
					"Inter-Group-Company Settlement Method not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
			Assert.fail(
					"Inter-Group-Company Settlement Method not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
		}
		WebElement billingCustAccount = driver.findElement(By.id("bill_account_name_1"));
		String billCustAcc = billingCustAccount.getAttribute("value");
		WebElement billingCustAccount1 = driver.findElement(By.id("bill_account_name_2"));
		String billCustAcc1 = billingCustAccount1.getAttribute("value");
		if (billCustAcc.equals(billCustAcc1)) {
			Reporter.log("Billing Customer Account copied from 1st contract line item to 2nd contract line item");
			System.out.println("Billing Customer Account copied from 1st contract line item to 2nd contract line item");
		} else {
			System.out.println(
					"Billing Customer Account not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
			Assert.fail(
					"Billing Customer Account not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
		}
		WebElement billingAffliate = driver.findElement(By.id("gc_organization_name_1"));
		String billAff = billingAffliate.getAttribute("value");
		WebElement billingAffliate1 = driver.findElement(By.id("gc_organization_name_2"));
		String billAff1 = billingAffliate1.getAttribute("value");
		if (billAff.equals(billAff1)) {
			Reporter.log("Billing Affiliate copied from 1st contract line item to 2nd contract line item");
			System.out.println("Billing Affiliate copied from 1st contract line item to 2nd contract line item");
		} else {
			System.out.println(
					"Billing Affiliate not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
			Assert.fail(
					"Billing Affiliate not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
		}
		Select sel = new Select(driver.findElement(By.id("payment_type_1")));
		String payType = sel.getFirstSelectedOption().getText();
		Select sel1 = new Select(driver.findElement(By.id("payment_type_2")));
		String payType1 = sel1.getFirstSelectedOption().getText();
		if (payType.equals(payType1)) {
			if (payType.equals("General: Direct Deposit")) {
				String bankCode = driver.findElement(By.id("dd_bank_code_1")).getText();
				String branchCode = driver.findElement(By.id("dd_branch_code_1")).getText();
				String depositType = driver.findElement(By.id("dd_deposit_type_1")).getText();
				String accNo = driver.findElement(By.id("dd_account_no_1")).getText();
				String payeeContactPersonNameLatin = driver.findElement(By.id("dd_person_name_1_1")).getText();
				String payeeContactPersonNameLocal = driver.findElement(By.id("dd_person_name_2_1")).getText();
				String payeeTelephone = driver.findElement(By.id("dd_payee_tel_no_1")).getText();
				String payeeEmail = driver.findElement(By.id("dd_payee_email_address_1")).getText();
				String remark = driver.findElement(By.id("dd_remarks_1")).getText();
				if (bankCode.equals(driver.findElement(By.id("dd_bank_code_2")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Bank Code copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Bank Code copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Bank Code not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Bank Code not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (branchCode.equals(driver.findElement(By.id("dd_branch_code_2")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Branch Code copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Branch Code copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Branch Code not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Branch Code not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (depositType.equals(driver.findElement(By.id("dd_deposit_type_2")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Deposit Type copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Deposit Type copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Deposit Type not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Deposit Type not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (accNo.equals(driver.findElement(By.id("dd_account_no_2")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Account Number copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Account Number copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Account Number not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Account Number not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (payeeContactPersonNameLatin.equals(driver.findElement(By.id("dd_person_name_1_2")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Payee Contract Person Name (Latin Characters) copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Payee Contract Person Name (Latin Characters) copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Payee Contract Person Name (Latin Characters) not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Payee Contract Person Name (Latin Characters) not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (payeeContactPersonNameLocal.equals(driver.findElement(By.id("dd_person_name_2_2")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Payee Contract Person Name (Local Characters) copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Payee Contract Person Name (Local Characters) copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Payee Contract Person Name (Local Characters) not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Payee Contract Person Name (Local Characters) not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (payeeTelephone.equals(driver.findElement(By.id("dd_payee_tel_no_2")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Payee Telephone Number copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Payee Telephone Number copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Payee Telephone Number not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Payee Telephone Number not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (payeeEmail.equals(driver.findElement(By.id("dd_payee_email_address_2")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Payee E-mail Address copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Payee E-mail Address copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Payee E-mail Address not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Payee E-mail Address not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (remark.equals(driver.findElement(By.id("dd_remarks_2")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Remarks copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Remarks copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Remarks not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Remarks not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
			} else if (payType.equals("Account Transfer")) {
				String accHolderName = driver.findElement(By.id("at_name_1")).getText();
				String depositType = driver.findElement(By.id("at_deposit_type_1")).getText();
				String bankCode = driver.findElement(By.id("at_bank_code_1")).getText();
				String branchCode = driver.findElement(By.id("at_branch_code_1")).getText();
				String accNo = driver.findElement(By.id("at_account_no_1")).getText();
				String accNoDisp = driver.findElement(By.id("at_account_no_display_1")).getText();
				if (accHolderName.equals(driver.findElement(By.id("at_name_2")).getText())) {
					Reporter.log(
							"Payment Type Account Transfer - Account Holder Name copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Account Transfer - Account Holder Name copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Account Holder Name not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Account Transfer - Account Holder Name not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (depositType.equals(driver.findElement(By.id("at_deposit_type_2")).getText())) {
					Reporter.log(
							"Payment Type Account Transfer - Deposit Type copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Account Transfer - Deposit type copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Deposit Type not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Account Transfer - Deposit Type not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (bankCode.equals(driver.findElement(By.id("at_bank_code_2")).getText())) {
					Reporter.log(
							"Payment Type Account Transfer - Bank Code copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Account Transfer - Bank Code copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Bank Code not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Account Transfer - Bank Code not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (branchCode.equals(driver.findElement(By.id("at_branch_code_2")).getText())) {
					Reporter.log(
							"Payment Type Account Transfer - Branch Code copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Account Transfer - Branch Code copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Branch Code not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Account Transfer - Branch Code not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (accNo.equals(driver.findElement(By.id("at_account_no_2")).getText())) {
					Reporter.log(
							"Payment Type Account Transfer - Branch Code copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Account Transfer - Branch Code copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Branch Code not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Account Transfer - Branch Code not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (accNoDisp.equals(driver.findElement(By.id("at_account_no_display_2")).getText())) {
					Reporter.log(
							"Payment Type Account Transfer - Account Number Display copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Account Transfer - Account Number Display copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Account Number Display not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Account Transfer - Account Number Display not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
			} else if (payType.equals("Postal Transfer")) {
				String accHolderName = driver.findElement(By.id("pt_name_1")).getText();
				String postPassbookMark = driver.findElement(By.id("pt_postal_passbook_mark_1")).getText();
				String postPassBookNo = driver.findElement(By.id("pt_postal_passbook_1")).getText();
				String postPassBookNoDisp = driver.findElement(By.id("pt_postal_passbook_no_display_1")).getText();
				if (accHolderName.equals(driver.findElement(By.id("pt_name_2")).getText())) {
					Reporter.log(
							"Payment Type Postal Transfer - Account Holder Name copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Postal Transfer - Account Holder Name copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Postal Transfer - Account Holder Name not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Postal Transfer - Account Holder Name not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (postPassbookMark.equals(driver.findElement(By.id("pt_postal_passbook_mark_2")).getText())) {
					Reporter.log(
							"Payment Type Postal Transfer - Postal Passbook Mark copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Mark copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Mark not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Postal Transfer - Postal Passbook Mark not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (postPassBookNo.equals(driver.findElement(By.id("pt_postal_passbook_2")).getText())) {
					Reporter.log(
							"Payment Type Postal Transfer - Postal Passbook Number copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Number copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Number not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Postal Transfer - Postal Passbook Number not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (postPassBookNoDisp.equals(driver.findElement(By.id("pt_postal_passbook_no_display_2")).getText())) {
					Reporter.log(
							"Payment Type Postal Transfer - Postal Passbook Number Display copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Number Display copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Number Display not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Postal Transfer - Postal Passbook Number Display not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
			} else if (payType.equals("Credit Card")) {
				String creditCardNo = driver.findElement(By.id("cc_credit_card_no_1")).getText();
				String expDate = driver.findElement(By.id("cc_expiration_date_1")).getText();
				if (creditCardNo.equals(driver.findElement(By.id("cc_credit_card_no_2")).getText())) {
					Reporter.log(
							"Payment Type Credit Card - Credit Card Number copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Credit Card - Credit Card Number copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Credit Card - Credit Card Number not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Credit Card - Credit Card Number not copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
				if (expDate.equals(driver.findElement(By.id("cc_expiration_date_2")).getText())) {
					Reporter.log(
							"Payment Type Credit Card - Expiration Date copied from 1st contract line item to 2nd contract line item");
					System.out.println(
							"Payment Type Credit Card - Expiration Date copied from 1st contract line item to 2nd contract line item");
				} else {
					System.out.println(
							"Payment Type Credit Card - Expiration Date copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Credit Card - Expiration Date copied from 1st contract line item to 2nd contract line item - RTC-062 fail");
				}
			}
		}
	}

	public void verifySeperateSecondNewLineItemFields() {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("add_line_item")).click();
		Select billScheme = new Select(driver.findElement(By.id("billing_type")));
		String billingScheme = billScheme.getFirstSelectedOption().getText();
		Select custContCurr = new Select(driver.findElement(By.id("cust_contract_currency_1")));
		String custContractCurr = custContCurr.getFirstSelectedOption().getText();
		Select custContCurr1 = new Select(driver.findElement(By.id("cust_contract_currency_2")));
		String custContractCurr1 = custContCurr1.getFirstSelectedOption().getText();
		if (custContractCurr.equals(custContractCurr1)) {
			System.out.println(
					"Customer Contract Currency copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
			Assert.fail(
					"Customer Contract Currency copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
		} else {
			System.out.println(
					"Customer Contract Currency not copied from 1st contract line item to 2nd contract line item");
			Reporter.log("Customer Contract Currency not copied from 1st contract line item to 2nd contract line item");
		}
		Select custBilCur = new Select(driver.findElement(By.id("cust_billing_currency_1")));
		String custBillingCurr = custBilCur.getFirstSelectedOption().getText();
		Select custBilCur1 = new Select(driver.findElement(By.id("cust_billing_currency_2")));
		String custBillingCurr1 = custBilCur1.getFirstSelectedOption().getText();
		if (custBillingCurr.equals(custBillingCurr1)) {
			Assert.fail(
					"Customer Billing Currency copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
			System.out.println(
					"Customer Billing Currency copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
		} else {
			System.out.println(
					"Customer Billing Currency not copied from 1st contract line item to 2nd contract line item");
			Reporter.log("Customer Billing Currency not copied from 1st contract line item to 2nd contract line item");
		}
		Select igcContCur = new Select(driver.findElement(By.id("igc_contract_currency_1")));
		String igcContractCurr = igcContCur.getFirstSelectedOption().getText();
		Select igcContCur1 = new Select(driver.findElement(By.id("igc_contract_currency_2")));
		String igcContractCurr1 = igcContCur1.getFirstSelectedOption().getText();
		if (igcContractCurr.equals(igcContractCurr1)) {
			Assert.fail(
					"Inter-Group-Company Contract Currency copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
			System.out.println(
					"Inter-Group-Company Contract Currency copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
		} else {
			System.out.println(
					"Inter-Group-Company Contract Currency not copied from 1st contract line item to 2nd contract line item");
			Reporter.log(
					"Inter-Group-Company Contract Currency not copied from 1st contract line item to 2nd contract line item");
		}
		Select igcBillCur = new Select(driver.findElement(By.id("igc_billing_currency_1")));
		String igcBillingCurr = igcBillCur.getFirstSelectedOption().getText();
		Select igcBillCur1 = new Select(driver.findElement(By.id("igc_billing_currency_2")));
		String igcBillingCurr1 = igcBillCur1.getFirstSelectedOption().getText();
		if (igcBillingCurr.equals(igcBillingCurr1)) {
			Assert.fail(
					"Inter-Group-Company Billing Currency copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
			System.out.println(
					"Inter-Group-Company Billing Currency copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
		} else {
			System.out.println(
					"Inter-Group-Company Billing Currency not copied from 1st contract line item to 2nd contract line item");
			Reporter.log(
					"Inter-Group-Company Billing Currency not copied from 1st contract line item to 2nd contract line item");
		}
		Select custBillMethod = new Select(driver.findElement(By.id("customer_billing_method_1")));
		String custBillingMethod = custBillMethod.getFirstSelectedOption().getText();
		Select custBillMethod1 = new Select(driver.findElement(By.id("customer_billing_method_2")));
		String custBillingMethod1 = custBillMethod1.getFirstSelectedOption().getText();
		if (custBillingMethod.equals(custBillingMethod1)) {
			Assert.fail(
					"Customer Billing Method copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
			System.out.println(
					"Customer Billing Method copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
		} else {
			System.out.println(
					"Customer Billing Method not copied from 1st contract line item to 2nd contract line item");
			Reporter.log("Customer Billing Method not copied from 1st contract line item to 2nd contract line item");
		}
		Select igcSettleMethod = new Select(driver.findElement(By.id("igc_settlement_method_1")));
		String igcSettlementMethod = igcSettleMethod.getFirstSelectedOption().getText();
		Select igcSettleMethod1 = new Select(driver.findElement(By.id("igc_settlement_method_2")));
		String igcSettlementMethod1 = igcSettleMethod1.getFirstSelectedOption().getText();
		if (igcSettlementMethod.equals(igcSettlementMethod1)) {
			Assert.fail(
					"Inter-Group-Company Settlement Method copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
			System.out.println(
					"Inter-Group-Company Settlement Method copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
		} else {
			System.out.println(
					"Inter-Group-Company Settlement Method not copied from 1st contract line item to 2nd contract line item");
			Reporter.log(
					"Inter-Group-Company Settlement Method not copied from 1st contract line item to 2nd contract line item");
		}
		WebElement billingCustAccount = driver.findElement(By.id("bill_account_name_1"));
		String billCustAcc = billingCustAccount.getAttribute("value");
		WebElement billingCustAccount1 = driver.findElement(By.id("bill_account_name_2"));
		String billCustAcc1 = billingCustAccount1.getAttribute("value");
		if (billCustAcc.equals(billCustAcc1)) {
			Assert.fail(
					"Billing Customer Account copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
			System.out.println(
					"Billing Customer Account copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
		} else {
			System.out.println(
					"Billing Customer Account not copied from 1st contract line item to 2nd contract line item");
			Reporter.log("Billing Customer Account not copied from 1st contract line item to 2nd contract line item");
		}
		/*
		 * WebElement billingAffliate =
		 * driver.findElement(By.id("gc_organization_name_1")); String billAff =
		 * billingAffliate.getAttribute("value"); WebElement billingAffliate1 =
		 * driver.findElement(By.id("gc_organization_name_2")); String billAff1
		 * = billingAffliate1.getAttribute("value");
		 * if(billAff.equals(billAff1)){ Assert.
		 * fail("Billing Affiliate copied from 1st contract line item to 2nd contract line item - RTC-063 fail"
		 * ); System.out.
		 * println("Billing Affiliate copied from 1st contract line item to 2nd contract line item - RTC-063 fail"
		 * ); } else { System.out.
		 * println("Billing Affiliate not copied from 1st contract line item to 2nd contract line item"
		 * ); Reporter.
		 * log("Billing Affiliate not copied from 1st contract line item to 2nd contract line item"
		 * ); }
		 */
		Select sel = new Select(driver.findElement(By.id("payment_type_1")));
		String payType = sel.getFirstSelectedOption().getText();
		Select sel1 = new Select(driver.findElement(By.id("payment_type_2")));
		String payType1 = sel1.getFirstSelectedOption().getText();
		if (payType.equals(payType1)) {
			if (payType.equals("General: Direct Deposit")) {
				String bankCode = driver.findElement(By.id("dd_bank_code_1")).getText();
				String branchCode = driver.findElement(By.id("dd_branch_code_1")).getText();
				String depositType = driver.findElement(By.id("dd_deposit_type_1")).getText();
				String accNo = driver.findElement(By.id("dd_account_no_1")).getText();
				String payeeContactPersonNameLatin = driver.findElement(By.id("dd_person_name_1_1")).getText();
				String payeeContactPersonNameLocal = driver.findElement(By.id("dd_person_name_2_1")).getText();
				String payeeTelephone = driver.findElement(By.id("dd_payee_tel_no_1")).getText();
				String payeeEmail = driver.findElement(By.id("dd_payee_email_address_1")).getText();
				String remark = driver.findElement(By.id("dd_remarks_1")).getText();
				if (bankCode.equals(driver.findElement(By.id("dd_bank_code_2")).getText())) {
					Assert.fail(
							"Payment Type Direct Deposit - Bank Code copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Direct Deposit - Bank Code copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Bank Code not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Direct Deposit - Bank Code not copied from 1st contract line item to 2nd contract line item");
				}
				if (branchCode.equals(driver.findElement(By.id("dd_branch_code_2")).getText())) {
					Assert.fail(
							"Payment Type Direct Deposit - Branch Code copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Direct Deposit - Branch Code copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Branch Code not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Direct Deposit - Branch Code not copied from 1st contract line item to 2nd contract line item");
				}
				if (depositType.equals(driver.findElement(By.id("dd_deposit_type_2")).getText())) {
					Assert.fail(
							"Payment Type Direct Deposit - Deposit Type copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Direct Deposit - Deposit Type copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Deposit Type not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Direct Deposit - Deposit Type not copied from 1st contract line item to 2nd contract line item");
				}
				if (accNo.equals(driver.findElement(By.id("dd_account_no_2")).getText())) {
					Assert.fail(
							"Payment Type Direct Deposit - Account Number copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Direct Deposit - Account Number copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Account Number not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Direct Deposit - Account Number not copied from 1st contract line item to 2nd contract line item");
				}
				if (payeeContactPersonNameLatin.equals(driver.findElement(By.id("dd_person_name_1_2")).getText())) {
					Assert.fail(
							"Payment Type Direct Deposit - Payee Contract Person Name (Latin Characters) copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Direct Deposit - Payee Contract Person Name (Latin Characters) copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Payee Contract Person Name (Latin Characters) not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Direct Deposit - Payee Contract Person Name (Latin Characters) not copied from 1st contract line item to 2nd contract line item");
				}
				if (payeeContactPersonNameLocal.equals(driver.findElement(By.id("dd_person_name_2_2")).getText())) {
					Assert.fail(
							"Payment Type Direct Deposit - Payee Contract Person Name (Local Characters) copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Direct Deposit - Payee Contract Person Name (Local Characters) copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Payee Contract Person Name (Local Characters) not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Direct Deposit - Payee Contract Person Name (Local Characters) not copied from 1st contract line item to 2nd contract line item");
				}
				if (payeeTelephone.equals(driver.findElement(By.id("dd_payee_tel_no_2")).getText())) {
					Assert.fail(
							"Payment Type Direct Deposit - Payee Telephone Number copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Direct Deposit - Payee Telephone Number copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Payee Telephone Number not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Direct Deposit - Payee Telephone Number not copied from 1st contract line item to 2nd contract line item");
				}
				if (payeeEmail.equals(driver.findElement(By.id("dd_payee_email_address_2")).getText())) {
					Assert.fail(
							"Payment Type Direct Deposit - Payee E-mail Address copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Direct Deposit - Payee E-mail Address copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Payee E-mail Address not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Direct Deposit - Payee E-mail Address not copied from 1st contract line item to 2nd contract line item");
				}
				if (remark.equals(driver.findElement(By.id("dd_remarks_2")).getText())) {
					Assert.fail(
							"Payment Type Direct Deposit - Remarks copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Direct Deposit - Remarks copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Remarks not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Direct Deposit - Remarks not copied from 1st contract line item to 2nd contract line item");
				}
			} else if (payType.equals("Account Transfer")) {
				String accHolderName = driver.findElement(By.id("at_name_1")).getText();
				String depositType = driver.findElement(By.id("at_deposit_type_1")).getText();
				String bankCode = driver.findElement(By.id("at_bank_code_1")).getText();
				String branchCode = driver.findElement(By.id("at_branch_code_1")).getText();
				String accNo = driver.findElement(By.id("at_account_no_1")).getText();
				String accNoDisp = driver.findElement(By.id("at_account_no_display_1")).getText();
				if (accHolderName.equals(driver.findElement(By.id("at_name_2")).getText())) {
					Assert.fail(
							"Payment Type Account Transfer - Account Holder Name copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Account Transfer - Account Holder Name copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Account Holder Name not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Account Transfer - Account Holder Name not copied from 1st contract line item to 2nd contract line item");
				}
				if (depositType.equals(driver.findElement(By.id("at_deposit_type_2")).getText())) {
					Assert.fail(
							"Payment Type Account Transfer - Deposit Type copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Account Transfer - Deposit type copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Deposit Type not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Account Transfer - Deposit Type not copied from 1st contract line item to 2nd contract line item");
				}
				if (bankCode.equals(driver.findElement(By.id("at_bank_code_2")).getText())) {
					Assert.fail(
							"Payment Type Account Transfer - Bank Code copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Account Transfer - Bank Code copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Bank Code not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Account Transfer - Bank Code not copied from 1st contract line item to 2nd contract line item");
				}
				if (branchCode.equals(driver.findElement(By.id("at_branch_code_2")).getText())) {
					Assert.fail(
							"Payment Type Account Transfer - Branch Code copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Account Transfer - Branch Code copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Branch Code not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Account Transfer - Branch Code not copied from 1st contract line item to 2nd contract line item");
				}
				if (accNo.equals(driver.findElement(By.id("at_account_no_2")).getText())) {
					Assert.fail(
							"Payment Type Account Transfer - Branch Code copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Account Transfer - Branch Code copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Branch Code not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Account Transfer - Branch Code not copied from 1st contract line item to 2nd contract line item");
				}
				if (accNoDisp.equals(driver.findElement(By.id("at_account_no_display_2")).getText())) {
					Assert.fail(
							"Payment Type Account Transfer - Account Number Display copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Account Transfer - Account Number Display copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Account Number Display not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Account Transfer - Account Number Display not copied from 1st contract line item to 2nd contract line item");
				}
			} else if (payType.equals("Postal Transfer")) {
				String accHolderName = driver.findElement(By.id("pt_name_1")).getText();
				String postPassbookMark = driver.findElement(By.id("pt_postal_passbook_mark_1")).getText();
				String postPassBookNo = driver.findElement(By.id("pt_postal_passbook_1")).getText();
				String postPassBookNoDisp = driver.findElement(By.id("pt_postal_passbook_no_display_1")).getText();
				if (accHolderName.equals(driver.findElement(By.id("pt_name_2")).getText())) {
					Assert.fail(
							"Payment Type Postal Transfer - Account Holder Name copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Postal Transfer - Account Holder Name copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Postal Transfer - Account Holder Name not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Postal Transfer - Account Holder Name not copied from 1st contract line item to 2nd contract line item");
				}
				if (postPassbookMark.equals(driver.findElement(By.id("pt_postal_passbook_mark_2")).getText())) {
					Assert.fail(
							"Payment Type Postal Transfer - Postal Passbook Mark copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Mark copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Mark not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Postal Transfer - Postal Passbook Mark not copied from 1st contract line item to 2nd contract line item");
				}
				if (postPassBookNo.equals(driver.findElement(By.id("pt_postal_passbook_2")).getText())) {
					Assert.fail(
							"Payment Type Postal Transfer - Postal Passbook Number copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Number copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Number not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Postal Transfer - Postal Passbook Number not copied from 1st contract line item to 2nd contract line item");
				}
				if (postPassBookNoDisp.equals(driver.findElement(By.id("pt_postal_passbook_no_display_2")).getText())) {
					Assert.fail(
							"Payment Type Postal Transfer - Postal Passbook Number Display copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Number Display copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Number Display not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Postal Transfer - Postal Passbook Number Display not copied from 1st contract line item to 2nd contract line item");
				}
			} else if (payType.equals("Credit Card")) {
				String creditCardNo = driver.findElement(By.id("cc_credit_card_no_1")).getText();
				String expDate = driver.findElement(By.id("cc_expiration_date_1")).getText();
				if (creditCardNo.equals(driver.findElement(By.id("cc_credit_card_no_2")).getText())) {
					System.out.println(
							"Payment Type Credit Card - Credit Card Number copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					Assert.fail(
							"Payment Type Credit Card - Credit Card Number copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Credit Card - Credit Card Number not copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Credit Card - Credit Card Number not copied from 1st contract line item to 2nd contract line item");
				}
				if (expDate.equals(driver.findElement(By.id("cc_expiration_date_2")).getText())) {
					System.out.println(
							"Payment Type Credit Card - Expiration Date copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
					Assert.fail(
							"Payment Type Credit Card - Expiration Date copied from 1st contract line item to 2nd contract line item - RTC-063 fail");
				} else {
					System.out.println(
							"Payment Type Credit Card - Expiration Date copied from 1st contract line item to 2nd contract line item");
					Reporter.log(
							"Payment Type Credit Card - Expiration Date copied from 1st contract line item to 2nd contract line item");
				}
			}
		}
	}

	public void verifyThirdNewLineItemFields() {
		// driver.switchTo().defaultContent();
		// driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("add_line_item")).click();
		Select billScheme = new Select(driver.findElement(By.id("billing_type")));
		String billingScheme = billScheme.getFirstSelectedOption().getText();

		Select custContCurr = new Select(driver.findElement(By.id("cust_contract_currency_1")));
		String custContractCurr = custContCurr.getFirstSelectedOption().getText();
		Select custContCurr1 = new Select(driver.findElement(By.id("cust_contract_currency_3")));
		String custContractCurr1 = custContCurr1.getFirstSelectedOption().getText();
		if (custContractCurr.equals(custContractCurr1)) {
			Reporter.log("Customer Contract Currency copied from 1st contract line item to 3rd contract line item");
			System.out
					.println("Customer Contract Currency copied from 1st contract line item to 3rd contract line item");
		} else {
			System.out.println(
					"Customer Contract Currency not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
			Assert.fail(
					"Customer Contract Currency not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
		}
		Select custBilCur = new Select(driver.findElement(By.id("cust_billing_currency_1")));
		String custBillingCurr = custBilCur.getFirstSelectedOption().getText();
		Select custBilCur1 = new Select(driver.findElement(By.id("cust_billing_currency_3")));
		String custBillingCurr1 = custBilCur1.getFirstSelectedOption().getText();
		if (custBillingCurr.equals(custBillingCurr1)) {
			Reporter.log("Customer Billing Currency copied from 1st contract line item to 3rd contract line item");
			System.out
					.println("Customer Billing Currency copied from 1st contract line item to 3rd contract line item");
		} else {
			System.out.println(
					"Customer Billing Currency not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
			Assert.fail(
					"Customer Billing Currency not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
		}
		Select igcContCur = new Select(driver.findElement(By.id("igc_contract_currency_1")));
		String igcContractCurr = igcContCur.getFirstSelectedOption().getText();
		Select igcContCur1 = new Select(driver.findElement(By.id("igc_contract_currency_3")));
		String igcContractCurr1 = igcContCur1.getFirstSelectedOption().getText();
		if (igcContractCurr.equals(igcContractCurr1)) {
			Reporter.log(
					"Inter-Group-Company Contract Currency copied from 1st contract line item to 3rd contract line item");
			System.out.println(
					"Inter-Group-Company Contract Currency copied from 1st contract line item to 3rd contract line item");
		} else {
			System.out.println(
					"Inter-Group-Company Contract Currency not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
			Assert.fail(
					"Inter-Group-Company Contract Currency not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
		}
		Select igcBillCur = new Select(driver.findElement(By.id("igc_billing_currency_1")));
		String igcBillingCurr = igcBillCur.getFirstSelectedOption().getText();
		Select igcBillCur1 = new Select(driver.findElement(By.id("igc_billing_currency_3")));
		String igcBillingCurr1 = igcBillCur1.getFirstSelectedOption().getText();
		if (igcBillingCurr.equals(igcBillingCurr1)) {
			Reporter.log(
					"Inter-Group-Company Billing Currency copied from 1st contract line item to 3rd contract line item");
			System.out.println(
					"Inter-Group-Company Billing Currency copied from 1st contract line item to 3rd contract line item");
		} else {
			System.out.println(
					"Inter-Group-Company Billing Currency not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
			Assert.fail(
					"Inter-Group-Company Billing Currency not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
		}
		Select custBillMethod = new Select(driver.findElement(By.id("customer_billing_method_1")));
		String custBillingMethod = custBillMethod.getFirstSelectedOption().getText();
		Select custBillMethod1 = new Select(driver.findElement(By.id("customer_billing_method_3")));
		String custBillingMethod1 = custBillMethod1.getFirstSelectedOption().getText();
		if (custBillingMethod.equals(custBillingMethod1)) {
			Reporter.log("Customer Billing Method copied from 1st contract line item to 3rd contract line item");
			System.out.println("Customer Billing Method copied from 1st contract line item to 3rd contract line item");
		} else {
			System.out.println(
					"Customer Billing Method not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
			Assert.fail(
					"Customer Billing Method not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
		}
		Select igcSettleMethod = new Select(driver.findElement(By.id("igc_settlement_method_1")));
		String igcSettlementMethod = igcSettleMethod.getFirstSelectedOption().getText();
		Select igcSettleMethod1 = new Select(driver.findElement(By.id("igc_settlement_method_3")));
		String igcSettlementMethod1 = igcSettleMethod1.getFirstSelectedOption().getText();
		if (igcSettlementMethod.equals(igcSettlementMethod1)) {
			Reporter.log(
					"Inter-Group-Company Settlement Method copied from 1st contract line item to 3rd contract line item");
			System.out.println(
					"Inter-Group-Company Settlement Method copied from 1st contract line item to 3rd contract line item");
		} else {
			System.out.println(
					"Inter-Group-Company Settlement Method not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
			Assert.fail(
					"Inter-Group-Company Settlement Method not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
		}
		WebElement billingCustAccount = driver.findElement(By.id("bill_account_name_1"));
		String billCustAcc = billingCustAccount.getAttribute("value");
		WebElement billingCustAccount1 = driver.findElement(By.id("bill_account_name_3"));
		String billCustAcc1 = billingCustAccount1.getAttribute("value");
		if (billCustAcc.equals(billCustAcc1)) {
			Reporter.log("Billing Customer Account copied from 1st contract line item to 3rd contract line item");
			System.out.println("Billing Customer Account copied from 1st contract line item to 3rd contract line item");
		} else {
			System.out.println(
					"Billing Customer Account not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
			Assert.fail(
					"Billing Customer Account not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
		}
		WebElement billingAffliate = driver.findElement(By.id("gc_organization_name_1"));
		String billAff = billingAffliate.getAttribute("value");
		WebElement billingAffliate1 = driver.findElement(By.id("gc_organization_name_3"));
		String billAff1 = billingAffliate1.getAttribute("value");
		if (billAff.equals(billAff1)) {
			Reporter.log("Billing Affiliate copied from 1st contract line item to 3rd contract line item");
			System.out.println("Billing Affiliate copied from 1st contract line item to 3rd contract line item");
		} else {
			System.out.println(
					"Billing Affiliate not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
			Assert.fail(
					"Billing Affiliate not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
		}
		Select sel = new Select(driver.findElement(By.id("payment_type_1")));
		String payType = sel.getFirstSelectedOption().getText();
		Select sel1 = new Select(driver.findElement(By.id("payment_type_3")));
		String payType1 = sel1.getFirstSelectedOption().getText();
		System.out.println("PayType::" + payType);
		if (payType.equals(payType1)) {
			if (payType.equals("General: Direct Deposit")) {
				String bankCode = driver.findElement(By.id("dd_bank_code_1")).getText();
				String branchCode = driver.findElement(By.id("dd_branch_code_1")).getText();
				String depositType = driver.findElement(By.id("dd_deposit_type_1")).getText();
				String accNo = driver.findElement(By.id("dd_account_no_1")).getText();
				String payeeContactPersonNameLatin = driver.findElement(By.id("dd_person_name_1_1")).getText();
				String payeeContactPersonNameLocal = driver.findElement(By.id("dd_person_name_2_1")).getText();
				String payeeTelephone = driver.findElement(By.id("dd_payee_tel_no_1")).getText();
				String payeeEmail = driver.findElement(By.id("dd_payee_email_address_1")).getText();
				String remark = driver.findElement(By.id("dd_remarks_1")).getText();
				if (bankCode.equals(driver.findElement(By.id("dd_bank_code_3")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Bank Code copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Bank Code copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Bank Code not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Bank Code not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (branchCode.equals(driver.findElement(By.id("dd_branch_code_3")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Branch Code copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Branch Code copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Branch Code not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Branch Code not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (depositType.equals(driver.findElement(By.id("dd_deposit_type_3")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Deposit Type copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Deposit Type copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Deposit Type not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Deposit Type not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (accNo.equals(driver.findElement(By.id("dd_account_no_3")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Account Number copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Account Number copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Account Number not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Account Number not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (payeeContactPersonNameLatin.equals(driver.findElement(By.id("dd_person_name_1_3")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Payee Contract Person Name (Latin Characters) copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Payee Contract Person Name (Latin Characters) copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Payee Contract Person Name (Latin Characters) not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Payee Contract Person Name (Latin Characters) not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (payeeContactPersonNameLocal.equals(driver.findElement(By.id("dd_person_name_2_3")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Payee Contract Person Name (Local Characters) copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Payee Contract Person Name (Local Characters) copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Payee Contract Person Name (Local Characters) not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Payee Contract Person Name (Local Characters) not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (payeeTelephone.equals(driver.findElement(By.id("dd_payee_tel_no_3")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Payee Telephone Number copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Payee Telephone Number copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Payee Telephone Number not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Payee Telephone Number not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (payeeEmail.equals(driver.findElement(By.id("dd_payee_email_address_3")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Payee E-mail Address copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Payee E-mail Address copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Payee E-mail Address not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Payee E-mail Address not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (remark.equals(driver.findElement(By.id("dd_remarks_3")).getText())) {
					Reporter.log(
							"Payment Type Direct Deposit - Remarks copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Direct Deposit - Remarks copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Direct Deposit - Remarks not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Direct Deposit - Remarks not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
			} else if (payType.equals("Account Transfer")) {
				String accHolderName = driver.findElement(By.id("at_name_1")).getText();
				String depositType = driver.findElement(By.id("at_deposit_type_1")).getText();
				String bankCode = driver.findElement(By.id("at_bank_code_1")).getText();
				String branchCode = driver.findElement(By.id("at_branch_code_1")).getText();
				String accNo = driver.findElement(By.id("at_account_no_1")).getText();
				String accNoDisp = driver.findElement(By.id("at_account_no_display_1")).getText();
				if (accHolderName.equals(driver.findElement(By.id("at_name_3")).getText())) {
					Reporter.log(
							"Payment Type Account Transfer - Account Holder Name copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Account Transfer - Account Holder Name copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Account Holder Name not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Account Transfer - Account Holder Name not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (depositType.equals(driver.findElement(By.id("at_deposit_type_3")).getText())) {
					Reporter.log(
							"Payment Type Account Transfer - Deposit Type copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Account Transfer - Deposit type copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Deposit Type not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Account Transfer - Deposit Type not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (bankCode.equals(driver.findElement(By.id("at_bank_code_3")).getText())) {
					Reporter.log(
							"Payment Type Account Transfer - Bank Code copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Account Transfer - Bank Code copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Bank Code not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Account Transfer - Bank Code not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (branchCode.equals(driver.findElement(By.id("at_branch_code_3")).getText())) {
					Reporter.log(
							"Payment Type Account Transfer - Branch Code copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Account Transfer - Branch Code copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Branch Code not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Account Transfer - Branch Code not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (accNo.equals(driver.findElement(By.id("at_account_no_3")).getText())) {
					Reporter.log(
							"Payment Type Account Transfer - Branch Code copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Account Transfer - Branch Code copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Branch Code not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Account Transfer - Branch Code not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (accNoDisp.equals(driver.findElement(By.id("at_account_no_display_3")).getText())) {
					Reporter.log(
							"Payment Type Account Transfer - Account Number Display copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Account Transfer - Account Number Display copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Account Transfer - Account Number Display not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Account Transfer - Account Number Display not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
			} else if (payType.equals("Postal Transfer")) {
				String accHolderName = driver.findElement(By.id("pt_name_1")).getText();
				String postPassbookMark = driver.findElement(By.id("pt_postal_passbook_mark_1")).getText();
				String postPassBookNo = driver.findElement(By.id("pt_postal_passbook_1")).getText();
				String postPassBookNoDisp = driver.findElement(By.id("pt_postal_passbook_no_display_1")).getText();
				if (accHolderName.equals(driver.findElement(By.id("pt_name_3")).getText())) {
					Reporter.log(
							"Payment Type Postal Transfer - Account Holder Name copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Postal Transfer - Account Holder Name copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Postal Transfer - Account Holder Name not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Postal Transfer - Account Holder Name not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (postPassbookMark.equals(driver.findElement(By.id("pt_postal_passbook_mark_3")).getText())) {
					Reporter.log(
							"Payment Type Postal Transfer - Postal Passbook Mark copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Mark copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Mark not copied from 1st contract line item to 3rd contract line item - RTC-062 fail ");
					Assert.fail(
							"Payment Type Postal Transfer - Postal Passbook Mark not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (postPassBookNo.equals(driver.findElement(By.id("pt_postal_passbook_3")).getText())) {
					Reporter.log(
							"Payment Type Postal Transfer - Postal Passbook Number copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Number copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Number not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Postal Transfer - Postal Passbook Number not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (postPassBookNoDisp.equals(driver.findElement(By.id("pt_postal_passbook_no_display_3")).getText())) {
					Reporter.log(
							"Payment Type Postal Transfer - Postal Passbook Number Display copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Number Display copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Postal Transfer - Postal Passbook Number Display not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Postal Transfer - Postal Passbook Number Display not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
			} else if (payType.equals("Credit Card")) {
				String creditCardNo = driver.findElement(By.id("cc_credit_card_no_1")).getText();
				String expDate = driver.findElement(By.id("cc_expiration_date_1")).getText();
				if (creditCardNo.equals(driver.findElement(By.id("cc_credit_card_no_3")).getText())) {
					Reporter.log(
							"Payment Type Credit Card - Credit Card Number copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Credit Card - Credit Card Number copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Credit Card - Credit Card Number not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Credit Card - Credit Card Number not copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
				if (expDate.equals(driver.findElement(By.id("cc_expiration_date_3")).getText())) {
					Reporter.log(
							"Payment Type Credit Card - Expiration Date copied from 1st contract line item to 3rd contract line item");
					System.out.println(
							"Payment Type Credit Card - Expiration Date copied from 1st contract line item to 3rd contract line item");
				} else {
					System.out.println(
							"Payment Type Credit Card - Expiration Date copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
					Assert.fail(
							"Payment Type Credit Card - Expiration Date copied from 1st contract line item to 3rd contract line item - RTC-062 fail");
				}
			}
		}
	}

	public void verifyProductSpecificaionTab() {
		String prodSpec = driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[2]")).getText();
		String prodSpec1 = driver.findElement(By.xpath(".//*[@id='dropdown-li-2']/div[2]")).getText();
		String prodSpec2 = driver.findElement(By.xpath(".//*[@id='dropdown-li-3']/div[2]")).getText();
		if (!prodSpec.isEmpty() && !prodSpec1.isEmpty() && !prodSpec2.isEmpty()) {
			System.out.println("Product specification name is displayed on closed line item tab");
			Reporter.log("Product specification name is displayed on closed line item tab");
		} else {
			System.out.println("Product specification name is not displayed on closed line item tab - RTC-072 fail");
			Assert.fail("Product specification name is not displayed on closed line item tab - RTC-072 fail");
		}
	}

	public void addNewLineItem() {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("add_line_item")).click();
		/*
		 * String billingScheme =
		 * driver.findElement(By.id("billing_type")).getText(); Select
		 * prodSpecification = new
		 * Select(driver.findElement(By.id("product_specification_2")));
		 * prodSpecification.
		 * selectByVisibleText("Managed Windows Server - on ECL2.0 (Add-on Optional)"
		 * ); if(!billingScheme.equals("One-stop")){ Select custContCurr = new
		 * Select(driver.findElement(By.id("cust_contract_currency_2")));
		 * custContCurr.selectByValue("JPY"); Select custBillCurr = new
		 * Select(driver.findElement(By.id("cust_billing_currency_2")));
		 * custBillCurr.selectByValue("AFN"); Select igcContCurr = new
		 * Select(driver.findElement(By.id("igc_contract_currency_2")));
		 * igcContCurr.selectByValue("AFN"); Select igcBillCurr = new
		 * Select(driver.findElement(By.id("igc_billing_currency_2")));
		 * igcBillCurr.selectByValue("AFN"); Select custBillMethod = new
		 * Select(driver.findElement(By.id("customer_billing_method_2")));
		 * custBillMethod.selectByValue("eCSS_Billing"); Select
		 * igcSettlementMethod = new
		 * Select(driver.findElement(By.id("igc_settlement_method_2")));
		 * igcSettlementMethod.selectByValue("No_Settlement_per_Contract");
		 * driver.findElement(By.id("btn_bill_account_name_2")).click(); String
		 * winHandleBefore = driver.getWindowHandle(); for (String handle :
		 * driver.getWindowHandles()) { driver.switchTo().window(handle); }
		 * driver.findElement(By.id("last_name_advanced")).clear(); //
		 * driver.findElement(By.id("last_name_advanced")).
		 * sendKeys("test 3/2/2016");
		 * driver.findElement(By.id("last_name_advanced")) .sendKeys(
		 * "Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789"
		 * ); driver.findElement(By.id("search_form_submit")).click();
		 * driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click(
		 * ); driver.switchTo().window(winHandleBefore);
		 * driver.switchTo().frame(driver.findElement(By.id("bwc-frame"))); }
		 * driver.findElement(By.id("btn_tech_account_name_2")).click(); String
		 * winHandleBefore1 = driver.getWindowHandle(); for (String handle :
		 * driver.getWindowHandles()) { driver.switchTo().window(handle); }
		 * driver.findElement(By.id("last_name_advanced")).clear();
		 * driver.findElement(By.id("last_name_advanced")) .sendKeys(
		 * "Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789"
		 * ); driver.findElement(By.id("search_form_submit")).click();
		 * 
		 * driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click(
		 * ); driver.switchTo().window(winHandleBefore1);
		 * driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		 */

	}

	/* Added by deep for filling new line item - End */

	/* Added By Sky */
	public void ContractDataFillAdmin(String ContractName, String ContractDesc, String ProductsID,
			String ProductOffering, String ContractStartDateTimeZone, String ContractEndDateTimeZone,
			String FactoryReferenceNumber, String SalesChannelCode, String SalesRepNameLatin, String SalesRepNameLocal,
			String DivisionLatin, String DivisionLocal, String TittleLatin, String TittleLocal, String Telephone,
			String ExtnNumber, String FaxNumber, String MobileNumber, String mailId, String ProductItem,
			String ProductSpecification, String ProductConfiguration, String ProductConfiguration1,
			String ProductConfiguration2, String ProductConfiguration3, String ProductConfiguration4,
			String ProductConfiguration5, String ProductConfiguration6, String ProductConfiguration7,
			String PaymentType, String ProductInformation1, String ProductInformation2, String ServiceStartDateTimeZone,
			String ServiceEndDateTimeZone, String BillingStartDateTimeZone, String BillingEndDateTimeZone)
			throws Exception {
		Thread.sleep(10000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		// WaitForElementPresent(".//*[@id='name']", 30000);
		driver.findElement(By.id("name")).sendKeys(ContractName);
		driver.findElement(By.id("description")).sendKeys(ContractDesc);
		Thread.sleep(1000);
		// Remove Gobal Team

		driver.findElement(By.xpath(".//*[@id='remove_team_name_collection_0']")).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(".//*[@id='teamSelect']")).click();
		Thread.sleep(1000);
		String winHandleBefore1 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}

		driver.findElement(By.id("team_name_input")).clear();
		driver.findElement(By.id("team_name_input")).sendKeys("NTTJ SO");
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath(".//*[@id='MassUpdate']/table[2]/tbody/tr[3]/td[2]")).click();

		driver.switchTo().window(winHandleBefore1);

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		driver.findElement(By.xpath(".//*[@id='primary_team_name_collection_0']")).click();

		driver.findElement(By.id("btn_pm_products_gc_contracts_1_name")).click();
		String winHandleBefore = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("products_id_advanced")).clear();
		driver.findElement(By.id("products_id_advanced")).sendKeys(ProductsID);
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore);

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		Select cSelect1 = new Select(driver.findElement(By.id("contract_type")));
		cSelect1.selectByValue("GMSA");

		Select cSelect2 = new Select(driver.findElement(By.id("contract_scheme")));
		cSelect2.selectByValue("GSC");

		Select cSelect3 = new Select(driver.findElement(By.id("product_offering")));
		cSelect3.selectByVisibleText(ProductOffering);
		// cSelect3.selectByValue("929");

		DateTimeFormatter format1 = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDateTime now1 = LocalDateTime.now();
		driver.findElement(By.id("contract_startdate")).sendKeys(now1.format(format1));

		// driver.findElement(By.id("contract_startdate_timezone")).sendKeys("IST");

		// driver.findElement(By.id("contract_period")).sendKeys("1");

		// Select cSelect4 = new
		// Select(driver.findElement(By.id("contract_period_unit")));
		// cSelect4.selectByValue("Days");

		driver.findElement(By.id("btn_accounts_gc_contracts_1_name")).click();
		String winHandleBeforen = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}
		// Added by deep - start /
		driver.findElement(By.id("search_form_clear"));
		driver.findElement(By.id("common_customer_id_c_advanced")).sendKeys("C0321124868");
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("html/body/table[4]/tbody/tr[3]/td[3]/a")).click();
		// Added by deep - End /
		// driver.findElement(By.xpath("html/body/table[4]/tbody/tr[5]/td[3]/a")).click();
		driver.switchTo().window(winHandleBeforen);

		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("btn_contacts_gc_contracts_1_name")).click();
		String winHandleBefore2 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("last_name_advanced")).clear();
		driver.findElement(By.id("last_name_advanced")).sendKeys(
				"Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore2);

		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		/* Added by deep for Selection Contract TimeZone - Start */
		driver.findElement(By.id("gc_timezone_id_c")).sendKeys(ContractStartDateTimeZone);
		driver.findElement(By.id("gc_timezone_id1_c")).sendKeys(ContractEndDateTimeZone);
		/* Added by deep for Selection Contract TimeZone - End */

		driver.findElement(By.id("factory_reference_no")).sendKeys(FactoryReferenceNumber);
		driver.findElement(By.id("sales_channel_code")).sendKeys(SalesChannelCode);
		/*
		 * driver.findElement(By.id("btn_corporate_name")).click();
		 * 
		 * String winHandleBefore3 = driver.getWindowHandle(); for (String
		 * handle : driver.getWindowHandles()) {
		 * 
		 * driver.switchTo().window(handle); }
		 * 
		 * 
		 * 
		 * 
		 * driver.findElement(By.xpath("//tbody/tr[3]/td[2]/a")).click();
		 * driver.switchTo().window(winHandleBefore3);
		 */

		driver.findElement(By.id("sales_rep_name")).sendKeys(SalesRepNameLatin);
		driver.findElement(By.id("sales_rep_name_1")).sendKeys(SalesRepNameLocal);
		driver.findElement(By.id("division_1")).sendKeys(DivisionLatin);
		driver.findElement(By.id("division_2")).sendKeys(DivisionLocal);
		driver.findElement(By.id("title_1")).sendKeys(TittleLatin);
		driver.findElement(By.id("title_2")).sendKeys(TittleLocal);
		driver.findElement(By.id("tel_no")).sendKeys(Telephone);
		driver.findElement(By.id("ext_no")).sendKeys(ExtnNumber);
		driver.findElement(By.id("fax_no")).sendKeys(FaxNumber);
		driver.findElement(By.id("mob_no")).sendKeys(MobileNumber);
		driver.findElement(By.id("email1")).sendKeys(mailId);

		Select cSelect5 = new Select(driver.findElement(By.id("product_specification_1")));
		// cSelect5.selectByValue("333");
		cSelect5.selectByVisibleText(ProductSpecification);

		/*
		 * driver.findElement(By.id("btn_product_name_1")).click(); String
		 * winHandleBefore4 = driver.getWindowHandle();
		 * 
		 * for(String handle : driver.getWindowHandles()){
		 * driver.switchTo().window(handle); }
		 * 
		 * 
		 * driver.findElement(By.id("name_advanced")).clear();
		 * driver.findElement(By.id("name_advanced")).sendKeys(ProductItem);
		 * driver.findElement(By.id("search_form_submit")).click();
		 * driver.findElement
		 * (By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		 * driver.switchTo().window(winHandleBefore4);
		 */

		Select cSelect6 = new Select(driver.findElement(By.id("cust_contract_currency_1")));
		cSelect6.selectByValue("JPY");

		Select cSelect7 = new Select(driver.findElement(By.id("cust_billing_currency_1")));
		cSelect7.selectByValue("AFN");

		Select cSelect8 = new Select(driver.findElement(By.id("igc_contract_currency_1")));
		cSelect8.selectByValue("AFN");

		Select cSelect9 = new Select(driver.findElement(By.id("igc_billing_currency_1")));
		cSelect9.selectByValue("AFN");

		Select cSelect10 = new Select(driver.findElement(By.id("customer_billing_method_1")));
		cSelect10.selectByValue("eCSS_Billing");

		Select cSelect11 = new Select(driver.findElement(By.id("igc_settlement_method_1")));
		// cSelect11.selectByValue("Quarterly_Settlement");
		cSelect11.selectByVisibleText("No Settlement per Contract");

		driver.findElement(By.id("description_1")).sendKeys("Testing");

		driver.findElement(By.id("btn_tech_account_name_1")).click();

		String winHandleBefore5 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("last_name_advanced")).clear();
		driver.findElement(By.id("last_name_advanced")).sendKeys(
				"Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("search_form_submit")).click();

		driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore5);

		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("btn_bill_account_name_1")).click();
		String winHandleBefore6 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("last_name_advanced")).clear();
		// driver.findElement(By.id("last_name_advanced")).sendKeys("test
		// 3/2/2016");
		driver.findElement(By.id("last_name_advanced")).sendKeys(
				"Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("search_form_submit")).click();

		driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore6);

		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime then = now.plusDays(10);
		driver.findElement(By.id("service_start_date_1")).sendKeys(now.format(format));
		driver.findElement(By.id("service_end_date_1")).sendKeys(then.format(format));
		driver.findElement(By.id("billing_start_date_1")).sendKeys(now.format(format));
		driver.findElement(By.id("billing_end_date_1")).sendKeys(then.format(format));

		/* Commented and added code for filling Timezone by deep */
		/*
		 * driver.findElement(By.id("service_start_date_timezone_1")).sendKeys(
		 * "IST"); driver.findElement(By.id("service_end_date_timezone_1"))
		 * .sendKeys("IST");
		 * driver.findElement(By.id("billing_start_date_timezone_1")).sendKeys(
		 * "IST"); driver.findElement(By.id("billing_end_date_timezone_1"))
		 * .sendKeys("IST");
		 */

		driver.findElement(By.id("service_start_date_timezone_1")).sendKeys(ServiceStartDateTimeZone);
		driver.findElement(By.id("service_end_date_timezone_1")).sendKeys(ServiceEndDateTimeZone);
		driver.findElement(By.id("billing_start_date_timezone_1")).sendKeys(BillingStartDateTimeZone);
		driver.findElement(By.id("billing_end_date_timezone_1")).sendKeys(BillingEndDateTimeZone);

		/*
		 * driver.findElement(By.id("btn_tech_account_name_1")).click();
		 * 
		 * String winHandleBefore5 = driver.getWindowHandle(); for (String
		 * handle : driver.getWindowHandles()) {
		 * driver.switchTo().window(handle); }
		 * driver.findElement(By.id("last_name_advanced")).clear();
		 * driver.findElement(By.id("last_name_advanced")) .sendKeys(
		 * "Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789"
		 * ); driver.findElement(By.id("search_form_submit")).click();
		 * 
		 * driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click(
		 * ); driver.switchTo().window(winHandleBefore5);
		 * 
		 * driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		 * driver.findElement(By.id("btn_bill_account_name_1")).click(); String
		 * winHandleBefore6 = driver.getWindowHandle(); for (String handle :
		 * driver.getWindowHandles()) { driver.switchTo().window(handle); }
		 * driver.findElement(By.id("last_name_advanced")).clear(); //
		 * driver.findElement(By.id("last_name_advanced")).
		 * sendKeys("test 3/2/2016");
		 * driver.findElement(By.id("last_name_advanced")) .sendKeys(
		 * "Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789"
		 * ); driver.findElement(By.id("search_form_submit")).click();
		 * 
		 * driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click(
		 * ); driver.switchTo().window(winHandleBefore6);
		 */

		if (PaymentType.equalsIgnoreCase("General: Direct Deposit")) {
			// driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));

			oSelect.selectByValue("00");

			driver.findElement(By.id("dd_bank_code_1")).sendKeys("1234");
			driver.findElement(By.id("dd_branch_code_1")).sendKeys("12");

			Select oSelect1 = new Select(driver.findElement(By.id("dd_deposit_type_1")));
			oSelect1.selectByVisibleText("Tax Payment (General)");

			driver.findElement(By.id("dd_account_no_1")).sendKeys("1234567");
			driver.findElement(By.id("dd_person_name_1_1")).sendKeys("ContactPerson Latin");
			driver.findElement(By.id("dd_person_name_2_1")).sendKeys("ContactPerson Local");
			driver.findElement(By.id("dd_payee_tel_no_1")).sendKeys("1234567890");
			driver.findElement(By.id("dd_payee_email_address_1")).sendKeys("dummy@email.com");
			driver.findElement(By.id("dd_remarks_1")).sendKeys("Remarks");
		} else if (PaymentType.equalsIgnoreCase("Account Transfer")) {
			// driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			try {
				Select oSelect = new Select(driver.findElement(By

						.id("payment_type_1")));

				oSelect.selectByValue("10");

				driver.findElement(By.id("at_account_no_1")).sendKeys("12345678963636363");
				driver.findElement(By.id("at_name_1")).sendKeys("Atul Dandin");
				driver.findElement(By.id("at_bank_code_1")).sendKeys("1234567890");
				driver.findElement(By.id("at_branch_code_1")).sendKeys("1234567890");

				Select oSelect1 = new Select(driver.findElement(By.id("at_deposit_type_1")));
				oSelect1.selectByVisibleText("Ordinary Deposit");

				Select oSelect2 = new Select(driver.findElement(By.id("at_account_no_display_1")));
				oSelect2.selectByVisibleText("Yes");

				Reporter.log("RTC_Contract_016 Passed- User was able to select account type Account transfer");
			} catch (Exception e) {
				Assert.fail(
						"RTC_Contract_016 failed an error occured while selecting the payment type as account transfer"
								+ e);
			}
		} else if (PaymentType.equalsIgnoreCase("Postal Transfer")) {
			// driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));

			oSelect.selectByValue("30");

			// driver.findElement(By.id("pt_postal_passbook_1")).click();
			// driver.findElement(By.id("pt_postal_passbook_1")).clear();
			driver.findElement(By.id("pt_postal_passbook_1")).sendKeys("123");
			driver.findElement(By.id("pt_name_1")).sendKeys("Testing account ");
			driver.findElement(By.id("pt_postal_passbook_mark_1")).sendKeys("Mark");

			Select oSelect2 = new Select(driver.findElement(By.id("pt_postal_passbook_no_display_1")));
			oSelect2.selectByVisibleText("Yes");

		} else if (PaymentType.equalsIgnoreCase("Credit Card")) {
			// driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Select oSelect = new Select(driver.findElement(By.id("payment_type_1")));

			oSelect.selectByValue("60");
			driver.findElement(By.id("cc_credit_card_no_1")).sendKeys("12345678963636363");
			driver.findElement(By.id("cc_expiration_date_1")).sendKeys("10/13");

		} else {
			Reporter.log("Invalid Paymemt Type");
			Assert.fail();
		}

		/*
		 * deep added for filling data based on ProductOffering and
		 * ProductSpecification
		 */
		if (ProductOffering.equals("Global Management One ECL2.0 Option")) {
			if (ProductSpecification.equals("GMOne ECL2.0 Option Base Specification (Base)")) {
				Select baseProductConfigSelect = new Select(driver.findElement(By.id("product_config_1_565")));
				baseProductConfigSelect.selectByVisibleText("1");
				driver.findElement(By.id("pi_role_value_1_876")).sendKeys("Product Information2");
			} else if (ProductSpecification.equals("Managed Windows Server - on ECL2.0 (Add-on Optional)")) {
				Select optionalProductConfigSelect1 = new Select(driver.findElement(By.id("product_config_1_563")));
				optionalProductConfigSelect1.selectByVisibleText("UNIT");
			} else if (ProductSpecification.equals("Managed Linux - on ECL2.0 (Add-on Optional)")) {
				Select optionalProductConfigSelect2 = new Select(driver.findElement(By.id("product_config_1_563")));
				optionalProductConfigSelect2.selectByVisibleText("UNIT");
			} else if (ProductSpecification.equals("Managed Oracle Basic - on ECL2.0 (Add-on Optional)")) {
				Select optionalProductConfigSelect3 = new Select(driver.findElement(By.id("product_config_1_613")));
				optionalProductConfigSelect3.selectByVisibleText("UNIT");
			} else if (ProductSpecification.equals("Managed Oracle Advanced - on ECL2.0 (Add-on Optional)")) {
				Select optionalProductConfigSelect4 = new Select(driver.findElement(By.id("product_config_1_614")));
				optionalProductConfigSelect4.selectByVisibleText("UNIT");
			}
		} else if (ProductOffering.equals("Global Management One UNO Option")) {

		} else if (ProductOffering.equals("Enterprise Mail")) {

		}

		/*
		 * Select cSelect13 = new
		 * Select(driver.findElement(By.id("product_config_1_561")));
		 * cSelect13.selectByValue("14014");
		 * 
		 * Select cSelect14 = new
		 * Select(driver.findElement(By.id("product_config_1_562")));
		 * cSelect14.selectByValue("14015");
		 */

		/*
		 * Select cSelect15 = new
		 * Select(driver.findElement(By.id("product_config_1_565")));
		 * cSelect15.selectByValue("1");
		 */

		/*
		 * Select cSelect16 = new
		 * Select(driver.findElement(By.id("product_config_1_567")));
		 * cSelect16.selectByValue("14035");
		 * 
		 * Select cSelect17 = new
		 * Select(driver.findElement(By.id("product_config_1_565")));
		 * cSelect17.selectByValue("1");
		 */

		// product_config_1_565

		/*
		 * driver.findElement(By.id("pi_role_value_1_861")).sendKeys(
		 * "Product Information1");
		 * driver.findElement(By.id("pi_role_value_1_862"
		 * )).sendKeys("Product Information2");
		 */
		// driver.findElement(By.id("pi_role_value_1_876")).sendKeys("Product
		// Information2");

		// driver.findElement(By.id("SAVE_FOOTER")).click();
		// srn.getscreenshot();

	}

	@AfterTest
	public void close() {
		driver.close();
	}

}
