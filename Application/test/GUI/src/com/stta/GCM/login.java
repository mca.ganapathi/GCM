package com.stta.GCM;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.stta.TestSuiteBase.SuiteBase;

public class login extends SuiteBase {

	@BeforeTest
	public void checkCaseToRun() throws IOException {
		init();

	}

	@Test
	public void Run() throws Exception {
		loadWebBrowser();
		Screen scrn = new Screen();
		System.out.print("Inside Run");
		driver.get(Param.getProperty("siteURL"));
		System.out.println("After Site URL");
		/* Added by Pradheep for brower wait Start */
		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
		/* Added by Pradheep for brower wait End */

		// driver.findElement(By.xpath(Object.getProperty("userName"))).clear();
		// driver.findElement(By.xpath(Object.getProperty("userName"))).sendKeys("user08@emeriocorp.com");
		// driver.findElement(By.xpath(Object.getProperty("userName"))).sendKeys("vijay.paudel@emeriocorp.com");
		// driver.findElement(By.xpath(Object.getProperty("Password"))).clear();
		// driver.findElement(By.xpath(Object.getProperty("Password"))).sendKeys("gcMtest2015");
		// driver.findElement(By.xpath(Object.getProperty("login"))).click();

		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[1]/span/input")).clear();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[1]/span/input"))
				.sendKeys(Param.getProperty("userName"));
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[2]/span/input")).clear();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[2]/span/input"))
				.sendKeys(Param.getProperty("password"));
		// scrn.getscreenshot();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[3]/span/a")).click();
		String winHandleBefore = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}
		/* Comment the below two lines if you are using IE */
		driver.findElement(By.xpath("html/body/div[5]/div[3]/div/a[2]/span")).click();
		driver.switchTo().window(winHandleBefore);
		// scrn.getscreenshot();
		driver.switchTo().defaultContent();
		//to be uncommented
		driver.findElement(By.xpath(".//*[@id='alerts']/div/div/button")).click();

	}
}
