package com.stta.GCM;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;

public class CidasCorporate extends SuiteGcmBase {

	CreateCorporateNewData ccnd = new CreateCorporateNewData();

	public void cidasSearch() {

		// WebElement element = driver.findElement(By.linkText("Corporates"));
		// Actions action = new Actions(driver);
		// action.moveToElement(element).build().perform();
		// driver.findElement(By.id("CreateCorporateAll")).click();
		// driver.findElement(By.xpath(".//*[@id='EditView']/table/tbody/tr/td[1]/div/input[3]")).click();
		//
		// String winHandleBefore = driver.getWindowHandle();
		// for (String handle : driver.getWindowHandles()) {
		//
		// driver.switchTo().window(handle);
		// }
		//
		// driver.findElement(By.id("commonCustomerId")).sendKeys("commonCustomerId");
		// driver.findElement(By.id("search_form_submit")).click();
		// driver.findElement(By.xpath("//tbody/tr[2]/td[1]/a")).click();
		// driver.switchTo().window(winHandleBefore);
		//

		try {

			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			String name = driver.findElement(By.id("common_customer_id_c")).getAttribute("value");
			Reporter.log("Common Customer ID:" + name);
			name = driver.findElement(By.id("name")).getAttribute("value");
			Reporter.log("Corporate Name (Latin Character):" + name);
			name = driver.findElement(By.id("name_2_c")).getAttribute("value");
			Reporter.log("Corporate Name (Local Character 1):" + name);
			name = driver.findElement(By.id("name_3_c")).getAttribute("value");
			Reporter.log("Corporate Name (Local Character 2):" + name);
			name = driver.findElement(By.id("hq_addr_1_c")).getAttribute("value");
			Reporter.log("Headquarter Address (Latin Character):" + name);
			name = driver.findElement(By.id("hq_addr_2_c")).getAttribute("value");
			Reporter.log("Headquarter Address (Local Character):" + name);
			name = driver.findElement(By.id("hq_postal_no_c")).getAttribute("value");
			Reporter.log("Headquarter Postal Number:" + name);
			name = driver.findElement(By.id("reg_addr_1_c")).getAttribute("value");
			Reporter.log("Registered Address (Latin Character):" + name);
			name = driver.findElement(By.id("reg_addr_2_c")).getAttribute("value");
			Reporter.log("Registered Address (Local Character):" + name);
			name = driver.findElement(By.id("reg_postal_no_c")).getAttribute("value");
			;

			Reporter.log("Registered Postal Number: " + name);
			name = driver.findElement(By.id("country_name_c")).getAttribute("value");
			Reporter.log("Country Name: " + name);
			name = driver.findElement(By.id("country_code_c")).getAttribute("value");
			Reporter.log("Country Name Code: " + name);

		} catch (Exception e) {
			Reporter.log("The value is not filled " + e);
			Assert.fail("An error occured while seraching the corporate account by CIDAS" + e);
		}
	}

	public void validataData() {
		try {
			driver.findElement(By.id("name")).sendKeys("update");

		} catch (Exception e) {
			Reporter.log("The value is not editable" + e);
		}

		String name = driver.findElement(By.id("name")).getAttribute("value");
		if (name.equalsIgnoreCase("update")) {
			Reporter.log("The CIDAS value is editable RTC_009 is failed");
			Assert.fail();
		} else {
			Reporter.log("The CIDAS value is editable not RTC_009 is passed ");
		}

	}

	public void cidasNewdata() throws InterruptedException {

		driver.switchTo().defaultContent();
		ccnd.CreateCorpNavigation();
		Thread.sleep(10000);
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		Thread.sleep(10000);
		driver.findElement(By.xpath("//*[@id='EditView']/table/tbody/tr/td[1]/div/input[3]")).click();

		String winHandleBefore4 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}

		driver.findElement(By.id("commonCustomerId")).sendKeys("C0000111113");
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//table[4]/tbody/tr[2]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore4);
		cidasSearch();
		validataData();
		driver.findElement(By.id("save")).click();

	}

}
