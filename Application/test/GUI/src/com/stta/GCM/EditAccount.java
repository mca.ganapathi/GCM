package com.stta.GCM;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class EditAccount extends SuiteGcmBase {

	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[] = null;
	static boolean TestCasePass = true;
	static int DataSet = -1;
	static boolean Testskip = false;
	static boolean Testfail = false;
	Screen scrn = new Screen();

	login log = new login();

	@BeforeTest
	public void checkCaseToRun() throws Exception {
		// Called init() function from SuiteBase class to Initialize .xls Files
		init();
		log.Run();
		// To set SuiteOne.xls file's path In FilePath Variable.
		FilePath = TestCaseListExcelOne;
		TestCaseName = this.getClass().getSimpleName();
		// SheetName to check CaseToRun flag against test case.
		SheetName = "TestCasesList";
		// Name of column In TestCasesList Excel sheet.
		ToRunColumnNameTestCase = "CaseToRun";
		// Name of column In Test Case Data sheets.
		ToRunColumnNameTestData = "DataToRun";
		// Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(TestCaseName + " : Execution started.");

		// To check test case's CaseToRun = Y or N In related excel sheet.
		// If CaseToRun = N or blank, Test case will skip execution. Else It
		// will be executed.
		if (!SuiteUtility.checkToRunUtility(FilePath, SheetName, ToRunColumnNameTestCase, TestCaseName)) {
			Add_Log.info(TestCaseName + " : CaseToRun = N for So Skipping Execution.");
			// To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
			// To throw skip exception for this test case.
			throw new SkipException(
					TestCaseName + "'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of " + TestCaseName);
		}
		// To retrieve DataToRun flags of all data set lines from related test
		// data sheet.
		TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);

		System.out.println(TestDataToRun);
	}

	@Test(dataProvider = "EditAccountData")
	public void EditAccountTest(String Test_CaseId, String Searchkey, String ContactType, String CompanyName,
			String ContactPersonNameLatin, String ContactPersonNameLocal, String DivisionLatin, String DivisionLocal,
			String TitleLatin, String TitleLocal, String Telephone, String ExtnNumber, String FaxNumber, String Mobile,
			String eMailAddress, String Country, String PostalNumber, String AddressUS, String USCity,
			String japanAddress1, String japanAddress2, String AddressLatin, String AddressLocal) throws Exception {

		DataSet++;

		// If found DataToRun = "N" for data set then execution will be skipped
		// for that data set.
		if (!TestDataToRun[DataSet].equalsIgnoreCase("Y")) {
			Add_Log.info(TestCaseName + " : DataToRun = N for data set line " + (DataSet + 1)
					+ " So skipping Its execution.");
			// If DataToRun = "N", Set Testskip=true.
			Testskip = true;
			throw new SkipException(
					"DataToRun for row number " + DataSet + " Is No Or Blank. So Skipping Its Execution.");
		}

		CreateAccount Caccount = new CreateAccount();
		EditAccount Eaccount = new EditAccount();

		if (Test_CaseId.equalsIgnoreCase("TC_NJ-62_005")) {
			try {

				// Caccount.navigationViewAccount(Searchkey);

				Eaccount.NavigationViewEditNoPop(Searchkey);
				/* Added for automation start */
				Thread.sleep(6000);
				driver.switchTo().defaultContent(); // outside the frame
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame"))); // switching
																					// to
																					// required
																					// frame
				driver.findElement(By.id("edit_button")).click();
				Thread.sleep(6000);
				/* Added for automation end */

				Caccount.clearData();
				driver.findElement(By.id("SAVE_HEADER")).click();
				String error = driver.findElement(By.xpath("//table/tbody/tr[2]/td[2]/div")).getText();
				if (error.isEmpty()) {
					Reporter.log("Mandetory check failed for Contact Person Name");
					Assert.fail();
				} else {
					Reporter.log("Mandetory check Passed for Contact Person Name" + error);
				}

				error = driver.findElement(By.xpath(".//*[@id='LBL_CONTACT_INFORMATION']/tbody/tr[2]/td[2]/div"))
						.getText();
				if (error.isEmpty()) {
					Reporter.log("Mandetory check failed for Company Name");
					Assert.fail();
				} else {
					Reporter.log("Mandetory check Passed for Company Name" + error);

				}
				// scrn.getscreenshot();

				/*
				 * Commented by Pradheep, as per the new requirement count is
				 * not mandetory
				 */
				/*
				 * error = driver.findElement(By.xpath(
				 * "//div[3]/table/tbody/tr/td[2]/div")).getText();
				 * if(error.isEmpty()){
				 * Reporter.log("Mandetory check failed for Country");
				 * Assert.fail(); }else{
				 * Reporter.log("Mandetory check Passed for Country"+error);
				 * 
				 * }
				 */

			} catch (Exception e) {
				Assert.fail("An error occured while executing TC_NJ-62_005 " + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-62_006")) {
			try {
				Eaccount.NavigationViewEdit(Searchkey);
				// driver.findElement(By.id("edit_button")).click();

				/* Added for automation start */
				Thread.sleep(6000);
				driver.switchTo().defaultContent(); // outside the frame
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame"))); // switching
																					// to
																					// required
																					// frame
				driver.findElement(By.id("edit_button")).click();
				Thread.sleep(6000);
				/* Added for automation end */

				Caccount.clearData();

				Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
				dropdown.selectByVisibleText("Japan");
				driver.findElement(By.id("Contacts0emailAddress0")).click();
				driver.findElement(By.id("SAVE_FOOTER")).click();
				/*
				 * Added for automation to check Japan specific fields visible
				 * or not start
				 */
				boolean japanSpecific1 = driver.findElement(By.id("ndb_addr1")).isDisplayed();
				boolean japanSpecific2 = driver.findElement(By.id("ndb_addr2")).isDisplayed();
				if (japanSpecific1 && japanSpecific2) {
					Reporter.log("Specific fields check for Japan country passed");
				} else {
					Reporter.log("Specific fields check for Japan country failed");
				}
				// scrn.getscreenshot();
				/*
				 * Added for automation to check Japan specific fields visible
				 * or not end
				 */

				/*
				 * Commented by Pradheep, as per the new requirement country is
				 * not mandatory
				 */
				/*
				 * String error =
				 * driver.findElement(By.xpath("//table/tbody/tr[1]/td[2]/div"))
				 * .getText(); if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address1  for Japan country"
				 * ); }else{ Reporter.
				 * log("Mandetory check  for Address1 for Passed for Japan country"
				 * ); }
				 * 
				 * error =
				 * driver.findElement(By.xpath("//table/tbody/tr[2]/td[2]/div"))
				 * .getText(); if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address2  for Japan country"
				 * ); }else{ Reporter.
				 * log("Mandetory check  for Address2  Passed for Japan country"
				 * ); }
				 */

			} catch (Exception e) {
				Assert.fail("An error occured while executing TC_NJ-62_006 " + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-62_007a")) {
			try {
				Eaccount.NavigationViewEdit(Searchkey);
				// driver.findElement(By.id("edit_button")).click();

				/* Added for automation start */
				Thread.sleep(6000);
				driver.switchTo().defaultContent(); // outside the frame
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame"))); // switching
																					// to
																					// required
																					// frame
				driver.findElement(By.id("edit_button")).click();
				Thread.sleep(6000);
				/* Added for automation end */

				Caccount.clearData();

				Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
				dropdown.selectByVisibleText("United States");
				// driver.findElement(By.id("Contacts0emailAddress0")).click();
				// driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(2000);
				/*
				 * Added for automation to check US specific fields visible or
				 * not start
				 */
				boolean usSpecificAddress = driver.findElement(By.id("addr_general_c")).isDisplayed();
				boolean usSpecificCity = driver.findElement(By.id("city_general_c")).isDisplayed();
				boolean usSpecificState = driver.findElement(By.id("c_state_id_c")).isDisplayed();
				boolean usSpecificAttentionLine = driver.findElement(By.id("attention_line_c")).isDisplayed();
				driver.findElement(By.id("SAVE_FOOTER")).click();
				if (usSpecificAddress && usSpecificCity && usSpecificState && usSpecificAttentionLine) {
					Reporter.log("Specific fields check for US country passed");
				} else {
					Reporter.log("Specific fields check for US country failed");
				}
				// scrn.getscreenshot();
				/*
				 * Added for automation to check US specific fields visible or
				 * not end
				 */

				/*
				 * Commented by Pradheep, as per the new requirement country is
				 * not mandatory
				 */
				/*
				 * String error = driver.findElement(By.xpath(
				 * "//div/div[5]/table/tbody/tr[1]/td[2]/div")).getText();
				 * if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address1  for [US/Canada Specific] country"
				 * ); }else{ Reporter.
				 * log("Mandetory check  for Address [US/Canada Specific]  Passed for United states country"
				 * ); }
				 * 
				 * error = driver.findElement(By.xpath(
				 * "//div[5]/table/tbody/tr[2]/td[2]/div")).getText();
				 * if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address2  for United States City"
				 * ); }else{ Reporter.
				 * log("Mandetory check  for Address2 for Passed for United States City"
				 * ); } error = driver.findElement(By.xpath(
				 * "//div[5]/table/tbody/tr[2]/td[4]/div")).getText();
				 * if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address2  for United States State"
				 * ); }else{ Reporter.
				 * log("Mandetory check  for Address2 for Passed for United States State"
				 * ); }
				 */

			} catch (Exception e) {
				Assert.fail("An error occured while executing TC_NJ-62_007a " + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-62_007b")) {
			try {
				Eaccount.NavigationViewEdit(Searchkey);
				// driver.findElement(By.id("edit_button")).click();

				/* Added for automation start */
				Thread.sleep(6000);
				driver.switchTo().defaultContent(); // outside the frame
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame"))); // switching
																					// to
																					// required
																					// frame
				driver.findElement(By.id("edit_button")).click();
				Thread.sleep(6000);
				/* Added for automation end */

				Caccount.clearData();

				Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
				dropdown.selectByVisibleText("Canada");
				// driver.findElement(By.id("Contacts0emailAddress0")).click();
				Thread.sleep(2000);
				driver.findElement(By.id("SAVE_FOOTER")).click();

				/*
				 * Added for automation to check US specific fields visible or
				 * not start
				 */
				boolean canadaSpecificAddress = driver.findElement(By.id("addr_general_c")).isDisplayed();
				boolean canadaSpecificCity = driver.findElement(By.id("city_general_c")).isDisplayed();
				boolean canadaSpecificState = driver.findElement(By.id("c_state_id_c")).isDisplayed();
				boolean canadaSpecificAttentionLine = driver.findElement(By.id("attention_line_c")).isDisplayed();
				if (canadaSpecificAddress && canadaSpecificCity && canadaSpecificState && canadaSpecificAttentionLine) {
					Reporter.log("Specific fields check for Canada country passed");
				} else {
					Reporter.log("Specific fields check for Canada country failed");
				}
				// scrn.getscreenshot();
				/*
				 * Added for automation to check US specific fields visible or
				 * not end
				 */

				/*
				 * String error = driver.findElement(By.xpath(
				 * "//div/div[5]/table/tbody/tr[1]/td[2]/div")).getText();
				 * if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address1  for [US/Canada Specific] country"
				 * ); }else{ Reporter.
				 * log("Mandetory check  for Address [US/Canada Specific]  Passed for Canada country"
				 * ); }
				 * 
				 * error = driver.findElement(By.xpath(
				 * "//div[5]/table/tbody/tr[2]/td[2]/div")).getText();
				 * if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address2  for Canada City");
				 * }else{ Reporter.
				 * log("Mandetory check  for Address2 for Passed for Canada City"
				 * ); } error = driver.findElement(By.xpath(
				 * "//div[5]/table/tbody/tr[2]/td[4]/div")).getText();
				 * if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address2  for Canada State");
				 * }else{ Reporter.
				 * log("Mandetory check  for Address2 for Passed for Canada State"
				 * ); }
				 */

			} catch (Exception e) {
				Assert.fail("An error occured while executing TC_NJ-62_007b " + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-51_003a/RTC_004")) {
			try {
				Country.trim();
				if (Country.equalsIgnoreCase("Japan")) {

					// Eaccount.NavigationViewEditPop(Searchkey);
					Eaccount.NavigationViewEdit(Searchkey);
					// driver.findElement(By.id("edit_button")).click();

					/* Added for automation start */
					Thread.sleep(6000);
					driver.switchTo().defaultContent(); // outside the frame
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame"))); // switching
																						// to
																						// required
																						// frame
					driver.findElement(By.id("edit_button")).click();
					Thread.sleep(6000);
					/* Added for automation end */

					Caccount.clearData();
					// Select dropdown = new
					// Select(driver.findElement(By.id("c_country_id_c")));
					// dropdown.selectByVisibleText("Japan");
					Caccount.fillData(ContactType, CompanyName, ContactPersonNameLatin, ContactPersonNameLocal,
							DivisionLatin, DivisionLocal, TitleLatin, TitleLocal, Telephone, ExtnNumber, FaxNumber,
							Mobile, eMailAddress, Country, PostalNumber, AddressUS, USCity, japanAddress1,
							japanAddress2, AddressLatin, AddressLocal);
					Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
					dropdown.selectByVisibleText("Japan");
					driver.findElement(By.id("ndb_addr1")).sendKeys(japanAddress1);
					driver.findElement(By.id("ndb_addr2")).sendKeys(japanAddress2);
					driver.findElement(By.id("postal_no_c")).sendKeys(PostalNumber);
					// scrn.getscreenshot();
					driver.findElement(By.id("SAVE_FOOTER")).click();
					Caccount.countData(Country);
					Reporter.log("Updation was successul");
					// System.out.println("Japan Update success");
					// scrn.getscreenshot();
				} else {
					// System.out.println("Japan Update fail");
					Reporter.log("Its not a Japan country");
				}

			} catch (Exception e) {
				Assert.fail("An error occured while executing TC_NJ-51_003a/RTC_004 " + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-51_003b")) {
			try {
				Country.trim();
				if (Country.equalsIgnoreCase("United States")) {

					// Eaccount.NavigationViewEditNoPop(Searchkey);
					// driver.findElement(By.id("edit_button")).click();

					/* Added for automation start */
					// Eaccount.NavigationViewEdit(Searchkey);
					Eaccount.NavigationViewEditNoPop(Searchkey);
					Thread.sleep(6000);
					driver.switchTo().defaultContent(); // outside the frame
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame"))); // switching
																						// to
																						// required
																						// frame
					driver.findElement(By.id("edit_button")).click();
					Thread.sleep(6000);
					/* Added for automation end */

					Caccount.clearData();
					// Select dropdown = new
					// Select(driver.findElement(By.id("c_country_id_c")));
					// dropdown.selectByVisibleText("United States");
					Caccount.fillData(ContactType, CompanyName, ContactPersonNameLatin, ContactPersonNameLocal,
							DivisionLatin, DivisionLocal, TitleLatin, TitleLocal, Telephone, ExtnNumber, FaxNumber,
							Mobile, eMailAddress, Country, PostalNumber, AddressUS, USCity, japanAddress1,
							japanAddress2, AddressLatin, AddressLocal);
					Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
					dropdown.selectByVisibleText("United States");
					Thread.sleep(2000);
					driver.findElement(By.id("addr_general_c")).sendKeys(AddressUS);
					driver.findElement(By.id("city_general_c")).sendKeys(USCity);
					Select dropdown1 = new Select(driver.findElement(By.id("c_state_id_c")));
					dropdown1.selectByVisibleText("Iowa");
					driver.findElement(By.id("postal_no_c")).sendKeys(PostalNumber);
					// scrn.getscreenshot();
					driver.findElement(By.id("SAVE_FOOTER")).click();
					Caccount.countData(Country);
					Reporter.log("Updation was successul to united states ");
					// System.out.println("US update success");
					// scrn.getscreenshot();
				} else {
					// System.out.println("US update fail");
					Reporter.log("Its not a United States country");
				}

			} catch (Exception e) {
				Assert.fail("An error occured while executing TC_NJ-51_003b " + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-51_003c/RTC_005")) {
			try {

				Country.trim();
				if (Country.equalsIgnoreCase("Canada")) {

					// Eaccount.NavigationViewEditNoPop(Searchkey);
					// driver.findElement(By.id("edit_button")).click();

					/* Added for automation start */
					// Eaccount.NavigationViewEdit(Searchkey);
					Eaccount.NavigationViewEditNoPop(Searchkey);
					Thread.sleep(6000);
					driver.switchTo().defaultContent(); // outside the frame
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame"))); // switching
																						// to
																						// required
																						// frame
					driver.findElement(By.id("edit_button")).click();
					Thread.sleep(6000);
					/* Added for automation end */

					Caccount.clearData();
					// Select dropdown = new
					// Select(driver.findElement(By.id("c_country_id_c")));
					// dropdown.selectByVisibleText("Canada");
					Caccount.fillData(ContactType, CompanyName, ContactPersonNameLatin, ContactPersonNameLocal,
							DivisionLatin, DivisionLocal, TitleLatin, TitleLocal, Telephone, ExtnNumber, FaxNumber,
							Mobile, eMailAddress, Country, PostalNumber, AddressUS, USCity, japanAddress1,
							japanAddress2, AddressLatin, AddressLocal);
					Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
					dropdown.selectByVisibleText("Canada");
					Thread.sleep(2000);
					driver.findElement(By.id("addr_general_c")).sendKeys(AddressUS);
					driver.findElement(By.id("city_general_c")).sendKeys(USCity);
					Select dropdown1 = new Select(driver.findElement(By.id("c_state_id_c")));
					dropdown1.selectByVisibleText("Yukon");
					driver.findElement(By.id("postal_no_c")).sendKeys(PostalNumber);
					// scrn.getscreenshot();
					driver.findElement(By.id("SAVE_FOOTER")).click();
					Caccount.countData(Country);
					Reporter.log("Updation was successul");
					// System.out.println("Canada update success");
					// scrn.getscreenshot();
				} else {
					Reporter.log("Its not a Canada country");
					// System.out.println("Canada update fail");
				}

			} catch (Exception e) {
				Assert.fail("An error occured while executing TC_NJ-51_003c/RTC_005 " + e);
			}

		} else {

			Reporter.log("Test Case id did not match");
		}

	}

	@DataProvider
	public Object[][] EditAccountData() {
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}

	public void NavigationViewEdit(String Searchkey) throws Exception {

		/*
		 * WebElement element = driver.findElement(By.linkText("Accounts"));
		 * Actions action = new Actions(driver);
		 * action.moveToElement(element).build().perform();
		 * driver.findElement(By.id("ViewAccountsAll")).click();
		 */
		/* Added for automation viewAccount navigation start */
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/div[1]/ul/li[3]/span/button")).click();
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/div[1]/ul/li[3]/span/div/ul/li[2]/a"))
				.click();
		/* Added for automation viewAccount navigation start */
		// Alert alert = driver.switchTo().alert();
		// alert.accept();
		boolean confirmAlert = driver.findElement(By.className("alert-messages")).isDisplayed();
		if (confirmAlert) {
			driver.findElement(By.className("alert-btn-confirm")).click();
		}
		// driver.switchTo().defaultContent(); //outside the frame
		driver.switchTo().frame("bwc-frame");
		driver.findElement(By.id("last_name_basic")).clear();
		driver.findElement(By.id("last_name_basic")).sendKeys(Searchkey);
		// scrn.getscreenshot();
		driver.findElement(By.id("search_form_submit")).click();
		// scrn.getscreenshot();
		driver.findElement(By.xpath("//tbody/tr[3]/td[3]/b/a")).click();

	}

	public void NavigationViewEditPop(String Searchkey) throws Exception {

		/*
		 * WebElement elements = driver.findElement(By.linkText("Accounts"));
		 * Actions action = new Actions(driver);
		 * action.moveToElement(elements).build().perform();
		 * driver.findElement(By.id("ViewAccountsAll")).click();
		 */
		/* Added for automation viewAccount navigation start */
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/div[1]/ul/li[3]/span/button")).click();
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/div[1]/ul/li[3]/span/div/ul/li[2]/a"))
				.click();
		/* Added for automation viewAccount navigation start */
		// Alert alert = driver.switchTo().alert();
		// alert.accept();
		driver.switchTo().frame("bwc-frame");
		driver.findElement(By.id("last_name_basic")).clear();
		driver.findElement(By.id("last_name_basic")).sendKeys(Searchkey);
		// scrn.getscreenshot();
		driver.findElement(By.id("search_form_submit")).click();
		// scrn.getscreenshot();
		driver.findElement(By.xpath("//tbody/tr[3]/td[3]/b/a")).click();

	}

	public void NavigationViewEditNoPop(String Searchkey) throws Exception {

		/*
		 * WebElement element = driver.findElement(By.linkText("Accounts"));
		 * Actions action = new Actions(driver);
		 * action.moveToElement(element).build().perform();
		 * driver.findElement(By.id("ViewAccountsAll")).click();
		 */
		// Alert alert = driver.switchTo().alert();
		// alert.accept();
		driver.switchTo().defaultContent();

		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/div[1]/ul/li[3]/span/button")).click();
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/div[1]/ul/li[3]/span/div/ul/li[2]/a"))
				.click();
		driver.switchTo().frame("bwc-frame");
		driver.findElement(By.id("last_name_basic")).clear();
		driver.findElement(By.id("last_name_basic")).sendKeys(Searchkey);
		// scrn.getscreenshot();
		driver.findElement(By.id("search_form_submit")).click();
		// scrn.getscreenshot();
		driver.findElement(By.xpath("//tbody/tr[3]/td[3]/b/a")).click();

	}

	@AfterTest
	public void close() {
		System.out.println("Inside after close");
		driver.close();
	}

}
