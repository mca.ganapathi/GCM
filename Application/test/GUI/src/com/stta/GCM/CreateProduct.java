package com.stta.GCM;

import java.io.IOException;
import java.util.Properties;

import org.junit.AfterClass;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class CreateProduct extends SuiteGcmBase {
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[]=null;
	static boolean TestCasePass=true;
	static int DataSet=-1;	
	static boolean Testskip=false;
	static boolean Testfail=false;
	
	@BeforeTest
	public void checkCaseToRun() throws IOException{
		//Called init() function from SuiteBase class to Initialize .xls Files
		init();			
		//To set SuiteOne.xls file's path In FilePath Variable.
		FilePath = TestCaseListExcelOne;		
		TestCaseName = this.getClass().getSimpleName();	
		//SheetName to check CaseToRun flag against test case.
		SheetName = "TestCasesList";
		//Name of column In TestCasesList Excel sheet.
		ToRunColumnNameTestCase = "CaseToRun";
		//Name of column In Test Case Data sheets.
		ToRunColumnNameTestData = "DataToRun";
		//Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(TestCaseName+" : Execution started.");
		
		//To check test case's CaseToRun = Y or N In related excel sheet.
		//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
		if(!SuiteUtility.checkToRunUtility(FilePath, SheetName,ToRunColumnNameTestCase,TestCaseName)){
			Add_Log.info(TestCaseName+" : CaseToRun = N for So Skipping Execution.");
			//To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
			//To throw skip exception for this test case.
			throw new SkipException(TestCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+TestCaseName);
		}	
		//To retrieve DataToRun flags of all data set lines from related test data sheet.
		TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);
		
		
		System.out.println(TestDataToRun);
	}
	
	
	@Test(dataProvider="CreateProductData")
	public void CreateProductTest(String Test_CaseId, String ProductName, String Approve) throws InterruptedException{
		
        DataSet++;
        
        
		//If found DataToRun = "N" for data set then execution will be skipped for that data set.
		if(!TestDataToRun[DataSet].equalsIgnoreCase("Y")){	
			Add_Log.info(TestCaseName+" : DataToRun = N for data set line "+(DataSet+1)+" So skipping Its execution.");
			//If DataToRun = "N", Set Testskip=true.
			Testskip=true;
			throw new SkipException("DataToRun for row number "+DataSet+" Is No Or Blank. So Skipping Its Execution.");
		}
		
	  CreateProduct product = new CreateProduct();
	  
	  if(Test_CaseId.equalsIgnoreCase("TC_Product_005")){
		  product.CreateNavigation();
		  product.mandetory();
		  
		  
		  
	  }else if(Test_CaseId.equalsIgnoreCase("TC_Product_002")){
		  
		  product.CreateNavigation();
		  product.DataFill(ProductName);
		  product.NavigationView(ProductName);
		  product.CountData();
		  
		  
		  
	  }else if(Test_CaseId.equalsIgnoreCase("TC_Product_003")){
		  
		  product.NavigationView(ProductName);
		  
		  if(Approve.equalsIgnoreCase("yes")){
			  product.approve(); 
		  }else{
			
			  Reporter.log("Test Case marked as not approved");
				Assert.fail();
			  
		  }
		  
		  
	  }
		
		
		
		
	
	
	
	
	}
	
	@DataProvider
	public Object[][] CreateProductData(){
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}
	

	
	public void DataFill(String ProductName){
		driver.findElement(By.id("name")).sendKeys(ProductName);
		driver.findElement(By.id("btn_fm_factory_pm_products_name")).click();
		String winHandleBefore = driver.getWindowHandle();
	    for (String handle : driver.getWindowHandles()) {

	        driver.switchTo().window(handle);
	    }
		driver.findElement(By.xpath("//tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore);
		driver.findElement(By.id("product_introduction_date_trigger")).click();
		driver.findElement(By.xpath("//table/tbody/tr[5]/td[2]/a")).click();
		driver.findElement(By.id("product_sales_end_date_trigger")).click();
		driver.findElement(By.xpath("//table/tbody/tr[5]/td[2]/a")).click();
		driver.findElement(By.id("SAVE_FOOTER")).click();

	}
	
	public void ClearData(){
		
		
		driver.findElement(By.id("product_introduction_date")).clear();
		driver.findElement(By.id("product_sales_end_date")).clear();
		driver.findElement(By.id("fm_factory_pm_products_name")).clear();
		driver.findElement(By.id("name")).clear();
		
	
	}

	
	public void CreateNavigation(){
		WebElement element = driver.findElement(By.linkText("Products"));
		Actions action = new Actions(driver);
	    action.moveToElement(element).build().perform();
	    driver.findElement(By.id("CreateProductsAll")).click();
		
	}
	
	
	
	
	public void NavigationView(String ProductName){
		WebElement element = driver.findElement(By.linkText("Products"));
		Actions action = new Actions(driver);
	    action.moveToElement(element).build().perform();
	    driver.findElement(By.id("ViewProductsAll")).click();
	    driver.findElement(By.id("name_basic")).clear();
	    driver.findElement(By.id("name_basic")).sendKeys(ProductName);
	    driver.findElement(By.id("search_form_submit")).click();
	    driver.findElement(By.xpath("//table/tbody/tr[3]/td[3]/b/a")).click();
	    
	}

	public void CountData(){
		
		String count;
		count = driver.findElement(By.id("name")).getText();
		if(count.length()>100){
			Reporter.log("The productName  is more than 100="+count.length());
			Assert.fail();
		}else{
			
			Reporter.log("The productName  is less  than 100");
			
		}
		
	}
 
	public void approve() throws InterruptedException{
       Thread.sleep(5000);
		
		String status = driver.findElement(By.id("product_status")).getAttribute("value");
		
      //Reporter.log("**********************Status of the product************"+status);
      
      if(status.equalsIgnoreCase("draft")){
			driver.findElement(By.id("btn_approve_product")).click();
			 status = driver.findElement(By.id("product_status")).getAttribute("value");
			if(status.equalsIgnoreCase("Approved")){
				Reporter.log("The product is approved");
			}else{
				Reporter.log("The product is not  approved");
				Assert.fail();
				
			}
		}else{
			
			Reporter.log("The product is already Approved ");
			Assert.fail();
		}
		
	}
      
	public void mandetory(){
		driver.findElement(By.id("SAVE_FOOTER")).click();
		  String error;
		  
		  error = driver.findElement(By.xpath("//table/tbody/tr[1]/td[2]/div")).getText();
		  if(error.equalsIgnoreCase("")){
			  Reporter.log("Mandetory check failed for Prodcut Name");
			  Assert.fail();
			  
		  }else{
			  Reporter.log("Mandetory check passed for Prodcut Name and the error is "+error);
		  }
		  
		  error = driver.findElement(By.xpath("//div/table/tbody/tr[1]/td[4]/div[2]")).getText();
		  if(error.equalsIgnoreCase("")){
			  Reporter.log("Mandetory check failed for factory");
			  Assert.fail();
			  
		  }else{
			  Reporter.log("Mandetory check passed  for factory and the error is  "+error);
		  }
		  
		  error = driver.findElement(By.xpath("//div/table/tbody/tr[3]/td[2]/span/div")).getText();
		  if(error.equalsIgnoreCase("")){
			  Reporter.log("Mandetory check failed for Production introduction start date ");
			  Assert.fail();
			  
		  }else{
			  Reporter.log("Mandetory check passed  for Production introduction start date and the error is   ="+error);
		  }
		  
		  error = driver.findElement(By.xpath("//div/table/tbody/tr[3]/td[4]/span/div")).getText();
		  if(error.equalsIgnoreCase("")){
			  Reporter.log("Mandetory check failed for Production introduction End date ");
			  Assert.fail();
			  
		  }else{
			  Reporter.log("Mandetory check passed  for Production introduction End date and the error is   ="+error);
		  }
	}
	
	
}
