package com.stta.GCM;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class CreateAccount extends SuiteGcmBase {

	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[] = null;
	static boolean TestCasePass = true;
	static int DataSet = -1;
	static boolean Testskip = false;
	static boolean Testfail = false;
	Screen scrn = new Screen();

	login log = new login();

	@BeforeTest
	public void checkCaseToRun() throws Exception {
		// Called init() function from SuiteBase class to Initialize .xls Files
		init();
		log.Run();
		// To set SuiteOne.xls file's path In FilePath Variable.
		FilePath = TestCaseListExcelOne;
		TestCaseName = this.getClass().getSimpleName();
		// SheetName to check CaseToRun flag against test case.
		SheetName = "TestCasesList";
		// Name of column In TestCasesList Excel sheet.
		ToRunColumnNameTestCase = "CaseToRun";
		// Name of column In Test Case Data sheets.
		ToRunColumnNameTestData = "DataToRun";
		// Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(TestCaseName + " : Execution started.");

		// To check test case's CaseToRun = Y or N In related excel sheet.
		// If CaseToRun = N or blank, Test case will skip execution. Else It
		// will be executed.
		if (!SuiteUtility.checkToRunUtility(FilePath, SheetName, ToRunColumnNameTestCase, TestCaseName)) {
			Add_Log.info(TestCaseName + " : CaseToRun = N for So Skipping Execution.");
			// To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
			// To throw skip exception for this test case.
			throw new SkipException(
					TestCaseName + "'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of " + TestCaseName);
		}
		// To retrieve DataToRun flags of all data set lines from related test
		// data sheet.
		TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);

		System.out.println(TestDataToRun);
	}

	@Test(dataProvider = "CreateAccountData")
	public void CreateAccountTest(String Test_CaseId, String ContactType, String CompanyName,
			String ContactPersonNameLatin, String ContactPersonNameLocal, String DivisionLatin, String DivisionLocal,
			String TitleLatin, String TitleLocal, String Telephone, String ExtnNumber, String FaxNumber, String Mobile,
			String eMailAddress, String Country, String PostalNumber, String AddressUS, String USCity,
			String japanAddress1, String japanAddress2, String AddressLatin, String AddressLocal) throws Exception {

		DataSet++;

		// If found DataToRun = "N" for data set then execution will be skipped
		// for that data set.
		if (!TestDataToRun[DataSet].equalsIgnoreCase("Y")) {
			Add_Log.info(TestCaseName + " : DataToRun = N for data set line " + (DataSet + 1)
					+ " So skipping Its execution.");
			// If DataToRun = "N", Set Testskip=true.
			Testskip = true;
			throw new SkipException(
					"DataToRun for row number " + DataSet + " Is No Or Blank. So Skipping Its Execution.");
		}

		CreateAccount CA = new CreateAccount();

		if (Test_CaseId.equalsIgnoreCase("TC_NJ-62_005")) {
			try {
				System.out.println("Inside TC_NJ-62_005");
				Thread.sleep(5000);
				CA.navigationCreateAccounts();
				/* Added for automation start */
				Thread.sleep(15000);
				System.out.println("After Nvaigation ");
				driver.switchTo().defaultContent(); // outside the frame
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame"))); // selecting
																					// the
																					// frame
				/* Added for automation end */
				driver.findElement(By.id("SAVE_HEADER")).click();
				String error = driver.findElement(By.xpath("//table/tbody/tr[1]/td[2]/div")).getText();
				if (error.isEmpty()) {
					Reporter.log("Mandetory check failed for contact type");
					Assert.fail();
				} else {
					Reporter.log("Mandetory check Passed for contact type" + error);
					Add_Log.info("HEllo");

				}

				error = driver.findElement(By.xpath("//table/tbody/tr[2]/td[2]/div")).getText();
				if (error.isEmpty()) {
					Reporter.log("Mandetory check failed for Contact Person Name");
					Assert.fail();
				} else {
					Reporter.log("Mandetory check Passed for Contact Person Name" + error);

				}

				error = driver.findElement(By.xpath(".//*[@id='LBL_CONTACT_INFORMATION']/tbody/tr[1]/td[4]/div"))
						.getText();
				if (error.isEmpty()) {
					Reporter.log("Mandetory check failed for Company Name");
					Assert.fail();
				} else {
					Reporter.log("Mandetory check Passed for Company Name" + error);

				}
				// scrn.getscreenshot();

				/*
				 * Commented by Pradheep, as per the new requirement count is
				 * not mandetory
				 */
				/*
				 * error = driver.findElement(By.xpath(
				 * "//div[3]/table/tbody/tr/td[2]/div")).getText();
				 * if(error.isEmpty()){
				 * Reporter.log("Mandetory check failed for Country");
				 * Assert.fail(); }else{
				 * Reporter.log("Mandetory check Passed for Country"+error);
				 * 
				 * }
				 */
			} catch (Exception e) {
				Assert.fail("An error occured while executing TC_NJ-62_005 " + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-62_006")) {
			try {
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();

				// CA.navigationCreateAccounts(); //uncomment it if you need to
				// execute this test case only

				/* Added for automation start */
				Thread.sleep(30000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				/* Added for automation end */

				Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
				dropdown.selectByVisibleText("Japan");
				Thread.sleep(3000);
				driver.findElement(By.id("Contacts0emailAddress0")).click();
				/*
				 * Added for automation to check Japan specific fields visible
				 * or not start
				 */
				boolean japanSpecific1 = driver.findElement(By.id("ndb_addr1")).isDisplayed();
				boolean japanSpecific2 = driver.findElement(By.id("ndb_addr2")).isDisplayed();
				if (japanSpecific1 && japanSpecific2) {
					Reporter.log("Specific fields check for Japan country passed");
				} else {
					Reporter.log("Specific fields check for Japan country failed");
				}

			} catch (Exception e) {
				Assert.fail("An error occured while executing TC_NJ-62_005 " + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("RTC_023")) {
			try {
				driver.navigate().refresh();
				Alert alert = driver.switchTo().alert();
				alert.accept();

				// CA.navigationCreateAccounts(); //uncomment it if you need to
				// execute this test case only

				/* Added for automation start */
				Thread.sleep(30000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				/* Added for automation end */

				Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
				dropdown.selectByVisibleText("United States");
				Thread.sleep(3000);
				// driver.findElement(By.id("Contacts0emailAddress0")).click();

				/*
				 * Added for automation to check US specific fields visible or
				 * not start
				 */
				boolean usSpecificAddress = driver.findElement(By.id("addr_general_c")).isDisplayed();
				boolean usSpecificCity = driver.findElement(By.id("city_general_c")).isDisplayed();
				boolean usSpecificState = driver.findElement(By.id("c_state_id_c")).isDisplayed();
				boolean usSpecificAttentionLine = driver.findElement(By.id("attention_line_c")).isDisplayed();
				if (usSpecificAddress && usSpecificCity && usSpecificState && usSpecificAttentionLine) {
					Reporter.log("Specific fields check for US country passed");
				} else {
					Reporter.log("Specific fields check for US country failed");
				}
				// scrn.getscreenshot();
				/*
				 * Added for automation to check US specific fields visible or
				 * not end
				 */

				/*
				 * Commented by Pradheep, as per the new requirement country is
				 * not mandatory
				 */
				/*
				 * driver.findElement(By.id("SAVE_FOOTER")).click(); String
				 * error = driver.findElement(By.xpath(
				 * "//div/div[5]/table/tbody/tr[1]/td[2]/div")).getText();
				 * if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address1  for [US/Canada Specific] country"
				 * ); }else{ Reporter.
				 * log("Mandetory check  for Address [US/Canada Specific]  Passed for United states country"
				 * ); }
				 * 
				 * error = driver.findElement(By.xpath(
				 * "//div[5]/table/tbody/tr[2]/td[2]/div")).getText();
				 * if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address2  for United States City"
				 * ); }else{ Reporter.
				 * log("Mandetory check  for Address2 for Passed for United States City"
				 * ); } error = driver.findElement(By.xpath(
				 * "//div[5]/table/tbody/tr[2]/td[4]/div")).getText();
				 * if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address2  for United States State"
				 * ); }else{ Reporter.
				 * log("Mandetory check  for Address2 for Passed for United States State"
				 * ); }
				 */

			} catch (Exception e) {
				Assert.fail("An error occured while executing TC_NJ-62_006" + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("RTC_023a")) {
			try {
				// driver.navigate().refresh();
				// Alert alert = driver.switchTo().alert();
				// alert.accept();

				// Added for closing preventing dialog box
				/*
				 * Alert alert1 = driver.switchTo().alert(); alert1.accept();
				 * 
				 * Alert alert2 = driver.switchTo().alert(); alert2.accept();
				 */

				// CA.navigationCreateAccounts(); //uncomment it if you need to
				// execute this test case only

				/* Added for automation start */
				Thread.sleep(30000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				/* Added for automation end */

				Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
				dropdown.selectByVisibleText("Canada");
				Thread.sleep(3000);
				// driver.findElement(By.id("Contacts0emailAddress0")).click();

				/*
				 * Added for automation to check US specific fields visible or
				 * not start
				 */
				boolean canadaSpecificAddress = driver.findElement(By.id("addr_general_c")).isDisplayed();
				boolean canadaSpecificCity = driver.findElement(By.id("city_general_c")).isDisplayed();
				boolean canadaSpecificState = driver.findElement(By.id("c_state_id_c")).isDisplayed();
				boolean canadaSpecificAttentionLine = driver.findElement(By.id("attention_line_c")).isDisplayed();
				if (canadaSpecificAddress && canadaSpecificCity && canadaSpecificState && canadaSpecificAttentionLine) {
					Reporter.log("Specific fields check for Canada country passed");
				} else {
					Reporter.log("Specific fields check for Canada country failed");
				}
				// scrn.getscreenshot();

				// driver.navigate().refresh();
				// Alert alert = driver.switchTo().alert();
				// alert.accept();

				/*
				 * Added for automation to check US specific fields visible or
				 * not end
				 */

				/*
				 * Commented by Pradheep, as per the new requirement country is
				 * not mandatory
				 */
				/*
				 * driver.findElement(By.id("SAVE_FOOTER")).click(); String
				 * error = driver.findElement(By.xpath(
				 * "//div/div[5]/table/tbody/tr[1]/td[2]/div")).getText();
				 * if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address1  for [US/Canada Specific] country"
				 * ); }else{ Reporter.
				 * log("Mandetory check  for Address [US/Canada Specific]  Passed for Canada country"
				 * ); }
				 * 
				 * error = driver.findElement(By.xpath(
				 * "//div[5]/table/tbody/tr[2]/td[2]/div")).getText();
				 * if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address2  for Canada City");
				 * }else{ Reporter.
				 * log("Mandetory check  for Address2 for Passed for Canada City"
				 * ); } error = driver.findElement(By.xpath(
				 * "//div[5]/table/tbody/tr[2]/td[4]/div")).getText();
				 * if(error.isEmpty()){ Reporter.
				 * log("Mandetory check failed for Address2  for Canada State");
				 * }else{ Reporter.
				 * log("Mandetory check  for Address2 for Passed for Canada State"
				 * ); }
				 */

				/*
				 * else if(Test_CaseId.equalsIgnoreCase("TC_NJ-51_003")){
				 * if(Country.equalsIgnoreCase("Japan") &
				 * (Country.equalsIgnoreCase("United States")) &
				 * (Country.equalsIgnoreCase("Canada"))) {
				 * 
				 * Reporter.log("*******Validation Failed******");
				 * 
				 * 
				 * }else {
				 * 
				 * Select dropdown = new
				 * Select(driver.findElement(By.id("c_country_id_c")));
				 * dropdown.selectByVisibleText("India");
				 * CA.fillData(ContactType, CompanyName, ContactPersonNameLatin,
				 * ContactPersonNameLocal, DivisionLatin, DivisionLocal,
				 * TitleLatin, TitleLocal, Telephone, ExtnNumber,
				 * FaxNumber,Mobile,eMailAddress, Country, PostalNumber,
				 * AddressUS, USCity,japanAddress1, japanAddress2, AddressLatin,
				 * AddressLocal);
				 * driver.findElement(By.id("addr_1_c")).sendKeys(AddressLatin);
				 * driver.findElement(By.id("addr_2_c")).sendKeys(AddressLocal);
				 * driver.findElement(By.id("SAVE_FOOTER")).click();
				 * CA.countData(Country);
				 * 
				 * }
				 * 
				 * 
				 * 
				 * 
				 * }
				 */

			} catch (Exception e) {
				Assert.fail("An error occured while executing RTC_023a" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_003/RTC_025")) {

			try {
				Country.trim();

				if (Country.equalsIgnoreCase("Japan")) {

					CA.navigationCreateAccounts();
					Thread.sleep(10000);
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					Thread.sleep(10000);
					CA.fillData(ContactType, CompanyName, ContactPersonNameLatin, ContactPersonNameLocal, DivisionLatin,
							DivisionLocal, TitleLatin, TitleLocal, Telephone, ExtnNumber, FaxNumber, Mobile,
							eMailAddress, Country, PostalNumber, AddressUS, USCity, japanAddress1, japanAddress2,
							AddressLatin, AddressLocal);
					Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
					dropdown.selectByVisibleText("Japan");
					Thread.sleep(3000);
					driver.findElement(By.id("ndb_addr1")).sendKeys(japanAddress1);
					driver.findElement(By.id("ndb_addr2")).sendKeys(japanAddress2);
					driver.findElement(By.id("postal_no_c")).sendKeys(PostalNumber);
					// scrn.getscreenshot();
					driver.findElement(By.id("SAVE_FOOTER")).click();
					CA.countData(Country);
					// scrn.getscreenshot();
				} else {
					Reporter.log("Its not a Japan country");
				}

			} catch (Exception e) {
				Assert.fail("An error occured while executing RTC_003/RTC_025 " + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-51_003b")) {

			try {
				driver.navigate().refresh();
				// Alert alert = driver.switchTo().alert();
				// alert.accept();
				Country.trim();
				if (Country.equalsIgnoreCase("United States")) {
					CA.navigationCreateAccounts();
					// Select dropdown = new
					// Select(driver.findElement(By.id("c_country_id_c")));
					// dropdown.selectByVisibleText("United States");
					Thread.sleep(10000);
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					Thread.sleep(10000);

					CA.fillData(ContactType, CompanyName, ContactPersonNameLatin, ContactPersonNameLocal, DivisionLatin,
							DivisionLocal, TitleLatin, TitleLocal, Telephone, ExtnNumber, FaxNumber, Mobile,
							eMailAddress, Country, PostalNumber, AddressUS, USCity, japanAddress1, japanAddress2,
							AddressLatin, AddressLocal);

					Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
					dropdown.selectByVisibleText("United States");
					Thread.sleep(2000);
					driver.findElement(By.id("addr_general_c")).sendKeys(AddressUS);
					driver.findElement(By.id("city_general_c")).sendKeys(USCity);
					Select dropdown1 = new Select(driver.findElement(By.id("c_state_id_c")));
					dropdown1.selectByVisibleText("American Samoa");
					driver.findElement(By.id("postal_no_c")).sendKeys(PostalNumber);
					// scrn.getscreenshot();
					driver.findElement(By.id("SAVE_FOOTER")).click();
					CA.countData(Country);
					// scrn.getscreenshot();
				}

				else {
					Reporter.log("Its not a United States country");
				}

				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='header']/div/div/div/div[1]/ul/li[3]/span/button")).click();
				driver.findElement(By.xpath("//div[1]/div/div[2]/div/div/div/div[1]/ul/li[3]/span/div/ul/li[2]/a"))
						.click();
				Thread.sleep(1000);
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Thread.sleep(1000);
				driver.findElement(By.id("last_name_basic")).clear();
				driver.findElement(By.id("last_name_basic")).sendKeys(
						"Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789");
				driver.findElement(By.id("account_name_basic")).clear();
				driver.findElement(By.id("search_form_submit")).click();

				driver.findElement(By.xpath("//*[@id='MassUpdate']/table/tbody/tr[3]/td[3]/b/a")).click();
				Reporter.log("The account is searchable");

			} catch (Exception e) {
				Assert.fail("An error occured while executing the TC_NJ-51_003b" + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-51_003c/RTC_008/RTC_021")) {
			try {
				Country.trim();
				if (Country.equalsIgnoreCase("Canada")) {

					CA.navigationCreateAccounts();
					Thread.sleep(10000);
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					Thread.sleep(10000);
					CA.fillData(ContactType, CompanyName, ContactPersonNameLatin, ContactPersonNameLocal, DivisionLatin,
							DivisionLocal, TitleLatin, TitleLocal, Telephone, ExtnNumber, FaxNumber, Mobile,
							eMailAddress, Country, PostalNumber, AddressUS, USCity, japanAddress1, japanAddress2,
							AddressLatin, AddressLocal);
					Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
					dropdown.selectByVisibleText("Canada");
					Thread.sleep(2000);
					driver.findElement(By.id("addr_general_c")).sendKeys(AddressUS);
					driver.findElement(By.id("city_general_c")).sendKeys(USCity);
					Select dropdown1 = new Select(driver.findElement(By.id("c_state_id_c")));
					dropdown1.selectByVisibleText("British Columbia");
					driver.findElement(By.id("postal_no_c")).sendKeys(PostalNumber);
					// scrn.getscreenshot();
					driver.findElement(By.id("SAVE_FOOTER")).click();
					CA.countData(Country);
					Reporter.log("The Account is displayed in  detail view RTC_008/RTC_021 passed");
					// scrn.getscreenshot();
				} else {
					Reporter.log("Its not a Canada country");
				}

			} catch (Exception e) {
				Assert.fail("An error occured while executing the TC_NJ-51_003c/RTC_008/RTC_021" + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("RTC_024/RTC_026/RTC_027/RTC_006")) {
			try {

				CA.navigationCreateAccounts();
				Thread.sleep(10000);
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Thread.sleep(10000);
				CA.fillData(ContactType, CompanyName, ContactPersonNameLatin, ContactPersonNameLocal, DivisionLatin,
						DivisionLocal, TitleLatin, TitleLocal, Telephone, ExtnNumber, FaxNumber, Mobile, eMailAddress,
						Country, PostalNumber, AddressUS, USCity, japanAddress1, japanAddress2, AddressLatin,
						AddressLocal);
				Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
				dropdown.selectByVisibleText("India");
				driver.findElement(By.id("addr_1_c")).sendKeys("Non Japan/US/Canada");
				driver.findElement(By.id("addr_2_c")).sendKeys("Non Japan/US/Canada");
				driver.findElement(By.id("Contacts0_email_widget_add")).click();
				driver.findElement(By.id("Contacts0emailAddress1")).sendKeys("dummy02@gmail.com");
				Boolean bo = driver.findElement(By.id("attention_line_c")).isEnabled();
				driver.findElement(By.id("SAVE_FOOTER")).click();
				String Temp_CountryName = driver.findElement(By.id("c_country_id_c")).getText();

				String Temp_email2 = driver.findElement(By.xpath(".//*[@id='email1_span']/table/tbody/tr[2]/td/a"))
						.getText();
				String Temp_email1 = driver.findElement(By.xpath(".//*[@id='email1_span']/table/tbody/tr[1]/td/b/a"))
						.getText();

				if (Temp_CountryName.equalsIgnoreCase("India")) {
					Reporter.log("The value selected is non Japan/US/cannda   RTC_024 passed");
				} else {
					Assert.fail("An error occured in RTC_024");
				}

				if (bo) {
					Reporter.log("attentional line feild is present");
				} else {
					Reporter.log("attentional line feild is not present");
					Assert.fail("attentional line feild RTC_024");
				}

				if (Temp_email2.equalsIgnoreCase("dummy02@gmail.com")) {
					Reporter.log("The account can be created with multiuple e-mails id Frist e-mail is " + Temp_email1
							+ "And second is " + Temp_email2);
				} else {
					Assert.fail("The email did not match RTC_024");
				}

				driver.findElement(By.id("edit_button")).click();
				driver.findElement(By.id("Contacts0emailAddress0")).clear();
				driver.findElement(By.id("Contacts0emailAddress0")).sendKeys("edit@gmail.com");
				driver.findElement(By.id("SAVE_FOOTER")).click();
				String Temp_email3 = driver.findElement(By.xpath(".//*[@id='email1_span']/table/tbody/tr[1]/td/b/a"))
						.getText();

				if (Temp_email3.equalsIgnoreCase("edit@gmail.com")) {
					Reporter.log("The account can be edited  with multiuple e-mails id Frist e-mail is " + Temp_email1
							+ "And after update  is " + Temp_email3);
				} else {
					Assert.fail("The email did not match RTC_024");
				}
				driver.findElement(By.id("btn_view_change_log")).click();

				String winHandleBefore4 = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}

				String value = driver.findElement(By.xpath("//tbody/tr/td/table[2]/tbody/tr[2]/td[4]/div")).getText();
				if (value.isEmpty()) {
					Assert.fail();
					Reporter.log("View change log not updated");

				} else {
					Reporter.log("View change log updated");
				}

				driver.switchTo().window(winHandleBefore4);

			} catch (Exception e) {
				Assert.fail("An error occured while executing RTC_024/RTC_026/RTC_027/RTC_006 " + e);
			}

		}

	}

	@DataProvider
	public Object[][] CreateAccountData() {
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}

	public void navigationCreateAccounts() throws InterruptedException {

		// WebElement element = driver.findElement(By.linkText("Accounts"));
		// Actions action = new Actions(driver);
		// action.moveToElement(element).build().perform();
		// driver.findElement(By.id("CreateAccountAll")).click();

		// Added for automation to come out the frames

		System.out.println("Inside Navigation Create Accounts");
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[3]/span/button")).click();
		driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[3]/span/div/ul/li[1]/a")).click();

	}

	public void fillData(String ContactType, String CompanyName, String ContactPersonNameLatin,
			String ContactPersonNameLocal, String DivisionLatin, String DivisionLocal, String TitleLatin,
			String TitleLocal, String Telephone, String ExtnNumber, String FaxNumber, String Mobile,
			String eMailAddress, String Country, String PostalNumber, String AddressUS, String USCity,
			String japanAddress1, String japanAddress2, String AddressLatin, String AddressLocal)
			throws InterruptedException {

		Thread.sleep(2000);
		Select dropdown = new Select(driver.findElement(By.id("contact_type_c")));
		dropdown.selectByVisibleText(ContactType);

		driver.findElement(By.id("btn_account_name")).click();
		String winHandleBefore = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			//
			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("common_customer_id_c_advanced")).sendKeys("C2010040802");
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//tbody/tr[3]/td[2]/a")).click();
		driver.switchTo().window(winHandleBefore);
		driver.switchTo().frame("bwc-frame");
		driver.findElement(By.id("last_name")).sendKeys(ContactPersonNameLatin);
		driver.findElement(By.id("name_2_c")).sendKeys(ContactPersonNameLocal);
		driver.findElement(By.id("division_1_c")).sendKeys("Dummy NAme");
		driver.findElement(By.id("division_2_c")).sendKeys(DivisionLocal);
		driver.findElement(By.id("title")).sendKeys(TitleLatin);
		driver.findElement(By.id("title_2_c")).sendKeys(TitleLocal);
		driver.findElement(By.id("phone_work")).sendKeys(Telephone);
		driver.findElement(By.id("ext_no_c")).sendKeys(ExtnNumber);
		driver.findElement(By.id("phone_fax")).sendKeys(FaxNumber);
		driver.findElement(By.id("phone_mobile")).sendKeys(Mobile);
		driver.findElement(By.id("Contacts0emailAddress0")).sendKeys(eMailAddress);
		// driver.findElement(By.id("postal_no_c")).sendKeys(PostalNumber);

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("last_name")));
		driver.findElement(By.id("last_name")).sendKeys("****");

		// driver.findElement(By.xpath("/html/body/div[3]/div/table/tbody/tr/td/form/div[1]/div/div[1]/table/tbody/tr[2]/td[2]/input")).sendKeys("xyz");

	}

	public void clearData() throws InterruptedException {

		// driver.findElement(By.id("contact_type_c")).clear();
		driver.findElement(By.id("btn_clr_account_name")).click();
		driver.findElement(By.id("last_name")).clear();
		driver.findElement(By.id("name_2_c")).clear();
		driver.findElement(By.id("division_1_c")).clear();
		driver.findElement(By.id("division_2_c")).clear();
		driver.findElement(By.id("title")).clear();
		driver.findElement(By.id("title_2_c")).clear();
		driver.findElement(By.id("phone_work")).clear();
		driver.findElement(By.id("ext_no_c")).clear();
		driver.findElement(By.id("phone_fax")).clear();
		driver.findElement(By.id("phone_mobile")).clear();
		driver.findElement(By.id("Contacts0emailAddress0")).clear();
		driver.findElement(By.id("postal_no_c")).clear();
		Select dropdown = new Select(driver.findElement(By.id("c_country_id_c")));
		dropdown.selectByVisibleText("");
		Thread.sleep(5000);

	}

	public void navigationViewAccount(String ContactPersonNameLatin) {
		/*
		 * WebElement element = driver.findElement(By.linkText("Accounts"));
		 * Actions action = new Actions(driver);
		 * action.moveToElement(element).build().perform();
		 * System.out.println("accounts clicked");
		 * //driver.findElement(By.id("ViewAccountsAll")).click();
		 */
		/* Added for automation viewAccount navigation start */
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/div[1]/ul/li[3]/span/button")).click();
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/div[1]/ul/li[3]/span/div/ul/li[2]/a"))
				.click();
		/* Added for automation viewAccount navigation start */
		driver.switchTo().frame("bwc-frame");
		driver.findElement(By.id("last_name_basic")).clear();
		driver.findElement(By.id("last_name_basic")).sendKeys(ContactPersonNameLatin);
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//tbody/tr[3]/td[3]/b/a")).click();
	}

	public void countData(String Country) {
		String count = driver.findElement(By.id("last_name")).getText();
		Reporter.log("*************" + count.length());
		if ((count.length() > 60)) {
			Reporter.log("Contact person Name latin is more than 60 Characters" + count.length());
			Assert.fail();

		} else {
			Reporter.log("Contact person Name latin is less  than 60 Characters" + count.length());

		}

		count = driver.findElement(By.id("name_2_c")).getText();
		if ((count.length() > 60)) {
			Reporter.log("Contact person Name local  is more than 60 Characters" + count.length());
			Assert.fail();

		} else {
			Reporter.log("Contact person Name local is less  than 60 Characters" + count.length());

		}

		count = driver.findElement(By.id("postal_no_c")).getText();
		if ((count.length() > 9)) {
			Reporter.log("Postal number  is more than 9 Characters" + count.length());
			Assert.fail();

		} else {
			Reporter.log("Postal number  is less than 9 Characters" + count.length());

		}

		count = driver.findElement(By.id("division_1_c")).getText();
		if ((count.length() > 100)) {
			Reporter.log("Division Latin is more than 100 Characters" + count.length());
			Assert.fail();

		} else {
			Reporter.log("Division Latin is less than 100 Characters" + count.length());

		}

		count = driver.findElement(By.id("division_2_c")).getText();
		if ((count.length() > 100)) {
			Reporter.log("Division Local is more than 100 Characters" + count.length());
			Assert.fail();

		} else {
			Reporter.log("Division Local is less  than 100 Characters" + count.length());

		}

		count = driver.findElement(By.id("title")).getText();
		if ((count.length() > 100)) {
			Reporter.log("Tittle Latin is more than 100 Characters" + count.length());
			Assert.fail();

		} else {
			Reporter.log("Tittle Latin is less  than 100 Characters" + count.length());

		}

		count = driver.findElement(By.id("title_2_c")).getText();
		if ((count.length() > 100)) {
			Reporter.log("Tittle Local is more than 100 Characters" + count.length());
			Assert.fail();

		} else {
			Reporter.log("Tittle local is less  than 100 Characters" + count.length());

		}

		count = driver.findElement(By.xpath("//table/tbody/tr[3]/td[2]")).getText();
		if ((count.length() > 20)) {
			Reporter.log("Telephone Number is more than 20 Characters" + count.length());
			Assert.fail();

		} else {
			Reporter.log("Telephone Number is less  than 20 Characters" + count.length());

		}

		count = driver.findElement(By.id("ext_no_c")).getText();
		if ((count.length() > 5)) {
			Reporter.log("Extension Number is more than 5 Characters" + count.length());
			Assert.fail();

		} else {
			Reporter.log("Extension Number is less  than 5 Characters" + count.length());

		}

		count = driver.findElement(By.xpath("//table/tbody/tr[4]/td[2]")).getText();
		if ((count.length() > 20)) {
			Reporter.log("Fax Number is more than 20 Characters" + count.length());
			Assert.fail();

		} else {
			Reporter.log("Fax Number is less  than 20 Characters" + count.length());

		}

		count = driver.findElement(By.xpath("//table/tbody/tr[4]/td[4]")).getText();
		if ((count.length() > 20)) {
			Reporter.log("Mobile Number is more than 20 Characters" + count.length());
			Assert.fail();

		} else {
			Reporter.log("Mobile Number is less  than 20 Characters" + count.length());

		}

		count = driver.findElement(By.xpath("//span/table/tbody/tr/td/b/a")).getText();
		if ((count.length() > 140)) {
			Reporter.log("E-mail address is more than 140 Characters" + count.length());
			Assert.fail();

		} else {
			Reporter.log("E-mail address is less  than 140 Characters" + count.length());

		}

		if (Country.equalsIgnoreCase("United States")) {
			count = driver.findElement(By.id("addr_general_c")).getText();
			if ((count.length() > 100)) {
				Reporter.log("United state city is more than 100 Characters" + count.length());
				Assert.fail();

			} else {
				Reporter.log("United state city is less  than 100 Characters" + count.length());

			}

		}

		if (Country.equalsIgnoreCase("Canada")) {
			count = driver.findElement(By.id("addr_general_c")).getText();
			if ((count.length() > 100)) {
				Reporter.log("Canada city is more than 100 Characters" + count.length());
				Assert.fail();

			} else {
				Reporter.log("Canada city is less  than 100 Characters" + count.length());

			}

		}

	}

	@AfterClass
	public void close() {
		System.out.println("Inside after close");
		driver.close();
	}

}
