package com.stta.GCM;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class CreateCorporateNew extends SuiteGcmBase {

	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[] = null;
	static boolean TestCasePass = true;
	static int DataSet = -1;
	static boolean Testskip = false;
	static boolean Testfail = false;
	Screen scrn = new Screen();

	// Log4JLogger log = Logger.getLogger(log.class.getName()) ;

	login log = new login();

	@BeforeTest
	public void checkCaseToRun() throws Exception {
		// Called init() function from SuiteBase class to Initialize .xls Files
		init();

		log.Run();
		// To set SuiteOne.xls file's path In FilePath Variable.
		FilePath = TestCaseListExcelOne;
		TestCaseName = this.getClass().getSimpleName();
		// SheetName to check CaseToRun flag against test case.
		SheetName = "TestCasesList";
		// Name of column In TestCasesList Excel sheet.
		ToRunColumnNameTestCase = "CaseToRun";
		// Name of column In Test Case Data sheets.
		ToRunColumnNameTestData = "DataToRun";
		// Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(TestCaseName + " : Execution started.");

		// To check test case's CaseToRun = Y or N In related excel sheet.
		// If CaseToRun = N or blank, Test case will skip execution. Else It
		// will be executed.
		if (!SuiteUtility.checkToRunUtility(FilePath, SheetName, ToRunColumnNameTestCase, TestCaseName)) {
			Add_Log.info(TestCaseName + " : CaseToRun = N for So Skipping Execution.");
			// To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
			// To throw skip exception for this test case.
			throw new SkipException(
					TestCaseName + "'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of " + TestCaseName);
		}
		// To retrieve DataToRun flags of all data set lines from related test
		// data sheet.
		TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);

		System.out.println(TestDataToRun);
	}

	@Test(dataProvider = "CreateCorporateData")
	public void CreateCorporateTest(String Test_CaseId, String userName, String password, String CommonCustomerID,
			String CorporateNameLatinCharacter, String CorporateNameLocalCharacter1,
			String CorporateNameLocalCharacter2, String CountryName, String CountryNameCode,
			String HeadquarterPostalNumber, String HeadquarterAddressLatinCharacter,
			String HeadquarterAddressLocalCharacter, String RegisteredPostalNumber,
			String RegisteredAddressLatinCharacter, String RegisteredAddressLocalCharacter, String VATNumber)
			throws Exception {

		DataSet++;

		// If found DataToRun = "N" for data set then execution will be skipped
		// for that data set.
		if (!TestDataToRun[DataSet].equalsIgnoreCase("Y")) {
			Add_Log.info(TestCaseName + " : DataToRun = N for data set line " + (DataSet + 1)
					+ " So skipping Its execution.");
			// If DataToRun = "N", Set Testskip=true.
			Testskip = true;
			throw new SkipException(
					"DataToRun for row number " + DataSet + " Is No Or Blank. So Skipping Its Execution.");
		}

		CreateCorporateNewData ccnd = new CreateCorporateNewData();
		CidasCorporate cds = new CidasCorporate();
		EditCorporate EC = new EditCorporate();
		CreateAccount ca = new CreateAccount();
		CreateContract cc = new CreateContract();
		EditAccount ve = new EditAccount();

		try {
			if (Test_CaseId.equalsIgnoreCase("RTC_020")) {
				try {
					Thread.sleep(5000);
					ccnd.CreateCorpNavigation();
					Thread.sleep(10000);
					driver.switchTo().defaultContent(); // outside the frame
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame"))); // selecting
																						// the
																						// frame
					driver.findElement(By.id("save")).click();
					/* Added for automation start */
					String ErrrorMsg = driver
							.findElement(By.xpath(".//*[@id='LBL_EDITVIEW_PANEL2']/tbody/tr[1]/td[2]/div")).getText();
					if (ErrrorMsg.isEmpty()) {
						System.out.println("failed");
						Reporter.log("Mandetory check failed for Corporate Name");
						Assert.fail();
					} else {
						System.out.println("passed");
						Reporter.log("Mandetory check passed for Corporate Name" + ErrrorMsg);
					}
					// scrn.getscreenshot();
					//

				} catch (Exception e) {
					Assert.fail("An error occured while executing RTC_020 " + e);
				}
			} else if (Test_CaseId.equalsIgnoreCase(
					"TC_Corporate_001/TC_Corporate_002/TC_Corporate_003/TC_Corporate_004/TC_Corporate_006/TC_Corporate_007/TC_Corporate_008/TC_Corporate_009/TC_Corporate_010/TC_Corporate_011/TC_Corporate_012/TC_Corporate_013/TC_Corporate_014")) {
				/*
				 * Business logic to identify if the count in the feilds are
				 * correct
				 */

				ccnd.DataFillCorp(CommonCustomerID, CorporateNameLatinCharacter, CorporateNameLocalCharacter1,
						CorporateNameLocalCharacter2, CountryName, CountryNameCode, HeadquarterPostalNumber,
						HeadquarterAddressLatinCharacter, HeadquarterAddressLocalCharacter, RegisteredPostalNumber,
						RegisteredAddressLatinCharacter, RegisteredAddressLocalCharacter, VATNumber);
				ccnd.DataCount(); // To count the feild lenght

				/*
				 * Added for automation to verify all fields are present or not
				 */
				try {
					driver.navigate().refresh();
					Alert alert = driver.switchTo().alert();
					alert.accept();
					Thread.sleep(30000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

					boolean commonCustIds = driver.findElement(By.id("common_customer_id_c")).isDisplayed();
					boolean corpNameLatin = driver.findElement(By.id("name")).isDisplayed();
					boolean corpNameLoc1 = driver.findElement(By.id("name_2_c")).isDisplayed();
					boolean corpNameLoc2 = driver.findElement(By.id("name_3_c")).isDisplayed();
					boolean custStatusCode = driver.findElement(By.id("customer_status_code_c")).isDisplayed();
					boolean cntryname = driver.findElement(By.id("country_name_c")).isDisplayed();
					boolean cntryCodeName = driver.findElement(By.id("country_code_c")).isDisplayed();
					boolean hqAddressLatin = driver.findElement(By.id("hq_addr_1_c")).isDisplayed();
					boolean hqAddressLocal = driver.findElement(By.id("hq_addr_2_c")).isDisplayed();
					boolean hqPostNumber = driver.findElement(By.id("hq_addr_2_c")).isDisplayed();
					boolean regAddressLatin = driver.findElement(By.id("reg_addr_1_c")).isDisplayed();
					boolean regAddressLocal = driver.findElement(By.id("reg_addr_2_c")).isDisplayed();
					boolean redPostNumber = driver.findElement(By.id("reg_postal_no_c")).isDisplayed();
					if (commonCustIds) {
						// System.out.println("Available fields check for Common
						// Customer ID passed");
						Reporter.log("Available fields check for Common Customer ID passed");
					} else {
						// System.out.println("Available fields check for Common
						// Customer ID failed");
						Reporter.log("Available fields check for Common Customer ID failed");
					}

					if (corpNameLatin) {
						// System.out.println("Available fields check for
						// Corporate Name Latin passed");
						Reporter.log("Available fields check for Corporate Name Latin passed");
					} else {
						// System.out.println("Available fields check for
						// Corporate Name Latin failed");
						Reporter.log("Available fields check for Corporate Name Latin failed");
					}

					if (corpNameLoc1) {
						// System.out.println("Available fields check for
						// Corporate Name (Local Character 1) passed");
						Reporter.log("Available fields check for Corporate Name (Local Character 1) passed");
					} else {
						// System.out.println("Available fields check for
						// Corporate Name (Local Character 1) failed");
						Reporter.log("Available fields check for Corporate Name (Local Character 1) failed");
					}

					if (corpNameLoc2) {
						// System.out.println("Available fields check for
						// Corporate Name (Local Character 2) passed");
						Reporter.log("Available fields check for Corporate Name (Local Character 2) passed");
					} else {
						// System.out.println("Available fields check for
						// Corporate Name (Local Character 2) failed");
						Reporter.log("Available fields check for Corporate Name (Local Character 2) failed");
					}

					if (custStatusCode) {
						// System.out.println("Available fields check for
						// Customer Status Code passed");
						Reporter.log("Available fields check for Customer Status Code passed");
					} else {
						// System.out.println("Available fields check for
						// Customer Status Code failed");
						Reporter.log("Available fields check for Customer Status Code failed");
					}

					if (cntryname) {
						// System.out.println("Available fields check for
						// Country Name passed");
						Reporter.log("Available fields check for Country Name passed");
					} else {
						// System.out.println("Available fields check for
						// Country Name failed");
						Reporter.log("Available fields check for Country Name failed");
					}

					if (cntryCodeName) {
						// System.out.println("Available fields check for
						// Country Name Code passed");
						Reporter.log("Available fields check for Country Name Code passed");
					} else {
						// System.out.println("Available fields check for
						// Country Name Code failed");
						Reporter.log("Available fields check for Country Name Code failed");
					}

					if (hqAddressLatin) {
						// System.out.println("Available fields check for
						// Headquarter Address (Latin Character) passed");
						Reporter.log("Available fields check for Headquarter Address (Latin Character) passed");
					} else {
						// System.out.println("Available fields check for
						// Headquarter Address (Latin Character) failed");
						Reporter.log("Available fields check for Headquarter Address (Latin Character) failed");
					}

					if (hqAddressLocal) {
						// System.out.println("Available fields check for
						// Headquarter Address (Local Character) passed");
						Reporter.log("Available fields check for Headquarter Address (Local Character) passed");
					} else {
						// System.out.println("Available fields check for
						// Headquarter Address (Local Character) failed");
						Reporter.log("Available fields check for Headquarter Address (Local Character) failed");
					}

					if (hqPostNumber) {
						// System.out.println("Available fields check for
						// Headquarter Postal Number passed");
						Reporter.log("Available fields check for Headquarter Postal Number passed");
					} else {
						System.out.println("Available fields check for Headquarter Postal Number failed");
						Reporter.log("Available fields check for Headquarter Postal Number failed");
					}

					if (regAddressLatin) {
						// System.out.println("Available fields check for
						// Registered Address (Latin Character) passed");
						Reporter.log("Available fields check for Registered Postal Number passed");
					} else {
						// System.out.println("Available fields check for
						// Registered Address (Latin Character) failed");
						Reporter.log("Available fields check for Registered Postal Number failed");
					}

					if (regAddressLocal) {
						// System.out.println("Available fields check for
						// Registered Address (Local Character) passed");
						Reporter.log("Available fields check for Registered Address (Local Character) passed");
					} else {
						// System.out.println("Available fields check for
						// RRegistered Address (Local Character) failed");
						Reporter.log("Available fields check for Registered Address (Local Character) failed");
					}

					if (redPostNumber) {
						// System.out.println("Available fields check for
						// Registered Postal Number passed");
						Reporter.log("Available fields check for Registered Postal Number passed");
					} else {
						// System.out.println("Available fields check for
						// Registered Postal Number failed");
						Reporter.log("Available fields check for Registered Postal Number failed");
					}
					// scrn.getscreenshot();

				} catch (Exception e) {
					Assert.fail(
							"An error occured while executing TC_Corporate_001/TC_Corporate_002/TC_Corporate_003/TC_Corporate_004/TC_Corporate_006/TC_Corporate_007/TC_Corporate_008/TC_Corporate_009/TC_Corporate_010/TC_Corporate_011/TC_Corporate_012/TC_Corporate_013/TC_Corporate_014 "
									+ e);
				}

			} else if (Test_CaseId.equalsIgnoreCase("TC_NJ-40_001")) {

				// This test case will fail because with out CID we cannot
				// search in list view and corporate cannot be verified.
				// driver.navigate().refresh();
				// Alert alert1 = driver.switchTo().alert();
				// alert1.dismiss();
				// ccnd.DataFillCorp(CommonCustomerID,
				// CorporateNameLatinCharacter, CorporateNameLocalCharacter1,
				// CorporateNameLocalCharacter2, CountryName, CountryNameCode,
				// HeadquarterPostalNumber, HeadquarterAddressLatinCharacter,
				// HeadquarterAddressLocalCharacter, RegisteredPostalNumber,
				// RegisteredAddressLatinCharacter,
				// RegisteredAddressLocalCharacter, VATNumber);
				// driver.findElement(By.id("save")).click();
				// ccnd.DataCheckCorp(CommonCustomerID,
				// CorporateNameLatinCharacter, CorporateNameLocalCharacter1,
				// CorporateNameLocalCharacter2, CountryName, CountryNameCode,
				// HeadquarterPostalNumber, HeadquarterAddressLatinCharacter,
				// HeadquarterAddressLocalCharacter, RegisteredPostalNumber,
				// RegisteredAddressLatinCharacter,
				// RegisteredAddressLocalCharacter, VATNumber);
				//
				// /* Added for automation to verify all fields are present or
				// not */
				// //driver.navigate().refresh();
				// //ccnd.CreateCorpNavigation();
				// //Alert alert = driver.switchTo().alert();
				// //alert.accept();
				// Thread.sleep(5000);
				// driver.switchTo().defaultContent();
				// driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// corporateFillData(CommonCustomerID,
				// CorporateNameLatinCharacter, CorporateNameLocalCharacter1,
				// CorporateNameLocalCharacter2, CountryName, CountryNameCode,
				// HeadquarterPostalNumber, HeadquarterAddressLatinCharacter,
				// HeadquarterAddressLocalCharacter, RegisteredPostalNumber,
				// RegisteredAddressLatinCharacter,
				// RegisteredAddressLocalCharacter);
				// //scrn.getscreenshot();
				// //driver.findElement(By.id("save")).click();
				// /*Thread.sleep(3000);
				// corporateVerifyData(CommonCustomerID,
				// CorporateNameLatinCharacter, CorporateNameLocalCharacter1,
				// CorporateNameLocalCharacter2, CountryName, CountryNameCode,
				// HeadquarterPostalNumber, HeadquarterAddressLatinCharacter,
				// HeadquarterAddressLocalCharacter, RegisteredPostalNumber,
				// RegisteredAddressLatinCharacter,
				// RegisteredAddressLocalCharacter);
				// //scrn.getscreenshot();*/
			} else if (Test_CaseId.equalsIgnoreCase("RTC_011/RTC-022/RTC_015/RTC_005/RTC_004")) {

				try {
					// ccnd.CreateCorpNavigation();
					// Thread.sleep(1000);
					// driver.switchTo().defaultContent(); // outside the frame
					// driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					// Thread.sleep(5000);
					// System.out.println("Before clear data");
					ccnd.ClearDataFill();
					System.out.println("After clear data");
					ccnd.DataFillCorp(CommonCustomerID, CorporateNameLatinCharacter, CorporateNameLocalCharacter1,
							CorporateNameLocalCharacter2, CountryName, CountryNameCode, HeadquarterPostalNumber,
							HeadquarterAddressLatinCharacter, HeadquarterAddressLocalCharacter, RegisteredPostalNumber,
							RegisteredAddressLatinCharacter, RegisteredAddressLocalCharacter, VATNumber);
					driver.findElement(By.id("save")).click();
					String id = driver.findElement(By.id("id")).getText();
					if (id.isEmpty()) {
						Assert.fail("Object id for coporate is not generated- Failed to create a corporate");
						Reporter.log("Object id for coporate is not generated- Failed to create a corporate ");

					} else {
						Reporter.log("Corporate created sucessfully " + id);
					}
					ccnd.DataCheckCorp(CommonCustomerID, CorporateNameLatinCharacter, CorporateNameLocalCharacter1,
							CorporateNameLocalCharacter2, CountryName, CountryNameCode, HeadquarterPostalNumber,
							HeadquarterAddressLatinCharacter, HeadquarterAddressLocalCharacter, RegisteredPostalNumber,
							RegisteredAddressLatinCharacter, RegisteredAddressLocalCharacter, VATNumber);
					String CommonCustomerIDVerification = driver.findElement(By.id("common_customer_id_c")).getText();
					if (CommonCustomerIDVerification.isEmpty()) {
						Reporter.log("Common customer id is empty RTC_015 pass");
					} else {
						Reporter.log("Common customer id is not empty RTC_015 fail");
						Assert.fail("Common customer id is not empty RTC_015 fail");
					}

					Thread.sleep(10000);
					driver.findElement(By.id("edit_button")).click();
					driver.findElement(By.id("name")).clear();
					driver.findElement(By.id("name")).sendKeys("Nrew corporate");
					String CorporateName = driver.findElement(By.id("name")).getAttribute("value");

					if (CorporateName.contains("Nrew corporate")) {
						Reporter.log("Corporate Name Latin is  editable");
					} else {
						System.out.println("CorporateName" + CorporateName);
						Reporter.log("Corporate Name Latin is not editable");
						Assert.fail("Corporate Name Latin is not editable" + CorporateName);
					}

				} catch (Exception e) {
					Assert.fail("An error occured while executing RTC_011/RTC-022/RTC_015/RTC_005/RTC_004 " + e);
				}

			} else if (Test_CaseId.equalsIgnoreCase("RTC_012/RTC_013")) {
				// This part of the code is drafted by cloud
				try {
					try {
						driver.findElement(By.id("common_customer_id_c")).sendKeys("**********");
					} catch (Exception e) {
						Reporter.log("Common customer id feild is not editable and the error messgae is '\t' " + e);
					}
					driver.findElement(By.id("save")).click();
					driver.findElement(By.id("edit_button")).click();
					driver.findElement(By.id("name")).clear();
					driver.findElement(By.id("name")).sendKeys("Updated");
					driver.findElement(By.id("save")).click();
					driver.findElement(By.id("btn_view_change_log")).click();

					String winHandleBefore4 = driver.getWindowHandle();
					for (String handle : driver.getWindowHandles()) {
						driver.switchTo().window(handle);
					}

					String value = driver.findElement(By.xpath("//tbody/tr/td/table[2]/tbody/tr[2]/td[4]/div"))
							.getText();
					if (value.isEmpty()) {
						Assert.fail("View change log not updated");
						Reporter.log("View change log not updated");

					} else {
						Reporter.log("View change log updated");
					}

					driver.switchTo().window(winHandleBefore4);

				} catch (Exception e) {
					Assert.fail("An error occured while executing RTC_012/RTC_013" + e);
				}
			} else if (Test_CaseId.equalsIgnoreCase("RTC_018")) {
				// This part of the code is drafted by cloud
				try {
					ccnd.CreateCorpNavigation();
					Thread.sleep(10000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					driver.findElement(By.id("name")).sendKeys(CorporateNameLatinCharacter);
					driver.findElement(By.id("save")).click();
					String CorporateName = driver.findElement(By.id("name")).getText();

					if (CorporateName.isEmpty()) {
						Reporter.log(
								"RTC_018 -failed as corporate can be created without corporate naame latin or local");
						Assert.fail(
								"RTC_018 -failed as corporate can be created without corporate naame latin or local");
					} else {
						String id = driver.findElement(By.id("id")).getText();
						Reporter.log("The corporate is created with corporarate Name latin " + CorporateName
								+ "And the Id is " + id);

					}

				} catch (Exception e) {
					Assert.fail("An error occured while executing RTC_018" + e);
				}
			} else if (Test_CaseId.equalsIgnoreCase("RTC_019")) {
				// This part of the code is drafted by cloud
				try {
					driver.switchTo().defaultContent();
					ccnd.CreateCorpNavigation();
					Thread.sleep(10000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					driver.findElement(By.id("name_2_c")).sendKeys(CorporateNameLocalCharacter1);
					driver.findElement(By.id("save")).click();
					String CorporateName = driver.findElement(By.id("name_2_c")).getText();

					if (CorporateName.isEmpty()) {
						Reporter.log(
								"RTC_019 -failed as corporate can be created without corporate naame latin or local");
						Assert.fail(
								"RTC_019 -failed as corporate can be created without corporate naame latin or local");
					} else {
						String id = driver.findElement(By.id("id")).getText();
						Reporter.log("The corporate is created with corporarate Name local " + CorporateName
								+ "and the id is " + id);
					}

				} catch (Exception e) {
					Assert.fail("An error occured while executing RTC_019" + e);
				}
			} else if (Test_CaseId.equalsIgnoreCase("RTC_001/RTC_006/RTC_009/RTC_007/RTC_025/RTC_028")) {
				// This part of the code is drafted by cloud
				try {
					cds.cidasNewdata();

					String id = driver.findElement(By.id("id")).getText();
					Reporter.log("The corporate is created with Id " + id);
					driver.findElement(By.id("edit_button")).click();
					ccnd.CidasEditCheck();
					// This code is to validate RTC_007-to check if edit is
					// availble
					// for CIDAS
					driver.findElement(By.id("CANCEL_HEADER")).click();
					System.out.println("RTC_025");
					Reporter.log("RTC_025 cancel button is clicked");
					String Temp_tile = driver.getTitle();
					Reporter.log("RTC_025 passed navigated to last page" + Temp_tile);
					driver.findElement(By.xpath("//*[@id='EditView']/table/tbody/tr/td[1]/div/input[3]")).click();
					System.out.println("RTC_007 edit");

					String winHandleBefore5 = driver.getWindowHandle();
					for (String handle : driver.getWindowHandles()) {
						driver.switchTo().window(handle);
					}

					driver.findElement(By.xpath("//table[3]/tbody/tr[2]/td[1]/a")).click();
					System.out.println("RTC_007 After xpath");
					driver.switchTo().window(winHandleBefore5);
					Reporter.log("Edit was scuusess RTC_007 passed ");
					String Temp_tile1 = driver.getTitle();
					Reporter.log("RTC_025 passed navigated to last page" + Temp_tile1);

				} catch (Exception e) {
					Assert.fail(
							"An error occured while executing RTC_001/RTC_006/RTC_009/RTC_007/RTC_025/RTC_028 " + e);
				}
			} else if (Test_CaseId.equalsIgnoreCase("RTC_002/RTC_003/RTC_029/RTC_008")) {
				// This part of the code is drafted by cloud
				try {
					driver.navigate().refresh();
					driver.switchTo().defaultContent();
					ccnd.CreateCorpNavigation();
					Thread.sleep(10000);
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					Thread.sleep(10000);
					driver.findElement(By.xpath("//*[@id='EditView']/table/tbody/tr/td[1]/div/input[3]")).click();

					String winHandleBefore4 = driver.getWindowHandle();
					for (String handle : driver.getWindowHandles()) {
						driver.switchTo().window(handle);
					}

					try {
						driver.findElement(By.id("companyNameEng")).sendKeys("***********");
						Alert alert = driver.switchTo().alert();
						alert.accept();
						Reporter.log("Error meessage displayed when the value are  found in CIDAS");

					} catch (Exception e) {
						Reporter.log("Error meessage displayed when the value are not found in CIDAS" + e);
					}

					driver.findElement(By.id("companyNameEng")).clear();
					driver.findElement(By.id("companyNameEng")).sendKeys("Sugar 7 UAT Di Test Eng 02");
					driver.findElement(By.id("search_form_submit")).click();
					driver.findElement(By.xpath("//table[4]/tbody/tr[2]/td[1]/a")).click();
					driver.switchTo().window(winHandleBefore4);
					cds.cidasSearch();
					// driver.findElement(By.id("save")).click();
					Reporter.log("The search in CIDAS worked fine without error RTC_029 passed");

				} catch (Exception e) {
					Assert.fail("An error occured while executing RTC_002/RTC_003/RTC_029/RTC_008" + e);
				}
			} else if (Test_CaseId.equalsIgnoreCase("RTC_024")) {
				try {
					driver.switchTo().defaultContent();
					ccnd.CreateCorpNavigation();
					String Temp_CreateScreen = driver.getTitle();
					Reporter.log("The control is in create screen" + Temp_CreateScreen);
					Thread.sleep(10000);
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					Thread.sleep(10000);
					driver.findElement(By.id("CANCEL_HEADER")).click();
					String Temp_MainScreen = driver.getTitle();
					if (Temp_MainScreen.isEmpty()) {
						Assert.fail("The control is not in  main screen");
					} else {
						Reporter.log("The control is in main screen after clicking on cancel" + Temp_MainScreen);
					}
				} catch (Exception e) {
					Assert.fail("An error occured while executing RTC_024" + e);
				}
			} else if (Test_CaseId.equalsIgnoreCase("RTC_026/RTC_008")) {
				try {

					driver.navigate().refresh();
					Alert alert = driver.switchTo().alert();
					alert.accept();
					cds.cidasNewdata();

				} catch (Exception e) {
					Assert.fail("An error occured while executing RTC_026/RTC_008");
				}
			} else if (Test_CaseId.equalsIgnoreCase("RTC_028")) {
				try {
					EC.searchCorporate("C0000111111");

					driver.findElement(By.id("edit_button")).click();
					ccnd.CidasEditCheck();
					// RTC_008
				} catch (Exception e) {
					Assert.fail("An error occured while executing RTC_028" + e);
				}

			} else if (Test_CaseId.equalsIgnoreCase("RTC_008/RTC_031")) {
				try {

					driver.switchTo().defaultContent();
					ccnd.CreateCorpNavigation();
					Thread.sleep(10000);
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					Thread.sleep(10000);
					driver.findElement(By.xpath("//*[@id='EditView']/table/tbody/tr/td[1]/div/input[3]")).click();

					String winHandleBefore4 = driver.getWindowHandle();
					for (String handle : driver.getWindowHandles()) {
						driver.switchTo().window(handle);
					}

					driver.findElement(By.id("companyName")).clear();
					driver.findElement(By.id("companyName")).sendKeys("Automation");
					driver.findElement(By.id("search_form_submit")).click();
					driver.findElement(By.xpath("//table[4]/tbody/tr[2]/td[1]/a")).click();
					driver.switchTo().window(winHandleBefore4);
					Reporter.log("RTC_008 passed for Japan name ");
					Reporter.log("The pop up in screen RTC_031 passed");
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					Thread.sleep(1000);
					driver.findElement(By.id("CANCEL_HEADER")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();

					if (driver.findElements(By.className("alert-messages")).size() != 0) {
						driver.findElement(By.className("alert-btn-confirm")).click();
						System.out.println("I am inside pop up");
					}

				} catch (Exception e) {
					Assert.fail("An error occured while executing RTC_008/RTC_031 " + e);
				}

			} else if (Test_CaseId.equalsIgnoreCase("RTC_017")) {

				try {
					EC.searchCorporate("C2010040802");
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					Thread.sleep(10000);
					driver.findElement(
							By.xpath(".//*[@id='list_subpanel_account_subpanel']/table/tbody/tr[3]/td[2]/span/a"))
							.click();
					System.out.println("After selecting the corporate");
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					Thread.sleep(1000);
					String tempDivisionName = driver.findElement(By.xpath(".//*[@id='division_1_c']")).getText();
					String tempTitle = driver.getTitle();
					System.out.println("Tittle " + tempTitle);
					if (tempDivisionName.contains("Division Latin ")) {
						Reporter.log("RTC_017 passed -Navigated to account screen and page title is " + tempTitle);

					} else {
						Assert.fail("RTC_017 failed -The page did not navigate to accont screen" + tempTitle);
					}

				} catch (Exception e) {
					Assert.fail("An error occured while executing RTC_017 " + e);
				}

			} else if (Test_CaseId.equalsIgnoreCase("RTC_027")) {
				try {

					System.out.println("Inside 027");
					driver.switchTo().defaultContent();
					driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[3]/span/button")).click();
					driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[3]/span/div/ul/li[2]/a"))
							.click();
					System.out.println("Outside 027");

					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					Thread.sleep(5000);
					driver.findElement(By.id("last_name_basic")).clear();
					driver.findElement(By.id("account_name_basic")).clear();
					driver.findElement(By.id("account_name_basic")).sendKeys("NTT Canada ");
					driver.findElement(By.id("search_form_submit")).click();
					driver.findElement(By.xpath(".//*[@id='MassUpdate']/table/tbody/tr[3]/td[5]/a")).click();
					Thread.sleep(5000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					Thread.sleep(5000);
					driver.findElement(By.id("edit_button")).click();
					String tempCCID = driver.findElement(By.id("common_customer_id_c")).getText();

					if (tempCCID.isEmpty()) {
						Reporter.log("RTC_027  passed - The CCID is empty");
					} else {
						Assert.fail("RTC_027 failed -The CCID is not empty");
					}

				} catch (Exception e) {
					Assert.fail("An error occured while executing RTC_027 " + e);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			// driver.close();
			Reporter.log("" + e);
			Assert.fail("Error: " + e);

		}

	}

	@DataProvider
	public Object[][] CreateCorporateData() {
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}

	@AfterClass
	public void close() {
		System.out.println("Inside after close");
		driver.close();
		Alert alert = driver.switchTo().alert();
		alert.accept();

	}

}
