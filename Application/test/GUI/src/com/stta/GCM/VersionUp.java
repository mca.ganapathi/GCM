package com.stta.GCM;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class VersionUp extends SuiteGcmBase {
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[]=null;
	static boolean TestCasePass=true;
	static int DataSet=-1;	
	static boolean Testskip=false;
	static boolean Testfail=false;
	Screen scrn = new Screen();
	
	//Log4JLogger log = Logger.getLogger(log.class.getName()) ;
	
	login log = new login();

	login1 logwf = new login1();
	login1 logad = new login1();

	String WF1userName;
	String WF1password;
	String WF5userName;
	String WF5password;
	String WF8userName;
	String WF8password;
	
	String globalContractId = null;
	
	@BeforeTest
	public void checkCaseToRun() throws Exception {
		//Called init() function from SuiteBase class to Initialize .xls Files
		init();				
		log.Run();
		//To set SuiteOne.xls file's path In FilePath Variable.
		FilePath = TestCaseListExcelOne;		
		TestCaseName = this.getClass().getSimpleName();	
		//SheetName to check CaseToRun flag against test case.
		SheetName = "TestCasesList";
		//Name of column In TestCasesList Excel sheet.
		ToRunColumnNameTestCase = "CaseToRun";
		//Name of column In Test Case Data sheets.
		ToRunColumnNameTestData = "DataToRun";
		//Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(TestCaseName+" : Execution started.");
		//To check test case's CaseToRun = Y or N In related excel sheet.
		//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.		
		if(!SuiteUtility.checkToRunUtility(FilePath, SheetName,ToRunColumnNameTestCase,TestCaseName)){
			Add_Log.info(TestCaseName+" : CaseToRun = N for So Skipping Execution.");
			//To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
			//To throw skip exception for this test case.
			throw new SkipException(TestCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+TestCaseName);
		}
		//To retrieve DataToRun flags of all data set lines from related test data sheet.
		TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);
		System.out.println(TestDataToRun);
	}
	
	@SuppressWarnings("deprecation")
	@Test(dataProvider="VersionUpData")
	public void VersionUpTest(String Test_CaseId,String ContractId, String ContractName,
			String ContractDesc, String ProductsID, String ProductOffering,
			String ContractStartDateTimeZone, String ContractEndDate, String  ContractEndDateTimeZone,
			String FactoryReferenceNumber, String SalesChannelCode,
			String SalesRepNameLatin, String SalesRepNameLocal,
			String DivisionLatin, String DivisionLocal, String TittleLatin,
			String TittleLocal, String Telephone, String ExtnNumber,
			String FaxNumber, String MobileNumber, String mailId,
			String ProductSpecification, String ProductItem, 
			String ProductConfiguration, String ProductConfiguration1,
			String ProductConfiguration2, String ProductConfiguration3,
			String ProductConfiguration4, String ProductConfiguration5,
			String ProductConfiguration6, String ProductConfiguration7,
			String PaymentType, String AccountHolderName, String DepositType,
			String BankCode, String BranchCode, String AccountNumber,
			String AccountNumberDisplay,			
			String chargeType, String chargeperiod,
			String Currency, String listprice, String ContractPrice,
			String chargeType1, String chargeperiod1, String Currency1,
			String listprice1, String contractprice1,
			String ProductInformation1, String ProductInformation2, String CustomerContractCurrency,
			String CustomerBillingCurrency, String IGCContractCurrency, String IGCBillingCurrency,
			String CustomerBillingMethod, String IGCSettlementMethod,
			String TechnologyCustomerAccountLatin, String TechnologyCustomerAccountLocal,
			String BillingCustomerAccountLatin, String BillingCustomerAccountLocal,
			String ServiceStartDate, String ServiceStartDateTimeZone, 
			String ServiceEndDate, String ServiceEndDateTimeZone,
			String BillingStartDate, String BillingStartDateTimeZone, 
			String BillingEndDate, String BillingEndDateTimeZone,			
			String DiscountPercentage1,String DiscountPercentage2,String Discountprice1,
			String Discountprice2,String IGCSettlementPrice1,String IGCSettlementPrice2,
			String DocumentDescription,String DocumentDescription1,
			String DocumentDescription2, String DocumentDescription3, String DocumentName) throws Throwable{
		DataSet++;
		
		
		//If found DataToRun = "N" for data set then execution will be skipped for that data set.
		if(!TestDataToRun[DataSet].equalsIgnoreCase("Y")){
			Add_Log.info(TestCaseName+" : DataToRun = N for data set line "+(DataSet+1)+" So skipping Its execution.");
			//If DataToRun = "N", Set Testskip=true.
			Testskip=true;
			throw new SkipException("DataToRun for row number "+DataSet+" Is No Or Blank. So Skipping Its Execution.");
		}
		
		CreateContract cc = new CreateContract();
		EditContract ec = new EditContract();
		Workflow wc=new Workflow();
		
		try{
			if(Test_CaseId.equalsIgnoreCase("RTC_089/RTC_090")){				
				cc.ViewContractNavigation();
				cc.SelectContractId(ContractId);					
				try{
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					driver.findElement(By.xpath(".//*[@id='list_subpanel_gc_lineitem_documents_rowid']/table/thead/tr[1]/td/div/ul/li/a")).click();
					ec.CreateDocumentModule(DocumentDescription1);
					System.out.println("Verification of user can attach documents on new version of contract passed");
					Reporter.log("Verification of user can attach documents on new version of contract passed");					
				} catch(Exception e){
					e.printStackTrace();
					System.out.println("Verification of user can attach documents on new version of contract failed \t " + e);
					Reporter.log("Verification of user can attach documents on new version of contract failed \t " +e);
					Assert.fail();
				}
				try{
					driver.findElement(By.id("edit_button")).click();
					Thread.sleep(10000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					Select billScheme = new Select(driver.findElement(By.id("billing_type")));
					billScheme.selectByVisibleText("Separate");
					clearSecondLineItems();
					driver.findElement(By.id("add_invoice_subgroup_2")).click();
					String windowHandleBefore = driver.getWindowHandle();				
					for (String handle : driver.getWindowHandles()) {
						driver.switchTo().window(handle);
					}
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					driver.findElement(By.id("invoice_groupname")).sendKeys("Test");
					driver.findElement(By.id("invoice_groupdesc")).sendKeys("Test");
					driver.findElement(By.xpath("html/body/div[9]/div[3]/div/button")).click();
					driver.switchTo().window(windowHandleBefore);
					fillingSecondContractLineItems(ContractId, ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone, 
							ContractEndDate, ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin, SalesRepNameLocal, 
							DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone, ExtnNumber, FaxNumber, MobileNumber, mailId, 
							ProductSpecification, ProductItem, ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3, 
							ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7, PaymentType, AccountHolderName, 
							DepositType, BankCode, BranchCode, AccountNumber, AccountNumberDisplay, chargeType, chargeperiod, Currency, listprice, 
							ContractPrice, chargeType1, chargeperiod1, Currency1, listprice1, contractprice1, ProductInformation1, ProductInformation2, 
							CustomerContractCurrency, CustomerBillingCurrency, IGCContractCurrency, IGCBillingCurrency, CustomerBillingMethod, 
							IGCSettlementMethod, TechnologyCustomerAccountLatin, TechnologyCustomerAccountLocal, BillingCustomerAccountLatin, 
							BillingCustomerAccountLocal, ServiceStartDate, ServiceStartDateTimeZone, ServiceEndDate, ServiceEndDateTimeZone, 
							BillingStartDate, BillingStartDateTimeZone, BillingEndDate, BillingEndDateTimeZone, DiscountPercentage1, 
							DiscountPercentage2, Discountprice1, Discountprice2, IGCSettlementPrice1, IGCSettlementPrice2, DocumentDescription, 
							DocumentDescription1, DocumentDescription2, DocumentDescription3, DocumentName);
					Reporter.log("VersionUp contract LineItems are Editable");
					System.out.println("VersionUp contract LineItems are Editable");
				} catch(Exception e){					
					System.out.println("VersionUp contract LineItems are not Editable \t" + e);
					Reporter.log("VersionUp contract LineItems are not Editable \t" + e);
					Assert.fail();
				}				
			} else if(Test_CaseId.equalsIgnoreCase("RTC_091")){
				ec.ViewContractNavigationPop();
				ec.selectContractIdFrom2ndRow(ContractId);
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));	
				if(driver.findElements(By.id("send_termination_approval")).size() != 0){
					System.out.println("Contract termination when a Contract has more than one Version passed");
					Reporter.log("Contract termination when a Contract has more than one Version passed");
					//driver.findElement(By.id("send_termination_approval")).click();
				} else {
					System.out.println("Contract termination when a Contract has more than one Version failed");
					Reporter.log("Contract termination when a Contract has more than one Version failed");
					Assert.fail();
				}
			}
		} catch(Exception e){
			e.printStackTrace();
			Reporter.log("TestCase Not Found"+e);
			Assert.fail("TeaserCase Error \t" + e);
			
		}
		
		
		
		if(Test_CaseId.equalsIgnoreCase("RTC_Workflow_040")){	
			try
			{
			cc.CreateContractNavigation();
			ContractDataFillVersionUp(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
					ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
					SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
					ExtnNumber, FaxNumber, MobileNumber, mailId, ProductItem, ProductSpecification,
					ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
					ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
					PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
					ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
			addNewLineItem1Versionup();
			driver.findElement(By.id("SAVE_FOOTER")).click();
			Thread.sleep(3000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			globalContractId = driver.findElement(By.id("global_contract_id")).getText();
			System.out.println("Globl Contract Id::" + globalContractId);
			String ContractVersion=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
			System.out.println("Printing Contract Version before version up" +ContractVersion);
			cc.ViewContractNavigation();
			// EW.ViewContract(ContractName);
			cc.SelectContractId(globalContractId);
			wc.Validation_SendForApproval();
			// Thread.sleep(10000);
			// System.out.println("Workflow_001-passed");
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel1(globalContractId);
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel2(globalContractId);
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel3(globalContractId);
			Thread.sleep(5000);
			wc.ActivateVersionUp(globalContractId);

		
			wc.logOut();
			driver.quit();
			init();
			log.Run();
			
			cc.ViewContractNavigation();
			// EW.ViewContract(ContractName);
			cc.SelectContractId(globalContractId);
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));


			driver.findElement(By.id("modification_contract")).click();
			//driver.findElement(By.xpath(".//*[@id='modification_contract']")).click();
			Thread.sleep(5000);
			driver.findElement(By.id("SAVE_FOOTER")).click();
			Thread.sleep(5000);
			
			
			String ContractVersion_2=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
			System.out.println("Printing Contract Version after Version Up" +ContractVersion_2);
			if(ContractVersion_2.equals("2.0"))
					{
			wc.Validation_SendForApproval();
			// Thread.sleep(10000);
			// System.out.println("Workflow_001-passed");
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel1_VersionUp(globalContractId);
			String ContractVersion_1=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
			System.out.println("Printing Contract Version after approval level 1" +ContractVersion_1);
			
	
				Reporter.log("Approval level 1 success for version up contract:" +ContractVersion_1);
				Assert.assertTrue("Approval level 1 success for version up contract", true);
				System.out.println("Approval level 1 success for version up contract \t");

		
			
			wc.logOut();
			//driver.quit();
			
			System.out.println("VersionUp contract approved level 1 \t");
			
			
			

			Assert.assertTrue("RTC_Workflow_040-passed", TestCasePass);
			} 
			else{
				System.out.println("VersionUp not success");	
				Assert.fail("VersionUp not success");
				
			}
			} 
		catch(Exception e){					
				System.out.println("VersionUp contract level1 approval not done \t" + e);
				Reporter.log("VersionUp contract level1 approval not done  \t" + e);
								
		} 
			
		}
		
		if(Test_CaseId.equalsIgnoreCase("RTC_Workflow_041")){	
			try
			{
			cc.CreateContractNavigation();
			ContractDataFillVersionUp(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
					ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
					SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
					ExtnNumber, FaxNumber, MobileNumber, mailId, ProductItem, ProductSpecification,
					ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
					ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
					PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
					ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
			addNewLineItem1Versionup();
			driver.findElement(By.id("SAVE_FOOTER")).click();
			Thread.sleep(3000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			globalContractId = driver.findElement(By.id("global_contract_id")).getText();
			System.out.println("Globl Contract Id::" + globalContractId);
			String ContractVersion=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
			System.out.println("Printing Contract Version before Versionup" +ContractVersion);
			cc.ViewContractNavigation();
			// EW.ViewContract(ContractName);
			cc.SelectContractId(globalContractId);
			wc.Validation_SendForApproval();
			// Thread.sleep(10000);
			// System.out.println("Workflow_001-passed");
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel1(globalContractId);
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel2(globalContractId);
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel3(globalContractId);
			Thread.sleep(5000);
			wc.ActivateVersionUp(globalContractId);

		
			wc.logOut();
			driver.quit();
			init();
			log.Run();
			
			cc.ViewContractNavigation();
			// EW.ViewContract(ContractName);
			cc.SelectContractId(globalContractId);
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));


			driver.findElement(By.id("modification_contract")).click();
			//driver.findElement(By.xpath(".//*[@id='modification_contract']")).click();
			Thread.sleep(5000);
			driver.findElement(By.id("SAVE_FOOTER")).click();
			
			String ContractVersion_2=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
			System.out.println("Printing Contract Version after Version Up" +ContractVersion_2);
			if(ContractVersion_2.equals("2.0"))
					{
					
			Thread.sleep(5000);
			wc.Validation_SendForApproval();
			// Thread.sleep(10000);
			// System.out.println("Workflow_001-passed");
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_RejectLevel1_VersionUp(globalContractId);
			String ContractVersion_1=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
			System.out.println("Printing Contract Version after level 1 reject" +ContractVersion_1);
			

				Reporter.log("Reject level 1 success for version up contract:" +ContractVersion_1);
				Assert.assertTrue("Reject level 1 success for version up contract", true);
				
				System.out.println("Reject level 1 success for version up contract \t");

		
			
			wc.logOut();
			//driver.quit();
			
			System.out.println("VersionUp contract Rejected level 1 \t");
			
			
			

			Assert.assertTrue("RTC_Workflow_041-passed", TestCasePass);
					}
			else{
				System.out.println("VersionUp not success");	
				Assert.fail("VersionUp not success");
				
			}
			} 
		catch(Exception e){					
				System.out.println("VersionUp contract level1 reject not done \t" + e);
				Reporter.log("VersionUp contract level1 reject not done  \t" + e);
				Assert.fail("VersionUp contract level1 reject not done");				
		} 
		}
		
		
		
	if(Test_CaseId.equalsIgnoreCase("RTC_Workflow_042")){	
		try
		{
		cc.CreateContractNavigation();
		ContractDataFillVersionUp(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
				ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
				SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
				ExtnNumber, FaxNumber, MobileNumber, mailId, ProductItem, ProductSpecification,
				ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
				ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
				PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
				ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
		addNewLineItem1Versionup();
		driver.findElement(By.id("SAVE_FOOTER")).click();
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		globalContractId = driver.findElement(By.id("global_contract_id")).getText();
		System.out.println("Globl Contract Id::" + globalContractId);
		String ContractVersion=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
		System.out.println("Printing Contract Version before Versionup" +ContractVersion);
		cc.ViewContractNavigation();
		// EW.ViewContract(ContractName);
		cc.SelectContractId(globalContractId);
		wc.Validation_SendForApproval();
		// Thread.sleep(10000);
		// System.out.println("Workflow_001-passed");
		wc.logOut();
		driver.quit();
		init();
		wc.Validation_ApprovalLevel1(globalContractId);
		wc.logOut();
		driver.quit();
		init();
		wc.Validation_ApprovalLevel2(globalContractId);
		wc.logOut();
		driver.quit();
		init();
		wc.Validation_ApprovalLevel3(globalContractId);
		Thread.sleep(5000);
		wc.ActivateVersionUp(globalContractId);

	
		wc.logOut();
		driver.quit();
		init();
		log.Run();
		
		cc.ViewContractNavigation();
		// EW.ViewContract(ContractName);
		cc.SelectContractId(globalContractId);
		Thread.sleep(5000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));


		driver.findElement(By.id("modification_contract")).click();
		//driver.findElement(By.xpath(".//*[@id='modification_contract']")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("SAVE_FOOTER")).click();
		
		String ContractVersion_2=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
		System.out.println("Printing Contract Version after Version Up" +ContractVersion_2);
		if(ContractVersion_2.equals("2.0"))
				{
				
		Thread.sleep(5000);
		wc.Validation_SendForApproval();
		// Thread.sleep(10000);
		// System.out.println("Workflow_001-passed");
		wc.logOut();
		driver.quit();
		init();
	
		wc.Validation_ApprovalLevel1_VersionUp(globalContractId);
		wc.logOut();
		driver.quit();
		init();
		wc.Validation_ApprovalLevel2_VersionUp(globalContractId);
		wc.logOut();
		driver.quit();
		init();
		wc.Validation_ApprovalLevel3_VersionUp(globalContractId);
		Thread.sleep(5000);
		
		String ContractVersion_1=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
		System.out.println("Printing Contract Version after level 3 Approval" +ContractVersion_1);
		

			Reporter.log("Approval level 3 success for version up contract:" +ContractVersion_1);
			Assert.assertTrue("Approval level 3 success for version up contract", true);
			
			System.out.println("Approval level 3 success for version up contract \t");

	
		
		wc.logOut();
		//driver.quit();
		
		System.out.println("VersionUp contract approved level 3 \t");
		
		
		

		Assert.assertTrue("RTC_Workflow_042-passed", TestCasePass);

	
		wc.logOut();
	
		
		
		
			
		
		
		
		
		
		
		

				}
		else{
			System.out.println("VersionUp not success");	
			Assert.fail("VersionUp not success");
			
		}
		} 
	catch(Exception e){					
			System.out.println("VersionUp contract level3 approval not done \t" + e);
			Reporter.log("VersionUp contract level3 approval not done  \t" + e);
			Assert.fail("VersionUp contract level3 approval not done");				
	} 
	}
	
	if(Test_CaseId.equalsIgnoreCase("RTC_Workflow_044")){	
		try
		{
		cc.CreateContractNavigation();
		ContractDataFillVersionUp(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
				ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
				SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
				ExtnNumber, FaxNumber, MobileNumber, mailId, ProductItem, ProductSpecification,
				ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
				ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
				PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
				ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
		addNewLineItem1Versionup();
		driver.findElement(By.id("SAVE_FOOTER")).click();
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		globalContractId = driver.findElement(By.id("global_contract_id")).getText();
		System.out.println("Globl Contract Id::" + globalContractId);
		String ContractVersion=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
		System.out.println("Printing Contract Version before Versionup" +ContractVersion);
		cc.ViewContractNavigation();
		// EW.ViewContract(ContractName);
		cc.SelectContractId(globalContractId);
		wc.Validation_SendForApproval();
		// Thread.sleep(10000);
		// System.out.println("Workflow_001-passed");
		wc.logOut();
		driver.quit();
		init();
		wc.Validation_ApprovalLevel1(globalContractId);
		wc.logOut();
		driver.quit();
		init();
		wc.Validation_ApprovalLevel2(globalContractId);
		wc.logOut();
		driver.quit();
		init();
		wc.Validation_ApprovalLevel3(globalContractId);
		Thread.sleep(5000);
		wc.ActivateVersionUp(globalContractId);

	
		wc.logOut();
		driver.quit();
		init();
		log.Run();
		
		cc.ViewContractNavigation();
		// EW.ViewContract(ContractName);
		cc.SelectContractId(globalContractId);
		Thread.sleep(5000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));


		driver.findElement(By.id("modification_contract")).click();
		//driver.findElement(By.xpath(".//*[@id='modification_contract']")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("SAVE_FOOTER")).click();
		
		String ContractVersion_2=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
		System.out.println("Printing Contract Version after Version Up" +ContractVersion_2);
		if(ContractVersion_2.equals("2.0"))
				{
				
		Thread.sleep(5000);
		wc.Validation_SendForApproval();
		// Thread.sleep(10000);
		// System.out.println("Workflow_001-passed");
		wc.logOut();
		driver.quit();
		init();
	
		wc.Validation_ApprovalLevel1_VersionUp(globalContractId);
		wc.logOut();
		driver.quit();
		init();
		wc.Validation_ApprovalLevel2_VersionUp(globalContractId);
		wc.logOut();
		driver.quit();
		init();
		wc.Validation_ApprovalLevel3_VersionUp(globalContractId);
		Thread.sleep(5000);
	
		String ContractVersion_1=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
		System.out.println("Printing Contract Version after level 3 Approval" +ContractVersion_1);
		

			Reporter.log("Approval level 3 success for version up contract:" +ContractVersion_1);
			Assert.assertTrue("Approval level 3 success for version up contract", true);
			
			System.out.println("Approval level 3 success for version up contract \t");

	
		

		//driver.quit();
		
		System.out.println("VersionUp contract approved level 3 \t");
		
		Thread.sleep(5000);
		wc.ActivateVersionUp(globalContractId);

		System.out.println("VersionUp contract activated 3 \t");
		wc.logOut();
		driver.quit();
	
	init();
		log.Run();
		
		cc.ViewContractNavigation();
		// EW.ViewContract(ContractName);
		SelectContractIdRow1(globalContractId);
		cc.ViewContractNavigation();
		SelectContractIdRow2(globalContractId);
		Thread.sleep(5000);



		Assert.assertTrue("RTC_Workflow_043-passed", TestCasePass);

	
		wc.logOut();
	
		
		
		
			
		
		
		
		
		
		
		

				}
		else{
			System.out.println("VersionUp not success");	
			Assert.fail("VersionUp not success");
			
		}
		} 
	catch(Exception e){					
			System.out.println("VersionUp contract activation not done \t" + e);
			Reporter.log("VersionUp contract activation not done  \t" + e);
			Assert.fail("VersionUp contract activation not done");				
	} 
	}
	
	
	else if (Test_CaseId.equalsIgnoreCase("RTC_Workflow_038")) {
		try {
			
	cc.CreateContractNavigation();
				ContractDataFillVersionUp(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductItem, ProductSpecification,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				addNewLineItem1Versionup();
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				String ContractVersion=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
				System.out.println("Printing Contract Version before Versionup" +ContractVersion);
				cc.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				cc.SelectContractId(globalContractId);
				wc.Validation_SendForApproval();
				// Thread.sleep(10000);
				// System.out.println("Workflow_001-passed");
				wc.logOut();
				driver.quit();
				init();
				wc.Validation_ApprovalLevel1(globalContractId);
				wc.logOut();
				driver.quit();
				init();
				wc.Validation_ApprovalLevel2(globalContractId);
				wc.logOut();
				driver.quit();
				init();
				wc.Validation_ApprovalLevel3(globalContractId);
				Thread.sleep(5000);
				wc.ActivateVersionUp(globalContractId);

			
				wc.logOut();
				driver.quit();
				init();
				log.Run();
				
				cc.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				cc.SelectContractId(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));


				driver.findElement(By.id("modification_contract")).click();
				//driver.findElement(By.xpath(".//*[@id='modification_contract']")).click();
				Thread.sleep(5000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				
				String ContractVersion_2=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
				System.out.println("Printing Contract Version after Version Up" +ContractVersion_2);
				if(ContractVersion_2.equals("2.0"))
						{
						
				Thread.sleep(5000);
				wc.Validation_SendForApproval();
				// Thread.sleep(10000);
				// System.out.println("Workflow_001-passed");
				wc.logOut();
				driver.quit();
			
				init();
			
				wc.Validation_ApprovalLevel1_VersionUp(globalContractId);
				
		
				
				
				
				
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[2]")).click();
				

				Thread.sleep(5000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				Thread.sleep(10000);
				if (driver.getPageSource().contains("Approved by 1st level")) {
				
					if (driver.getPageSource().contains("GMOne 1st approver , GMOne SO Team")) {
					
						if (driver.getPageSource().contains("Accept")) {
							System.out.println(
									"Approver Level1 approved details updated in log along with workflow action");
							Reporter.log("Approver Level1 approved details updated in log along with workflow action");
						} else {
							System.out.println(
									"Approver Level1 approved details not updated in log along with workflow action");
							Assert.fail(
									"Approver Level1 approved details not updated in log along with workflow action");
						}
					}
				}
				driver.close();
				driver.switchTo().window(winHandleBefore);
				wc.logOut();
				driver.quit();
				init();
				wc.Validation_RejectLevel2_VersionUp(globalContractId);
			
		
			
			
			
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[4]")).click();
				Thread.sleep(10000);
				String winHandleBefore1 = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				Thread.sleep(10000);
				if (driver.getPageSource().contains("Rejected by 2nd level")) {
					if (driver.getPageSource().contains("GCMT , GMOne SO Team , GMOne 2nd approver")) {
						if (driver.getPageSource().contains("Reject")) {
							System.out.println(
									"Approver Level2 rejected details updated in log along with workflow action");
							Reporter.log("Approver Level2 rejected details updated in log along with workflow action");
						} else {
							System.out.println(
									"Approver Level2 rejected details not updated in log along with workflow action");
							Assert.fail(
									"Approver Level2 rejected details not updated in log along with workflow action");
						}
					}
				}
				driver.close();
				driver.switchTo().window(winHandleBefore1);
				driver.switchTo().defaultContent();

				Thread.sleep(3000);
				wc.logOut();
			driver.quit();
				//
			init();
				log.Run();
				cc.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				cc.SelectContractId_VersionUp(globalContractId);
				wc.Validation_SendForApproval();
				// Thread.sleep(10000);
				// System.out.println("Workflow_001-passed");
				wc.logOut();
				driver.quit();
				init();
				wc.Validation_ApprovalLevel1_VersionUp(globalContractId);
				wc.logOut();
				driver.quit();
				init();
				wc.Validation_ApprovalLevel2_VersionUp(globalContractId);
				wc.logOut();
				driver.quit();
								
				
				init();
				wc.Validation_SendForWithdrawLevel3_VersionUp(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[4]")).click();
				Thread.sleep(10000);
				String winHandleBefore3 = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				if (driver.getPageSource().contains("Withdrawn")) {
					if (driver.getPageSource().contains("GCMT , GMOne 3rd approver , GMOne SO Team ")) {
						if (driver.getPageSource().contains("Withdraw")) {
							System.out.println(
									"Approver Level3 withdraw details updated in log along with workflow action");
							Reporter.log("Approver Level3 withdraw details updated in log along with workflow action");
						} else {
							System.out.println(
									"Approver Level3 withdraw details not updated in log along with workflow action");
							Assert.fail(
									"Approver Level3 withdraw details not updated in log along with workflow action");
						}
					}
				}
				driver.close();
				driver.switchTo().window(winHandleBefore3);
				
	}
				else{
					System.out.println("VersionUp not success");	
					Assert.fail("VersionUp not success");
					
				}

			
		}

		catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	
	else if (Test_CaseId.equalsIgnoreCase("RTC_Contract_098")) {
		try {
			
			cc.CreateContractNavigation();
				ContractDataFillVersionUp(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductItem, ProductSpecification,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				addNewLineItem1Versionup();
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				globalContractId = driver.findElement(By.id("global_contract_id")).getText();
				System.out.println("Globl Contract Id::" + globalContractId);
				String ContractVersion=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
				System.out.println("Printing Contract Version before Versionup" +ContractVersion);
				cc.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				cc.SelectContractId(globalContractId);
				wc.Validation_SendForApproval();
				// Thread.sleep(10000);
				// System.out.println("Workflow_001-passed");
				wc.Validation_WithdrawNormalUser();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[4]")).click();
				Thread.sleep(10000);
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				if (driver.getPageSource().contains("Withdrawn")) {
					if (driver.getPageSource().contains("User14@emeriocorp.com")) {
						if (driver.getPageSource().contains("Withdraw")) {
							System.out.println(
									"Contract Withdraw details updated in log along with workflow action");
							Reporter.log("Contract Withdraw details updated in log along with workflow action");
						} else {
							System.out.println(
									"Contract Withdraw details not updated in log along with workflow action");
							Assert.fail(
									"Contract Withdraw details not updated in log along with workflow action");
						}
					}
				}
				driver.close();
				driver.switchTo().window(winHandleBefore);
				wc.Validation_SendForApproval();
				wc.logOut();
				driver.quit();
			
				
				init();
				wc.Validation_ApprovalLevel1(globalContractId);
				wc.logOut();
				driver.quit();
		
				init();
				log.Run();
				wc.Validation_WithdrawNormalUserLevel1(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[4]")).click();
				Thread.sleep(10000);
				String winHandleBeforenew = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				if (driver.getPageSource().contains("Withdrawn")) {
					if (driver.getPageSource().contains("User14@emeriocorp.com")) {
						if (driver.getPageSource().contains("Withdraw")) {
							System.out.println(
									"Contract Withdraw details updated in log along with workflow action");
							Reporter.log("Contract Withdraw details updated in log along with workflow action");
						} else {
							System.out.println(
									"Contract Withdraw details updated in log along with workflow action");
							Assert.fail(
									"Contract Withdraw details updated in log along with workflow action");
						}
					}
				}
				driver.close();
				driver.switchTo().window(winHandleBeforenew);
		
			
				//cc.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				//cc.SelectContractId(globalContractId);
				wc.Validation_SendForApproval();
				wc.logOut();
				driver.quit();
			

				init();
				wc.Validation_ApprovalLevel1(globalContractId);
				wc.logOut();
				driver.quit();
				
							
				
				init();
				wc.Validation_ApprovalLevel2(globalContractId);
								wc.logOut();
				driver.quit();
				
				
	
				
				init();
				log.Run();
				wc.Validation_WithdrawNormalUserLevel2(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[4]")).click();
				Thread.sleep(10000);
				String winHandleBefore1 = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				if (driver.getPageSource().contains("Withdrawn")) {
					if (driver.getPageSource().contains("User14@emeriocorp.com")) {
						if (driver.getPageSource().contains("Withdraw")) {
							System.out.println(
									"Contract Withdraw details updated in log along with workflow action");
							Reporter.log("Contract Withdraw details updated in log along with workflow action");
						} else {
							System.out.println(
									"Contract Withdraw details updated in log along with workflow action");
							Assert.fail(
									"Contract Withdraw details updated in log along with workflow action");
						}
					}
				}
				driver.close();
				driver.switchTo().window(winHandleBefore1);
				wc.Validation_SendForApproval();
				wc.logOut();
				driver.quit();
				init();
				wc.Validation_ApprovalLevel1(globalContractId);
				Thread.sleep(5000);
				wc.logOut();
				driver.quit();
				init();
				wc.Validation_ApprovalLevel2(globalContractId);
				Thread.sleep(5000);
				wc.logOut();
				driver.quit();
							
				init();
				wc.Validation_ApprovalLevel3(globalContractId);
				Thread.sleep(5000);
				wc.logOut();
				driver.quit();
				init();
				log.Run();
				cc.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				cc.SelectContractId(globalContractId);
				Thread.sleep(5000);
				
				if (driver.findElements(By.id("withdrawapprovalrequest"))
						.isEmpty()) {
					// THEN CLICK ON THE SUBMIT BUTTON
					System.out.println("Withdraw Approval Request button not displayed for approved contracts");
				} else {
					// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
					System.out.println("Withdraw Approval Request button displayed");
				}
				
				wc.logOut();
				driver.quit();
				
				init();
				wc.Activate(globalContractId);
				wc.logOut();
				driver.quit();	
				init();
				log.Run();
								
				
				cc.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				cc.SelectContractId(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));


				if (driver.findElements(By.id("withdrawapprovalrequest"))
						.isEmpty()) {
					// THEN CLICK ON THE SUBMIT BUTTON
					System.out.println("Withdraw Approval Request button not displayed for activated contracts");
				} else {
					// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
					System.out.println("Withdraw Approval Request button displayed");
				}
				driver.findElement(By.id("modification_contract")).click();
				//driver.findElement(By.xpath(".//*[@id='modification_contract']")).click();
				Thread.sleep(5000);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				
				String ContractVersion_2=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
				System.out.println("Printing Contract Version after Version Up" +ContractVersion_2);
				if(ContractVersion_2.equals("2.0"))
						{
						
				Thread.sleep(5000);
				wc.Validation_SendForApproval();
				// Thread.sleep(10000);
				// System.out.println("Workflow_001-passed");
				wc.Validation_WithdrawNormalUser();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[4]")).click();
				Thread.sleep(10000);
				String winHandleBefore2 = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				if (driver.getPageSource().contains("Withdrawn")) {
					if (driver.getPageSource().contains("User14@emeriocorp.com")) {
						if (driver.getPageSource().contains("Withdraw")) {
							System.out.println(
									"Contract Withdraw details updated in log along with workflow action");
							Reporter.log("Contract Withdraw details updated in log along with workflow action");
						} else {
							System.out.println(
									"Contract Withdraw details not updated in log along with workflow action");
							Assert.fail(
									"Contract Withdraw details not updated in log along with workflow action");
						}
					}
				}
				driver.close();
				driver.switchTo().window(winHandleBefore2);
				wc.Validation_SendForApproval();
				wc.logOut();
				driver.quit();
				
				
				
				init();
				wc.Validation_ApprovalLevel1_VersionUp(globalContractId);
				wc.logOut();
				driver.quit();
				
				init();
				log.Run();
				wc.Validation_WithdrawNormalUserLevel1_VersionUp(globalContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[4]")).click();
				Thread.sleep(10000);
				String winHandleBefore3 = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				if (driver.getPageSource().contains("Withdrawn")) {
					if (driver.getPageSource().contains("User14@emeriocorp.com")) {
						if (driver.getPageSource().contains("Withdraw")) {
							System.out.println(
									"Contract Withdraw details updated in log along with workflow action)");
							Reporter.log("Contract Withdraw details updated in log along with workflow action");
						} else {
							System.out.println(
									"Contract Withdraw details updated in log along with workflow action");
							Assert.fail(
									"Contract Withdraw details updated in log along with workflow action");
						}
					}
				}
				driver.close();
				driver.switchTo().window(winHandleBefore3);
				wc.Validation_SendForApproval();
				wc.logOut();
				driver.quit();
			

				init();
				wc.Validation_ApprovalLevel1_VersionUp(globalContractId);
				wc.logOut();
				driver.quit();
				
							
				
				init();
				wc.Validation_ApprovalLevel2_VersionUp(globalContractId);
								wc.logOut();
				driver.quit();
				
				
	
				
				init();
				log.Run();
				wc.Validation_WithdrawNormalUserLevel2_VersionUp(globalContractId);
		
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[4]")).click();
				Thread.sleep(10000);
				String winHandleBefore4 = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				if (driver.getPageSource().contains("Withdrawn")) {
					if (driver.getPageSource().contains("User14@emeriocorp.com")) {
						if (driver.getPageSource().contains("Withdraw")) {
							System.out.println(
									"Contract Withdraw details updated in log along with workflow action");
							Reporter.log("Contract Withdraw details updated in log along with workflow action");
						} else {
							System.out.println(
									"Contract Withdraw details updated in log along with workflow action");
							Assert.fail(
									"Contract Withdraw details updated in log along with workflow action");
						}
					}
				}
				driver.close();
				driver.switchTo().window(winHandleBefore4);
				wc.Validation_SendForApproval();
				wc.logOut();
				driver.quit();
				init();
				wc.Validation_ApprovalLevel1_VersionUp(globalContractId);
				Thread.sleep(5000);
				wc.logOut();
				driver.quit();
				init();
				wc.Validation_ApprovalLevel2_VersionUp(globalContractId);
				Thread.sleep(5000);
				wc.logOut();
				driver.quit();
							
				init();
				wc.Validation_ApprovalLevel3_VersionUp(globalContractId);
				Thread.sleep(5000);
				wc.logOut();
				driver.quit();
				init();
				log.Run();
				cc.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				cc.SelectContractId_VersionUp(globalContractId);
				Thread.sleep(5000);
				
				if (driver.findElements(By.id("withdrawapprovalrequest"))
						.isEmpty()) {
					// THEN CLICK ON THE SUBMIT BUTTON
					System.out.println("Withdraw Approval Request button not displayed for approved contracts");
				} else {
					// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
					System.out.println("Withdraw Approval Request button displayed");
				}
				
				wc.logOut();
				driver.quit();
				
				init();
				wc.Activate_VersionUp(globalContractId);
				wc.logOut();
				driver.quit();	
				
				init();
				log.Run();
				cc.ViewContractNavigation();
				// EW.ViewContract(ContractName);
				cc.SelectContractId_VersionUp(globalContractId);
				Thread.sleep(5000);
				
				if (driver.findElements(By.id("withdrawapprovalrequest"))
						.isEmpty()) {
					// THEN CLICK ON THE SUBMIT BUTTON
					System.out.println("Withdraw Approval Request button not displayed for activated contracts");
				} else {
					// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
					System.out.println("Withdraw Approval Request button displayed");
				}
				
				wc.logOut();
				driver.quit();
		}
				else{
					System.out.println("VersionUp not success");	
					Assert.fail("VersionUp not success");
					
				}
			
			
			
		}

		catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	
	else if (Test_CaseId.equalsIgnoreCase("RTC_Contract_107")) {
		try {
			cc.CreateContractNavigation();
			ContractDataFillVersionUp(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
					ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
					SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
					ExtnNumber, FaxNumber, MobileNumber, mailId, ProductItem, ProductSpecification,
					ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
					ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
					PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
					ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
			addNewLineItem1Versionup();
			driver.findElement(By.id("SAVE_FOOTER")).click();
			Thread.sleep(3000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			globalContractId = driver.findElement(By.id("global_contract_id")).getText();
			System.out.println("Globl Contract Id::" + globalContractId);
			String ContractVersion=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
			System.out.println("Printing Contract Version before Versionup" +ContractVersion);
			cc.ViewContractNavigation();
			// EW.ViewContract(ContractName);
			cc.SelectContractId(globalContractId);
			wc.Validation_SendForApproval();
			// Thread.sleep(10000);
			// System.out.println("Workflow_001-passed");
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel1(globalContractId);
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel2(globalContractId);
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel3(globalContractId);
			Thread.sleep(5000);
			wc.ActivateVersionUp(globalContractId);

		
			wc.logOut();
			driver.quit();
			init();
			log.Run();
			
			cc.ViewContractNavigation();
			// EW.ViewContract(ContractName);
			cc.SelectContractId(globalContractId);
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));


			driver.findElement(By.id("modification_contract")).click();
			//driver.findElement(By.xpath(".//*[@id='modification_contract']")).click();
			Thread.sleep(5000);
			driver.findElement(By.id("SAVE_FOOTER")).click();
			
			String ContractVersion_2=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
			System.out.println("Printing Contract Version after Version Up" +ContractVersion_2);
			if(ContractVersion_2.equals("2.0"))
			{
				
				Thread.sleep(5000);
		
			wc.Validation_SendForApproval();
		wc.Validation_WithdrawNormalUser();
			// Thread.sleep(10000);
			
			
			
	
			
			
			
		Thread.sleep(5000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]/div/div/input[4]")).click();
		
		

		Thread.sleep(10000);
		String winHandleBefore = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.switchTo().defaultContent();
		if (driver.getPageSource().contains("Contract submitted to workflow")) {
			if (driver.getPageSource().contains("User14@emeriocorp.com")) {
				if (driver.getPageSource().contains("Withdraw")) {
					System.out.println(
							"Contract Withdraw details updated in log along with workflow action(Contract Status-Sent For Approval)");
					Reporter.log("Contract Withdraw details updated in log along with workflow action(Contract Status-Sent For Approval)");
				} else {
					System.out.println(
							"Contract Withdraw details not updated in log along with workflow action(Contract Status-Sent For Approval)");
					Assert.fail(
							"Contract Withdraw details not updated in log along with workflow action(Contract Status-Sent For Approval)");
				}
			}
		}
		driver.close();
		driver.switchTo().window(winHandleBefore);
			wc.logOut();
			driver.quit();
	
			}
			else{
				System.out.println("VersionUp not success");	
				Assert.fail("VersionUp not success");
				
				}
		}

		catch (Exception e) {

			e.printStackTrace();
		}
	} 
	
	
	else if (Test_CaseId.equalsIgnoreCase("RTC_Contract_113")) {
		try {
			
			cc.CreateContractNavigation();
			ContractDataFillVersionUp(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
					ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
					SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
					ExtnNumber, FaxNumber, MobileNumber, mailId, ProductItem, ProductSpecification,
					ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
					ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
					PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
					ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
			addNewLineItem1Versionup();
			addNewLineItem2Versionup();
			addNewLineItem3Versionup();
	
			driver.findElement(By.id("SAVE_FOOTER")).click();
			Thread.sleep(3000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			globalContractId = driver.findElement(By.id("global_contract_id")).getText();
			System.out.println("Globl Contract Id::" + globalContractId);
			String ContractVersion=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
			System.out.println("Printing Contract Version before Versionup" +ContractVersion);
			cc.ViewContractNavigation();
			// EW.ViewContract(ContractName);
			cc.SelectContractId(globalContractId);
			wc.Validation_SendForApproval();
			// Thread.sleep(10000);
			// System.out.println("Workflow_001-passed");
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel1(globalContractId);
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel2(globalContractId);
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel3(globalContractId);
			Thread.sleep(5000);
			wc.ActivateVersionUp(globalContractId);

		
			wc.logOut();
			driver.quit();
			init();
			log.Run();
			
			cc.ViewContractNavigation();
			// EW.ViewContract(ContractName);
			cc.SelectContractId(globalContractId);
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

			Thread.sleep(2000);
			//Click Open all Button
						driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
						Thread.sleep(2000);	
						
						//To get line item IDs from Line Items
						String s1=	driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[1]/strong")).getText();
						System.out.println("s1" +s1);
						
						String s2=	driver.findElement(By.xpath(".//*[@id='dropdown-li-2']/div[1]/strong")).getText();
						System.out.println("s2" +s2);
						String s3=	driver.findElement(By.xpath(".//*[@id='dropdown-li-3']/div[1]/strong")).getText();
						System.out.println("s3" +s3);
						String s4=	driver.findElement(By.xpath(".//*[@id='dropdown-li-4']/div[1]/strong")).getText();
					    System.out.println("s4" +s4);
					  //Getting line items id in list			    
					    
					    ArrayList<String> al1= new ArrayList<String>();
				          al1.add(s1);
				          al1.add(s2);
				          al1.add(s3);
				          al1.add(s4);
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
						Thread.sleep(2000);	
			
			
			
			
			
			
			

			driver.findElement(By.id("modification_contract")).click();
			//driver.findElement(By.xpath(".//*[@id='modification_contract']")).click();
			Thread.sleep(5000);
			
			
			
			
			
			
			
		//Remove 2nd Line Item
			//driver.findElement(By.xpath(".//*[@id='btn_li_remove_2']")).click();
			driver.findElement(By.xpath(".//*[@id='terminate_line_item_2']")).click();
			
			
			Thread.sleep(5000);
			
			//Remove 3rd Line Item
			//driver.findElement(By.xpath(".//*[@id='btn_li_remove_3']")).click();
			
			driver.findElement(By.xpath(".//*[@id='terminate_line_item_3']")).click();
			Thread.sleep(5000);
			

			driver.findElement(By.id("SAVE_FOOTER")).click();
			Thread.sleep(5000);
	
		           
	          
	          
	          
			
			String ContractVersion_2=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
			System.out.println("Printing Contract Version after Version Up" +ContractVersion_2);
			if(ContractVersion_2.equals("2.0"))
					{
				
			//Verify Line Item Sequence	
				
				
				
				
				
				
				
			Thread.sleep(5000);
			wc.Validation_SendForApproval();
			// Thread.sleep(10000);
			// System.out.println("Workflow_001-passed");
			wc.logOut();
			driver.quit();
		
			init();
			wc.Validation_ApprovalLevel1_VersionUp(globalContractId);
			wc.logOut();
		    driver.quit();
		
			init();
			wc.Validation_ApprovalLevel2_VersionUp(globalContractId);
			wc.logOut();
		    driver.quit();
		
		    
			init();
			wc.Validation_ApprovalLevel3_VersionUp(globalContractId);
			Thread.sleep(5000);
			wc.ActivateVersionUp(globalContractId);

Thread.sleep(5000);

	
	//Get line item IDs after removing line items
			
			String w1=driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[1]/strong")).getText();
			String w2=driver.findElement(By.xpath(".//*[@id='dropdown-li-2']/div[1]/strong")).getText();
			
			
			System.out.println("w1" +w1);
			System.out.println("w2" +w2);
	//Getting line items id in list		
			ArrayList<String> al2= new ArrayList<String>();
	          al2.add(w1);
	          al2.add(w2);	
			
	          System.out.println("List of line Item Ids before removing two line items          : "+ al1);
	          System.out.println("List of line Item Ids after removing two line items           : " + al2);
	          List<String> base = new ArrayList<String>(al1);
	          base.retainAll(al2);
	          System.out.println("Common Line Items Ids: " + base);
	          base = new ArrayList<String>(al1);
	          base.removeAll(al2);
	          System.out.println("Subtraction List 1-List 2: " + base);
	          base = new ArrayList<String>(al2);
	          base.removeAll(al1);
	          System.out.println("Subtraction  List 2 -List 1: " + base);
	          base = new ArrayList<String>(al1);
	          base.addAll(al2);
	          System.out.println("Union List 1,List 2 : " + base);
	          

	          
	          if(!(al2.contains(s2)))
		   	   {
		        	  System.out.println("not");
		        	  
		   		   System.out.println("Removed item sequence id not present" +s2);
		   		   Assert.assertTrue("Removed item sequence id  not present", true);
		   		   
		   		}
		   	   else
		   	   {
		   	  	  System.out.println("not");
		   		   System.out.println("Removed item sequence id   present" +s2);
		   	  	Assert.fail();
		   		}
		             
	          
	          if(!(al2.contains(s3)))
		   	   {
		        	  System.out.println("not");
		        	  
		   		   System.out.println("Removed item sequence id not present" +s3);
		   		   Assert.assertTrue("Removed item sequence id  not present", true);
		   		   
		   		}
		   	   else
		   	   {
		   	  	  System.out.println("not");
		   		   System.out.println("Removed item sequence id present" +s3);
		   		Assert.fail();
		   		}





			wc.logOut();
		    driver.quit();
		    

			init();
			log.Run();
			
			cc.ViewContractNavigation();
			// EW.ViewContract(ContractName);
			cc.SelectContractId_VersionUp(globalContractId);
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));


			driver.findElement(By.id("modification_contract")).click();
			//driver.findElement(By.xpath(".//*[@id='modification_contract']")).click();
			Thread.sleep(5000);
		    
			addNewLineItem2Versionup();
			Thread.sleep(2000);
			
			
			driver.findElement(By.id("SAVE_FOOTER")).click();
			
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(2000);


			

			
			
			String ContractVersion_3=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
			System.out.println("Printing Contract Version after Version Up" +ContractVersion_3);
			if(ContractVersion_3.equals("3.0"))
					{
					
			Thread.sleep(5000);
			
			//Verify Line Item Sequence


wc.Validation_SendForApproval();
			// Thread.sleep(10000);
			// System.out.println("Workflow_001-passed");
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel1_VersionUp2(globalContractId);
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel2_VersionUp2(globalContractId);
			wc.logOut();
			driver.quit();
			init();
			wc.Validation_ApprovalLevel3_VersionUp2(globalContractId);
			Thread.sleep(5000);
			wc.ActivateVersionUp2(globalContractId);

		










String x1=driver.findElement(By.xpath(".//*[@id='dropdown-li-1']/div[1]/strong")).getText();
			String x2=driver.findElement(By.xpath(".//*[@id='dropdown-li-2']/div[1]/strong")).getText();
			String x3=driver.findElement(By.xpath(".//*[@id='dropdown-li-3']/div[1]/strong")).getText();
	
	

			System.out.println("x1" +x1);
			System.out.println("x2" +x2);
			System.out.println("x3" +x3);

	//Getting line items id in list		
			ArrayList<String> al3= new ArrayList<String>();
	          al3.add(x1);
	          al3.add(x2);	
	          al3.add(x3);	
			
	          
	          System.out.println("List of line Item Ids before removing two line items          : " + al1);
	          System.out.println("List of line Item Ids after removing two line items           : " + al2);
	          System.out.println("List of line Item Ids after adding a line item                : " + al3);
	          
	          
	          List<String> base1 = new ArrayList<String>(al1);
	          base.retainAll(al3);
	          System.out.println("Common Line Items Ids: " + base1);
	          base = new ArrayList<String>(al1);
	          base.removeAll(al3);
	          System.out.println("Subtraction List 1-List 2: " + base1);
	          base = new ArrayList<String>(al3);
	          base.removeAll(al1);
	          System.out.println("Subtraction  List 2 -List 1: " + base1);
	          base = new ArrayList<String>(al1);
	          base.addAll(al3);
	          System.out.println("Union List 1,List 2 : " + base1);
	          
	          
	          List<String> base2 = new ArrayList<String>(al1);
	          base.retainAll(al3);
	          System.out.println("Common Line Items Ids: " + base2);
	          base = new ArrayList<String>(al2);
	          base.removeAll(al3);
	          System.out.println("Subtraction List 1-List 2: " + base2);
	          base = new ArrayList<String>(al3);
	          base.removeAll(al2);
	          System.out.println("Subtraction  List 2 -List 1: " + base2);
	          base = new ArrayList<String>(al2);
	          base.addAll(al3);
	          System.out.println("Union List 1,List 2 : " + base2);
	          
	          
	          
	          
	          
	          
	          
	          
	          

	               
	          
	          
       if(!(al1.contains(x3)))
   	   {
        	  System.out.println("not");
        	  
   		   System.out.println("new item sequence id not  existing in Version 1 Contract" +x3);
   		   Assert.assertTrue("new item sequence id not  existing in Version 1 Contract", true);
   		   
   		}
   	   else
   	   {
   	  	  System.out.println("not");
   		   System.out.println("new item sequence id already existing in Version 1 Contract" +x3);
   	  	Assert.fail();
   		}
             
      
      if(!(al2.contains(x3)))
   	   {
        	 // System.out.println("not");
        	  
     		   System.out.println("new item sequence id not  existing in Version 2 Contract" +x3);
	   		   Assert.assertTrue("new item sequence id not  existing in Version 2 Contract", true);
   		}
   	   else
   	   {
   	  	  //System.out.println("not");
   		   System.out.println("new item sequence id already existing in Version 2 Contract" +x3);
   		Assert.fail();
   		}
              
	          
	          
	          
	          

			wc.logOut();
			//driver.quit();
          
	          
	          
	          
	          
	          
	          
	          
			
					}
			else{
				System.out.println("Level 1 VersionUp not success");	
				Assert.fail("Level 1 VersionUp not success");
				
			}

			
					}
			
			else{
				System.out.println("Level 2 VersionUp not success");	
				Assert.fail("Level 2 VersionUp not success");
				
			}
			
		}

		catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	
}
		
		
	public void SelectContractIdRow1(String ContractId) throws Exception {
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		if(driver.findElements(By.id("basic_search_link")).size() != 0){
			driver.findElement(By.id("basic_search_link")).click();
		}
		driver.findElement(By.id("global_contract_id_basic")).clear();
		driver.findElement(By.id("name_basic")).clear();
		driver.findElement(By.id("global_contract_id_basic")).sendKeys(ContractId);
		//srn.getscreenshot();
		driver.findElement(By.id("search_form_submit")).click();
		// driver.findElement(By.xpath("//table/tbody/tr[3]/td[4]/b/a")).click();
		Thread.sleep(10000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		//srn.getscreenshot();
		driver.findElement(By.xpath(".//*[@id='MassUpdate']/table/tbody/tr[3]/td[4]/b/a")).click();
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		String ContractVersion_2=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
		System.out.println("Printing Contract Version" +ContractVersion_2);
		WebElement TxtBoxContent014 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus7 = TxtBoxContent014.getText();

		WebElement TxtBoxContent015 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status7 = TxtBoxContent015.getText();

		System.out.println("Printing contract status" + tempContractStatus7);
		System.out.println("Printing workflow flag status" + status7);
	Reporter.log("Printing contract status" + tempContractStatus7);
	Reporter.log("Printing workflow flag status" + status7);
		Assert.assertTrue("Contract status and contract version validated", true);
		
	}	
		
		
	public void SelectContractIdRow2(String ContractId) throws Exception {
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		if(driver.findElements(By.id("basic_search_link")).size() != 0){
			driver.findElement(By.id("basic_search_link")).click();
		}
		driver.findElement(By.id("global_contract_id_basic")).clear();
		driver.findElement(By.id("name_basic")).clear();
		driver.findElement(By.id("global_contract_id_basic")).sendKeys(ContractId);
		//srn.getscreenshot();
		driver.findElement(By.id("search_form_submit")).click();
		// driver.findElement(By.xpath("//table/tbody/tr[3]/td[4]/b/a")).click();
		Thread.sleep(10000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		//srn.getscreenshot();
		driver.findElement(By.xpath(".//*[@id='MassUpdate']/table/tbody/tr[4]/td[4]/b/a")).click();
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		String ContractVersion_2=driver.findElement(By.xpath(".//*[@id='contract_version']")).getText();
		System.out.println("Printing Contract Version" +ContractVersion_2);
		WebElement TxtBoxContent014 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String tempContractStatus7 = TxtBoxContent014.getText();

		WebElement TxtBoxContent015 = driver.findElement(By.xpath("//div[1]/table/tbody/tr[5]/td[2]"));
		String status7 = TxtBoxContent015.getText();

		System.out.println("Printing contract status" + tempContractStatus7);
		System.out.println("Printing workflow flag status" + status7);
	Reporter.log("Printing contract status" + tempContractStatus7);
	Reporter.log("Printing workflow flag status" + status7);
		Assert.assertTrue("Contract status and contract version validated", true);
	}		
		
		
		
		

	
	public void clearSecondLineItems(){
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));		
		Select invoiceSubGroup = new Select(driver.findElement(By.id("li_invoice_subgroup_2")));
		invoiceSubGroup.selectByVisibleText("");
		Select custContCurr = new Select(driver.findElement(By.id("cust_contract_currency_2")));
		custContCurr.selectByVisibleText("");
		Select custBillCurr = new Select(driver.findElement(By.id("cust_billing_currency_2")));
		custBillCurr.selectByVisibleText("");
		Select igcContCurr = new Select(driver.findElement(By.id("igc_contract_currency_2")));
		igcContCurr.selectByVisibleText("");
		Select igcBillCurr = new Select(driver.findElement(By.id("igc_billing_currency_2")));
		igcBillCurr.selectByVisibleText("");	
		Select custBillMethod = new Select(driver.findElement(By.id("customer_billing_method_2")));
		custBillMethod.selectByVisibleText("");
		Select igcSettleMethod = new Select(driver.findElement(By.id("igc_settlement_method_2")));
		igcSettleMethod.selectByVisibleText("");		
		driver.findElement(By.id("service_start_date_2")).clear();
		driver.findElement(By.id("service_end_date_2")).clear();
		driver.findElement(By.id("billing_start_date_2")).clear();
		driver.findElement(By.id("billing_end_date_2")).clear();		
		Select serStartDateHr = new Select(driver.findElement(By.id("service_start_date_hours_2")));
		serStartDateHr.selectByVisibleText("00");
		Select serStartDateMi = new Select(driver.findElement(By.id("service_start_date_minutes_2")));
		serStartDateMi.selectByVisibleText("00");
		Select serStartDateSec = new Select(driver.findElement(By.id("service_start_date_seconds_2")));
		serStartDateSec.selectByVisibleText("00");
		Select serStartDateTZ = new Select(driver.findElement(By.id("service_start_date_timezone_2")));
		serStartDateTZ.selectByVisibleText("");
		Select serEndDateHr = new Select(driver.findElement(By.id("service_end_date_hours_2")));
		serEndDateHr.selectByVisibleText("00");
		Select serEndDateMi = new Select(driver.findElement(By.id("service_end_date_minutes_2")));
		serEndDateMi.selectByVisibleText("00");
		Select serEndDateSec = new Select(driver.findElement(By.id("service_end_date_seconds_2")));
		serEndDateSec.selectByVisibleText("00");
		Select serEndDateTZ = new Select(driver.findElement(By.id("service_end_date_timezone_2")));
		serEndDateTZ.selectByVisibleText("");
		Select billStartDateHr = new Select(driver.findElement(By.id("billing_start_date_hours_2")));
		billStartDateHr.selectByVisibleText("00");
		Select billStartDateMi = new Select(driver.findElement(By.id("billing_start_date_minutes_2")));
		billStartDateMi.selectByVisibleText("00");
		Select billStartDateSec = new Select(driver.findElement(By.id("billing_start_date_seconds_2")));
		billStartDateSec.selectByVisibleText("00");
		Select billStartDateTZ = new Select(driver.findElement(By.id("billing_start_date_timezone_2")));
		billStartDateTZ.selectByVisibleText("");
		Select billEndDateHr = new Select(driver.findElement(By.id("billing_end_date_hours_2")));
		billEndDateHr.selectByVisibleText("00");
		Select billEndDateMi = new Select(driver.findElement(By.id("billing_end_date_minutes_2")));
		billEndDateMi.selectByVisibleText("00");
		Select billEndDateSec = new Select(driver.findElement(By.id("billing_end_date_seconds_2")));
		billEndDateSec.selectByVisibleText("00");		
		Select billEndDateTZ = new Select(driver.findElement(By.id("billing_end_date_timezone_2")));
		billEndDateTZ.selectByVisibleText("");
		Select payType = new Select(driver.findElement(By.id("payment_type_2")));
		payType.selectByVisibleText("");
		driver.findElement(By.id("factory_reference_no_2")).clear();
		driver.findElement(By.id("description_2")).clear();
		Select pricingRule = new Select(driver.findElement(By.id("product_config_2_563")));
		pricingRule.selectByVisibleText("");
	}
	
	public void fillingSecondContractLineItems(String ContractId, String ContractName,
			String ContractDesc, String ProductsID, String ProductOffering,
			String ContractStartDateTimeZone, String ContractEndDate, String  ContractEndDateTimeZone,
			String FactoryReferenceNumber, String SalesChannelCode,
			String SalesRepNameLatin, String SalesRepNameLocal,
			String DivisionLatin, String DivisionLocal, String TittleLatin,
			String TittleLocal, String Telephone, String ExtnNumber,
			String FaxNumber, String MobileNumber, String mailId,
			String ProductSpecification, String ProductItem, 
			String ProductConfiguration, String ProductConfiguration1,
			String ProductConfiguration2, String ProductConfiguration3,
			String ProductConfiguration4, String ProductConfiguration5,
			String ProductConfiguration6, String ProductConfiguration7,
			String PaymentType, String AccountHolderName, String DepositType,
			String BankCode, String BranchCode, String AccountNumber,
			String AccountNumberDisplay,			
			String chargeType, String chargeperiod,
			String Currency, String listprice, String ContractPrice,
			String chargeType1, String chargeperiod1, String Currency1,
			String listprice1, String contractprice1,
			String ProductInformation1, String ProductInformation2, String CustomerContractCurrency,
			String CustomerBillingCurrency, String IGCContractCurrency, String IGCBillingCurrency,
			String CustomerBillingMethod, String IGCSettlementMethod,
			String TechnologyCustomerAccountLatin, String TechnologyCustomerAccountLocal,
			String BillingCustomerAccountLatin, String BillingCustomerAccountLocal, 
			String ServiceStartDate, String ServiceStartDateTimeZone, 
			String ServiceEndDate, String ServiceEndDateTimeZone,
			String BillingStartDate, String BillingStartDateTimeZone, 
			String BillingEndDate, String BillingEndDateTimeZone,			
			String DiscountPercentage1,String DiscountPercentage2,String Discountprice1,
			String Discountprice2,String IGCSettlementPrice1,String IGCSettlementPrice2,
			String DocumentDescription,String DocumentDescription1,
			String DocumentDescription2, String DocumentDescription3, String DocumentName) throws InterruptedException{		
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		Select custContCurr = new Select(driver.findElement(By.id("cust_contract_currency_2")));
		custContCurr.selectByValue(CustomerContractCurrency);
		Select custBillCurr = new Select(driver.findElement(By.id("cust_billing_currency_2")));
		custBillCurr.selectByValue(CustomerBillingCurrency);
		Select igcContCurr = new Select(driver.findElement(By.id("igc_contract_currency_2")));
		igcContCurr.selectByValue(IGCContractCurrency);
		Select igcBillCurr = new Select(driver.findElement(By.id("igc_billing_currency_2")));
		igcBillCurr.selectByValue(IGCBillingCurrency);	
		Select custBillMethod = new Select(driver.findElement(By.id("customer_billing_method_2")));
		custBillMethod.selectByValue(CustomerBillingMethod);
		Select igcSettleMethod = new Select(driver.findElement(By.id("igc_settlement_method_2")));
		igcSettleMethod.selectByValue(IGCSettlementMethod);		
		
		driver.findElement(By.id("btn_tech_account_name_2")).click();
		String windowHandleBefore = driver.getWindowHandle();				
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.switchTo().defaultContent();
		driver.findElement(By.id("search_form_clear")).click();
		driver.findElement(By.id("last_name_advanced")).sendKeys(TechnologyCustomerAccountLatin);
		driver.findElement(By.id("name_2_c_advanced")).sendKeys(TechnologyCustomerAccountLocal);
		driver.findElement(By.id("search_form_submit")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("html/body/table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(windowHandleBefore);
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("btn_bill_account_name_2")).click();
		String windowHandleBefore1 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.switchTo().defaultContent();
		driver.findElement(By.id("search_form_clear")).click();
		driver.findElement(By.id("last_name_advanced")).sendKeys(BillingCustomerAccountLatin);
		driver.findElement(By.id("name_2_c_advanced")).sendKeys(BillingCustomerAccountLocal);
		driver.findElement(By.id("search_form_submit")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("html/body/table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(windowHandleBefore1);
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("service_start_date_2")).sendKeys(ServiceStartDate);
		driver.findElement(By.id("service_end_date_2")).sendKeys(ServiceEndDate);
		driver.findElement(By.id("billing_start_date_2")).sendKeys(BillingStartDate);
		driver.findElement(By.id("billing_end_date_2")).sendKeys(BillingEndDate);		
		Select serStartDateHr = new Select(driver.findElement(By.id("service_start_date_hours_2")));
		serStartDateHr.selectByVisibleText("00");
		Select serStartDateMi = new Select(driver.findElement(By.id("service_start_date_minutes_2")));
		serStartDateMi.selectByVisibleText("00");
		Select serStartDateSec = new Select(driver.findElement(By.id("service_start_date_seconds_2")));
		serStartDateSec.selectByVisibleText("00");
		//Select serStartDateTZ = new Select(driver.findElement(By.id("service_start_date_timezone_2")));
		//serStartDateTZ.selectByVisibleText(ServiceStartDateTimeZone);
		driver.findElement(By.id("service_start_date_timezone_2")).sendKeys(ServiceStartDateTimeZone);
		Select serEndDateHr = new Select(driver.findElement(By.id("service_end_date_hours_2")));
		serEndDateHr.selectByVisibleText("00");
		Select serEndDateMi = new Select(driver.findElement(By.id("service_end_date_minutes_2")));
		serEndDateMi.selectByVisibleText("00");
		Select serEndDateSec = new Select(driver.findElement(By.id("service_end_date_seconds_2")));
		serEndDateSec.selectByVisibleText("00");
		//Select serEndDateTZ = new Select(driver.findElement(By.id("service_end_date_timezone_2")));
		//serEndDateTZ.selectByVisibleText(ServiceEndDateTimeZone);
		driver.findElement(By.id("service_end_date_timezone_2")).sendKeys(ServiceEndDateTimeZone);
		Select billStartDateHr = new Select(driver.findElement(By.id("billing_start_date_hours_2")));
		billStartDateHr.selectByVisibleText("00");
		Select billStartDateMi = new Select(driver.findElement(By.id("billing_start_date_minutes_2")));
		billStartDateMi.selectByVisibleText("00");
		Select billStartDateSec = new Select(driver.findElement(By.id("billing_start_date_seconds_2")));
		billStartDateSec.selectByVisibleText("00");
		//Select billStartDateTZ = new Select(driver.findElement(By.id("billing_start_date_timezone_2")));
		//billStartDateTZ.selectByVisibleText(BillingStartDateTimeZone);
		driver.findElement(By.id("billing_start_date_timezone_2")).sendKeys(BillingStartDateTimeZone);
		Select billEndDateHr = new Select(driver.findElement(By.id("billing_end_date_hours_2")));
		billEndDateHr.selectByVisibleText("00");
		Select billEndDateMi = new Select(driver.findElement(By.id("billing_end_date_minutes_2")));
		billEndDateMi.selectByVisibleText("00");
		Select billEndDateSec = new Select(driver.findElement(By.id("billing_end_date_seconds_2")));
		billEndDateSec.selectByVisibleText("00");		
		//Select billEndDateTZ = new Select(driver.findElement(By.id("billing_end_date_timezone_2")));
		//billEndDateTZ.selectByVisibleText(BillingEndDateTimeZone);
		driver.findElement(By.id("billing_end_date_timezone_2")).sendKeys(BillingEndDateTimeZone);
		Select payType = new Select(driver.findElement(By.id("payment_type_2")));
		payType.selectByVisibleText(PaymentType);
		if (PaymentType.equalsIgnoreCase("General: Direct Deposit")) {
			driver.findElement(By.id("dd_bank_code_2")).sendKeys("Bank007");
			driver.findElement(By.id("dd_branch_code_2")).sendKeys("dd_branch_code_2");
			Select oSelect1 = new Select(driver.findElement(By.id("dd_deposit_type_2")));
			oSelect1.selectByVisibleText("Tax Payment (General)");
			driver.findElement(By.id("dd_account_no_2")).sendKeys("12345678963636363");
			driver.findElement(By.id("dd_person_name_1_2")).sendKeys("Atul Dandin");
			driver.findElement(By.id("dd_person_name_2_2")).sendKeys("LocalAtul Dandin");
			driver.findElement(By.id("dd_payee_tel_no_2")).sendKeys("1234567890");
			driver.findElement(By.id("dd_payee_email_address_2")).sendKeys("dummy@email.com");
			driver.findElement(By.id("dd_remarks_2")).sendKeys("Remarks");
		} else if (PaymentType.equalsIgnoreCase("Account Transfer")) {
			driver.findElement(By.id("at_account_no_2")).sendKeys(AccountNumber);
			driver.findElement(By.id("at_name_2")).sendKeys(AccountHolderName);
			driver.findElement(By.id("at_bank_code_2")).sendKeys(BankCode);
			driver.findElement(By.id("at_branch_code_2")).sendKeys(BranchCode);
			Select oSelect1 = new Select(driver.findElement(By.id("at_deposit_type_2")));
			oSelect1.selectByVisibleText(DepositType);
			Select oSelect2 = new Select(driver.findElement(By.id("at_account_no_display_2")));
			oSelect2.selectByVisibleText(AccountNumberDisplay);
		} else if (PaymentType.equalsIgnoreCase("Postal Transfer")) {
			driver.findElement(By.id("pt_postal_passbook_2")).sendKeys("123");
			driver.findElement(By.id("pt_name_2")).sendKeys("Atul Dandin");
			driver.findElement(By.id("pt_postal_passbook_mark_2")).sendKeys("Remarks");
			Select oSelect2 = new Select(driver.findElement(By.id("pt_postal_passbook_no_display_2")));
			oSelect2.selectByVisibleText("Yes");
		} else if (PaymentType.equalsIgnoreCase("Credit Card")) {	
			driver.findElement(By.id("cc_credit_card_no_2")).sendKeys("12345678963636363");
			driver.findElement(By.id("cc_expiration_date_2")).sendKeys("10/13");
		} else {
			Reporter.log("Invalid Paymemt Type");
			Assert.fail();
		}
		driver.findElement(By.id("factory_reference_no_2")).sendKeys("1122");
		driver.findElement(By.id("description_2")).sendKeys("Test Edit VersionUp LineItems");
		Select pricingRule = new Select(driver.findElement(By.id("product_config_2_563")));
		pricingRule.selectByVisibleText("UNIT");
		driver.findElement(By.id("pp_discount_2_160461")).clear();
		driver.findElement(By.id("pp_discount_2_160461")).sendKeys(Discountprice1);
		driver.findElement(By.id("pp_discount_2_160462")).clear();
		driver.findElement(By.id("pp_discount_2_160462")).sendKeys(Discountprice2);
		driver.findElement(By.id("pp_igc_settlement_price_2_160461")).clear();
		driver.findElement(By.id("pp_igc_settlement_price_2_160461")).sendKeys(IGCSettlementPrice1);
		driver.findElement(By.id("pp_igc_settlement_price_2_160462")).clear();
		driver.findElement(By.id("pp_igc_settlement_price_2_160462")).sendKeys(IGCSettlementPrice2);		
	}
	
	
	public void addNewLineItem1Versionup(){
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("add_line_item")).click();
		//String billingScheme = driver.findElement(By.id("billing_type")).getText();
		Select prodSpecification = new Select(driver.findElement(By.id("product_specification_2")));
		prodSpecification.selectByVisibleText("Managed Windows Server - on ECL2.0 (Add-on Optional)");		
		
		//driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		
	}
	
	public void addNewLineItem2Versionup(){
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("add_line_item")).click();
		//String billingScheme = driver.findElement(By.id("billing_type")).getText();
		Select prodSpecification = new Select(driver.findElement(By.id("product_specification_3")));
		prodSpecification.selectByVisibleText("Managed Linux - on ECL2.0 (Add-on Optional)");		
		
		//driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		
	}
	
	
	public void addNewLineItem3Versionup(){
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("add_line_item")).click();
		//String billingScheme = driver.findElement(By.id("billing_type")).getText();
		Select prodSpecification = new Select(driver.findElement(By.id("product_specification_4")));
		prodSpecification.selectByVisibleText("Managed Oracle Basic - on ECL2.0 (Add-on Optional)");		
		
		//driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		
	}
	public void ContractDataFillVersionUp(String ContractName, String ContractDesc,
			String ProductsID, String ProductOffering,
			String ContractStartDateTimeZone, String  ContractEndDateTimeZone,
			String FactoryReferenceNumber, 
			String SalesChannelCode,
			String SalesRepNameLatin, String SalesRepNameLocal,
			String DivisionLatin, String DivisionLocal, String TittleLatin,
			String TittleLocal, String Telephone, String ExtnNumber,
			String FaxNumber, String MobileNumber, String mailId,
			String ProductItem, String ProductSpecification,
			String ProductConfiguration, String ProductConfiguration1,
			String ProductConfiguration2, String ProductConfiguration3,
			String ProductConfiguration4, String ProductConfiguration5,
			String ProductConfiguration6, String ProductConfiguration7,
			String PaymentType, String ProductInformation1,
			String ProductInformation2,
			String ServiceStartDateTimeZone, String ServiceEndDateTimeZone,
			String BillingStartDateTimeZone, String BillingEndDateTimeZone) throws Exception {
		Thread.sleep(10000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		// WaitForElementPresent(".//*[@id='name']", 30000);
		driver.findElement(By.id("name")).sendKeys(ContractName);
		driver.findElement(By.id("description")).sendKeys(ContractDesc);
		driver.findElement(By.id("btn_pm_products_gc_contracts_1_name"))
				.click();
		String winHandleBefore = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("products_id_advanced")).clear();
		driver.findElement(By.id("products_id_advanced")).sendKeys(ProductsID);
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore);

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		Select cSelect1 = new Select(driver.findElement(By.id("contract_type")));
		cSelect1.selectByValue("GMSA");

		Select cSelect2 = new Select(driver.findElement(By
				.id("contract_scheme")));
		cSelect2.selectByValue("GSC");

		Select cSelect3 = new Select(driver.findElement(By
				.id("product_offering")));		
		cSelect3.selectByVisibleText(ProductOffering);


		DateTimeFormatter format1 = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDateTime now1 = LocalDateTime.now();
		driver.findElement(By.id("contract_startdate")).sendKeys(
				now1.format(format1));


		
		driver.findElement(By.id("btn_accounts_gc_contracts_1_name")).click();
		String winHandleBefore1 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}
		 // Added by deep - start /
		  driver.findElement(By.id("search_form_clear"));
		  driver.findElement(By.id("common_customer_id_c_advanced")).sendKeys("C0321124868");
		  driver.findElement(By.id("search_form_submit")).click();
		  driver.findElement(By.xpath("html/body/table[4]/tbody/tr[3]/td[3]/a")).click();
		 //Added by deep - End /
		//driver.findElement(By.xpath("html/body/table[4]/tbody/tr[5]/td[3]/a")).click();
		driver.switchTo().window(winHandleBefore1);
		
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("btn_contacts_gc_contracts_1_name")).click();
		String winHandleBefore2 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {

			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("last_name_advanced")).clear();
		driver.findElement(By.id("last_name_advanced"))
				.sendKeys(
						"Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("search_form_submit")).click();
		driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore2);

		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		
		/* Added by deep for Selection Contract TimeZone - Start */
		driver.findElement(By.id("gc_timezone_id_c")).sendKeys(ContractStartDateTimeZone);
		driver.findElement(By.id("gc_timezone_id1_c")).sendKeys(ContractEndDateTimeZone);
		/* Added by deep for Selection Contract TimeZone - End */
		
		driver.findElement(By.id("factory_reference_no")).sendKeys(
				FactoryReferenceNumber);
		driver.findElement(By.id("sales_channel_code")).sendKeys(
				SalesChannelCode);
		
		driver.findElement(By.id("sales_rep_name")).sendKeys(SalesRepNameLatin);
		driver.findElement(By.id("sales_rep_name_1")).sendKeys(
				SalesRepNameLocal);
		driver.findElement(By.id("division_1")).sendKeys(DivisionLatin);
		driver.findElement(By.id("division_2")).sendKeys(DivisionLocal);
		driver.findElement(By.id("title_1")).sendKeys(TittleLatin);
		driver.findElement(By.id("title_2")).sendKeys(TittleLocal);
		driver.findElement(By.id("tel_no")).sendKeys(Telephone);
		driver.findElement(By.id("ext_no")).sendKeys(ExtnNumber);
		driver.findElement(By.id("fax_no")).sendKeys(FaxNumber);
		driver.findElement(By.id("mob_no")).sendKeys(MobileNumber);
		driver.findElement(By.id("email1")).sendKeys(mailId);
		
		Thread.sleep(3000);
		Select prodSpecification = new Select(driver.findElement(By.id("product_specification_1")));
	prodSpecification.selectByVisibleText("GMOne ECL2.0 Option Base Specification (Base)");	
		//prodSpecification.selectByVisibleText(ProductSpecification);	
		/*Select cSelect5 = new Select(driver.findElement(By
				.id("product_specification_1")));
		// cSelect5.selectByValue("333");		
		cSelect5.selectByVisibleText(ProductSpecification);*/


		
		Select cSelect6 = new Select(driver.findElement(By
				.id("cust_contract_currency_1")));
		cSelect6.selectByValue("JPY");

		Select cSelect7 = new Select(driver.findElement(By
				.id("cust_billing_currency_1")));
		cSelect7.selectByValue("AFN");

		Select cSelect8 = new Select(driver.findElement(By
				.id("igc_contract_currency_1")));
		cSelect8.selectByValue("AFN");

		Select cSelect9 = new Select(driver.findElement(By
				.id("igc_billing_currency_1")));
		cSelect9.selectByValue("AFN");

		Select cSelect10 = new Select(driver.findElement(By
				.id("customer_billing_method_1")));
		cSelect10.selectByValue("eCSS_Billing");

		Select cSelect11 = new Select(driver.findElement(By
				.id("igc_settlement_method_1")));
		// cSelect11.selectByValue("Quarterly_Settlement");
		cSelect11.selectByVisibleText("No Settlement per Contract");

		driver.findElement(By.id("description_1")).sendKeys("Testing");
		
		driver.findElement(By.id("btn_tech_account_name_1")).click();

		String winHandleBefore5 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("last_name_advanced")).clear();
		driver.findElement(By.id("last_name_advanced"))
				.sendKeys(
						"Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("search_form_submit")).click();

		driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore5);

		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("btn_bill_account_name_1")).click();
		String winHandleBefore6 = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("last_name_advanced")).clear();
		// driver.findElement(By.id("last_name_advanced")).sendKeys("test 3/2/2016");
		driver.findElement(By.id("last_name_advanced"))
				.sendKeys(
						"Update Japan Contact Person  Name Latin 0123456789012345678901234567890123456789012345678901234567890123456789");
		driver.findElement(By.id("search_form_submit")).click();

		driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
		driver.switchTo().window(winHandleBefore6);
		
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime then = now.plusDays(10);
		driver.findElement(By.id("service_start_date_1")).sendKeys(
				now.format(format));
		driver.findElement(By.id("service_end_date_1")).sendKeys(
				then.format(format));
		driver.findElement(By.id("billing_start_date_1")).sendKeys(
				now.format(format));
		driver.findElement(By.id("billing_end_date_1")).sendKeys(
				then.format(format));
		

		driver.findElement(By.id("service_start_date_timezone_1")).sendKeys(
				ServiceStartDateTimeZone);
		driver.findElement(By.id("service_end_date_timezone_1"))
				.sendKeys(ServiceEndDateTimeZone);
		driver.findElement(By.id("billing_start_date_timezone_1")).sendKeys(
				BillingStartDateTimeZone);
		driver.findElement(By.id("billing_end_date_timezone_1"))
				.sendKeys(BillingEndDateTimeZone);
		
		
		if (PaymentType.equalsIgnoreCase("General: Direct Deposit")) {
			//driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Select oSelect = new Select(driver.findElement(By
					.id("payment_type_1")));

			oSelect.selectByValue("00");

			driver.findElement(By.id("dd_bank_code_1")).sendKeys("1234");
			driver.findElement(By.id("dd_branch_code_1")).sendKeys(
					"12");

			Select oSelect1 = new Select(driver.findElement(By
					.id("dd_deposit_type_1")));
			oSelect1.selectByVisibleText("Tax Payment (General)");

			driver.findElement(By.id("dd_account_no_1")).sendKeys(
					"1234567");
			driver.findElement(By.id("dd_person_name_1_1")).sendKeys(
					"ContactPerson Latin");
			driver.findElement(By.id("dd_person_name_2_1")).sendKeys(
					"ContactPerson Local");
			driver.findElement(By.id("dd_payee_tel_no_1")).sendKeys(
					"1234567890");
			driver.findElement(By.id("dd_payee_email_address_1")).sendKeys(
					"dummy@email.com");
			driver.findElement(By.id("dd_remarks_1")).sendKeys("Remarks");
		} else if (PaymentType.equalsIgnoreCase("Account Transfer")) {
			//driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			try{
				Select oSelect = new Select(driver.findElement(By
		
					.id("payment_type_1")));

			oSelect.selectByValue("10");

			driver.findElement(By.id("at_account_no_1")).sendKeys(
					"12345678963636363");
			driver.findElement(By.id("at_name_1")).sendKeys("Atul Dandin");
			driver.findElement(By.id("at_bank_code_1")).sendKeys("1234567890");
			driver.findElement(By.id("at_branch_code_1"))
					.sendKeys("1234567890");

			Select oSelect1 = new Select(driver.findElement(By
					.id("at_deposit_type_1")));
			oSelect1.selectByVisibleText("Ordinary Deposit");

			Select oSelect2 = new Select(driver.findElement(By
					.id("at_account_no_display_1")));
			oSelect2.selectByVisibleText("Yes");
          
			Reporter.log("RTC_Contract_016 Passed- User was able to select account type Account transfer");
			}catch(Exception e){
				Assert.fail("RTC_Contract_016 failed an error occured while selecting the payment type as account transfer"+e);
			}
		} else if (PaymentType.equalsIgnoreCase("Postal Transfer")) {
			//driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Select oSelect = new Select(driver.findElement(By
					.id("payment_type_1")));

			oSelect.selectByValue("30");

			// driver.findElement(By.id("pt_postal_passbook_1")).click();
			// driver.findElement(By.id("pt_postal_passbook_1")).clear();
			driver.findElement(By.id("pt_postal_passbook_1")).sendKeys("123");
			driver.findElement(By.id("pt_name_1")).sendKeys("Testing account ");
			driver.findElement(By.id("pt_postal_passbook_mark_1")).sendKeys(
					"Mark");

			Select oSelect2 = new Select(driver.findElement(By
					.id("pt_postal_passbook_no_display_1")));
			oSelect2.selectByVisibleText("Yes");

		} else if (PaymentType.equalsIgnoreCase("Credit Card")) {
			//driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Select oSelect = new Select(driver.findElement(By
					.id("payment_type_1")));

			oSelect.selectByValue("60");
			driver.findElement(By.id("cc_credit_card_no_1")).sendKeys(
					"12345678963636363");
			driver.findElement(By.id("cc_expiration_date_1")).sendKeys("10/13");

		} else {
			Reporter.log("Invalid Paymemt Type");
			Assert.fail();
		}
		driver.findElement(By.id("pi_role_value_1_876")).sendKeys("Product Information2");
		/*
		 * deep added for filling data based on ProductOffering and
		 * ProductSpecification
		 */
		if (ProductOffering.equals("Global Management One ECL2.0 Option")) {
			if (ProductSpecification
					.equals("GMOne ECL2.0 Option Base Specification (Base)")) {
				Select baseProductConfigSelect = new Select(
						driver.findElement(By.id("product_config_1_565")));
				baseProductConfigSelect.selectByVisibleText("1");
				driver.findElement(By.id("pi_role_value_1_876")).sendKeys(
						"Product Information2");
			} else if (ProductSpecification
					.equals("Managed Windows Server - on ECL2.0 (Add-on Optional)")) {
				Select optionalProductConfigSelect1 = new Select(
						driver.findElement(By.id("product_config_1_563")));
				optionalProductConfigSelect1.selectByVisibleText("UNIT");
			} else if (ProductSpecification
					.equals("Managed Linux - on ECL2.0 (Add-on Optional)")) {
				Select optionalProductConfigSelect2 = new Select(
						driver.findElement(By.id("product_config_1_563")));
				optionalProductConfigSelect2.selectByVisibleText("UNIT");
			} else if (ProductSpecification
					.equals("Managed Oracle Basic - on ECL2.0 (Add-on Optional)")) {
				Select optionalProductConfigSelect3 = new Select(
						driver.findElement(By.id("product_config_1_613")));
				optionalProductConfigSelect3.selectByVisibleText("UNIT");
			} else if (ProductSpecification
					.equals("Managed Oracle Advanced - on ECL2.0 (Add-on Optional)")) {
				Select optionalProductConfigSelect4 = new Select(
						driver.findElement(By.id("product_config_1_614")));
				optionalProductConfigSelect4.selectByVisibleText("UNIT");
			}
		} else if (ProductOffering.equals("Global Management One UNO Option")) {

		} else if (ProductOffering.equals("Enterprise Mail")) {

		}

		// driver.findElement(By.id("SAVE_FOOTER")).click();
		// srn.getscreenshot();

	}

	
	
	@DataProvider
	public Object[][] VersionUpData(){
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}
	
	/*@AfterTest
	public void close(){
		System.out.println("Inside after close");
		driver.close();				
	}*/
	

}
