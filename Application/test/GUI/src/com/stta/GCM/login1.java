package com.stta.GCM;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class login1 extends SuiteGcmBase {

	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[]=null;
	static boolean TestCasePass=true;
	static int DataSet=-1;	
	static boolean Testskip=false;
	static boolean Testfail=false;
	
	

	@BeforeTest
	public void checkCaseToRun() throws IOException{
		//Called init() function from SuiteBase class to Initialize .xls Files
		init();			
		}
	
	

	
	public void WFLevel1login(String WF1userName, String WF1password) throws Exception{
		
		loadWebBrowser();
		Screen srn = new Screen();
	    System.out.print("Inside Run");
		driver.get(Param.getProperty("siteURL"));
		System.out.println("After Site URL");	
		/* Added by Pradheep for brower wait Start */
		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);		
		/* Added by Pradheep for brower wait End */
		
		//driver.findElement(By.xpath(Object.getProperty("userName"))).clear();
		//driver.findElement(By.xpath(Object.getProperty("userName"))).sendKeys("user08@emeriocorp.com");
		//driver.findElement(By.xpath(Object.getProperty("userName"))).sendKeys("vijay.paudel@emeriocorp.com");
		//driver.findElement(By.xpath(Object.getProperty("Password"))).clear();
		//driver.findElement(By.xpath(Object.getProperty("Password"))).sendKeys("gcMtest2015");
		//driver.findElement(By.xpath(Object.getProperty("login"))).click();
		
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[1]/span/input")).clear();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[1]/span/input")).sendKeys(Param.getProperty("WF1userName"));
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[2]/span/input")).clear();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[2]/span/input")).sendKeys(Param.getProperty("WF1password"));	
		//scrn.getscreenshot();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[3]/span/a")).click();
		String winHandleBefore = driver.getWindowHandle();
	    for (String handle : driver.getWindowHandles()) {

	        driver.switchTo().window(handle);
	        }
	    /* Comment the below two lines if you are using IE */
	     driver.findElement(By.xpath("html/body/div[5]/div[3]/div/a[2]/span")).click();		
	     driver.switchTo().window(winHandleBefore);
		 //scrn.getscreenshot();
		
		}
	
	public void WFLevel2login(String WF5userName, String WF5password) throws Exception{
		
		loadWebBrowser();
		Screen srn = new Screen();
	    System.out.print("Inside Run");
		driver.get(Param.getProperty("siteURL"));
		System.out.println("After Site URL");	
		/* Added by Pradheep for brower wait Start */
		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);		
		/* Added by Pradheep for brower wait End */
		
		//driver.findElement(By.xpath(Object.getProperty("userName"))).clear();
		//driver.findElement(By.xpath(Object.getProperty("userName"))).sendKeys("user08@emeriocorp.com");
		//driver.findElement(By.xpath(Object.getProperty("userName"))).sendKeys("vijay.paudel@emeriocorp.com");
		//driver.findElement(By.xpath(Object.getProperty("Password"))).clear();
		//driver.findElement(By.xpath(Object.getProperty("Password"))).sendKeys("gcMtest2015");
		//driver.findElement(By.xpath(Object.getProperty("login"))).click();
		
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[1]/span/input")).clear();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[1]/span/input")).sendKeys(Param.getProperty("WF5userName"));
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[2]/span/input")).clear();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[2]/span/input")).sendKeys(Param.getProperty("WF5password"));	
		//scrn.getscreenshot();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[3]/span/a")).click();
		
		String winHandleBefore = driver.getWindowHandle();
	    for (String handle : driver.getWindowHandles()) {

	        driver.switchTo().window(handle);
	        }
	    /* Comment the below two lines if you are using IE */
	     driver.findElement(By.xpath("html/body/div[5]/div[3]/div/a[2]/span")).click();		
	     driver.switchTo().window(winHandleBefore);
		 //scrn.getscreenshot();
		
		}
	
	public void WFLevel3login(String WF8userName, String WF8password) throws Exception{
		
		loadWebBrowser();
		Screen srn = new Screen();
	    System.out.print("Inside Run");
		driver.get(Param.getProperty("siteURL"));
		System.out.println("After Site URL");	
		/* Added by Pradheep for brower wait Start */
		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);		
		/* Added by Pradheep for brower wait End */
		
		//driver.findElement(By.xpath(Object.getProperty("userName"))).clear();
		//driver.findElement(By.xpath(Object.getProperty("userName"))).sendKeys("user08@emeriocorp.com");
		//driver.findElement(By.xpath(Object.getProperty("userName"))).sendKeys("vijay.paudel@emeriocorp.com");
		//driver.findElement(By.xpath(Object.getProperty("Password"))).clear();
		//driver.findElement(By.xpath(Object.getProperty("Password"))).sendKeys("gcMtest2015");
		//driver.findElement(By.xpath(Object.getProperty("login"))).click();
		
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[1]/span/input")).clear();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[1]/span/input")).sendKeys(Param.getProperty("WF8userName"));
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[2]/span/input")).clear();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[2]/span/input")).sendKeys(Param.getProperty("WF8password"));	
		//scrn.getscreenshot();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[3]/span/a")).click();
		String winHandleBefore = driver.getWindowHandle();
			    for (String handle : driver.getWindowHandles()) {

			        driver.switchTo().window(handle);
			        }
			    /* Comment the below two lines if you are using IE */
			     driver.findElement(By.xpath("html/body/div[5]/div[3]/div/a[2]/span")).click();		
			     driver.switchTo().window(winHandleBefore);
				 //scrn.getscreenshot();
		
		}

public void Adminlogin(String AdminUserName, String AdminPassword) throws Exception{
		
		loadWebBrowser();
		Screen srn = new Screen();
	    System.out.print("Inside Run");
		driver.get(Param.getProperty("siteURL"));
		System.out.println("After Site URL");	
		/* Added by Pradheep for brower wait Start */
		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);		
		/* Added by Pradheep for brower wait End */
		
		//driver.findElement(By.xpath(Object.getProperty("userName"))).clear();
		//driver.findElement(By.xpath(Object.getProperty("userName"))).sendKeys("user08@emeriocorp.com");
		//driver.findElement(By.xpath(Object.getProperty("userName"))).sendKeys("vijay.paudel@emeriocorp.com");
		//driver.findElement(By.xpath(Object.getProperty("Password"))).clear();
		//driver.findElement(By.xpath(Object.getProperty("Password"))).sendKeys("gcMtest2015");
		//driver.findElement(By.xpath(Object.getProperty("login"))).click();
		
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[1]/span/input")).clear();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[1]/span/input")).sendKeys(Param.getProperty("AdminUserName"));
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[2]/span/input")).clear();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[2]/span/input")).sendKeys(Param.getProperty("AdminPassword"));	
		//scrn.getscreenshot();
		driver.findElement(By.xpath(".//*[@id='content']/div/div/div/div[4]/form/div[3]/span/a")).click();
		String winHandleBefore = driver.getWindowHandle();
	    for (String handle : driver.getWindowHandles()) {

	        driver.switchTo().window(handle);
	        }
	    /* Comment the below two lines if you are using IE */
	     driver.findElement(By.xpath("html/body/div[5]/div[3]/div/a[2]/span")).click();		
	  
		 //scrn.getscreenshot();
			driver.switchTo().window(winHandleBefore);
			// scrn.getscreenshot();
			/*driver.switchTo().defaultContent();
			//to be uncommented
			driver.findElement(By.xpath(".//*[@id='alerts']/div/div/button")).click();*/
		
		}


}



