package com.stta.GCM;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class EditContract extends SuiteGcmBase {
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[] = null;
	static boolean TestCasePass = true;
	static int DataSet = -1;
	static boolean Testskip = false;
	static boolean Testfail = false;
	// Code by Sky
	login log = new login();
	// Code by Sky-Declared as global variable
	CreateContract CC = new CreateContract();

	@BeforeTest
	public void checkCaseToRun() throws Exception {
		// Called init() function from SuiteBase class to Initialize .xls Files
		init();
		log.Run();
		// To set SuiteOne.xls file's path In FilePath Variable.
		FilePath = TestCaseListExcelOne;
		TestCaseName = this.getClass().getSimpleName();
		// SheetName to check CaseToRun flag against test case.
		SheetName = "TestCasesList";
		// Name of column In TestCasesList Excel sheet.
		ToRunColumnNameTestCase = "CaseToRun";
		// Name of column In Test Case Data sheets.
		ToRunColumnNameTestData = "DataToRun";
		// Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(TestCaseName + " : Execution started.");

		// To check test case's CaseToRun = Y or N In related excel sheet.
		// If CaseToRun = N or blank, Test case will skip execution. Else It
		// will be executed.
		if (!SuiteUtility.checkToRunUtility(FilePath, SheetName, ToRunColumnNameTestCase, TestCaseName)) {
			Add_Log.info(TestCaseName + " : CaseToRun = N for So Skipping Execution.");
			// To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
			// To throw skip exception for this test case.
			throw new SkipException(
					TestCaseName + "'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of " + TestCaseName);
		}
		// To retrieve DataToRun flags of all data set lines from related test
		// data sheet.
		TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);

		System.out.println(TestDataToRun);
	}

	Screen srn = new Screen();

	// Code by sky-added parameters in editContractTest method
	// Deep added extra parameters
	@Test(dataProvider = "EditContractData")
	public void EditContractTest(String Test_CaseId, String ContractId, String ContractName, String ContractDesc,
			String ProductsID, String ProductOffering, String ContractStartDateTimeZone, String ContractEndDate,
			String ContractEndDateTimeZone, String FactoryReferenceNumber, String SalesChannelCode,
			String SalesRepNameLatin, String SalesRepNameLocal, String DivisionLatin, String DivisionLocal,
			String TittleLatin, String TittleLocal, String Telephone, String ExtnNumber, String FaxNumber,
			String MobileNumber, String mailId, String ProductSpecification, String ProductItem,
			String ProductConfiguration, String ProductConfiguration1, String ProductConfiguration2,
			String ProductConfiguration3, String ProductConfiguration4, String ProductConfiguration5,
			String ProductConfiguration6, String ProductConfiguration7, String PaymentType, String chargeType,
			String chargeperiod, String Currency, String listprice, String ContractPrice, String chargeType1,
			String chargeperiod1, String Currency1, String listprice1, String contractprice1,
			String ProductInformation1, String ProductInformation2, String CustomerContractCurrency,
			String CustomerBillingCurrency, String IGCContractCurrency, String IGCBillingCurrency,
			String ServiceStartDateTimeZone, String ServiceEndDateTimeZone, String BillingStartDateTimeZone,
			String BillingEndDateTimeZone, String DiscountPercentage1, String DiscountPercentage2,
			String Discountprice1, String Discountprice2, String IGCSettlementPrice1, String IGCSettlementPrice2,
			String DocumentDescription, String DocumentDescription1, String DocumentDescription2,
			String DocumentDescription3, String DocumentName, String DocumentDescriptionX1, String DocumentNameX1,
			String DocumentDescriptionX2, String DocumentNameX2, String DocumentDescriptionX3, String DocumentNameX3,
			String DocumentDescriptionX4, String DocumentNameX4, String DocumentDescriptionX5, String DocumentNameX5,
			String DocumentDescriptionX6, String DocumentNameX6, String DocumentDescriptionX7, String DocumentNameX7,
			String DocumentDescriptionX8, String DocumentNameX8, String DocumentDescriptionX9, String DocumentNameX9,
			String DocumentDescriptionX10, String DocumentNameX10, String DocumentDescriptionX11,
			String DocumentNameX11, String DocumentDescriptionX12, String DocumentNameX12,
			String DocumentDescriptionX13, String DocumentNameX13, String DocumentDescriptionX14,
			String DocumentNameX14) throws Exception {
		DataSet++;
		// If found DataToRun = "N" for data set then execution will be skipped
		// for that data set.
		if (!TestDataToRun[DataSet].equalsIgnoreCase("Y")) {
			Add_Log.info(TestCaseName + " : DataToRun = N for data set line " + (DataSet + 1)
					+ " So skipping Its execution.");
			// If DataToRun = "N", Set Testskip=true.
			Testskip = true;
			throw new SkipException(
					"DataToRun for row number " + DataSet + " Is No Or Blank. So Skipping Its Execution.");
		}

		/***** Start Coding from here ***/

		EditContract EC = new EditContract();
		CreateContract CC = new CreateContract();
		String ContractID_026 = "";
		// code by sky
		if (Test_CaseId.equalsIgnoreCase("RTC_002")) {
			try {
				CC.ViewContractNavigation();
				CC.SelectContract(ContractName);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String cName = driver.findElement(By.id("name")).getText();
				if (cName.equals(ContractName)) {
					Reporter.log("Contract can be search by contract name passed");
					System.out.println("Contract can be search by contract name passed");
				} else {
					Assert.fail("Contract can be search by contract name failed - RTC_002 fail");
					System.out.println("Contract can be search by contract name failed - RTC_002 fail");
				}
			} catch (Exception e) {
				System.out.println("Contract can be search by contract name failed - RTC_002 fail:\t" + e);
				Assert.fail("Contract can be search by contract name failed - RTC_002 fail:\t" + e);
			}
		}
		if (Test_CaseId.equalsIgnoreCase("RTC_003")) {
			try {
				CC.CreateContractNavigation();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Thread.sleep(5000);
				CC.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductItem, ProductSpecification,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String tempContractId = driver.findElement(By.id("global_contract_id")).getText();
				CC.ViewContractNavigation();
				CC.SelectContractId(tempContractId);

				//
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String id = driver.findElement(By.id("id")).getText();
				if (id.isEmpty()) {
					Assert.fail("The contract search failed - an error occured");
				} else {
					Reporter.log("Contract search by contract id is successfull The object id is " + id);
				}

				//
				// String gContractId =
				// driver.findElement(By.id("global_contract_id")).getText();
				// if (gContractId.equals(ContractId)) {
				// Reporter.log("Contract can be search by contract Id passed");
				// System.out.println("Contract can be search by contract Id
				// passed");
				// } else {
				// Assert.fail("Contract can be search by contract Id failed -
				// RTC_003 fail");
				// System.out.println("Contract can be search by contract Id
				// failed - RTC_003 fail");
				// }
			} catch (Exception e) {

				Assert.fail("An error occured while editing the  contract - RTC_003 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_025/RTC_027/RTC_031/RTC_034/RTC_035/RTC_036/RTC_40")) {
			try {
				CC.ViewContractNavigation();
				EC.SelectContractEdit(ContractName);
				// srn.getscreenshot();
				EC.EditProductOffering();
				// srn.getscreenshot();
				System.out.println("RC_025-passed/Product Offering field is editable");
				Reporter.log("RC_025-passed/Product Offering field is editable");
				EC.EditProductSpecification();
				System.out.println("RC_027-passed/Product Specification field is editable");
				Reporter.log("RC_027-passed/Product Specification field is editable");
				EC.EditProductInformationRule();
				System.out.println("RTC_031-passed/Product Information rule value is editable");
				Reporter.log("RTC_031-passed/Product Information rule value is editable");
				EC.EditProductPriceInformation();
				System.out.println("Product Price Information is displayed-passed");
				Reporter.log("Product Price Information is displayed-passed");
				EC.EditDiscountFieldPercentage(DiscountPercentage1, DiscountPercentage2);
				System.out.println("RTC_034-passed/Discount field percentage is editable-passed");
				Reporter.log("RTC_034-passed/Discount field percentage is editable-passed");
				EC.EditDiscountFieldPrice(Discountprice1, Discountprice2);
				System.out.println("RTC_035/RTC_036-passed/Discount field price is editable-passed");
				Reporter.log("RTC_035/RTC_036-passed/Discount field price is editable-passed");
				EC.EditContractPrice();
				System.out.println("Contract Price Displayed-passed");
				Reporter.log("Contract Price Displayed-passed");
				EC.EditIGCPrice(IGCSettlementPrice1, IGCSettlementPrice2);
				System.out.println("RTC_040-passed/IGC Settlement Price is editable-passed");
				Reporter.log("RTC_040-passed/IGC Settlement Price is editable-passed");
				driver.findElement(By.id("SAVE_FOOTER")).click();
			} catch (Exception e) {
				System.out.println("RC_025-failed/Product Offering field not editable");
				Assert.fail("RC_027-failed/Product Specification field not editable");
				System.out.println("RTC_031-failed/Product Information rule value not editable");
				Assert.fail("RTC_031-failed/Product Information rule value not editable");
				System.out.println("Product Price Information not displayed-failed");
				Assert.fail("Product Price Information not displayed-failed");
				System.out.println("RTC_034-failed/Discount field percentage not editable");
				Assert.fail("RTC_034-failed/Discount field percentage not editable");
				System.out.println("RTC_035-failed/Discount field price not  editable");
				Assert.fail("RTC_035-failed/Discount field price not  editable");
				System.out.println("Contract Price not Displayed-failed");
				Assert.fail("Contract Price not Displayed-failed");
				System.out.println("IGC Settlement Price not editable");
				Assert.fail("IGC Settlement Price not editable");
				System.out.println("RTC_040-failed/Edit Functionality not Working in Document Module");
				Assert.fail("RTC_040-failed/Edit Functionality not Working in Document Module");
			}

		} else if (Test_CaseId.equalsIgnoreCase("RTC_022/RTC_023/RTC_024/RTC_073/RTC_074/RTC_078")) {
			try {

				CC.CreateContractNavigation();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Thread.sleep(5000);
				CC.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductItem, ProductSpecification,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String tempContractId = driver.findElement(By.id("global_contract_id")).getText();

				CC.ViewContractNavigation();
				// EC.ViewContract(ContractName);
				CC.SelectContractId(tempContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

				EC.CreateDocumentModule(DocumentDescription1);

				System.out.println("RTC_023/Create Document Functionality Working in Document Module-passed");
				Reporter.log("RTC_023/Create Document Functionality Working in Document Module-passed");
				EC.EditDocumentModule(DocumentDescription, DocumentName);

				// driver.findElement(By.id("SAVE_FOOTER")).click();

				System.out.println("RTC_022/Edit Functionality Working in Document Module-passed");
				Reporter.log("RTC_022/Edit Functionality Working in Document Module-passed");

				EC.MaxUploadSizeDocumentModule(DocumentDescription2);

				System.out.println("RTC_024/Maximum size for document upload verified-passed");
				Reporter.log("RTC_024/Maximum size for document upload verified-passed");

				EC.CheckVisibilityOfElement();

				System.out.println("RTC_073/Open All & Close All button present-passed");
				Reporter.log("RTC_073/Open All & Close All button present-passed");
				EC.CreateDocumentModuleLI(DocumentDescription1);
				EC.RemoveDocumentModule();
				EC.RemoveDocumentModuleLI();
				System.out.println("RTC_078/Remove Functionality Working in Document Module-passed");
				Reporter.log("RTC_078/Remove Functionality Working in Document Module-passed");
				/*
				 * EC.CreateDocumentModuleLI(DocumentDescription3); System.out
				 * .println("RTC_023/Create Document Functionality Working in Document Module(Lite Item)-passed"
				 * ); Reporter.
				 * log("RTC_023/Create Document Functionality Working in Document Module(LineItem)-passed"
				 * ); EC.CreateDocumentModuleDraft(DocumentDescription1,
				 * DocumentDescription3); System.out.println(
				 * "RTC_074/Create Document Functionality Working in Document Module(Contract Header & Line Item)-passed"
				 * ); Reporter.log(
				 * "RTC_074/Create Document Functionality Working in Document Module(Contract Header & Line Item)-passed"
				 * );
				 */
			} catch (Exception e) {
				System.out.println("Edit Document Functionality not Working in Document Module");
				Assert.fail("Edit Document Functionality not Working in Document Module");
				System.out.println("Create Document Functionality not Working in Document Module");
				Assert.fail("Create Document Functionality not Working in Document Module");
				System.out.println("Maximum size for document upload not verified");
				Assert.fail("Maximum size for document upload not verified");
				System.out.println("Open All & Close All button not present");
				Assert.fail("Open All & Close All button not present");
				System.out.println("RTC_078/Remove Functionality not Working in Document Module");
				Assert.fail("RTC_078/Remove Functionality not Working in Document Module");
				System.out.println(
						"RTC_074/Create Document Functionality not Working in Document Module(Contract Header & Line Item) with draft status");
				Assert.fail(
						"RTC_074/Create Document Functionality not Working in Document Module(Contract Header & Line Item)");
			}

		} else if (Test_CaseId.equalsIgnoreCase("RTC_075")) {
			try {
				CC.ViewContractNavigation();
				// EC.ViewContract(ContractName);
				CC.SelectContractId(ContractId);
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				EC.CreateDocumentModuleSentForApproval(DocumentDescription1, DocumentDescription3);
				System.out.println(
						"RTC_075/Create Document Functionality not Working in Document Module(Contract Header & Line Item) with sent for approval status -passed");
				Reporter.log(
						"RTC_075/Create Document Functionality not Working in Document Module(Contract Header & Line Item) with sent for approval status-passed");
			} catch (Exception e) {
				System.out.println(
						"Error in checking Create Document Functionality not Working in Document Module(Contract Header & Line Item) with "
								+ "sent for approval status failed - RTC-075 fail:\t" + e);
				Assert.fail(
						"Error in checking Create Document Functionality not Working in Document Module(Contract Header & Line Item) with "
								+ "sent for approval status failed - RTC-075 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_076")) {
			try {
				CC.ViewContractNavigation();
				// EC.ViewContract(ContractName);
				CC.SelectContractId(ContractId);
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				EC.CreateDocumentModuleApproved(DocumentDescription1, DocumentDescription3);
				System.out.println(
						"RTC_076/Create Document Functionality not Working in Document Module(Contract Header & Line Item) with approved status -passed");
				Reporter.log(
						"RTC_076/Create Document Functionality not Working in Document Module(Contract Header & Line Item) with sent with approved status-passed");
			} catch (Exception e) {
				System.out.println(
						"Error in checking Create Document Functionality not Working in Document Module(Contract Header & Line Item) with "
								+ "sent with approved status failed - RTC-076 fail:\t" + e);
				Assert.fail(
						"Error in checking Create Document Functionality not Working in Document Module(Contract Header & Line Item) with "
								+ "sent with approved status failed - RTC-076 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_077")) {
			try {
				CC.ViewContractNavigation();
				// EC.ViewContract(ContractName);
				CC.SelectContractId(ContractId);
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				EC.CreateDocumentModuleActivated(DocumentDescription1, DocumentDescription3);
				System.out.println(
						"RTC_077/Create Document Functionality not Working in Document Module(Contract Header & Line Item) with activated status -passed");
				Reporter.log(
						"RTC_077/Create Document Functionality not Working in Document Module(Contract Header & Line Item) with activated status-passed");
			} catch (Exception e) {
				System.out.println(
						"Error in checking Create Document Functionality not Working in Document Module(Contract Header & Line Item) with "
								+ "activated status failed - RTC-077 fail:\t" + e);
				Assert.fail(
						"Error in checking Create Document Functionality not Working in Document Module(Contract Header & Line Item) with "
								+ "activated status failed - RTC-077 fail:\t" + e);
			}
		}

		/* Added by deep - start */
		else if (Test_CaseId.equalsIgnoreCase("RTC_006/RTC_020/RTC_026")) {
			String temoProductOffering;
			String tempProductSpecification;
			try {

				CC.CreateContractNavigation();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				Thread.sleep(5000);
				CC.ContractDataFill(ContractName, ContractDesc, ProductsID, ProductOffering, ContractStartDateTimeZone,
						ContractEndDateTimeZone, FactoryReferenceNumber, SalesChannelCode, SalesRepNameLatin,
						SalesRepNameLocal, DivisionLatin, DivisionLocal, TittleLatin, TittleLocal, Telephone,
						ExtnNumber, FaxNumber, MobileNumber, mailId, ProductItem, ProductSpecification,
						ProductConfiguration, ProductConfiguration1, ProductConfiguration2, ProductConfiguration3,
						ProductConfiguration4, ProductConfiguration5, ProductConfiguration6, ProductConfiguration7,
						PaymentType, ProductInformation1, ProductInformation2, ServiceStartDateTimeZone,
						ServiceEndDateTimeZone, BillingStartDateTimeZone, BillingEndDateTimeZone);
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String tempContractId = driver.findElement(By.id("global_contract_id")).getText();
				ContractID_026 = tempContractId;
				CC.ViewContractNavigation();
				CC.SelectContractId(tempContractId);
				Thread.sleep(5000);
				System.out.println("The contract id is " + tempContractId);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// srn.getscreenshot();
				driver.findElement(By.id("edit_button")).click();
				Thread.sleep(5000);
				// srn.getscreenshot();

				Select prodOffer = new Select(driver.findElement(By.id("product_offering")));
				String oldProd = prodOffer.getFirstSelectedOption().getText();
				System.out.println("Old Prod::" + oldProd);

				if (oldProd.equals("Global Management One UNO Option")) {
					temoProductOffering = "Global Management One ECL2.0 Option";
					prodOffer.selectByVisibleText(temoProductOffering);
					String windowHandleBefore = driver.getWindowHandle();
					for (String handle : driver.getWindowHandles()) {
						driver.switchTo().window(handle);
					}
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					// srn.getscreenshot();
					driver.findElement(By.id("yui-gen0-button")).click();
					driver.switchTo().window(windowHandleBefore);
					// srn.getscreenshot();
					Thread.sleep(3000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					// srn.getscreenshot();
					driver.findElement(By.id("name")).clear();
					driver.findElement(By.id("name")).sendKeys("UpdatedContractName");
					driver.findElement(By.id("description")).clear();
					driver.findElement(By.id("description")).sendKeys("UpdatedContractDesc");

					Select prodSpec = new Select(driver.findElement(By.id("product_specification_2")));
					tempProductSpecification = "GMOne ECL2.0 Option Base Specification (Base)";
					prodSpec.selectByVisibleText(tempProductSpecification);
					// srn.getscreenshot();
					driver.findElement(By.id("product_config_2_565")).sendKeys("123");
					driver.findElement(By.id("pi_role_value_2_876")).sendKeys("124563");

				} else {
					temoProductOffering = "Global Management One UNO Option";
					prodOffer.selectByVisibleText(temoProductOffering);
					String windowHandleBefore = driver.getWindowHandle();
					for (String handle : driver.getWindowHandles()) {
						driver.switchTo().window(handle);
					}
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					// srn.getscreenshot();
					driver.findElement(By.id("yui-gen0-button")).click();
					driver.switchTo().window(windowHandleBefore);
					// srn.getscreenshot();
					Thread.sleep(3000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					// srn.getscreenshot();
					Select prodSpec = new Select(driver.findElement(By.id("product_specification_2")));
					prodSpec.selectByVisibleText(ProductSpecification);
					// srn.getscreenshot();
					driver.findElement(By.id("pi_role_value_2_877")).sendKeys("Test123");
				}

				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(10000);
				// srn.getscreenshot();
				driver.findElement(By.id("btn_custom_view_change_log")).click();
				Thread.sleep(10000);
				// srn.getscreenshot();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				Thread.sleep(1000);
				String tempcontractName = driver
						.findElement(By.xpath("html/body/table/tbody/tr/td/table[2]/tbody/tr[4]/td[4]/div")).getText();
				String tempcontractDesc = driver
						.findElement(By.xpath("html/body/table/tbody/tr/td/table[2]/tbody/tr[2]/td[4]/div")).getText();
				String prodOff = driver
						.findElement(By.xpath("html/body/table/tbody/tr/td/table[2]/tbody/tr[3]/td[4]/div")).getText();
				// srn.getscreenshot();
				driver.close();
				driver.switchTo().window(winHandleBefore);
				System.out.println("The contract Name is " + tempcontractName);
				System.out.println("The contract description  is " + tempcontractDesc);
				boolean contName = driver.getPageSource().contains("UpdatedContractName");
				boolean contDesc = driver.getPageSource().contains("UpdatedContractDesc");
				boolean contPo = driver.getPageSource().contains("Global Management One ECL2.0 Option");
				if (contName) {
					Reporter.log("The contract Name  is audited");
				} else {

					Assert.fail("The contract Name not is audited");
				}
				if (contDesc) {
					Reporter.log("The  Contract Description is audited");
				} else {

					Assert.fail("The  Contract Description not is audited");
				}
				if (contPo) {
					Reporter.log("The  Product offering is audited");
				} else {

					Assert.fail("The  Product offering  is not audited");
				}
			} catch (Exception e) {
				System.out.println(
						"Error in verfication of Contract can view change log failed -  RTC-020/RTC-026 fail:\t" + e);
				Assert.fail(
						"Error in verfication of Contract can view change log failed -  RTC-020/RTC-026 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_028")) {
			try {

				CC.ViewContractNavigation();
				CC.SelectContractId(ContractID_026);
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// Click on open all button
				driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
				// srn.getscreenshot();
				int var_ele_size = driver.findElements(By.xpath(".//*[@id='btn_view_change_log']")).size();
				driver.findElements(By.xpath(".//*[@id='btn_view_change_log']")).get(var_ele_size - 1).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				// srn.getscreenshot();
				if (driver.getPageSource().contains("Product Specification")) {
					System.out.println("Verfication of Product Specication in view change log passed");
					Reporter.log("Verfication of Product Specication in view change log passed");
				} else {
					System.out.println("Verfication of Product Specication in view change log failed - RTC-028 fail");
					Assert.fail("Verfication of Product Specication in view change log failed - RTC-028 fail");
				}
				driver.close();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				System.out.println(
						"Error in checking Product Specication in view change log failed - RTC-028 fail:\t" + e);
				Assert.fail("Error in checking Product Specication in view change log failed - RTC-028 fail:\t" + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("RTC_041")) {
			try {
				CC.ViewContractNavigation();
				CC.SelectContractId(ContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='arrow-li-1']")).click();
				// srn.getscreenshot();
				int var_ele_size = driver.findElements(By.xpath(".//*[@id='btn_view_change_log']")).size();
				driver.findElements(By.xpath(".//*[@id='btn_view_change_log']")).get(var_ele_size - 2).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				// srn.getscreenshot();
				if (driver.getPageSource().contains("Minimum Monthly Charges- Inter-Group-Company Settlement Price")) {
					System.out.println(
							"Minimum Monthly Charges- Inter-Group-Company Settlement Price in view change log passed");
					Reporter.log(
							"Minimum Monthly Charges- Inter-Group-Company Settlement Price in view change log passed");
				} else {
					System.out.println(
							"Minimum Monthly Charges- Inter-Group-Company Settlement Price in view change log failed - RTC-041 fail");
					Assert.fail(
							"Minimum Monthly Charges- Inter-Group-Company Settlement Price in view change log failed - RTC-041 fail");
				}
				if (driver.getPageSource().contains(
						"Monthly Unit Price per Unit of Additional Standard Change- Inter-Group-Company Settlement Price")) {
					System.out.println(
							"Monthly Unit Price per Unit of Additional Standard Change- Inter-Group-Company Settlement Price in view change log passed");
					Reporter.log(
							"Monthly Unit Price per Unit of Additional Standard Change- Inter-Group-Company Settlement Price in view change log passed");
				} else {
					System.out.println(
							"Monthly Unit Price per Unit of Additional Standard Change- Inter-Group-Company Settlement Price in view change log failed - RTC-041 fail");
					Assert.fail(
							"Monthly Unit Price per Unit of Additional Standard Change- Inter-Group-Company Settlement Price in view change log failed - RTC-041 fail");
				}
				driver.close();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				System.out.println(
						"Error in checking Inter-Group-Company Settlement Price field is auditable or not failed - RTC-041 fail:\t"
								+ e);
				Assert.fail(
						"Error in checking Inter-Group-Company Settlement Price field is auditable or not failed - RTC-041 fail:\t"
								+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_032/RTC_037/RTC_044")) {
			try {
				CC.ViewContractNavigation();
				CC.SelectContractId(ContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.xpath(".//*[@id='arrow-li-2']")).click();
				// srn.getscreenshot();
				int var_ele_size = driver.findElements(By.xpath(".//*[@id='btn_view_change_log']")).size();
				driver.findElements(By.xpath(".//*[@id='btn_view_change_log']")).get(var_ele_size - 1).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				// srn.getscreenshot();
				if (driver.getPageSource().contains("Pricing Rule")) {
					System.out.println("Pricing Rule in view change log passed");
					Reporter.log("Pricing Rule in view change log passed");
				} else {
					System.out.println("Pricing Rule in view change log failed - RTC-032 fail");
					Assert.fail("Pricing Rule in view change log failed - RTC-032 fail");
				}
				if (driver.getPageSource().contains("Monthly Unit Price per CI- Discount")) {
					System.out.println("Monthly Unit Price per CI- Discount in view change log passed");
					Reporter.log("Monthly Unit Price per CI- Discount in view change log passed");
				} else {
					System.out.println("Monthly Unit Price per CI- Discount in view change log failed - RTC-037 fail");
					Assert.fail("Monthly Unit Price per CI- Discount in view change log failed - RTC-037 fail");
				}
				if (driver.getPageSource().contains("Installation Fee- Discount")) {
					System.out.println("Installation Fee- Discount in view change log passed");
					Reporter.log("Installation Fee- Discount in view change log passed");
				} else {
					System.out.println("Installation Fee- Discount in view change log failed - RTC-037 fail");
					Assert.fail("Installation Fee- Discount in view change log failed - RTC-037 fail");
				}
				if (driver.getPageSource().contains("Monthly Unit Price per CI- Customer Contract Price")) {
					System.out.println("Monthly Unit Price per CI- Customer Contract Price in view change log passed");
					Reporter.log("Monthly Unit Price per CI- Customer Contract Price in view change log passed");
				} else {
					System.out.println(
							"Monthly Unit Price per CI- Customer Contract Price in view change log failed - RTC-044 fail");
					Assert.fail(
							"Monthly Unit Price per CI- Customer Contract Price in view change log failed - RTC-044 fail");
				}
				if (driver.getPageSource().contains("Installation Fee- Customer Contract Price")) {
					System.out.println("Installation Fee- Customer Contract Price in view change log passed");
					Reporter.log("Installation Fee- Customer Contract Price in view change log passed");
				} else {
					System.out.println(
							"Installation Fee- Customer Contract Price in view change log failed - RTC-044 fail");
					Assert.fail("Installation Fee- Customer Contract Price in view change log failed - RTC-044 fail");
				}
				driver.close();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				System.out.println(
						"Error in checking log in Pricing Rule, Discount, Customer Contract Price in log failed - RTC_032/RTC_037/RTC_044 fail:\t"
								+ e);
				Assert.fail(
						"Error in checking log in Pricing Rule, Discount, Customer Contract Price in log failed - RTC_032/RTC_037/RTC_044 fail:\t"
								+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_065")) {
			try {
				CC.ViewContractNavigation();
				CC.SelectContractId(ContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// srn.getscreenshot();
				driver.findElement(By.xpath(".//*[@id='btn_custom_view_change_log']")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				Thread.sleep(5000);
				// srn.getscreenshot();
				if (driver.getPageSource().contains("Contract Line Item")) {
					System.out.println("Deleted contract line item are recorded in view change log passed");
				} else {
					System.out.println(
							"Deleted contract line item are recorded in view change log failed - RTC-065 fail");
				}
				driver.close();
				driver.switchTo().window(winHandleBefore);
			} catch (Exception e) {
				System.out.println(
						"Error in checking deleted contract line item are recorded in view change log failed - RTC-065 fail:\t"
								+ e);
				Assert.fail(
						"Error in checking deleted contract line item are recorded in view change log failed - RTC-065 fail:\t"
								+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_066/RTC_114")) {
			try {
				CC.ViewContractNavigation();
				CC.SelectContractId(ContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// srn.getscreenshot();
				driver.findElement(By.id("edit_button")).click();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// srn.getscreenshot();
				// driver.findElement(By.id("SAVE_FOOTER")).click();
				int var_ele_size = driver.findElements(By.xpath(".//*[@id='id']")).size();
				System.out.println("Size::" + var_ele_size);
				String value = driver.findElements(By.xpath(".//*[@id='id']")).get(var_ele_size - 1).getText();
				System.out.println("Value::" + value);
				// srn.getscreenshot();
				driver.findElement(By.id("btn_li_remove_2")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen0-button")).click();
				driver.switchTo().window(winHandleBefore);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				if (!driver.getPageSource().contains(value)) {
					System.out.println("Contract line item is deleted");
					Reporter.log("Contract line item is deleted");
				} else {
					System.out.println("Contract line item is not deleted - RTC-066 fail");
					Reporter.log("Contract line item is not deleted - RTC-066 fail");
					Assert.fail();
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("edit_button")).click();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// srn.getscreenshot();
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(5000);
				System.out.println("RTC-066/RTC-114 passed");
				Reporter.log("RTC-066/RTC-114 passed");
			} catch (Exception e) {
				System.out.println(
						"Error in checking contract line item is deleted,check DB level flag should be one failed - RTC-066/RTC-114 fail:/t"
								+ e);
				Assert.fail(
						"Error in checking contract line item is deleted,check DB level flag should be one failed - RTC-066/RTC-114 fail:/t"
								+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_048/RTC_050/RTC_052/RTC_054")) {
			try {
				CC.ViewContractNavigation();
				CC.SelectContractId(ContractId);
				// srn.getscreenshot();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String custContCurr = driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[3]/div[2]/div/span"))
						.getText();
				String curr = "JPY";
				// srn.getscreenshot();
				if (custContCurr.toLowerCase().indexOf(curr.toLowerCase()) != -1) {
					System.out.println("Customer Contract Currency is displayed ISO 4217 Code in Create/Edit view");
					Reporter.log("Customer Contract Currency is displayed ISO 4217 Code in Create/Edit view");
				} else {
					System.out.println(
							"Customer Contract Currency is not displayed ISO 4217 Code in Create/Edit view - RTC_048 fail");
					Assert.fail(
							"Customer Contract Currency is not displayed ISO 4217 Code in Create/Edit view - RTC_048 fail");
				}
				String billCurr = driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[3]/div[4]/div/span"))
						.getText();
				if (billCurr.toLowerCase().indexOf(curr.toLowerCase()) != -1) {
					System.out.println("Customer Billing Currency is displayed ISO 4217 Code in Create/Edit view");
					Reporter.log("Customer Billing Currency is displayed ISO 4217 Code in Create/Edit view");
				} else {
					System.out.println(
							"Customer Billing Currency is not displayed ISO 4217 Code in Create/Edit view - RTC_050 fail");
					Assert.fail(
							"Customer Billing Currency is not displayed ISO 4217 Code in Create/Edit view - RTC_050 fail");
				}
				String igcCustContCurr = driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[4]/div[2]/div/span"))
						.getText();
				if (igcCustContCurr.toLowerCase().indexOf(curr.toLowerCase()) != -1) {
					System.out.println(
							"Inter-Group-Company Contract Currency is displayed ISO 4217 Code in Create/Edit view");
					Reporter.log(
							"Inter-Group-Company Contract Currency is displayed ISO 4217 Code in Create/Edit view");
				} else {
					System.out.println(
							"Inter-Group-Company Contract Currency is not displayed ISO 4217 Code in Create/Edit view - RTC_052 fail");
					Assert.fail(
							"Inter-Group-Company Contract Currency is not displayed ISO 4217 Code in Create/Edit view - RTC_052 fail");
				}
				String igcCustBillCurr = driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[4]/div[4]/div/span"))
						.getText();
				if (igcCustBillCurr.toLowerCase().indexOf(curr.toLowerCase()) != -1) {
					System.out.println(
							"Inter-Group-Company Billing Currency is displayed ISO 4217 Code in Create/Edit view");
					Reporter.log("Inter-Group-Company Billing Currency is displayed ISO 4217 Code in Create/Edit view");
				} else {
					System.out.println(
							"Inter-Group-Company Billing Currency is not displayed ISO 4217 Code in Create/Edit view - RTC_054 fail");
					Assert.fail(
							"Inter-Group-Company Billing Currency is not displayed ISO 4217 Code in Create/Edit view - RTC_054 fail");
				}
			} catch (Exception e) {
				System.out.println(
						"Error in checking ISO 4217 Code in Create/Edit view - RTC_048/RTC_050/RTC_052/RTC_054 fail:/t"
								+ e);
				Assert.fail(
						"Error in checking ISO 4217 Code in Create/Edit view - RTC_048/RTC_050/RTC_052/RTC_054 fail:/t"
								+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_064/RTC_067/RTC_082")) {
			try {
				CC.ViewContractNavigation();
				CC.SelectContractId(ContractId);
				// srn.getscreenshot();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// srn.getscreenshot();
				if (driver.findElements(By.id("delete_button")).size() == 0) {
					System.out.println(
							"Contract line item cannot be deleted when contract is not in draft status passed");
					Reporter.log("Contract line item cannot be deleted when contract is not in draft status passed");
				} else {
					System.out.println(
							"Contract line item cannot be deleted when contract is not in draft status failed - RTC-064 fail");
					Reporter.log(
							"Contract line item cannot be deleted when contract is not in draft status failed - RTC-064 fail");
				}
				if (driver.findElements(By.id("edit_button")).size() == 0) {
					System.out.println(
							"Edit button is disabled for all users except for System Administrator when contract status changes to Activated state passed");
					Reporter.log(
							"edit button is disabled for all users except for System Administrator when contract status changes to Activated state passed");
				} else {
					System.out.println(
							"edit button is disabled for all users except for System Administrator when contract status changes to Activated state failed - RTC-067 fail");
					Reporter.log(
							"edit button is disabled for all users except for System Administrator when contract status changes to Activated state failed - RTC-067 fail");
				}
				if (driver.findElements(By.id("modification_contract")).size() != 0) {
					System.out.println("Modification button is visible in the contract detail view page");
					Reporter.log("Modification button is visible in the contract detail view page");
				} else {
					System.out.println(
							"Modification button is not visible in the contract detail view page - - RTC-082 fail");
					Reporter.log("Modification button is not visible in the contract detail view page - RTC-082 fail");
				}
			} catch (Exception e) {
				System.out.println(
						"Error in checking Edit/Delete/Mofication failed - RTC-064/RTC-067/RTC-082 fail:\t" + e);
				Assert.fail("Error in checking Edit/Delete/Mofication failed - RTC-064/RTC-067/RTC-082 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_080")) {
			try {
				CC.ViewContractNavigation();
				CC.SelectContractId(ContractId);
				Thread.sleep(5000);
				// srn.getscreenshot();
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String techAcc = driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[6]/div[2]/div/span/a"))
						.getText();
				// srn.getscreenshot();
				driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[6]/div[2]/div/span/a")).click();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String techAccount = driver.findElement(By.id("last_name")).getText();
				if (techAccount.equals(techAcc)) {
					System.out.println("Technology Customer Account was hyperlinked");
					Reporter.log("Technology Customer Account was hyperlinked");
				} else {
					System.out.println("Technology Customer Account was not hyperlinked - RTC-080 fail");
					Assert.fail("Technology Customer Account was not hyperlinked - RTC-080 fail");
				}
				// srn.getscreenshot();
				driver.navigate().back();
				driver.navigate().back();
				driver.switchTo().defaultContent();
				srn.getscreenshot();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String billAcc = driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[6]/div[4]/div/span/a"))
						.getText();
				// srn.getscreenshot();
				driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[6]/div[4]/div/span/a")).click();
				Thread.sleep(10000);
				// srn.getscreenshot();
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String billAccount = driver.findElement(By.id("last_name")).getText();
				if (billAccount.equals(billAcc)) {
					System.out.println("Billing Customer Account was hyperlinked");
					Reporter.log("Billing Customer Account was hyperlinked");
				} else {
					System.out.println("Billing Customer Account was not hyperlinked - RTC-080 fail");
					Assert.fail("Billing Customer Account was not hyperlinked - RTC-080 fail");
				}
			} catch (Exception e) {
				System.out.println(
						"Error in checking Technology Customer Account and Billing Customer Account are hyperlinked - RTC-080 fail:\t"
								+ e);
				Assert.fail(
						"Error in checking Technology Customer Account and Billing Customer Account are hyperlinked - RTC-080 fail:\t"
								+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_083")) {
			try {
				CC.ViewContractNavigation();
				CC.SelectContractId(ContractId);
				driver.switchTo().defaultContent();
				// srn.getscreenshot();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String oldId = driver
						.findElement(By
								.xpath(".//*[@id='list_subpanel_contract_version_history']/table/tbody/tr[4]/td[1]/span"))
						.getText();
				String newId = driver
						.findElement(By
								.xpath(".//*[@id='list_subpanel_contract_version_history']/table/tbody/tr[3]/td[1]/span"))
						.getText();
				// srn.getscreenshot();
				if (newId.equals(oldId)) {
					System.out.println("Global Contract ID of new version is same as that of previous version");
					Reporter.log("Global Contract ID of new version is same as that of previous version");
				} else {
					System.out.println(
							"Global Contract ID of new version is not same as that of previous version - RTC-083 fail");
					Assert.fail(
							"Global Contract ID of new version is not same as that of previous version - RTC-083 fail");
				}
			} catch (Exception e) {
				System.out.println(
						"Error in checking Global Contract ID of new version is not same as that of previous version - RTC-083 fail:\t"
								+ e);
				Assert.fail(
						"Error in checking Global Contract ID of new version is not same as that of previous version - RTC-083 fail:\t"
								+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_084/RTC_085")) {
			CC.ViewContractNavigation();
			CC.SelectContractId(ContractId);
			// srn.getscreenshot();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			// srn.getscreenshot();
			driver.findElement(By.id("edit_button")).click();
			Thread.sleep(10000);
			clearData();
			// srn.getscreenshot();
			try {
				driver.findElement(By.id("name")).sendKeys(ContractName);
				driver.findElement(By.id("description")).sendKeys(ContractDesc);
				driver.findElement(By.id("btn_pm_products_gc_contracts_1_name")).click();
				String winHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.findElement(By.id("products_id_advanced")).clear();
				driver.findElement(By.id("products_id_advanced")).sendKeys(ProductsID);
				driver.findElement(By.id("search_form_submit")).click();
				driver.findElement(By.xpath("//tbody/tr[3]/td[1]/a")).click();
				driver.switchTo().window(winHandleBefore);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("btn_accounts_gc_contracts_1_name")).click();
				String winHandleBefore1 = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.findElement(By.id("search_form_clear"));
				driver.findElement(By.id("common_customer_id_c_advanced")).sendKeys("C0321124868");
				driver.findElement(By.id("search_form_submit")).click();
				driver.findElement(By.xpath("html/body/table[4]/tbody/tr[3]/td[3]/a")).click();
				driver.switchTo().window(winHandleBefore1);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("btn_contacts_gc_contracts_1_name")).click();
				String winHandleBefore2 = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.findElement(By.id("last_name_advanced")).clear();
				driver.findElement(By.id("last_name_advanced")).sendKeys("AutomationTestPersonName");
				driver.findElement(By.id("search_form_submit")).click();
				driver.findElement(By.xpath("//table[4]/tbody/tr[3]/td[1]/a")).click();
				driver.switchTo().window(winHandleBefore2);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("contract_enddate")).sendKeys(ContractEndDate);
				driver.findElement(By.id("gc_timezone_id1_c")).sendKeys(ContractEndDateTimeZone);
				driver.findElement(By.id("factory_reference_no")).sendKeys(FactoryReferenceNumber);
				driver.findElement(By.id("sales_channel_code")).sendKeys(SalesChannelCode);
				driver.findElement(By.id("sales_rep_name")).sendKeys(SalesRepNameLatin);
				driver.findElement(By.id("sales_rep_name_1")).sendKeys(SalesRepNameLocal);
				driver.findElement(By.id("division_1")).sendKeys(DivisionLatin);
				driver.findElement(By.id("division_2")).sendKeys(DivisionLocal);
				driver.findElement(By.id("title_1")).sendKeys(TittleLatin);
				driver.findElement(By.id("title_2")).sendKeys(TittleLocal);
				driver.findElement(By.id("tel_no")).sendKeys(Telephone);
				driver.findElement(By.id("ext_no")).sendKeys(ExtnNumber);
				driver.findElement(By.id("fax_no")).sendKeys(FaxNumber);
				driver.findElement(By.id("mob_no")).sendKeys(MobileNumber);
				driver.findElement(By.id("email1")).sendKeys(mailId);
				// srn.getscreenshot();

				String contName = driver.findElement(By.id("name")).getAttribute("value");
				String contDesc = driver.findElement(By.id("description")).getAttribute("value");
				String product = driver.findElement(By.id("pm_products_gc_contracts_1_name")).getAttribute("value");
				String commonCustComp = driver.findElement(By.id("accounts_gc_contracts_1_name")).getAttribute("value");
				String commonCutAcc = driver.findElement(By.id("contacts_gc_contracts_1_name")).getAttribute("value");
				String contEndDate = driver.findElement(By.id("contract_enddate")).getAttribute("value");
				Select contEndTZ = new Select(driver.findElement(By.id("gc_timezone_id1_c")));
				String contEndTimeZone = contEndTZ.getFirstSelectedOption().getText();
				String factRefNo = driver.findElement(By.id("factory_reference_no")).getAttribute("value");
				String salesChannelCode = driver.findElement(By.id("sales_channel_code")).getAttribute("value");
				String salesRepNameLatin = driver.findElement(By.id("sales_rep_name")).getAttribute("value");
				String salesRepNameLocal = driver.findElement(By.id("sales_rep_name_1")).getAttribute("value");
				String divisionLatin = driver.findElement(By.id("division_1")).getAttribute("value");
				String divisionLocal = driver.findElement(By.id("division_2")).getAttribute("value");
				String titleLatin = driver.findElement(By.id("title_1")).getAttribute("value");
				String titleLocal = driver.findElement(By.id("title_2")).getAttribute("value");
				String telephone = driver.findElement(By.id("tel_no")).getAttribute("value");
				String extNo = driver.findElement(By.id("ext_no")).getAttribute("value");
				String fax = driver.findElement(By.id("fax_no")).getAttribute("value");
				String mobileNo = driver.findElement(By.id("mob_no")).getAttribute("value");
				String email = driver.findElement(By.id("email1")).getAttribute("value");
				if (contName.equals(ContractName)) {
					Reporter.log("ContractName is editable for new version");
					System.out.println("ContractName is editable for new version");
				} else {
					System.out.println("ContractName is not editable for new version - RTC-084 fail");
					Assert.fail("ContractName is not editable for new version - RTC-084 fail");
				}
				if (contDesc.equals(ContractDesc)) {
					Reporter.log("Contract Description is editable for new version");
					System.out.println("Contract Description is editable for new version");
				} else {
					System.out.println("Contract Description is not editable for new version - RTC-084 fail");
					Assert.fail("Contract Description is not editable for new version - RTC-084 fail");
				}
				if (product.equals("UAT test product")) {
					Reporter.log("Product Id is editable for new version");
					System.out.println("Product Id is editable for new version");
				} else {
					System.out.println("Product Id is not editable for new version - RTC-084 fail");
					Assert.fail("Product Id is not editable for new version - RTC-084 fail");
				}
				if (commonCustComp.equals("Emerio Technology Pvt Ltd")) {
					Reporter.log("Common Customer Company is editable for new version");
					System.out.println("Common Customer Company is editable for new version");
				} else {
					System.out.println("Common Customer Company is not editable for new version - RTC-084 fail");
					Assert.fail("Common Customer Company is not editable for new version - RTC-084 fail");
				}
				if (commonCutAcc.equals("AutomationTestPersonNameLatin")) {
					Reporter.log("Common Customer Account is editable for new version");
					System.out.println("Common Customer Account is editable for new version");
				} else {
					System.out.println("Common Customer Account is not editable for new version - RTC-084 fail");
					Assert.fail("Common Customer Account is not editable for new version - RTC-084 fail");
				}
				if (contEndDate.equals(ContractEndDate)) {
					Reporter.log("Contract End Date is editable for new version");
					System.out.println("Contract End Date is editable for new version");
				} else {
					System.out.println("Contract End Date is not editable for new version - RTC-084 fail");
					Assert.fail("Contract End Date is not editable for new version - RTC-084 fail");
				}
				if (contEndTimeZone.equals("India personal timezone (9:00)")) {
					Reporter.log("Contract End Date Time Zone is editable for new version");
					System.out.println("Contract End Date Time Zone is editable for new version");
				} else {
					System.out.println("Contract End Date Time Zone is not editable for new version - RTC-084 fail");
					Assert.fail("Contract End Date Time Zone is not editable for new version - RTC-084 fail");
				}
				if (factRefNo.equals(FactoryReferenceNumber)) {
					Reporter.log("Factory Reference Number is editable for new version");
					System.out.println("Factory Reference Number is editable for new version");
				} else {
					System.out.println("Factory Reference Number is not editable for new version - RTC-084 fail");
					Assert.fail("Factory Reference Number is not editable for new version - RTC-084 fail");
				}
				if (salesChannelCode.equals(SalesChannelCode)) {
					Reporter.log("Sales Channel code is editable for new version");
					System.out.println("Sales Channel code is editable for new version");
				} else {
					System.out.println("Sales Channel code is not editable for new version - RTC-084 fail");
					Assert.fail("Sales Channel code is not editable for new version - RTC-084 fail");
				}
				if (salesRepNameLatin.equals(SalesRepNameLatin)) {
					Reporter.log("Sales Representative Name (Latin Character) is editable for new version");
					System.out.println("Sales Representative Name (Latin Character) is editable for new version");
				} else {
					System.out.println(
							"Sales Representative Name (Latin Character) is not editable for new version - RTC-084 fail");
					Assert.fail(
							"Sales Representative Name (Latin Character) is not editable for new version - RTC-084 fail");
				}
				if (salesRepNameLocal.equals(SalesRepNameLocal)) {
					Reporter.log("Sales Representative Name (Local Character) is editable for new version");
					System.out.println("Sales Representative Name (Local Character) is editable for new version");
				} else {
					System.out.println(
							"Sales Representative Name (Local Character) is not editable for new version - RTC-084 fail");
					Assert.fail(
							"Sales Representative Name (Local Character) is not editable for new version - RTC-084 fail");
				}
				if (divisionLatin.equals(DivisionLatin)) {
					Reporter.log("Division (Latin Character) is editable for new version");
					System.out.println("Division (Latin Character) is editable for new version");
				} else {
					System.out.println("Division (Latin Character) is not editable for new version - RTC-084 fail");
					Assert.fail("Division (Latin Character) is not editable for new version - RTC-084 fail");
				}
				if (divisionLocal.equals(DivisionLocal)) {
					Reporter.log("Division (Local Character) is editable for new version");
					System.out.println("Division (Local Character) is editable for new version");
				} else {
					System.out.println("Division (Local Character) is not editable for new version - RTC-084 fail");
					Assert.fail("Division (Local Character) is not editable for new version - RTC-084 fail");
				}
				if (titleLatin.equals(TittleLatin)) {
					Reporter.log("Title (Latin Character) is editable for new version");
					System.out.println("Title (Latin Character) is editable for new version");
				} else {
					System.out.println("Title (Latin Character) is not editable for new version - RTC-084 fail");
					Assert.fail("Title (Latin Character) is not editable for new version - RTC-084 fail");
				}
				if (titleLocal.equals(TittleLocal)) {
					Reporter.log("Title (Local Character) is editable for new version");
					System.out.println("Title (Local Character) is editable for new version");
				} else {
					System.out.println("Title (Local Character) is not editable for new version - RTC-084 fail");
					Assert.fail("Title (Local Character) is not editable for new version - RTC-084 fail");
				}
				if (telephone.equals(Telephone)) {
					Reporter.log("Telephone number is editable for new version");
					System.out.println("Telephone number is editable for new version");
				} else {
					System.out.println("Telephone number is not editable for new version - RTC-084 fail");
					Assert.fail("Telephone number is not editable for new version - RTC-084 fail");
				}
				if (extNo.equals(ExtnNumber)) {
					Reporter.log("Extension number is editable for new version");
					System.out.println("Extension number is editable for new version");
				} else {
					System.out.println("Extension number is not editable for new version - RTC-084 fail");
					Assert.fail("Extension number is not editable for new version - RTC-084 fail");
				}
				if (fax.equals(FaxNumber)) {
					Reporter.log("FAX number is editable for new version");
					System.out.println("FAX number is editable for new version");
				} else {
					System.out.println("FAX number is not editable for new version - RTC-084 fail");
					Assert.fail("FAX number is not editable for new version - RTC-084 fail");
				}
				if (mobileNo.equals(MobileNumber)) {
					Reporter.log("Mobile number is editable for new version");
					System.out.println("Mobile number is editable for new version");
				} else {
					System.out.println("Mobile number is not editable for new version - RTC-084 fail");
					Assert.fail("Mobile number is not editable for new version - RTC-084 fail");
				}
				if (email.equals(mailId)) {
					Reporter.log("Email address is editable for new version");
					System.out.println("Email address is editable for new version");
				} else {
					System.out.println("Email address is not editable for new version - RTC-084 fail");
					Assert.fail("Email address is not editable for new version - RTC-084 fail");
				}
			} catch (Exception e) {
				System.out.println(
						"Error in checking user can edit some fields of Contract Header and Sales Representative in new version of contract - RTC-084 fail:\t"
								+ e);
				Assert.fail(
						"Error in checking user can edit some fields of Contract Header and Sales Representative in new version of contract - RTC-084 fail:\t"
								+ e);
			}
			try {
				driver.findElement(By.id("add_line_item")).click();
				srn.getscreenshot();
				driver.findElement(By.id("btn_li_remove_3")).click();
				String windowHandleBefore = driver.getWindowHandle();
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// srn.getscreenshot();
				driver.findElement(By.id("yui-gen0-button")).click();
				driver.switchTo().window(windowHandleBefore);
				// srn.getscreenshot();
				System.out.println("X button presented only for new line items");
				Reporter.log("X button presented only for new line items");
			} catch (Exception e) {
				System.out.println("X button not presented only for new line items - RTC-085 fail:\t" + e);
				Assert.fail("X button not presented only for new line items RTC-085 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_086/RTC_087")) {
			try {
				EC.ViewContractNavigationPop();
				EC.selectContractIdFrom2ndRow(ContractId);
				// srn.getscreenshot();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// srn.getscreenshot();
				driver.findElement(By.id("edit_button")).click();
				Thread.sleep(10000);
				boolean change = false;
				boolean terminate = false;
				// srn.getscreenshot();
				if (driver.findElement(By.xpath(".//*[@id='change_contract_1']")).isDisplayed()) {
					System.out.println("Change button was presented for only version up line items");
					Reporter.log("Change button was presented for only version up line items");
					change = true;
				} else {
					System.out.println("Change button was not presented for only version up line items - RTC-086 fail");
					Assert.fail("Change button was not presented for only version up line items - RTC-086 fail");
					change = false;
				}
				if (driver.findElement(By.xpath(".//*[@id='terminate_line_item_1']")).isDisplayed()) {
					System.out.println("Terminate button was presented for only version up line items");
					Reporter.log("Terminate button was presented for only version up line items");
					terminate = true;
				} else {
					System.out.println(
							"Terminate button was not presented for only version up line items - RTC-086 fail");
					Assert.fail("Terminate button was not presented for only version up line items - RTC-086 fail");
					terminate = false;
				}
				if (change) {
					driver.findElement(By.xpath(".//*[@id='change_contract_1']")).click();
					WebElement prodSpec = driver.findElement(By.id("product_specification_1"));
					prodSpec.click();
					if (!prodSpec.isSelected()) {
						System.out.println(
								"User can not edit the Product Specification in an existing Line Item in new version of contract");
						Reporter.log(
								"User can not edit the Product Specification in an existing Line Item in new version of contract");
					} else {
						System.out.println(
								"User can edit the Product Specification in an existing Line Item in new version of contract - RTC-087 fail");
						Assert.fail(
								"User can edit the Product Specification in an existing Line Item in new version of contract - RTC-087 fail");
					}
				} else {
					System.out.println("Change button not available - - RTC-087 fail");
					Assert.fail("Change button not available - RTC-087 fail");
				}
			} catch (Exception e) {
				System.out.println(
						"Error in checking Change, Terminate button was not presented and ProductSpecifcation can edit or not for only version up line items - RTC-086/RTC-087 fail:\t"
								+ e);
				Assert.fail(
						"Error in checking Change, Terminate button was not presented and ProductSpecifcation can edit or not for only version up line items - RTC-086/RTC-087 fail:\t"
								+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_088")) {
			try {
				EC.ViewContractNavigationPop();
				EC.selectContractIdFrom2ndRow(ContractId);
				// srn.getscreenshot();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// srn.getscreenshot();
				driver.findElement(By.id("edit_button")).click();
				Thread.sleep(10000);
				driver.findElement(By.xpath(".//*[@id='terminate_line_item_1']")).click();
				// srn.getscreenshot();
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Robot r = new Robot();
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String errorMessage = driver.findElement(By.className("bd")).getText();
				// srn.getscreenshot();
				r.keyPress(KeyEvent.VK_ENTER);
				r.keyRelease(KeyEvent.VK_ENTER);
				// srn.getscreenshot();
				if (errorMessage.equals("One Base specification to be included in contract")) {
					System.out.println("Error message was displayed");
					Reporter.log("Error message was displayed");
				} else {
					System.out.println("Error message was not displayed - RTC-088 fail");
					Reporter.log("Error message was not displayed - RTC-088 fail");
				}
			} catch (Exception e) {
				System.out.println("Error in checking Error mesasge was present or not - RTC-088 fail:\t" + e);
				Assert.fail("Error in checking Error mesasge was present or not - RTC-088 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_Contract_095/RTC_Contract_096/RTC_Contract_097")) {
			try {
				CC.ViewContractNavigation();
				CC.SelectContractId(ContractId);
				// srn.getscreenshot();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String custAccount = driver.findElement(By.xpath(".//*[@id='contacts_gc_contracts_1contacts_ida']"))
						.getText();
				String billingAccount = driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[6]/div[4]/div/span/a"))
						.getText();
				String techAccount = driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[6]/div[2]/div/span/a"))
						.getText();
				// srn.getscreenshot();
				driver.findElement(By.id("modification_contract")).click();
				Thread.sleep(10000);
				// srn.getscreenshot();
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				driver.findElement(By.id("SAVE_FOOTER")).click();
				Thread.sleep(10000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				String custAccount1 = driver.findElement(By.xpath(".//*[@id='contacts_gc_contracts_1contacts_ida']"))
						.getText();
				String billingAccount1 = driver
						.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[6]/div[4]/div/span/a")).getText();
				String techAccount1 = driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[6]/div[2]/div/span/a"))
						.getText();
				// srn.getscreenshot();
				if (custAccount.equals(custAccount1)) {
					System.out.println(
							"Existing contract account is copied when the user clicks on the modification button is created passed");
					Reporter.log(
							"Existing contract account is copied when the user clicks on the modification button is created passed");
				} else {
					System.out.println(
							"Existing contract account is copied when the user clicks on the modification button is created failed");
					Reporter.log(
							"Existing contract account is copied when the user clicks on the modification button is created failed");
					Assert.fail();
				}
				if (billingAccount.equals(billingAccount1)) {
					System.out.println(
							"Existing billing account is copied when the user clicks on the modification button is created passed");
					Reporter.log(
							"Existing billing account is copied when the user clicks on the modification button is created passed");
				} else {
					System.out.println(
							"Existing billing account is copied when the user clicks on the modification button is created failed");
					Reporter.log(
							"Existing billing account is copied when the user clicks on the modification button is created failed");
					Assert.fail();
				}
				if (techAccount.equals(techAccount1)) {
					System.out.println(
							"Existing technology account is copied when the user clicks on the modification button is created passed");
					Reporter.log(
							"Existing technology account is copied when the user clicks on the modification button is created passed");
				} else {
					System.out.println(
							"Existing technology account is copied when the user clicks on the modification button is created failed");
					Reporter.log(
							"Existing technology account is copied when the user clicks on the modification button is created failed");
					Assert.fail();
				}
				driver.findElement(By.id("delete_button")).click();
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(5000);
			} catch (Exception e) {
				System.out.println(
						"Error in checking existing Contract, Billing and Technology accounts is copied when the user clicks "
								+ "on the modification button is created failed RTC_Contract_095/RTC_Contract_096/RTC_Contract_097 fail:\t"
								+ e);
				Assert.fail(
						"Error in checking existing Contract, Billing and Technology accounts is copied when the user clicks "
								+ "on the modification button is created failed RTC_Contract_095/RTC_Contract_096/RTC_Contract_097 fail:\t"
								+ e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_103")) {
			try {
				CC.ViewContractNavigation();
				// srn.getscreenshot();
				EC.selectContractIdFrom2ndRow(ContractId);
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// srn.getscreenshot();
				if (driver
						.findElement(By
								.xpath(".//*[@id='subpanel_title_contract_version_history']/table/tbody/tr/td[1]/h3/span"))
						.isDisplayed()) {
					System.out.println("Revision history of a contract is displayed in details screen");
					Reporter.log("Revision history of a contract is displayed in details screen");
				} else {
					System.out.println(
							"Revision history of a contract is not displayed in details screen - RTC-103 fail");
					Assert.fail("Revision history of a contract is not displayed in details screen - RTC-103 fail");
				}
			} catch (Exception e) {
				System.out.println("Error in checking revision history of a contract - RTC-103 fail:\t" + e);
				Assert.fail("Error in checking revision history of a contract - RTC-103 fail:\t" + e);
			}
		} else if (Test_CaseId.equalsIgnoreCase("RTC_061/RTC_115")) {
			try {
				CC.ViewContractNavigation();
				CC.SelectContractId(ContractId);
				// srn.getscreenshot();
				Thread.sleep(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				srn.getscreenshot();
				driver.findElement(By.xpath(".//*[@id='arrow-sr-1']")).click();
				// srn.getscreenshot();
				if (driver.findElement(By.xpath(".//*[@id='arrow-sr-1']")).isDisplayed()) {
					System.out.println(
							"User should able to view the Edit button for Sales representative in the Contract detail screen");
					Reporter.log(
							"User should able to view the Edit button for Sales representative in the Contract detail screen");
				} else {
					System.out.println(
							"User should not able to view the Edit button for Sales representative in the Contract detail screen - RTC-115 fail");
					Assert.fail(
							"User should not able to view the Edit button for Sales representative in the Contract detail screen - RTC-115 fail");
				}
				if (driver.findElement(By.xpath(".//*[@id='btn_view_change_log']")).isDisplayed()) {
					System.out.println(
							"User should able to view the View Change Log button for Sales representative in the Contract detail screen");
					Reporter.log(
							"User should able to view the View Change Log button for Sales representative in the Contract detail screen");
				} else {
					System.out.println(
							"User should not able to view the View Change Log button for Sales representative in the Contract detail screen - RTC-115 fail");
					Assert.fail(
							"User should not able to view the View Change Log button for Sales representative in the Contract detail screen - RTC-115 fail");
				}
				if (driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[2]/div[4]/div/span/a")).isDisplayed()) {
					// srn.getscreenshot();
					driver.findElement(By.xpath(".//*[@id='li-1']/div[2]/div[2]/div[4]/div/span/a")).click();
					Thread.sleep(10000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
					// srn.getscreenshot();
					if (driver.findElement(By.xpath(".//*[@id='name']")).isDisplayed()) {
						System.out.println("Group Name in Invoice subtotal group is displayed");
						Reporter.log("Group Name in Invoice subtotal group is displayed");
					} else {
						System.out.println("Group Name in Invoice subtotal group is not displayed - RTC-061 fail");
						Assert.fail("Group Name in Invoice subtotal group is not displayed - RTC-061 fail");
					}
					if (driver.findElement(By.xpath(".//*[@id='description']")).isDisplayed()) {
						System.out.println("Description in Invoice subtotal group is displayed");
						Reporter.log("Description in Invoice subtotal group is displayed");
					} else {
						System.out.println("Description in Invoice subtotal group is not displayed - RTC-061 fail");
						Assert.fail("Description in Invoice subtotal group is not displayed - RTC-061 fail");
					}
					if (driver
							.findElement(By
									.xpath(".//*[@id='ndb_contract_details_tpl']/div/ul/li/div/div[1]/div[2]/div/span"))
							.isDisplayed()) {
						System.out.println("Global Contract ID in Invoice subtotal group is displayed");
						Reporter.log("Global Contract ID in Invoice subtotal group is displayed");
					} else {
						System.out.println(
								"Global Contract ID in Invoice subtotal group is not displayed - RTC-061 fail");
						Assert.fail("Global Contract ID in Invoice subtotal group is not displayed - RTC-061 fail");
					}
					if (driver
							.findElement(By
									.xpath(".//*[@id='ndb_contract_details_tpl']/div/ul/li/div/div[1]/div[4]/div/span"))
							.isDisplayed()) {
						System.out.println("Version in Invoice subtotal group is displayed");
						Reporter.log("Version in Invoice subtotal group is displayed");
					} else {
						System.out.println("Version in Invoice subtotal group is not displayed - RTC-061 fail");
						Assert.fail("Version in Invoice subtotal group is not displayed - RTC-061 fail");
					}
					if (driver
							.findElement(By
									.xpath(".//*[@id='ndb_contract_details_tpl']/div/ul/li/div/div[2]/div[2]/div/span"))
							.isDisplayed()) {
						System.out.println("Status in Invoice subtotal group is displayed");
						Reporter.log("Status in Invoice subtotal group is displayed");
					} else {
						System.out.println("Status in Invoice subtotal group is not displayed - RTC-061 fail");
						Assert.fail("Status in Invoice subtotal group is not displayed - RTC-061 fail");
					}
					if (driver
							.findElement(By
									.xpath(".//*[@id='ndb_contract_details_tpl']/div/ul/li/div/div[2]/div[4]/div/span/a"))
							.isDisplayed()) {
						System.out.println("Name in Invoice subtotal group is displayed");
						Reporter.log("Name in Invoice subtotal group is displayed");
					} else {
						System.out.println("Name in Invoice subtotal group is not displayed - RTC-061 fail");
						Assert.fail("Name in Invoice subtotal group is not displayed - RTC-061 fail");
					}
					if (driver
							.findElement(By
									.xpath(".//*[@id='ndb_contract_details_tpl']/div/ul/li/div/div[3]/div[2]/div/span/a"))
							.isDisplayed()) {
						System.out.println("Contracting Company in Invoice subtotal group is displayed");
						Reporter.log("Contracting Company in Invoice subtotal group is displayed");
					} else {
						System.out.println(
								"Contracting Company in Invoice subtotal group is not displayed - RTC-061 fail");
						Assert.fail("Contracting Company in Invoice subtotal group is not displayed - RTC-061 fail");
					}
					if (driver
							.findElement(By
									.xpath(".//*[@id='ndb_contract_details_tpl']/div/ul/li/div/div[3]/div[4]/div/span"))
							.isDisplayed()) {
						System.out.println("Product Offering in Invoice subtotal group is displayed");
						Reporter.log("GroupProduct Offering in Invoice subtotal group is displayed");
					} else {
						System.out
								.println("Product Offering in Invoice subtotal group is not displayed - RTC-061 fail");
						Assert.fail("Product Offering in Invoice subtotal group is not displayed - RTC-061 fail");
					}
					if (driver
							.findElement(By
									.xpath(".//*[@id='ndb_contract_details_tpl']/div/ul/li/div/div[4]/div[2]/div/span"))
							.isDisplayed()) {
						System.out.println("Date Created in Invoice subtotal group is displayed");
						Reporter.log("Date Created in Invoice subtotal group is displayed");
					} else {
						System.out.println("Date Created in Invoice subtotal group is not displayed - RTC-061 fail");
						Assert.fail("Date Created in Invoice subtotal group is not displayed - RTC-061 fail");
					}
					if (driver
							.findElement(By
									.xpath(".//*[@id='ndb_contract_details_tpl']/div/ul/li/div/div[4]/div[4]/div/span"))
							.isDisplayed()) {
						System.out.println("Date Modified in Invoice subtotal group is displayed");
						Reporter.log("Date Modified in Invoice subtotal group is displayed");
					} else {
						System.out.println("Date Modified in Invoice subtotal group is not displayed - RTC-061 fail");
						Assert.fail("Date Modified in Invoice subtotal group is not displayed - RTC-061 fail");
					}
					if (driver.findElements(By.xpath(".//*[@id='li_list_items']/tr/td[2]")).size() != 0) {
						System.out.println("Contract Line Items in Invoice subtotal group is displayed");
						Reporter.log("Contract Line Items in Invoice subtotal group is displayed");
					} else {
						System.out.println(
								"Contract Line Items in Invoice subtotal group is not displayed - RTC-061 fail");
						Assert.fail("Contract Line Items in Invoice subtotal group is not displayed - RTC-061 fail");
					}
				} else {
					System.out.println("Invoice subtotal group link not available - RTC-061 fail");
				}
			} catch (Exception e) {
				System.out.println(
						"Error in checking Edit and View Change Log buttons for Sales representative in the Contract detail screen "
								+ "and  Invoice subtotal group fields check failed - RTC-061/RTC_115 fail:\t" + e);
				Assert.fail(
						"Error in checking Edit and View Change Log buttons for Sales representative in the Contract detail screen "
								+ "and  Invoice subtotal group fields check failed - RTC-061/RTC_115 fail:\t" + e);
			}

		} else if (Test_CaseId.equalsIgnoreCase("RTC_092")) {
			try {
				EC.ViewContractNavigationPop();
				Thread.sleep(3000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
				// srn.getscreenshot();
				driver.findElement(By.xpath(".//*[@id='advanced_search_link']")).click();
				Thread.sleep(2000);
				// srn.getscreenshot();
				driver.findElement(By.id("global_contract_id_advanced")).clear();
				driver.findElement(By.id("name_advanced")).clear();
				Select contractStatus = new Select(driver.findElement(By.id("contract_status_advanced")));
				contractStatus.selectByVisibleText("Deactivated");
				// srn.getscreenshot();
				driver.findElement(By.id("search_form_submit_advanced")).click();
				Thread.sleep(5000);
				// srn.getscreenshot();
				if (driver.findElements(By.xpath(".//*[@id='MassUpdate']/table/tbody/tr[3]/td[4]/b/a")).size() != 0) {
					System.out.println("User can able to see a deactivated contract in search view");
					Reporter.log("User can able to see a deactivated contract in search view");
				} else {
					System.out.println("User can not see a deactivated contract in search view - RTC-092 fail");
					Assert.fail("User can not see a deactivated contract in search view - RTC-092 fail");
				}
				driver.findElement(By.id("basic_search_link")).click();
				driver.findElement(By.id("search_form_submit")).click();
			} catch (Exception e) {
				System.out.println(
						"Error in checking user can see a deactivated contract in search view or not - RTC-092 fail:\t"
								+ e);
				Assert.fail(
						"Error in checking user can see a deactivated contract in search view or not - RTC-092 fail:\t"
								+ e);
			}
		}

		/* Added by deep - end */
	}

	// Sky commented the code

	/*
	 * public void ClearData(){
	 * 
	 * driver.findElement(By.id("name")).clear();
	 * driver.findElement(By.id("description")).clear();
	 * driver.findElement(By.id("btn_clr_pm_products_gc_contracts_1_name")).
	 * click();
	 * driver.findElement(By.id("btn_clr_accounts_gc_contracts_1_name")).click()
	 * ;
	 * driver.findElement(By.id("btn_clr_contacts_gc_contracts_1_name")).click()
	 * ; driver.findElement(By.id("factory_reference_no")).clear();
	 * driver.findElement(By.id("sales_channel_code")).clear();
	 * driver.findElement(By.id("btn_clr_corporate_name")).click();
	 * driver.findElement(By.id("division_1")).clear();
	 * driver.findElement(By.id("division_2")).clear();
	 * driver.findElement(By.id("title_1")).clear();
	 * driver.findElement(By.id("title_2")).clear();
	 * driver.findElement(By.id("tel_no")).clear();
	 * driver.findElement(By.id("ext_no")).clear();
	 * driver.findElement(By.id("fax_no")).clear();
	 * driver.findElement(By.id("mob_no")).clear();
	 * driver.findElement(By.id("email1")).clear();
	 * driver.findElement(By.id("btn_clr_product_name_1")).click();
	 * driver.findElement(By.id("btn_clr_tech_account_name_1")).click();
	 * driver.findElement(By.id("btn_clr_bill_account_name_1")).click();
	 * 
	 * String payment;
	 * 
	 * payment = driver.findElement(By.id("payment_type_1")).getText();
	 * if(payment.equalsIgnoreCase("General: Direct Deposit")){
	 * 
	 * driver.findElement(By.id("dd_bank_code_1")).clear();
	 * driver.findElements(By.id("dd_branch_code_1")).clear();
	 * driver.findElement(By.id("dd_account_no_1")).clear();
	 * driver.findElement(By.id("dd_person_name_1_1")).clear();
	 * driver.findElement(By.id("dd_person_name_2_1")).clear();
	 * driver.findElement(By.id("dd_payee_tel_no_1")).clear();
	 * driver.findElement(By.id("dd_payee_email_address_1")).clear();
	 * driver.findElement(By.id("dd_remarks_1")).clear();
	 * 
	 * }else if(payment.equalsIgnoreCase("Account")){
	 * driver.findElement(By.id("at_name_1")).clear();
	 * driver.findElement(By.id("at_bank_code_1")).clear();
	 * driver.findElement(By.id("at_branch_code_1")).clear();
	 * driver.findElement(By.id("at_account_no_1")).clear();
	 * 
	 * 
	 * }else if(payment.equalsIgnoreCase("Postal Transfer")){
	 * 
	 * driver.findElement(By.id("pt_name_1")).clear();
	 * driver.findElement(By.id("pt_postal_passbook_mark_1")).clear();
	 * driver.findElement(By.id("pt_postal_passbook_1")).clear();
	 * 
	 * 
	 * }else if(payment.equalsIgnoreCase("Credit Card")){
	 * 
	 * driver.findElement(By.id("cc_credit_card_no_1")).clear();
	 * driver.findElement(By.id("cc_expiration_date_1")).clear();
	 * 
	 * 
	 * }else{
	 * 
	 * Reporter.log("Invalid paymenttype"+payment); }
	 * 
	 * 
	 * }
	 */

	// Code by sky
	public void SelectContractEdit(String ContractName) throws Exception {
		{

			Thread.sleep(3000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.id("global_contract_id_basic")).clear();
			driver.findElement(By.id("name_basic")).clear();
			driver.findElement(By.id("name_basic")).sendKeys(ContractName);
			driver.findElement(By.id("search_form_submit")).click();
			driver.findElement(By.xpath("//table/tbody/tr[3]/td[4]/b/a")).click();
			Thread.sleep(10000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.xpath(".//*[@id='edit_button']")).click();
			Thread.sleep(10000);
			// srn.getscreenshot();*/
		}
	}

	public void ViewContract(String ContractName) throws Exception {
		{

			Thread.sleep(3000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			driver.findElement(By.id("global_contract_id_basic")).clear();
			driver.findElement(By.id("name_basic")).clear();
			driver.findElement(By.id("name_basic")).sendKeys(ContractName);
			driver.findElement(By.id("search_form_submit")).click();
			driver.findElement(By.xpath("//table/tbody/tr[3]/td[4]/b/a")).click();
			Thread.sleep(10000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			Thread.sleep(10000);
			// srn.getscreenshot();*/
		}
	}

	public void EditProductOffering() throws Exception {
		driver.findElement(By.xpath(".//*[@id='product_offering']")).click();

		Select oSelect = new Select(driver.findElement(By.xpath(".//*[@id='product_offering']")));

		oSelect.selectByVisibleText("Global Management One ECL2.0 Option");
		// driver.findElement(By.xpath(".//*[@id='product_offering']")).sendKeys("Global
		// Management One ECL2.0 Option");

		System.out.println("Product Offering field editable");
		Thread.sleep(10000);
	}

	public void EditProductSpecification() throws Exception {
		try {
			// Waits for 20 seconds
			WebDriverWait wait01 = new WebDriverWait(driver, 20);

			// Wait until expected condition size of the dropdown increases and
			// becomes more than 1
			wait01.until((ExpectedCondition<Boolean>) new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver)

				{
					driver.findElement(By.xpath(".//*[@id='product_specification_1']")).click();
					Select select = new Select(driver.findElement(By.xpath(".//*[@id='product_specification_1']")));
					return select.getOptions().size() > 1;
				}
			});

			// To select the first option
			Select select = new Select(driver.findElement(By.xpath(".//*[@id='product_specification_1']")));
			select.selectByVisibleText("GMOne ECL2.0 Option Base Specification (Base)");
		} catch (Throwable e) {
			System.out.println("Error found: " + e.getMessage());
		}

		System.out.println("Product Specification for 1st line item selected");

		try {
			// Waits for 20 seconds
			WebDriverWait wait02 = new WebDriverWait(driver, 20);

			// Wait until expected condition size of the dropdown increases and
			// becomes more than 1
			wait02.until((ExpectedCondition<Boolean>) new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					driver.findElement(By.xpath(".//*[@id='product_specification_2']")).click();
					Select select = new Select(driver.findElement(By.xpath(".//*[@id='product_specification_2']")));
					return select.getOptions().size() > 1;
				}
			});

			// To select the first option
			Select select = new Select(driver.findElement(By.xpath(".//*[@id='product_specification_2']")));
			// select.selectByVisibleText("Managed Windows Server - on ECL2.0
			// (Add-on Optional)");
			select.selectByIndex(2);
		} catch (Throwable e) {
			System.out.println("Error found: " + e.getMessage());
		}

		System.out.println("Product Specification for 2nd line item selected");
		System.out.println("Product Specification field editable");
		Thread.sleep(10000);

	}

	// code for product price rule validation
	public void EditProductInformationRule() throws Exception {

		driver.findElement(By.xpath(".//*[@id='product_config_2_563']")).click();

		Select oSelect = new Select(driver.findElement(By.xpath(".//*[@id='product_config_2_563']")));

		oSelect.selectByVisibleText("UNIT");
		System.out.println("Pricing Rule selected");
	}

	/// code for product price information
	public void EditProductPriceInformation() throws Exception {

		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		if (driver.findElement(By.xpath(".//*[@id='product_price_1_160460']/table/tbody/tr[5]/td[2]")).isDisplayed()) {
			System.out.println(
					"Product Price-List Price of Monthly Unit Price per Unit of Additional Standard Change is displayed");
		} else {
			System.out.println(
					"Product Price-List Price of Monthly Unit Price per Unit of Additional Standard Change is invisible");
		}
		WebElement TxtBoxContent = driver
				.findElement(By.xpath(".//*[@id='product_price_1_160460']/table/tbody/tr[5]/td[2]"));
		String Txt1 = TxtBoxContent.getText();
		System.out.println("Printing " + Txt1);

		if (driver.findElement(By.xpath(".//*[@id='product_price_1_160459']/table/tbody/tr[5]/td[2]")).isDisplayed()) {
			System.out.println("Product Price-List Price of Minimum Monthly Charges is visible");
		} else {
			System.out.println("Product Price-List Price of Minimum Monthly Charges is invisible");
		}
		WebElement TxtBoxContent1 = driver
				.findElement(By.xpath(".//*[@id='product_price_1_160459']/table/tbody/tr[5]/td[2]"));
		String Txt = TxtBoxContent1.getText();
		System.out.println("Printing " + Txt);
	}

	// Code for edit discount field percentage

	public void EditDiscountFieldPercentage(String DiscountPercentage1, String DiscountPercentage2) {
		// TODO Auto-generated method stub
		try {
			driver.findElement(By.id("pp_discount_1_160459")).clear();
			driver.findElement(By.id("pp_discount_1_160459")).sendKeys(DiscountPercentage1);

			driver.findElement(By.id("pp_discount_1_160460")).clear();
			driver.findElement(By.id("pp_discount_1_160460")).sendKeys(DiscountPercentage2);

			System.out.println("Discount Percentage Value entered for 1st Line Item");
			System.out.println("Discount Field Percentage is editable");

		}

		catch (Exception e) {
			Reporter.log("Error in checking discount is text field with checkbox" + e);
			e.printStackTrace();
			Assert.fail();
		}
	}

	// Code for edit discount field price

	public void EditDiscountFieldPrice(String DiscountPrice1, String DiscountPrice2) {
		// TODO Auto-generated method stub
		try {
			driver.findElement(By.id("pp_discount_2_160461")).clear();
			driver.findElement(By.id("pp_discount_2_160461")).sendKeys(DiscountPrice1);

			driver.findElement(By.id("pp_discount_2_160462")).clear();
			driver.findElement(By.id("pp_discount_2_160462")).sendKeys(DiscountPrice2);

			System.out.println("Discount Field Price Value entered for 2nd Line Item ");
			System.out.println("Discount Field Percentage is editable");
		}

		catch (Exception e) {
			Reporter.log("Error in checking discount is text field with checkbox" + e);
			e.printStackTrace();
			Assert.fail();
		}
	}

	public void EditContractPrice() {
		String contractPrice1 = driver.findElement(By.xpath(".//*[@id='spn_customer_contract_price_2_160461']"))
				.getText();

		System.out.println("Customer contract price for Minimum Monthly Charges " + contractPrice1);

		if (contractPrice1.equals("0.000")) {
			Reporter.log("100% Discount applied for Minimum Monthly Charges");
			System.out.println("100% Discount applied for Minimum Monthly Charges");
		} else {
			Reporter.log("Contract price displayed for Minimum Monthly Charges");
			System.out.println("Contract price displayed for Minimum Monthly Charges");
		}
		String contractPrice2 = driver.findElement(By.xpath(".//*[@id='spn_customer_contract_price_2_160462']"))
				.getText();

		System.out.println("Customer contract price per Unit of Additional Standard Change " + contractPrice2);

		if (contractPrice2.equals("0.000")) {
			Reporter.log("100% Discount applied for Monthly Unit Price per Unit of Additional Standard Change");
			System.out.println("100% Discount applied for Monthly Unit Price per Unit of Additional Standard Change");
		} else {
			Reporter.log("Contract price displayed for Monthly Unit Price per Unit of Additional Standard Change");
			System.out
					.println("Contract price displayed for Monthly Unit Price per Unit of Additional Standard Change");
		}

	}

	public void EditIGCPrice(String IGCSettlementPrice1, String IGCSettlementPrice2) {

		driver.findElement(By.id("pp_igc_settlement_price_1_160460")).clear();

		driver.findElement(By.id("pp_igc_settlement_price_1_160460")).sendKeys(IGCSettlementPrice1);

		if (driver.findElement(By.id("pp_igc_settlement_price_1_160460")).isEnabled()) {
			Reporter.log(
					"IGC settlement  Price for Monthly Unit Price per Unit of Additional Standard Change is editable");

		} else {
			Assert.fail(
					"IGC settlement  price for Monthly Unit Price per Unit of Additional Standard Changenot editable");
		}

		driver.findElement(By.id("pp_igc_settlement_price_1_160459")).clear();

		driver.findElement(By.id("pp_igc_settlement_price_1_160459")).sendKeys(IGCSettlementPrice2);

		if (driver.findElement(By.id("pp_igc_settlement_price_1_160459")).isEnabled()) {
			Reporter.log("IGC settlement  Price for Minimum Monthly Charges is editable");

		} else {
			Assert.fail("IGC settlement  price for Minimum Monthly Charges not editable");
		}

	}

	// From Contract Header

	public void ContractStatus() {
		driver.switchTo().defaultContent();
		// driver.switchTo().frame("bwc-frame");
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		WebElement TxtBoxContent001 = driver.findElement(By.xpath(".//*[@id='DEFAULT']/tbody/tr[3]/td[2]"));
		String Txt001 = TxtBoxContent001.getText();
		System.out.println("Printing contract status:" + Txt001);
	}

	// From Contract Header

	// Code for file upload
	// Code by Sky
	public String Fileupload() throws AWTException, InterruptedException {

		String fullPath = "C:\\Users\\Public\\Pictures\\Sample Pictures\\Koala.jpg";
		int index = fullPath.lastIndexOf("\\");
		String fileName = fullPath.substring(index + 1);
		System.out.println("Printing File Name before Upload" + fileName);

		// Specify the file location with extension
		StringSelection sel = new StringSelection("C:\\Users\\Public\\Pictures\\Sample Pictures\\Koala.jpg");

		// Copy to clipboard
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel, null);
		System.out.println("selection" + sel);
		// Wait for 5 seconds
		Thread.sleep(5000);

		// This will click on Browse button
		driver.findElement(By.id("filename_file")).click();
		System.out.println("Browse button clicked");

		// Create object of Robot class
		Robot robot = new Robot();
		Thread.sleep(1000);

		// Press Enter
		robot.keyPress(KeyEvent.VK_ENTER);

		// Release Enter
		robot.keyRelease(KeyEvent.VK_ENTER);

		// Press CTRL+V
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		// Release CTRL+V
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(10000);

		// Press Enter
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(10000);
		System.out.println("File Uploaded successfully");
		return fileName;
	}

	public void MaxUploadSizeDocumentModule(String DocumentDescription2) throws InterruptedException {

		try {
			Thread.sleep(5000);
			driver.findElement(
					By.xpath(".//*[@id='list_subpanel_gc_lineitem_documents_rowid']/table/thead/tr[1]/td/div/ul"))
					.click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}
			CheckFileuploadSize();
			driver.navigate().refresh();
			driver.close();
			driver.switchTo().window(parentHandle1);
			// driver.switchTo().frame("bwc-frame");
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

		} catch (Exception e) {
			System.out.println("Error in validation of maximum size file upload" + e);
			Assert.fail("Error in validation of maximum size file upload" + e);
		}
	}
	// Code to check maximum size of file upload

	public void CheckFileuploadSize() throws AWTException, InterruptedException {

		try {
			StringSelection sel1 = new StringSelection("D:\\Software\\eclipse-java-helios-SR1-win32-x86_64");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel1, null);
			System.out.println("selection" + sel1);
			// Wait for 5 seconds
			Thread.sleep(5000);

			// This will click on Browse button
			driver.findElement(By.id("filename_file")).click();
			System.out.println("Browse button clicked");

			// Create object of Robot class
			Robot robot = new Robot();
			Thread.sleep(1000);

			// Press Enter
			robot.keyPress(KeyEvent.VK_ENTER);

			// Release Enter
			robot.keyRelease(KeyEvent.VK_ENTER);

			// Press CTRL+V
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);

			// Release CTRL+V
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);
			Thread.sleep(10000);

			// Press Enter
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(10000);
			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(10000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}
			WebElement TxtBoxContent11 = driver.findElement(By.xpath(".//*[@id='sugarMsgWindow']/div[2]"));
			String Txt11 = TxtBoxContent11.getText();
			System.out.println("Printing " + Txt11);
			driver.findElement(By.xpath(".//*[@id='yui-gen0-button']")).click();
		}

		catch (Exception e) {
			Assert.fail("File upload failed because of maximum size" + e);
			System.out.println("File upload failed because of maximum size");
		}

	}

	public void CheckVisibilityOfElement() {
		try {
			if (driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]"))
					.isDisplayed()) {
				System.out.println("Open All is Visible");
				Reporter.log("Open All is Visible");
			} else {
				System.out.println("Open All is InVisible");
				Assert.fail("Open All is InVisible");
			}

			if (driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[2]"))
					.isDisplayed()) {
				System.out.println("Close All is Visible");
				Reporter.log("Close All is Visible");
			} else {
				System.out.println("Close All  is InVisible");
				Assert.fail("Close All  is InVisible");
			}
		} catch (Exception e) {
			System.out.println("Error in checking Open All and Close All buttons are visible or not failed " + e);
			Assert.fail("Error in checking Open All and Close All buttons are visible or not failed " + e);
		}

	}

	// Code by Sky
	public void RemoveDocumentModule() {

		try {

			// ***Get No.of records before removing document*
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			List<WebElement> FName = driver.findElements(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[3]/a"));
			if (FName.isEmpty()) {
				System.out.println("There is no document attached to remove");
			} else {

				List<WebElement> elements1 = driver.findElements(
						By.xpath("//div[@id='list_subpanel_gc_lineitem_documents_rowid']/table[1]//tbody/tr"));
				System.out.println("Test7 number of elements: " + elements1.size());

				List<WebElement> rows = driver
						.findElements(By.xpath(".//table[@class='list view']/tbody[@id='contract_doc_list_items']/tr"));
				System.out.println(
						"Total number of records before removing the document in Contract Header :" + rows.size());

				// Getting Document Name
				WebElement TxtBox1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]/a"));
				String Tx1 = TxtBox1.getText();

				// Getting File Name
				WebElement TxtBox2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[3]/a"));
				String Tx2 = TxtBox2.getText();

				// Getting Revision Name
				WebElement TxtBox3 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[4]"));
				String Tx3 = TxtBox3.getText();

				// Getting Document Type
				WebElement TxtBox4 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
				String Tx4 = TxtBox4.getText();

				// Getting Category
				WebElement TxtBox5 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
				String Tx5 = TxtBox5.getText();

				System.out.println("Printing Document Name of the Document to be Removed: " + Tx1);
				System.out.println("Printing File Name of the Document to be Removed:" + Tx2);
				System.out.println("Printing Revision Name of the Document to be Removed:" + Tx3);
				System.out.println("Printing Document Type of the Document to be Removed:" + Tx4);
				System.out.println("Printing Category Name of the Document to be Removed:" + Tx5);

				driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[8]/ul/li/a")).click();
				Thread.sleep(5000);

				// Switching to Alert
				Alert alert = driver.switchTo().alert();

				// Capturing alert message.
				String alertMessage = driver.switchTo().alert().getText();

				// Displaying alert message
				System.out.println(alertMessage);

				// Accepting alert
				alert.accept();

				// System.out.println("OK");
				System.out.println("Document removed from document module");

				List<WebElement> rowscount = driver
						.findElements(By.xpath(".//table[@class='list view']/tbody[@id='contract_doc_list_items']/tr"));

				if (rowscount.size() > 1)

				{

					System.out.println("Total number of records after removing the document in Contract Header :"
							+ rowscount.size());

					/*
					 * List<WebElement> elements2 =
					 * driver.findElements(By.xpath(
					 * "//div[@id='list_subpanel_gc_lineitem_documents_rowid']/table[1]//tbody/tr"
					 * )); System.out.println("Test7 number of elements: " +
					 * elements2.size());
					 */

					Assert.assertTrue(true, "Document Removed Successfully!!");
				}

				else {
					System.out.println("Total number of records after removing the document in Contract Header :"
							+ (rowscount.size() - 1));

				}

			}

		} catch (Exception e) {

			e.printStackTrace();
			Assert.fail();
		}

	}

	public void RemoveDocumentModuleWF() {

		try {

			// ***Get No.of records before removing document*

			List<WebElement> F2Name = driver.findElements(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[3]"));
			if (F2Name.isEmpty()) {
				System.out.println("There is no document attached to remove");
			} else {

				// Getting File Name

				List<WebElement> elements1 = driver.findElements(
						By.xpath("//div[@id='list_subpanel_gc_lineitem_documents_rowid']/table[1]//tbody/tr"));
				System.out.println("Test7 number of elements: " + elements1.size());

				List<WebElement> rows = driver
						.findElements(By.xpath(".//table[@class='list view']/tbody[@id='contract_doc_list_items']/tr"));
				System.out.println(
						"Total number of records before removing the document in Contract Header :" + rows.size());

				// Getting Document Name
				WebElement TxtBox1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
				String Tx1 = TxtBox1.getText();

				// Getting File Name
				WebElement TxtBox2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[3]"));
				String Tx2 = TxtBox2.getText();

				// Getting Revision Name
				WebElement TxtBox3 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[4]"));
				String Tx3 = TxtBox3.getText();

				// Getting Document Type
				WebElement TxtBox4 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
				String Tx4 = TxtBox4.getText();

				// Getting Category
				WebElement TxtBox5 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
				String Tx5 = TxtBox5.getText();

				System.out.println("Printing Document Name of the Document to be Removed: " + Tx1);
				System.out.println("Printing File Name of the Document to be Removed:" + Tx2);
				System.out.println("Printing Revision Name of the Document to be Removed:" + Tx3);
				System.out.println("Printing Document Type of the Document to be Removed:" + Tx4);
				System.out.println("Printing Category Name of the Document to be Removed:" + Tx5);

				driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[8]/ul/li/a")).click();
				Thread.sleep(10000);

				// Switching to Alert
				Alert alert = driver.switchTo().alert();

				// Capturing alert message.
				String alertMessage = driver.switchTo().alert().getText();

				// Displaying alert message
				System.out.println(alertMessage);

				// Accepting alert
				alert.accept();

				// System.out.println("OK");
				System.out.println("Document removed from document module");

				List<WebElement> rowscount1 = driver
						.findElements(By.xpath(".//table[@class='list view']/tbody[@id='contract_doc_list_items']/tr"));

				if (rowscount1.size() > 1)

				{

					System.out.println("Total number of records after removing the document in Contract header :"
							+ rowscount1.size());

					/*
					 * List<WebElement> elements2 =
					 * driver.findElements(By.xpath(
					 * "//div[@id='list_subpanel_gc_lineitem_documents_rowid']/table[1]//tbody/tr"
					 * )); System.out.println("Test7 number of elements: " +
					 * elements2.size());
					 */

					Assert.assertTrue(true, "Document Removed Successfully!!");
				}

				else {
					System.out.println("Total number of records after removing the document in Contract Header:"
							+ (rowscount1.size() - 1));

				}

			}
		}

		catch (Exception e) {
			Reporter.log("Error in removing document" + e);
			Assert.fail("Error in removing document" + e);
		}

	}

	// Code by Sky
	public void RemoveDocumentModuleLI() {

		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			List<WebElement> F1Name = driver.findElements(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[3]/a"));
			if (F1Name.isEmpty()) {
				System.out.println("There is no document attached to remove");
			} else {

				// ***Get No.of records before removing document*

				List<WebElement> rowsL = driver
						.findElements(By.xpath(".//table[@class='list view']/tbody[@id='li_doc_list_items_1']/tr"));
				System.out
						.println("Total number of records before removing the document in Line Items :" + rowsL.size());

				// Getting Document Name
				WebElement TxtBox1L = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]/a"));
				String Tx1L = TxtBox1L.getText();

				// Getting File Name
				WebElement TxtBox2L = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[3]/a"));
				String Tx2L = TxtBox2L.getText();

				// Getting Revision Name
				WebElement TxtBox3L = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[4]"));
				String Tx3L = TxtBox3L.getText();

				// Getting Document Type
				WebElement TxtBox4L = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
				String Tx4L = TxtBox4L.getText();

				// Getting Category
				WebElement TxtBox5L = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
				String Tx5L = TxtBox5L.getText();

				System.out.println("Printing Document Name of the Document to be Removed: " + Tx1L);
				System.out.println("Printing File Name of the Document to be Removed:" + Tx2L);
				System.out.println("Printing Revision Name of the Document to be Removed:" + Tx3L);
				System.out.println("Printing Document Type of the Document to be Removed:" + Tx4L);
				System.out.println("Printing Category Name of the Document to be Removed:" + Tx5L);
				Thread.sleep(5000);
				driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[8]/ul/li/a")).click();
				Thread.sleep(5000);

				// Switching to Alert
				Alert alert = driver.switchTo().alert();

				// Capturing alert message.
				String alertMessage = driver.switchTo().alert().getText();

				// Displaying alert message
				System.out.println(alertMessage);

				// Accepting alert
				alert.accept();

				// System.out.println("OK");
				System.out.println("Document removed from document module");

				List<WebElement> rowsL1 = driver
						.findElements(By.xpath(".//table[@class='list view']/tbody[@id='li_doc_list_items_1']/tr"));

				if (rowsL1.size() > 1)

				{

					System.out.println(
							"Total number of records after removing the document in Line Items :" + rowsL1.size());
					Assert.assertTrue(true, "Document Removed Successfully!!");

					/*
					 * List<WebElement> elements2 =
					 * driver.findElements(By.xpath(
					 * "//div[@id='list_subpanel_gc_lineitem_documents_rowid']/table[1]//tbody/tr"
					 * )); System.out.println("Test7 number of elements: " +
					 * elements2.size());
					 */

				}

				else {
					System.out.println(
							"Total number of records after removing the document in Line Item :" + (rowsL1.size() - 1));

				}

			}
		} catch (Exception e) {
			Reporter.log("Error in removing document" + e);
			e.printStackTrace();
			Assert.fail();
		}

	}

	public void RemoveDocumentModuleLIWF() {

		try {

			// ***Get No.of records before removing document*

			List<WebElement> F3Name = driver.findElements(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[3]"));
			if (F3Name.isEmpty()) {
				System.out.println("There is no document attached to remove");
			} else {

				List<WebElement> rowsL = driver
						.findElements(By.xpath(".//table[@class='list view']/tbody[@id='li_doc_list_items_1']/tr"));
				System.out
						.println("Total number of records before removing the document in Line Items :" + rowsL.size());

				// Getting Document Name
				WebElement TxtBox1L = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
				String Tx1L = TxtBox1L.getText();

				// Getting File Name
				WebElement TxtBox2L = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[3]"));
				String Tx2L = TxtBox2L.getText();

				// Getting Revision Name
				WebElement TxtBox3L = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[4]"));
				String Tx3L = TxtBox3L.getText();

				// Getting Document Type
				WebElement TxtBox4L = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
				String Tx4L = TxtBox4L.getText();

				// Getting Category
				WebElement TxtBox5L = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
				String Tx5L = TxtBox5L.getText();

				System.out.println("Printing Document Name of the Document to be Removed: " + Tx1L);
				System.out.println("Printing File Name of the Document to be Removed:" + Tx2L);
				System.out.println("Printing Revision Name of the Document to be Removed:" + Tx3L);
				System.out.println("Printing Document Type of the Document to be Removed:" + Tx4L);
				System.out.println("Printing Category Name of the Document to be Removed:" + Tx5L);

				driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[8]/ul/li/a")).click();
				Thread.sleep(10000);

				// Switching to Alert
				Alert alert = driver.switchTo().alert();

				// Capturing alert message.
				String alertMessage = driver.switchTo().alert().getText();

				// Displaying alert message
				System.out.println(alertMessage);

				// Accepting alert
				alert.accept();

				// System.out.println("OK");
				System.out.println("Document removed from document module");

				List<WebElement> rowsL2 = driver
						.findElements(By.xpath(".//table[@class='list view']/tbody[@id='li_doc_list_items_1']/tr"));

				if (rowsL2.size() > 1)

				{

					System.out.println(
							"Total number of records after removing the document in Line Items :" + rowsL2.size());
					Assert.assertTrue(true, "Document Removed Successfully!!");

					/*
					 * List<WebElement> elements2 =
					 * driver.findElements(By.xpath(
					 * "//div[@id='list_subpanel_gc_lineitem_documents_rowid']/table[1]//tbody/tr"
					 * )); System.out.println("Test7 number of elements: " +
					 * elements2.size());
					 */

				}

				else {
					System.out.println(
							"Total number of records after removing the document in Line Item :" + (rowsL2.size() - 1));

				}

			}
		} catch (Exception e) {
			Reporter.log("Error in removing document" + e);
			e.printStackTrace();
			Assert.fail();
		}

	}

	public void CreateDocumentModule(String DocumentDescription1) throws InterruptedException {

		try {
			driver.findElement(
					By.xpath(".//*[@id='list_subpanel_gc_lineitem_documents_rowid']/table/thead/tr[1]/td/div/ul"))
					.click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}
			String fileName = Fileupload();

			WebElement TxtBoxContentFile = driver.findElement(By.xpath(".//*[@id='filename_file']"));
			String Txt4File = TxtBoxContentFile.getAttribute("value");
			System.out.println("Printing File name uploaded" + Txt4File);

			WebElement TxtBoxContent = driver.findElement(By.xpath(".//*[@id='document_name']"));
			String Txt4 = TxtBoxContent.getAttribute("value");
			System.out.println("Printing Document name from file upload window" + Txt4);
			WebElement TxtBoxContent1 = driver
					.findElement(By.xpath(".//*[@id='LBL_DOCUMENT_INFORMATION']/tbody/tr[3]/td[2]/input"));
			String Txt5 = TxtBoxContent1.getAttribute("value");
			System.out.println("Printing auto generated Revision Number from file uploaded window " + Txt5);
			/*
			 * try{
			 * 
			 * WebDriverWait wait05 = new WebDriverWait(driver, 100); By
			 * selectElementSelector = By.xpath(".//*[@id='category_id']");
			 * WebElement selectElement = wait05
			 * .until(ExpectedConditions.presenceOfElementLocated(
			 * selectElementSelector)); Select dropdown = new
			 * Select(selectElement); dropdown.selectByValue("Partner");
			 * 
			 * System.out.println(
			 * "Category field Chosen in file uploaded window");
			 * 
			 * } catch (Throwable e) { System.out.println(
			 * "Error found:Category field not Chosen in file uploaded window "
			 * + e.getMessage()); }
			 * 
			 * WebElement TxtBoxContentCategory =
			 * driver.findElement(By.xpath(".//*[@id='category_id']")); String
			 * TxtCategory = TxtBoxContentCategory.getText();
			 * System.out.println(
			 * "Printing Category Field from file uploaded window " +
			 * TxtCategory); Thread.sleep(10000);
			 * 
			 * try {
			 * 
			 * WebDriverWait wait06 = new WebDriverWait(driver, 100); By
			 * selectElementSelector = By.xpath(".//*[@id='template_type']");
			 * WebElement selectElement = wait06
			 * .until(ExpectedConditions.presenceOfElementLocated(
			 * selectElementSelector)); Select dropdown = new
			 * Select(selectElement); dropdown.selectByIndex(2);
			 * 
			 * System.out.println(
			 * "Document Type field Chosen in file uploaded window");
			 * 
			 * } catch (Throwable e) { System.out.println(
			 * "Error found:Document Type field not Chosen in file uploaded window "
			 * + e.getMessage()); }
			 * 
			 * WebElement TxtBoxContentDoc =
			 * driver.findElement(By.xpath(".//*[@id='template_type']")); String
			 * TxtDoc = TxtBoxContentDoc.getText(); System.out.println(
			 * "Printing Document Type field from file uploaded window " +
			 * TxtDoc);
			 */

			Thread.sleep(2000);
			Select se1 = new Select(driver.findElement(By.xpath(".//*[@id='category_id']")));
			se1.selectByValue("Customer");
			Thread.sleep(2000);

			String D1C = se1.getFirstSelectedOption().getText();
			System.out.println("Printing Category Field from file uploaded window " + D1C);

			Thread.sleep(2000);
			Select se11 = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));
			// se11.selectByVisibleText("Service Description");
			se11.selectByIndex(4);
			Thread.sleep(2000);

			String D2C = se11.getFirstSelectedOption().getText();
			System.out.println("Printing Document Type Field from file uploaded window " + D2C);

			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescription1);
			Thread.sleep(2000);

			WebElement TxtBoxContentDocDesc = driver.findElement(By.xpath(".//*[@id='description']"));
			String Txt4DocDesc = TxtBoxContentDocDesc.getAttribute("value");
			System.out.println("Printing Document Description from file upload window" + Txt4DocDesc);

			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			// driver.switchTo().frame("bwc-frame");
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String LabelA = Label1.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelA);

			WebElement Label2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[3]"));
			String LabelB = Label2.getText();
			System.out.println("Printing File Name  from Contract Detail Screen" + LabelB);

			WebElement Label3 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[4]"));
			String LabelC = Label3.getText();
			System.out.println("Printing Revision Number from Contract Detail Screen" + LabelC);

			WebElement Label4 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String LabelD = Label4.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelD);

			WebElement Label5 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String LabelE = Label5.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelE);

			// Assertions

			System.out.println("Filename" + fileName);
			System.out.println("FilenameB" + LabelB);

			if (fileName.equals(LabelB)) {
				Assert.assertTrue(TestCasePass, "File Name Validated");
			} else {
				Assert.fail("File Name not validated");

			}

			if (Txt4.equals(LabelA)) {
				Assert.assertTrue(TestCasePass, "Document Name Validated");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (Txt5.equals(LabelC)) {
				Assert.assertTrue(TestCasePass, "Auto Generated Revision Number");
			} else {
				Assert.fail("Auto Generated Revision Number not validated");

			}

			if (D1C.equals(LabelE)) {
				Assert.assertTrue(TestCasePass, "Category Name Validated");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (D2C.equals(LabelD)) {
				Assert.assertTrue(TestCasePass, "Document Type Validated");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in creating document" + e);
			e.printStackTrace();
			Assert.fail();
		}
	}

	public void CreateDocumentModuleLI(String DocumentDescription3) throws InterruptedException {
		try {
			Thread.sleep(20000);
			// Click Open All Button
			driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
			Thread.sleep(20000);
			// Click create button
			driver.findElement(
					By.xpath(".//*[@id='list_subpanel_gc_lineitem_documents_1']/table/thead/tr[1]/td/div/ul/li/a"))
					.click();
			String parentHandle10 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)
			}
			String fileName = Fileupload();

			WebElement TxtBoxContentFileLi = driver.findElement(By.xpath(".//*[@id='filename_file']"));
			String Txt4FileLi = TxtBoxContentFileLi.getAttribute("value");
			System.out.println("Printing File name uploaded" + Txt4FileLi);

			WebElement TxtBox6Li = driver.findElement(By.xpath(".//*[@id='document_name']"));
			String Txt6Li = TxtBox6Li.getAttribute("value");
			System.out.println("Printing Document name from file upload window" + Txt6Li);

			WebElement TxtBoxContent6Li = driver
					.findElement(By.xpath(".//*[@id='LBL_DOCUMENT_INFORMATION']/tbody/tr[3]/td[2]/input"));
			String Txt7Li = TxtBoxContent6Li.getAttribute("value");
			System.out.println("Printing Document name from file upload window" + Txt7Li);

			Thread.sleep(2000);
			Select se = new Select(driver.findElement(By.xpath(".//*[@id='category_id']")));
			se.selectByValue("Partner");
			Thread.sleep(2000);

			String D1 = se.getFirstSelectedOption().getText();
			System.out.println("Printing Category Field from file uploaded window " + D1);

			Thread.sleep(2000);
			Select se1 = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));
			// se1.selectByVisibleText("Service Description");
			// se1.selectByValue("Service Description");
			se1.selectByIndex(5);
			Thread.sleep(2000);

			String D2 = se1.getFirstSelectedOption().getText();
			System.out.println("Printing Document Type Field from file uploaded window " + D2);

			/*
			 * try { WebDriverWait wait07 = new WebDriverWait(driver, 100); By
			 * selectElementSelector7 = By.xpath(".//*[@id='category_id']");
			 * WebElement selectElement7 = wait07
			 * .until(ExpectedConditions.presenceOfElementLocated(
			 * selectElementSelector7)); Select dropdown7 = new
			 * Select(selectElement7); dropdown7.selectByValue("Partner");
			 * 
			 * String selectedValue =
			 * dropdown7.getFirstSelectedOption().getText();
			 * 
			 * System.out.println(
			 * "Printing Category Field from file uploaded window "
			 * +selectedValue);
			 * 
			 * System.out.println("Category field Chosen"); } catch (Throwable
			 * e) { System.out.println("Error found:Category field not Chosen "
			 * + e.getMessage()); }
			 * 
			 * 
			 * 
			 * 
			 * WebElement TxtBoxContentCategoryLi =
			 * driver.findElement(By.xpath(".//*[@id='category_id']")); String
			 * TxtCategoryLi = TxtBoxContentCategoryLi.getText();
			 * System.out.println(
			 * "Printing Category Field from file uploaded window " +
			 * TxtCategoryLi);
			 * 
			 * Thread.sleep(10000);
			 * 
			 * try { WebDriverWait wait08 = new WebDriverWait(driver, 100); By
			 * selectElementSelector8 = By.xpath(".//*[@id='template_type']");
			 * WebElement selectElement8 = wait08
			 * .until(ExpectedConditions.presenceOfElementLocated(
			 * selectElementSelector8)); Select dropdown8 = new
			 * Select(selectElement8); dropdown8.selectByIndex(2);
			 * System.out.println("Document Type field Chosen"); } catch
			 * (Throwable e) { System.out.println(
			 * "Error found:Document Type field not Chosen " + e.getMessage());
			 * }
			 * 
			 * WebElement TxtBoxContentDocLi =
			 * driver.findElement(By.xpath(".//*[@id='template_type']")); String
			 * TxtDocLi = TxtBoxContentDocLi.getText(); System.out.println(
			 * "Printing Document Type field from file uploaded window " +
			 * TxtDocLi);
			 */

			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescription3);

			WebElement TxtBoxContentDocDescLi = driver.findElement(By.xpath(".//*[@id='description']"));
			String Txt4DocDescLi = TxtBoxContentDocDescLi.getAttribute("value");
			System.out.println("Printing Document Description from file upload window" + Txt4DocDescLi);

			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle10);
			// driver.switchTo().frame("bwc-frame");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
			// driver.findElement(By.id(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
			WebElement Label1Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]/a"));
			String LabelALi = Label1Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelALi);

			WebElement Label2Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[3]/a"));
			String LabelBLi = Label2Li.getText();
			System.out.println("Printing File Name  from Contract Detail Screen" + LabelBLi);

			WebElement Label3Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[4]"));
			String LabelCLi = Label3Li.getText();
			System.out.println("Printing Revision Number from Contract Detail Screen" + LabelCLi);

			WebElement Label4Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String LabelDLi = Label4Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelDLi);

			WebElement Label5Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String LabelELi = Label5Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelELi);

			// Assertions

			if (fileName.equals(LabelBLi)) {
				Assert.assertTrue(TestCasePass, "File Name Validated");
			} else {
				Assert.fail("File Name not validated");

			}

			if (Txt6Li.equals(LabelALi)) {
				Assert.assertTrue(TestCasePass, "Document Name Validated");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (Txt7Li.equals(LabelCLi)) {
				Assert.assertTrue(TestCasePass, "Auto Generated Revision Number");
			} else {
				Assert.fail("Auto Generated Revision Number not validated");

			}

			if (D1.equals(LabelELi)) {
				Assert.assertTrue(TestCasePass, "Category Name Validated");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (D2.equals(LabelDLi)) {
				Assert.assertTrue(TestCasePass, "Document Type Validated");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in creating document" + e);
			Assert.fail("Error in creating document" + e);
		}
	}

	/* Added by net - Start */
	public void CreateDocumentInDraft(String DocumentDescription1, String DocumentDescription3)
			throws InterruptedException {
		ContractStatus();
		// Create document from contract header
		CreateDocumentModule(DocumentDescription1);
		System.out
				.println("Document successfully created from document module under contract header section(draft)!!!");
		/*
		 * ContractStatus(); CreateDocumentModuleLI(DocumentDescription3);
		 * System.out.println(
		 * "Document successfully created from document module under Line Item section(draft)!!!"
		 * );
		 */
	}
	/* Added by net - End */

	// TC-74-User should be able to Attach a document in contract header or line
	// item When the contract status is in "Draft".
	public void CreateDocumentModuleDraft(String DocumentDescription1, String DocumentDescription3)
			throws InterruptedException {
		ContractStatus();
		// Create document from contract header
		CreateDocumentModule(DocumentDescription1);
		System.out
				.println("Document successfully created from document module under contract header section(draft)!!!");
		ContractStatus();
		CreateDocumentModuleLI(DocumentDescription3);
		System.out.println("Document successfully created from document module under Line Item section(draft)!!!");
	}

	public void CreateDocumentModuleDraftWF(String DocumentDescription1, String DocumentDescription3)
			throws InterruptedException {
		ContractStatus();
		// Create document from contract header
		CreateDocumentModule(DocumentDescription1);
		System.out
				.println("Document successfully created from document module under contract header section(draft)!!!");
		ContractStatus();
		CreateDocumentModuleLIWF(DocumentDescription3);
		System.out.println("Document successfully created from document module under Line Item section(draft)!!!");
	}

	public void RemoveDocumentModuleDraft(String DocumentDescription1, String DocumentDescription3)
			throws InterruptedException {
		ContractStatus();
		// Create document from contract header
		RemoveDocumentModule();

		System.out
				.println("Document successfully removed from document module under contract header section(draft)!!!");
		ContractStatus();
		RemoveDocumentModuleLI();
		System.out.println("Document successfully created from document module under Line Item section(draft)!!!");
	}

	public void RemoveDocumentModuleDraftWF(String DocumentDescription1, String DocumentDescription3)
			throws InterruptedException {
		ContractStatus();
		// Create document from contract header
		RemoveDocumentModuleWF();

		System.out
				.println("Document successfully removed from document module under contract header section(draft)!!!");
		ContractStatus();
		RemoveDocumentModuleLIWF();
		System.out.println("Document successfully created from document module under Line Item section(draft)!!!");
	}

	public void CreateDocumentModuleSentForApproval(String DocumentDescription1, String DocumentDescription3)
			throws InterruptedException {
		ContractStatus();
		System.out.println("Check for Create button under Contract Header section");
		if (driver
				.findElements(
						By.xpath(".//*[@id='list_subpanel_gc_lineitem_documents_rowid']/table/thead/tr[1]/td/div/ul"))
				.isEmpty()) {
			// THEN CLICK ON THE SUBMIT BUTTON
			System.out.println(
					"Document cannot be created from document module under contract header section(Sent For Approval)!!!");
		} else {
			// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
			System.out.println("RTC_75 failed");
		}
		System.out.println("Check for Create button under Line Item section");

		// Click Open All Button
		driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
		Thread.sleep(10000);
		if (driver
				.findElements(
						By.xpath(".//*[@id='list_subpanel_gc_lineitem_documents_2']/table/thead/tr[1]/td/div/ul/li/a"))
				.isEmpty()) {
			// THEN CLICK ON THE SUBMIT BUTTON
			System.out.println(
					"Document cannot be created from document module under line item section(Sent For Approval)!!!");
		} else {
			// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
			System.out.println("RTC_75 failed");
		}
	}
	/*
	 * //Create document from contract header
	 * CreateDocumentModule(DocumentDescription1); System.out.println(
	 * "Document cannot be created from document module under contract header section(Sent For Approval)!!!"
	 * ); CreateDocumentModuleLI(DocumentDescription3); System.out.println(
	 * "Document cannot be created from document module under Line Item section(Sent For Approval)!!!"
	 * );
	 */

	public void CreateDocumentDraft(String DocumentDescription1, String DocumentDescription3) throws Exception {

		/** Attach Document in Draft Status **/
		CreateDocumentModule(DocumentDescription1);
		System.out
				.println("Document successfully created from document module under contract header section(draft)!!!");

		CreateDocumentModuleLIWF(DocumentDescription3);
		System.out.println("Document successfully created from document module under Line Item section(draft)!!!");

	}

	public void CreateDocumentModuleApproved(String DocumentDescription1, String DocumentDescription3)
			throws InterruptedException {
		ContractStatus();
		System.out.println("Check for Create button under Contract Header section");
		if (driver
				.findElements(
						By.xpath(".//*[@id='list_subpanel_gc_lineitem_documents_rowid']/table/thead/tr[1]/td/div/ul"))
				.isEmpty()) {
			// THEN CLICK ON THE SUBMIT BUTTON
			System.out.println(
					"Create button not displayed and Document cannot be created from document module under contract header section(Approved)!!!");
		} else {
			// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
			System.out.println("RTC_76 failed");
		}
		System.out.println("Check for Create button under Line Item section");

		// Click Open All Button
		driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
		if (driver
				.findElements(
						By.xpath(".//*[@id='list_subpanel_gc_lineitem_documents_2']/table/thead/tr[1]/td/div/ul/li/a"))
				.isEmpty()) {
			// THEN CLICK ON THE SUBMIT BUTTON
			System.out.println(
					"Create button not displayed and Document cannot be created from document module under line item section(Approved)!!!");
		} else {
			// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
			System.out.println("RTC_76 failed");
		}
	}

	public void CreateDocumentModuleApprovedWF(String DocumentDescription1, String DocumentDescription3)
			throws InterruptedException {
		ContractStatus();
		System.out.println("Check for Create button under Contract Header section");
		if (driver
				.findElements(
						By.xpath(".//*[@id='list_subpanel_gc_lineitem_documents_rowid']/table/thead/tr[1]/td/div/ul"))
				.isEmpty()) {
			// THEN CLICK ON THE SUBMIT BUTTON
			System.out.println(
					"Create button not displayed and Document cannot be created from document module under contract header section(Approved)!!!");
		} else {
			// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
			System.out.println("Create button displayed in Document Module of Contract header and test case failed");
		}
		System.out.println("Check for Create button under Line Item section");

		// Click Open All Button
		driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
		if (driver
				.findElements(
						By.xpath(".//*[@id='list_subpanel_gc_lineitem_documents_2']/table/thead/tr[1]/td/div/ul/li/a"))
				.isEmpty()) {
			// THEN CLICK ON THE SUBMIT BUTTON
			System.out.println(
					"Create button not displayed and Document cannot be created from document module under line item section(Approved)!!!");
		} else {
			// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
			System.out.println("Create button displayed in Document Module of Line Item and test case failed");
		}
	}

	public void CreateDocumentModuleActivated(String DocumentDescription1, String DocumentDescription3)
			throws InterruptedException {
		ContractStatus();
		System.out.println("Check for Create button under Contract Header section");
		if (driver
				.findElements(
						By.xpath(".//*[@id='list_subpanel_gc_lineitem_documents_rowid']/table/thead/tr[1]/td/div/ul"))
				.isEmpty()) {
			// THEN CLICK ON THE SUBMIT BUTTON
			System.out.println(
					"Create button not displayed and Document cannot be created from document module under contract header section(Activated)!!!");
		} else {
			// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
			System.out.println("RTC_77 failed");
		}
		System.out.println("Check for Create button under Line Item section");

		// Click Open All Button
		driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
		if (driver
				.findElements(
						By.xpath(".//*[@id='list_subpanel_gc_lineitem_documents_2']/table/thead/tr[1]/td/div/ul/li/a"))
				.isEmpty()) {
			// THEN CLICK ON THE SUBMIT BUTTON
			System.out.println(
					"Create button not displayed and Document cannot be created from document module under line item section(Activated)!!!");
		} else {
			// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
			System.out.println("RTC_77 failed");
		}
	}

	public void CreateDocumentModuleLIWF(String DocumentDescription3) throws InterruptedException {

		try {
			Thread.sleep(20000);
			// Click Open All Button
			driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
			Thread.sleep(20000);
			// Click create button
			driver.findElement(
					By.xpath(".//*[@id='list_subpanel_gc_lineitem_documents_1']/table/thead/tr[1]/td/div/ul/li/a"))
					.click();
			String parentHandle10 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)
			}
			String fileName = Fileupload();

			WebElement TxtBoxContentFileLi = driver.findElement(By.xpath(".//*[@id='filename_file']"));
			String Txt4FileLi = TxtBoxContentFileLi.getAttribute("value");
			System.out.println("Printing File name uploaded" + Txt4FileLi);

			WebElement TxtBox6Li = driver.findElement(By.xpath(".//*[@id='document_name']"));
			String Txt6Li = TxtBox6Li.getAttribute("value");
			System.out.println("Printing Document name from file upload window" + Txt6Li);

			WebElement TxtBoxContent6Li = driver
					.findElement(By.xpath(".//*[@id='LBL_DOCUMENT_INFORMATION']/tbody/tr[3]/td[2]/input"));
			String Txt7Li = TxtBoxContent6Li.getAttribute("value");
			System.out.println("Printing Document name from file upload window" + Txt7Li);

			Thread.sleep(2000);
			Select se = new Select(driver.findElement(By.xpath(".//*[@id='category_id']")));
			se.selectByValue("Partner");
			Thread.sleep(2000);

			String D1 = se.getFirstSelectedOption().getText();
			System.out.println("Printing Category Field from file uploaded window " + D1);

			Thread.sleep(2000);
			Select se1 = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));
			// se1.selectByVisibleText("Service Description");
			// se1.selectByValue("Service Description");
			se1.selectByIndex(5);
			Thread.sleep(2000);

			String D2 = se1.getFirstSelectedOption().getText();
			System.out.println("Printing Document Type Field from file uploaded window " + D2);

			/*
			 * try { WebDriverWait wait07 = new WebDriverWait(driver, 100); By
			 * selectElementSelector7 = By.xpath(".//*[@id='category_id']");
			 * WebElement selectElement7 = wait07
			 * .until(ExpectedConditions.presenceOfElementLocated(
			 * selectElementSelector7)); Select dropdown7 = new
			 * Select(selectElement7); dropdown7.selectByValue("Partner");
			 * 
			 * String selectedValue =
			 * dropdown7.getFirstSelectedOption().getText();
			 * 
			 * System.out.println(
			 * "Printing Category Field from file uploaded window "
			 * +selectedValue);
			 * 
			 * System.out.println("Category field Chosen"); } catch (Throwable
			 * e) { System.out.println("Error found:Category field not Chosen "
			 * + e.getMessage()); }
			 * 
			 * 
			 * 
			 * 
			 * WebElement TxtBoxContentCategoryLi =
			 * driver.findElement(By.xpath(".//*[@id='category_id']")); String
			 * TxtCategoryLi = TxtBoxContentCategoryLi.getText();
			 * System.out.println(
			 * "Printing Category Field from file uploaded window " +
			 * TxtCategoryLi);
			 * 
			 * Thread.sleep(10000);
			 * 
			 * try { WebDriverWait wait08 = new WebDriverWait(driver, 100); By
			 * selectElementSelector8 = By.xpath(".//*[@id='template_type']");
			 * WebElement selectElement8 = wait08
			 * .until(ExpectedConditions.presenceOfElementLocated(
			 * selectElementSelector8)); Select dropdown8 = new
			 * Select(selectElement8); dropdown8.selectByIndex(2);
			 * System.out.println("Document Type field Chosen"); } catch
			 * (Throwable e) { System.out.println(
			 * "Error found:Document Type field not Chosen " + e.getMessage());
			 * }
			 * 
			 * WebElement TxtBoxContentDocLi =
			 * driver.findElement(By.xpath(".//*[@id='template_type']")); String
			 * TxtDocLi = TxtBoxContentDocLi.getText(); System.out.println(
			 * "Printing Document Type field from file uploaded window " +
			 * TxtDocLi);
			 */

			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescription3);

			WebElement TxtBoxContentDocDescLi = driver.findElement(By.xpath(".//*[@id='description']"));
			String Txt4DocDescLi = TxtBoxContentDocDescLi.getAttribute("value");
			System.out.println("Printing Document Description from file upload window" + Txt4DocDescLi);

			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle10);
			driver.switchTo().frame("bwc-frame");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String LabelALi = Label1Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelALi);

			WebElement Label2Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[3]"));
			String LabelBLi = Label2Li.getText();
			System.out.println("Printing File Name  from Contract Detail Screen" + LabelBLi);

			WebElement Label3Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[4]"));
			String LabelCLi = Label3Li.getText();
			System.out.println("Printing Revision Number from Contract Detail Screen" + LabelCLi);

			WebElement Label4Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String LabelDLi = Label4Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelDLi);

			WebElement Label5Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String LabelELi = Label5Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelELi);

			// Assertions

			if (fileName.equals(LabelBLi)) {
				Assert.assertTrue(TestCasePass, "File Name Validated");
			} else {
				Assert.fail("File Name not validated");

			}

			if (Txt6Li.equals(LabelALi)) {
				Assert.assertTrue(TestCasePass, "Document Name Validated");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (Txt7Li.equals(LabelCLi)) {
				Assert.assertTrue(TestCasePass, "Auto Generated Revision Number");
			} else {
				Assert.fail("Auto Generated Revision Number not validated");

			}

			if (D1.equals(LabelELi)) {
				Assert.assertTrue(TestCasePass, "Category Name Validated");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (D2.equals(LabelDLi)) {
				Assert.assertTrue(TestCasePass, "Document Type Validated");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in creating document" + e);
			e.printStackTrace();
			Assert.fail();
		}

	}

	public void EditDocumentModule(String DocumentDescription, String DocumentName) throws InterruptedException {

		try {
			Thread.sleep(2000);
			WebElement TxtBoxContent = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]/a"));
			String Txt4 = TxtBoxContent.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt4);

			WebElement TxtBoxContent1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String D2C = TxtBoxContent1.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + D2C);

			WebElement TxtBoxContent2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String D1C = TxtBoxContent2.getText();
			System.out.println("Printing Category from Contract Detail Screen" + D1C);

			driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).click();

			Thread.sleep(10000);

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			// Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(5000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentName);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Partner");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(2);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(2);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescription);
			Thread.sleep(5000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			// driver.switchTo().frame("bwc-frame");
			driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String LabelA = Label1.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelA);

			WebElement Label2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[3]"));
			String LabelB = Label2.getText();
			System.out.println("Printing File Name  from Contract Detail Screen" + LabelB);

			WebElement Label3 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[4]"));
			String LabelC = Label3.getText();
			System.out.println("Printing Revision Number from Contract Detail Screen" + LabelC);

			WebElement Label4 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String LabelD = Label4.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelD);

			WebElement Label5 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String LabelE = Label5.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelE);

			// Assertions

			if (!(Txt4.equals(LabelA))) {
				Assert.assertTrue(TestCasePass, "Document Name Edited");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(D1C.equals(LabelE))) {
				Assert.assertTrue(TestCasePass, "Category Name Edited");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(D2C.equals(LabelD))) {
				Assert.assertTrue(TestCasePass, "Document Type Edited");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail();
		}
	}

	public void EditDocumentModuleActivated(String DocumentDescription1, String DocumentDescription3)
			throws InterruptedException

	{
		ContractStatus();
		System.out.println("Check for Edit button under Contract Header section");

		if (driver.findElements(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).isEmpty()) {
			// THEN CLICK ON THE SUBMIT BUTTON
			System.out.println(
					"Document cannot be Edited from document module under contract header section(Activated)!!!");
		} else {
			// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
			System.out.println("RTC_Workflow_030 failed");
		}

		System.out.println("Check for Edit button under Line Item section");

		// Click Open All Button

		driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
		Thread.sleep(10000);
		if (driver.findElements(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).isEmpty()) {
			// THEN CLICK ON THE SUBMIT BUTTON
			System.out.println("Document cannot be edited from document module under line item section(Activated)!!!");
		} else {
			// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
			System.out.println("RTC_Workflow_030 failed");
		}
	}

	public void EditDocumentModuleApprovedWF(String DocumentDescription1, String DocumentName)
			throws InterruptedException

	{
		try {

			WebElement TxtBoxContent = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String Txt4 = TxtBoxContent.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt4);

			WebElement TxtBoxContent1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String D2C = TxtBoxContent1.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + D2C);

			WebElement TxtBoxContent2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String D1C = TxtBoxContent2.getText();
			System.out.println("Printing Category from Contract Detail Screen" + D1C);
			driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).click();

			Thread.sleep(20000);

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentName);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Affiliate");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(6);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(2);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescription1);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Contract Header Section-Approved");
			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String LabelA = Label1.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelA);

			WebElement Label2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[3]"));
			String LabelB = Label2.getText();
			System.out.println("Printing File Name  from Contract Detail Screen" + LabelB);

			WebElement Label3 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[4]"));
			String LabelC = Label3.getText();
			System.out.println("Printing Revision Number from Contract Detail Screen" + LabelC);

			WebElement Label4 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String LabelD = Label4.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelD);

			WebElement Label5 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String LabelE = Label5.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelE);

			// Assertions

			if (!(Txt4.equals(LabelA))) {
				Assert.assertTrue(TestCasePass, "Document Name Edited");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(D1C.equals(LabelE))) {
				Assert.assertTrue(TestCasePass, "Category Name Edited");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(D2C.equals(LabelD))) {
				Assert.assertTrue(TestCasePass, "Document Type Edited");
			} else {
				Assert.fail("Document Type  not validated");

			}

			Assert.assertTrue(TestCasePass, "RTC_Workflow_029 Passed");
		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("RTC_Workflow_029 failed");

		}
	}

	public void EditDocumentModuleApprovedLIWF(String DocumentDescription1, String DocumentName)
			throws InterruptedException

	{
		try {

			Thread.sleep(2000);
			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement TxtBox6Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String Txt6Li = TxtBox6Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt6Li);

			WebElement TxtBox7Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String Txt7Li = TxtBox7Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + Txt7Li);

			WebElement TxtBox8Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String Txt8Li = TxtBox8Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + Txt8Li);

			driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[7]/ul/li/a")).click();

			Thread.sleep(20000);

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}
			Thread.sleep(20000);

			//

			Thread.sleep(20000);

			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentName);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Customer");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(2);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(2);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescription1);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Line Item Section-Approved");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String LabelALi = Label1Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelALi);

			WebElement Label4Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String LabelDLi = Label4Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelDLi);

			WebElement Label5Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String LabelELi = Label5Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelELi);

			// Assertions

			if (!(Txt6Li.equals(LabelALi))) {
				Assert.assertTrue(TestCasePass, "Document Name Validated");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(Txt8Li.equals(LabelELi))) {
				Assert.assertTrue(TestCasePass, "Category Name Validated");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(Txt7Li.equals(LabelDLi))) {
				Assert.assertTrue(TestCasePass, "Document Type Validated");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleSentForApproval(String DocumentDescription1, String DocumentDescription3)
			throws InterruptedException

	{
		ContractStatus();
		System.out.println("Check for Edit button under Contract Header section");

		if (driver.findElements(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).isEmpty()) {
			// THEN CLICK ON THE SUBMIT BUTTON
			System.out.println(
					"Document cannot be Edited from document module under contract header section(Sent For Approval)!!!");
		} else {
			// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
			System.out.println("RTC_Workflow_028 failed");
		}

		System.out.println("Check for Edit button under Line Item section");

		// Click Open All Button

		driver.findElement(By.xpath(".//*[@id='ndb_line_item_details_tpl']/div[1]/div/button[1]")).click();
		Thread.sleep(10000);
		if (driver.findElements(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).isEmpty()) {
			// THEN CLICK ON THE SUBMIT BUTTON
			System.out.println(
					"Document cannot be edited from document module under line item section(Sent For Approval)!!!");
		} else {
			// DO SOMETHING ELSE AS SUBMIT BUTTON IS NOT THERE
			System.out.println("RTC_Workflow_028 failed");
		}
	}

	public void EditDocumentModuleAdminDraftWF(String DocumentDescriptionX14, String DocumentNameX14)
			throws InterruptedException

	{
		try {
			Thread.sleep(2000);
			WebElement TxtBoxContent = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String Txt4 = TxtBoxContent.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt4);

			WebElement TxtBoxContent1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String D2C = TxtBoxContent1.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + D2C);

			WebElement TxtBoxContent2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String D1C = TxtBoxContent2.getText();
			System.out.println("Printing Category from Contract Detail Screen" + D1C);

			driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}
			Thread.sleep(20000);
			//

			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX14);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Affiliate");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(3);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(3);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX14);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Contract Header Section-Draft");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String LabelA = Label1.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelA);

			;

			WebElement Label4 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String LabelD = Label4.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelD);

			WebElement Label5 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String LabelE = Label5.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelE);

			// Assertions

			if (!(Txt4.equals(LabelA))) {
				Assert.assertTrue(TestCasePass, "Document Name Edited");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(D1C.equals(LabelE))) {
				Assert.assertTrue(TestCasePass, "Category Name Edited");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(D2C.equals(LabelD))) {
				Assert.assertTrue(TestCasePass, "Document Type Edited");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleAdminDraftLIWF(String DocumentDescriptionX1, String DocumentNameX1)
			throws InterruptedException

	{
		try {

			Thread.sleep(2000);
			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement TxtBox6Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String Txt6Li = TxtBox6Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt6Li);

			WebElement TxtBox7Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String Txt7Li = TxtBox7Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + Txt7Li);

			WebElement TxtBox8Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String Txt8Li = TxtBox8Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + Txt8Li);

			// Assertions

			driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle

			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);

			//

			Thread.sleep(20000);

			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX1);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Affiliate");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(3);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(3);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX1);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Line Item Section-Draft");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String LabelALi = Label1Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelALi);

			WebElement Label4Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String LabelDLi = Label4Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelDLi);

			WebElement Label5Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String LabelELi = Label5Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelELi);

			// Assertions

			if (!(Txt6Li.equals(LabelALi))) {
				Assert.assertTrue(TestCasePass, "Document Name Validated");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(Txt8Li.equals(LabelELi))) {
				Assert.assertTrue(TestCasePass, "Category Name Validated");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(Txt7Li.equals(LabelDLi))) {
				Assert.assertTrue(TestCasePass, "Document Type Validated");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleAdminSFAWF(String DocumentDescriptionX2, String DocumentNameX2)
			throws InterruptedException

	{
		try {
			Thread.sleep(2000);
			WebElement TxtBoxContent = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String Txt4 = TxtBoxContent.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt4);

			WebElement TxtBoxContent1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String D2C = TxtBoxContent1.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + D2C);

			WebElement TxtBoxContent2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String D1C = TxtBoxContent2.getText();
			System.out.println("Printing Category from Contract Detail Screen" + D1C);

			driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);

			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX2);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Partner");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(2);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(2);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX2);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Contract Header Section-Sent For Approval");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String LabelA = Label1.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelA);

			WebElement Label4 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String LabelD = Label4.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelD);

			WebElement Label5 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String LabelE = Label5.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelE);

			// Assertions

			if (!(Txt4.equals(LabelA))) {
				Assert.assertTrue(TestCasePass, "Document Name Edited");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(D1C.equals(LabelE))) {
				Assert.assertTrue(TestCasePass, "Category Name Edited");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(D2C.equals(LabelD))) {
				Assert.assertTrue(TestCasePass, "Document Type Edited");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleAdminSFALIWF(String DocumentDescriptionX3, String DocumentNameX3)
			throws InterruptedException

	{
		try {
			Thread.sleep(2000);
			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement TxtBox6Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String Txt6Li = TxtBox6Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt6Li);

			WebElement TxtBox7Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String Txt7Li = TxtBox7Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + Txt7Li);

			WebElement TxtBox8Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String Txt8Li = TxtBox8Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + Txt8Li);

			driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX3);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Customer");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(2);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(2);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX3);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Line Item Section-Sent For Approval");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String LabelALi = Label1Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelALi);

			WebElement Label4Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String LabelDLi = Label4Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelDLi);

			WebElement Label5Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String LabelELi = Label5Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelELi);

			// Assertions

			if (!(Txt6Li.equals(LabelALi))) {
				Assert.assertTrue(TestCasePass, "Document Name Validated");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(Txt8Li.equals(LabelELi))) {
				Assert.assertTrue(TestCasePass, "Category Name Validated");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(Txt7Li.equals(LabelDLi))) {
				Assert.assertTrue(TestCasePass, "Document Type Validated");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleAdminApprovedWF(String DocumentDescriptionX4, String DocumentNameX4)
			throws InterruptedException

	{
		try {
			Thread.sleep(2000);
			WebElement TxtBoxContent = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String Txt4 = TxtBoxContent.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt4);

			WebElement TxtBoxContent1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String D2C = TxtBoxContent1.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + D2C);

			WebElement TxtBoxContent2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String D1C = TxtBoxContent2.getText();
			System.out.println("Printing Category from Contract Detail Screen" + D1C);
			driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);

			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX4);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Affiliate");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(3);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(3);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX4);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Contract Header Section-Approved");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String LabelA = Label1.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelA);

			WebElement Label4 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String LabelD = Label4.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelD);

			WebElement Label5 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String LabelE = Label5.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelE);

			// Assertions

			if (!(Txt4.equals(LabelA))) {
				Assert.assertTrue(TestCasePass, "Document Name Edited");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(D1C.equals(LabelE))) {
				Assert.assertTrue(TestCasePass, "Category Name Edited");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(D2C.equals(LabelD))) {
				Assert.assertTrue(TestCasePass, "Document Type Edited");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleAdminApprovedLIWF(String DocumentDescriptionX5, String DocumentNameX5)
			throws InterruptedException

	{
		try {
			Thread.sleep(2000);
			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement TxtBox6Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String Txt6Li = TxtBox6Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt6Li);

			WebElement TxtBox7Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String Txt7Li = TxtBox7Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + Txt7Li);

			WebElement TxtBox8Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String Txt8Li = TxtBox8Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + Txt8Li);
			driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);

			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX5);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Affiliate");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(5);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(5);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX5);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Line Item Section-Approved");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String LabelALi = Label1Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelALi);

			WebElement Label4Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String LabelDLi = Label4Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelDLi);

			WebElement Label5Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String LabelELi = Label5Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelELi);

			// Assertions

			if (!(Txt6Li.equals(LabelALi))) {
				Assert.assertTrue(TestCasePass, "Document Name Validated");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(Txt8Li.equals(LabelELi))) {
				Assert.assertTrue(TestCasePass, "Category Name Validated");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(Txt7Li.equals(LabelDLi))) {
				Assert.assertTrue(TestCasePass, "Document Type Validated");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleAdminActivatedWF(String DocumentDescriptionX6, String DocumentNameX6)
			throws InterruptedException

	{
		try {
			Thread.sleep(2000);
			WebElement TxtBoxContent = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String Txt4 = TxtBoxContent.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt4);

			WebElement TxtBoxContent1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String D2C = TxtBoxContent1.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + D2C);

			WebElement TxtBoxContent2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String D1C = TxtBoxContent2.getText();
			System.out.println("Printing Category from Contract Detail Screen" + D1C);
			driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);

			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX6);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Partner");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(5);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(5);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX6);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Contract Header Section-Activated");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String LabelA = Label1.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelA);

			WebElement Label4 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String LabelD = Label4.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelD);

			WebElement Label5 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String LabelE = Label5.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelE);

			// Assertions
			if (!(Txt4.equals(LabelA))) {
				Assert.assertTrue(TestCasePass, "Document Name Edited");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(D1C.equals(LabelE))) {
				Assert.assertTrue(TestCasePass, "Category Name Edited");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(D2C.equals(LabelD))) {
				Assert.assertTrue(TestCasePass, "Document Type Edited");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleAdminActivatedLIWF(String DocumentDescriptionX7, String DocumentNameX7)
			throws InterruptedException

	{
		try {
			Thread.sleep(2000);
			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement TxtBox6Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String Txt6Li = TxtBox6Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt6Li);

			WebElement TxtBox7Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String Txt7Li = TxtBox7Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + Txt7Li);

			WebElement TxtBox8Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String Txt8Li = TxtBox8Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + Txt8Li);
			driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);

			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX7);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Customer");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(2);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(2);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX7);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Line Item Section-Activated");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String LabelALi = Label1Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelALi);

			WebElement Label4Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String LabelDLi = Label4Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelDLi);

			WebElement Label5Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String LabelELi = Label5Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelELi);

			// Assertions

			if (!(Txt6Li.equals(LabelALi))) {
				Assert.assertTrue(TestCasePass, "Document Name Validated");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(Txt8Li.equals(LabelELi))) {
				Assert.assertTrue(TestCasePass, "Category Name Validated");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(Txt7Li.equals(LabelDLi))) {
				Assert.assertTrue(TestCasePass, "Document Type Validated");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleAdminSFTAWF(String DocumentDescriptionX8, String DocumentNameX8)
			throws InterruptedException

	{
		try {
			Thread.sleep(2000);
			WebElement TxtBoxContent = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String Txt4 = TxtBoxContent.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt4);

			WebElement TxtBoxContent1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String D2C = TxtBoxContent1.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + D2C);

			WebElement TxtBoxContent2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String D1C = TxtBoxContent2.getText();
			System.out.println("Printing Category from Contract Detail Screen" + D1C);
			driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);

			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX8);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Customer");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(3);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(3);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX8);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Contract Header Section-Sent For Termination Approval");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String LabelA = Label1.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelA);

			WebElement Label4 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String LabelD = Label4.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelD);

			WebElement Label5 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String LabelE = Label5.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelE);

			// Assertions
			if (!(Txt4.equals(LabelA))) {
				Assert.assertTrue(TestCasePass, "Document Name Edited");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(D1C.equals(LabelE))) {
				Assert.assertTrue(TestCasePass, "Category Name Edited");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(D2C.equals(LabelD))) {
				Assert.assertTrue(TestCasePass, "Document Type Edited");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleAdminSFTALIWF(String DocumentDescriptionX9, String DocumentNameX9)
			throws InterruptedException

	{
		try {
			Thread.sleep(2000);
			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement TxtBox6Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String Txt6Li = TxtBox6Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt6Li);

			WebElement TxtBox7Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String Txt7Li = TxtBox7Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + Txt7Li);

			WebElement TxtBox8Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String Txt8Li = TxtBox8Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + Txt8Li);
			driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);

			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX9);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Affiliate");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(6);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(6);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX9);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Line Item Section-Sent For Termination Approval");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String LabelALi = Label1Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelALi);

			WebElement Label4Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String LabelDLi = Label4Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelDLi);

			WebElement Label5Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String LabelELi = Label5Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelELi);

			// Assertions

			if (!(Txt6Li.equals(LabelALi))) {
				Assert.assertTrue(TestCasePass, "Document Name Validated");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(Txt8Li.equals(LabelELi))) {
				Assert.assertTrue(TestCasePass, "Category Name Validated");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(Txt7Li.equals(LabelDLi))) {
				Assert.assertTrue(TestCasePass, "Document Type Validated");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleAdminTApprovedWF(String DocumentDescriptionX10, String DocumentNameX10)
			throws InterruptedException

	{
		try {
			Thread.sleep(2000);
			WebElement TxtBoxContent = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String Txt4 = TxtBoxContent.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt4);

			WebElement TxtBoxContent1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String D2C = TxtBoxContent1.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + D2C);

			WebElement TxtBoxContent2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String D1C = TxtBoxContent2.getText();
			System.out.println("Printing Category from Contract Detail Screen" + D1C);
			driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);

			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX10);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Partner");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(5);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(5);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX10);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Contract Header Section-Termination Approved");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String LabelA = Label1.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelA);

			WebElement Label4 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String LabelD = Label4.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelD);

			WebElement Label5 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String LabelE = Label5.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelE);

			// Assertions

			if (!(Txt4.equals(LabelA))) {
				Assert.assertTrue(TestCasePass, "Document Name Edited");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(D1C.equals(LabelE))) {
				Assert.assertTrue(TestCasePass, "Category Name Edited");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(D2C.equals(LabelD))) {
				Assert.assertTrue(TestCasePass, "Document Type Edited");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleAdminTApprovedLIWF(String DocumentDescriptionX11, String DocumentNameX11)
			throws InterruptedException

	{
		try {
			Thread.sleep(2000);
			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement TxtBox6Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String Txt6Li = TxtBox6Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt6Li);

			WebElement TxtBox7Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String Txt7Li = TxtBox7Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + Txt7Li);

			WebElement TxtBox8Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String Txt8Li = TxtBox8Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + Txt8Li);
			driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);
			//

			Thread.sleep(20000);

			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX11);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Customer");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(2);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(2);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX11);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Line Item Section-Termination Approved");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String LabelALi = Label1Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelALi);

			WebElement Label4Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String LabelDLi = Label4Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelDLi);

			WebElement Label5Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String LabelELi = Label5Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelELi);

			// Assertions

			if (!(Txt6Li.equals(LabelALi))) {
				Assert.assertTrue(TestCasePass, "Document Name Validated");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(Txt8Li.equals(LabelELi))) {
				Assert.assertTrue(TestCasePass, "Category Name Validated");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(Txt7Li.equals(LabelDLi))) {
				Assert.assertTrue(TestCasePass, "Document Type Validated");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleAdminDeactivatedWF(String DocumentDescriptionX12, String DocumentNameX12)
			throws InterruptedException

	{
		try {
			Thread.sleep(2000);
			WebElement TxtBoxContent = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String Txt4 = TxtBoxContent.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt4);

			WebElement TxtBoxContent1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String D2C = TxtBoxContent1.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + D2C);

			WebElement TxtBoxContent2 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String D1C = TxtBoxContent2.getText();
			System.out.println("Printing Category from Contract Detail Screen" + D1C);
			driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);

			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX12);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Partner");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(2);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(2);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX12);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Contract Header Section-Deactivated");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[2]"));
			String LabelA = Label1.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelA);

			WebElement Label4 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[5]"));
			String LabelD = Label4.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelD);

			WebElement Label5 = driver.findElement(By.xpath(".//*[@id='contract_doc_list_items']/tr/td[6]"));
			String LabelE = Label5.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelE);

			// Assertions
			if (!(Txt4.equals(LabelA))) {
				Assert.assertTrue(TestCasePass, "Document Name Edited");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(D1C.equals(LabelE))) {
				Assert.assertTrue(TestCasePass, "Category Name Edited");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(D2C.equals(LabelD))) {
				Assert.assertTrue(TestCasePass, "Document Type Edited");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	public void EditDocumentModuleAdminDeactivatedLIWF(String DocumentDescriptionX13, String DocumentNameX13)
			throws InterruptedException

	{
		try {
			Thread.sleep(2000);
			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement TxtBox6Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String Txt6Li = TxtBox6Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + Txt6Li);

			WebElement TxtBox7Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String Txt7Li = TxtBox7Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + Txt7Li);

			WebElement TxtBox8Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String Txt8Li = TxtBox8Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + Txt8Li);
			driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[7]/ul/li/a")).click();

			String parentHandle1 = driver.getWindowHandle(); // get the current
																// window handle
			Thread.sleep(20000);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of
														// WebDriver to the next
														// found window handle
														// (that's your newly
														// opened window)

			}

			Thread.sleep(20000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='document_name']")).sendKeys(DocumentNameX13);

			WebDriverWait wait03 = new WebDriverWait(driver, 100);
			By selectElementSelector = By.xpath(".//*[@id='category_id']");
			WebElement selectElement = wait03.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector));
			Select dropdown = new Select(selectElement);
			dropdown.selectByValue("Affiliate");
			System.out.println("Category field editable");
			Thread.sleep(10000);

			try {
				WebDriverWait wait04 = new WebDriverWait(driver, 100);
				By selectElementSelector2 = By.xpath(".//*[@id='template_type']");
				WebElement selectElement3 = wait04
						.until(ExpectedConditions.presenceOfElementLocated(selectElementSelector2));
				Select dropdown3 = new Select(selectElement3);
				dropdown3.selectByIndex(5);
				// To select the first option
				Select select = new Select(driver.findElement(By.xpath(".//*[@id='template_type']")));

				select.selectByIndex(5);
				System.out.println("Document Type field editable");
			} catch (Throwable e) {
				System.out.println("Error found:Document Type field not editable " + e.getMessage());
			}

			driver.findElement(By.xpath(".//*[@id='description']")).clear();

			driver.findElement(By.xpath(".//*[@id='description']")).sendKeys(DocumentDescriptionX13);
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@id='SAVE_FOOTER']")).click();
			driver.switchTo().window(parentHandle1);
			driver.switchTo().frame("bwc-frame");
			System.out.println("Document Editable from Line Item Section-Deactivated");

			System.out.println("*****Printing Document Uploaded Details from Contract Detail Screen*****");

			Thread.sleep(2000);

			WebElement Label1Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[2]"));
			String LabelALi = Label1Li.getText();
			System.out.println("Printing Document Name from Contract Detail Screen" + LabelALi);

			WebElement Label4Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[5]"));
			String LabelDLi = Label4Li.getText();
			System.out.println("Printing Document Type from Contract Detail Screen" + LabelDLi);

			WebElement Label5Li = driver.findElement(By.xpath(".//*[@id='li_doc_list_items_1']/tr/td[6]"));
			String LabelELi = Label5Li.getText();
			System.out.println("Printing Category from Contract Detail Screen" + LabelELi);

			// Assertions

			if (!(Txt6Li.equals(LabelALi))) {
				Assert.assertTrue(TestCasePass, "Document Name Validated");
			} else {
				Assert.fail("Document Name not validated");

			}

			if (!(Txt8Li.equals(LabelELi))) {
				Assert.assertTrue(TestCasePass, "Category Name Validated");
			} else {
				Assert.fail("Category Name not validated");

			}

			if (!(Txt7Li.equals(LabelDLi))) {
				Assert.assertTrue(TestCasePass, "Document Type Validated");
			} else {
				Assert.fail("Document Type  not validated");

			}

		} catch (Exception e) {
			Reporter.log("Error in editing document module" + e);
			e.printStackTrace();
			Assert.fail("Error in editing document module");

		}
	}

	/* Deep added for selecting 2nd row contract from the list view - start */
	public void selectContractIdFrom2ndRow(String ContractId) throws Exception {
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("global_contract_id_basic")).clear();
		driver.findElement(By.id("name_basic")).clear();
		driver.findElement(By.id("global_contract_id_basic")).sendKeys(ContractId);
		// srn.getscreenshot();
		driver.findElement(By.id("search_form_submit")).click();
		// driver.findElement(By.xpath("//table/tbody/tr[3]/td[4]/b/a")).click();
		Thread.sleep(10000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.xpath(".//*[@id='MassUpdate']/table/tbody/tr[4]/td[4]/b/a")).click();
		// srn.getscreenshot();

	}
	/* Deep added for selecting 2nd row contract from the list view - end */

	/* Deep added for clear - start */
	public void ViewContractNavigationPop() throws Exception {
		Thread.sleep(5000);
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[4]/span/button")).click();
		driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[4]/span/div/ul/li[2]/a")).click();
		if (driver.findElements(By.className("alert-messages")).size() != 0) {
			driver.findElement(By.className("alert-btn-confirm")).click();
		}
	}

	public void clearData() {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		driver.findElement(By.id("name")).clear();
		driver.findElement(By.id("description")).clear();
		driver.findElement(By.id("btn_clr_accounts_gc_contracts_1_name")).click();
		driver.findElement(By.id("btn_clr_contacts_gc_contracts_1_name")).click();
		Select contractScheme = new Select(driver.findElement(By.id("contract_scheme")));
		contractScheme.selectByVisibleText("");
		Select contractType = new Select(driver.findElement(By.id("contract_type")));
		contractType.selectByVisibleText("");
		driver.findElement(By.id("contract_enddate")).clear();
		Select contEndDateHr = new Select(driver.findElement(By.id("contract_enddate_hours")));
		contEndDateHr.selectByVisibleText("00");
		Select contEndDateMi = new Select(driver.findElement(By.id("contract_enddate_minutes")));
		contEndDateMi.selectByVisibleText("00");
		Select contEndDateSec = new Select(driver.findElement(By.id("contract_enddate_seconds")));
		contEndDateSec.selectByVisibleText("00");
		Select contEndDateTZ = new Select(driver.findElement(By.id("gc_timezone_id1_c")));
		contEndDateTZ.selectByVisibleText("");
		driver.findElement(By.id("pm_products_gc_contracts_1_name")).clear();
		driver.findElement(By.id("factory_reference_no")).clear();
		driver.findElement(By.id("sales_channel_code")).clear();
		Select salesComp = new Select(driver.findElement(By.id("sales_company_id")));
		salesComp.selectByVisibleText("");
		driver.findElement(By.id("sales_rep_name")).clear();
		driver.findElement(By.id("sales_rep_name_1")).clear();
		driver.findElement(By.id("division_1")).clear();
		driver.findElement(By.id("division_2")).clear();
		driver.findElement(By.id("title_1")).clear();
		driver.findElement(By.id("title_2")).clear();
		driver.findElement(By.id("tel_no")).clear();
		driver.findElement(By.id("ext_no")).clear();
		driver.findElement(By.id("fax_no")).clear();
		driver.findElement(By.id("mob_no")).clear();
		driver.findElement(By.id("email1")).clear();

	}
	/* Deep added for clear - end */

	@DataProvider
	public Object[][] EditContractData() {
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}

	@AfterTest
	public void close() {
		driver.close();
	}

}
