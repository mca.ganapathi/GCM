package com.stta.GCM;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;

public class CreateCorporateNewData extends SuiteGcmBase {

	/* Method for contract navigation */
	public void CreateCorpNavigation() throws InterruptedException {

		// WebElement element =
		// driver.findElement(By.xpath(Object.getProperty("corporatesheader")));
		// Actions action = new Actions(driver);
		// action.moveToElement(element).build().perform();

		/* Modified for automation CreateCorporation navigation */
		driver.findElement(By.xpath("//div/div[2]/div/div/div/div[1]/ul/li[2]/span/button")).click();
		driver.findElement(By.xpath(".//*[@id='header']/div/div/div/div[1]/ul/li[2]/span/div/ul/li[1]/a")).click();

		// driver.findElement(By.id(Object.getProperty("CreateCorporate"))).click();
	}

	/* Method to fill the data in the corporate module */
	public void DataFillCorp(String CommonCustomerID, String CorporateNameLatinCharacter,
			String CorporateNameLocalCharacter1, String CorporateNameLocalCharacter2, String CountryName,
			String CountryNameCode, String HeadquarterPostalNumber, String HeadquarterAddressLatinCharacter,
			String HeadquarterAddressLocalCharacter, String RegisteredPostalNumber,
			String RegisteredAddressLatinCharacter, String RegisteredAddressLocalCharacter, String VATNumber) {

		// driver.findElement(By.id(Object.getProperty("CorporateNameLatin"))).sendKeys(CommonCustomerID);
		driver.findElement(By.id(Object.getProperty("CorporateNameLatin"))).sendKeys(CorporateNameLatinCharacter);
		driver.findElement(By.id(Object.getProperty("CorporateNameLocal1"))).sendKeys(CorporateNameLocalCharacter1);
		driver.findElement(By.id(Object.getProperty("CorporateNameLocal2"))).sendKeys(CorporateNameLocalCharacter2);
		driver.findElement(By.id(Object.getProperty("CountryName"))).sendKeys(CountryName);
		driver.findElement(By.id(Object.getProperty("CountryNameCode"))).sendKeys(CountryNameCode);
		driver.findElement(By.id(Object.getProperty("PostalHead"))).sendKeys(HeadquarterPostalNumber);
		driver.findElement(By.id(Object.getProperty("PostalHeadAdd1"))).sendKeys(HeadquarterAddressLatinCharacter);
		driver.findElement(By.id(Object.getProperty("PostalHeadAdd2"))).sendKeys(HeadquarterAddressLocalCharacter);
		driver.findElement(By.id(Object.getProperty("PostalNumber"))).sendKeys(RegisteredPostalNumber);
		driver.findElement(By.id(Object.getProperty("RegisteredAddress1"))).sendKeys(RegisteredAddressLatinCharacter);
		driver.findElement(By.id(Object.getProperty("RegisteredAddress2"))).sendKeys(RegisteredAddressLocalCharacter);

	}

	// /* To clear the data */
	public void ClearDataFill() {
		driver.findElement(By.id(Object.getProperty("CorporateNameLatin"))).clear();
		driver.findElement(By.id(Object.getProperty("CorporateNameLocal1"))).clear();
		driver.findElement(By.id(Object.getProperty("CorporateNameLocal2"))).clear();
		driver.findElement(By.id(Object.getProperty("CountryName"))).clear();
		driver.findElement(By.id(Object.getProperty("CountryNameCode"))).clear();
		driver.findElement(By.id(Object.getProperty("PostalHead"))).clear();
		driver.findElement(By.id(Object.getProperty("PostalHeadAdd1"))).clear();
		driver.findElement(By.id(Object.getProperty("PostalHeadAdd2"))).clear();
		driver.findElement(By.id(Object.getProperty("PostalNumber"))).clear();
		driver.findElement(By.id(Object.getProperty("RegisteredAddress1"))).clear();
		driver.findElement(By.id(Object.getProperty("RegisteredAddress2"))).clear();
	}

	//
	//
	public void DataCount() {

		String TempName = driver.findElement(By.id("common_customer_id_c")).getAttribute("value");
		int count = TempName.length();
		if (count <= 12) {
			Reporter.log("-. The Corporate id is less than 12 and the count =" + count);
			Reporter.log("-. TC_Corporate_001 executed successfuly");

		} else {
			Reporter.log("-. TC_Corporate_001 failed. The Corporate id is more than 12 characters and the count =  "
					+ count);
			Assert.fail();
		}

		//
		//
		TempName = driver.findElement(By.id(Object.getProperty("CorporateNameLatin"))).getAttribute("value");
		count = TempName.length();
		if (count <= 100) {
			Reporter.log("-.The Corporate_Name_Latin_Character is less than equal to  100 and the count =" + count);
			Reporter.log("-.TC_Corporate_002 executed successfuly");
		} else {
			Reporter.log("-.The Corporate_Name_Latin_Character is more than 100 and the count =" + count);
			Assert.fail();
		}
		//
		//
		//
		TempName = driver.findElement(By.id(Object.getProperty("CorporateNameLocal1"))).getAttribute("value");
		count = TempName.length();
		if (count <= 100) {
			Reporter.log("-.The Corporate_Name_Local_Character1 is less than equal to  100 and the count=" + count);
			Reporter.log("-.TC_Corporate_003 executed successfuly");
		} else {
			Reporter.log("-.The Corporate_Name_Local_Character1 is more than 100 and the count =" + count);
			Assert.fail();
		}

		//
		TempName = driver.findElement(By.id(Object.getProperty("CorporateNameLocal2"))).getAttribute("value");
		count = TempName.length();
		if (count <= 100) {
			// Reporter.log(TempName);
			Reporter.log("-.The Corporate_Name_Local_Character2 is less than equal to  100 and the count=" + count);
			Reporter.log("-.TC_Corporate_004 executed successfuly");
		} else {

			Reporter.log(TempName);
			Reporter.log("-.The Corporate_Name_Local_Character2 is more than 100 and the count =" + count);
			Assert.fail();
		}

		TempName = driver.findElement(By.id(Object.getProperty("CountryName"))).getAttribute("value");
		count = TempName.length();
		if (count <= 20) {
			// Reporter.log(TempName);
			Reporter.log("-.The countryName  is less than equal to  20  and the count=" + count);
			Reporter.log("-.TC_Corporate_006 executed successfuly");
		} else {

			Reporter.log(TempName);
			Reporter.log("-.The countryName is more than 20 and the count =" + count);
			Assert.fail();
		}

		TempName = driver.findElement(By.id(Object.getProperty("CountryNameCode"))).getAttribute("value");
		count = TempName.length();
		if (count <= 3) {
			// Reporter.log(TempName);
			Reporter.log("-.The countrycode  is less than equal to 3 and the count=" + count);
			Reporter.log("-.TC_Corporate_007 executed successfuly");
		} else {

			Reporter.log(TempName);
			Reporter.log("-.The countryName is more than 3 and the count =" + count);
			Assert.fail();
		}

		TempName = driver.findElement(By.id(Object.getProperty("PostalHead"))).getAttribute("value");
		count = TempName.length();
		if (count <= 10) {
			// Reporter.log(TempName);
			Reporter.log("-.The HeadQuatersPostalNumber  is less than equal to 10 and the count=" + count);
			Reporter.log("-.TC_Corporate_008 executed successfuly");
		} else {

			Reporter.log(TempName);
			Reporter.log("-.The HeadQuatersPostalNumber is more than 10 and the count =" + count);
			Assert.fail();
		}

		TempName = driver.findElement(By.id(Object.getProperty("PostalHeadAdd1"))).getAttribute("value");
		count = TempName.length();
		if (count <= 400) {
			// Reporter.log(TempName);
			Reporter.log("-.The HeadQuatersAddressLatinCharacters  is less than equal to 400 and the count=" + count);
			Reporter.log("-.TC_Corporate_009 executed successfuly");
		} else {

			Reporter.log(TempName);
			Reporter.log("-.The HeadQuatersAddressLatinCharacters is more than 400 and the count =" + count);
			Assert.fail();
		}

		TempName = driver.findElement(By.id(Object.getProperty("PostalHeadAdd2"))).getAttribute("value");
		count = TempName.length();
		if (count <= 400) {
			// Reporter.log(TempName);

			Reporter.log("-.The HeadQuatersAddressLatinCharacters  is less than equal to 400 and the count=" + count);
			Reporter.log("-.TC_Corporate_010 executed successfuly");
		} else {

			Reporter.log(TempName);
			Reporter.log("-.The HeadQuatersAddressLatinCharacters is more than 400 and the count =" + count);
			Assert.fail();
		}

		TempName = driver.findElement(By.id(Object.getProperty("PostalNumber"))).getAttribute("value");
		count = TempName.length();
		if (count <= 10) {
			// Reporter.log(TempName);
			Reporter.log("-.The Reg_PostalNumber  is less than equal to 10 and the count=" + count);
			Reporter.log("-.TC_Corporate_011 executed successfuly");
		} else {

			Reporter.log(TempName);
			Reporter.log("-.The Reg_PostalNumber is more than 10 and the count =" + count);
			Assert.fail();
		}

		TempName = driver.findElement(By.id(Object.getProperty("RegisteredAddress1"))).getAttribute("value");
		count = TempName.length();
		if (count <= 400) {
			// Reporter.log(TempName);

			Reporter.log("-.The Reg_Address_Latin  is less than equal to 400 and the count=" + count);
			Reporter.log("-.TC_Corporate_011 executed successfuly");
		} else {

			Reporter.log(TempName);
			Reporter.log("-.The Reg_Address_Latin  is more than 400 and the count =" + count);
			Assert.fail();
		}

		TempName = driver.findElement(By.id(Object.getProperty("RegisteredAddress2"))).getAttribute("value");
		count = TempName.length();
		if (count <= 400) {
			// Reporter.log(TempName);

			Reporter.log("-.The Reg_Address_Local  is less than equal to 400 and the count=" + count);
			Reporter.log("-.TC_Corporate_013 executed successfuly");
		} else {

			Reporter.log(TempName);
			Reporter.log("-.The Reg_Address_Local is more than 400 and the count =" + count);
			Assert.fail();
		}

		TempName = driver.findElement(By.id(Object.getProperty("CorporateNameLocal1"))).getAttribute("value");
		count = TempName.length();
		if (count <= 400) {
			// Reporter.log(TempName);
			Reporter.log("-.The Reg_Address_Latin  is less than equal to 400 and the count=" + count);
			Reporter.log("-.TC_Corporate_013 executed successfuly");
		} else {

			Reporter.log(TempName);
			Reporter.log("-.The Reg_Address_Latin is more than 400 and the count =" + count);
			Assert.fail();
		}

	}

	//
	//
	public void DataCheckCorp(String CommonCustomerID, String CorporateNameLatinCharacter,
			String CorporateNameLocalCharacter1, String CorporateNameLocalCharacter2, String CountryName,
			String CountryNameCode, String HeadquarterPostalNumber, String HeadquarterAddressLatinCharacter,
			String HeadquarterAddressLocalCharacter, String RegisteredPostalNumber,
			String RegisteredAddressLatinCharacter, String RegisteredAddressLocalCharacter, String VATNumber) {

		String CommonCustomerIDVerification = driver.findElement(By.id("common_customer_id_c")).getText();
		String CorporateNameLatinCharacterVerification = driver.findElement(By.id("name")).getText();
		String CorporateNameLocalCharacter1Verification = driver.findElement(By.id("name_2_c")).getText();
		String CorporateNameLocalCharacter2Verification = driver.findElement(By.id("name_3_c")).getText();
		String CountryNameVerification = driver.findElement(By.id("country_name_c")).getText();
		String CountryNameCodeVerification = driver.findElement(By.id("country_code_c")).getText();
		String HeadquarterPostalNumberVerification = driver.findElement(By.id("hq_postal_no_c")).getText();
		String HeadquarterAddressLatinCharacterVerification = driver.findElement(By.id("hq_addr_1_c")).getText();
		String HeadquarterAddressLocalCharacterVerification = driver.findElement(By.id("hq_addr_2_c")).getText();
		String RegisteredPostalNumberVerification = driver.findElement(By.id("reg_postal_no_c")).getText();
		String RegisteredAddressLatinCharacterVerification = driver.findElement(By.id("reg_addr_1_c")).getText();
		String RegisteredAddressLocalCharacterVerification = driver.findElement(By.id("reg_addr_2_c")).getText();
		// driver.findElement(By.id("vat_no_c")).sendKeys(VATNumber);

		// Verify if the common customer id is same
		if (CommonCustomerIDVerification.equalsIgnoreCase(CommonCustomerID)) {
			Reporter.log(
					CommonCustomerID + "The CommonCustomer id is matching with input" + CommonCustomerIDVerification);
		} else {
			Reporter.log(CommonCustomerID + "The CommonCustomer id is not matching with input"
					+ CommonCustomerIDVerification);
			Assert.fail(CommonCustomerID + "The CommonCustomer id is not matching with input"
					+ CommonCustomerIDVerification);
		}
		//
		// Verify if the Customer Name Latin feild
		if (CorporateNameLatinCharacterVerification.equalsIgnoreCase(CorporateNameLatinCharacter)) {
			Reporter.log(CorporateNameLatinCharacter + "The Corporation name latin is matching with input"
					+ CorporateNameLatinCharacterVerification);
		} else {
			Reporter.log(CorporateNameLatinCharacter + "The Corporation name latin  is not matching with input"
					+ CorporateNameLatinCharacterVerification);
			Assert.fail(CorporateNameLatinCharacter + "The Corporation name latin  is not  matching with input"
					+ CorporateNameLatinCharacterVerification);
		}

		// Verify if the Customer Name Local1 feild
		if (CorporateNameLocalCharacter1Verification.equalsIgnoreCase(CorporateNameLocalCharacter1)) {
			Reporter.log(CorporateNameLocalCharacter1Verification + "TheCorporation name local  is matching with input"
					+ CorporateNameLocalCharacter1);
		} else {
			Reporter.log(CorporateNameLocalCharacter1 + "The Corporation name local is not matching with input"
					+ CorporateNameLocalCharacter1Verification);
			Assert.fail(CorporateNameLocalCharacter1 + "The TheCorporation name local is not  matching with input"
					+ CorporateNameLocalCharacter1Verification);
		}

		// Verify if the Customer Name Local2 feild
		if (CorporateNameLocalCharacter2Verification.equalsIgnoreCase(CorporateNameLocalCharacter2)) {
			Reporter.log(CorporateNameLocalCharacter2Verification + "The customer name local is matching with input"
					+ CorporateNameLocalCharacter2);
		} else {
			Reporter.log(CorporateNameLocalCharacter2Verification + "The customer name local is not matching with input"
					+ CorporateNameLocalCharacter2);
			Assert.fail(CorporateNameLocalCharacter2Verification + "The customer name local is not  matching with input"
					+ CorporateNameLocalCharacter2);
		}

		// Verify if the countyrName
		if (CountryNameVerification.equalsIgnoreCase(CountryName)) {
			Reporter.log(CountryNameVerification + ":The Country Name  is matching with input:" + CountryName);
		} else {
			Reporter.log(CountryNameVerification + ":The  Country Name is not matching with input:" + CountryName);
			Assert.fail(CountryNameVerification + ":The  Country Name is not  matching with input:" + CountryName);
		}

		// Verify if the countyrcode
		if (CountryNameCodeVerification.equalsIgnoreCase(CountryNameCode)) {
			Reporter.log(CountryNameCodeVerification + "The country code is matching with input" + CountryNameCode);
		} else {
			Reporter.log(CountryNameCodeVerification + "The country code is not matching with input" + CountryNameCode);
			Assert.fail(CountryNameCodeVerification + "The country code is not  matching with input" + CountryNameCode);
		}

		// Verify if the Head Postal address
		if (HeadquarterPostalNumberVerification.equalsIgnoreCase(HeadquarterPostalNumber)) {
			Reporter.log(HeadquarterPostalNumberVerification + "The  HQ postal number is matching with input"
					+ HeadquarterPostalNumber);
		} else {
			// Reporter.log(HeadquarterPostalNumberVerification + "The HQ postal
			// number is not matching with input" +HeadquarterPostalNumber);
			Assert.fail(HeadquarterPostalNumberVerification + "The HQ postal number is not  matching with input"
					+ HeadquarterPostalNumber);
			System.out.println("HeadquarterPostalNumberVerification");
		}

		// Verify if the Head quaters Latin address
		if (HeadquarterAddressLatinCharacterVerification.equalsIgnoreCase(HeadquarterAddressLatinCharacter)) {
			Reporter.log(HeadquarterAddressLatinCharacterVerification + "The  HQ Address Latin is matching with input"
					+ HeadquarterAddressLatinCharacter);
		} else {
			Reporter.log(HeadquarterAddressLatinCharacterVerification
					+ "The HQ Address Latin is not matching with input" + HeadquarterAddressLatinCharacter);
			Assert.fail(HeadquarterAddressLatinCharacterVerification
					+ "The HQ Address Latin is not  matching with input" + HeadquarterAddressLatinCharacter);
		}

		// Verify if the Head quaters Local address
		if (HeadquarterAddressLocalCharacterVerification.equalsIgnoreCase(HeadquarterAddressLocalCharacter)) {
			Reporter.log(HeadquarterAddressLocalCharacterVerification + "The HQ Address Local is matching with input"
					+ HeadquarterAddressLocalCharacter);
		} else {
			Reporter.log(HeadquarterAddressLocalCharacterVerification
					+ "The HQ Address Local is not matching with input" + HeadquarterAddressLocalCharacter);
			Assert.fail(HeadquarterAddressLocalCharacterVerification
					+ "The HQ Address Local is not  matching with input" + HeadquarterAddressLocalCharacter);
		}

		// Verify if the Postasl address
		if (RegisteredPostalNumberVerification.equalsIgnoreCase(RegisteredPostalNumber)) {
			Reporter.log(RegisteredPostalNumberVerification + "The postal number is matching with input"
					+ RegisteredPostalNumber);
		} else {
			Reporter.log(RegisteredPostalNumberVerification + "The postal number is not matching with input"
					+ RegisteredPostalNumber);
			Assert.fail(RegisteredPostalNumberVerification + "The postal number is not  matching with input"
					+ RegisteredPostalNumber);
		}

		// Verify if the Registerded address latin characters
		if (RegisteredAddressLatinCharacterVerification.equalsIgnoreCase(RegisteredAddressLatinCharacter)) {
			Reporter.log(RegisteredAddressLatinCharacterVerification
					+ "The registered address latin is matching with input" + RegisteredAddressLatinCharacter);
		} else {
			Reporter.log(RegisteredAddressLatinCharacterVerification
					+ "The registered address latin is not matching with input" + RegisteredAddressLatinCharacter);
			Assert.fail(RegisteredAddressLatinCharacterVerification
					+ "The registered address latin is not  matching with input" + RegisteredAddressLatinCharacter);
		}

		// Verify if the Registerded address local characters
		if (RegisteredAddressLocalCharacterVerification.equalsIgnoreCase(RegisteredAddressLocalCharacter)) {
			Reporter.log(RegisteredAddressLocalCharacterVerification
					+ "The registered address local  is matching with input" + RegisteredAddressLocalCharacter);
		} else {
			Reporter.log(RegisteredAddressLocalCharacterVerification
					+ "The registered address local is not matching with input" + RegisteredAddressLocalCharacter);
			Assert.fail(RegisteredAddressLocalCharacterVerification
					+ "The registered address local is not  matching with input" + RegisteredAddressLocalCharacter);
		}

	}

	public void corporateVerifyData(String CommonCustomerID, String CorporateNameLatinCharacter,
			String CorporateNameLocalCharacter1, String CorporateNameLocalCharacter2, String CountryName,
			String CountryNameCode, String HeadquarterPostalNumber, String HeadquarterAddressLatinCharacter,
			String HeadquarterAddressLocalCharacter, String RegisteredPostalNumber,
			String RegisteredAddressLatinCharacter, String RegisteredAddressLocalCharacter) {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("bwc-frame")));
		String commonCustId = driver.findElement(By.id("common_customer_id_c")).getText();
		String corpNameLatin = driver.findElement(By.id("name")).getText();
		String corpNameLoc1 = driver.findElement(By.id("name_2_c")).getText();
		String corpNameLoc2 = driver.findElement(By.id("name_3_c")).getText();
		String cntryName = driver.findElement(By.id("country_name_c")).getText();
		String cntryCodeName = driver.findElement(By.id("country_code_c")).getText();
		String hqPostNumber = driver.findElement(By.id("hq_postal_no_c")).getText();
		String hqAddressLatin = driver.findElement(By.id("hq_addr_1_c")).getText();
		String hqAddressLocal = driver.findElement(By.id("hq_addr_2_c")).getText();
		String regPostNumber = driver.findElement(By.id("reg_postal_no_c")).getText();
		String regAddressLatin = driver.findElement(By.id("reg_addr_1_c")).getText();
		String regAddressLocal = driver.findElement(By.id("reg_addr_2_c")).getText();
		// Verification for Common Customer Id
		/*
		 * if(commonCustId.equalsIgnoreCase(CommonCustomerID)){
		 * System.out.println("CommonCustomerId is matching with input value");
		 * Reporter.log(CommonCustomerID +
		 * "The CommonCustomer id is matching with input" + commonCustId); }
		 * else{ System.out.
		 * println("CommonCustomerId is not matching with input value");
		 * Reporter.log(CommonCustomerID +
		 * "The CommonCustomer id is not matching with input" + commonCustId);
		 * Assert.fail(CommonCustomerID +
		 * "The CommonCustomer id is not matching with input" + commonCustId); }
		 */

		// Verification for Corporate Name (Latin Character)
		if (CorporateNameLatinCharacter.equalsIgnoreCase(corpNameLatin)) {
			// System.out.println(CorporateNameLatinCharacter + "The Corporation
			// name latin is matching with input" + corpNameLatin);
			Reporter.log(
					CorporateNameLatinCharacter + "The Corporation name latin is matching with input" + corpNameLatin);
		} else {
			// System.out.println(CorporateNameLatinCharacter + "The Corporation
			// name latin is not matching with input" + corpNameLatin);
			Reporter.log(CorporateNameLatinCharacter + "The Corporation name latin is not matching with input"
					+ corpNameLatin);
			Assert.fail(CorporateNameLatinCharacter + "The Corporation name latin is not matching with input"
					+ corpNameLatin);
		}

		// Verification for Corporate Name (Local Character 1)
		if (CorporateNameLocalCharacter1.equalsIgnoreCase(corpNameLoc1)) {
			// System.out.println(CorporateNameLocalCharacter1 + "The
			// Corporation name local 1 is matching with input" + corpNameLoc1);
			Reporter.log(CorporateNameLocalCharacter1 + "The Corporation name local 1 is matching with input"
					+ corpNameLoc1);
		} else {
			// System.out.println(CorporateNameLocalCharacter1 + "The
			// Corporation name local 1 is not matching with input" +
			// corpNameLoc1);
			Reporter.log(CorporateNameLocalCharacter1 + "The Corporation name local 1 is not matching with input"
					+ corpNameLoc1);
			Assert.fail(CorporateNameLocalCharacter1 + "The Corporation name local 1 is not matching with input"
					+ corpNameLoc1);
		}

		// Verification for Corporate Name (Local Character 2)
		if (CorporateNameLocalCharacter2.equalsIgnoreCase(corpNameLoc2)) {
			// System.out.println(CorporateNameLocalCharacter2 + "The
			// Corporation name local 2 is matching with input" + corpNameLoc2);
			Reporter.log(CorporateNameLocalCharacter2 + "The Corporation name local 2 is matching with input"
					+ corpNameLoc2);
		} else {
			// System.out.println(CorporateNameLocalCharacter2 + "The
			// Corporation name local 2 is not matching with input" +
			// corpNameLoc2);
			Reporter.log(CorporateNameLocalCharacter2 + "The Corporation name local 2 is not matching with input"
					+ corpNameLoc2);
			Assert.fail(CorporateNameLocalCharacter2 + "The Corporation name local 2 is not matching with input"
					+ corpNameLoc2);
		}

		// Verification for Country Name
		if (CountryName.equalsIgnoreCase(cntryName)) {
			// System.out.println(CountryName + "The Country name is matching
			// with input" + cntryName);
			Reporter.log(CountryName + "The Country name is matching with input" + cntryName);
		} else {
			// System.out.println(CountryName + "The Country name is not
			// matching with input" + cntryName);
			Reporter.log(CountryName + "The Country name is not matching with input" + cntryName);
			// Assert.fail(CountryName + "The Country name is not matching with
			// input" + cntryName);
		}

		// Verification for Country Code Name
		/*
		 * if(CountryNameCode.equalsIgnoreCase(cntryCodeName)){
		 * System.out.println(CountryNameCode +
		 * "The Country Name Code is matching with input" + cntryCodeName);
		 * Reporter.log(CountryNameCode +
		 * "The Country Name Code is matching with input" + cntryCodeName); }
		 * else { System.out.println(CountryNameCode +
		 * "The Country Name Code is not matching with input" + cntryCodeName);
		 * Reporter.log(CountryNameCode +
		 * "The Country Name Code is not matching with input" + cntryCodeName);
		 * //Assert.fail(CountryNameCode +
		 * "The Country Name Code is not matching with input" + cntryCodeName);
		 * }
		 */

		// Verification for Headquarter Postal Number
		if (HeadquarterPostalNumber.equalsIgnoreCase(hqPostNumber)) {
			// System.out.println(HeadquarterPostalNumber + "The Headquarter
			// Postal Number is matching with input" + hqPostNumber);
			Reporter.log(
					HeadquarterPostalNumber + "The Headquarter Postal Number is matching with input" + hqPostNumber);
		} else {
			// System.out.println(HeadquarterPostalNumber + "The Headquarter
			// Postal Number is not matching with input" + hqPostNumber);
			Reporter.log(HeadquarterPostalNumber + "The Headquarter Postal Number is not matching with input"
					+ hqPostNumber);
			// Assert.fail(HeadquarterPostalNumber + "The Headquarter Postal
			// Number is not matching with input" + hqPostNumber);
		}

		// Verification for Headquarter Address (Latin Character)
		if (HeadquarterAddressLatinCharacter.equalsIgnoreCase(hqAddressLatin)) {
			/// System.out.println(HeadquarterAddressLatinCharacter + "The
			/// Headquarter Address (Latin Character) is matching with input" +
			/// hqAddressLatin);
			Reporter.log(HeadquarterAddressLatinCharacter
					+ "The Headquarter Address (Latin Character) is matching with input" + hqAddressLatin);
		} else {
			// System.out.println(HeadquarterAddressLatinCharacter + "The
			// Headquarter Address (Latin Character) is not matching with input"
			// + hqAddressLatin);
			Reporter.log(HeadquarterAddressLatinCharacter
					+ "The Headquarter Address (Latin Character) is not matching with input" + hqAddressLatin);
			// Assert.fail(HeadquarterAddressLatinCharacter + "The Headquarter
			// Address (Latin Character) is not matching with input" +
			// hqAddressLatin);
		}

		// Verification for Headquarter Address (Local Character)
		if (HeadquarterAddressLocalCharacter.equalsIgnoreCase(hqAddressLocal)) {
			// System.out.println("Headquarter Address (Local Character)
			// matching with input value");
			Reporter.log(HeadquarterAddressLatinCharacter
					+ "The Headquarter Address (Local Character) is matching with input" + hqAddressLocal);
		} else {
			// System.out.println("Headquarter Address (Latin Character) not
			// matching with input value");
			Reporter.log(HeadquarterAddressLatinCharacter
					+ "The Headquarter Address (Local Character) is not matching with input" + hqAddressLocal);
			// Assert.fail(HeadquarterAddressLatinCharacter + "The Headquarter
			// Address (Local Character) is not matching with input" +
			// hqAddressLocal);
		}

		// Verification for Registered Postal Number
		if (RegisteredPostalNumber.equalsIgnoreCase(regPostNumber)) {
			// System.out.println(RegisteredPostalNumber + "The Registered
			// Postal Number is matching with input" + regPostNumber);
			Reporter.log(
					RegisteredPostalNumber + "The Registered Postal Number is matching with input" + regPostNumber);
		} else {
			// System.out.println(RegisteredPostalNumber + "The Registered
			// Postal Number is not matching with input" + regPostNumber);
			Reporter.log(
					RegisteredPostalNumber + "The Registered Postal Number is not matching with input" + regPostNumber);
			// Assert.fail(RegisteredPostalNumber + "The Registered Postal
			// Number is not matching with input" + regPostNumber);
		}

		// Verification for Registered Address (Latin Character)
		if (RegisteredAddressLatinCharacter.equalsIgnoreCase(regAddressLatin)) {
			// System.out.println(RegisteredAddressLatinCharacter + "The
			// Registered Address (Latin Character) is matching with input" +
			// regAddressLatin);
			Reporter.log(RegisteredAddressLatinCharacter
					+ "The Registered Address (Latin Character) is matching with input" + regAddressLatin);
		} else {
			// System.out.println(RegisteredAddressLatinCharacter + "The
			// Registered Address (Latin Character) is not matching with input"
			// + regAddressLatin);
			Reporter.log(RegisteredAddressLatinCharacter
					+ "The Registered Address (Latin Character) is not matching with input" + regAddressLatin);
			// Assert.fail(RegisteredAddressLatinCharacter + "The Registered
			// Address (Latin Character) is not matching with input" +
			// regAddressLatin);
		}

		// Verification for Registered Address (Local Character)
		if (RegisteredAddressLocalCharacter.equalsIgnoreCase(regAddressLocal)) {
			// System.out.println(RegisteredAddressLocalCharacter + "The
			// Registered Address (Local Character) is matching with input" +
			// regAddressLocal);
			Reporter.log(RegisteredAddressLocalCharacter
					+ "The Registered Address (Local Character) is matching with input" + regAddressLocal);
		} else {
			// System.out.println(RegisteredAddressLocalCharacter + "The
			// Registered Address (Local Character) is not matching with input"
			// + regAddressLocal);
			Reporter.log(RegisteredAddressLocalCharacter
					+ "The Registered Address (Local Character) is not matching with input" + regAddressLocal);
			// Assert.fail(RegisteredAddressLocalCharacter + "The Registered
			// Address (Local Character) is not matching with input" +
			// regAddressLocal);
		}
	}

	public void corporateFillData(String CommonCustomerID, String CorporateNameLatinCharacter,
			String CorporateNameLocalCharacter1, String CorporateNameLocalCharacter2, String CountryName,
			String CountryNameCode, String HeadquarterPostalNumber, String HeadquarterAddressLatinCharacter,
			String HeadquarterAddressLocalCharacter, String RegisteredPostalNumber,
			String RegisteredAddressLatinCharacter, String RegisteredAddressLocalCharacter)
			throws InterruptedException {

		driver.findElement(By.id("common_customer_id_c")).sendKeys(CommonCustomerID);
		driver.findElement(By.id("name")).sendKeys(CorporateNameLatinCharacter);
		driver.findElement(By.id("name_2_c")).sendKeys(CorporateNameLocalCharacter1);
		driver.findElement(By.id("name_3_c")).sendKeys(CorporateNameLocalCharacter2);
		driver.findElement(By.id("country_name_c")).sendKeys(CountryName);
		driver.findElement(By.id("country_code_c")).sendKeys(CountryNameCode);
		driver.findElement(By.id("hq_postal_no_c")).sendKeys(HeadquarterPostalNumber);
		driver.findElement(By.id("hq_addr_1_c")).sendKeys(HeadquarterAddressLatinCharacter);
		driver.findElement(By.id("hq_addr_2_c")).sendKeys(HeadquarterAddressLocalCharacter);
		driver.findElement(By.id("reg_postal_no_c")).sendKeys(RegisteredPostalNumber);
		driver.findElement(By.id("reg_addr_1_c")).sendKeys(RegisteredAddressLatinCharacter);
		driver.findElement(By.id("reg_addr_2_c")).sendKeys(RegisteredAddressLocalCharacter);
	}

	public void CidasEditCheck() {
		System.out.println("Inside Cideas *************edit check ");
		try {
			driver.findElement(By.id("common_customer_id_c")).clear();

		} catch (Exception e) {
			System.out.println("Inside Cideas *************CCID ");
			Reporter.log("Common customer id is not editable " + e);

		}

		try {
			driver.findElement(By.id("name")).clear();

		} catch (Exception e) {
			Reporter.log("CorporateName Latin is not editable " + e);
		}

		try {
			driver.findElement(By.id("name_2_c")).clear();

		} catch (Exception e) {
			Reporter.log("CorporateName Local1 is not editable " + e);
		}

		try {
			driver.findElement(By.id("name_3_c")).clear();

		} catch (Exception e) {
			Reporter.log("CorporateName Local2 is not editable " + e);
		}

		try {
			driver.findElement(By.id("country_name_c")).clear();

		} catch (Exception e) {
			Reporter.log("Country Name  is not editable " + e);
		}

		try {
			driver.findElement(By.id("country_code_c")).clear();

		} catch (Exception e) {
			Reporter.log("Country code  is not editable " + e);
		}

		try {
			driver.findElement(By.id("hq_postal_no_c")).clear();

		} catch (Exception e) {
			Reporter.log("Head quaters postal  code  is not editable " + e);
		}

		try {
			driver.findElement(By.id("hq_addr_1_c")).clear();

		} catch (Exception e) {
			Reporter.log("Head address l   is not editable " + e);
		}

		try {
			driver.findElement(By.id("hq_addr_2_c")).clear();

		} catch (Exception e) {
			Reporter.log("Head address 2   is not editable " + e);
		}

		try {
			driver.findElement(By.id("reg_postal_no_c")).clear();

		} catch (Exception e) {
			Reporter.log("Registered postal   is not editable " + e);
		}

		try {
			driver.findElement(By.id("reg_addr_1_c")).clear();

		} catch (Exception e) {
			Reporter.log("Registered address 1   is not editable " + e);
		}

		try {
			driver.findElement(By.id("reg_addr_2_c")).clear();

		} catch (Exception e) {
			Reporter.log("Registered address 2   is not editable " + e);
		}

	}

}
