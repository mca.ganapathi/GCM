<?php

require '../SugarCRM/custom/include/nttcomUUID/nttcomUUID.php';

/**
 * Test class for NTTComUUID.
 */
class NTTComUUIDTest extends PHPUnit_Framework_TestCase
{
    /** SystemID(GCM) */
    const SYSTEM_ID = 730107;

    public static function setUpBeforeClass()
    {
        NTTComUUID::setSystemId(self::SYSTEM_ID);
    }

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
    }

    public function testIsValid()
    {
        $this->assertTrue(NTTComUUID::isValid());
    }

    public function testUuidObject()
    {
        $uuidObj = NTTComUUID::randomUUID();
        $this->assertEquals(self::SYSTEM_ID, $uuidObj->systemId());
    }


    public function testRandomUUID()
    {
        // Creates new array to keep generated UUID
        $uuid_collection = array();

        for($i=0; $i<10000; $i++)
        {
            // Generates UUID
            $uuid = NTTComUUID::randomUUID()->toString();
            // Checks that generated UUID is unique
            $this->assertFalse(in_array($uuid, $uuid_collection));

            // Adds element to array
            $uuid_collection[] = $uuid;
        }
        //var_dump($uuid_collection); // *For visual confirmation
    }

    public function testFromString()
    {
        // This test must executes under 64bit-PHP. So checks PHP_INT_SIZE.
        if(PHP_INT_SIZE==4) {  // 32bit
            return;
        }
        
        // Creates new array to keep generated UUID
        $uuid_collection = array();

        for($i=0; $i<10000; $i++)
        {
            // Generates UUID
            $uuid_collection[] = NTTComUUID::randomUUID()->toString();
        }
        //var_dump($uuid_collection); // *For visual confirmation

        //Convert from UUID string to object. Then asserts SystemID value.
        foreach($uuid_collection as $uuid_str)
        {
            $uuid_obj = NTTComUUID::fromString($uuid_str);
            $this->assertEquals(self::SYSTEM_ID, $uuid_obj->systemId());
        }
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }
}
?>