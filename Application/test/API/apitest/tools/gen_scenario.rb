#!/usr/bin/env ruby

def gen_header
  <<"EOS"
  # encoding: utf-8
  # language: en

  @expectation_steps
  @operation_steps
  @api_steps
  Feature: APIv2
    Background:
      Given initialize a test environment
      And create a session ID
EOS
end

def gen_post_scenario(title, req, res)
  <<"EOS"
  Scenario: #{title}
    Given copy "#{req}" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    And save the response body to "#{res}"
    And the response body corresponds with the content of "#{res}" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
EOS
end

def gen_get_scenario(title, req, res)
  <<"EOS"
  Scenario: #{title}
    Given copy "#{req}" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And send a "GET" request to "/api/v2/contract-information/@{UUID}"
    # Then the status code of the HTTP response is "200"
    # And write the response body to stdout
    And save the response body to "#{res}"
    And the response body corresponds with the content of "#{res}" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
EOS
end

def extract_filepath_in_scenario(filepath)
  return nil unless filepath
  ns = filepath.split "/"
  "#{ns[-4]}/#{ns[-3]}/#{ns[-2]}/#{ns[-1]}"
end

dir = ARGV[0]

scenarios = [gen_header]

dirs = Dir.glob(dir + '**/**/*.xml').map { |f| File.dirname(f) }
dirs.uniq.each do |d|
  names = d.split '/'
  title = "#{names[-2]}/#{names[-1]}"

  xmls = {}
  filenames = Dir.glob(d + '/**/*.xml')

  fs = filenames.select { |f| f.include?('GET') }
  xmls[:get_res] = extract_filepath_in_scenario fs.first unless fs.empty?

  fs = filenames.select do |f|
    f.include?('POST') && (f.include?('REQ') || f.include?('Req'))
  end
  xmls[:post_req] = extract_filepath_in_scenario fs.first

  fs = filenames.select do |f|
    f.include?('POST') && !f.include?('GET') && (f.include?('RES') || f.include?('Res'))
  end
  xmls[:post_res] = extract_filepath_in_scenario fs.first

  raise 'XML of POST Request Not Found' if xmls[:post_req].nil?
  raise 'XML of POST Response Not Found' if xmls[:post_res].nil?

  scenarios << gen_post_scenario("#{title} POST", xmls[:post_req], xmls[:post_res])
  # scenarios << gen_get_scenario("#{title} GET", xmls[:post_req], xmls[:get_res]) if xmls[:get_res]
end

puts scenarios.join("\n")
