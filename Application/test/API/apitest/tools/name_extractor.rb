#!/usr/bin/env ruby

require 'rexml/document'

filename = ARGV[0]
doc = REXML::Document.new File.read(filename)
doc.elements.each('/contract-info:contract-information/*/*') do |e|
  name = e.elements['cbe:name/text()']
  if name
    if e.name == 'atomic-product-price'
      object_ids = REXML::XPath.match e, './/product:reference-to-atomic-product//cbe:object-id/text()'
      e.add_attribute 'name', "#{name} - #{object_ids[0]}"
    else
      e.add_attribute 'name', name
    end
  else
    if e.name == 'product-biitem'
      object_ids = REXML::XPath.match e, './/cbe:object-id/text()'
      e.add_attribute 'name', object_ids[2]
    else
      object_ids = REXML::XPath.match e, './/cbe:object-id/text()'
      e.add_attribute 'name', object_ids[1]
    end
  end
end

puts doc.to_s.gsub(/\'(.+?)\'/, '"\1"')
