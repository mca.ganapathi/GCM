require 'active_support/all'

Encoding.default_external = 'UTF-8'

Dir["#{SPEC_ROOT}/lib/*.rb"].each { |f| require f }
Dir["#{SPEC_ROOT}/steps/**/*steps.rb"].each { |f| require f }

module AutoTest
  class <<self
    attr_accessor :logger
  end
end

AutoTest.logger = Logger.new APP_ROOT + '/' + Settings.logger.filename
AutoTest.logger.level = Logger.const_get Settings.logger.level

RSpec.configure do |config|
  config.before :each do
    $data = {}

    AutoTest.logger.info "Execute: #{::RSpec.current_example.full_description}"
  end

  config.after :each do
    AutoTest.logger.info "Result: #{::RSpec.current_example.execution_result}"
  end
end
