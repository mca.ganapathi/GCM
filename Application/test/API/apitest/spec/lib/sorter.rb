#!/usr/bin/env ruby

require 'rexml/document'
require 'cgi'
require 'optparse'

class XMLSorter

  def initialize
    @debug_mode = false
    @debug_msg = ''
  end

  def sort(doc)
    sort_atomic_product doc
    sort_atomic_product_price doc
    sort_biitem doc
    sort_characteristic_value doc
    # remove_comment doc
  end

  def debug_mode=(b)
    @debug_mode = b
  end

  def debug_message
    @debug_msg
  end

  private

  def debug(msg)
    @debug_msg << msg + "\n" if @debug_mode
  end

  def sort_atomic_product(doc)
    doc.elements.each('/contract-info:contract-information/product:atomic-products') do |e|
      atomic_products = e.elements

      new_els = []
      atomic_products.each do |el|
        object_id = el.elements['.//product:reference-to-atomic-product-offering//cbe:object-id/text()']
        new_els << [object_id, el]

        doc.delete_element '/contract-info:contract-information/product:atomic-products/product:atomic-product'
      end
      new_els.sort! { |v1, v2| v1[0] <=> v2[0] }
      debug '<< atomic-product >>'
      new_els.each do |k, v|
        debug k.to_s
        e.add_element v
      end
    end
    doc
  end

  def sort_atomic_product_price(doc)
    doc.elements.each('/contract-info:contract-information/product:atomic-product-prices') do |e|
      atomic_product_prices = e.elements

      new_els = []
      atomic_product_prices.each do |el|
        ap_loc_path = el.elements['.//product:reference-to-atomic-product//cbe:xml-path/cbe:location-path/text()']
        path = "//#{ap_loc_path.value}//product:reference-to-atomic-product-offering//cbe:object-id/text()"
        first_key = doc.elements[path]

        second_key = el.elements['.//product:atomic-product-offering-price-key//cbe:object-id/text()']
        new_els << [[first_key, second_key], el]

        doc.delete_element '/contract-info:contract-information/product:atomic-product-prices/product:atomic-product-price'
      end

      new_els.sort! do |v1, v2|
        keys1 = v1[0]
        keys2 = v2[0]
        keys1[0] != keys2[0] ? keys1[0] <=> keys2[0] : keys1[1] <=> keys2[1]
      end
      debug '<< atomic-product-price >>'
      new_els.each do |v|
        debug v[0].inspect
        e.add_element v[1]
      end
    end
    doc
  end

  def sort_biitem(doc)
    doc.elements.each('/contract-info:contract-information/bi:product-biitems') do |e|
      new_els = []
      e.elements.each do |el|
        ap_loc_path = el.elements['.//product:reference-to-atomic-product//cbe:xml-path/cbe:location-path/text()']
        path = "//#{ap_loc_path.value}//product:reference-to-atomic-product-offering//cbe:object-id/text()"

        first_key = doc.elements[path]
        second_key = el.elements['.//bi:reference-to-product-biitem-spec//cbe:object-id/text()']
        new_els << [[first_key, second_key], el]

        doc.delete_element '/contract-info:contract-information/bi:product-biitems/bi:product-biitem'
      end

      new_els.sort! do |v1, v2|
        keys1 = v1[0]
        keys2 = v2[0]
        keys1[0] != keys2[0] ? keys1[0] <=> keys2[0] : keys1[1] <=> keys2[1]
      end
      debug '<< product-biitem >>'
      new_els.each do |v|
        debug v[0].inspect
        e.add_element v[1]
      end
    end
    doc
  end

  # TODO: refactor
  def sort_characteristic_value(doc)
    doc.elements.each('/contract-info:contract-information/cbe:characteristic-values') do |e|
      es = {
        atomic_product: [],
        biitem: [],
        price: [],
        product_contract: []
      }
      e.elements.each do |el|
        location_path = el.elements[".//cbe:association-end[./cbe:no/text()='1']//cbe:location-path/text()"]
        tag = location_path.to_s.split('/')[0]
        key = case tag
        when /^product\:atomic-products.*/
          :atomic_product
        when /^bi\:product-biitems.*/
          :biitem
        when /^bi\:product-contracts.*/
          :product_contract
        when /^product\:atomic-product-prices.*/
          :price
        else
          raise "Can not sort characteristic values for #{tag}"
        end
        es[key] << el
      end

      sorted = []
      els = []
      es[:atomic_product].each do |el|
          loc_path = CGI.unescapeHTML el.elements[".//cbe:association-end[./cbe:no/text()='1']//cbe:location-path/text()"].to_s
          spec_path = "/contract-info:contract-information/#{loc_path}/cbe:described-by//cbe:object-id/text()"

          first_key = doc.elements[spec_path]
          second_key = el.elements['.//cbe:reference-to-atomic-entity-spec-charc//cbe:object-id/text()']
          third_key = el.elements['./*[last()]']
          els << [[first_key, second_key, third_key], el]
      end
      sorted.concat els.sort { |v1, v2| three_keys_sort v1[0], v2[0] }

      els = []
      es[:product_contract].each do |el|
          loc_path = CGI.unescapeHTML el.elements[".//cbe:association-end[./cbe:no/text()='1']//cbe:location-path/text()"].to_s
          spec_path = "/contract-info:contract-information/#{loc_path}/cbe:described-by//cbe:object-id/text()"

          first_key = doc.elements[spec_path]
          second_key = el.elements['.//cbe:reference-to-atomic-entity-spec-charc//cbe:object-id/text()']
          third_key = el.elements['./*[last()]']
          els << [[first_key, second_key, third_key], el]
      end
      sorted.concat els.sort { |v1, v2| three_keys_sort v1[0], v2[0] }

      els = []
      es[:biitem].each do |el|
        # path to biitem
        loc_path = CGI.unescapeHTML el.elements[".//cbe:association-end[./cbe:no/text()='1']//cbe:location-path/text()"].to_s
        # path to atomic-product
        spec_path = "/contract-info:contract-information/#{loc_path}/cbe:association-ends/cbe:association-end[./cbe:no/text()='2']//cbe:location-path/text()"
        loc_path = CGI.unescapeHTML el.elements[spec_path].to_s
        # atomic-produc-offering id
        spec_path = "/contract-info:contract-information/#{loc_path}/cbe:described-by//cbe:object-id/text()"

        first_key = el.elements[spec_path]
        second_key = el.elements['.//cbe:reference-to-atomic-entity-spec-charc//cbe:object-id/text()']
        third_key = el.elements['./*[last()]']
        els << [[first_key, second_key, third_key], el]
      end
      sorted.concat els.sort { |v1, v2| three_keys_sort v1[0], v2[0] }

      els = []
      es[:price].each do |el|
        # path to atomic-product-price
        loc_path = CGI.unescapeHTML el.elements[".//cbe:association-end[./cbe:no/text()='1']//cbe:location-path/text()"].to_s
        # path to atomic-product
        spec_path = "/contract-info:contract-information/#{loc_path}/cbe:association-ends/cbe:association-end[./cbe:no/text()='2']//cbe:location-path/text()"
        ap_loc_path = CGI.unescapeHTML el.elements[spec_path].to_s
        # atomic-produc-offering id
        spec_path = "/contract-info:contract-information/#{ap_loc_path}/cbe:described-by//cbe:object-id/text()"

        first_key = el.elements[spec_path]
        # atomic-produc-offering-price-key id
        second_key = el.elements["/contract-info:contract-information/#{loc_path}/cbe:association-ends/cbe:association-end[./cbe:no/text()='1']//cbe:object-id/text()"]
        third_key = el.elements['.//cbe:reference-to-atomic-entity-spec-charc//cbe:object-id/text()']
        forth_key = el.elements['./*[last()]']
        els << [[first_key, second_key, third_key, forth_key], el]
      end
      sorted.concat els.sort { |v1, v2| three_keys_sort v1[0], v2[0] }

      sorted.size.times.each do |_|
        doc.delete_element '/contract-info:contract-information/cbe:characteristic-values/cbe:characteristic-value'
      end

      debug '<< characteristic-value >>'
      sorted.each do |v|
        debug v[0].inspect + ' - '+ v[0].last.text.inspect
        e.add_element v[1]
      end
    end
    doc
  end

  def three_keys_sort(a, b)
      return a[0] <=> b[0] unless a[0] == b[0]
      return a[1] <=> b[1] unless a[1] == b[1]
      a[2].to_s <=> b[2].to_s
  end

  def remove_comment(doc)
    doc.elements.each('//comment()') { |e| doc.delete_element e }
  end
end

if __FILE__ == $PROGRAM_NAME
  params = ARGV.getopts('d')
  filename = ARGV[0]
  debug_mode = !!params['d']

  doc = REXML::Document.new File.read(filename)
  sorter = XMLSorter.new
  sorter.debug_mode = debug_mode
  sorted = sorter.sort doc

  puts sorter.debug_message if debug_mode
  puts sorted
end
