# -*- coding: utf-8 -*-
require 'capybara/rspec'
require 'selenium-webdriver'
require 'turnip'
require 'turnip/capybara'
require 'turnip/rspec'

require_relative 'settings.rb'

Capybara.register_driver :default_driver do |app|
  profile = Selenium::WebDriver::Firefox::Profile.new
  profile['browser.download.folderList'] = 2
  profile['browser.download.dir'] = '/tmp/'
  profile['browser.helperApps.neverAsk.saveToDisk'] =
    'text/html,application/octet-stream'
  Capybara::Selenium::Driver.new(app, browser: :firefox,
                                      profile: profile)
end

Capybara.default_driver = :default_driver
Capybara.app_host = (Settings.ssl ? "https://" : "http://") + Settings.host
Capybara.default_max_wait_time = 120
