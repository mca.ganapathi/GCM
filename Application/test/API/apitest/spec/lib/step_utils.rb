module StepUtils
  UAT_SESSION_PATH = '/GcmApiClient/oauth_session.php'

  module STATUS
    CREATED = 201
  end

  def generate_https(host, port = 443)
    https = Net::HTTP.new host, port
    https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE
    https
  end

  def generate_http(host, port = 80)
    http = Net::HTTP.new host, port
    http.use_ssl = false
    http
  end

  def fetch_data_file(filename)
    dir = "#{APP_ROOT}/#{Settings.data_dir}"
    File.read "#{dir}/#{filename}"
  end

  def save_to_file(filename, s)
    dir = "#{APP_ROOT}/#{Settings.data_dir}"
    path = File.dirname "#{dir}/#{filename}"
    FileUtils.mkdir_p(path) unless FileTest.exist?(path)
    File.write "#{dir}/#{filename}", s
  end

  def generate_uuid(doc, xpath)
    d = REXML::Document.new doc
    d.elements.each(xpath) { |id| id.text = SecureRandom.uuid }
    d.to_s
  end

  def remove_utf8_bom(s)
    s.force_encoding('UTF-8').gsub(/\xEF\xBB\xBF/, '')
  end

  def strip_each_lines(doc)
    doc.lines.reduce('') do |a, e|
      str = remove_utf8_bom e.strip
      if str.blank?
        a
      elsif str.first != '<'
        a + ' ' + str
      else
        a + str
      end
    end
  end

  def normalize_xpaths(doc, xpaths)
    norm_paths = []
    d = REXML::Document.new doc
    xpaths.each do |p|
      d.elements.each(p) { |e| norm_paths << e.xpath }
    end
    norm_paths
  end

  def stringify_headers(msg)
    hs = ''
    hs = msg.each_header { |k, v| hs << "#{k}: #{v}, " }
    hs.blank? ? '{}' : hs[0..-2]
  end

  def send_http_request(path, method, body = nil, params = {})
    host = params[:host] || Settings.host
    port = params[:port] || Settings.port
    ssl = params.key?(:ssl) ? params[:ssl] : Settings.ssl
    http = ssl ? generate_https(host, port) : generate_http(host, port)
    http.read_timeout = Settings.http_read_timeout

    req = eval("Net::HTTP::#{method.to_s.downcase.camelize}").new path
    req['Host'] = host
    req['Session-Id'] = $data[:session_id]
    if body
      req['Content-Type'] = 'text/xml'
      req.body = body
    end

    (@header_filter || []).each do |f|
      case f[:type]
      when :replace
        req[f[:to]] = req.delete f[:from]
      when :set
        req[f[:name]] = f[:value]
      end
    end

    AutoTest.logger.debug "Send request: method=#{req.method}, path=#{req.path}, " \
                          "headers=#{stringify_headers(req)}, body=#{req.body}"

    @request = req
    res = http.request req

    AutoTest.logger.debug "Receive response: code=#{res.code}, " \
                          "headers=#{stringify_headers(res)}, body=#{res.body}"
    res
  end

  def replace_text(file, xpath, text)
    filename = "#{APP_ROOT}/#{Settings.data_dir}/#{file}"
    doc = REXML::Document.new File.read(filename)
    els = REXML::XPath.match(doc, xpath)
    raise "Element specified with #{xpath} not found" if els.empty?
    els.each { |el| el.text = text}
    File.write filename, doc.to_s
  end

  def set_attributes(file, xpath, attrs)
    filename = "#{APP_ROOT}/#{Settings.data_dir}/#{file}"
    doc = REXML::Document.new File.read(filename)
    els = REXML::XPath.match(doc, xpath)
    raise "Element specified with #{xpath} not found" if els.empty?
    els.each { |el| el.add_attributes attrs }
    File.write filename, doc.to_s
  end

  def remove_elements(file, xpath)
    filename = "#{APP_ROOT}/#{Settings.data_dir}/#{file}"
    doc = REXML::Document.new File.read(filename)
    els = REXML::XPath.match(doc, xpath)
    raise "Element specified with #{xpath} not found" if els.empty?
    doc.delete_element xpath
    File.write filename, doc.to_s
  end

  def embed_parameters(text, params)
    embedded_text = text.dup
    params.each { |k, v| embedded_text.gsub!("@{#{k}}", v.to_s) }
    embedded_text
  end

  def fetch_resource_keys(doc)
    keys = []
    doc.elements.each('/contract-info:contract-information/*/*/cbe:identified-by/*/cbe:key-type-for-cbe-standard/cbe:object-id') do |e|
      keys << e.get_text.value
    end
    keys
  end

  def replace_all_uuids(file, ref)
    filename = "#{APP_ROOT}/#{Settings.data_dir}/#{file}"
    file = File.read filename

    sorter = XMLSorter.new
    sorter.debug_mode = true
    target_doc = sorter.sort(REXML::Document.new(file))
    File.write 'debug-1.txt', sorter.debug_message

    sorter = XMLSorter.new
    sorter.debug_mode = true
    ref_doc = sorter.sort(REXML::Document.new(ref))
    File.write 'debug-2.txt', sorter.debug_message

    target_keys = fetch_resource_keys target_doc
    ref_keys = fetch_resource_keys ref_doc

    t_size = target_keys.size
    r_size = ref_keys.size

    if target_keys.size != ref_keys.size
      time = Time.now.strftime '%Y-%m-%d-%H%M%S'
      prefix = "#{APP_ROOT}/log/dump/#{time}"
      File.write "#{prefix}-t.xml", target_doc.to_s
      File.write "#{prefix}-r.xml", ref_doc.to_s
      raise "Mismatch of the number of object-id, file=#{t_size}, ref=#{r_size}"
    end

    # doc = target_doc.to_s
    # target_keys.size.times do |i|
    #   t = target_keys[i]
    #   r = ref_keys[i]

    #   puts t == r.inspect

    #   # escaped_t = t.gsub '\-', '\\-'
    #   # escaped_r = r.gsub '\-', '\\-'
    #   # file.gsub! escaped_t, escaped_r
    #   doc.gsub! t, r
    # end

    id_prefix = "tmp-"
    doc = file.to_s

    target_keys.size.times do |i|
      t = target_keys[i]
      doc.gsub! t, "#{id_prefix}#{t}"
    end

    target_keys.size.times do |i|
      t = target_keys[i]
      r = ref_keys[i]

      # escaped_t = t.gsub '\-', '\\-'
      # escaped_r = r.gsub '\-', '\\-'
      # file.gsub! escaped_t, escaped_r
      doc.gsub! "#{id_prefix}#{t}", r
    end

    File.write "t.xml", target_doc.to_s
    File.write "r.xml", ref_doc.to_s

    File.write filename, doc.to_s
  end
end
