require 'settingslogic'

class Settings < Settingslogic
  source "#{SPEC_ROOT}/config/settings.yml"
  namespace((ENV['ENV'] && ENV['ENV'].downcase) || 'qa')
end
