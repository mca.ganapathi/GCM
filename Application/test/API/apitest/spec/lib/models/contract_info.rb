require 'rexml/document'

class ContractInfo
  attr_reader :uuid, :doc

  def initialize(uuid, xml)
    @uuid = uuid
    @doc = REXML::Document.new xml, ignore_whitespace_nodes: :all
  end

  def contract_id
    xpath = Settings.xpath.add_response.contract_id
    doc.elements.each(xpath) { |e| return e }
  end
end
