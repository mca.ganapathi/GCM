# -*- coding: utf-8 -*-

require 'net/https'
require 'lorax'
require 'rexml/document'

require_relative '../lib/net/generic_request'
require_relative '../lib/lorax/delta'
require_relative '../lib/lorax/delta_set'
require_relative '../lib/models/contract_info'
require_relative '../lib/step_utils'

steps_for :api_v1_steps do

  # ---------------------- GET API -------------------------------------

  step 'send a request for :id to GET API' do |id|
    send '契約情報取得APIに :id のリクエストを送信する', id
  end

  step '契約情報取得APIに :id のリクエストを送信する' do |id|
    send '契約情報取得APIにUUID :id のリクエストを送信する', $data[:contract_info][id].uuid
  end

  step 'send a request with UUID :id to GET API' do
    send '契約情報取得APIにUUID :id のリクエストを送信する', id
  end

  step '契約情報取得APIにUUID :id のリクエストを送信する' do |id|
    path = Settings.path.contract.get % id
    @response = send_http_request path, :get
  end

  step 'send a request for :id with HTTP method :method to GET API' do |method, id|
    send '契約情報取得APIに :method メソッドで :id のリクエストを送信する', method, id
  end

  step '契約情報取得APIに :method メソッドで :id のリクエストを送信する' do |method, id|
    path = Settings.path.contract.get % $data[:contract_info][id].uuid
    @response = send_http_request path, method
  end

  # ------------------- Search API (GET request with search parameter) ------

  step 'send a request for :id to Search API' do |id|
    send '契約情報検索APIに契約ID :id のリクエストを送信する', id
  end

  step '契約情報検索APIに :id の契約IDを用いてリクエストを送信する' do |id|
    contract_id = $data[:contract_info][id].contract_id
    send '契約情報検索APIに契約ID :id のリクエストを送信する', contract_id
  end

  step 'send a request with contractID :id to Search API' do |id|
    send '契約情報検索APIに契約ID :id のリクエストを送信する', id
  end

  step '契約情報検索APIに契約ID :id のリクエストを送信する' do |id|
    path = Settings.path.contract.search % id
    @response = send_http_request path, :get
  end

  step 'send a request for :id with HTTP method :method to Search API' do |method, id|
    send '契約情報検索APIに :method メソッドで :id の契約IDを用いてリクエストを送信する', method, id
  end

  step '契約情報検索APIに :method メソッドで :id の契約IDを用いてリクエストを送信する' do |method, id|
    path = Settings.path.contract.search % $data[:contract_info][id].contract_id
    @response = send_http_request path, method
  end

  # ---------------------- POST API --------------------------------------

  step 'send a request based on :filename to POST API' do |id|
    send '契約情報登録APIに :filename の契約情報を用いてリクエストを送信する', filename
  end

  step '契約情報登録APIに :filename の契約情報を用いてリクエストを送信する' do |filename|
    send '契約情報登録APIに :method メソッドで :filename の契約情報を用いてリクエストを送信する', :post, filename
  end

  step 'send a :method request based on :filename to POST API' do |id|
    send '契約情報登録APIに :method メソッドで :filename の契約情報を用いてリクエストを送信する', method, filename
  end

  step '契約情報登録APIに :method メソッドで :filename の契約情報を用いてリクエストを送信する' do |method, filename|
    path = Settings.path.contract.add

    raw_body = fetch_data_file filename
    xpath = 'req-v0:request/bi-v0:productContract/cbe-v0:clientRequestID'
    body = generate_uuid raw_body, xpath

    @response = send_http_request path, method, body
  end

  step 'send a :method request using :filename as the body to POST API' do |id|
    send '契約情報登録APIにボディが :filename のリクエストを送信する', method, filename
  end

  step '契約情報登録APIにボディが :filename のリクエストを送信する' do |filename|
    path = Settings.path.contract.add
    raw_body = fetch_data_file filename
    @response = send_http_request path, :post, raw_body
  end

  # ---------------------- DELETE API -------------------------------------

  step 'send a request for :id to DELETE API' do |id|
    send '契約情報削除APIに :id のリクエストを送信する', id
  end

  step '契約情報削除APIに :id のリクエストを送信する' do |id|
    send '契約情報削除APIにUUID :id のリクエストを送信する', $data[:contract_info][id].uuid
  end

  step 'send a request with UUID :id to DELETE API' do |id|
    send '契約情報削除APIにUUID :id のリクエストを送信する', id
  end

  step '契約情報削除APIにUUID :id のリクエストを送信する' do |id|
    path = Settings.path.contract.delete % id
    @response = send_http_request path, :delete
  end

  step 'send a request for :id with HTTP method :method to DELETE API' do |method, id|
    send '契約情報削除APIに :method メソッドで :id の契約IDを用いてリクエストを送信する', method, id
  end

  step '契約情報削除APIに :method メソッドで :id の契約IDを用いてリクエストを送信する' do |method, id|
    path = Settings.path.contract.delete % $data[:contract_info][id].contract_id
    @response = send_http_request path, method
  end

  # ---------------------- Others -------------------------------------

  step 'create a contract information identified by :id based on :filename' do |id, filename|
    send ':filename を元に契約情報を :id として作成する', filename, id
  end

  step ':filename を元に契約情報を :id として作成する' do |filename, id|
    path = Settings.path.contract.add

    raw_body = fetch_data_file filename
    xpath = 'req-v0:request/bi-v0:productContract/cbe-v0:clientRequestID'
    body = generate_uuid raw_body, xpath

    @response = send_http_request path, :post, body

    unless @response.code == STATUS::CREATED.to_s
      fail "Contract data #{filename} creation failed: " \
           "code=#{@response.code}, body=#{@response.body}"
    end

    uuid = @response['Location'].split('/').last
    $data[:contract_info][id] = ContractInfo.new uuid, @response.body
  end
end
