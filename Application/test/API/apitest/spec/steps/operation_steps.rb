# coding: utf-8
require 'uri'

steps_for :expectation_steps do

  step 'initialize a test environment' do
    send '初期設定を行う'
  end

  step '初期設定を行う' do
    $data[:contract_info] = {}
  end

  step 'create a session ID' do
    send 'セッションIDを発行する'
  end

  step 'セッションIDを発行する' do
    path = Settings.path.session
    params = {}
    if path.start_with? 'http'
      uri = URI.parse path
      params[:ssl] = path.start_with? 'https'
      params[:host] = uri.host
      params[:port] = uri.port
      path = uri.path
    end

    res = send_http_request path, :get, nil, params

    if path == UAT_SESSION_PATH || Settings.path.session.start_with?('http')
      $data[:session_id] = res.body.strip
    else
      r = Regexp.compile '\[id\] => (.+)'
      $data[:session_id] = r.match(res.body)[1]
    end

    fail 'Failed to get session ID' unless $data[:session_id]
  end

  step 'save the response body to :filename' do |filename|
    send 'レスポンスのボディを :filename に保存する', filename
  end

  step 'レスポンスのボディを :filename に保存する' do |filename|
    save_to_file filename, @response.body
  end

  step 'レスポンスのボディを16進数で表示する' do
    send 'write the response body to stdout'
  end

  step 'write the response body to stdout' do
    puts @response.body.unpack('H*').first.scan(/../).join(' ')
  end

  step 'レスポンスのボディを標準出力に表示する' do
    send 'write the response body to stdout'
  end

  step 'write the response body to stdout' do
    puts @response.body
  end

  step ':src_file を :dest_file にコピーする' do |src_file, dest_file|
    send 'copy :src_file to :dest_file', src_file, dest_file
  end

  step 'copy :src_file to :dest_file' do |src_file, dest_file|
    dir = "#{APP_ROOT}/#{Settings.data_dir}"
    FileUtils.cp "#{dir}/#{src_file}", "#{dir}/#{dest_file}"
  end

  step ':file の :xpath のテキストを :text に変更する' do |file, xpath, text|
    send 'replace texts selected by :xpath with :text in :file', xpath, text, file
  end

  step 'replace texts selected by :xpath with :text in :file' do |xpath, text, file|
    embedded_val = embed_parameters text, (@params || {})
    replace_text file, xpath, embedded_val
  end

  step ':file の :xpath のテキストをランダムなUUIDに変更する' do |file, xpath|
    send 'replace texts selected by :xpath with random UUID in :file', xpath, file
  end

  step 'replace texts selected by :xpath with random UUID in :file' do |xpath, file|
    replace_text file, xpath, SecureRandom.uuid
  end

  step ':file の :xpath を削除する' do |file, xpath|
    send 'remove the elements selected by :xpath in :file', xpath, file
  end

  step 'remove the elements selected by :xpath in :file' do |xpath, file|
    remove_elements file, xpath
  end

  step 'HTTPヘッダ :name に :value を設定する' do |name, value|
    send 'set :value to :name header', value, name
  end

  step 'set :value to :name header' do |value, name|
    @header_filter = [] unless @header_filter
    @header_filter << {
      type: :set,
      name: name,
      value: value
    }
  end

  step ':old_name のHTTPヘッダ名の代わりに :name を利用する' do |old_name, name|
    send 'use header name :name instead of :old_name', name, old_name
  end

  step 'use header name :name instead of :old_name' do |name, old_name|
    @header_filter = [] unless @header_filter
    @header_filter << {
      type: :replace,
      from: old_name,
      to: name
    }
  end

  step 'set :xpath in response to :param parameter' do |xpath, param|
    doc = REXML::Document.new @response.body
    els = REXML::XPath.match(doc, xpath)
    raise "Element specified with #{xpath} not found" if els.empty?
    raise "More than one element specified with #{xpath} exist" if els.size > 1
    @params = {} unless @params
    @params[param] = els.first
  end

  # TODO: refactor this step because this step constitutes of dirty code
  step %(wait for ajax) do
    if Settings.version == 7
      # page.driver.browser.switch_to.frame 'bwc-frame'

      # sleep Settings.ajax_wait_time
      # expect(page).to have_css('iframe#bwc-frame')

      # within_frame('bwc-frame') do
      #   expect(page).to have_content('Contract Status')
      # end

      Settings.ajax_wait_num.times do
        begin
          unless has_css? '#bwc-frame'
            sleep 1
            next
          end

          within_frame('bwc-frame') do
            break if has_content? 'Contract Status'
            sleep 1
          end
        rescue
        end
      end
    else
      expect(page).to have_css('h2')
    end
  end

  # TODO: refactor this step because this step constitutes of dirty code
  step 'activate the product contract whose contract id is :contract_id' do |contract_id|
    embedded_val = embed_parameters contract_id, (@params || {})
    if Settings.version > 6
      visit '/#logout/?clear=1'
      expect(page).to have_content('Login with GCM account')

      fill_in "username", with: Settings.username
      fill_in "password", with: Settings.password
      click_link "Log In"
      visit Settings.path.contract_search

      frame_name = 'bwc-frame'

      send "wait for ajax"
      within_frame(frame_name) do
        fill_in "Global Contract ID", with: embedded_val
        click_button "Search"
      end

      send "wait for ajax"
      within_frame(frame_name) do
        find(:xpath, "//table[@class='list view']/tbody/tr[3]/td[4]/b/a").click
      end

      buttons = [{ id: '#sendforapproval', name: 'Send For Approval' },
                 { id: '#yui-gen1-button', name: 'OK' },
                 { id: '#approvecontract', name: 'Approve' },
                 { id: '#yui-gen1-button', name: 'OK' },
                 { id: '#approvecontract', name: 'Approve' },
                 { id: '#yui-gen1-button', name: 'OK' },
                 { id: '#approvecontract', name: 'Approve' },
                 { id: '#yui-gen1-button', name: 'OK' },
                 { id: '#activate_contract', name: 'Activate' },
                 { id: '#sugarMsgWindow', name: 'Yes' }]
      buttons.each do |b|
        Settings.ajax_wait_num.times do
          begin
            unless has_css? '#bwc-frame'
              sleep 1
              next
            end
            within_frame(frame_name) do
              break if has_css? b[:id]
              sleep 1
            end
          rescue
          end
        end
        within_frame(frame_name) do
          click_button b[:name]
        end
      end
    else
      visit Settings.path.contract_search

      fill_in "User Name:", with: Settings.username
      fill_in "Password:", with: Settings.password
      click_button "Log In"
      visit Settings.path.contract_search
      send "wait for ajax"
      fill_in "Global Contract ID", with: embedded_val
      click_button "Search"
      send "wait for ajax"
      find(:xpath, "//table[@class='list view']/tbody/tr[3]/td[4]/b/a").click
      send "wait for ajax"
      click_button "Send For Approval"
      send "wait for ajax"
      click_button "OK"
      send "wait for ajax"
      click_button "Approve"
      send "wait for ajax"
      click_button "OK"
      send "wait for ajax"
      click_button "Approve"
      send "wait for ajax"
      click_button "OK"
      send "wait for ajax"
      click_button "Approve"
      send "wait for ajax"
      click_button "OK"
      send "wait for ajax"
      click_button "Activate"
      send "wait for ajax"
      click_button "Yes"
      send "wait for ajax"
    end
  end

  step 'set the :attr attribute of :xpath to :val in :file' do |attr, xpath, val, file|
    embedded_val = embed_parameters val, (@params || {})
    set_attributes file, xpath, { attr => embedded_val }
  end

  step 'replace all UUIDs in :file according to the GET response' do |file|
    res = @response.body
    raise "Response body does not exist" unless res
    replace_all_uuids file, res
  end
end
