# -*- coding: utf-8 -*-
#
# page オブジェクトに関するステップ

step %(:text と表示されている) do |text|
  # expect(page).to have_content(text)
  expect(page).to have_content(text)
end

step %(正規表現で :regexp とマッチする部分がある) do |regexp|
  expect(page).to have_content(Regexp.new(regexp))
end

step %(正規表現で :regexp とマッチする部分がない) do |regexp|
  expect(page).not_to have_content(Regexp.new(regexp))
end

step '正規表現で :regexp とマッチする箇所が :num 箇所ある' do |regexp, num|
  expect(page).to have_content(Regexp.new(regexp), count: num.to_i)
end

step %(:path のページが表示されている) do |path|
  expect(page.current_path).to eq(path)
end

step ':label リンクが表示されている' do |label|
  expect(page).to have_link(label)
end

step ':label リンクが表示されていない' do |label|
  expect(page).not_to have_link(label)
end

step ':label ボタンが表示されている' do |label|
  expect(page).to have_button(label)
end

step ':label ボタンが表示されていない' do |label|
  expect(page).not_to have_button(label)
end

step ':label がチェックされている' do |label|
  expect(page).to have_checked_field(label)
end

step ':label がチェックされていない' do |label|
  expect(page).to have_unchecked_field(label)
end

step %(:text が :cnt 個表示されている) do |text, cnt|
  expect(page).to have_content(text, count: cnt)
end

step %(:text と表示されていない) do |text|
  expect(page).not_to have_content(text)
end

step %(:locator のフィールドが表示されている) do |locator|
  expect(page).to have_field(locator)
end

step %(:locator のフィールドが表示されていない) do |locator|
  expect(page).to have_no_field(locator)
end

step %(アクセス表示権限エラーが出ていない) do
  expect(page).not_to have_content('リンク先のページを表示する権限がありません')
end

step %(アクセス表示権限エラーが出ている) do
  expect(page).to have_content('リンク先のページを表示する権限がありません')
end

step %(CSVファイルが出力される) do
  expect(page.response_headers['Content-Type']).to eq 'text/csv'
  expect(CSV.parse(page.body)).to be_a(Array)
end

step %(ファイル :filename がダウンロードされる) do |filename|
  expect(page.response_headers['Content-Disposition']).to do
    include(%(filename="#{filename}"))
  end
end

step %[セレクトボックス :selectbox に :items がある(区切り文字 :delim)] do |selectbox,
                                                                   items, delim|
  expect(page).to have_select(selectbox, with_options: \
    items.split(delim.present? ? delim : ' '))
end

step %[セレクトボックス :selectbox に :items がない(区切り文字 :delim)] do |selectbox,
                                                                  items, delim|
  expect(page).not_to have_select(selectbox, with_options: \
    items.split(delim.present? ? delim : ' '))
end

step %(:selector 内に :text と表示されている) do |selector, text|
  within(selector) do
    expect(page).to have_content text
  end
end

step %(:selector 内に :text と表示されていない) do |selector, text|
  within(selector) do
    expect(page).to have_no_content text
  end
end

step %(XPATH :xpath 内に :text と表示されている) do |xpath, text|
  within(:xpath, xpath) do
    expect(page).to have_content text
  end
end

step %(:cont_selector 内に :tgt_selector が存在する) do |cont_selector, tgt_selector|
  within(cont_selector) do
    expect(page).to have_css tgt_selector
  end
end

step %(:cont_selector 内に :tgt_selector が存在しない) do |cont_selector, tgt_selector|
  within(cont_selector) do
    expect(page).to have_no_css tgt_selector
  end
end

# 2016/02/24杉本追加
step %(:selector の :attr_name 属性値に :text が含まれている) do |selector, attr_name, text|
  obj = page.find(selector.to_s)
  expect(obj[attr_name.to_sym]).to include text
end
step %(:selector の :attr_name 属性値に :text が含まれていない) do |selector, attr_name, text|
  obj = page.find(selector.to_s)
  expect(obj[attr_name.to_sym]).not_to include text
end

step ':fragment のフラグメントが表示されている' do |fragment|
  f = URI.parse(current_url).fragment
  expect(f).to eq(fragment)
end

step ':field のフィールドは :value の値を持つ' do |field, value|
  v = find_field(field).value
  expect(v).to eq(value)
end

step ':field のセレクトフィールドは :text が選択されている' do |field, text|
  v = find_field(field).find('option[selected]').text
  expect(v).to eq(text)
end

step ':field のセレクトフィールドは選択されていない' do |field|
  field = find_field(field)
  expect(field).to have_no_field('option[selected]')
end

step %(確認ダイアログに :text と表示されている) do |text|
  msg = page.driver.browser.switch_to.alert.text
  # expect(text).to eq(msg)
  # 失敗時のexpectedとgotが逆だったので修正(2016/02/08杉本)
  expect(msg).to eq(text)
end

# 指定された選択項目が選択されているかを確認する
# Label と Value のカラムを持つ table を引数に取ります。
# Label は セレクトフィールドの locator を、
# Value は選択されている文字列を指定します。
step '選択項目に設定されている' do |table|
  table.hashes.each do |columns|
    step "'#{columns['Label']}' のセレクトフィールドは '#{columns['Value']}' が選択されている"
  end
end

# 指定されたチェックボックスがチェックされているかを
# まとめて調べる
# items にはスペース区切りで locator を記載する
step ':items が全てチェックされている' do |items|
  items.split(' ').each do |item|
    step "'#{item}' がチェックされている"
  end
end

# 指定されたチェックボックスがチェックされていないかを
# まとめて調べる
# items にはスペース区切りで locator を記載する
step ':items が全てチェックされていない' do |items|
  items.split(' ').each do |item|
    step "'#{item}' がチェックされていない"
  end
end

step ':selector の日時が現在時刻の :seconds 秒の誤差内にある' do |selector, seconds|
  within(selector) do
    actual = DateTime.parse page.text
    value = DateTime.now
    expect(actual).to be_within(seconds.to_i).of(value)
  end
end

step 'disableでない :button ボタンが表示されている' do |button|
  expect(page).to have_button(button, disabled: false)
end

step 'disableである :button ボタンが表示されている' do |button|
  expect(page).to have_button(button, disabled: true)
end

step 'disableでない :link リンクが表示されている' do |link|
  disabled = find_link(link)[:disabled]
  expect(disabled).to be_nil
end

step 'disableである :link リンクが表示されている' do |link|
  disabled = find_link(link)[:disabled]
  expect(disabled).to eq('true')
end

step %(Windowタイトルに :text と表示されている) do |text|
  expect(page).to have_title(text)
end

step %(パンくずにactive状態（リンクなし）で :text と表示されている) do |text|
  # activeクラスのものは１つのはず、という想定でwithinを使用。
  within('div#breadcrumb>ol.breadcrumb li.active') do
    expect(page).to have_content text
  end
end

step %(パンくずの :no 番目にリンク付きで :text と表示されている) do |no, text|
  # 複数該当要素がある場合にどこかでtrueがあれば、というのが複雑になるので何番目か指定。
  # ちなみにwithinだと複数該当要素がある場合エラーになる。
  expect(page.all('div#breadcrumb>ol.breadcrumb>li')[no.to_i - 1].find('a'))\
    .to have_content text
end

# ペンディング用。Rspec３からpendingメソッドが変更になったためskipメソッドを使用。
step %(PENDING=>理由： :reason) do |reason|
  # skip "PENDING=>理由： #{reason}"
  skip "skiped"
end

step %(PENDING： :step =>理由：:reason) do |step, reason|
  skip "skiped #{step}"
end
