# -*- coding: utf-8 -*-

step %(click :text link) do |text|
  click_link text
end

step %(click :text button) do |text|
  click_button text
end

step %(set :value to :field field) do |value, field|
  fill_in field, with: value
end

step %(select :value of :option) do |value, option|
  select value, from: option
end

step %(choose :choice) do |choice|
  choose choice
end

step %(check :choice) do |choice|
  check choice
end

step %(uncheck :choice) do |choice|
  uncheck choice
end

step %(open :path) do |path|
  visit path
end

step 'save and open the current page' do
  save_and_open_page
end

step %(click OK button in the confirmation dialog) do
  page.driver.browser.switch_to.alert.accept
end

step %(click cancel button in the confirmation dialog) do
  page.driver.browser.switch_to.alert.dismiss
end

step %(wait for :seconds secs) do |seconds|
    sleep(seconds.to_f)
end

step %(close the window) do
  page.execute_script 'window.close();'
end
