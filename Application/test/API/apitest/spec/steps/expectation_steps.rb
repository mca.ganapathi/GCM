require 'net/https'
require 'lorax'
require 'rexml/document'
require 'equivalent-xml'

require_relative '../lib/net/generic_request'
require_relative '../lib/lorax/delta'
require_relative '../lib/lorax/delta_set'
require_relative '../lib/sorter'

include StepUtils

steps_for :expectation_steps do
  step 'the status code of the HTTP response is :code' do |code|
    send 'レスポンスのステータスコードが :code である', code
  end

  step 'レスポンスのステータスコードが :code である' do |code|
    expect(@response.code).to eq code
  end

  step 'the response body corresponds with the content of :filename' do |filename|
    send 'レスポンスのボディが :filename と一致する', filename
  end

  step 'レスポンスのボディが :filename と一致する' do |filename|
    send 'レスポンスのボディが :filename と一致する。ただし、次のXPATHで指定するノードは除く', filename, nil
  end

  step 'the response body corresponds with the content of :filename excluding the XML elements selected by the following XPaths' do |filename, table|
    send 'レスポンスのボディが :filename と一致する。ただし、次のXPATHで指定するノードは除く', filename, table
  end

  step 'レスポンスのボディが :filename と一致する。ただし、次のXPATHで指定するノードは除く' do |filename, table|
    doc1 = strip_each_lines fetch_data_file(filename)
    doc2 = strip_each_lines @response.body

    if doc1.empty?
      expect(doc2).to be_empty
      return
    end

    if @request.method.to_s.downcase == 'get' && !@request.path.start_with?('/api/v1')
      sorter = XMLSorter.new
      doc1 = sorter.sort(REXML::Document.new(doc1)).to_s
      doc2 = sorter.sort(REXML::Document.new(doc2)).to_s
      # File.write("doc1-o.xml", doc1)
      # File.write("doc2-o.xml", doc2)
    end

    # TODO: refactor
    paths = table ? table.rows.map(&:first) : []
    paths = [] unless @response.code.first == '2'
    paths << '/*/@xsi:schemaLocation'
    xml1 = Nokogiri::XML doc1
    xml1.xpath(paths.join('|')).each do |node|
      if node.is_a? Nokogiri::XML::Attr
        node.value = 'REPLACED'
      else
        node.inner_html = 'REPLACED'
      end
    end
    xml2 = Nokogiri::XML doc2
    xml2.xpath(paths.join('|')).each do |node|
      if node.is_a? Nokogiri::XML::Attr
        node.value = 'REPLACED'
      else
        node.inner_html = 'REPLACED'
      end
    end
    xml1.xpath('//comment()').remove
    xml2.xpath('//comment()').remove
    doc1 = xml1; doc2 = xml2

    # pretty_formatter = REXML::Formatters::Pretty.new
    # output = StringIO.new
    # pretty_formatter.write(REXML::Document.new(doc1.to_s), output)
    # File.write("doc1.xml", output.string)
    #
    # pretty_formatter = REXML::Formatters::Pretty.new
    # output = StringIO.new
    # pretty_formatter.write(REXML::Document.new(doc2.to_s), output)
    # File.write("doc2.xml", output.string)

    # resu = EquivalentXml.equivalent?(doc1, doc2, :element_order => true) do |n1, n2, r|
    #   puts [n1.path, n2.path, r].inspect unless r
    # end
    # puts resu

    delta_set = Lorax.diff(doc1, doc2)
    norm_paths = normalize_xpaths doc1, paths
    deltas = delta_set.deltas.reject do |d|
      desc = d.descriptor

      xpath, content = case desc.first
                       when :modify
                         [desc.last[:new][:xpath], desc.last[:new][:content]]
                       when :insert
                         [desc.last[:xpath], desc.last[:content]]
                       when :delete
                         [desc.last[:xpath], desc.last[:content]]
                       end

      norm_paths.any? do |norm_path|
        if content
          m = content.match '^<([^>]+)>.*</[^<]+>$'
          np = m.nil? ? norm_path : (xpath + '/' + m[1])
          xpath.start_with?(np)
        else
          false
        end
      end
    end.map(&:descriptor)

    if deltas.empty?
      msg = 'No difference between expected XML and actual XML'
    else
      @diff = JSON.pretty_generate deltas
      msg = "Differences found between expected XML and actual XML: \n" + @diff
    end
    AutoTest.logger.info msg

    expect(deltas).to be_empty
  end

  step 'the text content selected by XPath :xpath matches :string' do |xpath, string|
    send 'XPATH :xpath の文字列が :string と一致する', xpath, string
  end

  step 'XPATH :xpath の文字列が :string と一致する' do |xpath, string|
    doc = REXML::Document.new @response.body
    els = REXML::XPath.match(doc, xpath)
    raise "Element specified with #{xpath} not found" if els.empty?
    els.each { |e| expect(e).to eq string }
  end

  step 'the text content selected by XPath :xpath matches regex :regex' do |xpath, regex|
    send 'XPATH :xpath の文字列が正規表現 :regex と一致する', xpath, regex
  end

  step 'XPATH :xpath の文字列が正規表現 :regex と一致する' do |xpath, regex|
    doc = REXML::Document.new @response.body
    els = REXML::XPath.match(doc, xpath)
    raise "Element specified with #{xpath} not found" if els.empty?
    els.each do |el|
      expect(el.to_s).to match Regexp.new(regex)
    end
  end

  step 'the text content selected by XPath :xpath is UUID' do |xpath|
    send 'XPATH :xpath の文字列がUUIDである', xpath
  end

  step 'XPATH :xpath の文字列がUUIDである' do |xpath|
    x = '[a-z0-9]'
    regex = "^#{x}{8}-#{x}{4}-#{x}{4}-#{x}{4}-#{x}{12}"
    send 'XPATH :xpath の文字列が正規表現 :regex と一致する', xpath, regex
  end

  step ':xpath1 で指定される要素に対して :xpath2 で指定される要素が存在する' do |xpath1, xpath2|
    send 'each element specified with :xpath has any elements specified with :xpath2', xpath1, xpath2
  end

  step 'each element specified with :xpath has any elements specified with :xpath2' do |xpath1, xpath2|
    doc = REXML::Document.new @response.body
    els = REXML::XPath.match(doc, xpath1)
    els.each do |el|
      expect(el.get_elements(xpath2).size).not_to be 0
    end
  end

  step 'XPath :xpath で指定される要素が存在しない' do |xpath|
    send 'there is no element specified with :xpath', xpath
  end

  step 'there is no element specified with :xpath' do |xpath|
    doc = REXML::Document.new @response.body
    els = REXML::XPath.match(doc, xpath)
    expect(els.size).to be 0
  end

  step 'XPath :xpath で指定される要素が存在する' do |xpath|
    send 'there are elements specified with :xpath', xpath
  end

  step 'there are elements specified with :xpath' do |xpath|
    doc = REXML::Document.new @response.body
    els = REXML::XPath.match(doc, xpath)
    expect(els.size).to be > 0
  end

  step 'ボディの改行コードがLFである' do
    @response.body.each_line do |line|
      expect(line.last).not_to eq "\r"
    end
  end

  step 'response has no body' do
    expect(@response.body).to be_nil
  end

  step 'レスポンスボディが空である' do
    send 'response has no body'
  end

  step 'header :name matches :regex' do |name, regex|
    expect(@response[name]).to match Regexp.new(regex)
  end
end
