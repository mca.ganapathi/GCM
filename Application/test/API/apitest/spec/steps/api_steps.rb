steps_for :api_steps do
  step "send a :method request to :path" do |method, path|
    embedded_path = embed_parameters path, (@params || {})
    @response = send_http_request embedded_path, method
  end

  step "send a :method request to :path with :filename" do |method, path, filename|
    body = fetch_data_file filename
    embedded_path = embed_parameters path, (@params || {})
    @response = send_http_request embedded_path, method, body
  end
end
