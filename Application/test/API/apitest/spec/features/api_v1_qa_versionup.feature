
# encoding: utf-8
# language: en

@expectation_steps
@operation_steps
@api_steps
Feature: APIv2
Background:
Given initialize a test environment
And create a session ID

  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_001 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_001/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_001/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_001.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_001-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_001/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                      
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_003 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_003/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_003/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_004 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_004/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_004/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_006 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_006/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_006/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_007 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_007/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_007/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_007.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_007-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_007/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_008 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_008/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_008/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_008.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_008-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_008/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
	| XPATH |                                                                                                                     
	| //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_009 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_009/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_009/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_009.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_009-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_009/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                      
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_010 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_010/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_010/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_010.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_010-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_010/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH  |                                                                                                                    
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_011 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_011/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_011/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_011.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_011-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_011/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_013 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_013/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_013/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	And set the "id" attribute of "//product:atomic-product[1]" to "" in "tmp/ver-up-sample.xml"
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_015 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_015/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_015/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_016 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_016/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_016/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_017 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_017/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_017/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_017.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_017-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_017/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_018 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_018/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_018/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_018.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_018-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_018/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_020 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_020/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_020/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_021 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_021/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_021/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_022 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_022/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_022/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_023 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_023/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_023/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_024 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_024/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_024/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_024.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_024-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_024/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_027 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_027/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_027/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_028 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_028/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_028/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3009"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3011"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[3]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[3]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[4]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[4]/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_029 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_029/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_029/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3020"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_030 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_030/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_030/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_031 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_031/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_031/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_032 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_032/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_032/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_033 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_033/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_033/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_034 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_034/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_034/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_035 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_035/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_035/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_036 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_036/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_036/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_038 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_038/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_038/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_038.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_038-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_038/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_039 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_039/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_039/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_039.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_039-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_039/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_040 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_040/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_040/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_040.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_040-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_040/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |

  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_041 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_041/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_041/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_041.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_041-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_041/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
   
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_043 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_043/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_043/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_043.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_043-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_043/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_044 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_044/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_044/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_044.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_044-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_044/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_045 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_045/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_045/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_045.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_045-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_045/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_046 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_046/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_046/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_047 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_047/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_047/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "500"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3005"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_048 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_048/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_048/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_049 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_049/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_049/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_050 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_050/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_050/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_051 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_051/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_051/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_052 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_052/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_052/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_052.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_052-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_052/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_053 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_053/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_053/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_053.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_053-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_053/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_054 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_054/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_054/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_054.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_054-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_054/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_055 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_055/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_055/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_055.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_055-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_055/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_056 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_056/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_056/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_056.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_056-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_056/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_057 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_057/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_057/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_058 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_058/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_058/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_059 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_059/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_059/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_060 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_060/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_060/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_061 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_061/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_061/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_061.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_061-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_061/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_062 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_062/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_062/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_062.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_062-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_062/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_063 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_063/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_063/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_063.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_063-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_063/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_064 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_064/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_064/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_065 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_065/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_065/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_065.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_065-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_065/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_066 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_066/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_066/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_067 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_067/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_067/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_068 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_068/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_068/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_068.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_068-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_068/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_069 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_069/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_069/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_069.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_069-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_069/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_070 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_070/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_070/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_070.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_070-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_070/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_071 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_071/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_071/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_071.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_071-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_071/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_072 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_072/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_072/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_072.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_072-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_072/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_073 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_073/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_073/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_073.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_073-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_073/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_074 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_074/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_074/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_075 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_075/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_075/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_076 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_076/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_076/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_077 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_077/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_077/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_077.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_077-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_077/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_078 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_078/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_078/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_078.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_078-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_078/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_079 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_079/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_079/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_079.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_079-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_079/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_080 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_080/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_080/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_080.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_080-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_080/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_081 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_081/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_081/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_082 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_082/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_082/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_083 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_083/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_083/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_084 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_084/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_084/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_084.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_084-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_084/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_085 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_085/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_085/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_085.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_085-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_085/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_086 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_086/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_086/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_086.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_086-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_086/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_087 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_087/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_087/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_087.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_087-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_087/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_088 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_088/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_088/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_088.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_088-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_088/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_089 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_089/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_089/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_089.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_089-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_089/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |

  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_091 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_091/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_091/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_092 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_092/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_092/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_093 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_093/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_093/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_094 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_094/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_094/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_094.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_094-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_094/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_095 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_095/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_095/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_095.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_095-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_095/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_096 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_096/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_096/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_097 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_097/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_097/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"

  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_099 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_099/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_099/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_100 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_100/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_100/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_101 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_101/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_101/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_102 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_102/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_102/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_103 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_103/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_103/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_104 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_104/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_104/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_104.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_104-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_104/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_106 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_106/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_106/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"

Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_107 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_107/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_107/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"

Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_108 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_108/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_108/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_109 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_109/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_109/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_109.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_109-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_109/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_111 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_111/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_111/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_112 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_112/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_112/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_113 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_113/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_113/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_114 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_114/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_114/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_114.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_114-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_114/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_116 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_116/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_116/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_117 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_117/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_117/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_118 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_118/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_118/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_119 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_119/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_119/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_120 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_120/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_120/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_120.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_120-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_120/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_122 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_122/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_122/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_123 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_123/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_123/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_124 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_124/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_124/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_125 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_125/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_125/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_126 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_126/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_126/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_126.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_126-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_126/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |	
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_128 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_128/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_128/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_129 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_129/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_129/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_130 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_130/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_130/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_131 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_131/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_131/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_131.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_131-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_131/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_133 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_133/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_133/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_134 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_134/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_134/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_135 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_135/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_135/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_136 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_136/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_136/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_136.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_136-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_136/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |	

  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_138 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_138/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_138/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_139 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_139/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_139/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"	

  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_140 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_140/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_140/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"	
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_141 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_141/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_141/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"	
 
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_142 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_142/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_142/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"	
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_143 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_143/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_143/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_143.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_143-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_143/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |		
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_145 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_145/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_145/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_146 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_146/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_146/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_147 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_147/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_147/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_147.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_147-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_147/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_149 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_149/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_149/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()" 
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_150 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_150/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_150/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_151 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_151/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_151/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_151.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_151-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_151/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_153 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_153/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_153/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_154 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_154/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    	And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_154/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	And replace texts selected by "//cbe:characteristic-value[59]/descendant::cbe:object-id[1]" with "a97da205-0b23-4fbe-b583-918d9220cc62" in "tmp/ver-up-sample.xml"
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()" 	
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_155 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_155/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_155/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"

  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_156 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_156/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_156/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_156.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_156-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_156/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_158 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_158/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_158/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()" 
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_159 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_159/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_159/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
And replace texts selected by "//cbe:characteristic-value[60]/descendant::cbe:object-id[1]" with "a97da205-0b23-4fbe-b583-918d9220cc62" in "tmp/ver-up-sample.xml"
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_160 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_160/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_160/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_161 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_161/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_161/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_161.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_161-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_161/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |	
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_163 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_163/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_163/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"   	
  
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_164 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_164/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_164/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
And replace texts selected by "//cbe:characteristic-value[61]/descendant::cbe:object-id[1]" with "a97da205-0b23-4fbe-b583-918d9220cc62" in "tmp/ver-up-sample.xml"
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_165 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_165/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_165/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"  	
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_166 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_166/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_166/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_166.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_166-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_166/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_168 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_168/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_168/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"  	  
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_169 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_169/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_169/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
And replace texts selected by "//cbe:characteristic-value[62]/descendant::cbe:object-id[1]" with "a97da205-0b23-4fbe-b583-918d9220cc62" in "tmp/ver-up-sample.xml"
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"  
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_170 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_170/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_170/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"   
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_171 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_171/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_171/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_171.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_171-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_171/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |	

  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_177 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_177/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_177/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_177.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_177-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_177/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_179 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_179/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_179/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
And replace texts selected by "//cbe:characteristic-value[63]/descendant::cbe:object-id[1]" with "a97da205-0b23-4fbe-b583-918d9220cc62" in "tmp/ver-up-sample.xml"
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()" 
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_180 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_180/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_180/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()" 
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_181 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_181/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_181/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_181.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_181-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_181/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_183 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_183/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_183/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"  
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_184 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_184/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_184/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"  
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_185 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_185/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_185/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_185.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_185-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_185/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_186 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_186/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_186/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"  
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_188 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_188/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_188/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()" 
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_189 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_189/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_189/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_190 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_190/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_190/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_190.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_190-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_190/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_192 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_192/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_192/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_193 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_193/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_193/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_193.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_193-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_193/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user | 
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_194 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_194/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_194/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_196 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_196/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_196/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"	
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_197 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_197/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_197/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"	
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_199 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_199/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_199/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_199.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_199-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_199/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_201 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_201/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_201/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_202 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_202/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_202/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_203 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_203/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_203/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_204 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_204/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_204/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_205 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_205/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_205/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_205.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_205-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_205/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_206 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_206/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_206/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_206.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_206-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_206/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_209 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_209/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_209/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_210 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_210/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_210/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_211 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_211/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_211/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_212 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_212/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_212/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_212.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_212-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_212/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_213 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_213/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_213/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_213.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_213-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_213/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_216 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_216/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_216/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_217 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_217/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_217/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_218 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_218/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_218/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_219 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_219/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_219/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_219.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_219-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_219/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_220 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_220/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_220/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_220.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_220-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_220/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_223 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_223/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_223/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_224 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_224/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_224/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_225 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_225/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_225/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_226 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_226/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_226/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_226.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_226-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_226/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_227 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_227/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_227/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_227.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_227-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_227/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_230 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_230/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_230/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_231 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_231/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_231/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_232 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_232/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_232/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_233 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_233/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_233/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_233.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_233-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_233/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_234 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_234/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_234/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_234.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_234-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_234/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_237 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_237/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_237/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_238 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_238/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_238/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_239 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_239/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_239/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_240 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_240/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_240/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_240.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_240-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_240/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_243 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_243/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_243/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_244 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_244/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_244/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_245 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_245/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_245/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "201"
	And save the response body to "actual-response/TC_DF-370_PREQ_245.xml"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}"
	And save the response body to "actual-response/TC_DF-370_PREQ_245-get.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_245/GET_VER_2_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                     
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_248 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_248/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_248/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_POST_versionup_request/TC_DF-370_PREQ_249 POST
  	Given copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_249/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_POST_versionup_request/TC_DF-370_PREQ_249/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
