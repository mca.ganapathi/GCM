# encoding: utf-8
# language: en

@expectation_steps
@operation_steps
@api_steps
Feature: APIv2
  Background:
    Given initialize a test environment
    And create a session ID

  # AC: IF
  Scenario: [IF-1..3]
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    Then the status code of the HTTP response is "201"
    # And header "Location" matches "^https\:\/\/.+/api/v2/contract-information/.*$"
    And save the response body to "actual-response/IF-1-3.xml"
    And the response body corresponds with the content of "v2_uat/gmone-contract-info-test1-post-response.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: [IF-4..5]
    Given copy "v2_uat/IF-4-5.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And save the response body to "actual-response/IF-4-5.xml"
    Then the status code of the HTTP response is "400"
    And the text content selected by XPath "/exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
    And there are elements specified with "/exp:exceptions/exp:exception/exp:error-message/text()"

  Scenario: [IF-6..7]
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And save the response body to "actual-response/IF-6-7.xml"
    # And save the response body to "v2_uat/gmone-contract-info-test1-get-response0b.xml"
    And the response body corresponds with the content of "v2_uat/gmone-contract-info-test1-get-response0b.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |

  Scenario: [IF-8..9]
    When send a "GET" request to "/api/v2/contract-information/c19b764b-561d-4ba3-8a9e-6410e8dba30d:1"
    Then the status code of the HTTP response is "404"
    # And write the response body to stdout
    And save the response body to "actual-response/IF-8-9.xml"
    And the text content selected by XPath "/exp:exceptions/exp:exception/exp:error-code/text()" matches "6001"
    And there are elements specified with "/exp:exceptions/exp:exception/exp:error-message/text()"

  Scenario: [IF-10..11]
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And send a "GET" request to "/api/v2/contract-information/@{UUID}"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And save the response body to "actual-response/IF-10-11.xml"
    And the response body corresponds with the content of "v2_uat/gmone-contract-info-test1-get-response0b.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |

  Scenario: [IF-12..13]
    When send a "GET" request to "/api/v2/contract-information/c19b764b-561d-4ba3-8a9e-6410e8dba30d"
    Then the status code of the HTTP response is "404"
    # And write the response body to stdout
    And save the response body to "actual-response/IF-12-13.xml"
    And the text content selected by XPath "/exp:exceptions/exp:exception/exp:error-code/text()" matches "6001"
    And there are elements specified with "/exp:exceptions/exp:exception/exp:error-message/text()"

  Scenario: [IF-14..15]
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And set "//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    When send a "GET" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}&cbe:object-version=1"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And save the response body to "actual-response/IF-14-15.xml"
    And the response body corresponds with the content of "v2_uat/gmone-contract-info-test1-get-response0b.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |

  Scenario: [IF-16..17]
    When send a "GET" request to "/api/v2/contract-information/search?bi:contract-id=111111&cbe:object-version=1"
    # And write the response body to stdout
    Then the status code of the HTTP response is "404"
    And save the response body to "actual-response/IF-16-17.xml"
    And the text content selected by XPath "/exp:exceptions/exp:exception/exp:error-code/text()" matches "6001"
    And there are elements specified with "/exp:exceptions/exp:exception/exp:error-message/text()"

  Scenario: [IF-18..19]
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And set "//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    When send a "GET" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}&cbe:object-version=2"
    # And write the response body to stdout
    Then the status code of the HTTP response is "404"
    And save the response body to "actual-response/IF-18-19.xml"
    And the text content selected by XPath "/exp:exceptions/exp:exception/exp:error-code/text()" matches "6001"
    And there are elements specified with "/exp:exceptions/exp:exception/exp:error-message/text()"

  Scenario: [IF-20..21]
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And set "//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    When send a "GET" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And save the response body to "actual-response/IF-20-21.xml"
    And the response body corresponds with the content of "v2_uat/gmone-contract-info-test1-get-response0b.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |

  Scenario: [IF-22..23]
    When send a "GET" request to "/api/v2/contract-information/search?bi:contract-id=1111"
    # And write the response body to stdout
    Then the status code of the HTTP response is "404"
    And save the response body to "actual-response/IF-22-23.xml"
    And the text content selected by XPath "/exp:exceptions/exp:exception/exp:error-code/text()" matches "6001"
    And there are elements specified with "/exp:exceptions/exp:exception/exp:error-message/text()"

  Scenario: [IF-24..25]
    When send a "GET" request to "/api/v2/contract-information/search"
    # And write the response body to stdout
    Then the status code of the HTTP response is "400"
    And save the response body to "actual-response/IF-24-25.xml"
    And the text content selected by XPath "/exp:exceptions/exp:exception/exp:error-code/text()" matches "3002"
    And there are elements specified with "/exp:exceptions/exp:exception/exp:error-message/text()"

  Scenario: [IF-26..27]
    Given pending
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    When send a "GET" request to "/api/v2/contract-information/@{UUID}/search?bi:interaction-status=being-created"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And save the response body to "actual-response/IF-26-27.xml"
    And the response body corresponds with the content of "v2_uat/gmone-contract-info-test1-get-response0b.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |

  Scenario: [IF-30..31]
    When send a "GET" request to "/api/v2/contract-information/c19b764b-561d-4ba3-8a9e-6410e8dba30d/search?bi:interaction-status=being-created"
    # And write the response body to stdout
    Then the status code of the HTTP response is "404"
    And save the response body to "actual-response/IF-30-31.xml"
    And the text content selected by XPath "/exp:exceptions/exp:exception/exp:error-code/text()" matches "6001"
    And there are elements specified with "/exp:exceptions/exp:exception/exp:error-message/text()"

  Scenario: [IF-32..33]
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    When send a "GET" request to "/api/v2/contract-information/@{UUID}/search?bi:interaction-status=aborted"
    # And write the response body to stdout
    Then the status code of the HTTP response is "404"
    And save the response body to "actual-response/IF-32-33.xml"
    And the text content selected by XPath "/exp:exceptions/exp:exception/exp:error-code/text()" matches "6001"
    And there are elements specified with "/exp:exceptions/exp:exception/exp:error-message/text()"

  Scenario: [IF-34..35]
    Given copy "v2_uat/gmone-contract-info-v1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    And save the response body to "tmp/ver-up-get.xml"
    And copy "v2_uat/ver-up-request-action-test.xml" to "tmp/ver-up-sample.xml"
    And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
    And replace texts selected by "//bi:product-contract/cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
    And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
    When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
    Then the status code of the HTTP response is "201"
    And save the response body to "actual-response/IF-34-35.xml"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}"
    And save the response body to "actual-response/IF-34-35-get.xml"
    And the response body corresponds with the content of "v2_uat/expected-ver-up-get-res.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |

  Scenario: [IF-36..37]
    Given copy "v2_uat/gmone-contract-info-v1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    And save the response body to "tmp/ver-up-get.xml"
    And copy "v2_uat/ver-up-request-invalid.xml" to "tmp/ver-up-sample.xml"
    And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
    When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
    Then the status code of the HTTP response is "400"
    And the text content selected by XPath "/exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
    And there are elements specified with "/exp:exceptions/exp:exception/exp:error-message/text()"

  Scenario: [IF-38..39]
    Given copy "v2_uat/gmone-contract-info-v1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    And save the response body to "tmp/ver-up-get.xml"
    And copy "v2_uat/ver-up-request-action-test.xml" to "tmp/ver-up-sample.xml"
    And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
    And replace texts selected by "//bi:product-contract/cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
    And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
    When send a "POST" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/ver-up-sample.xml"
    # And write the response body to stdout
    Then the status code of the HTTP response is "201"
    And save the response body to "actual-response/IF-38-39.xml"

  Scenario: [IF-40..41] OK
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And copy "v2_uat/patch-request.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
    Then the status code of the HTTP response is "200"

  Scenario: [IF-42..43] OK
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And save the response body to "actual-response/test-post.xml"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/test-get.xml"
    And copy "v2_uat/patch-request.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And save the response body to "actual-response/get-res-after-patch.xml"
    And the text content selected by XPath "//gcm:contract-customer/gcm:person-in-charge/gcm:individual/gcm:name/text()" matches "Tatsuya Ogawa"
    And the response body corresponds with the content of "v2_uat/expected-patch-res.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |

  Scenario: [IF-45..46] OK
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And copy "v2_uat/patch-request-invalid.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
    Then the status code of the HTTP response is "400"
    And the text content selected by XPath "/exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
    And there are elements specified with "/exp:exceptions/exp:exception/exp:error-message/text()"

  # AC: Product

  Scenario: [Product-1] Can client systems create a GMOne UNO option contract by API v2?
    Given copy "v2_uat/post-request-UNO-Option.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    Then the status code of the HTTP response is "201"
    And save the response body to "actual-response/P-1-post-response.xml"
    And the response body corresponds with the content of "v2_uat/P-1-post-response.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //bi:product-contract/bi:contract-id |
    | //@id |

  Scenario: [Product-2] Can client systems get a GMOne UNO option contract by API v2?
    Given copy "v2_uat/post-request-UNO-Full-Option.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And send a "GET" request to "/api/v2/contract-information/@{UUID}"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And save the response body to "actual-response/P-2-get-response.xml"
    # And save the response body to "v2_uat/P-2-get-response.xml"
    And the response body corresponds with the content of "v2_uat/P-2-get-response.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |

  Scenario: [Product-3] Can client systems upgrade a GMOne UNO option contract by API v2?
    # covered by [IF-34..35]

  Scenario: [Product-4] Can client systems terminate a GMOne UNO option contract by API v2?
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And save the response body to "actual-response/test-post.xml"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/test-get.xml"
    And copy "v2_uat/patch-request-terminate.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And save the response body to "actual-response/get-res-after-patch.xml"
    And the text content selected by XPath "//bi:product-contract//cbe:standard-business-state/text()" matches "Sent for Termination Approval"

  Scenario: [Product-4-2] Can client systems terminate a non-workflow contract by API v2?
    Given copy "v2_uat/non-workflow-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And save the response body to "actual-response/test-post.xml"
    And copy "v2_uat/patch-request-terminate-non-workflow.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And save the response body to "actual-response/get-res-after-patch.xml"
    And the text content selected by XPath "//bi:product-contract//cbe:standard-business-state/text()" matches "Deactivated"

  Scenario: [Product-5] Can client systems create a GMOne ECL2.0 option contract by API v2?
    # covered by [IF-1..3]

  Scenario: [Product-6] Can client systems get a GMOne ECL2.0 option contract by API v2?
    # covered by [IF-6..7]

  Scenario: [biitem-1] Can client systems add a line item by API v2?
    # covered by [IF-34..35]

  Scenario: [biitem-2] Can client systems change a line item by API v2?
    # covered by [IF-34..35]

  Scenario: [biitem-3] Can client systems not change a line item by API v2?
    # covered by [IF-34..35]

  Scenario: [biitem-4] Can client systems remove a line item by API v2?
    # covered by [IF-34..35]

  Scenario: [contract-customer-1] Can client systems update the indivisual block of contract-customer by PATCH?
    # covered by [IF-40..41]

  Scenario: [contract-customer-2] Can client systems update the indivisual block of contract-customer by Version Up?
    # covered by [IF-34..35]

  Scenario: [DF-1053] billing-affiliate
    Given copy "v2_uat/gmone-contract-info-test1-post-request_billing-affiliate.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And save the response body to "actual-response/DF-1053.xml"
    And the response body corresponds with the content of "v2_uat/gmone-contract-info-test1-get-response0c.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |

  Scenario: [DF-1050-1] attention-line entry by POST
    Given copy "v2_uat/gmone-contract-info-test1-post-request_attention-line.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And save the response body to "actual-response/DF-1050-1.xml"
    And the response body corresponds with the content of "v2_uat/gmone-contract-info-test1-get-response0d.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |

  Scenario: [DF-1050-2] attention-line update by PATCH
    Given copy "v2_uat/gmone-contract-info-test1-post-request_attention-line.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/DF-1050-1.xml"
    And copy "v2_uat/patch-request-update-attention-line.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    And write the response body to stdout
    Then the status code of the HTTP response is "200"
    # And write the response body to stdout
    And send a "GET" request to "/api/v2/contract-information/@{UUID}"
    And save the response body to "actual-response/DF-1050-2.xml"
    # And write the response body to stdout
    And the status code of the HTTP response is "200"
    And the response body corresponds with the content of "v2_uat/expected-patch-res-update-attention-line-1.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |

  Scenario: [DF-1050-3] attention-line update by POST
    Given copy "v2_uat/gmone-contract-info-v1-post-request_attention-line.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    And save the response body to "tmp/ver-up-get.xml"
    And copy "v2_uat/ver-up-request-action-test_attention-line.xml" to "tmp/ver-up-sample.xml"
    And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
    And replace texts selected by "//bi:product-contract/cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
    And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
    When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
    # And write the response body to stdout
    And save the response body to "actual-response/DF-1050-3.xml"
    Then the status code of the HTTP response is "201"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}"
    And save the response body to "actual-response/DF-1050-3.xml"
    And the response body corresponds with the content of "v2_uat/expected-ver-up-get-res-attention-line.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
