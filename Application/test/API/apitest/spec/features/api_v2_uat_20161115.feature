# encoding: utf-8
# language: en

@expectation_steps
@operation_steps
@api_steps
Feature: APIv2
  Background:
    Given initialize a test environment
    And create a session ID

  Scenario: [DF-1053] billing-affiliate
    Given copy "v2_uat/gmone-contract-info-test1-post-request_billing-affiliate.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And save the response body to "actual-response/DF-1053.xml"
    And the response body corresponds with the content of "v2_uat/gmone-contract-info-test1-get-response0c.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |

  Scenario: [DF-1050-1] attention-line entry by POST
    Given copy "v2_uat/gmone-contract-info-test1-post-request_attention-line.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And save the response body to "actual-response/DF-1050-1.xml"
    And the response body corresponds with the content of "v2_uat/gmone-contract-info-test1-get-response0d.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |

  Scenario: [DF-1050-2] attention-line update by PATCH
    Given copy "v2_uat/gmone-contract-info-test1-post-request_attention-line.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/DF-1050-1.xml"
    And copy "v2_uat/patch-request-update-attention-line.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    # And write the response body to stdout
    And send a "GET" request to "/api/v2/contract-information/@{UUID}"
    And save the response body to "actual-response/DF-1050-2.xml"
    # And write the response body to stdout
    And the status code of the HTTP response is "200"
    And the response body corresponds with the content of "v2_uat/expected-patch-res-update-attention-line-1.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |

  Scenario: [DF-1050-3] attention-line update by POST
    Given copy "v2_uat/gmone-contract-info-v1-post-request_attention-line.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    And save the response body to "tmp/ver-up-get.xml"
    And copy "v2_uat/ver-up-request-action-test_attention-line.xml" to "tmp/ver-up-sample.xml"
    And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
    And replace texts selected by "//bi:product-contract/cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
    And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
    When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
    # And write the response body to stdout
    And save the response body to "actual-response/DF-1050-3.xml"
    Then the status code of the HTTP response is "201"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}"
    And save the response body to "actual-response/DF-1050-3.xml"
    And the response body corresponds with the content of "v2_uat/expected-ver-up-get-res-attention-line.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |

  Scenario: [IF-1085] 501 error test scenario
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    And save the response body to "tmp/patch-get.xml"
    And copy "v2_uat/patch-request-DF-1085-1.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
    And write the response body to stdout
    And save the response body to "tmp/DF-1085-1-patch-res.xml"
    Then the status code of the HTTP response is "501"
    And the text content selected by XPath "/exp:exceptions/exp:exception/exp:error-code/text()" matches "9001"
