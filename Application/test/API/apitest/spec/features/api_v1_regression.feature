# encoding: utf-8
# language: ja

@api_v1_steps
@expectation_steps
@operation_steps
フィーチャ: 契約情報API

  背景:
    前提 初期設定を行う
    かつ セッションIDを発行する
    かつ "add-request.xml" を元に契約情報を "testdata1" として作成する

  #----------------------- 契約情報取得API -------------------------------------

  シナリオ: 契約情報取得APIにリクエストを送った際に正しい結果を返す
    もし 契約情報取得APIに "testdata1" のリクエストを送信する
    ならば レスポンスのステータスコードが "200" である
    かつ レスポンスのボディが "get-response.xml" と一致する。ただし、次のXPATHで指定するノードは除く
    | XPATH                                                                                                                                            |
    | //cbe-v0:createdDate                                                                                                                             |
    | //cbe-v0:objectID                                              |
    | /res-v0:response/bi-v0:productContract/cbe-v0:clientRequestID                                                                                    |
    | /res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/cbe-v0:identifiedBy/bi-v0:productItemKey/cbe-v0:objectID |
    | /res-v0:response/bi-v0:productContract/bi-v0:contractID                                                                                          |
    かつ XPATH "//cbe-v0:createdDate/text()" の文字列が正規表現 "^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$" と一致する
    かつ XPATH "//cbe-v0:objectID/text()" の文字列がUUIDである
    かつ XPATH "/res-v0:response/bi-v0:productContract/cbe-v0:clientRequestID/text()" の文字列がUUIDである
    # かつ XPATH "/res-v0:response/bi-v0:productContract/bi-v0:contractID/text()" の文字列が正規表現 "^\d{15}$" と一致する

  シナリオ: 契約情報取得APIに削除済みの契約情報を取得するリクエストを送った際に正しい結果を返す
    前提 "add-request.xml" を元に契約情報を "testdata-get-deleted" として作成する
    かつ 契約情報削除APIに "testdata-get-deleted" のリクエストを送信する
    もし 契約情報取得APIに "testdata-get-deleted" のリクエストを送信する
    ならば レスポンスのステータスコードが "200" である
    かつ レスポンスのボディが "get-response-deleted.xml" と一致する。ただし、次のXPATHで指定するノードは除く
    | XPATH                                                                                                                                            |
    | //cbe-v0:createdDate                                                                                                                             |
    | /res-v0:response/bi-v0:productContract/cbe-v0:identifiedBy/bi-v0:productContractKey/cbe-v0:objectID                                              |
    | /res-v0:response/bi-v0:productContract/cbe-v0:clientRequestID                                                                                    |
    | //cbe-v0:objectID                                              |
    | /res-v0:response/bi-v0:productContract/bi-v0:contractID                                                                                          |
    | /res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:billingPeriod/prodCntrctItem0001:billingEndDate/prodCntrctItem0001:characteristicValue/prodCntrctItem0001:billingEndDate                                                                                                                                          |
    | /res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:servicePeriod/prodCntrctItem0001:serviceEndDate/prodCntrctItem0001:characteristicValue/prodCntrctItem0001:serviceEndDate                                                                                                                                                                                                                                                                                                                                                                                                                        |
    かつ XPATH "//cbe-v0:createdDate/text()" の文字列が正規表現 "^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$" と一致する
    かつ XPATH "/res-v0:response/bi-v0:productContract/cbe-v0:identifiedBy/bi-v0:productContractKey/cbe-v0:objectID/text()" の文字列がUUIDである
    かつ XPATH "/res-v0:response/bi-v0:productContract/cbe-v0:clientRequestID/text()" の文字列がUUIDである
    かつ XPATH "/res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/cbe-v0:identifiedBy/bi-v0:productItemKey/cbe-v0:objectID/text()" の文字列がUUIDである
    かつ XPATH "/res-v0:response/bi-v0:productContract/bi-v0:contractID/text()" の文字列が正規表現 "^\d{15}$" と一致する
    かつ XPATH "/res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:billingPeriod/prodCntrctItem0001:billingEndDate/prodCntrctItem0001:characteristicValue/prodCntrctItem0001:billingEndDate/text()" の文字列が正規表現 "^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(Z|\+\d{2}:\d{2})$" と一致する
    かつ XPATH "/res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:servicePeriod/prodCntrctItem0001:serviceEndDate/prodCntrctItem0001:characteristicValue/prodCntrctItem0001:serviceEndDate/text()" の文字列が正規表現 "^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(Z|\+\d{2}:\d{2})$" と一致する

  シナリオ: 契約情報検索APIに存在しないUUIDを指定してリクエストを送った際に正しいエラーレスポンスを返す
    前提 契約情報取得APIにUUID "eaaf4540-7d22-4f49-9e70-587d6399cf7a" のリクエストを送信する
    ならば レスポンスのステータスコードが "404" である
    かつ レスポンスのボディが "get-response-404.xml" と一致する

  シナリオ: 契約情報検索APIに許可されていないメソッドでリクエストを送信した場合に正しいエラーレスポンスを返す
    前提 契約情報取得APIに "HEAD" メソッドで "testdata1" のリクエストを送信する
    ならば レスポンスのステータスコードが "405" である

  #----------------------- 契約情報検索API -------------------------------------

  シナリオ: 契約情報検索APIにリクエストを送った際に正しい結果を返す
    前提 契約情報検索APIに "testdata1" の契約IDを用いてリクエストを送信する
    ならば レスポンスのステータスコードが "200" である
    かつ レスポンスのボディが "get-response.xml" と一致する。ただし、次のXPATHで指定するノードは除く
    | XPATH                                                                                                                                            |
    | //cbe-v0:createdDate                                                                                                                             |
    | /res-v0:response/bi-v0:productContract/cbe-v0:identifiedBy/bi-v0:productContractKey/cbe-v0:objectID                                              |
    | /res-v0:response/bi-v0:productContract/cbe-v0:clientRequestID                                                                                    |
    | //cbe-v0:objectID                                              |
    | /res-v0:response/bi-v0:productContract/bi-v0:contractID                                                                                          |
    かつ XPATH "//cbe-v0:createdDate/text()" の文字列が正規表現 "^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$" と一致する
    かつ XPATH "/res-v0:response/bi-v0:productContract/cbe-v0:identifiedBy/bi-v0:productContractKey/cbe-v0:objectID/text()" の文字列がUUIDである
    かつ XPATH "/res-v0:response/bi-v0:productContract/cbe-v0:clientRequestID/text()" の文字列がUUIDである
    かつ XPATH "/res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/cbe-v0:identifiedBy/bi-v0:productItemKey/cbe-v0:objectID/text()" の文字列がUUIDである
    かつ XPATH "/res-v0:response/bi-v0:productContract/bi-v0:contractID/text()" の文字列が正規表現 "^\d{15}$" と一致する

  シナリオ: 契約情報検索APIに存在しないcontractIDを指定してリクエストを送った際に正しいエラーメッセージを返す
    前提 契約情報検索APIに契約ID "000000000000000" のリクエストを送信する
    ならば レスポンスのステータスコードが "404" である
    かつ レスポンスのボディが "search-response-404.xml" と一致する

  # TODO: http_status_code=404, msg=Cannot find requested Contract Record Id into the GCM System. で正しいか確認
  シナリオ: 契約情報検索APIに許可されていないメソッドでリクエストを送信した場合に正しいエラーメッセージを返す
    前提 "add-request.xml" を元に契約情報を "testdata-search-deleted" として作成する
    もし 契約情報検索APIに "DELETE" メソッドで "testdata-search-deleted" の契約IDを用いてリクエストを送信する
    ならば レスポンスのステータスコードが "404" である
    かつ レスポンスのボディが "search-response-404.xml" と一致する

  #----------------------- 契約情報登録API -------------------------------------

  シナリオ: 契約情報登録APIにリクエストを送った際に正しい結果を返す
    もし 契約情報登録APIに "add-request.xml" の契約情報を用いてリクエストを送信する
    ならば レスポンスのステータスコードが "201" である
    かつ レスポンスのボディが "add-response.xml" と一致する。ただし、次のXPATHで指定するノードは除く
    | XPATH                                                                                                                                            |
    | /res-v0:response/bi-v0:productContract/cbe-v0:identifiedBy/bi-v0:productContractKey/cbe-v0:objectID                                              |
    | //cbe-v0:objectID                                              |
    | /res-v0:response/bi-v0:productContract/bi-v0:contractID                                                                                          |
    かつ XPATH "/res-v0:response/bi-v0:productContract/cbe-v0:identifiedBy/bi-v0:productContractKey/cbe-v0:objectID/text()" の文字列がUUIDである
    かつ XPATH "/res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/cbe-v0:identifiedBy/bi-v0:productItemKey/cbe-v0:objectID/text()" の文字列がUUIDである
    かつ XPATH "/res-v0:response/bi-v0:productContract/bi-v0:contractID/text()" の文字列が正規表現 "^\d{15}$" と一致する

  シナリオ: 契約情報登録APIのリクエストデータに問題がある場合に正しいエラーメッセージを返す
    もし 契約情報登録APIに "add-request-error.xml" の契約情報を用いてリクエストを送信する
    ならば レスポンスのステータスコードが "400" である
    かつ レスポンスのボディが "add-response-error.xml" と一致する

  シナリオ: 契約情報検索APIに許可されていないメソッドでリクエストを送信した場合に正しいエラーメッセージを返す
    もし 契約情報登録APIに "HEAD" メソッドで "add-request.xml" の契約情報を用いてリクエストを送信する
    ならば レスポンスのステータスコードが "405" である

  #----------------------- 契約情報変更API -------------------------------------

  シナリオ: 契約情報変更APIにリクエストを送った際に正しい結果を返す
    前提 仕様変更中のためペンディング

  シナリオ: リクエストデータに問題がある場合に正しいエラーメッセージを返す
    前提 仕様変更中のためペンディング

  シナリオ: 許可されていないメソッドでリクエストを送信した場合に正しいエラーメッセージを返す
    前提 仕様変更中のためペンディング

  #----------------------- 契約情報削除API -------------------------------------

  シナリオ: 契約情報削除APIにリクエストを送った際に正しい結果を返す
    前提 "add-request.xml" を元に契約情報を "testdata-delete" として作成する
    もし 契約情報削除APIに "testdata-delete" のリクエストを送信する
    ならば レスポンスのステータスコードが "200" である

  シナリオ: 契約情報検索APIに存在しないUUIDを指定してリクエストを送った際に正しいエラーレスポンスを返す
    前提 契約情報削除APIにUUID "eaaf4540-7d22-4f49-9e70-587d6399cf7a" のリクエストを送信する
    ならば レスポンスのステータスコードが "404" である
    かつ レスポンスのボディが "get-response-404.xml" と一致する

  シナリオ: 契約情報削除APIに削除済みの契約情報に対してリクエストを送った際に正しいエラーレスポンスを返す
    前提 "add-request.xml" を元に契約情報を "testdata-delete-deleted" として作成する
    もし 契約情報削除APIに "testdata-delete-deleted" のリクエストを送信する
    かつ 契約情報削除APIに "testdata-delete-deleted" のリクエストを送信する
    ならば レスポンスのステータスコードが "400" である
    かつ レスポンスのボディが "delete-response-2.xml" と一致する

  シナリオ: 契約情報検索APIに許可されていないメソッドでリクエストを送信した場合に正しいエラーメッセージを返す
    前提 "add-request.xml" を元に契約情報を "testdata-delete-method-not-allowed" として作成する
    もし 契約情報削除APIに "HEAD" メソッドで "testdata-delete-method-not-allowed" の契約IDを用いてリクエストを送信する
    ならば レスポンスのステータスコードが "405" である

  # -------------------- Test Scenarios for APIv1 ------------------------------------

  シナリオ: No.1-1
    もし 契約情報取得APIに "testdata1" のリクエストを送信する
    ならば レスポンスのステータスコードが "200" である

  シナリオ: No.1-2
    もし 契約情報登録APIに "add-request.xml" の契約情報を用いてリクエストを送信する
    ならば レスポンスのステータスコードが "201" である

  シナリオ: No.1-4
    前提 契約情報登録APIにボディが "add-request.xml" のリクエストを送信する
    かつ 契約情報登録APIにボディが "add-request.xml" のリクエストを送信する
    ならば レスポンスのステータスコードが "400" である
    かつ XPATH "//exp-v0:messageCode/text()" の文字列が "2002" と一致する

  シナリオ: No.1-5
    前提 契約情報登録APIにボディが "empty.xml" のリクエストを送信する
    ならば レスポンスのステータスコードが "400" である
    かつ XPATH "//exp-v0:messageCode/text()" の文字列が "3002" と一致する

  シナリオ: No.1-6
    前提 "add-request.xml" を "tmp/add-request.xml" にコピーする
    かつ "tmp/add-request.xml" の "//billingCustomerPR:characteristicValue/billingCustomerPR:corporationName" のテキストを "" に変更する
    かつ 契約情報登録APIにボディが "tmp/add-request.xml" のリクエストを送信する
    ならば レスポンスのステータスコードが "400" である
    かつ XPATH "//exp-v0:messageCode/text()" の文字列が "3004" と一致する

  シナリオ: No.1-7
    前提 HTTPヘッダ "Session-Id" に "invalid-session-id" を設定する
    もし 契約情報取得APIに "testdata1" のリクエストを送信する
    ならば レスポンスのステータスコードが "401" である
    かつ XPATH "//exp-v0:messageCode/text()" の文字列が "3001" と一致する

  シナリオ: No.1-8
    前提 契約情報削除APIにUUID "eaaf4540-7d22-4f49-9e70-587d6399cf7a" のリクエストを送信する
    ならば レスポンスのステータスコードが "404" である

  シナリオ: No.2
    前提 契約情報検索APIに "testdata1" の契約IDを用いてリクエストを送信する
    もし 契約情報取得APIに "testdata1" のリクエストを送信する
    ならば XPATH "//prodCntrctItem0001:characteristicValue/prodCntrctItem0001:serviceStartDate/text()" の文字列が正規表現 "^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$" と一致する
    かつ XPATH "//prodCntrctItem0001:characteristicValue/prodCntrctItem0001:serviceEndDate/text()" の文字列が正規表現 "^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$" と一致する
    かつ XPATH "//prodCntrctItem0001:characteristicValue/prodCntrctItem0001:billingStartDate/text()" の文字列が正規表現 "^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$" と一致する
    かつ XPATH "//prodCntrctItem0001:characteristicValue/prodCntrctItem0001:billingEndDate/text()" の文字列が正規表現 "^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$" と一致する

  シナリオ: No.3
    前提 "add-request.xml" を "tmp/add-request.xml" にコピーする
    かつ "tmp/add-request.xml" の "//datatypes-v0:startDateTime" のテキストを "invalid value" に変更する
    もし 契約情報登録APIに "tmp/add-request.xml" の契約情報を用いてリクエストを送信する
    かつ XPATH "//exp-v0:message/text()" の文字列が "Error 1824: Element &#039;{https://qa.devgcm-system.com/custom/XMLSchema/cbe-datatypes}startDateTime&#039;: &#039;invalid value&#039; is not a valid value of the atomic type &#039;xs:dateTime&#039;. in /var/www/html/ on line 21|Error 1824: Element &#039;{https://qa.devgcm-system.com/custom/XMLSchema/cbe-datatypes}startDateTime&#039;: &#039;invalid value&#039; is not a valid value of the atomic type &#039;xs:dateTime&#039;. in /var/www/html/ on line 41|Error 1824: Element &#039;{https://qa.devgcm-system.com/custom/XMLSchema/cbe-datatypes}startDateTime&#039;: &#039;invalid value&#039; is not a valid value of the atomic type &#039;xs:dateTime&#039;. in /var/www/html/ on line 212|" と一致する

  シナリオ: No.4
    前提 "add-request.xml" を元に契約情報を "testdata-delete" として作成する
    もし 契約情報削除APIに "testdata-delete" のリクエストを送信する
    ならば レスポンスのステータスコードが "200" である
    かつ レスポンスのボディが "empty.xml" と一致する

  シナリオ: No.5
    もし 契約情報取得APIに "testdata1" のリクエストを送信する
    ならば XPATH "//cbe-v0:createdDate/text()" の文字列が正規表現 "^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?Z|\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?\+\d{2}:\d{2})$" と一致する
    もし XPATH "//datatypes-v0:startDateTime/text()" の文字列が正規表現 "^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?Z|\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?\+\d{2}:\d{2})$" と一致する
    もし XPATH "//datatypes-v0:endDateTime/text()" の文字列が正規表現 "^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?Z|\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?\+\d{2}:\d{2})$" と一致する
    もし XPATH "//bi-v0:interactionDate/text()" の文字列が正規表現 "^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?Z|\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?\+\d{2}:\d{2})$" と一致する
    もし XPATH "//bi-v0:interactionDateComplete/text()" の文字列が正規表現 "^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?Z|\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?\+\d{2}:\d{2})$" と一致する
    もし XPATH "//prodCntrctItem0001:characteristicValue/prodCntrctItem0001:billingStartDate/text()" の文字列が正規表現 "^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?Z|\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?\+\d{2}:\d{2})$" と一致する
    もし XPATH "//prodCntrctItem0001:characteristicValue/prodCntrctItem0001:serviceStartDate/text()" の文字列が正規表現 "^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?Z|\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?\+\d{2}:\d{2})$" と一致する
    もし XPATH "///prodCntrctItem0001:characteristicValue/prodCntrctItem0001:serviceEndDate/text()" の文字列が正規表現 "^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?Z|\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?\+\d{2}:\d{2})$" と一致する

  シナリオ: No.6-1
    前提 "add-request.xml" を "tmp/add-request.xml" にコピーする
    かつ "tmp/add-request.xml" の "//salesChannelPR:salesChannelPseudoRole" を削除する
    もし "tmp/add-request.xml" を元に契約情報を "testdata-No6" として作成する
    かつ レスポンスのステータスコードが "201" である
    かつ 契約情報取得APIに "testdata-No6" のリクエストを送信する
    かつ XPath "//salesChannelPR:corporationName" で指定される要素が存在しない

  シナリオ: No.6-2
    前提 "add-request.xml" を "tmp/add-request.xml" にコピーする
    かつ "tmp/add-request.xml" の "//pICSalesPR:divisionNameLatin" を削除する
    もし "tmp/add-request.xml" を元に契約情報を "testdata-No6" として作成する
    かつ レスポンスのステータスコードが "201" である
    かつ 契約情報取得APIに "testdata-No6" のリクエストを送信する
    かつ レスポンスのステータスコードが "200" である
    かつ XPath "//pICSalesPR:divisionNameLatin" で指定される要素が存在しない

  シナリオ: No.6-3
    前提 "add-request.xml" を "tmp/add-request.xml" にコピーする
    かつ "tmp/add-request.xml" の "//pICSalesPR:individualNameLatin" を削除する
    もし "tmp/add-request.xml" を元に契約情報を "testdata-No6" として作成する
    かつ レスポンスのステータスコードが "201" である
    かつ 契約情報取得APIに "testdata-No6" のリクエストを送信する
    かつ レスポンスのステータスコードが "200" である
    かつ XPath "//pICSalesPR:individualNameLatin" で指定される要素が存在しない

  シナリオ: No.6-4
    前提 "add-request.xml" を "tmp/add-request.xml" にコピーする
    かつ "tmp/add-request.xml" の "//party-v0:partyRole[.//cbe-v0:validIDOnlyInMessage/text()='customerTechRepRole01']" を削除する
    もし "tmp/add-request.xml" を元に契約情報を "testdata-No6" として作成する
    かつ レスポンスのステータスコードが "201" である
    かつ 契約情報取得APIに "testdata-No6" のリクエストを送信する
    かつ レスポンスのステータスコードが "200" である
    かつ XPath "//pICSalesPR:individualNameLatin" で指定される要素が存在しない

  シナリオ: No.6-5
    前提 "add-request.xml" を "tmp/add-request.xml" にコピーする
    かつ "tmp/add-request.xml" の "//contactMedium:faxNumber" を削除する
    もし "tmp/add-request.xml" を元に契約情報を "testdata-No6" として作成する
    かつ レスポンスのステータスコードが "201" である
    かつ 契約情報取得APIに "testdata-No6" のリクエストを送信する
    かつ レスポンスのステータスコードが "200" である
    かつ XPath "//contactMedium:faxNumber" で指定される要素が存在しない

  シナリオ: No.7
    前提 "add-request.xml" を "tmp/add-request.xml" にコピーする
    かつ "tmp/add-request.xml" の "//salesChannelPR:salesChannelPseudoRole" を削除する
    もし 契約情報登録APIに "tmp/add-request.xml" の契約情報を用いてリクエストを送信する
    かつ レスポンスのステータスコードが "201" である

  シナリオ: No.8
    前提 Pending
    もし 契約情報登録APIに "add-request.xml" の契約情報を用いてリクエストを送信する
    # もし レスポンスのボディを標準出力に表示する
    ならば "//cbe-v0:identifiedBy" で指定される要素に対して ".//cbe-v0:objectID" で指定される要素が存在する

  シナリオ: No.9
    前提 "Session-Id" のHTTPヘッダ名の代わりに "SESSION-Id" を利用する
    もし 契約情報登録APIに "add-request.xml" の契約情報を用いてリクエストを送信する
    ならば レスポンスのステータスコードが "201" である

  シナリオ: No.10
    前提 "add-request.xml" を "tmp/add-request.xml" にコピーする
    かつ "tmp/add-request.xml" の "//contactMedium:characteristicValue/contactMedium:telephoneNumber" のテキストを "+8527280749" に変更する
    もし 契約情報登録APIに "tmp/add-request.xml" の契約情報を用いてリクエストを送信する
    ならば レスポンスのステータスコードが "201" である

  シナリオ: No.11-1(CountryCodeが056の場合)
    前提 Pending
    前提 "add-request.xml" を "tmp/add-request.xml" にコピーする
    かつ "tmp/add-request.xml" の "//location-v0:countryKey[@name='United Kingdom']/location-v0:iso3166_1numeric" のテキストを "056" に変更する
    もし "tmp/add-request.xml" を元に契約情報を "testdata-No11" として作成する
    ならば レスポンスのステータスコードが "201" である
    かつ 契約情報取得APIに "testdata-No11" のリクエストを送信する
    # もし レスポンスのボディを標準出力に表示する
    かつ XPATH "//location-v0:countryKey[@name='United Kingdom']/location-v0:iso3166_1numeric/text()" の文字列が "056" と一致する

  シナリオ: No.11-2(CountryCodeが004の場合)
    前提 Pending
    前提 "add-request.xml" を "tmp/add-request.xml" にコピーする
    かつ "tmp/add-request.xml" の "//location-v0:countryKey[@name='United Kingdom']/location-v0:iso3166_1numeric" のテキストを "004" に変更する
    もし "tmp/add-request.xml" を元に契約情報を "testdata-No11" として作成する
    ならば レスポンスのステータスコードが "201" である
    かつ 契約情報取得APIに "testdata-No11" のリクエストを送信する
    かつ XPATH "//location-v0:countryKey[@name='United Kingdom']/location-v0:iso3166_1numeric/text()" の文字列が "004" と一致する

  シナリオ: No.13
    前提 "add-request.xml" を "tmp/add-request.xml" にコピーする
    かつ "tmp/add-request.xml" の "//billingCustomerPR:characteristicValue/billingCustomerPR:corporationName" のテキストを "<>&'" に変更する
    かつ "tmp/add-request.xml" を元に契約情報を "testdata-No13" として作成する
    もし 契約情報取得APIに "testdata-No13" のリクエストを送信する
    ならば レスポンスのステータスコードが "200" である
    かつ XPATH "//billingCustomerPR:characteristicValue/billingCustomerPR:corporationName/text()" の文字列が "&lt;&gt;&amp;&#039;" と一致する

  シナリオ: No.15
    前提 "add-request.xml" を "tmp/add-request.xml" にコピーする
    かつ 契約情報登録APIに "tmp/add-request.xml" の契約情報を用いてリクエストを送信する
    かつ "tmp/add-request.xml" の "//billingCustomerPR:characteristicValue/billingCustomerPR:corporationNameLatin" のテキストを "Corp Name Duplication Check 3333" に変更する
    かつ "tmp/add-request.xml" を元に契約情報を "testdata-No15" として作成する
    かつ レスポンスのステータスコードが "201" である
    かつ 契約情報取得APIに "testdata-No15" のリクエストを送信する
    ならば レスポンスのステータスコードが "200" である
    かつ XPATH "//billingCustomerPR:characteristicValue/billingCustomerPR:corporationNameLatin/text()" の文字列が "Corp Name Duplication Check 3333" と一致する

  シナリオ: No.16-1(GETのレスポンスの改行コードがLFで統一されている事を確認)
    もし 契約情報登録APIに "add-request.xml" の契約情報を用いてリクエストを送信する
    ならば レスポンスのステータスコードが "201" である
    かつ ボディの改行コードがLFである

  シナリオ: No.16-2(POSTのレスポンスの改行コードがLFで統一されている事を確認)
    もし 契約情報取得APIに "testdata1" のリクエストを送信する
    ならば レスポンスのステータスコードが "200" である
    かつ ボディの改行コードがLFである

  シナリオ: No.20(エラーメッセージにエスケープ必要な文字が含まれてい他場合もエラーを返すことができる)
    もし 契約情報登録APIに "add-request-OM83.xml" の契約情報を用いてリクエストを送信する
    ならば レスポンスのステータスコードが "400" である
    かつ XPATH "//exp-v0:messageCode/text()" の文字列が "3004" と一致する
