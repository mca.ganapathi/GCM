# encoding: utf-8
# language: en

@expectation_steps
@operation_steps
@api_steps
Feature: APIv2
Background:
Given initialize a test environment
And create a session ID



  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_001 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_001/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3010"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_002 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_002/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_002/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_003 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_003/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_003/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_004 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_004/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_004/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3008"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[3]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[3]/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_005 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_005/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_005/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_006 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_006/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_006/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3009"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[3]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[3]/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_007 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_007/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_007/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3009"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3011"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[3]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[3]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[4]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[4]/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_008 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_008/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_008/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3012"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_009 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_009/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_009/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
   Scenario: Evidence_HTTP_errorcodes/TC_DF-859_010 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_010/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3001"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_011 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_011/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_011/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_012 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_012/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_012/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_013 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_013/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_013/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_014 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_014/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_014/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_015 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_015/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_015/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3023"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_HTTP_errorcodes/TC_DF-859_016 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_016/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3012"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
	Scenario: Evidence_HTTP_errorcodes/TC_DF-859_017 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_017/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_017/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3020"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_018 PATCH
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_018/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_018/PATCH_REQ.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3022"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_019 PATCH
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_019/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_019/PATCH_REQ.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3022"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_020 PATCH
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_020/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_020/PATCH_REQ.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3022"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
	Scenario: Evidence_HTTP_errorcodes/TC_DF-859_021 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_021/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_021/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3020"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"

  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_022 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_022/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_022/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_023 PATCH
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_023/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_023/PATCH_REQ.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_024 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_024/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_024/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3023"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_026 PATCH
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_026/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_026/PATCH_REQ.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "501"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "9001"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_027 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_027/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_027/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3020"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_028 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_028/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_028/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3020"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_029 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_029/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_029/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_030 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_031/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3001"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
   Scenario: Evidence_HTTP_errorcodes/TC_DF-859_031 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_031/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3002"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_032 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_032/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_032/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3004"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_033 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_033/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_033/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3007"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_038 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_038/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    	And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_038/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_039 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_039/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_039/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_040 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_040/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_040/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_041 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_041/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_041/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_042 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_042/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_042/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[1]/text()" matches "3021"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[1]/text()"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code[2]/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message[2]/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_043 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_043/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_043/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3013"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_044 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_044/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_044/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3014"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_045 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_045/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_045/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3015"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_046 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_046/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_046/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3016"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_047 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_047/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_047/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3016"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_048 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_048/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_048/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3016"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_049 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_049/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_049/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3017"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_050 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_050/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_050/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_051 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_051/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_051/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3017"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_053 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_053/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3020"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_054 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_054/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_054/PATCH_REQ.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3022"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
  Scenario: Evidence_HTTP_errorcodes/TC_DF-859_055 POST
  	Given copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_055/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	And save the response body to "tmp/ver-up-get.xml"
	And copy "qa_testcase/Evidence_HTTP_errorcodes/TC_DF-859_055/POST_VER_2_REQ.xml" to "tmp/ver-up-sample.xml"
	And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
	#And replace texts selected by "//bi:product-contract//cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
	And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
	When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3016"
	And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
