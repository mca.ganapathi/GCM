# encoding: utf-8
# language: en

@expectation_steps
@operation_steps
@api_steps
Feature: APIv2
  Background:
    Given initialize a test environment
    And create a session ID

  # AC: IF
  Scenario: [DF-1084-1] update any-address without location:iso3166-1numeric by PATCH request
    # Given pending
    Given copy "v2_uat/gmone-contract-info-test1-post-request_contract-customer-other-address.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And save the response body to "actual-response/test-post.xml"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/test-get.xml"
    And copy "v2_uat/patch-request-update-address.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And the status code of the HTTP response is "200"
    And save the response body to "actual-response/DF-1084-1.xml"
    And the response body corresponds with the content of "v2_uat/expected-patch-res-update-address.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |

  Scenario: [DF-1084-2] update american-property-address without location:iso3166-1numeric by PATCH request
    # Given pending
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And save the response body to "actual-response/test-post.xml"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/test-get.xml"
    And copy "v2_uat/patch-request-update-address-2.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And save the response body to "actual-response/DF-1084-2.xml"
    And the status code of the HTTP response is "200"
    And the response body corresponds with the content of "v2_uat/expected-patch-res-update-address-2.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |

  Scenario: [DF-1084-3] update canadian-property-address without location:iso3166-1numeric by PATCH request
    # Given pending
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And save the response body to "actual-response/test-post.xml"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/test-get.xml"
    And copy "v2_uat/patch-request-update-address-3.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And the status code of the HTTP response is "200"
    And save the response body to "actual-response/DF-1084-3.xml"
    And the response body corresponds with the content of "v2_uat/expected-patch-res-update-address-3.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |

  Scenario: [DF-1084-4] update japanese-property-address without location:iso3166-1numeric by PATCH request
    # Given pending
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And save the response body to "actual-response/test-post.xml"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/test-get.xml"
    And copy "v2_uat/patch-request-update-address-4.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And the status code of the HTTP response is "200"
    And save the response body to "actual-response/DF-1084-4.xml"
    And the response body corresponds with the content of "v2_uat/expected-patch-res-update-address-4.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |

  Scenario: [DF-1066-1] update billing-customer by PATCH request
    # Given pending
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And save the response body to "actual-response/test-post.xml"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/test-get.xml"
    And copy "v2_uat/patch-request-update-billing-customer.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And the status code of the HTTP response is "200"
    And save the response body to "actual-response/DF-1066-1.xml"
    And the response body corresponds with the content of "v2_uat/expected-patch-res-update-billing-customer.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |

  Scenario: [DF-1066-2] update billing-customer by PATCH request
    # Given pending
    Given copy "v2_uat/gmone-contract-info-test1-post-request_withou_cid.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And save the response body to "actual-response/test-post.xml"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    And save the response body to "actual-response/test-get.xml"
    And copy "v2_uat/patch-request-update-billing-customer-1.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And the status code of the HTTP response is "200"
    And save the response body to "actual-response/DF-1066-2.xml"
    And the response body corresponds with the content of "v2_uat/expected-patch-res-update-billing-customer-1.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |

  Scenario: [DF-1067] update sales-channel by PATCH request
    # Given pending
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And save the response body to "actual-response/test-post.xml"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/test-get.xml"
    And copy "v2_uat/patch-request-update-sales-channel.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And the status code of the HTTP response is "200"
    And save the response body to "actual-response/DF-1067.xml"
    And the response body corresponds with the content of "v2_uat/expected-patch-res-update-sales-channel.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |

  Scenario: [DF-1069-1] update customer-technical-representative by PATCH request
    # Given pending
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And save the response body to "actual-response/test-post.xml"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/test-get.xml"
    And copy "v2_uat/patch-request-update-customer-technical-representative.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And the status code of the HTTP response is "200"
    And save the response body to "actual-response/DF-1069-1.xml"
    And the response body corresponds with the content of "v2_uat/expected-patch-res-update-customer-technical-representative.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |

  Scenario: [DF-1069-2] update customer-technical-representative by PATCH request
    # Given pending
    Given copy "v2_uat/gmone-contract-info-test1-post-request_withou_cid-customer-technical-representative.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And save the response body to "actual-response/test-post.xml"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/test-get.xml"
    And copy "v2_uat/patch-request-update-customer-technical-representative-1.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    # And write the response body to stdout
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And the status code of the HTTP response is "200"
    And save the response body to "actual-response/DF-1069-2.xml"
    And the response body corresponds with the content of "v2_uat/expected-patch-res-update-customer-technical-representative-1.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |
