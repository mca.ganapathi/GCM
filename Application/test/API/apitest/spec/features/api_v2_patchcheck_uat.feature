# encoding: utf-8
# language: en

@expectation_steps
@operation_steps
@api_steps
Feature: APIv2
  Background:
    Given initialize a test environment
    And create a session ID

  # AC: IF
  Scenario: [DF-1097-1] update gcm:contract-customer without person-in-charge_name by PATCH request
    # Given pending
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And save the response body to "actual-response/test-post.xml"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/test-get.xml"
    And copy "v2_uat/patch-request-update-contract-customer-1.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    And save the response body to "actual-response/patch-res.xml"
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And the status code of the HTTP response is "200"
    And save the response body to "actual-response/get-res-after-patch.xml"
    # And the text content selected by XPath "//gcm:contract-customer/gcm:person-in-charge/gcm:individual/gcm:name/text()" matches "Tatsuya Ogawa"
    And the response body corresponds with the content of "v2_uat/expected-patch-res-update-contract-customer-1.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |

  Scenario: [DF-1097-2] update by PATCH request
    # Given pending
    Given copy "v2_uat/gmone-contract-info-test1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And save the response body to "actual-response/test-post.xml"
    # And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    # And the status code of the HTTP response is "200"
    # And save the response body to "actual-response/test-get.xml"
    And copy "v2_uat/patch-request-1.xml" to "tmp/patch-sample.xml"
    And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
    And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
    When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
    And save the response body to "actual-response/patch-res.xml"
    Then the status code of the HTTP response is "200"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    # And write the response body to stdout
    And the status code of the HTTP response is "200"
    And save the response body to "actual-response/get-res-after-patch.xml"
    # And the text content selected by XPath "//gcm:contract-customer/gcm:person-in-charge/gcm:individual/gcm:name/text()" matches "Tatsuya Ogawa"
    And the response body corresponds with the content of "v2_uat/expected-patch-res-1.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |

  Scenario: [DF-1097-3] entry with standard-corporation-name billing-customer by POST request
    # Given pending
    Given copy "v2_uat/gmone-contract-info-test1-post-request_withou_cid-1.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    And save the response body to "actual-response/test-post.xml"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    And save the response body to "actual-response/get-res.xml"
    Then the status code of the HTTP response is "200"
    And the response body corresponds with the content of "v2_uat/expected-post-res-standard-corp-name-1.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |

  Scenario: [DF-1097-4] update by POST request
    Given copy "v2_uat/gmone-contract-info-v1-post-request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    And send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And the status code of the HTTP response is "201"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
    And activate the product contract whose contract id is "@{CONTRACT_ID}"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
    And save the response body to "tmp/ver-up-get.xml"
    And copy "v2_uat/ver-up-request-action-test-3.xml" to "tmp/ver-up-sample.xml"
    And replace all UUIDs in "tmp/ver-up-sample.xml" according to the GET response
    And replace texts selected by "//bi:product-contract/cbe:client-request-id" with random UUID in "tmp/ver-up-sample.xml"
    And replace texts selected by "//bi:product-contract//bi:contract-id" with "@{CONTRACT_ID}" in "tmp/ver-up-sample.xml"
    When send a "POST" request to "/api/v2/contract-information/@{UUID}" with "tmp/ver-up-sample.xml"
    # And write the response body to stdout
    And save the response body to "actual-response/DF-1097-4.xml"
    Then the status code of the HTTP response is "201"
    And send a "GET" request to "/api/v2/contract-information/@{UUID}"
    And save the response body to "actual-response/DF-1097-4-get.xml"
    And the response body corresponds with the content of "v2_uat/expected-ver-up-get-res-1.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //bi:sequence |
    | //cbe:sequence |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user     |
