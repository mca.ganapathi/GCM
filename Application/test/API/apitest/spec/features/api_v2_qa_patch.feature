# encoding: utf-8
# language: en

@expectation_steps
@operation_steps
@api_steps
Feature: APIv2
Background:
Given initialize a test environment
And create a session ID
  	
Scenario: Evidence_PATCH/TC_DF_369_005 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_369_005/POST_REQ_ECL 1.0.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And save the response body to "actual-response/test-post.xml"
	And copy "qa_testcase/Evidence_PATCH/TC_DF_369_005/PATCH_REQ_DF_369_005.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "200"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	# And write the response body to stdout
	And save the response body to "actual-response/get-res-after-patch.xml"
	And the text content selected by XPath "//gcm:contract-customer/gcm:person-in-charge/gcm:individual/gcm:name/text()" matches "Ashu"
	And the response body corresponds with the content of "qa_testcase/Evidence_PATCH/TC_DF_369_005/GET_005.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                      
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //cbe:client-request-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
Scenario: Evidence_PATCH/TC_DF_369_007 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_369_007/POST_REQ_ECL 1.0.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And save the response body to "actual-response/test-post.xml"
	And copy "qa_testcase/Evidence_PATCH/TC_DF_369_007/PATCH_REQ_DF_369_007.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "200"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	# And write the response body to stdout
	And save the response body to "actual-response/get-res-after-patch.xml"
	And the text content selected by XPath "//gcm:contract-customer/gcm:person-in-charge/gcm:individual/gcm:name-latin/text()" matches "Ashu"
	And the response body corresponds with the content of "qa_testcase/Evidence_PATCH/TC_DF_369_007/GET_007.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                      
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //cbe:client-request-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
Scenario: Evidence_PATCH/TC_DF_369_016 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_369_016/POST_REQ_ECL 1.0.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And copy "qa_testcase/Evidence_PATCH/TC_DF_369_016/PATCH_REQ_DF_369_016.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_PATCH/TC_DF_369_017 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_369_017/POST_REQ_ECL 1.0.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And copy "qa_testcase/Evidence_PATCH/TC_DF_369_017/PATCH_REQ_DF_369_017.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_PATCH/TC_DF_369_018 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_369_018/POST_REQ_ECL 1.0.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And copy "qa_testcase/Evidence_PATCH/TC_DF_369_018/PATCH_REQ_DF_369_018.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_PATCH/TC_DF_369_019 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_369_019/POST_REQ_ECL 1.0.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And copy "qa_testcase/Evidence_PATCH/TC_DF_369_019/PATCH_REQ_DF_369_019.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3022"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_PATCH/TC_DF_369_020 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_369_020/POST_REQ_ECL 1.0.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And copy "qa_testcase/Evidence_PATCH/TC_DF_369_020/PATCH_REQ_DF_369_020.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3022"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_PATCH/TC_DF_371_001 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_371_001/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And save the response body to "actual-response/test-post.xml"
	And copy "qa_testcase/Evidence_PATCH/TC_DF_371_001/PATCH_DF_371_001.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "200"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	# And write the response body to stdout
	And save the response body to "actual-response/get-res-after-patch.xml"
	And the text content selected by XPath "//bi:product-contract/cbe:standard-business-state/text()" matches "Sent for Termination Approval"
	And the response body corresponds with the content of "qa_testcase/Evidence_PATCH/TC_DF_371_001/GET_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                      
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //cbe:client-request-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
Scenario: Evidence_PATCH/TC_DF_371_005 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_371_005/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And save the response body to "actual-response/test-post.xml"
	And copy "qa_testcase/Evidence_PATCH/TC_DF_371_005/PATCH_DF_371_005.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "200"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	# And write the response body to stdout
	And save the response body to "actual-response/get-res-after-patch.xml"
	And the text content selected by XPath "//gcm:contract-customer/gcm:person-in-charge/gcm:individual/gcm:name/text()" matches "Ashu"
	And the response body corresponds with the content of "qa_testcase/Evidence_PATCH/TC_DF_371_005/GET_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                      
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //cbe:client-request-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
	
Scenario: Evidence_PATCH/TC_DF_371_007 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_371_007/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And save the response body to "actual-response/test-post.xml"
	And copy "qa_testcase/Evidence_PATCH/TC_DF_371_007/PATCH_DF_371_007.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "200"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	# And write the response body to stdout
	And save the response body to "actual-response/get-res-after-patch.xml"
	And the text content selected by XPath "//gcm:contract-customer/gcm:person-in-charge/gcm:individual/gcm:name-latin/text()" matches "Ashu"
	And the response body corresponds with the content of "qa_testcase/Evidence_PATCH/TC_DF_371_007/GET_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                      
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //cbe:client-request-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
Scenario: Evidence_PATCH/TC_DF_371_009 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_371_009/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
   	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And save the response body to "actual-response/test-post.xml"
	And copy "qa_testcase/Evidence_PATCH/TC_DF_371_009/PATCH_DF_371_009.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/search?bi:contract-id=@{CONTRACT_ID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "200"
	And send a "GET" request to "/api/v2/contract-information/@{UUID}:1"
	# And write the response body to stdout
	And save the response body to "actual-response/get-res-after-patch.xml"
	And the text content selected by XPath "//gcm:contract-customer/gcm:person-in-charge/gcm:individual/gcm:name/text()" matches "Ben"
	And the response body corresponds with the content of "qa_testcase/Evidence_PATCH/TC_DF_371_009/GET_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH |                                                                                                                      
    | //cbe:object-id |
	| //cbe:link/cbe:href |
	| //cbe:xml-path/cbe:location-path |
	| //bi:contract-id |
	| //cbe:client-request-id |
	| //@id |
	| //bi:sequence |
	| //cbe:sequence |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
	| //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-user |
	
Scenario: Evidence_PATCH/TC_DF_371_012 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_371_012/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And copy "qa_testcase/Evidence_PATCH/TC_DF_371_012/PATCH_REQ_DF_371_012.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3022"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_PATCH/TC_DF_371_015 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_371_015/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And copy "qa_testcase/Evidence_PATCH/TC_DF_371_015/PATCH_REQ_DF_371_015.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_PATCH/TC_DF_371_016 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_371_016/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And copy "qa_testcase/Evidence_PATCH/TC_DF_371_016/PATCH_REQ_DF_371_016.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_PATCH/TC_DF_371_017 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_371_017/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And copy "qa_testcase/Evidence_PATCH/TC_DF_371_017/PATCH_REQ_DF_371_017.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3006"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_PATCH/TC_DF_371_018 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_371_018/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And copy "qa_testcase/Evidence_PATCH/TC_DF_371_018/PATCH_REQ_DF_371_018.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3018"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_PATCH/TC_DF_371_019 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_371_019/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And copy "qa_testcase/Evidence_PATCH/TC_DF_371_019/PATCH_REQ_DF_371_019.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3022"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
	
Scenario: Evidence_PATCH/TC_DF_371_020 PATCH
  	Given copy "qa_testcase/Evidence_PATCH/TC_DF_371_020/POST_VER_1_REQ.xml" to "tmp/post-sample.xml"
   	And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
	When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
	And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
	And set "//bi:product-contract//bi:contract-id/text()" in response to "CONTRACT_ID" parameter
	And activate the product contract whose contract id is "@{CONTRACT_ID}"	
	And copy "qa_testcase/Evidence_PATCH/TC_DF_371_020/PATCH_REQ_DF_371_020.xml" to "tmp/patch-sample.xml"
	And set the "id" attribute of "//bi:product-contract" to "@{UUID}:1" in "tmp/patch-sample.xml"
	And replace texts selected by "//bi:product-contract//cbe:object-id" with "@{UUID}" in "tmp/patch-sample.xml"
	When send a "PATCH" request to "/api/v2/contract-information/@{UUID}" with "tmp/patch-sample.xml"
	Then the status code of the HTTP response is "400"
	And the text content selected by XPath "exp:exceptions/exp:exception/exp:error-code/text()" matches "3022"
    And there are elements specified with "exp:exceptions/exp:exception/exp:error-message/text()"
