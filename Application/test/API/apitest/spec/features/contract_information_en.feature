# encoding: utf-8
# language: en

@expectation_steps
@operation_steps
@api_steps
Feature: Contract Information API Test Sample

  Background:
    Given initialize a test environment
    And create a session ID
    And create a contract information identified by "testdata1" based on "add-request.xml"

  #----------------------- GET API -------------------------------------

  Scenario: The GET API returns a correct response when a correct request is sent to the API
    When send a request for "testdata1" to GET API
    Then the status code of the HTTP response is "200"
    And the response body corresponds with the content of "get-response.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe-v0:createdDate                                                                                                                             |
    | /res-v0:response/bi-v0:productContract/cbe-v0:identifiedBy/bi-v0:productContractKey/cbe-v0:objectID                                              |
    | /res-v0:response/bi-v0:productContract/cbe-v0:clientRequestID                                                                                    |
    | /res-v0:response/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/cbe-v0:identifiedBy/bi-v0:productItemKey/cbe-v0:objectID |
    | /res-v0:response/bi-v0:productContract/bi-v0:contractID                                                                                          |
    And the text content selected by XPath "//cbe-v0:createdDate/text()" matches regex "^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$"
    And the text content selected by XPath "//cbe-v0:objectID/text()" is UUID
    And the text content selected by XPath "/res-v0:response/bi-v0:productContract/cbe-v0:clientRequestID/text()" is UUID
    And the text content selected by XPath "/res-v0:response/bi-v0:productContract/bi-v0:contractID/text()" matches regex "^\d{15}$"
