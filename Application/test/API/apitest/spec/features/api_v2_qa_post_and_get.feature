  # encoding: utf-8
  # language: en

  @expectation_steps
  @operation_steps
  @api_steps
  Feature: APIv2
    Background:
      Given initialize a test environment
      And create a session ID

  Scenario: Evidence_Nullable/TC_API_2.0_NF_001 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_001/TC_API_2.0_NF_001_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_001/TC_API_2.0_NF_001_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_001/TC_API_2.0_NF_001_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_002 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_002/TC_API_2.0_NF_002_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_002/TC_API_2.0_NF_002_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_002/TC_API_2.0_NF_002_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_003 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_003/TC_API_2.0_NF_003_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_003/TC_API_2.0_NF_003_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_003/TC_API_2.0_NF_003_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_004 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_004/TC_API_2.0_NF_004_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_004/TC_API_2.0_NF_004_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_004/TC_API_2.0_NF_004_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_005 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_005/TC_API_2.0_NF_005_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_005/TC_API_2.0_NF_005_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_005/TC_API_2.0_NF_005_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_006 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_006/TC_API_2.0_NF_006_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_006/TC_API_2.0_NF_006_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_006/TC_API_2.0_NF_006_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_007 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_007/TC_API_2.0_NF_007_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_007/TC_API_2.0_NF_007_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_007/TC_API_2.0_NF_007_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_008 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_008/TC_API_2.0_NF_008_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_008/TC_API_2.0_NF_008_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_008/TC_API_2.0_NF_008_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_009 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_009/TC_API_2.0_NF_009_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_009/TC_API_2.0_NF_009_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_009/TC_API_2.0_NF_009_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_010 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_010/TC_API_2.0_NF_010_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_010/TC_API_2.0_NF_010_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_010/TC_API_2.0_NF_010_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_011 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_011/TC_API_2.0_NF_011_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_011/TC_API_2.0_NF_011_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_011/TC_API_2.0_NF_011_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_012 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_012/TC_API_2.0_NF_012_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_012/TC_API_2.0_NF_012_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_012/TC_API_2.0_NF_012_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_013 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_013/TC_API_2.0_NF_013_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_013/TC_API_2.0_NF_013_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_013/TC_API_2.0_NF_013_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_014 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_014/TC_API_2.0_NF_014_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_014/TC_API_2.0_NF_014_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_014/TC_API_2.0_NF_014_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_015 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_015/TC_API_2.0_NF_015_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_015/TC_API_2.0_NF_015_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_015/TC_API_2.0_NF_015_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_016 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_016/TC_API_2.0_NF_016_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_016/TC_API_2.0_NF_016_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_016/TC_API_2.0_NF_016_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_017 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_017/TC_API_2.0_NF_017_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_017/TC_API_2.0_NF_017_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_017/TC_API_2.0_NF_017_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_018 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_018/TC_API_2.0_NF_018_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_018/TC_API_2.0_NF_018_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_018/TC_API_2.0_NF_018_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_019 POST
    Given Pending
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_019/TC_API_2.0_NF_019_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_019/TC_API_2.0_NF_019_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_019/TC_API_2.0_NF_019_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_020 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_020/TC_API_2.0_NF_020_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_020/TC_API_2.0_NF_020_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_020/TC_API_2.0_NF_020_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_021 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_021/TC_API_2.0_NF_021_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_021/TC_API_2.0_NF_021_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_021/TC_API_2.0_NF_021_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_022 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_022/TC_API_2.0_NF_022_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_022/TC_API_2.0_NF_022_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_022/TC_API_2.0_NF_022_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_023 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_023/TC_API_2.0_NF_023_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_023/TC_API_2.0_NF_023_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_023/TC_API_2.0_NF_023_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_024 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_024/TC_API_2.0_NF_024_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_024/TC_API_2.0_NF_024_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_024/TC_API_2.0_NF_024_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_025 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_025/TC_API_2.0_NF_025_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_025/TC_API_2.0_NF_025_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_025/TC_API_2.0_NF_025_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_026 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_026/TC_API_2.0_NF_026_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_026/TC_API_2.0_NF_026_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_026/TC_API_2.0_NF_026_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_027 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_027/TC_API_2.0_NF_027_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_027/TC_API_2.0_NF_027_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_027/TC_API_2.0_NF_027_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_028 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_028/TC_API_2.0_NF_028_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_028/TC_API_2.0_NF_028_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_028/TC_API_2.0_NF_028_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_029 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_029/TC_API_2.0_NF_029_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_029/TC_API_2.0_NF_029_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_029/TC_API_2.0_NF_029_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_030 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_030/TC_API_2.0_NF_030_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_030/TC_API_2.0_NF_030_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_030/TC_API_2.0_NF_030_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_031 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_031/TC_API_2.0_NF_031_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_031/TC_API_2.0_NF_031_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_031/TC_API_2.0_NF_031_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_032 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_032/TC_API_2.0_NF_032_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_032/TC_API_2.0_NF_032_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_032/TC_API_2.0_NF_032_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_033 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_033/TC_API_2.0_NF_033_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_033/TC_API_2.0_NF_033_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_033/TC_API_2.0_NF_033_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_034 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_034/TC_API_2.0_NF_034_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_034/TC_API_2.0_NF_034_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_034/TC_API_2.0_NF_034_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_035 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_035/TC_API_2.0_NF_035_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_035/TC_API_2.0_NF_035_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_035/TC_API_2.0_NF_035_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_036 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_036/TC_API_2.0_NF_036_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_036/TC_API_2.0_NF_036_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_036/TC_API_2.0_NF_036_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_037 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_037/TC_API_2.0_NF_037_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_037/TC_API_2.0_NF_037_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_037/TC_API_2.0_NF_037_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_038 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_038/TC_API_2.0_NF_038_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_038/TC_API_2.0_NF_038_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_038/TC_API_2.0_NF_038_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_039 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_039/TC_API_2.0_NF_039_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_039/TC_API_2.0_NF_039_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_039/TC_API_2.0_NF_039_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_040 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_040/TC_API_2.0_NF_040_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_040/TC_API_2.0_NF_040_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_040/TC_API_2.0_NF_040_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_041 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_041/TC_API_2.0_NF_041_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_041/TC_API_2.0_NF_041_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_041/TC_API_2.0_NF_041_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_042 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_042/TC_API_2.0_NF_042_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_042/TC_API_2.0_NF_042_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_042/TC_API_2.0_NF_042_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_043 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_043/TC_API_2.0_NF_043_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_043/TC_API_2.0_NF_043_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_043/TC_API_2.0_NF_043_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_044 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_044/TC_API_2.0_NF_044_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_044/TC_API_2.0_NF_044_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_044/TC_API_2.0_NF_044_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_045 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_045/TC_API_2.0_NF_045_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_045/TC_API_2.0_NF_045_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_045/TC_API_2.0_NF_045_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_046 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_046/TC_API_2.0_NF_046_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_046/TC_API_2.0_NF_046_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_046/TC_API_2.0_NF_046_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_047 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_047/TC_API_2.0_NF_047_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_047/TC_API_2.0_NF_047_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_047/TC_API_2.0_NF_047_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_048 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_048/TC_API_2.0_NF_048_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_048/TC_API_2.0_NF_048_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_048/TC_API_2.0_NF_048_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_049 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_049/TC_API_2.0_NF_049_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_049/TC_API_2.0_NF_049_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_049/TC_API_2.0_NF_049_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_050 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_050/TC_API_2.0_NF_050_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_050/TC_API_2.0_NF_050_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_050/TC_API_2.0_NF_050_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_051 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_051/TC_API_2.0_NF_051_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_051/TC_API_2.0_NF_051_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_051/TC_API_2.0_NF_051_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_052 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_052/TC_API_2.0_NF_052_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_052/TC_API_2.0_NF_052_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_052/TC_API_2.0_NF_052_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_053 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_053/TC_API_2.0_NF_053_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_053/TC_API_2.0_NF_053_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_053/TC_API_2.0_NF_053_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_054 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_054/TC_API_2.0_NF_054_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_054/TC_API_2.0_NF_054_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_054/TC_API_2.0_NF_054_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_055 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_055/TC_API_2.0_NF_055_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_055/TC_API_2.0_NF_055_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_055/TC_API_2.0_NF_055_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_056 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_056/TC_API_2.0_NF_056_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_056/TC_API_2.0_NF_056_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_056/TC_API_2.0_NF_056_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_057 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_057/TC_API_2.0_NF_057_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_057/TC_API_2.0_NF_057_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_057/TC_API_2.0_NF_057_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_058 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_058/TC_API_2.0_NF_058_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_058/TC_API_2.0_NF_058_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_058/TC_API_2.0_NF_058_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_059 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_059/TC_API_2.0_NF_059_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_059/TC_API_2.0_NF_059_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_059/TC_API_2.0_NF_059_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_060 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_060/TC_API_2.0_NF_060_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_060/TC_API_2.0_NF_060_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_060/TC_API_2.0_NF_060_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_061 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_061/TC_API_2.0_NF_061_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_061/TC_API_2.0_NF_061_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_061/TC_API_2.0_NF_061_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_062 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_062/TC_API_2.0_NF_062_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_062/TC_API_2.0_NF_062_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_062/TC_API_2.0_NF_062_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_063 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_063/TC_API_2.0_NF_063_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_063/TC_API_2.0_NF_063_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_063/TC_API_2.0_NF_063_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_064 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_064/TC_API_2.0_NF_064_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_064/TC_API_2.0_NF_064_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_064/TC_API_2.0_NF_064_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_065 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_065/TC_API_2.0_NF_065_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_065/TC_API_2.0_NF_065_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_065/TC_API_2.0_NF_065_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_066 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_066/TC_API_2.0_NF_066_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_066/TC_API_2.0_NF_066_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_066/TC_API_2.0_NF_066_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_067 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_067/TC_API_2.0_NF_067_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_067/TC_API_2.0_NF_067_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_067/TC_API_2.0_NF_067_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_068 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_068/TC_API_2.0_NF_068_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_068/TC_API_2.0_NF_068_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_068/TC_API_2.0_NF_068_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_069 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_069/TC_API_2.0_NF_069_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_069/TC_API_2.0_NF_069_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_069/TC_API_2.0_NF_069_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_070 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_070/TC_API_2.0_NF_070_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_070/TC_API_2.0_NF_070_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_070/TC_API_2.0_NF_070_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_071 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_071/TC_API_2.0_NF_071_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_071/TC_API_2.0_NF_071_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_071/TC_API_2.0_NF_071_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_072 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_072/TC_API_2.0_NF_072_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_072/TC_API_2.0_NF_072_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_072/TC_API_2.0_NF_072_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_073 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_073/TC_API_2.0_NF_073_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_073/TC_API_2.0_NF_073_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_073/TC_API_2.0_NF_073_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_074 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_074/TC_API_2.0_NF_074_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_074/TC_API_2.0_NF_074_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_074/TC_API_2.0_NF_074_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_075 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_075/TC_API_2.0_NF_075_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_075/TC_API_2.0_NF_075_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_075/TC_API_2.0_NF_075_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_076 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_076/TC_API_2.0_NF_076_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_076/TC_API_2.0_NF_076_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_076/TC_API_2.0_NF_076_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_077 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_077/TC_API_2.0_NF_077_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_077/TC_API_2.0_NF_077_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_077/TC_API_2.0_NF_077_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_078 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_078/TC_API_2.0_NF_078_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_078/TC_API_2.0_NF_078_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_078/TC_API_2.0_NF_078_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_079 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_079/TC_API_2.0_NF_079_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_079/TC_API_2.0_NF_079_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_079/TC_API_2.0_NF_079_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_080 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_080/TC_API_2.0_NF_080_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_080/TC_API_2.0_NF_080_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_080/TC_API_2.0_NF_080_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_Nullable/TC_API_2.0_NF_081 POST
    Given copy "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_081/TC_API_2.0_NF_081_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_081/TC_API_2.0_NF_081_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_Nullable/TC_API_2.0_NF_081/TC_API_2.0_NF_081_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/Spl Scenario POST
    Given copy "qa_testcase/Evidence_POST_request/Spl Scenario/Global Management One UNO Option POST Request.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/Spl Scenario/Global Management One UNO Option POST Res.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/Spl Scenario/Global Management One UNO Option POST Res.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_001 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_001/TC_API_2.0_PS_001_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_001/TC_API_2.0_PS_001_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_001/TC_API_2.0_PS_001_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_002 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_002/TC_API_2.0_PS_002_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_002/TC_API_2.0_PS_002_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_002/TC_API_2.0_PS_002_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_003 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_003/TC_API_2.0_PS_003_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_003/TC_API_2.0_PS_003_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_003/TC_API_2.0_PS_003_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_004 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_004/TC_API_2.0_PS_004_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_004/TC_API_2.0_PS_004_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_004/TC_API_2.0_PS_004_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_005 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_005/TC_API_2.0_PS_005_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_005/TC_API_2.0_PS_005_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_005/TC_API_2.0_PS_005_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_006 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_006/TC_API_2.0_PS_006_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_006/TC_API_2.0_PS_006_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_006/TC_API_2.0_PS_006_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_007 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_007/TC_API_2.0_PS_007_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_007/TC_API_2.0_PS_007_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_007/TC_API_2.0_PS_007_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_008 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_008/TC_API_2.0_PS_008_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_008/TC_API_2.0_PS_008_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_008/TC_API_2.0_PS_008_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_009 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_009/TC_API_2.0_PS_009_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_009/TC_API_2.0_PS_009_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_009/TC_API_2.0_PS_009_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_010 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_010/TC_API_2.0_PS_010_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_010/TC_API_2.0_PS_010_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_010/TC_API_2.0_PS_010_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_011 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_011/TC_API_2.0_PS_011_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_011/TC_API_2.0_PS_011_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_011/TC_API_2.0_PS_011_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_012 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_012/TC_API_2.0_PS_012_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_012/TC_API_2.0_PS_012_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_012/TC_API_2.0_PS_012_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_013 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_013/TC_API_2.0_PS_013_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_013/TC_API_2.0_PS_013_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_013/TC_API_2.0_PS_013_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_014 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_014/TC_API_2.0_PS_014_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_014/TC_API_2.0_PS_014_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_014/TC_API_2.0_PS_014_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_015 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_015/TC_API_2.0_PS_015_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_015/TC_API_2.0_PS_015_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_015/TC_API_2.0_PS_015_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_016 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_016/TC_API_2.0_PS_016_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_016/TC_API_2.0_PS_016_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_016/TC_API_2.0_PS_016_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_017 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_017/TC_API_2.0_PS_017_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_017/TC_API_2.0_PS_017_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_017/TC_API_2.0_PS_017_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_018 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_018/TC_API_2.0_PS_018_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_018/TC_API_2.0_PS_018_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_018/TC_API_2.0_PS_018_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_019 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_019/TC_API_2.0_PS_019_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_019/TC_API_2.0_PS_019_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_019/TC_API_2.0_PS_019_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_020 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_020/TC_API_2.0_PS_020_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_020/TC_API_2.0_PS_020_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_020/TC_API_2.0_PS_020_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_021 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_021/TC_API_2.0_PS_021_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_021/TC_API_2.0_PS_021_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_021/TC_API_2.0_PS_021_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_022 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_022/TC_API_2.0_PS_022_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_022/TC_API_2.0_PS_022_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_022/TC_API_2.0_PS_022_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_023 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_023/TC_API_2.0_PS_023_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_023/TC_API_2.0_PS_023_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_023/TC_API_2.0_PS_023_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_024 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_024/TC_API_2.0_PS_024_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_024/TC_API_2.0_PS_024_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_024/TC_API_2.0_PS_024_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_025 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_025/TC_API_2.0_PS_025_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_025/TC_API_2.0_PS_025_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_025/TC_API_2.0_PS_025_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_026 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_026/TC_API_2.0_PS_026_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_026/TC_API_2.0_PS_026_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_026/TC_API_2.0_PS_026_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_027 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_027/TC_API_2.0_PS_027_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_027/TC_API_2.0_PS_027_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_027/TC_API_2.0_PS_027_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_028 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_028/TC_API_2.0_PS_028_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_028/TC_API_2.0_PS_028_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_028/TC_API_2.0_PS_028_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_029 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_029/TC_API_2.0_PS_029_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_029/TC_API_2.0_PS_029_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_029/TC_API_2.0_PS_029_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_030 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_030/TC_API_2.0_PS_030_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_030/TC_API_2.0_PS_030_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_030/TC_API_2.0_PS_030_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_031 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_031/TC_API_2.0_PS_031_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_031/TC_API_2.0_PS_031_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_031/TC_API_2.0_PS_031_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_032 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_032/TC_API_2.0_PS_032_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_032/TC_API_2.0_PS_032_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_032/TC_API_2.0_PS_032_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_033 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_033/TC_API_2.0_PS_033_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_033/TC_API_2.0_PS_033_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_033/TC_API_2.0_PS_033_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_034 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_034/TC_API_2.0_PS_034_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_034/TC_API_2.0_PS_034_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_034/TC_API_2.0_PS_034_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_035 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_035/TC_API_2.0_PS_035_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_035/TC_API_2.0_PS_035_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_035/TC_API_2.0_PS_035_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_036 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_036/TC_API_2.0_036_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_036/TC_API_2.0_036_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_036/TC_API_2.0_036_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_037 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_037/TC_API_2.0_PS_037_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_037/TC_API_2.0_PS_037_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_037/TC_API_2.0_PS_037_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_038 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_038/TC_API_2.0_PS_038_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_038/TC_API_2.0_PS_038_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_038/TC_API_2.0_PS_038_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_039 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_039/TC_API_2.0_PS_039_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_039/TC_API_2.0_PS_039_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_039/TC_API_2.0_PS_039_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_040 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_040/TC_API_2.0_PS_040_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_040/TC_API_2.0_PS_040_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_040/TC_API_2.0_PS_040_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_041 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_041/TC_API_2.0_PS_041_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_041/TC_API_2.0_PS_041_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_041/TC_API_2.0_PS_041_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_043 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_043/TC_API_2.0_PS_043_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_043/TC_API_2.0_PS_043_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_043/TC_API_2.0_PS_043_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_046 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_046/TC_API_2.0_PS_046_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_046/TC_API_2.0_PS_046_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_046/TC_API_2.0_PS_046_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_047 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_047/TC_API_2.0_PS_047_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_047/TC_API_2.0_PS_047_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_047/TC_API_2.0_PS_047_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_048 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_048/TC_API_2.0_PS_048_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_048/TC_API_2.0_PS_048_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_048/TC_API_2.0_PS_048_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_049 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_049/TC_API_2.0_PS_049_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_049/TC_API_2.0_PS_049_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_049/TC_API_2.0_PS_049_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_050 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_050/TC_API_2.0_PS_050_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_050/TC_API_2.0_PS_050_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_050/TC_API_2.0_PS_050_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_051 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_051/TC_API_2.0_PS_051_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_051/TC_API_2.0_PS_051_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_051/TC_API_2.0_PS_051_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_052 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_052/TC_API_2.0_PS_052_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_052/TC_API_2.0_PS_052_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_052/TC_API_2.0_PS_052_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_053 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_053/TC_API_2.0_PS_053_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_053/TC_API_2.0_PS_053_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_053/TC_API_2.0_PS_053_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_054 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_054/TC_API_2.0_PS_054_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_054/TC_API_2.0_PS_054_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_054/TC_API_2.0_PS_054_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_055 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_055/TC_API_2.0_PS_055_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_055/TC_API_2.0_PS_055_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_055/TC_API_2.0_PS_055_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_056 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_056/TC_API_2.0_PS_056_POST_REQ.xml.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_056/TC_API_2.0_PS_056_POST_RES.xml.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_056/TC_API_2.0_PS_056_POST_RES.xml.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_057 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_057/TC_API_2.0_PS_057_POST_REQ.xml.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_057/TC_API_2.0_PS_057_POST_RES.xml.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_057/TC_API_2.0_PS_057_POST_RES.xml.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_058 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_058/TC_API_2.0_PS_058_POST_REQ.xml.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_058/TC_API_2.0_PS_058_POST_RES.xml.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_058/TC_API_2.0_PS_058_POST_RES.xml.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_059 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_059/TC_API_2.0_PS_059_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_059/TC_API_2.0_PS_059_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_059/TC_API_2.0_PS_059_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_060 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_060/TC_API_2.0_PS_060_POST_REQ.xml.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_060/TC_API_2.0_PS_060_POST_RES.xml.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_060/TC_API_2.0_PS_060_POST_RES.xml.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_061 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_061/TC_API_2.0_PS_061_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_061/TC_API_2.0_PS_061_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_061/TC_API_2.0_PS_061_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_062 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_062/TC_API_2.0_PS_062_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_062/TC_API_2.0_PS_062_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_062/TC_API_2.0_PS_062_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_063 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_063/TC_API_2.0_PS_063_POST_REQ.xml.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_063/TC_API_2.0_PS_063_POST_RES.xml.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_063/TC_API_2.0_PS_063_POST_RES.xml.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_064 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_064/TC_API_2.0_PS_064_POST_REQ.xml.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_064/TC_API_2.0_PS_064_POST_RES.xml.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_064/TC_API_2.0_PS_064_POST_RES.xml.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_065 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_065/TC_API_2.0_PS_065_POST_REQ.xml.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_065/TC_API_2.0_PS_065_POST_RES.xml.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_065/TC_API_2.0_PS_065_POST_RES.xml.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_066 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_066/TC_API_2.0_PS_066_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_066/TC_API_2.0_PS_066_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_066/TC_API_2.0_PS_066_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_067 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_067/TC_API_2.0_PS_067_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_067/TC_API_2.0_PS_067_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_067/TC_API_2.0_PS_067_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_068 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_068/TC_API_2.0_PS_068_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_068/TC_API_2.0_PS_068_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_068/TC_API_2.0_PS_068_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_069 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_069/TC_API_2.0_PS_069_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_069/TC_API_2.0_PS_069_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_069/TC_API_2.0_PS_069_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_070 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_070/TC_API_2.0_PS_070_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_070/TC_API_2.0_PS_070_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_070/TC_API_2.0_PS_070_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_071 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_071/TC_API_2.0_PS_071_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_071/TC_API_2.0_PS_071_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_071/TC_API_2.0_PS_071_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_072 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_072/TC_API_2.0_PS_072_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_072/TC_API_2.0_PS_072_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_072/TC_API_2.0_PS_072_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_073 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_073/TC_API_2.0_PS_073_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_073/TC_API_2.0_PS_073_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_073/TC_API_2.0_PS_073_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_074 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_074/TC_API_2.0_PS_074_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_074/TC_API_2.0_PS_074_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_074/TC_API_2.0_PS_074_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_075 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_075/TC_API_2.0_PS_075_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_075/TC_API_2.0_PS_075_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_075/TC_API_2.0_PS_075_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_076 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_076/TC_API_2.0_PS_076_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_076/TC_API_2.0_PS_076_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_076/TC_API_2.0_PS_076_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_077 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_077/TC_API_2.0_PS_077_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_077/TC_API_2.0_PS_077_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_077/TC_API_2.0_PS_077_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_078 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_078/TC_API_2.0_PS_078_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_078/TC_API_2.0_PS_078_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_078/TC_API_2.0_PS_078_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_079 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_079/TC_API_2.0_PS_079_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_079/TC_API_2.0_PS_079_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_079/TC_API_2.0_PS_079_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_080 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_080/TC_API_2.0_PS_080_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_080/TC_API_2.0_PS_080_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_080/TC_API_2.0_PS_080_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_081 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_081/TC_API_2.0_PS_081_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_081/TC_API_2.0_PS_081_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_081/TC_API_2.0_PS_081_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_082 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_082/TC_API_2.0_PS_082_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_082/TC_API_2.0_PS_082_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_082/TC_API_2.0_PS_082_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_083 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_083/TC_API_2.0_PS_083_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_083/TC_API_2.0_PS_083_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_083/TC_API_2.0_PS_083_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_084 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_084/TC_API_2.0_PS_084_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_084/TC_API_2.0_PS_084_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_084/TC_API_2.0_PS_084_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_085 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_085/TC_API_2.0_PS_085_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_085/TC_API_2.0_PS_085_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_085/TC_API_2.0_PS_085_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_086 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_086/TC_API_2.0_PS_086_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_086/TC_API_2.0_PS_086_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_086/TC_API_2.0_PS_086_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_087 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_087/TC_API_2.0_PS_087_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_087/TC_API_2.0_PS_087_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_087/TC_API_2.0_PS_087_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_088 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_088/TC_API_2.0_PS_088_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_088/TC_API_2.0_PS_088_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_088/TC_API_2.0_PS_088_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_092 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_092/TC_API_2.0_PS_092_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_092/TC_API_2.0_PS_092_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_092/TC_API_2.0_PS_092_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_093 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_093/TC_API_2.0_PS_093_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_093/TC_API_2.0_PS_093_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_093/TC_API_2.0_PS_093_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_094 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_094/TC_API_2.0_PS_094_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_094/TC_API_2.0_PS_094_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_094/TC_API_2.0_PS_094_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_095 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_095/TC_API_2.0_PS_095_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_095/TC_API_2.0_PS_095_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_095/TC_API_2.0_PS_095_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_098 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_098/TC_API_2.0_PS_098_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_098/TC_API_2.0_PS_098_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_098/TC_API_2.0_PS_098_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_099 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_099/TC_API_2.0_PS_099_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_099/TC_API_2.0_PS_099_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_099/TC_API_2.0_PS_099_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_100 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_100/TC_API_2.0_PS_100_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_100/TC_API_2.0_PS_100_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_100/TC_API_2.0_PS_100_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_101 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_101/TC_API_2.0_PS_101_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_101/TC_API_2.0_PS_101_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_101/TC_API_2.0_PS_101_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_102 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_102/TC_API_2.0_PS_102_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_102/TC_API_2.0_PS_102_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_102/TC_API_2.0_PS_102_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_105 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_105/TC_API_2.0_PS_105_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_105/TC_API_2.0_PS_105_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_105/TC_API_2.0_PS_105_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_106 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_106/TC_API_2.0_PS_106_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_106/TC_API_2.0_PS_106_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_106/TC_API_2.0_PS_106_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_107 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_107/TC_API_2.0_PS_107_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_107/TC_API_2.0_PS_107_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_107/TC_API_2.0_PS_107_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_108 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_108/TC_API_2.0_PS_108_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_108/TC_API_2.0_PS_108_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_108/TC_API_2.0_PS_108_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_109 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_109/TC_API_2.0_PS_109_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_109/TC_API_2.0_PS_109_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_109/TC_API_2.0_PS_109_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_112 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_112/TC_API_2.0_PS_112_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_112/TC_API_2.0_PS_112_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_112/TC_API_2.0_PS_112_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_113 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_113/TC_API_2.0_PS_113_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_113/TC_API_2.0_PS_113_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_113/TC_API_2.0_PS_113_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_114 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_114/TC_API_2.0_PS_114_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_114/TC_API_2.0_PS_114_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_114/TC_API_2.0_PS_114_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_115 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_115/TC_API_2.0_PS_115_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_115/TC_API_2.0_PS_115_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_115/TC_API_2.0_PS_115_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_116 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_116/TC_API_2.0_PS_116_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_116/TC_API_2.0_PS_116_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_116/TC_API_2.0_PS_116_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_119 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_119/TC_API_2.0_PS_119_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_119/TC_API_2.0_PS_119_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_119/TC_API_2.0_PS_119_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_120 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_120/TC_API_2.0_PS_120_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_120/TC_API_2.0_PS_120_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_120/TC_API_2.0_PS_120_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_121 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_121/TC_API_2.0_PS_121_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_121/TC_API_2.0_PS_121_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_121/TC_API_2.0_PS_121_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_122 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_122/TC_API_2.0_PS_122_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_122/TC_API_2.0_PS_122_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_122/TC_API_2.0_PS_122_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_123 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_123/TC_API_2.0_PS_123_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_123/TC_API_2.0_PS_123_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_123/TC_API_2.0_PS_123_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_126 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_126/TC_API_2.0_PS_126_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_126/TC_API_2.0_PS_126_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_126/TC_API_2.0_PS_126_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_127 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_127/TC_API_2.0_PS_127_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_127/TC_API_2.0_PS_127_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_127/TC_API_2.0_PS_127_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_128 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_128/TC_API_2.0_PS_128_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_128/TC_API_2.0_PS_128_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_128/TC_API_2.0_PS_128_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_129 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_129/TC_API_2.0_PS_129_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_129/TC_API_2.0_PS_129_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_129/TC_API_2.0_PS_129_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_130 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_130/TC_API_2.0_PS_130_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_130/TC_API_2.0_PS_130_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_130/TC_API_2.0_PS_130_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_133 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_133/TC_API_2.0_PS_133_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_133/TC_API_2.0_PS_133_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_133/TC_API_2.0_PS_133_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_134 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_134/TC_API_2.0_PS_134_POST_REQ.xml.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_134/TC_API_2.0_PS_134_POST_RES.xml.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_134/TC_API_2.0_PS_134_POST_RES.xml.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_135 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_135/TC_API_2.0_PS_135_POST_REQ.xml.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_135/TC_API_2.0_PS_135_POST_RES.xml.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_135/TC_API_2.0_PS_135_POST_RES.xml.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_136 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_136/TC_API_2.0_PS_136_POST_REQ.xml.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_136/TC_API_2.0_PS_136_POST_RES.xml.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_136/TC_API_2.0_PS_136_POST_RES.xml.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_request/TC_API_2.0_137 POST
    Given copy "qa_testcase/Evidence_POST_request/TC_API_2.0_137/TC_API_2.0_PS_137_POST_REQ.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # Then the status code of the HTTP response is "201"
    # And write the response body to stdout
    # And save the response body to "qa_testcase/Evidence_POST_request/TC_API_2.0_137/TC_API_2.0_PS_137_POST_RES.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_request/TC_API_2.0_137/TC_API_2.0_PS_137_POST_RES.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //@id |

  Scenario: Evidence_POST_response POST
    Given copy "qa_testcase/Evidence_POST_response/post-request-UNO-Full-Option.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    # And write the response body to stdout
    # Then the status code of the HTTP response is "201"
    # And save the response body to "qa_testcase/Evidence_POST_response/P-1-post-response.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_POST_response/P-1-post-response.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //bi:product-contract/bi:contract-id |
    | //@id |

  Scenario: Evidence_GET_response GET
    Given copy "qa_testcase/Evidence_GET_response/post-request-UNO-Full-Option.xml" to "tmp/post-sample.xml"
    And replace texts selected by "//cbe:client-request-id" with random UUID in "tmp/post-sample.xml"
    When send a "POST" request to "/api/v2/contract-information" with "tmp/post-sample.xml"
    And set "//bi:product-contract//cbe:object-id/text()" in response to "UUID" parameter
    And send a "GET" request to "/api/v2/contract-information/@{UUID}"
    # And write the response body to stdout
    # Then the status code of the HTTP response is "200"
    # And save the response body to "qa_testcase/Evidence_GET_response/P-2-get-response.xml"
    And the response body corresponds with the content of "qa_testcase/Evidence_GET_response/P-2-get-response.xml" excluding the XML elements selected by the following XPaths
    | XPATH                                                                                                                                            |
    | //cbe:object-id |
    | //cbe:link/cbe:href |
    | //cbe:xml-path/cbe:location-path |
    | //bi:contract-id |
    | //cbe:client-request-id |
    | //@id |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-created-date |
    | //contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:object-last-modified-date |
