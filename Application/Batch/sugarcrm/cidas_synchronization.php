<?php
require_once(BATCH_ROOT . '/include/helper/cidas_client.php');
require_once(BATCH_ROOT . '/include/helper/cidas_response.php');
require_once(BATCH_ROOT . '/include/helper/cidas_gcm_mapper.php');
require_once(BATCH_ROOT . '/include/persistence/sugarcrm_users.php');
require_once(BATCH_ROOT . '/include/persistence/sugarcrm_accounts.php');

/**
 * Class for synchronize corporate information from CIDAS to GCM(sugarcrm).
 * @author Satoshi Mamada
 */
class CidasSynchronization extends BatchBase {

	/** [Const] Database Name */
	const DB_NAME = 'sugarcrm';
	/** [Const] (SugarCRM)User Name for Batch */
	const USER_NAME_BATCH = 'SYS_GCM_BATCH';


	/** Database Connection(Master)  */
	private $sugarDbConnMst;

	/** Helper for inquire to CIDAS. */
	private $cidasClient;

	/**
	 * Constructor.
	 * Connect to Database 'sugarcrm' (Master)
	 * @param string $batchModuleName Name of batch module
	 */
	public function __construct($batchModuleName) {

		// Connect to Database(using batch configuration)
		global $batch_config;
		$dbConfig = $batch_config['db'];

		$this->sugarDbConnMst = connectToDatabase($dbConfig['host_name']['master'],
		                                          self::DB_NAME,
		                                          $dbConfig['sugarcrm']['user_name'],
		                                          $dbConfig['sugarcrm']['password']);
		$this->sugarDbConnMst->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$this->cidasClient = new CidasClient($batch_config['cidas']['url'],
		                                     $batch_config['cidas']['user'],
		                                     $batch_config['cidas']['passcode'],
		                                     $batch_config['cidas']['proxy_url']);

		// Calls parent's constructor
		parent::__construct($batchModuleName);
	}

	/**
	 * Checks arguments before performs batch module.
	 * @param string[] $args command-line arguments
	 * @return int check result - 0: No error / Non-0: Error
	 */
	protected function checkArguments($args) {
		// Nothing to do.
		return 0;
	}

	/**
	 * Performs batch module.
	 * @param string[] $args command-line arguments
	 * @return integer Result value. It will be return code of batch process.
	 */
	protected function perform($args) {
		global $logger;

		$existWarning = false;
		$logger->info(get_class($this).'.perform() - Start');

		// 1.Find id of the batch user to DB. (i.e. Checks that the batch user is exist.)
 		$daoUser = new SugarcrmUsers($this->sugarDbConnMst);
 		$batchUser = $daoUser->findByUserName(self::USER_NAME_BATCH);
		if(empty($batchUser)) {
			$logger->warn("User '".self::USER_NAME_BATCH."' is not exist - abort the sync.");
			return -1;
		}
 		$userId = $batchUser['id'];

		// 2.Search for corporate information of the synchronization object to be processed to the DB
		$daoAccount = new SugarcrmAccounts($this->sugarDbConnMst);
		$corporates = $daoAccount->searchWithValidCID();

		// 3.Synchronous processing
		foreach($corporates as $corporate) {
			$existWarning |= $this->synchronizeCorporateInformation($corporate, $userId);
		}

		$logger->info(get_class($this).'.perform() - End');

		return $existWarning ? 1 : 0;
	}


	/**
	 * Synchronize corporate information from CIDAS to DB.
	 *  1.Inquiry to CIDAS
	 *  2.Compare with the DB value
	 *  3.Update DB (if different)
	 * @param array[column=>value] $dbCorporate Acquired company information from DB
	 * @param string               $userId      UserID
	 * @return boolean true:WARNING occured / false:No WARNING
	 */
	private function synchronizeCorporateInformation($dbCorporate, $userId) {
		global $logger;

		// 1.Inquiry to CIDAS
		$cidasCorporate = $this->inquiryToCidas($dbCorporate['common_customer_id_c']);
		if(!$cidasCorporate) { // Couldn't get corporate info from CIDAS.
			return true;       // Aborts synchronous processing.
		}

		// 2.Compare with the DB value
		$mapper = new CidasGcmMapper($cidasCorporate);
		$cidasCorporateForCompare = $mapper->convertToGcmCorporate();
		$differences = $this->extractDifference($dbCorporate, $cidasCorporateForCompare, $mapper->getMappedFieldNames());
		if(empty($differences)) {
			$logger->info("CID '" . $dbCorporate['common_customer_id_c'] . "' has same values with DB and CIDAS -> Not update DB.");
			return false;
		}

		// 3.Updates corporate informations on DB.
		return $this->updateCorporate($dbCorporate['id'], $differences, $userId, $dbCorporate['common_customer_id_c']);
	}

	/**
	 * Inquiry corporate information to CIDAS.
	 * @param string $commonCustomerId CID
	 * @return mixed false|array
	 */
	private function inquiryToCidas($commonCustomerId) {
		global $logger;

		// Inquire to CIDAS with CID in all status.
		$responseXml = $this->cidasClient->inquireByCID($commonCustomerId, CidasClient::CHAR_CODE_UTF8, CidasClient::STATUS_CODE_ALL);

		// Checks response XML is exist.
		if(empty($responseXml)) {
			$logger->warn('Response from CIDAS was empty. CID=' . $commonCustomerId);
			return false;
		}
		// Checks contents of XML.
		$cidasResponse = new CidasResponse($responseXml);
		if($cidasResponse->hasError()) {
			$logger->warn('Response from CIDAS was error. CID=' . $commonCustomerId . ' ErrorCode=' . $cidasResponse->getErrorCode());
			return false;
		}
		if(intval($cidasResponse->getRecordCount()) < 1) {
			$logger->warn('Corporate Information was not found in CIDAS. CID=' . $commonCustomerId);
			return false;
		}

		// Extract Corporate Information to array
		$results = $cidasResponse->toArray();
		return $results[0]; // Uses first element only.(Because this response is searched by CID)
	}

	/**
	 * Extracts column that has different values.
	 * When comparing each element value of both, it includes the following conditions.
	 *  - NULL and empty is considered to be the same.
	 *  - Strip whitespace from the beginning and end of a value using trim() function.
	 * @param Associative array $dbCorporate    Acquired company information from DB
	 * @param Associative array $cidasCorporate Latest Company information from CIDAS
	 * @param             array $columnNames    Column names to compare
	 * @return Associative array[column=>array(OldValue, LatestValue)]
	 */
	private function extractDifference($dbCorporate, $cidasCorporate, $columnNames) {
		$results = array();

		foreach($columnNames as $columnName) {
			// Taking out a comparison value of DB
			$dbValue = $dbCorporate[$columnName];
			$dbValueForCompare = is_null($dbValue) ? '' : trim($dbValue);

			// Taking out a comparison value of CIDAS
			$cidasValue = array_key_exists($columnName, $cidasCorporate) ? $cidasCorporate[$columnName] : null;
			$cidasValueForCompare = is_null($cidasValue) ? '' : trim($cidasValue);

			// Compare
			if($dbValueForCompare == $cidasValueForCompare) {  // Values are same.
				continue;                                      // -> Do nothing.
			}

			// Extract differet values to results
			$results[$columnName] = array($dbValue, $cidasValue);
		}
		return $results;
	}

	/**
	 * Updates corporate informations on DB.
	 *  1.Updates accounts/accounts_cstm table.
	 *  2.Inserts accounts_audit table as ChangeLog
	 * @param string $primaryKey PrimaryKey (i.e. value of 'id' column)
	 * @param array  $differences Different Elements (See function extractDifference())
	 * @param string $userId UserID
	 * @param string $commonCustomerId CID (*Uses for the log message)
	 */
	private function updateCorporate($primaryKey, $differences, $userId, $commonCustomerId) {
		global $logger;

		$existWarning = false;
		try {
			// Creates DataAccessObject to update
			$daoAccount = new SugarcrmAccounts($this->sugarDbConnMst);

			// Keeps current time as update(modify) time
			$modifyTime = new DateTime();

			// Begin Transaction
			$this->sugarDbConnMst->beginTransaction();

			// Update accounts and accounts_cstm table
			$updateContents = array();
			foreach($differences as $key => $differenceValues) {
				$updateContents[$key] = $differenceValues[1];
			}
			$daoAccount->update($primaryKey, $updateContents, $userId, $modifyTime);

			// Insert accounts_audit table
			foreach($differences as $key => $differentValues) {
				$daoAccount->insertAudit($primaryKey, $key, $differentValues[0], $differentValues[1], $userId, $modifyTime);
			}

			// Commit
			$this->sugarDbConnMst->commit();

			$logger->info("CID '" . $commonCustomerId . "' has different values. -> Update Success");

		} catch(Exception $e) {
			// Rollback
			$this->sugarDbConnMst->rollBack();

			$logger->warn("CID '" . $commonCustomerId . "' has different values. But " . get_class($e) . " occured. -> Rollback");
			$existWarning = true;
		}
		return $existWarning;
	}
}
