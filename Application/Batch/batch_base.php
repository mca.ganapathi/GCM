<?php
/**
 * Base class for batch
 * @author Satoshi Mamada
 */
abstract class BatchBase {
	private $batchModuleName;

	/**
	 * Constructor.
	 * @param string $batchModuleName Name of batch module
	 */
	public function __construct($batchModuleName) {
		// Keep batchModuleName
		$this->batchModuleName = $batchModuleName;
	}

	/**
	 * Create file to exclude same batch process.
	 * @return int 0:success / minus:failed(already exist) / plus:failed(can't write file or directory)
	 */
	protected function getLock() {
		global $batch_config;
		// Checks - directory to store lock-file is exist.
		if(!is_dir($batch_config['lock_dir'])) { // Not Exist
			// Try to make directory (recursive)
			@mkdir($batch_config['lock_dir'], 0777, true);
			// ReCheck
			if(!is_dir($batch_config['lock_dir'])) {
				return 1; // Can't make directory to store lock-file.
			}
		}
		// Checks that lock-file is exist.
		$lockFile = $this->getLockFileName();
		if(is_file($lockFile)) { // Already exist
			return -1;
		}
		// Create(touch) lock-file.
		return touch($lockFile) ? 0 : 1;
	}

	/**
	 * Delete lock-file.
	 * @return void
	 */
	protected function releaseLock() {
		@unlink($this->getLockFileName());
	}

	private function getLockFileName() {
		global $batch_config;
		return  $batch_config['lock_dir']
		      . (substr($batch_config['lock_dir'], -1, 1) == '/' ? '.' : '/.')
		      . $this->batchModuleName;
	}

	/**
	 * Executes batch.
	 * @param  string[] $args
	 * @return int      return code of batch process
	 */
	public final function doBatch($args) {
		global $logger;

		// Checks argument
		$checkResult = $this->checkArguments($args);
		if($checkResult != 0) {
			$logger->error(get_class($this).'#checkArguments()');
			return $checkResult;
		}

		//  Get Lock
		if(($lockResult=$this->getLock()) != 0) {
			// Failed to get lock.
			$msg = get_class($this).'#getLock() - ';
			if($lockResult > 0) {
				$msg .= "Can't write lock file.";
			} else {
				$msg .= "Lock file is already exists.";
			}
			$logger->error($msg);
			return -1;
		}
		//  Performs batch procedure
		$performResult = $this->perform($args);

		//  After performed(=finally) - Release Lock
		$this->releaseLock();

		return $performResult;
	}

	/**
	 * Checks arguments before performs batch module.
	 * @param string[] $args command-line arguments
	 * @return int check result - 0: No error / Non-0: Error
	 */
	abstract protected function checkArguments($args);

	/**
	 * Performs batch module.
	 * @param string[] $args command-line arguments
	 * @return int Result value. It will be return code of batch process.
	 */
	abstract protected function perform($args);
}