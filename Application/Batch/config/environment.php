<?php
/**
 * Setting informations - depended on environment.
 */
// Common
$batch_config['lock_dir'] = '/var/log/batch/lock/';
$batch_config['db']['host_name']['master'] = '';
$batch_config['db']['host_name']['slave']  = '';

//// Running on Batch(Step) Server ////
// Database - Master(sugarcrm)
$batch_config['db']['sugarcrm_master']['user_name'] = '';
$batch_config['db']['sugarcrm_master']['password']  = '';
// Database - Slave(sugarcrm)
$batch_config['db']['sugarcrm_slave']['user_name']  = '';
$batch_config['db']['sugarcrm_slave']['password']   = '';
// Database - Master(apilog)
$batch_config['db']['apilog_master']['user_name']   = '';
$batch_config['db']['apilog_master']['password']    = '';
// Database - Slave(apilog)
$batch_config['db']['apilog_slave']['user_name']    = '';
$batch_config['db']['apilog_slave']['password']     = '';


//// Running on WebAP Server ////
// * Same values are defined at /var/www/html/config_environmets.php.

// Database - sugarcrm(Master)
$batch_config['db']['sugarcrm']['user_name'] = '';
$batch_config['db']['sugarcrm']['password']  = '';

// External system - CIDAS
$batch_config['cidas']['url'] = 'https://dev-gcm-stub/';
$batch_config['cidas']['user'] = 'GCMTEST';
$batch_config['cidas']['passcode'] = 'cidasapp';
$batch_config['cidas']['proxy_url'] = 'tcp://dev-gcm-proxy:8888';
