<?php
require_once('apilog_batch.php');

/**
 * Class to retracts the inspected apilog.
 * @author Satoshi Mamada
 */
class ApilogRetract extends ApilogBatch {

	/** [Const] Date format for argument.  */
	const DATE_FORMAT = 'Y-m-d';

	/** [Const] (Windows)Directory Separator. */
	const DIRECTORY_SEPARATOR_WINDOWS = "\\";

	private static function usage() {
		echo "Usage:\n";
		echo "php execute.php apilog.ApilogRetract YYYY-MM-DD PATH\n\n" ;
		echo "This batch outputs the apilog that checked_date is older than date of argument.\n\n" ;
		echo "*This script uses \"SELECT...INTO OUTFILE\" statement to output. So, \"PATH\" means on the DB(slave) server.\n";
		echo " And that should not be root directory.\n";
		echo "*Do not use a char'\\' as directory separator into \"PATH\" also on Windows.\n";
		return -1;
	}

	/**
	 * Checks arguments before performs batch module.
	 * @param string[] $args command-line arguments
	 * @return int check result - 0: No error / Non-0: Error
	 */
	protected function checkArguments($args) {
		// Checks the argument is date-string.
		if(count($args) !== 2) {
			return self::usage();
		}
		try {
			// Checks first argument is date-string.
			$argDate = DateTime::createFromFormat(self::DATE_FORMAT, $args[0]);
			if(!($argDate instanceof DateTime)) {
				return self::usage();
			}
			// Checks second argument is not root directory.
			$argDir = trim($args[1]);
			if(strlen($argDir) < 2) {  // Invalid directory.
				return self::usage();
			}
			if(strpos($argDir, self::DIRECTORY_SEPARATOR_WINDOWS) !== false) {
				return self::usage();
			}
		} catch(Exception $e) {
			// Exception occured in some reason.
			return self::usage();
		}
		// Check OK
		return 0;
	}

	/**
	 * Performs batch module.
	 * @param string[] $args command-line arguments
	 * @return integer Result value. It will be return code of batch process.
	 */
	protected function perform($args) {
		global $logger;

		$boundaryDate = $args[0];
		$boundaryDateTime = DateTime::createFromFormat(self::DATE_FORMAT, $boundaryDate)->setTime(0, 0, 0)->format('Y-m-d H:i:s');
		$retractTime = (new DateTime('now', new DateTimeZone('UTC')))->format('YmdHis');

		$directoryRetract = endsWith($args[1], '/') ? $args[1] : $args[1] . '/';

		$result = 0;
		// Get list of table names in 'apilog'
		$logTableNames = parent::getLogTableNames();
		// Checks for each tables
		$logger->info(get_class($this).".perform() Retract Start - Target: checked_datetime is older than '" . $boundaryDateTime . "'");
		foreach($logTableNames as $logTableName) {
			try {
				$this->retractLog($logTableName, $boundaryDate, $boundaryDateTime, $retractTime, $directoryRetract);
			} catch(Exception $e) {
				$logger->error($e);
				$result = -1;
			}
		}
		$logger->info(get_class($this).'.perform() Retract End');
		return $result;
	}


	private function retractLog($logTableName, $boundaryDate, $boundaryDateTime, $retractTime, $directoryRetract) {
		global $logger;
		// Retract(SELECT INTO OUTFILE...)
		$sql  = 'SELECT * INTO OUTFILE "'. $directoryRetract.'Before'.$boundaryDate.'_apilog.'.$logTableName.'.'.$retractTime.'.dat" FROM `' . $logTableName . '`';
		$sql .= " WHERE checked <> :checked";
		$sql .= "   AND check_datetime < STR_TO_DATE(:check_datetime, '%Y-%m-%d %H:%i:%s')";

		$stmt = $this->apiDbConnSlv->prepare($sql);
		$stmt->bindValue(':checked',        parent::CHKSTAT_UNCHECKED, PDO::PARAM_STR);
		$stmt->bindValue(':check_datetime', $boundaryDateTime,         PDO::PARAM_STR);
		$stmt->execute();
		$stmt->closeCursor();
		$logger->info(' Retract apilog `' . $logTableName . '` : Done.');
	}
}