<?php
require_once('apilog_batch.php');

/**
 * Class to optimize tables in apilog database.
 * @author Satoshi Mamada
 */
class ApilogOptimize extends ApilogBatch {

	/**
	 * Checks arguments before performs batch module.
	 * @param string[] $args command-line arguments
	 * @return int check result - 0: No error / Non-0: Error
	 */
	protected function checkArguments($args) {
		// No argument -> No check
		return 0;
	}

	/**
	 * Performs batch module.
	 * @param string[] $args command-line arguments
	 * @return integer Result value. It will be return code of batch process.
	 */
	protected function perform($args) {
		global $logger;

		$result = 0;
		// Get list of table names in 'apilog'
		$logTableNames = parent::getLogTableNames();
		// Checks for each tables
		foreach($logTableNames as $logTableName) {
			$logger->info(get_class($this).'.perform() Optimize Start - ' . $logTableName);
			try {
				$this->optimizeTable($logTableName);
			} catch(Exception $e) {
				$logger->error($e);
				$result = -1;
			}
			$logger->info(get_class($this).'.perform() Optimize End - ' . $logTableName);
		}
		return $result;
	}

	private function optimizeTable($logTableName) {
		global $logger;
		// Optimize table
		$stmt = $this->apiDbConnMst->query('OPTIMIZE TABLE `' . $logTableName . '`');
		$i=0;
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$keyValues = array();
			foreach($row as $key => $value) {
				$keyValues[]  = $key . '=' . $value;
			}
			$logger->info(' Result[' . ++$i. '] - ' . join($keyValues, ' | '));
		}
	}
}