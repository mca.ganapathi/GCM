<?php
/**
 * Inspect '/api/v2/contract-information' API log.
 * <Inspected processing>
 *  - POST  -> Http Response Status = 201
 *  - PATCH -> Http Response Status = 200
 *  - GET   -> Http Response Status = 200
 * @author Satoshi Mamada
 */
class ApiV2ContractInformationInspector extends DefaultInspector {

	/** [Const] Countory Code - Japan */
	const COUNTRY_CODE_JPN = '392';
	/** [Const] Countory Code - United States */
	const COUNTRY_CODE_USA = '840';
	/** [Const] Countory Code - Canada */
	const COUNTRY_CODE_CAN = '124';

	/** [Const] Path of self */
	const REST_PATH_SELF   = '/api/v2/contract-information';
	const REST_PATH_SEARCH = '/api/v2/contract-information/search';


	const PARAM_KEY_GCID   = 'bi:contract-id';
	const PARAM_KEY_OBJVER = 'cbe:object-version';
	const PARAM_KEY_STATUS = 'bi:interaction-status';

	/**
	 * Inspection process - GET request was succeed.
	 * To check whether or not there is fraud in the response XML
	 * @return int Check Status
	 */
	protected function inspectGet200() {
		global $logger;
		$logger->info(get_class($this).'#'.__FUNCTION__.'() Start');
		try {
			// Parse response XML
			$sxe = new SimpleXMLElement($this->response_body);
		}
		catch (Exception $e) {
			// Parse Error occured.
			$logger->warn(get_class($this).'#'.__FUNCTION__.'() End - CheckStatus: '.ApilogBatch::CHKSTAT_ABNORMAL);
			return ApilogBatch::CHKSTAT_ABNORMAL; // Check NG
		}
		$logger->info(get_class($this).'#'.__FUNCTION__.'() End - CheckStatus: '.ApilogBatch::CHKSTAT_CHECKED);
		return ApilogBatch::CHKSTAT_CHECKED;      // Check OK
	}

	/**
	 * Inspection process - POST request was succeed.
	 * @return int Check status
	 */
	protected function inspectPost201() {
		global $logger;
		$logger->info(get_class($this).'#'.__FUNCTION__.'() Start');

		// Initialize variables to pass parameters
		$objectId = $globalContractId = $versionNumber = '';

/* ===	// Step1. Judge the request is 'New' or 'VersionUp' (i.e. Parameter is exist or not)

		// Extracts request parameters
		if(endsWith($this->path, self::REST_PATH_SEARCH)) { // contract-information/search?key=value&key2=value2...
			$globalContractId  = array_key_exists(self::PARAM_KEY_GCID,   $_REQUEST) ? $_REQUEST[self::PARAM_KEY_GCID]   : '';
			$versionNumber     = array_key_exists(self::PARAM_KEY_OBJVER, $_REQUEST) ? $_REQUEST[self::PARAM_KEY_OBJVER] : '';
		}
		else {  // contract-information/[object-id]:[object-version]
			$params = $this->extractParameterInRequestPath();
			$objectId      = $params[0];
			$versionNumber = $params[1];
		}

		// Tentative implementation -> response is empty(=not XML). So skip inspection.
		if(!empty($objectId) || !empty($globalContractId)  || !empty($versionNumber)) {
			return ApilogBatch::CHKSTAT_SKIPPED;
		}                                                                                           === */

		// Step2. Picks up Contract Object-ID and Version from Response XML
		$contractObjectExtId = $contractVersion = '';
		try {
			$sxeResponse = new SimpleXMLElement($this->response_body);

			$contractObjectExtId = getElementValueByXpath($sxeResponse, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:identified-by/bi:product-contract-key/cbe:key-type-for-cbe-standard/cbe:object-id');
			$contractGlobalId = getElementValueByXpath($sxeResponse, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/bi:contract-id');
			$contractVersion     = getElementValueByXpath($sxeResponse, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:identified-by/bi:product-contract-key/cbe:key-type-for-cbe-standard/cbe:object-version');
		} catch(Exception $e) {
			$logger->error("Cannot pick up Contract-ObjectID from response XML (Exception occured)", $e);
		}
		if(empty($contractObjectExtId) || empty($contractVersion)) {
			$logger->warn("Contract-ObjectID or Version is empty in response XML - abort to inspect");
			return ApilogBatch::CHKSTAT_ABNORMAL;
		}
		$logger->info(' Contract-ObjectID(External)=' . $contractObjectExtId . ' / Contract-Version=' . $contractVersion);
		$logger->info(' Global Contract ID=' . $contractGlobalId . ' / Contract-Version=' . $contractVersion);

		// Step3. Parse Request XML
		// * Not enclose try-catch. Because if invalid xml was saved to apilog, then that should be returns error.
		$sxe = new SimpleXMLElement($this->request_body);

/* ===	// Step4. Checks that contents of Request XML and DB stored Data are same or not.
		if(empty($objectId)) {                                                                             === */
			$chkResult = $this->verifyPostNewInformationAndDB($sxe, $contractObjectExtId, $contractVersion);
/* ===	} else {
			$chkResult = $this->verifyPostVupInformationAndDB($sxe, $contractObjectExtId, $contractVersion);
		}                                                                                                  === */

		$logLevel = ($chkResult==ApilogBatch::CHKSTAT_CHECKED ? 'info' : 'warn');
		$logger->$logLevel(get_class($this).'#'.__FUNCTION__.'() End - CheckStatus: '.$chkResult);
		return $chkResult;
	}
/* ===
	private function verifyPostVupInformationAndDB($sxe, $contractObjectExtId, $contractVersion) {
		return $this->verifyPostNewInformationAndDB($sxe,$contractObjectExtId, $contractVersion);
	}                                                                                                      === */

	private function verifyPostNewInformationAndDB($sxe, $contractObjectExtId, $contractVersion) {
		global $logger;
		$existUnmatch = false;

		//==== [Read-DB] Read tables from application database(sugarcrm) using Contract's ID ====

		// gc_contracts Table (Base for all verification)
		$gc_contract = $this->searchByParams(
			"SELECT * FROM gc_contracts WHERE global_contract_id_uuid=? AND contract_version=? AND deleted=0"
		   , array($contractObjectExtId, $contractVersion));

		$contractObjectId = $gc_contract['id'];
		if(empty($contractObjectId)) {
			$logger->warn('  Record not exist in gc_contracts table. => Aborts verifying.');
			return ApilogBatch::CHKSTAT_ABNORMAL;
		}

		// accounts Table(i.e. Corporate Module) of Contract
		$accountOfContract = $this->searchById(
			"SELECT ac.*, acc.* FROM accounts ac, accounts_cstm acc WHERE ac.id=acc.id_c AND ac.deleted=0 AND ac.id=(SELECT rel.accounts_gc_contracts_1accounts_ida FROM accounts_gc_contracts_1_c rel WHERE rel.accounts_gc_contracts_1gc_contracts_idb=? AND rel.deleted=0)"
		   , $contractObjectId);

		// contacts Table of Contract(i.e. contacts_cstm.contact_type_c='Contract')
		$contactOfContract = $this->searchById(
			"SELECT ct.*, ctc.* FROM contacts ct, contacts_cstm ctc WHERE ct.id=ctc.id_c AND ct.deleted=0 AND ct.id=(SELECT rel.contacts_gc_contracts_1contacts_ida FROM contacts_gc_contracts_1_c rel WHERE rel.contacts_gc_contracts_1gc_contracts_idb=? AND rel.deleted=0) AND ctc.contact_type_c='Contract'"
		   , $contractObjectId);

		// email_address Table of Contract's contact
		$emailAddressOfContract = $this->searchById(
			"SELECT * FROM email_addresses WHERE deleted=0 AND id=(SELECT rel.email_address_id FROM email_addr_bean_rel rel WHERE rel.bean_id=? AND rel.deleted=0)"
		   , $contactOfContract['id']);

		// c_country Table of Contracst's contact
		$countryOfContract = $this->searchById(
			"SELECT * FROM c_country WHERE deleted=0 AND id=?"
		   , $contactOfContract['c_country_id_c']);

		// gc_line_item_contract_history Table
		$gcLineItemContractHistory = $this->searchById(
			"SELECT * FROM gc_line_item_contract_history WHERE deleted=0 AND contracts_id=?"
		   , $contractObjectId);

		// contacts Table of Billing(i.e. contacts_cstm.contact_type_c='Billing')
		$contactOfBilling = $this->searchById(
			"SELECT ct.*, ctc.* FROM contacts ct, contacts_cstm ctc WHERE ct.id=ctc.id_c AND ct.deleted=0 AND ct.id=? AND ctc.contact_type_c='Billing'"
		   , $gcLineItemContractHistory['bill_account_id']);

		// accounts Table(i.e. Corporate Module) of Billing
		$accountOfBilling = $this->searchById(
			"SELECT ac.*, acc.* FROM accounts ac, accounts_cstm acc WHERE ac.id=acc.id_c AND ac.deleted=0 AND ac.id=(SELECT rel.account_id FROM accounts_contacts rel WHERE rel.contact_id=? AND rel.deleted=0)"
		   , $contactOfBilling['id']);

		// c_country Table of Billing's contact
		$countryOfBilling = $this->searchById(
			"SELECT * FROM c_country WHERE deleted=0 AND id=?"
		   , $contactOfBilling['c_country_id_c']);

		// email_address Table of Billing contact
		$emailAddressOfBilling = $this->searchById(
			"SELECT * FROM email_addresses WHERE deleted=0 AND id=(SELECT rel.email_address_id FROM email_addr_bean_rel rel WHERE rel.bean_id=? AND rel.deleted=0)"
		   , $contactOfBilling['id']);

		// gc_salesrepresentative Table
		$gc_salesrepresentative = $this->searchById(
			"SELECT * FROM gc_salesrepresentative WHERE deleted=0 AND contracts_id=?"
		   , $contractObjectId);


		//==== [Verify] Compares data in XML and data in database ====

		// CorporateID of Contract - 共通顧客ID
		$corporateId_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:corporation/gcm:corporation-id');
		$existUnmatch |= self::verifyWithLog('CorporateID of Contract', $corporateId_Of_Contract, $accountOfContract['common_customer_id_c']);

		// Contract Name
		$contractName = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:name');
		$existUnmatch |= self::verifyWithLog('Contract Name', $contractName, $gc_contract['name']);

		// Contract Description
		$contractDescription = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:description');
		$existUnmatch |= self::verifyWithLog('Contract Description', $contractDescription, $gc_contract['description']);

		// Division (Local Character) - 契約者部課名
		$divisionLocal_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:person-in-charge/gcm:division/gcm:name');
		$existUnmatch |= self::verifyWithLog('Division (Local Character)', $divisionLocal_Contact_Of_Contract, $contactOfContract['division_2_c']);

		// Contact Person Name (Local Character) - 契約者担当者名
		$personNameLocal_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:person-in-charge/gcm:individual/gcm:name');
		$existUnmatch |= self::verifyWithLog('Contact Person Name (Local Character)', $personNameLocal_Contact_Of_Contract, $contactOfContract['name_2_c']);

		// Telephone number - 契約者電話番号
		$telephoneNumber_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:person-in-charge/gcm:contact-medium/gcm:telephone-number');
		$existUnmatch |= self::verifyWithLog('Telephone number', $telephoneNumber_Contact_Of_Contract, $contactOfContract['phone_work']);

		// Email address - 契約者メールアドレス
		$emailAddress_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:person-in-charge/gcm:contact-medium/gcm:email-address');
		$existUnmatch |= self::verifyWithLog('Email address', $emailAddress_Contact_Of_Contract, $emailAddressOfContract['email_address']);

		// VAT Number - 契約企業のVAT
		$vatNo = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:corporation/gcm:vat-no');
		$existUnmatch |= self::verifyWithLog('VAT Number', $vatNo, $gc_contract['vat_no']);

		// Contact Person Name (Latin Character)
		$personNameLatin_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:person-in-charge/gcm:division/gcm:name-latin');
		$existUnmatch |= self::verifyWithLog('Contact Person Name (Latin Character)', $personNameLatin_Contact_Of_Contract, $contactOfContract['last_name']);

		// Division (Latin Character)
		$divisionLatin_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/contract-info:contract-customer/contract-info:person-in-charge/contract-info:division/contract-info:name-latin');
		$existUnmatch |= self::verifyWithLog('Division (Latin Character)', $divisionLatin_Contact_Of_Contract, $contactOfContract['division_1_c']);

		// FAX number
		$faxNumber_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:person-in-charge/gcm:contact-medium/gcm:fax-number');
		$existUnmatch |= self::verifyWithLog('FAX number', $faxNumber_Contact_Of_Contract, $contactOfContract['phone_fax']);

		// Country
		$country_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/location:iso3166-1numeric');
		$existUnmatch |= self::verifyWithLog('Country', $country_Contact_Of_Contract, $countryOfContract['country_code_numeric']);

		switch ($country_Contact_Of_Contract) {
		  case self::COUNTRY_CODE_JPN:		// Japan
			// Contract Postal Number
			$postalNumber_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:japanese-property-address/gcm:postal-code');
			$existUnmatch |= self::verifyWithLog('Contract Postal Number', $postalNumber_Contact_Of_Contract, $contactOfContract['postal_no_c']);

			//==== [Read-DB] Japan Specific Address ====
			$japanSpecificAddress = $this->searchById(
				"SELECT * FROM js_accounts_js WHERE deleted=0 AND contact_id_c=?"
			   , $contactOfContract['id']);

			// Contract Address 1 [Japan specific]
			$jsAddress1_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:japanese-property-address/gcm:address');
			$existUnmatch |= self::verifyWithLog('Contract Address 1 [Japan specific]', $jsAddress1_Contact_Of_Contract, $japanSpecificAddress['addr1']);

			// Contract Address 2 [Japan specific]
			$jsAddress2_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:japanese-property-address/gcm:address-line');
			$existUnmatch |= self::verifyWithLog('Contract Address 2 [Japan specific]', $jsAddress2_Contact_Of_Contract, $japanSpecificAddress['addr2']);

			break;

		  case self::COUNTRY_CODE_USA:		// United States
			// Contract Address [US Specific]
			$addressLine_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:american-property-address/gcm:address-line');
			$existUnmatch |= self::verifyWithLog('Contract Address [US Specific]', $addressLine_Contact_Of_Contract, $contactOfContract['addr_general_c']);

			// Contract City [US Specific]
			$city_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:american-property-address/gcm:city');
			$existUnmatch |= self::verifyWithLog('Contract City [US Specific]', $city_Contact_Of_Contract, $contactOfContract['city_general_c']);
			
			// Contract Attention Line [US Specific]
			$attentionLine_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:american-property-address/gcm:attention-line');
			$existUnmatch |= self::verifyWithLog('Contract Attention Line [US Specific]', $attentionLine_Contact_Of_Contract, $contactOfContract['attention_line_c']);
				
			//==== [Read-DB] State ====
			$c_state = $this->searchById(
					"SELECT * FROM c_state WHERE deleted=0 AND id=?"
					, $contactOfContract['c_state_id_c']);

			// Contract State [US Specific]
			$stateAbbr_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:american-property-address/gcm:state-abbr');
			$existUnmatch |= self::verifyWithLog('Contract State [US Specific]', $stateAbbr_Contact_Of_Contract, $c_state['state_code']);

			// Contract Postal Number
			$zipCode_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:american-property-address/gcm:zip-code');
			$existUnmatch |= self::verifyWithLog('Contract Postal Number', $zipCode_Contact_Of_Contract, $contactOfContract['postal_no_c']);

			break;

		  case self::COUNTRY_CODE_CAN:		// Canada
			// Contract Address [Canada Specific]
			$addressLine_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:canadian-property-address/gcm:address-line');
			$existUnmatch |= self::verifyWithLog('Contract Address [Canada Specific]', $addressLine_Contact_Of_Contract, $contactOfContract['addr_general_c']);

			// Contract City [Canada Specific]
			$city_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:canadian-property-address/gcm:city');
			$existUnmatch |= self::verifyWithLog('Contract City [Canada Specific]', $city_Contact_Of_Contract, $contactOfContract['city_general_c']);

			// Contract Attention Line [Canada Specific]
			$attentionLine_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:canadian-property-address/gcm:attention-line');
			$existUnmatch |= self::verifyWithLog('Contract Attention Line [Canada Specific]', $attentionLine_Contact_Of_Contract, $contactOfContract['attention_line_c']);
			
			//==== [Read-DB] State ====
			$c_state = $this->searchById(
					"SELECT * FROM c_state WHERE deleted=0 AND id=?"
					, $contactOfContract['c_state_id_c']);

			// Contract State [Canada Specific]
			$stateAbbr_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:canadian-property-address/gcm:state-abbr');
			$existUnmatch |= self::verifyWithLog('Contract State [Canada Specific]', $stateAbbr_Contact_Of_Contract, $c_state['state_code']);

			// Contract Postal Number
			$postalCode_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:canadian-property-address/gcm:postal-code');
			$existUnmatch |= self::verifyWithLog('Contract Postal Number', $postalCode_Contact_Of_Contract, $contactOfContract['postal_no_c']);

			break;

		  default:							// Other
		  	// Contract Address (Local Character)
			$address_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:any-address/gcm:address');
			$existUnmatch |= self::verifyWithLog('Contract Address (Local Character) [ANY]', $address_Contact_Of_Contract, $contactOfContract['addr_2_c']);

			// Contract Attention Line (Any)
			$attentionLine_Contact_Of_Contract = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:contract-customer/gcm:address/gcm:any-address/gcm:attention-line');
			$existUnmatch |= self::verifyWithLog('Contract Attention Line [ANY]', $attentionLine_Contact_Of_Contract, $contactOfContract['attention_line_c']);
			
		  	break;
		}

		// Billing Corporate CID - 請求先企業のCorporateID
		$corporateId_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:corporation/gcm:corporation-id');
		if(!empty($corporateId_Of_Billing)) {
			$existUnmatch |= self::verifyWithLog('Billing Corporate (CorporateID)', $corporateId_Of_Billing, $accountOfBilling['common_customer_id_c']);

		} else { // 請求先企業のCorporateID不明(空) -> gcm:standard-corporation-name 配下の要素で名称を比較
			// Billing Corporate Name (Local Character 2) - 送付先カナ
			$billingNameKana = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:corporation/gcm:standard-corporation-name/gcm:name-furigana');
			$existUnmatch |= self::verifyWithLog('Billing Corporate Name (Local Character 2)', $billingNameKana, $accountOfBilling['name_3_c']);

			// Billing Corporate Name (Local Character 1) - 送付先名
			$billingName = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:corporation/gcm:standard-corporation-name/gcm:name');
			$existUnmatch |= self::verifyWithLog('Billing Corporate Name (Local Character 1)', $billingName, $accountOfBilling['name_2_c']);

			// Billing Corporate Name (Latin Character) - 送付先名(英名)
			$billingNameLatin = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:corporation/gcm:standard-corporation-name/gcm:name-latin');
			$existUnmatch |= self::verifyWithLog('Billing Corporate Name (Latin Character)', $billingNameLatin, $accountOfBilling['name'], 'Not Available');
		}

		// Country of Billing contact
		$country_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/location:iso3166-1numeric');
		$existUnmatch |= self::verifyWithLog('Billing Country', $country_Contact_Of_Billing, $countryOfBilling['country_code_numeric']);

		switch ($country_Contact_Of_Billing) {
		  case self::COUNTRY_CODE_JPN:		// Japan
			// Billing Postal Number - 送付先郵便番号
			$postalNumber_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:japanese-property-address/gcm:postal-code');
			$existUnmatch |= self::verifyWithLog('Billing Postal Number', $postalNumber_Contact_Of_Billing, $contactOfBilling['postal_no_c']);

			//==== [Read-DB] Japan Specific Address ====
			$japanSpecificAddress = $this->searchById(
				"SELECT * FROM js_accounts_js WHERE deleted=0 AND contact_id_c=?"
			   , $contactOfBilling['id']);

			// Billing Address 1 [Japan specific] - 送付先住所
			$jsAddress1_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:japanese-property-address/gcm:address');
			$existUnmatch |= self::verifyWithLog('Billing Address 1 [Japan specific]', $jsAddress1_Contact_Of_Billing, $japanSpecificAddress['addr1']);

			// Billing Address 2 [Japan specific] - 送付先番地等
			$jsAddress2_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:japanese-property-address/gcm:address-line');
			$existUnmatch |= self::verifyWithLog('Billing Address 2 [Japan specific]', $jsAddress2_Contact_Of_Billing, $japanSpecificAddress['addr2']);
			break;
		  case self::COUNTRY_CODE_USA:		// United States
		    // Billing Address [US Specific]
			$addressLine_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:american-property-address/gcm:address-line');
			$existUnmatch |= self::verifyWithLog('Billing Address [US Specific]', $addressLine_Contact_Of_Billing, $contactOfBilling['addr_general_c']);

			// Billing City [US Specific]
			$city_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:american-property-address/gcm:city');
			$existUnmatch |= self::verifyWithLog('Billing City [US Specific]', $city_Contact_Of_Billing, $contactOfBilling['city_general_c']);

		    // Attention Line [US Specific]
			$attentionLine_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:american-property-address/gcm:attention-line');
			$existUnmatch |= self::verifyWithLog('Billing Attention Line [US Specific]', $attentionLine_Contact_Of_Billing, $contactOfBilling['attention_line_c']);
			
			//==== [Read-DB] State ====
			$c_state = $this->searchById(
					"SELECT state_code FROM c_state WHERE deleted=0 AND id=?"
					, $contactOfBilling['c_state_id_c']);

			// Billing State [US Specific]
			$stateAbbr_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:american-property-address/gcm:state-abbr');
			$existUnmatch |= self::verifyWithLog('Billing State [US Specific]', $stateAbbr_Contact_Of_Billing, $c_state['state_code']);

			// Billing Postal Number
			$zipCode_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:american-property-address/gcm:zip-code');
			$existUnmatch |= self::verifyWithLog('Billing Postal Number', $zipCode_Contact_Of_Billing, $contactOfBilling['postal_no_c']);

		    break;
		  case self::COUNTRY_CODE_CAN:		// Canada
		    // Billing Address [Canada Specific]
			$addressLine_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:canadian-property-address/gcm:address-line');
			$existUnmatch |= self::verifyWithLog('Billing Address [Canada Specific]', $addressLine_Contact_Of_Billing, $contactOfBilling['addr_general_c']);

			// Billing City [Canada Specific]
			$city_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:canadian-property-address/gcm:city');
			$existUnmatch |= self::verifyWithLog('Billing City [Canada Specific]', $city_Contact_Of_Billing, $contactOfBilling['city_general_c']);

		    // Billing Attention Line [Canada Specific]
			$attentionLine_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:canadian-property-address/gcm:attention-line');
			$existUnmatch |= self::verifyWithLog('Billing Attention Line [Canada Specific]', $attentionLine_Contact_Of_Billing, $contactOfBilling['attention_line_c']);
		    //==== [Read-DB] State ====
			$c_state = $this->searchById(
					"SELECT state_code FROM c_state WHERE deleted=0 AND id=?"
					, $contactOfBilling['c_state_id_c']);

			// Billing State [Canada Specific]
			$stateAbbr_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:canadian-property-address/gcm:state-abbr');
			$existUnmatch |= self::verifyWithLog('Billing State [Canada Specific]', $stateAbbr_Contact_Of_Billing, $c_state['state_code']);

			// Billing Postal Number
			$postalCode_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:canadian-property-address/gcm:postal-code');
			$existUnmatch |= self::verifyWithLog('Billing Postal Number', $postalCode_Contact_Of_Billing, $contactOfBilling['postal_no_c']);

			break;
		  default:							// Other
		    // Billing Address (Local Character)
			$address_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:any-address/gcm:address');
			$existUnmatch |= self::verifyWithLog('Billing Address (Local Character) [ANY]', $address_Contact_Of_Billing, $contactOfBilling['addr_2_c']);

		    // Billing Attention Line (Any)
			$attentionLine_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:address/gcm:any-address/gcm:attention-line');
			$existUnmatch |= self::verifyWithLog('Billing Attention Line [ANY]', $attentionLine_Contact_Of_Billing, $contactOfBilling['attention_line_c']);
		    break;
		}
		
		$code1 = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-affiliate/gcm:organization-code/gcm:code1');
		$code2 = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-affiliate/gcm:organization-code/gcm:code2');
		if(!empty($code1)) {
			if(!empty($code2)) {
				//==== [Read-DB] gc_organization ====
				$gc_organization = $this->searchByParams(
						"SELECT id FROM gc_organization WHERE deleted=0 AND organization_code_1=? AND organization_code_2=?"
						, array($code1, $code2));
			} else {
				//==== [Read-DB] gc_organization ====
				$gc_organization = $this->searchById(
						"SELECT id FROM gc_organization WHERE deleted=0 AND organization_code_1=?"
						, $code1);
			}
			$existUnmatch |= self::verifyWithLog('Billing Affiliate', $gc_organization['id'], $gcLineItemContractHistory['gc_organization_id_c']);
		}

		// Billing Division (Local Character) - 送付先部課名
		$divisionLocal_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:person-in-charge/gcm:division/gcm:name');
		$existUnmatch |= self::verifyWithLog('Billing Division (Local Character)', $divisionLocal_Contact_Of_Billing, $contactOfBilling['division_2_c']);

		// Billing Person Name (Local Character) - 送付先担当者名
		$personNameLocal_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:person-in-charge/gcm:individual/gcm:name');
		$existUnmatch |= self::verifyWithLog('Billing Person Name (Local Character)', $personNameLocal_Contact_Of_Billing, $contactOfBilling['name_2_c']);

		// Billing Telephone number - 送付先電話番号
		$telephoneNumber_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:person-in-charge/gcm:contact-medium/gcm:telephone-number');
		$existUnmatch |= self::verifyWithLog('Billing Telephone number', $telephoneNumber_Contact_Of_Billing, $contactOfBilling['phone_work']);

		// Billing Email address - 送付先メールアドレス
		$emailAddress_Contact_Of_Billing = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:billing-customer/gcm:person-in-charge/gcm:contact-medium/gcm:email-address');
		$existUnmatch |= self::verifyWithLog('Billing Email address', $emailAddress_Contact_Of_Billing, $emailAddressOfBilling['email_address']);

		// Sales Channel Code - 販売チャネルコード
		$salesChannelCode = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:sales-channel/gcm:corporation/gcm:sales-channel-code');
		$existUnmatch |= self::verifyWithLog('Sales Channel Code', $salesChannelCode, $gc_salesrepresentative['sales_channel_code']);

		// Sales Representative Name (Local Character) - 販売担当者名
		$salesRepresentativeName = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:sales-channel/gcm:person-in-charge/gcm:individual/gcm:name');
		$existUnmatch |= self::verifyWithLog('Sales Representative Name (Local Character)', $salesRepresentativeName, $gc_salesrepresentative['sales_rep_name_1']);

		// Sales Representative) Division (Local Character) - 販売担当者所属部課名
		$salesDivision = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:sales-channel/gcm:person-in-charge/gcm:division/gcm:name');
		$existUnmatch |= self::verifyWithLog('Sales Representative Division (Local Character)', $salesDivision, $gc_salesrepresentative['division_2']);

		// Sales Representative) Telephone number - 販売担当者電話番号
		$telephoneNumber_Contact_Of_Sales = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:sales-channel/gcm:person-in-charge/gcm:contact-medium/gcm:telephone-number');
		$existUnmatch |= self::verifyWithLog('Sales Telephone number', $telephoneNumber_Contact_Of_Sales, $gc_salesrepresentative['tel_no']);

		// Sales Representative) Email address - 販売担当者メールアドレス
		$emailAddress_Contact_Of_Sales = getElementValueByXpath($sxe, '/contract-info:contract-information/bi:product-contracts/bi:product-contract/cbe:expansions/gcm:expansion-of-product-contract-by-gcm/gcm:sales-channel/gcm:person-in-charge/gcm:contact-medium/gcm:email-address');
		$existUnmatch |= self::verifyWithLog('Sales Email address', $emailAddress_Contact_Of_Sales, $gc_salesrepresentative['email1']);
/*  Not implemented yet.
		// Service Start Date
		$serviceStartDate = getElementValueByXpath($sxe, '/req-v0:request/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:servicePeriod/prodCntrctItem0001:serviceStartDate/prodCntrctItem0001:characteristicValue/prodCntrctItem0001:serviceStartDate');
		$existUnmatch |= self::verifyWithLog('Service Start Date', self::convertDate($serviceStartDate), $gcLineItemContractHistory['service_start_date']);

		// Service End Date
		$serviceEndDate = getElementValueByXpath($sxe, '/req-v0:request/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:servicePeriod/prodCntrctItem0001:serviceEndDate/prodCntrctItem0001:characteristicValue/prodCntrctItem0001:serviceEndDate');
		$existUnmatch |= self::verifyWithLog('Service End Date', self::convertDate($serviceEndDate), $gcLineItemContractHistory['service_end_date']);

		// Billing Start Date (利用開始日)
		$billingStartDate = getElementValueByXpath($sxe, '/req-v0:request/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:billingPeriod/prodCntrctItem0001:billingStartDate/prodCntrctItem0001:characteristicValue/prodCntrctItem0001:billingStartDate');
		$existUnmatch |= self::verifyWithLog('Billing Start Date', self::convertDate($billingStartDate), $gcLineItemContractHistory['billing_start_date']);

		// Billing End Date
		$billingEndDate = getElementValueByXpath($sxe, '/req-v0:request/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:billingPeriod/prodCntrctItem0001:billingEndDate/prodCntrctItem0001:characteristicValue/prodCntrctItem0001:billingEndDate');
		$existUnmatch |= self::verifyWithLog('Billing End Date', self::convertDate($billingEndDate), $gcLineItemContractHistory['billing_end_date']);
*/
		return $existUnmatch ? ApilogBatch::CHKSTAT_ABNORMAL : ApilogBatch::CHKSTAT_CHECKED;
	}

	private static function convertDate($dateInXml) {
		$result = $dateInXml;
		try {
			$objDateTime = DateTime::createFromFormat(DateTime::RFC3339, $dateInXml);
			if($objDateTime instanceof DateTime) {
				$result = $objDateTime->format('Y-m-d H:i:s');
			}
		} catch(Exception $e) {} // Do Nothing.

		return $result;
	}

	/**
	 * To verify data with the log output.
	 * @param string $itemName Name of item to veriry
	 * @param string $xmlData  XML data for comparison
	 * @param string $dbData   DB data for comparison
	 * @param string $defaultValue (Optional) Defult value set by the application when XML data was empty.
	 * @return boolean true: unmatch / false: match(=No Problem)
	 */
	private static function verifyWithLog($itemName, $xmlData, $dbData, $defaultValue=null) {
		$unmatch = false;

		global $logger;
		if($xmlData == $dbData) {
			$logger->info(' Verify - '.$itemName.' => OK');
		} else if(!is_null($defaultValue) && empty($xmlData) && $dbData == $defaultValue) {
			$logger->info(' Verify - '.$itemName.' => OK (Maybe set default value by application)');
		} else {
			$logger->warn(' Verify - '.$itemName.' => NG : XML='.self::getStringStatus($xmlData).' / DB='.self::getStringStatus($dbData));
			$unmatch = true;
		}
		return $unmatch;
	}

	/**
	 * Returns status of String.
	 * @param string $src String to check the state
	 * @return string If $src is not empty, as it is to return the $src
	 */
	private static function getStringStatus($src) {
		if(is_null($src))    return '(NULL)';
		if(strlen($src) < 1) return '(EMPTY)';
		return $src;
	}

	/**
	 * Extracts requested parameter in path.
	 * @return array [0]:ObjectId / [1]:ObjectVersion
	 */
	private function extractParameterInRequestPath() {
		$paramString = substr($this->path, strlen(self::REST_PATH_SELF) + 1); // + 1 is Length of '/'
		$params = explode(':', $paramString);

		return array(count($params)>0 ? $params[0] : '', count($params)>1 ? $params[1] : '');
	}


	/**
	 * (Convenience Function) Executes SQL statement and returns first row.
	 * @param $sql SQL statement - This must includes '?' one time.
	 * @param $id  Value of ID to search using SQL statement.
	 * @return array | null
	 */
	private function searchById($sql, $id) {
		return self::searchByParams($sql, array($id));
	}

	/**
	 * (Convenience Function) Executes SQL statement and returns first row.
	 * @param string $sql   SQL statement - This must includes '?' multi time.
	 * @param array  $param Values to search using SQL statement.
	 * @return array | null
	 */
	private function searchByParams($sql, $params) {
		$resultRow = null;

		$stmt = $this->sugarDbConnSlv->prepare($sql);
		if($stmt->execute($params)) {
			if($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$resultRow = $row;
			}
		}
		$stmt->closeCursor();

		return $resultRow;
	}
}
