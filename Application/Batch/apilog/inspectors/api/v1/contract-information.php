<?php
/**
 * Inspect '/api/v1/contract-information' API log.
 * <Inspected processing>
 *  - POST -> Http Response Status = 201
 *  - GET  -> Http Response Status = 200
 * @author Satoshi Mamada
 */
class ApiV1ContractInformationInspector extends DefaultInspector {

	/** [Const] Countory Code - Japan */
	const COUNTRY_CODE_JPN = '392';
	/** [Const] Countory Code - United States */
	const COUNTRY_CODE_USA = '840';
	/** [Const] Countory Code - Canada */
	const COUNTRY_CODE_CAN = '124';

	/**
	 * Inspection process - GET request was succeed.
	 * To check whether or not there is fraud in the response XML
	 * @return int Check Status
	 */
	protected function inspectGet200() {
		global $logger;
		$logger->info(get_class($this).'#'.__FUNCTION__.'() Start');
		try {
			// Parse response XML
			$sxe = new SimpleXMLElement($this->response_body);
		}
		catch (Exception $e) {
			// Parse Error occured.
			$logger->warn(get_class($this).'#'.__FUNCTION__.'() End - CheckStatus: '.ApilogBatch::CHKSTAT_ABNORMAL);
			return ApilogBatch::CHKSTAT_ABNORMAL; // Check NG
		}
		$logger->info(get_class($this).'#'.__FUNCTION__.'() End - CheckStatus: '.ApilogBatch::CHKSTAT_CHECKED);
		return ApilogBatch::CHKSTAT_CHECKED;      // Check OK
	}

	/**
	 * Inspection process - POST request was succeed.
	 * @return int Check status
	 */
	protected function inspectPost201() {
		global $logger;
		$logger->info(get_class($this).'#'.__FUNCTION__.'() Start');

		// Step1. Picks up Object-ID(i.e. Internal ID of global-contract-information) from Response XML
		$contractObjectId = '';
		try {
			$sxeResponse = new SimpleXMLElement($this->response_body);

			$contractObjectId = getElementValueByXpath($sxeResponse, '/res-v0:response/bi-v0:productContract/cbe-v0:identifiedBy/bi-v0:productContractKey/cbe-v0:objectID');
		} catch(Exception $e) {
			$logger->error("Cannot pick up Contract-ObjectID from Response XML (Exception occured)", $e);
		}
		if(empty($contractObjectId)) {
			$logger->warn("Contract-ObjectID is empty - abort to inspect");
			return ApilogBatch::CHKSTAT_ABNORMAL;
		}
		$logger->info(' Contract-ObjectID=' . $contractObjectId);

		// Step2. Parse Request XML
		// * Not enclose try-catch. Because if invalid xml was saved to apilog, then that should be returns error.
		$sxe = new SimpleXMLElement($this->request_body);

		// Step3. Checks that contents of Request XML and DB stored Data are same or not.
		$chkResult = $this->verifyPostInformationAndDB($sxe, $contractObjectId);

		$logLevel = ($chkResult==ApilogBatch::CHKSTAT_CHECKED ? 'info' : 'warn');
		$logger->$logLevel(get_class($this).'#'.__FUNCTION__.'() End - CheckStatus: '.$chkResult);
		return $chkResult;
	}

	private function verifyPostInformationAndDB($sxe, $contractObjectId) {
		$existUnmatch = false;

		//==== [Read-DB] Read tables from application database(sugarcrm) using Contract's ID ====

		// gc_contracts Table (Base for all verification)
		$gc_contract = $this->searchById(
			"SELECT * FROM gc_contracts WHERE id=? AND deleted=0"
		   , $contractObjectId);

		// accounts Table(i.e. Corporate Module) of Contract
		$accountOfContract = $this->searchById(
			"SELECT ac.*, acc.* FROM accounts ac, accounts_cstm acc WHERE ac.id=acc.id_c AND ac.deleted=0 AND ac.id=(SELECT rel.accounts_gc_contracts_1accounts_ida FROM accounts_gc_contracts_1_c rel WHERE rel.accounts_gc_contracts_1gc_contracts_idb=? AND rel.deleted=0)"
		   , $contractObjectId);

		// contacts Table of Contract(i.e. contacts_cstm.contact_type_c='Contract')
		$contactOfContract = $this->searchById(
			"SELECT ct.*, ctc.* FROM contacts ct, contacts_cstm ctc WHERE ct.id=ctc.id_c AND ct.deleted=0 AND ct.id=(SELECT rel.contacts_gc_contracts_1contacts_ida FROM contacts_gc_contracts_1_c rel WHERE rel.contacts_gc_contracts_1gc_contracts_idb=? AND rel.deleted=0) AND ctc.contact_type_c='Contract'"
		   , $contractObjectId);

		// email_address Table of Contract's contact
		$emailAddressOfContract = $this->searchById(
			"SELECT * FROM email_addresses WHERE deleted=0 AND id=(SELECT rel.email_address_id FROM email_addr_bean_rel rel WHERE rel.bean_id=? AND rel.deleted=0)"
		   , $contactOfContract['id']);

		// c_country Table of Contracst's contact
		$countryOfContract = $this->searchById(
			"SELECT * FROM c_country WHERE deleted=0 AND id=?"
		   , $contactOfContract['c_country_id_c']);

		// gc_line_item_contract_history Table
		$gcLineItemContractHistory = $this->searchById(
			"SELECT * FROM gc_line_item_contract_history WHERE deleted=0 AND contracts_id=?"
		   , $contractObjectId);

		// contacts Table of Billing(i.e. contacts_cstm.contact_type_c='Billing')
		$contactOfBilling = $this->searchById(
			"SELECT ct.*, ctc.* FROM contacts ct, contacts_cstm ctc WHERE ct.id=ctc.id_c AND ct.deleted=0 AND ct.id=? AND ctc.contact_type_c='Billing'"
		   , $gcLineItemContractHistory['bill_account_id']);

		// accounts Table(i.e. Corporate Module) of Billing
		$accountOfBilling = $this->searchById(
			"SELECT ac.*, acc.* FROM accounts ac, accounts_cstm acc WHERE ac.id=acc.id_c AND ac.deleted=0 AND ac.id=(SELECT rel.account_id FROM accounts_contacts rel WHERE rel.contact_id=? AND rel.deleted=0)"
		   , $contactOfBilling['id']);

		// c_country Table of Billing's contact
		$countryOfBilling = $this->searchById(
			"SELECT * FROM c_country WHERE deleted=0 AND id=?"
		   , $contactOfBilling['c_country_id_c']);

		// email_address Table of Billing contact
		$emailAddressOfBilling = $this->searchById(
			"SELECT * FROM email_addresses WHERE deleted=0 AND id=(SELECT rel.email_address_id FROM email_addr_bean_rel rel WHERE rel.bean_id=? AND rel.deleted=0)"
		   , $contactOfBilling['id']);

		// gc_salesrepresentative Table
		$gc_salesrepresentative = $this->searchById(
			"SELECT * FROM gc_salesrepresentative WHERE deleted=0 AND contracts_id=?"
		   , $contractObjectId);


		//==== [Read-XML] Read XML to get array of elements ====
		$partyRoleElements     = $sxe->xpath('/req-v0:request/party-v0:partyRole');
		$contactMediumElements = $sxe->xpath('/req-v0:request/party-v0:contactMedium');

		$placePartyRoleAssociationElements = $sxe->xpath('/req-v0:request/location-v0:placePartyRoleAssociation');
		$geographicAddressElements         = $sxe->xpath('/req-v0:request/location-v0:geographicAddress');


		//==== [Verify] Compares data in XML and data in database ====

		// CorporateID of Contract - 共通顧客ID
		$corporateId_Of_Contract = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyIdentification/ptyIdent001:corporateIdentification/ptyIdent001:corporationID/ptyIdent001:characteristicValue/ptyIdent001:corporationID');
		$existUnmatch |= self::verifyWithLog('CorporateID of Contract', $corporateId_Of_Contract, $accountOfContract['common_customer_id_c']);

		// Contract Name
		$contractName = getElementValueByXpath($sxe, '/req-v0:request/bi-v0:productContract/cbe-v0:name');
		$existUnmatch |= self::verifyWithLog('Contract Name', $contractName, $gc_contract['name']);

		// Contract Description
		$contractDescription = getElementValueByXpath($sxe, '/req-v0:request/bi-v0:productContract/cbe-v0:description');
		$existUnmatch |= self::verifyWithLog('Contract Description', $contractDescription, $gc_contract['description']);

		// Division (Local Character) - 契約者部課名
		$divisionLocal_Contact_Of_Contract = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyRole/pICContractPR:pICContractPR/pICContractPR:divisionName/pICContractPR:divisionName/pICContractPR:characteristicValue/pICContractPR:divisionName');
		$existUnmatch |= self::verifyWithLog('Division (Local Character)', $divisionLocal_Contact_Of_Contract, $contactOfContract['division_2_c']);

		// Contact Person Name (Local Character) - 契約者担当者名
		$personNameLocal_Contact_Of_Contract = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyRole/pICContractPR:pICContractPR/pICContractPR:individualName/pICContractPR:individualName/pICContractPR:characteristicValue/pICContractPR:individualName');
		$existUnmatch |= self::verifyWithLog('Contact Person Name (Local Character)', $personNameLocal_Contact_Of_Contract, $contactOfContract['name_2_c']);

		// Telephone number - 契約者電話番号
		$telephoneNumber_Contact_Of_Contract = null;
		$contactMediumKeyContract = self::findContactMediumKeyFromPartyRoleElements($partyRoleElements, 'pICContractRole');
		$contractMediumElementContract = empty($contactMediumKeyContract) ? null : self::findContactMediumElementById($contactMediumElements, $contactMediumKeyContract);
		if($contractMediumElementContract != null) {
			$telephoneNumber_Contact_Of_Contract = getElementValueByXpath($contractMediumElementContract, 'contactMedium:contactMedium/contactMedium:telephoneNumber/contactMedium:characteristicValue/contactMedium:telephoneNumber');
		}
		$existUnmatch |= self::verifyWithLog('Telephone number', $telephoneNumber_Contact_Of_Contract, $contactOfContract['phone_work']);

		// Email address - 契約者メールアドレス
		$emailAddress_Contact_Of_Contract = '';
		if($contractMediumElementContract != null) {
			$emailAddress_Contact_Of_Contract = getElementValueByXpath($contractMediumElementContract, 'contactMedium:contactMedium/contactMedium:eMailAddress/contactMedium:characteristicValue/contactMedium:eMailAddress');
		}
		$existUnmatch |= self::verifyWithLog('Email address', $emailAddress_Contact_Of_Contract, $emailAddressOfContract['email_address']);

		// VAT Number - 契約企業のVAT
		$vatNo = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyIdentification/ptyIdent003:vatNoIdentification/ptyIdent003:vatNo/ptyIdent003:characteristicValue/ptyIdent003:vatNo');
		$existUnmatch |= self::verifyWithLog('VAT Number', $vatNo, $accountOfContract['vat_no_c']);

		// Contact Person Name (Latin Character)
		$personNameLatin_Contact_Of_Contract = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyRole/pICContractPR:pICContractPR/pICContractPR:individualName/pICContractPR:individualNameLatin/pICContractPR:characteristicValue/pICContractPR:individualNameLatin');
		$existUnmatch |= self::verifyWithLog('Contact Person Name (Latin Character)', $personNameLatin_Contact_Of_Contract, $contactOfContract['last_name']);

		// Division (Latin Character)
		$divisionLatin_Contact_Of_Contract = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyRole/pICContractPR:pICContractPR/pICContractPR:divisionName/pICContractPR:divisionNameLatin/pICContractPR:characteristicValue/pICContractPR:divisionNameLatin');
		$existUnmatch |= self::verifyWithLog('Division (Latin Character)', $divisionLatin_Contact_Of_Contract, $contactOfContract['division_1_c']);

		// FAX number
		$faxNumber_Contact_Of_Contract = null;
		if($contractMediumElementContract != null) {
			$faxNumber_Contact_Of_Contract = getElementValueByXpath($contractMediumElementContract, 'contactMedium:contactMedium/contactMedium:faxNumber/contactMedium:characteristicValue/contactMedium:faxNumber');
		}
		$existUnmatch |= self::verifyWithLog('FAX number', $faxNumber_Contact_Of_Contract, $contactOfContract['phone_fax']);

		// Country
		$country_Contact_Of_Contract = null;
		$geoAddressKeyContract = self::findGeoAddressKeyFromPlacePartyRoleAssociationElements($placePartyRoleAssociationElements, 'contractCustomerRole');
		$geographicAddressElementContract = self::findGeographicAddressElementById($geographicAddressElements, $geoAddressKeyContract);
		if($geographicAddressElementContract != null) {
			$country_Contact_Of_Contract = getElementValueByXpath($geographicAddressElementContract, 'location-v0:countryKey/location-v0:iso3166_1numeric');
		}
		$existUnmatch |= self::verifyWithLog('Country', $country_Contact_Of_Contract, $countryOfContract['country_code_numeric']);

		switch ($country_Contact_Of_Contract) {
		  case self::COUNTRY_CODE_JPN:		// Japan
			// Contract Postal Number
			$postalNumber_Contact_Of_Contract = null;
			if($geographicAddressElementContract != null) {
				$postalNumber_Contact_Of_Contract = getElementValueByXpath($geographicAddressElementContract, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/jpnAddr:japanesePropertyAddress/jpnAddr:postalCode/jpnAddr:characteristicValue/jpnAddr:postalCode');
			}
			$existUnmatch |= self::verifyWithLog('Contract Postal Number', $postalNumber_Contact_Of_Contract, $contactOfContract['postal_no_c']);

			//==== [Read-DB] Japan Specific Address ====
			$japanSpecificAddress = $this->searchById(
				"SELECT * FROM js_accounts_js WHERE deleted=0 AND contact_id_c=?"
			   , $contactOfContract['id']);

			// Contract Address 1 [Japan specific]
			$jsAddress1_Contact_Of_Contract = null;
			if($geographicAddressElementContract != null) {
				$jsAddress1_Contact_Of_Contract = getElementValueByXpath($geographicAddressElementContract, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/jpnAddr:japanesePropertyAddress/jpnAddr:address/jpnAddr:characteristicValue/jpnAddr:address');
			}
			$existUnmatch |= self::verifyWithLog('Contract Address 1 [Japan specific]', $jsAddress1_Contact_Of_Contract, $japanSpecificAddress['addr1']);

			// Contract Address 2 [Japan specific]
			$jsAddress2_Contact_Of_Contract = null;
			if($geographicAddressElementContract != null) {
				$jsAddress2_Contact_Of_Contract = getElementValueByXpath($geographicAddressElementContract, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/jpnAddr:japanesePropertyAddress/jpnAddr:addressLine/jpnAddr:characteristicValue/jpnAddr:addressLine');
			}
			$existUnmatch |= self::verifyWithLog('Contract Address 2 [Japan specific]', $jsAddress2_Contact_Of_Contract, $japanSpecificAddress['addr2']);

			break;

		  case self::COUNTRY_CODE_USA:		// United States
			// Contract Address [US Specific]
			$addressLine_Contact_Of_Contract = null;
			if($geographicAddressElementContract != null) {
				$addressLine_Contact_Of_Contract = getElementValueByXpath($geographicAddressElementContract, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/usaAddr:americanPropertyAddress/usaAddr:addressLine/usaAddr:characteristicValue/usaAddr:addressLine');
			}
			$existUnmatch |= self::verifyWithLog('Contract Address [US Specific]', $addressLine_Contact_Of_Contract, $contactOfContract['addr_general_c']);

			// Contract City [US Specific]
			$city_Contact_Of_Contract = null;
			if($geographicAddressElementContract != null) {
				$city_Contact_Of_Contract = getElementValueByXpath($geographicAddressElementContract, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/usaAddr:americanPropertyAddress/usaAddr:city/usaAddr:characteristicValue/usaAddr:city');
			}
			$existUnmatch |= self::verifyWithLog('Contract City [US Specific]', $city_Contact_Of_Contract, $contactOfContract['city_general_c']);

			//==== [Read-DB] State ====
			$c_state = $this->searchById(
					"SELECT * FROM c_state WHERE deleted=0 AND id=?"
					, $contactOfContract['c_state_id_c']);

			// Contract State [US Specific]
			$stateAbbr_Contact_Of_Contract = null;
			if($geographicAddressElementContract != null) {
				$stateAbbr_Contact_Of_Contract = getElementValueByXpath($geographicAddressElementContract, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/usaAddr:americanPropertyAddress/usaAddr:stateAbbr/usaAddr:characteristicValue/usaAddr:stateAbbr');
			}
			$existUnmatch |= self::verifyWithLog('Contract State [US Specific]', $stateAbbr_Contact_Of_Contract, $c_state['state_code']);

			// Contract Postal Number
			$zipCode_Contact_Of_Contract = null;
			if($geographicAddressElementContract != null) {
				$zipCode_Contact_Of_Contract = getElementValueByXpath($geographicAddressElementContract, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/usaAddr:americanPropertyAddress/usaAddr:zipCode/usaAddr:characteristicValue/usaAddr:zipCode');
			}
			$existUnmatch |= self::verifyWithLog('Contract Postal Number', $zipCode_Contact_Of_Contract, $contactOfContract['postal_no_c']);

			break;

		  case self::COUNTRY_CODE_CAN:		// Canada
			// Contract Address [Canada Specific]
			$addressLine_Contact_Of_Contract = null;
			if($geographicAddressElementContract != null) {
				$addressLine_Contact_Of_Contract = getElementValueByXpath($geographicAddressElementContract, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/canAddr:canadianPropertyAddress/canAddr:addressLine/canAddr:characteristicValue/canAddr:addressLine');
			}
			$existUnmatch |= self::verifyWithLog('Contract Address [Canada Specific]', $addressLine_Contact_Of_Contract, $contactOfContract['addr_general_c']);

			// Contract City [Canada Specific]
			$city_Contact_Of_Contract = null;
			if($geographicAddressElementContract != null) {
				$city_Contact_Of_Contract = getElementValueByXpath($geographicAddressElementContract, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/canAddr:canadianPropertyAddress/canAddr:city/canAddr:characteristicValue/canAddr:city');
			}
			$existUnmatch |= self::verifyWithLog('Contract City [Canada Specific]', $city_Contact_Of_Contract, $contactOfContract['city_general_c']);

			//==== [Read-DB] State ====
			$c_state = $this->searchById(
					"SELECT * FROM c_state WHERE deleted=0 AND id=?"
					, $contactOfContract['c_state_id_c']);

			// Contract State [Canada Specific]
			$stateAbbr_Contact_Of_Contract = null;
			if($geographicAddressElementContract != null) {
				$stateAbbr_Contact_Of_Contract = getElementValueByXpath($geographicAddressElementContract, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/canAddr:canadianPropertyAddress/canAddr:stateAbbr/canAddr:characteristicValue/canAddr:stateAbbr');
			}
			$existUnmatch |= self::verifyWithLog('Contract State [Canada Specific]', $stateAbbr_Contact_Of_Contract, $c_state['state_code']);

			// Contract Postal Number
			$postalCode_Contact_Of_Contract = null;
			if($geographicAddressElementContract != null) {
				$postalCode_Contact_Of_Contract = getElementValueByXpath($geographicAddressElementContract, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/canAddr:canadianPropertyAddress/canAddr:postalCode/canAddr:characteristicValue/canAddr:postalCode');
			}
			$existUnmatch |= self::verifyWithLog('Contract Postal Number', $postalCode_Contact_Of_Contract, $contactOfContract['postal_no_c']);

			break;

		  default:							// Other
		  	// Contract Address (Local Character)
			$address_Contact_Of_Contract = null;
			if($geographicAddressElementContract != null) {
				$address_Contact_Of_Contract = getElementValueByXpath($geographicAddressElementContract, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/anyAddr:anyAddress/anyAddr:address/anyAddr:characteristicValue/anyAddr:address');
			}
			$existUnmatch |= self::verifyWithLog('Contract Address (Local Character) [ANY]', $address_Contact_Of_Contract, $contactOfContract['addr_2_c']);

		  	break;
		}

		// Billing Corporate Name (Local Character 2) - 送付先カナ
		$billingNameKana = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyRole/billingCustomerPR:billingCustomerPseudoRole/billingCustomerPR:corporationNameFurigana/billingCustomerPR:characteristicValue/billingCustomerPR:corporationNameFurigana');
		$existUnmatch |= self::verifyWithLog('Billing Corporate Name (Local Character 2)', $billingNameKana, $accountOfBilling['name_3_c']);

		// Billing Corporate Name (Local Character 1) - 送付先名
		$billingName = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyRole/billingCustomerPR:billingCustomerPseudoRole/billingCustomerPR:corporationName/billingCustomerPR:characteristicValue/billingCustomerPR:corporationName');
		$existUnmatch |= self::verifyWithLog('Billing Corporate Name (Local Character 1)', $billingName, $accountOfBilling['name_2_c']);

		// Billing Corporate Name (Latin Character) - 送付先名(英名)
		$billingNameLatin = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyRole/billingCustomerPR:billingCustomerPseudoRole/billingCustomerPR:corporationNameLatin/billingCustomerPR:characteristicValue/billingCustomerPR:corporationNameLatin');
		$existUnmatch |= self::verifyWithLog('Billing Corporate Name (Latin Character)', $billingNameLatin, $accountOfBilling['name'], 'Not Available');


		// Country of Billing contact
		$country_Contact_Of_Billing = null;
		$geoAddressKeyBilling = self::findGeoAddressKeyFromPlacePartyRoleAssociationElements($placePartyRoleAssociationElements, 'billingCustomerRole');
		$geographicAddressElementBilling = self::findGeographicAddressElementById($geographicAddressElements, $geoAddressKeyBilling);
		if($geographicAddressElementBilling != null) {
			$country_Contact_Of_Billing = getElementValueByXpath($geographicAddressElementBilling, 'location-v0:countryKey/location-v0:iso3166_1numeric');
		}
		$existUnmatch |= self::verifyWithLog('Billing Country', $country_Contact_Of_Billing, $countryOfBilling['country_code_numeric']);

		switch ($country_Contact_Of_Billing) {
		  case self::COUNTRY_CODE_JPN:		// Japan
			// Billing Postal Number - 送付先郵便番号
			$postalNumber_Contact_Of_Billing = null;
			if($geographicAddressElementBilling != null) {
				$postalNumber_Contact_Of_Billing = getElementValueByXpath($geographicAddressElementBilling, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/jpnAddr:japanesePropertyAddress/jpnAddr:postalCode/jpnAddr:characteristicValue/jpnAddr:postalCode');
			}
			$existUnmatch |= self::verifyWithLog('Billing Postal Number', $postalNumber_Contact_Of_Billing, $contactOfBilling['postal_no_c']);

			//==== [Read-DB] Japan Specific Address ====
			$japanSpecificAddress = $this->searchById(
				"SELECT * FROM js_accounts_js WHERE deleted=0 AND contact_id_c=?"
			   , $contactOfBilling['id']);

			// Billing Address 1 [Japan specific] - 送付先住所
			if($geographicAddressElementBilling != null) {
				$jsAddress1_Contact_Of_Billing = getElementValueByXpath($geographicAddressElementBilling, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/jpnAddr:japanesePropertyAddress/jpnAddr:address/jpnAddr:characteristicValue/jpnAddr:address');
			}
			$existUnmatch |= self::verifyWithLog('Billing Address 1 [Japan specific]', $jsAddress1_Contact_Of_Billing, $japanSpecificAddress['addr1']);

			// Billing Address 2 [Japan specific] - 送付先番地等
			$jsAddress2_Contact_Of_Billing = null;
			if($geographicAddressElementBilling != null) {
				$jsAddress2_Contact_Of_Billing = getElementValueByXpath($geographicAddressElementBilling, 'location-v0:usedAddressRepresentation/location-v0:countrySpecificAddress/jpnAddr:japanesePropertyAddress/jpnAddr:addressLine/jpnAddr:characteristicValue/jpnAddr:addressLine');
			}
			$existUnmatch |= self::verifyWithLog('Billing Address 2 [Japan specific]', $jsAddress2_Contact_Of_Billing, $japanSpecificAddress['addr2']);
			break;
		}

		// Billing Division (Local Character) - 送付先部課名
		$divisionLocal_Contact_Of_Billing = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyRole/pICBillingPR:pICBillingPR/pICBillingPR:divisionName/pICBillingPR:divisionName/pICBillingPR:characteristicValue/pICBillingPR:divisionName');
		$existUnmatch |= self::verifyWithLog('Billing Division (Local Character)', $divisionLocal_Contact_Of_Billing, $contactOfBilling['division_2_c']);

		// Billing Person Name (Local Character) - 送付先担当者名
		$personNameLocal_Contact_Of_Billing = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyRole/pICBillingPR:pICBillingPR/pICBillingPR:individualName/pICBillingPR:individualName/pICBillingPR:characteristicValue/pICBillingPR:individualName');
		$existUnmatch |= self::verifyWithLog('Contact Person Name (Local Character)', $personNameLocal_Contact_Of_Billing, $contactOfBilling['name_2_c']);

		// Billing Telephone number - 送付先電話番号
		$telephoneNumber_Contact_Of_Billing = null;
		$contactMediumKeyBilling = self::findContactMediumKeyFromPartyRoleElements($partyRoleElements, 'pICBillingRole');
		$contactMediumElementBilling = empty($contactMediumKeyBilling) ? null : self::findContactMediumElementById($contactMediumElements, $contactMediumKeyBilling);
		if($contactMediumElementBilling != null) {
			$telephoneNumber_Contact_Of_Billing = getElementValueByXpath($contactMediumElementBilling, 'contactMedium:contactMedium/contactMedium:telephoneNumber/contactMedium:characteristicValue/contactMedium:telephoneNumber');
		}
		$existUnmatch |= self::verifyWithLog('Telephone number', $telephoneNumber_Contact_Of_Billing, $contactOfBilling['phone_work']);

		// Billing Email address - 送付先メールアドレス
		$emailAddress_Contact_Of_Billing = '';
		if($contactMediumElementBilling != null) {
			$emailAddress_Contact_Of_Billing = getElementValueByXpath($contactMediumElementBilling, 'contactMedium:contactMedium/contactMedium:eMailAddress/contactMedium:characteristicValue/contactMedium:eMailAddress');
		}
		$existUnmatch |= self::verifyWithLog('Email address', $emailAddress_Contact_Of_Billing, $emailAddressOfBilling['email_address']);

		// Sales Channel Code - 販売チャネルコード
		$salesChannelCode = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyRole/salesChannelPR:salesChannelPseudoRole/salesChannelPR:salesChannelCode/salesChannelPR:characteristicValue/salesChannelPR:salesChannelCode');
		$existUnmatch |= self::verifyWithLog('Sales Channel Code', $salesChannelCode, $gc_salesrepresentative['sales_channel_code']);

		// Sales Representative Name (Local Character) - 販売担当者名
		$salesRepresentativeName = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyRole/pICSalesPR:pICSalesPR/pICSalesPR:individualName/pICSalesPR:individualName/pICSalesPR:characteristicValue/pICSalesPR:individualName');
		$existUnmatch |= self::verifyWithLog('Sales Representative Name (Local Character)', $salesRepresentativeName, $gc_salesrepresentative['sales_rep_name_1']);

		// Sales Representative) Division (Local Character) - 販売担当者所属部課名
		$salesDivision = getElementValueByXpath($sxe, '/req-v0:request/party-v0:partyRole/pICSalesPR:pICSalesPR/pICSalesPR:divisionName/pICSalesPR:divisionName/pICSalesPR:characteristicValue/pICSalesPR:divisionName');
		$existUnmatch |= self::verifyWithLog('Sales Representative Division (Local Character)', $salesDivision, $gc_salesrepresentative['division_2']);

		// Sales Representative) Telephone number - 販売担当者電話番号
		$contactMediumKeySales = self::findContactMediumKeyFromPartyRoleElements($partyRoleElements, 'pICSalesRole');
		$contactMediumElementSales = empty($contactMediumKeySales) ? null : self::findContactMediumElementById($contactMediumElements, $contactMediumKeySales);
		if($contactMediumElementSales != null) {
			$telephoneNumber_Contact_Of_Sales = getElementValueByXpath($contactMediumElementSales, 'contactMedium:contactMedium/contactMedium:telephoneNumber/contactMedium:characteristicValue/contactMedium:telephoneNumber');
		}
		$existUnmatch |= self::verifyWithLog('Telephone number', $telephoneNumber_Contact_Of_Sales, $gc_salesrepresentative['tel_no']);

		// Sales Representative) Email address - 販売担当者メールアドレス
		$emailAddress_Contact_Of_Sales = '';
		if($contactMediumElementSales != null) {
			$emailAddress_Contact_Of_Sales = getElementValueByXpath($contactMediumElementSales, 'contactMedium:contactMedium/contactMedium:eMailAddress/contactMedium:characteristicValue/contactMedium:eMailAddress');
		}
		$existUnmatch |= self::verifyWithLog('Email address', $emailAddress_Contact_Of_Sales, $gc_salesrepresentative['email1']);

		// Service Start Date
		$serviceStartDate = getElementValueByXpath($sxe, '/req-v0:request/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:servicePeriod/prodCntrctItem0001:serviceStartDate/prodCntrctItem0001:characteristicValue/prodCntrctItem0001:serviceStartDate');
		$existUnmatch |= self::verifyWithLog('Service Start Date', self::convertDate($serviceStartDate), $gcLineItemContractHistory['service_start_date']);

		// Service End Date
		$serviceEndDate = getElementValueByXpath($sxe, '/req-v0:request/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:servicePeriod/prodCntrctItem0001:serviceEndDate/prodCntrctItem0001:characteristicValue/prodCntrctItem0001:serviceEndDate');
		$existUnmatch |= self::verifyWithLog('Service End Date', self::convertDate($serviceEndDate), $gcLineItemContractHistory['service_end_date']);

		// Billing Start Date (利用開始日)
		$billingStartDate = getElementValueByXpath($sxe, '/req-v0:request/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:billingPeriod/prodCntrctItem0001:billingStartDate/prodCntrctItem0001:characteristicValue/prodCntrctItem0001:billingStartDate');
		$existUnmatch |= self::verifyWithLog('Billing Start Date', self::convertDate($billingStartDate), $gcLineItemContractHistory['billing_start_date']);

		// Billing End Date
		$billingEndDate = getElementValueByXpath($sxe, '/req-v0:request/bi-v0:productContract/bi-v0:businessInteractionItems/bi-v0:productItem/prodCntrctItem0001:productContractItem0001/prodCntrctItem0001:billingPeriod/prodCntrctItem0001:billingEndDate/prodCntrctItem0001:characteristicValue/prodCntrctItem0001:billingEndDate');
		$existUnmatch |= self::verifyWithLog('Billing End Date', self::convertDate($billingEndDate), $gcLineItemContractHistory['billing_end_date']);

		return $existUnmatch ? ApilogBatch::CHKSTAT_ABNORMAL : ApilogBatch::CHKSTAT_CHECKED;
	}

	private static function findPartyRoleElement($partyRoleElements, $partyRoleKey) {
		$targetPartyRoleElement = null;
		foreach($partyRoleElements as $partyRoleElement) {
			$validIdElements = $partyRoleElement->xpath('cbe-v0:identifiedBy/party-v0:partyRoleKey/cbe-v0:validIDOnlyInMessage');
			$validIdValue = empty($validIdElements) ? '(NotFound-validIDOnlyInMessageTag)' : (string)$validIdElements[0];
			if($validIdValue == $partyRoleKey) {
				$targetPartyRoleElement = $partyRoleElement;
				break;
			}
		}
		return $targetPartyRoleElement;
	}

	private static function findContactMediumKeyFromPartyRoleElements($partyRoleElements, $partyRoleKey) {
		$targetPartyRoleElement = self::findPartyRoleElement($partyRoleElements, $partyRoleKey);
		if($targetPartyRoleElement == null) {
			return null;
		}
		$valudIdElements = $targetPartyRoleElement->xpath('party-v0:contactableVia/party-v0:contactMediumKey/cbe-v0:validIDOnlyInMessage');
		return empty($valudIdElements) ? null : (string)$valudIdElements[0];
	}

	private static function findContactMediumElementById($contactMediumElements, $contactMediumKey) {
		$targetContactMediumElement = null;
		foreach($contactMediumElements as $contactMediumElement) {
			$validIdElements = $contactMediumElement->xpath('cbe-v0:identifiedBy/party-v0:contactMediumKey/cbe-v0:validIDOnlyInMessage');
			$validIdValue = empty($validIdElements) ? '(NotFound-validIDOnlyInMessageTag)' : (string)$validIdElements[0];
			if($validIdValue == $contactMediumKey) {
				$targetContactMediumElement = $contactMediumElement;
				break;
			}
		}
		return $targetContactMediumElement;
	}


	private static function findGeoAddressKeyFromPlacePartyRoleAssociationElements($placePartyRoleAssociationElements, $partyRoleKey) {
		$targetPlacePartyRoleAssociationElement = null;
		foreach($placePartyRoleAssociationElements as $placePartyRoleAssociationElement) {
			$validIdElements = $placePartyRoleAssociationElement->xpath('cbe-v0:associationEnds/party-v0:partyRoleKey/cbe-v0:validIDOnlyInMessage');
			$validIdValue = empty($validIdElements) ? '(NotFound-validIDOnlyInMessageTag)' : (string)$validIdElements[0];
			if($validIdValue == $partyRoleKey) {
				$targetPlacePartyRoleAssociationElement = $placePartyRoleAssociationElement;
				break;
			}
		}
		if($targetPlacePartyRoleAssociationElement == null) {
			return null;
		}
		$valudIdElements = $targetPlacePartyRoleAssociationElement->xpath('cbe-v0:associationEnds/location-v0:geoAddressKey/cbe-v0:validIDOnlyInMessage');
		return empty($valudIdElements) ? null : (string)$valudIdElements[0];
	}

	private static function findGeographicAddressElementById($geographicAddressElements, $geoAddressKey) {
		$targetGeographicAddressElement = null;
		foreach($geographicAddressElements as $geographicAddressElement) {
			$validIdElements = $geographicAddressElement->xpath('cbe-v0:identifiedBy/location-v0:geoAddressKey/cbe-v0:validIDOnlyInMessage');
			$validIdValue = empty($validIdElements) ? '(NotFound-validIDOnlyInMessageTag)' : (string)$validIdElements[0];
			if($validIdValue == $geoAddressKey) {
				$targetGeographicAddressElement = $geographicAddressElement;
				break;
			}
		}
		return $targetGeographicAddressElement;
	}

	private static function convertDate($dateInXml) {
		$result = $dateInXml;
		try {
			$objDateTime = DateTime::createFromFormat(DateTime::RFC3339, $dateInXml);
			if($objDateTime instanceof DateTime) {
				$result = $objDateTime->format('Y-m-d H:i:s');
			}
		} catch(Exception $e) {} // Do Nothing.

		return $result;
	}

	/**
	 * To verify data with the log output.
	 * @param string $itemName Name of item to veriry
	 * @param string $xmlData  XML data for comparison
	 * @param string $dbData   DB data for comparison
	 * @param string $defaultValue (Optional) Defult value set by the application when XML data was empty.
	 * @return boolean true: unmatch / false: match(=No Problem)
	 */
	private static function verifyWithLog($itemName, $xmlData, $dbData, $defaultValue=null) {
		$unmatch = false;

		global $logger;
		if($xmlData == $dbData) {
			$logger->info(' Verify - '.$itemName.' => OK');
		} else if(!is_null($defaultValue) && empty($xmlData) && $dbData == $defaultValue) {
			$logger->info(' Verify - '.$itemName.' => OK (Maybe set default value by application)');
		} else {
			$logger->warn(' Verify - '.$itemName.' => NG : XML='.self::getStringStatus($xmlData).' / DB='.self::getStringStatus($dbData));
			$unmatch = true;
		}
		return $unmatch;
	}

	/**
	 * Returns status of String.
	 * @param string $src String to check the state
	 * @return string If $src is not empty, as it is to return the $src
	 */
	private static function getStringStatus($src) {
		if(is_null($src))    return '(NULL)';
		if(strlen($src) < 1) return '(EMPTY)';
		return $src;
	}


	/**
	 * (Convenience Function) Executes SQL statement and returns first row.
	 * @param $sql SQL statement - This must includes '?' one time.
	 * @param $id  Value of ID to search using SQL statement.
	 * @return array | null
	 */
	private function searchById($sql, $id) {
		$resultRow = null;

		$stmt = $this->sugarDbConnSlv->prepare($sql);
		if($stmt->execute(array($id))) {
			if($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$resultRow = $row;
			}
		}
		$stmt->closeCursor();

		return $resultRow;
	}
}