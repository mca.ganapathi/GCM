<?php
require_once('inspector.php');

/**
 * Default Inspection class.
 * This class used from ApilogInspect when concrete class has no inspection function.
 * @author Satoshi Mamada
 */
class DefaultInspector extends Inspector {

	/** [Const] Prefix for inspection function name */
	const PREFIX_FUNCTION_INSPECT = 'inspect';

	/**
	 * Default inspection process.
	 *
	 * Search concrete inspection method in self($this).
	 * If method is exist, then calls that.(Method Invokation)
	 * If method is not exit, then do nothing.(Always skip)
	 *
	 * Format of inspection method: inspect[HttpMethod][HttpStatus]
	 * (Example) inspectPost201() inspectGet200() inspectPut200() ... etc.
	 *
	 * @return int Check Status(Result Value)
	 */
	function inspect() {
		global $logger;

		$inspectMethod = self::PREFIX_FUNCTION_INSPECT . Inflector::camelize(strtolower($this->method)) . $this->http_status;
		if(!method_exists($this, $inspectMethod)) { // Inspect method is not exist => Do Nothing.
			$logger->info('Method='.$this->method.' HttpStatus='.$this->http_status.' - SKIP(Implementation of inspection is not exist)');
			return ApilogBatch::CHKSTAT_SKIPPED;    // (Check Status = SKIP)
		}
		return $this->$inspectMethod();
	}
}