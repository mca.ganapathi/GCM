<?php
/**
 * (Abstract) Base class to implements inspection process.
 * @author Satoshi Mamada
 */
abstract class Inspector {

	/** Database Connection(Slave)  */
	protected $sugarDbConnSlv;

	/** LogInfo - Method */
	protected $method;
	/** LogInfo - Path */
	protected $path;
	/** LogInfo - Request Body */
	protected $request_body;
	/** LogInfo - HTTP Response Status Code */
	protected $http_status;
	/** LogInfo - Response Body */
	protected $response_body;

	/**
	 * Constructor - To hold the information necessary to check
	 * @param PDO    $sugarDbConnSlv Database Connection (For application slave)
	 * @param string $method
	 * @param string $path
	 * @param string $request_body
	 * @param int    $http_status
	 * @param string $response_body
	 */
	function __construct($sugarDbConnSlv, $method, $path, $request_body, $http_status, $response_body) {
		$this->sugarDbConnSlv = $sugarDbConnSlv;

		$this->method         = $method;
		$this->path           = $path;
		$this->request_body   = $request_body;
		$this->http_status    = $http_status;
		$this->response_body  = $response_body;
	}

	/**
	 * To implement inspect logic
	 * @return int CHECK STATUS
	 */
	abstract function inspect();
}