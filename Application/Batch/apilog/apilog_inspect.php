<?php
require_once('apilog_batch.php');
require_once('inspectors/default_inspector.php');

/**
 * Class for inspect the apilog.
 * @author Satoshi Mamada
 */
class ApilogInspect extends ApilogBatch {

	/** [Const] Database name for application */
	const DB_NAME = 'sugarcrm';

	/** [Const] Relative path located source-code of insprctors */
	const PATH_INSPECTORS = 'inspectors';

	/** {Const] Naming rule - Suffix for inspector's class name */
	const SUFFIX_INSPECTOR = 'inspector';

	/** Database Connection(Slave)  */
	private $sugarDbConnSlv;

	/**
	 * Constructor.
	 * Connect to Database 'sugarcrm' (Slave only)
	 * @param string $batchModuleName Name of batch module
	 */
	public function __construct($batchModuleName) {

		// Connect to Database(using batch configuration)
		global $batch_config;
		$dbConfig = $batch_config['db'];

		$this->sugarDbConnSlv = connectToDatabase($dbConfig['host_name']['slave'],
		                                          self::DB_NAME,
		                                          $dbConfig['sugarcrm_slave']['user_name'],
		                                          $dbConfig['sugarcrm_slave']['password']);
		$this->sugarDbConnSlv->query('SET wait_timeout=28800'); // 28800 seconds(=8 hours) is default value of MySQL.

		// Calls parent's constructor
		parent::__construct($batchModuleName);
	}

	/**
	 * Checks arguments before performs batch module.
	 * @param string[] $args command-line arguments
	 * @return int check result - 0: No error / Non-0: Error
	 */
	protected function checkArguments($args) {
		// Nothing to do.
		return 0;
	}

	/**
	 * Performs batch module.
	 * @param string[] $args command-line arguments
	 * @return integer Result value. It will be return code of batch process.
	 */
	protected function perform($args) {
		global $logger;

		$result = 0;
		// Get list of table names in 'apilog'
		$logTableNames = parent::getLogTableNames();
		// Checks for each tables
		foreach($logTableNames as $logTableName) {
			$logger->info(get_class($this).'.perform() Inspect Start - ' . $logTableName);
			try {
				$errorExist = $this->inspectUncheckedContents($logTableName);
				if($errorExist) {
					$result = -1;
				}
			} catch(Exception $e) {
				$logger->error($e);
				$result = -1;
			}
			$logger->info(get_class($this).'.perform() Inspect End - ' . $logTableName);
		}
		return $result;
	}


	private function inspectUncheckedContents($logTableName) {
		global $logger;

		$errorExist = false;

		$sql  = 'SELECT * FROM `' . $logTableName . '`';
		$sql .= ' WHERE checked=' . ApilogBatch::CHKSTAT_UNCHECKED;
		$sql .= ' ORDER BY seq';

		$stmt = $this->apiDbConnSlv->query($sql);
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$inspectorInstance = $this->createInspector($row);

			$logger->info('Calls ' . get_class($inspectorInstance).'.inspect() - Seq# ' . $row['seq']);
			$inspectResult = $inspectorInstance->inspect();

			$updateSql  = 'UPDATE `' . $logTableName . '`';
			$updateSql .= ' SET check_datetime = NOW(), checked = :checked';
			$updateSql .= ' WHERE seq = '.$row['seq'];

			$updateStmt = $this->apiDbConnMst->prepare($updateSql);
			$updateStmt->bindValue(':checked', $inspectResult, PDO::PARAM_INT);
			$updateStmt->execute();

			if(!($inspectResult==ApilogBatch::CHKSTAT_CHECKED || $inspectResult==ApilogBatch::CHKSTAT_SKIPPED)) {
				$errorExist = true;
			}
		}
		$stmt->closeCursor();

		return $errorExist;
	}

	private function createInspector($row) {
		//  Removes query string from path
		$requestPath = $row['path'];
		if(($posQuery=strpos($row['path'], '?')) !== false) {
			$requestPath = substr($row['path'], 0, $posQuery);
		}

		// Default Inspector Class Name
		$className = Inflector::classify('default_' . self::SUFFIX_INSPECTOR);

		//  Finds inspector source file by value of 'path' column.
		$concreteClassFile = $this->findConcreteClassFile(explode('/', $requestPath));

		if(!empty($concreteClassFile)) { // Found
			// Load concrete class (.php) file.
			require_once($concreteClassFile);
			$baseName = substr(basename($concreteClassFile), 0, -4); // -4 is length of '.php'
			$packageDir = substr(dirname($concreteClassFile), strlen(dirname(__FILE__) . DIRECTORY_SEPARATOR . self::PATH_INSPECTORS) + 1);
			// Derives a class name from package directory and base file name
			$className = Inflector::classify(str_replace(DIRECTORY_SEPARATOR, '_', $packageDir)
			                               . '_' . str_replace('-', '_', $baseName)
			                               . '_' . self::SUFFIX_INSPECTOR);
		}

		// Checks to class definition is exist(e.g. Class was loaded)
		if(!class_exists($className)) {  // Illegal implementation.
			$className = Inflector::classify('default_' . self::SUFFIX_INSPECTOR);
		}

		// Create Instance of Inspector
		return new $className($this->sugarDbConnSlv,
				$row['method'], $row['path'], $row['request_body'], $row['http_status'], $row['response_body']);
	}


	/**
	 * Find concrete class(.php) file in recursive.
	 * @param array $pathHierarchy relative path heerarchy
	 * @return string Concrete class file name(if not found, then return null)
	 */
	function findConcreteClassFile($pathHierarchy) {
		$relativePath = join(DIRECTORY_SEPARATOR, $pathHierarchy);
		$concreteClassFile = dirname(__FILE__) . DIRECTORY_SEPARATOR . self::PATH_INSPECTORS . $relativePath . '.php';
		if(is_file($concreteClassFile)) {
			return $concreteClassFile;
		}

		if(count($pathHierarchy) < 1) {
			return null; // Not Found.
		}
		array_pop($pathHierarchy); // Deletes Last Element
		return $this->findConcreteClassFile($pathHierarchy);
	}
}