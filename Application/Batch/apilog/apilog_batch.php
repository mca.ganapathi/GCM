<?php
/**
 * Common parent class for apilog batch
 * @author Satoshi Mamada
 */
abstract class ApilogBatch extends BatchBase {

	/** [Const] Database Name */
	const DB_NAME = 'apilog';

	/** [Const] Check Status - Unchecked */
	const CHKSTAT_UNCHECKED = 0;
	/** [Const] Check Status - Checked */
	const CHKSTAT_CHECKED   = 1;
	/** [Const] Check Status - Checked but ended in abnormal */
	const CHKSTAT_ABNORMAL  = 2;
	/** [Const] Check Status - Checked but ended by illegal error */
	const CHKSTAT_ILLEGAL   = 3;
	/** [Const] Check Status - Skipped */
	const CHKSTAT_SKIPPED   = 9;

	/** Database Connection(Master)  */
	protected $apiDbConnMst;
	/** Database Connection(Slave)  */
	protected $apiDbConnSlv;

	/**
	 * Constructor.
	 * Connect to Database 'apilog' (Master and Slave)
	 * @param string $batchModuleName Name of batch module
	 */
	public function __construct($batchModuleName) {

		// Connect to Database(using batch configuration)
		global $batch_config;
		$dbConfig = $batch_config['db'];

		$this->apiDbConnMst = connectToDatabase($dbConfig['host_name']['master'],
		                                        self::DB_NAME,
		                                        $dbConfig['apilog_master']['user_name'],
		                                        $dbConfig['apilog_master']['password']);
		$this->apiDbConnMst->query('SET wait_timeout=28800');

		$this->apiDbConnSlv = connectToDatabase($dbConfig['host_name']['slave'],
		                                        self::DB_NAME,
		                                        $dbConfig['apilog_slave']['user_name'],
		                                        $dbConfig['apilog_slave']['password']);
		$this->apiDbConnSlv->query('SET wait_timeout=28800');
		
		// Calls parent's constructor
		parent::__construct($batchModuleName);
	}

	/**
	 * Get all of table name in 'apilog' DB.
	 * @return string[] table names.
	 */
	protected function getLogTableNames() {
		$logTableNames = array();
		$stmt = $this->apiDbConnSlv->query('SHOW TABLES');
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$values = array_values($row);
			if(count($values)>0) {
				$logTableNames[] = $values[0];
			}
		}
		$stmt->closeCursor();

		return $logTableNames;
	}
}