<?php
require_once('apilog_batch.php');

/**
 * Class for extract the apilog.
 * @author Satoshi Mamada
 */
class ApilogExtract extends ApilogBatch {

	private static function usage() {
		echo "Usage:\n";
		echo "php execute.php apilog.ApilogExtract {LogTableName} {SequenceNumber}\n\n" ;
		echo "This batch extracts the apilog of argument." ;
		return -1;
	}

	/**
	 * Checks arguments before performs batch module.
	 * @param string[] $args command-line arguments
	 * @return int check result - 0: No error / Non-0: Error
	 */
	protected function checkArguments($args) {
		// Checks number of arguments.
		if(count($args) !== 2) {
			return self::usage();
		}
		// Checks the argument is numeric-string.
		if(!is_numeric($args[1])) {
			return self::usage();
		}

		// Checks the LogTable exists in apilog Database
		$logTableNames = parent::getLogTableNames();
		if(!in_array($args[0], $logTableNames)) {
			echo 'Error - LogTable "' . $args[0] . '" not exists.';
			return -1;
		}
		// Check OK
		return 0;
	}

	/**
	 * Performs batch module.
	 * @param string[] $args command-line arguments
	 * @return integer Result value. It will be return code of batch process.
	 */
	protected function perform($args) {
		global $logger;

		$result = 0;

		$logger->info(get_class($this).'.perform() Extract Start - ' . $args[0] . ' #' . $args[1]);
		try {
			$errorExist = $this->extractApilog($args[0], $args[1]);
			if($errorExist) {
				$result = -1;
			} else {
				$logger->info('Extract Success.');
			}
		} catch(Exception $e) {
			$logger->error($e);
			$result = -1;
		}
		$logger->info(get_class($this).'.perform() Extract End');

		return $result;
	}

	private function extractApilog($logTableName, $sequenceNumber) {
		global $logger;

		$sql  = 'SELECT * FROM `' . $logTableName . '` WHERE seq = :seq';
		$stmt = $this->apiDbConnSlv->prepare($sql);
 		//$stmt->bindValue(':seq', $sequenceNumber, PDO::PARAM_INT);
		if(!$stmt->execute(array(':seq' => $sequenceNumber))) {
			return true; // Error occured.
		}
		if($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			// Writes row data to temporally files.
			$accessInfo  = '              seq : ' . $row['seq']               . "\n";
			$accessInfo .= '           method : ' . $row['method']            . "\n";
			$accessInfo .= '             path : ' . $row['path']              . "\n";
			$accessInfo .= ' request_datetime : ' . $row['request_datetime']  . "\n";
			$accessInfo .= '      http_status : ' . $row['http_status']       . "\n";
			$accessInfo .= 'response_datetime : ' . $row['response_datetime'] . "\n";
			$accessInfo .= '          checked : ' . $row['checked']           . "\n";
			$accessInfo .= '   check_datetime : ' . $row['check_datetime']    . "\n";
			$accessInfoFile = LOG_DIR . 'access_info.txt';
			file_put_contents($accessInfoFile, $accessInfo);

			$requestBodyFile = LOG_DIR . 'request_body.txt';
			file_put_contents($requestBodyFile, $row['request_body']);

			$responseBodyFile = LOG_DIR . 'response_body.txt';
			file_put_contents($responseBodyFile, $row['response_body']);

			// Archive files
			$this->archiveFiles(LOG_DIR, $logTableName . '#' . $sequenceNumber,
			                    array($accessInfoFile, $requestBodyFile, $responseBodyFile));
		}
		else {
			echo 'Record not found. - ' . $logTableName . ' #' . $sequenceNumber;
			$logger->info('Record was not exist.');
		}
		return false;
	}



	/**
	 * アーカイブ(ZIP)生成処理
	 * @param string $outputDir ZIP生成先ディレクトリ
	 * @param string $archiveBaseName アーカイブファイルのベース部分(拡張子は .zip 固定)
	 * @param array  $srcFiles  格納する実ファイル(フルパス)の配列
	 * @return string ZIPファイルパス(フルパス)
	 */
	private function archiveFiles($outputDir, $archiveBaseName, $srcFiles) {
		$zip = new ZipArchive();
		// ZIPファイルをオープン
		// open file
		$filePath  = $outputDir . (endsWith($outputDir, DIRECTORY_SEPARATOR) ? '' : DIRECTORY_SEPARATOR);
		$filePath .= $archiveBaseName . '.zip';
		$res = $zip->open($filePath, ZipArchive::CREATE);

		// zipファイルのオープンに成功した場合
		if ($res === true) {
			//  圧縮するファイルを指定
			foreach($srcFiles as $srcFile) {
				$zip->addFile($srcFile, basename($srcFile));
			}
			// ZIPファイルをクローズ(圧縮実行)
			$zip->close();
		}

		// ZIPファイルの生成が成功していたら、元ファイルを削除する
		if (file_exists($filePath)) {
			foreach($srcFiles as $srcFile) {
				unlink($srcFile);
			}
		}
		return $filePath;
	}

}