<?php
require_once('apilog_batch.php');

/**
 * Class to cleaning the inspected apilog.
 * @author Satoshi Mamada
 */
class ApilogClean extends ApilogBatch {

	/** [Const] Date format for argument.  */
	const DATE_FORMAT = 'Y-m-d';

	private static function usage() {
		echo "Usage:\n";
		echo "php execute.php apilog.ApilogClean yyyy-mm-dd\n\n" ;
		echo "This batch deletes the apilog that checked_date is older than date of argument." ;
		return -1;
	}

	/**
	 * Checks arguments before performs batch module.
	 * @param string[] $args command-line arguments
	 * @return int check result - 0: No error / Non-0: Error
	 */
	protected function checkArguments($args) {
		// Checks the argument is date-string.
		if(count($args) !== 1) {
			return self::usage();
		}
		try {
			$argDate = DateTime::createFromFormat(self::DATE_FORMAT, $args[0]);
			if(!($argDate instanceof DateTime)) {
				return self::usage();
			}
		} catch(Exception $e) {
			// Exception occured in some reason.
			return self::usage();
		}
		// Check OK
		return 0;
	}

	/**
	 * Performs batch module.
	 * @param string[] $args command-line arguments
	 * @return integer Result value. It will be return code of batch process.
	 */
	protected function perform($args) {
		global $logger;

		$boundaryDateTime = DateTime::createFromFormat(self::DATE_FORMAT, $args[0])->setTime(0, 0, 0)->format('Y-m-d H:i:s');

		$result = 0;
		// Get list of table names in 'apilog'
		$logTableNames = parent::getLogTableNames();
		// Checks for each tables
		$logger->info(get_class($this).".perform() Cleaning Start - Target: checked_datetime is older than '" . $boundaryDateTime . "'");
		foreach($logTableNames as $logTableName) {
			try {
				$this->deleteLog($logTableName, $boundaryDateTime);
			} catch(Exception $e) {
				$logger->error($e);
				$result = -1;
			}
		}
		$logger->info(get_class($this).'.perform() Cleaning End');
		return $result;
	}


	private function deleteLog($logTableName, $boundaryDateTime) {
		global $logger;

		$sql  = 'DELETE FROM `' . $logTableName . '`';
		$sql .= " WHERE checked <> :checked";
		$sql .= "   AND check_datetime < STR_TO_DATE(:check_datetime, '%Y-%m-%d %H:%i:%s')";

		$stmt = $this->apiDbConnMst->prepare($sql);
		$stmt->bindValue(':checked',        parent::CHKSTAT_UNCHECKED, PDO::PARAM_STR);
		$stmt->bindValue(':check_datetime', $boundaryDateTime,         PDO::PARAM_STR);
		$stmt->execute();
		$count = $stmt->rowCount();
		$logger->info(' Delete apilog `' . $logTableName . '` : ' . $count . ' record' . ($count>1 ? 's.' : '.'));
		$stmt->closeCursor();
	}
}