<?php
/**
 * Helper class to inquire CIDAS.
 * @author Satoshi Mamada
 */
class CidasClient {

	/** [Const] Path to inquire */
	const INQUIRE_PATH = '/cid_search/corp_cust';

	/** [Const] DefaultValue of QueryParameter 'limitCount' : 100 */
	const LIMIT_COUNT_DEFAULT = 100;

	/** [Const] Value of QueryParameter 'exactMatch' : Exact Match */
	const MATCH_EXACT = 'on';
	/** [Const] Value of QueryParameter 'exactMatch' : Forward Match */
	const MATCH_FORWARD = '';

	/** [Const] Value of QueryParameter 'charCode' : UTF-8 */
	const CHAR_CODE_UTF8 = 'UTF8';
	/** [Const] Value of QueryParameter 'charCode' : Windows-31J */
	const CHAR_CODE_DEFAULT = '';

	/** [Const] Value of QueryParameter 'statusCode' : All status(Enabled and Disabled) */
	const STATUS_CODE_ALL = '1';
	/** [Const] Value of QueryParameter 'statusCode' : Enabled Only */
	const STATUS_CODE_ENABLED = '';

	/** BaseURL */
	private $baseUrl;

	/** UserName */
	private $userName;

	/** PassCode */
	private $passCode;

	/** Resource Context */
	private $resourceContext;


	/**
	 * Constructor.
	 * Keeps setting information to connect CIDAS.
	 * @param string $baseUrl  BaseURL of CIDAS - i.e. https://cidas.ntt.com
	 * @param string $userName UserName to inquire to CIDAS
	 * @param string $passCode PassCode to inquire to CIDAS
	 * @param string $proxyUrl Proxy for CIDAS - i.e. tcp://proxyHost:8888
	 */
	public function __construct($baseUrl, $userName, $passCode, $proxyUrl) {
		$this->baseUrl  = $baseUrl . (endsWith($baseUrl, '/') ? substr(self::INQUIRE_PATH, 1) : self::INQUIRE_PATH);
		$this->userName = $userName;
		$this->passCode = $passCode;
		if(empty($proxyUrl)) {
			$this->resourceContext = null;
		}
		else {
			$this->resourceContext = stream_context_create(array(
				'ssl'   => array('verify_peer' => false, 'verify_peer_name' => false),
				'https' => array('proxy' => $proxyUrl, 'request_fulluri' => true),
				'http'  => array('proxy' => $proxyUrl, 'request_fulluri' => true)
			));
		}
	}

	/**
	 * Inquire to CIDAS using parameters.
	 * @param string $commonCustomerId - Query Parameter(Optional)
	 * @param string $companyName      - Query Parameter(Optional)
	 * @param string $address          - Query Parameter(Optional)
	 * @param string $companyNameEng   - Query Parameter(Optional)
	 * @param string $countryNameCode  - Query Parameter(Optional)
	 * @param string $cityName         - Query Parameter(Optional)
	 * @param string $stateName        - Query Parameter(Optional)
	 * @param string $exactMatch       - Query Parameter(Optional)
	 * @param string $charCode         - Query Parameter(Optional)
	 * @param string $statusCode       - Query Parameter(Optional)
	 * @param number $limitCount       - Query Parameter(Required: Default=100)
	 * @return boolean(FALSE) | string ResponseXML
	 */
	public function inquire($commonCustomerId, $companyName, $address, $companyNameEng, $countryNameCode,
	                        $cityName, $stateName, $exactMatch, $charCode, $statusCode, $limitCount=self::LIMIT_COUNT_DEFAULT) {
		// Build url string(BasePath and required parameters)
		$inquiryUrl  = $this->baseUrl;
		$inquiryUrl .= '?user='       . urlencode($this->userName);  // Required Parameter
		$inquiryUrl .= '&passCode='   . urlencode($this->passCode);  // Required Parameter
		$inquiryUrl .= '&limitCount=' . urlencode($limitCount);      // Required Parameter

		// Adds optional parameters if value is exist.
		if(!empty($commonCustomerId)) $inquiryUrl .= '&commonCustomerId=' . urlencode($commonCustomerId);
		if(!empty($companyName))      $inquiryUrl .= '&companyName='      . urlencode($companyName);
		if(!empty($address))          $inquiryUrl .= '&address='          . urlencode($address);
		if(!empty($companyNameEng))   $inquiryUrl .= '&companyNameEng='   . urlencode($companyNameEng);
		if(!empty($countryNameCode))  $inquiryUrl .= '&countryNameCode='  . urlencode($countryNameCode);
		if(!empty($cityName))         $inquiryUrl .= '&cityName='         . urlencode($cityName);
		if(!empty($stateName))        $inquiryUrl .= '&stateName='        . urlencode($stateName);
		if(!empty($exactMatch))       $inquiryUrl .= '&exactMatch='       . urlencode($exactMatch);
		if(!empty($charCode))         $inquiryUrl .= '&charCode='         . urlencode($charCode);
		if(!empty($statusCode))       $inquiryUrl .= '&statusCode='       . urlencode($statusCode);

		// Inquire (i.e. Executes HTTP-Request)
		$responseXml = file_get_contents($inquiryUrl, false, $this->resourceContext);

		// Returns response
		return $responseXml;
	}

	/**
	 * (Convinience Method) Inquire to CIDAS using CID.
	 * @param string $commonCustomerId CID
	 * @param string $charCode         CharCode
	 * @param string $statusCode       StatusCode
	 * @return boolean(FALSE) | string ResponseXML
	 */
	public function inquireByCID($commonCustomerId, $charCode, $statusCode) {
		return $this->inquire($commonCustomerId, null, null, null, null, null, null, null, $charCode, $statusCode);
	}
}