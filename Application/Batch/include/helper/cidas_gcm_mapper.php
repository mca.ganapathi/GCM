<?php
/**
 * Class responsible for the mapping process between CIDAS and GCM
 * @author Satoshi Mamada
 */
class CidasGcmMapper {

	/** [Const] (CIDAS)CustomerStatusCode Value - Valid */
	const CIDAS_CUSTOMER_STATUS_CODE_VALID   = ' ';
	/** [Const] (CIDAS)CustomerStatusCode (CIDAS)Value - Invalid */
	const CIDAS_CUSTOMER_STATUS_CODE_INVALID = '1';

	/** [Const] CustomerStatusCode Value - Valid */
	const GCM_CUSTOMER_STATUS_CODE_VALID   = '0';
	/** [Const] CustomerStatusCode Value - Invalid */
	const GCM_CUSTOMER_STATUS_CODE_INVALID = '1';

	/** (Define) CIDAS-GCM MappingInfo */
	private $mappingInfoCidasToGcm;

	/** Corporate information array */
	private $cidasCorporate;

	/**
	 * Constructor.
	 * Keeps corporate information array in private variables.
	 * @param array $cidasCorporate Corporete information array(CIDAS)
	 */
	public function __construct($cidasCorporate) {

		$this->cidasCorporate = $cidasCorporate;

		$this->mappingInfoCidasToGcm = array(
		//	   CIDAS Field                     GCM Field(Column)
			  'commonCustomerId'           => 'common_customer_id_c'
			, 'companyName'                => 'name_2_c'
			, 'companyNameKana'            => 'name_3_c'
			, 'companyNameEng'             => 'name'
			, 'countryName'                => 'country_name_c'
			, 'countryNameCode'            => 'country_code_c'
			, 'headquarterPostalNumber'    => 'hq_postal_no_c'
			, 'headquarterAddress'         => 'hq_addr_2_c'
		//	, 'headquarterAddressCode'     => '' // Not Used in GCM
			, 'headquarterAddressEng'      => 'hq_addr_1_c'
			, 'registerPostalNumber'       => 'reg_postal_no_c'
			, 'registerAddress'            => 'reg_addr_2_c'
		//	, 'registerAddressCode'        => '' // Not Used in GCM
			, 'registerAddressEng'         => 'reg_addr_1_c'
		//	, 'tdbNumber'                  => '' // Not Used in GCM
		//	, 'dunsNumber'                 => '' // Not Used in GCM
		//	, 'customerSegmentCode5'       => '' // Not Used in GCM
		//	, 'inUseCommonCustomerIdFlag'  => '' // Not Used in GCM
			, 'customerStatusCode'         => 'customer_status_code_c'
		//	, 'integratedCommonCustomerId' => '' // Not Used in GCM
		);
	}

	/**
	 * Converts elements for comparation.
	 * @param  array[key=>cidasValue] $cidasCorporate Corporate Information(CIDAS)
	 * @return array[column=>dbValue] Corporate Information(GCM)
	 */
	public function convertToGcmCorporate() {
		$result = array();
		foreach($this->cidasCorporate as $cidasField => $cidasValue) {
			if(!array_key_exists($cidasField, $this->mappingInfoCidasToGcm)) {// This field(key) is not exist in mapping definition.
				continue;                                                     // -> Ignore this field.
			}

			// Convert value to the GCM side
			switch($cidasField) {
				case 'companyName':      // These fields have NOT NULL constraints in the GCM side(i.e. DB),
				case 'companyNameKana':  //                                       and the default value is empty string.
				case 'companyNameEng':   // So, if the CIDAS side is NULL, convert to the empty string.
					$gcmValue = is_null($cidasValue) ? '' : $cidasValue;
					break;

				case 'customerStatusCode':
					// CIDAS : VALID=' ' / INVALID='1'
					// GCM   : VALID='0' / INVALID='1'
					$gcmValue = $cidasValue==self::CIDAS_CUSTOMER_STATUS_CODE_INVALID ? self::GCM_CUSTOMER_STATUS_CODE_INVALID : self::GCM_CUSTOMER_STATUS_CODE_VALID;
					break;

				default:
					// Pass a value as it is.
					$gcmValue = $cidasValue;
			}
			// Get Field(Column) name of GCM
			$gcmField = $this->mappingInfoCidasToGcm[$cidasField];
			//
			$result[$gcmField] = $gcmValue;
		}
		return $result;
	}

	/**
	 * Provides field names mapped to GCM.
	 * @return array Field names(GCM)
	 */
	public function getMappedFieldNames() {
		return array_values($this->mappingInfoCidasToGcm);
	}
}