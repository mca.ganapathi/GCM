<?php
/**
 * Helper class to handle ResponseXML from CIDAS.
 * @author Satoshi Mamada
 */
class CidasResponse {

	/** SimpleXMLElement of ResponseXML*/
	private $sxe;

	/**
	 * Constructor
	 * @param string $xml XML-string from CIDAS
	 */
	public function __construct($xml) {
		$this->sxe = new SimpleXMLElement($xml);
	}

	/**
	 * Checks error is exist or not.
	 * @return boolean
	 */
	public function hasError() {
		$errorCode = $this->getErrorCode();
 		return (!empty($errorCode));
	}

	/**
	 * Get value of 'tns:errorCode' element.
	 * @return NULL|string
	 */
	public function getErrorCode() {
		return getElementValueByXpath($this->sxe, '/tns:root/tns:error/tns:errorCode');
	}

	/**
	 * Get value of 'tns:recordCount' element.
	 * @return NULL|string
	 */
	public function getRecordCount() {
		return getElementValueByXpath($this->sxe, '/tns:root/tns:result/tns:recordCount');
	}

	/**
	 * Convert to Array.
	 * @return array [RecordCount][tns:key => tns:value]
	 */
	public function toArray() {
		$results = array();
		$recordElements = $this->sxe->xpath('/tns:root/tns:result/tns:record');
		if(empty($recordElements)) {
			return $results;  // Returns empty array.
		}
		foreach($recordElements as $recordElement) {
			$record = array();
			$columnElements = $recordElement->xpath('tns:column');
			foreach($columnElements as $columnElement) {
				$key   = getElementValueByXpath($columnElement, 'tns:key');
				$value = getElementValueByXpath($columnElement, 'tns:value');
				$record[$key] = $value;
			}
			$results[] = $record;
		}
		return $results;
	}
}