<?php
require_once('persistence_base.php');

/**
 * Class for handle sugarcrm#accounts table and relation tables.
 * @author Satoshi Mamada
 */
class SugarcrmAccounts extends PersistenceBase {

	/** [Define] ColumnNames in accounts table */
	private $coreColumnNames;

	/** [Define] FiledNames data_type is text(for accounts_audit) */
	private $textColumnNames;

	/** [Define] String values(for accounts_audit) */
	private $auditStringValues;

	/**
	 * Constructor.
	 * Keeps connection to DB.
	 * @param PDO $conn conntction to DB
	 */
	public function __construct($conn) {

		$this->coreColumnNames = array('id', 'name', 'date_entered', 'date_modified', 'modified_user_id', 'created_by',
			'description', 'deleted', 'assigned_user_id', 'team_id', 'team_set_id', 'account_type', 'industry',
			'annual_revenue', 'phone_fax', 'billing_address_street', 'billing_address_city', 'billing_address_state',
			'billing_address_postalcode', 'billing_address_country', 'rating', 'phone_office', 'phone_alternate',
			'website', 'ownership', 'employees', 'ticker_symbol', 'shipping_address_street', 'shipping_address_city',
			'shipping_address_state', 'shipping_address_postalcode', 'shipping_address_country',
			'parent_id', 'sic_code', 'campaign_id'
		);

		$this->textColumnNames = array('description', 'hq_addr_2_c', 'hq_addr_1_c', 'reg_addr_2_c', 'reg_addr_1_c');

		$this->auditStringValues = array(
			'customer_status_code_c' => array('0' => 'Valid', '1' => 'Invalid')
		);

		// Calls parent's constructor
		parent::__construct($conn);
	}

	/**
	 * Search accounts that has valid CommonCustomerID (i.e. CommonCustomerID forward matches 'C')
	 * @param $deleted Deleted (Default=NOT_DELETED)
	 * @return array[]['column'=>'value']
	 */
	public function searchWithValidCID($deleted=parent::NOT_DELETED) {
		$results = array();

		$sql = "SELECT accounts.*, accounts_cstm.*"
		     . " FROM  accounts, accounts_cstm"
		     . " WHERE accounts.id = accounts_cstm.id_c"
		     . " AND   accounts_cstm.common_customer_id_c LIKE 'C%'"
		     . " AND   accounts.deleted=?";

		$stmt = $this->conn->prepare($sql);
		if($stmt->execute(array($deleted))) {
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$results[] = $row;
			}
		}
		$stmt->closeCursor();

		return $results;
	}

	/**
	 * Updates accounts/accounts_cstm table.
	 * @param string   $primaryKey     Value of 'id' column
	 * @param array    $updateContents Contents to be updated
	 * @param string   $modifyUserId
	 * @param DateTime $modifyTime
	 * @return Result of PDOStatement::execute()
	 */
	public function update($primaryKey, $updateContents, $modifyUserId, $modifyTime) {

		// 1. Build string for target of set statement - Different with DB and CIDAS.
		$updateColumns = array();
		$bindValues    = array();
		foreach($updateContents as $coulmn => $value) {
			$updateColumns[] = $this->getTableName($coulmn) . '.' . $coulmn . '=?';
			$bindValues[]    = $value;
		}
		// 2. Build string for target of SET statement - accounts.modified_user_id column
		$updateColumns[] = 'accounts.modified_user_id=?';
		$bindValues[]    = $modifyUserId;
		// 3. Build string for target of SET statement - accounts.date_modified column
		$updateColumns[] = "accounts.date_modified=STR_TO_DATE(?, '%Y-%m-%d %H:%i:%s')";
		$bindValues[]    = $modifyTime->format('Y-m-d H:i:s');
		// 4. Build SQL string / Bind value of WHERE condition
		$updateSql = 'UPDATE accounts, accounts_cstm'
		           . ' SET ' . join(', ', $updateColumns)
		           . ' WHERE accounts.id=accounts_cstm.id_c'
		           . ' AND   accounts.id=?';
		$bindValues[] = $primaryKey;

// echo $updateSql."\n";
		// 5.Prepare statement
		$stmt = $this->conn->prepare($updateSql);
		// 6.Bind values and execute
		$pdoResult = $stmt->execute($bindValues);
		// 7.Init cursor
		$stmt->closeCursor();

		return $pdoResult;
	}

	/**
	 * Inserts accounts_audit table.
	 * @param string   $parentId Value of 'id' column of accounts table
	 * @param string   $fieldName Name of column
	 * @param string   $beforeValue Value before update to accounts(or accounts_cstm) table
	 * @param string   $afterValue  Value before updated
	 * @param string   $modifyUserId
	 * @param DateTime $modifyTime
	 * @return Result of PDOStatement::execute()
	 */
	public function insertAudit($parentId, $fieldName, $beforeValue, $afterValue, $modifyUserId, $modifyTime) {
		// 1. Build SQL
		$insertSql = 'INSERT INTO accounts_audit (`id`'
		           . ',`parent_id`'
		           . ',`date_created`'
		           . ',`created_by`'
		           . ',`field_name`'
		           . ',`data_type`'
		           . ',' . ($this->isTextField($fieldName) ? '`before_value_text`' : '`before_value_string`')
		           . ',' . ($this->isTextField($fieldName) ? '`after_value_text`'  : '`after_value_string`')
		           . ') VALUES (UUID()'
		           . ',?'                                              // 1: parent_id
		           . ",STR_TO_DATE(?, '%Y-%m-%d %H:%i:%s')"            // 2: date_created
		           . ',?'                                              // 3: created_by
		           . ',?'                                              // 4: field_name
		           . ',?'                                              // 5: data_type
		           . ',?'                                              // 6: before_value
		           . ',?'                                              // 7: after_value
		           . ')';

// echo $insertSql."\n";
		// 2. Build bind values
		$bindValues = array(
			  $parentId                                                // 1: parent_id
			, $modifyTime->format('Y-m-d H:i:s')                       // 2: date_created
			, $modifyUserId                                            // 3: created_by
			, $fieldName                                               // 4: field_name
			, $this->getAuditDataType($fieldName)                      // 5: data_type
			, $this->convertAuditStringValue($fieldName, $beforeValue) // 6: before_value
			, $this->convertAuditStringValue($fieldName, $afterValue)  // 7: after_value
		);
		// 3.Prepare statement
		$stmt = $this->conn->prepare($insertSql);
		// 4.Bind values and execute
		$pdoResult = $stmt->execute($bindValues);
		// 5.Init cursor
		$stmt->closeCursor();

		return $pdoResult;
	}

	private function convertAuditStringValue($fieldName, $srcValue) {
		$result = null;
		if(array_key_exists($fieldName, $this->auditStringValues)) {      // Convert to display string value
			$stringValues = $this->auditStringValues[$fieldName];
			$result = array_key_exists($srcValue, $stringValues) ? $stringValues[$srcValue] : $srcValue;
		}
		else {
			$result = is_null($srcValue) ? '' : $srcValue;
		}
		return $result;
	}

	private function getTableName($columnName) {
		return in_array($columnName, $this->coreColumnNames) ? 'accounts' : 'accounts_cstm';
	}

	private function getAuditDataType($fieldName) {
		return $this->isTextField($fieldName) ? 'text' : 'varchar';
	}

	private function isTextField($fieldName) {
		return in_array($fieldName, $this->textColumnNames);
	}
}
