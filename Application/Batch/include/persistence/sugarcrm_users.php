<?php
require_once('persistence_base.php');

/**
 * Class for handle sugarcrm#users table and relation tables.
 * @author Satoshi Mamada
 */
class SugarcrmUsers extends PersistenceBase {

	/**
	 * Constructor.
	 * Keeps connection to DB.
	 * @param PDO $conn conntction to DB
	 */
	public function __construct($conn) {
		// Calls parent's constructor
		parent::__construct($conn);
	}


	/**
	 * Find a user by UserName
	 * @param $userName UserName
	 * @param $deleted  Deleted (Default=NOT_DELETED)
	 * @return NULL|array 'NULL' means NotFound.
	 */
	public function findByUserName($userName, $deleted=parent::NOT_DELETED) {
		$result = null;

		$sql = 'SELECT users.*'
		     . ' FROM  users'
		     . ' WHERE user_name=?'
		     . ' AND   deleted=?';
		$stmt = $this->conn->prepare($sql);
		if($stmt->execute(array($userName, $deleted))) {
			if($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$result = $row;
			}
		}
		$stmt->closeCursor();

		return $result;
	}
}
