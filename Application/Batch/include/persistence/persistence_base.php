<?php
/**
 * Base class for persistence layer.
 * @author Satoshi Mamada
 */
class PersistenceBase {

	/** [Const] Value - DELETED */
	const DELETED = 1;
	/** [Const] Value - NOT DELETED */
	const NOT_DELETED = 0;

	/** Database Connection */
	protected $conn;

	/**
	 * Constructor.
	 * Keeps connection to DB.
	 * @param PDO $conn conntction to DB
	 */
	public function __construct($conn) {
		$this->conn = $conn;
	}
}
