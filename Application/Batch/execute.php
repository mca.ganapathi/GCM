<?php
/**
 * Batch execution script - Main
 */

//=====================
// Define - Valiables
//=====================
ini_set('display_errors', 0);  // Uncomment when git push.
define('BATCH_ROOT', dirname(__FILE__));
define('LOG_DIR', '/var/log/batch/'); // If modified fileAppender in log4php.xml, then modify 'LOG_DIR' value for same directory.

// Reuires common library, modules.
require_once('config/environment.php');
require_once('lib/log4php/Logger.php');
require_once('lib/Cake/Utility/Inflector.php');
require_once('utility.php');

//=====================
//  Entry Point(Main)
//=====================

// Checks number of command-line arguments.
if(count($argv) < 2) { // No Argument
	print('php ' . basename(__FILE__) . ' BatchModuleName [arguments...]' ."\n");
	exit(-1);
}
// Checks concrete source of batch module is exist.
array_shift($argv);                 // To ignore the first element.
$batchModule = array_shift($argv);  // Pick out the module name.
$arrBatchModule = explode('.', $batchModule);
$batchClassName = array_pop($arrBatchModule);
$batchSourceName = Inflector::underscore($batchClassName);
$batchSourceFullpath = (count($arrBatchModule) < 1 ? '' : join('/', $arrBatchModule) . '/') . $batchSourceName . '.php';
if(!is_file($batchSourceFullpath)) {
	print("BatchModule '" . $batchModule ."' was not exist.\n");
	print('(It must be placed to ' . $batchSourceFullpath . ')');
	exit(-1);
}

// Reuires batch modules.
require_once('batch_base.php');
require_once($batchSourceFullpath);
// Checks to class definition is exist(e.g. Class was loaded)
if(!class_exists($batchClassName)) {
	throw new Exception("Class \"" . $batchClassName ."\" is not exist in " . $batchSourceFullpath);
}

// (Prepare) Configure logger and creates instance in global.
Logger::configure('config/log4php.xml');
$logger = Logger::getLogger("batchLogger");
$logger->getAppender("fileAppender")->setModuleName($batchModule);

// Create Instance
$batchClassInstance = null;
try {
	$batchClassInstance = new $batchClassName($batchModule);
} catch(Exception $e) {
	$logger->error("Exception occured when class '" . $batchClassName . "' create new instance.", $e);
	exit(-1);
}
// Checks whether the instance is a RestfulProxy
if(!($batchClassInstance instanceof BatchBase)) {
	$logger->error("Bad implementation class '" . $batchClassName ."' in " . $batchSourceFullpath);
	exit(-1);
}

// Calls doBatch method.
$returnCode = 0; // Default value if batch module returns no value.
try {
	$logger->info('Batch Process Start - Module: ' . $batchModule);
	$returnCode = $batchClassInstance->doBatch($argv);
	$logger->info('Batch Process End - ReturnCode: ' . $returnCode);

} catch(Exception $e) {
	$returnCode = -1;
	$logger->error('Batch Process End (Exception occured)', $e);
}
// Exit process.
exit($returnCode);

?>