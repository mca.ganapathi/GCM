<?php

class LoggerAppenderDailyModuleFile extends LoggerAppenderDailyFile {

	private $moduleName = '';

	/**
	 * Sets the 'moduleName' parameter.
	 * @param string $moduleName
	 */
	public function setModuleName($moduleName) {
		$this->moduleName = $moduleName;
	}

	/**
	 * Determines target file. Replaces %m in file path with a moduleName.
	 * @override
	 */
	protected function getTargetFile() {
		return str_replace('%m', $this->moduleName, parent::getTargetFile());
	}
}
