# Git Project of GCM Project
---

Administrator: Naoto Yoshida  
E-mail:<naoto.yoshida@ntt.com>

# Directory Structure
---
## Application
----------
Place program codes about SugarCRM Program & Test

## Chef
----------
Place chef codes(cookbooks)

## Database
----------
Place DDL & DML

# Documents
----------
Place documents about GCM(None-Use --> To use Redmine)

# Scripts
----------
Place code of Jenkins/Selenium/Some GCM Job etcetc...

# Git Commit Rule
----------

1. Separate subject from body with a blank line
2. Limit the subject line to 50 characters
3. Capitalize the subject line
4. Do not end the subject line with a period
5. Use the imperative mood in the subject line
6. Wrap the body at 72 characters
7. Use the body to explain what and why vs. how

Points to Note: 

1. Commit messages with bodies are not so easy to write with the -m option. You're better off writing the message in a proper text editor. 

2. Not every commit requires both a subject and a body. Sometimes a single line(subject) is fine, especially when the change is so simple that no further context is necessary
