-- ===============================================================
--     SQL for OM-308. Script to return non-null records info
-- ===============================================================


--
-- Accounts info and related Contracts - Contract Header Level
--

SELECT '===SELECT STRAIGHT_JOIN DISTINCT
    gc.global_contract_id     AS "Global Contract Id", 
	gc.id					  AS "Contract Object Id",
	gc.contract_version	  	  AS "Contract Version",
    c.id                      AS "Account Object Id",
    c.title                   AS "Account Title (Latin Character)",
    c.phone_mobile            AS "Account Mobile number",
    cstm.addr_2_c             AS "Account Address (Local Character)"
FROM
	gc_contracts gc    
  JOIN contacts_gc_contracts_1_c AS cgc ON cgc.contacts_gc_contracts_1gc_contracts_idb = gc.id AND gc.deleted = 0 AND cgc.deleted = 0    
	JOIN accounts_contacts AS ac_cnts ON ac_cnts.contact_id = cgc.contacts_gc_contracts_1contacts_ida AND ac_cnts.deleted = 0
  JOIN accounts AS ac ON ac.id = ac_cnts.account_id AND ac.deleted = 0
  JOIN  `accounts_cstm` AS acnt_cstm ON acnt_cstm.id_c = ac.id
	JOIN contacts_cstm AS cstm ON cstm.id_c = cgc.contacts_gc_contracts_1contacts_ida
	JOIN contacts AS c ON c.id = cstm.id_c
WHERE
  IFNULL(c.title, '') <> ''
  OR
  IFNULL(c.phone_mobile, '') <> ''
  OR
  IFNULL(cstm.addr_2_c, '') <> ''
 ORDER BY c.last_name, gc.global_contract_id
;' AS '';

SELECT STRAIGHT_JOIN DISTINCT
    gc.global_contract_id     AS "Global Contract Id", 
	gc.id					  AS "Contract Object Id",
	gc.contract_version	  	  AS "Contract Version",
    c.id                      AS "Account Object Id",
    c.title                   AS "Account Title (Latin Character)",
    c.phone_mobile            AS "Account Mobile number",
    cstm.addr_2_c             AS "Account Address (Local Character)"
FROM
	gc_contracts gc    
  JOIN contacts_gc_contracts_1_c AS cgc ON cgc.contacts_gc_contracts_1gc_contracts_idb = gc.id AND gc.deleted = 0 AND cgc.deleted = 0    
	JOIN accounts_contacts AS ac_cnts ON ac_cnts.contact_id = cgc.contacts_gc_contracts_1contacts_ida AND ac_cnts.deleted = 0
  JOIN accounts AS ac ON ac.id = ac_cnts.account_id AND ac.deleted = 0
  JOIN  `accounts_cstm` AS acnt_cstm ON acnt_cstm.id_c = ac.id
	JOIN contacts_cstm AS cstm ON cstm.id_c = cgc.contacts_gc_contracts_1contacts_ida
	JOIN contacts AS c ON c.id = cstm.id_c
WHERE
  IFNULL(c.title, '') <> ''
  OR
  IFNULL(c.phone_mobile, '') <> ''
  OR
  IFNULL(cstm.addr_2_c, '') <> ''
 ORDER BY c.last_name, gc.global_contract_id
;
 
--
-- Accounts related to Contracts - Line Items level
-- 

SELECT '===SELECT DISTINCT
	gl.global_contract_id     AS "Global Contract Id", 
	gl.id					  AS "Contract Object Id",
	gl.contract_version	      AS "Contract Version",
	c.id                      AS "Account Object Id",
	c.title                   AS "Account Title (Latin Character)",
	c.phone_mobile            AS "Account Mobile number",
	cstm.addr_2_c             AS "Account Address (Local Character)"
FROM
	gc_contracts g
  JOIN (SELECT gc.global_contract_id, gc.id, gc.contract_version, gln.tech_account_id AS account_id
    FROM gc_contracts gc 
      JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = gc.id AND gln.deleted = 0 
          AND gln.tech_account_id IS NOT NULL
   UNION 
   SELECT gc.global_contract_id, gc.id, gc.contract_version, gln.bill_account_id AS account_id
    FROM gc_contracts gc 
      JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = gc.id AND gln.deleted = 0 
           AND gln.bill_account_id IS NOT NULL
   ) AS gl ON gl.global_contract_id = g.global_contract_id AND gl.id = g.id
  JOIN accounts_contacts AS ac_cnts ON ac_cnts.contact_id = gl.account_id AND ac_cnts.deleted = 0
  JOIN accounts AS ac ON ac.id = ac_cnts.account_id AND ac.deleted = 0
  JOIN accounts_cstm AS acnt_cstm ON acnt_cstm.id_c = ac.id
  JOIN contacts_cstm AS cstm ON cstm.id_c = ac_cnts.contact_id
  JOIN contacts AS c ON c.id = cstm.id_c AND c.deleted = 0
WHERE
  IFNULL(c.title, '') <> ''
  OR
  IFNULL(c.phone_mobile, '') <> ''
  OR
  IFNULL(cstm.addr_2_c, '') <> ''
 ORDER BY g.date_entered DESC
;' AS '';

SELECT DISTINCT
	gl.global_contract_id     AS "Global Contract Id", 
	gl.id					  AS "Contract Object Id",
	gl.contract_version	      AS "Contract Version",
	c.id                      AS "Account Object Id",
	c.title                   AS "Account Title (Latin Character)",
	c.phone_mobile            AS "Account Mobile number",
	cstm.addr_2_c             AS "Account Address (Local Character)"
FROM
	gc_contracts g
  JOIN (SELECT gc.global_contract_id, gc.id, gc.contract_version, gln.tech_account_id AS account_id
    FROM gc_contracts gc 
      JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = gc.id AND gln.deleted = 0 
          AND gln.tech_account_id IS NOT NULL
   UNION 
   SELECT gc.global_contract_id, gc.id, gc.contract_version, gln.bill_account_id AS account_id
    FROM gc_contracts gc 
      JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = gc.id AND gln.deleted = 0 
           AND gln.bill_account_id IS NOT NULL
   ) AS gl ON gl.global_contract_id = g.global_contract_id AND gl.id = g.id
  JOIN accounts_contacts AS ac_cnts ON ac_cnts.contact_id = gl.account_id AND ac_cnts.deleted = 0
  JOIN accounts AS ac ON ac.id = ac_cnts.account_id AND ac.deleted = 0
  JOIN accounts_cstm AS acnt_cstm ON acnt_cstm.id_c = ac.id
  JOIN contacts_cstm AS cstm ON cstm.id_c = ac_cnts.contact_id
  JOIN contacts AS c ON c.id = cstm.id_c AND c.deleted = 0
WHERE
  IFNULL(c.title, '') <> ''
  OR
  IFNULL(c.phone_mobile, '') <> ''
  OR
  IFNULL(cstm.addr_2_c, '') <> ''
 ORDER BY g.date_entered DESC
;

 
 -- 
 -- Sales Representative Title / Mobile no.
 --
 
SELECT '===SELECT 
  gc.global_contract_id     AS "Global Contract Id",
  gc.id     				AS "Contract Object Id",
  gc.contract_version	  	AS "Contract Version",
  rep.id      				AS "Sales Representative Object ID",
  rep.title_1               AS "Sales Representative Title (Latin Character)",
  rep.mob_no                AS "Sales Representative Mobile number"
FROM
    gc_contracts gc
    JOIN gc_salesrepresentative AS rep ON rep.contracts_id = gc.id AND rep.deleted = 0 AND gc.deleted = 0
 WHERE
     IFNULL(rep.title_1, '') <> ''
     OR 
     IFNULL(rep.mob_no, '') <> ''
 ORDER BY rep.sales_rep_name_1 , gc.global_contract_id     
 ;' AS '';
 
 SELECT 
  gc.global_contract_id     AS "Global Contract Id",
  gc.id     				AS "Contract Object Id",
  gc.contract_version	  	AS "Contract Version",
  rep.id      				AS "Sales Representative Object ID",
  rep.title_1               AS "Sales Representative Title (Latin Character)",
  rep.mob_no                AS "Sales Representative Mobile number"
FROM
    gc_contracts gc
    JOIN gc_salesrepresentative AS rep ON rep.contracts_id = gc.id AND rep.deleted = 0 AND gc.deleted = 0
 WHERE
     IFNULL(rep.title_1, '') <> ''
     OR 
     IFNULL(rep.mob_no, '') <> ''
 ORDER BY rep.sales_rep_name_1 , gc.global_contract_id     
 ;
 
 --
 -- Direct Deposit Payee Contact Person Name (Latin Character)
 --
 
 SELECT '===SELECT
  gc.global_contract_id             AS "Global Contract Id",
  gc.id     						AS "Contract Object Id",
  gc.contract_version	  			AS "Contract Version",
  dep.id   							AS "Direct Deposit Object ID",
  dep.payee_contact_person_name_1   AS "Direct Deposit Payee Contact Person Name (Latin Character)"
  
FROM
    gc_contracts gc
    JOIN gc_line_item_contract_history gl ON gl.contracts_id = gc.id AND gc.deleted = 0 AND gl.deleted = 0
    JOIN gc_directdeposit AS dep ON dep.contract_history_id = gl.id AND dep.deleted = 0
 WHERE
     IFNULL(dep.payee_contact_person_name_1, '') <> ''
 ORDER BY dep.payee_contact_person_name_1, gc.global_contract_id     
 ;' AS '';
 
 SELECT
  gc.global_contract_id             AS "Global Contract Id",
  gc.id     						AS "Contract Object Id",
  gc.contract_version	  			AS "Contract Version",
  dep.id   							AS "Direct Deposit Object ID",
  dep.payee_contact_person_name_1   AS "Direct Deposit Payee Contact Person Name (Latin Character)"
  
FROM
    gc_contracts gc
    JOIN gc_line_item_contract_history gl ON gl.contracts_id = gc.id AND gc.deleted = 0 AND gl.deleted = 0
    JOIN gc_directdeposit AS dep ON dep.contract_history_id = gl.id AND dep.deleted = 0
 WHERE
     IFNULL(dep.payee_contact_person_name_1, '') <> ''
 ORDER BY dep.payee_contact_person_name_1, gc.global_contract_id     
 ;
 