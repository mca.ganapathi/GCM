-- =============================================
--     SQL for DF-1050
-- =============================================


-- 
--
-- Alter Query contacts_cstm 
--
ALTER TABLE `contacts_cstm` ADD `attention_line_c` varchar(100) DEFAULT NULL;

-- ------------------------------------------------------------------------------------------------------------------

-- =============================================
--   Confirm Table structure
-- =============================================
SELECT '=== DESC contacts_cstm' AS '';      DESC contacts_cstm;