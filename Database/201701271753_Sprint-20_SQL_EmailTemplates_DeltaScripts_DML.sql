-- =============================================
-- SQL for Code_Correction_SugarCRM_Upgrade git branch
-- =============================================
/* Data for the table `email_templates` */

UPDATE `email_templates` SET `subject` = 'Contract ({::future::gc_Contracts::global_contract_id::}) – Actioned by Approver', `body` = 'Dear Team members,\r\nContract, {::future::gc_Contracts::global_contract_id::} is submitted to workflow by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.\r\n\r\nKindly view the details at GCM.\r\n\r\nURL for contract id: {::href_link::gc_Contracts::href_link::}\r\n\r\n\r\nBest Regards,\r\nGCM Contracts', `body_html` = '<div id=\"sugar_text_html_div\" style=\"background-color:#ffffff;padding:5px;\">\r\n<p>Dear Team members,<br />Contract, {::future::gc_Contracts::global_contract_id::} is submitted to workflow by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.</p>\r\n<p>Kindly view the details at GCM.</p>\r\n<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>\r\n<p><br />Best Regards,<br />GCM Contracts</p>\r\n</div>' WHERE id = '66fef16f-0b23-4fbf-90fa-b4dfd03e963e';

UPDATE `email_templates` SET `body` = 'Dear Role members,\r\nContract, {::future::gc_Contracts::global_contract_id::} is submitted to your approval via workflow engine.\r\nKindly approve or reject the contract with appropriate comments.\r\nURL for contract id: {::href_link::gc_Contracts::href_link::}\r\n\r\nBest Regards,\r\nGCM Contracts', `body_html` = '<p>Dear Role members,<br />Contract, {::future::gc_Contracts::global_contract_id::} is submitted to your approval via workflow engine.<br />Kindly approve or reject the contract with appropriate comments.<br />URL for contract id: {::href_link::gc_Contracts::href_link::}</p>\r\n<p><br />Best Regards,<br />GCM Contracts</p>' WHERE id = '923424e3-0b23-4fbc-b608-60603610c971';

UPDATE `email_templates` SET `body` = 'Dear Team member,\r\n\r\nContract, {::future::gc_Contracts::global_contract_id::} is approved by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.\r\n\r\nKindly view the details at GCM.\r\n\r\nURL for contract id: {::href_link::gc_Contracts::href_link::}\r\n\r\n \r\n\r\nBest Regards,\r\n\r\nGCM Contracts', `body_html` = '<p>Dear Team member,</p>\r\n<p>Contract, {::future::gc_Contracts::global_contract_id::} is approved by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.</p>\r\n<p>Kindly view the details at GCM.</p>\r\n<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>\r\n<p> </p>\r\n<p>Best Regards,</p>\r\n<p>GCM Contracts</p>' WHERE id = '46a712f5-0b23-4fb3-9878-c896795871dc';

UPDATE `email_templates` SET `body` = 'Dear Team member,\r\n\r\nContract, {::future::gc_Contracts::global_contract_id::} is rejected by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.\r\n\r\nKindly view the details at GCM.\r\n\r\nURL for contract id: {::href_link::gc_Contracts::href_link::}\r\n\r\n \r\n\r\nBest Regards,\r\n\r\nGCM Contracts', `body_html` = '<p>Dear Team member,</p>\r\n<p>Contract, {::future::gc_Contracts::global_contract_id::} is rejected by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.</p>\r\n<p>Kindly view the details at GCM.</p>\r\n<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>\r\n<p> </p>\r\n<p>Best Regards,</p>\r\n<p>GCM Contracts</p>' WHERE id = 'b74da531-0b23-4fb1-8652-d6cac54ffa00';

UPDATE `email_templates` SET `subject` = 'Contract  ({::future::gc_Contracts::global_contract_id::}) - Actioned by Approver' WHERE id = 'f62390c5-0b23-4fbe-b734-a53ec7368300';


-- =============================================
-- Validate the Table data
-- =============================================
SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '66fef16f-0b23-4fbf-90fa-b4dfd03e963e';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '66fef16f-0b23-4fbf-90fa-b4dfd03e963e';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '923424e3-0b23-4fbc-b608-60603610c971';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '923424e3-0b23-4fbc-b608-60603610c971';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '46a712f5-0b23-4fb3-9878-c896795871dc';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '46a712f5-0b23-4fb3-9878-c896795871dc';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'b74da531-0b23-4fb1-8652-d6cac54ffa00';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'b74da531-0b23-4fb1-8652-d6cac54ffa00';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'f62390c5-0b23-4fbe-b734-a53ec7368300';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'f62390c5-0b23-4fbe-b734-a53ec7368300';