-- =============================================
--     SQL for DF-1473
-- =============================================

-- 
--
-- /* Data for the table `fields_meta_data ` - required, len and audited */
--

UPDATE `fields_meta_data` SET `required` = 1, `len`=100, `audited`=1 WHERE `id`='Contactsname_2_c';

UPDATE `fields_meta_data` SET `audited` = 0 WHERE `id`='Contactsaddr_2_c';

-- ------------------------------------------------------------------------------------------------------------------

-- =============================================
--   Validate the table data
-- =============================================

SELECT "=== SELECT id, required, len, audited FROM fields_meta_data WHERE  id = 'Contactsname_2_c';" AS ''; 
SELECT id, required, len, audited FROM fields_meta_data WHERE  id = 'Contactsname_2_c';

SELECT "=== SELECT id, audited FROM fields_meta_data WHERE  id = 'Contactsaddr_2_c';" AS ''; 
SELECT id, audited FROM fields_meta_data WHERE  id = 'Contactsaddr_2_c';


