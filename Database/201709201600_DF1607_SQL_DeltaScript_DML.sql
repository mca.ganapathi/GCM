-- =============================================================================================
--
--  SQL for DF-1607.
--  
-- Last updated: 2017/Sep/21
-- =============================================================================================

--
-- Step 1: Create temporary tables and populate contract records which satifies the data patch condition.
--

-- Query 1: Update Contract Description
--
CREATE TEMPORARY TABLE tmp_gc_contracts LIKE gc_contracts;

-- Populate records that satifies the data patch condition
INSERT INTO tmp_gc_contracts
	(
	 id,
	 global_contract_id,
	 description,
	 date_modified,
	 modified_user_id
	)
SELECT
	g.id				 ,
	g.global_contract_id ,
	g.description		 ,
	g.date_modified		 ,
	g.modified_user_id
FROM 
	gc_contracts g
WHERE 
	product_offering = '2808'
	AND global_contract_id <= 120000000001498
	AND TRIM(IFNULL(description, '')) = ''
;


--
-- Query 2: Update Tech account to NULL
--
CREATE TEMPORARY TABLE tmp_gc_contracts_tech_ac LIKE gc_line_item_contract_history ;
ALTER TABLE tmp_gc_contracts_tech_ac 
	ADD gc_date_modified DATETIME, ADD gc_modified_user_id VARCHAR(40);

-- Populate records that satifies the data patch condition
INSERT INTO tmp_gc_contracts_tech_ac
	(
	 id,
	 contracts_id,
	 tech_account_id,
	 date_modified,
	 modified_user_id,
	 gc_date_modified,
	 gc_modified_user_id
	)
SELECT
	gl.id			,
	gl.contracts_id		,
	gl.tech_account_id	,
	gl.date_modified	,
	gl.modified_user_id	,
	g.date_modified		,
	g.modified_user_id
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gl ON gl.contracts_id = g.id AND IFNULL(gl.tech_account_id, '') <> ''
  JOIN contacts_cstm AS cstm ON cstm.id_c = gl.tech_account_id
  JOIN contacts AS c ON c.id = cstm.id_c
 WHERE
  g.product_offering = '2808'
  AND g.global_contract_id <= 120000000001498
  AND IFNULL(c.last_name, '') = ''
  AND IFNULL(cstm.name_2_c, '') = ''
  AND IFNULL(cstm.division_1_c, '') = ''
  AND IFNULL(c.title, '') = ''
  AND IFNULL(c.phone_work, '') = ''
  AND IFNULL(c.phone_fax, '') = ''
  AND IFNULL(cstm.division_2_c, '') = ''
  AND IFNULL(cstm.title_2_c, '') = ''
  AND IFNULL(cstm.ext_no_c, '') = ''
  AND IFNULL(c.phone_mobile, '') = ''
  AND IFNULL(cstm.c_country_id_c, '') = ''
  AND IFNULL(cstm.postal_no_c, '') = ''
  AND IFNULL(cstm.addr_1_c, '') = ''
  AND IFNULL(cstm.addr_2_c, '') = ''
  AND IFNULL(cstm.attention_line_c, '') = ''
  AND IFNULL(cstm.addr_general_c, '') = ''
  AND IFNULL(cstm.city_general_c, '') = ''
  AND IFNULL(cstm.c_state_id_c, '') = ''
  AND NOT EXISTS 
 (
  SELECT 1 FROM email_addr_bean_rel emr
  JOIN email_addresses em ON em.id = emr.email_address_id AND IFNULL(em.email_address, '') != ''
    WHERE emr.bean_id = c.id
 )
  AND 
   NOT EXISTS 
 (SELECT 1 FROM js_accounts_js js
  WHERE js.contact_id_c = c.id 
   AND IFNULL(js.name, '') != ''
   AND IFNULL(js.description, '') != ''
   AND IFNULL(js.addr1, '') != ''
   AND IFNULL(js.addr2, '') != ''
 )
;

--
-- Query 3: Update customer billing currency to JPY
--
CREATE TEMPORARY TABLE tmp_gc_contracts_custbill_curr LIKE gc_line_item_contract_history ;
ALTER TABLE tmp_gc_contracts_custbill_curr 
	ADD gc_date_modified DATETIME, ADD gc_modified_user_id VARCHAR(40);

-- Populate records that satifies the data patch condition
INSERT INTO tmp_gc_contracts_custbill_curr
	(
	 id,
	 contracts_id,
	 cust_billing_currency,
	 date_modified,
	 modified_user_id,
	 gc_date_modified,
	 gc_modified_user_id
	)
SELECT
	gln.id			,
	gln.contracts_id,
	gln.cust_billing_currency,
	gln.date_modified,
	gln.modified_user_id,
	g.date_modified,
	g.modified_user_id
FROM
   gc_contracts g
   JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
   JOIN contacts_cstm AS cstm ON cstm.id_c = gln.bill_account_id
   JOIN c_country AS cty  ON cty.id = cstm.c_country_id_c
   JOIN contacts AS c ON c.id = cstm.id_c
 WHERE
   g.product_offering = '2808'
   AND g.global_contract_id <= 120000000001498
   AND cty.country_code_numeric = '392'
   AND (gln.cust_billing_currency IS NULL OR TRIM(gln.cust_billing_currency) = '')
;

--
-- Query 4: Update Payment type to NULL
--
CREATE TEMPORARY TABLE tmp_gc_contracts_payment_type LIKE gc_line_item_contract_history ;
ALTER TABLE tmp_gc_contracts_payment_type 
	ADD gc_date_modified DATETIME, ADD gc_modified_user_id VARCHAR(40);

-- Populate records that satifies the data patch condition
INSERT INTO tmp_gc_contracts_payment_type
	(
	 id,
	 contracts_id,
	 payment_type,
	 date_modified,
	 modified_user_id,
	 gc_date_modified,
	 gc_modified_user_id
	)
SELECT
	gln.id,
	gln.contracts_id,
	gln.payment_type,
	gln.date_modified,
	gln.modified_user_id,
	g.date_modified,
	g.modified_user_id
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
        AND gln.payment_type = '00'
        AND g.product_offering = '2808'
        AND g.global_contract_id <= 120000000001498
  JOIN contacts_cstm AS cstm ON cstm.id_c = gln.bill_account_id
  JOIN c_country AS cty  ON cty.id = cstm.c_country_id_c AND cty.country_code_numeric != '392'
  JOIN contacts AS c ON c.id = cstm.id_c
  LEFT JOIN gc_directdeposit dp ON dp.contract_history_id = gln.id
 WHERE
   dp.id IS NULL
   OR
   (
    dp.id IS NOT NULL 
    AND IFNULL(dp.name, '') = '' 
    AND IFNULL(dp.bank_code, '') = ''
    AND IFNULL(branch_code, '') = ''
    AND IFNULL(deposit_type, '') = ''
    AND IFNULL(account_no, '') = ''
    AND IFNULL(payee_contact_person_name_1, '') = ''
    AND IFNULL(payee_contact_person_name_2, '') = ''
    AND IFNULL(payee_tel_no, '') = ''
    AND IFNULL(payee_email_address, '') = ''
    AND IFNULL(remarks, '') = ''
  )
;
 

--
-- Query 5: Update Billing end date
--
CREATE TEMPORARY TABLE tmp_gc_contracts_billing_enddate LIKE gc_line_item_contract_history ;
ALTER TABLE tmp_gc_contracts_billing_enddate 
	ADD gc_date_modified DATETIME, ADD gc_modified_user_id VARCHAR(40);

-- Populate records that satifies the data patch condition
INSERT INTO tmp_gc_contracts_billing_enddate
	(
	 id,
	 contracts_id,
	 billing_end_date,
	 date_modified,
	 modified_user_id,
	 gc_date_modified,
	 gc_modified_user_id
	)
SELECT 
	gln.id,
	gln.contracts_id,
	gln.billing_end_date,
	gln.date_modified,
	gln.modified_user_id,
	g.date_modified,
	g.modified_user_id
FROM
     gc_contracts g 
 JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id  
 JOIN 
     (SELECT tz.utcoffset, c.id FROM c_country c 
         JOIN gc_timezone tz ON tz.c_country_id_c = c.id
  WHERE
     c.country_code_numeric = '392'
 ) AS jst 
 JOIN gc_timezone AS tz ON tz.id = gln.gc_timezone_id3_c
   AND tz.utcoffset = jst.utcoffset
   AND (IFNULL(tz.c_country_id_c, '') = '' OR tz.c_country_id_c = jst.id)
 WHERE    
    g.product_offering = '2808'
    AND g.global_contract_id <= 120000000001498
    AND (gln.billing_end_date = '9999-12-31 14:59:59' OR gln.billing_end_date = '9999-12-31 06:00:00')
;


--
-- Query 6: Service end date
--
CREATE TEMPORARY TABLE tmp_gc_contracts_service_enddate LIKE gc_line_item_contract_history ;
ALTER TABLE tmp_gc_contracts_service_enddate 
	ADD gc_date_modified DATETIME, ADD gc_modified_user_id VARCHAR(40);

-- Populate records that satifies the data patch condition
INSERT INTO tmp_gc_contracts_service_enddate
	(
	 id,
	 contracts_id,
	 service_end_date,
	 date_modified,
	 modified_user_id,
	 gc_date_modified,
	 gc_modified_user_id
	)
SELECT 
	gln.id,
	gln.contracts_id,
	gln.service_end_date,
	gln.date_modified,
	gln.modified_user_id,
	g.date_modified,
	g.modified_user_id
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
  JOIN 
     (SELECT tz.utcoffset, c.id FROM c_country c
    JOIN gc_timezone tz ON tz.c_country_id_c = c.id
   WHERE
   c.country_code_numeric = '392'
  ) AS jst
  JOIN gc_timezone AS tz ON tz.id = gln.gc_timezone_id1_c
AND tz.utcoffset = jst.utcoffset
AND (IFNULL(tz.c_country_id_c, '') = '' OR tz.c_country_id_c = jst.id)
WHERE
  g.product_offering = '2808'
  AND g.global_contract_id <= 120000000001498
  AND
  (gln.service_end_date = '9999-12-31 14:59:59' OR gln.service_end_date = '9999-12-31 06:00:00')
;
 
--
-- Query 7:  Update contract start/end date as service start/end date where contract dates are empty
--
CREATE TEMPORARY TABLE tmp_gc_contracts_agreement_dates LIKE gc_contracts ;

-- Populate records that satifies the data patch condition
INSERT INTO tmp_gc_contracts_agreement_dates
	(
	 id,
	 global_contract_id,
	 contract_startdate,
	 contract_enddate,
	 date_modified,
	 modified_user_id,
	 gc_timezone_id_c,
	 gc_timezone_id1_c
	)
SELECT 
	gln.id,
	g.global_contract_id,
	g.contract_startdate,
	g.contract_enddate,
	g.date_modified,
	g.modified_user_id,
	g.gc_timezone_id_c,
	g.gc_timezone_id1_c
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
        AND gln.service_start_date IS NOT NULL AND gln.service_end_date IS NOT NULL
 WHERE
   g.product_offering = '2808'
  AND g.global_contract_id <= 120000000001498
    AND 
   (
    IFNULL(g.contract_startdate, '0000-00-00 00:00:00') = '0000-00-00 00:00:00' 
    AND 
    IFNULL(g.contract_enddate, '0000-00-00 00:00:00') = '0000-00-00 00:00:00'
   )
;


-- -
-- -     Data patch update statements.
-- -


-- Specify the User account created for the purpose of applying Data patch; eg: support@ntt.com
-- Provide a valid user account name below:

SET @modified_user_name := "SYS_GCM_BATCH";

--
SELECT id INTO @modified_user FROM users WHERE user_name = @modified_user_name;
SET @timestamp := UTC_TIMESTAMP();

SELECT "=== SELECT @modified_user AS 'User Id', @modified_user_name AS 'User Name' ==="
;

SELECT @modified_user AS "User Id", @modified_user_name AS "User Name";
--
-- Step 3: Run data patch
-- 

--
START TRANSACTION;

-- Data patch- Query 1: Update contract description to 'Enterprise Cloud 2.0'
--
UPDATE gc_contracts g 
SET 
     description 	= 'Enterprise Cloud 2.0',
     date_modified 	= @timestamp,
     modified_user_id = @modified_user
WHERE 
     product_offering = '2808'
     AND global_contract_id <= 120000000001498
     AND TRIM(IFNULL(description, '')) = ''
;

-- Data patch- Query 2: Update Tech account Id to NULL for empty/dummy account informations
--
UPDATE gc_line_item_contract_history gln
 INNER JOIN tmp_gc_contracts_tech_ac t_gln ON t_gln.id = gln.id
 INNER JOIN gc_contracts g ON g.id = gln.contracts_id
SET 
    gln.tech_account_id = NULL,
    gln.date_modified 	= @timestamp,
    gln.modified_user_id = @modified_user,
    g.date_modified 	= @timestamp,
    g.modified_user_id 	= @modified_user
;

-- Data patch- Query 3: Update customer billing currency to 'JPY'
--
UPDATE gc_line_item_contract_history gln
 INNER JOIN tmp_gc_contracts_custbill_curr t_gln ON t_gln.id = gln.id
 INNER JOIN gc_contracts g ON g.id = gln.contracts_id
SET 
    gln.cust_billing_currency 	= 'JPY',
    gln.date_modified 			= @timestamp,
    gln.modified_user_id 		= @modified_user,
    g.date_modified 			= @timestamp,
    g.modified_user_id 			= @modified_user
;


-- Data patch- Query 4: Update payment type to NULL, for billing customers country is other than Japan and their payment details are empty.
--
UPDATE gc_line_item_contract_history gln
 INNER JOIN tmp_gc_contracts_payment_type t_gln ON t_gln.id = gln.id
 INNER JOIN gc_contracts g ON g.id = gln.contracts_id
SET 
    gln.payment_type = NULL,
    gln.date_modified = @timestamp,
    gln.modified_user_id = @modified_user,
	g.date_modified 	= @timestamp,
    g.modified_user_id 	= @modified_user
;

-- Data patch- Query 4, continued.., mark direct deposit records as deleted.
UPDATE gc_directdeposit dp
   INNER JOIN tmp_gc_contracts_payment_type tgc ON tgc.id = dp.contract_history_id
SET 
	dp.deleted 		    = 1,
    dp.date_modified	= @timestamp,
    dp.modified_user_id = @modified_user
;


-- Data patch- Query 5: Update billing end date to display date as '9000-01-01 00:00:00'
--
UPDATE gc_line_item_contract_history gln
 INNER JOIN tmp_gc_contracts_billing_enddate t_gln ON t_gln.id = gln.id
 INNER JOIN gc_contracts g ON g.id = gln.contracts_id
SET 
    gln.billing_end_date = '9000-01-01 00:00:00',
    gln.gc_timezone_id3_c = (SELECT tz.id FROM gc_timezone tz WHERE tz.name = 'UTC' AND IFNULL(tz.c_country_id_c, '') = ''),
    gln.date_modified = @timestamp,
    gln.modified_user_id = @modified_user,
	g.date_modified 	 = @timestamp,
    g.modified_user_id 	 = @modified_user
;


-- Data patch- Query 6: Update service end date to display date as '9000-01-01 00:00:00'
--
UPDATE gc_line_item_contract_history gln
 INNER JOIN tmp_gc_contracts_service_enddate t_gln ON t_gln.id = gln.id
 INNER JOIN gc_contracts g ON g.id = gln.contracts_id
SET 
    gln.service_end_date = '9000-01-01 00:00:00',
    gln.gc_timezone_id1_c = (SELECT tz.id FROM gc_timezone tz WHERE tz.name = 'UTC' AND IFNULL(tz.c_country_id_c, '') = ''),
    gln.date_modified = @timestamp,
    gln.modified_user_id = @modified_user,
	g.date_modified 	 = @timestamp,
    g.modified_user_id 	 = @modified_user
;


-- Data patch- Query 7: Update contract start/end date as service start/end date where contract dates are empty
--
UPDATE gc_contracts g
 INNER JOIN gc_line_item_contract_history gln ON gln.contracts_id = g.id
 INNER JOIN tmp_gc_contracts_agreement_dates t_gln ON t_gln.id = gln.id
SET 
    g.contract_startdate 	= gln.service_start_date,
    g.contract_enddate	 	= gln.service_end_date,
	g.gc_timezone_id_c		= gln.gc_timezone_id_c,
	g.gc_timezone_id1_c		= gln.gc_timezone_id1_c,
    g.date_modified 		= @timestamp,
    g.modified_user_id      = @modified_user
;



--
-- Step 4: Data verifications SQLs
--

-- Verification Query 1: Contract Description
SELECT 
"=== 
SELECT
	g.id					AS 'Contract Object Id',
	g.global_contract_id	AS 'Global Contract Id',
	tmp_gc.description		AS 'Contract Desc - Before Update',
	g.description			AS 'Contract Desc - After Update',
	tmp_gc.date_modified	AS 'Date Modified - Before Update',
	g.date_modified			AS 'Date Modified - After Update',
	tmp_gc.modified_user_id	AS 'Modified - Before Update',
	g.modified_user_id		AS 'Modified - After Update'
 FROM 
	tmp_gc_contracts tmp_gc
	JOIN gc_contracts g ON tmp_gc.id = g.id
 WHERE 
	g.product_offering 	= '2808'
	AND g.global_contract_id <= 120000000001498
 ORDER BY 
	g.global_contract_id
 ==="
;

 SELECT
	g.id					AS "Contract Object Id",
	g.global_contract_id	AS "Global Contract Id",
	tmp_gc.description		AS "Contract Desc - Before Update",
	g.description			AS "Contract Desc - After Update",
	tmp_gc.date_modified	AS "Date Modified - Before Update",
	g.date_modified			AS "Date Modified - After Update",
	tmp_gc.modified_user_id	AS "Modified - Before Update",
	g.modified_user_id		AS "Modified - After Update"
 FROM 
	tmp_gc_contracts tmp_gc
	JOIN gc_contracts g ON tmp_gc.id = g.id
 WHERE 
	g.product_offering 	= '2808'
	AND g.global_contract_id <= 120000000001498
 ORDER BY 
	g.global_contract_id
;

-- Verification Query 2: Tech Account
SELECT 
"=== 
SELECT
 g.id						AS 'Contract Id',
 g.global_contract_id		AS 'Global Contract Id',
 g.description				AS 'Contract Description',
 tmp_gc.gc_date_modified	AS 'GCM Date Modified - Before',
 g.date_modified            AS 'GCM Date Modified - After',
 tmp_gc.gc_modified_user_id	AS 'GCM Modified UserID - Before',
 g.modified_user_id         AS 'GCM Modified UserID - After',
 tmp_gc.tech_account_id		AS 'Tech AccountId - Before Update',
 gln.tech_account_id		AS 'Tech AccountId - After Update',
 tmp_gc.date_modified		AS 'Date Modified - Before Update',
 gln.date_modified			AS 'Date Modified - After Update',
 tmp_gc.modified_user_id	AS 'Modified User- Before Update',
 gln.modified_user_id		AS 'Date Modified - After Update'
FROM 
    tmp_gc_contracts_tech_ac tmp_gc    
    JOIN gc_line_item_contract_history gln ON tmp_gc.id = gln.id
    JOIN gc_contracts g ON g.id = tmp_gc.contracts_id
ORDER BY 
	g.global_contract_id
 ==="
;

SELECT
 g.id						AS "Contract Id",
 g.global_contract_id		AS "Global Contract Id",
 g.description				AS "Contract Description",
 tmp_gc.gc_date_modified	AS "GCM Date Modified - Before",
 g.date_modified            AS "GCM Date Modified - After",
 tmp_gc.gc_modified_user_id	AS "GCM Modified UserID - Before",
 g.modified_user_id         AS "GCM Modified UserID - After",
 tmp_gc.tech_account_id		AS "Tech AccountId - Before Update",
 gln.tech_account_id		AS "Tech AccountId - After Update",
 tmp_gc.date_modified		AS "Date Modified - Before Update",
 gln.date_modified			AS "Date Modified - After Update",
 tmp_gc.modified_user_id	AS "Modified User- Before Update",
 gln.modified_user_id		AS "Date Modified - After Update"
FROM 
    tmp_gc_contracts_tech_ac tmp_gc    
    JOIN gc_line_item_contract_history gln ON tmp_gc.id = gln.id
    JOIN gc_contracts g ON g.id = tmp_gc.contracts_id
ORDER BY 
	g.global_contract_id
;

-- Verification Query 3: Customer Billing Currency
SELECT 
"=== 
SELECT
 g.id						AS 'Contract Id',
 g.global_contract_id		AS 'Global Contract Id',
 g.description				AS 'Contract Description',
 tmp_gc.gc_date_modified	AS 'GCM Date Modified - Before',
 g.date_modified            AS 'GCM Date Modified - After',
 tmp_gc.gc_modified_user_id	AS 'GCM Modified UserID - Before',
 g.modified_user_id         AS 'GCM Modified UserID - After',
 tmp_gc.cust_billing_currency	AS 'Customer Billing Currency - Before Update',
 gln.cust_billing_currency	AS 'Customer Billing Currency - After Update',
 tmp_gc.date_modified		AS 'Date Modified - Before Update',
 gln.date_modified			AS 'Date Modified - After Update',
 tmp_gc.modified_user_id	AS 'Modified User - Before Update',
 gln.modified_user_id		AS 'Modified User - After Update'
FROM 
    tmp_gc_contracts_custbill_curr tmp_gc
    JOIN gc_line_item_contract_history gln ON tmp_gc.id = gln.id
	JOIN gc_contracts g ON g.id = tmp_gc.contracts_id
ORDER BY 
    g.global_contract_id
==="
;

SELECT
 g.id						AS "Contract Id",
 g.global_contract_id		AS "Global Contract Id",
 g.description				AS "Contract Description",
 tmp_gc.gc_date_modified	AS "GCM Date Modified - Before",
 g.date_modified            AS "GCM Date Modified - After",
 tmp_gc.gc_modified_user_id	AS "GCM Modified UserID - Before",
 g.modified_user_id         AS "GCM Modified UserID - After",
 tmp_gc.cust_billing_currency	AS "Customer Billing Currency - Before Update",
 gln.cust_billing_currency	AS "Customer Billing Currency - After Update",
 tmp_gc.date_modified		AS "Date Modified - Before Update",
 gln.date_modified			AS "Date Modified - After Update",
 tmp_gc.modified_user_id	AS "Modified User - Before Update",
 gln.modified_user_id		AS "Modified User - After Update"
FROM 
    tmp_gc_contracts_custbill_curr tmp_gc
    JOIN gc_line_item_contract_history gln ON tmp_gc.id = gln.id
	JOIN gc_contracts g ON g.id = tmp_gc.contracts_id
ORDER BY 
    g.global_contract_id
;


-- Verification Query 4: Payment type
SELECT 
"=== 
SELECT
 g.id					AS 'Contract Id',
 g.global_contract_id	AS 'Global Contract Id',
 g.description			AS 'Contract Description',
 tmp_gc.gc_date_modified	AS 'GCM Date Modified - Before',
 g.date_modified            AS 'GCM Date Modified - After',
 tmp_gc.gc_modified_user_id	AS 'GCM Modified UserID - Before',
 g.modified_user_id         AS 'GCM Modified UserID - After',
 tmp_gc.payment_type	AS 'Payment Type - Before Update',
 gln.payment_type		AS 'Payment Type - After Update',
 tmp_gc.date_modified	AS 'Date Modified - Before Update',
 gln.date_modified		AS 'Date Modified - After Update',
 tmp_gc.modified_user_id	AS 'Modified User- Before Update',
 gln.modified_user_id		AS 'Date Modified - After Update'
FROM 
    tmp_gc_contracts_payment_type tmp_gc
    JOIN gc_line_item_contract_history gln ON tmp_gc.id = gln.id
    JOIN gc_contracts g ON g.id = tmp_gc.contracts_id
ORDER BY 
	g.global_contract_id
==="
;

SELECT
 g.id					AS "Contract Id",
 g.global_contract_id	AS "Global Contract Id",
 g.description			AS "Contract Description",
 tmp_gc.gc_date_modified	AS "GCM Date Modified - Before",
 g.date_modified		AS "GCM Date Modified - After",
 tmp_gc.gc_modified_user_id	AS "GCM Modified UserID - Before",
 g.modified_user_id		AS "GCM Modified UserID - After",
 tmp_gc.payment_type	AS "Payment Type - Before Update",
 gln.payment_type		AS "Payment Type - After Update",
 tmp_gc.date_modified	AS "Date Modified - Before Update",
 gln.date_modified		AS "Date Modified - After Update",
 tmp_gc.modified_user_id	AS "Modified User- Before Update",
 gln.modified_user_id		AS "Date Modified - After Update"
FROM 
    tmp_gc_contracts_payment_type tmp_gc
    JOIN gc_line_item_contract_history gln ON tmp_gc.id = gln.id
    JOIN gc_contracts g ON g.id = tmp_gc.contracts_id
ORDER BY 
	g.global_contract_id
;

-- Verification Query 5: Billing End date
SELECT 
"=== 
SELECT
 g.id						AS 'Contract Id',
 g.global_contract_id		AS 'Global Contract Id',
 g.description				AS 'Contract Description',
 tmp_gc.gc_date_modified	AS 'GCM Date Modified - Before',
 g.date_modified            AS 'GCM Date Modified - After',
 tmp_gc.gc_modified_user_id	AS 'GCM Modified UserID - Before',
 g.modified_user_id         AS 'GCM Modified UserID - After',
 tmp_gc.billing_end_date	AS 'Billing End date - Before Update',
 gln.billing_end_date		AS 'Billing End date - After Update', 
 tmp_gc.date_modified		AS 'Date Modified - Before Update',
 gln.date_modified			AS 'Date Modified - After Update',
 tmp_gc.modified_user_id	AS 'Modified User- Before Update',
 gln.modified_user_id		AS 'Date Modified - After Update'
FROM
    tmp_gc_contracts_billing_enddate tmp_gc
    JOIN gc_line_item_contract_history gln ON tmp_gc.id = gln.id
    JOIN gc_contracts g ON g.id = tmp_gc.contracts_id
ORDER BY 
	g.global_contract_id
==="
;

SELECT
 g.id						AS "Contract Id",
 g.global_contract_id		AS "Global Contract Id",
 g.description				AS "Contract Description",
 tmp_gc.gc_date_modified	AS "GCM Date Modified - Before",
 g.date_modified		AS "GCM Date Modified - After",
 tmp_gc.gc_modified_user_id	AS "GCM Modified UserID - Before",
 g.modified_user_id		AS "GCM Modified UserID - After",
 tmp_gc.billing_end_date	AS "Billing End date - Before Update",
 gln.billing_end_date		AS "Billing End date - After Update", 
 tmp_gc.date_modified		AS "Date Modified - Before Update",
 gln.date_modified			AS "Date Modified - After Update",
 tmp_gc.modified_user_id	AS "Modified User- Before Update",
 gln.modified_user_id		AS "Date Modified - After Update"
FROM
    tmp_gc_contracts_billing_enddate tmp_gc
    JOIN gc_line_item_contract_history gln ON tmp_gc.id = gln.id
    JOIN gc_contracts g ON g.id = tmp_gc.contracts_id
ORDER BY 
	g.global_contract_id
;

-- Verification Query 6: Service End date
SELECT 
"=== 
SELECT
 g.id						AS 'Contract Id',
 g.global_contract_id		AS 'Global Contract Id',
 g.description				AS 'Contract Description',
 tmp_gc.gc_date_modified	AS 'GCM Date Modified - Before',
 g.date_modified            AS 'GCM Date Modified - After',
 tmp_gc.gc_modified_user_id	AS 'GCM Modified UserID - Before',
 g.modified_user_id         AS 'GCM Modified UserID - After',
 tmp_gc.service_end_date	AS 'Service End date - Before Update',
 gln.service_end_date		AS 'Service End date - After Update', 
 tmp_gc.date_modified		AS 'Date Modified - Before Update',
 gln.date_modified			AS 'Date Modified - After Update',
 tmp_gc.modified_user_id	AS 'Modified User- Before Update',
 gln.modified_user_id		AS 'Date Modified - After Update'
FROM
    tmp_gc_contracts_service_enddate tmp_gc
    JOIN gc_line_item_contract_history gln ON tmp_gc.id = gln.id
    JOIN gc_contracts g ON g.id = tmp_gc.contracts_id
ORDER BY 
	g.global_contract_id
==="
;

SELECT
 g.id						AS "Contract Id",
 g.global_contract_id		AS "Global Contract Id",
 g.description				AS "Contract Description",
 tmp_gc.gc_date_modified	AS "GCM Date Modified - Before",
 g.date_modified            AS "GCM Date Modified - After",
 tmp_gc.gc_modified_user_id	AS "GCM Modified UserID - Before",
 g.modified_user_id         AS "GCM Modified UserID - After",
 tmp_gc.service_end_date	AS "Service End date - Before Update",
 gln.service_end_date		AS "Service End date - After Update", 
 tmp_gc.date_modified		AS "Date Modified - Before Update",
 gln.date_modified			AS "Date Modified - After Update",
 tmp_gc.modified_user_id	AS "Modified User- Before Update",
 gln.modified_user_id		AS "Date Modified - After Update"
FROM
    tmp_gc_contracts_service_enddate tmp_gc
    JOIN gc_line_item_contract_history gln ON tmp_gc.id = gln.id
    JOIN gc_contracts g ON g.id = tmp_gc.contracts_id
ORDER BY 
	g.global_contract_id
;


-- Verification Query 7: Contract agreement dates
SELECT 
"=== 
SELECT
 g.id						AS 'Contract Id',
 g.global_contract_id		AS 'Global Contract Id',
 g.description				AS 'Contract Description',
 tmp_gc.contract_startdate	AS 'Contract Start date - Before Update',
 g.contract_startdate		AS 'Contract Start date - After Update', 
 tmp_gc.contract_enddate	AS 'Contract End date - Before Update',
 g.contract_enddate			AS 'Contract End date - After Update',
 tmp_gc.gc_timezone_id_c    AS 'Contract Start Dt Timezone- Before Update',
 g.gc_timezone_id_c         AS 'Contract Start Dt Timezone- After Update',
 tmp_gc.gc_timezone_id1_c   AS 'Contract End Dt Timezone- Before Update',
 g.gc_timezone_id1_c		AS 'Contract End Dt Timezone- After Update',
 tmp_gc.date_modified		AS 'Date Modified - Before Update',
 g.date_modified			AS 'Date Modified - After Update',
 tmp_gc.modified_user_id	AS 'Modified User- Before Update',
 g.modified_user_id			AS 'Modified User - After Update'
FROM
    tmp_gc_contracts_agreement_dates tmp_gc
    JOIN gc_contracts g ON g.global_contract_id = tmp_gc.global_contract_id
ORDER BY 
	g.global_contract_id
==="
;

SELECT
 g.id						AS "Contract Id",
 g.global_contract_id		AS "Global Contract Id",
 g.description				AS "Contract Description",
 tmp_gc.contract_startdate	AS "Contract Start date - Before Update",
 g.contract_startdate		AS "Contract Start date - After Update", 
 tmp_gc.contract_enddate	AS "Contract End date - Before Update",
 g.contract_enddate			AS "Contract End date - After Update", 
 tmp_gc.gc_timezone_id_c    AS "Contract Start Dt Timezone- Before Update",
 g.gc_timezone_id_c         AS "Contract Start Dt Timezone- After Update",
 tmp_gc.gc_timezone_id1_c   AS "Contract End Dt Timezone- Before Update",
 g.gc_timezone_id1_c		AS "Contract End Dt Timezone- After Update",
 tmp_gc.date_modified		AS "Date Modified - Before Update",
 g.date_modified			AS "Date Modified - After Update",
 tmp_gc.modified_user_id	AS "Modified User- Before Update",
 g.modified_user_id			AS "Modified User - After Update"
FROM
    tmp_gc_contracts_agreement_dates tmp_gc
    JOIN gc_contracts g ON g.global_contract_id = tmp_gc.global_contract_id
ORDER BY 
	g.global_contract_id
;


-- Step 5: Complete the transaction after verification.
-- Issue either Commit or Rollback upon verification
--
  COMMIT ;
-- ROLLBACK ;


-- Clean up temporary tables (optional)
DROP TEMPORARY TABLE IF EXISTS tmp_gc_contracts;
DROP TEMPORARY TABLE IF EXISTS tmp_gc_contracts_tech_ac;
DROP TEMPORARY TABLE IF EXISTS tmp_gc_contracts_custbill_curr;
DROP TEMPORARY TABLE IF EXISTS tmp_gc_contracts_payment_type;
DROP TEMPORARY TABLE IF EXISTS tmp_gc_contracts_billing_enddate;
DROP TEMPORARY TABLE IF EXISTS tmp_gc_contracts_service_enddate;
DROP TEMPORARY TABLE IF EXISTS tmp_gc_contracts_agreement_dates;
