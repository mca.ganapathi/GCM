-- =============================================
-- SQL for DF-1414 (Line spacing in the mail triggered for approval and termination flow is not according to the format)
-- =============================================

/* Data for the table `email_templates` - body, body_html */

-- Email for Applicant team about contract submission

UPDATE `email_templates` SET `body` = 'Dear Team members,

Contract, {::future::gc_Contracts::global_contract_id::} is submitted to workflow by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.

Kindly view the details at GCM.

URL for contract id: {::href_link::gc_Contracts::href_link::}

Best Regards,
GCM Contracts', `body_html` = '<p>Dear Team members,<br /><br />Contract, {::future::gc_Contracts::global_contract_id::} is submitted to workflow by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.</p>
<p>Kindly view the details at GCM.</p>
<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>
<p><br />Best Regards,<br />GCM Contracts</p>' WHERE `id` = '66fef16f-0b23-4fbf-90fa-b4dfd03e963e';


-- Email for Approver team about contract submission

UPDATE `email_templates` SET `body` = 'Dear Role members,

Contract, {::future::gc_Contracts::global_contract_id::} is submitted to your approval via workflow engine.

Kindly approve or reject the contract with appropriate comments.

URL for contract id: {::href_link::gc_Contracts::href_link::}

Best Regards,
GCM Contracts', `body_html` = '<p>Dear Role members,<br /><br />Contract, {::future::gc_Contracts::global_contract_id::} is submitted to your approval via workflow engine.</p>
<p>Kindly approve or reject the contract with appropriate comments.</p>
<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>
<p><br />Best Regards,<br />GCM Contracts</p>' WHERE `id` = '923424e3-0b23-4fbc-b608-60603610c971';


-- GCM_Contract_Notification_Email_Approved

UPDATE `email_templates` SET `body` = 'Dear Team member,

Contract, {::future::gc_Contracts::global_contract_id::} is approved by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.

Kindly view the details at GCM.

URL for contract id: {::href_link::gc_Contracts::href_link::}

Best Regards,
GCM Contracts', `body_html` = '<p>Dear Team member,<br /><br />Contract, {::future::gc_Contracts::global_contract_id::} is approved by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.</p>
<p>Kindly view the details at GCM.</p>
<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>
<p><br />Best Regards,<br />GCM Contracts</p>' WHERE `id` = '46a712f5-0b23-4fb3-9878-c896795871dc';


-- GCM_Contract_Notification_Email_Rejected

UPDATE `email_templates` SET `body` = 'Dear Team member,

Contract, {::future::gc_Contracts::global_contract_id::} is rejected by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.

Kindly view the details at GCM.

URL for contract id: {::href_link::gc_Contracts::href_link::}

Best Regards,
GCM Contracts', `body_html` = '<p>Dear Team member,<br /><br />Contract, {::future::gc_Contracts::global_contract_id::} is rejected by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.</p>
<p>Kindly view the details at GCM.</p>
<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>
<p><br />Best Regards,<br />GCM Contracts</p>' WHERE `id` = 'b74da531-0b23-4fb1-8652-d6cac54ffa00';


-- Email for Applicant team about contract Termination submission

UPDATE `email_templates` SET `body` = 'Dear Team members,

Contract, {::future::gc_Contracts::global_contract_id::} is submitted to Termination workflow by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.

Kindly view the details at GCM.

URL for contract id: {::href_link::gc_Contracts::href_link::}

Best Regards,
GCM Contracts', `body_html` = '<p>Dear Team members,<br /><br />Contract, {::future::gc_Contracts::global_contract_id::} is submitted to Termination workflow by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.</p>
<p>Kindly view the details at GCM.</p>
<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>
<p><br />Best Regards,<br />GCM Contracts</p>' WHERE `id` = 'f62390c5-0b23-4fbe-b734-a53ec7368300';


-- Email for Approver team about contract Termination submission

UPDATE `email_templates` SET `body` = 'Dear Role members,

Contract, {::future::gc_Contracts::global_contract_id::} is submitted to your approval via workflow engine.

Kindly approve or reject the contract with appropriate comments.

URL for contract id: {::href_link::gc_Contracts::href_link::}

Best Regards,
GCM Contracts', `body_html` = '<p>Dear Role members,<br /><br />Contract, {::future::gc_Contracts::global_contract_id::} is submitted to your approval via workflow engine.</p>
<p>Kindly approve or reject the contract with appropriate comments.<br /><br />URL for contract id: {::href_link::gc_Contracts::href_link::}</p>
<p><br />Best Regards,<br />GCM Contracts</p>' WHERE `id` = 'e644f05d-0b23-4fba-9448-37de77ca3631';


-- GCM_Contract_Notification_Email_Termination_Approved

UPDATE `email_templates` SET `body` = 'Dear Team member,
 
Contract, Termination of {::future::gc_Contracts::global_contract_id::} is approved by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.
 
Kindly view the details at GCM.
 
URL for contract id: {::href_link::gc_Contracts::href_link::}
 
Best Regards,
GCM Contracts', `body_html` = '<p>Dear Team member,<br /><br />Contract, Termination of {::future::gc_Contracts::global_contract_id::} is approved by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.</p>
<p>Kindly view the details at GCM.</p>
<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>
<p><br />Best Regards,<br />GCM Contracts</p>' WHERE `id` = 'f801e4ab-0b23-4fbb-9039-7b298ef9b5e9';


-- GCM_Contract_Notification_Email_Termination_Rejected

UPDATE `email_templates` SET `body` = 'Dear Team member,
 
Contract, Termination of {::future::gc_Contracts::global_contract_id::} is rejected by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.
 
Kindly view the details at GCM.
 
URL for contract id: {::href_link::gc_Contracts::href_link::}
 
Best Regards,
GCM Contracts', `body_html` = '<p>Dear Team member,<br /><br />Contract, Termination of {::future::gc_Contracts::global_contract_id::} is rejected by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.</p>
<p>Kindly view the details at GCM.</p>
<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>
<p><br />Best Regards,<br />GCM Contracts</p>' WHERE `id` = 'fb1e5625-0b23-4fb5-b280-f66fc5749b7b';


-- GCM_Contract_Notification_Email_Termination_Withdrawn

UPDATE `email_templates` SET `body` = 'Dear Team member,

Termination approval request for contract, {::future::gc_Contracts::global_contract_id::} is withdrawn by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.

Kindly view the details at GCM.

URL for contract id: {::href_link::gc_Contracts::href_link::}

Best Regards,
GCM Contracts', `body_html` = '<p>Dear Team member,<br /><br />Termination approval request for contract, {::future::gc_Contracts::global_contract_id::} is withdrawn by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.</p>
<p>Kindly view the details at GCM.</p>
<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>
<p><br />Best Regards,<br />GCM Contracts</p>' WHERE `id` = '5863c637-0b23-4fb3-8433-38670a89469c';


-- GCM_Contract_Notification_Email_Withdrawn

UPDATE `email_templates` SET `body` = 'Dear Team member,

Approval request for contract, {::future::gc_Contracts::global_contract_id::} is withdrawn by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.

Kindly view the details at GCM.

URL for contract id: {::href_link::gc_Contracts::href_link::}

Best Regards,
GCM Contracts', `body_html` = '<p>Dear Team member,<br /><br />Approval request for contract, {::future::gc_Contracts::global_contract_id::} is withdrawn by {::future::gc_Contracts::modified_by_name::} on {::future::gc_Contracts::date_modified::}.</p>
<p>Kindly view the details at GCM.</p>
<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>
<p><br />Best Regards,<br />GCM Contracts</p>' WHERE `id` = '7300aac2-0b23-4fb0-aaa0-df0d3936e617';


-- GCM_Reminder_Email

UPDATE `email_templates` SET `body` = 'Dear Team member,
 
This is a reminder mail.

Contract, {::future::gc_Contracts::global_contract_id::} is awaiting action from your end, since {::future::gc_Contracts::date_modified::}.

Kindly approve or reject the contract with appropriate comments.

URL for contract id: {::href_link::gc_Contracts::href_link::}
 
Best Regards
GCM Contracts', `body_html` = '<p>Dear Team member,<br /><br />This is a reminder mail.<br /><br />Contract, {::future::gc_Contracts::global_contract_id::} is awaiting action from your end, since {::future::gc_Contracts::date_modified::}.</p>
<p>Kindly approve or reject the contract with appropriate comments.</p>
<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>
<p><br />Best Regards,<br />GCM Contracts</p>' WHERE `id` = '5d181a15-0b23-4fb9-b4b7-293b30c9fac3';


-- GCM_Reminder_Email_Termination

UPDATE `email_templates` SET `body` = 'Dear Team member, 

This is a reminder mail.

Contract,  {::future::gc_Contracts::global_contract_id::} is awaiting action from your end, since {::future::gc_Contracts::date_modified::}. 

Kindly approve or reject termination of the contract with appropriate comments. 

URL for contract id: {::href_link::gc_Contracts::href_link::} 

Best Regards,
GCM Contracts', `body_html` = '<p>Dear Team member,<br /><br />This is a reminder mail.</p>
<p>Contract, {::future::gc_Contracts::global_contract_id::} is awaiting action from your end, since {::future::gc_Contracts::date_modified::}.</p>
<p>Kindly approve or reject termination of the contract with appropriate comments.</p>
<p>URL for contract id: {::href_link::gc_Contracts::href_link::}</p>
<p><br />Best Regards,<br />GCM Contracts</p>' WHERE `id` = '1a780e9d-0b23-4fbf-8988-b01a2a522cec';



-- =============================================
-- Validate the Table data
-- =============================================

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '66fef16f-0b23-4fbf-90fa-b4dfd03e963e';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '66fef16f-0b23-4fbf-90fa-b4dfd03e963e';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '923424e3-0b23-4fbc-b608-60603610c971';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '923424e3-0b23-4fbc-b608-60603610c971';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '46a712f5-0b23-4fb3-9878-c896795871dc';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '46a712f5-0b23-4fb3-9878-c896795871dc';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'b74da531-0b23-4fb1-8652-d6cac54ffa00';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'b74da531-0b23-4fb1-8652-d6cac54ffa00';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'f62390c5-0b23-4fbe-b734-a53ec7368300';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'f62390c5-0b23-4fbe-b734-a53ec7368300';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'e644f05d-0b23-4fba-9448-37de77ca3631';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'e644f05d-0b23-4fba-9448-37de77ca3631';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'f801e4ab-0b23-4fbb-9039-7b298ef9b5e9';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'f801e4ab-0b23-4fbb-9039-7b298ef9b5e9';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'fb1e5625-0b23-4fb5-b280-f66fc5749b7b';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = 'fb1e5625-0b23-4fb5-b280-f66fc5749b7b';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '5863c637-0b23-4fb3-8433-38670a89469c';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '5863c637-0b23-4fb3-8433-38670a89469c';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '7300aac2-0b23-4fb0-aaa0-df0d3936e617';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '7300aac2-0b23-4fb0-aaa0-df0d3936e617';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '5d181a15-0b23-4fb9-b4b7-293b30c9fac3';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '5d181a15-0b23-4fb9-b4b7-293b30c9fac3';

SELECT "=== SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '1a780e9d-0b23-4fbf-8988-b01a2a522cec';" AS ''; 
SELECT email_templates.id, email_templates.name, email_templates.subject, email_templates.body, email_templates.body_html FROM email_templates WHERE id = '1a780e9d-0b23-4fbf-8988-b01a2a522cec';