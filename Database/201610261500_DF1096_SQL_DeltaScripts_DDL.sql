-- =============================================
--     SQL for DF-1096
-- =============================================

-- ------------------------------------------------Alter Query-------------------------------------------------------
--
-- Revert to SugarCRM default Constraints for name field in accounts table 
--

ALTER TABLE accounts MODIFY name varchar(150) NULL DEFAULT NULL;

--
-- Revert to SugarCRM default Constraints for name_2_c and name_3_c fields in accounts_cstm table 
--

ALTER TABLE accounts_cstm MODIFY name_2_c varchar(100) NULL DEFAULT NULL;

ALTER TABLE accounts_cstm MODIFY name_3_c varchar(100) NULL DEFAULT NULL;

-- ------------------------------------------------------------------------------------------------------------------

-- =============================================
--   Confirm Table structure
-- =============================================

SELECT '=== DESC accounts;' AS '';         DESC accounts;

SELECT '=== DESC accounts_cstm;' AS '';         DESC accounts_cstm;

