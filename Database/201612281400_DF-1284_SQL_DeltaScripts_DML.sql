-- =============================================
-- SQL for DF-1284
-- =============================================

/* Data for the table `gc_organization` - billing_affiliate_code */

UPDATE gc_organization SET billing_affiliate_code = 'DMY' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '99999';  

-- =============================================
-- Validate the Table data
-- =============================================

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '99999';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '99999';
