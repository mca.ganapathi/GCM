-- =============================================
-- SQL for DF-1253
-- =============================================

/* Data for the table `gc_organization` - billing_affiliate_code */

UPDATE gc_organization SET billing_affiliate_code = '23' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00712';  
UPDATE gc_organization SET billing_affiliate_code = '22' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00735';

-- =============================================
-- Validate the Table data
-- =============================================

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00712';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00712';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00735';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00735';
