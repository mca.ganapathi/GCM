-- =============================================
--     SQL for DF-1053
-- =============================================

--
-- Table structure for table `gc_organization`
--

DROP TABLE IF EXISTS `gc_organization`;
CREATE TABLE IF NOT EXISTS `gc_organization` (
  `id` char(36) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `organization_name_2_local` varchar(100) DEFAULT NULL,
  `organization_name_3_local` varchar(100) DEFAULT NULL,
  `organization_name_4_local` varchar(100) DEFAULT NULL,
  `organization_name_1_latin` varchar(100) DEFAULT NULL,
  `organization_name_2_latin` varchar(100) DEFAULT NULL,
  `organization_name_3_latin` varchar(100) DEFAULT NULL,
  `organization_name_4_latin` varchar(100) DEFAULT NULL,
  `organization_code_1` varchar(8) DEFAULT NULL,
  `organization_code_2` varchar(5) DEFAULT NULL,
  `sales_org_flag` tinyint(1) DEFAULT '0',
  `billing_org_flag` tinyint(1) DEFAULT '0',
  `service_org_flag` tinyint(1) DEFAULT '0',
  `billing_affiliate_code` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `gc_organization_audit`
--

DROP TABLE IF EXISTS `gc_organization_audit`;
CREATE TABLE IF NOT EXISTS `gc_organization_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_gc_organization_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
--
-- Alter Query gc_line_item_contract_history 
--
ALTER TABLE `gc_line_item_contract_history` add COLUMN `gc_organization_id_c` char(36)  NULL;

-- ------------------------------------------------------------------------------------------------------------------

-- =============================================
--   Confirm Table structure
-- =============================================
SELECT '=== DESC gc_organization' AS '';      DESC gc_organization;
SELECT '=== DESC gc_organization_audit' AS '';      DESC gc_organization_audit;
SELECT '=== DESC gc_line_item_contract_history' AS '';      DESC gc_line_item_contract_history;