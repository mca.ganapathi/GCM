-- =============================================
-- Data Patch & Verification SQL for DF-1480 500 - Internal Server Error when Dates as 9999-12-31 23:59:59
-- =============================================

-- Request 1: Data Patch SQL For DF-1480

-- SQL to adjust correct contract_startdate column using gc_timezone
-- Time Zone +09:00
SELECT "=== SELECT id, contract_startdate FROM gc_contracts WHERE gc_timezone_id_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND contract_startdate IS NOT NULL; " AS "";
SELECT id, contract_startdate FROM gc_contracts WHERE gc_timezone_id_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND contract_startdate IS NOT NULL;
SELECT "=== UPDATE gc_contracts SET contract_startdate = DATE_SUB(contract_startdate, INTERVAL 9 HOUR) WHERE gc_timezone_id_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND contract_startdate IS NOT NULL; " AS "";
UPDATE gc_contracts SET contract_startdate = DATE_SUB(contract_startdate, INTERVAL 9 HOUR) WHERE gc_timezone_id_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND contract_startdate IS NOT NULL;
SELECT "=== SELECT id, contract_startdate FROM gc_contracts WHERE gc_timezone_id_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND contract_startdate IS NOT NULL; " AS "";
SELECT id, contract_startdate FROM gc_contracts WHERE gc_timezone_id_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND contract_startdate IS NOT NULL;

-- SQL to adjust correct contract_enddate column using gc_timezone
-- Time Zone +09:00
SELECT "=== SELECT id, contract_enddate FROM gc_contracts WHERE gc_timezone_id1_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND contract_enddate IS NOT NULL; " AS "";
SELECT id, contract_enddate FROM gc_contracts WHERE gc_timezone_id1_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND contract_enddate IS NOT NULL;
SELECT "=== UPDATE gc_contracts SET contract_enddate = DATE_SUB(contract_enddate, INTERVAL 9 HOUR) WHERE gc_timezone_id1_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND contract_enddate IS NOT NULL; " AS "";
UPDATE gc_contracts SET contract_enddate = DATE_SUB(contract_enddate, INTERVAL 9 HOUR) WHERE gc_timezone_id1_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND contract_enddate IS NOT NULL;
SELECT "=== SELECT id, contract_enddate FROM gc_contracts WHERE gc_timezone_id1_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND contract_enddate IS NOT NULL; " AS "";
SELECT id, contract_enddate FROM gc_contracts WHERE gc_timezone_id1_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND contract_enddate IS NOT NULL;

-- SQL to adjust correct service_start_date column using gc_timezone
-- Time Zone +09:00
SELECT "=== SELECT id, service_start_date FROM gc_line_item_contract_history WHERE gc_timezone_id_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND service_start_date IS NOT NULL; " AS "";
SELECT id, service_start_date FROM gc_line_item_contract_history WHERE gc_timezone_id_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND service_start_date IS NOT NULL;
SELECT "=== UPDATE gc_line_item_contract_history SET service_start_date = DATE_SUB(service_start_date, INTERVAL 9 HOUR) WHERE gc_timezone_id_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND service_start_date IS NOT NULL; " AS "";
UPDATE gc_line_item_contract_history SET service_start_date = DATE_SUB(service_start_date, INTERVAL 9 HOUR) WHERE gc_timezone_id_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND service_start_date IS NOT NULL;
SELECT "=== SELECT id, service_start_date FROM gc_line_item_contract_history WHERE gc_timezone_id_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND service_start_date IS NOT NULL; " AS "";
SELECT id, service_start_date FROM gc_line_item_contract_history WHERE gc_timezone_id_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND service_start_date IS NOT NULL;

-- SQL to adjust correct service_end_date column using gc_timezone
-- Time Zone +09:00
SELECT "=== SELECT id, service_end_date FROM gc_line_item_contract_history WHERE gc_timezone_id1_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND service_end_date IS NOT NULL; " AS "";
SELECT id, service_end_date FROM gc_line_item_contract_history WHERE gc_timezone_id1_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND service_end_date IS NOT NULL;
SELECT "=== UPDATE gc_line_item_contract_history SET service_end_date = DATE_SUB(service_end_date, INTERVAL 9 HOUR) WHERE gc_timezone_id1_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND service_end_date IS NOT NULL; " AS "";
UPDATE gc_line_item_contract_history SET service_end_date = DATE_SUB(service_end_date, INTERVAL 9 HOUR) WHERE gc_timezone_id1_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND service_end_date IS NOT NULL;
SELECT "=== SELECT id, service_end_date FROM gc_line_item_contract_history WHERE gc_timezone_id1_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND service_end_date IS NOT NULL; " AS "";
SELECT id, service_end_date FROM gc_line_item_contract_history WHERE gc_timezone_id1_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND service_end_date IS NOT NULL;

-- SQL to adjust correct billing_start_date column using gc_timezone
-- Time Zone +09:00
SELECT "=== SELECT id, billing_start_date FROM gc_line_item_contract_history WHERE gc_timezone_id2_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND billing_start_date IS NOT NULL; " AS "";
SELECT id, billing_start_date FROM gc_line_item_contract_history WHERE gc_timezone_id2_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND billing_start_date IS NOT NULL;
SELECT "=== UPDATE gc_line_item_contract_history SET billing_start_date = DATE_SUB(billing_start_date, INTERVAL 9 HOUR) WHERE gc_timezone_id2_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND billing_start_date IS NOT NULL; " AS "";
UPDATE gc_line_item_contract_history SET billing_start_date = DATE_SUB(billing_start_date, INTERVAL 9 HOUR) WHERE gc_timezone_id2_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND billing_start_date IS NOT NULL;
SELECT "=== SELECT id, billing_start_date FROM gc_line_item_contract_history WHERE gc_timezone_id2_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND billing_start_date IS NOT NULL; " AS "";
SELECT id, billing_start_date FROM gc_line_item_contract_history WHERE gc_timezone_id2_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND billing_start_date IS NOT NULL;

-- SQL to adjust correct billing_end_date column using gc_timezone
-- Time Zone +09:00
SELECT "=== SELECT id, billing_end_date FROM gc_line_item_contract_history WHERE gc_timezone_id3_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND billing_end_date IS NOT NULL; " AS "";
SELECT id, billing_end_date FROM gc_line_item_contract_history WHERE gc_timezone_id3_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND billing_end_date IS NOT NULL;
SELECT "=== UPDATE gc_line_item_contract_history SET billing_end_date = DATE_SUB(billing_end_date, INTERVAL 9 HOUR) WHERE gc_timezone_id3_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND billing_end_date IS NOT NULL; " AS "";
UPDATE gc_line_item_contract_history SET billing_end_date = DATE_SUB(billing_end_date, INTERVAL 9 HOUR) WHERE gc_timezone_id3_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND billing_end_date IS NOT NULL;
SELECT "=== SELECT id, billing_end_date FROM gc_line_item_contract_history WHERE gc_timezone_id3_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND billing_end_date IS NOT NULL; " AS "";
SELECT id, billing_end_date FROM gc_line_item_contract_history WHERE gc_timezone_id3_c IN (SELECT id FROM gc_timezone WHERE utcoffset='+09:00') AND billing_end_date IS NOT NULL;


-- Request 2: Verification SQL For DF-1480

-- Verification - Records with contract start date time zone values other than +00:00 or +09:00 or NULL
SELECT "=== SELECT id, contract_startdate FROM gc_contracts WHERE (gc_timezone_id_c NOT IN (SELECT id FROM gc_timezone WHERE utcoffset IN ('+00:00', '+09:00')) AND gc_timezone_id_c IS NOT NULL) AND contract_startdate IS NOT NULL; " AS "";
SELECT id, contract_startdate FROM gc_contracts WHERE (gc_timezone_id_c NOT IN (SELECT id FROM gc_timezone WHERE utcoffset IN ('+00:00', '+09:00')) AND gc_timezone_id_c IS NOT NULL) AND contract_startdate IS NOT NULL;

-- Verification - Records with contract end date time zone values other than +00:00 or +09:00 or NULL
SELECT "=== SELECT id, contract_enddate FROM gc_contracts WHERE (gc_timezone_id1_c NOT IN (SELECT id FROM gc_timezone WHERE utcoffset IN ('+00:00', '+09:00')) AND gc_timezone_id1_c IS NOT NULL) AND contract_enddate IS NOT NULL; " AS "";
SELECT id, contract_enddate FROM gc_contracts WHERE (gc_timezone_id1_c NOT IN (SELECT id FROM gc_timezone WHERE utcoffset IN ('+00:00', '+09:00')) AND gc_timezone_id1_c IS NOT NULL) AND contract_enddate IS NOT NULL;

-- Verification - Records with service start date time zone values other than +00:00 or +09:00 or NULL
SELECT "=== SELECT id, service_start_date FROM gc_line_item_contract_history WHERE (gc_timezone_id_c NOT IN (SELECT id FROM gc_timezone WHERE utcoffset IN ('+00:00', '+09:00')) AND gc_timezone_id_c IS NOT NULL) AND service_start_date IS NOT NULL; " AS "";
SELECT id, service_start_date FROM gc_line_item_contract_history WHERE (gc_timezone_id_c NOT IN (SELECT id FROM gc_timezone WHERE utcoffset IN ('+00:00', '+09:00')) AND gc_timezone_id_c IS NOT NULL) AND service_start_date IS NOT NULL;

-- Verification - Records with service end date time zone values other than +00:00 or +09:00 or NULL
SELECT "=== SELECT id, service_end_date FROM gc_line_item_contract_history WHERE (gc_timezone_id1_c NOT IN (SELECT id FROM gc_timezone WHERE utcoffset IN ('+00:00', '+09:00')) AND gc_timezone_id1_c IS NOT NULL) AND service_end_date IS NOT NULL; " AS "";
SELECT id, service_end_date FROM gc_line_item_contract_history WHERE (gc_timezone_id1_c NOT IN (SELECT id FROM gc_timezone WHERE utcoffset IN ('+00:00', '+09:00')) AND gc_timezone_id1_c IS NOT NULL) AND service_end_date IS NOT NULL;

-- Verification - Records with billing start date time zone values other than +00:00 or +09:00 or NULL
SELECT "=== SELECT id, billing_start_date FROM gc_line_item_contract_history WHERE (gc_timezone_id2_c NOT IN (SELECT id FROM gc_timezone WHERE utcoffset IN ('+00:00', '+09:00')) AND gc_timezone_id2_c IS NOT NULL) AND billing_start_date IS NOT NULL; " AS "";
SELECT id, billing_start_date FROM gc_line_item_contract_history WHERE (gc_timezone_id2_c NOT IN (SELECT id FROM gc_timezone WHERE utcoffset IN ('+00:00', '+09:00')) AND gc_timezone_id2_c IS NOT NULL) AND billing_start_date IS NOT NULL;

-- Verification - Records with billing end date time zone values other than +00:00 or +09:00 or NULL
SELECT "=== SELECT id, billing_end_date FROM gc_line_item_contract_history WHERE (gc_timezone_id3_c NOT IN (SELECT id FROM gc_timezone WHERE utcoffset IN ('+00:00', '+09:00')) AND gc_timezone_id3_c IS NOT NULL) AND billing_end_date IS NOT NULL; " AS "";
SELECT id, billing_end_date FROM gc_line_item_contract_history WHERE (gc_timezone_id3_c NOT IN (SELECT id FROM gc_timezone WHERE utcoffset IN ('+00:00', '+09:00')) AND gc_timezone_id3_c IS NOT NULL) AND billing_end_date IS NOT NULL;

