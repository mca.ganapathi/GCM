-- =============================================
--     DB migrate SQL  -  yyyy/MM/dd HH:mm
-- =============================================
/*!40101 SET NAMES utf8 */;

-- =============================================
--   DDL
-- =============================================
CREATE DATABASE IF NOT EXISTS apilog CHARACTER SET utf8;

-- =============================================
--   Confirm
-- =============================================
SELECT '=== SHOW DATABASES' AS '';                SHOW DATABASES;

USE apilog;
SELECT '=== SHOW TABLES(apilog Database)' AS '';  SHOW TABLES;
