-- =============================================
--     SQL for DF-1910
-- =============================================

-- ------------------------------------------------Alter Query-------------------------------------------------------
--
-- Increase Sales Representative Name (Latin Character) length from 60 to 100
-- Increase Sales Representative  Name (Local Character) length from 60 to 100
--

ALTER TABLE `gc_salesrepresentative` MODIFY `name` varchar(100) NULL DEFAULT NULL;
ALTER TABLE `gc_salesrepresentative` MODIFY `sales_rep_name_1` varchar(100) NULL DEFAULT NULL;



-- ------------------------------------------------------------------------------------------------------------------

-- =============================================
--   Confirm Table structure
-- =============================================

SELECT '=== DESC gc_salesrepresentative;' AS '';         DESC gc_salesrepresentative;


