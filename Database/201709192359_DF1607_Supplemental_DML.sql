-- =============================================================================================
--  Supplimental SQL for DF-1607
-- =============================================================================================

-- ===== Create Temporary tables by quote from 201709201600_DF1607_SQL_DeltaScript_DML.sql =====
--
-- Query 4: Update Payment type to NULL
--
CREATE TEMPORARY TABLE tmp_gc_contracts_payment_type LIKE gc_line_item_contract_history ;
ALTER TABLE tmp_gc_contracts_payment_type 
	ADD gc_date_modified DATETIME, ADD gc_modified_user_id VARCHAR(40);

-- Populate records that satifies the data patch condition
INSERT INTO tmp_gc_contracts_payment_type
	(
	 id,
	 contracts_id,
	 payment_type,
	 date_modified,
	 modified_user_id,
	 gc_date_modified,
	 gc_modified_user_id
	)
SELECT
	gln.id,
	gln.contracts_id,
	gln.payment_type,
	gln.date_modified,
	gln.modified_user_id,
	g.date_modified,
	g.modified_user_id
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
        AND gln.payment_type = '00'
        AND g.product_offering = '2808'
        AND g.global_contract_id <= 120000000001498
  JOIN contacts_cstm AS cstm ON cstm.id_c = gln.bill_account_id
  JOIN c_country AS cty  ON cty.id = cstm.c_country_id_c AND cty.country_code_numeric != '392'
  JOIN contacts AS c ON c.id = cstm.id_c
  LEFT JOIN gc_directdeposit dp ON dp.contract_history_id = gln.id
 WHERE
   dp.id IS NULL
   OR
   (
    dp.id IS NOT NULL 
    AND IFNULL(dp.name, '') = '' 
    AND IFNULL(dp.bank_code, '') = ''
    AND IFNULL(branch_code, '') = ''
    AND IFNULL(deposit_type, '') = ''
    AND IFNULL(account_no, '') = ''
    AND IFNULL(payee_contact_person_name_1, '') = ''
    AND IFNULL(payee_contact_person_name_2, '') = ''
    AND IFNULL(payee_tel_no, '') = ''
    AND IFNULL(payee_email_address, '') = ''
    AND IFNULL(remarks, '') = ''
  )
;
--
-- Query 5: Update Billing end date
--
CREATE TEMPORARY TABLE tmp_gc_contracts_billing_enddate LIKE gc_line_item_contract_history ;
ALTER TABLE tmp_gc_contracts_billing_enddate 
	ADD gc_date_modified DATETIME, ADD gc_modified_user_id VARCHAR(40);

-- Populate records that satifies the data patch condition
INSERT INTO tmp_gc_contracts_billing_enddate
	(
	 id,
	 contracts_id,
	 billing_end_date,
	 gc_timezone_id3_c,		-- Added by MMd
	 date_modified,
	 modified_user_id,
	 gc_date_modified,
	 gc_modified_user_id
	)
SELECT 
	gln.id,
	gln.contracts_id,
	gln.billing_end_date,
	gln.gc_timezone_id3_c,	-- Added by MMd
	gln.date_modified,
	gln.modified_user_id,
	g.date_modified,
	g.modified_user_id
FROM
     gc_contracts g 
 JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id  
 JOIN 
     (SELECT tz.utcoffset, c.id FROM c_country c 
         JOIN gc_timezone tz ON tz.c_country_id_c = c.id
  WHERE
     c.country_code_numeric = '392'
 ) AS jst 
 JOIN gc_timezone AS tz ON tz.id = gln.gc_timezone_id3_c
   AND tz.utcoffset = jst.utcoffset
   AND (IFNULL(tz.c_country_id_c, '') = '' OR tz.c_country_id_c = jst.id)
 WHERE    
    g.product_offering = '2808'
    AND g.global_contract_id <= 120000000001498
    AND (gln.billing_end_date = '9999-12-31 14:59:59' OR gln.billing_end_date = '9999-12-31 06:00:00')
;

--
-- Query 6: Service end date
--
CREATE TEMPORARY TABLE tmp_gc_contracts_service_enddate LIKE gc_line_item_contract_history ;
ALTER TABLE tmp_gc_contracts_service_enddate 
	ADD gc_date_modified DATETIME, ADD gc_modified_user_id VARCHAR(40);

-- Populate records that satifies the data patch condition
INSERT INTO tmp_gc_contracts_service_enddate
	(
	 id,
	 contracts_id,
	 service_end_date,
	 gc_timezone_id1_c,		-- Added by MMd
	 date_modified,
	 modified_user_id,
	 gc_date_modified,
	 gc_modified_user_id
	)
SELECT 
	gln.id,
	gln.contracts_id,
	gln.service_end_date,
	gln.gc_timezone_id1_c,	-- Added by MMd
	gln.date_modified,
	gln.modified_user_id,
	g.date_modified,
	g.modified_user_id
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
  JOIN 
     (SELECT tz.utcoffset, c.id FROM c_country c
    JOIN gc_timezone tz ON tz.c_country_id_c = c.id
   WHERE
   c.country_code_numeric = '392'
  ) AS jst
  JOIN gc_timezone AS tz ON tz.id = gln.gc_timezone_id1_c
AND tz.utcoffset = jst.utcoffset
AND (IFNULL(tz.c_country_id_c, '') = '' OR tz.c_country_id_c = jst.id)
WHERE
  g.product_offering = '2808'
  AND g.global_contract_id <= 120000000001498
  AND
  (gln.service_end_date = '9999-12-31 14:59:59' OR gln.service_end_date = '9999-12-31 06:00:00')
;


-- ===== Select Before datas. =====
SELECT '===== Suppliment query 4(continued)  =====' AS '';
SELECT
  dp.id					AS 'DirectDeposit ID'
, dp.deleted			AS 'DirectDeposit Deleted - Before'
, dp.date_modified		AS 'DirectDeposit Modified - Before'
, dp.modified_user_id	AS 'DirectDeposit Modified UserID - Before'
FROM 
  gc_directdeposit dp
INNER JOIN tmp_gc_contracts_payment_type tgc ON tgc.id = dp.contract_history_id;


SELECT '===== Suppliment query 5 =====' AS '';
SELECT
 g.global_contract_id		AS "Global Contract Id",
 tmp_gc.gc_timezone_id3_c	AS "Billing End date Timezone - Before Update"
FROM
    tmp_gc_contracts_billing_enddate tmp_gc
    JOIN gc_line_item_contract_history gln ON tmp_gc.id = gln.id
    JOIN gc_contracts g ON g.id = tmp_gc.contracts_id
ORDER BY 
	g.global_contract_id
;

SELECT '===== Suppliment query 6 =====' AS '';
SELECT
 g.global_contract_id		AS "Global Contract Id",
 tmp_gc.gc_timezone_id1_c	AS "Service End date Timezone - Before Update"
FROM
    tmp_gc_contracts_service_enddate tmp_gc
    JOIN gc_line_item_contract_history gln ON tmp_gc.id = gln.id
    JOIN gc_contracts g ON g.id = tmp_gc.contracts_id
ORDER BY 
	g.global_contract_id
;
