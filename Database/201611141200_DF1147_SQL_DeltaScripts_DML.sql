-- =============================================
-- SQL for DF-1147 (Organization Master Data - Billing Affiliate Code)
-- =============================================

/* Data for the table `gc_organization` - billing_affiliate_code */

UPDATE gc_organization SET billing_affiliate_code = '1' WHERE  organization_code_1 = '50611200' AND organization_code_2 = '00212';  
UPDATE gc_organization SET billing_affiliate_code = '2' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00701';
UPDATE gc_organization SET billing_affiliate_code = '5' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00702';
UPDATE gc_organization SET billing_affiliate_code = '11' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00711';
UPDATE gc_organization SET billing_affiliate_code = '20' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00721';
UPDATE gc_organization SET billing_affiliate_code = '9' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00722';
UPDATE gc_organization SET billing_affiliate_code = '10' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00723';
UPDATE gc_organization SET billing_affiliate_code = '19' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00725';
UPDATE gc_organization SET billing_affiliate_code = '17' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00726';
UPDATE gc_organization SET billing_affiliate_code = '3' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00727';
UPDATE gc_organization SET billing_affiliate_code = '8' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00728';
UPDATE gc_organization SET billing_affiliate_code = '6' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00741';
UPDATE gc_organization SET billing_affiliate_code = '7' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00742';
UPDATE gc_organization SET billing_affiliate_code = '21' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00745';
UPDATE gc_organization SET billing_affiliate_code = '18' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00746';
UPDATE gc_organization SET billing_affiliate_code = '14' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00748';
UPDATE gc_organization SET billing_affiliate_code = '15' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00749';
UPDATE gc_organization SET billing_affiliate_code = '12' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00750';
UPDATE gc_organization SET billing_affiliate_code = '16' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00751';
UPDATE gc_organization SET billing_affiliate_code = '13' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00753';
UPDATE gc_organization SET billing_affiliate_code = '4' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00754';
UPDATE gc_organization SET billing_affiliate_code = '107' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00971';
UPDATE gc_organization SET billing_affiliate_code = '106' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00972';
UPDATE gc_organization SET billing_affiliate_code = '104' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00973';
UPDATE gc_organization SET billing_affiliate_code = '109' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00974';
UPDATE gc_organization SET billing_affiliate_code = '110' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00975';
UPDATE gc_organization SET billing_affiliate_code = '105' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00976';
UPDATE gc_organization SET billing_affiliate_code = '108' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00977';
UPDATE gc_organization SET billing_affiliate_code = '101' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00978';
UPDATE gc_organization SET billing_affiliate_code = '102' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00979';
UPDATE gc_organization SET billing_affiliate_code = '103' WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00980';

-- =============================================
-- Validate the Table data
-- =============================================

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50611200' AND organization_code_2 = '00212';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50611200' AND organization_code_2 = '00212';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00701';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00701';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00702';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00702';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00711';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00711';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00721';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00721';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00722';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00722';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00723';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00723';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00725';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00725';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00726';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00726';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00727';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00727';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00728';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00728';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00741';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00741';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00742';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00742';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00745';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00745';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00746';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00746';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00748';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00748';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00749';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00749';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00750';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00750';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00751';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00751';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00753';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00753';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00754';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00754';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00971';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00971';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00972';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00972';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00973';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00973';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00974';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00974';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00975';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00975';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00976';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00976';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00977';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00977';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00978';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00978';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00979';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00979';

SELECT "=== SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00980';" AS ''; 
SELECT id, organization_code_1, organization_code_2, billing_affiliate_code FROM gc_organization WHERE  organization_code_1 = '50617000' AND organization_code_2 = '00980';
