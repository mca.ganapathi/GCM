-- =============================================
--     SQL for DF-2412
--     ALter tables for InnoDB Cluster
-- =============================================

/* Data for the table `upgrade_history` */
ALTER TABLE `config` ADD `seq_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;
ALTER TABLE `current_schema` ADD `seq_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;
ALTER TABLE `gc_line_item_contract_history_bk_23_11_15` ADD `seq_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;
ALTER TABLE emails_text ENGINE InnoDB;


-- =============================================
--   Validate the Table data
-- =============================================
SELECT '=== DESC config table' AS '';
DESC config;
SELECT '=== DESC current_schema table' AS '';
DESC current_schema;
SELECT '=== DESC gc_line_item_contract_history_bk_23_11_15' AS '';
DESC gc_line_item_contract_history_bk_23_11_15;
SELECT '=== Confirming storage engine for emails_text' AS '';
select table_schema,table_name, engine from information_schema.tables where table_name="emails_text";
