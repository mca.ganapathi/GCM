-- ========================================================================================
--     SQL for DF-1648. Script to return impacted contracts of data patch (DF-1607)
-- ========================================================================================

--
-- Note: Please uncomment and replace 120000000001498 with appropriate Id from test env for below condition:
--                       AND g.global_contract_id <= 930000000002900
--



--
-- 1) Technical Account
--

SELECT 
"SELECT
  g.global_contract_id      AS 'Global Contract Id',
  g.id                      AS 'Contract Object Id',
  g.contract_version	    AS 'Contract Version',
  c.id                      AS 'Account Object Id',
  c.last_name		    	AS 'Account Name (Latin)',
  cstm.name_2_c		    	AS 'Account Name (Local)'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gl ON gl.contracts_id = g.id AND IFNULL(gl.tech_account_id, '') <> ''
  JOIN contacts_cstm AS cstm ON cstm.id_c = gl.tech_account_id
  JOIN contacts AS c ON c.id = cstm.id_c
WHERE
  g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  AND IFNULL(c.last_name, '') = ''
  AND IFNULL(cstm.name_2_c, '') = ''
  AND IFNULL(cstm.division_1_c, '') = ''
  AND IFNULL(c.title, '') = ''
  AND IFNULL(c.phone_work, '') = ''
  AND IFNULL(c.phone_fax, '') = ''
  AND IFNULL(cstm.division_2_c, '') = ''
  AND IFNULL(cstm.title_2_c, '') = ''
  AND IFNULL(cstm.ext_no_c, '') = ''
  AND IFNULL(c.phone_mobile, '') = ''
  AND IFNULL(cstm.c_country_id_c, '') = ''
  AND IFNULL(cstm.postal_no_c, '') = ''
  AND IFNULL(cstm.addr_1_c, '') = ''
  AND IFNULL(cstm.addr_2_c, '') = ''
  AND IFNULL(cstm.attention_line_c, '') = ''
  AND IFNULL(cstm.addr_general_c, '') = ''
  AND IFNULL(cstm.city_general_c, '') = ''
  AND IFNULL(cstm.c_state_id_c, '') = ''
  AND NOT EXISTS 
	(SELECT 1 FROM email_addr_bean_rel emr
		JOIN email_addresses em ON em.id = emr.email_address_id AND IFNULL(em.email_address, '') != ''
	    WHERE emr.bean_id = c.id
	)
  AND 
   NOT EXISTS 
	(SELECT 1 FROM js_accounts_js js
		WHERE js.contact_id_c = c.id 
			AND IFNULL(js.name, '') != ''
			AND IFNULL(js.description, '') != ''
			AND IFNULL(js.addr1, '') != ''
			AND IFNULL(js.addr2, '') != ''
	)
ORDER BY g.global_contract_id
;" AS ''
;

SELECT
  g.global_contract_id      AS 'Global Contract Id',
  g.id                      AS 'Contract Object Id',
  g.contract_version	    AS 'Contract Version',
  c.id                      AS 'Account Object Id',
  c.last_name		    	AS 'Account Name (Latin)',
  cstm.name_2_c		    	AS 'Account Name (Local)'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gl ON gl.contracts_id = g.id AND IFNULL(gl.tech_account_id, '') <> ''
  JOIN contacts_cstm AS cstm ON cstm.id_c = gl.tech_account_id
  JOIN contacts AS c ON c.id = cstm.id_c
WHERE
  g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  AND IFNULL(c.last_name, '') = ''
  AND IFNULL(cstm.name_2_c, '') = ''
  AND IFNULL(cstm.division_1_c, '') = ''
  AND IFNULL(c.title, '') = ''
  AND IFNULL(c.phone_work, '') = ''
  AND IFNULL(c.phone_fax, '') = ''
  AND IFNULL(cstm.division_2_c, '') = ''
  AND IFNULL(cstm.title_2_c, '') = ''
  AND IFNULL(cstm.ext_no_c, '') = ''
  AND IFNULL(c.phone_mobile, '') = ''
  AND IFNULL(cstm.c_country_id_c, '') = ''
  AND IFNULL(cstm.postal_no_c, '') = ''
  AND IFNULL(cstm.addr_1_c, '') = ''
  AND IFNULL(cstm.addr_2_c, '') = ''
  AND IFNULL(cstm.attention_line_c, '') = ''
  AND IFNULL(cstm.addr_general_c, '') = ''
  AND IFNULL(cstm.city_general_c, '') = ''
  AND IFNULL(cstm.c_state_id_c, '') = ''
  AND NOT EXISTS 
	(SELECT 1 FROM email_addr_bean_rel emr
		JOIN email_addresses em ON em.id = emr.email_address_id AND IFNULL(em.email_address, '') != ''
	    WHERE emr.bean_id = c.id
	)
  AND 
   NOT EXISTS 
	(SELECT 1 FROM js_accounts_js js
		WHERE js.contact_id_c = c.id 
			AND IFNULL(js.name, '') != ''
			AND IFNULL(js.description, '') != ''
			AND IFNULL(js.addr1, '') != ''
			AND IFNULL(js.addr2, '') != ''
	)
ORDER BY g.global_contract_id
;


--
-- 2) Agreement Period
--

SELECT 
"SELECT
  g.global_contract_id     AS 'Global Contract Id',
  g.id                     AS 'Contract Object Id',
  g.contract_version	   AS 'Contract Version',
  gln.service_start_date   AS 'Service Start Date',
  gln.service_end_date	   AS 'Service End Date'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
        AND gln.service_start_date IS NOT NULL AND gln.service_end_date IS NOT NULL
WHERE
   g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
    AND 
   (
    IFNULL(g.contract_startdate, '0000-00-00 00:00:00') = '0000-00-00 00:00:00' 
    AND 
    IFNULL(g.contract_enddate, '0000-00-00 00:00:00') = '0000-00-00 00:00:00'
   )
ORDER BY 
    g.global_contract_id
;" AS ''
;

SELECT
  g.global_contract_id     AS 'Global Contract Id',
  g.id                     AS 'Contract Object Id',
  g.contract_version	   AS 'Contract Version',
  gln.service_start_date   AS 'Service Start Date',
  gln.service_end_date	   AS 'Service End Date'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
        AND gln.service_start_date IS NOT NULL AND gln.service_end_date IS NOT NULL
WHERE
   g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
    AND 
   (
    IFNULL(g.contract_startdate, '0000-00-00 00:00:00') = '0000-00-00 00:00:00' 
    AND 
    IFNULL(g.contract_enddate, '0000-00-00 00:00:00') = '0000-00-00 00:00:00'
   )
ORDER BY 
    g.global_contract_id
;

--
-- 3) Payment Type
-- 

SELECT 
"SELECT 
  g.global_contract_id     AS 'Global Contract Id',
  g.id                     AS 'Contract Object Id',
  gln.payment_type         AS 'Payment Type',
  dp.id                    AS 'DP Id',
  dp.name                  AS 'Bank Name',
  dp.bank_code             AS 'Bank Code',
  gln.bill_account_id	   AS 'Bill Account Id',
  c.last_name		   AS 'Account Name',
  cstm.name_2_c		    AS 'Account Name (Local)'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
        AND gln.payment_type = '00'
        AND g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  JOIN contacts_cstm AS cstm ON cstm.id_c = gln.bill_account_id
  JOIN c_country AS cty  ON cty.id = cstm.c_country_id_c AND cty.country_code_numeric != '392'
  JOIN contacts AS c ON c.id = cstm.id_c
  LEFT JOIN gc_directdeposit dp ON dp.contract_history_id = gln.id
WHERE
   dp.id IS NULL
   OR
   (
    dp.id IS NOT NULL 
    AND IFNULL(dp.name, '') = '' 
    AND IFNULL(dp.bank_code, '') = ''
    AND IFNULL(branch_code, '') = ''
    AND IFNULL(deposit_type, '') = ''
    AND IFNULL(account_no, '') = ''
    AND IFNULL(payee_contact_person_name_1, '') = ''
    AND IFNULL(payee_contact_person_name_2, '') = ''
    AND IFNULL(payee_tel_no, '') = ''
    AND IFNULL(payee_email_address, '') = ''
    AND IFNULL(remarks, '') = ''
  )
ORDER BY g.global_contract_id
;" AS ''
;


SELECT 
  g.global_contract_id     AS 'Global Contract Id',
  g.id                     AS 'Contract Object Id',
  gln.payment_type         AS 'Payment Type',
  dp.id                    AS 'DP Id',
  dp.name                  AS 'Bank Name',
  dp.bank_code             AS 'Bank Code',
  gln.bill_account_id	   AS 'Bill Account Id',
  c.last_name		   AS 'Account Name',
  cstm.name_2_c		    AS 'Account Name (Local)'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
        AND gln.payment_type = '00'
        AND g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  JOIN contacts_cstm AS cstm ON cstm.id_c = gln.bill_account_id
  JOIN c_country AS cty  ON cty.id = cstm.c_country_id_c AND cty.country_code_numeric != '392'
  JOIN contacts AS c ON c.id = cstm.id_c
  LEFT JOIN gc_directdeposit dp ON dp.contract_history_id = gln.id
WHERE
   dp.id IS NULL
   OR
   (
    dp.id IS NOT NULL 
    AND IFNULL(dp.name, '') = '' 
    AND IFNULL(dp.bank_code, '') = ''
    AND IFNULL(branch_code, '') = ''
    AND IFNULL(deposit_type, '') = ''
    AND IFNULL(account_no, '') = ''
    AND IFNULL(payee_contact_person_name_1, '') = ''
    AND IFNULL(payee_contact_person_name_2, '') = ''
    AND IFNULL(payee_tel_no, '') = ''
    AND IFNULL(payee_email_address, '') = ''
    AND IFNULL(remarks, '') = ''
  )
ORDER BY g.global_contract_id
;

 
--
-- 4) Customer Billing currency
--

SELECT 
"SELECT
  g.global_contract_id     	AS 'Global Contract Id',
  g.id                      AS 'Contract Object Id',
  gln.bill_account_id      	AS 'Billing account Id',
  c.last_name               AS 'Account Name',
  cty.name                  AS 'Country',
  gln.cust_billing_currency 	AS 'Currency'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
  JOIN contacts_cstm AS cstm ON cstm.id_c = gln.bill_account_id
  JOIN c_country AS cty  ON cty.id = cstm.c_country_id_c
  JOIN contacts AS c ON c.id = cstm.id_c
WHERE
  g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  AND cty.country_code_numeric = '392'
  AND (gln.cust_billing_currency IS NULL OR TRIM(gln.cust_billing_currency) = '')
ORDER BY g.global_contract_id  
;" AS ''
;

SELECT
  g.global_contract_id     	AS 'Global Contract Id',
  g.id                      AS 'Contract Object Id',
  gln.bill_account_id      	AS 'Billing account Id',
  c.last_name               AS 'Account Name',
  cty.name                  AS 'Country',
  gln.cust_billing_currency 	AS 'Currency'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
  JOIN contacts_cstm AS cstm ON cstm.id_c = gln.bill_account_id
  JOIN c_country AS cty  ON cty.id = cstm.c_country_id_c
  JOIN contacts AS c ON c.id = cstm.id_c
WHERE
  g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  AND cty.country_code_numeric = '392'
  AND (gln.cust_billing_currency IS NULL OR TRIM(gln.cust_billing_currency) = '')
ORDER BY g.global_contract_id  
;

--
-- 5) Billing End Date
--

SELECT 
"SELECT
 g.global_contract_id     AS 'Global Contract Id',
 g.id    		          AS 'Contract Object Id',  
 gln.billing_end_date 	  AS 'Billing End date'
FROM
     gc_contracts g 
JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id  
JOIN 
     (SELECT tz.utcoffset, c.id FROM c_country c 
         JOIN gc_timezone tz ON tz.c_country_id_c = c.id
	  WHERE	   
	     c.country_code_numeric = '392'
	 ) AS jst 
JOIN gc_timezone AS tz ON tz.id = gln.gc_timezone_id3_c 
	 AND tz.utcoffset = jst.utcoffset
	 AND (IFNULL(tz.c_country_id_c, '') = '' OR tz.c_country_id_c = jst.id)
WHERE    
    g.product_offering = '2808'
    AND g.global_contract_id <= 930000000002900    
    AND    (gln.billing_end_date = '9999-12-31 14:59:59' OR gln.billing_end_date = '9999-12-31 06:00:00')
ORDER BY 
   g.global_contract_id
;" AS ''
;
--

SELECT
 g.global_contract_id     AS 'Global Contract Id',
 g.id    		          AS 'Contract Object Id',  
 gln.billing_end_date 	  AS 'Billing End date'
FROM
     gc_contracts g 
JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id  
JOIN 
     (SELECT tz.utcoffset, c.id FROM c_country c 
         JOIN gc_timezone tz ON tz.c_country_id_c = c.id
	  WHERE	   
	     c.country_code_numeric = '392'
	 ) AS jst 
JOIN gc_timezone AS tz ON tz.id = gln.gc_timezone_id3_c 
	 AND tz.utcoffset = jst.utcoffset
	 AND (IFNULL(tz.c_country_id_c, '') = '' OR tz.c_country_id_c = jst.id)
WHERE    
    g.product_offering = '2808'
    AND g.global_contract_id <= 930000000002900    
    AND    (gln.billing_end_date = '9999-12-31 14:59:59' OR gln.billing_end_date = '9999-12-31 06:00:00')
ORDER BY 
   g.global_contract_id
; 


--
-- 6) Service End Date
--

SELECT 
"SELECT
  g.global_contract_id     AS 'Global Contract Id',
  g.id			           AS 'Contract Object Id',
  gln.service_end_date 	   AS 'Service End date'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
  JOIN 
      (SELECT tz.utcoffset, c.id FROM c_country c
	    JOIN gc_timezone tz ON tz.c_country_id_c = c.id
	   WHERE
	   c.country_code_numeric = '392'
	  ) AS jst
  JOIN gc_timezone AS tz ON tz.id = gln.gc_timezone_id1_c
	AND tz.utcoffset = jst.utcoffset
	AND (IFNULL(tz.c_country_id_c, '') = '' OR tz.c_country_id_c = jst.id)
WHERE
  g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  AND
  (gln.service_end_date = '9999-12-31 14:59:59' OR gln.service_end_date = '9999-12-31 06:00:00')
ORDER BY 
    g.global_contract_id
;" AS ''
;
--

SELECT
  g.global_contract_id     AS 'Global Contract Id',
  g.id			           AS 'Contract Object Id',
  gln.service_end_date 	   AS 'Service End date'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
  JOIN 
      (SELECT tz.utcoffset, c.id FROM c_country c
	    JOIN gc_timezone tz ON tz.c_country_id_c = c.id
	   WHERE
	   c.country_code_numeric = '392'
	  ) AS jst
  JOIN gc_timezone AS tz ON tz.id = gln.gc_timezone_id1_c
	AND tz.utcoffset = jst.utcoffset
	AND (IFNULL(tz.c_country_id_c, '') = '' OR tz.c_country_id_c = jst.id)
WHERE
  g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  AND
  (gln.service_end_date = '9999-12-31 14:59:59' OR gln.service_end_date = '9999-12-31 06:00:00')
ORDER BY 
    g.global_contract_id
;


-- =======================================================================================================================
--    Script to validate contracts that are not applicable for data patch (DF-1607)
--
--    Below SQL's returns contracts that might appear in impacted list, but fails to meet all criteria for data patch.
-- =======================================================================================================================

-- Last modified date : 27/07/2017
--

--
-- Note: Please uncomment and replace 120000000001498 with appropriate Id from test env for below condition:
--                       AND g.global_contract_id <= 930000000002900
--


--
-- Technical Account
--

SELECT 
"SELECT
  g.global_contract_id      AS 'Global Contract Id',
  g.id                      AS 'Contract Object Id',
  g.contract_version	    AS 'Contract Version',
  c.id                      AS 'Account Object Id',
  c.last_name		    AS 'Account Name (Latin)',
  cstm.name_2_c		    AS 'Account Name (Local)'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gl ON gl.contracts_id = g.id AND IFNULL(gl.tech_account_id, '') <> ''
  JOIN contacts_cstm AS cstm ON cstm.id_c = gl.tech_account_id
  JOIN contacts AS c ON c.id = cstm.id_c
WHERE
  g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  AND 
(  
  IFNULL(c.last_name, '') != ''
  OR IFNULL(cstm.name_2_c, '') != ''
  OR IFNULL(cstm.division_1_c, '') != ''
  OR IFNULL(c.title, '') != ''
  OR IFNULL(c.phone_work, '') != ''
  OR IFNULL(c.phone_fax, '') != ''
  OR IFNULL(cstm.division_2_c, '') != ''
  OR IFNULL(cstm.title_2_c, '') != ''
  OR IFNULL(cstm.ext_no_c, '') != ''
  OR IFNULL(c.phone_mobile, '') != ''
  OR IFNULL(cstm.c_country_id_c, '') != ''
  OR IFNULL(cstm.postal_no_c, '') != ''
  OR IFNULL(cstm.addr_1_c, '') != ''
  OR IFNULL(cstm.addr_2_c, '') != ''
  OR IFNULL(cstm.attention_line_c, '') != ''
  OR IFNULL(cstm.addr_general_c, '') != ''
  OR IFNULL(cstm.city_general_c, '') != ''
  OR IFNULL(cstm.c_state_id_c, '') != ''
  OR EXISTS 
	(SELECT 1 FROM email_addr_bean_rel emr
		JOIN email_addresses em ON em.id = emr.email_address_id AND IFNULL(em.email_address, '') <> ''
	    WHERE emr.bean_id = c.id
	)
  OR
  EXISTS 
	(SELECT 1 FROM js_accounts_js js
		WHERE js.contact_id_c = c.id 
			AND IFNULL(js.name, '') != ''
			AND IFNULL(js.description, '') != ''
			AND IFNULL(js.addr1, '') != ''
			AND IFNULL(js.addr2, '') != ''
	)
)
ORDER BY g.global_contract_id
;" AS ''
;

SELECT
  g.global_contract_id      AS 'Global Contract Id',
  g.id                      AS 'Contract Object Id',
  g.contract_version	    AS 'Contract Version',
  c.id                      AS 'Account Object Id',
  c.last_name		    AS 'Account Name (Latin)',
  cstm.name_2_c		    AS 'Account Name (Local)'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gl ON gl.contracts_id = g.id AND IFNULL(gl.tech_account_id, '') <> ''
  JOIN contacts_cstm AS cstm ON cstm.id_c = gl.tech_account_id
  JOIN contacts AS c ON c.id = cstm.id_c
WHERE
  g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  AND 
(  
  IFNULL(c.last_name, '') != ''
  OR IFNULL(cstm.name_2_c, '') != ''
  OR IFNULL(cstm.division_1_c, '') != ''
  OR IFNULL(c.title, '') != ''
  OR IFNULL(c.phone_work, '') != ''
  OR IFNULL(c.phone_fax, '') != ''
  OR IFNULL(cstm.division_2_c, '') != ''
  OR IFNULL(cstm.title_2_c, '') != ''
  OR IFNULL(cstm.ext_no_c, '') != ''
  OR IFNULL(c.phone_mobile, '') != ''
  OR IFNULL(cstm.c_country_id_c, '') != ''
  OR IFNULL(cstm.postal_no_c, '') != ''
  OR IFNULL(cstm.addr_1_c, '') != ''
  OR IFNULL(cstm.addr_2_c, '') != ''
  OR IFNULL(cstm.attention_line_c, '') != ''
  OR IFNULL(cstm.addr_general_c, '') != ''
  OR IFNULL(cstm.city_general_c, '') != ''
  OR IFNULL(cstm.c_state_id_c, '') != ''
  OR EXISTS 
	(SELECT 1 FROM email_addr_bean_rel emr
		JOIN email_addresses em ON em.id = emr.email_address_id AND IFNULL(em.email_address, '') <> ''
	    WHERE emr.bean_id = c.id
	)
  OR
  EXISTS 
	(SELECT 1 FROM js_accounts_js js
		WHERE js.contact_id_c = c.id 
			AND IFNULL(js.name, '') != ''
			AND IFNULL(js.description, '') != ''
			AND IFNULL(js.addr1, '') != ''
			AND IFNULL(js.addr2, '') != ''
	)
)
ORDER BY g.global_contract_id
;
 
--
-- Agreement period
-- 

SELECT 
"SELECT
  g.global_contract_id     AS 'Global Contract Id',
  g.id                     AS 'Contract Object Id',
  g.contract_version	   AS 'Contract Version',
  gln.service_start_date   AS 'Service Start Date',
  gln.service_end_date	   AS 'Service End Date'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
WHERE
   g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
   AND 
    IFNULL(g.contract_startdate, '0000-00-00 00:00:00') = '0000-00-00 00:00:00' 
    AND 
    IFNULL(g.contract_enddate, '0000-00-00 00:00:00') = '0000-00-00 00:00:00'
  AND 
  (
   IFNULL(gln.service_start_date, '0000-00-00 00:00:00') = '0000-00-00 00:00:00'
   OR 
   IFNULL(gln.service_end_date, '0000-00-00 00:00:00') = '0000-00-00 00:00:00'
   OR 
   gln.service_start_date > gln.service_end_date
   )
ORDER BY 
   g.global_contract_id
;" AS ''
;

SELECT
  g.global_contract_id     AS 'Global Contract Id',
  g.id                     AS 'Contract Object Id',
  g.contract_version	   AS 'Contract Version',
  gln.service_start_date   AS 'Service Start Date',
  gln.service_end_date	   AS 'Service End Date'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
WHERE
   g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
   AND 
    IFNULL(g.contract_startdate, '0000-00-00 00:00:00') = '0000-00-00 00:00:00' 
    AND 
    IFNULL(g.contract_enddate, '0000-00-00 00:00:00') = '0000-00-00 00:00:00'
  AND 
  (
   IFNULL(gln.service_start_date, '0000-00-00 00:00:00') = '0000-00-00 00:00:00'
   OR 
   IFNULL(gln.service_end_date, '0000-00-00 00:00:00') = '0000-00-00 00:00:00'
   OR 
   gln.service_start_date > gln.service_end_date
   )
ORDER BY 
   g.global_contract_id
;

--
-- Payment method
--

SELECT 
"SELECT
  g.global_contract_id     AS 'Global Contract Id',
  g.id                     AS 'Contract Object Id',
  gln.payment_type         AS 'Payment Type',
  dp.id                    AS 'DP Id',
  dp.name                  AS 'Bank Name',
  dp.bank_code             AS 'Bank Code',
  gln.bill_account_id	   AS 'Bill Account Id',
  c.last_name		       AS 'Account Name',
  cstm.name_2_c		       AS 'Account Name (Local)'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
        AND gln.payment_type = '00'
        AND g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  JOIN contacts_cstm AS cstm ON cstm.id_c = gln.bill_account_id
  JOIN c_country AS cty ON cty.id = cstm.c_country_id_c AND cty.country_code_numeric <> '392'
  JOIN contacts AS c ON c.id = cstm.id_c
  JOIN gc_directdeposit dp ON dp.contract_history_id = gln.id
WHERE
  (
       IFNULL(dp.name, '') != '' 
    OR IFNULL(dp.bank_code, '') != ''
    OR IFNULL(branch_code, '') != ''
    OR IFNULL(deposit_type, '') != ''
    OR IFNULL(account_no, '') != ''
    OR IFNULL(payee_contact_person_name_1, '') != ''
    OR IFNULL(payee_contact_person_name_2, '') != ''
    OR IFNULL(payee_tel_no, '') != ''
    OR IFNULL(payee_email_address, '') != ''
    OR IFNULL(remarks, '') != ''
  )
ORDER BY g.global_contract_id  
;" AS ''
;

SELECT
  g.global_contract_id     AS 'Global Contract Id',
  g.id                     AS 'Contract Object Id',
  gln.payment_type         AS 'Payment Type',
  dp.id                    AS 'DP Id',
  dp.name                  AS 'Bank Name',
  dp.bank_code             AS 'Bank Code',
  gln.bill_account_id	   AS 'Bill Account Id',
  c.last_name		       AS 'Account Name',
  cstm.name_2_c		       AS 'Account Name (Local)'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
        AND gln.payment_type = '00'
        AND g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  JOIN contacts_cstm AS cstm ON cstm.id_c = gln.bill_account_id
  JOIN c_country AS cty ON cty.id = cstm.c_country_id_c AND cty.country_code_numeric <> '392'
  JOIN contacts AS c ON c.id = cstm.id_c
  JOIN gc_directdeposit dp ON dp.contract_history_id = gln.id
WHERE
  (
       IFNULL(dp.name, '') != '' 
    OR IFNULL(dp.bank_code, '') != ''
    OR IFNULL(branch_code, '') != ''
    OR IFNULL(deposit_type, '') != ''
    OR IFNULL(account_no, '') != ''
    OR IFNULL(payee_contact_person_name_1, '') != ''
    OR IFNULL(payee_contact_person_name_2, '') != ''
    OR IFNULL(payee_tel_no, '') != ''
    OR IFNULL(payee_email_address, '') != ''
    OR IFNULL(remarks, '') != ''
  )
ORDER BY g.global_contract_id  
;


--
-- Billing end date
--

SELECT 
"SELECT
  g.global_contract_id     AS 'Global Contract Id',
  g.id                     AS 'Contract Object Id',
  gln.billing_start_date   AS 'Billing Start date',
  gln.billing_end_date 	   AS 'Billing End date'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
  JOIN
     (SELECT tz.utcoffset, c.id FROM c_country c 
         JOIN gc_timezone tz ON tz.c_country_id_c = c.id
	  WHERE	   
	     c.country_code_numeric = '392'
	 ) AS jst 
  JOIN gc_timezone AS tz ON tz.id = gln.gc_timezone_id3_c 
	 AND tz.utcoffset = jst.utcoffset
	 AND (IFNULL(tz.c_country_id_c, '') = '' OR tz.c_country_id_c = jst.id)
WHERE
    g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
    AND 
   (gln.billing_end_date = '9999-12-31 14:59:59' OR gln.billing_end_date = '9999-12-31 06:00:00')
   AND 
   (gln.billing_start_date > gln.billing_end_date OR gln.billing_start_date > '9000-01-01 00:00:00')
  ORDER BY 1
;" AS ''
;


SELECT
  g.global_contract_id     AS 'Global Contract Id',
  g.id                     AS 'Contract Object Id',
  gln.billing_start_date   AS 'Billing Start date',
  gln.billing_end_date 	   AS 'Billing End date'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
  JOIN
     (SELECT tz.utcoffset, c.id FROM c_country c 
         JOIN gc_timezone tz ON tz.c_country_id_c = c.id
	  WHERE	   
	     c.country_code_numeric = '392'
	 ) AS jst 
  JOIN gc_timezone AS tz ON tz.id = gln.gc_timezone_id3_c 
	 AND tz.utcoffset = jst.utcoffset
	 AND (IFNULL(tz.c_country_id_c, '') = '' OR tz.c_country_id_c = jst.id)
WHERE
    g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
    AND 
   (gln.billing_end_date = '9999-12-31 14:59:59' OR gln.billing_end_date = '9999-12-31 06:00:00')
   AND 
   (gln.billing_start_date > gln.billing_end_date OR gln.billing_start_date > '9000-01-01 00:00:00')
  ORDER BY 1
;

--
--  Service end date
--

SELECT 
"SELECT
  g.global_contract_id     AS 'Global Contract Id',
  g.id                     AS 'Contract Object Id',
  gln.service_start_date   AS 'Service Start date',  
  gln.service_end_date 	   AS 'Service End date'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
  JOIN (SELECT tz.utcoffset, c.id FROM c_country c
	  JOIN gc_timezone tz ON tz.c_country_id_c = c.id
	WHERE
	   c.country_code_numeric = '392'
	) jst
  JOIN gc_timezone AS tz ON tz.id = gln.gc_timezone_id1_c 
	    AND tz.utcoffset = jst.utcoffset
		AND (IFNULL(tz.c_country_id_c, '') = '' OR tz.c_country_id_c = jst.id)
WHERE
  g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  AND
  (gln.service_end_date = '9999-12-31 14:59:59' OR gln.service_end_date = '9999-12-31 06:00:00')
  AND
  (gln.service_start_date > gln.service_end_date OR gln.service_start_date > '9000-01-01 00:00:00')
 ORDER BY 1
 ;" AS ''
;

SELECT
  g.global_contract_id     AS 'Global Contract Id',
  g.id                     AS 'Contract Object Id',
  gln.service_start_date   AS 'Service Start date',
  gln.service_end_date 	   AS 'Service End date'
FROM
  gc_contracts g
  JOIN gc_line_item_contract_history AS gln ON gln.contracts_id = g.id
  JOIN (SELECT tz.utcoffset, c.id FROM c_country c
	  JOIN gc_timezone tz ON tz.c_country_id_c = c.id
	WHERE
	   c.country_code_numeric = '392'
	) jst
  JOIN gc_timezone AS tz ON tz.id = gln.gc_timezone_id1_c 
	    AND tz.utcoffset = jst.utcoffset
		AND (IFNULL(tz.c_country_id_c, '') = '' OR tz.c_country_id_c = jst.id)
WHERE
  g.product_offering = '2808'
  AND g.global_contract_id <= 930000000002900
  AND
  (gln.service_end_date = '9999-12-31 14:59:59' OR gln.service_end_date = '9999-12-31 06:00:00')
  AND
  (gln.service_start_date > gln.service_end_date OR gln.service_start_date > '9000-01-01 00:00:00')
 ORDER BY 1
 ;
