-- =============================================
-- SQL for DF-1398
-- =============================================
/* change existing key name  with new key name */
UPDATE `app_settings` SET `key`='AccessControlExt_GMOne' WHERE `KEY` = 'AccessControlExt_gc_Contracts';

/*Data for new Enterprise Mail workFlow approver */
INSERT INTO `app_settings` (`key`, `value`) VALUES
('AccessControlExt_EnterpriseMail', '{"module_access":"gc_Contracts","1":"f2d0ef85-0b23-4fb9-8d6e-c57a37ed6aed"}');


-- =============================================
-- Validate the Table data
-- =============================================

SELECT "=== SELECT `key`, `value` FROM app_settings WHERE  `key` = 'AccessControlExt_GMOne';" AS '';
SELECT `key`, `value` FROM app_settings WHERE  `key` = 'AccessControlExt_GMOne';

SELECT "=== SELECT `key`, `value` FROM app_settings WHERE  `key` = 'AccessControlExt_EnterpriseMail';" AS '';
SELECT `key`, `value` FROM app_settings WHERE  `key` = 'AccessControlExt_EnterpriseMail';




