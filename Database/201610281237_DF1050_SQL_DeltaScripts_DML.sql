-- =============================================
--     SQL for DF-1050
-- =============================================

/* Data for the table `fields_meta_data` */
INSERT INTO `fields_meta_data` (`id`, `name`, `vname`, `comments`, `help`, `custom_module`, `type`, `len`, `required`, `default_value`, `date_modified`, `deleted`, `audited`, `massupdate`, `duplicate_merge`, `reportable`, `importable`, `ext1`, `ext2`, `ext3`, `ext4`) 
VALUES('Contactsattention_line_c','attention_line_c','LBL_ATTENTION_LINE','','','Contacts','varchar','100','0','','2016-10-27 11:47:51','0','1','0','0','0','true','','','','');

-- =============================================
--   Validate the Table data
-- =============================================
SELECT '=== SELECT * FROM fields_meta_data WHERE id = "Contactsattention_line_c";' AS '';
SELECT * FROM fields_meta_data WHERE id = "Contactsattention_line_c";