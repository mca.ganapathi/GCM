-- =============================================
--     Master Data DDL Statements
-- =============================================

--
-- Table structure for table `c_country`
--

DROP TABLE IF EXISTS `c_country`;
CREATE TABLE `c_country` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `team_id` char(36) DEFAULT NULL,
  `team_set_id` char(36) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `country_code_alphabet` varchar(3) DEFAULT NULL,
  `country_code_numeric` varchar(3) DEFAULT NULL,
  `region` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_c_country_tmst_id` (`team_set_id`) USING BTREE,
  KEY `idx_c_country_date_modfied` (`date_modified`),
  KEY `idx_c_country_id_del` (`id`,`deleted`),
  KEY `idx_c_country_date_entered` (`date_entered`),
  KEY `idx_c_country_name_del` (`name`,`deleted`),
  KEY `idx_c_country_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `c_state`
--

DROP TABLE IF EXISTS `c_state`;
CREATE TABLE `c_state` (
  `id` char(36) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `team_id` char(36) DEFAULT NULL,
  `team_set_id` char(36) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `state_type` varchar(100) DEFAULT NULL,
  `state_code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_c_state_tmst_id` (`team_set_id`),
  KEY `idx_c_state_date_modfied` (`date_modified`),
  KEY `idx_c_state_id_del` (`id`,`deleted`),
  KEY `idx_c_state_date_entered` (`date_entered`),
  KEY `idx_c_state_name_del` (`name`,`deleted`),
  KEY `idx_c_state_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Relationship Table structure for tables `c_country` and `c_state`
--

CREATE TABLE `c_country_c_state_1_c` (
 `id` varchar(36) NOT NULL,
 `date_modified` datetime DEFAULT NULL,
 `deleted` tinyint(1) DEFAULT '0',
 `c_country_c_state_1c_country_ida` varchar(36) DEFAULT NULL,
 `c_country_c_state_1c_state_idb` varchar(36) DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `c_country_c_state_1_ida1` (`c_country_c_state_1c_country_ida`),
 KEY `c_country_c_state_1_alt` (`c_country_c_state_1c_state_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `gc_nttcomgroupcompany`
--

DROP TABLE IF EXISTS `gc_nttcomgroupcompany`;
CREATE TABLE `gc_nttcomgroupcompany` (
  `id` char(36) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_gc_nttcomgroupcompany_date_modfied` (`date_modified`),
  KEY `idx_gc_nttcomgroupcompany_id_del` (`id`,`deleted`),
  KEY `idx_gc_nttcomgroupcompany_date_entered` (`date_entered`),
  KEY `idx_gc_nttcomgroupcompany_name_del` (`name`,`deleted`),
  KEY `idx_gc_nttcomgroupcompany_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `gc_timezone`
--

DROP TABLE IF EXISTS `gc_timezone`;
CREATE TABLE `gc_timezone` (
  `id` char(36) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `c_country_id_c` char(36) DEFAULT NULL,
  `utcoffset` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_gc_timezone_date_modfied` (`date_modified`),
  KEY `idx_gc_timezone_id_del` (`id`,`deleted`),
  KEY `idx_gc_timezone_date_entered` (`date_entered`),
  KEY `idx_gc_timezone_name_del` (`name`,`deleted`),
  KEY `idx_gc_timezone_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `gc_organization`
--

DROP TABLE IF EXISTS `gc_organization`;
CREATE TABLE `gc_organization` (
  `id` char(36) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `organization_name_2_local` varchar(100) DEFAULT NULL,
  `organization_name_3_local` varchar(100) DEFAULT NULL,
  `organization_name_4_local` varchar(100) DEFAULT NULL,
  `organization_name_1_latin` varchar(100) DEFAULT NULL,
  `organization_name_2_latin` varchar(100) DEFAULT NULL,
  `organization_name_3_latin` varchar(100) DEFAULT NULL,
  `organization_name_4_latin` varchar(100) DEFAULT NULL,
  `organization_code_1` varchar(8) DEFAULT NULL,
  `organization_code_2` varchar(5) DEFAULT NULL,
  `sales_org_flag` tinyint(1) DEFAULT '0',
  `billing_org_flag` tinyint(1) DEFAULT '0',
  `service_org_flag` tinyint(1) DEFAULT '0',
  `billing_affiliate_code` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_gc_organization_date_modfied` (`date_modified`),
  KEY `idx_gc_organization_id_del` (`id`,`deleted`),
  KEY `idx_gc_organization_date_entered` (`date_entered`),
  KEY `idx_gc_organization_name_del` (`name`,`deleted`),
  KEY `idx_gc_organization_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- =============================================
--   Confirm Table structure
-- =============================================

SELECT '=== DESC c_country' AS 'Country Table Structure';      DESC c_country;
SELECT '=== DESC c_state' AS 'State Table Structure';      DESC c_state;
SELECT '=== DESC c_country_c_state_1_c' AS 'Country and State Relationship Table Structure';      DESC c_country_c_state_1_c;
SELECT '=== DESC gc_nttcomgroupcompany' AS 'NTT Com Group Company Table Structure';      DESC gc_nttcomgroupcompany;
SELECT '=== DESC gc_timezone' AS 'Time Zone TableStructure';      DESC gc_timezone;
SELECT '=== DESC gc_organization' AS 'Organization Table Structure';      DESC gc_organization;