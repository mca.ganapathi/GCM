--
-- UPG-73 workflow_actions DML Statements
--

UPDATE workflow_actions SET workflow_actions.deleted = 1 WHERE workflow_actions.id IN ('4f8e8324-0b23-4fb0-9b0f-d1fc312faa56', '514f9b9c-0b23-4fb8-8c5d-932b8ee24336', '5d665f48-0b23-4fbf-af72-d21f55251590', '77dd9ed6-0b23-4fb9-8ceb-b5b0cf2a5324', 'b07cfd5c-0b23-4fbb-84d7-07f4351a4693', 'b1808866-0b23-4fbe-9c9d-34ecf5d6dc6b');


--
-- UPG-73 workflow_actionshells DML Statements
--

UPDATE workflow_actionshells SET workflow_actionshells.deleted = 1 WHERE workflow_actionshells.id IN ('00d954bf-0b23-4fb4-a380-18a0941c00ca', '16b1bd4f-0b23-4fb3-8c96-2801725d5301', '2683a1c1-0b23-4fbb-854b-cef3f1778123', '3a031cb9-0b23-4fbb-a874-428edb8f7929', '69f710b5-0b23-4fbf-ad61-7ada752cd23c', '73f3daec-0b23-4fbf-8108-008b383992ec');


-- =============================================
--   Validate the Table data
-- =============================================

SELECT '=== SELECT * FROM workflow_actions WHERE id IN ("4f8e8324-0b23-4fb0-9b0f-d1fc312faa56", "514f9b9c-0b23-4fb8-8c5d-932b8ee24336", "5d665f48-0b23-4fbf-af72-d21f55251590", "77dd9ed6-0b23-4fb9-8ceb-b5b0cf2a5324", "b07cfd5c-0b23-4fbb-84d7-07f4351a4693", "b1808866-0b23-4fbe-9c9d-34ecf5d6dc6b");' AS '';

SELECT * FROM workflow_actions WHERE id IN ("4f8e8324-0b23-4fb0-9b0f-d1fc312faa56", "514f9b9c-0b23-4fb8-8c5d-932b8ee24336", "5d665f48-0b23-4fbf-af72-d21f55251590", "77dd9ed6-0b23-4fb9-8ceb-b5b0cf2a5324", "b07cfd5c-0b23-4fbb-84d7-07f4351a4693", "b1808866-0b23-4fbe-9c9d-34ecf5d6dc6b");


SELECT '=== SELECT * FROM workflow_actionshells WHERE id IN ("00d954bf-0b23-4fb4-a380-18a0941c00ca", "16b1bd4f-0b23-4fb3-8c96-2801725d5301", "2683a1c1-0b23-4fbb-854b-cef3f1778123", "3a031cb9-0b23-4fbb-a874-428edb8f7929", "69f710b5-0b23-4fbf-ad61-7ada752cd23c", "73f3daec-0b23-4fbf-8108-008b383992ec");' AS '';

SELECT * FROM workflow_actionshells WHERE id IN ("00d954bf-0b23-4fb4-a380-18a0941c00ca", "16b1bd4f-0b23-4fb3-8c96-2801725d5301", "2683a1c1-0b23-4fbb-854b-cef3f1778123", "3a031cb9-0b23-4fbb-a874-428edb8f7929", "69f710b5-0b23-4fbf-ad61-7ada752cd23c", "73f3daec-0b23-4fbf-8108-008b383992ec");